<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Hedtech.Identity.Mvc</name>
    </assembly>
    <members>
        <member name="T:Hedtech.Identity.Mvc.Controllers.SamlSsoController">
            <summary>
            The SAML SSO Controller resolves a SAML authentication Response into an authentication
            method callback that will define the context user. In the event of an authentication
            failure, the controller will call an SSO failure callback method to invalidate the
            session and the user's identity.
            </summary>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Controllers.SamlSsoController.#ctor(Hedtech.Identity.IdentityManagementSettings,slf4net.ILogger)">
            <summary>
            Initializes a new instance of the <see cref="T:Hedtech.Identity.Mvc.Controllers.SamlSsoController"/> class.
            </summary>
            <param name="settings">The Identity Management settings.</param>
            <param name="logger">The logger.</param>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Controllers.SamlSsoController.Index">
            <summary>
            The default POST endpoint for the SAML SSO controller that will consume an inbound SAMLResponse
            and RelayState for the purpose of processing a Service Provider's authentication callback method
            in an attempt to identify the context user and define their identity. If the SAMLResponse does
            not contain a valid <see cref="T:ComponentPro.Saml2.Response"/>, a valid <see cref="T:ComponentPro.Saml2.LogoutResponse"/> will be sought
            to perform the Service Provider's logout.
            </summary>
            <returns>On successful authentication, return a Redirect to the requested RelayState URL.
            On successful logout, return a Redirect to the requested RelayState URL.
            Default is a Redirect to the Home/Index action.</returns>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Controllers.SamlSsoController.ProcessLoginResponse">
            <summary>
            Processes the SAML login response.
            After validating the SAML token may be used and is intended for the Service Provider,
            the authentication callback method defined in the Identity Management settings will
            be called to instantiate the context user.
            </summary>
            <returns>
            Redirect to the Relaystate defined in the response.
            </returns>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Controllers.SamlSsoController.ProcessLogoutResponse">
            <summary>
            Processes the logout response.
            </summary>
            <returns>Redirect to the requested relay state, or the default page.</returns>
        </member>
        <member name="T:Hedtech.Identity.Mvc.IdentityAuthorizeAttribute">
            <summary>
            The Identity authorization attribute that indicates whether the <see cref="T:Hedtech.Identity.Mvc.Modules.SamlSsoModule"/> is enabled.
            </summary>
        </member>
        <member name="M:Hedtech.Identity.Mvc.IdentityAuthorizeAttribute.AuthorizeCore(System.Web.HttpContextBase)">
            <summary>
            When overridden, provides an entry point for custom authorization checks for the <see cref="T:Hedtech.Identity.Mvc.Modules.SamlSsoModule"/>.
            </summary>
            <param name="httpContext">The HTTP context, which encapsulates all HTTP-specific information about an individual HTTP request.</param>
            <returns>
            true if the user is authorized or if the <see cref="T:Hedtech.Identity.Mvc.Modules.SamlSsoModule"/> is not enabled; otherwise, false.
            </returns>
        </member>
        <member name="T:Hedtech.Identity.Mvc.Modules.SamlSsoModule">
            <summary>
            MVC Module that adds Identity Provider SSO to an application.
            </summary>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Modules.SamlSsoModule.Init(System.Web.HttpApplication)">
            <summary>
            Initializes the <see cref="T:Hedtech.Identity.Mvc.Modules.SamlSsoModule"/> module.
            </summary>
            <param name="httpApp">The HTTP application.</param>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Modules.SamlSsoModule.OnAuthentication(System.Object,System.EventArgs)">
            <summary>
            Called when an AuthenticateRequest event is triggered.
            If the application context user is not yet instantiated and the Identity Management settings indicate
            the SAML SSO is enabled, then a SAML AuthnRequest is generated and posted to the Identity Provider's URL.
            </summary>
            <param name="sender">An object that contains state information for the current request.</param>
            <param name="args">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Modules.SamlSsoModule.OnSignOut(System.Object,System.EventArgs)">
            <summary>
            Called when a PostRequestHandlerExecute event is triggered (after every request).
            If the application context user is instantiated and the Identity Management settings indicate
            the SAML SSO is enabled, then if the request's URL contains the Logout Path, then generate a
            SAML LogoutRequest that will perform a single-sign-out of the Identity Provider's session.
            </summary>
            <param name="sender">An object that contains state information for the current request.</param>
            <param name="args">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        </member>
        <member name="M:Hedtech.Identity.Mvc.Modules.SamlSsoModule.Dispose">
            <summary>
            Disposes of the resources (other than memory) used by the module.
            </summary>
        </member>
        <member name="P:Hedtech.Identity.Mvc.Modules.SamlSsoModule.ModuleName">
            <summary>
            Gets the name of the module.
            </summary>
            <value>
            The name of the module.
            </value>
        </member>
        <member name="T:Hedtech.Identity.Mvc.SamlActions">
            <summary>
            MVC static classes that support Identity Provider Single-Sign-On.
            </summary>
        </member>
        <member name="M:Hedtech.Identity.Mvc.SamlActions.SamlLoginActionResult(System.String)">
            <summary>
            Generates a SAML Login (Authentication) Request redirect.
            </summary>
            <param name="returnUrl">The return URL to redirect to after login.</param>
            <returns>Empty ActionResult.</returns>
        </member>
        <member name="M:Hedtech.Identity.Mvc.SamlActions.SamlLoginRedirect(System.Web.HttpApplication,System.String)">
            <summary>
            Generates a SAML Login (Authentication) Request redirect.
            </summary>
            <param name="application">The HTTP Application.</param>
            <param name="returnUrl">The return URL to redirect to after login.</param>
        </member>
        <member name="M:Hedtech.Identity.Mvc.SamlActions.SamlLogoutActionResult(System.String)">
            <summary>
            Generates a SAML Logout Request redirect.
            </summary>
            <param name="relayState">The return URL to redirect to after the IdP logout.</param>
            <returns>Empty ActionResult.</returns>
        </member>
        <member name="M:Hedtech.Identity.Mvc.SamlActions.SamlLogoutRedirect(System.Web.HttpApplication,System.String)">
            <summary>
            Generates a SAML Logout Request redirect.
            </summary>
            <param name="application">The HTTP application.</param>
            <param name="relayState">The return URL to redirect to after the IdP logout.</param>
        </member>
    </members>
</doc>
