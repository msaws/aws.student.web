﻿var path = require("path"),
    webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin');


function webpackConfigFactory(buildType, entryObj, outDirRelative) {
    
    var isColdDev = buildType === "development-cold",
        isHotDev = buildType === "development-hot",
        isDev = isColdDev || isHotDev,
        isProd = buildType === "production",
        isColdTest = buildType === "test-cold",
        isHotTest = buildType === "test-hot",
        isTest = isColdTest || isHotTest;

    var config = {
        resolve: {
            alias: {
                Site: path.resolve(__dirname, "./Scripts"),
                SiteContent: path.resolve(__dirname, "./Content"),
                TimeManagement: path.resolve(__dirname, "./Areas/TimeManagement/Scripts"),
                TimeManagementContent: path.resolve(__dirname, "./Areas/TimeManagement/Content"),
                BankingInformation: path.resolve(__dirname, "./Areas/HumanResources/Scripts/BankingInformation"),
                HumanResourcesContent: path.resolve(__dirname, "./Areas/HumanResources/Content"),
                jquery: path.resolve(__dirname, "./Scripts/jquery-1.8.3.js"), //note that jquery is configured as an external, so this doesn't do much at the moment
                ko: path.resolve(__dirname, "./Scripts/knockout-3.4.0.debug.js"),
                moment: path.resolve(__dirname, "./Scripts/moment.js"),
            }
        },
        entry: entryObj, //this is populated by the gulp file
        output: {
            filename: outDirRelative + (isDev ? "[name].bundle.js" : "[name].bundle.min.js"),
            path: path.resolve(__dirname, outDirRelative)

        },
        devtool: isProd ? "" : "eval",
        externals: {
            //Externals processing happens before resolving the rest of the webpack configuration.
            //removing this external would push jquery into the bundle
            "jquery": "jQuery",
            "ko": "ko",
            "moment": "moment",
        },
        module: {        
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [ 
                        {
                            loader: 'imports-loader?define=>false',
                        }
                    ]
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                cacheDirectory: true,
                                presets: [
                                    [
                                        require.resolve("babel-preset-env"), {
                                            "targets": {
                                                "browsers": ["last 2 versions", "ie >= 10"]
                                            }
                                        }
                                    ]
                                ]
                            }
                        }
                    ]
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: 'html-loader',
                            options: {
                                attrs: ["img:src"],
                                minimize: false,
                                root: "."
                            }
                        }
                    ] 
                },
                {
                    test: /\.(jpe?g|png|gif)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192,
                                emitFile: false,
                                name: function (absolutePath) {
                                    return "[path][name].[ext]";
                                }
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                        fallback: "style-loader", //use style loader in development
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: isDev,// ? true : false
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: isDev,// ? true : false,
                                    includePaths: [
                                        path.resolve(__dirname, "./Content"),
                                    ]
                                }
                            }
                        ]
                    })
                }
            ]
        },
        plugins: [
            //jquery is configured as an external at the moment.
            //this plugin would require jquery whenever the variable $, jquery or window.jquery are used,        
            new webpack.ProvidePlugin({ 
                $: "jquery",
                jquery: "jquery",
                "window.jquery": "jquery",
                ko: "ko",
                "window.ko": "ko"
            }),

            new ExtractTextPlugin({
                filename: outDirRelative + "[name].bundle.min.css",
                disable: isColdDev,// ? true : false, //use style loader in development
                allChunks: false,
            }),

            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                },
                mangle: false,
                sourceMap: !isProd,
                beautify: !isProd
            }),
            new webpack.DefinePlugin({
                __DEV__: isDev,
                __DEBUGTEST__: isHotTest,
            })
        ],
        profile: isDev,

        
    }
    return Object.assign(config);
}


module.exports = webpackConfigFactory;