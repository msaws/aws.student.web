﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
namespace Ellucian.Web.Student
{
    public static class GlobalResourceFiles
    {
        public static readonly string AdminResources = "AdminResources";
        public static readonly string DaysOfWeekResources = "DaysOfWeek";
        public static readonly string NavigationResources = "Navigation";
        public static readonly string SiteResources = "SiteResources";
        public static readonly string StudentResources = "StudentResources";
        public static readonly string UserAccountResources = "UserAccountResources";
        public static readonly string SharedConstants = "SharedConstants";
        public static readonly string UserProfileResources = "UserProfile";
        public static readonly string PersonProxyResources = "PersonProxyResources";
        public static readonly string HumanResourcesResources = "HumanResources";
        public static readonly string CourseResources = "Courses";
        public static readonly string CourseSearchResultResources = "CourseSearchResult";
        public static readonly string PersonResources = "PersonResources";
        public static readonly string SectionCourseCommonDetailsResources = "SectionCourseCommonDetails";
        public static readonly string ErrorMessageResources = "ErrorMessageResources";
        public static readonly string OrganizationalStructureResources = "OrganizationalStructure";
    }
}