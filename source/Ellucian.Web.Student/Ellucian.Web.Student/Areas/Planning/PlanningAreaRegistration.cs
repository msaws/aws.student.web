﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System.Web.Mvc;
using System.ComponentModel;
using Ellucian.Web.Student.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Colleague.Configuration.Licensing;

namespace Ellucian.Web.Student.Areas.Planning
{
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Planning)]
    public class PlanningAreaRegistration : LicenseAreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Planning";
            }
        }

        public override void LicensedRegistration(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Planning_default",
                "Planning/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional, controller = "Home" },
                new string[] { "Ellucian.Web.Student.Areas.Planning.Controllers" }
            );
        }
    }
}
