﻿// Copyright 2012 - 2015 Ellucian Company L.P. and its affiliates.
namespace Ellucian.Web.Student.Areas.Planning
{
    public static class PlanningResourceFiles
    {
        public static readonly string AdvisorResources = "Advisors";
        public static readonly string CourseResources = "Courses";
        public static readonly string CourseSearchResultResources = "CourseSearchResult";
        public static readonly string DegreePlanResources = "DegreePlans";
        public static readonly string DetailResources = "Details";
        public static readonly string FacultyResources = "Faculty";
        public static readonly string PlanningHomeResources = "PlanningHome";
        public static readonly string ProgramResources = "Programs";
        public static readonly string TestScoreResources = "TestScores";
        public static readonly string TranscriptResources = "Transcripts";
        public static readonly string GraduationResources = "Graduation";

    }
}