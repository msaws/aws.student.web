﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Planning.Models
{
    public class DepartmentModel : IComparable<DepartmentModel>
    {
        public string Code { get; set; }
        public string Description { get; set; }

        public int CompareTo(DepartmentModel other)
        {
            // Compare the descriptions (Associates of Math, etc), as that's what most people would expect used in an alpha sort of departments.
            return this.Description.CompareTo(other.Description);
        }
    }
}