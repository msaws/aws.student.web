﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models
{
    /// <summary>
    /// Information for a particular test taken by a student
    /// </summary>
    public class TestScoreViewModel
    {
        /// <summary>
        /// Description of the test
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Date the test was taken
        /// </summary>
        public string DateTaken { get; set; }
        /// <summary>
        /// Score received by the student
        /// </summary>
        public string Score { get; set; }
        /// <summary>
        /// Percentile of the student's score
        /// </summary>
        public int? Percentile { get; set; }
        /// <summary>
        /// Status of the test - generally indicates whether the test score is being applied or is only notational
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Indicates whether the score was accepted, notational, withdrawn or other
        /// </summary>
        public string StatusType { get; set; }
        /// <summary>
        /// Date the status was applied to the test
        /// </summary>
        public string StatusDate { get; set; }
        /// <summary>
        /// Type of test - whether a sub test of another or a main test
        /// </summary>
        public string TestType { get; set; }

        /// <summary>
        /// Constructor for the test score view model
        /// </summary>
        /// <param name="description"></param>
        /// <param name="dateTaken"></param>
        public TestScoreViewModel(string description, DateTime dateTaken)
        {
            Description = description;
            DateTaken = dateTaken.ToShortDateString();
        }
    }
}