﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class StudentWaiverViewModel
    {

        /// <summary>
        /// Id of the student for whom the waiver is for
        /// </summary>C:\ellucian\StudentSelfService\Dev_Team_Student\Source\Ellucian.Web.Student\Ellucian.Web.Student\Areas\Planning\
        public string StudentId { get; set; }

                
        /// <summary>
        /// Date and Time the waiver was updated
        /// </summary>
        public string UpdatedOn { get; set; }

       /// <summary>
       /// Course
       /// </summary>
        public string Course {get;set;}
        /// <summary>
        /// Section
        /// </summary>
        public string Section { get; set; }
        /// <summary>
        /// Term
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// Start Date - only displayed when term is not provided
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// End Date - only used with start date when Term is not provided
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// prerequisite or co-requisite text
        /// </summary>
        public string Prerequisite { get; set; }
        /// <summary>
        /// status
        /// </summary>
        public string Status { get; set; }



        public StudentWaiverViewModel()
        {
       
        }
    }
}