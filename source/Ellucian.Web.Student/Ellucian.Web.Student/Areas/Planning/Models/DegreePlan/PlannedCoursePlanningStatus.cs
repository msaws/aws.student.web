﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public enum PlannedCoursePlanningStatus
    {
        IsInProgress,
        IsPreregistered,
        IsCompleted,
        IsWithdrawn,
        IsPlanned,
        IsWaitlisted,
        Undetermined
    }
}