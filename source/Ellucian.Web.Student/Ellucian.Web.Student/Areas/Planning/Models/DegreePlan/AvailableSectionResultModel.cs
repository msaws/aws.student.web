﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class AvailableSectionResultModel
    {
        /// <summary>
        /// Contains a list of available section results (one for each planned course supplied) that has the list of applicable PlannedSectionModels
        /// </summary>
        public List<AvailableSectionResult> AvailableSectionResults = new List<AvailableSectionResult>();
        /// <summary>
        /// Gets or sets the filter list of DaysOfWeek that was applied to this search.
        /// </summary>
        public List<FilterModel> DaysOfWeek = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Locations applied to this search.
        /// </summary>
        public List<FilterModel> Locations = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Faculty applied to this search.
        /// </summary>
        public List<FilterModel> Faculty = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list for the TimeOfDays applied to this search
        /// </summary>
        public List<FilterModel> TimeOfDay = new List<FilterModel>();

        /// <summary>
        /// Gets or sets the Availability filters (Open Sections Only or All)
        /// </summary>
        public List<FilterModel> Availability = new List<FilterModel>();

        /// <summary>
        /// Gets or sets the Topic filters
        /// </summary>
        public List<FilterModel> Topics = new List<FilterModel>();

        /// <summary>
        /// Constructor for the AvailableSectionResultModel
        /// </summary>
        /// <param name="coursePage">CourseSearch result</param>
        /// <param name="plannedCourses">List of PlannedCourses for which the available sections were requested. (Needed to exclude the planned course's section where applicable.)</param>
        /// <param name="courses">Course DTOs - used to construct the PlannedSectionModels</param>
        /// <param name="sections">Section DTOs - used to construct the PlannedSectionModels</param>
        /// <param name="locations">Location DTOs - used to construct the PlannedSectionModels</param>
        /// <param name="faculty">Faculty DTOs - used to construct the PlannedSectionModels</param>
        /// <param name="buildings">Building DTOs - used to construct the PlannedSectionModels</param>
        /// <param name="rooms">Room DTOs - used to construct the PlannedSectionModels</param>
        /// <param name="instructionalMethods">Instructional Method DTOs - used to construct the PlannedSectionModels</param>
        /// <param name="topics">Topic DTOs - used to construct the PlannedSectionModels</param>
        public AvailableSectionResultModel(CoursePage2 coursePage, IEnumerable<PlannedCourseSearch> plannedCourses, bool availableOnly, IEnumerable<Course2> courses, IEnumerable<Section3> sections, IEnumerable<Location> locations, IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty, IEnumerable<Building> buildings, IEnumerable<Room> rooms, IEnumerable<InstructionalMethod> instructionalMethods, IEnumerable<TopicCode> topics)
        {
            if (coursePage == null)
            {
                throw new ArgumentNullException("coursePage", "coursePage must not be null");
            }
            if (sections == null)
            {
                sections = new List<Section3>();
            }
            if (locations == null)
            {
                locations = new List<Location>();
            }
            if (faculty == null)
            {
                faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>();
            }
            if (buildings == null)
            {
                buildings = new List<Building>();
            }
            if (rooms == null)
            {
                rooms = new List<Room>();
            }
            if (instructionalMethods == null)
            {
                instructionalMethods = new List<InstructionalMethod>();
            }
            if (topics == null)
            {
                 topics = new List<TopicCode>();
            }
            this.AvailableSectionResults = new List<AvailableSectionResult>();

            // Update the filters

            // Availability - there is always just an open or all option here
            var openSections = sections.Where(s => (s.Waitlisted == 0 && (s.Available == null || s.Available > 0))).ToList();
            Availability.Add(new FilterModel() { Description = GlobalResources.GetString(PlanningResourceFiles.CourseSearchResultResources, "OpenSectionsOnly"), Selected = availableOnly, Value = "Open", Count = openSections.Count() });

            // Start and end times in minutes from midnight.  1am == 60.  9:45am == 585.  Find the category of times that the earliest time falls into and go with that.
            // No counts provided.
            // When a time filter was not selected both earliest time and latest time are 0.
            bool earlyTimeSelected = coursePage.LatestTime != 0 && coursePage.EarliestTime >= 0 && coursePage.EarliestTime < 480 ? true : false;
            bool morningTimeSelected = coursePage.EarliestTime >= 480  && coursePage.EarliestTime < 720 ? true : false;
            bool afternoonTimeSelected = coursePage.EarliestTime >= 720 && coursePage.EarliestTime < 960 ? true : false;
            bool eveningTimeSelected = coursePage.EarliestTime >= 960 && coursePage.EarliestTime < 1200 ? true : false;
            bool nightTimeSelected = coursePage.EarliestTime >= 1200 && coursePage.EarliestTime <= 1440 ? true : false;
            if (earlyTimeSelected || (coursePage.EarliestTime == 0 && coursePage.LatestTime == 0)) TimeOfDay.Add(new FilterModel() { Description = GlobalResources.GetString(PlanningResourceFiles.CourseSearchResultResources, "TimeFilterEarlyMorning"), Selected = earlyTimeSelected, Value = "Early" });
            if (morningTimeSelected || (coursePage.EarliestTime == 0 && coursePage.LatestTime == 0)) TimeOfDay.Add(new FilterModel() { Description = GlobalResources.GetString(PlanningResourceFiles.CourseSearchResultResources, "TimeFilterMorning"), Selected = morningTimeSelected, Value = "Morning" });
            if (afternoonTimeSelected || (coursePage.EarliestTime == 0 && coursePage.LatestTime == 0)) TimeOfDay.Add(new FilterModel() { Description = GlobalResources.GetString(PlanningResourceFiles.CourseSearchResultResources, "TimeFilterAfternoon"), Selected = afternoonTimeSelected, Value = "Afternoon" });
            if (eveningTimeSelected || (coursePage.EarliestTime == 0 && coursePage.LatestTime == 0)) TimeOfDay.Add(new FilterModel() { Description = GlobalResources.GetString(PlanningResourceFiles.CourseSearchResultResources, "TimeFilterEvening"), Selected = eveningTimeSelected, Value = "Evening" });
            if (nightTimeSelected || (coursePage.EarliestTime == 0 && coursePage.LatestTime == 0)) TimeOfDay.Add(new FilterModel() { Description = GlobalResources.GetString(PlanningResourceFiles.CourseSearchResultResources, "TimeFilterNight"), Selected = nightTimeSelected, Value = "Night" });

            foreach (var day in coursePage.DaysOfWeek)
            {
                var description = DayOfWeekConverter.IntToDayString(Int32.Parse(day.Value));
                DaysOfWeek.Add(new FilterModel { Count = day.Count, Description = description, Selected = day.Selected, Value = day.Value });
            }
            DaysOfWeek = DaysOfWeek.OrderBy(x => x.Value).ToList();

            foreach (var location in coursePage.Locations)
            {
                var description = locations.Where(x => location.Value == x.Code).FirstOrDefault() == null ? location.Value : locations.Where(x => location.Value == x.Code).FirstOrDefault().Description;
                if (!string.IsNullOrEmpty(description))
                { //description = "None Specified";
                    Locations.Add(new FilterModel { Count = location.Count, Description = description, Selected = location.Selected, Value = location.Value });
                }
            }
            Locations = Locations.OrderByDescending(x => x.Count).ThenBy(x => x.Description).ToList();

            foreach (var fac in coursePage.Faculty)
            {
                var description = faculty.Where(x => fac.Value == x.Id).FirstOrDefault() == null ? fac.Value : NameHelper.FacultyDisplayName(faculty.Where(x => fac.Value == x.Id).FirstOrDefault());
                Faculty.Add(new FilterModel { Count = fac.Count, Description = description, Selected = fac.Selected, Value = fac.Value });
            }
            Faculty = Faculty.OrderBy(x => x.Description).ToList();

            // If the search is only requested for sections with seat availability - drop out all closed sections from the list of sections returned.
            if (availableOnly)
            {
                sections = openSections;
            }

            //Work for topics
            foreach (var topic in coursePage.TopicCodes)
            {
                 var description = topics.Where(x => topic.Value == x.Code).FirstOrDefault() == null ? topic.Value : topics.Where(x => topic.Value == x.Code).FirstOrDefault().Description;
                 if (!string.IsNullOrEmpty(description))
                 {
                      Topics.Add(new FilterModel { Count = topic.Count, Description = description, Selected = topic.Selected, Value = topic.Value });
                 }
            }
            // Now update the results

            foreach (var plannedCourse in plannedCourses)
            {
                var availableSections = new List<PlannedSectionModel>();
                if (coursePage.CurrentPageItems != null || coursePage.CurrentPageItems.Count() > 0)
                {
                    var availableSectionIds = coursePage.CurrentPageItems.Where(c => c.Id == plannedCourse.CourseId).SelectMany(s => s.MatchingSectionIds);

                    if (plannedCourse.SectionId != null)
                    {
                        availableSectionIds = availableSectionIds.Except(new List<string> { plannedCourse.SectionId });
                    }

                    foreach (var sectionId in availableSectionIds)
                    {
                        Section3 section = sections.Where(sec => sec.Id == sectionId).FirstOrDefault();
                        Course2 course = courses.Where(c => c.Id == plannedCourse.CourseId).FirstOrDefault();
                        if (section != null)
                        {
                            availableSections.Add(CreateAvailablePlannedSection(section, course, faculty, rooms, buildings, instructionalMethods, locations));
                        }
                    }

                    availableSections = SortSectionList(availableSections).ToList();
                }
                AvailableSectionResults.Add(new AvailableSectionResult() { AvailableSections = availableSections, CourseId = plannedCourse.CourseId });
            }
        }

        private PlannedSectionModel CreateAvailablePlannedSection(Section3 sec, Course2 course, IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty, IEnumerable<Room> rooms, IEnumerable<Building> buildings, IEnumerable<InstructionalMethod> instructionalMethods, IEnumerable<Location> locations)
        {
            PlannedSectionModel sectionModel = new PlannedSectionModel(sec);

            if (course != null)
            {
                sectionModel.FormattedDisplay = course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + course.Number +
                    GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + sec.Number;
            }
            else
            {
                sectionModel.FormattedDisplay = "Unavailable";
            }

            var matchingLocation = locations.Where(l => l.Code == sec.Location).FirstOrDefault();
            sectionModel.LocationDisplay = matchingLocation != null ? matchingLocation.Description : string.Empty;
            List<string> facultyList = new List<string>();
            foreach (var facId in sec.FacultyIds)
            {
                Ellucian.Colleague.Dtos.Student.Faculty fac = faculty.Where(x => facId == x.Id).FirstOrDefault();
                string facultyName = string.Empty;
                if (fac != null)
                {
                    facultyName = NameHelper.FacultyDisplayName(fac);
                }
                else { facultyName = "Unavailable"; }
                facultyList.Add(facultyName);
            }
            sectionModel.Faculty = facultyList;
            List<PlannedSectionMeetingTimeModel> meetingList = new List<PlannedSectionMeetingTimeModel>();
            foreach (var meeting in sec.Meetings)
            {
                try
                {
                    meetingList.Add(new PlannedSectionMeetingTimeModel(meeting, instructionalMethods.ToList(), rooms.ToList(), buildings.ToList()));
                }
                catch
                {
                    // Meeting time model item not created (logging not available)
                }
            }
            sectionModel.PlannedMeetings = meetingList;

            // For available sections we are not filling out requisite warnings or timeconflict warnings.
            sectionModel.MeetingTimeConflict = false;
            sectionModel.Requisites = null;

            return sectionModel;
        }

        /// <summary>
        /// Sorts a set of fully-hydrated PlannedSectionModel objects by Number and then by Time (e.g. MATH-100-01 with an 8:00am meeting comes 
        /// before MATH-100-01 with a 10:00am meeting time). Number should be sufficient for most cases.
        /// </summary>
        /// <param name="sections">A set of unsorted PlannedSectionModel objects</param>
        /// <returns>The sorted set of PlannedSectionModel objects, sorted by Number and then Time</returns>
        private IEnumerable<PlannedSectionModel> SortSectionList(IEnumerable<PlannedSectionModel> sections)
        {
            if (sections == null || sections.Count() == 0)
            {
                return sections;
            }

            List<PlannedSectionModel> sorted = new List<PlannedSectionModel>();

            //we generally want to force anything with no or blank meeting times (i.e. the TBD ones) to the bottom of the list
            sorted.AddRange(sections.OrderBy(s => s.Number).ThenBy(s => s.PlannedMeetings == null || s.PlannedMeetings.Count() == 0 ? "ZZZ" :
                s.PlannedMeetings.OrderBy(m => string.IsNullOrEmpty(m.StartTimeIso8601) ? "ZZZ" : m.StartTimeIso8601).First().StartTimeIso8601));

            return sorted;
        }
    }
}