﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.Planning.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class PlannedCoursePreviewModel : CourseModel
    {
        /// <summary>
        /// Creates a new, empty PlannedCourseModel instance.
        /// </summary>
        public PlannedCoursePreviewModel()
            : base()
        {
            Credits = null;
            AcademicHistory = false;
        }

        /// <summary>
        /// Gets or sets the AcademicHistory record associated with this PlannedCourse.
        /// </summary>
        public bool AcademicHistory { get; set; }

        /// <summary>
        /// Gets or sets the specific amount of Credits that were taken for this PlannedCourse item. This can be null if no specific value was chosen,
        /// in which case the defaults will be pulled out of the Course and Section items.
        /// </summary>
        public Decimal? Credits { get; set; }

        /// <summary>
        /// Indicates the instance of this course in the plan (is it the first, second, etc time it appears in this term?)
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets a string representation of the Credits and CEUs attained for this planned course.
        /// </summary>
        new public string CreditsCeusDisplay
        {
            get
            {
                string creditsCeus = "0";

                decimal? credits = null;
                decimal? ceus = null;

                if (this.Credits.HasValue) credits = this.Credits.Value;

                else if (this.MinimumCredits.HasValue) credits = this.MinimumCredits.Value;

                if (this.Ceus.HasValue) ceus = this.Ceus.Value;

                if (ceus.HasValue && ceus.Value == 0) ceus = null;


                if (credits.HasValue && ceus.HasValue)
                {
                    creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeusAndCredits"), CreditsFormatter.FormattedCreditsString(credits.Value), CreditsFormatter.FormattedCreditsString(ceus.Value));
                }
                else if (credits.HasValue)
                {
                    creditsCeus = CreditsFormatter.FormattedCreditsString(credits.Value);
                }
                else if (ceus.HasValue)
                {
                    creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeus"), CreditsFormatter.FormattedCreditsString(ceus.Value));
                }

                return creditsCeus;
            }
        }

        /// <summary>
        /// Gets the full, formatted title display for this item, based on whether it is planned/registered, has a full or section or only a course, etc.
        /// </summary>
        public string FullTitleDisplay
        {
            get
            {
                string subject = string.Empty;
                string number = string.Empty;
                 string name = string.Empty;

                //course subject and number

                if (!string.IsNullOrEmpty(SubjectCode))
                {
                    subject = SubjectCode;
                }
                if (!string.IsNullOrEmpty(Number))
                {
                    number = Number;
                }

                //title
                name = Title;


                string title = string.Empty;

                if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty((number)))
                {
                    title = subject + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + number;
                }
                else if (!string.IsNullOrEmpty(subject))
                {
                    title = subject;
                }
                else if (!string.IsNullOrEmpty(number))
                {
                    title = number;
                }

                if (!string.IsNullOrEmpty(name))
                {
                    title += ": " + name;
                }

                return title;
            }
        }

        /// <summary>
        /// Gets the display title of the course, irrespective of whether this planned item has an attached section or academic credit object.
        /// </summary>
        public string CourseTitleDisplay
        {
            get
            {
                string title = string.Empty;

                title = SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + Number + ": " + Title;

                return title;
            }
        }
    }
}