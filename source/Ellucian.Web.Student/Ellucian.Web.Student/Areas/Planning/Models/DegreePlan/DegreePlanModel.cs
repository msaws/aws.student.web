﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Advising;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using AutoMapper;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Models.Courses;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents a full model that encapsulates a student's degree plan and related data, such as terms that can be added to the plan.
    /// </summary>
    public class DegreePlanModel
    {
        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;
        private const string _WITHDRAWN_TEXT = "Withdrawn";

        /// <summary>
        /// Gets the ID of the plan.
        /// </summary>
        public int Id { get { return DegreePlanDto != null ? DegreePlanDto.Id : 0; } }

        /// <summary>
        /// Gets the version flag on this plan.
        /// </summary>
        public int Version { get { return DegreePlanDto != null ? DegreePlanDto.Version : 0; } }

        /// <summary
        /// Gets or sets the actual degree plan DTO that forms the basis of this model.
        /// </summary>
        public Ellucian.Colleague.Dtos.Planning.DegreePlan4 DegreePlanDto { get; set; }

        /// <summary>
        /// True if the degree plan contains any planned courses
        /// </summary>
        public bool HasPlannedCourses
        {
            get
            {
                if (Terms != null)
                {
                    foreach (var term in Terms)
                    {
                        if (term.PlannedCourses != null && term.PlannedCourses.Count > 0)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// If the UILoadSampleDegreePlan label is empty, this flag is set to false, and the UI does not provide the Load Sample Course Plan button.
        /// </summary>
        public bool IsLoadSamplePlanAllowed
        {
            get
            {
                string label = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "UiLoadSampleDegreePlan");
                return (string.IsNullOrEmpty(label)) ? false : true;
            }
        }

        /// <summary>
        /// Gets or sets the list of notes associated with this advisor-advisee interaction.
        /// </summary>
        public IEnumerable<AdvisingNote> AdvisingNotes { get; set; }

        /// <summary>
        /// Gets or sets the ID of the person with whom this plan model is associated.
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// True if the student has an assigned advisor
        /// </summary>
        public bool StudentHasAdvisor { get; set; }

        /// <summary>
        /// Gets or sets the set of terms planned in this model. That is, terms that the student currently has on their plan.
        /// </summary>
        public List<PlannedTermModel> Terms { get; set; }

        /// <summary>
        /// Gets or sets the set of unplanned terms that can be added to the plan. That is, terms that do not exist on their student's plan but that
        /// can be added because they fall in the present or future. Completed terms cannot be added even if they were never on the plan.
        /// </summary>
        public List<Term> UnplannedTerms { get; set; }

        /// <summary>
        /// Gets or sets a list of notifications associated with the building of this plan model.
        /// </summary>
        public List<Notification> Notifications { get; set; }

        /// <summary>
        /// Gets or sets a term that should be focused immediately in the UI when this model is processed and displayed. Note that this should be the
        /// term description (such as Fall 2012), NOT the code.
        /// </summary>
        public string TermToShow { get; set; }

        /// <summary>
        /// Gets or sets the name of the person who last reviewed the associated plan.
        /// </summary>
        public string LastReviewedBy { get; set; }

        /// <summary>
        /// Gets or sets a string representation of the timestamp on which the associated plan was last reviewed.
        /// </summary>
        public string LastReviewedDate { get; set; }

        /// <summary>
        /// Assigned advisors for this student. 
        /// </summary>
        public List<StudentAdvisingModel> AdvisingContacts { get; set; }

        /// <summary>
        /// Index of each course and it's planning status (indicated by hardcoded C=Completed, I=In progress, W=Withdrawn, P=Planned)
        /// Java script uses this for quick access for showing the status of each course on the catalog for the given student's plan
        /// </summary>
        public List<PlannedCoursePlanningStatusModel> PlanningStatuses { get; set; }

        /// <summary>
        /// Indicates if there are any protected planned courses on the plan. If so the overall plan is protected and certain features are not 
        /// available to the student of the plan.
        /// </summary>
        public bool IsPlanProtected { get { return Terms.Any(t => t.IsTermProtected); } }

        /// <summary>
        /// Gets or sets the set of terms that can be cleared. That is, terms that have at least one planned course, 
        /// can be cleared because they fall in the present or future and have no academic credits (registration activity). 
        /// </summary>
        public List<TermModel> ClearableTerms
        {
            get
            {
                return Terms.Where(t => t.IsTermClearable).Select(plannedTermModel => new TermModel() { Code = plannedTermModel.Code, Description = plannedTermModel.Description }).ToList();
            }
        }

        /// <summary>
        /// The list of terms that are available for applying a sample plan
        /// </summary>
        public List<Term> AvailableSamplePlanTerms { get; set; }

        /// <summary>
        /// Creates a new, fully-hydrated instance of a DegreePlanModel using the provided data sets.
        /// </summary>
        /// <param name="degreePlan">The student's degree plan DTO</param>
        /// <param name="academicHistory">The student's academic history DTO</param>
        /// <param name="terms">The set of Term DTOs that appear somewhere in the plan or history</param>
        /// <param name="registrationTerms">The set of Term DTOs that are open for registration</param>
        /// <param name="courses">The set of Course DTOs that appear somewhere in the plan or history</param>
        /// <param name="sections">The set of Section DTOs that appear somewhere in the plan or history</param>
        /// <param name="faculty">The set of Faculty DTOs that appear somewhere in the plan or history, attached to a Section</param>
        /// <param name="rooms">The set of Room DTOs that appear somewhere in the plan or history, attached to a Section</param>
        /// <param name="buildings">The set of Building DTOs that appear somewhere in the plan or history, attached to a Section</param>
        /// <param name="instructionalMethods">The set of InstructionalMethod DTOs that appear somewhere in the plan or history, attached to a Section</param>
        /// <param name="grades">The set of Grade DTOs that appear somewhere in history as a graded section</param>
        /// <param name="notifications">A set of notification messages to associate with this model</param>
        /// <param name="requirementsMapping">A mapping of requirement specifications by code</param>
        /// <param name="locations">A set of all Locations that may host a course</param>
        /// <param name="dpNotes">The set of fully-hydrated advising notes associated with the plan</param>
        public DegreePlanModel(IAdapterRegistry adapterRegistry, Ellucian.Colleague.Dtos.Planning.DegreePlan4 degreePlan, AcademicHistory3 academicHistory, IEnumerable<Term> terms,
            IEnumerable<Term> registrationTerms, IEnumerable<Course2> courses, IEnumerable<Section3> sections, IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty,
            IEnumerable<Room> rooms, IEnumerable<Building> buildings, IEnumerable<InstructionalMethod> instructionalMethods, IEnumerable<Grade> grades,
            List<Notification> notifications, Dictionary<string, string> requirementsMapping, IEnumerable<Location> locations, IEnumerable<AdvisingNote> dpNotes,
            Advisor lastReviewedBy, IEnumerable<Advisor> studentAdvisors, IEnumerable<Advisement> studentAdvisements, IEnumerable<AdvisorType> advisorTypes, IEnumerable<SectionRegistrationDate> sectionRegistrationDates)
        {
            _adapterRegistry = adapterRegistry;
            DegreePlanDto = degreePlan;
            AdvisingNotes = dpNotes;

            this.PersonId = degreePlan.PersonId;
            this.Terms = new List<PlannedTermModel>();
            this.UnplannedTerms = new List<Term>();
            this.AvailableSamplePlanTerms = new List<Term>();

            if (degreePlan.LastReviewedDate.HasValue)
            {
                this.LastReviewedDate = degreePlan.LastReviewedDate.GetValueOrDefault().ToShortDateString();
            }
            if (lastReviewedBy != null)
            {
                this.LastReviewedBy = NameHelper.PersonDisplayName(lastReviewedBy.LastName, lastReviewedBy.FirstName, lastReviewedBy.MiddleName, NameFormats.LastFirstMiddleInitial);
            }

            if (notifications == null)
            {
                this.Notifications = new List<Notification>();
            }
            else
            {
                this.Notifications = notifications;
            }

            // Add a "term" for the non-term coursework
            if (academicHistory.NonTermAcademicCredits.Count > 0)
            {
                var nonTermTerm = new PlannedTermModel();
                nonTermTerm.Description = GlobalResources.GetString(GlobalResourceFiles.StudentResources, "NonTermSectionHeaderComplete");
                nonTermTerm.Code = "NonTermPlaceholder";
                foreach (var ac in academicHistory.NonTermAcademicCredits)
                {
                    var course = courses.FirstOrDefault(c => c.Id == ac.CourseId);
                    var section = sections.FirstOrDefault(s => s.Id == ac.SectionId);
                    var secRegistrationDate = sectionRegistrationDates.Where(s => s.SectionId == ac.SectionId).FirstOrDefault();
                    var pcFromAc = CreatePlannedCourseFromAcademicCredit(ac, true, course, section, faculty, rooms, buildings, instructionalMethods, requirementsMapping, grades, sections, courses, locations, terms, null, null, secRegistrationDate);
                    if (pcFromAc != null)
                    {
                        var courseid = "";
                        if (course != null) { courseid = course.Id; }
                        pcFromAc.Count = nonTermTerm.PlannedCourses.Count(c => c.Id == courseid);
                        nonTermTerm.PlannedCourses.Add(pcFromAc);
                    }
                }
                this.Terms.Add(nonTermTerm);
            }

            terms = terms.OrderBy(t => (t == null) ? 0 : t.ReportingYear).ThenBy(t => (t == null) ? 0 : t.Sequence);
            foreach (var term in terms)
            {
                var plannedTermModel = new PlannedTermModel(term);

                bool isOnPlan = false;

                // Is there a term in the set of academic credit that mathes this term?
                var acadCreditTerm = academicHistory.AcademicTerms.FirstOrDefault(t => t.TermId == term.Code);
                if (acadCreditTerm != null)
                {
                    isOnPlan = true;
                    plannedTermModel.CompletedGpa = acadCreditTerm.GradePointAverage;
                    foreach (var ac in acadCreditTerm.AcademicCredits)
                    {
                        var course = courses.FirstOrDefault(c => c.Id == ac.CourseId);
                        var section = sections.FirstOrDefault(s => s.Id == ac.SectionId);
                        var secRegistrationDate = sectionRegistrationDates.Where(s => s.SectionId == ac.SectionId).FirstOrDefault();
                        var pcFromAc = CreatePlannedCourseFromAcademicCredit(ac, false, course, section, faculty, rooms, buildings, instructionalMethods, requirementsMapping, grades, sections, courses, locations, terms, plannedTermModel.StartDate, plannedTermModel.EndDate, secRegistrationDate);
                        if (pcFromAc != null)
                        {
                            if (course != null)
                            {
                                pcFromAc.Count = plannedTermModel.PlannedCourses.Count(c => c.Id == course.Id);
                            }
                            plannedTermModel.PlannedCourses.Add(pcFromAc);
                        }

                    }
                }

                // Is there a term on the degree plan that matches this term?
                var plannedTerm = degreePlan.Terms.FirstOrDefault(t => t.TermId == term.Code);
                if (plannedTerm != null)
                {
                    isOnPlan = true;
                    var courseOccurrenceCount = new Dictionary<string, int>();
                    // Ordered by descending section Id to ensure planned courses with sections are paired up with academic credits first
                    foreach (var pc in plannedTerm.PlannedCourses.OrderBy(x => x.CourseId).ThenByDescending(x => x.SectionId))
                    {
                        // Track the number of times this course comes up in the planned courses
                        if (courseOccurrenceCount.ContainsKey(pc.CourseId))
                        {
                            courseOccurrenceCount[pc.CourseId] += 1;
                        }
                        else
                        {
                            courseOccurrenceCount.Add(pc.CourseId, 0);
                            courseOccurrenceCount[pc.CourseId] += 1;
                        }
                        // There can be planned courses in a past term (which has been graded) that you never actually registered, and don't have grades.
                        // In these cases, show these courses, based on the following logic:
                        // 1. An academic credit will be "paired" with a planned course for the same section, or, if none with the same section, a planned course with no section.
                        // 2. A planned course that does not have any associated academic credit appears as a planned course in the term.
                        // If there are no items found where there is academic history with a section and this planned course item's course Id and section Id (or the pc section id is null)
                        if (!plannedTermModel.PlannedCourses.Where(x => x.AcademicHistory.Section != null).Any(cc => cc.Id == pc.CourseId && (cc.AcademicHistory.Section.Id == pc.SectionId || string.IsNullOrEmpty(pc.SectionId))))
                        {
                            var approval = DegreePlanDto.Approvals.OrderByDescending(x => x.Date).Where(x => x.CourseId == pc.CourseId && x.TermCode == plannedTermModel.Code).FirstOrDefault();
                            var count = plannedTermModel.PlannedCourses.Count(c => c.Id == pc.CourseId);
                            plannedTermModel.PlannedCourses.Add(CreatePlannedCourse(pc, false, approval, count, courses, sections, faculty, rooms, buildings, instructionalMethods, requirementsMapping, locations, terms, plannedTermModel.StartDate, plannedTermModel.EndDate, sectionRegistrationDates));
                        }

                        // If the planned course has a matching acad credit, we need to update the approval status of the planned course in the outbound degree plan model
                        if (plannedTermModel.PlannedCourses.Where(x => x.AcademicHistory.Section != null).Any(cc => cc.Id == pc.CourseId && (cc.AcademicHistory.Section.Id == pc.SectionId || string.IsNullOrEmpty(pc.SectionId))))
                        {
                            var matches = plannedTermModel.PlannedCourses.Where(x => x.AcademicHistory.Section != null).Where(cc => cc.Id == pc.CourseId && (cc.AcademicHistory.Section.Id == pc.SectionId || string.IsNullOrEmpty(pc.SectionId)));
                            foreach (var match in matches)
                            {
                                var count = plannedTermModel.PlannedCourses.Count(c => c.Id == pc.CourseId);
                                var approval = DegreePlanDto.Approvals.OrderByDescending(x => x.Date).Where(x => x.CourseId == pc.CourseId && x.TermCode == plannedTermModel.Code).FirstOrDefault();
                                match.ApprovalStatus = approval == null ? string.Empty : approval.Status.ToString();
                            }
                            // If there are more planned courses than items already in the model, add an entry for this planned course to the model
                            if (courseOccurrenceCount[pc.CourseId] > (plannedTermModel.PlannedCourses.Where(cc => cc.Id == pc.CourseId).Count()))
                            {
                                var count = plannedTermModel.PlannedCourses.Count(c => c.Id == pc.CourseId);
                                var approval = DegreePlanDto.Approvals.OrderByDescending(x => x.Date).Where(x => x.CourseId == pc.CourseId && x.TermCode == plannedTermModel.Code).FirstOrDefault();
                                plannedTermModel.PlannedCourses.Add(CreatePlannedCourse(pc, false, approval, count, courses, sections, faculty, rooms, buildings, instructionalMethods, requirementsMapping, locations, terms, plannedTermModel.StartDate, plannedTermModel.EndDate, sectionRegistrationDates));
                            }
                        }
                    }
                }

                // Are there any planned non-term courses that match this term?
                // If so, they go into the plannedTermModel, but marked to indicate that they are not term-based
                if (plannedTermModel.StartDate != null && plannedTermModel.EndDate != null)
                {
                    var nonTermPlannedCourses = degreePlan.NonTermPlannedCourses.Where(c => sections.Any(s => s.Id == c.SectionId && s.StartDate >= plannedTermModel.StartDate && s.StartDate <= plannedTermModel.EndDate));
                    if (nonTermPlannedCourses.Count() > 0)
                    {
                        isOnPlan = true;
                        foreach (var pc in nonTermPlannedCourses)
                        {
                            var approval = DegreePlanDto.Approvals.OrderByDescending(x => x.Date).Where(x => x.CourseId == pc.CourseId && x.TermCode == plannedTermModel.Code).FirstOrDefault();
                            var count = plannedTermModel.PlannedCourses.Count(c => c.Id == pc.CourseId);
                            var ac = academicHistory.NonTermAcademicCredits.Where(a => a.CourseId != null && a.SectionId != null && pc.CourseId != null && pc.SectionId != null && a.CourseId == pc.CourseId && a.SectionId == pc.SectionId).FirstOrDefault();
                            if (ac != null)
                            {
                                var course = courses.FirstOrDefault(c => c.Id == ac.CourseId);
                                var section = sections.FirstOrDefault(s => s.Id == ac.SectionId);
                                var secRegistrationDate = sectionRegistrationDates.Where(s => s.SectionId == ac.SectionId).FirstOrDefault();
                                var pcFromAc = CreatePlannedCourseFromAcademicCredit(ac, true, course, section, faculty, rooms, buildings, instructionalMethods, requirementsMapping, grades, sections, courses, locations, terms, plannedTermModel.StartDate, plannedTermModel.EndDate, secRegistrationDate);
                                if (pcFromAc != null)
                                {
                                    pcFromAc.Count = count;
                                    pcFromAc.ApprovalStatus = count > 0 || approval == null ? string.Empty : approval.Status.ToString();
                                    plannedTermModel.PlannedCourses.Add(pcFromAc);
                                }
                            }
                            else
                            {
                                plannedTermModel.PlannedCourses.Add(CreatePlannedCourse(pc, true, approval, count, courses, sections, faculty, rooms, buildings, instructionalMethods, requirementsMapping, locations, terms, plannedTermModel.StartDate, plannedTermModel.EndDate, sectionRegistrationDates));
                            }
                        }
                    }
                }

                // Is this a registration term?
                if (registrationTerms.Any(x => term.Code == x.Code))
                {
                    plannedTermModel.IsRegistrationTerm = true;
                }

                if (isOnPlan)
                {
                    this.Terms.Add(plannedTermModel);
                }
                else
                {
                    // neither planned, nor any credits, add it to the list of "terms that we can add"; this can only be present and future terms - past terms are not considered
                    if (term.StartDate > DateTime.Now || term.EndDate > DateTime.Now)
                    {
                        this.UnplannedTerms.Add(term);
                    }
                }
            }

            // Sort the terms and the items with them
            SortTermDetails();

            // Find the active term, start by finding a term that start before, and ends after, today
            this.TermToShow = string.Empty;
            foreach (PlannedTermModel term in this.Terms)
            {
                if (term.StartDate <= DateTime.Now && term.EndDate >= DateTime.Now)
                {
                    this.TermToShow = term.Code;
                    break;
                }
            }
            // If that fails, find the first term that starts after today
            if (string.IsNullOrEmpty(this.TermToShow))
            {
                foreach (PlannedTermModel term in this.Terms)
                {
                    if (term.StartDate >= DateTime.Now)
                    {
                        this.TermToShow = term.Code;
                        break;
                    }
                }
            }
            // If all else fails, use the first term available
            if (string.IsNullOrEmpty(this.TermToShow))
            {
                if (this.Terms == null || this.Terms.Count == 0)
                {
                    this.TermToShow = null;
                }
                else
                {
                    this.TermToShow = this.Terms.ElementAt(0).Code;
                }
            }

            #region Grade restriction messages
            if (academicHistory != null && academicHistory.GradeRestriction != null)
            {
                if (academicHistory.GradeRestriction.IsRestricted && academicHistory.GradeRestriction.Reasons != null)
                {
                    foreach (string reason in academicHistory.GradeRestriction.Reasons)
                    {
                        if (!string.IsNullOrEmpty(reason))
                        {
                            Notifications.Add(new Notification(reason, NotificationType.Warning, false));
                        }
                    }
                }
            }
            #endregion

            // Setup the Student's Assigned Advising Models, if any.
            AdvisingContacts = new List<StudentAdvisingModel>();
            if (studentAdvisements != null && studentAdvisements.Count() > 0)
            {
                foreach (var advisement in studentAdvisements)
                {
                    if (!string.IsNullOrEmpty(advisement.AdvisorId))
                    {
                        var studentAdvising = new StudentAdvisingModel();
                        var advisor = studentAdvisors.Where(a => a.Id == advisement.AdvisorId).FirstOrDefault();
                        if (advisor != null)
                        {
                            studentAdvising.Name = advisor.FirstName + " " + advisor.LastName;
                            if (advisor.EmailAddresses != null && advisor.EmailAddresses.Count() > 0)
                            {
                                studentAdvising.EmailAddress = advisor.EmailAddresses.FirstOrDefault();
                            }
                            // Translate the advisor type
                            var advisorType = advisorTypes.Where(at => at.Code == advisement.AdvisorType).FirstOrDefault();
                            studentAdvising.AdvisorType = advisorType != null ? "(" + advisorType.Description + ")" : string.Empty;
                            AdvisingContacts.Add(studentAdvising);
                        }
                    }
                }
            }

            // Now that degree plan model is built, build a list of the "latest" planning status of each course found
            // across all the terms. Planning status displays on each course in the course catalog.
            PlanningStatuses = new List<PlannedCoursePlanningStatusModel>();
            foreach (var term in this.Terms)
            {
                var termCoursePlanningStatus = new Dictionary<string, string>();
                bool isPriorTerm = term.EndDate < DateTime.Today || (term.StartDate < DateTime.Today && !term.IsRegistrationTerm);
                // Sort the planned courses by planning statuses in the enum defined sequence so that planned courses do not override
                // courses that are in progress, withdrawn or complete.
                foreach (var course in term.PlannedCourses.OrderByDescending(pc => pc.PlanningStatus))
                {
                    // Create a PlannedCoursePlanningStatusModel object only if the planning status has been determined as something we want to display.
                    if (course != null && course.PlanningStatus != PlannedCoursePlanningStatus.Undetermined)
                    {
                        var coursePlanningStatus = PlanningStatuses.Where(c => c.CourseId == course.Id).FirstOrDefault();
                        if (coursePlanningStatus == null)
                        {
                            coursePlanningStatus = new PlannedCoursePlanningStatusModel() { CourseId = course.Id };
                            PlanningStatuses.Add(coursePlanningStatus);
                        }
                        switch (course.PlanningStatus)
                        {
                            case PlannedCoursePlanningStatus.IsCompleted:
                                coursePlanningStatus.Class = "search-course-completed-banner";
                                coursePlanningStatus.Text = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "IsAttemptedCompleted");
                                break;
                            case PlannedCoursePlanningStatus.IsInProgress:
                                coursePlanningStatus.Class = "search-course-in-progress-banner";
                                coursePlanningStatus.Text = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "IsInProgress");
                                break;
                            case PlannedCoursePlanningStatus.IsPreregistered:
                                coursePlanningStatus.Class = "search-course-planned-banner";
                                coursePlanningStatus.Text = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "IsPreregistered");
                                break;
                            case PlannedCoursePlanningStatus.IsWaitlisted:
                                coursePlanningStatus.Class = "search-course-planned-banner";
                                if (isPriorTerm)
                                {
                                    // If course was planned in a prior term, use a different resource and build text that includes the term in which it was planned.
                                    coursePlanningStatus.Text = string.Format(GlobalResources.GetString(PlanningResourceFiles.CourseResources, "IsPlannedPriorTerm"), term.Description);
                                }
                                else if (course.SectionWaitlistStatus == GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ScheduleWaitlistStatusActive") || course.SectionWaitlistStatus == GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ScheduleWaitlistStatusPermissionToRegister"))
                                {
                                    // Display if waitlisted in an active term
                                    coursePlanningStatus.Text = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "IsWaitlisted");
                                }
                                break;
                            case PlannedCoursePlanningStatus.IsPlanned:
                                coursePlanningStatus.Class = "search-course-planned-banner";
                                coursePlanningStatus.Text = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "IsPlanned");
                                break;
                            case PlannedCoursePlanningStatus.IsWithdrawn:
                                coursePlanningStatus.Class = "search-course-withdrawn-banner";
                                coursePlanningStatus.Text = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "IsWithdrawn");
                                break;
                        }
                    }
                }
            }

            // Using the list of terms and degree plan information, determine the set of terms to which the user can apply a sample plan
            // Throw out any planning terms that have ended, or aren't default terms, or fall before the firstTermCode specified
            var futurePlanningTerms = terms.Where(t => t.EndDate > DateTime.Today);
            var defaultFuturePlanningTerms = futurePlanningTerms.Where(t => t.DefaultOnPlan == true);
            // create the list of terms that are ON THE PLAN that we can use to apply the sample
            // same first term applies, same end date check, but don't care if it's a default (because it's already on the plan)
            var availableFuturePlannedTerms = futurePlanningTerms.Where(t => degreePlan.Terms.Any(pt => pt.TermId == t.Code));
            AvailableSamplePlanTerms = defaultFuturePlanningTerms.Union(availableFuturePlannedTerms).OrderBy(t => t.ReportingYear).ThenBy(t => t.Sequence).ToList();

        }

        /// <summary>
        /// Creates a new, empty DegreePlanModel with the specified set of notifications (likely errors).
        /// </summary>
        /// <param name="notifications">A set of notification messages to associate with this model</param>
        public DegreePlanModel(List<Notification> notifications)
        {
            Terms = new List<PlannedTermModel>();
            if (notifications == null)
            {
                Notifications = new List<Notification>();
            }
            else
            {
                Notifications = notifications;
            }
        }

        #region Private creation methods
        private PlannedCourseModel CreatePlannedCourse(PlannedCourse4 pc, bool isNonTerm, DegreePlanApproval2 approval, int count, IEnumerable<Course2> courses, IEnumerable<Section3> sections, IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty, IEnumerable<Room> rooms, IEnumerable<Building> buildings, IEnumerable<InstructionalMethod> instructionalMethods, Dictionary<string, string> requirements, IEnumerable<Location> locations, IEnumerable<Term> terms, DateTime? termStartDate, DateTime? termEndDate, IEnumerable<SectionRegistrationDate> sectionRegistrationDates)
        {
            var course = courses.Where(c => c.Id == pc.CourseId).FirstOrDefault();
            PlannedCourseModel plannedCourseModel = null;
            var plannedCourseMapAdapter = _adapterRegistry.GetAdapter<Course2, PlannedCourseModel>();

            if (course != null)
            {
                plannedCourseModel = plannedCourseMapAdapter.MapToType(course);
                plannedCourseModel.IsNonTermSection = isNonTerm;
            }
            else
            {
                plannedCourseModel = plannedCourseMapAdapter.MapToType(new Course2() { Id = pc.CourseId, Description = "Course ID: " + pc.CourseId + " could not be found." });
                plannedCourseModel.IsNonTermSection = isNonTerm;
            }
            plannedCourseModel.Count = count;

            plannedCourseModel.ApprovalStatus = approval == null ? string.Empty : approval.Status.ToString();
            plannedCourseModel.Credits = pc.Credits;
            plannedCourseModel.SectionWaitlistStatus = pc.SectionWaitlistStatus.ToString();
            plannedCourseModel.IsProtected = pc.IsProtected;
            if (!String.IsNullOrEmpty(pc.SectionId))
            {
                Section3 sec = sections.Where(s => s.Id == pc.SectionId).FirstOrDefault();
                SectionRegistrationDate sectionRegistrationDate = sectionRegistrationDates.Where(s => s.SectionId == pc.SectionId).FirstOrDefault();
                if (sec != null)
                {
                    plannedCourseModel.SetSection(CreatePlannedSection(sec, pc, isNonTerm, courses, sections, faculty, rooms, buildings, instructionalMethods, requirements, locations, terms));
                }
                plannedCourseModel.CanRegisterForSection = SetCanRegisterForSection(sectionRegistrationDate);
                plannedCourseModel.CanDropSection = SetCanDropSection(sectionRegistrationDate);
            }

            plannedCourseModel.GradingType = pc.GradingType;

            #region All Other Warnings, Including Prerequisites
            if (pc.Warnings != null)
            {
                var invalidCreditsMessages = pc.Warnings.Where(m => m.Type == PlannedCourseWarningType.InvalidPlannedCredits);
                if (invalidCreditsMessages.Count() > 0)
                {
                    foreach (var message in invalidCreditsMessages)
                    {
                        plannedCourseModel.Warnings.Add(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "InvalidPlannedCredits"));
                    }
                }

                var courseOfferingConflictMessages = pc.Warnings.Where(m => m.Type == PlannedCourseWarningType.CourseOfferingConflict);
                if (courseOfferingConflictMessages.Count() > 0)
                {
                    foreach (var message in courseOfferingConflictMessages)
                    {
                        plannedCourseModel.Warnings.Add(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "TermOfferingConflict"));
                    }
                }

                var prereqsMessages = pc.Warnings.Where(m => m.Type == PlannedCourseWarningType.UnmetRequisite);
                plannedCourseModel.CorequisiteCourseWarnings = new List<CorequisiteCourseModel>();
                if (prereqsMessages.Count() > 0)
                {
                    foreach (var message in prereqsMessages)
                    {
                        if (message.Requisite != null && !string.IsNullOrEmpty(message.Requisite.RequirementCode))
                        {
                            plannedCourseModel.RequisiteWarnings.Add(BuildRequisiteWarning(message, requirements));
                        }
                        else if (message.Requisite != null && !string.IsNullOrEmpty(message.Requisite.CorequisiteCourseId))
                        {
                            // Corequisite in the old format - continue using the CorequisiteCourseModel. That ensures they will still be able to do a search on the link.
                            plannedCourseModel.CorequisiteCourseWarnings.Add(BuildCorequisiteCourseWarning(message, courses));
                        }
                        else if (message.SectionRequisite != null && message.SectionRequisite.CorequisiteSectionIds.Count() > 0)
                        {
                            plannedCourseModel.CorequisiteSectionWarnings.Add(BuildCorequisiteSectionWarning(message, courses, sections));
                        }
                    }
                }
            }
            #endregion

            return plannedCourseModel;
        }

        private RequisiteModel BuildRequisiteWarning(PlannedCourseWarning2 message, Dictionary<string, string> requirements)
        {
            var requisite = new RequisiteModel();
            if (!string.IsNullOrEmpty(message.Requisite.RequirementCode))
            {
                if (requirements.ContainsKey(message.Requisite.RequirementCode))
                {
                    string displayTextExtension = string.Empty;
                    if (message.Requisite != null)
                    {
                        // First determine which display text extensions to use.
                        if (message.Requisite.IsRequired == true && message.Requisite.CompletionOrder == RequisiteCompletionOrder.Previous)
                        {
                            displayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousRequisiteRequired");
                        }
                        if (message.Requisite.IsRequired == false && message.Requisite.CompletionOrder == RequisiteCompletionOrder.Previous)
                        {
                            displayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousRequisiteRecommended");
                        }
                        if (message.Requisite.IsRequired == true && message.Requisite.CompletionOrder == RequisiteCompletionOrder.Concurrent)
                        {
                            displayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRequired");
                        }
                        if (message.Requisite.IsRequired == false && message.Requisite.CompletionOrder == RequisiteCompletionOrder.Concurrent)
                        {
                            displayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRecommended");
                        }
                        if (message.Requisite.IsRequired == true && message.Requisite.CompletionOrder == RequisiteCompletionOrder.PreviousOrConcurrent)
                        {
                            displayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousOrConcurrentRequired");
                        }
                        if (message.Requisite.IsRequired == false && message.Requisite.CompletionOrder == RequisiteCompletionOrder.PreviousOrConcurrent)
                        {
                            displayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousOrConcurrentRecommended");
                        }
                    }
                    requisite = (new RequisiteModel() { DisplayText = requirements[message.Requisite.RequirementCode] + " " + displayTextExtension, RequisiteId = message.Requisite.RequirementCode });
                }
                else
                {
                    // If there is a requirement code but it cannot be found in the list of requirements show "unknown message".  Not showing the extension in this situation.
                    requisite = (new RequisiteModel() { DisplayText = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RequisiteUnsatisfiedUnknown"), RequisiteId = string.Empty });
                }
            }
            return requisite;
        }

        private CorequisiteCourseModel BuildCorequisiteCourseWarning(PlannedCourseWarning2 message, IEnumerable<Course2> courses)
        {
            CorequisiteCourseModel corequisiteCourse = null;
            if (!string.IsNullOrEmpty(message.Requisite.CorequisiteCourseId))
            {
                // Corequisite in the old format - continue using the CorequisiteCourseModel. That ensures they will still be able to do a search on the link.
                Course2 corequisite = courses.Where(c => c.Id == message.Requisite.CorequisiteCourseId).FirstOrDefault();
                string displayMessage = string.Empty;
                if (message.Requisite.IsRequired == true)
                {
                    displayMessage = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CorequisiteRequiredGeneric");
                }
                else
                {
                    displayMessage = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CorequisiteOptionalGeneric");
                }
                if (corequisite != null)
                {
                    if (!string.IsNullOrEmpty(corequisite.SubjectCode) && !string.IsNullOrEmpty(corequisite.Number) && !string.IsNullOrEmpty(corequisite.Title))
                    {
                        displayMessage += " " + corequisite.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + corequisite.Number;
                    }
                    corequisiteCourse = new CorequisiteCourseModel(corequisite) { IsRequired = message.Requisite.IsRequired, DisplayMessage = displayMessage };
                }
            }
            return corequisiteCourse;
        }


        private PlannedCourseModel CreatePlannedCourseFromAcademicCredit(AcademicCredit2 ac, bool isNonTerm, Course2 course, Section3 section,
            IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty, IEnumerable<Room> rooms, IEnumerable<Building> buildings, IEnumerable<InstructionalMethod> instructionalMethods,
            Dictionary<string, string> requirements, IEnumerable<Grade> grades, IEnumerable<Section3> sections, IEnumerable<Course2> courses, IEnumerable<Location> locations,
            IEnumerable<Term> terms, DateTime? termStartDate, DateTime? termEndDate, SectionRegistrationDate sectionRegistrationDate)
        {
            PlannedCourseModel plannedCourseModel = null;
            PlannedSectionModel sectionModel = null;

            var plannedCourseMapAdapter = _adapterRegistry.GetAdapter<Course2, PlannedCourseModel>();
            if (course != null)
            {
                plannedCourseModel = plannedCourseMapAdapter.MapToType(course);
            }
            else
            {
                plannedCourseModel = new PlannedCourseModel();
            }

            if (section != null)
            {
                sectionModel = new PlannedSectionModel(section);
                if (course != null)
                {
                    sectionModel.FormattedDisplay = course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + course.Number +
                        GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + section.Number;
                }
                var matchingLocation = locations.Where(l => l.Code == section.Location).FirstOrDefault();
                sectionModel.LocationDisplay = matchingLocation != null ? matchingLocation.Description : null;
                List<string> facultyList = new List<string>();
                foreach (var facId in section.FacultyIds)
                {
                    Ellucian.Colleague.Dtos.Student.Faculty fac = faculty.Where(x => facId == x.Id).FirstOrDefault();
                    string facultyName = string.Empty;
                    if (fac != null)
                    {
                        facultyName = NameHelper.FacultyDisplayName(fac);
                    }
                    else { facultyName = "Unavailable"; }
                    facultyList.Add(facultyName);
                }
                sectionModel.Faculty = facultyList;
                List<PlannedSectionMeetingTimeModel> meetingList = new List<PlannedSectionMeetingTimeModel>();
                foreach (var meeting in section.Meetings)
                {
                    try
                    {
                        meetingList.Add(new PlannedSectionMeetingTimeModel(meeting, instructionalMethods.ToList(), rooms.ToList(), buildings.ToList()));
                    }
                    catch
                    {
                        // Meeting time model item not created (logging not available)  
                    }
                }
                sectionModel.PlannedMeetings = meetingList;

                List<string> corequisiteSectionIds = new List<string>();
                int coreqsNumberNeeded = 0;
                foreach (var secReq in section.SectionRequisites)
                {
                    if (secReq.IsRequired)
                    {
                        corequisiteSectionIds.AddRange(secReq.CorequisiteSectionIds);
                        coreqsNumberNeeded += secReq.NumberNeeded;
                    }
                }
                sectionModel.SectionCorequisites = corequisiteSectionIds;
                sectionModel.SectionCorequisitesNumberNeeded = coreqsNumberNeeded;
                plannedCourseModel.CanRegisterForSection = SetCanRegisterForSection(sectionRegistrationDate);
                plannedCourseModel.CanDropSection = SetCanDropSection(sectionRegistrationDate);
            }
            var grade = grades.Where(g => g.Id == ac.VerifiedGradeId).FirstOrDefault();
            var hist = new AcademicHistoryModel()
            {
                CeusOverride = ac.ContinuingEducationUnits,
                CreditsOverride = ac.Credit,
                GradePoints = ac.GradePoints,
                TitleOverride = !string.IsNullOrEmpty(ac.Title) ? ac.Title : ac.CourseName,
                GradeDisplay = grade != null ? grade.LetterGrade : GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CompletedCourseNoGradeMarker"),
                Section = sectionModel,
                GradingType = ac.GradingType,
                IsNonTermSection = isNonTerm,
                CourseName = ac.CourseName,
                IsGraded = ac.HasVerifiedGrade,
                IsCompletedCredit = ac.IsCompletedCredit,
                IsWithdrawn = ac.Status == _WITHDRAWN_TEXT ? true : (grade == null ? false : grade.IsWithdraw)
            };

            if (plannedCourseModel != null)
            {
                plannedCourseModel.AcademicHistory = hist;
            }

            return plannedCourseModel;
        }

        private PlannedSectionModel CreatePlannedSection(Section3 sec, PlannedCourse4 pc, bool isNonTerm, IEnumerable<Course2> courses, IEnumerable<Section3> sections, IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty, IEnumerable<Room> rooms, IEnumerable<Building> buildings, IEnumerable<InstructionalMethod> instructionalMethods, Dictionary<string, string> requirements, IEnumerable<Location> locations, IEnumerable<Term> terms)
        {
            PlannedSectionModel sectionModel = new PlannedSectionModel(sec);
            if (pc != null)
            {
                Course2 course = courses.Where(c => c.Id == pc.CourseId).FirstOrDefault();
                if (course != null)
                {
                    sectionModel.FormattedDisplay = course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + course.Number +
                        GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + sec.Number;
                }
            }
            var matchingLocation = locations.Where(l => l.Code == sec.Location).FirstOrDefault();
            sectionModel.LocationDisplay = matchingLocation != null ? matchingLocation.Description : string.Empty;
            List<string> facultyList = new List<string>();
            foreach (var facId in sec.FacultyIds)
            {
                Ellucian.Colleague.Dtos.Student.Faculty fac = faculty.Where(x => facId == x.Id).FirstOrDefault();
                string facultyName = string.Empty;
                if (fac != null)
                {
                    facultyName = NameHelper.FacultyDisplayName(fac);
                }
                else { facultyName = "Unavailable"; }
                facultyList.Add(facultyName);
            }
            sectionModel.Faculty = facultyList;
            List<PlannedSectionMeetingTimeModel> meetingList = new List<PlannedSectionMeetingTimeModel>();
            foreach (var meeting in sec.Meetings)
            {
                try
                {
                    meetingList.Add(new PlannedSectionMeetingTimeModel(meeting, instructionalMethods.ToList(), rooms.ToList(), buildings.ToList()));
                }
                catch
                {
                    // Meeting time model item not created (logging not available)
                }
            }
            sectionModel.PlannedMeetings = meetingList;

            if (pc == null)
            {
                sectionModel.MeetingTimeConflict = false;
                sectionModel.Requisites = null;
            }
            else
            {
                // time conflict messages
                var timeConflictMessages = pc.Warnings.Where(m => m.Type == PlannedCourseWarningType.TimeConflict);
                if (timeConflictMessages.Count() > 0)
                {
                    var section = sections.Where(s => s.Id == timeConflictMessages.First().SectionId).FirstOrDefault();
                    if (section != null)
                    {
                        var course = courses.Where(c => c.Id == section.CourseId).FirstOrDefault();
                        if (string.IsNullOrEmpty(section.TermId))
                        {
                            sectionModel.MeetingTimeConflictMessage = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiSectionTimeConflictMessageSpecific"),
                                course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + course.Number +
                                GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + section.Number,
                                GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiSectionTimeConflictMessageNonTermSection"));
                        }
                        else
                        {
                            if (section.TermId != pc.TermId)
                            {
                                var term = terms.Where(t => t.Code == section.TermId).FirstOrDefault();
                                if (term != null && course != null)
                                {
                                    sectionModel.MeetingTimeConflictMessage = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiSectionTimeConflictMessageSpecificOtherTerm"),
                                        course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + course.Number +
                                        GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + section.Number, term.Description);
                                }
                            }
                            else
                            {
                                sectionModel.MeetingTimeConflictMessage = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiSectionTimeConflictMessageSpecific"),
                                    course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + course.Number +
                                    GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + section.Number);
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(sectionModel.MeetingTimeConflictMessage))
                    {
                        sectionModel.MeetingTimeConflictMessage = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiSectionTimeConflictMessage");
                    }
                    sectionModel.MeetingTimeConflict = true;
                }
            }

            return sectionModel;
        }

        private CorequisiteSectionModel BuildCorequisiteSectionWarning(PlannedCourseWarning2 warning, IEnumerable<Course2> courses, IEnumerable<Section3> sections)
        {
            var sectionCorequisiteWarning = new CorequisiteSectionModel();

            if (warning.SectionRequisite != null && warning.SectionRequisite.CorequisiteSectionIds.Count() > 0)
            {
                // Single required or recommended section
                if (warning.SectionRequisite.CorequisiteSectionIds.Count() == 1)
                {
                    Section3 secCorequisite = sections.Where(s => s.Id == warning.SectionRequisite.CorequisiteSectionIds.ElementAt(0)).FirstOrDefault();
                    if (secCorequisite != null)
                    {
                        string displayMessage = null;
                        if (warning.SectionRequisite.IsRequired == true)
                        {
                            displayMessage = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CorequisiteRequiredGeneric");
                        }
                        else
                        {
                            displayMessage = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CorequisiteOptionalGeneric");
                        }
                        if (!string.IsNullOrEmpty(secCorequisite.CourseId))
                        {
                            Course2 c = courses.Where(q => q.Id == secCorequisite.CourseId).FirstOrDefault();
                            if (c != null && !string.IsNullOrEmpty(c.SubjectCode) && !string.IsNullOrEmpty(c.Number) && !string.IsNullOrEmpty(secCorequisite.Number) && !string.IsNullOrEmpty(secCorequisite.Title))
                            {
                                displayMessage += " " + c.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + c.Number + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + secCorequisite.Number;
                            }
                        }
                        sectionCorequisiteWarning = new CorequisiteSectionModel()
                        {
                            IsRequired = false,
                            SectionIds = new List<string>() { secCorequisite.Id },
                            DisplayMessage = displayMessage
                        };
                    }

                }

                // Multisection corequisite, ie 2 out of 3 sections
                else
                {
                    var sectionItems = new List<Section3>();
                    var sectionNames = new List<string>();
                    foreach (var secId in warning.SectionRequisite.CorequisiteSectionIds)
                    {
                        if (!string.IsNullOrEmpty(secId))
                        {
                            Section3 secCorequisite = sections.Where(s => s.Id == secId).FirstOrDefault();
                            if (secCorequisite != null)
                            {
                                if (!string.IsNullOrEmpty(secCorequisite.CourseId))
                                {
                                    Course2 c = courses.Where(q => q.Id == secCorequisite.CourseId).FirstOrDefault();
                                    if (c != null && !string.IsNullOrEmpty(c.SubjectCode) && !string.IsNullOrEmpty(c.Number) && !string.IsNullOrEmpty(secCorequisite.Number) && !string.IsNullOrEmpty(secCorequisite.Title))
                                    {
                                        sectionItems.Add(secCorequisite);
                                        sectionNames.Add(c.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + c.Number + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + secCorequisite.Number);
                                    }
                                }
                            }
                        }
                    }
                    // Build the message
                    string displayMessage = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CorequisiteMultipleSections")
                        , warning.SectionRequisite.NumberNeeded) + " " + string.Join(", ", sectionNames.ToArray()) + " " + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRequired");

                    // Add this section requisite to the model
                    sectionCorequisiteWarning = new CorequisiteSectionModel()
                        {
                            IsRequired = true,
                            SectionIds = sectionItems.Select(s => s.Id).ToList(),
                            DisplayMessage = displayMessage
                        };
                }
            }
            return sectionCorequisiteWarning;
        }
        #endregion

        /// <summary>
        /// Sorts the collection of PlannedTermModel objects by ReportingYear and then by Sequence. 
        /// </summary>
        /// <param name="unsortedTerms">An IEnumerable of unsorted PlannedTermModel objects</param>
        /// <returns>A sorted IOrderedEnumerable collection of the given PlannedTermModel set</returns>
        private void SortTermDetails()
        {
            if (this.Terms != null)
            {
                foreach (var term in this.Terms)
                {
                    if (term.PlannedCourses.Count > 0)
                    {
                        //you have to do this ugly padding because the course number is actually a string and can be non-numeric
                        int longest = term.PlannedCourses.Max(c => (c == null || string.IsNullOrEmpty(c.Number)) ? 0 : c.Number.Length);
                        term.PlannedCourses = term.PlannedCourses.OrderBy(c => c == null ? string.Empty : c.SubjectCode)
                            .ThenBy(c => (c == null || string.IsNullOrEmpty(c.Number)) ? string.Empty.PadLeft(longest, '0') : c.Number.PadLeft(longest, '0'))
                            .ThenBy(c => (c == null || c.Section == null || string.IsNullOrEmpty(c.Section.Number)) ? string.Empty.PadLeft(longest, '0') : c.Section.Number.PadLeft(longest, '0'))
                            .ToList();
                    }
                }

                this.Terms.OrderBy(t => (t == null) ? 0 : t.ReportingYear).ThenBy(t => (t == null) ? 0 : t.Sequence).ToList();
            }
        }

        private bool SetCanRegisterForSection(SectionRegistrationDate sectionRegistrationDate)
        {
            // If no override date information is available for this section assume it's open for registration and let backend processing confirm.
            if (sectionRegistrationDate == null)
            {
                return true;
            }

            // The only time we will prevent registration for a section is when all of the registration periods have dates and today doesn't fall into any of them. 

            if (sectionRegistrationDate.PreRegistrationStartDate.HasValue && sectionRegistrationDate.PreRegistrationEndDate.HasValue &&
                sectionRegistrationDate.RegistrationStartDate.HasValue && sectionRegistrationDate.RegistrationEndDate.HasValue &&
                sectionRegistrationDate.AddStartDate.HasValue && sectionRegistrationDate.AddEndDate.HasValue &&
                (DateTime.Today < sectionRegistrationDate.PreRegistrationStartDate || DateTime.Today > sectionRegistrationDate.PreRegistrationEndDate) &&
                (DateTime.Today < sectionRegistrationDate.RegistrationStartDate || DateTime.Today > sectionRegistrationDate.RegistrationEndDate) &&
                (DateTime.Today < sectionRegistrationDate.AddStartDate || DateTime.Today > sectionRegistrationDate.AddEndDate)
                )
            {
                return false;
            }

            return true;
        }

        private bool SetCanDropSection(SectionRegistrationDate sectionRegistrationDate)
        {
            // If no override date information is available for this section assume a drop can be attempted and let backend processing confirm.
            if (sectionRegistrationDate == null)
            {
                return true;
            }
            // The only time we will prevent drop for a section is when all of the registration periods have dates (including drop) and today doesn't fall into any of them. 
            if (sectionRegistrationDate.PreRegistrationStartDate.HasValue && sectionRegistrationDate.PreRegistrationEndDate.HasValue &&
                sectionRegistrationDate.RegistrationStartDate.HasValue && sectionRegistrationDate.RegistrationEndDate.HasValue &&
                sectionRegistrationDate.AddStartDate.HasValue && sectionRegistrationDate.AddEndDate.HasValue &&
                (DateTime.Today < sectionRegistrationDate.PreRegistrationStartDate || DateTime.Today > sectionRegistrationDate.PreRegistrationEndDate) &&
                (DateTime.Today < sectionRegistrationDate.RegistrationStartDate || DateTime.Today > sectionRegistrationDate.RegistrationEndDate) &&
                (DateTime.Today < sectionRegistrationDate.AddStartDate || DateTime.Today > sectionRegistrationDate.AddEndDate) &&
                (DateTime.Today < sectionRegistrationDate.DropStartDate || DateTime.Today > sectionRegistrationDate.DropEndDate)
                )
            {
                return false;
            }

            return true;
        }
    }
}
