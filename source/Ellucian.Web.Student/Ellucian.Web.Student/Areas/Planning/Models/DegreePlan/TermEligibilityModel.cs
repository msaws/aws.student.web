﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class TermEligibilityModel
    {
        /// <summary>
        /// The identifier for this term
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// A message describing the student's ability to register
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Indicates whether or not the student register now (based on date, overrides, eligibility)
        /// </summary>
        public bool AllowRegistration { get; set; }

        /// <summary>
        /// Indicates whether or not the student can skip the waitlist (if there are open seats in the section AND a waitlist exists)
        /// </summary>
        public bool AllowSkippingWaitlist { get; set; }

        public TermEligibilityModel(string term, string message, bool allowRegistration, bool allowSkippingWaitlist)
        {
            Term = term;

            Message = message;

            AllowRegistration = allowRegistration;

            AllowSkippingWaitlist = allowSkippingWaitlist;
        }
    }
}