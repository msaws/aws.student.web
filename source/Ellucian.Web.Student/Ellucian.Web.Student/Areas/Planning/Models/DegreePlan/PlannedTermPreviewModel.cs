﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class PlannedTermPreviewModel
    {
        /// <summary>
        /// Gets or sets the code for this term, such as SP/12.
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the name/full description of this term, such as Spring 2012.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the date on which this term begins.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets the date on which this term ends.
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Gets or sets the reporting year associated with this term.
        /// </summary>
        public int ReportingYear { get; set; }
        /// <summary>
        /// Gets or sets the sequential identifier for this term, based on its reporting year.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Gets whether this term is currently open/active, based on the date.
        /// </summary>
        public bool IsTermActive { get { return (this.StartDate <= DateTime.Now) && (this.EndDate > DateTime.Now); } }

        /// <summary>
        /// Gets or sets the list of planned courses associated with this term.
        /// </summary>
        public List<PlannedCoursePreviewModel> PlannedCourses { get; set; }

        /// <summary>
        /// Gets the total amount of planned credits for this term.
        /// </summary>
        public decimal PlannedCreditsDecimal
        {
            get
            {
                decimal total = 0;

                if (this.PlannedCourses != null)
                {
                    foreach (PlannedCoursePreviewModel plannedCourse in this.PlannedCourses)
                    {
                        if (plannedCourse.AcademicHistory == false)
                        {
                            if (plannedCourse.Credits.HasValue)
                            {
                                total += plannedCourse.Credits.Value;
                            }
                            else if (plannedCourse.MinimumCredits.HasValue)
                            {
                                total += plannedCourse.MinimumCredits.Value;
                            }
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the planned credit display of this collection of plan items. This will be 0 if the collection does not include any courses.
        /// Note that this does NOT include continuing education units - use the PlannedCeusDisplay property for that,
        /// </summary>
        public string PlannedCreditsDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(PlannedCreditsDecimal);
            }
        }

        /// <summary>
        /// Gets the total number of planned CEUs for this term.
        /// </summary>
        public decimal PlannedCeusDecimal
        {
            get
            {
                decimal total = 0;

                if (this.PlannedCourses != null)
                {
                    foreach (PlannedCoursePreviewModel plannedCourse in this.PlannedCourses)
                    {
                        if (plannedCourse.AcademicHistory == false)
                        {
                            if (plannedCourse.Ceus.HasValue)
                            {
                                total += plannedCourse.Ceus.Value;
                            }
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the total CEUs display of this collection of plan items. This will be 0 if the collection does not include any courses.
        /// Note that this only accounts for CEUs, not Credits.
        /// </summary>
        public string PlannedCeusDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(PlannedCeusDecimal);
            }
        }

        /// <summary>
        /// Creates a PlannedTermModel instance based on the provided Term DTO.
        /// </summary>
        /// <param name="term"></param>
        public PlannedTermPreviewModel(Term term)
            : this()
        {
            if (term != null)
            {
                Code = term.Code;
                Description = term.Description;
                StartDate = term.StartDate;
                EndDate = term.EndDate;
                ReportingYear = term.ReportingYear;
                Sequence = term.Sequence;
            }
        }

        /// <summary>
        /// Creates an empty PlannedTermModel instance.
        /// </summary>
        public PlannedTermPreviewModel()
        {
            PlannedCourses = new List<PlannedCoursePreviewModel>();
        }
    }
}