﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Models;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Adapters;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents a fleshed out sample degree plan preview with full course and term information for the sample plan and indicators from the student's academic history. 
    /// </summary>
    public class DegreePlanPreviewModel
    {
        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Gets or sets the merged degree plan DTO that was built with the degree plan preview. This represents the
        /// degree plan with the sample plan applied - ready for update. 
        /// </summary>
        public Ellucian.Colleague.Dtos.Planning.DegreePlan4 DegreePlan { get; set; }

        /// <summary>
        /// Gets or sets the set of terms planned in this model. That is, terms that the student currently has on their plan.
        /// </summary>
        public List<PlannedTermPreviewModel> Terms { get; set; }

        public DegreePlanPreviewModel(IAdapterRegistry adapterRegistry, Ellucian.Colleague.Dtos.Planning.DegreePlan4 preview, Ellucian.Colleague.Dtos.Planning.DegreePlan4 mergedDegreePlan, AcademicHistory3 academicHistory, IEnumerable<Term> terms,
            IEnumerable<Course2> courses)
        {
            _adapterRegistry = adapterRegistry;
            DegreePlan = mergedDegreePlan;
            this.Terms = new List<PlannedTermPreviewModel>();
            IEnumerable<AcademicCredit2> academicCredits = GatherAcademicCreditsFromHistory(academicHistory);
            foreach (var degreePlanTerm in preview.Terms)
            {
                Term term = terms.Where(t => t.Code == degreePlanTerm.TermId).FirstOrDefault();
                if (term != null)
                {
                    PlannedTermPreviewModel plannedTerm = new PlannedTermPreviewModel(term);
                    foreach (var plannedCourse in degreePlanTerm.PlannedCourses)
                    {
                        var count = plannedTerm.PlannedCourses.Count(c => c.Id == plannedCourse.CourseId);
                        var plannedCoursePreviewModel = CreatePlannedCoursePreview(plannedCourse, count, courses, terms);
                        
                        if (plannedCoursePreviewModel != null)
                        {
                            // Has the student completed or enrolled in this course? Considered true if an academic history item exists.
                            if (academicCredits.Where(c => c.CourseId == plannedCourse.CourseId).Count() > 0)
                            {
                                plannedCoursePreviewModel.AcademicHistory = true;
                            }
                        }
                        plannedTerm.PlannedCourses.Add(plannedCoursePreviewModel);
                    }
                    this.Terms.Add(plannedTerm);
                }
            }
        }

        /// <summary>
        /// Creates a new Planned Course Preview item based on a Planned Course DTO.
        /// </summary>
        /// <param name="pc">The planned course DTO</param>
        /// <param name="count">Number of time this course is already on the term.</param>
        /// <param name="courses">Courses related to this sample plan preview</param>
        /// <param name="terms">Terms related to this sample plan preview</param>
        /// <returns></returns>
        private PlannedCoursePreviewModel CreatePlannedCoursePreview(PlannedCourse4 pc, int count, IEnumerable<Course2> courses, IEnumerable<Term> terms)
        {
            var course = courses.Where(c => c.Id == pc.CourseId).FirstOrDefault();
            PlannedCoursePreviewModel plannedCoursePreviewModel = null;
            var plannedCourseMapAdapter = _adapterRegistry.GetAdapter<Course2, PlannedCoursePreviewModel>();

            if (course != null)
            {
                plannedCoursePreviewModel = plannedCourseMapAdapter.MapToType(course);
             }
            else
            {
                plannedCoursePreviewModel = plannedCourseMapAdapter.MapToType(new Course2() { Id = pc.CourseId, Description = "Course ID: " + pc.CourseId + " could not be found." });
            }
            plannedCoursePreviewModel.Count = count;

            plannedCoursePreviewModel.Credits = pc.Credits;

            return plannedCoursePreviewModel;
        }

        private IEnumerable<AcademicCredit2> GatherAcademicCreditsFromHistory(AcademicHistory3 academicHistory)
        {
            List<AcademicCredit2> academicCredits = new List<AcademicCredit2>();
            academicCredits.AddRange(academicHistory.NonTermAcademicCredits);
            foreach (var term in academicHistory.AcademicTerms)
            {
                academicCredits.AddRange(term.AcademicCredits);
            }
            return academicCredits;
        }
    }
}