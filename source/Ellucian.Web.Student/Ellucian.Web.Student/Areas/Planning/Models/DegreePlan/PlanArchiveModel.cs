﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.using System;
using System;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class PlanArchiveModel
    {
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string StudentId { get; set; }
        public string Title { get; set; }
        public DateTime SortDate { get; set; }

        public PlanArchiveModel(int id, string studentId)
        {
            Id = id;
            StudentId = studentId;
        }
    }
}
