﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Linq;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Models.Courses;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents a planned item on the degree plan for a student. A planned item always contains a course, and may or may not contain a
    /// specific section depending on whether the student has chosen a specific one. If registered or completed there will also be associated
    /// AcademicHistory attached this PlannedCourse.
    /// </summary>
    public class PlannedCourseModel : CourseModel
    {
        /// <summary>
        /// Creates a new, empty PlannedCourseModel instance.
        /// </summary>
        public PlannedCourseModel()
            : base()
        {
            _Section = null;
            Credits = null;
            CorequisiteCourseWarnings = new List<CorequisiteCourseModel>();
            IsNonTermSection = false;
            Warnings = new List<string>();
            AcademicHistory = new AcademicHistoryModel();
            GradingType = Colleague.Dtos.Student.GradingType.Graded;
            RequisiteWarnings = new List<RequisiteModel>();
            CorequisiteSectionWarnings = new List<CorequisiteSectionModel>();
            // By default we will set the CanRegisterForSection and CanDropSection elements to false
            // but, ultimately, if there is a section on the planned course it will default to true unless the section override dates specifically indicate the applicable periods are not open.
            CanRegisterForSection = false;
            CanDropSection = false;
        }

        /// <summary>
        /// Gets or sets a list of warnings associate with this PlannedCourse, such as co-requisites not being met.
        /// </summary>
        public List<string> Warnings { get; set; }

        /// <summary>
        /// Gets or sets the AcademicHistory record associated with this PlannedCourse.
        /// </summary>
        public AcademicHistoryModel AcademicHistory { get; set; }

        /// <summary>
        /// Gets or sets the specific Section associated with this PlannedCourse. This can be null if the student has not yet chosen a concrete Section.
        /// </summary>
        private PlannedSectionModel _Section { get; set; }

        /// <summary>
        /// Sets the PlannedCourse's section attribute
        /// </summary>
        /// <param name="section"></param>
        public void SetSection(PlannedSectionModel section)
        {
            _Section = section;
        }

        /// <summary>
        /// Is the student on the waitlist for this planned course?
        /// </summary>
        public string SectionWaitlistStatus { get; set; }

        /// <summary>
        /// Gets or sets the specific amount of Credits that were taken for this PlannedCourse item. This can be null if no specific value was chosen,
        /// in which case the defaults will be pulled out of the Course and Section items.
        /// </summary>
        public Decimal? Credits { get; set; }

        /// <summary>
        /// Gets or sets a list of associated co-requisites for this PlannedCourse.
        /// </summary>
        public List<CorequisiteCourseModel> CorequisiteCourseWarnings { get; set; }

        /// <summary>
        /// Gets or sets the requisite for this PlannedCourse
        /// </summary>
        public List<RequisiteModel> RequisiteWarnings { get; set; }

        /// <summary>
        /// Gets or sets the full list of co-requisite sections attached to this planned section.
        /// </summary>
        public List<CorequisiteSectionModel> CorequisiteSectionWarnings { get; set; }

        /// <summary>
        /// Gets or sets whether this item represents a non-term-based section.
        /// </summary>
        public bool IsNonTermSection { get; set; }

        public string ApprovalStatus { get; set; }

        /// <summary>
        /// Indicates the instance of this course in the plan (is it the first, second, etc time it appears in this term?)
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets a string representation of the Credits and CEUs attained for this planned course.
        /// </summary>
        new public string CreditsCeusDisplay
        {
            get
            {
                string creditsCeus = "0";

                decimal? credits = null;
                decimal? ceus = null;
                if (this.Credits.HasValue)
                {
                    credits = this.Credits.Value;
                }
                else if (this.Section != null && this.Section.MinimumCredits.HasValue)
                {
                    credits = this.Section.MinimumCredits.Value;
                }
                else if (this.MinimumCredits.HasValue)
                {
                    credits = this.MinimumCredits.Value;
                }

                if (this.Section != null && this.Section.Ceus.HasValue)
                {
                    ceus = this.Section.Ceus.Value;
                }
                else if (this.Ceus.HasValue)
                {
                    ceus = this.Ceus.Value;
                }
                if (ceus.HasValue && ceus.Value == 0)
                {
                    ceus = null;
                }

                if (this.IsNonTermSection)
                {
                    if (credits.HasValue && ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeusAndCreditsNonTerm"), CreditsFormatter.FormattedCreditsString(credits.Value), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                    else if (credits.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCreditsNonTerm"), CreditsFormatter.FormattedCreditsString(credits.Value));
                    }
                    else if (ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeusNonTerm"), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                }
                else
                {
                    if (credits.HasValue && ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeusAndCredits"), CreditsFormatter.FormattedCreditsString(credits.Value), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                    else if (credits.HasValue)
                    {
                        creditsCeus = CreditsFormatter.FormattedCreditsString(credits.Value);
                    }
                    else if (ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeus"), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                }

                //special cases if Audit or Pass/Fail
                if (GradingType == Colleague.Dtos.Student.GradingType.Audit)
                {
                    creditsCeus = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCreditsAudited");
                }
                else if (GradingType == Colleague.Dtos.Student.GradingType.PassFail)
                {
                    creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCreditsPassFail"), creditsCeus);
                }

                return creditsCeus;
            }
        }

        /// <summary>
        /// Gets or sets the grading type selected for this planned course. This generally only applies to sections.
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public GradingType GradingType { get; set; }

        /// <summary>
        /// Gets the full, formatted title display for this item, based on whether it is planned/registered, has a full or section or only a course, etc.
        /// </summary>
        public string FullTitleDisplay
        {
            get
            {
                //course subject and number
                string title = string.Empty;
                if (!string.IsNullOrEmpty(AcademicHistory.CourseName))
                {
                    title = AcademicHistory.CourseName;
                }
                else if (!string.IsNullOrEmpty(SubjectCode) && !string.IsNullOrEmpty((Number)))
                {
                    title = SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + Number;
                }
                else if (!string.IsNullOrEmpty(SubjectCode))
                {
                    title = SubjectCode;
                }
                else if (!string.IsNullOrEmpty(Number))
                {
                    title = Number;
                }

                //section
                if (Section != null)
                {
                    if (string.IsNullOrEmpty(title))
                    {
                        title = Section.Number;
                    }
                    else
                    {
                        title += GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + Section.Number;
                    }
                }

                //title
                string name = "";
                if (!string.IsNullOrEmpty(AcademicHistory.TitleOverride))
                {
                    name = AcademicHistory.TitleOverride;
                }
                else if (_Section != null)
                {
                    name = _Section.Title;
                }
                else
                {
                    name = Title;
                }


                if (!string.IsNullOrEmpty(name))
                {
                    title += ": " + name;
                }

                return title;
            }
        }

        /// <summary>
        /// Gets the display title of the course, irrespective of whether this planned item has an attached section or academic credit object.
        /// </summary>
        public string CourseTitleDisplay
        {
            get
            {
                string title = string.Empty;

                title = SubjectCode + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + Number + ": " + Title;

                return title;
            }
        }

        /// <summary>
        /// Return true if this planned course has a section attached
        /// </summary>
        public bool HasSection
        {
            get
            {
                return _Section != null;
            }
        }

        /// <summary>
        /// Return true if this planned section has been registered
        /// </summary>
        public bool HasRegisteredSection
        {
            get
            {
                if (AcademicHistory.HasSection)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Retrieve the deepest populated section from this planned item - this can the AcademicHistory section, the main section, or null
        /// </summary>
        public PlannedSectionModel Section
        {
            get
            {
                return AcademicHistory.Section != null ? AcademicHistory.Section : _Section;
            }
        }

        /// <summary>
        /// Retrieves the deepest grading type value from this planned item - this can come from the AcademicHistory or the main PlannedCourse
        /// </summary>
        public GradingType GetGradingType
        {
            get
            {
                return AcademicHistory.Section != null ? AcademicHistory.GradingType : GradingType;
            }
        }

        /// <summary>
        /// Returns true if the planned course has a section with a completed academic credit that is marked completed that is not withdrawn. 
        /// </summary>
        public bool IsCompleted
        {
            get
            {
                return PlanningStatus == PlannedCoursePlanningStatus.IsCompleted;
            }
        }

        /// <summary>
        /// Returns true if the planned course is registered, but the section has not started. 
        /// </summary>
        public bool IsPreregistered
        {
            get
            {
                return PlanningStatus == PlannedCoursePlanningStatus.IsPreregistered;
            }
        }

        /// <summary>
        /// Returns true if the planned course has a section with completed academic credit but a withdrawn grade.
        /// </summary>
        public bool IsWithdrawn
        {
            get
            {
                return PlanningStatus == PlannedCoursePlanningStatus.IsWithdrawn;
            }
        }

        /// <summary>
        /// Returns true if the planned course has a registered section that it is not yet complete. It is therefore "in progress".
        /// </summary>
        public bool IsInProgress
        {
            get
            {
                return PlanningStatus == PlannedCoursePlanningStatus.IsInProgress;
            }
        }

        /// <summary>
        /// If there is no academic history for this item, it can only be a planned course.
        /// </summary>
        public bool IsPlanned
        {
            get
            {
                return PlanningStatus == PlannedCoursePlanningStatus.IsPlanned;
            }
        }

        /// <summary>
        /// If the user is on the waitlist for this class
        /// </summary>
        public bool IsWaitlisted
        {
            get
            {
                return PlanningStatus == PlannedCoursePlanningStatus.IsWaitlisted;
            }
        }

        public PlannedCoursePlanningStatus PlanningStatus
        {
            get
            {
                // If there is no academic history (registration activity), the course is just planned, regardless of whether a section is related
                if (AcademicHistory.Section == null)
                {
                    if (SectionWaitlistStatus == WaitlistStatus.Active.ToString() || SectionWaitlistStatus == WaitlistStatus.PermissionToRegister.ToString())
                    {
                        return PlannedCoursePlanningStatus.IsWaitlisted;
                    }
                    return PlannedCoursePlanningStatus.IsPlanned;
                }
                else if (HasRegisteredSection)
                {
                    
                    // The academic credit is considered completed if it has a verified grade or is a transfer/noncourse or there is
                    // no grade scheme or end date has passed.
                    if (!AcademicHistory.IsCompletedCredit)
                    {
                        // If the credit isn't completed, it can either be, withdrawn, in-progress or pre-registered
                        if (AcademicHistory.IsWithdrawn)
                        {
                            return PlannedCoursePlanningStatus.IsWithdrawn;
                        }
                        if (this.Section.StartDate <= DateTime.Now)
                        {
                            return PlannedCoursePlanningStatus.IsInProgress;
                        }
                        else
                        {
                            return PlannedCoursePlanningStatus.IsPreregistered;
                        }
                    }
                    else
                    {
                        // If the credit is completed, it can either be completed or withdrawn
                        if (!AcademicHistory.IsWithdrawn)
                        {
                            return PlannedCoursePlanningStatus.IsCompleted;
                        }
                        else
                        {
                            return PlannedCoursePlanningStatus.IsWithdrawn;
                        }
                    }
                }
                return PlannedCoursePlanningStatus.Undetermined;
            }
        }

        public bool HasMeetings
        {
            get
            {
                return (Section != null && Section.PlannedMeetings != null && Section.PlannedMeetings.Count() > 0);
            }
        }

        /// <summary>
        /// Indicates whether this planned course is protected so that only an authorized advisor or staff can remove or move it.
        /// </summary>
        public bool IsProtected { get; set; }

        /// <summary>
        /// If false, the section is missing registration date information, there are missing registration values for this section, OR today doesn't fall within any of the registration periods
        /// </summary>
        public bool CanRegisterForSection { get; set; }

        /// <summary>
        /// If false, the drop period calculated for this section is missing, has missing drop date values or the section doesn't fall within the drop period.
        /// </summary>
        public bool CanDropSection { get; set; }

    }
}