﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
     /// <summary>
     /// Data transfer object used to send in criteria to execute a GetAvailableSections Search
     /// </summary>
     public class AvailableSectionSearch
     {
          /// <summary>
          /// List of items that contains the planned course courseIds and any associated planned section
          /// </summary>
          public List<PlannedCourseSearch> PlannedCourses { get; set; }
          /// <summary>
          /// Term code for which available sections are required
          /// </summary>
          public string TermCode { get; set; }
          /// <summary>
          /// Location filter code selected - null no location filter selected
          /// </summary>
          public string Location { get; set; }
          /// <summary>
          /// Faculty Id filter selected - null if no faculty filter selected
          /// </summary>
          public string Faculty { get; set; }
          /// <summary>
          /// Day filter selected - null if no day filter selected
          /// </summary>
          public string Day { get; set; }
          /// <summary>
          /// Indicates whether only open sections should be returned. (If only open section are requested this will contain "Open").
          /// </summary>
          public string AvailableOnly { get; set; }
          /// <summary>
          /// Minutes from midnight if a time filter was selected - otherwise null.
          /// Possible values include Early, Morning, Afternoon, Evening, Night.
          /// </summary>
          public string TimeCategory { get; set; }
          /// <summary>
          /// Topic filter code selected - null if no topic filter selected
          /// </summary>
          public string Topic { get; set; }
     }
}
