﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Contains the basic data about a student program, but without the overhead of a full student program with requirement data
    /// </summary>
    public class StudentProgram
    {
        /// <summary>
        /// Gets or sets the Code of the program.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the full Title of the program.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Catalog associated with the program.
        /// </summary>
        public string Catalog { get; set; }

        /// <summary>
        /// Gets or sets a long Description of the program.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the ID of the student with whom this program is associated.
        /// </summary>
        public string StudentId { get; set; }

        /// <summary>
        /// The related "fastest path" programs associated to this program
        /// </summary>
        public IEnumerable<string> RelatedPrograms { get; set; }

    }
}