﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.using System;
using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents a single meeting pattern for a section, such as MWF 1-2pm in Jefferson Hall Room 101.
    /// </summary>
    public class PlannedSectionMeetingTimeModel
    {
        private string _StartTime;
        private string _EndTime;

        /// <summary>
        /// Gets or sets the instructional method being used for this specific meeting.
        /// </summary>
        public string InstructionalMethod { get; set; }

        /// <summary>
        /// Gets or sets whether this meeting time is an online type of meeting.
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// Gets the hour at which this meeting time begins.
        /// </summary>
        public int StartTimeHour
        {
            get
            {
                if (string.IsNullOrEmpty(_StartTime)) { return 0; }
                DateTime startTime = DateTime.Parse(this._StartTime);
                return startTime.Hour;
            }
        }

        /// <summary>
        /// Gets the minute at which this meeting time begins.
        /// </summary>
        public int StartTimeMinute
        {
            get
            {
                if (string.IsNullOrEmpty(_StartTime)) { return 0; }
                DateTime startTime = DateTime.Parse(this._StartTime);
                return startTime.Minute;
            }
        }

        /// <summary>
        /// Gets or sets a formatted start time string for this meeting time object.
        /// </summary>
        public string StartTime
        {
            get
            {
                if (string.IsNullOrEmpty(_StartTime)) { return string.Empty; }
                DateTime startTime = DateTime.Parse(this._StartTime);
                return startTime.ToShortTimeString();
            }
            set { _StartTime = value; }
        }

        /// <summary>
        /// Gets the ISO 8601 formatted start time for this meeting time object. This helps avoid the JSON date-parsing
        /// issues that arise when sending dates as part of asynchronous communication.
        /// </summary>
        public string StartTimeIso8601
        {
            get
            {
                if (string.IsNullOrEmpty(_StartTime)) { return string.Empty; }
                DateTime startTime = DateTime.Parse(this._StartTime);
                return startTime.ToString("s") + "Z";
            }
        }

        /// <summary>
        /// Gets the hour at which this meeting time ends.
        /// </summary>
        public int EndTimeHour
        {
            get
            {
                if (string.IsNullOrEmpty(_EndTime)) { return 0; }
                DateTime endTime = DateTime.Parse(this._EndTime);
                return endTime.Hour;
            }
        }

        /// <summary>
        /// Gets the minute at which this meeting time ends.
        /// </summary>
        public int EndTimeMinute
        {
            get
            {
                if (string.IsNullOrEmpty(_EndTime)) { return 0; }
                DateTime endTime = DateTime.Parse(this._EndTime);
                return endTime.Minute;
            }
        }

        /// <summary>
        /// Gets or sets a formatted end time string for this meeting time object.
        /// </summary>
        public string EndTime
        {
            get
            {
                if (string.IsNullOrEmpty(_EndTime)) { return string.Empty; }
                DateTime endTime = DateTime.Parse(this._EndTime);
                return endTime.ToShortTimeString();
            }
            set { _EndTime = value; }
        }

        /// <summary>
        /// Gets the ISO 8601 formatted end time for this meeting time object. This helps avoid the JSON date-parsing
        /// issues that arise when sending dates as part of asynchronous communication.
        /// </summary>
        public string EndTimeIso8601
        {
            get
            {
                if (string.IsNullOrEmpty(_EndTime)) { return string.Empty; }
                DateTime endTime = DateTime.Parse(this._EndTime);
                return endTime.ToString("s") + "Z";
            }
        }

        // Returns the start/end times formatted for display [start] - [end]
        public string FormattedTime
        {
            get
            {
                if (!string.IsNullOrEmpty(StartTime))
                {
                    return StartTime + " - " + EndTime;
                }
                else if (IsOnline)
                {
                    return null;
                }
                else
                {
                    return GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ScheduleDetailTBDText");
                }
            }
        }
        
        /// <summary>
        /// Gets or sets the set of days on which this meeting falls.
        /// </summary>
        public IEnumerable<DayOfWeek> Days { get; set; }

        /// <summary>
        /// Gets or sets the building in which this meeting is held.
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// Gets or sets the specific room in which this meeting is held.
        /// </summary>
        public string Room { get; set; }

        public string MeetingLocation
        {
            get
            {
                if (!(string.IsNullOrEmpty(Building) && string.IsNullOrEmpty(Room)))
                {
                    return (string.IsNullOrEmpty(Building) ? string.Empty : Building + " ") +
                           (string.IsNullOrEmpty(Room) ? string.Empty : Room + " ");
                }
                else if (IsOnline)
                {
                    return string.Empty;
                }
                else
                {
                    return GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ScheduleDetailTBDText");
                }
            }
        }

        /// <summary>
        /// Gets a formatted display of the list of the days of week on which this meeting is taught.
        /// </summary>
        public string DaysOfWeek
        {
            get
            {
                var days = "";
                foreach (var day in Days)
                {
                    switch (day)
                    {
                        case DayOfWeek.Sunday:
                            days += "S";
                            break;
                        case DayOfWeek.Monday:
                            days += "M";
                            break;
                        case DayOfWeek.Tuesday:
                            days += "T";
                            break;
                        case DayOfWeek.Wednesday:
                            days += "W";
                            break;
                        case DayOfWeek.Thursday:
                            days += "Th";
                            break;
                        case DayOfWeek.Friday:
                            days += "F";
                            break;
                        case DayOfWeek.Saturday:
                            days += "Sa";
                            break;
                    }
                }
                return days;
            }
        }

        public string Times
        {
            get
            {
                if (!(string.IsNullOrEmpty(DaysOfWeek) && string.IsNullOrEmpty(StartTime) && string.IsNullOrEmpty(EndTime)))
                {
                    return DaysOfWeek + " " + StartTime + " - " + EndTime;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public DateTime? StartDate;
        public DateTime? EndDate;

        public string StartDateString
        {
            get
            {
                if (StartDate == null)
                    return string.Empty;
                else
                {
                    return StartDate.GetValueOrDefault().ToShortDateString();
                }
            }
        }

        public string EndDateString
        {
            get
            {
                if (EndDate == null)
                    return string.Empty;
                else
                {
                    return EndDate.GetValueOrDefault().ToShortDateString();
                }
            }
        }

        /// <summary>
        /// The beginning and ending dates of this set of meetings.
        /// </summary>
        public string Dates
        {
            get
            {
                if (string.IsNullOrEmpty(StartDateString) || string.IsNullOrEmpty(EndDateString))
                {
                    if (!string.IsNullOrEmpty(StartDateString))
                    {
                        return StartDateString;
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return StartDateString + " - " + EndDateString;
                }
            }
        }

        public PlannedSectionMeetingTimeModel(SectionMeeting2 meeting, List<InstructionalMethod> instructionalMethods, List<Room> rooms, List<Building> buildings)
        {
            if (meeting == null)
            {
                throw new ArgumentNullException("meeting", "Meeting cannot be null");
            }
            this.InstructionalMethod = instructionalMethods.Where(x => x.Code == meeting.InstructionalMethodCode).FirstOrDefault().Description;
            if (meeting.StartTime.HasValue)
            {
                this._StartTime = meeting.StartTime.Value.LocalDateTime.ToShortTimeString();
            }
            if (meeting.EndTime.HasValue)
            {
                this._EndTime = meeting.EndTime.Value.LocalDateTime.ToShortTimeString();
            }
            this.Days = meeting.Days;
            this.StartDate = meeting.StartDate;
            this.EndDate = meeting.EndDate;
            this.IsOnline = meeting.IsOnline;
            var meetingRoom = rooms.Where(r => r.Id == meeting.Room).FirstOrDefault();
            if (meetingRoom != null)
            {
                var meetingBuilding = buildings.Where(b => b.Code == meetingRoom.BuildingCode).FirstOrDefault().Description;
                this.Building = meetingBuilding == null ? string.Empty : meetingBuilding.Trim();
                this.Room = meetingRoom == null ? string.Empty : meetingRoom.Code.Trim();
            }
        }
    }
}