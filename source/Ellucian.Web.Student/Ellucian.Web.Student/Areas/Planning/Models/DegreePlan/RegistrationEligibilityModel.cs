﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Models;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class RegistrationEligibilityModel
    {
        /// <summary>
        /// List of notifications, built from the list of messages returned by the eligibility check
        /// </summary>
        public List<Notification> Notifications;

        /// <summary>
        /// Indicates whether the user has overall registration overrides
        /// This is used to override the section by section registration date parameters
        /// </summary>
        public bool HasRegistrationOverride { get; set; }

        /// <summary>
        /// List of term eligibility objects
        /// </summary>
        public List<TermEligibilityModel> Terms;

        public RegistrationEligibilityModel()
        {
            Notifications = new List<Notification>();
            Terms = new List<TermEligibilityModel>();
        }
    }
}