﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents a section of a specific course that has been planned on a student's schedule.
    /// </summary>
    public class PlannedSectionModel
    {
        #region Inherited from Section DTO
        /// <summary>
        /// Gets or sets whether this section allows Audit grading.
        /// </summary>
        public bool AllowAudit { get; set; }
        /// <summary>
        /// Gets or sets whether this section allows Pass/Fail grading.
        /// </summary>
        public bool AllowPassNoPass { get; set; }
        /// <summary>
        /// Gets or sets the number of available seats in this section.
        /// </summary>
        public int? Available { get; set; }
        /// <summary>
        /// Gets or sets the Books for this section.
        /// </summary>
        public IEnumerable<SectionBook> Books { get; set; }
        /// <summary>
        /// Gets or sets the total capacity of seats for this section, if there is a limit.
        /// </summary>
        public int? Capacity { get; set; }
        /// <summary>
        /// Gets or sets the CEUs assigned to this section, if there is an amount.
        /// </summary>
        public decimal? Ceus { get; set; }
        /// <summary>
        /// Gets or sets the list of co-requisites attached to the general course of this section.
        /// </summary>
        public IEnumerable<Corequisite> CourseCorequisites { get; set; }
        /// <summary>
        /// Gets or sets the ID of the general course of this section.
        /// </summary>
        public string CourseId { get; set; }
        /// <summary>
        /// Gets or sets the date on which this section completes.
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Gets or sets the list of IDs of faculty teaching this section.
        /// </summary>
        public IEnumerable<string> FacultyIds { get; set; }
        /// <summary>
        /// Gets or sets the ID of this section.
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Gets or sets a flag indicating whether this section is active.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Gets or sets the location for where this section takes place.
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// Gets or sets the full location for display in the UI (e.g. 'Main Campus' versus the code of 'MC').
        /// </summary>
        public string LocationDisplay { get; set; }
        /// <summary>
        /// Gets or sets the maximum amount of credits for this section, if it is variable.
        /// </summary>
        public decimal? MaximumCredits { get; set; }
        /// <summary>
        /// Gets or sets the meeting blocks for this section.
        /// </summary>
        public IEnumerable<SectionMeeting2> Meetings { get; set; }
        /// <summary>
        /// Gets or sets the minimum amount of credits for this section. If this section is not variable credits then this is the only amount.
        /// </summary>
        public decimal? MinimumCredits { get; set; }
        /// <summary>
        /// Gets or sets the number of this section (e.g. Math 101 01 versus Math 101 02).
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// Gets or sets a flag indicating whether this section only allows Pass/Fail grading.
        /// </summary>
        public bool OnlyPassNoPass { get; set; }
        /// <summary>
        /// List of requisites for this section
        /// </summary>
        public IEnumerable<Requisite> Requisites { get; set; }
        /// <summary>
        /// List of section co-requisite Ids for this section - derived from the section's required SectionRequisites
        /// </summary>
        public IEnumerable<string> SectionCorequisites { get; set; }
        /// <summary>
        /// If there are required section co-requisites then this is the number of them that are required.
        /// </summary>
        public int SectionCorequisitesNumberNeeded { get; set; }
        /// <summary>
        /// Gets or sets the date on which this section begins.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets the ID of the term in which this section takes places. This will be empty if the section is a non-term item.
        /// </summary>
        public string TermId { get; set; }
        /// <summary>
        /// Gets or sets the title of this specific section.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the increment value for credits, if this is a variable credit course. (e.g. 0.5, which yields credit values 3.0, 3.5, 4.0).
        /// </summary>
        public decimal? VariableCreditIncrement { get; set; }
        /// <summary>
        /// Gets or sets a flag indicating whether a waitlist is available for this section.
        /// </summary>
        public bool WaitlistAvailable { get; set; }
        /// <summary>
        /// Gets or sets the number of students on the waitlist for this section.
        /// </summary>
        public int Waitlisted { get; set; }
        #endregion

        //Properties from the Section DTO that are not being used and are not represented here:
        //  ActiveStudentIds
        //  LearningProvider
        //  LearningProviderSideId
        //  PrimarySectionId

        /// <summary>
        /// Gets or sets the fully-formatted list of meeting items for this planned section.
        /// </summary>
        public IEnumerable<PlannedSectionMeetingTimeModel> PlannedMeetings { get; set; }
        /// <summary>
        /// Gets or sets a string-formatted list of the faculty assigned to teach this course. That is, the full names of each instructor for printing in the UI.
        /// </summary>
        public IEnumerable<string> Faculty { get; set; }
        /// <summary>
        /// Gets or sets a flag indicating whether this planned section conflicts with another planned item with respect to meeting time(s).
        /// </summary>
        public bool MeetingTimeConflict { get; set; }
        /// <summary>
        /// Gets or sets a time conflict message for this section, if there is indeed a conflict.
        /// </summary>
        public string MeetingTimeConflictMessage { get; set; }
 
        /// <summary>
        /// Gets a formatted display value for this section's start date.
        /// </summary>
        public string StartDateDisplay
        {
            get { return StartDate.ToShortDateString(); }
        }

        /// <summary>
        /// Gets a formatted display value for this section's end date, if it has one.
        /// </summary>
        public string EndDateDisplay
        {
            get { return this.EndDate.HasValue ? EndDate.Value.ToShortDateString() : string.Empty; }
        }

         /// <summary>
         /// Gets the topic code for the specific section
         /// </summary>
        public string TopicCode { get; set; }

        /// <summary>
        /// Gets a list of all days of the week on which this section is offered. This list may not necessarily be in order.
        /// </summary>
        public IEnumerable<string> DaysOfWeek
        {
            get
            {
                List<string> days = new List<string>();

                if (this.Meetings != null && this.Meetings.Count() > 0)
                {
                    foreach (var m in this.Meetings)
                    {
                        if (m != null && m.Days != null && m.Days.Count() > 0)
                        {
                            foreach (DayOfWeek day in m.Days)
                            {
                                switch (day)
                                {
                                    case DayOfWeek.Monday:
                                        if (!days.Contains(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Monday")))
                                        {
                                            days.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Monday"));
                                        }
                                        break;

                                    case DayOfWeek.Tuesday:
                                        if (!days.Contains(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Tuesday")))
                                        {
                                            days.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Tuesday"));
                                        }
                                        break;

                                    case DayOfWeek.Wednesday:
                                        if (!days.Contains(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Wednesday")))
                                        {
                                            days.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Wednesday"));
                                        }
                                        break;

                                    case DayOfWeek.Thursday:
                                        if (!days.Contains(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Thursday")))
                                        {
                                            days.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Thursday"));
                                        }
                                        break;

                                    case DayOfWeek.Friday:
                                        if (!days.Contains(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Friday")))
                                        {
                                            days.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Friday"));
                                        }
                                        break;

                                    case DayOfWeek.Saturday:
                                        if (!days.Contains(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Saturday")))
                                        {
                                            days.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Saturday"));
                                        }
                                        break;

                                    case DayOfWeek.Sunday:
                                        if (!days.Contains(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Sunday")))
                                        {
                                            days.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Sunday"));
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                return days;
            }
        }

        /// <summary>
        /// Gets a flag indicating whether this section has open seats.
        /// </summary>
        public bool IsOpen
        {
            get
            {
                if (this.Available.HasValue)
                {
                    return this.Available.Value > 0;
                }
                return true;
            }
        }

        /// <summary>
        /// Gets or sets a fully-formatted string for displaying this section in a UI (such as MATH-100-01).
        /// </summary>
        public string FormattedDisplay { get; set; }

        /// <summary>
        /// Show the waitlisted banner and show the waitlisted count if the section allows waitlisting and if there either is a waitlist or the section is full.
        /// Note: If a seat comes available when there is a waitlist we should still go with the default behavior of saying that there is a waitlist and showing that count.
        /// </summary>
        public bool ShowWaitlistInfo
        {
            get { return WaitlistAvailable && ((Waitlisted > 0) || (Capacity.HasValue && Capacity > 0 && Available.HasValue && Available <= 0)); }
        }

        /// <summary>
        /// Creates a new PlannedSectionModel based on the provided Section DTO.
        /// </summary>
        /// <param name="section">A Section DTO instance forming the basis of this PlannedSectionModel</param>
        public PlannedSectionModel(Ellucian.Colleague.Dtos.Student.Section3 section)
        {
            if (section == null)
            {
                return;
            }
            this.AllowAudit = section.AllowAudit;
            this.AllowPassNoPass = section.AllowPassNoPass;
            this.Available = section.Available;
            this.Books = section.Books;
            this.Capacity = section.Capacity;
            this.Ceus = section.Ceus;
            this.CourseId = section.CourseId;
            this.EndDate = section.EndDate;
            this.FacultyIds = section.FacultyIds;
            this.Id = section.Id;
            this.IsActive = section.IsActive;
            this.Location = section.Location;
            this.MaximumCredits = section.MaximumCredits;
            this.Meetings = section.Meetings;
            this.MinimumCredits = section.MinimumCredits;
            this.Number = section.Number;
            this.OnlyPassNoPass = section.OnlyPassNoPass;
            this.StartDate = section.StartDate;
            this.TermId = section.TermId;
            this.Title = section.Title;
            this.TopicCode = section.TopicCode;
            this.VariableCreditIncrement = section.VariableCreditIncrement;
            this.WaitlistAvailable = section.WaitlistAvailable;
            this.Waitlisted = section.Waitlisted;

            //set the properties that are not part of the Section DTO
            PlannedMeetings = new List<PlannedSectionMeetingTimeModel>();
            Faculty = new List<string>();
            MeetingTimeConflict = false;
            MeetingTimeConflictMessage = string.Empty;

        }

    }
}
