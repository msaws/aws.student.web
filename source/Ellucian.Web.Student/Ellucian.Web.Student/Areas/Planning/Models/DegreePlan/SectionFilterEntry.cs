﻿namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents a filter to be used in filtering sections.
    /// </summary>
    public class SectionFilterEntry
    {
        /// <summary>
        /// Gets or sets the text of the filter entry.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a count of how many items this entry covers.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets a formatted display value for this filter.
        /// </summary>
        public string Display
        {
            get { return Value + " (" + Count.ToString() + ")"; }
        }

        /// <summary>
        /// Creates a new, empty SectionFilterCount.
        /// </summary>
        public SectionFilterEntry()
        {
            Value = string.Empty;
            Count = 0;
        }

        /// <summary>
        /// Creates a new SectionFilterCount with the specified value and count.
        /// </summary>
        /// <param name="value">The text of the filter</param>
        /// <param name="count">How many items this filter accounts for</param>
        public SectionFilterEntry(string value, int count)
        {
            Value = value;
            Count = count;
        }
    }
}