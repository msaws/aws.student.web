﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents basic term information. 
    /// </summary>
    public class TermModel
    {
        /// <summary>
        /// Gets or sets the code for this term, such as SP/12.
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the name/full description of this term, such as Spring 2012.
        /// </summary>
        public string Description { get; set; }
    }
}