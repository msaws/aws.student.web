﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class AvailableSectionResult
    {
        /// <summary>
        /// Course for which available sections was requested
        /// </summary>
        public string CourseId { get; set; }
        /// <summary>
        /// A list of planned section models for each applicable available section for for the associated course.
        /// </summary>
        public List<PlannedSectionModel> AvailableSections { get; set; }
    }
}