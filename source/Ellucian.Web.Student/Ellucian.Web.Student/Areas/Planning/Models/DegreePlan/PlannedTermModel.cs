﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Represents a term that has been planned on a degree plan. A planned term can be either a completed term (past) for which
    /// the student has accumulated history, or a current/future term for which the student has planned and enrolled in courses (although
    /// it also can be empty).
    /// </summary>
    public class  PlannedTermModel
    {
        /// <summary>
        /// Gets or sets the code for this term, such as SP/12.
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the name/full description of this term, such as Spring 2012.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the date on which this term begins.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets the date on which this term ends.
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Gets or sets the reporting year associated with this term.
        /// </summary>
        public int ReportingYear { get; set; }
        /// <summary>
        /// Gets or sets the sequential identifier for this term, based on its reporting year.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Gets or sets whether this term is available for registration.
        /// </summary>
        public bool IsRegistrationTerm { get; set; }

        /// <summary>
        /// Gets or whether this term can be removed wholesale from the plan. Terms that have existing academic history or are have already finished are not removable.
        /// </summary>
        public bool IsTermRemovable { get { return EndDate > DateTime.Now && (PlannedCourses.Count == 0 || (PlannedCourses.Where(c => c.AcademicHistory.Section != null).Count() == 0 && PlannedCourses.Where(c => c.IsNonTermSection).Count() == 0)); } }

        /// <summary>
        /// Gets or sets whether this term has been completed. Planned items that belong to a term which has not been completed should be removable.
        /// </summary>
        public bool IsTermCompleted { get { return this.EndDate < DateTime.Now; } }

        /// <summary>
        /// Gets whether this term is currently open/active, based on the date.
        /// </summary>
        public bool IsTermActive { get { return (this.StartDate <= DateTime.Now) && (this.EndDate > DateTime.Now); } }

        /// <summary>
        /// Gets whether this term has an associated academic history record available.
        /// </summary>
        public bool IsAcademicHistoryAvailable { get { return this.PlannedCourses.Count(h => h.AcademicHistory.Section != null) > 0 ? true : false; } }

        /// <summary>
        /// Indicates whether this term is "clearable" (i.e. have all it's planned courses removed.)
        /// </summary>
        public bool IsTermClearable { get { return EndDate > DateTime.Now && PlannedCourses.Count > 0 && (PlannedCourses.Where(c => c.AcademicHistory.Section != null).Count() == 0) && (PlannedCourses.Where(c => c.IsNonTermSection).Count() == 0); } }

        /// <summary>
        /// True if any planned course on the term is marked as protected.  This is used to determine which features should
        /// be offered to the student (non-proxy).  Students are not allowed to delete a protected term.
        /// </summary>
        public bool IsTermProtected { get { return PlannedCourses.Any(pc => pc.IsProtected); } }
        
        // Return true if there are any planned course items with CanRegisterForSection equal true. 
        // For a planned course CanRegisterForSection will default to false unless there is a section (planned or credit).  If there is a section for the planned course 
        // this defaults to true unless there are specific section registration dates and none of them are open for registration now. 
        public bool AllowRegistration
        {
            get
            {
                if (PlannedCourses.Count() > 0)
                {
                    return PlannedCourses.Count(c => c.CanRegisterForSection) > 0;
                }
                return false;
            }
        }

        // Return true if all planned courses have academic credit
        public bool AllCoursesHaveCredit
        {
            get
            {
                foreach (var course in PlannedCourses)
                {
                    if (course.AcademicHistory.Section == null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Gets or sets the list of planned courses associated with this term.
        /// </summary>
        public List<PlannedCourseModel> PlannedCourses { get; set; }

        /// <summary>
        /// Gets the list of planned courses without scheduled meeting times associated with this term.
        /// These will not display on the weekly calendar but will appear in a different view.
        /// </summary>
        public List<PlannedCourseModel> PlannedSectionsWithNoMeetingTimes
        { 
            get 
            {
                List<PlannedCourseModel> untimedPlannedCourses = new List<PlannedCourseModel>();

                foreach (var pc in PlannedCourses)
                {
                    if (pc.Section != null )
                    {
                        // If any of the section meetings have a start time this is not an untimed planned course.
                        var meetingsWithTimes = pc.Section.Meetings.Where(m => m.StartTime.HasValue).ToList();
                        if (meetingsWithTimes.Count() == 0)
                        {
                            untimedPlannedCourses.Add(pc);
                        }
                    }
                }
                return untimedPlannedCourses;
            }
        }

        /// <summary>
        /// Gets or sets the completed GPA of this term.
        /// </summary>
        public decimal? CompletedGpa { get; set; }

        /// <summary>
        /// Gets a formatted string which prints the GPA and a label.
        /// </summary>
        public string CompletedGpaDisplay
        {
            get
            {
                 return GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "TermGPALabel") + " " + ((CompletedGpa == null) ? "" : CreditsFormatter.FormattedGpaString((decimal)CompletedGpa));
            }
        }

        #region Credit/CEU Formatting
        /// <summary>
        /// Gets the total amount of planned credits for this term.
        /// </summary>
        public decimal PlannedCreditsDecimal
        {
            get
            {
                decimal total = 0;

                if (this.PlannedCourses != null)
                {
                    foreach (PlannedCourseModel plannedCourse in this.PlannedCourses)
                    {
                        if (!plannedCourse.IsNonTermSection && plannedCourse.AcademicHistory.HasSection == false && plannedCourse.AcademicHistory.IsCompletedCredit == false && plannedCourse.GradingType != Colleague.Dtos.Student.GradingType.Audit)
                        {
                            if (plannedCourse.Credits.HasValue)
                            {
                                total += plannedCourse.Credits.Value;
                            }
                            else if (plannedCourse.Section != null && plannedCourse.Section.MinimumCredits.HasValue)
                            {
                                total += plannedCourse.Section.MinimumCredits.Value;
                            }
                            else if (plannedCourse.MinimumCredits.HasValue)
                            {
                                total += plannedCourse.MinimumCredits.Value;
                            }
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the non-waitlisted, planned credit display of this collection of plan items. This will be 0 if the collection does not include any courses.
        /// Note that this does NOT include continuing education units - use the PlannedCeusDisplay property for that,
        /// </summary>
        public string PlannedCreditsDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(PlannedCreditsDecimal);
            }
        }

        /// <summary>
        /// Gets the total number of planned continuing education units for this term.
        /// </summary>
        public decimal PlannedCeusDecimal
        {
            get
            {
                decimal total = 0;

                if (this.PlannedCourses != null)
                {
                    foreach (PlannedCourseModel plannedCourse in this.PlannedCourses)
                    {
                        if (!plannedCourse.IsNonTermSection && plannedCourse.AcademicHistory == null && plannedCourse.GradingType != Colleague.Dtos.Student.GradingType.Audit)
                        {
                            if (plannedCourse.Section != null && plannedCourse.Section.Ceus.HasValue)
                            {
                                total += plannedCourse.Section.Ceus.Value;
                            }
                            else if (plannedCourse.Ceus.HasValue)
                            {
                                total += plannedCourse.Ceus.Value;
                            }
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the total non-waitlisted CEUs display of this collection of plan items. This will be 0 if the collection does not include any courses.
        /// Note that this only accounts for CEUs, not Credits.
        /// </summary>
        public string PlannedCeusDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(PlannedCeusDecimal);
            }
        }

        /// <summary>
        /// Gets the total amount of enrolled/registered credits for this term.
        /// </summary>
        public decimal EnrolledCreditsDecimal
        {
            get
            {
                decimal total = 0;
                if (this.IsAcademicHistoryAvailable)
                {
                    foreach (PlannedCourseModel pc in this.PlannedCourses)
                    {
                        if (pc.AcademicHistory.Section != null && pc.AcademicHistory.GradingType != Colleague.Dtos.Student.GradingType.Audit)
                        {
                            total += pc.AcademicHistory.CreditsOverride;
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the enrolled credit display of this collection of plan items. This will be 0 if the collection does not include any courses.
        /// Note that this does NOT include CEUs - use the EnrolledCeusDisplay property for that,
        /// </summary>
        public string EnrolledCreditsDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(EnrolledCreditsDecimal);
            }
        }

        /// <summary>
        /// Gets or sets the number of enrolled CEUs for this term.
        /// </summary>
        public decimal EnrolledCeusDecimal
        {
            get
            {
                decimal total = 0;
                if (this.IsAcademicHistoryAvailable)
                {
                    foreach (PlannedCourseModel pc in this.PlannedCourses)
                    {
                        if (pc.AcademicHistory.Section != null && pc.AcademicHistory.GradingType != Colleague.Dtos.Student.GradingType.Audit)
                        {
                            total += pc.AcademicHistory.CeusOverride;
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the enrolled credit display of this collection of plan items. This will be 0 if the collection does not include any courses.
        /// Note that this does NOT include continuing education units - use the EnrolledCeusDisplay property for that,
        /// </summary>
        public string EnrolledCeusDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(EnrolledCeusDecimal);
            }
        }

        /// <summary>
        /// Gets or sets the number of waitlisted credits for this term.
        /// </summary>
        public decimal WaitlistedCreditsDecimal
        {
            get
            {
                decimal total = 0;

                if (this.PlannedCourses != null)
                {
                    foreach (PlannedCourseModel plannedCourse in this.PlannedCourses)
                    {
                        if (!plannedCourse.IsNonTermSection && plannedCourse.AcademicHistory == null && plannedCourse.GradingType != Colleague.Dtos.Student.GradingType.Audit && plannedCourse.SectionWaitlistStatus != "NotWaitlisted")
                        {
                            if (plannedCourse.Credits.HasValue)
                            {
                                total += plannedCourse.Credits.Value;
                            }
                            else if (plannedCourse.Section != null && plannedCourse.Section.MinimumCredits.HasValue)
                            {
                                total += plannedCourse.Section.MinimumCredits.Value;
                            }
                            else if (plannedCourse.MinimumCredits.HasValue)
                            {
                                total += plannedCourse.MinimumCredits.Value;
                            }
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Displays the number of credits for which the student is waitlisted this term
        /// </summary>
        public string WaitlistedCreditsDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(WaitlistedCreditsDecimal);
            }
        }

        /// <summary>
        /// Gets or sets the number of waitlisted CEUs for this term.
        /// </summary>
        public decimal WaitlistedCeusDecimal
        {
            get
            {
                decimal total = 0;

                if (this.PlannedCourses != null)
                {
                    foreach (PlannedCourseModel plannedCourse in this.PlannedCourses)
                    {
                        if (!plannedCourse.IsNonTermSection && plannedCourse.AcademicHistory == null && plannedCourse.GradingType != Colleague.Dtos.Student.GradingType.Audit && plannedCourse.SectionWaitlistStatus != "NotWaitlisted")
                        {
                            if (plannedCourse.Section != null && plannedCourse.Section.Ceus.HasValue)
                            {
                                total += plannedCourse.Section.Ceus.Value;
                            }
                            else if (plannedCourse.Ceus.HasValue)
                            {
                                total += plannedCourse.Ceus.Value;
                            }
                        }
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the total waitlisted CEUs display of this collection of plan items. This will be 0 if the collection does not include any courses.
        /// Note that this only accounts for CEUs, not Credits.
        /// </summary>
        public string WaitlistedCeusDisplay
        {
            get
            {
                return CreditsFormatter.FormattedCreditsString(WaitlistedCeusDecimal);
            }
        }
        #endregion

        /// <summary>
        /// Gets a formatted string displaying the total credits and (if applicable) CEUs count for this term. This will factor in whether this
        /// is a past or future term and format the string appropriately.
        /// </summary>
        public string TermCreditsCeusDisplay
        {
            get
            {
                string display = string.Empty;
                string credits = PlannedCreditsDisplay;
                string ceus = PlannedCeusDisplay;

                if (credits != "0" && ceus != "0")
                {
                    //format: 12 Planned Credits (3 CEUs)
                    display = credits + " " + ((IsTermCompleted) ? GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiEnrolledCreditsString") : GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiPlannedCreditsString"));
                    display += " (" + ceus + " " + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiTotalCeusParentheticalString") + ")";
                }
                else if (credits != "0" && ceus == "0")
                {
                    //format: 12 Planned Credits
                    display = credits + " " + ((IsTermCompleted) ? GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiEnrolledCreditsString") : GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiPlannedCreditsString"));
                }
                else if (credits == "0" && ceus != "0")
                {
                    //format: 12 Planned CEUs
                    display = ceus + " " + ((IsTermCompleted) ? GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiEnrolledCeusString") : GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiPlannedCeusString"));
                }
                else if (credits == "0" && ceus == "0")
                {
                    //format: 0 Planned Credits
                    display = credits + " " + ((IsTermCompleted) ? GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiEnrolledCreditsString") : GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiPlannedCreditsString"));
                }

                //display format: Enrolled Credits, Enrolled CEUs, Planned Credits, Planned CEUs  [values of 0 are omitted]
                string display2 = string.Empty;
                List<string> pieces = new List<string>();
                if (EnrolledCreditsDecimal > 0)
                {
                    pieces.Add(string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiEnrolledCreditsString"), EnrolledCreditsDisplay));
                }
                if (EnrolledCeusDecimal > 0)
                {
                    pieces.Add(string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiEnrolledCeusString"), EnrolledCeusDisplay));
                }
                if (PlannedCreditsDecimal > 0)
                {
                    pieces.Add(string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiPlannedCreditsString"), PlannedCreditsDisplay));
                }
                if (PlannedCeusDecimal > 0)
                {
                    pieces.Add(string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiPlannedCeusString"), PlannedCeusDisplay));
                }
                display2 = string.Join(", ", pieces);


                return display2;
            }
        }

        /// <summary>
        /// Creates a PlannedTermModel instance based on the provided Term DTO.
        /// </summary>
        /// <param name="term"></param>
        public PlannedTermModel(Term term)
            : this()
        {
            if (term != null)
            {
                Code = term.Code;
                Description = term.Description;
                StartDate = term.StartDate;
                EndDate = term.EndDate;
                ReportingYear = term.ReportingYear;
                Sequence = term.Sequence;
                
            }
        }

        /// <summary>
        /// Creates an empty PlannedTermModel instance.
        /// </summary>
        public PlannedTermModel()
        {
            PlannedCourses = new List<PlannedCourseModel>();
            IsRegistrationTerm = false;

        }
    }
}