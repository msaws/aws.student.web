﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    /// <summary>
    /// Information about a student's advising contacts. This could be a specific advisor or an advising office
    /// </summary>
    public class StudentAdvisingModel
    {
        /// <summary>
        /// Name of student's advisor or advising office 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Email address to be used to contact the advisor or advising office. 
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// Type of advisor (such as Major, Minor, Dissertation).  Can be blank. Values will be in parenthesis.
        /// </summary>
        public string AdvisorType { get; set; }
    }
}