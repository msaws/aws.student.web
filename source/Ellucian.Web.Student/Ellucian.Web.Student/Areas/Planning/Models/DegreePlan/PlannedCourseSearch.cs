﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class PlannedCourseSearch
    {
        /// <summary>
        /// Course Id from the Planned Course Model for which available sections is requested
        /// </summary>
        public string CourseId { get; set; }
        /// <summary>
        /// If the planned course model has an associated section, this is the section - otherwise null
        /// </summary>
        public string SectionId { get; set; }
    }
}