﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public enum CourseExistsStatus
    {
        CourseNotInPlan, // the student has not yet planned this course
        CourseInThisTerm, // the student has planned the course in this term
        CourseInAnotherTerm, // the student has planned the course in another term
        CourseTakenInThisTerm, // the student has an academic credit for the course
        CourseTakenInAnotherTerm
    }
}