﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{

    public class AcademicHistoryModel
    {
        /// <summary>
        /// Gets or sets the course title at the time the student took the course. This may have since changed in the catalog,
        /// but should be reported as it was originally taken. 
        /// </summary>
        public string TitleOverride { get; set; }

        /// <summary>
        /// Gets or sets the display of the grade earned for this completed course. This should be something meaningful to students,
        /// such as A-, rather than an internal code.
        /// </summary>
        public string GradeDisplay { get; set; }

        /// <summary>
        /// Indicates if this academic credit has been graded and therefore is not eligible for registration functions
        /// </summary>
        public bool IsGraded { get; set; }

        /// <summary>
        /// Indicates whether this grade indicates that the student withdrew from the course
        /// </summary>
        public bool IsWithdrawn { get; set; }

        /// <summary>
        /// Created to conglomerate all the conditions of a completed academic credit where they all need to be checked
        /// </summary>
        public bool IsCompletedCredit { get; set; }

        /// <summary>
        /// Gets or sets the number of enrolled credits for this completed course.
        /// </summary>
        public decimal CreditsOverride { get; set; }

        /// <summary>
        /// Gets or sets the grade points earned for this completed course.
        /// </summary>
        public decimal GradePoints { get; set; }

        /// <summary>
        /// Gets or sets the CEUs earned for this completed course.
        /// </summary>
        public decimal CeusOverride { get; set; }

        /// <summary>
        /// Gets or sets the associated Section that was taken for this course.
        /// </summary>
        //public Section SectionTaken { get; set; }
        public PlannedSectionModel Section { get; set; }

        /// <summary>
        /// Gets or sets whether this section was completed as a non-term section.
        /// </summary>
        public bool IsNonTermSection { get; set; }

        /// <summary>
        /// Gets or sets the grading type that was used for this course.
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public GradingType GradingType { get; set; }

        /// <summary>
        /// Gets whether this completed course contains full section information, or was just completed as a generic course.
        /// </summary>
        public bool HasSection
        {
            get { return Section != null; }
        }

        /// <summary>
        /// Gets or sets the name of the course that was taken for this academic credit.
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// Gets a string representation of the Credits and CEUs attained for this completed course.
        /// </summary>
        public string CreditsCeusDisplay
        {
            get
            {
                string creditsCeus = "0";

                decimal? credits = null;
                decimal? ceus = null;
                if (this.CreditsOverride > 0) credits = this.CreditsOverride;
                else if (this.Section != null && this.Section.MinimumCredits.HasValue) credits = this.Section.MinimumCredits.Value;
                if (this.CeusOverride > 0) ceus = this.CeusOverride;
                else if (this.Section != null && this.Section.Ceus.HasValue) ceus = this.Section.Ceus.Value;
                if (ceus.HasValue && ceus.Value == 0) ceus = null;

                if (this.IsNonTermSection)
                {
                    if (credits.HasValue && ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeusAndCreditsNonTerm"), CreditsFormatter.FormattedCreditsString(credits.Value), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                    else if (credits.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCreditsNonTerm"), CreditsFormatter.FormattedCreditsString(credits.Value));
                    }
                    else if (ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeusNonTerm"), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                }
                else
                {
                    if (credits.HasValue && ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeusAndCredits"), CreditsFormatter.FormattedCreditsString(credits.Value), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                    else if (credits.HasValue)
                    {
                        creditsCeus = CreditsFormatter.FormattedCreditsString(credits.Value);
                    }
                    else if (ceus.HasValue)
                    {
                        creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCeus"), CreditsFormatter.FormattedCreditsString(ceus.Value));
                    }
                }

                if (GradingType == Colleague.Dtos.Student.GradingType.Audit)
                {
                    creditsCeus = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCreditsAudited");
                }
                else if (GradingType == Colleague.Dtos.Student.GradingType.PassFail)
                {
                    creditsCeus = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseCreditsPassFail"), creditsCeus);
                }

                return creditsCeus;
            }
        }

        public AcademicHistoryModel()
        {
            TitleOverride = string.Empty;
            GradeDisplay = string.Empty;
            CreditsOverride = 0;
            GradePoints = 0;
            CeusOverride = 0;
            Section = null;
            GradingType = Colleague.Dtos.Student.GradingType.Graded;
            IsNonTermSection = false;
            CourseName = string.Empty;
            IsWithdrawn = false;
            IsCompletedCredit = false;
        }
    }
}