﻿// Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.DegreePlan
{
    public class PlannedCoursePlanningStatusModel
    {
        public string CourseId { get; set; }
        public string Class { get; set; }
        public string Text { get; set; }
    }
}