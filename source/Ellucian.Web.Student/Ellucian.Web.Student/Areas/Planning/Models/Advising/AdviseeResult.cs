﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Planning;
using System;
using System.Collections.Generic;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Advising
{
    /// <summary>
    /// Represents a specific advisee (i.e. student) that an advisor can manage.
    /// </summary>
    public class AdviseeResult
    {
        /// <summary>
        /// Gets or sets the name of the advisee 
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the department in which the advisee is enrolled.
        /// </summary>
        public List<string> ProgramDisplay { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether this advisee is waiting on a term approval.
        /// </summary>
        public bool HasApprovalPending { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether this advisee belongs to the current advisor.
        /// </summary>
        public bool IsAdvisee { get; set; }

        /// <summary>
        /// The names of the advisors assigned to this advisee
        /// </summary>
        public List<string> AdvisorNames { get; set; }

        /// <summary>
        /// String describing the advisee's stated educational goal
        /// </summary>
        public string EducationalGoal { get; set; }

        #region Inherited from Person
        /// <summary>
        /// Gets or sets the ID of this advisee.
        /// </summary>
        public string Id { get; set; }
        #endregion

        #region Inherited from Student
        /// <summary>
        /// Gets or sets the ID of the degree plan associated with this advisee.
        /// </summary>
        public int? DegreePlanId { get; set; }

        /// <summary>
        /// Gets or sets the preferred e-mail address of this advisee.
        /// </summary>
        public string PreferredEmailAddress { get; set; }

        /// <summary>
        /// Indicates whether or not this student has a privacy flag set on their records.
        /// </summary>
        public bool HasPrivacyRestriction { get; set; }

        /// <summary>
        /// If there is a privacy code, display this message to users
        /// </summary>
        public string PrivacyMessage { get; set; }
        #endregion

        //These items from Student and Person are not needed and thus are not included here:
        //  Catalog
        //  ProgramIds
        //  FirstName
        //  MiddleName
        //  LastName
        //  PreferredAddress
        //  PreferredName

        /// <summary>
        /// Creates a new, empty Advisee instance.
        /// </summary>
        public AdviseeResult()
        {
            HasApprovalPending = false;
            DegreePlanId = null;
            Id = string.Empty;
            ProgramDisplay = new List<string>();
            DisplayName = string.Empty;
            PreferredEmailAddress = string.Empty;
            AdvisorNames = new List<string>();
            HasPrivacyRestriction = false;
            PrivacyMessage = string.Empty;
        }

        /// <summary>
        /// Creates a new AdviseeResult object based on the provided Advisee DTO.
        /// </summary>
        /// <param name="advisee">An Advisee DTO to use as a base for this AdviseeResult</param>
        /// <param name="privacyMessage">The message to display related to privacy information</param>
        /// <param name="hasPrivacyRestriction">True if the data being returned is restricted/limited due to privacy requests</param>
        public AdviseeResult(Advisee advisee, string privacyMessage, bool hasPrivacyRestriction)
            : this()
        {
            this.DegreePlanId = advisee.DegreePlanId;
            this.Id = advisee.Id;
            this.HasApprovalPending = advisee.ApprovalRequested;
            this.IsAdvisee = advisee.IsAdvisee;
            this.PreferredEmailAddress = advisee.PreferredEmailAddress;
            this.EducationalGoal = advisee.EducationalGoal;
            this.AdvisorNames = new List<string>();
            this.ProgramDisplay = new List<string>();
            this.HasPrivacyRestriction = hasPrivacyRestriction;
            this.PrivacyMessage = privacyMessage;
            var displayNameResult = NameHelper.GetDisplayNameResult(advisee, advisee.PersonDisplayName, NameFormats.LastFirstMiddleInitial);
            this.DisplayName = displayNameResult.FullName;
        }
    }
}