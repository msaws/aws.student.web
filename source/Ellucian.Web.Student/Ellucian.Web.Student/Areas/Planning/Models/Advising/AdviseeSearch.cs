﻿using System.Collections.Generic;
using Ellucian.Web.Student.Models;

namespace Ellucian.Web.Student.Areas.Planning.Models.Advising
{
    /// <summary>
    /// Represents a ViewModel for the Advisee list - this can encompass either a list of advisees that matched a search,
    /// or a default list of assigned advisees for a given advisor.
    /// </summary>
    public class AdviseeSearch
    {
        /// <summary>
        /// Creates a new instance of AdviseeSearchViewModel.
        /// </summary>
        public AdviseeSearch()
        {
            Advisees = new List<AdviseeResult>();
            Notification = null;
        }
        /// <summary>
        /// Gets or sets a list of Advisees to be shown in a list view.
        /// </summary>
        public IEnumerable<AdviseeResult> Advisees { get; set; }

        /// <summary>
        /// Gets or sets an associated notification to be shown when the view-model is applied.
        /// </summary>
        public Notification Notification { get; set; }
    }
}