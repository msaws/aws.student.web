﻿using System;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Advising
{
    /// <summary>
    /// Encapsulates a note that can be shared between advisors and advisees.
    /// </summary>
    public class AdvisingNote
    {
        /// <summary>
        /// Gets or sets the message content of this note.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the full display name of the person who wrote this note. This should NOT just be the person ID, as this property is meant for UI display.
        /// </summary>
        public string Person { get; set; }

        /// <summary>
        /// Gets or sets a DateTime object specifying exactly when this note was added. This can be null if no time is recorded.
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or sets an indicator determining who wrote this note (advisor, student, etc).
        /// </summary>
        public PersonType PersonType { get; set; }

        /// <summary>
        /// Gets a formatted timestamp display value that includes the note writer and display-formatted date and time values.
        /// </summary>
        public string Timestamp
        {
            get
            {
                if (Date.HasValue)
                {
                    return Person + GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "NoteNameDateSeparator") +
                        Date.Value.ToShortDateString() +
                        GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "NoteDateTimeSeparator") +
                        Date.Value.ToString(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "NoteTimeFormat"));
                }
                else
                {
                    return Person;
                }
            }
        }

        /// <summary>
        /// Creates an empty AdvisingNoteModel instance.
        /// </summary>
        public AdvisingNote()
        {
            Message = string.Empty;
            Person = string.Empty;
            PersonType = Colleague.Dtos.Student.PersonType.Advisor;
            Date = DateTime.Now;
        }

        /// <summary>
        /// Creates a new AdvisingNoteModel instance with the specified values.
        /// </summary>
        /// <param name="message">The note message content</param>
        /// <param name="person">The person who added the note</param>
        /// <param name="personType">The type of person responsible for writing this note</param>
        /// <param name="date">The DateTime on which the note was added</param>
        public AdvisingNote(string message, string person, PersonType personType, DateTime? date)
        {
            Message = message;
            Person = person;
            PersonType = personType;
            Date = date;
        }
    }
}