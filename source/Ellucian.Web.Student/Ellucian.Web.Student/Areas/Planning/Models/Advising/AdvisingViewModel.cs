﻿using System.Collections.Generic;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Models;

namespace Ellucian.Web.Student.Areas.Planning.Models.Advising
{
    /// <summary>
    /// Defines a ViewModel for the Advising view, which allows an advisor to approve and deny advisee schedules.
    /// </summary>
    public class AdvisingViewModel
    {
        public string TermContentPartial = "AdvisingTermContent";
        /// <summary>
        /// Gets or sets the Advisee whom the Advisor is managing.
        /// </summary>
        public AdviseeResult Advisee { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether this advisor has permissions to modify an advisee's plan.
        /// </summary>
        public bool CanAdvisorModifyPlan { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether this advisor has permissions to view an advisee's plan.
        /// </summary>
        public bool CanAdvisorReviewPlan { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether this advisor has permissions to register an advisee for course sections
        /// </summary>
        public bool CanAdvisorRegister { get; set; }

        public AdvisingViewModel()
        {
            Advisee = null;
            Notification = null;
            CanAdvisorModifyPlan = false;
            CanAdvisorReviewPlan = false;
        }

        public AdvisingViewModel(AdviseeResult advisee)
        {
            Advisee = advisee;
            Notification = null;
            CanAdvisorModifyPlan = false;
            CanAdvisorReviewPlan = false;
        }

        /// <summary>
        /// Gets or sets an Notification to show when the view-model loads.
        /// </summary>
        public Notification Notification { get; set; }
    }
}