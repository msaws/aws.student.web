﻿using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Areas.Planning.Models.Courses
{
    public class CorequisiteSectionModel
    {
        public bool IsRequired { get; set; }
        public List<string> SectionIds { get; set; }
        public string DisplayMessage { get; set; }

        public CorequisiteSectionModel()
        {
            IsRequired = false;
            SectionIds = new List<string>();
            DisplayMessage = string.Empty;
        }
    }
}