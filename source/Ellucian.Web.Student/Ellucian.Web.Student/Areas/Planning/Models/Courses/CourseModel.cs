﻿using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Models.Courses;

namespace Ellucian.Web.Student.Areas.Planning.Models.Courses
{
    /// <summary>
    /// Represents a model for showing full details about a Course.
    /// </summary>
    public class CourseModel : Course2
    {
        /// <summary>
        /// Creates an empty CourseDetailsModel instance.
        /// </summary>
        public CourseModel() : base()
        {
            //set the items that are not part of the Course DTO
            this.LocationItems = new List<string>();
            this.RequisiteItems = new List<RequisiteModel>();
        }

        public string CourseName 
        { 
            get 
            { 
                return SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + Number; 
            } 
        }

        /// <summary>
        /// The Requisite display block for the course
        /// </summary>
        public IEnumerable<RequisiteModel> RequisiteItems { get; set; }

        /// <summary>
        /// Gets or sets the list of Locations to show in the details dialog.
        /// </summary>
        public IEnumerable<string> LocationItems { get; set; }

        /// <summary>
        /// Gets or sets a list of LocationCycleRestrictionModels
        /// </summary>
        public List<LocationCycleRestrictionModel> LocationCycleRestrictionDescriptions { get; set; }


        /// <summary>
        /// Gets the display value of the credits or CEUs amount for this course.
        /// </summary>
        public string CreditsCeusDisplay
        {
            get
            {
                string display = string.Empty;
                if (this.Ceus.HasValue && this.Ceus.Value >= 0)
                {
                    display = (this.Ceus.Value % 1 == 0) ? this.Ceus.Value.ToString("0") : this.Ceus.Value.ToString("0.#####");
                }
                else if (this.MaximumCredits.HasValue && this.MaximumCredits.Value > 0 && this.MinimumCredits.HasValue)
                {
                    display = (this.MinimumCredits.Value % 1 == 0) ? this.MinimumCredits.Value.ToString("0") : this.MinimumCredits.Value.ToString("0.####");
                    display += " to ";
                    display += (this.MaximumCredits.Value % 1 == 0) ? this.MaximumCredits.Value.ToString("0") : this.MaximumCredits.Value.ToString("0.#####");
                }
                else if (this.MinimumCredits.HasValue)
                {
                    display = (this.MinimumCredits.Value % 1 == 0) ? this.MinimumCredits.Value.ToString("0") : this.MinimumCredits.Value.ToString("0.#####");
                }
                return display;
            }
        }
    }
}