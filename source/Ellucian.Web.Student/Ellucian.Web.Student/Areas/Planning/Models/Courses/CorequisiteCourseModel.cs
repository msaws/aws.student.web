﻿using Ellucian.Colleague.Dtos.Student;
namespace Ellucian.Web.Student.Areas.Planning.Models.Courses
{
    /// <summary>
    /// Represents a full co-requisite item associated with a planned item.
    /// </summary>
    public class CorequisiteCourseModel
    {
        /// <summary>
        /// Gets or sets a flag indicating whether this is a required or recommended co-requisite item.
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Gets or sets a succinct, formatted message representing this co-requisite for display in the UI.
        /// </summary>
        public string DisplayMessage { get; set; }

        public string Id { get; set; }
        /// <summary>
        /// Creates a new, empty Co-requisite Course Model instance.
        /// </summary>
        public CorequisiteCourseModel(Course2 course)
        {
            IsRequired = false;
            DisplayMessage = string.Empty;
            Id = course.Id;
        }
    }
}