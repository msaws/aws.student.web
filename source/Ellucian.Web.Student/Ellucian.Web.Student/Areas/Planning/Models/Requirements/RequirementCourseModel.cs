﻿using System.Collections.Generic;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    /// <summary>
    /// Represents a model for showing full details about a Course.
    /// </summary>
    public class RequirementCourseModel
    {
        /// <summary>
        /// Creates an empty CourseDetailsModel instance.
        /// </summary>
        public RequirementCourseModel()
        {
        }

        public string Id { get; set; }
        public string SubjectCode { get; set; }
        public string Number { get; set; }
        public string Title { get; set; }
        public List<string> EquatedCourseIds { get; set; }
        public bool IsPseudoCourse { get; set; }

        public string CourseName
        {
            get
            {
                return SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + Number;
            }
        }
    }
}