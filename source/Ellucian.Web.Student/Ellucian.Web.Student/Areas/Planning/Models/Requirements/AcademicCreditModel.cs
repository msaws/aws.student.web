﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System;

namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    public class AcademicCreditModel
    {
        /// <summary>
        /// Id of the student academic credit
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Course Id for the academic credit (unless it is a non-course type of credit)
        /// </summary>
        public string CourseId { get; set; }

        /// <summary>
        /// Course name 
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// Course title 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Credit or CEU value for the academic credit
        /// </summary>
        public decimal Credit { get; set; }

        /// <summary>
        /// Verified grade may not be present if the student has a grade restriction
        /// </summary>
        public string VerifiedGrade { get; set; }
        /// <summary>
        /// HasVerifiedGrade should always be set correctly, regardless of grade restrictions
        /// </summary>
        public bool HasVerifiedGrade { get; set; }

        /// <summary>
        /// The term associated with this academic credit
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// Indicates whether or not this credit was granted based on a non-course equivalency (portfolio, life credit, etc)
        /// </summary>
        public bool IsNonCourse { get; set; }

        /// <summary>
        /// Indicates whether academic credit has a status of Withdrawn or has a grade that indicates that the student withdrew from the course
        /// </summary>
        public bool IsWithdrawn { get; set; }

        /// <summary>
        /// Created to conglomerate all the conditions of a completed academic credit where they all need to be checked
        /// </summary>
        public bool IsCompletedCredit { get; set; }

        /// <summary>
        /// Used to show the GPA adjusted credit instead of the regular credits when a class is to be counted to GPA only.
        /// </summary>
        public decimal AdjustedGpaCredit { get; set; }

        /// <summary>
        /// Primarily used within the Other Courses list to note which of the items should not use the "Completed" wording - such as Failed or Incomplete courses.
        /// </summary>
        public bool OtherAttemptedButNotCompleted { get; set; }

        /// <summary>
        /// Indicates that this specific Academic Credit is being applied to GPA only and not to associated sub-requirement/requirement.
        /// </summary>
        public bool IsGpaOnlyCredit { get; set; }

        /// <summary>
        /// Indicates that this specific Academic Credit was applied based on an override for the sub-requirement/requirement.
        /// </summary>
        public bool AllowedByOverride { get; set; }
        /// <summary>
        /// is this course extra taken
        /// </summary>
        public bool IsExtraCourse { get; set; }

        /// <summary>
        /// Calculates the status of academic credits. Ensures that all statuses are mutually-exclusive. 
        /// </summary>
        public string DisplayStatus
        {
            get
            {
                if (IsWithdrawn)
                {
                    return AcademicCreditDisplayStatus.Withdrawn.ToString();
                }
                if (IsCompletedCredit && !IsGpaOnlyCredit)
                {
                    return AcademicCreditDisplayStatus.Completed.ToString();
                }
                if (IsCompletedCredit && IsGpaOnlyCredit)
                {
                    return AcademicCreditDisplayStatus.Attempted.ToString();
                }
                if (StartDate.HasValue && StartDate.Value.ToLocalTime() <= DateTime.Now)
                {
                    return AcademicCreditDisplayStatus.InProgress.ToString();
                }
                return AcademicCreditDisplayStatus.Preregistered.ToString();
            }
        }

        /// <summary>
        /// Indicates that this academic credit has been replaced by another academic credit for the same course or an equated course.
        /// </summary>
        public string ReplacedStatus { get; set; }

        /// <summary>
        /// Indicates that this academic credit is the replacement for another academic credit for the same course or an equated course
        /// </summary>
        public string ReplacementStatus { get; set; }

        /// <summary>
        /// Build string to display based on whether course is replaced or a replacement
        /// </summary>
        public string ReplacementNotation
        {
            get
            {
                var replacementNotationText = string.Empty;
                if (ReplacedStatus == "Replaced")
                    replacementNotationText = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Replaced");
                if (ReplacementStatus == "Replacement")
                    replacementNotationText = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Replacement");
                if (ReplacedStatus == "ReplaceInProgress")
                    replacementNotationText = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "ReplaceInProgress");
                if (ReplacementStatus == "PossibleReplacement")
                    replacementNotationText = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "PossibleReplacement");
                return replacementNotationText;
            }
        }

        /// <summary>
        /// Build string to show override applied notation 
        /// </summary>
        public string OverrideNotation
        {
            get
            {
                return AllowedByOverride ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "OverrideApplied") : string.Empty;
            }
        }

        /// <summary>
        /// Start date for the academic credit
        /// </summary>
        public DateTime? StartDate { private get; set; }

        /// <summary>
        /// Base Constructor
        /// </summary>
        public AcademicCreditModel()
        {
            AllowedByOverride = false;
        }

        /// <summary>
        /// Constructor to essentially accomplish a copy of the academic credit
        /// </summary>
        /// <param name="baseCredit"></param>
        public AcademicCreditModel(AcademicCreditModel baseCredit)
        {
            Id = baseCredit.Id;
            CourseId = baseCredit.CourseId;
            CourseName = baseCredit.CourseName;
            Credit = baseCredit.Credit;
            Title = baseCredit.Title;
            VerifiedGrade = baseCredit.VerifiedGrade;
            HasVerifiedGrade = baseCredit.HasVerifiedGrade;
            Term = baseCredit.Term;
            AdjustedGpaCredit = baseCredit.AdjustedGpaCredit;
            IsGpaOnlyCredit = baseCredit.IsGpaOnlyCredit;
            IsCompletedCredit = baseCredit.IsCompletedCredit;
            IsExtraCourse = baseCredit.IsExtraCourse;
            IsNonCourse = baseCredit.IsNonCourse;
            IsWithdrawn = baseCredit.IsWithdrawn;
            OtherAttemptedButNotCompleted = baseCredit.OtherAttemptedButNotCompleted;
            ReplacedStatus = baseCredit.ReplacedStatus;
            ReplacementStatus = baseCredit.ReplacementStatus;
            AllowedByOverride = baseCredit.AllowedByOverride;
            StartDate = baseCredit.StartDate;
        }
    }

    public enum AcademicCreditDisplayStatus
    {
        Attempted,
        Completed,
        InProgress,
        Withdrawn,
        Preregistered
    }
}
