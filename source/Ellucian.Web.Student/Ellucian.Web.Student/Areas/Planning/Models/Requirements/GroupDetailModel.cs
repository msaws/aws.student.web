﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    public class GroupDetailModel
    {
        public string Id { get; set; }
        public string Code { get; set; }

        public List<RequirementCourseModel> FromCourses { get; set; }
        public List<RequirementCourseModel> Courses { get; set; }

        public List<string> FromLevels { get; set; }  //course level
        public List<Department> FromDepartments { get; set; }
        public List<Subject> FromSubjects { get; set; }

        public int? MinCoursesPerDepartment { get; set; }
        public int? MinCoursesPerSubject { get; set; }
        public decimal? MinCredits { get; set; }
        public decimal? MinCreditsPerCourse { get; set; }
        public decimal? MinCreditsPerDepartment { get; set; }
        public decimal? MinCreditsPerSubject { get; set; }
        public int? MinCourses { get; set; }
        public int? MinDepartments { get; set; }
        public int? MinSubjects { get; set; }

        public List<RequirementCourseModel> ButNotCourses { get; set; }
        public List<string> ButNotCourseLevels { get; set; }
        public List<Department> ButNotDepartments { get; set; }
        public List<Subject> ButNotSubjects { get; set; }

        public int? MaxCourses { get; set; }
        public int? MaxCoursesPerDepartment { get; set; }
        public int? MaxCoursesPerSubject { get; set; }
        public decimal? MaxCredits { get; set; }
        public decimal? MaxCreditsPerCourse { get; set; }
        public decimal? MaxCreditsPerDepartment { get; set; }
        public decimal? MaxCreditsPerSubject { get; set; }
        public int? MaxDepartments { get; set; }
        public int? MaxSubjects { get; set; }

        public MaxCreditAtLevels MaxCreditsAtLevels { get; set; }
        public MaxCoursesAtLevels MaxCoursesAtLevels { get; set; }

        public List<string> AcademicCreditRules { get; set; }

        public int? MaxCoursesPerRule { get; set; }
        public string MaxCoursesRule { get; set; }

        public decimal? MaxCreditsPerRule { get; set; }
        public string MaxCreditsRule { get; set; }

        public Boolean HasRules
        {
            get
            {
                return (AcademicCreditRules != null && AcademicCreditRules.Count > 0) || !string.IsNullOrEmpty(MaxCreditsRule) || !string.IsNullOrEmpty(MaxCoursesRule);
            }
        }

        // This boolean comes directly from the API and indicates whether any rule in the list of AcademicCreditRules has
        // AcademicCredit as the evaluated entity. If it does, the UI cannot allow course catalog search for the
        // group because this type of rule cannot be properly evaluated for course catalog search. MaxCreditsRule and MaxCoursesRule
        // are irrelevant for this setting.
        public bool HasAcademicCreditBasedRules { get; set; }

        // PRINT command text
        public string DisplayText { get; set; }

        // Indicates if this is the only group in a subrequirement and there is print text defined for the subrequirement. This prevents
        // the translation from displaying by default, which will likely be a duplicate of the print text.
        public bool IsSingleGroupWithPrintText { get; set; }

        // This is the text displayed for each group
        // If there is a PRINT command (DisplayText), show that
        // If there is no PRINT command and this is a single group with print text defined in the subrequirement, return nothing. 
        //     (Subrequirement print text will display
        // if there is no PRINT command in any other group/print text situation, show the translation (GroupText)
        public string Directive
        {
            get
            {
                if (string.IsNullOrEmpty(DisplayText))
                {
                    if (!IsSingleGroupWithPrintText)
                    {
                        return GroupText();
                    }
                    else return string.Empty;
                }
                else return DisplayText;
            }
        }

        // Passed to course search when you click on search on a group. Populated by RequirementResultModel when it is building the GroupResultModel.
        // Designed to show the same requirement information that the user sees in the UI. 
        public string SearchText { get; set; }

        // Translated specification 
        private string GroupText()
        {
            StringBuilder text = new StringBuilder();
            text.Append(this.MinCoursesText());
            text.Append(this.MinCreditsText());
            text.Append(this.TakeCoursesText());
            text.Append(this.FromCoursesText());
            text.Append(this.FromSubjectsText());
            text.Append(this.FromDepartmentsText());
            text.Append(this.FromLevelsText());
            text.Append(this.FromRulesText());
            text.Append(this.ButNotCoursesText());
            text.Append(this.ButNotSubjectsText());
            text.Append(this.ButNotDepartmentsText());
            text.Append(this.ButNotLevelsText());
            text.Append(this.MinSubjectsText());
            text.Append(this.MinDepartmentsText());
            text.Append(this.MinCoursesPerSubjectText());
            text.Append(this.MinCoursesPerDepartmentText());
            text.Append(this.MinCreditsPerCourseText());
            text.Append(this.MinCreditsPerSubjectText());
            text.Append(this.MinCreditsPerDepartmentText());
            text.Append(this.MaxCoursesText());
            text.Append(this.MaxCoursesPerSubjectText());
            text.Append(this.MaxCoursesPerDepartmentText());
            text.Append(this.MaxCreditsText());
            text.Append(this.MaxCreditsPerCourseText());
            text.Append(this.MaxCreditsPerSubjectText());
            text.Append(this.MaxCreditsPerDepartmentText());
            text.Append(this.MaxSubjectsText());
            text.Append(this.MaxDepartmentsText());
            text.Append(this.MaxCoursesAtLevelsText());
            text.Append(this.MaxCreditsAtLevelsText());
            text.Append(this.MaxCoursesPerRuleText());
            text.Append(this.MaxCreditsPerRuleText());

            // If there is no text built, insert a default.
            if (text.Length == 0)
            {
                text.Append(GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "GroupTextDefault"));
            }

            return text.ToString();
        }

        private string MinCoursesText()
        {
            StringBuilder text = new StringBuilder();
            if (MinCourses.HasValue)
            {
                text.Append("Complete ");
                text.Append(MinCourses.Value);
                text.Append(" course");
                if (MinCourses.Value > 1)
                {
                    text.Append('s');
                }
                text.Append(". ");
            }
            return text.ToString();
        }

        private string MinCreditsText()
        {
            StringBuilder text = new StringBuilder();
            if (MinCredits.HasValue)
            {
                text.Append("Complete ");
                text.Append(MinCredits.Value.ToString(".#####"));
                text.Append(" credits. ");
            }
            return text.ToString();
        }

        private string TakeCoursesText()
        {
            StringBuilder text = new StringBuilder();
            if (!MinCourses.HasValue && Courses != null && Courses.Count() > 0)
            {
                // Count up unique subject+number combinations, there can be more than one
                // MATH*100 but the student doesn't know or care.

                List<string> coursenames = new List<string>();
                foreach (var crs in Courses)
                {
                    string coursename = crs.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + crs.Number;
                    if (!coursenames.Contains(coursename)) { coursenames.Add(coursename); }
                }


                if (coursenames.Count() == 1)
                {
                    text.Append("Take course ");
                    text.Append(coursenames.ElementAt(0));
                }
                else
                {
                    text.Append("Take courses ");
                    for (int i = 0; i < coursenames.Count(); i++)
                    {
                        text.Append(coursenames.ElementAt(i));
                        if (i < Courses.Count() - 1)
                        {
                            text.Append(", ");
                        }
                    }
                }
                text.Append(". ");
            }
            return text.ToString();
        }

        private string FromCoursesText()
        {
            StringBuilder text = new StringBuilder();
            if (FromCourses != null && FromCourses.Count() > 0)
            {
                // Count up unique subject+number combinations, there can be more than one
                // MATH*100 but the student doesn't know or care.

                List<string> fromcoursenames = new List<string>();
                foreach (var fcrs in FromCourses)
                {
                    string fromcoursename = fcrs.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + fcrs.Number;
                    if (!fromcoursenames.Contains(fromcoursename)) { fromcoursenames.Add(fromcoursename); }
                }


                if (fromcoursenames.Count() == 1)
                {
                    text.Append("Take course ");
                }
                else
                {
                    text.Append("Choose from the courses ");
                }
                int i = 1;
                foreach (var course in fromcoursenames)
                {
                    text.Append(course);
                    if (i < fromcoursenames.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }


            }
            return text.ToString();
        }

        private string FromSubjectsText()
        {
            StringBuilder text = new StringBuilder();
            if (FromSubjects != null && FromSubjects.Count() > 0)
            {
                if (FromSubjects.Count() == 1)
                {
                    text.Append("Choose from the subject of ");
                }
                else
                {
                    text.Append("Choose from the subjects of ");
                }
                int i = 1;
                foreach (var subj in FromSubjects)
                {
                    text.Append(subj.Description);
                    if (i < FromSubjects.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }
            }
            return text.ToString();
        }

        private string FromDepartmentsText()
        {

            StringBuilder text = new StringBuilder();
            if (FromDepartments != null && FromDepartments.Count() > 0)
            {
                if (FromDepartments.Count() == 1)
                {
                    text.Append("Choose from the department of ");
                }
                else
                {
                    text.Append("Choose from the departments of ");
                }
                int i = 1;
                foreach (var dept in FromDepartments)
                {
                    text.Append(dept.Description);
                    if (i < FromDepartments.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }
            }
            return text.ToString();
        }

        private string FromLevelsText()
        {
            StringBuilder text = new StringBuilder();
            if (FromLevels != null && FromLevels.Count() > 0)
            {
                if (FromLevels.Count() == 1)
                {
                    text.Append("Choose from the level of ");
                }
                else
                {
                    text.Append("Choose from the levels of ");
                }
                int i = 1;
                foreach (var level in FromLevels)
                {
                    text.Append(level);
                    if (i < FromLevels.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }
            }
            return text.ToString();
        }

        private string FromRulesText()
        {
            string text = string.Empty;
            if (AcademicCreditRules != null && AcademicCreditRules.Count() > 0)
            {
                text = "Courses must be from rule(s): ";
                var i = 1;
                foreach (var rule in AcademicCreditRules)
                {
                    text += rule;
                    if (i < AcademicCreditRules.Count())
                    {
                        text += ", ";
                    }
                    else
                    {
                        text += ". ";
                    }
                    i++;
                }
            }
            return text;
        }

        private string ButNotCoursesText()
        {
            StringBuilder text = new StringBuilder();
            if (ButNotCourses != null && ButNotCourses.Count() > 0)
            {
                if (ButNotCourses.Count() == 1)
                {
                    text.Append("Excluding the course ");
                }
                else
                {
                    text.Append("Excluding the courses ");
                }
                int i = 1;
                foreach (var notCourse in ButNotCourses)
                {
                    text.Append(notCourse.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + notCourse.Number);
                    if (i < ButNotCourses.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }
            }
            return text.ToString();
        }

        private string ButNotSubjectsText()
        {
            StringBuilder text = new StringBuilder();
            if (ButNotSubjects != null && ButNotSubjects.Count() > 0)
            {
                if (ButNotSubjects.Count() == 1)
                {
                    text.Append("Excluding the subject ");
                }
                else
                {
                    text.Append("Excluding the subjects ");
                }
                int i = 1;
                foreach (var notSubject in ButNotSubjects)
                {
                    text.Append(notSubject.Description);
                    if (i < ButNotSubjects.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }
            }
            return text.ToString();
        }

        private string ButNotDepartmentsText()
        {
            StringBuilder text = new StringBuilder();
            if (ButNotDepartments != null && ButNotDepartments.Count() > 0)
            {
                if (ButNotDepartments.Count() == 1)
                {
                    text.Append("Excluding the department ");
                }
                else
                {
                    text.Append("Excluding the departments ");
                }
                int i = 1;
                foreach (var notDept in ButNotDepartments)
                {
                    text.Append(notDept.Description);
                    if (i < ButNotDepartments.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }
            }
            return text.ToString();
        }

        private string ButNotLevelsText()
        {
            StringBuilder text = new StringBuilder();
            if (ButNotCourseLevels != null && ButNotCourseLevels.Count() > 0)
            {
                if (ButNotCourseLevels.Count() == 1)
                {
                    text.Append("Excluding the level ");
                }
                else
                {
                    text.Append("Excluding the levels ");
                }
                int i = 1;
                foreach (var notLevel in ButNotCourseLevels)
                {
                    text.Append(notLevel);
                    if (i < ButNotCourseLevels.Count())
                    {
                        text.Append(", ");
                    }
                    else
                    {
                        text.Append(". ");
                    }
                    i++;
                }
            }
            return text.ToString();
        }

        private string MinDepartmentsText()
        {
            if (MinDepartments.HasValue == true && MinDepartments > 0)
            {
                return "Minimum of " + MinDepartments + " departments. ";
            }
            return string.Empty;
        }

        private string MinSubjectsText()
        {
            if (MinSubjects.HasValue == true && MinSubjects > 0)
            {
                return "Minimum of " + MinSubjects + " subjects. ";
            }
            return string.Empty;
        }

        private string MinCoursesPerDepartmentText()
        {
            if (MinCoursesPerDepartment.HasValue == true && MinCoursesPerDepartment > 0)
            {
                return "Minimum of " + MinCoursesPerDepartment + " courses per department. ";
            }
            return string.Empty;
        }

        private string MinCoursesPerSubjectText()
        {
            if (MinCoursesPerSubject.HasValue == true && MinCoursesPerSubject > 0)
            {
                return "Minimum of " + MinCoursesPerSubject + " courses per subject. ";
            }
            return string.Empty;
        }

        private string MinCreditsPerCourseText()
        {
            if (MinCreditsPerCourse.HasValue == true && MinCreditsPerCourse > 0)
            {
                return "Minimum of " + MinCreditsPerCourse.Value.ToString(".#####") + " credits per course. ";
            }
            return string.Empty;
        }

        private string MinCreditsPerDepartmentText()
        {
            if (MinCreditsPerDepartment.HasValue == true && MinCreditsPerDepartment > 0)
            {
                return "Minimum of " + MinCreditsPerDepartment.Value.ToString(".#####") + " credits per department. ";
            }
            return string.Empty;
        }

        private string MinCreditsPerSubjectText()
        {
            if (MinCreditsPerSubject.HasValue == true && MinCreditsPerSubject > 0)
            {
                return "Minimum of " + MinCreditsPerSubject.Value.ToString(".#####") + " credits per subject. ";
            }
            return string.Empty;
        }

        private string MaxCoursesText()
        {
            if (MaxCourses.HasValue == true && MaxCourses > 0)
            {
                return "Maximum of " + MaxCourses + " courses. ";
            }
            return string.Empty;
        }

        private string MaxCoursesPerDepartmentText()
        {
            if (MaxCoursesPerDepartment.HasValue == true && MaxCoursesPerDepartment > 0)
            {
                return "Maximum of " + MaxCoursesPerDepartment + " courses per department. ";
            }
            return string.Empty;
        }

        private string MaxCoursesPerSubjectText()
        {
            if (MaxCoursesPerSubject.HasValue == true && MaxCoursesPerSubject > 0)
            {
                return "Maximum of " + MaxCoursesPerSubject + " courses per subject. ";
            }
            return string.Empty;
        }

        private string MaxCreditsText()
        {
            if (MaxCredits.HasValue == true && MaxCredits > 0)
            {
                return "Maximum of " + MaxCredits.Value.ToString(".#####") + " credits. ";
            }
            return string.Empty;
        }

        private string MaxCreditsPerCourseText()
        {
            if (MaxCreditsPerCourse.HasValue == true && MaxCreditsPerCourse > 0)
            {
                return "Maximum of " + MaxCreditsPerCourse.Value.ToString(".#####") + " credits per course. ";
            }
            return string.Empty;
        }

        private string MaxCreditsPerDepartmentText()
        {
            if (MaxCreditsPerDepartment.HasValue == true && MaxCreditsPerDepartment > 0)
            {
                return "Maximum of " + MaxCreditsPerDepartment.Value.ToString(".#####") + " credits per department. ";
            }
            return string.Empty;
        }

        private string MaxCreditsPerSubjectText()
        {
            if (MaxCreditsPerSubject.HasValue == true && MaxCreditsPerSubject > 0)
            {
                return "Maximum of " + MaxCreditsPerSubject.Value.ToString(".#####") + " credits per subject. ";
            }
            return string.Empty;
        }

        private string MaxDepartmentsText()
        {
            if (MaxDepartments.HasValue == true && MaxDepartments > 0)
            {
                return "Maximum of " + MaxDepartments + " departments. ";
            }
            return string.Empty;
        }

        private string MaxSubjectsText()
        {
            if (MaxSubjects.HasValue == true && MaxSubjects > 0)
            {
                return "Maximum of " + MaxSubjects + " subjects. ";
            }
            return string.Empty;
        }

        private string MaxCoursesAtLevelsText()
        {
            StringBuilder text = new StringBuilder();
            if (MaxCoursesAtLevels != null && MaxCoursesAtLevels.MaxCourses > 0 && MaxCoursesAtLevels.Levels.Count() > 0)
            {
                text.Append("Maximum of " + MaxCoursesAtLevels.MaxCourses + " course");
                if (MaxCoursesAtLevels.MaxCourses > 1)
                {
                    text.Append("s");
                }
                text.Append(" at the ");
                if (MaxCoursesAtLevels.Levels.Count() == 1)
                {
                    text.Append(MaxCoursesAtLevels.Levels.ElementAt(0) + " level.");
                }
                else
                {
                    for (int i = 0; i < MaxCoursesAtLevels.Levels.Count(); i++)
                    {
                        text.Append(MaxCoursesAtLevels.Levels.ElementAt(i));
                        if (i < MaxCoursesAtLevels.Levels.Count() - 1)
                        {
                            text.Append(", ");
                        }
                    }
                    text.Append(" levels. ");
                }
            }
            return text.ToString();
        }

        private string MaxCreditsAtLevelsText()
        {
            StringBuilder text = new StringBuilder();
            if (MaxCreditsAtLevels != null && MaxCreditsAtLevels.MaxCredits > 0 && MaxCreditsAtLevels.Levels.Count() > 0)
            {
                text.Append("Maximum of " + MaxCreditsAtLevels.MaxCredits + " credits at the ");
                if (MaxCreditsAtLevels.Levels.Count() == 1)
                {
                    text.Append(MaxCreditsAtLevels.Levels.ElementAt(0) + " level.");
                }
                else
                {
                    for (int i = 0; i < MaxCreditsAtLevels.Levels.Count(); i++)
                    {
                        text.Append(MaxCreditsAtLevels.Levels.ElementAt(i));
                        if (i < MaxCreditsAtLevels.Levels.Count() - 1)
                        {
                            text.Append(", ");
                        }
                    }
                    text.Append(" levels. ");
                }
            }
            return text.ToString();
        }

        private string MaxCoursesPerRuleText()
        {
            if (MaxCoursesPerRule.HasValue && MaxCoursesPerRule > 0 && !string.IsNullOrEmpty(MaxCoursesRule))
            {
                return "Maximum of " + MaxCoursesPerRule + " courses from rule: " + MaxCoursesRule + ". ";
            }
            return string.Empty;
        }

        private string MaxCreditsPerRuleText()
        {
            if (MaxCreditsPerRule.HasValue && MaxCreditsPerRule > 0 && !string.IsNullOrEmpty(MaxCreditsRule))
            {
                return "Maximum of " + MaxCreditsPerRule + " credits from rule: " + MaxCreditsRule + ". ";
            }
            return string.Empty;
        }
    }
}
