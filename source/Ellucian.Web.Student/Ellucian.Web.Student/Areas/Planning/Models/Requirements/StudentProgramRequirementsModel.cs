﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ellucian.Web.Student.Models;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    public class StudentProgramRequirementsModel
    {
        public StudentProgramRequirementsModel(string studentId, bool currentUserIsStudent)
        {
            this.studentId = studentId;
            this.currentUserIsStudent = currentUserIsStudent;
            this.Notifications = this.notifications.AsReadOnly();
            this.RelatedPrograms = this.relatedPrograms.AsReadOnly();
        }


        private string studentId { get; set; }
        /// <summary>
        /// Student to whom this model data belongs
        /// </summary>
        public string StudentId { get { return studentId; } }

        private bool currentUserIsStudent { get; set; }
        /// <summary>
        /// Boolean indicates whether it's the student himself who is logged in
        /// </summary>
        public bool CurrentUserIsStudent { get { return currentUserIsStudent; } }

        private readonly List<Notification> notifications = new List<Notification>();
        /// <summary>
        /// Gets or sets an error message associated with this model instance.
        /// </summary>
        public ReadOnlyCollection<Notification> Notifications { get; private set; }

        /// <summary>
        /// Gets or sets the program for this model instance.
        /// </summary>
        public StudentProgramModel Program { get; set; }

        private List<string> relatedPrograms = new List<string>();
        /// <summary>
        /// The related "fastest path" programs associated to this program
        /// </summary>
        public ReadOnlyCollection<string> RelatedPrograms { get; set; }

        /// <summary>
        /// Add a notification to the object
        /// </summary>
        /// <param name="notification">A new notification to be added to the collection of notifications</param>
        public void AddNotification(Notification notification)
        {
            if (notification == null)
            {
                throw new ArgumentNullException("notification");
            }

            if (!notifications.Contains(notification))
            {
                notifications.Add(notification);
            }
        }

        /// <summary>
        /// Add a set of related programs to the object. This method will remove duplicates from the list.
        /// </summary>
        /// <param name="programs">The set of related program codes to be added to the collection of related program codes</param>
        public void AddRelatedPrograms(IEnumerable<string> programs)
        {
            if (programs == null)
            {
                throw new ArgumentNullException("programs");
            }

            relatedPrograms.AddRange(programs);
            relatedPrograms = relatedPrograms.Distinct().ToList();
        }
    }
}