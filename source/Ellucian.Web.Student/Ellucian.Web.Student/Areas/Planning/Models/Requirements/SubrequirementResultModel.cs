﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    public class SubrequirementResultModel
    {
        public string Id { get; set; }
        public string Code { get; set; }

        public string DisplayText { get; set; }

        public string MinGpa { get; set; }

        public int? MinGroups { get; set; }
        public List<GroupResultModel> Groups { get; set; }

        public string Gpa { get; set; }

        // Has this requirement been completed (w/ actual completed coursework, not planned or in-progress work)
        public CompletionStatus CompletionStatus { get; set; }
        // Has this requirement been completely planned?
        public PlanningStatus PlanningStatus { get; set; }

        public List<string> ModificationMessages { get; set; }

        public string Directive
        {
            get
            {
                return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Complete") + " " + (MinGroups != null ? MinGroups.ToString() : GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "All")) + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "OfTheFollowing") + " " + (MinGroups != null ? Groups.Count.ToString() : "") + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Items") + ".";
            }
        }

        public string StatusDirective
        {
            get
            {
                if (CompletionStatus == CompletionStatus.Waived) return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "UiWaived");

                var numCompleted = Groups.Where(x => x.CompletionStatus == CompletionStatus.Completed || x.CompletionStatus == CompletionStatus.Waived).Count();
                var numToComplete = (MinGroups != null ? (int)MinGroups : Groups.Count());

                // 2 of 1 completed makes sense to a programmer but not to a student.
                if (numCompleted > numToComplete) { numCompleted = numToComplete; }

                return numCompleted + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "OfLabel") + " " + numToComplete + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Completed") + ".";
            }
        }

        public string PlanningDirective
        {
            get
            {
                // If not yet complete and some items planned, add planning directive
                if (!(CompletionStatus == CompletionStatus.Completed))
                {
                    if (PlanningStatus == PlanningStatus.CompletelyPlanned)
                    {
                        {
                            return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "FullyPlanned");
                        }
                    }
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Statement of the minimum institutional credits required to satisfy this sub-requirement
        /// </summary>
        public string MinInstitutionalCredits { get; set; }

        /// <summary>
        /// Statement of the number of institutional credits achieved
        /// </summary>
        public string InstitutionalCredits { get; set; }

        /// <summary>
        /// If a special class needed to display the GPA (if GPA is too low) this is set to the class name
        /// </summary>
        public string GpaDisplayClass { get; set; }

        /// <summary>
        /// If a special class needed to display the institutional credits (institutional credits is too low) this is set to the class name
        /// </summary>
        public string InstitutionalCreditsDisplayClass { get; set; }
    }
}
