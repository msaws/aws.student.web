﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    public class RequirementResultModel
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        public string Gpa { get; set; }

        // Has this requirement been completed (w/ actual completed coursework, not planned or in-progress work)
        public CompletionStatus CompletionStatus { get; set; }
        // Has this requirement been completely planned?
        public PlanningStatus PlanningStatus { get; set; }

        public string MinGpa { get; set; }

        public int? MinSubrequirements { get; set; }
        public List<SubrequirementResultModel> Subrequirements { get; set; }

        public List<string> ModificationMessages { get; set; }

        public string Directive
        {
            get
            {
                return Subrequirements.Count > 1 ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Complete") + " " + (MinSubrequirements != null ? MinSubrequirements.ToString() : GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "All")) + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "OfTheFollowing") + " " + (MinSubrequirements != null ? Subrequirements.Count.ToString() : "") + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Items") + "." : GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "CompleteTheFollowingItem");
            }
        }

        public string StatusDirective
        {
            get
            {
                if (CompletionStatus == CompletionStatus.Waived) return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "RequirementWaived");

                var numCompleted = Subrequirements.Where(x => x.CompletionStatus == CompletionStatus.Completed || x.CompletionStatus == CompletionStatus.Waived).Count();
                var numToComplete = (MinSubrequirements != null ? (int)MinSubrequirements : Subrequirements.Count());

                // 2 of 1 completed makes sense to a programmer but not to a student.
                if (numCompleted > numToComplete) { numCompleted = numToComplete; }

                return numCompleted + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "OfLabel") + " " + numToComplete + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Completed") + ".";
            }
        }

        public string PlanningDirective
        {
            get
            {
                // If not yet complete and some items planned, add planning directive
                if (!(CompletionStatus == CompletionStatus.Completed))
                {
                    if (PlanningStatus == PlanningStatus.CompletelyPlanned)
                    {
                        {
                            return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "FullyPlanned");
                        }
                    }
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Statement of the minimum number of institutional credits needed to satisfy this requirement
        /// </summary>
        public string MinInstitutionalCredits { get; set; }

        /// <summary>
        /// Statement of the number of institutional credits achieved
        /// </summary>
        public string InstitutionalCredits { get; set; }

        /// <summary>
        /// When the GPA is not meeting the required level, this will be contain a special class to display the values more
        /// noticeably.
        /// </summary>
        public string GpaDisplayClass { get; set; }

        /// <summary>
        /// When the Institutional Credits is not meeting the required number, this will contain the special class to display the 
        /// values more noticeably.
        /// </summary>
        public string InstitutionalCreditsDisplayClass { get; set; }

        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        public RequirementResultModel(IAdapterRegistry adapterRegistry, ProgramEvaluation3 result, Requirement req, IEnumerable<AcademicCreditModel> academicCredits, IEnumerable<AcademicCreditModel> plannedCourses, IEnumerable<Course2> courses, IEnumerable<Subject> subjects, IEnumerable<Department> departments)
        {
            if (req == null)
            {
                throw new ArgumentNullException("req", "Requirement is required input to RequirementResultModel");
            }
            if (result == null)
            {
                throw new ArgumentNullException("result", "Result is required input to RequirementResultModel");
            }

            _adapterRegistry = adapterRegistry;
            var courseMapAdapter = _adapterRegistry.GetAdapter<Course2, RequirementCourseModel>();

            Id = req.Id;
            Code = req.Code;
            Description = req.Description;
            MinSubrequirements = req.MinSubRequirements;
            Subrequirements = new List<SubrequirementResultModel>();

            MinGpa = req.MinGpa != null ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "MustHaveMinGpa").Replace("{0}", Math.Round(req.MinGpa.Value, 3).ToString("0.000")) : null;

            MinInstitutionalCredits = req.MinInstitutionalCredits != null ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "RequirementMinimumInstitutionCredits").Replace("{0}", Math.Round(req.MinInstitutionalCredits.Value, 2).ToString("0.00")) : null;

            Ellucian.Colleague.Dtos.Student.Requirements.RequirementResult3 reqResult = null;
            if (result.RequirementResults != null)
            {
                reqResult = result.RequirementResults.Where(x => x.RequirementId == req.Id).FirstOrDefault();
            }
            if (reqResult != null)
            {
                CompletionStatus = reqResult.CompletionStatus;
                PlanningStatus = reqResult.PlanningStatus;

                Gpa = (reqResult.Gpa == null) ? "" : GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "CurrentGpa").Replace("{0}", Math.Round((decimal)reqResult.Gpa, 3).ToString("0.000"));

                InstitutionalCredits = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "RequirementInstitutionCreditsApplied").Replace("{0}", Math.Round(reqResult.InstitutionalCredits, 2).ToString("0.00"));

                ModificationMessages = reqResult.ModificationMessages;

                GpaDisplayClass = reqResult.MinGpaIsNotMet ? "gpa-warning" : string.Empty;
                InstitutionalCreditsDisplayClass = reqResult.MinInstitutionalCreditsIsNotMet ? "inst-credits-warning" : string.Empty;
            }
            // Now build the Subrequirement view models
            foreach (var subreq in req.Subrequirements)
            {
                var Subreqmodel = new SubrequirementResultModel();
                Subreqmodel.Groups = new List<GroupResultModel>();

                Subreqmodel.Id = subreq.Id;
                Subreqmodel.Code = subreq.Code;
                // Do not carry in display text that is a specification (evidenced by #GROUP.ID), or only spaces
                Subreqmodel.DisplayText = (string.IsNullOrEmpty(subreq.DisplayText) || (subreq.DisplayText.Replace(" ", "").Length == 0) ||
                    subreq.DisplayText.Replace(" ", "").StartsWith("#GROUP.ID")) ? string.Empty : subreq.DisplayText;
                Subreqmodel.MinGroups = subreq.MinGroups;
                Subreqmodel.MinGpa = subreq.MinGpa != null ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "GpaRequired").Replace("{0}", Math.Round(subreq.MinGpa.Value, 3).ToString("0.000")) : null;
                Subreqmodel.MinInstitutionalCredits = subreq.MinInstitutionalCredits != null ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "SubrequirementMinimumInstitutionCredits").Replace("{0}", Math.Round(subreq.MinInstitutionalCredits.Value, 2).ToString("0.00")) : null;

                if (reqResult != null && reqResult.SubrequirementResults != null)
                {
                    var SubrequirementResult = reqResult.SubrequirementResults.Where(s => s.SubrequirementId == subreq.Id).FirstOrDefault();
                    if (SubrequirementResult != null)
                    {
                        Subreqmodel.CompletionStatus = SubrequirementResult.CompletionStatus;
                        Subreqmodel.PlanningStatus = SubrequirementResult.PlanningStatus;
                        // var xx = Math.Round(SubrequirementResult.Gpa, 3); // I don't believe this is necessary... variable never used.
                        Subreqmodel.Gpa = (SubrequirementResult.Gpa == null) ? "" : GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "CurrentGpa").Replace("{0}", Math.Round((decimal)SubrequirementResult.Gpa, 3).ToString("0.000"));
                        Subreqmodel.ModificationMessages = SubrequirementResult.ModificationMessages;
                        Subreqmodel.InstitutionalCredits = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "SubrequirementInstitutionCreditsApplied").Replace("{0}", Math.Round(SubrequirementResult.InstitutionalCredits, 2).ToString("0.00"));
                        Subreqmodel.GpaDisplayClass = SubrequirementResult.MinGpaIsNotMet ? "gpa-warning" : string.Empty;
                        Subreqmodel.InstitutionalCreditsDisplayClass = SubrequirementResult.MinInstitutionalCreditsIsNotMet ? "inst-credits-warning" : string.Empty;
                    }
                }
                // Now build each group
                foreach (var group in subreq.Groups)
                {
                    var groupResultModel = new GroupResultModel();

                    groupResultModel.Id = group.Id;
                    groupResultModel.Code = group.Code;

                    groupResultModel.RequirementCode = req.Code;
                    groupResultModel.SubrequirementId = subreq.Id;

                    groupResultModel.FromCourses = new List<RequirementCourseModel>();
                    foreach (var course in courses.Where(x => group.FromCourses.Contains(x.Id)))
                    {
                        groupResultModel.FromCourses.Add(courseMapAdapter.MapToType(course));
                    }

                    groupResultModel.Courses = new List<RequirementCourseModel>();
                    foreach (var course in courses.Where(x => group.Courses.Contains(x.Id)))
                    {
                        groupResultModel.Courses.Add(courseMapAdapter.MapToType(course));
                    }

                    groupResultModel.FromLevels = group.FromLevels.ToList();
                    groupResultModel.FromDepartments = departments.Where(x => group.FromDepartments.Contains(x.Code)).ToList();
                    groupResultModel.FromSubjects = subjects.Where(x => group.FromSubjects.Contains(x.Code)).ToList();

                    groupResultModel.DisplayText = group.DisplayText;
                    groupResultModel.IsSingleGroupWithPrintText = subreq.Groups.Count() == 1 && !string.IsNullOrEmpty(Subreqmodel.DisplayText); // If it's a single group with print text provided, hide the translation behind the "view details" link


                    groupResultModel.MinCoursesPerDepartment = group.MinCoursesPerDepartment;
                    groupResultModel.MinCoursesPerSubject = group.MinCoursesPerSubject;
                    groupResultModel.MinCredits = group.MinCredits;
                    groupResultModel.MinCreditsPerCourse = group.MinCreditsPerCourse;
                    groupResultModel.MinCreditsPerDepartment = group.MinCreditsPerDepartment;
                    groupResultModel.MinCreditsPerSubject = group.MinCreditsPerSubject;
                    groupResultModel.MinCourses = group.MinCourses;
                    groupResultModel.MinDepartments = group.MinDepartments;
                    groupResultModel.MinSubjects = group.MinSubjects;

                    groupResultModel.ButNotCourses = new List<RequirementCourseModel>();
                    foreach (var course in courses.Where(x => group.ButNotCourses.Contains(x.Id)))
                    {
                        groupResultModel.ButNotCourses.Add(courseMapAdapter.MapToType(course));
                    }

                    groupResultModel.ButNotCourseLevels = group.ButNotCourseLevels;
                    groupResultModel.ButNotDepartments = departments.Where(x => group.ButNotDepartments.Contains(x.Code)).ToList();
                    groupResultModel.ButNotSubjects = subjects.Where(x => group.ButNotSubjects.Contains(x.Code)).ToList();

                    groupResultModel.MaxCourses = group.MaxCourses;
                    groupResultModel.MaxCoursesPerDepartment = group.MaxCoursesPerDepartment;
                    groupResultModel.MaxCoursesPerSubject = group.MaxCoursesPerSubject;
                    groupResultModel.MaxCredits = group.MaxCredits;
                    groupResultModel.MaxCreditsPerCourse = group.MaxCreditsPerCourse;
                    groupResultModel.MaxCreditsPerDepartment = group.MaxCreditsPerDepartment;
                    groupResultModel.MaxCreditsPerSubject = group.MaxCreditsPerSubject;
                    groupResultModel.MaxDepartments = group.MaxDepartments;
                    groupResultModel.MaxSubjects = group.MaxSubjects;

                    groupResultModel.MaxCoursesAtLevels = group.MaxCoursesAtLevels;
                    groupResultModel.MaxCreditsAtLevels = group.MaxCreditsAtLevels;

                    groupResultModel.AcademicCreditRules = group.AcademicCreditRules.ToList();

                    // Set to True if any of the AcademicCreditRules are based on AcademicCredit (cannot be used for course catalog search)
                    groupResultModel.HasAcademicCreditBasedRules = group.HasAcademicCreditBasedRules;

                    // The number of courses that may be applied from a rule
                    groupResultModel.MaxCoursesPerRule = group.MaxCoursesPerRule;
                    // The rule that corresponds to MaxCoursesPerRule
                    groupResultModel.MaxCoursesRule = group.MaxCoursesRule;

                    // The number of credits/hours that may be applied from a rule
                    groupResultModel.MaxCreditsPerRule = group.MaxCreditsPerRule;
                    // The rule that corresponds to MaxCreditsPerRule
                    groupResultModel.MaxCreditsRule = group.MaxCreditsRule;

                    Ellucian.Colleague.Dtos.Student.Requirements.SubrequirementResult3 subreqResult = null;
                    Ellucian.Colleague.Dtos.Student.Requirements.GroupResult3 groupResult = null;
                    if (reqResult != null)
                    {
                        subreqResult = reqResult.SubrequirementResults.Where(s => s.SubrequirementId == subreq.Id).FirstOrDefault();
                        groupResult = subreqResult == null ? null : subreqResult.GroupResults.Where(g => g.GroupId == group.Id).FirstOrDefault();
                    }

                    groupResultModel.AppliedAcademicCredits = new List<AcademicCreditModel>();
                    groupResultModel.AcademicCreditsIncludedInGPA = new List<AcademicCreditModel>();
                    groupResultModel.AppliedPlannedCourses = new List<AcademicCreditModel>();

                    groupResultModel.MinGpa = group.MinGpa != null ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "GpaRequired").Replace("{0}", Math.Round(group.MinGpa.Value, 3).ToString("0.000")) : null;
                    groupResultModel.MinInstitutionalCredits = group.MinInstitutionalCredits != null ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "GroupMinimumInstitutionCredits").Replace("{0}", Math.Round(group.MinInstitutionalCredits.Value, 2).ToString("0.00")) : null;

                    if (groupResult != null)
                    {
                        groupResultModel.Gpa = (groupResult.Gpa == null) ? "" : GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "CurrentGpa").Replace("{0}", Math.Round((decimal)groupResult.Gpa, 3).ToString("0.000"));
                        groupResultModel.GpaDisplayClass = groupResult.MinGpaIsNotMet ? "gpa-warning" : string.Empty;
                        groupResultModel.InstitutionalCredits = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "GroupInstitutionCreditsApplied").Replace("{0}", Math.Round(groupResult.InstitutionalCredits, 2).ToString("0.00"));
                        groupResultModel.InstitutionalCreditsDisplayClass = groupResult.MinInstitutionalCreditsIsNotMet ? "inst-credits-warning" : string.Empty;
                        groupResultModel.CompletionStatus = groupResult.CompletionStatus;
                        groupResultModel.PlanningStatus = groupResult.PlanningStatus;
                        groupResultModel.ModificationMessages = groupResult.ModificationMessages;

                        var appliedAcademicCreditIds=groupResult.AppliedAcademicCredits.Select(ar => ar.AcademicCreditId).ToList<string>();
                        foreach (var credit in academicCredits.Where(ac => appliedAcademicCreditIds.Contains(ac.Id)))
                        {
                            var acadresult = groupResult.AppliedAcademicCredits.FirstOrDefault(ar => ar.AcademicCreditId == credit.Id);
                            bool isExtraCourse=acadresult!=null?(acadresult.Explanation==AcadResultExplanation.Extra)?true:false  :false;
                            //If this academic credit is also in the ForceAppliedAcademicCreditIds list need to add the override notation
                            if (groupResult.ForceAppliedAcademicCreditIds.Contains(credit.Id))
                            {
                                var overrideCredit = new AcademicCreditModel(credit);
                                overrideCredit.AllowedByOverride = true;
                                overrideCredit.IsExtraCourse = isExtraCourse;
                                groupResultModel.AppliedAcademicCredits.Add(overrideCredit);
                            }
                            else
                            {
                                var appliedAcadCredit = new AcademicCreditModel(credit);
                                appliedAcadCredit.IsExtraCourse = isExtraCourse;
                                groupResultModel.AppliedAcademicCredits.Add(appliedAcadCredit);
                            }

                        }

                        // These items that are included for GPA are not really applied, but are to display with the group nonetheless.
                        foreach (var credit in academicCredits.Where(ac => groupResult.AcademicCreditIdsIncludedInGPA.Contains(ac.Id)))
                        {
                            credit.IsGpaOnlyCredit = true;
                            groupResultModel.AcademicCreditsIncludedInGPA.Add(credit);
                        }

                        var appliedPlannedCourseIds = groupResult.AppliedPlannedCourses.Select(ar => ar.CourseId).ToList<string>();
                            foreach (var credit in plannedCourses.Where(pc => appliedPlannedCourseIds.Contains(pc.CourseId)))
                        {
                            var acadresult = groupResult.AppliedPlannedCourses.FirstOrDefault(ar => ar.CourseId == credit.CourseId);
                            bool isExtraCourse = acadresult != null ? (acadresult.Explanation == AcadResultExplanation.Extra) ? true : false : false;
                            var appliedAcadCredit = new AcademicCreditModel(credit);
                            appliedAcadCredit.IsExtraCourse = isExtraCourse;
                            groupResultModel.AppliedPlannedCourses.Add(appliedAcadCredit);
                        }
                        if (groupResult.Explanations != null && groupResult.Explanations.Any())
                        {
                            foreach (var explanation in groupResult.Explanations)
                            {
                                var addExplanation = string.Empty;
                                if (explanation == GroupExplanation.MinDepartments)
                                {
                                    addExplanation = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "MinimumDepartmentsNotMet");
                                }
                                if (explanation == GroupExplanation.MinSubjects)
                                {
                                    addExplanation = (GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "MinimumSubjectsNotMet"));
                                }
                                if (!string.IsNullOrEmpty(addExplanation))
                                {
                                    groupResultModel.AdditionalExplanations = string.IsNullOrEmpty(groupResultModel.AdditionalExplanations) ? addExplanation : groupResultModel.AdditionalExplanations + " " + addExplanation;
                                }
                            }
                        }
                    }

                    // remove pseudo courses that do not have applied credits in this group result
                    if (groupResultModel.Courses.Where(c => c.IsPseudoCourse).Count() > 0)
                    {
                        // LINQ = remove all that are pseudo and do not have applied credits
                        groupResultModel.Courses.RemoveAll(c => c.IsPseudoCourse &&
                            (groupResultModel.AppliedAcademicCredits.Where(ac => ac.CourseId == c.Id).Count() == 0));
                    }
                    if (groupResultModel.FromCourses.Where(c => c.IsPseudoCourse).Count() > 0)
                    {
                        groupResultModel.FromCourses.RemoveAll(c => c.IsPseudoCourse &&
                            (groupResultModel.AppliedAcademicCredits.Where(ac => ac.CourseId == c.Id).Count() == 0));
                    }
                    if (groupResultModel.ButNotCourses.Where(c => c.IsPseudoCourse).Count() > 0)
                    {
                        groupResultModel.ButNotCourses.RemoveAll(c => c.IsPseudoCourse &&
                            (groupResultModel.AppliedAcademicCredits.Where(ac => ac.CourseId == c.Id).Count() == 0));
                    }

                    // Calculate the group result searchText, which needs to be the same as the requirement the user sees on My Progress:
                    //   If this is the only group in the subrequirement and print text is defined on the subrequirement, show both the
                    //       print text and the group text
                    //   Otherwise, show the group text only.
                    groupResultModel.SearchText = (groupResultModel.IsSingleGroupWithPrintText == true) ? Subreqmodel.DisplayText + groupResultModel.Directive : groupResultModel.Directive;
                    if (groupResultModel.AppliedAcademicCredits != null)
                    {
                        groupResultModel.AppliedAcademicCredits = groupResultModel.AppliedAcademicCredits.OrderBy(a => a.IsExtraCourse).ToList<AcademicCreditModel>();
                    }
                    if (groupResultModel.AppliedPlannedCourses != null)
                    {
                        groupResultModel.AppliedPlannedCourses = groupResultModel.AppliedPlannedCourses.OrderBy(a => a.IsExtraCourse).ToList<AcademicCreditModel>();
                    }
                    Subreqmodel.Groups.Add(groupResultModel);
                }

                Subrequirements.Add(Subreqmodel);
            }
        }
    }
}
