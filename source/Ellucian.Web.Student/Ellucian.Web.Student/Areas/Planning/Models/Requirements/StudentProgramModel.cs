﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Adapters;


namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    public class StudentProgramModel
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Catalog { get; set; }
        public string Description { get; set; }
        public List<string> Departments { get; set; }
        public IEnumerable<string> Majors { get; set; }
        public IEnumerable<string> Minors { get; set; }
        public IEnumerable<string> Ccds { get; set; }
        public IEnumerable<string> Specializations { get; set; }
        public string Degree { get; set; }

        public decimal MinimumCredits { get; set; }
        public decimal MinimumInstitutionalCredits { get; set; }

        public decimal CompletedCredits { get; set; }
        public decimal CompletedInstitutionalCredits { get; set; }
        public decimal InProgressCredits { get; set; }
        public decimal InProgressInstitutionalCredits { get; set; }
        public decimal PlannedCredits { get; set; }
        public decimal PlannedInstitutionalCredits { get; set; }

        public decimal TotalCredits
        {
            get { return CompletedCredits + InProgressCredits + PlannedCredits; }
        }
        public decimal TotalInstitutionalCredits
        {
            get { return CompletedInstitutionalCredits + InProgressInstitutionalCredits + PlannedInstitutionalCredits; }
        }

        public string InstitutionalCreditsModificationMessage { get; set; }
        public string InstitutionalGpaModificationMessage { get; set; }
        public string OverallCreditsModificationMessage { get; set; }
        public string OverallGpaModificationMessage { get; set; }

        public string CumulativeGpa { get; set; }
        public bool IsCumulativeGpaTooLow { get; set; }

        public string InstitutionGpa { get; set; }
        public bool IsInstitutionGpaTooLow { get; set; }

        public string MinimumOverallGpa { get; set; }
        public string MinimumInstitutionGpa { get; set; }

        public List<RequirementResultModel> Requirements { get; set; }

        public bool Proposed { get; set; }

        public List<AcademicCreditModel> NotAppliedAcademicCredits { get; set; }
        public List<AcademicCreditModel> NotAppliedPlannedCredits { get; set; }

        // The following three fields used to display the progress for each related program in the View a New Program (what-if) dialog.
        public int RequiredRequirementCount { get; set; }
        public int CompletedRequirementCount { get; set; }

        public decimal CompletedRequirementPercent
        {
            get
            {
                // Calculate a completed percent for sorting and progress display
                if (RequiredRequirementCount == 0)
                {
                    return 0m;
                }
                else
                {
                    return Math.Round((decimal)CompletedRequirementCount / (decimal)RequiredRequirementCount * 100m);
                }
            }
        }

        public string CompletionProgressText
        {
            get
            {
                if (CompletedRequirementPercent > 95)
                {
                    return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "ProgressBarNearly100Percent");
                }
                if (CompletedRequirementPercent > 50)
                {
                    return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "ProgressBarGreaterThan50Percent");
                }
                if (CompletedRequirementPercent > 0)
                {
                    return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "ProgressBarBetweenZeroAnd50Percent");
                }
                return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "ProgressBarAtZero");
            }
        }

        /// <summary>
        /// Boolean indicates if a disclaimer is to be displayed instead of description
        /// </summary>
        public bool HasDisclaimer
        {
            get
            {
                return (GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "DisclaimerText")).Length > 0;
            }
        }

        /// <summary>
        /// Notices to this student and program
        /// </summary>
        public IEnumerable<EvaluationNotice> Notices { get; set; }

        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        public StudentProgramModel(IAdapterRegistry adapterRegistry, Program program, StudentProgram studentProgram, ProgramEvaluation3 result, IEnumerable<AcademicCreditModel> academicCredits, IEnumerable<AcademicCreditModel> plannedCourses, IEnumerable<Course2> courses, IEnumerable<Subject> subjects, IEnumerable<Department> departments, bool proposed = false)
        {
            _adapterRegistry = adapterRegistry;

            Code = program.Code;
            Title = program.Title;

            Proposed = proposed;

            Catalog = result.CatalogCode;
            Description = program.Description;
            Departments = new List<string>();
            if (program.Departments != null)
            {
                foreach (var dept in program.Departments)
                {
                    var department = departments.FirstOrDefault(x => x.Code == dept);
                    Departments.Add(department == null ? dept : department.Description);
                }
            }
            Majors = program.Majors;
            Minors = program.Minors;
            Ccds = program.Ccds;
            Specializations = program.Specializations;
            Degree = program.Degree;
            Notices = new List<EvaluationNotice>();

            // These may have been modified by Exception
            MinimumCredits = result.ProgramRequirements.MinimumCredits;
            MinimumInstitutionalCredits = result.ProgramRequirements.MinimumInstitutionalCredits;

            MinimumOverallGpa = decimal.Round(result.ProgramRequirements.MinOverallGpa, 3, MidpointRounding.AwayFromZero).ToString("0.000");
            MinimumInstitutionGpa = decimal.Round(result.ProgramRequirements.MinInstGpa, 3, MidpointRounding.AwayFromZero).ToString("0.000");

            CompletedCredits = result.Credits;
            CompletedInstitutionalCredits = result.InstitutionalCredits;

            InProgressCredits = result.InProgressCredits;
            InProgressInstitutionalCredits = result.InProgressCredits;

            PlannedCredits = result.PlannedCredits;
            PlannedInstitutionalCredits = result.PlannedCredits;

            CumulativeGpa = (result.CumGpa != null) ? decimal.Round((decimal)result.CumGpa, 3, MidpointRounding.AwayFromZero).ToString("0.000") : string.Empty;
            InstitutionGpa = (result.InstGpa != null) ? decimal.Round((decimal)result.InstGpa, 3, MidpointRounding.AwayFromZero).ToString("0.000") : string.Empty;

            IsCumulativeGpaTooLow = (result.CumGpa ?? 0) < result.ProgramRequirements.MinOverallGpa;
            IsInstitutionGpaTooLow = (result.InstGpa ?? 0) < result.ProgramRequirements.MinInstGpa;

            InstitutionalCreditsModificationMessage = result.InstitutionalCreditsModificationMessage;
            InstitutionalGpaModificationMessage = result.InstitutionalGpaModificationMessage;
            OverallCreditsModificationMessage = result.OverallCreditsModificationMessage;
            OverallGpaModificationMessage = result.OverallGpaModificationMessage;

            // Pull out the planned courses that were not applied to any requirement
            NotAppliedPlannedCredits = new List<AcademicCreditModel>();
            foreach (var plannedCredit in result.OtherPlannedCredits)
            {
                var pc = plannedCourses.Where(p => p.CourseId == plannedCredit.CourseId && p.Term == plannedCredit.TermCode).FirstOrDefault();
                if (pc != null) { NotAppliedPlannedCredits.Add(pc); }
            }

            // Identify the planned courses that were applied, and send only those into the model build. ("plannedCourses" is the
            // unfiltered degree plan list; we want only the subset included in the evaluation.)
            var appliedPlannedCredits = new List<AcademicCreditModel>();
            var plannedCredits = result.RequirementResults.SelectMany(rr => rr.SubrequirementResults.SelectMany(sr => sr.GroupResults.SelectMany(gr => gr.AppliedPlannedCredits)));
            foreach (var plannedCredit in plannedCredits)
            {
                var pc = plannedCourses.Where(p => p.CourseId == plannedCredit.CourseId && p.Term == plannedCredit.TermCode).FirstOrDefault();
                if (pc != null) { appliedPlannedCredits.Add(pc); }
            }
            appliedPlannedCredits = appliedPlannedCredits.Distinct().ToList();

            // Use join to get the academic credits that were not applied to any requirement.
            NotAppliedAcademicCredits = (from acadCredId in result.OtherAcademicCredits
                                         join acadCredit in academicCredits
                                            on acadCredId equals acadCredit.Id
                                         select acadCredit).ToList();

            Requirements = new List<RequirementResultModel>();
            foreach (var req in result.ProgramRequirements.Requirements)
            {
                Requirements.Add(new RequirementResultModel(_adapterRegistry, result, req, academicCredits, appliedPlannedCredits, courses, subjects, departments));
            }

            if (studentProgram != null)
            {
                foreach (var req in studentProgram.AdditionalRequirements)
                {
                    // Additional Requirements frequently have an associated major, minor, specialization, or certificate,
                    // so add those to the program summary (user doesn't care that these are "additions", only that the extra is listed
                    if (req.Type == AwardType.Major)
                    {
                        Majors = Majors.Union(new List<string>() { req.AwardName });
                    }
                    if (req.Type == AwardType.Minor)
                    {
                        Minors = Minors.Union(new List<string>() { req.AwardName });
                    }
                    if (req.Type == AwardType.Specialization)
                    {
                        Specializations = Specializations.Union(new List<string>() { req.AwardName });
                    }
                    if (req.Type == AwardType.Ccd)
                    {
                        Ccds = Ccds.Union(new List<string>() { req.AwardName });
                    }

                    if (req.Requirement != null)
                    {
                        Requirements.Add(new RequirementResultModel(_adapterRegistry, result, req.Requirement, academicCredits, appliedPlannedCredits, courses, subjects, departments));


                        // Get the evaluation result for the additional requirement, and increment the completed and required counts accordingly
                        var additionalResult = result.RequirementResults.FirstOrDefault(x => x.RequirementId == req.Requirement.Id);
                        if (additionalResult != null)
                        {
                            foreach (var subreqResult in additionalResult.SubrequirementResults.OrderByDescending(sr => sr.CompletionStatus))
                            {
                                // Get the subrequirement that goes with this subrequirement result
                                var subreq = req.Requirement.Subrequirements.Where(s => s.Id == subreqResult.SubrequirementId).FirstOrDefault();
                                // Get the number of required groups
                                int requiredGroupCount = (subreq.MinGroups.HasValue && subreq.MinGroups > 0) ? subreq.MinGroups.GetValueOrDefault() : subreq.Groups.Count();

                                // Calculate the number of subrequirements still needed to complete the requirement (remaining)
                                var requiredSubreqCount = (req.Requirement.MinSubRequirements.HasValue && req.Requirement.MinSubRequirements > 0) ? req.Requirement.MinSubRequirements.GetValueOrDefault() : req.Requirement.Subrequirements.Count();
                                var completedSubreqCount = additionalResult.SubrequirementResults.Where(sr => sr.CompletionStatus == CompletionStatus.Waived || sr.CompletionStatus == CompletionStatus.Completed).Count();
                                // Reduce completed subrequirement count to the required count if completed is higher than required (shouldn't happen unless all waived)
                                completedSubreqCount = (completedSubreqCount > requiredSubreqCount) ? requiredSubreqCount : completedSubreqCount;
                                // Determine the number of subrequirements remaining, used for looping below
                                var remainingSubreqCount = requiredSubreqCount - completedSubreqCount;

                                // If the additional requirement is complete or waived, include all required groups and consider all required groups complete.
                                if (additionalResult.CompletionStatus == CompletionStatus.Completed || additionalResult.CompletionStatus == CompletionStatus.Waived || additionalResult.CompletionStatus == CompletionStatus.Waived)
                                {
                                    RequiredRequirementCount += requiredGroupCount;
                                    CompletedRequirementCount += requiredGroupCount;
                                }
                                // If more subrequirements are needed to complete this requirement
                                else if (remainingSubreqCount > 0)
                                {
                                    // Add this subrequirement's groups to the required count
                                    RequiredRequirementCount += requiredGroupCount;
                                    // As long as the requirement and the subrequirement GPA both are above the minimum GPA, Add the number of completed/waived groups (up to the required number) to the Completed count
                                    // If either one is below the required GPA level, do not consider any groups complete.
                                    if (!additionalResult.MinGpaIsNotMet && !subreqResult.MinGpaIsNotMet && !additionalResult.MinInstitutionalCreditsIsNotMet && !subreqResult.MinInstitutionalCreditsIsNotMet)
                                    {
                                        var completedGroupCount = subreqResult.GroupResults.Where(gr => gr.CompletionStatus == CompletionStatus.Waived || gr.CompletionStatus == CompletionStatus.Completed).Count();
                                        // reduce completed group count to the required group count if it ends up higher
                                        completedGroupCount = (completedGroupCount > requiredGroupCount) ? requiredGroupCount : completedGroupCount;
                                        // Add to the total number of complete groups                                
                                        CompletedRequirementCount += completedGroupCount;

                                        // Reduce the remaining subrequirement count so we only count the groups for the necessary number of subrequirements.
                                        remainingSubreqCount -= 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Count all completed groups, reqs and subreqs.
            // Start by looping through each requirement result
            foreach (var reqResult in result.RequirementResults)
            {
                // Get the requirement that goes with this requirement result
                var req = result.ProgramRequirements.Requirements.Where(r => r.Id == reqResult.RequirementId).FirstOrDefault();
                if (req != null)
                {
                    // Calculate the number of subrequirements still needed to complete the requirement (remaining)
                    var requiredSubreqCount = (req.MinSubRequirements.HasValue && req.MinSubRequirements > 0) ? req.MinSubRequirements.GetValueOrDefault() : req.Subrequirements.Count();
                    var completedSubreqCount = reqResult.SubrequirementResults.Where(sr => sr.CompletionStatus == CompletionStatus.Waived || sr.CompletionStatus == CompletionStatus.Completed).Count();
                    // Reduce completed subrequirement count to the required count if completed is higher than required (shouldn't happen unless all waived)
                    completedSubreqCount = (completedSubreqCount > requiredSubreqCount) ? requiredSubreqCount : completedSubreqCount;
                    // Determine the number of subrequirements remaining, used for looping below
                    var remainingSubreqCount = requiredSubreqCount - completedSubreqCount;

                    // Loop through each subrequirement, in descending sequence of completion, therefore processing partially completed subreqs before not started subreqs
                    foreach (var subreqResult in reqResult.SubrequirementResults.OrderByDescending(sr => sr.CompletionStatus))
                    {
                        // Get the subrequirement that goes with this subrequirement result
                        var subreq = req.Subrequirements.Where(s => s.Id == subreqResult.SubrequirementId).FirstOrDefault();
                        // Get the number of required groups
                        int requiredGroupCount = (subreq.MinGroups.HasValue && subreq.MinGroups > 0) ? subreq.MinGroups.GetValueOrDefault() : subreq.Groups.Count();

                        // If the requirement or sub-requirement is complete or waived, include all required groups and consider all required groups complete.
                        if (subreqResult.CompletionStatus == CompletionStatus.Completed || subreqResult.CompletionStatus == CompletionStatus.Waived || reqResult.CompletionStatus == CompletionStatus.Waived)
                        {
                            RequiredRequirementCount += requiredGroupCount;
                            CompletedRequirementCount += requiredGroupCount;
                        }

                        // If more subrequirements are needed to complete this requirement
                        else if (remainingSubreqCount > 0)
                        {
                            // Add this subrequirement's groups to the required count
                            RequiredRequirementCount += requiredGroupCount;
                            // As long as the requirement and the subrequirement GPA both are above the minimum GPA, Add the number of completed/waived groups (up to the required number) to the Completed count
                            // If either one is below the required GPA level, do not consider any groups complete.
                            if (!reqResult.MinGpaIsNotMet && !subreqResult.MinGpaIsNotMet && !reqResult.MinInstitutionalCreditsIsNotMet && !subreqResult.MinInstitutionalCreditsIsNotMet)
                            {
                                var completedGroupCount = subreqResult.GroupResults.Where(gr => gr.CompletionStatus == CompletionStatus.Waived || gr.CompletionStatus == CompletionStatus.Completed).Count();
                                // reduce completed group count to the required group count if it ends up higher
                                completedGroupCount = (completedGroupCount > requiredGroupCount) ? requiredGroupCount : completedGroupCount;
                                // Add to the total number of complete groups                                
                                CompletedRequirementCount += completedGroupCount;

                                // Reduce the remaining subrequirement count so we only count the groups for the necessary number of subrequirements.
                                remainingSubreqCount -= 1;
                            }
                        }
                    }
                }
            }
        }
    }
}

