﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Requirements
{
    public class GroupResultModel : GroupDetailModel
    {
        public CompletionStatus CompletionStatus { get; set; }
        public PlanningStatus PlanningStatus { get; set; }

        public List<AcademicCreditModel> AppliedAcademicCredits { get; set; }
        public List<AcademicCreditModel> AcademicCreditsIncludedInGPA { get; set; }
        public List<AcademicCreditModel> AppliedPlannedCourses { get; set; }

        public List<string> ModificationMessages { get; set; }

        /// <summary>
        /// This is the list of FromCourses and Courses that are still remaining to be planned/taken
        /// </summary>
        public List<RequirementCourseModel> CoursesThatNeedPlanned
        {
            get
            {
                var courses = new List<RequirementCourseModel>();

                foreach (var course in this.Courses)
                {
                    if (!AppliedAcademicCredits.Any(ac => ac.CourseId == course.Id || course.EquatedCourseIds.Contains(ac.CourseId)) && !AppliedPlannedCourses.Any(pc => pc.CourseId == course.Id || course.EquatedCourseIds.Contains(pc.CourseId)))
                    {
                        courses.Add(course);
                    }
                }
                foreach (var course in this.FromCourses)
                {
                    if (!AppliedAcademicCredits.Any(ac => ac.CourseId == course.Id || course.EquatedCourseIds.Contains(ac.CourseId)) && !AppliedPlannedCourses.Any(pc => pc.CourseId == course.Id || course.EquatedCourseIds.Contains(pc.CourseId)))
                    {
                        courses.Add(course);
                    }
                }
                return courses;
            }
        }

        // The requirement to which this group is attached
        public string RequirementCode { get; set; }
        // The subrequirement to which this group is attached
        public string SubrequirementId { get; set; }

        public string Gpa { get; set; }
        public string MinGpa { get; set; }

        public string InstitutionalCredits { get; set; }
        public string MinInstitutionalCredits { get; set; }

        public string GpaDisplayClass { get; set; }
        public string InstitutionalCreditsDisplayClass { get; set; }

        public string StatusDirective
        {
            get
            {
                var creditsOrCourses = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Courses");
                if (CompletionStatus == CompletionStatus.Waived) return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "UiWaived");

                decimal numToComplete = 0;
                decimal numCompleted = 0;

                if (MinCourses.HasValue && MinCourses > 0)
                {
                    numToComplete = MinCourses.Value;
                    numCompleted = AppliedAcademicCredits.Count(cred => cred.IsCompletedCredit);
                }
                else if (MinCredits.HasValue && MinCredits > 0)
                {
                    creditsOrCourses = GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Credits");
                    numToComplete = MinCredits.Value;
                    numCompleted = (from cred in AppliedAcademicCredits where (cred.IsCompletedCredit) select cred.Credit).Sum();
                }
                else if (Courses.Count > 0)
                {
                    numToComplete = Courses.Count;
                    numCompleted = AppliedAcademicCredits.Count(cred => cred.IsCompletedCredit);
                }
                else if (FromCourses.Count > 0)
                {
                    numToComplete = FromCourses.Count;
                    numCompleted = AppliedAcademicCredits.Count(cred => cred.IsCompletedCredit);
                }
                else
                {
                    numToComplete = 0;
                    numCompleted = 0;
                }

                // 2 of 1 completed makes sense to a programmer but not to a student.
                if (numCompleted > numToComplete) { numCompleted = numToComplete; }
                string stringNumCompleted = "0";
                string stringNumToComplete = "0";
                if (numToComplete > 0 )
                {
                    stringNumToComplete = numToComplete.ToString(".#####");
                }
                if (numCompleted > 0)
                {
                    stringNumCompleted = numCompleted.ToString(".#####");
                }
                return stringNumCompleted + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "OfLabel") + " " + stringNumToComplete + " " + creditsOrCourses + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "Completed") + ".";
            }
        }

        public string PlanningDirective
        {
            get
            {
                // If not completed, also add directive statement regarding planned status.
                if (CompletionStatus != CompletionStatus.Completed)
                {
                    if (PlanningStatus == PlanningStatus.CompletelyPlanned)
                    {
                        return GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "FullyPlanned");
                    }
                }
                // Return empty string if no planning found
                return string.Empty;
            }
        }

        public string AdditionalExplanations { get; set; }
    }
}