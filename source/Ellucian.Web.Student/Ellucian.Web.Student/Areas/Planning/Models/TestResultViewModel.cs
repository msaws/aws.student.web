﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models
{
    public class TestResultViewModel
    {
        /// <summary>
        /// List of Admission type tests taken by a student
        /// </summary>
        public List<TestScoreViewModel> AdmissionTests { get; set; }
        /// <summary>
        /// List of Placement type of tests taken by a student
        /// </summary>
        public List<TestScoreViewModel> PlacementTests { get; set; }
        /// <summary>
        /// List of Other tests taken by a student
        /// </summary>
        public List<TestScoreViewModel> OtherTests { get; set; }

        /// <summary>
        /// Creates a new TestResultViewModel instance.
        /// </summary>
        public TestResultViewModel()
        {
            AdmissionTests = new List<TestScoreViewModel>();
            PlacementTests = new List<TestScoreViewModel>();
            OtherTests = new List<TestScoreViewModel>();
        }

        /// <summary>
        /// Creates a fully built TestResultViewModel from a list of TestResult DTOs, Test DTOs, and NonCourseStatus DTOs.
        /// </summary>
        /// <param name="testResultDtos"></param>
        /// <param name="testDtos"></param>
        /// <param name="statuses"></param>
        public TestResultViewModel(IEnumerable<TestResult2> testResultDtos, IEnumerable<Test> testDtos, IEnumerable<NoncourseStatus> statuses)
        {
            AdmissionTests = new List<TestScoreViewModel>();
            PlacementTests = new List<TestScoreViewModel>();
            OtherTests = new List<TestScoreViewModel>();
            // Order the results first
            IEnumerable<TestResult2> orderedTestResultDtos = testResultDtos.OrderBy(t => t.Description);
            foreach (var test in orderedTestResultDtos)
            {
                List<TestScoreViewModel> testScores = new List<TestScoreViewModel>();
                // Create the test score view models
                TestScoreViewModel testScore = new TestScoreViewModel(test.Description, test.DateTaken);
                testScore.Percentile = test.Percentile;
                testScore.Score = CreditsFormatter.FormattedTestScoreString(test.Score);
                if (!string.IsNullOrEmpty(testScore.Score) && !string.IsNullOrEmpty(test.Code))
                {
                    var testDto = testDtos.Where(d => d.Code == test.Code).FirstOrDefault();
                    if (testDto != null && testDto.MaximumScore != null && testDto.MaximumScore > 0)
                    {
                        testScore.Score += " of " + testDto.MaximumScore.ToString();
                    }
                }
                testScore.Status = string.Empty;
                testScore.StatusDate = test.StatusDate == null ? string.Empty : test.StatusDate.GetValueOrDefault().ToShortDateString();
                testScore.StatusType = "empty";
                if (!string.IsNullOrEmpty(test.StatusCode))
                {
                    var testStatus = statuses.Where(c => c.Code == test.StatusCode).FirstOrDefault();
                    if (testStatus != null)
                    {
                        testScore.Status = testStatus.Description;
                        testScore.StatusType = testStatus.StatusType.ToString().ToLower();

                    }
                }
                testScore.TestType = "maintest";
                testScores.Add(testScore);
                if (test.SubTests != null && test.SubTests.Count() > 0)
                {
                    IEnumerable<SubTestResult2> orderedSubTestResults = test.SubTests.OrderBy(s => s.Description);
                    foreach (var subtest in orderedSubTestResults)
                    {
                        // Create the subtest test score view models
                        TestScoreViewModel subTestScore = new TestScoreViewModel(subtest.Description, subtest.DateTaken);
                        subTestScore.Percentile = subtest.Percentile;
                        subTestScore.Score = CreditsFormatter.FormattedTestScoreString(subtest.Score);
                        if (!string.IsNullOrEmpty(subTestScore.Score) && !string.IsNullOrEmpty(subtest.Code))
                        {
                            var subtestDto = testDtos.Where(d => d.Code == subtest.Code).FirstOrDefault();
                            if (subtestDto != null && subtestDto.MaximumScore != null && subtestDto.MaximumScore > 0)
                            {
                                subTestScore.Score += " of " + subtestDto.MaximumScore.ToString();
                            }
                        }
                        subTestScore.Status = string.Empty;
                        subTestScore.StatusDate = subtest.StatusDate == null ? string.Empty : subtest.StatusDate.GetValueOrDefault().ToShortDateString();
                        subTestScore.StatusType = "empty";
                        if (!string.IsNullOrEmpty(subtest.StatusCode))
                        {
                            var subtestStatus = statuses.Where(c => c.Code == subtest.StatusCode).FirstOrDefault();
                            if (subtestStatus != null)
                            {
                                subTestScore.Status = subtestStatus.Description;
                                subTestScore.StatusType = subtestStatus.StatusType.ToString().ToLower();
                            }
                        }
                        subTestScore.TestType = "subtest";
                        testScores.Add(subTestScore);
                    }
                }
                // Finally add all of these into the appropriate list within the view model.
                // Using the category of the main TestResult DTO to determine which list they all go into.
                if (test.Category == TestType.Admissions)
                {
                    AdmissionTests.AddRange(testScores);
                }
                else if (test.Category == TestType.Placement)
                {
                    PlacementTests.AddRange(testScores);
                }
                else
                {
                    OtherTests.AddRange(testScores);
                }
            }
        }
    }
}