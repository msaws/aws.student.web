﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Utility;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class FacultySectionDetailModel
    {
        /// <summary>
        /// Gets or sets the id of the section.
        /// </summary>
        public string SectionId { get; set; }
        /// <summary>
        /// Requisite models for those requisites that are "Previous" or "Previous or Concurrent"
        /// </summary>
        public List<FacultySectionRequisiteModel> CoursePrerequisites { get; set; }
        /// <summary>
        /// Requisite models for those requisites that are "Concurrent"
        /// </summary>
        public List<FacultySectionRequisiteModel> CourseCorequisites { get; set; }
        /// <summary>
        /// Student Waiver models 
        /// </summary>
        public List<StudentWaiverModel> StudentWaivers { get; set; }
        /// <summary>
        /// Indicates whether the faculty should be allowed to add a new waiver
        /// </summary>
        public bool CanFacultyAddWaivers { get; set; }
        /// <summary>
        /// List of waiver reasons that can be used when adding a new waiver
        /// </summary>
        public List<StudentWaiverReason> WaiverReasons { get; set; }

        public FacultySectionDetailModel(string sectionId, IEnumerable<StudentWaiverReason> allReasons)
            
        {
            SectionId = sectionId;
            CoursePrerequisites = new List<FacultySectionRequisiteModel>();
            CourseCorequisites = new List<FacultySectionRequisiteModel>();
            StudentWaivers = new List<StudentWaiverModel>();
            CanFacultyAddWaivers = false;
            WaiverReasons = allReasons != null ? allReasons.ToList() : new List<StudentWaiverReason>();
        }

    }
}
