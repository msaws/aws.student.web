﻿// Copyright 2012 - 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Utility;
using System.Collections.ObjectModel;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class FacultyTermModel
    {
        /// <summary>
        /// Term code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Term Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The faculty Section models associated with this term
        /// </summary>
        private readonly List<FacultySectionModel> sections = new List<FacultySectionModel>();
        public ReadOnlyCollection<FacultySectionModel> Sections { get; private set; }

        public FacultyTermModel(string code, string description) {

            Code = code;
            Description = description;
            Sections = this.sections.AsReadOnly();
        }

        public void AddSections(IEnumerable<FacultySectionModel> sections)
        {
            if (sections == null)
            {
                throw new ArgumentNullException("sections");
            }
            if (sections.Count() == 0)
            {
                throw new ArgumentOutOfRangeException("sections", "Parameter 'sections' must contain items to be added.");
            }
            this.sections.AddRange(sections);
        }

    }
}