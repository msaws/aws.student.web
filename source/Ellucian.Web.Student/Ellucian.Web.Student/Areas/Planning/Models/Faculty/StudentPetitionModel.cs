﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    /// <summary>
    /// model for petition - it is the same used for student petitions and faculty consents
    /// </summary>
    public class StudentPetitionModel
    {
        /// <summary>
        /// Gets or sets the name of the student formatted in [LFM.] format.
        /// </summary>
        public string DisplayNameLfm { get; set; }

        /// <summary>
        /// Id of the student for whom the petition is for
        /// </summary>
        public string StudentId { get; set; }

        /// <summary>
        /// Petition Id
        /// </summary>
        public string PetitionId { get; set; }

        /// <summary>
        /// Section Id that student belongs to
        /// </summary>
        public string SectionId { get; set; }

        /// <summary>
        /// Section Number for the course
        /// </summary>
        public string Section { get; set; }
        /// <summary>
        /// Status  for this petition 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Name of the person who authorized the petition
        /// </summary>
        public string UpdatedBy { get; set; }

        /// <summary>
        /// Date and Time the petition was updated
        /// </summary>
        public string UpdatedOn { get; set; }

        /// <summary>
        /// code of the petition reason - translated from the code
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Comments of the petition if provided
        /// </summary>
        public string FreeformExplanation { get; set; }

        /// <summary>
        /// Term Description
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// Start Date - only displayed when term is not provided
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// End Date - only used with start date when Term is not provided
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// Course Id
        /// </summary>
        public string Course { get; set; }

    }
}