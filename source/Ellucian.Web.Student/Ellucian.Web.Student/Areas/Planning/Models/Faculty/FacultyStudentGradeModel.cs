﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class FacultyStudentGradeModel
    {
        public string SectionId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string LastName { get; private set; }
        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public List<string> DisplayNameLfm { get; set; }
        public bool? NeverAttended { get; set; }
        public string LastDateAttended { get; set; }
        public string FinalGrade { get; set; }
        public string GradeExpireDate { get; set; }
        public string AcademicCreditStatus { get; set; }
        public string CreditsCeus { get; set; }
        public bool? IsGradeVerified { get; set; }
        public List<string> VerifyGradeResponseErrors { get; set; }
        // Separating midterm grades into separate sortable properties which simplies the hiding of the columns as well.
        public string Midterm1Grade { get; set; }
        public string Midterm2Grade { get; set; }
        public string Midterm3Grade { get; set; }
        public string Midterm4Grade { get; set; }
        public string Midterm5Grade { get; set; }
        public string Midterm6Grade { get; set; }
        //class levels
        public string ClassLevelDescription { get; set; }
        public int? ClassLevelOrder { get; set; }

        public FacultyStudentGradeModel(string sectionId, Colleague.Dtos.Student.AcademicCredit2 academicCredit, Colleague.Dtos.Student.StudentBatch3 student, IEnumerable<Colleague.Dtos.Student.Grade> grades, Colleague.Dtos.Student.Section3 crossListedSection, bool hasVerifiedGrade, IEnumerable<string> classLevelCodes, IEnumerable<ClassLevel> classLevels)
            : this()
        {
            if (academicCredit != null)
            {
                DisplayNameLfm = new List<string>();
                StudentId = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "UnknownName");
                StudentName = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "UnknownName");
                this.SectionId = sectionId;
                if (student != null)
                {
                    StudentId = string.IsNullOrEmpty(student.Id) ? StudentId : student.Id;
                    var displayNameResult = NameHelper.GetDisplayNameResult(student.LastName, student.FirstName, student.MiddleName, student.PersonDisplayName, NameFormats.LastFirstMiddleInitial);
                    StudentName = displayNameResult.FullName;
                    LastName = displayNameResult.LastName;
                    FirstName = displayNameResult.FirstName;
                    MiddleName = displayNameResult.MiddleName;
                    //class levels
                    if (student.ClassLevelCodes != null && student.ClassLevelCodes.Any())
                    {
                        List<ClassLevel> classLevelDetails = student.ClassLevelCodes.Select(c => classLevels.Where(cl => cl.Code == c).FirstOrDefault()).ToList();
                        string classlevelDescription = string.Join(",", classLevelDetails.Where(c => c != null && !string.IsNullOrEmpty(c.Description)).Select(c => c.Description));
                        int? maxClassLevelSortOrder = classLevelDetails.Where(c => c != null).Select(c => c.SortOrder).Max();
                        this.ClassLevelDescription = classlevelDescription;
                        this.ClassLevelOrder = maxClassLevelSortOrder;
                    }
                }
                if (academicCredit.Status == "Dropped")
                {
                    StudentName = StudentName + GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "DroppedAcademicCreditDisplayText");
                }
                if (academicCredit.Status == "Withdrawn")
                {
                    StudentName = StudentName + GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "WithdrawnAcademicCreditDisplayText");
                }
                DisplayNameLfm.Add(StudentName);

                if (crossListedSection != null)
                {
                    DisplayNameLfm.Add(GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "CrosslistedDisplayText") + crossListedSection.CourseName + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + crossListedSection.Number);
                }
                NeverAttended = academicCredit.NeverAttended;
                LastDateAttended = academicCredit.LastAttendanceDate.HasValue ? academicCredit.LastAttendanceDate.Value.ToShortDateString() : string.Empty;
                var displayGradeId = string.IsNullOrEmpty(academicCredit.VerifiedGradeId) ? academicCredit.FinalGradeId : academicCredit.VerifiedGradeId;
                if (!string.IsNullOrEmpty(displayGradeId))
                {
                    var grade = grades.Where(g => g.Id == displayGradeId).FirstOrDefault();
                    FinalGrade = grade != null ? grade.LetterGrade : string.Empty;
                    GradeExpireDate = academicCredit.FinalGradeExpirationDate.HasValue ? academicCredit.FinalGradeExpirationDate.Value.ToShortDateString() : string.Empty;
                }

                CreditsCeus = CreditsFormatter.FormattedCreditsCeusString(academicCredit.Credit, academicCredit.ContinuingEducationUnits);

                AcademicCreditStatus = academicCredit.Status;
                IsGradeVerified = hasVerifiedGrade;
                if (academicCredit.MidTermGrades != null)
                {
                    foreach (var midtermGrade in academicCredit.MidTermGrades)
                    {
                        if (midtermGrade != null)
                        {
                            var grade = grades.Where(g => g.Id == midtermGrade.GradeId).FirstOrDefault();
                            if (grade != null)
                            {
                                switch (midtermGrade.Position)
                                {
                                    case 1:
                                        Midterm1Grade = grade.LetterGrade;
                                        break;
                                    case 2:
                                        Midterm2Grade = grade.LetterGrade;
                                        break;
                                    case 3:
                                        Midterm3Grade = grade.LetterGrade;
                                        break;
                                    case 4:
                                        Midterm4Grade = grade.LetterGrade;
                                        break;
                                    case 5:
                                        Midterm5Grade = grade.LetterGrade;
                                        break;
                                    case 6:
                                        Midterm6Grade = grade.LetterGrade;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                    }
                }

            }

        }
        public FacultyStudentGradeModel()
        {
            VerifyGradeResponseErrors = new List<string>();
            DisplayNameLfm = new List<string>();
        }

    }


}