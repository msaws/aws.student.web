﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class StudentWaiverModel
    {
        /// <summary>
        /// Gets or sets the name of the student formatted in [LFM.] format.
        /// </summary>
        public string DisplayNameLfm { get; set; }

        /// <summary>
        /// Id of the student for whom the waiver is for
        /// </summary>
        public string StudentId { get; set; }

        /// <summary>
        /// Name of the person who authorized the waiver
        /// </summary>
        public string AuthorizedBy { get; set; }
        
        /// <summary>
        /// Date and Time the waiver was updated
        /// </summary>
        public string UpdatedOn { get; set; }

        /// <summary>
        /// Description of the waiver reason - translated from the code
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Freeform explanation of the waiver if provided
        /// </summary>
        public string FreeformExplanation { get; set; }

        /// <summary>
        /// Indicates whether the waiver has been revoked by the registrar and is no longer in effect
        /// </summary>
        public bool IsRevoked { get; set; }

        public StudentWaiverModel()
        {
            // For clarity, showing that IsRevoke will default to false.
            IsRevoked = false;
        }

    }
}