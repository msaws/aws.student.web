﻿// Copyright 2012 - 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Models.Courses;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    /// <summary>
    /// to map to header for section
    /// </summary>
    public class FacultySectionModel
    {
        /// <summary>
        /// Gets or sets the id of the section.
        /// </summary>
        public string SectionId { get; set; }

        /// <summary>
        /// Gets or sets a fully-formatted string for displaying this section in a UI (such as MATH-100-01).
        /// </summary>
        public string FormattedNameDisplay { get; set; }

        /// <summary>
        /// Gets or sets a fully-formatted string for displaying the section term.
        /// </summary>
        public string TermName { get; set; }

        /// <summary>
        /// Gets or sets the date on which this section completes.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the date on which this section begins.
        /// </summary>
        public DateTime StartDate { get; set; }
                
        /// <summary>
        /// Gets or sets the Location value to be displayed for this section.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the list of meeting time objects for this section.
        /// </summary>
        public List<CourseSearchResultMeetingTime> Meetings { get; set; }

        public FacultySectionModel(Section3 section, Dictionary<string, Location> allLocations, Dictionary<string, Building> allBuildings, Dictionary<string, Room> allRooms, Dictionary<string, InstructionalMethod> allInstrMethods, Dictionary<string,Term> allTerms)
        {
            if (section == null)
            {
                throw new ArgumentNullException("section", "section must not be null");
            }
            this.SectionId = section.Id;
            this.EndDate = section.EndDate;
            this.StartDate = section.StartDate;
            this.FormattedNameDisplay = section.CourseName + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + section.Number + ": " + section.Title;
            if (allLocations != null && allLocations.Count() > 0)
            {
                this.Location = string.IsNullOrEmpty(section.Location) ? string.Empty : allLocations.ContainsKey(section.Location) ? allLocations[section.Location].Description : string.Empty;
            }
            if (allTerms != null && allTerms.Count() > 0)
            {
                this.TermName = string.IsNullOrEmpty(section.TermId) ? string.Empty : allTerms.ContainsKey(section.TermId) ? allTerms[section.TermId].Description : string.Empty;
            }
            this.Meetings = new List<CourseSearchResultMeetingTime>();
            
            foreach (SectionMeeting2 mt in section.Meetings)
            {
                CourseSearchResultMeetingTime csrmt = new CourseSearchResultMeetingTime(mt);
                if (mt.Room != null && allRooms != null && allRooms.ContainsKey(mt.Room))
                {
                    Room r = allRooms[mt.Room];
                    if (allBuildings != null && allBuildings.ContainsKey(r.BuildingCode))
                    {
                        Building b = allBuildings[r.BuildingCode];
                        csrmt.BuildingDisplay = b.Description;
                    }
                    csrmt.RoomDisplay = r.Code;
                }
                if (allInstrMethods != null)
                {
                    csrmt.InstructionalMethodDisplay = allInstrMethods.ContainsKey(mt.InstructionalMethodCode) ? allInstrMethods[mt.InstructionalMethodCode].Description : mt.InstructionalMethodCode;
                }
                this.Meetings.Add(csrmt);
            }
        }
    }
}