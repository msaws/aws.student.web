﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class FacultySectionRequisiteModel
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        public FacultySectionRequisiteModel() { }

        /// <summary>
        /// Needed to match up prerequisites with the requisite waivers. If one doesn't have this it is a co-requisite type that we don't care about matching up anyway.
        /// </summary>
        public string RequirementCode { get; set; }
        /// <summary>
        /// Needed to match up prerequisites with the requisite waivers. 
        /// </summary>
        public bool IsRequired { get; set; }
        /// <summary>
        /// Root text to display for this requisite. Describes what the requisite is about.
        /// </summary>
        public string DisplayText { get; set; }
        /// <summary>
        /// Indicates if requisite is required. Will contain "Required" or "Recommended" verbiage based on the resource file.
        /// </summary>
        public string EnforcementText { get; set; }
        /// <summary>
        /// Indicates type of the requisite
        /// </summary>
        public RequisiteCompletionOrder CompletionOrder { get; set; }
        /// <summary>
        /// Displays the completion order display
        /// </summary>
        public string CompletionOrderDisplay { get; set; }
    }
}