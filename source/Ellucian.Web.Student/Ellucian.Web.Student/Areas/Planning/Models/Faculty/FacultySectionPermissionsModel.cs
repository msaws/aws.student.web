﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    /// <summary>
    /// model for faculty section permissions that contains student petitions and faculty consents.
    /// Both the collection are of same type StudentPetitionModel
    /// </summary>
    public class FacultySectionPermissionsModel
    {
        /// <summary>
        /// List of Faculty Consents for a section
        /// </summary>
        public List<StudentPetitionModel> FacultyConsents { get; set; }
        /// <summary>
        /// List of Student Petitions for a section
        /// </summary>
        public List<StudentPetitionModel> StudentPetitions { get; set; }
        /// <summary>
        /// List of all student petition statuses that can be used when adding a new student petition or faculty consent
        /// </summary>
        public List<PetitionStatus> PetitionStatuses { get; set; }
        /// <summary>
        /// List of all student petition reasons that can be used when adding a new student petition or faculty consent
        /// </summary>
        public List<StudentPetitionReason> PetitionReasons { get; set; }
        /// <summary>
        /// Indicates whether the faculty should be allowed to add a new faculty consent
        /// </summary>
        public bool CanFacultyAddFacultyConsents { get; set; }
        /// <summary>
        /// Indicates whether the faculty should be allowed to add a new student petition
        /// </summary>
        public bool CanFacultyAddStudentPetitions { get; set; }

        public FacultySectionPermissionsModel(IEnumerable<PetitionStatus> petitionStatuses, IEnumerable<StudentPetitionReason> petitionReasons)
        {
            FacultyConsents = new List<StudentPetitionModel>();
            StudentPetitions = new List<StudentPetitionModel>();
            PetitionStatuses = petitionStatuses != null ? petitionStatuses.ToList() : new List<PetitionStatus>();
            PetitionReasons = petitionReasons != null ? petitionReasons.ToList() : new List<StudentPetitionReason>();
            CanFacultyAddFacultyConsents = false;
            CanFacultyAddStudentPetitions = false;
        }
    }
}