﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    /// <summary>
    /// A simple view model with a formatted name (LFM) and email of the student
    /// </summary>
    public class StudentModel
    {
        /// <summary>
        /// Formatted name display to be used for the student (in LFM format)
        /// </summary>
        public string NameDisplay { get; set; }
        /// <summary>
        /// The email the student has identified as their preferred email address
        /// </summary>
        public string PreferredEmail { get; set; }
        /// <summary>
        /// Id of the student
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Constructor for the student model using a student DTO
        /// </summary>
        /// <param name="student"></param>
        public StudentModel(Ellucian.Colleague.Dtos.Planning.PlanningStudent student)
        {
            if (student != null)
            {
                this.PreferredEmail = student.PreferredEmailAddress;
                var displayNameResult = NameHelper.GetDisplayNameResult(student.LastName, student.FirstName, student.MiddleName, student.PersonDisplayName, NameFormats.LastFirstMiddleInitial);
                this.NameDisplay = displayNameResult.FullName;
                this.Id = student.Id;
            }
        }
    }
}