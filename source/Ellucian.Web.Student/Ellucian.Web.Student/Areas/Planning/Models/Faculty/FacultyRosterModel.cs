﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class FacultyRosterModel
    {
        /// <summary>
        /// This is preferred email address of the faculty. 
        /// this will be for the logged in faculty
        /// </summary>
        public string PreferredEmailAddress { get; set; }
        public List<FacultyRosterStudentModel> RosterStudents { get; set; }
    
        public FacultyRosterModel()
        {
            RosterStudents = new List<FacultyRosterStudentModel>();
        }
    }

}