﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class FacultyRosterStudentModel
    {
        public string StudentId { get; set; }
        public string DisplayNameLfm { get; set; }
        public string ClassLevel { get; set; }
        public string NameSort1 { get; set; }
        public string NameSort2 { get; set; }
        public string NameSort3 { get; set; }
        public string PreferredEmail { get; set; }

        public FacultyRosterStudentModel(Colleague.Dtos.Student.StudentBatch3 student, IEnumerable<Colleague.Dtos.Student.ClassLevel> classLevels)
        {
            if (student == null)
            {
                throw new ArgumentNullException("student", "Student is required.");
            }
            StudentId = student.Id;
            var displayNameResult = NameHelper.GetDisplayNameResult(student.LastName, student.FirstName, student.MiddleName, student.PersonDisplayName, NameFormats.LastFirstMiddleInitial);
            DisplayNameLfm = displayNameResult.FullName;
            NameSort1 = !string.IsNullOrEmpty(displayNameResult.LastName) ? displayNameResult.LastName : displayNameResult.FullName;
            NameSort2 = displayNameResult.FirstName;
            NameSort3 = displayNameResult.MiddleName;
            foreach (var classLevelCode in student.ClassLevelCodes)
            {
                string classLevelDesc = null;
                if (!string.IsNullOrEmpty(classLevelCode) && classLevels != null)
                {
                    var classLevel = classLevels.Where(c => c.Code == classLevelCode).FirstOrDefault();
                    classLevelDesc = (classLevel != null) ? classLevel.Description : classLevelCode;
                }
                else
                {
                    classLevelDesc = classLevelCode;
                }
                ClassLevel = (string.IsNullOrEmpty(ClassLevel)) ? classLevelDesc : ClassLevel + ", " + classLevelDesc;
            }
            PreferredEmail = student.PreferredEmailAddress;
        }
    }
}