﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Planning.Models.Faculty
{
    public class FacultyStudentGradesModel
    {
        public List<FacultyStudentGradeModel> StudentGrades { get; set; }
        /// <summary>
        /// Tuple with gradeScheme as Item1 and initial grade value as Item2
        /// </summary>
        public List<Tuple<string,string,bool>> GradeTypes { get; set; }
        /// <summary>
        /// if the user have permission to edit grades
        /// </summary>
        public DateTime? SectionStartDate { get; set; }
        public DateTime? SectionEndDate { get; set; }
        public bool CanVerifyGrades { get; set; }
        /// <summary>
        /// is the term available for grading
        /// </summary>
        public bool IsTermOpen { get; set; }
        public string SectionId { get; set; }
        public int NumberOfMidtermGradesToShow { get; set; }
        /// <summary>
        /// Tuple with a fixed code (O,F, etc) in Item1 and description in Item2
        /// </summary>
        public List<Tuple<string, string>> MobileGradeViews { get; set; }
    
        public FacultyStudentGradesModel()
        {
            StudentGrades = new List<FacultyStudentGradeModel>();
            MobileGradeViews = new List<Tuple<string, string>>() { Tuple.Create<string, string>("O", GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "GradingOverviewTab")) };
            GradeTypes = new List<Tuple<string, string,bool>>();
            CanVerifyGrades = false;
            IsTermOpen = false;
            NumberOfMidtermGradesToShow = 0;
        }
       
    }
}