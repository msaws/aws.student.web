﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.Advising;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Utility;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Areas.Planning.Utility
{
    /// <summary>
    /// Provides utility functionality for retrieving and organizing sets of data to be used in construction
    /// and manipulation of the degree plan.
    /// </summary>
    public class DegreePlanModelBuilder
    {
        private readonly ILogger _logger;
        private ColleagueApiClient _client;
        private List<Advisor> advisorDtos = new List<Advisor>();

        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        public DegreePlanModelBuilder(IAdapterRegistry adapterRegistry, ColleagueApiClient client, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _logger = logger;
            _client = client;
        }

        /// <summary>
        /// Builds a set of all terms that exist as planning terms in the database or as existing
        /// planned terms on the active student's plan. For example, if FA12 is on a student's plan (even if it
        /// is not a planning term per the database data) and SP13 is a planning term in the database, the final
        /// Dictionary constructed here would contain both terms. Put another way, this list contains every term
        /// that a student may have on their plan or can add to their plan.
        /// </summary>
        /// <param name="degreePlan">The active student's DegreePlan object</param>
        /// <param name="academicHistory">The active student's past academic history data, which may include other terms not on the plan</param>
        /// <returns>A set of all plan-able and planned terms</returns>
        private async Task<IEnumerable<Term>> GetAvailablePlanTermsAsync(DegreePlan4 degreePlan, AcademicHistory3 academicHistory)
        {
            if (degreePlan == null)
            {
                throw new ArgumentNullException("degreePlan");
            }
            if (academicHistory == null)
            {
                throw new ArgumentNullException("academicHistory");
            }

            List<Term> availablePlanTerms = new List<Term>();
            availablePlanTerms.AddRange(await _client.GetCachedPlanningTermsAsync());

            IEnumerable<Term> allTerms = null;
            if (degreePlan.Terms != null)
            {
                foreach (var term in degreePlan.Terms)
                {
                    if (!availablePlanTerms.Any(t => t.Code == term.TermId))
                    {
                        try
                        {
                            if (allTerms == null)
                            {
                                allTerms =await _client.GetCachedTermsAsync();
                            }
                            availablePlanTerms.Add(allTerms.Where(t => t.Code == term.TermId).First());
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex, "Unable to get term: " + term);
                        }
                    }
                }
            }

            if (academicHistory.AcademicTerms != null)
            {
                foreach (var term in academicHistory.AcademicTerms)
                {

                    if (!availablePlanTerms.Any(t => t.Code == term.TermId))
                    {
                        try
                        {
                            if (allTerms == null)
                            {
                                allTerms = await _client.GetCachedTermsAsync();
                            }
                            availablePlanTerms.Add(allTerms.Where(t => t.Code == term.TermId).First());
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex, "Unable to get term: " + term);
                        }
                    }
                }
            }

            return availablePlanTerms.Distinct();
        }

        /// <summary>
        /// Given a DegreePlan object, builds and returns a list of distinct associated course IDs.
        /// </summary>
        /// <param name="degreePlan">A DegreePlan with any number of associated DegreePlanTerm objects</param>
        /// <returns>The list of all distinct course IDs</returns>
        private IEnumerable<string> GetPlannedCourseIds(DegreePlan4 degreePlan)
        {
            if (degreePlan == null)
            {
                throw new ArgumentNullException("degreePlan");
            }

            //enumerate all of the courses that they have planned
            List<string> courseIds = new List<string>();

            //courses on the degree plan, both in terms and non-term
            foreach (var termCourse in degreePlan.Terms)
            {
                if (termCourse.PlannedCourses.Count > 0)
                {
                    foreach (var plannedcourse in termCourse.PlannedCourses)
                    {
                        if (!string.IsNullOrEmpty(plannedcourse.CourseId) && !courseIds.Contains(plannedcourse.CourseId))
                        {
                            courseIds.Add(plannedcourse.CourseId);
                        }
                    }
                }
            }
            foreach (var pc in degreePlan.NonTermPlannedCourses)
            {
                if (!string.IsNullOrEmpty(pc.CourseId) && !courseIds.Contains(pc.CourseId))
                {
                    courseIds.Add(pc.CourseId);
                }
            }

            return courseIds.Distinct();
        }

        /// <summary>
        /// Given an academic history object, builds and returns a list of distinct associated course IDs.
        /// </summary>
        /// <param name="academicHistory">Academic history objects</param>
        /// <returns>The list of all distinct course IDs</returns>
        private IEnumerable<string> GetAcademicHistoryCourseIds(AcademicHistory3 academicHistory)
        {
            if (academicHistory == null)
            {
                throw new ArgumentNullException("academicHistory");
            }

            //enumerate all of the courses that they have taken
            List<string> courseIds = new List<string>();

            //courses in the academic history
            if (academicHistory.AcademicTerms != null)
            {
                foreach (AcademicTerm3 term in academicHistory.AcademicTerms)
                {
                    if (term.AcademicCredits.Count() > 0)
                    {
                        foreach (AcademicCredit2 credit in term.AcademicCredits)
                        {
                            if (!string.IsNullOrEmpty(credit.CourseId) && !courseIds.Contains(credit.CourseId))
                            {
                                courseIds.Add(credit.CourseId);
                            }
                        }
                    }
                }
            }
            if (academicHistory.NonTermAcademicCredits != null)
            {
                foreach (AcademicCredit2 credit in academicHistory.NonTermAcademicCredits)
                {
                    if (!string.IsNullOrEmpty(credit.CourseId) && !courseIds.Contains(credit.CourseId))
                    {
                        courseIds.Add(credit.CourseId);
                    }
                }
            }

            return courseIds.Distinct();
        }

        /// <summary>
        /// Given a DegreePlan object, builds and returns a list of distinct associated co-requisite (course or section) course IDs.
        /// </summary>
        /// <param name="degreePlan">A DegreePlan with any number of associated DegreePlanTerm objects</param>
        /// <returns>The list of all distinct co-requisite (course or section) course IDs</returns>
        private async Task<IEnumerable<string>> GetCorequisiteCourseIdsAsync(DegreePlan4 degreePlan)
        {
            if (degreePlan == null)
            {
                throw new ArgumentNullException("degreePlan");
            }

            // get any corequisite courses
            List<string> courseIds = new List<string>();
            List<string> coreqCourses;
            List<string> coreqSections;
            if (GetCorequisiteCourseAndSectionIds(degreePlan, out coreqCourses, out coreqSections))
            {
                if (coreqCourses != null)
                {
                    foreach (string s in coreqCourses)
                    {
                        if (!courseIds.Contains(s))
                        {
                            courseIds.Add(s);
                        }
                    }
                }

                // need to chase-down the course id from the coreq sections
                if ((coreqSections != null) && (coreqSections.Count() > 0))
                {
                    IEnumerable<Section3> sections = await _client.GetSections4Async(coreqSections);
                    if (sections != null && sections.Count() > 0)
                    {
                        foreach (Section3 s in sections)
                        {
                            if (!string.IsNullOrEmpty(s.CourseId))
                            {
                                if (!courseIds.Contains(s.CourseId))
                                {
                                    courseIds.Add(s.CourseId);
                                }
                            }
                        }
                    }
                }
            }

            return courseIds.Distinct();
        }

        /// <summary>
        /// Given a DegreePlan object, builds a set of distinct requisite codes
        /// </summary>
        /// <param name="degreePlan">A degree plan</param>
        /// <returns>A set of requisite codes</returns>
        private IEnumerable<string> GetRequisiteCodes(DegreePlan4 degreePlan)
        {
            if (degreePlan == null)
            {
                throw new ArgumentNullException("degreePlan");
            }

            var requirementCodes = new List<string>();

            foreach (var term in degreePlan.Terms)
            {
                foreach (var course in term.PlannedCourses)
                {
                    var unmetRequisiteCodes = course.Warnings.Where(w => w.Requisite != null).Select(w => w.Requisite.RequirementCode);
                    foreach (var req in unmetRequisiteCodes)
                    {
                        if (!string.IsNullOrEmpty(req))
                        {
                            if (!requirementCodes.Contains(req))
                            {
                                requirementCodes.Add(req);
                            }

                        }
                    }
                }
            }
            foreach (var course in degreePlan.NonTermPlannedCourses)
            {
                var unmetRequisiteCodes = course.Warnings.Where(w => w.Requisite != null).Select(w => w.Requisite.RequirementCode);
                foreach (var req in unmetRequisiteCodes)
                {
                    if (!string.IsNullOrEmpty(req))
                    {
                        if (!requirementCodes.Contains(req))
                        {
                            requirementCodes.Add(req);
                        }
                    }
                }
            }

            return requirementCodes.Distinct();
        }

        /// <summary>
        /// Get any course and section ids identified on the plan as co-requisites to a planned course/section.
        /// </summary>
        /// <param name="degreePlan">The DegreePlan object</param>
        /// <param name="courseIds">List of course ids identified by the co-requisite course or section.</param>
        /// <param name="sectionIds"></param>
        /// <returns>True if any co-requisites were found, false if none were found.</returns>
        private bool GetCorequisiteCourseAndSectionIds(DegreePlan4 degreePlan, out List<string> courseIds, out List<string> sectionIds)
        {
            List<string> courses = new List<string>();
            List<string> sections = new List<string>();

            if (degreePlan != null)
            {
                // non-term
                if (degreePlan.NonTermPlannedCourses != null && degreePlan.NonTermPlannedCourses.Count > 0)
                {
                    foreach (var pc in degreePlan.NonTermPlannedCourses)
                    {
                        if (pc.Warnings != null)
                        {
                            // courses
                            var cRequisites = pc.Warnings.Where(ww => ww.Requisite != null).Select(w => w.Requisite).ToList();
                            if (cRequisites != null && cRequisites.Count() > 0)
                            {
                                var requisiteCourseIds = cRequisites.Where(rr => !string.IsNullOrEmpty(rr.CorequisiteCourseId)).Select(r => r.CorequisiteCourseId).ToList();
                                if (requisiteCourseIds != null && requisiteCourseIds.Count() > 0)
                                {
                                    foreach (string id in requisiteCourseIds)
                                    {
                                        if (!courses.Contains(id))
                                        {
                                            courses.Add(id);
                                        }
                                    }
                                }
                            }

                            // sections
                            var sMessages = pc.Warnings.Where(x => !string.IsNullOrEmpty(x.SectionId)).Select(x => x.SectionId);
                            if (sMessages.Count() > 0)
                            {
                                foreach (string id in sMessages)
                                {
                                    if (!sections.Contains(id))
                                    {
                                        sections.Add(id);
                                    }
                                }
                            }

                            // list of corequisite sections
                            var coreqSectionIds = pc.Warnings.Where(w => string.IsNullOrEmpty(w.SectionId) && w.SectionRequisite != null && w.SectionRequisite.CorequisiteSectionIds != null && w.SectionRequisite.CorequisiteSectionIds.Count() > 0).SelectMany(w => w.SectionRequisite.CorequisiteSectionIds);
                            if (coreqSectionIds != null)
                            {
                                foreach (var id in coreqSectionIds)
                                {
                                    if (!sections.Contains(id))
                                    {
                                        sections.Add(id);
                                    }
                                }
                            }
                        }
                    }
                }

                // term
                if (degreePlan.Terms != null && degreePlan.Terms.Count > 0)
                {
                    foreach (var dpt in degreePlan.Terms)
                    {
                        if (dpt != null && dpt.PlannedCourses.Count > 0)
                        {
                            foreach (var pc in dpt.PlannedCourses)
                            {
                                if (pc.Warnings != null)
                                {
                                    // courses
                                    var cRequisites = pc.Warnings.Where(ww => ww.Requisite != null).Select(w => w.Requisite).ToList();
                                    if (cRequisites != null && cRequisites.Count() > 0)
                                    {
                                        var requisiteCourseIds = cRequisites.Where(rr => !string.IsNullOrEmpty(rr.CorequisiteCourseId)).Select(r => r.CorequisiteCourseId).ToList();
                                        if (requisiteCourseIds != null && requisiteCourseIds.Count() > 0)
                                        {
                                            foreach (string id in requisiteCourseIds)
                                            {
                                                if (!courses.Contains(id))
                                                {
                                                    courses.Add(id);
                                                }
                                            }
                                        }
                                    }

                                    // sections
                                    var sMessages = pc.Warnings.Where(x => !string.IsNullOrEmpty(x.SectionId)).Select(x => x.SectionId);
                                    if (sMessages.Count() > 0)
                                    {
                                        foreach (string id in sMessages)
                                        {
                                            if (!sections.Contains(id))
                                            {
                                                sections.Add(id);
                                            }
                                        }
                                    }

                                    // list of corequisite sections
                                    var coreqSectionIds = pc.Warnings.Where(w => string.IsNullOrEmpty(w.SectionId) && w.SectionRequisite != null && w.SectionRequisite.CorequisiteSectionIds != null && w.SectionRequisite.CorequisiteSectionIds.Count() > 0).SelectMany(w => w.SectionRequisite.CorequisiteSectionIds);
                                    if (coreqSectionIds != null)
                                    {
                                        foreach (var id in coreqSectionIds)
                                        {
                                            if (!sections.Contains(id))
                                            {
                                                sections.Add(id);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            courseIds = courses;
            sectionIds = sections;
            return (courseIds.Count > 0 || sectionIds.Count > 0);
        }

        /// <summary>
        /// Creates a full DegreePlanModel from the provided DegreePlan, studentId, and optional set of Notifications.
        /// </summary>
        /// <param name="degreePlan">The student's degree plan DTO - required</param>
        /// <param name="student">The student DTO - required</param>
        /// <param name="notifications">An optional set of Notifications</param>
        /// <param name="academicHistory">An optional academic history record</param>
        /// <param name="retrieveNotes">A flag indicating whether to retrieve the advising notes, which may not be needed in some scenarios</param>
        /// <returns>The full DegreePlanModel after hydration</returns>
        public async Task<DegreePlanModel> BuildDegreePlanModelAsync(DegreePlan4 degreePlan, Ellucian.Colleague.Dtos.Planning.PlanningStudent student, List<Notification> notifications = null, AcademicHistory3 academicHistory = null, bool retrieveNotes = true)
        {
            if (student == null)
            {
                throw new ArgumentNullException("student", "Must provide a student.");
            }
            if (degreePlan == null)
            {
                throw new ArgumentNullException("degreePlan", "Must provide a degree plan.");
            }
            _logger.Info("Begin building degree plan model for plan: " + degreePlan.Id);
            _logger.Info("Build DegreePlanModel for " + student.Id);

            //get academic history
            if (academicHistory == null)
            {
                academicHistory = await _client.GetAcademicHistory3Async(student.Id);
                _logger.Info("Had to go to API to get academic history; not passed into BuildDegreePlanModel.");
            }

            //enumerate all of the courses that they have planned OR that are on their academic history OR are related to requisites.
            // courseIds is the full list.
            var plannedCourseIds = GetPlannedCourseIds(degreePlan);
            _logger.Info("Course Ids found on degree plan for student (" + student.Id + "): " + string.Join(",", plannedCourseIds));

            var corequisiteCourseIds = await GetCorequisiteCourseIdsAsync(degreePlan);
            _logger.Info("Corequisite course Ids found on degree plan for student (" + student.Id + "): " + string.Join(",", corequisiteCourseIds));

            var academicHistoryCourseIds = GetAcademicHistoryCourseIds(academicHistory);
            _logger.Info("Course Ids found on academic history for student (" + student.Id + "): " + string.Join(",", academicHistoryCourseIds));

            var courseIds = plannedCourseIds.Concat(corequisiteCourseIds).Concat(academicHistoryCourseIds).Distinct();

            if (notifications == null)
            {
                notifications = new List<Notification>();
            }

            // Get the list of courses through the qapi endpoint
            var courses = new List<Course2>();
            if (courseIds.Count() > 0)
            {
                try
                {
                    courses = (await _client.QueryCourses2Async(new CourseQueryCriteria() { CourseIds = courseIds })).ToList();
                    // Determine the courses that could not be found
                    var notFoundCourseIds = courseIds.Except(courses.Select(c => c.Id));
                    foreach (var notFoundCourseId in notFoundCourseIds)
                    {
                        var message = "Unable to read course " + notFoundCourseId;
                        _logger.Error("BuildDegreePlanModel: " + message);

                        // Add a notification, because the if student has planned a course that can't be found, they probably should know about it
                        notifications.Add(new Notification(message, NotificationType.Error, false));
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error retrieving courses for courseids " + courseIds + ": " + ex.Message);
                }
            }

            var registrationTerms = await _client.GetCachedRegistrationTermsAsync();
            _logger.Info("Registration terms found: " + string.Join(", ", registrationTerms.Select(t => t.Code)));

            // Get the section DTOs for any sections found on the plan or academic history
            var cachedSectionIds = new List<string>();
            var uncachedSectionIds = new List<string>();
            var sections = new List<Section3>();
            // First go through the nonterm sections on the plan.
            foreach (var plannedCourse in degreePlan.NonTermPlannedCourses)
            {
                if (!string.IsNullOrEmpty(plannedCourse.CourseId) && !string.IsNullOrEmpty(plannedCourse.SectionId))
                {
                    // Non-term section info will be pulled uncached since we don't know the term.
                    uncachedSectionIds.Add(plannedCourse.SectionId);
                }

                // Also, for EVERY PlannedCourse on the plan, if that PlannedCourse has any attached warnings that provide a section ID 
                // (whether corequisite section, or recommended section) then we need to also tack that warning's section ID onto the list to retrieve
                var requiredSectionsWarnings = plannedCourse.Warnings.Where(m => m.SectionRequisite != null);
                if (requiredSectionsWarnings.Count() > 0)
                {
                    foreach (var warning in requiredSectionsWarnings)
                    {
                        if (warning != null && !string.IsNullOrEmpty(warning.SectionId))
                        {
                            cachedSectionIds.Add(warning.SectionId);
                        }
                        if (warning != null && string.IsNullOrEmpty(warning.SectionId) && warning.SectionRequisite != null && warning.SectionRequisite.CorequisiteSectionIds != null && warning.SectionRequisite.CorequisiteSectionIds.Count() > 0)
                        {
                            cachedSectionIds.AddRange(warning.SectionRequisite.CorequisiteSectionIds);
                        }
                    }
                }
            }
            // Now go through the regular terms
            foreach (var term in degreePlan.Terms)
            {
                if (term.PlannedCourses != null)
                {
                    foreach (var plannedCourse in term.PlannedCourses)
                    {
                        if (!string.IsNullOrEmpty(plannedCourse.CourseId) && !string.IsNullOrEmpty(plannedCourse.SectionId))
                        {
                            // If the term is a registration term, get non-cached (live) section data
                            // Otherwise, get the cached section
                            if (registrationTerms.Any(t => t.Code == term.TermId))
                            {
                                uncachedSectionIds.Add(plannedCourse.SectionId);
                            }
                            else
                            {
                                cachedSectionIds.Add(plannedCourse.SectionId);
                            }
                        }

                        // Also, for EVERY PlannedCourse on the plan, if that PlannedCourse has any attached warnings that provide a section ID 
                        // (whether corequisite section, or recommended section) then we need to also tack that warning's section ID onto the list to retrieve
                        var requiredSectionsWarnings = plannedCourse.Warnings.Where(m => m.SectionRequisite != null);
                        if (requiredSectionsWarnings.Count() > 0)
                        {
                            foreach (var warning in requiredSectionsWarnings)
                            {
                                if (warning != null && !string.IsNullOrEmpty(warning.SectionId))
                                {
                                    cachedSectionIds.Add(warning.SectionId);
                                }
                                if (warning != null && string.IsNullOrEmpty(warning.SectionId) && warning.SectionRequisite != null && warning.SectionRequisite.CorequisiteSectionIds != null && warning.SectionRequisite.CorequisiteSectionIds.Count() > 0)
                                {
                                    cachedSectionIds.AddRange(warning.SectionRequisite.CorequisiteSectionIds);
                                }
                            }
                        }
                    }
                }
            }

            if (academicHistory != null && academicHistory.AcademicTerms != null)
            {
                // for the nonterm credits
                foreach (var credit in academicHistory.NonTermAcademicCredits)
                {
                    if (credit != null && !string.IsNullOrEmpty(credit.CourseId) && !string.IsNullOrEmpty(credit.SectionId))
                    {
                        cachedSectionIds.Add(credit.SectionId);
                    }
                }
                // for the regular credits
                foreach (var term in academicHistory.AcademicTerms)
                {
                    if (term.AcademicCredits != null)
                    {
                        foreach (var credit in term.AcademicCredits)
                        {
                            if (credit != null && !string.IsNullOrEmpty(credit.CourseId) && !string.IsNullOrEmpty(credit.SectionId))
                            {
                                cachedSectionIds.Add(credit.SectionId);
                            }
                        }
                    }
                }
            }

            // Get fresh section data and section registration dates for the uncached sectionIDs.
            // These ids are actually the sections that are in terms open for registration and therefore the only ones that are eligibile for registering.
            IEnumerable<SectionRegistrationDate> sectionRegistrationDates = new List<SectionRegistrationDate>();
            if (uncachedSectionIds.Count() > 0)
            {
                // Make sure we don't get any dupes
                uncachedSectionIds = uncachedSectionIds.Distinct().ToList();
                try
                {
                    sections.AddRange(await _client.GetSections4Async(uncachedSectionIds.ToList(), false));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Unable to get sections: " + string.Join(", ", uncachedSectionIds));
                }
                try
                {
                    sectionRegistrationDates = await _client.GetSectionRegistrationDatesAsync(uncachedSectionIds);
                }
                catch (Exception dex)
                {
                    _logger.Error(dex, "Unable to get section registration dates for the sections on the plan: " + string.Join(", ", uncachedSectionIds));
                }
            }

            // Get cached section data for the cached sectionIds
            if (cachedSectionIds.Count() > 0)
            {
                //don't need to get cached version of a section that we just got uncached
                cachedSectionIds = cachedSectionIds.Except(uncachedSectionIds).ToList();

                cachedSectionIds = cachedSectionIds.Distinct().ToList();

                // Get cached section data for those that are not plannable.
                if (cachedSectionIds.Count > 0)
                {
                    sections.AddRange(await _client.GetSections4Async(cachedSectionIds, true));
                }
            }

            // Faculty for the retrieved sections
            var faculty = new List<Faculty>();

            // get the Ids for the faculty in all sections
            var facultyIds = sections.SelectMany(s => s.FacultyIds);
            try
            {
                faculty = (await _client.QueryFacultyAsync(new FacultyQueryCriteria() { FacultyIds = facultyIds })).ToList();
                var notFoundFacultyIds = facultyIds.Except(faculty.Select(f => f.Id));
                foreach (var facId in notFoundFacultyIds)
                {
                    _logger.Error("Unable to get faculty: " + facId);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }

            var roomTask = _client.GetCachedRoomsAsync();
            var buildingTask = _client.GetCachedBuildingsAsync();
            var instructionalMethodsTask = _client.GetCachedInstructionalMethodsAsync();
            var gradesTask = _client.GetCachedGradesAsync();
            var locationsTask = _client.GetCachedLocationsAsync();
            var advisorTypesTask = _client.GetCachedAdvisorTypesAsync();

            await Task.WhenAll(roomTask,buildingTask,instructionalMethodsTask,gradesTask,locationsTask,advisorTypesTask);

            var rooms = roomTask.Result;
            var buildings = buildingTask.Result;
            var instructionalMethods =instructionalMethodsTask.Result;
            var grades = gradesTask.Result;
            var locations = locationsTask.Result;
            var advisorTypes = advisorTypesTask.Result;

            _logger.Info("Rooms found: " + string.Join(", ", rooms.Select(r => r.Code)));
            _logger.Info("Buildings found: " + string.Join(", ", buildings.Select(b => b.Code)));
            _logger.Info("Instructional methods found: " + string.Join(", ", instructionalMethods.Select(i => i.Code)));
            _logger.Info("Grades found: " + string.Join(", ", grades.Select(i => i.Id)));
            _logger.Info("Locations found: " + string.Join(", ", locations.Select(l => l.Code)));
            _logger.Info("Advisor Types found: " + string.Join(", ", advisorTypes.Select(at => at.Code)));

            var availablePlanTerms = await GetAvailablePlanTermsAsync(degreePlan, academicHistory);
            _logger.Info("Available terms found on degree plan: " + string.Join(", ", availablePlanTerms.Select(t => t.Code)));

            //gather all of the Requirements we'll need for warnings
            var requisiteCodes = GetRequisiteCodes(degreePlan);
            IEnumerable<Requirement> requirements = new List<Requirement>();
            if (requisiteCodes != null && requisiteCodes.Count() > 0)
            {
                requirements = await _client.QueryRequirementsAsync(requisiteCodes.ToList());
            }
            // allRequisites is keyed by requisite code, and the value is the text representation of the requisite
            Dictionary<string, string> allRequisites = new Dictionary<string, string>();
            foreach (string code in requisiteCodes)
            {
                var requisite = requirements.Where(r => r.Code == code).FirstOrDefault();

                if (requisite != null)
                {
                    allRequisites.Add(requisite.Code, (string.IsNullOrEmpty(requisite.DisplayText) ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "DefaultRequisiteText") : requisite.DisplayText));
                }
            }

            // pre-retrieve advisors
            var advisorIds = new List<string>() { degreePlan.LastReviewedAdvisorId };
            advisorIds.AddRange(student.AdvisorIds);
            advisorIds.AddRange(degreePlan.Notes.Where(n => n.PersonId != null).Select(n => n.PersonId));
            advisorDtos =(await _client.QueryAdvisorsAsync(advisorIds.Distinct().ToList())).ToList();

            // Get the name of the advisor who last reviewed the plan
            Advisor lastReviewedBy = null;
            if (!string.IsNullOrEmpty(degreePlan.LastReviewedAdvisorId))
            {
                try
                {
                    lastReviewedBy = advisorDtos.Where(a => a.Id == degreePlan.LastReviewedAdvisorId).First();
                }
                catch (AdvisingException)
                {
                    // no-op - just ignore that we can't get the advisor's name.
                    _logger.Warn("Advisor cannot be retreived for this plans LastReviewdAdvisorId (" + degreePlan.LastReviewedAdvisorId + ")");
                }
            }

            // Get the student's advisement information for the Contact Advisor actions.
            List<Advisor> studentAdvisors = new List<Advisor>();
            List<Advisement> studentAdvisements = new List<Advisement>();
            if (student.Advisements != null && student.Advisements.Count() > 0)
            {
                foreach (var advisement in student.Advisements)
                {
                    if (advisement.AdvisorId != null)
                    {
                        try
                        {
                            var advisor = advisorDtos.Where(a => a.Id == advisement.AdvisorId).First();
                            if (advisor != null)
                            {
                                studentAdvisements.Add(advisement);
                                studentAdvisors.Add(advisor);
                            }
                        }
                        catch (Exception)
                        {
                            // no-op - just ignore that we can't get the advisor's information
                            _logger.Warn("Advisor " + advisement.AdvisorId + "cannot be retrieved");
                        }
                    }
                }
            }

            DegreePlanModel degreePlanModel = null;
            try
            {
                _logger.Info("All data required for plan has been retreived.  Now call DegreePlanModel constructor...");
                degreePlanModel = new DegreePlanModel(_adapterRegistry, degreePlan, academicHistory, availablePlanTerms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, allRequisites, locations, retrieveNotes ? await GetAdvisingNotesAsync(degreePlan, student) : new List<AdvisingNote>(), lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);

                degreePlanModel.StudentHasAdvisor = student.HasAdvisor;
                _logger.Info(ObjectFormatter.FormatAsXml(degreePlanModel));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }

            return degreePlanModel;
        }

        /// <summary>
        /// Gets a full list of all advising notes that are attached to the given DegreePlan DTO.
        /// </summary>
        /// <param name="plan">The DegreePlan DTO that has advising notes associated with it</param>
        /// <returns>A list of fully-hydrated AdvisingNoteModel objects representing the list of notes attached to the given plan</returns>
        private async Task<IEnumerable<AdvisingNote>> GetAdvisingNotesAsync(DegreePlan4 plan, Ellucian.Colleague.Dtos.Planning.PlanningStudent student)
        {
            List<AdvisingNote> notes = new List<AdvisingNote>();

            if (plan != null && plan.Notes != null && plan.Notes.Count > 0)
            {
                foreach (var n in plan.Notes)
                {
                    string person = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "UnknownPerson");

                    try
                    {
                        //name display format should be Last, First MI
                        if (n.PersonType == PersonType.Advisor)
                        {
                            var p = await RetrieveAdvisorAsync(n.PersonId);
                            if (p != null)
                            {
                                person = NameHelper.PersonDisplayName(p.LastName, p.FirstName, p.MiddleName, NameFormats.LastFirstMiddleInitial);
                            }

                        }
                        else if (n.PersonType == PersonType.Student)
                        {
                            if (student != null)
                            {
                                // Use best display name for student - factoring in the calculated person display name in this case
                                var personNameResult = NameHelper.GetDisplayNameResult(student.LastName, student.FirstName, student.MiddleName, student.PersonDisplayName, NameFormats.LastFirstMiddleInitial);
                                person = personNameResult.FullName;

                            }
                        }
                    }
                    catch
                    {
                        person = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "UnknownPerson");
                    }
                    //the KO binding to show these notes uses html versus text, so these notes MUST be encoded! (but be careful to avoid a double encode)
                    //we cannot allow unencoded HTML because it will be dumped onto the page as part of the JSON-ized result var; HTML chars in it will cause script errors or worse (execution)
                    string encodedNote = System.Web.HttpUtility.HtmlDecode(n.Text);
                    encodedNote = System.Web.HttpUtility.HtmlEncode(encodedNote).Replace("\n", "<br/>");  //textarea does newline as \n
                    notes.Add(new AdvisingNote(encodedNote, person, n.PersonType, n.Date.Value.LocalDateTime));
                }
            }

            notes = notes.OrderByDescending(an => an.Date.HasValue ? an.Date : DateTime.Now).ToList();

            return notes;
        }

        /// <summary>
        /// Retrieves an advisor from a previously retrieved list since it is likely that most of the advisor actions will be by the same person. 
        /// If it fails to find it there it will get it via the service client.
        /// </summary>
        /// <param name="advisorId">Id of the advisor to retrieve</param>
        /// <returns>An Advisor data transfer object</returns>
        private async Task<Advisor> RetrieveAdvisorAsync(string advisorId)
        {
            Advisor advisor = advisorDtos.Where(av => av.Id == advisorId).FirstOrDefault();
            if (advisor == null)
            {
                advisor = await _client.GetAdvisorAsync(advisorId);
                if (advisor != null)
                {
                    advisorDtos.Add(advisor);
                }
            }
            return advisor;
        }

        public async Task<IEnumerable<PlanArchiveModel>> BuildPlanArchiveModelsAsync(IEnumerable<DegreePlanArchive2> archiveDtos, string currentUserId)
        {
            var archives = new List<PlanArchiveModel>();
            Advisee advisee = null;

            // If there are no archives, don't bother trying to build the view models
            if (archiveDtos != null && archiveDtos.Count() > 0)
            {
                var advisorIds = archiveDtos.Where(a => a.CreatedBy != null).Select(a => a.CreatedBy).Distinct().ToList();
                var advisors = await _client.QueryAdvisorsAsync(advisorIds);

                foreach (var archiveDto in archiveDtos.OrderBy(a => a.StudentId))
                {
                    var archive = new PlanArchiveModel(archiveDto.Id, archiveDto.StudentId);

                    Advisor advisor = null;
                    try
                    {
                        advisor = advisors.Where(a => a.Id == archiveDto.CreatedBy).First();
                    }
                    catch
                    {
                        // do nothing, don't care if we can't find the faculty record - we just wanted the name to be nice.
                    }

                    // Get the advisee from the service client only if we don't have this advisee yet
                    if (advisee == null || advisee.Id != archiveDto.StudentId)
                    {
                        try
                        {
                            advisee = await _client.GetAdviseeAsync(currentUserId, archiveDto.StudentId);
                        }
                        catch
                        {
                            _logger.Error("Advisee " + archiveDto.StudentId + " could not be retrieved using advisor " + currentUserId + ". Skipping archive " + archiveDto.Id + ".");
                            continue;
                        }
                    }
                    archive.CreatedBy = (advisor == null) ?
                        archive.CreatedBy = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "UnknownPerson") + " (" + archiveDto.CreatedBy + ") " :
                        archive.CreatedBy = advisor.LastName + ", " + advisor.FirstName;
                    archive.CreatedDate = archiveDto.CreatedDate.LocalDateTime.ToShortDateString() + "  " + archiveDto.CreatedDate.LocalDateTime.ToLongTimeString();
                    archive.SortDate = archiveDto.CreatedDate.LocalDateTime;
                    archive.Title = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ArchivePDFDownloadLink");
                    archives.Add(archive);
                }
            }
            return archives.OrderByDescending(x => x.SortDate);
        }

        public async Task<List<StudentWaiverViewModel>> BuildStudentWaiversAsync(IEnumerable<StudentWaiver> studentWaivers, IEnumerable<Course2> courses, IEnumerable<Section3> sections, IEnumerable<Term> terms)
        {
            List<StudentWaiverViewModel> waivers = new List<StudentWaiverViewModel>();
            if (studentWaivers != null && studentWaivers.Count() > 0)
            {
                foreach (StudentWaiver waiver in studentWaivers)
                {
                    try
                    {
                        if (!waiver.IsRevoked && waiver.RequisiteWaivers != null && waiver.RequisiteWaivers.Any())
                        {
                            var courseDto = !string.IsNullOrEmpty(waiver.CourseId) && courses != null && courses.Any() ? courses.FirstOrDefault(c => c.Id == waiver.CourseId) : null;
                            var sectionDto = !string.IsNullOrEmpty(waiver.SectionId) && sections != null && sections.Any() ? sections.FirstOrDefault(c => c.Id == waiver.SectionId) : null;
                            var termDto = !string.IsNullOrEmpty(waiver.TermCode) && terms != null && terms.Any() ? terms.FirstOrDefault(t => t.Code == waiver.TermCode) : null;
                            List<RequisiteWaiver> requisiteWaivers = new List<RequisiteWaiver>();
                            requisiteWaivers = waiver.RequisiteWaivers.Where(w => w.Status == WaiverStatus.Waived).ToList();
                            foreach (var requisteWaiver in requisiteWaivers)
                            {
                                var studentwaiverModel = new StudentWaiverViewModel();
                                studentwaiverModel.Course = courseDto != null ? courseDto.SubjectCode + "-" + courseDto.Number : string.Empty;
                                studentwaiverModel.Section = sectionDto != null ? sectionDto.Number : string.Empty;
                                if (string.IsNullOrEmpty(studentwaiverModel.Course) && sectionDto != null)
                                {
                                    studentwaiverModel.Course = sectionDto.CourseName;
                                }
                                studentwaiverModel.Term = termDto != null ? termDto.Description : string.Empty;
                                studentwaiverModel.Status = WaiverStatus.Waived.ToString();
                                studentwaiverModel.StartDate = waiver.StartDate.HasValue && waiver.StartDate != default(DateTime) ? waiver.StartDate.Value.ToShortDateString() : string.Empty;
                                studentwaiverModel.EndDate = waiver.EndDate.HasValue && waiver.EndDate != default(DateTime) ? waiver.EndDate.Value.ToShortDateString() : string.Empty;
                                var requirement = await _client.GetRequirementAsync(requisteWaiver.RequisiteId);
                                studentwaiverModel.Prerequisite = string.IsNullOrEmpty(requirement.DisplayText) ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "DefaultRequisiteText") : requirement.DisplayText;
                                waivers.Add(studentwaiverModel);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, ex.Message);
                    }
                }
            }
            return waivers.OrderBy(w => w.Course, StringComparer.CurrentCultureIgnoreCase).ToList();
        }
    }
}
