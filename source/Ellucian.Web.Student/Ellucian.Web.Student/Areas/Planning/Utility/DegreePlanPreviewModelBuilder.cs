﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Ellucian.Web.Student.Infrastructure;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Areas.Planning.Utility
{
    /// <summary>
    /// Provides utility functionality for retrieving and organizing sets of data to be used in construction
    /// and manipulation of the degree plan preview.
    /// </summary>
    public class DegreePlanPreviewModelBuilder
    {
        private readonly ILogger _logger;
        private ColleagueApiClient _client;

        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        public DegreePlanPreviewModelBuilder(IAdapterRegistry adapterRegistry, ColleagueApiClient client, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _logger = logger;
            _client = client;
        }

        /// <summary>
        /// Constructs a degree plan preview - a limited view of the plan with the sample plan course work and completion indicators
        /// </summary>
        /// <param name="preview">The degree plan preview </param>
        /// <param name="mergedDegreePlan">The student's plan merged with the sample degree plan - ready for update.</param>
        /// <returns></returns>
        public async Task<DegreePlanPreviewModel> BuildDegreePlanPreviewModelAsync(DegreePlan4 preview, DegreePlan4 mergedDegreePlan, AcademicHistory3 academicHistory = null)
        {
            // Get Academic Credits for the student if not passed in
            if (academicHistory == null)
            {
                academicHistory = await _client.GetAcademicHistory3Async(preview.PersonId);
            }

            // Collect the courses needed to fill out the complete preview
            var courseIds = preview.Terms.Where(t=>t.PlannedCourses != null).SelectMany(t => t.PlannedCourses).Select(pc => pc.CourseId);
            IEnumerable<Course2> previewCourses = new List<Course2>();
            if (courseIds.Count() > 0)
            {
                try
                {
                    previewCourses = await _client.QueryCourses2Async(new CourseQueryCriteria() { CourseIds = courseIds });
                    var notFoundCourses = courseIds.Except(previewCourses.Select(c => c.Id));
                    // log missing course items
                    foreach (var crsId in notFoundCourses)
                    {
                        _logger.Error("BuildDegreePlanPreviewModel: Unable to get course " + crsId);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error retrieving courses using courseids " + courseIds + ": " + ex.Message);
                }
            }

            // Get Terms objects needed to fill out the complete preview
            List<Term> previewTerms = new List<Term>();
            foreach (var degreePlanTerm in preview.Terms)
            {
                try
                {
                    Term term =( await _client.GetCachedTermsAsync()).Where(t=>t.Code == degreePlanTerm.TermId).First();
                    previewTerms.Add(term);
                }
                catch (Exception tex)
                {
                    _logger.Error(tex, "Unable to get term: " + degreePlanTerm.TermId);
                }
            }
            DegreePlanPreviewModel degreePlanPreview = new DegreePlanPreviewModel(_adapterRegistry, preview, mergedDegreePlan, academicHistory, previewTerms, previewCourses);
            return degreePlanPreview;
        }
    }
}