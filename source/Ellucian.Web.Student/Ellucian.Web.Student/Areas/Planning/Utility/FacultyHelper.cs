﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;

namespace Ellucian.Web.Student.Areas.Planning.Utility
{
    public static class FacultyHelper
    {
        /// <summary>
        /// Builds a list of student waiver models from waiver DTOs and related necessary data
        /// </summary>
        /// <param name="studentWaivers">The waiver DTOs to convert to models</param>
        /// <param name="waiverReasons">All waiver reasons</param>
        /// <param name="students">Students associated to the waivers</param>
        /// <param name="faculty">Faculty associated to the waivers</param>
        /// <returns></returns>
        public static List<StudentWaiverModel> BuildStudentWaivers(IEnumerable<StudentWaiver> studentWaivers, IEnumerable<StudentWaiverReason> waiverReasons, IEnumerable<Ellucian.Colleague.Dtos.Planning.PlanningStudent> students, IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty)
        {
            List<StudentWaiverModel> waiverModels = new List<StudentWaiverModel>();
            if (waiverReasons == null)
            {
                waiverReasons = new List<StudentWaiverReason>();
            }
            if (students == null)
            {
                students = new List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>();
            }
            if (faculty == null)
            {
                faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>();
            }
            if (studentWaivers != null && studentWaivers.Count() > 0)
            {
                foreach (var waiver in studentWaivers)
                {
                    // Only build a waiver if there is a student Id. Otherwise skip it. 
                    if (!string.IsNullOrEmpty(waiver.StudentId))
                    {
                        var waiverModel = new StudentWaiverModel();
                        waiverModel.StudentId = waiver.StudentId;

                        var student = students.Where(s => s.Id == waiver.StudentId).FirstOrDefault();
                        if (student != null)
                        {
                            waiverModel.DisplayNameLfm = NameHelper.GetDisplayNameResult(student.LastName, student.FirstName, student.MiddleName, student.PersonDisplayName, NameFormats.LastFirstMiddleInitial).FullName;
                        }
                        else
                        {
                            waiverModel.DisplayNameLfm = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "UnknownName");
                        }

                        var reason = !string.IsNullOrEmpty(waiver.ReasonCode) ? waiverReasons.Where(r => r.Code == waiver.ReasonCode).FirstOrDefault() : null;
                        waiverModel.Reason = reason != null ? reason.Description : string.Empty;
                        waiverModel.FreeformExplanation = waiver.Comment != null ? waiver.Comment : string.Empty;
                        waiverModel.UpdatedOn = waiver.DateTimeChanged.LocalDateTime.ToShortDateString() + " " + waiver.DateTimeChanged.LocalDateTime.ToLongTimeString();
                        waiverModel.IsRevoked = waiver.IsRevoked;
                        if (!string.IsNullOrEmpty(waiver.AuthorizedBy))
                        {
                            int temp;
                            if (int.TryParse(waiver.AuthorizedBy, out temp))
                            {

                                var facultyDto = faculty.Where(f => f.Id == waiver.AuthorizedBy).FirstOrDefault();
                                if (facultyDto != null)
                                {
                                    waiverModel.AuthorizedBy = NameHelper.FacultyDisplayName(facultyDto);
                                }
                                else
                                {
                                    waiverModel.AuthorizedBy = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "WaiverAuthorizedByOther");
                                }
                            }
                            else
                            {
                                // otherwise it could be an OPERS Id so just use as is.
                                waiverModel.AuthorizedBy = waiver.AuthorizedBy;
                            }
                        }
                        else
                        {
                            waiverModel.AuthorizedBy = string.Empty;
                        }

                        waiverModels.Add(waiverModel);
                    }
                }
            }
            return waiverModels;
        }

        /// <summary>
        /// Builds a list of student petition or faculty consent models from section permission DTOs and related necessary data
        /// </summary>
        /// <param name="studentPetitionDtos">The list of student petition or faculty consent DTOs to convert to models</param>
        /// <param name="petitionStatusDtos">All section permission statuses</param>
        /// <param name="petitionReasonDtos">All section permission reasons</param>
        /// <param name="studentDtos">Students associated to the section permissions (student petitions or faculty consents)</param>
        /// <param name="facultyDtos">Faculty associated to the section permissions (student petitions or faculty consents)</param>
        /// <returns></returns>
        public static List<StudentPetitionModel> BuildStudentPetitions(IEnumerable<StudentPetition> studentPetitionDtos,
            IEnumerable<PetitionStatus> petitionStatusDtos, IEnumerable<StudentPetitionReason> petitionReasonDtos,
            IEnumerable<PlanningStudent> studentDtos, IEnumerable<Faculty> facultyDtos)
        {
            var studentPetitionModels = new List<StudentPetitionModel>();
            if (petitionStatusDtos == null)
            {
                petitionStatusDtos = new List<PetitionStatus>();
            }
            if (petitionReasonDtos == null)
            {
                petitionReasonDtos = new List<StudentPetitionReason>();
            }
            if (studentDtos == null)
            {
                studentDtos = new List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>();
            }
            if (facultyDtos == null)
            {
                facultyDtos = new List<Ellucian.Colleague.Dtos.Student.Faculty>();
            }
            if (studentPetitionDtos != null && studentPetitionDtos.Count() > 0)
            {
                foreach (var studentPetitionDto in studentPetitionDtos)
                {
                    // Only build a petition or consent if there is a student Id. Otherwise skip it. 
                    if (!string.IsNullOrEmpty(studentPetitionDto.StudentId))
                    {
                        var studentPetitionModel = new StudentPetitionModel();
                        studentPetitionModel.StudentId = studentPetitionDto.StudentId;

                        var student = studentDtos.Where(s => s.Id == studentPetitionDto.StudentId).FirstOrDefault();
                        if (student != null)
                        {
                            studentPetitionModel.DisplayNameLfm = NameHelper.GetDisplayNameResult(student.LastName, student.FirstName, student.MiddleName, student.PersonDisplayName, NameFormats.LastFirstMiddleInitial).FullName;
                        }
                        else
                        {
                            studentPetitionModel.DisplayNameLfm = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "UnknownName");
                        }
                        studentPetitionModel.PetitionId = studentPetitionDto.Id;
                        studentPetitionModel.SectionId = studentPetitionDto.SectionId;

                        var statusDto = !string.IsNullOrEmpty(studentPetitionDto.StatusCode) ? petitionStatusDtos.Where(s => s.Code == studentPetitionDto.StatusCode).FirstOrDefault() : null;
                        studentPetitionModel.Status = statusDto != null ? statusDto.Description : string.Empty;

                        var reasonDto = !string.IsNullOrEmpty(studentPetitionDto.ReasonCode) ? petitionReasonDtos.Where(r => r.Code == studentPetitionDto.ReasonCode).FirstOrDefault() : null;
                        studentPetitionModel.Reason = reasonDto != null ? reasonDto.Description : string.Empty;

                        studentPetitionModel.FreeformExplanation = studentPetitionDto.Comment != null ? studentPetitionDto.Comment : string.Empty;
                        studentPetitionModel.UpdatedOn = studentPetitionDto.DateTimeChanged.LocalDateTime.ToShortDateString() + " " + studentPetitionDto.DateTimeChanged.LocalDateTime.ToLongTimeString();

                        if (!string.IsNullOrEmpty(studentPetitionDto.UpdatedBy))
                        {
                            int updatedById;
                            if (int.TryParse(studentPetitionDto.UpdatedBy, out updatedById))
                            {
                                var faculty = facultyDtos.Where(f => f.Id == studentPetitionDto.UpdatedBy).FirstOrDefault();
                                if (faculty != null)
                                {
                                    studentPetitionModel.UpdatedBy = NameHelper.FacultyDisplayName(faculty);
                                }
                                else
                                {
                                    studentPetitionModel.UpdatedBy = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "PetitionAuthorizedByOther");
                                }
                            }
                            else
                            {
                                studentPetitionModel.UpdatedBy = studentPetitionDto.UpdatedBy;
                            }
                        }
                        else
                        {
                            studentPetitionModel.UpdatedBy = string.Empty;
                        }
                        studentPetitionModels.Add(studentPetitionModel);
                    }
                }
            }
            return studentPetitionModels;
        }


        /// <summary>
        /// Builds a list of student petition or faculty consent models from student petitions DTOs , terms, sections, courses
        /// </summary>
        /// <param name="studentPetitionDtos">The list of student petition or faculty consent DTOs to convert to models</param>
        /// <param name="petitionStatusDtos">All petitions statuses</param>
        /// <param name="petitionReasonDtos">All petitions reasons</param>
        /// <param name="courseDtos">All course details</param>
        /// /// <param name="sectionDtos">All section details</param>
        /// /// <param name="termsDtos">All terms details</param>
        /// <returns></returns>
        public static List<StudentPetitionModel> BuildStudentPetitions(IEnumerable<StudentPetition> studentPetitionDtos,
            IEnumerable<PetitionStatus> petitionStatusDtos, IEnumerable<StudentPetitionReason> petitionReasonDtos, IEnumerable<Course2> courseDtos,
            IEnumerable<Section3> sectionDtos,
            IEnumerable<Term> termsDtos)
        {
            try
            {
                var studentPetitionModels = new List<StudentPetitionModel>();
                if (studentPetitionDtos != null && studentPetitionDtos.Count() > 0)
                {
                    foreach (var studentPetitionDto in studentPetitionDtos)
                    {
                            var studentPetitionModel = new StudentPetitionModel();
                            studentPetitionModel.PetitionId = studentPetitionDto.Id;
                            //retrieve petition status
                            var statusDto = !string.IsNullOrEmpty(studentPetitionDto.StatusCode) && petitionStatusDtos != null && petitionStatusDtos.Any() ? petitionStatusDtos.Where(s => s.Code == studentPetitionDto.StatusCode).FirstOrDefault() : null;
                            studentPetitionModel.Status = statusDto != null ? statusDto.Description : string.Empty;
                            //retrieve petition reason
                            var reasonDto = !string.IsNullOrEmpty(studentPetitionDto.ReasonCode) && petitionReasonDtos != null && petitionReasonDtos.Any() ? petitionReasonDtos.Where(r => r.Code == studentPetitionDto.ReasonCode).FirstOrDefault() : null;
                            studentPetitionModel.Reason = reasonDto != null ? reasonDto.Description : string.Empty;
                            //retrieve date when petition was updated
                            studentPetitionModel.UpdatedOn = studentPetitionDto.DateTimeChanged != default(DateTimeOffset) ? studentPetitionDto.DateTimeChanged.LocalDateTime.ToShortDateString() + " " + studentPetitionDto.DateTimeChanged.LocalDateTime.ToLongTimeString() : string.Empty;
                            //retrieve course details
                            var courseDto = !string.IsNullOrEmpty(studentPetitionDto.CourseId) && courseDtos != null && courseDtos.Any() ? courseDtos.FirstOrDefault(c => c.Id == studentPetitionDto.CourseId) : null;
                            studentPetitionModel.Course = courseDto != null ? courseDto.SubjectCode + "-" + courseDto.Number : string.Empty;
                            //retrieve section details
                            var sectionDto = !string.IsNullOrEmpty(studentPetitionDto.SectionId) && sectionDtos != null && sectionDtos.Any() ? sectionDtos.FirstOrDefault(c => c.Id == studentPetitionDto.SectionId) : null;
                            studentPetitionModel.Section = sectionDto != null ? sectionDto.Number : string.Empty;
                            //retreive term details
                            var termDto = !string.IsNullOrEmpty(studentPetitionDto.TermCode) && termsDtos != null && termsDtos.Any() ? termsDtos.FirstOrDefault(t => t.Code == studentPetitionDto.TermCode) : null;
                            studentPetitionModel.Term = termDto != null ? termDto.Description : string.Empty;
                            studentPetitionModel.StartDate = studentPetitionDto.StartDate.HasValue && studentPetitionDto.StartDate != default(DateTime) ? studentPetitionDto.StartDate.Value.ToShortDateString() : string.Empty;
                            studentPetitionModel.EndDate = studentPetitionDto.EndDate.HasValue && studentPetitionDto.EndDate != default(DateTime) ? studentPetitionDto.EndDate.Value.ToShortDateString() : string.Empty;
                            studentPetitionModels.Add(studentPetitionModel);
                    }
                }
                return studentPetitionModels.OrderBy(s => s.Course, StringComparer.CurrentCultureIgnoreCase).ToList();
            }
            catch
            {
                throw;
            }
        }
    }
}