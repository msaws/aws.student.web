﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Areas.Planning.Utility
{
    public class DegreePlanHelper
    {
        // The system ID for the current user
        public string UserId { get; set; }

        #region Constructor
        /// <summary>
        /// Creates an empty DegreePlanHelper instance.
        /// </summary>
        public DegreePlanHelper()
        {
        }
        #endregion

        #region Adding/Removing Terms and Courses
        /// <summary>
        /// Adds a new term to the specified degree plan.
        /// </summary>
        /// <param name="degreePlan">The plan on which to add a term</param>
        /// <param name="termId">The ID of the term to be added</param>
        /// <returns>True if the term was added, otherwise false (for example, if the term already existed)</returns>
        public bool AddTerm(DegreePlan4 degreePlan, string termId)
        {
            bool added = false;

            if (degreePlan != null && !string.IsNullOrEmpty(termId))
            {
                if (degreePlan.Terms == null)
                {
                    degreePlan.Terms = new List<DegreePlanTerm4>();
                }
                bool alreadyHas = false;
                foreach (var dpt in degreePlan.Terms)
                {
                    if (dpt.TermId.Equals(termId, StringComparison.OrdinalIgnoreCase))
                    {
                        alreadyHas = true;
                        break;
                    }
                }
                if (!alreadyHas)
                {
                    degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = termId, PlannedCourses = new List<PlannedCourse4>() });
                    added = true;
                }
            }

            return added;
        }

        /// <summary>
        /// Adds a course to the specified degree plan. If the provided term does not exist on the plan it will be created.
        /// </summary>
        /// <param name="degreePlan">The degree plan on which to add a course</param>
        /// <param name="termId">The ID of the term to add</param>
        /// <param name="courseId">The ID of the course to add</param>
        /// <param name="gradingType">The grading scheme for this course, such as Audit</param>
        /// <param name="sectionId">The ID of the specific section being taken (omit this to add a generic course without a section)</param>
        /// <param name="credits">The number of credits to take for this course (use -1 or omit to use the default)</param>
        /// <returns>True if the course was added, otherwise false</returns>
        public bool AddCourse(DegreePlan4 degreePlan, string termId, string courseId, GradingType gradingType, string sectionId = "", decimal? credits = -1)
        {
            bool added = false;

            if (degreePlan != null)
            {
                if (!string.IsNullOrEmpty(termId))
                {
                    if (degreePlan.Terms == null)
                    {
                        degreePlan.Terms = new List<DegreePlanTerm4>();
                    }
                    bool foundTerm = false;

                    //find the matching term to insert into
                    for (int i = 0; i < degreePlan.Terms.Count; i++)
                    {
                        var degreePlanTerm = degreePlan.Terms[i];
                        if (degreePlanTerm.TermId.Equals(termId, StringComparison.OrdinalIgnoreCase))
                        {
                            foundTerm = true;

                            //if just adding an instance of the course w/out a section, add it regularly
                            if (string.IsNullOrEmpty(sectionId))
                            {
                                degreePlanTerm.PlannedCourses.Add(new PlannedCourse4() { CourseId = courseId, SectionId = string.Empty, Credits = credits, TermId = termId, GradingType = gradingType, AddedBy = UserId, AddedOn = DateTime.Now });
                            }
                            else
                            {
                                //adding a section requires the following logic:
                                //  1. First make sure that this section isn't already planned - if it is, just jump out with no action
                                //  2. Look for an existing PlannedCourse entry that has this courseId and no sectionId
                                //  3. If found, update that PlannedCourse to contain the sectionId
                                //  4. If you find a matching courseId with a DIFFERENT sectionId, add a new PlannedCourseEntry for this new item
                                //  5. If nothing else matches, add a new PlannedCourse entry with the section

                                // 1. Make sure not a duplicate section
                                // If PlannedCourses is null, initialize it
                                if (degreePlanTerm.PlannedCourses == null) degreePlanTerm.PlannedCourses = new List<PlannedCourse4>();
                                foreach (var pc in degreePlanTerm.PlannedCourses)
                                {
                                    if (pc.CourseId == courseId && pc.SectionId == sectionId)
                                    {
                                        //already have it - do not update the credits
                                        return false;
                                    }
                                }

                                // 2/3. See if we have a matching course without a section, update
                                bool foundEmpty = false;
                                foreach (var pc in degreePlanTerm.PlannedCourses)
                                {
                                    if (pc.CourseId == courseId && string.IsNullOrEmpty(pc.SectionId))
                                    {
                                        pc.SectionId = sectionId;
                                        pc.Credits = credits;
                                        pc.TermId = termId;
                                        pc.GradingType = gradingType;
                                        foundEmpty = true;
                                        break;
                                    }
                                }

                                // 4/5. Anything else means we didn't find an existing, matching course with an empty section, have to add a new entry
                                if (!foundEmpty)
                                {
                                    degreePlanTerm.PlannedCourses.Add(new PlannedCourse4() { CourseId = courseId, SectionId = sectionId, Credits = credits, TermId = termId, GradingType = gradingType, AddedBy = UserId, AddedOn = DateTime.Now });
                                }
                            }
                            added = true;
                            break;
                        }
                    }

                    //if we did not find the term in our list then it's a new one that we need to add first before inserting the course/section
                    if (!foundTerm)
                    {
                        degreePlan.Terms.Add(new DegreePlanTerm4()
                        {
                            TermId = termId,
                            PlannedCourses = new List<PlannedCourse4>()
                            {
                                new PlannedCourse4()
                                {
                                    CourseId = courseId,
                                    Credits = credits,
                                    SectionId = sectionId,
                                    TermId = termId,
                                    GradingType = gradingType,
                                    AddedBy = UserId,
                                    AddedOn = DateTime.Now
                                }
                            }
                        });
                        added = true;
                    }
                }
                else
                {
                    //no term means that this is a non-term item that goes into the special pool
                    //in this case it cannot be a repeat (and recall that non-term items are sections ONLY, not generic courses)
                    if (!DoesCourseSectionExistInNonTerm(degreePlan, courseId, sectionId))
                    {
                        degreePlan.NonTermPlannedCourses.Add(new PlannedCourse4()
                        {
                            CourseId = courseId,
                            Credits = credits,
                            SectionId = sectionId,
                            TermId = string.Empty,
                            GradingType = gradingType,
                            AddedBy = UserId,
                            AddedOn = DateTime.Now
                        });
                        added = true;
                    }
                }
            }

            return added;
        }

        /// <summary>
        /// Removes a term from the specified degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan from which to remove the term</param>
        /// <param name="termId">The ID of the term to remove</param>
        /// <returns>True if the term was found and removed, otherwise false</returns>
        public bool RemoveTerm(DegreePlan4 degreePlan, string termId)
        {
            bool removed = false;

            if (degreePlan != null && degreePlan.Terms != null && !string.IsNullOrEmpty(termId))
            {
                for (int i = 0; i < degreePlan.Terms.Count; i++)
                {
                    if (degreePlan.Terms[i].TermId.Equals(termId, StringComparison.OrdinalIgnoreCase))
                    {
                        degreePlan.Terms.RemoveAt(i);
                        removed = true;
                        break;
                    }
                }
            }

            return removed;
        }

        /// <summary>
        /// Removes a non-term section, by section ID, from the special bucket of the plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan from which to remove the non-term section</param>
        /// <param name="sectionId">The section ID to be removed</param>
        public void RemoveNonTermSection(DegreePlan4 degreePlan, string sectionId)
        {
            if (!string.IsNullOrEmpty(sectionId) && degreePlan != null)
            {
                for (int i = 0; i < degreePlan.NonTermPlannedCourses.Count; i++)
                {
                    if (degreePlan.NonTermPlannedCourses[i].SectionId == sectionId)
                    {
                        degreePlan.NonTermPlannedCourses.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Removes a course from the specified term on the provided degree plan. To remove a specific Section
        /// from the Plan, use the RemoveSection method.
        /// </summary>
        /// <param name="degreePlan">The degree plan from which to remove the course</param>
        /// <param name="termId">The term from which to remove the course</param>
        /// <param name="courseId">The ID of the course to remove</param>
        /// <returns>True if the course was found and removed, otherwise false</returns>
        public bool RemoveCourse(DegreePlan4 degreePlan, string termId, string courseId, string sectionId)
        {
            bool removed = false;

            if (degreePlan != null)
            {
                if (degreePlan.Terms != null && !string.IsNullOrEmpty(termId))
                {
                    bool completed = false;
                    for (int i = 0; i < degreePlan.Terms.Count; i++)
                    {
                        if (degreePlan.Terms[i].TermId.Equals(termId, StringComparison.OrdinalIgnoreCase))
                        {
                            for (int j = 0; j < degreePlan.Terms[i].PlannedCourses.Count; j++)
                            {
                                if (degreePlan.Terms[i].PlannedCourses[j].CourseId == courseId)
                                {
                                    // In order for a planned course to be removed it must match both the courseId and the sectionId provided.
                                    if ((degreePlan.Terms[i].PlannedCourses[j].SectionId == sectionId) ||
                                       (string.IsNullOrEmpty(sectionId) && string.IsNullOrEmpty(degreePlan.Terms[i].PlannedCourses[j].SectionId)))
                                    {
                                        degreePlan.Terms[i].PlannedCourses.RemoveAt(j);
                                        removed = true;
                                        completed = true;
                                        break;
                                    }
                                }
                            }

                            if (completed)
                            {
                                break;
                            }
                        }
                    }
                }
                else if (degreePlan.NonTermPlannedCourses != null && string.IsNullOrEmpty(termId))
                {
                    // look for it in the non-term list 
                    // Note: since a nonterm course cannot be "planned" without a section there is no way to remove a planned course without a section.
                    for (int i = 0; i < degreePlan.NonTermPlannedCourses.Count; i++)
                    {
                        if (degreePlan.NonTermPlannedCourses[i] != null && degreePlan.NonTermPlannedCourses[i].CourseId == courseId && degreePlan.NonTermPlannedCourses[i].SectionId == sectionId)
                        {
                            degreePlan.NonTermPlannedCourses.RemoveAt(i);
                            removed = true;
                            break;
                        }
                    }
                }
            }

            return removed;
        }

        /// <summary>
        /// Removes a section from the provided degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan from which to remove the section</param>
        /// <param name="sectionId">The ID of the section to remove</param>
        /// <returns>True if the section was found and removed, otherwise false</returns>
        public bool RemoveSection(DegreePlan4 degreePlan, string sectionId)
        {
            bool removed = false;
            if (degreePlan != null && !string.IsNullOrEmpty(sectionId))
            {
                if (degreePlan.Terms != null)
                {
                    bool completed = false;
                    for (int i = 0; i < degreePlan.Terms.Count; i++)
                    {
                        for (int j = 0; j < degreePlan.Terms[i].PlannedCourses.Count; j++)
                        {
                            if (degreePlan.Terms[i].PlannedCourses[j].SectionId == sectionId)
                            {
                                degreePlan.Terms[i].PlannedCourses[j].SectionId = "";
                                removed = true;
                                completed = true;
                                break;
                            }
                        }

                        if (completed)
                        {
                            break;
                        }
                    }
                }

                if (!removed && degreePlan.NonTermPlannedCourses != null)
                {
                    //look for it in the non-term list
                    for (int i = 0; i < degreePlan.NonTermPlannedCourses.Count; i++)
                    {
                        if (degreePlan.NonTermPlannedCourses[i].SectionId != null && degreePlan.NonTermPlannedCourses[i].SectionId == sectionId)
                        {
                            degreePlan.NonTermPlannedCourses.RemoveAt(i);
                            removed = true;
                            break;
                        }
                    }
                }
            }

            return removed;
        }
        #endregion

        /// <summary>
        /// Determines whether the provided course or section already exists on the plan or academic history.
        /// </summary>
        /// <param name="degreePlan">The degree plan in which to check for existence of the course</param>
        /// <param name="academicHistory">The academic history to check for the existence of the course</param>
        /// <param name="termId">The term to check</param>
        /// <param name="courseId">The course to check</param>
        /// <param name="sectionId">The section to check</param>
        /// <returns>True if the course already exists on the plan for the provided term, otherwise false</returns>
        public CourseExistsStatus DoesCourseExistInPlan(DegreePlan4 degreePlan, AcademicHistory3 academicHistory, string termId, string courseId)
        {
            if (degreePlan == null)
            {
                throw new ArgumentNullException("degreePlan", "degreePlan is a required parameter.");
            }
            if (academicHistory == null)
            {
                throw new ArgumentNullException("academicHistory", "academicHistory is a required parameter.");
            }
            if (string.IsNullOrEmpty(termId))
            {
                throw new ArgumentNullException("termId", "termId is a required parameter.");
            }
            if (string.IsNullOrEmpty(courseId))
            {
                throw new ArgumentNullException("courseId", "courseId is a required parameter.");
            }

            // Check for the course in academic history first - we only give one warning, and if they've actually taken the course,
            // that's probably more important than having planned it elsewhere.
            if (academicHistory.AcademicTerms != null)
            {
                foreach (AcademicTerm3 acadTerm in academicHistory.AcademicTerms)
                {
                    foreach (var credit in acadTerm.AcademicCredits)
                    {
                        if (!string.IsNullOrEmpty(credit.CourseId) && credit.CourseId.Equals(courseId, StringComparison.OrdinalIgnoreCase))
                        {

                            if (acadTerm.TermId.Equals(termId, StringComparison.OrdinalIgnoreCase))
                            {
                                return CourseExistsStatus.CourseTakenInThisTerm;
                            }
                            else
                            {
                                return CourseExistsStatus.CourseTakenInAnotherTerm;
                            }
                        }
                    }
                }
            }

            if (degreePlan.Terms != null)
            {
                foreach (var dpt in degreePlan.Terms)
                {
                    foreach (string course in dpt.GetPlannedCourseIds())
                    {
                        if (course.Equals(courseId, StringComparison.OrdinalIgnoreCase))
                        {
                            if (dpt.TermId.Equals(termId, StringComparison.OrdinalIgnoreCase))
                            {
                                return CourseExistsStatus.CourseInThisTerm;
                            }
                            else
                            {
                                return CourseExistsStatus.CourseInAnotherTerm;
                            }
                        }
                    }
                }
            }
            return CourseExistsStatus.CourseNotInPlan;
        }

        /// <summary>
        /// Determines whether the provided course for the provided section already exists on the plan or academic history when the section is a non-term section.
        /// </summary>
        /// <param name="degreePlan">The degree plan in which to check for existence of the course</param>
        /// <param name="academicHistory">The academic history to check for the existence of the course</param>
        /// <param name="startDate">The start date of the section to check</param>
        /// <param name="endDate">The end date of the section to check</param>
        /// <param name="courseId">The course to check</param>
        /// <param name="sectionId">The section to check</param>
        /// <param name="planningTerms">List of planning term DTOs, used to compare dates</param>
        /// <returns>True if the section's course already exists on the plan, otherwise false</returns>
        public CourseExistsStatus DoesCourseExistInPlanNonTerm(DegreePlan4 degreePlan, AcademicHistory3 academicHistory, DateTime? startDate, DateTime? endDate, string courseId, string sectionId, List<Term> planningTerms)
        {
            if (degreePlan == null)
            {
                throw new ArgumentNullException("degreePlan", "degreePlan is a required parameter.");
            }
            if (academicHistory == null)
            {
                throw new ArgumentNullException("academicHistory", "academicHistory is a required parameter.");
            }
            if (string.IsNullOrEmpty(courseId))
            {
                throw new ArgumentNullException("courseId", "courseId is a required parameter.");
            }
            if (string.IsNullOrEmpty(sectionId))
            {
                throw new ArgumentNullException("sectionId", "sectionId is a required parameter.");
            }

            // Check for the course in academic history first - we only give one warning, and if they've actually taken the course,
            // that's probably more important than having planned it elsewhere.
            if (academicHistory.AcademicTerms != null)
            {
                foreach (AcademicTerm3 acadTerm in academicHistory.AcademicTerms)
                {
                    foreach (var credit in acadTerm.AcademicCredits)
                    {
                        if (!string.IsNullOrEmpty(credit.CourseId) && credit.CourseId.Equals(courseId, StringComparison.OrdinalIgnoreCase))
                        {
                            if (planningTerms != null && planningTerms.Count() > 0)
                            {
                                var aterm = planningTerms.Where(pt => pt.Code == acadTerm.TermId).FirstOrDefault();
                                if (aterm != null)
                                {
                                    if (aterm.StartDate <= startDate && aterm.EndDate >= endDate)
                                    {
                                        return CourseExistsStatus.CourseTakenInThisTerm;
                                    }
                                    else
                                    {
                                        return CourseExistsStatus.CourseTakenInAnotherTerm;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (degreePlan.Terms != null)
            {
                foreach (var dpt in degreePlan.Terms)
                {
                    foreach (var course in dpt.PlannedCourses)
                    {
                        // If we find the course in this term then we need to determine if this is the
                        // same term as the nonterm section being added or if it is a different term.
                        if (course.CourseId.Equals(courseId, StringComparison.OrdinalIgnoreCase))
                        {

                            if (planningTerms != null && planningTerms.Count() > 0)
                            {
                                var dterm = planningTerms.Where(pt => pt.Code == dpt.TermId).FirstOrDefault();
                                if (dterm != null)
                                {
                                    if (dterm.StartDate <= startDate && dterm.EndDate >= endDate)
                                    {
                                        // Since nonterm section are always added again - even if the course exists on the term with 
                                        // no section. Go ahead and give this message.
                                        return CourseExistsStatus.CourseTakenInThisTerm;

                                    }
                                    else
                                    {
                                        return CourseExistsStatus.CourseTakenInAnotherTerm;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return CourseExistsStatus.CourseNotInPlan;
        }

        /// <summary>
        /// Checks whether the given course and section already exist in the NonTermPlannedCourses grouping
        /// of the degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan in which to check for existence of the course section</param>
        /// <param name="courseId">The ID of the course to check</param>
        /// <param name="sectionId">The ID of the section to check</param>
        /// <returns>True if the course already exists on the plan as a non-term item, otherwise false</returns>
        public bool DoesCourseSectionExistInNonTerm(DegreePlan4 degreePlan, string courseId, string sectionId)
        {
            bool exists = false;
            if (degreePlan != null && !string.IsNullOrEmpty(courseId))
            {
                if (degreePlan.NonTermPlannedCourses == null)
                {
                    degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                }
                foreach (var nonTermCourse in degreePlan.NonTermPlannedCourses)
                {
                    if (nonTermCourse.CourseId.Equals(courseId, StringComparison.OrdinalIgnoreCase) && nonTermCourse.SectionId.Equals(sectionId, StringComparison.OrdinalIgnoreCase))
                    {
                        exists = true;
                        break;
                    }
                }
            }

            return exists;
        }

        /// <summary>
        /// Determines whether the provided course+section already exists on the provided term. Note that this checks
        /// both items, including section.
        /// </summary>
        /// <param name="degreePlan">The degree plan in which to check for existence of the course</param>
        /// <param name="termId">The term to check</param>
        /// <param name="courseId">The course to check</param>
        /// <param name="sectionId">The section to check</param>
        /// <returns>True if the course+section already exists on the plan for the provided term, otherwise false</returns>
        public bool DoesCourseSectionExistInTerm(DegreePlan4 degreePlan, string termId, string courseId, string sectionId)
        {
            bool exists = false;
            if (degreePlan != null && !string.IsNullOrEmpty(termId) && !string.IsNullOrEmpty(courseId) && !string.IsNullOrEmpty(sectionId))
            {
                if (degreePlan.Terms == null)
                {
                    degreePlan.Terms = new List<DegreePlanTerm4>();
                }
                foreach (var dpt in degreePlan.Terms)
                {
                    if (dpt.TermId.Equals(termId, StringComparison.OrdinalIgnoreCase))
                    {
                        foreach (var pc in dpt.PlannedCourses)
                        {
                            if (pc.CourseId.Equals(courseId, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(pc.SectionId) && pc.SectionId.Equals(sectionId, StringComparison.OrdinalIgnoreCase))
                            {
                                exists = true;
                                break;
                            }
                        }
                    }
                }
            }

            return exists;
        }

        /// <summary>
        /// Removes all unprotected planned courses from the specified list of terms on the provided degree plan. This includes any associated planned sections.
        /// </summary>
        /// <param name="degreePlan">The degree plan from which to remove the course</param>
        /// <param name="termId">The term from which to remove the course</param>
        /// <returns>True if the planned courses were removed, otherwise false.  False would really just indicate that term wasn't on the plan or the term or there was nothing to clear.</returns>
        public bool ClearPlannedCourses(DegreePlan4 degreePlan, IEnumerable<string> termIds)
        {
            bool removed = false;
            if (termIds == null || termIds.Count() == 0 || degreePlan == null || degreePlan.Terms.Count() == 0)
            {
                return removed;
            }

            for (int i = 0; i < degreePlan.Terms.Count; i++)
            {
                // Is this one of the terms that should be cleared?  
                if (termIds.Contains(degreePlan.Terms[i].TermId) && degreePlan.Terms[i].PlannedCourses.Count > 0)
                {
                    degreePlan.Terms[i].PlannedCourses = new List<PlannedCourse4>();
                    removed = true;
                }
            }

            return removed;
        }

        /// <summary>
        /// Update the IsProtected property for all or specific the planned courses in the degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan to update</param>
        /// <param name="protect">Indicates whether the items are to be marked as Protected (true) or not protected (false)</param>
        /// <param name="courseIds">Optional. Ids of specific courses to protect.</param>
        /// <remarks>This process will default to protecting the entire plan unless a list of specific course is provided. </remarks>
        /// <returns>true or false.</returns>
        public bool UpdatePlanProtection(DegreePlan4 degreePlan, bool protect, List<string> courseIds = null)
        {
            bool updated = false;
            if (degreePlan == null)
            {
                return updated;
            }
            bool doAll = (courseIds == null || courseIds.Count <= 0);

            // First go through the nonterm planned courses
            if (degreePlan.NonTermPlannedCourses != null && degreePlan.NonTermPlannedCourses.Count > 0)
            {
                foreach (var npc in degreePlan.NonTermPlannedCourses)
                {
                    if (doAll)
                    {
                        npc.IsProtected = protect;
                        updated = true;
                    }
                    else
                    {
                        // Only do those courses in the list of courses and only affect the change if value actually changed.
                        if (courseIds.Contains(npc.CourseId) && npc.IsProtected != protect)
                        {
                            npc.IsProtected = protect;
                            updated = true;
                        }
                    }
                }
            }

            // Next go though the planned courses in each term
            if (degreePlan.Terms != null && degreePlan.Terms.Count > 0)
            {
                foreach (var term in degreePlan.Terms)
                {
                    if (term.PlannedCourses != null && term.PlannedCourses.Count > 0)
                    {
                        if (doAll)
                        {
                            foreach (var pc in term.PlannedCourses)
                            {
                                pc.IsProtected = protect;
                                updated = true;
                            }
                        }
                        else
                        {
                            foreach (var pc in term.PlannedCourses)
                            {
                                // Only do those courses in the list of courses.
                                if (courseIds.Contains(pc.CourseId) && pc.IsProtected != protect)
                                {
                                    pc.IsProtected = protect;
                                    updated = true;
                                }
                            }
                        }
                    }
                }

            }
            return updated;
        }
    }
}