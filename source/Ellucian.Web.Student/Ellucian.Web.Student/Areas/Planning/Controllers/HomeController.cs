﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System.Web.Mvc;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using slf4net;
using System.Collections.Generic;
using Ellucian.Web.Student.Utility;
using System;
using Ellucian.Web.Adapters;

namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class HomeController : BaseStudentController
    {
        /// <summary>
        /// Creates a new HomeController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public HomeController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        /// <summary>
        /// Action method for the Home page of Student Planning.
        /// </summary>
        /// <returns></returns>
        [LinkHelp]
        [PageAuthorize("apHome")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.PlanningHomeResources, "AreaDescription");

            return View();
        }
    }
}
