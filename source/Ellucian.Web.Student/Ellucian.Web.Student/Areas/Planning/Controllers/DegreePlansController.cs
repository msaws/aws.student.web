﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;


namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class DegreePlansController : BasePlanningController
    {
        private DegreePlanModelBuilder DegreePlanModelBuilder;

        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Creates a new DegreePlanController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public DegreePlansController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _adapterRegistry = adapterRegistry;

            DegreePlanModelBuilder = new DegreePlanModelBuilder(_adapterRegistry, ServiceClient, logger);
        }


        /// <summary>
        /// The Controller action to show the combined degree plan and schedule view for a student. This represents the new default Index page for /DegreePlans.
        /// </summary>
        /// <remarks>This action method corresponds to a <code>GET /DegreePlans</code> request.</remarks>
        /// <returns>The Index view and associated model to display the full degree plan and schedule UI</returns>
        [LinkHelp]
        [PageAuthorize("schedulePlan")]
        public ActionResult Index(string message, NotificationType? notificationType)
        {

            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AreaDescription");
            ViewBag.SubMenuOverride = "Index";

            Notification notification = null;
            if (message != null && message.Length > 0)
            {
                notification = new Notification(message, (notificationType != null) ? (NotificationType)notificationType : NotificationType.Information, (notificationType == NotificationType.Error) ? false : true);
            }

            ViewBag.Notification = JsonConvert.SerializeObject(notification);
            return View();
        }

        /// <summary>
        /// The Controller action to preview a degree plan based on a sample plan. 
        /// </summary>
        /// <param name="program">The program code</param>
        /// <returns>A degree plan preview model</returns>
        [ActionName("PreviewSamplePlan")]
        public async Task<ActionResult> PreviewSamplePlanAsync(string program, int degreePlanId, string termCode)
        {
            try
            {
                var degreePlanPreview = await ServiceClient.PreviewSampleDegreePlan5Async(degreePlanId, program, termCode);
                if (degreePlanPreview == null || degreePlanPreview.Preview == null || degreePlanPreview.MergedDegreePlan == null)
                {
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.SamplePlanNotFound };
                }
                var DegreePlanPreviewModelBuilder = new DegreePlanPreviewModelBuilder(_adapterRegistry, ServiceClient, Logger);
                var degreePlanPreviewModel = await DegreePlanPreviewModelBuilder.BuildDegreePlanPreviewModelAsync(degreePlanPreview.Preview, degreePlanPreview.MergedDegreePlan, degreePlanPreview.AcademicHistory);
                return Json(degreePlanPreviewModel, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to generate preview of plan.", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Load the degree plan that has been updated by the sample plan specified in the parameters and then display the degree plan
        /// </summary>
        /// <param name="degreePlanJson">The updated degree plan to post </param>
        /// <returns>The degree plan view</returns>
        [ActionName("LoadSamplePlan")]
        public async Task<ActionResult> LoadSamplePlanAsync(string degreePlanJson, bool retrieveNotes = true)
        {
            try
            {
                if (string.IsNullOrEmpty(degreePlanJson))
                {
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var text = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "SamplePlanLoaded");
                // If the Current user is the student redirect to Index with a success. If the Current user is not the student
                // assume this originated from an advisor proxy and redirect them back to that page.
                if (CurrentUser.PersonId != degreePlan.PersonId)
                {
                    return RedirectToAction("Advise", "Advisors", new { id = degreePlan.PersonId });
                }
                return RedirectToAction("Index", new { message = text, notificationType = NotificationType.Success });
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                string text = "";
                if (dpe.Code == DegreePlanExceptionCodes.SamplePlanNotFound)
                {
                    text = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "SamplePlanNotFoundError");
                }
                else
                {
                    text = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UnableToUpdatePlan");
                }
                return RedirectToAction("Index", new { message = text, notificationType = (dpe.Code == DegreePlanExceptionCodes.SamplePlanNotFound) ? NotificationType.Information : NotificationType.Error });
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                var text = ioe.Message;
                return RedirectToAction("Index", new { message = text, notificationType = NotificationType.Error });
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                var text = e.Message;
                return RedirectToAction("Index", new { message = text, notificationType = NotificationType.Error });
            }
        }


        /// <summary>
        /// Retrieves the degree plan data and student programs for the provided student Id. 
        /// </summary>
        /// <param name="studentId">The ID of the student for which info is requested</param>
        /// <param name="retrieveNotes">A flag indicating whether to retrieve the advising notes with the plan</param>
        /// <returns>The student's DegreePlan view model and student programs as a JSON structure.</returns>
        public async Task<JsonResult> CurrentAsync(string studentId)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId)) { studentId = GetEffectivePersonId(); };
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId", "studentId is required to get plan.");
                }

                // Get student program Ids from service client
                var studentPrograms = await ServiceClient.GetStudentProgramsAsync(studentId);
                var studentProgramModels = new List<Ellucian.Web.Student.Areas.Planning.Models.DegreePlan.StudentProgram>();
                var programs = await ServiceClient.GetCachedProgramsAsync();
                foreach (var studentProgram in studentPrograms)
                {
                    var program = programs.Where(p => p.Code == studentProgram.ProgramCode).FirstOrDefault();
                    studentProgramModels.Add(new Ellucian.Web.Student.Areas.Planning.Models.DegreePlan.StudentProgram() { Catalog = studentProgram.CatalogCode, Code = studentProgram.ProgramCode, StudentId = studentProgram.StudentId, Title = program.Title, Description = program.Description, RelatedPrograms = program.RelatedPrograms });
                }

                // Get the student's degree plan and convert it into a model
                var student = await ServiceClient.GetPlanningStudentAsync(studentId);
                var degreePlanAcademicHistory = await GetOrCreateDegreePlanAsync(student);
                if (degreePlanAcademicHistory.DegreePlan == null)
                {
                    //This means it could not get or create a plan for the student, so this is an error condition
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(degreePlanAcademicHistory.DegreePlan, student, null, degreePlanAcademicHistory.AcademicHistory);
                return new JsonResult() { Data = new { DegreePlan = degreePlanModel, StudentPrograms = studentProgramModels }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message, JsonRequestBehavior.AllowGet);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiGenericError"), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Controller action to add a term to a degree plan.
        /// </summary>
        /// <param name="addTermId">The ID of the term to be added, such as 2013/SP</param>
        /// <param name="degreePlanJson">The active DegreePlan object as a JSON string</param>
        /// <param name="currentAcadHistoryJson">The active AcademicHistory object as a JSON string</param>
        /// <returns>A JSON representation of the updated ViewModel</returns>
        public async Task<JsonResult> AddTermAsync(string addTermId, string degreePlanJson)
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                if (degreePlan == null)
                {
                    throw new Exception();
                }

                (new Utility.DegreePlanHelper()).AddTerm(degreePlan, addTermId);
                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, null, updateDegreePlanResponse.AcademicHistory);

                //set this new term as the one to focus on in the UI
                var term = degreePlanModel.Terms.Where(t => t.Code == addTermId).FirstOrDefault();
                if (term != null && !string.IsNullOrEmpty(term.Description))
                {
                    degreePlanModel.TermToShow = term.Code;
                }
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddTermFailedGeneric"));
            }
        }

        /// <summary>
        /// Controller action to remove a term from a degree plan.
        /// </summary>
        /// <param name="removeTermId">The ID of the term to be removed, such as 2013/SP</param>
        /// <param name="degreePlanJson">The active DegreePlan object as a JSON string</param>
        /// <param name="currentAcadHistoryJson">The active AcademicHistory object as a JSON string</param>
        /// <returns>A JSON representation of the updated ViewModel</returns>
        public async Task<JsonResult> RemoveTermAsync(string removeTermId, string degreePlanJson)
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                if (degreePlan == null)
                {
                    throw new Exception();
                }

                (new Utility.DegreePlanHelper()).RemoveTerm(degreePlan, removeTermId);
                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, null, updateDegreePlanResponse.AcademicHistory);
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RemoveTermFailedGeneric"));
            }
        }

        /// <summary>
        /// Controller action to add a course to a specific term on a degree plan. This Action method will return the updated
        /// degree plan model but will not allow the specification of a section.
        /// </summary>
        /// <param name="courseId">The ID of the course to be added</param>
        /// <param name="termId">The ID of the term on which to add the course</param>
        /// <param name="degreePlanJson">The current DegreePlan model as a JSON string</param>
        /// <returns>A JSON representation of the updated DegreePlanModel</returns>
        public async Task<JsonResult> AddCourseAsync(string courseId, string termId, string credits, string degreePlanJson)
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
                if (degreePlan == null)
                {
                    throw new Exception();
                }

                Utility.DegreePlanHelper degreePlanHelper = new Utility.DegreePlanHelper();
                degreePlanHelper.UserId = CurrentUser.PersonId;

                // How many credits?  Check with the course itself, because it might be variable
                var course = await ServiceClient.GetCourseAsync(courseId);
                decimal creditsParse;
                decimal? creditValue = decimal.TryParse(credits, out creditsParse) ? creditsParse : -1;
                if (creditValue == -1 && course != null && course.MinimumCredits.HasValue)
                {
                    creditValue = course.MinimumCredits.Value;
                }
                else if (creditValue == -1)
                {
                    creditValue = null;
                }

                // Build the message to be returned
                string description = string.Empty;
                string message = string.Empty;
                if (course != null)
                {
                    description = course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + course.Number;
                }
                else
                {
                    description = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddingCourseNoCourseInfo");
                }

                // Was the course already planned?
                var alreadyPlannedThisCourse = degreePlanHelper.DoesCourseExistInPlan(degreePlan, new AcademicHistory3(), termId, courseId);

                degreePlanHelper.AddCourse(degreePlan, termId, courseId, GradingType.Graded, string.Empty, creditValue);
                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, new List<Notification>(), updateDegreePlanResponse.AcademicHistory);

                //courses are only added from the search page so we don't care about most notifications
                degreePlanModel.Notifications.Clear();

                //the only ones we do care about are course add status messages
                degreePlanModel.Notifications.Add(new Notification(string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CourseAddedToPlan"), description), NotificationType.Success, true));

                //Was course already taken?
                var alreadyTakenThisCourse = degreePlanHelper.DoesCourseExistInPlan(new DegreePlan4(), updateDegreePlanResponse.AcademicHistory, termId, courseId);

                if (alreadyPlannedThisCourse == CourseExistsStatus.CourseInThisTerm)
                {
                    message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicate"), description);
                }
                else if (alreadyPlannedThisCourse == CourseExistsStatus.CourseInAnotherTerm)
                {
                    message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicateAnotherTerm"), description);
                }
                else if (alreadyTakenThisCourse == CourseExistsStatus.CourseTakenInThisTerm)
                {
                    message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicateTakenThisTerm"), description);
                }
                else if (alreadyTakenThisCourse == CourseExistsStatus.CourseTakenInAnotherTerm)
                {
                    message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicateTakenAnotherTerm"), description);
                }
                if (!string.IsNullOrEmpty(message))
                {
                    degreePlanModel.Notifications.Add(new Notification(message, NotificationType.Information, true));
                }

                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseFailedGeneric"));
            }
        }

        /// <summary>
        /// Controller action to remove a course from a specific term on a degree plan.
        /// </summary>
        /// <param name="removeCourseId">The ID of the course to be removed</param>
        /// <param name="removeCourseTermId">The ID of the term from which to remove the course</param>
        /// <param name="currentModelJson">The active DegreePlan object as a JSON string</param>
        /// <param name="currentAcadHistoryJson">The active AcademicHistory object as a JSON string</param>
        /// <returns>A JSON representation of the updated ViewModel</returns>
        public async Task<JsonResult> RemoveCourseAsync(string removeCourseId, string removeCourseTermId, string removeCourseSectionId, string degreePlanJson)
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                if (degreePlan == null)
                {
                    throw new Exception();
                }

                (new Utility.DegreePlanHelper()).RemoveCourse(degreePlan, removeCourseTermId, removeCourseId, removeCourseSectionId);
                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, null, updateDegreePlanResponse.AcademicHistory);
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RemoveCourseFailedGeneric"));
            }
        }

        /// <summary>
        /// Controller action to add a course and specific section to a specific term on a degree plan. This Action method will only
        /// return a success/failure code, and will NOT return a fully updated view model.
        /// </summary>
        /// <param name="courseId">The ID of the course to be added</param>
        /// <param name="termId">The ID of the term on which to add the course</param>
        /// <param name="sectionId">The ID of the section to add</param>
        /// <param name="gradingType">The grade type chosen by the user for this section, such as Graded or Audit</param>
        /// <param name="credits">The number of credits chosen for this course, or -1 if it does not have any (i.e. it is a CEU course)</param>
        /// <returns>A code indicating success or failure and any possible message that occurred during the course add</returns>
        public async Task<JsonResult> AddSectionAsync(string courseId, string termId, string sectionId, string degreePlanJson, string credits = "-1", Ellucian.Colleague.Dtos.Student.GradingType gradingType = GradingType.Graded)
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
                if (degreePlan == null)
                {
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                Utility.DegreePlanHelper degreePlanHelper = new Utility.DegreePlanHelper();
                degreePlanHelper.UserId = CurrentUser.PersonId;
                bool alreadyHadThisSection = string.IsNullOrEmpty(termId) ? degreePlanHelper.DoesCourseSectionExistInNonTerm(degreePlan, courseId, sectionId) :
                                    degreePlanHelper.DoesCourseSectionExistInTerm(degreePlan, termId, courseId, sectionId);

                Course2 course = await ServiceClient.GetCourseAsync(courseId);
                Section3 section = await ServiceClient.GetSection3Async(sectionId);
                if (!alreadyHadThisSection)
                {
                    //how many credits? Check with the course itself, because it might be variable
                    decimal creditsParse;
                    decimal? creditValue = decimal.TryParse(credits, out creditsParse) ? creditsParse : -1;
                    if (creditValue == -1 && course != null && course.MinimumCredits.HasValue)
                    {
                        creditValue = course.MinimumCredits.Value;
                    }
                    else if (creditValue == -1)
                    {
                        creditValue = null;
                    }
                    //grading type
                    GradingType grading = gradingType;

                    // Was this course already in the plan? Check term/nonterm based on the section.
                    CourseExistsStatus alreadyPlannedThisCourse = CourseExistsStatus.CourseNotInPlan;
                    if (!string.IsNullOrEmpty(termId))
                    {
                        alreadyPlannedThisCourse = degreePlanHelper.DoesCourseExistInPlan(degreePlan, new AcademicHistory3(), termId, courseId);
                        // If a section was added but the course was already planned for the term without an associated section yet, 
                        // reset this status so that we do not warn that a duplicate course was added to the term, because the empty
                        // course will now be associated for this added section. 
                        if (alreadyPlannedThisCourse == CourseExistsStatus.CourseInThisTerm)
                        {
                            var matchingTerm = degreePlan.Terms.Where(t => t.TermId == termId).FirstOrDefault();
                            if (matchingTerm != null)
                            {
                                var matchingCourses = matchingTerm.PlannedCourses.Where(pc => pc.CourseId == courseId && string.IsNullOrEmpty(pc.SectionId));
                                if (matchingCourses.Count() > 0)
                                {
                                    alreadyPlannedThisCourse = CourseExistsStatus.CourseNotInPlan;
                                }
                            }
                        }
                    }
                    else
                    {
                        var planningTerms = (await ServiceClient.GetCachedPlanningTermsAsync()).ToList();
                        alreadyPlannedThisCourse = degreePlanHelper.DoesCourseExistInPlanNonTerm(degreePlan, new AcademicHistory3(), section.StartDate, section.EndDate, courseId, section.Id, planningTerms);
                    }

                    degreePlanHelper.AddCourse(degreePlan, termId, courseId, grading, sectionId, creditValue);
                    var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                    var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                    var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, new List<Notification>(), updateDegreePlanResponse.AcademicHistory);

                    //sections are only added from the search page and the schedule so we don't care about most notifications
                    degreePlanModel.Notifications.Clear();

                    string message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "SectionAddedToPlan"), course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + course.Number + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + section.Number);
                    degreePlanModel.Notifications.Add(new Notification(message, NotificationType.Success, true));

                    // Determine if the course was already taken (ergo, if it is found in the academic history)
                    var alreadyTakenThisCourse = CourseExistsStatus.CourseNotInPlan;
                    if (!string.IsNullOrEmpty(termId))
                    {
                        // Was this course already taken? (check term/nonterm based on the section)
                        alreadyTakenThisCourse = degreePlanHelper.DoesCourseExistInPlan(new DegreePlan4(), updateDegreePlanResponse.AcademicHistory, termId, courseId);
                    }
                    else
                    {
                        // Was this course was already taken non-term?
                        var planningTerms = (await ServiceClient.GetCachedPlanningTermsAsync()).ToList();
                        alreadyTakenThisCourse = degreePlanHelper.DoesCourseExistInPlanNonTerm(new DegreePlan4(), updateDegreePlanResponse.AcademicHistory, section.StartDate, section.EndDate, courseId, section.Id, planningTerms);
                    }

                    // Build string for notification
                    string description = string.Empty;
                    if (course != null)
                    {
                        description = course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + course.Number;
                    }
                    else
                    {
                        description = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddingCourseNoCourseInfo");
                    }

                    // Display a message if the course was already planned or already taken. 
                    // Don't display a message if a section was attached to an already planned course.
                    if (alreadyTakenThisCourse == CourseExistsStatus.CourseTakenInThisTerm)
                    {
                        message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicateTakenThisTerm"), description);
                    }
                    else if (alreadyTakenThisCourse == CourseExistsStatus.CourseTakenInAnotherTerm)
                    {
                        message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicateTakenAnotherTerm"), description);
                    }
                    else if (alreadyPlannedThisCourse == CourseExistsStatus.CourseInThisTerm)
                    {
                        message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicate"), description);
                    }
                    else if (alreadyPlannedThisCourse == CourseExistsStatus.CourseInAnotherTerm)
                    {
                        message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddCourseDuplicateAnotherTerm"), description);
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        degreePlanModel.Notifications.Add(new Notification(message, NotificationType.Information, true));
                    }

                    return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
                }
                else
                {
                    //return success, no warnings for this type of add (since you cannot add the same section twice)
                    string message = (course == null) ? GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CourseSectionAlreadyExistsGeneric") :
                        string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CourseSectionAlreadyExistsSpecific"), course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + course.Number);
                    var student = await ServiceClient.GetPlanningStudentAsync(degreePlan.PersonId);
                    var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(degreePlan, student, new List<Notification>());

                    //sections are only added from the search page and the schedule so we don't care about most notifications
                    degreePlanModel.Notifications.Clear();

                    //the only ones we do care about are section add status messages
                    degreePlanModel.Notifications.Add(new Notification(message, NotificationType.Warning, true));

                    return Json(degreePlanModel);
                }
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddSectionFailedGeneric"));
            }
        }

        /// <summary>
        /// Controller action to remove a section from a degree plan.
        /// </summary>
        /// <param name="degreePlanId">The active DegreePlan Id</param>
        /// <param name="removeSectionId">The ID of the section to be removed</param>
        /// <returns>A JSON representation of the updated DegreePlanViewModel</returns>
        public async Task<JsonResult> RemoveSectionAsync(string degreePlanJson, string removeSectionId)
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                if (degreePlan == null)
                {
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                (new Utility.DegreePlanHelper()).RemoveSection(degreePlan, removeSectionId);
                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, null, updateDegreePlanResponse.AcademicHistory);
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RemoveSectionFailedGeneric"));
            }
        }

        #region Data Retrieval for Building Model
        private async Task<Ellucian.Colleague.Dtos.Planning.PlanningStudent> GetActiveStudentAsync()
        {
            var loggedIn = await ServiceClient.GetPlanningStudentAsync(GetEffectivePersonId());
            if (loggedIn == null)
            {
                throw new InvalidOperationException(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ActiveStudentError"));
            }
            return loggedIn;
        }
        #endregion

        /// <summary>
        /// Given a term and a degree plan id, check the student's eligibility to register
        /// </summary>
        /// <param name="degreePlanId">The degree plan's Id</param>
        /// <param name="termId">The term in which to register</param>
        /// <returns>A list of messages, if any present indicates ineligibility</returns>
        [ActionName("CheckRegistrationEligibility")]
        public async Task<JsonResult> CheckRegistrationEligibilityAsync(string id)
        {
            try
            {
                // If an id isn't provided, check the current user's registration eligibility.
                if (string.IsNullOrEmpty(id))
                {
                    id = GetEffectivePersonId();
                }
                var result = await ServiceClient.CheckRegistrationEligibilityAsync(id);

                var isAdvisor = GetEffectivePersonId() != id;

                RegistrationEligibilityModel registrationEligibility = new RegistrationEligibilityModel();

                // Overall eligibility messages are returned pre-formatted, so loop through them and add to the set of Notifications
                foreach (var message in result.Messages)
                {
                    registrationEligibility.Notifications.Add(new Notification(message.Message, NotificationType.Error, false));
                }

                // Set overall registration override if applicable based on the overall status of the registration eligibility.
                // If a person has an overall override the Registration buttons will be enabled and it is up to Colleague to deal with any
                // fine nuances that might occur within the override group. 
                registrationEligibility.HasRegistrationOverride = result.HasOverride;

                // Term-based registration eligibility (also impacted by term rules and registration priority) aren't formatted, 
                // so loop through and format them, then add them to the set of term messages
                var terms = await ServiceClient.GetCachedPlanningTermsAsync();
                foreach (var term in result.Terms)
                {
                    var message = string.Empty;
                    if (term.AnticipatedTimeForAdds.HasValue)
                    {
                        // Try to get the term's description
                        var termObject = terms.FirstOrDefault(x => x.Code == term.TermCode);
                        var termDescription = term.TermCode;
                        if (termObject != null)
                        {
                            termDescription = termObject.Description;
                        }
                        var messageSubject = isAdvisor ? GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RegistrationEligibilityTermAddMessageAdvisor") : GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RegistrationEligibilityTermAddMessageStudent");
                        message = string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RegistrationEligibilityTermAddMessage"), messageSubject, termDescription, term.AnticipatedTimeForAdds.Value.LocalDateTime.ToString("d"), term.AnticipatedTimeForAdds.Value.LocalDateTime.ToString("t"));
                    }
                    // If the user overall is allowed to register then we need to specifically check the terms to see if they are allowed term by term.  If they fail eligibility overall then none of the terms are eligible.
                    // Term by Term the AllowRegistration property is set by looking specifically at whether the person failed registration rules or priorities.  This means that
                    // even when the term period dates are not specifically met it will still show as Allowed here and then the specific section registration dates can be applied as needed.
                    bool termAllowRegistration = false;
                    if (result.IsEligible || result.HasOverride)
                    {
                        termAllowRegistration = (!term.FailedRegistrationPriorities && !term.FailedRegistrationTermRules) || term.Status == RegistrationEligibilityTermStatus.HasOverride;
                    }
                    registrationEligibility.Terms.Add(new TermEligibilityModel(term.TermCode, message, termAllowRegistration, term.SkipWaitlistAllowed));
                }

                return Json(registrationEligibility, JsonRequestBehavior.AllowGet);
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message, JsonRequestBehavior.AllowGet);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UnableToCheckEligibilityError"), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Register the selected section
        /// </summary>
        /// <param name="studentId">The student's ID</param>
        /// <param name="sectionRegistrations">The registration requests which include the section Id and the action to take</param>
        /// <returns>A JSON Result containing a list of any registration notification messages</returns>
        [ActionName("RegisterSections")]
        public async Task<JsonResult> RegisterSectionsAsync(string studentId, IEnumerable<SectionRegistration> sectionRegistrations)
        {
            var response = new RegistrationResponse();
            var messages = new List<RegistrationMessage>();

            try
            {
                var notifications = new List<Notification>();
                response = await ServiceClient.RegisterAsync(studentId, sectionRegistrations);
                messages = response.Messages;

                NotificationType type = NotificationType.Warning;
                if (!Enum.TryParse<NotificationType>(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RegistrationNotificationType"), out type))
                {
                    type = NotificationType.Warning;
                }
                foreach (var message in messages)
                {
                    notifications.Add(new Notification(message.Message, type, false));
                }
                // Add a notification if the student is required to make a payment
                AddPaymentNotification(response.PaymentControlId, notifications);

                return new JsonResult() { Data = notifications, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UnableToProcessRegistrationError"));
            }
        }

        /// <summary>
        /// Action method for asynchronously submitting a plan for advisor approval.
        /// </summary>
        /// <param name="degreePlanJson">The active DegreePlan object as a JSON string</param>
        /// <returns>A JSON model representing the updated plan view</returns>
        public async Task<JsonResult> SendToAdvisorAsync(string degreePlanJson)
        {
            try
            {
                if (degreePlanJson == null)
                {
                    throw new ArgumentNullException("degreePlanJson");
                }

                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                if (degreePlan == null)
                {
                    throw new InvalidOperationException("Unable to deserialize the degree plan.");
                }

                degreePlan.ReviewRequested = true;

                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);

                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                Notification notify = new Notification(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "PlanSubmitted"), NotificationType.Success);
                DegreePlanModel degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, new List<Notification>() { notify }, updateDegreePlanResponse.AcademicHistory);

                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "RemoveTermFailedGeneric"));
            }
        }

        /// <summary>
        /// Return an iCal file containing meeting info for registered sections in the selected term
        /// </summary>
        /// <param name="termId">The term Id from which to build the iCal</param>
        /// <returns>An iCal (string) that contains meeting info</returns>
        [ActionName("GetScheduleIcal")]
        public async Task<ActionResult> GetScheduleIcalAsync(string termId)
        {
            try
            {
                AcademicHistory3 academicHistory = null;
                AcademicTerm3 term = null;
                try
                {
                    academicHistory = await ServiceClient.GetAcademicHistory3Async(GetEffectivePersonId());
                    term = academicHistory.AcademicTerms.Where(a => a.TermId == termId).First();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.ToString());
                    return File(Encoding.UTF8.GetBytes("Unable read academic history for term: " + termId), "text/plain", "error.txt");
                }

                var iCal = string.Empty;
                if (term.AcademicCredits != null)
                {
                    var sectionIds = new List<String>();
                    foreach (var credit in term.AcademicCredits.Where(c => !string.IsNullOrEmpty(c.SectionId)))
                    {
                        sectionIds.Add(credit.SectionId);
                    }
                    iCal = await ServiceClient.GetSectionEventsAsync(sectionIds);
                }
                var rgx = new Regex(@"[^\w]|_");
                var fileName = "schedule." + rgx.Replace(termId, "") + ".ics";
                return File(Encoding.UTF8.GetBytes(iCal), "text/calendar", fileName);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                return File(Encoding.UTF8.GetBytes("Unable to generate iCal for term: " + termId), "text/plain", "error.txt");
            }
        }

        /// <summary>
        /// Return a simple schedule view that prints better than the full schedule
        /// </summary>
        /// <returns>A printable view of the student's schedule</returns>
        [ActionName("PrintSchedule")]
        public async Task<ActionResult> PrintScheduleAsync(string termId)
        {
            try
            {
                //who is logged in?
                var student = await ServiceClient.GetPlanningStudentAsync(GetEffectivePersonId());

                //get their plan if they have one; if not, this should create one implicitly
                var degreePlanAcademicHistory = await GetOrCreateDegreePlanAsync(student, false);

                if (degreePlanAcademicHistory.DegreePlan == null)
                {
                    //the student somehow does not already have a degree plan even after trying to implicitly create one, so this is an error condition
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(degreePlanAcademicHistory.DegreePlan, student, null, degreePlanAcademicHistory.AcademicHistory, true);
                degreePlanModel.TermToShow = termId;

                ViewBag.ResultJSON = JsonConvert.SerializeObject(degreePlanModel);

                ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ScheduleAreaDescription");
                Settings appSettings = DependencyResolver.Current.GetService<Settings>();
                ViewBag.HeaderLogoPath = appSettings.HeaderLogoPath;

                return PartialView();
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                var viewModel = new DegreePlanModel(new List<Notification>() { new Notification(ioe.Message, NotificationType.Error) });
                ViewBag.ResultJSON = JsonConvert.SerializeObject(viewModel);
                return PartialView();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                var viewModel = new DegreePlanModel(new List<Notification>() { new Notification(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiUnknownError"), NotificationType.Error) });
                ViewBag.ResultJSON = JsonConvert.SerializeObject(viewModel);
                return PartialView();
            }
        }

        /// <summary>
        /// Updates a course by moving it from one term to another. This can only be done on courses - specific sections are stuck in the terms
        /// in which they have been planned.
        /// </summary>
        /// <param name="courseId">The ID of the planned course</param>
        /// <param name="oldTerm">The term in which it used to be placed</param>
        /// <param name="newTerm">The term to which the course should be moved</param>
        /// <param name="degreePlanJson">The existing degree plan in JSON format</param>
        /// <returns>A JSON response containing the updated data model</returns>
        public async Task<JsonResult> UpdateCourseAsync(string courseId, string oldTerm, string newTerm, string degreePlanJson = "")
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                if (degreePlan == null)
                {
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                bool courseMoved = false;

                var removeFromTerm = degreePlan.Terms.Where(t => t.TermId == oldTerm).FirstOrDefault();
                var moveToTerm = degreePlan.Terms.Where(t => t.TermId == newTerm).FirstOrDefault();
                DegreePlanAcademicHistory2 updateDegreePlanResponse = new DegreePlanAcademicHistory2();
                if (removeFromTerm != null)
                {
                    var course = removeFromTerm.PlannedCourses.Where(c => c.CourseId == courseId).FirstOrDefault();
                    if (course != null)
                    {
                        removeFromTerm.PlannedCourses.Remove(course);
                        if (moveToTerm != null)
                        {
                            moveToTerm.PlannedCourses.Add(course);
                        }
                        else
                        {
                            // Add new term
                            degreePlan.Terms.Add(new DegreePlanTerm4()
                            {
                                TermId = newTerm,
                                PlannedCourses = new List<PlannedCourse4>() { course }
                            });
                        }
                        courseMoved = true;
                        updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                    }
                }
                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, null, updateDegreePlanResponse.AcademicHistory);

                if (courseMoved)
                {
                    // Build the message to be returned in notification
                    string description = string.Empty;
                    string message = string.Empty;
                    var movedCourse = await ServiceClient.GetCourseAsync(courseId);
                    string movedToTermDescription = newTerm;
                    var allTerms = await ServiceClient.GetCachedTermsAsync();
                    if (allTerms != null)
                    {
                        var movedToTerm = allTerms.Where(t => t.Code == newTerm).FirstOrDefault();
                        if (movedToTerm != null)
                        {
                            movedToTermDescription = movedToTerm.Description;
                        }
                    }
                    if (movedCourse != null)
                    {
                        description = movedCourse.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + movedCourse.Number;
                    }
                    else
                    {
                        description = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "AddingCourseNoCourseInfo");
                    }
                    degreePlanModel.Notifications.Add(new Notification(string.Format(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "CourseMovedOnPlan"), description, movedToTermDescription), NotificationType.Success, true));
                }
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UpdateCourseFailedGeneric"));
            }
        }

        /// <summary>
        /// Controller action to asynchronously add a new advising note to a plan.
        /// </summary>
        /// <remarks>This method cannot be in the AdvisorsController, because that entire controller requires menu-based authorization;
        /// in other words, only users with an advisor role can hit action methods in the AdvisorsController class, which blocks students from adding notes.</remarks>
        /// <param name="message">The new message to add.</param>
        /// <param name="personType">The type of author (student, advisor, etc)</param>
        /// <param name="degreePlanJson">The existing degree plan as a JSON string.</param>
        /// <returns>A JSON representation of the updated model.</returns>
        public async Task<JsonResult> AddAdvisingNoteAsync(string message, Ellucian.Colleague.Dtos.Student.PersonType personType, string degreePlanJson)
        {
            try
            {
                //don't even try to submit an empty note; also, the plan is required
                if (string.IsNullOrEmpty(message))
                {
                    throw new Exception();
                }
                if (string.IsNullOrEmpty(degreePlanJson))
                {
                    throw new ArgumentException("degreePlanJson must not be null", "degreePlanJson");
                }

                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
                if (degreePlan == null)
                {
                    throw new ArgumentException("degreePlanJson could not be deserialized", "degreePlanJson");
                }

                Ellucian.Colleague.Dtos.Planning.DegreePlanNote2 note = new DegreePlanNote2()
                {
                    Date = null,
                    Id = 0, //0 signifies a new note; no need to enter ID or Date here (tx will provide)
                    PersonId = null,
                    PersonType = personType,
                    //an argument could be made for sanitization here as well, but we're going with the purist approach - what goes to the API is the true note contents
                    Text = message
                };

                degreePlan.Notes.Add(note);
                var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                var dpm = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, null, updateDegreePlanResponse.AcademicHistory);
                return new JsonResult() { Data = dpm, MaxJsonLength = Int32.MaxValue };
            }
            catch (ArgumentException e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "NoteAddNoPlanFailure"));
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "NoteAddGenericFailure"));
            }
        }

        /// <summary>
        /// POST endpoint to get available sections for a set of planned courses in a term on a plan.
        /// </summary>
        /// <param name="plannedCourses">Required. The Planned Courses for which the available sections are requested.  Must have at least 1.</param>
        /// <param name="term">The term for which the available sections are being requested.</param>
        /// <param name="locations">Selected location filters from the schedule</param>
        /// <param name="days">Selected day of week filters from the schedule</param>
        /// <param name="startTime">Selected start time from the time filters on the schedule</param>
        /// <param name="endTime">Selected end time from the time filters on the schedule</param>
        /// <param name="faculty">Selected faculty from the instructor filters on the schedule</param>
        /// <returns></returns>
        public async Task<JsonResult> GetAvailableSectionsAsync(AvailableSectionSearch availableSectionSearch)
        {
            try
            {
                if (availableSectionSearch == null)
                {
                    throw new ArgumentNullException("availableSectionSearch");
                }

                if (availableSectionSearch.PlannedCourses == null || availableSectionSearch.PlannedCourses.Count <= 0)
                {
                    throw new ArgumentOutOfRangeException("availableSectionSearch", "At least one planned courses required to retrieve available sections");
                }
                if (string.IsNullOrEmpty(availableSectionSearch.TermCode))
                {
                    throw new ArgumentOutOfRangeException("availableSectionSearch", "A valid term must exist in the section search.");
                }

                var courseIds = availableSectionSearch.PlannedCourses.Select(c => c.CourseId);

                // Set the quantity per page to a high number to get them all....
                int quantityPerPage = Int16.MaxValue;
                int pageNumber = 1;

                // Set up the input criteria
                var terms = new List<string>() { availableSectionSearch.TermCode };
                var locations = !string.IsNullOrEmpty(availableSectionSearch.Location) ? availableSectionSearch.Location.Split(new char[] { ',' }).ToList() : null;
                var faculty = !string.IsNullOrEmpty(availableSectionSearch.Faculty) ? availableSectionSearch.Faculty.Split(new char[] { ',' }).ToList() : null;
                var days = !string.IsNullOrEmpty(availableSectionSearch.Day) ? availableSectionSearch.Day.Split(new char[] { ',' }).ToList() : null;
                var topics = !string.IsNullOrEmpty(availableSectionSearch.Topic) ? availableSectionSearch.Topic.Split(new char[] { ',' }).ToList() : null;
                var availableOnly = false;
                if (availableSectionSearch.AvailableOnly == "Open")
                {
                    availableOnly = true;
                }
                int? earliestTime = null;
                int? latestTime = null;
                if (!string.IsNullOrEmpty(availableSectionSearch.TimeCategory))
                {
                    if (availableSectionSearch.TimeCategory == "Early")
                    {
                        earliestTime = 0;
                        latestTime = 480;
                    }
                    if (availableSectionSearch.TimeCategory == "Morning")
                    {
                        earliestTime = 480;
                        latestTime = 720;
                    }
                    if (availableSectionSearch.TimeCategory == "Afternoon")
                    {
                        earliestTime = 720;
                        latestTime = 960;
                    }
                    if (availableSectionSearch.TimeCategory == "Evening")
                    {
                        earliestTime = 960;
                        latestTime = 1200;
                    }
                    if (availableSectionSearch.TimeCategory == "Night")
                    {
                        earliestTime = 1200;
                        latestTime = 1440;
                    }
                }

                var result = await ServiceClient.SearchCoursesAsync(courseIds, null, null, null, null, topics, terms, days, locations, faculty, earliestTime, latestTime, null, null, null, null, null, quantityPerPage, pageNumber);

                List<Course2> courseDTOs = new List<Course2>();
                if (courseIds.Count() > 0)
                {
                    try
                    {
                        courseDTOs = (await ServiceClient.QueryCourses2Async(new CourseQueryCriteria() { CourseIds = courseIds })).ToList();
                        var notFoundCourseIds = courseIds.Except(courseDTOs.Select(c => c.Id));
                        foreach (var courseId in notFoundCourseIds)
                        {
                            Logger.Error("GetAvailableSectionsAsync: Cannot retrieve course DTO for course " + courseId);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, "Error to retrieving courses for courseids " + courseIds + ": " + ex.Message);
                    }
                }

                // Gather up any DTOs necessary to build the view model
                IEnumerable<Section3> sectionDTOs = new List<Section3>();
                try
                {
                    var sectionIds = result.CurrentPageItems.SelectMany(s => s.MatchingSectionIds).ToList();
                    // Drop out the sections already contained on the planned courses supplied.
                    IEnumerable<string> plannedSectionIds = availableSectionSearch.PlannedCourses.Where(p => p.SectionId != null).Select(x => x.SectionId).ToList();
                    sectionIds = sectionIds.Except(plannedSectionIds).ToList();
                    sectionDTOs = await ServiceClient.GetSections4Async(sectionIds, false);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "Cannot retreive section DTOs");
                    // model will still be returned with no available sections for each planned course.
                }



                var locationDTOs = await ServiceClient.GetCachedLocationsAsync();

                var facultyDTOs = new List<Faculty>();
                var facultyIds = result.Faculty.Select(f => f.Value).Distinct();
                try
                {
                    facultyDTOs = (await ServiceClient.QueryFacultyAsync(new FacultyQueryCriteria() { FacultyIds = facultyIds })).ToList();
                    var notFoundFacultyIds = facultyIds.Except(facultyDTOs.Select(f => f.Id));
                    foreach (var facId in notFoundFacultyIds)
                    {
                        Logger.Error("Cannot retrieve faculty DTO for facultyId " + facId + ", building an empty DTO to allow UI to continue.");
                        facultyDTOs.Add(new Faculty() { Id = facId, FirstName = "Unavailable", LastName = "Unavailable", ProfessionalName = "Unavailable", EmailAddresses = new List<string>(), Phones = new List<Phone>() });
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                }

                var buildingDTOs = await ServiceClient.GetCachedBuildingsAsync();
                var roomDTOs = await ServiceClient.GetCachedRoomsAsync();
                var instructionalMethodDTOs = await ServiceClient.GetCachedInstructionalMethodsAsync();
                var topicDTOs = await ServiceClient.GetCachedTopicCodesAsync();

                // Return a model that has the filters and the planned section models needed to re-render the page.

                var viewModel = new AvailableSectionResultModel(result, availableSectionSearch.PlannedCourses, availableOnly, courseDTOs, sectionDTOs, locationDTOs, facultyDTOs, buildingDTOs, roomDTOs, instructionalMethodDTOs, topicDTOs);
                return new JsonResult() { Data = viewModel, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiNoOtherSectionsLabel"));
            }
        }

        /// <summary>
        /// Add a notification that payment is required
        /// </summary>
        /// <param name="payControlId">Payment control ID</param>
        /// <param name="notifications">List of notifications</param>
        private void AddPaymentNotification(string payControlId, List<Notification> notifications)
        {
            if (!String.IsNullOrEmpty(payControlId))
            {
                var message = GlobalResources.GetString(GlobalResourceFiles.StudentResources, "RegistrationPaymentNotificationMessage");
                NotificationType type = NotificationType.Information;
                if (!Enum.TryParse<NotificationType>(GlobalResources.GetString(GlobalResourceFiles.StudentResources, "RegistrationPaymentNotificationType"), out type))
                {
                    type = NotificationType.Error;
                }
                notifications.Add(new Notification(message, type, false));
            }
        }

        public async Task<JsonResult> ArchivePlanAsync(string degreePlanJson)
        {
            try
            {
                if (string.IsNullOrEmpty(degreePlanJson))
                {
                    throw new ArgumentNullException("degreePlanJson");
                }

                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                var archiveDto = await ServiceClient.ArchiveDegreePlan3Async(degreePlan);

                var archives = await DegreePlanModelBuilder.BuildPlanArchiveModelsAsync(new List<DegreePlanArchive2>() { archiveDto }, CurrentUser.PersonId);

                return Json(archives, JsonRequestBehavior.AllowGet);

            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to retrieve archives");
            }
        }

        [ActionName("GetPlanArchives")]
        public async Task<JsonResult> GetPlanArchivesAsync(string degreePlanId)
        {
            try
            {
                if (string.IsNullOrEmpty(degreePlanId))
                {
                    throw new ArgumentNullException("degreePlanId");
                }

                var planArchives = await ServiceClient.GetArchivedPlans2Async(degreePlanId);
                var planArchiveModels = await DegreePlanModelBuilder.BuildPlanArchiveModelsAsync(planArchives, CurrentUser.PersonId);
                return Json(planArchiveModels, JsonRequestBehavior.AllowGet);

            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to retrieve archives");
            }
        }

        /// <summary>
        /// Retrieves a plan archive report (PDF)
        /// </summary>
        /// <param name="archiveId">The plan archive for which to retrieve the report.</param>
        /// <returns></returns>
        [ActionName("GetPlanArchiveReport")]
        public async Task<ActionResult> GetPlanArchiveReportAsync(string archiveId)
        {
            try
            {
                string fileName = string.Empty;
                var result = await ServiceClient.GetArchivedPlanReportAsync(archiveId);
                var report = result.Item1;
                fileName = result.Item2;
                MemoryStream memoryStream = new MemoryStream(report);
                return File(memoryStream, "application/Pdf", fileName);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return null;
            }
        }

        /// <summary>
        /// Controller action to remove all planned courses from a specific term on a degree plan.
        /// </summary>
        /// <param name="removeCourseTermId">The ID of the term from which to remove the course. Required. </param>
        /// <param name="currentModelJson">The active DegreePlan object as a JSON string</param>
        /// <param name="currentAcadHistoryJson">The active AcademicHistory object as a JSON string</param>
        /// <returns>A JSON representation of the updated degree plan ViewModel</returns>
        public async Task<JsonResult> ClearPlannedCoursesAsync(IEnumerable<string> clearTermIds, string degreePlanJson)
        {
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                if (degreePlan == null)
                {
                    throw new Exception();
                }
                if (clearTermIds == null || clearTermIds.Count() == 0)
                {
                    return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ClearPlannedCoursesFailedGeneric"));
                }
                bool cleared = (new Utility.DegreePlanHelper()).ClearPlannedCourses(degreePlan, clearTermIds);
                if (cleared)
                {
                    var updateDegreePlanResponse = await ServiceClient.UpdateDegreePlan5Async(degreePlan);
                    var student = await ServiceClient.GetPlanningStudentAsync(updateDegreePlanResponse.DegreePlan.PersonId);
                    var degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(updateDegreePlanResponse.DegreePlan, student, null, updateDegreePlanResponse.AcademicHistory);
                    return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
                }
                // If not cleared it means that term was not on the plan or there were no planned courses that could be removed.
                throw new Exception();

            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ClearPlannedCoursesFailedGeneric"));
            }
        }

        // <summary>
        /// The Complete Registration method retrieves the student's degree plan after a registration action and includes
        /// any registration messages that are to be shown in the notification center. 
        /// This portion of RegisterSections was broken out to provide the student with a "progress" message.
        /// This method only returns the degree plan model in the json result (different than Current Async).
        /// </summary>
        /// <param name="studentId">The student's ID</param>
        /// <param name="messages">Messages to be included in the notification center.</param>
        /// <returns>An updated, JSON serialized Degree Plan including registration notifications.</returns>
        [ActionName("CompleteRegistration")]
        public async Task<JsonResult> CompleteRegistrationAsync(string studentId, IEnumerable<Notification> notifications = null)
        {
            try
            {
                if (notifications == null)
                {
                    notifications = new List<Notification>();
                }
                var student = await ServiceClient.GetPlanningStudentAsync(studentId);
                var degreePlanAcademicHistory = await GetOrCreateDegreePlanAsync(student);
                DegreePlanModel degreePlanModel = await DegreePlanModelBuilder.BuildDegreePlanModelAsync(degreePlanAcademicHistory.DegreePlan, student, notifications.ToList(), degreePlanAcademicHistory.AcademicHistory, true);
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.Error(ioe.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ioe.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UnableToProcessRegistrationError"));
            }
        }

        /// <summary>
        /// Retrieve student petitions , faculty consents and waivers
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public async Task<JsonResult> GetStudentPetitionsWaiversAsync(string studentId)
        {
            if (string.IsNullOrEmpty(studentId)) { studentId = GetEffectivePersonId(); };
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException("studentId", "studentId is required to get petitions and waivers.");
            }
            try
            {
                List<StudentPetitionModel> petitions = new List<StudentPetitionModel>();
                List<StudentPetitionModel> consents = new List<StudentPetitionModel>();
                List<StudentWaiverViewModel> waivers = new List<StudentWaiverViewModel>();
                List<string> courseIds = new List<string>();
                List<string> sectionIds = new List<string>();
                IEnumerable<Term> terms = null;
                IEnumerable<Course2> courses = null;
                IEnumerable<Section3> sections = null;
                IEnumerable<StudentPetition> studentPetitions = await ServiceClient.GetStudentPetitionsAsync(studentId);
                IEnumerable<StudentWaiver> studentWaivers = await ServiceClient.GetStudentWaiversAsync(studentId);
                //get distinct courseIds and sectionIds from petitions and waivers to retrieve Dtos from API in single shot
                if (studentPetitions != null && studentPetitions.Any())
                {
                    courseIds.AddRange(studentPetitions.Where(s => !string.IsNullOrEmpty(s.CourseId)).Select(s => s.CourseId));
                    sectionIds.AddRange(studentPetitions.Where(s => !string.IsNullOrEmpty(s.SectionId)).Select(s => s.SectionId));
                }
                if (studentWaivers != null && studentWaivers.Any())
                {
                    courseIds.AddRange(studentWaivers.Where(s => !string.IsNullOrEmpty(s.CourseId)).Select(s => s.CourseId));
                    sectionIds.AddRange(studentWaivers.Where(s => !string.IsNullOrEmpty(s.SectionId)).Select(s => s.SectionId));
                }
                courses = (courseIds != null && courseIds.Any()) ? await ServiceClient.QueryCourses2Async(new CourseQueryCriteria() { CourseIds = courseIds.Distinct() }) : null;
                sections = (sectionIds != null && sectionIds.Any()) ? await ServiceClient.GetSections4Async(sectionIds.Distinct()) : null;
                terms = await ServiceClient.GetCachedTermsAsync();
                if (studentPetitions != null && studentPetitions.Any())
                {
                    var petitionStatuses = await ServiceClient.GetCachedPetitionStatusesAsync();
                    var petitionReasons = await ServiceClient.GetCachedStudentPetitionReasonsAsync();

                    petitions = FacultyHelper.BuildStudentPetitions(studentPetitions.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatuses, petitionReasons, courses, sections, terms);
                    consents = FacultyHelper.BuildStudentPetitions(studentPetitions.Where(s => s.Type == StudentPetitionType.FacultyConsent), petitionStatuses, petitionReasons, courses, sections, terms);
                }
                if (studentWaivers != null && studentWaivers.Any())
                {
                    waivers = await DegreePlanModelBuilder.BuildStudentWaiversAsync(studentWaivers, courses, sections, terms);
                }
                return Json(new { StudentPetitions = petitions, StudentConsents = consents, StudentWaivers = waivers }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiGenericError"), JsonRequestBehavior.AllowGet);
            }
        }
    }
}