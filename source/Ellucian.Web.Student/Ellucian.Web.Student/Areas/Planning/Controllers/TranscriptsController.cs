﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using slf4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class TranscriptsController : BaseStudentController
    {

        /// <summary>
        /// Creates a new TranscriptsController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public TranscriptsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        /// <summary>
        /// Index page for viewing unofficial transcripts.
        /// </summary>
        /// <returns>A View listing all links to view unofficial transcripts for a student</returns>
        [LinkHelp]
        [PageAuthorize("unofficialTranscripts")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.TranscriptResources, "AreaDescription");

            return View();
        }

        public async Task<JsonResult> GetStudentTranscriptGroupings(string studentId)
        {
            if (string.IsNullOrEmpty(studentId)) studentId = GetEffectivePersonId();

            var studentProgramTask = ServiceClient.GetStudentProgramsAsync(studentId, false);
            var programTask = ServiceClient.GetCachedProgramsAsync();
            var transcriptGroupingTask = ServiceClient.GetSelectableTranscriptGroupingsAsync();
            await Task.WhenAll(studentProgramTask, programTask, transcriptGroupingTask);

            var studentPrograms = studentProgramTask.Result;
            var programs = programTask.Result;
            var transcriptGroupings = transcriptGroupingTask.Result;

            var transcriptGroupingCodes = new List<string>();
            foreach (var studentProgram in studentPrograms)
            {
                var program = programs.Where(p => p.Code == studentProgram.ProgramCode).FirstOrDefault();
                if (!string.IsNullOrEmpty(program.UnofficialTranscriptGrouping))
                {
                    transcriptGroupingCodes.Add(program.UnofficialTranscriptGrouping);
                }
                else
                {
                transcriptGroupingCodes.Add(program.TranscriptGrouping);
            }
            }

            transcriptGroupings = from tgs in transcriptGroupings
                                  where transcriptGroupingCodes.Contains(tgs.Id)
                                  select tgs;

            return Json(transcriptGroupings, JsonRequestBehavior.AllowGet);
        }

        ///<summary>
        ///Retrieves a student's unofficial transcripts
        ///</summary>
        ///<param name="studentId">Student id for which unofficial transcripts are requested</param>
        ///<returns>The list of unofficial transcript results for this student</returns>
        public async Task<ActionResult> GetUnofficialTranscriptAsync(string transcriptGrouping, string studentId)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId)) { studentId = GetEffectivePersonId(); };
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId", "studentId is required to get unofficial transcripts");
                }
                var result = await ServiceClient.GetUnofficialTranscriptAsync(studentId, transcriptGrouping);
                var report = result.Item1;
                string fileName  = result.Item2;
                MemoryStream memoryStream = new MemoryStream(report);
                return File(memoryStream, "application/pdf", fileName);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                var message = GlobalResources.GetString(PlanningResourceFiles.TranscriptResources, "TranscriptError");
                return Content(message, "text/plain");
            }
        }

        public async Task<ActionResult> GetTranscriptRestrictionAsync(string studentId)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId)) { studentId = GetEffectivePersonId(); };
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId", "studentId is required to get transcript restriction");
                }

                TranscriptAccess transcriptAccess = await ServiceClient.GetTranscriptRestrictions2Async(studentId);
                return Json(transcriptAccess, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                var message = GlobalResources.GetString(PlanningResourceFiles.TranscriptResources, "TranscriptError");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new HttpNotFoundResult();
            }
        }
    }
}