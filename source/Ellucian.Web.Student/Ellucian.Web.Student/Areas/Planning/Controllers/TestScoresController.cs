﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Planning.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class TestScoresController : BaseStudentController
    {

        /// <summary>
        /// Creates a new TestScoresController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public TestScoresController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        /// <summary>
        /// Index page for viewing tests.
        /// </summary>
        /// <returns>A View listing all tests for a student</returns>
        [LinkHelp]
        [PageAuthorize("testSummary")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.TestScoreResources, "AreaDescription");
            return View();
        }


        /// <summary>
        /// Retrieves a student's test scores
        /// </summary>
        /// <param name="studentId">Student id for which scores are requested</param>
        /// <returns>The list of test results for this student</returns>
        [PageAuthorize("testSummary", "advisees")]
        public async Task<JsonResult> GetTestScoresAsync(string studentId)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId)) { studentId = GetEffectivePersonId(); };
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId", "studentId is required to get student test scores");
                }

                var testResultTask = ServiceClient.GetTestResults2Async(studentId, null);
                var testTask = ServiceClient.GetTestsAsync();
                var noncourseStatusTask = ServiceClient.GetNoncourseStatusesAsync();

                await Task.WhenAll(testResultTask, testTask, noncourseStatusTask);

                var testResultViewModel = new TestResultViewModel(testResultTask.Result, testTask.Result, noncourseStatusTask.Result);
                return Json(testResultViewModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to retrieve test scores");
            }
        }
    }
}
