﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Planning;
using slf4net;
using Newtonsoft.Json;
using Ellucian.Web.Student.Areas.Planning.Utility;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [MenuAuthorize("faculty")]
    [OutputCache(CacheProfile = "NoCache")]
    public class FacultyController : BaseStudentController
    {
        /// <summary>
        /// Creates a new FacultyController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public FacultyController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        /// <summary>
        /// Returns the default Index view for Faculty, which shows the logged in faculty's list of active course sections (if any).
        /// </summary>
        /// <returns>The Faculty Index View</returns>
        [PageAuthorize("sections")]
        [LinkHelp]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "AreaDescription");

            return View();
        }

        /// <summary>
        /// Navigate to the options for faculty selected course section 
        /// </summary>
        /// /// <param name="id">Section Id</param>
        /// <returns></returns>
        [HttpGet]
        [LinkHelp]
        public ActionResult FacultyNavigation(string id)
        {
            // Configure the view
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "AreaDescription");
            ViewBag.SectionId = id;
            return View();
        }

        /// <summary>
        /// Validates and returns information about a waived student. 
        /// If the student Id is not a valid student OR if the student already has a waiver, then an exception is thrown.
        /// </summary>
        /// <param name="studentId">The student Id to retrieve</param>
        /// <param name="sectionId">The section for which a waiver is being added.</param>
        /// <returns>A JSON StudentModel</returns>
        public async Task<JsonResult> ValidateWaivedStudentAsync(string studentId, string sectionId)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId");
                }
                if (string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException("sectionId");
                }
                var student = await ServiceClient.GetPlanningStudentAsync(studentId);
                if (student != null)
                {
                    // verify that this student does not already have a waiver for the specified section
                    var studentWaivers = (await ServiceClient.GetSectionStudentWaiversAsync(sectionId)).ToList();
                    if (studentWaivers.Where(sw => sw.StudentId == studentId).FirstOrDefault() == null)
                    {
                        StudentModel waivedStudent = new StudentModel(student);
                        return Json(waivedStudent, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        // If the student already has a waiver set a message and return a different status code (409).
                        Response.StatusCode = (int)HttpStatusCode.Conflict;
                        return Json(GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "StudentAlreadyHasWaiverMessage"), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    throw new Exception("Student not found.");
                }
            }
            catch (Exception exception)
            {
                // Throw a 404
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// to retrieve student petitions and faculty consents
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<JsonResult> GetFacultySectionPetitionsAsync(string sectionId)
        {
            try
            {
                //var termData = Term("2015/SP", "Spring 2015 Term", DateTime.Parse("2015-01-13"), DateTime.Parse("2015-04-12"), 2015, 1, false, false, "2015/SP", false);
              //  var values = await ServiceClient.QueryStudentAffiliationsAsync(new List<string>() { "0000304", "0000404", "0000504", "0000604", "0000704" },"2015/SP","");
                if (string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException("sectionId", "sectionId is required to get section information");
                }

                // Get the section to figure out who the section instructors are
                var sectionDto = await ServiceClient.GetSection3Async(sectionId);
                if (sectionDto != null)
                {
                    var petitionStatusTask = ServiceClient.GetCachedPetitionStatusesAsync();
                    var petitionReasonTask = ServiceClient.GetCachedStudentPetitionReasonsAsync();
                    await Task.WhenAll(petitionStatusTask, petitionReasonTask);
                    var petitionStatusDtos = petitionStatusTask.Result;
                    var petitionReasonDtos = petitionReasonTask.Result;

                    var sectionPermissionViewModel = new FacultySectionPermissionsModel(petitionStatusDtos, petitionReasonDtos);
                    SectionPermission sectionPermissionDto = new SectionPermission();
                    // Get any section permissions for this section 
                    try
                    {
                        sectionPermissionDto = await ServiceClient.GetSectionPermissionsAsync(sectionId);
                    }
                    catch (Exception)
                    {
                        // If we cannot get the section permisions (i.e. it is a section this faculty does not teach)
                        // do nothing. Rest of section info can be retrieved.
                    }

                    // Get the students for any of the section permisions (student petitions and faculty consents)
                    var studentDtos = new List<PlanningStudent>();
                    try
                    {
                        var studentIds = new List<string>();
                        studentIds.AddRange(sectionPermissionDto.StudentPetitions.Select(sp => sp.StudentId).Where(s => !string.IsNullOrEmpty(s)));
                        studentIds.AddRange(sectionPermissionDto.FacultyConsents.Select(fc => fc.StudentId).Where(s => !string.IsNullOrEmpty(s)));
                        studentIds = studentIds.Distinct().ToList();
                        studentDtos = (await ServiceClient.QueryPlanningStudentsAsync(studentIds)).ToList();
                    }
                    catch (Exception)
                    {
                        // Do nothing. If we can't get the student due to permissions or something then send thru an empty list
                        // and student data will be unknown.
                    }

                    // Get the faculty for any of the section permissions (student petitions and faculty consents)
                    var facultyDtos = new List<Faculty>();
                    if (sectionDto != null && sectionDto.FacultyIds != null && sectionDto.FacultyIds.Count() > 0)
                    {
                        try
                        {
                            facultyDtos = (await ServiceClient.GetFacultyByIdsAsync(sectionDto.FacultyIds)).ToList();
                        }
                        catch
                        {
                            // Really no reason this should fail but if it does send through an empty list and the faculty information will just be missing.
                        }
                    }

                    //add student petitions and faculty consents to View Model
                    sectionPermissionViewModel.StudentPetitions = FacultyHelper.BuildStudentPetitions(sectionPermissionDto.StudentPetitions, petitionStatusDtos, petitionReasonDtos, studentDtos, facultyDtos);
                    sectionPermissionViewModel.FacultyConsents = FacultyHelper.BuildStudentPetitions(sectionPermissionDto.FacultyConsents, petitionStatusDtos, petitionReasonDtos, studentDtos, facultyDtos);

                    //get faculty permissions
                    try
                    {
                        var permissions = await ServiceClient.GetFacultyPermissionsAsync();
                        sectionPermissionViewModel.CanFacultyAddStudentPetitions = permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "PermissionsCreateStudentPetition"), StringComparer.InvariantCultureIgnoreCase);
                        sectionPermissionViewModel.CanFacultyAddFacultyConsents = permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "PermissionsCreateFacultyConsent"), StringComparer.InvariantCultureIgnoreCase);
                    }
                    catch (Exception exception)
                    {
                        // If we can't get the person's faculty permissions the CanFacultyAddStudentPetitions and CanFacultyAddFacultyConsents default to false. 
                        Logger.Error(exception.ToString());
                    }

                    return Json(sectionPermissionViewModel, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Logger.Error("Unable to retrieve faculty section " + sectionId);
                    throw new Exception();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Validates and returns information about a student petition student. 
        /// If the student Id is not a valid student OR if the student already has a student petition, then an exception is thrown.
        /// </summary>
        /// <param name="studentId">The student Id to retrieve</param>
        /// <param name="sectionId">The section for which a student petition is being added.</param>
        /// <returns>A JSON StudentModel</returns>
        public async Task<JsonResult> ValidatePetitionStudentAsync(string studentId, string sectionId)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId");
                }
                if (string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException("sectionId");
                }

                var student = await ServiceClient.GetPlanningStudentAsync(studentId);
                if (student != null)
                {
                    // verify that this student does not already have a student petition for the specified section
                    var sectionPermissionDto = await ServiceClient.GetSectionPermissionsAsync(sectionId);
                    if (sectionPermissionDto.StudentPetitions.Where(sp => sp.StudentId == studentId).FirstOrDefault() == null)
                    {
                        StudentModel petitionStudent = new StudentModel(student);
                        return Json(petitionStudent, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        // If the student already has a student petition set a message and return a different status code (409).
                        Response.StatusCode = (int)HttpStatusCode.Conflict;
                        return Json(GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "StudentAlreadyHasStudentPetitionMessage"), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    throw new Exception("Student not found.");
                }
            }
            catch (Exception exception)
            {
                // Throw a 404
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Validates and returns information about a faculty consent student. 
        /// If the student Id is not a valid student OR if the student already has a faculty consent, then an exception is thrown.
        /// </summary>
        /// <param name="studentId">The student Id to retrieve</param>
        /// <param name="sectionId">The section for which a faculty consent is being added.</param>
        /// <returns>A JSON StudentModel</returns>
        public async Task<JsonResult> ValidateConsentStudentAsync(string studentId, string sectionId)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId");
                }
                if (string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException("sectionId");
                }

                var student = await ServiceClient.GetPlanningStudentAsync(studentId);
                if (student != null)
                {
                    // verify that this student does not already have a faculty consent for the specified section
                    var sectionPermissionDto = await ServiceClient.GetSectionPermissionsAsync(sectionId);
                    if (sectionPermissionDto.FacultyConsents.Where(fc => fc.StudentId == studentId).FirstOrDefault() == null)
                    {
                        StudentModel petitionStudent = new StudentModel(student);
                        return Json(petitionStudent, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        // If the student already has a faculty consent set a message and return a different status code (409).
                        Response.StatusCode = (int)HttpStatusCode.Conflict;
                        return Json(GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "StudentAlreadyHasFacultyConsentMessage"), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    throw new Exception("Student not found.");
                }
            }
            catch (Exception exception)
            {
                // Throw a 404
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Controller action to add a new section permission (student petition or faculty consent) for a student
        /// </summary>
        /// <param name="petitionType">The type of Faculty Permission (StudentPetition or FacultyConsent)(required)</param>
        /// <param name="sectionId">Id of the section (required)</param>
        /// <param name="studentId">Id of student (required)</param>
        /// <param name="statusCode">The Petition or Consent Status Code</param>
        /// <param name="reasonCode">Petition or Consent Reason Code</param>
        /// <param name="comment">Freeform additional comments related to the petition or consent</param>
        /// <returns>A JSON representation the new waiver</returns>
        public async Task<JsonResult> AddFacultySectionPermissionsAsync(string petitionType, string sectionId, string studentId, string statusCode, string reasonCode, string comment)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId) || string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException();
                }

                //add the new consent
                var studentPetitionDto = BuildStudentPetitionDto(petitionType, sectionId, studentId, statusCode, reasonCode, comment);
                var addedStudentPetition = await ServiceClient.AddStudentPetitionAsync(studentPetitionDto);

                var petitionStatusTask = ServiceClient.GetCachedPetitionStatusesAsync();
                var petitionReasonTask = ServiceClient.GetCachedStudentPetitionReasonsAsync();
                await Task.WhenAll(petitionStatusTask, petitionReasonTask);
                var petitionStatusDtos = petitionStatusTask.Result;
                var petitionReasonDtos = petitionReasonTask.Result;

                // Get the student for the section permission 
                var studentDtos = new List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>();
                if (!string.IsNullOrEmpty(addedStudentPetition.StudentId))
                {
                    try
                    {
                        var student = await ServiceClient.GetPlanningStudentAsync(addedStudentPetition.StudentId);
                        if (student != null)
                        {
                            studentDtos.Add(student);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }

                // Get the faculty for the section permission
                Faculty facultyDto = null;
                try
                {
                    facultyDto = await ServiceClient.GetFacultyAsync(addedStudentPetition.UpdatedBy);
                }
                catch (Exception)
                { }

                //Build a student petition or faculty consent models from section permission DTOs and related necessary data
                var studentPetitionModels = FacultyHelper.BuildStudentPetitions(new List<StudentPetition>() { addedStudentPetition }, petitionStatusDtos, petitionReasonDtos, studentDtos, new List<Faculty>() { facultyDto });
                var studentPetitionModel = studentPetitionModels.First();
                return Json(studentPetitionModel, JsonRequestBehavior.AllowGet);
            }

            catch (Exception exception)
            {
                var exceptionMessage = exception.Message;
                if (exception is AggregateException)
                {
                    AggregateException ae = exception as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(exceptionMessage, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Private method used to create a new student petition DTO
        /// </summary>
        /// <param name="petitionType">The type of Faculty Permission (StudentPetition or FacultyConsent)(required)</param>
        /// <param name="sectionId">Id of the section (required)</param>
        /// <param name="studentId">Id of student (required)</param>
        /// <param name="statusCode">The Petition or Consent Status Code</param>
        /// <param name="reasonCode">Petition or Consent Reason Code</param>
        /// <param name="comment">Freeform additional comments related to the petition or consent</param>
        /// <returns>A new Student Petition DTO</returns>
        private StudentPetition BuildStudentPetitionDto(string petitionType, string sectionId, string studentId, string statusCode, string reasonCode, string comment)
        {
            if (string.IsNullOrEmpty(petitionType))
            {
                throw new ArgumentNullException("petitionType", "StudentPetitionType is required to add a student petition or faculty consent");
            }
            if (string.IsNullOrEmpty(sectionId))
            {
                throw new ArgumentNullException("sectionId", "sectionId is required to add a student petition or faculty consent");
            }
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException("studentId", "studentId is required to add a student petition or faculty consent");
            }
            if (string.IsNullOrEmpty(statusCode))
            {
                throw new ArgumentOutOfRangeException("statusCode", "statusCode is required to add a student petition or faculty consent");
            }
            if (string.IsNullOrEmpty(reasonCode) && string.IsNullOrEmpty(comment))
            {
                throw new ArgumentOutOfRangeException("Must have either a reason or an comment to add a student petition or faculty consent");
            }

            return new StudentPetition()
            {
                StudentId = studentId,
                SectionId = sectionId,
                StatusCode = statusCode,
                ReasonCode = reasonCode,
                Comment = comment,
                UpdatedBy = GetEffectivePersonId(),
                DateTimeChanged = DateTime.UtcNow,
                Type = (StudentPetitionType)Enum.Parse(typeof(StudentPetitionType), petitionType),
            };
        }

        /// <summary>
        /// To retrieve roster of students for the given section
        /// </summary>
        /// <param name="sectionId">A Section Id</param>
        /// <returns>JSON Result of FacultyRosterStudentModel</returns>
        public async Task<JsonResult> GetFacultySectionRosterAsync(string sectionId)
        {
            var facultyRosterModel = new FacultyRosterModel();
            try
            {
                //retrieve preferred address for currently logged in faculty
                var facultyDto = await ServiceClient.GetFacultyAsync(CurrentUser.PersonId);
                facultyRosterModel.PreferredEmailAddress = facultyDto.EmailAddresses!=null?facultyDto.EmailAddresses.FirstOrDefault():string.Empty;
                // Bypass the cache to ensure latest data, which can be necessary for updates to succeed
                var rosterStudents = await ServiceClient.GetSectionRosterStudentsAsync(sectionId);
                var rosterStudentModels = new List<FacultyRosterStudentModel>();
                if (rosterStudents != null && rosterStudents.Count() > 0)
                {
                    IEnumerable<Ellucian.Colleague.Dtos.Student.ClassLevel> classLevels = new List<Ellucian.Colleague.Dtos.Student.ClassLevel>();
                    try
                    {
                        classLevels = await ServiceClient.GetClassLevelsAsync();
                    }
                    catch
                    {
                        Logger.Info("Error retrieving Class levels");
                    }
                    var students = await ServiceClient.QueryStudentsById4Async(rosterStudents.Select(r => r.Id));
                    foreach (var rosterStudent in rosterStudents)
                    {
                        var student = students.Where(s => s.Id == rosterStudent.Id).FirstOrDefault();
                        if (student != null)
                        {
                            rosterStudentModels.Add(new FacultyRosterStudentModel(student, classLevels));
                        }
                        
                    }
                    facultyRosterModel.RosterStudents.AddRange(rosterStudentModels.OrderBy(rsm => rsm.NameSort1).ThenBy(rsm => rsm.NameSort2).ThenBy(rsm => rsm.NameSort3));
                }
            }
            catch (Exception exception)
            {
                Logger.Info("No roster students returned for section " + sectionId + ": " + exception.ToString());
            }
            return Json(facultyRosterModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To retrieve student grades for the given section
        /// </summary>
        /// <param name="sectionId">Id of section</param>
        /// <returns>The JSON result of FacultyStudentGradesModel</returns>
        public async Task<JsonResult> GetFacultyStudentGradesAsync(string sectionId)
        {
             if (sectionId == null)
            {
                Logger.Error("SectionId provided is null, cannot retrieve student grades");
                throw new ArgumentNullException("sectionId");
            }
           
            var facultyStudentGradesModel = new FacultyStudentGradesModel();
            var studentGrades = new List<FacultyStudentGradeModel>();
            IEnumerable<Ellucian.Colleague.Dtos.Student.Grade> grades = new List<Ellucian.Colleague.Dtos.Student.Grade>();
            IEnumerable<Ellucian.Colleague.Dtos.Student.ClassLevel> classLevels = new List<Ellucian.Colleague.Dtos.Student.ClassLevel>();


            try
            {
                var gradingConfiguration = await ServiceClient.GetFacultyGradingConfigurationAsync();
                var queryCriteria = CreateGradeQueryCriteria(gradingConfiguration,sectionId);
                var academicCreditDtosTask = ServiceClient.QueryAcademicCreditsAsync(queryCriteria);
                var sectionDtoTask = ServiceClient.GetSection3Async(sectionId, false);
                var gradesTask = ServiceClient.GetCachedGradesAsync();
                var classLevelsTask =  ServiceClient.GetClassLevelsAsync();

                await Task.WhenAll(academicCreditDtosTask, sectionDtoTask, gradesTask, classLevelsTask);


                var academicCreditDtos = academicCreditDtosTask.Result;
                Ellucian.Colleague.Dtos.Student.Section3 sectionDto = sectionDtoTask.Result;
                grades = gradesTask.Result;
                classLevels = classLevelsTask.Result;

                studentGrades = (await CreateStudentGradesAsync(academicCreditDtos,queryCriteria, sectionId, grades, classLevels)).ToList<FacultyStudentGradeModel>();
                
                //build faculty studentGrades model
                #region facultyStudentGradesModel
                Course2 course = await ServiceClient.GetCourseAsync(sectionDto.CourseId);
                facultyStudentGradesModel.StudentGrades.AddRange(studentGrades.OrderBy(s => s.LastName).ThenBy(s => s.FirstName).ThenBy(s => s.MiddleName));
                // Be sure grade are sorted 
                facultyStudentGradesModel.GradeTypes = grades != null ? grades.Where(g => g.GradeSchemeCode == sectionDto.GradeSchemeCode && !g.ExcludeFromFacultyGrading).OrderBy(gr => gr.LetterGrade).Select(g => Tuple.Create<string, string, bool>(g.LetterGrade, g.IncompleteGrade, g.RequireLastAttendanceDate)).ToList() : new List<Tuple<string, string, bool>>();
                facultyStudentGradesModel.SectionId = sectionId;
                if (((gradingConfiguration.AllowedGradingTerms != null && sectionDto.TermId != null && gradingConfiguration.AllowedGradingTerms.Contains(sectionDto.TermId)) || string.IsNullOrEmpty(sectionDto.TermId)))
                {
                    facultyStudentGradesModel.IsTermOpen = true;
                    facultyStudentGradesModel.MobileGradeViews.Add(Tuple.Create<string, string>("F", GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "FinalGradeTab")));
                }

                facultyStudentGradesModel.SectionStartDate = sectionDto.StartDate;
                facultyStudentGradesModel.SectionEndDate = sectionDto.EndDate;

                if (gradingConfiguration.VerifyGrades.HasValue && gradingConfiguration.VerifyGrades.Value)
                {
                    facultyStudentGradesModel.CanVerifyGrades = true;
                }
                else if (gradingConfiguration.VerifyGrades.HasValue && !gradingConfiguration.VerifyGrades.Value)
                {
                    facultyStudentGradesModel.CanVerifyGrades = false;
                }
                //override with ACOI settings
                if (course.VerifyGrades.HasValue && course.VerifyGrades.Value)
                {
                    facultyStudentGradesModel.CanVerifyGrades = true;
                }
                else if (course.VerifyGrades.HasValue && !course.VerifyGrades.Value)
                {
                    facultyStudentGradesModel.CanVerifyGrades = false;
                }
                facultyStudentGradesModel.NumberOfMidtermGradesToShow = gradingConfiguration.NumberOfMidtermGrades;
                for (int i = 0; i < facultyStudentGradesModel.NumberOfMidtermGradesToShow; i++)
                {
                    facultyStudentGradesModel.MobileGradeViews.Add(Tuple.Create<string, string>("M" + (i+1), GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "MidtermGradeLabel") + " " + (i+1)));
                }

                #endregion
            }
            catch (Exception exception)
            {
                Logger.Info("Unable to retrieve student grades for section " + sectionId + ": " + exception.ToString());
            }

           
            JsonResult result = Json(facultyStudentGradesModel, JsonRequestBehavior.AllowGet);
            return result;
        }


        [HttpPut]
        public async Task<JsonResult> PutFacultyStudentGradesAsync(string studentGrade)
        {

            if (studentGrade == null)
            {
                Logger.Error("Student Grade is null or empty, cannot complete student grade update");
                throw new ArgumentNullException("studentGrade");
            }
            try
            {
            var studentGradeModel = JsonConvert.DeserializeObject<FacultyStudentGradeModel>(studentGrade, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                SectionGrades3 sectionGrades = new SectionGrades3();
                sectionGrades.SectionId = studentGradeModel.SectionId;
                sectionGrades.ForceNoVerifyFlag = true;
                sectionGrades.StudentGrades = new List<StudentGrade2>()
            {
                new StudentGrade2()
                { 
                    StudentId=studentGradeModel.StudentId,
                    FinalGrade=(studentGradeModel.IsGradeVerified.HasValue && studentGradeModel.IsGradeVerified.Value) ?null:studentGradeModel.FinalGrade,
                    FinalGradeExpirationDate=(!string.IsNullOrEmpty(studentGradeModel.GradeExpireDate)?Convert.ToDateTime(studentGradeModel.GradeExpireDate):default(DateTime?)),
                    LastAttendanceDate=(!string.IsNullOrEmpty(studentGradeModel.LastDateAttended)?Convert.ToDateTime(studentGradeModel.LastDateAttended):default(DateTime?)),
                    NeverAttended=studentGradeModel.NeverAttended,
                    MidtermGrade1=studentGradeModel.Midterm1Grade,
                    MidtermGrade2=studentGradeModel.Midterm2Grade,
                    MidtermGrade3=studentGradeModel.Midterm3Grade,
                    MidtermGrade4=studentGradeModel.Midterm4Grade,
                    MidtermGrade5=studentGradeModel.Midterm5Grade,
                    MidtermGrade6=studentGradeModel.Midterm6Grade,
                    ClearLastAttendanceDateFlag=string.IsNullOrEmpty(studentGradeModel.LastDateAttended)?true:false,
                    ClearFinalGradeExpirationDateFlag=string.IsNullOrEmpty(studentGradeModel.GradeExpireDate)?true:false

                }
            };
                IEnumerable<SectionGradeResponse> result = await ServiceClient.PutCollectionOfStudentGrades4Async(studentGradeModel.SectionId, sectionGrades);
            List<string> responseErrors = null;
                if (result != null && result.Any())
            {
                var errors = result.ToList()[0].Errors;
                    responseErrors = errors != null ? errors.Select(e => e.Message).ToList() : null;
            }
              return Json(new { gradeEntryErrors = responseErrors.Distinct() });
        }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        [HttpPut]
        public async Task<JsonResult> VerifyFacultyStudentGradesAsync(string sectionId)
        {

            if (sectionId == null)
            {
                Logger.Error("SectionId provided is null, cannot verify student grades");
                throw new ArgumentNullException("sectionId");
            }
            List<FacultyStudentGradeModel> studentGrades = new List<FacultyStudentGradeModel>();
            IEnumerable<SectionGradeResponse> verifyResult = null;
            IEnumerable<Ellucian.Colleague.Dtos.Student.ClassLevel> classLevels = new List<Ellucian.Colleague.Dtos.Student.ClassLevel>();

            try
            {
               
                var gradingConfiguration = await ServiceClient.GetFacultyGradingConfigurationAsync();
               
                
                var queryCriteria = CreateGradeQueryCriteria(gradingConfiguration, sectionId);

                var academicCreditDtosTask = ServiceClient.QueryAcademicCreditsAsync(queryCriteria);
                var gradesTask = ServiceClient.GetCachedGradesAsync();
                var classLevelsTask = ServiceClient.GetClassLevelsAsync();

                 await Task.WhenAll(academicCreditDtosTask, gradesTask, classLevelsTask);


                IEnumerable<AcademicCredit2> academicCreditDtos = academicCreditDtosTask.Result;
                var grades = gradesTask.Result;
                classLevels = classLevelsTask.Result;

              
                if (academicCreditDtos != null && academicCreditDtos.Count() > 0)
                {
                    //build collection
                    SectionGrades3 sectionGrades = new SectionGrades3();
                    sectionGrades.SectionId = sectionId;
                    sectionGrades.ForceNoVerifyFlag = false;
                    sectionGrades.StudentGrades = new List<StudentGrade2>();
                    sectionGrades.StudentGrades.AddRange(academicCreditDtos.Where(a => !a.HasVerifiedGrade && !string.IsNullOrEmpty(a.FinalGradeId)).Select(c =>

                    new StudentGrade2()
                    {
                        StudentId = c.StudentId,
                        FinalGrade = grades.FirstOrDefault(g => g.Id == c.FinalGradeId).LetterGrade,
                        FinalGradeExpirationDate = (c.FinalGradeExpirationDate.HasValue ? c.FinalGradeExpirationDate.Value : default(DateTime?)),
                        LastAttendanceDate = (c.LastAttendanceDate.HasValue ? c.LastAttendanceDate.Value : default(DateTime?)),
                        NeverAttended = c.NeverAttended
                    })
                    );
                    if (sectionGrades != null && sectionGrades.StudentGrades != null && sectionGrades.StudentGrades.Any())
                    {
                        //send to API
                        verifyResult = await ServiceClient.PutCollectionOfStudentGrades4Async(sectionId, sectionGrades);
                    }

                   
                        //create a faculty view model with errors if any found while verifying
                        IEnumerable<AcademicCredit2> finalAcademicCredits = await ServiceClient.QueryAcademicCreditsAsync(queryCriteria);

                        //create  student grade model and attach greade verification errors to model
                        studentGrades = (await CreateStudentGradesAsync(finalAcademicCredits, queryCriteria, sectionId, grades, classLevels)).OrderBy(x => x.LastName.ToUpper()).ThenBy(s=>s.FirstName.ToUpper()).ThenBy(a=>a.MiddleName.ToUpper()).ToList<FacultyStudentGradeModel>();

                        if (verifyResult != null && verifyResult.Any())
                        {
                            foreach (var student in studentGrades)
                            {
                                SectionGradeResponse verifiedStudentResult = verifyResult.FirstOrDefault(v => v.StudentId == student.StudentId);
                                if (verifiedStudentResult != null)
                                {
                                    var errors = verifiedStudentResult != null ? verifiedStudentResult.Errors : null;
                                student.VerifyGradeResponseErrors = errors != null ? errors.Select(e => e.Message).Distinct().ToList() : null;
                                }
                            }

                        }

                }
                JsonResult jsonContents = Json(new { StudentGrades = studentGrades }, JsonRequestBehavior.AllowGet);
                return jsonContents;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                throw ex;
            }
        }




        //Private Methods
        #region privateMethods

        private AcademicCreditQueryCriteria CreateGradeQueryCriteria(FacultyGradingConfiguration gradingConfiguration, string sectionId)
        {
              var queryCriteria = new AcademicCreditQueryCriteria();
                queryCriteria.SectionIds = new List<string>() { sectionId };
                queryCriteria.IncludeCrossListedCredits = gradingConfiguration.IncludeCrosslistedStudents;
                queryCriteria.CreditStatuses = new List<CreditStatus>() {
                    CreditStatus.New,
                    CreditStatus.Add
                };
                if (gradingConfiguration.IncludeDroppedWithdrawnStudents)
                {
                    queryCriteria.CreditStatuses.Add(CreditStatus.Dropped);
                    queryCriteria.CreditStatuses.Add(CreditStatus.Withdrawn);
                }
                return queryCriteria;
        }

        private async Task<IEnumerable<FacultyStudentGradeModel>> CreateStudentGradesAsync(IEnumerable<AcademicCredit2> academicCredits, AcademicCreditQueryCriteria queryCriteria, string sectionId, IEnumerable<Grade> grades, IEnumerable<ClassLevel> classLevels)
        {
            var studentGrades = new List<FacultyStudentGradeModel>();
            var students = await ServiceClient.QueryStudentsById4Async(academicCredits.Select(r => r.StudentId));
            IEnumerable<Section3> crossListedSections = new List<Section3>();

            if (queryCriteria.IncludeCrossListedCredits)
            {
                // Get all the section Ids returned in the list of academicCreditDtos and get the section DTOs for each since
                // we will need the names of those sections.
                var sectionIds = academicCredits.Select(a => a.SectionId).Distinct().ToList();
                crossListedSections = await ServiceClient.GetSections4Async(sectionIds, true, false);
            }

            foreach (var studentGrade in academicCredits)
            {
                var student = students.Where(s => s.Id == studentGrade.StudentId).FirstOrDefault();
                Section3 crossListedSection = null;
                if (queryCriteria.IncludeCrossListedCredits && crossListedSections != null && crossListedSections.Any())
                {
                    if (studentGrade.SectionId != sectionId)
                    {
                        crossListedSection = crossListedSections.Where(s => s.Id == studentGrade.SectionId).FirstOrDefault();
                    }
                }
                studentGrades.Add(new FacultyStudentGradeModel(sectionId, studentGrade, student, grades, crossListedSection, studentGrade.HasVerifiedGrade,student.ClassLevelCodes, classLevels));

            }
            return studentGrades;
        }
        #endregion
    }
}
