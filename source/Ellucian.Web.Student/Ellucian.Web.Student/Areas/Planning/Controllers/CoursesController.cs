﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Planning.Models;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Courses;
using Newtonsoft.Json.Serialization;
using System.Dynamic;


namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [GuestAuthorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class CoursesController : BaseStudentController
    {
        public IEnumerable<FilterModel> filters;
        private CourseSearchHelper _helper;

        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        public CoursesController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _adapterRegistry = adapterRegistry;

            _helper = new CourseSearchHelper(ServiceClient, logger, adapterRegistry, true);
        }

        /// <summary>
        /// Index page for starting a course search.
        /// </summary>
        /// <returns>A View listing all course subjects for navigation</returns>
        [LinkHelp]
        [GuestRedirect("Courses", "Index")]
        [PageAuthorize("courseCatalog")]
        public async Task<ActionResult> Index()
        {
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "AreaDescription");
            var catalogConfiguration = await ServiceClient.GetCachedCourseCatalogConfigurationAsync();
            dynamic dynamicModel = _helper.RetrieveDynamicViewModel(catalogConfiguration, null);
            return View(dynamicModel);
        }

        /// <summary>
        /// Get a list of all subjects available in the course catalog
        /// used by advisor catalog search
        /// </summary>
        /// <returns>List of all subjects, sorted by description.</returns>
        [ActionName("GetSubjects")]
        public async Task<JsonResult> GetSubjectsAsync()
        {
            //need to ensure that these are both sorted (by description) and filtered based on whether the subject is marked to show
            var subjects = (await ServiceClient.GetCachedSubjectsAsync()).Where(s => s.ShowInCourseSearch).OrderBy(s => s.Description).ToList();
            return Json(subjects, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Get lookup details for course catalog advanced search
        /// </summary>
        /// <returns>advanced search item model that contains all dropdown items.</returns>
        public async Task<JsonResult> GetCatalogAdvancedSearchAsync()
        {
           CatalogAdvancedSearchItemsModel searchModel= await _helper.RetrieveAdvancedSearchItemsAsync();
           return Json(searchModel, JsonRequestBehavior.AllowGet);
          
        }

        /// <summary>
        /// Receive advanced search model and other query parameters if passed
        /// course catalog when searched from subjects list or by entering a keyword or through my progress or through plan & schedule- 
        /// values are passed as query string For eg: courseid, requirements
        /// These values get mapped to searchParams by MVC default mode binder
        /// whereas advanced search form is posted as json string as 'model' that gets mapped to model parameter.
        /// </summary>
        /// <returns>search result view.</returns>
        [LinkHelp]
        [GuestRedirect("Courses", "Search")]
        [PageAuthorize("courseCatalogSearch")]
        public async Task<ActionResult> Search(string model, CatalogSearchCriteriaModel searchParams) 
        {
            CatalogSearchCriteriaModel searchCriteriaModel = new CatalogSearchCriteriaModel();
            if (model != null)
            {
                 searchCriteriaModel = _helper.RetrieveCatalogSearchCriteriaFromAdvance(model);
            }
            else
            {
                searchCriteriaModel = searchParams;
            }
            var catalogConfiguration = await ServiceClient.GetCachedCourseCatalogConfigurationAsync();
            dynamic dynamicModel = _helper.RetrieveDynamicViewModel(catalogConfiguration, searchCriteriaModel);
            
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "AreaDescription");
            ViewBag.SubMenuOverride = "Index";
            this.ViewBag.IsWithDegreePlan = true;
            return View("SearchResult", dynamicModel);
        }

        /// <summary>
        /// Searches for courses used by advisor catalog search. 
        /// </summary>
        /// <param name="searchParameters"></param>
        /// <returns>JSON response containing search results</returns>
        [ValidateInput(false)]
        [HttpGet]
        public async Task<JsonResult> SearchAsync(CatalogSearchCriteriaModel searchCriterialModel)
        {
            var searchParameters =JsonConvert.SerializeObject(searchCriterialModel, new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
            });

            var viewModel = await _helper.CreateCourseSearchResultAsync(searchCriterialModel);
            return Json(new { resultModel = viewModel, searchModel = searchParameters }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// used by course catalog search where search criteria model is posted back
        /// </summary>
        /// <param name="searchParameters"> this is json string of search criteria javascript model</param>
        /// <returns>JSON response containing search results</returns>
        [ValidateInput(false)]
        [HttpPost]
        public async Task<JsonResult> SearchAsync(string searchParameters)
        {
            CatalogSearchCriteriaModel searchCriteriaModel = _helper.RetrieveCatalogSearchCriteria(searchParameters);
            var viewModel = await _helper.CreateCourseSearchResultAsync(searchCriteriaModel);
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves Course details for the given course ID and returns it as a JSON model.
        /// </summary>
        /// <param name="courseId">The ID of the course for which to retrieve details</param>
        /// <returns>A JSON model of the full course details</returns>
        [ActionName("CourseDetails")]
        public async Task<JsonResult> CourseDetailsAsync(string courseId)
        {
            CourseModel model = new CourseModel();

            if (!string.IsNullOrEmpty(courseId))
            {
                try
                {
                    Course2 course = await ServiceClient.GetCourseAsync(courseId);
                    if (course != null)
                    {
                        var courseMapAdapter = _adapterRegistry.GetAdapter<Course2, CourseModel>();
                        model = courseMapAdapter.MapToType(course);

                        // Build requisites for this course based simply on the courses requisites. In this case there won't be any section requisites.
                        model.RequisiteItems = await BuildRequisiteListAsync(course.Requisites, new List<SectionRequisite>());

                        List<string> locationItems = new List<string>();
                        Dictionary<string, Location> allLocations = await _helper.GetAllLocationsAsync();
                        foreach (string location in course.LocationCodes)
                        {
                            if (allLocations.ContainsKey(location))
                            {
                                locationItems.Add(allLocations[location].Description);
                            }
                        }
                        model.LocationItems = locationItems;
                        model.LocationCycleRestrictionDescriptions = new List<LocationCycleRestrictionModel>();
                        if (course.LocationCycleRestrictions != null && course.LocationCycleRestrictions.Any())
                        {
                            Dictionary<string, SessionCycle> allSessionCycles = await _helper.GetAllSessionCyclesAsync();
                            Dictionary<string, YearlyCycle> allYearlyCycles = await _helper.GetAllYearlyCyclesAsync();
                            model.LocationCycleRestrictionDescriptions = _helper.BuildCourseLocationCycleRestrictionModels(course, allLocations, allSessionCycles, allYearlyCycles);

                        }
                    }
                }
                catch (Exception e)
                {
                    //just log errors and return an empty model for no data
                    Logger.Error(e.ToString());
                    model = new CourseModel();
                }
            }

            return Json(model);
        }

        /// <summary>
        /// Retrieves Section details for the given section ID and returns it as a JSON model.
        /// </summary>
        /// <param name="sectionId">The ID of the specific section of a course for which to retrieve details</param>
        /// <param name="studentId">The ID of the student reviewing the section details or adding the section. Need to determine which grading options to show.</param>
        /// <returns>A JSON model of the full section details</returns>
        [ActionName("SectionDetails")]
        public async Task<JsonResult> SectionDetailsAsync(string sectionId, string studentId)
        {
            SectionDetailsModel model = null;

            model = await _helper.CreateSectionDetailsAsync(sectionId, studentId, CurrentUser.PersonId);
            return Json(model);
        }

        /// <summary>
        /// Asynchronously retrieves section data for the given course.
        /// </summary>
        /// <param name="courseId">The ID of the course for which to retrieve the full section data list</param>
        /// <param name="sectionIds">A collection of matching section IDs to retrieve</param>
        /// <returns>A collection of sections and associated data for this course, sorted by number (BIO100-01, BIO100-02, BIO100-03, ...)</returns>
        [ActionName("SectionsAsync")]
        public async Task<JsonResult> SectionsAsync(string courseId, IEnumerable<string> sectionIds)
        {
            SectionRetrievalModel sections = await _helper.CreateSectionRetrievalModelAsync(courseId, sectionIds);


            return new JsonResult() { Data = sections, MaxJsonLength = Int32.MaxValue };
        }


        /// <summary>
        /// Controller action to asynchronously get a faculty's list of active course sections. The faculty is the person logged in.
        /// </summary>
        /// <returns>A list of faculty section models specific to the logged in person.</returns>
        public async Task<JsonResult> FacultySectionsAsync()
        {
            try
            {
                // Retrieve the faculty's assigned sections.  By not including a date range it will use the Allowed Terms on Registration Web Parameters (RGWP),
                // Grading Web Parameters (GRWP), and Schedule Web Parameters (CSWP).
                IEnumerable<Section3> sections = await ServiceClient.GetFacultySections4Async(GetEffectivePersonId(), null, null);
                var facultySections = await BuildFacultyTermModelsAsync(sections.ToList(), await _helper.GetAllLocationsAsync(),
                            await _helper.GetAllBuildingsAsync(), await _helper.GetAllRoomsAsync(), await _helper.GetAllInstructionalMethodsAsync(), await GetAllTermsAsync());
                return Json(facultySections, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retrieves the waiver information for a faculty section.
        /// </summary>
        /// <param name="sectionId">The faculty section Id to retrieve</param>
        /// <returns>A FacultySectionDetailModel</returns>
        public async Task<JsonResult> GetFacultySectionWaiversAsync(string sectionId)
        {
            try
            {
                if (string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException("sectionId", "sectionId is required to get section information");
                }
                // Requesting cached data since there isn't any live data needed currently.
                Ellucian.Colleague.Dtos.Student.Section3 sectionDto = await ServiceClient.GetSection3Async(sectionId, false);
                if (sectionDto != null)
                {
                    //create the  viewmodel which that contains the student waiver information.
                    var allReasons = await ServiceClient.GetStudentWaiverReasonsAsync();
                    FacultySectionDetailModel facultySectionViewModel = new FacultySectionDetailModel(sectionId, allReasons);

                    // Add in the necessary Requisites - this can really only be done if the section's course can be found.
                    if (!string.IsNullOrEmpty(sectionDto.CourseId))
                    {
                        Course2 course = await ServiceClient.GetCourseAsync(sectionDto.CourseId);
                        if (course != null)
                        {
                            // Other requisites might come from the course or the section, depending on override
                            var requisites = new List<Requisite>();
                            if (sectionDto.OverridesCourseRequisites)
                            {
                                requisites = sectionDto.Requisites.ToList();
                                // Even if the section overrides the course requisites, any protected course requisites will also still apply
                                var protectedCourseRequisites = course.Requisites.Where(r => !string.IsNullOrEmpty(r.RequirementCode) && r.IsProtected).ToList();
                                if (protectedCourseRequisites.Count() > 0)
                                {
                                    requisites.AddRange(protectedCourseRequisites);
                                }
                            }
                            else
                            {
                                // If section does not override course requisites, get all the requirement-coded requisites from the course.
                                requisites = course.Requisites.Where(r => !string.IsNullOrEmpty(r.RequirementCode)).ToList();
                                requisites.AddRange(sectionDto.Requisites.Where(r => !string.IsNullOrEmpty(r.CorequisiteCourseId)));
                            }
                            IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Requirement> requisiteRequirements = new List<Ellucian.Colleague.Dtos.Student.Requirements.Requirement>();
                            var requirementCodes = requisites.Select(r => r.RequirementCode).Where(r => !string.IsNullOrEmpty(r)).Distinct().ToList();
                            if (requirementCodes != null && requirementCodes.Count() > 0)
                            {
                                requisiteRequirements = await ServiceClient.QueryRequirementsAsync(requirementCodes);
                            }
                            var allRequisites = await BuildFacultyRequisitesAsync(requisites, sectionDto.SectionRequisites, requisiteRequirements);
                            facultySectionViewModel.CourseCorequisites = allRequisites.Where(r => r.CompletionOrder == RequisiteCompletionOrder.Concurrent).ToList();
                            facultySectionViewModel.CoursePrerequisites = allRequisites.Where(r => r.CompletionOrder != RequisiteCompletionOrder.Concurrent).ToList();
                        }
                    }
                    // Get any waivers for this section 
                    IEnumerable<StudentWaiver> waivers = new List<StudentWaiver>();
                    try
                    {
                        waivers = await ServiceClient.GetSectionStudentWaiversAsync(sectionId);
                    }
                    catch (Exception)
                    {
                        // If we cannot get the section waivers (i.e. it is a section this faculty does not teach)
                        // do nothing. Rest of section info can be retrieved.
                    }

                    // Get the student for any of the waivers
                    IEnumerable<Ellucian.Colleague.Dtos.Planning.PlanningStudent> students = new List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>();
                    try
                    {
                        var studentIds = waivers.Select(w => w.StudentId).Where(s => !string.IsNullOrEmpty(s)).Distinct();
                        students = (await ServiceClient.QueryPlanningStudentsAsync(studentIds)).ToList();
                    }
                    catch (Exception)
                    {
                        // Do nothing. If we can't get the student due to permissions or something then send thru an empty list
                        // and student data will be unknown.
                    }

                    // Get the faculty for any of the waivers
                    IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>();
                    try
                    {
                        // We will only show faculty names for waivers submitted by any faculty teaching the specific section
                        // All others will be either null or a generic text from the resource file.

                        var waiverFacultyIds = waivers.Select(w => w.AuthorizedBy).Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        if (waiverFacultyIds.Count() > 0 && sectionDto.FacultyIds != null && sectionDto.FacultyIds.Count() > 0)
                        {
                            var facultyIds = sectionDto.FacultyIds.Intersect(waiverFacultyIds).ToList();
                            if (facultyIds.Count() > 0)
                            {
                                faculty = await ServiceClient.GetFacultyByIdsAsync(facultyIds);
                            }
                        }

                    }
                    catch (Exception)
                    {
                        // Really no reason this should fail but if it does send through an empty list and the faculty information will just be missing.
                    }

                    // Add in the student waiver models.
                    facultySectionViewModel.StudentWaivers = FacultyHelper.BuildStudentWaivers(waivers, allReasons, students, faculty);

                    // If this section has any required prerequisites determine if this faculty has the permissions to add a new waiver for this section.
                    // Otherwise leave CanFacultyAddWaivers as false - from constructor.
                    if (facultySectionViewModel.CoursePrerequisites != null && facultySectionViewModel.CoursePrerequisites.Count(r => r.IsRequired) > 0)
                    {
                        try
                        {
                            var permissions = await ServiceClient.GetFacultyPermissionsAsync();
                            facultySectionViewModel.CanFacultyAddWaivers = permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "PermissionsCreatePrerequisiteWaiver"), StringComparer.InvariantCultureIgnoreCase);

                        }
                        catch (Exception exception)
                        {
                            // If we can't get the person's faculty permissions the CanFacultyAddWaivers defaults to false to just keep going. 
                            Logger.Error(exception.ToString());
                        }
                    }
                    return Json(facultySectionViewModel, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    Logger.Error("Unable to retrieve faculty section " + sectionId);
                    throw new Exception();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This is to retrieve only the faculty section header information through ajax call.
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<JsonResult> GetFacultySectionHeaderAsync(string sectionId)
        {
            try
            {
                if (string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException("sectionId", "sectionId is required to get section information");
                }
                // Requesting cached data since there isn't any live data needed currently.
                Ellucian.Colleague.Dtos.Student.Section3 sectionDto = await ServiceClient.GetSection3Async(sectionId, false);
                if (sectionDto != null)
                {

                    FacultySectionModel sectionHeaderViewModel = new FacultySectionModel(sectionDto, await _helper.GetAllLocationsAsync(),
                      await _helper.GetAllBuildingsAsync(), await _helper.GetAllRoomsAsync(), null, await GetAllTermsAsync());
                    return Json(sectionHeaderViewModel, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Logger.Error("Unable to retrieve faculty section " + sectionId);
                    throw new Exception();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<IEnumerable<FacultyTermModel>> BuildFacultyTermModelsAsync(List<Section3> sections, Dictionary<string, Location> allLocations, Dictionary<string, Building> buildings, Dictionary<string, Room> rooms, Dictionary<string, InstructionalMethod> instructionalMethods, Dictionary<string, Term> allTerms)
        {
            var facultyTerms = new List<FacultyTermModel>();

            // If there aren't any active sections, no need to continue, just return the empty list of FacultyTermModels
            var activeSections = sections == null ? null : sections.Where(cs => cs.IsActive == true).Distinct().ToList();
            if (activeSections == null || activeSections.Count() <= 0)
            {
                return facultyTerms;
            }
            // Loop through the distinct terms in the set of active sections, creating FacultyTermModels as we go
            foreach (var termCode in activeSections.Select(x => x.TermId).Distinct())
            {
                // Get the term DTO for this term
                Term term = null;
                if (!string.IsNullOrEmpty(termCode))
                {
                    term = allTerms[termCode];
                }
                if (term == null)
                {
                    // If allTerms doesn't already have a code for NonTermPlaceholder, add one to the list. In the case of no term use that.
                    if (!(allTerms.ContainsKey("NonTermPlaceholder")))
                    {
                        allTerms["NonTermPlaceholder"] = new Term() { Code = "NonTermPlaceholder", Description = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "NonTermDescription"), StartDate = DateTime.MinValue };
                    }
                    term = allTerms["NonTermPlaceholder"];
                }
                var facultyTerm = new FacultyTermModel(term.Code, term.Description);

                var sectionModels = new List<FacultySectionModel>();

                foreach (var section in activeSections.Where(x => x.TermId == termCode))
                {
                    sectionModels.Add(new FacultySectionModel(section, allLocations, buildings, rooms, instructionalMethods, await GetAllTermsAsync()));
                }

                // Sort by section start date and then by formatted name.  Then add them to the term's list
                var sortedSectionModels = sectionModels.OrderBy(t => t.StartDate).ThenBy(t => t.EndDate).ThenBy(t => t.FormattedNameDisplay);
                facultyTerm.AddSections(sortedSectionModels);

                facultyTerms.Add(facultyTerm);
            }

            // Now sort the facultyTerms based on the allTerms dates by descending so that it is recent to oldest
            var sortedFacultyTerms = from term in allTerms
                                     join facultyTerm in facultyTerms
                                        on term.Value.Code equals facultyTerm.Code
                                     orderby term.Value.StartDate descending
                                     select facultyTerm;

            return sortedFacultyTerms;
        }

        /// <summary>
        /// Builds a Dictionary of all Faculty members that are associated with any of the provided sections.
        /// </summary>
        /// <param name="sections">A list of sections for which to examine and build the Dictionary of Faculty members</param>
        /// <returns>A Dictionary mapping the associated Faculty items by their IDs</returns>
        private async Task<Dictionary<string, Faculty>> GetFacultyDictionaryFromSectionsAsync(List<Section3> sections)
        {
            Dictionary<string, Faculty> faculty = new Dictionary<string, Faculty>();
            List<string> ids = new List<string>();
            if (sections != null && sections.Count > 0)
            {
                foreach (Section3 section in sections)
                {
                    if (section.FacultyIds != null)
                    {
                        foreach (string id in section.FacultyIds)
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(id) && !faculty.ContainsKey(id))
                                {
                                    faculty.Add(id, await ServiceClient.GetFacultyAsync(id));
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, "Unable to get faculty: " + id);
                            }
                        }
                    }
                }
            }

            return faculty;
        }


        /// <summary>
        /// Builds list of Requisites
        /// </summary>
        /// <param name="requisites">List of requisite DTOs for a given course/section</param>
        /// <returns>List of RequisiteModel items</returns>
        private async Task<IEnumerable<RequisiteModel>> BuildRequisiteListAsync(IEnumerable<Requisite> requisites, IEnumerable<SectionRequisite> sectionRequisites, IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Requirement> requisiteRequirements = null)
        {
            var reqModels = new List<RequisiteModel>();

            foreach (var requisite in requisites)
            {
                var reqModel = new RequisiteModel();
                reqModel.IsRequired = requisite.IsRequired;
                // First determine which display text extension to use.
                if (requisite.IsRequired == true && requisite.CompletionOrder == RequisiteCompletionOrder.Previous)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousRequisiteRequired");
                }
                if (requisite.IsRequired == false && requisite.CompletionOrder == RequisiteCompletionOrder.Previous)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousRequisiteRecommended");
                }
                if (requisite.IsRequired == true && requisite.CompletionOrder == RequisiteCompletionOrder.Concurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRequired");
                }
                if (requisite.IsRequired == false && requisite.CompletionOrder == RequisiteCompletionOrder.Concurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRecommended");
                }
                if (requisite.IsRequired == true && requisite.CompletionOrder == RequisiteCompletionOrder.PreviousOrConcurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousOrConcurrentRequired");
                }
                if (requisite.IsRequired == false && requisite.CompletionOrder == RequisiteCompletionOrder.PreviousOrConcurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "PreviousOrConcurrentRecommended");
                }
                // Get the requirement text or course name to display, depending on what's in the incoming requisite
                if (!string.IsNullOrEmpty(requisite.RequirementCode))
                {
                    Ellucian.Colleague.Dtos.Student.Requirements.Requirement req = null;
                    if (requisiteRequirements != null)
                    {
                        req = requisiteRequirements.Where(r => r.Code == requisite.RequirementCode).FirstOrDefault();
                    }
                    else
                    {
                        req = await ServiceClient.GetRequirementAsync(requisite.RequirementCode);
                    }
                    if (req != null)
                    {
                        reqModel.DisplayText = string.IsNullOrEmpty(req.DisplayText) ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "DefaultRequisiteText") : req.DisplayText;
                    }
                }
                else if (!string.IsNullOrEmpty(requisite.CorequisiteCourseId))
                {
                    // This will occur only if client has not yet converted to the new format that always uses requirements
                    var reqCourse = await ServiceClient.GetCourseAsync(requisite.CorequisiteCourseId);
                    if (reqCourse != null)
                    {
                        reqModel.DisplayText = reqCourse.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + reqCourse.Number;
                    }
                }
                else
                {
                    // If requisite does not have a requirement code or a course id it's invalid. Log it.
                    Logger.Info("Requisite is not compliant with any expected format--ignoring");
                }

                reqModels.Add(reqModel);
            }
            foreach (var requisite in sectionRequisites)
            {
                var reqModel = new RequisiteModel();
                reqModel.IsRequired = requisite.IsRequired;
                if (requisite.CorequisiteSectionIds != null && requisite.CorequisiteSectionIds.Count() > 0)
                {
                    // Single required or recommended corequisite section
                    if (requisite.CorequisiteSectionIds.Count() == 1)
                    {
                        reqModel.DisplayText = await BuildSectionDetailTextAsync(requisite.CorequisiteSectionIds.ElementAt(0));
                        reqModel.DisplayTextExtension = requisite.IsRequired ? GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRequired") : GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRecommended");
                    }
                    else
                    {
                        // Multiple corequisite section: ie Take X of the following sections 
                        reqModel.DisplayText = string.Format(GlobalResources.GetString(PlanningResourceFiles.CourseResources, "MultipleNumberOfSectionsRequisiteText"), requisite.NumberNeeded.ToString());
                        for (int i = 0; i < requisite.CorequisiteSectionIds.Count(); i++)
                        {
                            var connector = (i == 0 ? " " : ", ");
                            reqModel.DisplayText += connector + await BuildSectionDetailTextAsync(requisite.CorequisiteSectionIds.ElementAt(i));
                        }
                        reqModel.DisplayTextExtension = GlobalResources.GetString(PlanningResourceFiles.CourseResources, "ConcurrentRequisiteRequired");
                    }
                }
                reqModels.Add(reqModel);
            }
            return reqModels;
        }

        private async Task<string> BuildSectionDetailTextAsync(string sectionId)
        {
            var reqSection = await ServiceClient.GetSection3Async(sectionId);
            var reqCourse = await ServiceClient.GetCourseAsync(reqSection.CourseId);
            return (reqCourse.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + reqCourse.Number + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + reqSection.Number);
        }

        /// <summary>
        /// Gets all terms .
        /// </summary>
        /// <returns>A Dictionary of all terms mapped by their IDs</returns>
        private async Task<Dictionary<string, Term>> GetAllTermsAsync()
        {
            Dictionary<string, Term> terms = new Dictionary<string, Term>();
            List<Term> allTerms = (await ServiceClient.GetCachedTermsAsync()).ToList();
            if (allTerms != null && allTerms.Count > 0)
            {
                foreach (Term term in allTerms)
                {
                    if (!terms.ContainsKey(term.Code))
                    {
                        terms.Add(term.Code, term);
                    }
                }
            }

            return terms;
        }

        /// <summary>
        /// Builds list of Faculty Section Requisites
        /// </summary>
        /// <param name="requisites">List of requisite DTOs for a given course/section</param>
        /// <param name="sectionRequisites">List of section requisite DTOs for a given course/section</param>
        /// <param name="requisiteRequirements">Requirement DTOs related to the requisites provided.</param>
        /// <returns>List of FacultySectionRequisiteModel items</returns>
        private async Task<IEnumerable<FacultySectionRequisiteModel>> BuildFacultyRequisitesAsync(IEnumerable<Requisite> requisites, IEnumerable<SectionRequisite> sectionRequisites, IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Requirement> requisiteRequirements = null)
        {
            var reqModels = new List<FacultySectionRequisiteModel>();
            if (requisites != null && requisites.Count() > 0)
            {
                foreach (var requisite in requisites)
                {
                    var reqModel = new FacultySectionRequisiteModel();
                    reqModel.IsRequired = requisite.IsRequired;
                    reqModel.EnforcementText = requisite.IsRequired ? GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "Required") : GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "Recommended");
                    reqModel.RequirementCode = requisite.RequirementCode;
                    reqModel.CompletionOrder = requisite.CompletionOrder;
                    reqModel.CompletionOrderDisplay = requisite.CompletionOrder == RequisiteCompletionOrder.Previous ? GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "PreviousRequisite") : requisite.CompletionOrder == RequisiteCompletionOrder.Concurrent ? GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "ConcurrentRequisite") : GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "PreviousOrConcurrentRequisite");
                    // Get the requirement text or course name to display, depending on what's in the incoming requisite
                    if (!string.IsNullOrEmpty(requisite.RequirementCode))
                    {
                        Ellucian.Colleague.Dtos.Student.Requirements.Requirement req = null;
                        if (requisiteRequirements != null)
                        {
                            req = requisiteRequirements.Where(r => r.Code == requisite.RequirementCode).FirstOrDefault();
                        }
                        else
                        {
                            req = await ServiceClient.GetRequirementAsync(requisite.RequirementCode);
                        }
                        if (req != null)
                        {
                            reqModel.DisplayText = string.IsNullOrEmpty(req.DisplayText) ? GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "DefaultRequisiteText") : req.DisplayText;
                        }
                    }
                    else
                    {
                        // If requisite does not have a requirement code it's invalid. Log it but just show it as invalid.
                        Logger.Info("Requisite is not compliant with any expected format.");
                        reqModel.DisplayText = "Invalid specification";
                    }

                    reqModels.Add(reqModel);
                }
            }
            if (sectionRequisites != null && sectionRequisites.Count() > 0)
            {
                foreach (var requisite in sectionRequisites)
                {
                    var reqModel = new FacultySectionRequisiteModel();
                    reqModel.EnforcementText = requisite.IsRequired ? GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "Required") : GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "Recommended");
                    // Section requisites must be concurrent.
                    reqModel.CompletionOrder = RequisiteCompletionOrder.Concurrent;
                    reqModel.CompletionOrderDisplay = GlobalResources.GetString(PlanningResourceFiles.FacultyResources, "ConcurrentRequisite");
                    if (requisite.CorequisiteSectionIds != null && requisite.CorequisiteSectionIds.Count() > 0)
                    {
                        // Single required or recommended corequisite section
                        if (requisite.CorequisiteSectionIds.Count() == 1)
                        {
                            reqModel.DisplayText = await BuildSectionDetailTextAsync(requisite.CorequisiteSectionIds.ElementAt(0));
                        }
                        else
                        {
                            // Multiple corequisite section: ie Take X of the following sections 
                            reqModel.DisplayText = string.Format(GlobalResources.GetString(PlanningResourceFiles.CourseResources, "MultipleNumberOfSectionsRequisiteText"), requisite.NumberNeeded.ToString());
                            for (int i = 0; i < requisite.CorequisiteSectionIds.Count(); i++)
                            {
                                var connector = (i == 0 ? " " : ", ");
                                reqModel.DisplayText += connector + await BuildSectionDetailTextAsync(requisite.CorequisiteSectionIds.ElementAt(i));
                            }
                        }
                    }
                    reqModels.Add(reqModel);
                }
            }
            return reqModels;
        }

        /// <summary>
        /// Controller action to add a new waiver for a student
        /// </summary>
        /// <param name="facultyPermissionType">Type of Faculty Permission: Petition or Consent (required)</param>
        /// <param name="studentId">Id of student (required)</param>
        /// <param name="sectionId">Id of the section (required)</param>
        /// <param name="waiverReason">Petition Reason Code</param>
        /// <param name="waiverExplanation">Freeform additional comments related to the waiver</param>
        /// <param name="isWaived">Boolean indicating if the waiver was granted or denied</param>
        /// <param name="prerequisiteModels">JSON list of the prerequisites being waive. Only required ones in this list will be included.</param>
        /// <returns>A JSON representation the new waiver</returns>
        [ActionName("AddWaiverAsync")]
        [Authorize]
        public async Task<JsonResult> AddWaiverAsync(string facultyPermissionType, string sectionId, string studentId, string waiverReason, string waiverExplanation, bool isWaived, string prerequisiteModels)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId) || string.IsNullOrEmpty(sectionId))
                {
                    throw new ArgumentNullException();
                }

                var studentWaiverDto = BuildStudentWaiverDto(studentId, sectionId, waiverReason, waiverExplanation, isWaived, prerequisiteModels);
                var addedWaiver = await ServiceClient.AddStudentWaiverAsync(studentWaiverDto);

                // Once the waiver has been added need to build and return a waiver model.
                var waiverReasons = await ServiceClient.GetStudentWaiverReasonsAsync();
                var students = new List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>();
                if (!string.IsNullOrEmpty(addedWaiver.StudentId))
                {
                    try
                    {
                        var student = await ServiceClient.GetPlanningStudentAsync(addedWaiver.StudentId);
                        if (student != null)
                        {
                            students.Add(student);
                        }
                    }
                    catch (Exception)
                    {
                    }

                }
                // Get the section to figure out who the section instructors are
                var SectionDto = await ServiceClient.GetSection3Async(addedWaiver.SectionId);
                var faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>();
                if (!string.IsNullOrEmpty(addedWaiver.AuthorizedBy) && SectionDto != null && SectionDto.FacultyIds != null && SectionDto.FacultyIds.Count() > 0)
                {
                    if (SectionDto.FacultyIds.Contains(addedWaiver.AuthorizedBy))
                    {
                        try
                        {
                            var facultyDto = await ServiceClient.GetFacultyAsync(addedWaiver.AuthorizedBy);
                            if (facultyDto != null)
                            {
                                faculty.Add(facultyDto);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                var waiverModels = FacultyHelper.BuildStudentWaivers(new List<StudentWaiver>() { addedWaiver }, waiverReasons, students, faculty);
                var newWaiverModel = waiverModels.First();
                return Json(newWaiverModel, JsonRequestBehavior.AllowGet);

            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Private method used to create a new student waiver DTO
        /// </summary>
        /// <param name="studentId">Id of student (required)</param>
        /// <param name="sectionId">Id of the section (required)</param>
        /// <param name="waiverReason">Waiver Reason Code</param>
        /// <param name="waiverExplanation">Freeform additional comments related to the waiver</param>
        /// <param name="isWaived">Boolean indicating if the waiver was granted or denied</param>
        /// <param name="prerequisiteModels">JSON list of the prerequisites being waive. Only required ones in this list will be included.</param>
        /// <returns>A new Waiver DTO</returns>
        private StudentWaiver BuildStudentWaiverDto(string studentId, string sectionId, string waiverReason, string waiverExplanation, bool isWaived, string prerequisiteModels)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException("studentId", "studentId is required to add a waiver");
            }
            if (string.IsNullOrEmpty(sectionId))
            {
                throw new ArgumentNullException("sectionId", "sectionId is required to add a waiver");
            }
            if (string.IsNullOrEmpty(waiverReason) && string.IsNullOrEmpty(waiverExplanation))
            {
                throw new ArgumentOutOfRangeException("Must have either a reason or an explanation to add a waiver");
            }
            var requisiteModels = !string.IsNullOrEmpty(prerequisiteModels) ? JsonConvert.DeserializeObject<List<FacultySectionRequisiteModel>>(prerequisiteModels) : new List<FacultySectionRequisiteModel>();
            var requiredPrerequisites = requisiteModels.Where(r => r.IsRequired && !string.IsNullOrEmpty(r.RequirementCode) && (r.CompletionOrder == RequisiteCompletionOrder.Previous || r.CompletionOrder == RequisiteCompletionOrder.PreviousOrConcurrent));
            if (requiredPrerequisites.Count() == 0)
            {
                throw new ArgumentNullException("prerequisiteModels", "Unable to create a waiver with no prerequisites.");
            }

            var studentWaiver = new StudentWaiver()
            {
                StudentId = studentId,
                SectionId = sectionId,
                AuthorizedBy = GetEffectivePersonId(),
                Comment = waiverExplanation,
                ReasonCode = waiverReason,
                ChangedBy = GetEffectivePersonId(),
                DateTimeChanged = DateTime.UtcNow,
                RequisiteWaivers = new List<RequisiteWaiver>()
            };
            var waiverStatus = isWaived ? WaiverStatus.Waived : WaiverStatus.Denied;
            foreach (var prereq in requiredPrerequisites)
            {
                var reqWaiver = new RequisiteWaiver() { RequisiteId = prereq.RequirementCode, Status = waiverStatus };
                studentWaiver.RequisiteWaivers.Add(reqWaiver);
            }
            return studentWaiver;
        }
    }
}
