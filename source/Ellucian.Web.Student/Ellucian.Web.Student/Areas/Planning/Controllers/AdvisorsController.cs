﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Planning.Models.Advising;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [MenuAuthorize("advising")]
    [OutputCache(CacheProfile = "NoCache")]
    public class AdvisorsController : BaseStudentController
    {
        private DegreePlanModelBuilder _data;
        private const int MinSearchLength = 3;
        private const int DefaultPageSize = 10;
        private CourseSearchHelper _helper;

        /// <summary>
        /// Creates a new AdvisorsController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public AdvisorsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _data = new DegreePlanModelBuilder(adapterRegistry, ServiceClient, logger);
            _helper = new CourseSearchHelper(ServiceClient, logger, adapterRegistry, true);
        }

        /// <summary>
        /// Returns the default Index view for Advising, which shows the logged-in advisor's list of advisees (if any).
        /// </summary>
        /// <returns>The advisee list Index view</returns>
        [PageAuthorize("advisees")]
        [LinkHelp]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "AreaDescription");

            return View();
        }

        /// <summary>
        /// Controller action to asynchronously get an advisor's advisees.
        /// </summary>
        /// <returns>A list of matching items to display as results for the advisee list</returns>
        public async Task<JsonResult> AdviseesAsync(int pageSize = DefaultPageSize, int pageIndex = 1)
        {
            try
            {
                IEnumerable<Advisee> advisees = await ServiceClient.GetAdviseesAsync(GetEffectivePersonId(), pageSize, pageIndex);
                return Json(await GetAdviseeList(advisees), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Controller action to asynchronously search the global pool of students.
        /// </summary>
        /// <param name="search">The search string</param>
        /// <returns>A list of matching student records, which may be empty</returns>
        public async Task<JsonResult> SearchAsync(string adviseeKeyword, string advisorKeyword, int pageSize = DefaultPageSize, int pageIndex = 1)
        {
            try
            {
                IEnumerable<Advisee> advisees = await ServiceClient.QueryAdviseesAsync(adviseeKeyword, advisorKeyword, pageSize, pageIndex);
                return Json(await GetAdviseeList(advisees), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Controller action for managing a specific advisee. This is the page from which an advisor can approve or deny an advisee's plan.
        /// </summary>
        /// <param name="id">The ID of the advisee to manage</param>
        /// <returns>A view and view-model representing the UI to manage an advisee</returns>
        [LinkHelp]
        public async Task<ActionResult> Advise(string id)
        {
            try
            {
                Ellucian.Colleague.Dtos.Planning.Advisee adviseeDto = await ServiceClient.GetAdviseeAsync(CurrentUser.PersonId, id);
                var hasPrivacyRestriction = false;
                var privacyMessage = string.Empty;
                List<string> programDisplay = null;
                if (adviseeDto != null)
                {                    
                    // If there is a privacy status code, make sure the advisor can access the record
                    Staff staff = null;
                    try
                    {
                        staff = await ServiceClient.GetStaffAsync(CurrentUser.PersonId);
                    }
                    catch
                    {
                        staff = new Staff(CurrentUser.PersonId, string.Empty) { PrivacyCodes = new List<string>() };
                    }

                    hasPrivacyRestriction = !(string.IsNullOrEmpty(adviseeDto.PrivacyStatusCode) || staff.PrivacyCodes.Contains(adviseeDto.PrivacyStatusCode));
                    var privacyConfig = await ServiceClient.GetPrivacyConfigurationAsync();
                 
                    if (hasPrivacyRestriction)
                    {
                        // If there is a restriction, use the record denial message from PID5
                        privacyMessage = privacyConfig != null ? privacyConfig.RecordDenialMessage : string.Empty;
                    }
                    else
                    {
                        // If no restriction, get the privacy warning message that should be displayed to anybody accessing the record
                        privacyMessage = await ServiceClient.GetCachedPrivacyMessageAsync(adviseeDto.PrivacyStatusCode);

                        // If the advisee does not yet have a degree plan - create it.
                        if (!adviseeDto.DegreePlanId.HasValue)
                        {
                            var newDegreePlanAcademicHistory = await ServiceClient.AddDegreePlan5Async(adviseeDto.Id);
                            if (newDegreePlanAcademicHistory != null && newDegreePlanAcademicHistory.DegreePlan != null)
                            {
                                adviseeDto.DegreePlanId = newDegreePlanAcademicHistory.DegreePlan.Id;
                            }
                        }
                        programDisplay = await GetProgramDisplay(adviseeDto.ProgramIds);
                    }

                    //create the AdviseeResult 
                    AdviseeResult advisee = new AdviseeResult(adviseeDto, privacyMessage, hasPrivacyRestriction);
                    advisee.ProgramDisplay = programDisplay != null ? programDisplay : new List<string>();

                    if (!hasPrivacyRestriction)
                    {
                        // Get the names of the advisors
                        IEnumerable<Advisor> advisors = new List<Advisor>();
                        if (adviseeDto.AdvisorIds != null && adviseeDto.AdvisorIds.Count() > 0)
                        {
                            advisors = await ServiceClient.QueryAdvisorsAsync(adviseeDto.AdvisorIds);
                        }
                        foreach (var advisorId in adviseeDto.AdvisorIds)
                        {
                            var advisor = advisors.Where(a => a.Id == advisorId).FirstOrDefault();
                            if (advisor != null)
                            {
                                advisee.AdvisorNames.Add(advisor.FirstName + " " + advisor.LastName);
                            }
                        }
                    }

                    //create the master viewmodel
                    AdvisingViewModel advisingViewModel = new AdvisingViewModel(advisee);

                    //figure out which permissions this advisor has
                    IEnumerable<string> permissions = null;
                    try
                    {
                        permissions = await ServiceClient.GetAdvisorPermissionsAsync();
                        advisingViewModel.CanAdvisorRegister = permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsAllAccessAnyAdvisee"), StringComparer.InvariantCultureIgnoreCase)
                            || (permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsAllAccessAssignedAdvisees"), StringComparer.InvariantCultureIgnoreCase) && advisee.IsAdvisee);
                        advisingViewModel.CanAdvisorModifyPlan = permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsUpdateAnyAdvisee"), StringComparer.InvariantCultureIgnoreCase)
                            || permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsAllAccessAnyAdvisee"), StringComparer.InvariantCultureIgnoreCase)
                            || (permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsUpdateAssignedAdvisees"), StringComparer.InvariantCultureIgnoreCase) && advisee.IsAdvisee)
                            || (permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsAllAccessAssignedAdvisees"), StringComparer.InvariantCultureIgnoreCase) && advisee.IsAdvisee);
                        advisingViewModel.CanAdvisorReviewPlan = permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsReviewAnyAdvisee"), StringComparer.InvariantCultureIgnoreCase)
                            || permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsUpdateAnyAdvisee"), StringComparer.InvariantCultureIgnoreCase)
                            || permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsAllAccessAnyAdvisee"), StringComparer.InvariantCultureIgnoreCase)
                            || (permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsReviewAssignedAdvisees"), StringComparer.InvariantCultureIgnoreCase) && advisee.IsAdvisee)
                            || (permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsUpdateAssignedAdvisees"), StringComparer.InvariantCultureIgnoreCase) && advisee.IsAdvisee)
                            || (permissions.Contains(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "PermissionsAllAccessAssignedAdvisees"), StringComparer.InvariantCultureIgnoreCase) && advisee.IsAdvisee);
                    }
                    catch (AdvisingException)
                    {
                        advisingViewModel.CanAdvisorModifyPlan = false;
                        advisingViewModel.CanAdvisorReviewPlan = false;
                    }

                    ViewBag.AdvisingViewModelJson = JsonConvert.SerializeObject(advisingViewModel);
                    ViewBag.AdvisingError = null;

                    ViewBag.Title = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "AreaDescription");
                    ViewBag.SubMenuOverride = "Index";
                    ViewBag.TermContentPartial = "AdvisingTermContent";


                    var catalogConfiguration = await ServiceClient.GetCachedCourseCatalogConfigurationAsync();
                    dynamic dynamicModel = _helper.RetrieveDynamicViewModel(catalogConfiguration, advisingViewModel);

                    return View(dynamicModel);
                }
                else
                {
                    throw new AdvisingException(AdvisingExceptionCodes.PlanNotFound);
                }
            }
            catch (AdvisingException ex)
            {
                Logger.Error(ex.ToString());
                var viewModel = new AdvisingViewModel();
                if (ex.Code == AdvisingExceptionCodes.UnauthorizedAdvisor)
                {
                    ViewBag.AdvisingError = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "NotAnAdvisorError");
                }
                if (ex.Code == AdvisingExceptionCodes.UnauthorizedAdvisorViewPlan)
                {
                    ViewBag.AdvisingError = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "CannotViewPlanError");
                }
                else if (ex.Code == AdvisingExceptionCodes.PlanNotFound)
                {
                    ViewBag.AdvisingError = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoDegreePlan");
                }
                else
                {
                    ViewBag.AdvisingError = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "UnknownError");
                }
                ViewBag.ResultJSON = JsonConvert.SerializeObject(viewModel);
                return View();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                var viewModel = new AdvisingViewModel();
                ViewBag.AdvisingError = ex.Message;
                ViewBag.ResultJSON = JsonConvert.SerializeObject(viewModel);
                return View();
            }
        }

        /// <summary>
        /// Controller action for an advisor to mark plan review as completed.
        /// </summary>
        /// <param name="degreePlanJson">The degree plan as it was provided to the UI</param>
        /// <returns>A JSON representation of the updated view-model</returns>
        public async Task<JsonResult> ReviewCompleteAsync(string degreePlanJson)
        {
            if (string.IsNullOrEmpty(degreePlanJson))
            {
                Logger.Error(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoDegreePlan"));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoDegreePlan");
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorGenericAdvisingFailed"));
            }

            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                string advisorId = GetEffectivePersonId();

                degreePlan.LastReviewedAdvisorId = advisorId;
                degreePlan.LastReviewedDate = DateTime.Now;
                degreePlan.ReviewRequested = false;
                var updateDegreePlanResponse = ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = ServiceClient.GetPlanningStudentAsync(degreePlan.PersonId);

                await Task.WhenAll(updateDegreePlanResponse, student);

                DegreePlanModel degreePlanModel = await _data.BuildDegreePlanModelAsync(updateDegreePlanResponse.Result.DegreePlan, student.Result,
                    new List<Notification>() { new Notification(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ReviewCompleted"), NotificationType.Success) },
                    updateDegreePlanResponse.Result.AcademicHistory);
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorGenericAdvisingFailed"));
            }
        }

        /// <summary>
        /// Controller action for an advisor to approve courses.
        /// </summary>
        /// <param name="degreePlanJson">The degree plan as it was provided to the UI</param>
        /// <param name="term">The term in which to update approvals</param>
        /// <param name="status">The status code (Approved, Denied)</param>
        /// <param name="courses">List of course IDs</param>
        /// <remarks>degreePlan, term, and status are required.  If no courses are provided, all courses in the selected term will be updated.</remarks>
        /// <returns>A JSON representation of the updated view-model</returns>
        public async Task<JsonResult> ApproveCoursesAsync(string degreePlanJson, string term, DegreePlanApprovalStatus status, List<string> courses)
        {
            if (string.IsNullOrEmpty(degreePlanJson))
            {
                Logger.Error(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoDegreePlan"));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoDegreePlan");
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorGenericAdvisingFailed"));
            }
            if (string.IsNullOrEmpty(term))
            {
                Logger.Error(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoTerm"));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoTerm");
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorGenericAdvisingFailed"));
            }

            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                var courseIds = new List<string>();
                if (courses != null) // If the user submitted courses, update those specific approvals
                {
                    courseIds.AddRange(courses);
                }

                // Ensure the degree plan has a list of approvals (even if empty)
                if (degreePlan.Approvals == null)
                {
                    degreePlan.Approvals = new List<DegreePlanApproval2>();
                }

                foreach (var c in courseIds)
                {
                    var approval = degreePlan.Approvals.Where(x => x.CourseId == c)
                                    .Where(x => x.TermCode == term)
                                    .FirstOrDefault();

                    // An approval exists, update it
                    if (approval != null)
                    {
                        approval.Status = status;
                        approval.Date = DateTime.Now;
                        approval.PersonId = GetEffectivePersonId();
                    }
                    else // no approval for this course/term, create one
                    {
                        degreePlan.Approvals.Add(new DegreePlanApproval2() { PersonId = GetEffectivePersonId(), Date = DateTime.Now, Status = status, TermCode = term, CourseId = c });
                    }
                }


                var updateDegreePlanResponse = ServiceClient.UpdateDegreePlan5Async(degreePlan);
                var student = ServiceClient.GetPlanningStudentAsync(degreePlan.PersonId);

                await Task.WhenAll(updateDegreePlanResponse, student);

                DegreePlanModel degreePlanModel = await _data.BuildDegreePlanModelAsync(updateDegreePlanResponse.Result.DegreePlan, student.Result, null, updateDegreePlanResponse.Result.AcademicHistory);
                return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorGenericAdvisingFailed"));
            }
        }

        /// <summary>
        /// Used to make a student's degree plan protected or to unprotect it.
        /// </summary>
        /// <param name="degreePlanJson">The degree plan as it was provided to the UI. Required.</param>
        /// <param name="protectedState">Indicates how to update planned courses.</param>
        /// <param name="courses">If provided, only the planned courses with these course Ids will be updated on the plan. If not provided all planned courses are updated.</param>
        /// <remarks>DegreePlanJson and protectStatus are required.  If no courseId is provided, all planned courses on the plan will be updated.</remarks>
        /// <returns></returns>
        public async Task<JsonResult> UpdatePlanProtectionAsync(string degreePlanJson, bool protectedState, List<string> courses = null)
        {

            if (string.IsNullOrEmpty(degreePlanJson))
            {
                Logger.Error(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoDegreePlan"));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorNoDegreePlan");
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorGenericAdvisingFailed"));
            }
            try
            {
                var degreePlan = JsonConvert.DeserializeObject<DegreePlan4>(degreePlanJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });

                var courseIds = new List<string>();
                if (courses != null) // If the user submitted courses, update those specific approvals
                {
                    courseIds.AddRange(courses);
                }

                // Add or remove the protections
                bool success = (new Utility.DegreePlanHelper()).UpdatePlanProtection(degreePlan, protectedState, courseIds);
                if (success)
                {
                    var updateDegreePlanReponse = ServiceClient.UpdateDegreePlan5Async(degreePlan);
                    var student = ServiceClient.GetPlanningStudentAsync(degreePlan.PersonId);

                    await Task.WhenAll(updateDegreePlanReponse, student);

                    DegreePlanModel degreePlanModel = await _data.BuildDegreePlanModelAsync(updateDegreePlanReponse.Result.DegreePlan, student.Result, null, updateDegreePlanReponse.Result.AcademicHistory);
                    return new JsonResult() { Data = degreePlanModel, MaxJsonLength = Int32.MaxValue };
                }
                // If not updated it means that the plan was null or there were no planned courses on the plan. Give message that nothing happened.
                throw new Exception();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "ErrorGenericAdvisingFailed"));
            }


        }

        private async Task<List<AdviseeResult>> GetAdviseeList(IEnumerable<Advisee> adviseeDtos)
        {

            List<AdviseeResult> adviseesList = new List<AdviseeResult>();

            try
            {
                var advisorIds = adviseeDtos.Where(adv => adv.AdvisorIds != null).SelectMany(adv => adv.AdvisorIds).Distinct().ToList();
                IEnumerable<Advisor> advisors = new List<Advisor>();
                if (advisorIds != null && advisorIds.Count > 0)
                {
                    advisors = await ServiceClient.QueryAdvisorsAsync(advisorIds);
                }
                // If there is a privacy status code, make sure the advisor can access the record
                // Use the staff record to set privacy restrictions, if not staff record, everything is private
                Staff staff = null;
                try
                {
                    staff = await ServiceClient.GetStaffAsync(CurrentUser.PersonId);
                }
                catch (Exception ex)
                {
                    staff = new Staff(CurrentUser.PersonId, string.Empty) { PrivacyCodes = new List<string>() };
                }
                foreach (var advisee in adviseeDtos)
                {                    
                    var hasPrivacyRestriction = !(string.IsNullOrEmpty(advisee.PrivacyStatusCode) || staff.PrivacyCodes.Contains(advisee.PrivacyStatusCode));
                    var privacyMessage = string.Empty;
                    var privacyConfig = await ServiceClient.GetPrivacyConfigurationAsync();

                    if (hasPrivacyRestriction)
                    {
                        // If there is a restriction, use the record denial message from PID5
                        privacyMessage = privacyConfig != null ? privacyConfig.RecordDenialMessage : string.Empty;
                    }
                    else
                    {
                        privacyMessage = await ServiceClient.GetCachedPrivacyMessageAsync(advisee.PrivacyStatusCode);
                    }
                    AdviseeResult adviseeRecord = new AdviseeResult(advisee, privacyMessage, hasPrivacyRestriction);

                    if (!hasPrivacyRestriction)
                    {
                        adviseeRecord.ProgramDisplay = await GetProgramDisplay(advisee.ProgramIds);
                        adviseeRecord.HasApprovalPending = advisee.ApprovalRequested;
                        // Get the names of the advisors
                        if (advisee.AdvisorIds != null)
                        {
                            foreach (var advisorId in advisee.AdvisorIds)
                            {
                                var advisor = advisors.Where(adv => adv.Id == advisorId).FirstOrDefault();
                                if (advisor != null)
                                {
                                    adviseeRecord.AdvisorNames.Add(advisor.FirstName + " " + advisor.LastName);
                                }
                            }
                        }
                    }
                    adviseesList.Add(adviseeRecord);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                throw (ex);
            }

            return adviseesList;
        }

        private async Task<List<string>> GetProgramDisplay(List<string> programIds)
        {
            List<string> programList = new List<string>();

            if (programIds != null && programIds.Count > 0)
            {
                var allPrograms = await ServiceClient.GetCachedProgramsAsync();
                foreach (var programId in programIds)
                {
                    var program = allPrograms.Where(p => p.Code == programId).FirstOrDefault();
                    programList.Add(program != null ? program.Title : GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "UndeclaredAdviseeProgram"));
                }
            }
            if (!programList.Any())
            {
                programList.Add(GlobalResources.GetString(PlanningResourceFiles.AdvisorResources, "UndeclaredAdviseeProgram"));
            }
            return programList;
        }
    }
}
