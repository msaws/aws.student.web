﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Mvc.Models;
using slf4net;
using Ellucian.Colleague.Api.Client.Exceptions;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    /// <summary>
    /// Specific to student planning controllers - inherits standard BaseStudentController but has addition method(s) needed by planning.
    /// </summary>
    public class BasePlanningController : BaseStudentController
    {
        /// <summary>
        /// Creates a new BasePlanningController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public BasePlanningController(Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        /// <summary>
        /// Used to either get a student's existing degree plan (or if they do not already have one it will create one.)
        /// </summary>
        /// <param name="student">Student DTO - required</param>
        /// <returns>Degre Plan Dto</returns>
        protected async Task< Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory2> GetOrCreateDegreePlanAsync(Ellucian.Colleague.Dtos.Planning.PlanningStudent student, bool validate = true)
        {
            Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory2 degreePlanAcademicHistory = null;
            if (student == null)
            {
                throw new ArgumentNullException("student", "student must be provided.");
            }

            // If the student already has a degreePlanId then get it, otherwise create a plan for this student.
            if (student.DegreePlanId.HasValue)
            {
                try
                {
                    degreePlanAcademicHistory =  await ServiceClient.GetDegreePlan5Async(student.DegreePlanId.ToString(), validate);
                }
                catch (Exception)
                {
                    Logger.Error("Unable to get Degree Plan for student " + student.Id);
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }
            }
            else
            {
                try
                {
                    degreePlanAcademicHistory = await ServiceClient.AddDegreePlan5Async(student.Id);
                }
                catch (Exception)
                {
                    Logger.Error("Unable to add a degree plan for student " + student.Id);
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }
            }

            return degreePlanAcademicHistory;

        }
    }
}
