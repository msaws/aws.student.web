﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Planning.Models;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Ellucian.Web.Student.Areas.Planning.Models.Requirements;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using Ellucian.Rest.Client.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Areas.Planning.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class ProgramsController : BasePlanningController
    {
        IAdapterRegistry AdapterRegistry;
        private DegreePlanModelBuilder DegreePlanModelBuilder;
        private const string _WITHDRAWN_TEXT = "Withdrawn";

        public ProgramsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            AdapterRegistry = adapterRegistry;

            DegreePlanModelBuilder = new DegreePlanModelBuilder(adapterRegistry, ServiceClient, logger);
        }

        /// <summary>
        /// Get the student's program requirements and, optionally, focus on a specific program.
        /// </summary>
        /// <param name="selectedProgram">The program to show upon loading of the page</param>
        /// <returns>The program requirements view</returns>
        [LinkHelp]
        [PageAuthorize("myprogress")]
        public ActionResult MyProgress(string selectedProgram)
        {
            ViewBag.SelectedProgram = selectedProgram;
            return View("MyProgress");
        }

        public async Task<JsonResult> GetActiveProgramsAsync()
        {
            //show only the active programs in this list
            var programs = (await ServiceClient.GetCachedActiveProgramsAsync()).OrderBy(a => a.Title);
            return Json(programs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Return the program evaluations AND ALL other data necessary to render the MyProgress or Advising pages for the given student ID.
        /// </summary>
        /// <param name="studentId">The student whose evaluations are being requested</param>
        /// <returns>The StudentProgramRequirementsModels for the given student</returns>
        [ActionName("GetStudentProgramEvaluations")]
        public async Task<JsonResult> GetStudentProgramEvaluationsAsync(string studentId)
        {
            if (studentId == null) studentId = GetEffectivePersonId();
            try
            {
                var student = await ServiceClient.GetPlanningStudentAsync(studentId);

                var degreePlanAcademicHistory = await GetOrCreateDegreePlanAsync(student);
                if (degreePlanAcademicHistory.DegreePlan == null)
                {
                    //the student somehow does not already have a degree plan, so this is an error condition
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                // Build the evaluation view models
                var evaluations = await BuildStudentProgramRequirementsModelAsync(student, new List<string>(), false, (GetEffectivePersonId() == studentId), degreePlanAcademicHistory.DegreePlan, degreePlanAcademicHistory.AcademicHistory);

                // Retrieve the lits of currently active programs, sorted by title
                var activePrograms =(await  ServiceClient.GetCachedActiveProgramsAsync()).OrderBy(a => a.Title);

                // Use the JSON.Net serializer, because it will pass through enum values as strings intead of integers
                var serializedEvaluations = JsonConvert.SerializeObject(evaluations);
                return new JsonResult() { Data = new { Evaluations = serializedEvaluations, ActivePrograms = activePrograms }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            }
            catch (DegreePlanException dpe)
            {
                Logger.Error(dpe.ToString());
                if (dpe.Code == DegreePlanExceptionCodes.StalePlan)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (dpe.Code == DegreePlanExceptionCodes.NotFound)
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Response.StatusDescription = GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "ErrorNoDegreePlan");
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                return Json(dpe.Message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "UiGenericError"), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Run an evaluation for the selected program (also works for a what-if evaluation)
        /// </summary>
        /// <param name="program"></param>
        /// <returns>The results of a program evaluation when run for a student against the selected program</returns>
        ///
        [ActionName("ProgramEvaluation")]
        public async Task<JsonResult> ProgramEvaluationAsync(string program, bool isWhatIfEvaluation, string studentId)
        {
            try
            {
                var student = await ServiceClient.GetPlanningStudentAsync(studentId);

                // Retrieve the degree plan for this student, if unable, throw exception
                var degreePlanAcademicHistory = await GetOrCreateDegreePlanAsync(student, false);
                if (degreePlanAcademicHistory.DegreePlan == null)
                {
                    //the student somehow does not already have a degree plan, so this is an error condition
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                var viewModel = await BuildStudentProgramRequirementsModelAsync(student, new List<string>() { program }, isWhatIfEvaluation, (GetEffectivePersonId() == studentId), degreePlanAcademicHistory.DegreePlan, degreePlanAcademicHistory.AcademicHistory);
                var json = JsonConvert.SerializeObject(viewModel.FirstOrDefault());
                return new JsonResult() { Data = json, MaxJsonLength = Int32.MaxValue };
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "UiGenericError"));
            }
        }

        /// <summary>
        /// Asynchronously retrieves "what-if" program evaluation results for a list of programs for a given student
        /// and sorts the resulting list in proper order.
        /// </summary>
        /// <param name="studentId">Student Id for whom the what-if evaluations are requested.</param>
        /// <param name="programs">List of program codes that should be evaluated.</param>
        /// <returns>The results of a set of program evaluations when run for a student for a set of program codes</returns>
        [ActionName("EvaluateRelatedPrograms")]
        public async Task<JsonResult> EvaluateRelatedProgramsAsync(string studentId, IEnumerable<string> relatedPrograms)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId");
                }
                if (relatedPrograms == null || relatedPrograms.Count() == 0)
                {
                    throw new ArgumentNullException("relatedPrograms", "relatedPrograms must be properly initialized and contain values.");
                }

                var activeProgramCodesTask = ServiceClient.GetCachedActiveProgramsAsync();
                var studentTask = ServiceClient.GetPlanningStudentAsync(studentId);
                await Task.WhenAll(activeProgramCodesTask, studentTask);

                // Reduce the list of related program codes to just distinct active related programs.
                var activeProgramCodes = (activeProgramCodesTask.Result).Select(ap => ap.Code);
                var activeRelatedPrograms = relatedPrograms.Intersect(activeProgramCodes).Distinct();

                var student = studentTask.Result;
                var degreePlanAcademicHistory = await GetOrCreateDegreePlanAsync(student, false);
                if (degreePlanAcademicHistory.DegreePlan == null)
                {
                    //the student somehow does not already have a degree plan, so this is an error condition
                    throw new DegreePlanException() { Code = DegreePlanExceptionCodes.NotFound };
                }

                var programResults = await BuildStudentProgramRequirementsModelAsync(student, activeRelatedPrograms, true, (GetEffectivePersonId() == studentId), degreePlanAcademicHistory.DegreePlan, degreePlanAcademicHistory.AcademicHistory);
                if (programResults.Count() > 0)
                {
                    // Take the resulting list of program evaluation results and sort the list so that the programs closest to completion are listed first.
                    List<StudentProgramRequirementsModel> sortedProgramResults = null;
                    if (programResults != null && programResults.Count() > 0)
                    {
                        sortedProgramResults = programResults.OrderByDescending(t => t.Program.CompletedRequirementPercent).ThenBy(t => t.Program.Title).ToList();
                        programResults = sortedProgramResults;
                    }
                }
                var jsonProgramResults = JsonConvert.SerializeObject(programResults);
                return new JsonResult() { Data = jsonProgramResults, MaxJsonLength = Int32.MaxValue };
            }
            catch (Exception exception)
            {
                // Problem getting the active programs and building list of active related programs.
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "UiGenericError"));
            }
        }

        /// <summary>
        /// Asynchronously retrieves notices for a given students program evaluation.
        /// </summary>
        /// <param name="studentId">Student Id</param>
        /// <param name="programCode">Program Code</param>
        /// <returns>A list of notices for the student program evaluation</returns>
        public async Task<JsonResult> GetProgramEvaluationNotices(string studentId, string programCode)
        {
            try
            {
                if (string.IsNullOrEmpty(studentId))
                {
                    throw new ArgumentNullException("studentId", "Must provide a student Id");
                }
                if (string.IsNullOrEmpty(programCode))
                {
                    throw new ArgumentNullException("programCode", "Must provide a program code.");
                }

                var notices = await ServiceClient.GetProgramEvaluationNoticesAsync(studentId, programCode);

                // Sequence notices by program text, then student program text, then program and rule-based start, then rule-based end
                List<EvaluationNotice> orderedNotices = notices.Where(n => n.Type == EvaluationNoticeType.Program).ToList();
                orderedNotices.AddRange(notices.Where(n => n.Type == EvaluationNoticeType.StudentProgram).ToList());
                orderedNotices.AddRange(notices.Where(n => n.Type == EvaluationNoticeType.Start).ToList());
                orderedNotices.AddRange(notices.Where(n => n.Type == EvaluationNoticeType.End).ToList());

                var jsonNotices = JsonConvert.SerializeObject(orderedNotices);
                return Json(jsonNotices);
            }
            catch (Exception exception)
            {
                // Problem getting the notices for the student program.
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "UiGenericError"));
            }
        }

        /// <summary>
        /// Build a set of StudentProgramRequirementsModel objects for the given student ID and program codes.
        /// </summary>
        /// <param name="studentId">The student for whom to build out the full model</param>
        /// <param name="program">The programs to be evaluated</param>
        /// <returns>The StudentProgramModels for the given student</returns>
        private async Task<IEnumerable<StudentProgramRequirementsModel>> BuildStudentProgramRequirementsModelAsync(
            PlanningStudent student, IEnumerable<string> programCodes, bool isWhatIfEvaluation,
            bool currentUserIsStudent, DegreePlan4 degreePlan, AcademicHistory3 academicHistory = null)
        {
            List<StudentProgramRequirementsModel> viewModels = new List<StudentProgramRequirementsModel>();

            var watch = new Stopwatch();
            Logger.Info("TIMING " + DateTime.Now.ToLongTimeString() + " Entering BuildStudentProgramRequirementsModel for student " + student.Id);

            try
            {
                // Subjects and departments are required by the various data models (to get the names/descriptions)
                var subjectsTask = ServiceClient.GetCachedSubjectsAsync();
                //GetDepartments is not async - its part of the base API Client
                var departmentsTask = ServiceClient.GetCachedDepartmentsAsync();  //leave this to get ALL Departments in case the student has an inactive one still
                var gradesTask =  ServiceClient.GetCachedGradesAsync();

                var courses = new List<Course2>();

                // Get the set of planned courses from the student's plan
                watch.Reset();
                watch.Start();
                var plannedCoursesTask = GetPlannedCoursesForEvaluation(degreePlan);

                await Task.WhenAll(subjectsTask, departmentsTask, gradesTask, plannedCoursesTask);
                watch.Stop();
                Logger.Info("TIMING GetPlannedCoursesForEvaluation completed in " + watch.ElapsedMilliseconds.ToString());

                var subjects = subjectsTask.Result;
                var departments = departmentsTask.Result;
                var grades = gradesTask.Result;
                var plannedCourses = plannedCoursesTask.Result;

                // Get the academic history, get the individual credits, and note any grade restrictions
                if (academicHistory == null)
                {
                    watch.Reset();
                    watch.Start();
                    academicHistory = await  ServiceClient.GetAcademicHistory3Async(student.Id);
                    watch.Stop();
                    Logger.Info("TIMING GetAcademicHistory2 completed in " + watch.ElapsedMilliseconds.ToString());
                }

                // Build academic credits
                var academicCredits = GetAcademicCredits(academicHistory, grades);

                // Get the student programs and get the program codes from the student programs, if not otherwise provided.
                var studentPrograms = await  ServiceClient.GetStudentProgramsAsync(student.Id);
                if (programCodes == null || programCodes.Count() == 0)
                {
                    programCodes = studentPrograms.Select(sp => sp.ProgramCode);
                }

                // Get program, or all programs if student has more than one
                var programs = new List<Program>();
                if (programCodes.Count() == 1)
                {
                    programs.Add(await  ServiceClient.GetProgramAsync(programCodes.First()));
                }
                else
                {
                    programs = (await ServiceClient.GetCachedProgramsAsync()).ToList();
                }

                // Create a StudentProgramRequirements view model for each program
                foreach (var programCode in programCodes)
                {
                    Logger.Info("TIMING " + DateTime.Now.ToLongTimeString() + " Building StudentProgramRequirementsModel for program " + programCode);

                    StudentProgramRequirementsModel viewModel = new StudentProgramRequirementsModel(student.Id, currentUserIsStudent);

                    try
                    {
                        if (academicHistory != null && academicHistory.GradeRestriction != null)
                        {
                            if (academicHistory.GradeRestriction.IsRestricted && academicHistory.GradeRestriction.Reasons != null)
                            {
                                foreach (string reason in academicHistory.GradeRestriction.Reasons)
                                {
                                    if (!string.IsNullOrEmpty(reason))
                                    {
                                        viewModel.AddNotification(new Notification(reason, NotificationType.Warning, false));
                                    }
                                }
                            }
                        }
                        
                        var program = programs.Where(p => p.Code == programCode).FirstOrDefault();

                        var studentProgram = studentPrograms.FirstOrDefault(sp => sp.ProgramCode == programCode);

                        viewModel.AddRelatedPrograms(program.RelatedPrograms);

                        var programResult = new ProgramEvaluation3();
                        try
                        {
                            programResult = await  ServiceClient.GetProgramEvaluation3Async(student.Id, programCode);
                        }

                        // If this program not found in the list of programs returned, handle it gracefully
                        catch (Exception ex)
                        {
                            Logger.Error(ex.ToString());
                        }
                        
                        // If a program was not found or has no requirements, minimally initialize items needed to build a model.
                        if (programResult.ProgramRequirements == null)
                        {
                            programResult.ProgramRequirements = new ProgramRequirements();
                            programResult.ProgramRequirements.Requirements = new List<Requirement>();
                            programResult.OtherPlannedCredits = new List<PlannedCredit>();
                            programResult.OtherAcademicCredits = new List<string>();
                            programResult.RequirementResults = new List<RequirementResult3>();
                        }

                        var courseIds = new List<string>();

                        if (programResult.ProgramRequirements != null && programResult.ProgramRequirements.Requirements != null)
                        {
                            // Collect all the course Ids cited in the groups from the Program Requirements
                            var groups = programResult.ProgramRequirements.Requirements.SelectMany(r => r.Subrequirements).SelectMany(s => s.Groups);
                            // Get any course Ids cited in the "Courses" specification of the group
                            courseIds.AddRange(groups.Where(g => g != null && g.Courses != null).SelectMany(g => g.Courses));
                            // Get any course Ids cited in the "Courses" specification of the group
                            courseIds.AddRange(groups.Where(g => g != null && g.FromCourses != null).SelectMany(g => g.FromCourses));
                            // Get any course Ids cited in the "Courses" specification of the group
                            courseIds.AddRange(groups.Where(g => g != null && g.ButNotCourses != null).SelectMany(g => g.ButNotCourses));
                        }
                        if (studentProgram != null && studentProgram.AdditionalRequirements != null)
                        {
                            // Collect all the course Ids cited in the groups from the Program Additional Requirements
                            var groups = studentProgram.AdditionalRequirements.Where(ar => ar.Requirement != null).Select(ar => ar.Requirement).SelectMany(r => r.Subrequirements).SelectMany(s => s.Groups);
                            // Get any course Ids cited in the "Courses" specification of the group
                            courseIds.AddRange(groups.Where(g => g != null && g.Courses != null).SelectMany(g => g.Courses));
                            // Get any course Ids cited in the "Courses" specification of the group
                            courseIds.AddRange(groups.Where(g => g != null && g.FromCourses != null).SelectMany(g => g.FromCourses));
                            // Get any course Ids cited in the "Courses" specification of the group
                            courseIds.AddRange(groups.Where(g => g != null && g.ButNotCourses != null).SelectMany(g => g.ButNotCourses));
                        }
                        // Remove any dupes.
                        courseIds = courseIds.Distinct().ToList();

                        var notifications = new List<Notification>();

                        // Retrieve the courses
                        if (courseIds.Count() > 0)
                        {
                            try
                            {
                                courses = (await  ServiceClient.QueryCourses2Async(new CourseQueryCriteria() { CourseIds = courseIds })).ToList();
                                // Display a notification for any courses not found
                                var notFoundCourses = courseIds.Except(courses.Select(c => c.Id));
                                foreach (var courseId in notFoundCourses)
                                {
                                    // This usually happens if there is an acadcred that points to an undefined course.
                                    var message = "Unable to read course " + courseId;
                                    Logger.Error(message);
                                    notifications.Add(new Notification(message, NotificationType.Error, false));
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, "Error to retrieving courses for courseids " + courseIds + ": " + ex.Message);
                            }
                        }

                        viewModel.Program = new StudentProgramModel(AdapterRegistry, program, studentProgram, programResult, academicCredits, plannedCourses, courses, subjects, departments, isWhatIfEvaluation);

                        // If the program has no requirements, enter a notification
                        if (programResult.ProgramRequirements.Requirements.Count == 0)
                        {
                            viewModel.AddNotification(new Notification(program.Title + " " + GlobalResources.GetString(PlanningResourceFiles.ProgramResources, "NoRequirements"), NotificationType.Information, true));
                        }
                    }
                    catch (Exception ex)
                    {
                        // Catch and log any program codes that cannot be evaluated.  Processing can continue for those that do not fail
                        Logger.Error(ex.ToString());
                    }

                    viewModels.Add(viewModel);
                }
                Logger.Info("TIMING " + DateTime.Now.ToLongTimeString() + " Exiting BuildStudentProgramRequirementsModel for student " + student.Id);

                return viewModels;
            }
            catch (ResourceNotFoundException rnfe)
            {
                Logger.Error(rnfe.ToString());
                throw;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Given an Academic History object, return a list of all the included academic credits.
        /// </summary>
        /// <param name="academicHistory">An AcademicHistory object</param>
        /// <returns>A list of Academic Credits</returns>
        private List<AcademicCreditModel> GetAcademicCredits(AcademicHistory3 academicHistory, IEnumerable<Grade> grades)
        {
            var credits = new List<AcademicCreditModel>();
            if (academicHistory.NonTermAcademicCredits != null)
            {
                foreach (var credit in academicHistory.NonTermAcademicCredits)
                {
                    var grade = grades.Where(g => g.Id == credit.VerifiedGradeId).FirstOrDefault();
                    Term term = null;
                    credits.Add(new AcademicCreditModel()
                    {
                        Id = credit.Id,
                        CourseId = credit.CourseId,
                        CourseName = credit.CourseName,
                        Title = credit.Title,
                        Term = term == null ? credit.TermCode : term.Description,
                        VerifiedGrade = grade == null ? "" : grade.LetterGrade,
                        HasVerifiedGrade = credit.HasVerifiedGrade,
                        Credit = credit.IsCompletedCredit ? credit.AdjustedCredit : credit.Credit,
                        IsNonCourse = credit.IsNonCourse,
                        IsWithdrawn = credit.Status == _WITHDRAWN_TEXT ? true : (grade == null ? false : grade.IsWithdraw),
                        OtherAttemptedButNotCompleted = credit.AttemptedCredit > 0 && credit.CompletedCredit <= 0,
                        IsCompletedCredit = credit.IsCompletedCredit,
                        ReplacedStatus = credit.ReplacedStatus.ToString(),
                        ReplacementStatus = credit.ReplacementStatus.ToString(),
                        AdjustedGpaCredit = credit.AdjustedGpaCredit,
                        StartDate = credit.StartDate
                    });
                }
            }
            if (academicHistory.AcademicTerms != null)
            {
                foreach (var aterm in academicHistory.AcademicTerms)
                {
                    if (aterm.AcademicCredits != null)
                    {
                        foreach (var credit in aterm.AcademicCredits)
                        {
                            var grade = grades.Where(g => g.Id == credit.VerifiedGradeId).FirstOrDefault();
                            Term term = null;
                            credits.Add(new AcademicCreditModel()
                            {
                                Id = credit.Id,
                                CourseId = credit.CourseId,
                                CourseName = credit.CourseName,
                                Title = credit.Title,
                                Term = term == null ? credit.TermCode : term.Description,
                                VerifiedGrade = grade == null ? "" : grade.LetterGrade,
                                HasVerifiedGrade = credit.HasVerifiedGrade,
                                Credit = credit.IsCompletedCredit ? credit.AdjustedCredit : credit.Credit,
                                IsNonCourse = credit.IsNonCourse,
                                IsWithdrawn = credit.Status == _WITHDRAWN_TEXT ? true : (grade == null ? false : grade.IsWithdraw),
                                OtherAttemptedButNotCompleted = credit.AttemptedCredit > 0 && credit.CompletedCredit <= 0,
                                IsCompletedCredit = credit.IsCompletedCredit,
                                ReplacedStatus = credit.ReplacedStatus.ToString(),
                                ReplacementStatus = credit.ReplacementStatus.ToString(),
                                AdjustedGpaCredit = credit.AdjustedGpaCredit,
                                StartDate = credit.StartDate
                            });
                        }
                    }
                }
            }

            return credits;
        }

        /// <summary>
        /// Given a Student ID, builds a list of AcademicCreditModel items representing the planned course items on a student's plan.
        /// </summary>
        /// <param name="degreePlan">The ID of the student</param>
        /// <returns>A list of AcademicCreditModel items representing the planned course items on a student's plan</returns>
        private async Task<List<AcademicCreditModel>> GetPlannedCoursesForEvaluation(DegreePlan4 degreePlan)
        {
            var watch = new Stopwatch();

            watch.Reset();
            watch.Start();

            var plannedCourses = new List<AcademicCreditModel>();
            var plannedCourseItems = degreePlan.Terms.SelectMany(t => t.PlannedCourses).ToList();
            plannedCourseItems.AddRange(degreePlan.NonTermPlannedCourses);

            var termsTask = ServiceClient.GetCachedTermsAsync();

            // Get all courses
            IEnumerable<string> courseIds = plannedCourseItems.Where(pc => !string.IsNullOrEmpty(pc.CourseId)).Select(pc => pc.CourseId).Distinct();
            Task<IEnumerable<Course2>> coursesTask = null;
            if (courseIds.Count() > 0)
            {
                try
                {
                    coursesTask =  ServiceClient.QueryCourses2Async(new CourseQueryCriteria() { CourseIds = courseIds });
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "Error retrieving courses for courseids " + courseIds + ": " + ex.Message);
                }
            }
            else
            {
                //create an empty completed task for with an empty result of <T> for the Task.WhenAll
                coursesTask = Task.FromResult<IEnumerable<Course2>>(new List<Course2>());
            }

            // Get all sections
            IEnumerable<string> sectionIds = plannedCourseItems.Where(pc => !string.IsNullOrEmpty(pc.SectionId)).Select(pc => pc.SectionId).Distinct();
            Task<IEnumerable<Section3>> sectionsTask = null;
            if (sectionIds.Count() > 0)
            {
                try
                {
                    sectionsTask =  ServiceClient.GetSections4Async(sectionIds.ToList(), true);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "Error retrieving sections for sectionIds " + sectionIds + ": " + ex.Message);
                }
            }
            else
            {
                //create an empty completed task for with an empty result of <T> for the Task.WhenAll
                sectionsTask = Task.FromResult<IEnumerable<Section3>>(new List<Section3>());
            }

            await Task.WhenAll(termsTask, coursesTask, sectionsTask);
            var allTerms = termsTask.Result;
            var courses = coursesTask.Result.ToList();
            var sections = sectionsTask.Result.ToList();

            // Log any courses not found
            var notFoundCourses = courseIds.Except(courses.Select(c => c.Id));
            foreach (var courseId in notFoundCourses)
            {
                // This usually happens if there is an acadcred that points to an undefined course.
                var message = "Unable to read course " + courseId;
                Logger.Error("GetPlannedCoursesForEvaluation " + message);
            }


            plannedCourseItems = null;

            if (degreePlan.Terms != null)
            {
                var termCodes = degreePlan.Terms.Select(t => t.TermId);
                foreach (var plannedTerm in degreePlan.Terms)
                {
                    if (plannedTerm.PlannedCourses != null)
                    {
                        Term term = allTerms.Where(t => t.Code == plannedTerm.TermId).FirstOrDefault();
                        if (term == null)
                        {
                            var message = "Unable to read term " + plannedTerm.TermId + " when getting planned courses for evaluation for degree plan " + degreePlan.Id;
                            Logger.Error(message);
                        }
                        foreach (var plannedCourse in plannedTerm.PlannedCourses)
                        {
                            var plannedCourseTerm = term;
                            // On the off chance that the planned course exists in a term, but references a different term, get the planned course's term
                            if (!string.IsNullOrEmpty(plannedCourse.TermId) && term != null && plannedCourse.TermId != term.Code)
                            {
                                plannedCourseTerm = allTerms.Where(t => t.Code == plannedCourse.TermId).FirstOrDefault();
                            }

                            // Find the course in the list of courses retrieved
                            var course = courses.FirstOrDefault(x => x.Id == plannedCourse.CourseId);
                            if (course == null)
                            {
                                // This usually happens if there is an acadcred that points to an undefined course.
                                var message = "Unable to read course " + plannedCourse.CourseId + " when getting planned courses for evaluation for degree plan " + degreePlan.Id;
                                Logger.Error(message);
                            }

                            // Find the section in the list of sections retrieved
                            Section3 section = null;
                            if (!string.IsNullOrEmpty(plannedCourse.SectionId))
                            {
                                section = sections.FirstOrDefault(s => s.Id == plannedCourse.SectionId);
                                // Can't find a section, skip it - build the PlannedCourse without it.
                            }

                            plannedCourses.Add(new AcademicCreditModel()
                            {
                                Id = "PlannedCourse",
                                CourseId = plannedCourse.CourseId,
                                CourseName = course != null ? course.SubjectCode + GlobalResources.GetString(PlanningResourceFiles.CourseResources, "CourseDelimiter") + course.Number : string.Empty,
                                Title = section != null ? section.Title : (course != null ? course.Title : ""),
                                Term = plannedCourseTerm == null ? plannedTerm.TermId : plannedCourseTerm.Code,
                                VerifiedGrade = "",
                                HasVerifiedGrade = false,
                                Credit = plannedCourse.Credits.HasValue ? plannedCourse.Credits.Value : (course != null && course.MinimumCredits.HasValue ? course.MinimumCredits.Value : 0)
                            });
                        }
                    }
                }
            }
            watch.Stop();
            Logger.Info("TIMING        PlannedCourses build completed in " + watch.ElapsedMilliseconds.ToString());

            return plannedCourses;
        }
    }
}