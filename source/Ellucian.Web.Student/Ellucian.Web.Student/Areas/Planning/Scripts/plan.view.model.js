﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.Planning = Ellucian.Planning || {};

Ellucian.Planning.PlanViewModel = function PlanViewModel(isProxyUser, baseUrl, planRepository) {

    var self = this;

    // "Inherit" the base view model
    BaseViewModel.call(self);
    //to publish in order to notify subscribers (prpgramRequirementSubscriberModel) if degree-plan is modified
    this.degreePlanChanged = ko.observable(false).publishOn("degreePlanChanged");
    //this is to subscribe to sample plan if gets loaded
    this.samplePlanLoaded = ko.observable(false).subscribeTo("samplePlanLoaded");

    this.samplePlanLoaded.subscribe(function (result) {
        if (result === true) {
            planRepository.removeSessionKeys(self.DegreePlan().PersonId())
            self.degreePlanChanged(true);
}
});

    this.isPageLoading = ko.observable(true);

    var exposedCourses = new Array();

    var sectionPreviewNormalClass = "preview-normal-cal-item";
    var sectionPreviewWarningClass = "preview-warning-cal-item";
    var sectionPreviewErrorClass = "preview-error-cal-item";
    var sectionPreviewIdPrefix = "pre_";

    // Track the last element that was focused so that a screen reader isn't thrown off after they close a dialog box
    var lastFocus = null;

    // adding sections from the schedule
    var addingSectionData = {
        courseId: null,
        sectionId: null,
        termId: null,
        courseCreditsFreeform: false,
        courseFreeformMin: 0,
        courseFreeformMax: 0,
        courseStaticCredits: 0,
        courseNonTerm: false
    };
    var defaultAddOptions = { chosenCredits: null, chosenTerm: null };

    // Height fix for schedule list for online/hybrid courses
    var calendarHeight = 600; // default calendar height
    function calendarHeightFix() {
        // get the height of the currently visible calendar & online/hybrid courses container
        var calendarHeight = $('*[id*=plan-calendar]:visible').height();

        // apply the container height to the schedule list on the left
        $('.schedule-list').height(calendarHeight);
    }

    // Change desktop structure to tab mobiles structure
    // This should probably be done with a custom knokcout binding
    this.changeToMobile = function () {

    }

    // Change tabbed mobile structure back to desktop view
    this.changeToDesktop = function () {

        // Fix calendar
        calendarHeightFix();
    }

    this.planningTerms = ko.observableArray().publishOn("planningTerms");
    this.updatePlanningTerms = function (data) {
        self.planningTerms.removeAll();
        for (var i = 0; i < data.length; i++) {
            self.planningTerms.push(new Term(data[i]));
        }
    }

    this.Notification = ko.observable({ 'Type': ko.observable(''), 'Message': ko.observable('') });

    this.isShowingSchedule = true;

    // isProxyUser should be set to true if the plan is being accessed by a proxy user (advisor)
    // isProxyUser publishes on the "isProxyUser" topic so other view models can restrict features 
    // based on the setting
    if (typeof isProxyUser === "undefined") { isProxyUser = false; }
    this.isProxyUser = ko.observable(isProxyUser).publishOn("isProxyUser");
    this.searchUrl = ko.observable(baseUrl);
    this.searchAsync = ko.observable(false); // When using advising, this gets updated to true

    this.DegreePlan = ko.observable().syncWith("degreePlan").extend({
        rateLimit: {
            timeout: 500, method: "notifyWhenChangesStop"
        }
    });
    this.DegreePlan.subscribe(function () {
        // If the degree plan gets updated, trigger a change on current term to ensure UI is updated
        self.currentTermCode.valueHasMutated();
    });

    // Post to the "catalogResult" topic - when another view model receives a result, it gets posted 
    // to the "catalogResult" topic, allowing the catalog result view model to display the data.
    this.catalogResult = ko.observable(null).publishOn("catalogResult");

    // catalogResultsDisplayed is used by embedded (asynchronous) course catalogs. When a search result is displayed, 
    // a message (with value of true) is posted to the "catalogResultDisplayed" topic, 
    // allowing the catalog index to hide and the catalog results to show.
    this.catalogResultsDisplayed = ko.observable(false).publishOn("catalogResultDisplayed");

    // Is there an ajax request being processed that should block the page?
    this.isLoading = ko.observable(false).syncWith("isLoading", true);
    this.LoadingMessage = ko.observable("").syncWith("LoadingMessage", true);

    this.currentTermCode = ko.observable(null);

    // Subscribe to changes of currentTermCode 
    self.currentTermCode.subscribeChanged(function (prevTerm, newTerm) {
        if (self.isShowingSchedule) {

            // Close all available sections when a new term is selected
            if (prevTerm == null || prevTerm != newTerm) {
                self.ResetToNoCoursesOpen();
            }
        }

        if (typeof (self.DegreePlan().Terms) !== "undefined") {
            self.currentTerm(
                ko.utils.arrayFirst(self.DegreePlan().Terms(), function (term) {
                    return term.Code() == self.currentTermCode();
                })
            );
        } else {
            self.currentTerm(null);
        }

        // Check to see if we're in a mobile-sized page, so we can render the updated plan accordingly
        self.checkForMobile(window, document);

        calendarHeightFix();
    });

    this.currentTerm = ko.observable(null);

    this.FirstTerm = ko.computed(function () {
        if (typeof self.DegreePlan() == "undefined" || typeof self.DegreePlan().Terms == "undefined" || self.DegreePlan().Terms().length < 1) return "";
        return self.DegreePlan().Terms()[0].Code();
    });
    this.LastTerm = ko.computed(function () {
        if (typeof self.DegreePlan() == "undefined" || typeof self.DegreePlan().Terms == "undefined" || self.DegreePlan().Terms().length < 1) return "";
        return self.DegreePlan().Terms()[self.DegreePlan().Terms().length - 1].Code();
    });

    this.termEligibility = ko.observableArray();
    this.hasRegistrationOverride = ko.observable();
    this.currentTermAddMessage = ko.computed(function () {
        if (self.currentTermCode() == null) return "";
        var term = ko.utils.arrayFirst(self.termEligibility(), function (term) {
            return term.Term == self.currentTermCode();
        });
        return term ? term.Message : null;
    });
    this.canRegister = ko.computed(function () {
        if (self.currentTermCode() == null) return false;
        var term = ko.utils.arrayFirst(self.termEligibility(), function (term) {
            return term.Term == self.currentTermCode();
        });
        return forceEnableRegistrationButtons === true || self.hasRegistrationOverride() === true || (term ? term.AllowRegistration : false);
    });
    this.canSkipWaitlist = ko.computed(function () {
        if (self.currentTermCode() == null) return false;
        var term = ko.utils.arrayFirst(self.termEligibility(), function (term) {
            return term.Term == self.currentTermCode();
        });
        return term ? term.AllowSkippingWaitlist : false;
    });

    // The term to be added to the plan (from Add Term dialog, or if a course is added to a new term)
    this.addingTerm = ko.observable();
    // The term to be removed from the plan
    this.removingTerm = ko.observable();

    // The course info used when removing a course or courses from the plan (codes for the update, descriptions for the dialog box)
    this.removingCourseId = ko.observable();
    this.removingCourseTitle = ko.observable();
    this.removingCourseTerm = ko.observable();
    this.removingCourseTermTitle = ko.observable();
    this.removingCourseSectionId = ko.observable();

    //Force timeline to auto-scroll to a given term
    this.ScrollToTerm = function (focusTerm) {
        //do we have to scroll horizontal (normal views) or vertical (mobile view)?
        var scrollHoriz = ($("#dp-plans").css("overflow-y") === "hidden");

        $(".dp-termouter").each(function (index) {
            // If the degree plan isn't initialized yet, don't bother trying to scroll the UI
            if (self.DegreePlan() === null || typeof self.DegreePlan() === 'undefined') return false;

            //the actual term text is two children down in the dp-termlisting DIV; within there will be a link or a span with the text
            var focusDiv = $(this).find(".dp-termlisting").children(":first");
            var name = focusDiv.text();

            if (name === self.currentTerm().Description()) {
                focusDiv = focusDiv.parent();
                if (scrollHoriz === true) {
                    var offset = $("#dp-plans").scrollLeft() + $(this).position().left - $("#scroll-left").outerWidth(true);
                    $("#dp-plans").animate({ scrollLeft: offset }, self.ToggleScrollButtons);
                } else {
                    var offset = $("#dp-plans").scrollTop() + $(this).position().top;
                    $("#dp-plans").animate({ scrollTop: offset }, self.ToggleScrollButtons);
                }
                if (focusTerm === true) {
                    // Attempt to set focus to a specific term.
                    this.FocusInTerm = function (focusDiv) {
                        if (focusDiv !== null) {
                            try {
                                var focused = false;
                                //try to focus on the first course if there is one
                                //to get to the course bubbles you need to hit focusDiv.parent.nextSibling.children -- this is the collection of courses/sections
                                var children = focusDiv.parent().next().children();
                                if (children.length > 0) {
                                    children.first().find(".dp-planneditemlink").first().focus();
                                    focused = true;
                                }

                                //if no courses/sections, just focus on the title - the passed focusDiv should have it as text (dp-termlisting)
                                if (!focused) {
                                    focusDiv.focus();
                                }
                            } catch (e) {
                                //not much we can do if this blows up - just leave UI unchanged
                            }
                        }
                    };
                }
            }
        });
    }

    // flags determining which UI pieces are available or showing
    this.canApprove = ko.computed(function () {
        var isProxy = self.isProxyUser();
        return !isProxy;
    });
    this.canEdit = ko.computed(function () {
        var isProxy = self.isProxyUser();
        return !isProxy;
    }).publishOn("canEdit"); // publish to canEdit topic, so advisors can add from catalog results, etc

    // showRegister indicates whether or not to display UI elements related to registration (Add, Drop buttons, etc)
    this.showRegister = ko.computed(function () {
        if (self.currentTerm() == null) return false;
        var isProxy = self.isProxyUser();
        return !isProxy
            && (self.currentTerm().AllowRegistration() === true || forceEnableRegistrationButtons === true)
            && self.currentTerm().IsTermCompleted() === false;
    });
    this.canSubmitForApproval = ko.computed(function () {
        var plan = ko.utils.unwrapObservable(self.DegreePlan);
        return plan != null && plan.StudentHasAdvisor() && !self.isProxyUser();
    });

    this.showEventTitleAsLinks = ko.observable(true);

    // items for dropping sections from schedule
    this.OpenDropDialog = ko.observable(false);
    this.DropSection = ko.observable();
    this.DropOtherSections = ko.observableArray();
    this.DropMessage = ko.computed(function () {
        var dropMessage = "";
        var dropSection = self.DropSection();
        if (dropSection != null) {
            // OLD VERSION OF CODE: dropMessage = "You have elected to drop: " + dropSection.SubjectCode() + "-" + dropSection.Number() + "-" + dropSection.AcademicHistory.Section.Number() + " (" + dropSection.AcademicHistory.CreditsCeusDisplay() + " Credits)";
            dropMessage = stringDroppingSectionMessage + dropSection.SubjectCode() + "-" + dropSection.Number() + "-" + dropSection.AcademicHistory.Section.Number() + " (" + dropSection.AcademicHistory.CreditsCeusDisplay() + " Credits)";
        }
        return dropMessage;
    });


    // Does the student need to complete payment arrangements for registration?
    // Set in document ready; updated after registration actions.
    this.IsRegistrationPaymentRequired = ko.observable(false);

    // update the calendar visuals for courses that are showing other sections
    this.queuedRepaints = new Object();
    this.RepaintAvailableSections = function (course) {

        if (typeof course !== "undefined") {

            // RepaintAvailableSections gets called numerous times (3+) in quick succession in response to user interaction
            // such as changing a filter. It is extremely expensive to call RemoveAvailableSectionsOnCalendar and
            // ShowAvailableSectionsOnCalendar in such rapid-fire fashion, especially because the end result of each call
            // should be the same. So, queue up these calls in a timeout, and each time you queue a new one, if the old one
            // has not yet executed, cancel it.

            if (course.Id() in self.queuedRepaints) {
                clearTimeout(self.queuedRepaints[course.Id()]);
            }
            self.queuedRepaints[course.Id()] = setTimeout(function () {
                self.RemoveAvailableSectionsOnCalendar(course);
                self.ShowAvailableSectionsOnCalendar(course);
                delete self.queuedRepaints[course.Id()];
            }, 50);
        }
    };

    // This method is used to close all courses with ViewAvailableSections open. This can occur when filters are changed or
    // when moving from one term to another.
    this.ResetToNoCoursesOpen = function () {

        if (self.currentTerm() != null) {
            for (var i = 0; i < self.currentTerm().PlannedCourses().length; i++) {
                // See which courses on the current term have available sections open and for those mark them closed
                // and pull those sections off the calendar
                if (self.currentTerm().PlannedCourses()[i].DisplayingAvailableSections()) {
                    self.currentTerm().PlannedCourses()[i].DisplayingAvailableSections(false);
                    self.RemoveAvailableSectionsOnCalendar(self.currentTerm().PlannedCourses()[i]);
                }
            }
        }
        // If the user is switching from one term to another set the term they were looking at to closed
        // and make sure to pull all sections off the calendar.
        $(".availSections").hide();
        if (typeof prevTerm !== "undefined" && typeof prevTerm.PlannedCourses === "function") {
            ko.utils.arrayForEach(prevTerm.PlannedCourses(), function (plannedCourse) {
                if (plannedCourse.DisplayingAvailableSections) {
                    plannedCourse.DisplayingAvailableSections(false);
                    self.RemoveAvailableSectionsOnCalendar(plannedCourse);
                }
            });
        }
    };

    this.getAvailableSections = function (course, event) {

        // If there is no current term (hopefully because we're looking at non-term courses)
        // Then don't look for available sections because there won't be any
        if (self.currentTermCode() == "NonTermPlaceholder") {
            self.currentTerm().availableSectionsRetrieved(true);
            return;
        }

        var availableSectionSearch = new Object();
        availableSectionSearch.TermCode = self.currentTermCode();
        var plannedCourseSearchs = new Array();
        var plannedCourseModels = self.currentTerm().PlannedCourses();
        for (var i = 0; i < plannedCourseModels.length; i++) {
            var pc = plannedCourseModels[i];
            if (pc.HasSection() || pc.HasRegisteredSection()) {
                plannedCourseSearchs[i] = {
                    CourseId: pc.Id(), SectionId: pc.Section.Id()
                };
            }
            else {
                plannedCourseSearchs[i] = {
                    CourseId: pc.Id()
                };
            }
        }
        availableSectionSearch.PlannedCourses = plannedCourseSearchs;

        var location = ko.utils.arrayFilter(self.currentTerm().locationFilters(), function (filter) {
            return filter.Selected() == true;
        });
        availableSectionSearch.Location = location ? location.map(function (elem) {
            return elem.Value();
        }).join(",") : null;

        var faculty = ko.utils.arrayFilter(self.currentTerm().facultyFilters(), function (filter) {
            return filter.Selected() == true;
        });
        availableSectionSearch.Faculty = faculty ? faculty.map(function (elem) {
            return elem.Value();
        }).join(",") : null;

        var availability = ko.utils.arrayFirst(self.currentTerm().availabilityFilters(), function (filter) {
            return filter.Selected() == true;
        });
        availableSectionSearch.AvailableOnly = availability ? availability.Value() : null;

        var day = ko.utils.arrayFilter(self.currentTerm().dayOfWeekFilters(), function (filter) {
            return filter.Selected() == true;
        });
        availableSectionSearch.Day = day ? day.map(function (elem) {
            return elem.Value();
        }).join(",") : null;

        var time = ko.utils.arrayFirst(self.currentTerm().timeOfDayFilters(), function (filter) {
            return filter.Selected() == true;
        });
        availableSectionSearch.TimeCategory = time ? time.Value() : null;

        var topic = ko.utils.arrayFirst(self.currentTerm().topicFilters(), function (filter) {
            return filter.Selected() == true;
        });
        availableSectionSearch.Topic = topic ? topic.Value() : null;

        $.ajax({
            url: getAvailableSectionsActionUrl,
            data: JSON.stringify(availableSectionSearch),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                if (typeof Ellucian.Planning.getAvailableSections === 'string') {
                    self.LoadingMessage(Ellucian.Planning.planViewModelMessages.getAvailableSections);
                }
                self.isLoading(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    // For each set of available sections (grouped by course id), find that course in the current term, and add the available sections to it.
                    for (var i = 0; i < data.AvailableSectionResults.length; i++) {
                        var availableSectionResult = data.AvailableSectionResults[i];

                        // Find the planned courses in this term that have the same ID as the current availableSectionResult
                        var plannedCourses = ko.utils.arrayFilter(self.currentTerm().PlannedCourses(), function (plannedCourse) {
                            return plannedCourse.Id() == availableSectionResult.CourseId;
                        });

                        // For every planned course that has the same ID as the availableSectionResult,
                        // add the available sections to the planned course
                        for (var j = 0; j < plannedCourses.length; j++) {
                            plannedCourses[j].AvailableSections(availableSectionResult.AvailableSections);
                        }
                    }
                    // Create a list of all sections to use later for filtering
                    self.allSections = ko.observableArray();
                    for (var i = 0; i < data.AvailableSectionResults.length; i++) {
                        ko.utils.arrayPushAll(self.allSections(), data.AvailableSectionResults[i].AvailableSections);
                    }
                    // Loop through the filters and update the view model accordingly
                    self.currentTerm().availabilityFilters.removeAll();
                    for (var i = 0; i < data.Availability.length; i++) {
                        self.currentTerm().availabilityFilters.push(new Filter(data.Availability[i]));
                    }

                    self.currentTerm().locationFilters.removeAll();
                    for (var i = 0; i < data.Locations.length; i++) {
                        //Only push filters that are associated with a section
                        var existingSection = ko.utils.arrayFirst(self.allSections(), function (section) {
                            return section.Location == data.Locations[i].Value;
                        });
                        if (existingSection) {
                            self.currentTerm().locationFilters.push(new Filter(data.Locations[i]));
                        }
                        //self.currentTerm().locationFilters.push(new Filter(data.Locations[i]));
                    }

                    self.currentTerm().dayOfWeekFilters.removeAll();
                    for (var i = 0; i < data.DaysOfWeek.length; i++) {
                        self.currentTerm().dayOfWeekFilters.push(new Filter(data.DaysOfWeek[i]));
                    }

                    self.currentTerm().timeOfDayFilters.removeAll();
                    for (var i = 0; i < data.TimeOfDay.length; i++) {
                        self.currentTerm().timeOfDayFilters.push(new Filter(data.TimeOfDay[i]));
                    }
                    self.currentTerm().topicFilters.removeAll();
                    for (var i = 0; i < data.Topics.length; i++) {
                        //Only push filters that are associated with a section
                        var existingSection = ko.utils.arrayFirst(self.allSections(), function (section) {
                            return section.TopicCode == data.Topics[i].Value;
                        });
                        if (existingSection) {
                            self.currentTerm().topicFilters.push(new Filter(data.Topics[i]));
                        }
                    }
                    self.currentTerm().facultyFilters.removeAll();
                    for (var i = 0; i < data.Faculty.length; i++) {
                        self.currentTerm().facultyFilters.push(new Filter(data.Faculty[i]));
                    }

                    self.currentTerm().availableSectionsRetrieved(true);

                    // If there is a course passed in
                    if (typeof course !== 'undefined') {
                        self.exposeAvailableSections(course, event);
                    }
                    else {
                        // Call the method to re-show previously selected sections on the calendar.
                        for (var i = 0; i < exposedCourses.length; i++) {
                            if (exposedCourses[i] != null) {
                                self.exposeAvailableSections(exposedCourses[i], event);
                            }
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 409) {
                    updateFailedStaleData();
                } else {
                    $('#notificationHost').notificationCenter('addNotification', {
                        message: Ellucian.Planning.planViewModelMessages.unableToGetAvailableSections.format(self.currentTermCode()), type: "error"
                    });
                }
            },
            complete: function () { // Don't use ajax complete handler, as it triggers some UI updates we don't want.
                self.isLoading(false);
            }
        });

    };

    this.CommitDrop = function () {
        var registerList = [];

        // Add the selected drops to the list
        var dropSections = self.dropDialogDropSections();
        for (var i = 0; i < dropSections.length; i++) {
            if (dropSections[i].selected) {
                registerList.push({
                    plannedCourse: dropSections[i].plannedCourse, action: "Drop"
                });
            }
        }

        // Add adds to the list
        var addSections = self.dropDialogAddSections();
        for (var i = 0; i < addSections.length; i++) {
            if (addSections[i].selected) {
                registerList.push({
                    plannedCourse: addSections[i].plannedCourse, action: "Add"
                });
            }
        }

        // Process the list and close the dialog
        self.RegisterSections(registerList);
        self.OpenDropDialog(false);
    };

    this.CancelDropSection = function () {
        self.DropOtherSections([]);
        self.OpenDropDialog(false);
    };

    // Returns the events for populating the graphic view of the schedule
    this.currentTermEvents = function () {
        var events = new Array();

        var term = self.currentTerm();

        if (term !== null) {
            for (var i = 0; i < term.PlannedCourses().length; i++) {
                var pc = term.PlannedCourses()[i];
                var course = term.PlannedCourses()[i];
                var section = term.PlannedCourses()[i].Section;
                var acadHistory = ko.utils.unwrapObservable(term.PlannedCourses()[i].AcademicHistory);

                var useConflictClass = false;

                if (section != null && typeof section !== 'function' && ko.utils.unwrapObservable(acadHistory.Section) == null) {
                    // style differnt when time conflict
                    // OR section not available/cancelled
                    // OR section is full
                    if (section.MeetingTimeConflict()
                            || !section.IsActive()
                            || (section.Available() != null && section.Available() <= 0)) {
                        useConflictClass = true;
                    }
                    for (var j = 0; j < section.PlannedMeetings().length; j++) {
                        var meeting = section.PlannedMeetings()[j];
                        for (var k = 0; k < meeting.Days().length; k++) {
                            if (!(meeting.StartTimeHour() == 0 && meeting.EndTimeHour() == 0)) {
                                var start = new Date();
                                start.setDate(start.getDate() - (start.getDay() - meeting.Days()[k]));
                                start.setHours(meeting.StartTimeHour(), meeting.StartTimeMinute());
                                var end = new Date(start);
                                end.setHours(meeting.EndTimeHour(), meeting.EndTimeMinute());
                                var event = {
                                    title: course.SubjectCode() + '-' + course.Number() + '-' + section.Number(),
                                    allDay: false,
                                    start: start,
                                    end: end,
                                    className: useConflictClass ? "planned-conflict-cal-item" : "planned-cal-item",
                                    sectionId: section.Id(),
                                    allowRemove: self.canEdit() && !(term.PlannedCourses()[i].SectionWaitlistStatus() == "Active" || term.PlannedCourses()[i].SectionWaitlistStatus() == "PermissionToRegister") ? true : false,
                                    course: course,
                                    section: section,
                                    credits: pc.Credits(),  //chosen data for this planned item
                                    gradingType: pc.GetGradingType(),
                                    allowAdding: false,
                                    showEventTitleAsLink: ko.utils.unwrapObservable(self.showEventTitleAsLinks)
                                };
                                events.push(event);
                            }
                        }
                    }
                }


                if (ko.utils.unwrapObservable(acadHistory.Section) != null) {
                    if (ko.utils.unwrapObservable(acadHistory.Section).Meetings().length > 0) {
                        section = acadHistory.Section;
                        for (var j = 0; j < section.PlannedMeetings().length; j++) {
                            var meeting = section.PlannedMeetings()[j];
                            for (var k = 0; k < meeting.Days().length; k++) {
                                if (!(meeting.StartTimeHour() == 0 && meeting.EndTimeHour() == 0)) {
                                    var start = new Date();
                                    start.setDate(start.getDate() - (start.getDay() - meeting.Days()[k]));
                                    start.setHours(meeting.StartTimeHour(), meeting.StartTimeMinute());
                                    var end = new Date(start);
                                    end.setHours(meeting.EndTimeHour(), meeting.EndTimeMinute());
                                    var event = {
                                        id: acadHistory.Section.Id(),
                                        title: course.SubjectCode() + '-' + course.Number() + '-' + acadHistory.Section.Number(),
                                        allDay: false,
                                        start: start,
                                        end: end,
                                        className: "registered-cal-item",
                                        sectionId: acadHistory.Section.Id(),
                                        allowRemove: false,
                                        course: course,
                                        section: section,
                                        credits: pc.Credits(),
                                        gradingType: pc.GetGradingType(),
                                        allowAdding: false,
                                        showEventTitleAsLink: ko.utils.unwrapObservable(self.showEventTitleAsLinks)
                                    };
                                    events.push(event);
                                }
                            }
                        }
                    }
                }
            }
        }
        return events;
    };

    this.ShowAvailableSections = function (data, event) {
        // If the available sections haven't been loaded, get them now...
        if (!self.currentTerm().availableSectionsRetrieved()) {
            lastFocus = document.activeElement;
            // getAvailbleSections will call exposeAvailableSections on the ajax success
            self.getAvailableSections(data, event);
            lastFocus.focus();
        }
        else {
            self.exposeAvailableSections(data, event);
        }
    };

    this.exposeAvailableSections = function (course, event) {
        //Note: you do NOT want to manage the visibility of the other sections DIV via an observable - doing so can create a KO dependency chain that
        //essentially causes the schedule UI to re-render several times in response to a simple event like expanding/collapsing the section list.
        //This in turn makes the page feel sluggish, especially with a screen reader attached.

        var div = $(".availSections[data-course='" + course.Id() + "']");  //get to the nested DIV with class of 'availSections' that matches the current course
        if (course.DisplayingAvailableSections()) {
            div.slideUp(50);
            course.DisplayingAvailableSections(false);
            exposedCourses[course.Id()] = null;
            setTimeout(function () {
                self.RemoveAvailableSectionsOnCalendar(course);
            }, 65);
        } else {
            div.slideDown(50);
            course.DisplayingAvailableSections(true);
            exposedCourses[course.Id()] = course;
            setTimeout(function () { self.ShowAvailableSectionsOnCalendar(course); }, 130);
        }
    };

    this.MouseOverAvailableSection = function (mouseIn, data, event) {
        var target;
        if (event.target) {
            target = event.target;
        } else if (event.srcElement) {
            target = event.srcElement;
        }

        if (mouseIn) {
            self.HighlightAvailableSectionsOnCalendar(data.Id, true);
        } else {
            self.HighlightAvailableSectionsOnCalendar(data.Id, false);
        }
    };

    this.AddSectionToPlan = function (courseModel, section) {
        if (courseModel != null) {
            // Grab the Course object out of the model
            var course = courseModel.Course;
            if (typeof course === "undefined") {
                course = courseModel;
            }

            if (section != null) {
                // Open global Section Details dialog
                showSectionDetails({
                    course: course, section: section, allowAdding: true, addTerms: null, addCallback: self.commitAddSection, creditsOverride: null, studentId: self.DegreePlan().PersonId(), canSkipWaitlist: self.canSkipWaitlist()
                });

                // Hide the term selection menu
                addingSectionData.sectionId = section.Id;
                addingSectionData.sectionNumber = section.Number;
                addingSectionData.termId = self.currentTermCode();
                var sectionIsNonTerm = courseModel.IsNonTermSection();
                // Since some sections within a course may be nonterm and others not - check the specific section for a term.
                if (section.TermId === null || section.TermId === "") {
                    sectionIsNonTerm = true;
                }
                addingSectionData.courseNonTerm = sectionIsNonTerm;

                // Set up credit tracking if user decided to add from global dialog
                if (isCourseOrSectionVariableCredit(ko.mapping.toJS(section))) {
                    var interval = section.VariableCreditIncrement;
                    var min = section.MinimumCredits;
                    var max = section.MaximumCredits;

                    // Increment can be null, in which case the user can type any value they want
                    if (interval != null) {
                        addingSectionData.courseCreditsFreeform = false;
                    } else {
                        addingSectionData.courseCreditsFreeform = true;
                        addingSectionData.courseFreeformMin = min;
                        addingSectionData.courseFreeformMax = max;
                    }
                } else {
                    addingSectionData.courseCreditsFreeform = false;
                    if (!isNullOrEmpty(section.MinimumCredits)) {
                        addingSectionData.courseStaticCredits = section.MinimumCredits;
                    } else {
                        addingSectionData.courseStaticCredits = -1;
                    }
                }

                // Store the data for next step of submission
                addingSectionData.courseId = course.Id();
                addingSectionData.courseSectionId = section.Id;
            }
        }
    };

    // Removing courses from a plan
    this.removeCourseDialogIsOpen = ko.observable(false);
    this.removeCourseDialogOptions = ko.observable({
        autoOpen: false, modal: true, minWidth: $(window).width() / 3,
        show: {
            effect: "fade", duration: dialogFadeTimeMs
        }, hide: {
            effect: "fade", duration: dialogFadeTimeMs
        }, resizable: false
    });

    // Show remove course dialog
    this.ShowRemoveCourseDialog = function (course, term, isNonTerm) {
        self.removeCourseDialogIsOpen(true);
        if (isNonTerm) {
            self.removingCourseTerm("");
            self.removingCourseTermTitle(Ellucian.Planning.planViewModelMessages.removeNonTermSectionLabel);
        } else {
            self.removingCourseTerm(term.Code());
            self.removingCourseTermTitle(term.Description());
        }
        self.removingCourseId(course.Id());
        self.removingCourseTitle(course.CourseTitleDisplay());
        self.removingCourseSectionId(null);
        if (course.Section != null && typeof course.Section !== 'function') {
            self.removingCourseSectionId(course.Section.Id());
        }
    };


    this.CancelRemoveCourse = function () {
        self.removeCourseDialogIsOpen(false);
        self.removingCourseId(null);
        self.removingCourseTitle("");
        self.removingCourseTerm(null);
        self.removingCourseTermTitle("");
        self.removingCourseSectionId(null);
    };

    // Commit course removal
    this.commitRemoveCourse = function () {
        if (self.removingCourseId() != "" && self.removingCourseId() != null) {

            if (typeof Ellucian.Planning.planViewModelMessages.removingCourseProcessing === 'string') {
                self.LoadingMessage(Ellucian.Planning.planViewModelMessages.removingCourseProcessing);
            }
            self.isLoading(true);
            self.removeCourseDialogIsOpen(false);

            planRepository.removeCourse(self.DegreePlan().PersonId(), self.removingCourseId(), self.removingCourseTerm(), self.removingCourseSectionId(), ko.mapping.toJSON(ko.utils.unwrapObservable(self.DegreePlan).DegreePlanDto))
                .then(function (plan) {
                    self.degreePlanChanged(true);
                    self.repositorySuccessHandler(plan);
                })
                .catch(function (error) {
                    self.repositoryErrorHandler(error);
                })
                .then(removeCourseComplete, removeCourseComplete);
        }
    };

    var removeCourseComplete = function () {
        self.removingCourseId(null);
        self.removingCourseTitle("");
        self.removingCourseTerm(null);
        self.removingCourseTermTitle("");
        self.removingCourseSectionId(null);
        updateCompleted();
    };

    // Update degree plan in memory when a stale copy is detected
    var updateFailedStaleData = function (message) {
        planRepository.get(self.DegreePlan().PersonId())
            .then(function (data) {
                self.degreePlanChanged(true);
                self.repositorySuccessHandler(data);
            })
            .catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
            })
            .then(updateCompleted, updateCompleted);
    };

    this.commitAddSection = function (options) {
        var options = $.extend({}, defaultAddOptions, options);

        if (addingSectionData.courseId != null) {

            // First ensure the chosen number of credits (if variable) is OK
            if (addingSectionData.courseCreditsFreeform) {
                var value = options.chosenCredits;
                // Must not be null
                if (value === null || value === "" || value === undefined) {
                    alert(Ellucian.Planning.planViewModelMessages.specifyCredits);
                    focusAddingSection();
                    return;
                }
                // Must be a number
                if (!isNumber(value)) {
                    alert(Ellucian.Planning.planViewModelMessages.specifyCreditsRange.format(addingSectionData.courseFreeformMin, addingSectionData.courseFreeformMax));
                    focusAddingSection();
                    return;
                }
                // Must be within the range
                if (value > addingSectionData.courseFreeformMax || value < addingSectionData.courseFreeformMin) {
                    alert(Ellucian.Planning.planViewModelMessages.specifyCreditsRange.format(addingSectionData.courseFreeformMin, addingSectionData.courseFreeformMax));
                    focusAddingSection();
                    return;
                }
            }

            // How many credits? Is it variable?
            var numCredits;
            if (!isNullOrEmpty(options.chosenCredits)) {
                numCredits = options.chosenCredits;
            }
            else {
                //static credits amount
                numCredits = addingSectionData.courseStaticCredits;
            }

            //if this one is a non-term item, strip off the the term ID
            if (addingSectionData.courseNonTerm) {
                addingSectionData.termId = "";
            }

            self.LoadingMessage(Ellucian.Planning.planViewModelMessages.updatingMessage);
            self.ajaxBeforeSendHandler();

            //close the details dialog
            closeSectionDetails();

            planRepository.addSection(self.DegreePlan().PersonId(), addingSectionData.courseId, addingSectionData.termId, addingSectionData.sectionId, options.chosenGradingType, numCredits, ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
            .then(function (data) {
                addingSectionData.courseId = null;
                addingSectionData.sectionId = null;
                addingSectionData.sectionNumber = null;
                addingSectionData.termId = null;
                self.degreePlanChanged(true);
                self.repositorySuccessHandler(data);
            })
            .catch(function (error) {
                addingSectionData.courseId = null;
                addingSectionData.sectionId = null;
                addingSectionData.sectionNumber = null;
                addingSectionData.termId = null;
                self.repositoryErrorHandler(error);
            })
            .then(updateCompleted, updateCompleted);
        }
    };

    this.PlannedCourseCredits = function () {
        var credits = 0;
        if (self.currentTerm() != null) {
            var courses = self.currentTerm().PlannedCourses();
            for (i = 0; i < courses.length; i++) {
                var c = courses[i];
            }
        }

        return credits;
    };

    this.HighlightAvailableSectionsOnCalendar = function (id, enable) {
        var eId = sectionPreviewIdPrefix + id;

        var events = $("div[data-previewid = '" + eId + "']");
        $.each(events, function (i, event) {
            if (enable) {
                $(event).addClass('preview-fullopacity-cal-item');
            } else {
                $(event).removeClass('preview-fullopacity-cal-item');
            }
        });
    };

    this.ShowAvailableSectionsOnCalendar = function (plannedCourse) {
        var events = new Array();
        var course = plannedCourse;
        var filteredSections = plannedCourse.PagedAvailableSections();

        if (filteredSections.length > 0) {
            for (var i = 0; i < filteredSections.length; i++) {
                var previewClassName = sectionPreviewNormalClass;
                var section = filteredSections[i];
                var theBorderColor = "#E31769";
                if (section != null && typeof section !== 'function') {

                    // Logic for the UI: if the section is full and there IS a waitlist, show as a warning
                    //  If the section is full and there is NOT a waitlist, show as an error

                    // is there a waitlist? if so, change preview look
                    if (section.WaitlistAvailable === true && (section.Waitlisted > 0 || (section.Available !== null && section.Available === 0 && section.Capacity !== null))) {
                        previewClassName = sectionPreviewWarningClass;
                    }
                        // otherwise if the section is full, change preview look
                    else if (section.Available != null && section.Available <= 0) {
                        previewClassName = sectionPreviewErrorClass;
                    }

                    for (var j = 0; j < section.PlannedMeetings.length; j++) {
                        var meeting = section.PlannedMeetings[j];

                        if (meeting.StartTime != null && meeting.EndTime != null) {

                            for (var k = 0; k < meeting.Days.length; k++) {

                                var start = new Date();
                                start.setDate(start.getDate() - (start.getDay() - meeting.Days[k]));
                                start.setHours(meeting.StartTimeHour, meeting.StartTimeMinute);
                                var end = new Date(start);
                                end.setHours(meeting.EndTimeHour, meeting.EndTimeMinute);

                                var event = {
                                    id: sectionPreviewIdPrefix + section.Id,
                                    title: course.SubjectCode() + '-' + course.Number() + '-' + section.Number,
                                    allDay: false,
                                    start: start,
                                    end: end,
                                    className: previewClassName,
                                    sectionId: section.Id,
                                    allowRemove: false,
                                    //for these available sections that can be added you need to send the PlannedCourse item, since it will be re-routed
                                    //to the AddSectionToPlan method to set up the add workflow; otherwise you can just send the Course object
                                    course: plannedCourse,
                                    section: section,
                                    allowAdding: true,
                                    showEventTitleAsLink: ko.utils.unwrapObservable(self.showEventTitleAsLinks)
                                };
                                events.push(event);
                            }
                        }
                    }
                }
            }
        }

        // add preview sections to calendar
        ko.utils.arrayForEach(events, function (event) {
            setTimeout(function () {
                $('.schedule-calendar').fullCalendar('renderEvent', event);
            }, 10);
        });
    };

    this.RemoveAvailableSectionsOnCalendar = function (plannedCourse) {
        var ids = new Array();
        var course = plannedCourse.Course;
        for (var i = 0; i < plannedCourse.AvailableSections().length; i++) {
            var section = plannedCourse.AvailableSections()[i];
            if (section != null && typeof section !== 'function') {
                ids.push(sectionPreviewIdPrefix + section.Id);
            }
        }
        ko.utils.arrayForEach(ids, function (id) {
            $('.schedule-calendar').fullCalendar('removeEvents', function (event) {
                if (event.id != undefined) {
                    return (event.id == id);
                }
                return false;
            });
        });

    };

    // Event click handler for removing events from calendar
    this.RemoveSectionClickHandler = function (calEvent) {

        self.LoadingMessage(Ellucian.Planning.planViewModelMessages.updatingMessage);
        self.ajaxBeforeSendHandler();

        planRepository.removeSection(self.DegreePlan().PersonId(), calEvent.sectionId, ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
            .then(function (data) {
                self.degreePlanChanged(true);
                self.repositorySuccessHandler(data);
            })
            .catch(function (error) {
                self.repositoryErrorHandler(error);
            })
            .then(updateCompleted, updateCompleted);
    };

    this.SectionDetailsClickHandler = function (calEvent) {
        if (calEvent !== null) {
            if (calEvent.allowAdding) {
                self.AddSectionToPlan(calEvent.course, calEvent.section);
            } else {
                showSectionDetails({
                    course: calEvent.course, section: calEvent.section, allowAdding: calEvent.allowAdding, creditsOverride: calEvent.credits, gradingTypeOverride: calEvent.gradingType, addCallback: null, addTerms: null, studentId: self.DegreePlan().PersonId(), canSkipWaitlist: self.canSkipWaitlist()
                });
            }
        }
    };

    this.RegisterAllHandler = function () {
        var registerList = [];

        // Add all the planned sections that haven't been registered (no academic credit)
        var sections = self.notRegisteredPlannedCourses();
        for (var i = 0; i < sections.length; i++) {
            registerList.push({
                plannedCourse: sections[i], action: "Add"
            });
        }

        self.RegisterSections(registerList);
    };

    this.RegisterSectionHandler = function (plannedCourse, action, plannedTerm) {
        // If the user wants to drop a section, prompt them for any other adjustments to the schedule
        // This allows them to "wash" two sections (avoid late drop fees/partial refunds)
        // Also allows them to adjust co-requisite sections
        if (action == "Drop") {

            // Track the plannedCourse that was selected to be dropped
            self.DropSection(plannedCourse);

            // Now verify other registered planned sections on this term were not relying on this section to have their required coreq sections met.
            var dropOtherPlannedCourses = self.VerifyCorequisiteSections();
            self.DropOtherSections(dropOtherPlannedCourses);

            var registeredSections = self.registeredPlannedCourses();
            var dropDialogDropSections = [];

            for (var i = 0; i < registeredSections.length; i++) {
                // Is this plannedCourse in the list of other sections to drop?
                var isCorrequisite = self.DropOtherSections.indexOf(registeredSections[i]) >= 0;
                dropDialogDropSections.push({
                    selected: isCorrequisite || plannedCourse == registeredSections[i], plannedCourse: registeredSections[i], message: isCorrequisite ? "(requires " + plannedCourse.SubjectCode() + "-" + plannedCourse.Number() + "-" + plannedCourse.AcademicHistory.Section.Number() + ")" : ""
                });
            }
            self.dropDialogDropSections(dropDialogDropSections);

            var notRegisteredSections = self.notRegisteredPlannedCourses();
            var dropDialogAddSections = [];
            for (var i = 0; i < notRegisteredSections.length; i++) {
                dropDialogAddSections.push({
                    selected: false, plannedCourse: notRegisteredSections[i]
                });
            }
            self.dropDialogAddSections(dropDialogAddSections);

            self.OpenDropDialog(true);
        } else {
            self.RegisterSections([{
                plannedCourse: plannedCourse, action: action
            }]);
        }
    };

    // Validate planned courses with registered sections in a planned term when one of the items is being dropped.
    // If one of the remaining planned courses has a sections with unmet required section corequisites, then that
    // planned course item is returned.  It will also need to be dropped. 
    this.registeredPlannedCourses = ko.computed(function () {
        if (self.currentTerm() === null
            || typeof self.currentTerm().PlannedCourses === 'undefined') return [];
        var registeredPlannedCourses = [];

        for (var i = 0; i < self.currentTerm().PlannedCourses().length; i++) {
            if (ko.utils.unwrapObservable(self.currentTerm().PlannedCourses()[i].AcademicHistory) != null && self.currentTerm().PlannedCourses()[i].AcademicHistory.HasSection() == true) {
                registeredPlannedCourses.push(self.currentTerm().PlannedCourses()[i]);
            }
        }
        return registeredPlannedCourses;
    }).extend({
        throttle: 10
    });

    this.notRegisteredPlannedCourses = ko.computed(function () {
        if (self.currentTerm() == null
            || typeof self.currentTerm().PlannedCourses === 'undefined') return [];
        var notRegisteredPlannedCourses = [];

        var plannedCourses = self.currentTerm().PlannedCourses();
        for (var i = 0; i < plannedCourses.length; i++) {
            var plannedCourse = plannedCourses[i];
            if (ko.utils.unwrapObservable(plannedCourse.AcademicHistory.HasSection) === false
                && ko.utils.unwrapObservable(plannedCourse.AcademicHistory.IsCompletedCredit) === false
                && ko.utils.unwrapObservable(plannedCourse.Section) != null) {
                notRegisteredPlannedCourses.push(plannedCourse);
            }
        }
        return notRegisteredPlannedCourses;
    }).extend({
        throttle: 10
    });

    // Two observable arrays to track the users selections on the drop dialog
    // These are just associated arrays of plannedCourses, a message, and a boolean that indicates if they were selected for action this transaction
    this.dropDialogDropSections = ko.observableArray();
    this.dropDialogAddSections = ko.observableArray();

    this.VerifyCorequisiteSections = function () {
        var dropSectionId = self.DropSection().AcademicHistory.Section.Id();
        var otherCoursesToDrop = [];
        for (var i = 0; i < self.registeredPlannedCourses().length; i++) {
            if (dropSectionId !== self.registeredPlannedCourses()[i].AcademicHistory.Section.Id()) {
                var courseToVerify = self.registeredPlannedCourses()[i];
                if (courseToVerify.AcademicHistory.Section != null) {
                    var otherSec = courseToVerify.AcademicHistory.Section;
                    var otherSecCoReqs = ko.utils.unwrapObservable(otherSec.SectionCorequisites);
                    if (otherSecCoReqs != null && otherSecCoReqs.indexOf(dropSectionId) !== -1) {
                        var numberRequired = otherSec.SectionCorequisitesNumberNeeded();
                        var numberFound = 0;
                        for (var j = 0; j < otherSecCoReqs.length; j++) {
                            for (var k = 0; k < self.registeredPlannedCourses().length; k++) {
                                var plannedCourseOnSchedule = self.registeredPlannedCourses()[k];
                                if (plannedCourseOnSchedule.AcademicHistory.HasSection() === true && dropSectionId !== plannedCourseOnSchedule.AcademicHistory.Section.Id()) {
                                    var registeredSectionId = plannedCourseOnSchedule.AcademicHistory.Section.Id();
                                    if (registeredSectionId === otherSecCoReqs[j]) {
                                        numberFound += 1;
                                    }
                                }
                            }
                        }
                        // If the number found is less than the number required, then verification of the schedule failed.
                        // Return the planned course on the schedule that was no longer valid.
                        if (numberFound < numberRequired) {
                            otherCoursesToDrop.push(ko.utils.unwrapObservable(courseToVerify));
                        }

                    }
                }
            }
        }
        return otherCoursesToDrop;
    };

    this.RegisterSections = function (registerList) {
        if (registerList.length > 0) {
            var sectionRegistrations = [];
            for (var i = 0; i < registerList.length; i++) {
                var plannedCourse = registerList[i].plannedCourse;
                var action = registerList[i].action;

                var section = ko.utils.unwrapObservable(plannedCourse.Section)
                var sectionId = section == null ? plannedCourse.AcademicHistory.Section.Id() : section.Id();
                var credits = plannedCourse.Credits();

                // Pass/Fail and Audit are handled as separate actions (Graded equates to the default Add for purposes of registration)
                if (action == "Add") {

                    if ((plannedCourse.GradingType() == Ellucian.Course.SectionDetails.stringAuditGradingCode) || (plannedCourse.GradingType() == Ellucian.Course.SectionDetails.stringPassFailGradingCode))
                    {
                        action = plannedCourse.GradingType();
                    }
                }
                sectionRegistrations.push({
                    SectionId: sectionId, Credits: credits, Action: action
                });
            }

            self.ajaxBeforeSendHandler(stringRegistrationMessage);

            planRepository.register(self.DegreePlan().PersonId(), sectionRegistrations)
            .then(function (data) {
                self.LoadingMessage(stringRegistrationCompleteMessage);
                self.CompleteRegistration(data);
            })
            .catch(function (error) {
                self.repositoryErrorHandler(error);
            })
            .then(null, updateCompleted);
        }
    };

    this.CompleteRegistration = function (notificationList) {

        planRepository.completeRegistration(self.DegreePlan().PersonId(), notificationList)
        .then(function (plan) {
            self.degreePlanChanged(true);
            self.repositorySuccessHandler(plan);
        })
        .catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
        })
        .then(updateCompleted, updateCompleted);
    };

    this.requestReview = function () {

        self.LoadingMessage(stringSubmitTermMessage);
        self.ajaxBeforeSendHandler();

        planRepository.requestReview(self.DegreePlan().PersonId(), ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
        .then(function (data) {
            self.repositorySuccessHandler(data);
        })
        .catch(function (error) {
            self.repositoryErrorHandler(error);
        })
        .then(updateCompleted, updateCompleted);
    };

    this.Details = function (course, section) {
        showCourseOrSectionDetails({
            course: course, allowAdding: false, addTerms: null, addCallback: null, creditsOverride: null, canSkipWaitlist: self.canSkipWaitlist()
        });
    };

    this.updateEvaluations = ko.observable().syncWith("updateEvaluations");


    this.ajaxBeforeSendHandler = function (data) {
        self.LoadingMessage(Ellucian.Planning.planViewModelMessages.updatingMessage);
        self.isLoading(true);
    };

    // A successful plan update has occurred, do some clean-up.
    var updateCompleted = function () {
        self.CheckIfPaymentRequired();
        self.currentTermCode.valueHasMutated();
        self.isLoading(false);
    };

    this.SelectPreviousTerm = function () {
        var oldTerm = ko.utils.arrayFirst(self.DegreePlan().Terms(), function (term) {
            return term.Code() == self.currentTermCode();
        });
        var newIndex = self.DegreePlan().Terms().indexOf(oldTerm) - 1;
        self.currentTermCode(self.DegreePlan().Terms()[newIndex].Code());
    };

    this.SelectNextTerm = function () {
        var oldTerm = ko.utils.arrayFirst(self.DegreePlan().Terms(), function (term) {
            return term.Code() == self.currentTermCode();
        });
        var newIndex = self.DegreePlan().Terms().indexOf(oldTerm) + 1;
        self.currentTermCode(self.DegreePlan().Terms()[newIndex].Code());
    };

    this.ShowScheduleTerm = function (termCode) {
        //switch to schedule view - assume that this can only be called from timeline view
        $("#schedplan-schedule-tablink").click();

        setTimeout(function () {
            self.currentTermCode(termCode);
        }, 5);
    };

    //================================
    // Adding and Removing Terms

    // Remove term dialog - for removing terms
    this.removeTermDialogIsOpen = ko.observable(false);
    

    // Show remove term dialog
    this.ShowRemoveTermDialog = function (term) {
        self.removeTermDialogIsOpen(true);
        if (typeof term !== 'undefined') {
            self.removingTerm(term);
        } else {
            self.removingTerm(self.currentTerm());
        }
    };

    //since this dialog is part of the time line view it is already bound
    // Commit term removal
    this.CommitRemoveTerm = function (data) {
        var nextTermCode = null;
        if (self.removingTerm() != null) {

            if (self.currentTermCode() === self.removingTerm().Code()) {
                for (var i = 0; i < self.DegreePlan().Terms().length; i++) {
                    if (self.DegreePlan().Terms()[i].Code() === self.removingTerm().Code()) {
                        // If this is not the last term in the array, the one after will become the current term. Otherwise use the one before.
                        if (i + 1 < self.DegreePlan().Terms().length) {
                            nextTermCode = self.DegreePlan().Terms()[i + 1].Code();
                        } else {
                            nextTermCode = self.DegreePlan().Terms()[i - 1].Code();
                        }
                    }
                }
            }

            if (typeof stringRemovingTermProcessing === 'string') {
                self.LoadingMessage(stringRemovingTermProcessing);
            }
            self.isLoading(true);
            self.removeTermDialogIsOpen(false);

            planRepository.removeTerm(self.DegreePlan().PersonId(), self.removingTerm().Code(), ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
                .then(function (data) {
                    self.degreePlanChanged(true);
                    if (nextTermCode != null) {
                        var currentTerm = ko.utils.arrayFirst(self.DegreePlan().Terms(), function (term) {
                            return term.Code() == nextTermCode;
                        });
                        self.currentTermCode(currentTerm.Code());
                    }
                    self.repositorySuccessHandler(data);
                    self.removingTerm(null);
                })
                .catch(function (error) {
                    self.repositoryErrorHandler(error);
                })
                .then(updateCompleted, updateCompleted);
        }
    };

    this.CancelRemoveTerm = function () {
        self.removeTermDialogIsOpen(false);
        self.addingTerm(null);
    };

    this.addTermDialogIsOpen = ko.observable(false);
  

    this.ShowAddTermDialog = function () {
        var terms = self.DegreePlan().UnplannedTerms();
        try {
            // Show the dialog or auto-submit if only one item to pick
            if (terms.length == 1) {
                $("#addTermId").attr("selectedIndex", 0);
                $("#schedplan-addtermformaddbutton").click();
            }
            else {
                self.addTermDialogIsOpen(true);
            }
        } catch (e) {
            $('#notificationHost').notificationCenter('addNotification', {
                message: Ellucian.Planning.planViewModelMessages.noTermsCanBeAdded, type: "error"
            });
        }
    };

    // Commit term add
    this.CommitAddTerm = function () {
        if (self.addingTerm() != null) {

            if (typeof stringAddingTermProcessing === 'string') {
                self.LoadingMessage(stringAddingTermProcessing);
            }
            self.isLoading(true);
            self.addTermDialogIsOpen(false);

            planRepository.addTerm(self.DegreePlan().PersonId(), self.addingTerm(), ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
                .then(function (data) {
                    self.repositorySuccessHandler(data);
                    self.addingTerm(null);

                    // Make the new term the current term in the model
                    for (var i = 0; i < self.DegreePlan().Terms().length; i++) {
                        if (self.DegreePlan().Terms()[i] !== null && self.DegreePlan().Terms()[i].Code() === data.TermToShow) {
                            self.currentTermCode(self.DegreePlan().Terms()[i].Code());
                            break;
                        }
                    }

                    //focus on the new one in whatever view is active
                    if (self.isShowingSchedule) {

                        self.ShowScheduleTerm(self.currentTermCode());
                    } else {
                        self.ScrollToTerm(true);
                    }
                })
                .catch(function (error) {
                    self.repositoryErrorHandler(error);
                })
                .then(updateCompleted, updateCompleted);
        }
    };

    this.CancelAddTerm = function () {
        self.addTermDialogIsOpen(false);
        self.addingTerm(null);
    };

    // Compute the set of unplanned terms that fall between two given terms (e.g. between Spring 2013 and Fall 2013)
    this.NextUnplannedTerms = function (currentTerm, index) {
        if (typeof self.DegreePlan === "undefined") return false;

        var currentReportingYear = currentTerm.ReportingYear();
        var currentSequence = currentTerm.Sequence();

        return ko.utils.arrayFilter(self.DegreePlan().UnplannedTerms(), function (term) {
            var termReportingYear = term.ReportingYear();
            var termSequence = term.Sequence();

            // IF the currentTerm is "less" than the unplanned term
            if (currentReportingYear < termReportingYear || (currentReportingYear == termReportingYear && currentSequence < termSequence)) {
                if (index() + 1 < self.DegreePlan().Terms().length) {

                    // AND the unplanned term is "less" than the next term
                    var nextTermReportingYear = self.DegreePlan().Terms()[index() + 1].ReportingYear();
                    var nextTermSequence = self.DegreePlan().Terms()[index() + 1].Sequence();
                    if (termReportingYear < nextTermReportingYear || (termReportingYear == nextTermReportingYear && termSequence < nextTermSequence)) {
                        return true;
                    }
                }
                else return true;
            }
            return false;
        });
    };

    // Update course
    this.UpdateCourse = function (options) {
        if (options !== null && !isNullOrEmpty(options.newTerm)) {
            //can skip it if no actual change is occurring
            if (options.oldTerm === options.newTerm) {
                closeCourseDetails();
                return;
            }

            if (typeof stringUpdatingCourseProcessing === 'string') {
                self.LoadingMessage(stringUpdatingCourseProcessing);
            }
            closeCourseDetails();
            self.isLoading(true);

            planRepository.updateCourse(self.DegreePlan().PersonId(), options.courseId, options.oldTerm, options.newTerm, ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
                .then(function (data) {
                    self.degreePlanChanged(true);
                    self.repositorySuccessHandler(data);
                })
                .catch(function (error) {
                    self.repositoryErrorHandler(error);
                })
                .then(updateCompleted, updateCompleted);
        }
    };

    // Add a new advising note
    this.AddAdvisingNoteFromStudent = function (note) {

        if (!isNullOrEmpty(note)) {
            var dp = null;
            try {
                dp = self.DegreePlan().DegreePlanDto;
                if (typeof dp === "undefined" || dp === "") {
                    dp = null;
                }
            } catch (e) {
                dp = null;
            }

            if (typeof stringAddingAdvisingNote === 'string') {
                self.LoadingMessage(stringAddingAdvisingNote);
            }
            self.isLoading(true);

            planRepository.submitNote(self.DegreePlan().PersonId(), note, "STUDENT", ko.mapping.toJSON(dp))
            .then(function (data) {
    ko.mapping.fromJS(data, planMapping, self.DegreePlan);
    $("#advising-notes-compose-box").val("");
    $("#advising-notes-compose-box").focus();
})
            .catch(function (error) {
                self.repositoryErrorHandler(error);
})
            .then(updateCompleted, updateCompleted);
        }
    };

    // Requisite Warning link handler.
    this.RequisiteWarningLinkHandler = function (context) {
        var link = self.searchUrl();
        if (requisiteModel.prototype.isPrototypeOf(context)) {
            link += "?requirement=" + encodeURIComponent(context.RequisiteId()) + "&requirementText=" + encodeURIComponent(context.DisplayText());
        } else if (corequisiteCourseModel.prototype.isPrototypeOf(context)) {
            link += "?CourseIds=" + encodeURIComponent(context.Id()) + "&requirementText=" + encodeURIComponent(context.DisplayMessage());
        } else if (corequisiteSectionModel.prototype.isPrototypeOf(context)) {
            link += "?SectionIds=" + encodeURIComponent(context.SectionIds()) + "&requirementText=" + encodeURIComponent(context.DisplayMessage());
        }

        if (!self.searchAsync()) {
            window.location = link;
            return;
        } else {
            $.ajax({
                url: link,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.catalogResult(data);
                        self.catalogResultsDisplayed(true);
                        $("#advising-content-nav").tabs("option", "selected", 3);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0)
                        $('#notificationHost').notificationCenter('addNotification', {
                            message: "Unable to retrieve search results.", type: "error"
                        });
                },
                complete: function (jqXHR, textStatus) {

                }
            });
        }
        return false;
    }

    this.scrollingTimelineWait = false;
    this.ScrollTerms = function (direction) {
        //ignore subsequent clicks while an animation is currently running; note that 'fast' animation speed is 200ms
        if (self.scrollingTimelineWait === true) {
            return;
        }
        self.scrollingTimelineWait = true;
        setTimeout(function () { self.scrollingTimelineWait = false; }, 205);
        var termWidth = $(".dp-termwrapper").children(".dp-termouter:first-child").outerWidth(true);
        if (direction == 'left') {
            $("#dp-plans").animate({ scrollLeft: $("#dp-plans").scrollLeft() - termWidth }, "fast", self.ToggleScrollButtons);
        } else if (direction == 'right') {
            $("#dp-plans").animate({ scrollLeft: $("#dp-plans").scrollLeft() + termWidth }, "fast", self.ToggleScrollButtons);
        }
    };

    this.LeftScrollEnabled = ko.observable(true);
    this.RightScrollEnabled = ko.observable(true);

    // Check the position of the first and last term, and toggle scroll buttons accordingly
    this.ToggleScrollButtons = function () {
        var leftOn = true;
        var rightOn = true;

        // If there is no scrollbar, all terms are fully visible, so disable the scroll buttons
        if ($("#dp-plans").HasScrollBar() === false) {
            leftOn = false;
            rightOn = false;
        }

        var containerLeft = $("#dp-plans").scrollLeft() + $("#scroll-left").outerWidth(true);
        var containerRight = $("#dp-plans").width() - $("#scroll-right").outerWidth(true);

        // If the first term is fully visible, disable the left scroll
        var firstLeft = $(".dp-termwrapper").children(".dp-termouter:first-child").position().left;
        if (firstLeft >= containerLeft) {
            leftOn = false;
        }

        // If the last term is fully visible, disable the right scroll
        var lastLeft = $(".dp-termwrapper").children(".dp-termouter:last-child").position().left;
        var lastRight = lastLeft + $(".dp-termwrapper").children(".dp-termouter:last-child").width();
        if (lastRight <= containerRight) {
            rightOn = false;
        }

        self.LeftScrollEnabled(leftOn);
        self.RightScrollEnabled(rightOn);
    };

    //---  Start of functions used by Immediate Payment Control  ---//

    // This method does an AJAX call to determine if a payment is required.
    this.CheckIfPaymentRequired = function () {
        // Determine whether student needs to pay for a registration.
        if (paymentRequiredActionUrl !== "" && self.isProxyUser() === false) {
            $.ajax({
                url: paymentRequiredActionUrl,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        if (data.length > 0) {
                            self.IsRegistrationPaymentRequired(true);
                        } else {
                            self.IsRegistrationPaymentRequired(false);
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // If this fails for any reason, payments are not required
                    self.IsRegistrationPaymentRequired(false);
                }
            });
        } else {
            // Either Student Finance isn't available, or this is a proxy user. In either
            // case, payment is not required.
            self.IsRegistrationPaymentRequired(false);
        }
    };

    // This function is invoked when the student clicks the "Pay for Registration" button.
    // We capture info so we can later return the student to where they were.
    this.ImmediatePayment = function () {
        // Transfer control to the Immediate Payment workflow
        if (immediatePaymentActionUrl) {
            // Get the current term and current view
            var term = self.currentTermCode();
            var currentTab = $("#schedplan-tabs").tabs("option", "selected") || 0;
            var url = window.location.href;
            var model = {
                "ReturnUrl": url, "TermId": term, "SelectedTab": currentTab
            };
            ko.utils.postJson(immediatePaymentActionUrl, {
                model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            });
        }
    };

    //---  End of functions used by Immediate Payment Control  ---//

    // Toggle filters showing via clicking button
    // data is the current term object
    this.toggleSectionFilters = function (data, event) {
        // If the available sections haven't been loaded, and there are planned courses get them now...
        if (!data.availableSectionsRetrieved() && data.PlannedCourses().length > 0) {
            self.getAvailableSections();
        }

        data.showingFilters(!data.showingFilters());
    };

    // Handle any click events on availability filters
    // Lives at the root (plan) level instead of down at the term or filter
    // because it needs access to the getSectionAvailability method
    this.filterClickHandler = function (data, event) {
        data.Selected(!data.Selected());
        self.ResetToNoCoursesOpen();
        self.getAvailableSections();
    };

    // CLEARING planned courses from a plan 

    // NOTE REUSING THE removingCourseTerm AND removingCourseTermTitle OBSERVABLES

    // This is an associated arrays of clearable term ids, descriptions, and a boolean that indicates if they were selected for action this transaction
    this.clearDialogTerms = ko.observableArray();

    // This is just a boolean that determines if there are any values in the clearDialogTerms array that have selected set
    this.clearTermSelected = ko.computed(function () {
        if (typeof self.clearDialogTerms() === "undefined" || self.clearDialogTerms().length === 0) return false;
        for (var i = 0; i < self.clearDialogTerms().length; i++) {
            if (self.clearDialogTerms()[i].selected()) {
                return true;
            }
        }
        return false;
    });

    this.clearPlannedTermDialogIsOpen = ko.observable(false);

    

    // Show for the single term clear (on schedule)
    this.ShowClearPlannedTermDialog = function () {

        self.removingCourseTerm(self.currentTermCode());
        self.removingCourseTermTitle(self.currentTerm().Description());
        self.clearDialogTerms.push({
            termCode: self.currentTermCode(), description: self.currentTerm().Description(), selected: ko.observable(true)
        });
        self.clearPlannedTermDialogIsOpen(true);
    };


    // Show for the multiple term clear (on timeline)
    this.ShowClearPlannedTermsDialog = function () {
        if (self.DegreePlan().ClearableTerms().length == 1) {
            self.removingCourseTerm(self.DegreePlan().ClearableTerms()[0].Code());
            self.removingCourseTermTitle(self.DegreePlan().ClearableTerms()[0].Description())
        }
        for (var i = 0; i < self.DegreePlan().ClearableTerms().length; i++) {
            var term = self.DegreePlan().ClearableTerms()[i];
            self.clearDialogTerms.push({
                termCode: self.DegreePlan().ClearableTerms()[i].Code(), description: self.DegreePlan().ClearableTerms()[i].Description(), selected: ko.observable(true)
            });
        }

        self.clearPlannedTermDialogIsOpen(true);
    };

    // Cancel clear planned terms dialogs
    this.CancelClearPlannedTerms = function () {
        self.clearPlannedTermDialogIsOpen(false);
        self.removingCourseTerm(null);
        self.removingCourseTermTitle("");
        self.clearDialogTerms([]);
    };

    // Commit clear planned courses removal
    this.CommitClearPlannedCourses = function () {
        var clearTermList = [];
        var clearTerms = self.clearDialogTerms();
        for (var i = 0; i < clearTerms.length; i++) {
            var term = clearTerms[i];
            if (clearTerms[i].selected()) {
                clearTermList.push(clearTerms[i].termCode);
            }
        }
        // Reset the clear dialog term observable array.
        self.clearDialogTerms([]);
        self.ClearPlannedCourses(clearTermList);
        self.clearPlannedTermDialogIsOpen(false);
    };

    // Process the clear
    this.ClearPlannedCourses = function (clearTermList) {
        if (clearTermList.length > 0) {

            if (typeof stringClearPlannedCourseProcessing === 'string') {
                self.LoadingMessage(stringClearPlannedCourseProcessing);
            }
            self.isLoading(true);
            self.clearPlannedTermDialogIsOpen(false);

            planRepository.clearPlan(self.DegreePlan().PersonId(), clearTermList, ko.mapping.toJSON(ko.utils.unwrapObservable(self.DegreePlan).DegreePlanDto))
                .then(function (data) {
                    self.degreePlanChanged(true);
                    self.repositorySuccessHandler(data);
                })
                .catch(function (error) {
                    self.repositoryErrorHandler(error);
                })
                .then(clearPlannedCoursesComplete, clearPlannedCoursesComplete);
        }
    };

    var clearPlannedCoursesComplete = function () {
        self.removingCourseTerm(null);
        self.removingCourseTermTitle("");
        updateCompleted();
    };

    this.RefreshRegistrationEligibility = function (studentId) {
        // On initial page load, the degree plan might not yet be loaded, so allow the caller to pass in the student's ID
        var url = registrationEligibilityActionUrl, id;
        if (studentId != null) {
            url += "?id=" + studentId;
        }
        else {
            // Use the person assigned to the plan, if available, otherwise leave the id null 
            url += typeof self.DegreePlan() !== 'undefined' ? "?id=" + self.DegreePlan().PersonId() : "";
        }
        $.ajax({
            url: url,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                if (typeof refreshingRegistrationEligibility === 'string') {
                    self.LoadingMessage(refreshingRegistrationEligibility);
                }
                self.isLoading(true);
                self.termEligibility.removeAll();
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    if (data.Notifications.length > 0) {
                        $('#notificationHost').notificationCenter('addNotification', data.Notifications);
                    }

                    for (var i = 0; i < data.Terms.length; i++) {
                        self.termEligibility.push(data.Terms[i]);
                    }
                    self.hasRegistrationOverride(data.HasRegistrationOverride);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', {
                        message: "Unable to determine registration eligibility", type: "error"
                    });
                }
            },
            complete: updateCompleted
        });
    }
    this.studentPetitionsWaiversInstance = new StudentPetitionsWaiversViewModel();

    this.repositorySuccessHandler = function (data) {

        // save current term, reapply it after re-mapping model
        var savedCurrentTermCode = self.currentTermCode();

        // If currentTermCode isn't in the terms passed back in data, then use TermToShow instead
        // This can happen if a student drops all courses in a term and that term was never added to the plan (ie, the add was via Colleague UI)
        var currentTermMissing = true;
        for (var i = 0; i < data.Terms.length; i++) {
            if (savedCurrentTermCode === data.Terms[i].Code) currentTermMissing = false;
        }
        if (currentTermMissing === true) {
            savedCurrentTermCode = data.TermToShow;
            self.currentTermCode(savedCurrentTermCode);
        }

        ko.mapping.fromJS(data, planMapping, self.DegreePlan);

        // force the display to mobile or desktop. The re-mapping of the DegreePlan above forces the DOM back to initial (desktop) state.
        self.isMobile.valueHasMutated();

        // reset the notification center, then repopulate with new messages
        setTimeout(function () {
            $('#notificationHost').notificationCenter('reset');
            if (self.DegreePlan().Notifications().length > 0) {
                for (var i = 0; i < self.DegreePlan().Notifications().length; i++) {
                    var notification = ko.toJS(self.DegreePlan().Notifications()[i]);
                    $('#notificationHost').notificationCenter('addNotification', notification);
                }
            }
        }, 350);

        //if the current term was the one that was deleted then don't try to triger an update, as that term is not in the list
        //CommitRemoveTerm will handle setting the 'new' term in that case
        var newTerm = ko.utils.arrayFirst(self.DegreePlan().Terms(), function (term) {
            return term.Code() == savedCurrentTermCode;
        });

        if (typeof newTerm !== "undefined" && newTerm !== null) {
            self.currentTermCode(newTerm.Code());
        }

        self.updateEvaluations(true);
        self.updatePlanningTerms(self.DegreePlan().AvailableSamplePlanTerms());
    };


    // Handle errors that occur in the repository
    this.repositoryErrorHandler = function (error) {
        // Check if the error is caused by stale data
        if (error.hasOwnProperty("isStale") && error.isStale === true) {
            updateFailedStaleData(error.message);
        }
        // If not stale, check for a message property on error (if not there, just use the whole error)
        var message = error.hasOwnProperty("message") ? error.message : error;
        $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
    };

    // Button objects for modal dialogs
    this.addTermButton = {
        id: 'schedplan-addtermformaddbutton',
        title: Ellucian.DegreePlans.Resources.UiAddTermPopupButtonText,
        isPrimary: true,
        callback: self.CommitAddTerm
    };

    this.addTermCancelButton = {
        id: 'schedplan-addtermformcancelbutton',
        title: Ellucian.DegreePlans.Resources.UiAddTermCancelButtonText,
        isPrimary: false,
        callback: self.CancelAddTerm
    };

    this.clearTermButton = {
        id: 'dp-clearterm-remove',
        title: Ellucian.DegreePlans.Resources.RemoveItemButtonText,
        isPrimary: true,
        callback: self.CommitClearPlannedCourses
    };

    this.clearTermCancelButton = {
        id: 'dp-clearterm-cancel',
        title: Ellucian.DegreePlans.Resources.RemoveItemCancelButtonText,
        enabled: self.clearTermSelected,
        isPrimary: false,
        callback: self.CancelClearPlannedTerms
    };

    this.dropSectionButton = {
        id: 'dp-dropsection-commit',
        title: Ellucian.DegreePlans.Resources.DropDialogDrop,
        isPrimary: true,
        callback: self.CommitDrop
    };

    this.dropSectionCancelButton = {
        id: 'dp-dropsection-cancel',
        title: Ellucian.DegreePlans.Resources.DropDialogCancel,
        isPrimary: false,
        callback: self.CancelDropSection
    };

    this.removeCourseRemoveButton = {
        id: 'schedplan-removecourse-remove',
        title: Ellucian.DegreePlans.Resources.RemoveItemButtonText,
        isPrimary: true,
        callback: self.commitRemoveCourse
    };

    this.removeCourseCancelButton = {
        id: 'schedplan-removecourse-cancel',
        title: Ellucian.DegreePlans.Resources.RemoveItemCancelButtonText,
        isPrimary: false,
        callback: self.CancelRemoveCourse
    };

    this.removeTermButton = {
        id: 'dp-removeterm-remove',
        title: Ellucian.DegreePlans.Resources.RemoveItemButtonText,
        isPrimary: true,
        callback: self.CommitRemoveTerm
    };

    this.removeTermCancelButton = {
        id: 'dp-removeterm-cancel',
        title: Ellucian.DegreePlans.Resources.RemoveItemCancelButtonText,
        isPrimary: false,
        callback: self.CancelRemoveTerm
    };
}

$.fn.HasScrollBar = function () {
    //note: clientHeight= height of holder
    //scrollHeight= we have content till this height
    var _elm = $(this)[0];
    if (!_elm) return false;
    var _hasScrollBar = false;
    if ((_elm.clientHeight < _elm.scrollHeight) || (_elm.clientWidth < _elm.scrollWidth)) {
        _hasScrollBar = true;
    }
    return _hasScrollBar;
}
