﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.



//view model for faculty section header
function facultyHeaderModel() {
    var self = this;
    this.FormattedNameDisplay = ko.observable("");
    this.TermName = ko.observable("");
    this.Location = ko.observable("");
    this.Meetings = ko.observableArray();
    this.SectionId = ko.observable("");
    this.retrieved = ko.observable(false);
}

//view model when permission tab is selected
function facultyPermissionModel() {
    var self = this;

    this.FormattedNameDisplay = ko.observable("");
    this.TermName = ko.observable("");

    //to control hide and show
    this.showWaiverContents = ko.observable(false);
    this.isAnyPermissionSelected = ko.observable(false);
    this.showPetitionContents = ko.observable(false);
    this.SectionId = ko.observable("");
    this.isWaiverRetrieved = ko.observable(false);
    this.isPetitionsRetrieved = ko.observable(false);
    this.isPermissionsTabSelected = ko.observable(false);
    this.isGradingTabSelected = ko.observable(false);

    //waiver details
    this.CourseCorequisites = ko.observableArray();
    this.CoursePrerequisites = ko.observableArray();
    this.WaiverReasons = ko.observableArray();
    this.StudentWaivers = ko.observableArray();
    this.hasWaivers = ko.computed(function () {
        return (self.StudentWaivers().length > 0)
    });
    this.hasPrerequisites = ko.computed(function () {
        return (self.CoursePrerequisites().length > 0)
    });
    this.hasCorequisites = ko.computed(function () {
        return (self.CourseCorequisites().length > 0)
    });
    this.CanFacultyAddWaivers = ko.observable(false);

    //computed to sort the student waivers by student name.
    this.sortedWaivers = ko.computed(function () {
        return self.StudentWaivers().sort(function (l, r) { return l.DisplayNameLfm() > r.DisplayNameLfm() ? 1 : -1 })
    });

    // Add Waiver Dialog Actions
    this.IsLoading = ko.observable(false);
    this.LoadingMessage = ko.observable("");
    // Items on the Dialog
    this.studentIdToWaive = ko.observable("");
    this.waivedStudent = ko.observable(null);
    this.studentValidationMessage = ko.observable("");
    this.waiverExplanation = ko.observable("");
    this.isWaived = ko.observable(false);
    this.waiverReason = ko.observable("");
    this.addWaiverError = ko.observable("");
    this.waiverIsApproved = ko.observable(true);

    this.addWaiverDialogIsOpen = ko.observable(false);
    this.addWaiverDialogOptions = {
        autoOpen: false,
        modal: true,
        width: $(window).width() / 2,
        resizable: true,
        show: { effect: "fade", duration: dialogFadeTimeMs },
        hide: { effect: "fade", duration: dialogFadeTimeMs }
    };
    this.openAddWaiverDialog = function () {
        self.addWaiverDialogIsOpen(true);
    };
    this.cancelAddWaiverDialog = function () {
        self.studentIdToWaive("");
        self.waiverExplanation("");
        self.isWaived(false);
        self.waiverReason("");
        self.addWaiverError("");
        self.addWaiverDialogIsOpen(false);
        self.waivedStudent(null);
        self.studentValidationMessage("");
    };

    this.validateStudentId = function () {
        if (self.studentIdToWaive() != '') {
            $.ajax({
                url: validateWaivedStudentUrl + "?studentId=" + self.studentIdToWaive() + "&sectionId=" + facultyPermissionInstance.SectionId(),
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {

                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.waivedStudent(ko.mapping.fromJS(data, {}));
                        self.studentValidationMessage("");
                        // Be sure to reset focus before setting waivedStudent since that will hid the div that Jaws is currently on.
                        if (self.WaiverReasons().length !== 0) {
                            $("#addReasonId").focus();
                        } else {
                            $("#waiver-explanation").focus();
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Set validation message depending on the result
                    if (jqXHR.status === 409) {
                        self.studentValidationMessage(stringStudentHasWaiverMessage);
                    }
                    else if (jqXHR.status != 0) {
                        // Set validation message depending on the result
                        self.studentValidationMessage(stringInvalidStudentMessage);
                    }
                    $("#student-id").focus();
                }
            });
        } else {
            self.studentValidationMessage(stringInvalidStudentMessage);
            $("#student-id").focus();
        }
    };

    this.waiverIsApproved.ForEditing = ko.computed({
        read: function () {
            return this.waiverIsApproved().toString();
        },
        write: function (newValue) {
            this.waiverIsApproved(newValue === "true");
        },
        owner: this
    });

    this.approveOrDeny = function () {
        if (self.waiverIsApproved() === true) {
            self.waivePrerequisites();
        }
        else {
            self.denyWaiver();
        }
    };

    this.waivePrerequisites = function () {
        if ((self.waiverReason() != undefined && self.waiverReason() != '') || (self.waiverExplanation() != undefined && self.waiverExplanation() != '')) {
            self.isWaived(true);
            self.submitWaiver();
        } else {
            self.addWaiverError(stringWaiverReasonError);
            $("#waiver-explanation").focus();
        }
    };
    this.denyWaiver = function () {
        if ((self.waiverReason() != undefined && self.waiverReason() != '') || (self.waiverExplanation() != undefined && self.waiverExplanation() != '')) {
            self.isWaived(false);
            self.submitWaiver();
        } else {
            self.addWaiverError(stringWaiverReasonError);
            $("#waiver-explanation").focus();
        }
    };
    this.submitWaiver = function () {
        if (self.studentIdToWaive() != '') {
            var isWaived = self.isWaived() ? true : false;
            var prerequisitesJson = ko.toJSON(facultyPermissionInstance.CoursePrerequisites());
            var jsonData = { sectionId: facultyPermissionInstance.SectionId(), studentId: self.studentIdToWaive(), waiverExplanation: self.waiverExplanation(), waiverReason: self.waiverReason(), isWaived: isWaived, prerequisiteModels: prerequisitesJson };
            $.ajax({
                url: addWaiverActionUrl,
                data: JSON.stringify(jsonData),
                type: "POST",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {
                    if (typeof stringAddingWaiverProcessing === 'string') {
                       self.LoadingMessage(stringAddingWaiverProcessing);
                    }
                    self.IsLoading(true);
                    self.addWaiverDialogIsOpen(false);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.StudentWaivers.push(ko.mapping.fromJS(data, {}));
                        $("#student-waiver-table").makeTableResponsive();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.textStatus != 0) {
                        // Add error notification
                        $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToAddWaiverError, type: "error" });
                    }
                },
                complete: self.waiveAjaxCompleteHandler()
            });
        }
    };

    this.waiveAjaxCompleteHandler = function () {
        setTimeout(function () { self.IsLoading(false) }, 200);
        self.studentIdToWaive("");
        self.waiverExplanation("");
        self.waiverReason("");
        self.isWaived(false);
        self.addWaiverError("");
        self.waivedStudent(null);
        self.studentValidationMessage("");
    };

    //petition details
    this.hasPetitions = ko.observable(false);
    this.hasConsents = ko.observable(false);
    this.FacultyConsents = ko.observableArray();
    this.StudentPetitions = ko.observableArray();
    this.PetitionStatuses = ko.observableArray();
    this.PetitionReasons = ko.observableArray();

    this.hasPetitions = ko.computed(function () {
        return (self.StudentPetitions().length > 0)
    });

    this.hasConsents = ko.computed(function () {
        return (self.FacultyConsents().length > 0)
    });

    this.CanFacultyAddStudentPetitions = ko.observable(false);
    this.CanFacultyAddFacultyConsents = ko.observable(false);

    $("#student-waiver-table").makeTableResponsive();
    $("#student-petition-table").makeTableResponsive();
    $("#faculty-consent-table").makeTableResponsive();


    // Add Student Petition Dialog Actions
    this.IsLoading = ko.observable(false);
    // Items on the Dialog
    this.studentIdToPetition = ko.observable("");
    this.petitionStudent = ko.observable(null);
    this.studentValidationMessage = ko.observable("");
    this.petitionComment = ko.observable("");
    this.petitionStatus = ko.observable("");
    this.petitionReason = ko.observable("");
    this.addPetitionStatusError = ko.observable("");
    this.addPetitionReasonError = ko.observable("");


    this.addPetitionDialogIsOpen = ko.observable(false);
    this.addPetitionDialogOptions = {
        autoOpen: false,
        modal: true,
        width: $(window).width() / 2,
        resizable: true,
        show: { effect: "fade", duration: dialogFadeTimeMs },
        hide: { effect: "fade", duration: dialogFadeTimeMs }
    };
    this.openAddPetitionDialog = function () {
        self.addPetitionDialogIsOpen(true);
    };
    this.cancelAddPetitionDialog = function () {
        self.studentIdToPetition("");
        self.petitionComment("");
        self.petitionStatus("");
        self.petitionReason("");
        self.addPetitionStatusError("");
        self.addPetitionReasonError("");
        self.addPetitionDialogIsOpen(false);
        self.petitionStudent(null);
        self.studentValidationMessage("");
    };

    this.validatePetitionStudentId = function () {
        if (self.studentIdToPetition() != '') {
            $.ajax({
                url: validatePetitionStudentUrl + "?studentId=" + self.studentIdToPetition() + "&sectionId=" + facultyPermissionInstance.SectionId(),
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {

                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.petitionStudent(ko.mapping.fromJS(data, {}));
                        self.studentValidationMessage("");
                        // Be sure to reset focus before setting petitionStudent since that will hide the div that Jaws is currently on.
                        $("#addPetitionStatusId").focus();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Set validation message depending on the result
                    if (jqXHR.status === 409) {
                        self.studentValidationMessage(stringStudentHasStudentPetitionMessage);
                    }
                    else if (jqXHR.status != 0) {
                        // Set validation message depending on the result
                        self.studentValidationMessage(stringInvalidStudentMessage);
                    }
                    $("#student-id").focus();
                }
            });
        } else {
            self.studentValidationMessage(stringInvalidStudentMessage);
            $("#student-id").focus();
        }
    };

    this.savePetition = function () {
        self.addPetitionStatusError("");
        self.addPetitionReasonError("");
        if (self.petitionStatus() === undefined || self.petitionStatus() === '') {
            self.addPetitionStatusError("Must enter a petition status.");
            $("#addPetitionStatusId").focus();
        }
        else if ((self.petitionReason() != undefined && self.petitionReason() != '') || (self.petitionComment() != undefined && self.petitionComment() != '')) {
            self.submitPetition();
        } else {
            self.addPetitionReasonError("Must enter either a reason or a comment.");
            $("#petition-comment").focus();
        }
    };
    this.submitPetition = function () {
        if (self.studentIdToPetition() != '') {
            var jsonData = { petitionType: 'StudentPetition', sectionId: facultyPermissionInstance.SectionId(), studentId: self.studentIdToPetition(), statusCode: self.petitionStatus(), reasonCode: self.petitionReason(), comment: self.petitionComment() };
            $.ajax({
                url: addSectionPermissionsActionUrl,
                data: JSON.stringify(jsonData),
                type: "POST",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {
                    if (typeof stringAddingPetitionProcessing === 'string') {
                        self.LoadingMessage(stringAddingPetitionProcessing);
                    }
                    self.IsLoading(true);
                    self.addPetitionDialogIsOpen(false);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.StudentPetitions.push(ko.mapping.fromJS(data, {}));
                        $("#student-petition-table").makeTableResponsive();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.textStatus != 0) {
                        // Add error notification
                        $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToAddPetitionError, type: "error" });
                    }
                },
                complete: self.petitionAjaxCompleteHandler()
            });
        }
    };

    this.petitionAjaxCompleteHandler = function () {
        setTimeout(function () { self.IsLoading(false) }, 200);
        self.studentIdToPetition("");
        self.petitionComment("");
        self.petitionReason("");
        self.petitionStatus("");
        self.addPetitionStatusError("");
        self.addPetitionReasonError("");
        self.petitionStudent(null);
        self.studentValidationMessage("");
    };


    // Add Faculty Consent Dialog Actions
    this.IsLoading = ko.observable(false);
    // Items on the Dialog
    this.studentIdToConsent = ko.observable("");
    this.consentStudent = ko.observable(null);
    this.studentValidationMessage = ko.observable("");
    this.consentComment = ko.observable("");
    this.consentStatus = ko.observable("");
    this.consentReason = ko.observable("");
    this.addConsentStatusError = ko.observable("");
    this.addConsentReasonError = ko.observable("");

    this.addConsentDialogIsOpen = ko.observable(false);
    this.addConsentDialogOptions = {
        autoOpen: false,
        modal: true,
        width: $(window).width() / 2,
        resizable: true,
        show: { effect: "fade", duration: dialogFadeTimeMs },
        hide: { effect: "fade", duration: dialogFadeTimeMs }
    };
    this.openAddConsentDialog = function () {
        self.addConsentDialogIsOpen(true);
    };
    this.cancelAddConsentDialog = function () {
        self.studentIdToConsent("");
        self.consentComment("");
        self.consentStatus("");
        self.consentReason("");
        self.addConsentStatusError("");
        self.addConsentReasonError("");
        self.addConsentDialogIsOpen(false);
        self.consentStudent(null);
        self.studentValidationMessage("");
    };

    this.validateConsentStudentId = function () {
        if (self.studentIdToConsent() != '') {
            $.ajax({
                url: validateConsentStudentUrl + "?studentId=" + self.studentIdToConsent() + "&sectionId=" + facultyPermissionInstance.SectionId(),
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {

                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.consentStudent(ko.mapping.fromJS(data, {}));
                        self.studentValidationMessage("");
                        // Be sure to reset focus before setting consentStudent since that will hide the div that Jaws is currently on.
                        $("#addConsentStatusId").focus();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Set validation message depending on the result
                    if (jqXHR.status === 409) {
                        self.studentValidationMessage(stringStudentHasFacultyConsentMessage);
                    }
                    else if (jqXHR.status != 0) {
                        // Set validation message depending on the result
                        self.studentValidationMessage(stringInvalidStudentMessage);
                    }
                    $("#student-id").focus();
                }
            });
        } else {
            self.studentValidationMessage(stringInvalidStudentMessage);
            $("#student-id").focus();
        }
    };

    this.saveConsent = function () {
        self.addConsentStatusError("");
        self.addConsentReasonError("");
        if (self.consentStatus() === undefined || self.consentStatus() === '') {
            self.addConsentStatusError("Must enter a status.");
            $("#addConsentStatusId").focus();
        }
        else if ((self.consentReason() != undefined && self.consentReason() != '') || (self.consentComment() != undefined && self.consentComment() != '')) {
            self.submitConsent();
        } else {
            self.addConsentReasonError("Must enter either a reason or a comment.");
            $("#consent-comment").focus();
        }
    };
    this.submitConsent = function () {
        if (self.studentIdToConsent() != '') {
            var jsonData = { petitionType: 'FacultyConsent', sectionId: facultyPermissionInstance.SectionId(), studentId: self.studentIdToConsent(), statusCode: self.consentStatus(), reasonCode: self.consentReason(), comment: self.consentComment() };
            $.ajax({
                url: addSectionPermissionsActionUrl,
                data: JSON.stringify(jsonData),
                type: "POST",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {
                    if (typeof stringAddingConsentProcessing === 'string') {
                        self.LoadingMessage(stringAddingConsentProcessing);
                    }
                    self.IsLoading(true);
                    self.addConsentDialogIsOpen(false);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.FacultyConsents.push(ko.mapping.fromJS(data, {}));
                        $("#faculty-consent-table").makeTableResponsive();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.textStatus != 0) {
                        // Add error notification
                        $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToAddConsentError, type: "error" });
                    }
                },
                complete: self.consentAjaxCompleteHandler()
            });
        }
    };

    this.consentAjaxCompleteHandler = function () {
        setTimeout(function () { self.IsLoading(false) }, 200);
        self.studentIdToConsent("");
        self.consentComment("");
        self.consentReason("");
        self.consentStatus("");
        self.addConsentStatusError("");
        self.addConsentReasonError("");
        self.consentStudent(null);
        self.studentValidationMessage("");
    };

    // Computed observables to enable/disable save buttons on faculty modal dialogs
    this.addConsentSaveButtonEnabled = ko.pureComputed(function () {
        return !(typeof self.consentStudent === 'undefined' || self.consentStudent() == null);
    });

    this.addPetitionSaveButtonEnabled = ko.pureComputed(function () {
        return !(typeof self.petitionStudent === 'undefined' || self.petitionStudent() == null);
    });

    this.addWaiverDenyButtonEnabled = ko.pureComputed(function () {
        return !(typeof self.waivedStudent === 'undefined' || self.waivedStudent() == null);
    });

    // Button objects for modal dialogs
    this.addConsentSaveButton = {
        id: 'addconsentsavebutton',
        title: Ellucian.Faculty.Resources.AddConsentSaveButtonText,
        isPrimary: true,
        enabled: self.addConsentSaveButtonEnabled,
        aria: Ellucian.Faculty.Resources.AddConsentSaveButtonAriaLabel,
        callback: self.saveConsent
    };

    this.addConsentCancelButton = {
        id: 'addconsentcancelbutton',
        title: Ellucian.Faculty.Resources.AddConsentCancelButtonText,
        isPrimary: false,
        callback: self.cancelAddConsentDialog
    };

    this.addPetitionSaveButton = {
        id: 'addpetitionsavebutton',
        title: Ellucian.Faculty.Resources.AddPetitionSaveButtonText,
        isPrimary: true,
        enabled: self.addPetitionSaveButtonEnabled,
        aria: Ellucian.Faculty.Resources.AddPetitionSaveButtonAriaLabel,
        callback: self.savePetition
    };

    this.addPetitionCancelButton = {
        id: 'addpetitioncancelbutton',
        title: Ellucian.Faculty.Resources.AddPetitionCancelButtonText,
        isPrimary: false,
        callback: self.cancelAddPetitionDialog
    };

    this.addWaiverDenybutton = {
        id: 'addwaiverdenybutton',
        title: Ellucian.Faculty.Resources.AddWaiverSaveButtonText,
        isPrimary: true,
        enabled: self.addWaiverDenyButtonEnabled,
        aria: Ellucian.Faculty.Resources.AddWaiverSaveButtonAriaLabel,
        callback: self.approveOrDeny
    };

    this.addWaiverCancelButton = {
        id: 'addwaivercancelbutton',
        title: Ellucian.Faculty.Resources.AddWaiverCancelButtonText,
        isPrimary: false,
        callback: self.cancelAddWaiverDialog
    };
}

function facultyRosterModel() {
    var self = this;
    this.PreferredEmailAddress = ko.observable("");
    this.RosterStudents = ko.observableArray();
    this.hasStudents = ko.observable(false);
    this.retrieved = ko.observable(false);
    this.hasStudents = ko.computed(function () {
        return (self.RosterStudents().length > 0)
    });
    this.emailToAllTheStudents = ko.pureComputed(function () {
        var bccAddresses = "bcc=";
        ko.utils.arrayForEach(self.RosterStudents(), function (item) {
            var value = item.PreferredEmail();
            if (!isNullOrEmpty(value)) {
                bccAddresses += value + ";";
            }
        });
        var facultyEmail = self.PreferredEmailAddress();
        var emails = facultyEmail ? facultyEmail : "";
        emails = emails + "?" + bccAddresses;
            return emails;
    }).extend({ deferred: true });
}

function isRetrievalCompleted() {

    if (facultyPermissionInstance.isWaiverRetrieved() === true && facultyHeaderInstance.retrieved() === true && facultyPermissionInstance.isPetitionsRetrieved() === true && facultyRosterInstance.retrieved() === true && facultyGradingNavigationInstance.areGradesRetrieved() === true)
    { return true; }
    else
    { return false; }

};

function spinnerModel() {
    var self = this;
    this.isLoading = ko.observable(true);
    this.isPageLoaded = ko.computed(function () {
        if (self.isLoading() === false) {
            $('#faculty-content-nav').show();
            $('#section-wrapper-id').addClass('section-wrapper');
        }

    });
};

//instances of models
var facultyHeaderInstance = new facultyHeaderModel();
var facultyPermissionInstance = new facultyPermissionModel();
var facultyGradingNavigationInstance= new  facultyGradingNavigationModel();
var facultyRosterInstance = new facultyRosterModel();
var spinnerInstance = new spinnerModel();



function retrieveStudentGrades()
{
    $.ajax({
        url: facultySectionGradesUrl + "?sectionId=" + sectionId,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                facultyGradingNavigationInstance.fromJS(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToRetrieveSection, type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
           
            self.checkForMobile(window, document);
            //// apply bindings for grading
            ko.applyBindings(facultyGradingNavigationInstance, document.getElementById("grading-content-nav"));
            facultyGradingNavigationInstance.displayGradeOverview(true);
            facultyGradingNavigationInstance.displayFinalGrades(false);
            facultyGradingNavigationInstance.displayMidterm1Grades(false);
            facultyGradingNavigationInstance.displayMidterm2Grades(false);
            facultyGradingNavigationInstance.displayMidterm3Grades(false);
            facultyGradingNavigationInstance.displayMidterm4Grades(false);
            facultyGradingNavigationInstance.displayMidterm5Grades(false);
            facultyGradingNavigationInstance.displayMidterm6Grades(false);
            $("#faculty-overview-grading-table").makeTableResponsive();
            $("#faculty-final-grading-table").makeTableResponsive();
            $(".faculty-midterm-grading-table").makeTableResponsive();
            facultyGradingNavigationInstance.registerEventHandlers();

            if (isRetrievalCompleted()) {
                spinnerInstance.isLoading(false);
            }

        }
    });
}

$(document).ready(function () {

    //apply bindings for navigation
    ko.applyBindings(facultyHeaderInstance, document.getElementById("user-profile-header"));
    //apply bindings for waivers and petitions
    ko.applyBindings(facultyPermissionInstance, document.getElementById("faculty-permissions-nav"));
    // apply bindings for roster
    ko.applyBindings(facultyRosterInstance, document.getElementById("roster-content-nav"))
    // apply bindings for page loading spinner
    ko.applyBindings(spinnerInstance, document.getElementById("spinner"));

    //activate tabs
    $("#faculty-content-nav").tabs({
        create: function (event, ui) {
            //retrieve all the permission data
            //make ajax call to retrieve waivers
            $.ajax({
                
                url: facultySectionActionUrl + "?sectionId=" + sectionId,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        ko.mapping.fromJS(data, {}, facultyPermissionInstance);
                        facultyPermissionInstance.isWaiverRetrieved(true);
                        facultyPermissionInstance.FormattedNameDisplay(facultyHeaderInstance.FormattedNameDisplay());
                        facultyPermissionInstance.TermName(facultyHeaderInstance.TermName());

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToRetrieveSection, type: "error" });
                    }
                },
                complete: function (jqXHR, textStatus) {
                    $("#student-waiver-table").makeTableResponsive();
                    if (isRetrievalCompleted()) {
                        spinnerInstance.isLoading(false);
                    }
                }
            });

            //make ajax call to retrieve  petitions
            $.ajax({
                url: facultySectionPetitionsUrl + "?sectionId=" + sectionId,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        ko.mapping.fromJS(data, {}, facultyPermissionInstance);
                        facultyPermissionInstance.isPetitionsRetrieved(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToRetrieveSection, type: "error" });
                    }
                },
                complete: function (jqXHR, textStatus) {
                    $("#student-petition-table").makeTableResponsive();
                    $("#faculty-consent-table").makeTableResponsive();
                    if (isRetrievalCompleted()) {
                        spinnerInstance.isLoading(false);
                    }
                }
            });

            //ajax call to retrieve roster students
            $.ajax({
                url: facultySectionRosterUrl + "?sectionId=" + sectionId,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        ko.mapping.fromJS(data, {}, facultyRosterInstance);
                        facultyRosterInstance.retrieved(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToRetrieveSection, type: "error" });
                    }
                },
                complete: function (jqXHR, textStatus) {
                    $("#faculty-roster-table").makeTableResponsive();
                    if (isRetrievalCompleted()) {
                        spinnerInstance.isLoading(false);
                    }
                }
            });

            //ajax call to retrieve student grades
            retrieveStudentGrades();
        }
    });

    


    //make ajax call to retrieve faculty header section
    $.ajax({
        url: facultySectionHeaderActionUrl + "?sectionId=" + sectionId,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, {}, facultyHeaderInstance);
                facultyHeaderInstance.retrieved(true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToRetrieveSection, type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
            if (isRetrievalCompleted()) {
                spinnerInstance.isLoading(false);
            }
    }
    });

});