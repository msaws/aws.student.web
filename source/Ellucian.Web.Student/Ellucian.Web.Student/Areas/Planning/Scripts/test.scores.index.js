﻿//Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
//viewmodels and mappings for the test score components are defined in test.score.view.model.js

var testScoreInstance = new testResultViewModel();
var studentId = "";
var throbber = null;

$(document).ready(function () {

    // bind testScoreInstance to the view of the test scores
    ko.applyBindings(testScoreInstance, document.getElementById("main"));


    $.ajax({
        url: testScoresActionUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, testScoreMapping, testScoreInstance);
                testScoreInstance.isLoaded(true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve test scores", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {

        }
    });


});