﻿//Copyright 2012-2017 Ellucian Company L.P. and its affiliates
var planRepository = Ellucian.Planning.planRepository;
var requirementsRepository = Ellucian.Planning.requirementsRepository;

var planViewModelInstance = new Ellucian.Planning.PlanViewModel(true, Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl, planRepository); // set isProxy parameter to true (it's not really, but home page needs to disable edits)

planViewModelInstance.showEventTitleAsLinks(false);
var programRequirementsViewModelInstance = new programRequirementsViewModel();

function spinnerModel() {
    var self = this;
    this.isLoading = ko.observable(true);
}
spinner = new spinnerModel();

$(document).ready(function () {

    // Add some event handlers so a Program Description link click doesn't bubble up to the At a Glance div
    $('#home-at-a-glance').on('click', 'a', function (e) {
        e.stopPropagation();
    });

    // Add click event handler to navigate from At-A-Glance to My Progress
    $('#home-at-a-glance').click(function () {
        window.location = loadMyProgressUrl;
    });

    // Add click event handler to navigate from calendar to Plan
    $('#home-calendar').click(function () {
        window.location = loadPlanUrl;
    });

    // Start the spinner
    ko.applyBindings(spinner, document.getElementById("spinner"));

    // bind something to the course search form, so the watermark binding can function
    ko.applyBindings({}, document.getElementById("search-form"));

    // Get the student's program so we can render the program overview
    ko.applyBindings(programRequirementsViewModelInstance, document.getElementById("home-at-a-glance"));

    // "RegistrationMode" is set in web.config and reduces the home page load by skipping over the retrieval
    // of home page data used to display the progress graphs and calendar.
    if (registrationMode !== "undefined") {
        if (registrationMode === 'true') {
            // turn off the spinner
            spinner.isLoading(false);
            return;
        }
    }

    var degreePlanPromise = planRepository.get(currentUserId)
        .then(function (results) {
            var degreePlan = results[0];

                // Map the degree plan to a local view model
                planViewModelInstance.DegreePlan(ko.mapping.fromJS(degreePlan, planMapping));

                // Determine the correct term to display
                var currentTerm = null;
                if (typeof (planViewModelInstance.DegreePlan().Terms) !== "undefined") {
                    if (planViewModelInstance.DegreePlan().Terms().length > 0) {
                        if (degreePlan.TermToShow.length > 0) {
                            currentTerm = ko.utils.arrayFilter(planViewModelInstance.DegreePlan().Terms(), function (term) {
                                return term.Code() == degreePlan.TermToShow;
                            })[0];
                        } else {
                            currentTerm = planViewModelInstance.DegreePlan.Terms()[0];
                        }
                        planViewModelInstance.currentTermCode(currentTerm.Code());
                    }
                }

                // Bind the degree plan view model to the UI
                ko.applyBindings(planViewModelInstance, document.getElementById("home-calendar"));
        }).catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
        });

    var evaluationsPromise = requirementsRepository.get(currentUserId)
        .then(function (results) {

            var evaluations = results[0];

                // Add each evaluation to the local list of evaluations, sorted by title
                for (var i = 0; i < evaluations.length; i++) {
                    var result = new programModel(evaluations[i].Program);
                    programRequirementsViewModelInstance.Programs.push(result);
                }
                programRequirementsViewModelInstance.Programs.sort(function (left, right) {
                    return left.Title() == right.Title() ? 0 : (left.Title() < right.Title() ? -1 : 1)
                });
        }).catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
        });

    Promise.all([degreePlanPromise, evaluationsPromise]).then(function () {
            spinner.isLoading(false);
    });
});