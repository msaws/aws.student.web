﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.

function transcriptsViewModel(data) {
    var self = this;

    this.isLoaded = ko.observable(false);

    this.isProxyUser = ko.observable().subscribeTo("isProxyUser", true);

    this.transcriptGroupings = ko.observableArray([]);

    // Boolean to indicate if this student has a transcript restriction.
    // Default to true to ensure the transcript isn't shown by accident.
    this.transcriptsAreRestricted = ko.observable(self.isProxyUser() === true ? false : true);
    this.transcriptRestrictionMessages = ko.observableArray();
}


