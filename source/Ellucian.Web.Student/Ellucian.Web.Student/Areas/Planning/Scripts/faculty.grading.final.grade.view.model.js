﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

//for final grading 
var finalGradingModel = function () {

    var self = this;
    this.studentGradesHolder = [];
    this.StudentGrades = ko.observableArray([]);

       this.countOfGradesNotVerified = ko.pureComputed(function () {
        return ko.utils.arrayFilter(self.StudentGrades(), function (item) {
            return (item.IsGradeVerified() === false && !isNullOrEmpty(item.FinalGrade()) && item.isValid() && item.isExpireDateRequired() && item.isLDARequired() )
        }).length;
       });

       this.countOfGradeEnterInProgress = ko.pureComputed(function () {
           return ko.utils.arrayFilter(self.StudentGrades(), function (item) {
            return (item.isWaiting() === true)
           }).length;
       });

   
       this.areThereAnyErrors = ko.computed(function () {
           return ko.utils.arrayFilter(self.StudentGrades(), function (item) {
            return (item.hasErrors() === true)
           }).length;
       });

     
    this.GradeTypes = ko.observableArray([]);

    this.SectionId = ko.observable();

    this.hasStudentGrades = ko.computed(function () {
        if (self.StudentGrades() !== null && typeof self.StudentGrades() !== 'undefined') {
            return (self.StudentGrades().length > 0)
        }
        else {
            return false;
    }
       });

           //add student grade
    this.addStudentGrade = function (studentGrade) {
        this.studentGradesHolder.push(studentGrade);

       };
};




//Student grade model is created from raw json data that is received from server
//this raw data is mapped to student grade knockout model that adds upon 
//validations on mapped observable
//passing four parameters to it
//data- is raw data
//intitialGradeData - this is an object which can be either null or can have object with letter grade and 
//incomplete grade value
//it is null when a student does not have initial grade provided
function studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData) {
    ko.mapping.fromJS(data, { 'ignore': ['Midterm1Grade', 'Midterm2Grade', 'Midterm3Grade', 'Midterm4Grade', 'Midterm5Grade', 'Midterm6Grade']
    }, this);
    var self = this;
    baseStudentGradeModel.call(self);
    //local observables not mapped
    this.sectionStartDate = moment(sectionStartDate);
    this.sectionEndDate = moment(sectionEndDate);
    this.hasErrors = ko.computed(function () {
        if (!self.isValid() || !self.isExpireDateRequired() || !self.isLDARequired() || self.gradeEntryErrors().length > 0)
            return true;
        else
            return false;
    });
  
    self.lastDateValidationMessage = ko.observable("");
    this.isLDARequired = ko.observable(true);
    this.LastDateAttended.isValid = ko.observable(true);

    self.gradeExpireDateMessage = ko.observable("");
    this.isExpireDateRequired = ko.observable(true);
    this.GradeExpireDate.isValid = ko.observable(true);

    this.isValid = ko.computed(function () {
        return self.LastDateAttended.isValid() && self.GradeExpireDate.isValid();
    });

    this.finalGradeSelected = ko.observable(initialGradeData);


    this.finalGradeSelected.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.finalGradeSelected())) {
            //here Item1 is letter grade and Item2 is incomplete value associated with letter grade
            if (!isNullOrEmpty(self.finalGradeSelected().Item2()) && isNullOrEmpty(self.GradeExpireDate())) {
                self.isExpireDateRequired(false);

            }
            else {
                self.isExpireDateRequired(true);

            }
            if (isNullOrEmpty(self.finalGradeSelected().Item2()) && !isNullOrEmpty(self.GradeExpireDate())) {
                self.GradeExpireDate("");
            }


          //  validate LDA when grade is changed. Item3 is bool value to verify if LDA is required
            if (self.finalGradeSelected().Item3() == true && isNullOrEmpty(self.LastDateAttended()) && self.NeverAttended() !== true) {
                self.isLDARequired(false);
            }
            else {
                self.isLDARequired(true);

            }


            self.FinalGrade(self.finalGradeSelected().Item1());

            if (self.IsGradeVerified() === null || typeof self.IsGradeVerified === 'undefined' || self.IsGradeVerified() === false) {
                self.checkIfNeedToSave();
            }
        }
        else {
            self.FinalGrade(null);
        }
    }
    );


    self.validateLastDate=function(value){

        //check for validity based upon grade type
                if (value == null || value.length == 0) return true;
                var dateString = Globalize.parseDate(value);
                if (dateString == null) {
                    self.lastDateValidationMessage(ValidDateErrorMessage);
                    return false;
                }
                else
                    {
                    var attendanceDate = moment(value);
                    if (!isNullOrEmpty(self.sectionStartDate) && attendanceDate < self.sectionStartDate) {
                        self.lastDateValidationMessage(AttendanceDateErrorMesssage);
                        return false;
                    }
                    if (!isNullOrEmpty(self.sectionEndDate) && attendanceDate > self.sectionEndDate) {
                        self.lastDateValidationMessage(AttendanceDateErrorMesssage);
                        return false;
                    }
                    return true;

                }
            
    };

   

    this.LastDateAttended.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.finalGradeSelected())) {
            if (self.finalGradeSelected().Item3() == true && isNullOrEmpty(newValue) && self.NeverAttended()!==true) {
                self.isLDARequired(false);
            }
            else {
                self.isLDARequired(true);
            }
        }

        self.isFieldDirty(true);
        var result = self.validateLastDate(self.LastDateAttended());
        self.LastDateAttended.isValid(result);
            self.isSuccessfullyUpdated(null);
            
    });
   




    self.validateGradeExpireDate = function (value) {

        if ((value == null || value.length == 0))
            return true;
        if (value != null && value.length > 0) {
            var dateString = Globalize.parseDate(value);
            if (dateString == null) {
                self.gradeExpireDateMessage(ValidDateErrorMessage);
                return false;
            }
            else {

                var expireDate = moment(value);

                if (!isNullOrEmpty(self.sectionEndDate) && expireDate <= self.sectionEndDate) {
                    self.gradeExpireDateMessage(ExpirationDateErrorMessage);
                    return false;
                }
                return true;

            }
        }
    };


    //attached subscribers 
    this.GradeExpireDate.subscribe(function (newValue) {
     
            if (!isNullOrEmpty(self.finalGradeSelected())) {
                if (!isNullOrEmpty(self.finalGradeSelected().Item2()) && isNullOrEmpty(newValue)) {
                    self.isExpireDateRequired(false);
                }
                else {
                    self.isExpireDateRequired(true);
                }
            }
        
            self.isFieldDirty(true);
            var result = self.validateGradeExpireDate(self.GradeExpireDate());
            self.GradeExpireDate.isValid(result);
            self.isSuccessfullyUpdated(null);
    });

    this.FinalGrade.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.FinalGrade())) {
            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
        }
    });


  

    this.NeverAttended.subscribe(function (newValue) {
        if (newValue === null)
            return;
            if (newValue === true)
            {
                self.LastDateAttended("");
            }
                if (!isNullOrEmpty(self.finalGradeSelected())) {
                    if (self.finalGradeSelected().Item3() == true && isNullOrEmpty(self.LastDateAttended()) && newValue !==true) {
                        self.isLDARequired(false);
                    }
                    else
                    {
                        self.isLDARequired(true);
                    }
                   
                }

            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
            self.checkIfNeedToSave();
    });



    this.updateNotificationOnGradeVerify = ko.computed(function () {
        if (self.IsGradeVerified() == null)
            return;
            if (self.IsGradeVerified() === true) {
                //notify 
                facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter(self.StudentId.peek(), null);
            }
            else if (self.IsGradeVerified() === false && self.gradeEntryErrors.peek().length > 0) {
                facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter(self.StudentId.peek(), self.gradeEntryErrors.peek());
            }
    });
  
    //functions
    this.checkIfNeedToSave = function () {
        if (self.isFieldDirty() === true && self.isValid() && self.isExpireDateRequired() && self.isLDARequired()) //also check for validity
        {
            //make an ajax call
            self.postStudentGrade();
        }
    };

};


//this is to override toJSON method for studentGradeModel
//instead of creating another transfer model to server
//this model is itself cleaned and sent
studentGradeModel.prototype.toJSON = function () {
    return baseStudentGradeModel.prototype.toJSON.call(this);
};




//this is a verification model - happens when 'post grades' is pressed to  all the final grades are sent to
//verify state. this in turn retrieves collection of students verified grades along with any associated errors if happened
//during verification. 
//verify grades can differ from final grades. Suppose  a final grade 'A' when verified is equal to 'P' then grade received
//back will have 'P' though grade entered before verification was 'A'. Hence overview of grades should also modify to show new verified grade.
function verifyGradeModel(parent,sibling) {
    
    var self = this;
    var finalGradingModel = sibling;
    this.isVerifying = ko.observable(false);
    this.verifyGradesDialogisOpen = ko.observable(false);
    this.verifyGradeIsClicked = function () {
        self.verifyGradesDialogisOpen(true);
    };

   
   

    //this transforms verified grades to final grades
    this.transform = function (newValue) {
        ko.utils.arrayForEach(newValue, function (verifyGradeResponse) {
            ko.utils.arrayFirst(sibling.StudentGrades(), function (finalGrade) {
                if (verifyGradeResponse.StudentId === finalGrade.StudentId()) {
                   
                    finalGrade.gradeEntryErrors(verifyGradeResponse.VerifyGradeResponseErrors);
                    finalGrade.GradeExpireDate(verifyGradeResponse.GradeExpireDate);
                    finalGrade.NeverAttended(verifyGradeResponse.NeverAttended);
                    finalGrade.LastDateAttended(verifyGradeResponse.LastDateAttended);
                    finalGrade.IsGradeVerified(null);
                    finalGrade.IsGradeVerified(verifyGradeResponse.IsGradeVerified);
                  
                    var gradeType = ko.utils.arrayFirst(sibling.GradeTypes(), function (gradeType) {
                        if (gradeType.Item1() === verifyGradeResponse.FinalGrade) {
                            return gradeType;

                        }
                    });
                        
                        if (gradeType !== null && typeof gradeType !== 'undefined') {
                            finalGrade.finalGradeSelected(gradeType);

                        }

                       
                }
            });
        });
            };



    this.verifyGrades = function () {
     
            var json = { sectionId: sibling.SectionId() };
            $.ajax({
                url: verifyStudentGradesUrl,
                data: JSON.stringify(json),
                type: "PUT",
                dataType: "json",
                contentType: jsonContentType,
                beforeSend: function () {
                    self.isVerifying(true);
                },
                success: function (data, textStatus, jqXHR) {
                    try {
                        //map received verified grades to overview model so that overview have latest verified grades
                        ko.mapping.fromJS(data, {}, parent.overviewGradingModelInstance);
                        self.transform(data.StudentGrades);
                        parent.isGradeEntryPerformed(false);
                    }
                    catch(e)
                    {
                        console.error(e);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.textStatus != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: VerificationFailedErrorMessage, type: "error" });
                    }
                },
                complete: function () {
                    self.isVerifying(false);
                }
            });
    };

   
    this.countOfGradesToVerify = ko.pureComputed(function () {
        return sibling.countOfGradesNotVerified();
    });

    

    this.verifyDialogOptions = function () {
        return {
            autoOpen: false,
            modal: true,
            resizable: false,
            minWidth: 500,
            scrolltop: 0,
            scrollleft: 0,
            show: { effect: "fade", duration: dialogFadeTimeMs },
            hide: { effect: "fade", duration: dialogFadeTimeMs }
        };
    };

    // Close the  dialog box
    this.closeDialog = function () {
        self.verifyGradesDialogisOpen(false);
    };

    this.okayToVerifyGrades = function () {
        
        self.verifyGrades();
        self.closeDialog();
    };

    // Computed observables to show/hide buttons on modal dialog
    this.displayActionButtons = ko.pureComputed(function () {
        return self.countOfGradesToVerify() > 0;
    });

    this.displayOkButton = ko.pureComputed(function () {
        return self.countOfGradesToVerify() == 0;
    });

    // Button objects for modal dialog
    this.postGradesButton = {
        id: 'postGradesButton',
        title: Ellucian.Faculty.Resources.PostGradesButtonText,
        isPrimary: true,
        visible: self.displayActionButtons,
        callback: self.okayToVerifyGrades
    };

    this.okButton = {
        id: 'okButton',
        title: Ellucian.Faculty.Resources.OKButtonText,
        isPrimary: true,
        visible: self.displayOkButton,
        callback: self.closeDialog
    };

    this.cancelButton = {
        id: 'cancelButton',
        title: Ellucian.Faculty.Resources.CancelButtonText,
        isPrimary: false,
        visible: self.displayActionButtons,
        callback: self.closeDialog
    };
};


//this is a notification model to control notifications on page
//this logic was added so that there is one notification per student
//suppose a server error occurs while saving a grade for the student (eg: record locked)
//then a notification will appear with studentId for that student
//suppose another server error occurs for another student (like attendance date not within range of cross section) then a notification
//will be stacked over theexisting notifications with student id of this student.
//now suppose for 2nd student date is fixed, but other error is received, then placeholder of notification will replace the old error with new one.
//and if no server error happens, saving was successful, notification will be removed.
function notificationModel() {
    var self = this;
    this.currentNotificationIndex = 0;
    this.notificationMessages = [];

    this.updateNotificationCenter = function (studentId, gradeEntryErrors) {

        var StudentId = studentId;
        var associatedIndex = -1;
        var allGradeErrors = "";
        //combine all the errors
        if (gradeEntryErrors !== null) {
            gradeEntryErrors.forEach(function (item, index, array) {
                allGradeErrors = allGradeErrors.concat(item, "<br>");
            });
        }
        //find the index if student id already has notifications
        var index = -1;
        for (var i = 0; i < self.notificationMessages.length; i++) {

            if (self.notificationMessages[i].studentId == StudentId) {
                index = i;
            }
        };


        if (index !== -1) {
            associatedIndex = self.notificationMessages[index].associatedIndex;

            $('#notificationHost').notificationCenter('removeNotification', { index: associatedIndex });
            if (gradeEntryErrors !== null) {
                self.notificationMessages[index].associatedIndex = self.currentNotificationIndex;
                $('#notificationHost').notificationCenter('addNotification', { message: studentId + ': ' + allGradeErrors, type: "error" });
                self.currentNotificationIndex = self.currentNotificationIndex + 1;
            }
        }
        else {
            if (gradeEntryErrors !== null) {
                self.notificationMessages.push({ studentId: studentId, associatedIndex: self.currentNotificationIndex });
                $('#notificationHost').notificationCenter('addNotification', { message: studentId + ': ' + allGradeErrors, type: "error" });
                self.currentNotificationIndex = self.currentNotificationIndex + 1;
            }
        }
    };

};
