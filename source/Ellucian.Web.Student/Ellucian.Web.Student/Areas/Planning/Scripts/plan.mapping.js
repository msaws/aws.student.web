﻿// Copyright 2015 - 2017 Ellucian Company L.P. and its affiliates.
// planMapping is used to map the JSON returned by the UI server to 
// objects used in the knockoutJS data binding.
var planMapping = {
    'DegreePlan': {
        create: function (options) {
            return new DegreePlan(options.data);
        }
    },
    'Terms': {
        create: function (options) {
            return new Term(options.data);
        }
    },
    'PlannedCourses': {
        create: function (options) {
            return new Course(options.data);
        }
    },
    'CompletedCourses': {
        create: function (options) {
            return new Course(options.data);
        }
    },
    'RequisiteWarnings': {
        create: function (options) {
            return new requisiteModel(options.data);
        }
    },
    'CorequisiteCourseWarnings': {
        create: function (options) {
            return new corequisiteCourseModel(options.data);
        }
    },
    'CorequisiteSectionWarnings': {
        create: function (options) {
            return new corequisiteSectionModel(options.data);
        }
    },
    'PlannedSectionsWithNoMeetingTimes': {
        create: function (options) {
            return new Course(options.data);
        }
    },
    'StudentWaivers' :{
        update: function (options)
        {
           return UpdatePetitionsWaivers(options);
            
        }
    },
    'StudentPetitions': {
        update: function (options) {
            return UpdatePetitionsWaivers(options);
        }
    },
    'StudentConsents': {
        update: function (options) {
            return UpdatePetitionsWaivers(options);
        }
    },
    'copy': ['DegreePlanDto']
};


function UpdatePetitionsWaivers(options) {
    return new function () {
        ko.mapping.fromJS(options.data, {}, this);
        this.Period = ko.computed(function () {
            var startDateFormatted = isNullOrEmpty(this.StartDate()) ? " " : this.StartDate();
            var endDateFormatted = isNullOrEmpty(this.EndDate()) ? "Unlimited" : this.EndDate();
            var period = isNullOrEmpty(this.Term()) ? startDateFormatted + " - " + endDateFormatted : this.Term();
            return period;
        }, this);
    }
};

function DegreePlan(data) {
    ko.mapping.fromJS(data, planMapping, this);

    var self = this;

    this.isProxyUser = ko.observable().subscribeTo("isProxyUser", true);
};

function Term(data) {
    ko.mapping.fromJS(data, planMapping, this);

    var self = this;

    this.availableSectionsRetrieved = ko.observable(false);

    this.coursesSelected = ko.computed(function () {
        if (typeof self.PlannedCourses === 'undefined') return false;
        for (var i = 0; i < self.PlannedCourses().length; i++) {
            if (self.PlannedCourses()[i].selected() === true) {
                return true;
            }
        }
        return false;
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Other Section Filtering
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    this.showingFilters = ko.observable(false);

    //slide in/out filter menus on click
    this.filterHeaderClickHandler = function (data, event) {
        // collapse all other open filters
        $('#filters-menu li a.filter-header-link').each(function (index) {
            if (!$(this).is(event.currentTarget)) {
                $('ul', $(this).parent()).stop().slideUp(100);
                $(this).data("collapsed", "true");
                $(this).removeClass("filter-header-link-expanded").addClass("filter-header-link-collapsed");
            }
        });

        // toggle state of this one to open/closed
        var ul = $(event.currentTarget).next("ul.filters-menu-filter");

        if (typeof $(event.currentTarget).data("collapsed") === "undefined" || $(event.currentTarget).data("collapsed") === "true") {
            $(event.currentTarget).data("collapsed", "false");
            ul.stop().slideDown(100, function () { ul.css('overflow', 'auto') });
            $(event.currentTarget).removeClass("filter-header-link-collapsed").addClass("filter-header-link-expanded");
        } else {
            $(event.currentTarget).data("collapsed", "true");
            ul.stop().slideUp(100);
            $(event.currentTarget).removeClass("filter-header-link-expanded").addClass("filter-header-link-collapsed");
        }
    }

    // selected filter options
    this.availabilityFilters = ko.observableArray();
    this.locationFilters = ko.observableArray();
    this.dayOfWeekFilters = ko.observableArray();
    this.timeOfDayFilters = ko.observableArray();
    this.facultyFilters = ko.observableArray();
    this.topicFilters = ko.observableArray();
    this.atLeastOneFilterSelected = function () {
        return self.availabilityFilters().length > 0 || self.locationFilters().length > 0 || self.dayOfWeekFilters().length > 0 ||
            self.timeOfDayFilters().length > 0 || self.facultyFilters().length > 0 || self.topicFilters().length > 0;
    };

    // display items for selected filters
    this.filtersDisplay = function (filterSet) {
        var selected = 0;
        var display = "";
        for (var i = 0; i < filterSet.length; i++) {
            if (filterSet[i].Selected() == true) {
                selected++;
            }
        }
        switch (selected) {
            case 0:
                display = typeof stringFilterNoValueSelected !== "undefined" ? stringFilterNoValueSelected : "All";
                break;
            case 1:
            case 2:
                for (var i = 0; i < filterSet.length; i++) {
                    display = filterSet[i].Selected() == true ? display + filterSet[i].Description() + "\n" : display;
                }
                break;
            default:
                var more = filterSet.length - 1;
                display = filterSet[0].Description() + "\n" + Ellucian.Planning.planViewModelMessages.moreFilters.format(more);
                break;
        }
        return display;
    };

    this.availabilityFiltersDisplay = ko.computed(function () {
        return self.filtersDisplay(self.availabilityFilters());
    }).extend({ throttle: 10 });
    this.locationFiltersDisplay = ko.computed(function () {
        return self.filtersDisplay(self.locationFilters());
    }).extend({ throttle: 10 });
    this.dayOfWeekFiltersDisplay = ko.computed(function () {
        return self.filtersDisplay(self.dayOfWeekFilters());
    }).extend({ throttle: 10 });
    this.timeOfDayFiltersDisplay = ko.computed(function () {
        return self.filtersDisplay(self.timeOfDayFilters());
    }).extend({ throttle: 10 });
    this.facultyFiltersDisplay = ko.computed(function () {
        return self.filtersDisplay(self.facultyFilters());
    }).extend({ throttle: 10 });
    this.topicFiltersDisplay = ko.computed(function () {
         return self.filtersDisplay(self.topicFilters());
    }).extend({ throttle: 10 });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  End Other Section Filtering
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Determine whether this term is planned or completed and select the proper CSS class for the term's courses display
    this.GetCourseBubbleClass = function () {
        if (self.IsTermCompleted() && self.IsAcademicHistoryAvailable()) {
            return "dp-coursebubble dp-coursebubble-complete";
        }
        else {
            return "dp-coursebubble dp-coursebubble-planned";
        }
    };

    this.printSchedule = function () {
        var url = printScheduleUrl + "?termId=" + self.Code();
        var printWindow = window.open(url);
        if (window.focus) printWindow.focus();
        return false;
    };

    this.saveIcal = function () {
        var url = saveAsIcalUrl + "?termId=" + self.Code();
        window.location = url;
    };

    // Returns any planned courses in the current term that have a section and do not have a meeting
    this.unscheduledSections = ko.computed(function () {
        var courses = new Array();
        if (typeof self.PlannedSectionsWithNoMeetingTimes === 'undefined') return courses;
        for (var i = 0; i < self.PlannedSectionsWithNoMeetingTimes().length; i++) {
            var course = self.PlannedSectionsWithNoMeetingTimes()[i];
            var acadHistory = ko.utils.unwrapObservable(course.AcademicHistory);
            if (ko.utils.unwrapObservable(acadHistory.Section) == null) {
                courses.push(course);
            }
        }
        return courses;
    }).extend({ throttle: 50 });

    this.unscheduledRegisteredSections = ko.computed(function () {
        var courses = new Array();
        if (typeof self.PlannedSectionsWithNoMeetingTimes === 'undefined') return courses;
        for (var i = 0; i < self.PlannedSectionsWithNoMeetingTimes().length; i++) {
            var course = self.PlannedSectionsWithNoMeetingTimes()[i];
            var acadHistory = ko.utils.unwrapObservable(course.AcademicHistory);
            if (ko.utils.unwrapObservable(acadHistory.Section) != null) {
                courses.push(course);
            }
        }
        return courses;
    }).extend({ throttle: 50 });
};

function Course(data) {
    ko.mapping.fromJS(data, planMapping, this);

    var self = this;

    this.selected = ko.observable(false);

    this.AvailableSections = ko.observableArray();

    this.DisplayingDetails = ko.observable(true);
    this.DisplayingAvailableSections = ko.observable(false);

    this.AvailableSectionsPage = ko.observable(0);
    this.AvailableSectionsPageDisplay = ko.computed(function () { return self.AvailableSectionsPage() + 1; });
    this.availableSectionsPerPage = 6;
    this.availableSectionsPageCount = ko.computed(function () {
        var pageCount = self.AvailableSections().length / self.availableSectionsPerPage;
        return pageCount > 1 ? Math.ceil(pageCount) : 1;
    });

    // Returns the current page of filtered sections, based on the page number.
    // Note that an external caller (ideally a master container with knowledge of all filters) must update the FilteredAvailableSections observable as filters change!
    this.PagedAvailableSections = function () {
        if (typeof self.AvailableSections() === "undefined" || self.AvailableSections() === null || self.AvailableSections().length === 0) {
            return new Array();
        }

        var start = self.AvailableSectionsPage() * self.availableSectionsPerPage;
        var end = start + self.availableSectionsPerPage - 1;
        var total = self.AvailableSections().length;

        //if start falls outside bounds of set, safety reset back to page 0
        if (start > (total - 1)) {
            start = 0;
            end = (end > total) ? total - 1 : this.availableSectionsPerPage - 1;
        }

        //if end falls outside bounds of set, adjust backwards
        if (end > (total - 1)) {
            end = total - 1;
        }

        //update current page, for scenarios where a filter operation takes you from, say, page 4 to a single page of sections
        var page = 0;
        var i = 0;
        while (i < start) {
            i += self.availableSectionsPerPage;
            page++;
        }
        self.AvailableSectionsPage(page);

        return self.AvailableSections.slice(start, end + 1);
    };

    this.AvailableSectionsFirstPage = function () {
        this.AvailableSectionsPage(0);
    };
    this.AvailableSectionsPreviousPage = function () {
        if (self.AvailableSectionsPage() > 0) {
            self.AvailableSectionsPage(self.AvailableSectionsPage() - 1);
        }
    };
    this.AvailableSectionsNextPage = function () {
        var np = self.AvailableSectionsPage() + 1;
        if (self.AvailableSectionsPage() < (self.availableSectionsPageCount() - 1)) {
            self.AvailableSectionsPage(np);
        }
    };
    this.AvailableSectionsLastPage = function () {
        if (self.AvailableSectionsPage() < (self.availableSectionsPageCount() - 1)) {
            self.AvailableSectionsPage(self.availableSectionsPageCount() - 1);
        }
    };
    this.AvailableSectionsJumpPage = function (page) {
        if (page >= 1 && page <= self.availableSectionsPageCount()) {
            self.AvailableSectionsPage(page - 1);
        }
    };
    this.HandlePageInput = function (data, event) {
        //some ugly browser differences in how the new value is passed along
        if (event && typeof event.srcElement !== "undefined" && event.srcElement) {
            self.AvailableSectionsJumpPage(event.srcElement.value);
        }
        else if (event && typeof event.currentTarget !== "undefined" && event.currentTarget) {
            self.AvailableSectionsJumpPage(event.currentTarget.value);
        }
    };

    this.AvailableSectionsCanPageFirst = function () {
        if (self.AvailableSectionsPage() === 0) {
            return false;
        }
        return true;
    };
    this.AvailableSectionsCanPagePrevious = function () {
        if (self.AvailableSectionsPage() === 0) {
            return false;
        }
        return true;
    };
    this.AvailableSectionsCanPageNext = function () {
        return self.AvailableSectionsPage() < (self.availableSectionsPageCount() - 1);
    };
    this.AvailableSectionsCanPageLast = function () {
        return self.AvailableSectionsPage() < (self.availableSectionsPageCount() - 1);
    };

    // Figure out the display value of the grading type based on the enum code
    this.GetGradingTypeText = function () {
        var code = self.GetGradingType();
        if (code == Ellucian.Course.SectionDetails.stringGradedGradingCode ) return Ellucian.Course.SectionDetails.stringGradedGrading;
        if (code == Ellucian.Course.SectionDetails.stringAuditGradingCode ) return Ellucian.Course.SectionDetails.stringAuditGrading;
        if (code == Ellucian.Course.SectionDetails.stringPassFailGradingCode ) return Ellucian.Course.SectionDetails.stringPassFailGrading;
        return code;
    };

    // Determine what text to display for coreq and prereq warnings
    this.GetCoreqPrereqWarningText = function (separator) {
        var text = "";
        if (!separator) separator = "\n";

        //warnings on the planned course (which will include prerequisites)
        try {
            if (typeof self.Warnings === "function" && self.Warnings().length > 0) {
                for (var i = 0; i < self.Warnings().length; i++) {
                    try {
                        if (text.length > 0) { text += separator; }
                        text += self.Warnings()[i];
                    } catch (e) { }
                }
            }
        } catch (a) { }

        //prerequisite items on the course
        try {
            if (ko.utils.unwrapObservable(self.Requisite) != null) {
                var req = ko.utils.unwrapObservable(self.Requisite);
                if (req.DisplayText() != null && req.DisplayText() != "") {
                    try {
                        if (text.length > 0) { text += separator; }
                        text += req.DisplayText();
                    } catch (e) { }
                }
            }
        } catch (a) { }

        //corequisite items on the course
        try {
            if (self.CorequisiteCourses().length > 0) {
                for (var i = 0; i < self.CorequisiteCourses().length; i++) {
                    var item = self.CorequisiteCourses()[i];
                    if (item != null) {
                        if (item.IsRequired) {
                            if (item.DisplayMessage() != null && item.DisplayMessage() != "") {
                                try {
                                    if (text.length > 0) { text += separator; }
                                    text += item.DisplayMessage();
                                } catch (e) { }
                            }
                        } else {
                            if (item.DisplayMessage() != null && item.DisplayMessage() != "") {
                                try {
                                    if (text.length > 0) { text += separator; }
                                    text += item.DisplayMessage();
                                } catch (e) { }
                            }
                        }
                    }
                }
            }
        } catch (a) { }

        //corequisite items on the section
        try {
            if (self.Section != null) {
                if (self.Section.CorequisiteSections().length > 0) {
                    for (var i = 0; i < self.Section.CorequisiteSections().length; i++) {
                        var item = self.Section.CorequisiteSections()[i];
                        if (item != null) {
                            if (item.IsRequired) {
                                if (item.DisplayMessage() != null && item.DisplayMessage() != "") {
                                    try {
                                        if (text.length > 0) { text += separator; }
                                        text += item.DisplayMessage();
                                    } catch (e) { }
                                }
                            } else {
                                if (item.DisplayMessage() != null && item.DisplayMessage() != "") {
                                    try {
                                        if (text.length > 0) { text += separator; }
                                        text += item.DisplayMessage();
                                    } catch (e) { }
                                }
                            }
                        }
                    }
                }
            }
        } catch (a) { }

        return text;
    };

    this.ShowMeetingInfo = ko.observable(false);
    this.ToggleMeetingInfo = function () {
        self.ShowMeetingInfo() == true ? self.ShowMeetingInfo(false) : self.ShowMeetingInfo(true);
    };
    this.MeetingInfoStyle = ko.computed(function () {
        if (self.ShowMeetingInfo() == true) return "meeting-information-visible";
        return "meeting-information-hidden";
    });

    this.courseListItemStyle = ko.computed(function () {
        if (self.AcademicHistory.HasSection()) return "schedule-listitem-registeredsection";
        else return "schedule-listitem-plannedsection";
    });

    this.courseBannerWrapperStyle = ko.computed(function () {
        if ((self.HasSection() && !self.HasRegisteredSection())
            || ko.utils.unwrapObservable(self.Section) == null) {
            return "schedule-listitem-plannedsection-wrapper";
        }
        else return "";
    });

    this.courseBannerStyle = ko.computed(function () {
        if (self.IsInProgress() || self.IsPreregistered()) return "schedule-listitem-registeredsectionbanner";
        if (self.IsPlanned() || self.IsWaitlisted()) return "schedule-listitem-plannedsectionbanner";
        if (self.IsCompleted()) return "schedule-listitem-registeredsectionbanner";
        if (self.IsWithdrawn()) return "schedule-listitem-withdrawnsectionbanner";
    });

    this.courseBannerText = ko.computed(function () {
        if (self.IsInProgress()) return scheduleStatusRegistered;
        if (self.IsPreregistered()) return scheduleStatusPreregistered;
        if (self.IsCompleted()) return scheduleStatusCompleted;
        if (self.IsWithdrawn()) return scheduleStatusWithdrawn;
        if (self.IsPlanned()) return scheduleStatusPlanned;
        if (self.IsWaitlisted()) return scheduleStatusWaitlisted;
    });

    this.coursePrintStyle = ko.computed(function () {
        if (self.IsInProgress() || self.IsCompleted() || self.IsPreregistered()) {
            return 'schedule-details-table-registered';
        } else {
            return 'schedule-details-table-planned';
        }
    });

    this.showDropButton = ko.computed(function () {
        if (!self.AcademicHistory.IsGraded() && !self.IsWithdrawn()) {
            return true;
        }
        return false;
    });
};

function Filter(filter) {
    var self = this;
    this.Value = ko.observable(filter.Value);
    this.Description = ko.observable(filter.Description);
    this.Selected = ko.observable(filter.Selected);
    this.Count = ko.observable(filter.Count);
};

// Requisite link handler when requisite Id is available.
function requisiteModel(data) {
    ko.mapping.fromJS(data, planMapping, this);

    var self = this;
};

// Corequisite Course link handler when course ID is available.
function corequisiteCourseModel(data) {
    ko.mapping.fromJS(data, planMapping, this);

    var self = this;
};

// Corequisite Section link handler when course ID is available.
function corequisiteSectionModel(data) {
    ko.mapping.fromJS(data, planMapping, this);

    var self = this;
};


function StudentPetitionsWaiversViewModel() {
    var self = this;
    this.isLoaded = ko.observable(false);
    this.StudentWaivers = ko.observableArray();
    this.StudentPetitions = ko.observableArray();
    this.StudentConsents = ko.observableArray();
    this.hasPetitions = ko.observable(false);
    this.hasConsents = ko.observable(false);
    this.hasWaivers = ko.observable(false);

    this.hasPetitions = ko.computed(function () {
        return (self.StudentPetitions().length > 0)
    });

    this.hasConsents = ko.computed(function () {
        return (self.StudentConsents().length > 0)
    });

    this.hasWaivers = ko.computed(function () {
        return (self.StudentWaivers().length > 0)


    });

};
