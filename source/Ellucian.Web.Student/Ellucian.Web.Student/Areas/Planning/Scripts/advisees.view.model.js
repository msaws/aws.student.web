﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.Planning = Ellucian.Planning || {};

Ellucian.Planning.AdviseesViewModel = function AdviseesViewModel() {
    var self = this;

    this.Advisees = ko.observableArray();

    this.noAssignedFound = ko.observable(false);
    this.noAssignedFound.subscribe(function (string) {
        // If no assigned advisees are found, set focus to search input box
        $("#advising-search").focus();
    });
    this.noAdviseesFound = ko.observable(false);
    this.noAdviseesFound.subscribe(function (string) {
        // If no advisees are found, set focus to search input box
        $("#advising-search").focus();
    });
    this.moreAdvisees = ko.observable(false);

    this.pageSize = 10;
    this.pageIndex = 1;
    this.isSearchPage = false; // false if listing assigned advisees, true if listing search results
    this.isLoading = ko.observable(false);

    this.startThrobber = function () {
        self.isLoading(true);
    };
    this.stopThrobber = function () {
        self.isLoading(false);
    };

    this.loadMoreAdvisees = function () {
        if (!self.isSearchPage) {
            self.adviseeRequest();
        } else {
            self.adviseeSearch();
        }
    };

    this.adviseeRequest = function () {

        // if changing request type (assigned vs search), reset advisee list, reset the page index
        if (self.isSearchPage) {
            self.Advisees.removeAll();
            self.pageIndex = 1;
            self.isSearchPage = false;
        }
        self.getadviseesxhr = $.ajax({
            url: adviseeListActionUrl + "?pageSize=" + self.pageSize + "&pageIndex=" + self.pageIndex,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                self.startAdviseeRequest();
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var advisees = ko.mapping.fromJS(data, {});
                    self.Advisees.pushAll(advisees());
                    for (var i = 0; i < advisees().length; i++) {
                        if (advisees()[i].HasApprovalPending() === true) {
                            $('#notificationHost').notificationCenter('addNotification', { message: stringPendingApprovals, type: "info" });
                            break;
                        }
                    }
                    if (data.length === 0 && self.Advisees().length === 0) {
                        self.noAssignedFound(true);
                    } else if (data.length < self.pageSize && self.Advisees().length !== 0) {
                        self.moreAdvisees(false);
                    } else {
                        self.moreAdvisees(true);
                        self.pageIndex += 1;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: "Request for advisees failed.", type: "error" });
            },
            complete: function (data) {
                self.adviseeRequestComplete();
            }
        });
    }

    // Global search of advisees
    this.newSearch = false;
    this.selectedSearch = ko.observable("Advisee");
    this.searchString = ko.observable("");

    this.formSubmit = function () {
        // If the type of search changes between by advisee and by advisor, reset the pageIndex and set new search
        self.pageIndex = 1;
        self.isSearchPage = true;
        self.newSearch = true;
        self.adviseeSearch();
    }
    this.adviseeSearch = function () {
        if (self.searchString().length > 2) {
            // if this is a new search clear list
            if (self.newSearch) {
                self.Advisees.removeAll();
                self.newSearch = false;
            }
        
            var data = {};
            if (self.selectedSearch() == "Advisee") {
                data = { adviseeKeyword: $.trim(self.searchString()) };
            } else {
                data = { advisorKeyword: $.trim(self.searchString()) };
            }
            self.searchadviseesxhr = $.ajax({
                url: adviseeSearchActionUrl + "?pageSize=" + self.pageSize + "&pageIndex=" + self.pageIndex,
                data: JSON.stringify(data),
                type: "POST",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {
                    self.startAdviseeRequest();
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        var advisees = ko.mapping.fromJS(data, {});
                        self.Advisees.pushAll(advisees());
                        for (var i = 0; i < advisees().length; i++) {
                            if (advisees()[i].HasApprovalPending() === true) {
                                $('#notificationHost').notificationCenter('addNotification', { message: stringPendingApprovals, type: "info" });
                                break;
                            }
                        }
                        if (data.length === 0 && self.Advisees().length === 0) {
                            self.noAdviseesFound(true);
                        } else if (data.length < self.pageSize && self.Advisees().length !== 0) {
                            self.moreAdvisees(false);
                        } else {
                            self.moreAdvisees(true);
                            self.pageIndex += 1;
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0)
                        $('#notificationHost').notificationCenter('addNotification', { message: "Search for advisees failed.", type: "error" });
                },
                complete: function (data) {
                    self.adviseeRequestComplete();
                }
            });
        }
    
    };

    // Keep track of outstanding XMLHttpRequests 
    this.searchadviseesxhr = null;
    this.getadviseesxhr = null;

    // Start search or advisee request
    this.startAdviseeRequest = function () {
        // Cancelling searches
        if (self.searchadviseesxhr && self.searchadviseesxhr.readyState != 4) {
            self.searchadviseesxhr.abort();
        }
        if (self.getadviseesxhr && self.getadviseesxhr.readyState != 4) {
            self.getadviseesxhr.abort();
        }
        self.noAssignedFound(false);
        self.noAdviseesFound(false);
        self.moreAdvisees(false);
        self.startThrobber();
    };

    this.adviseeRequestComplete = function () {
        self.stopThrobber();
        self.searchadviseesxhr = null;
        self.getadviseesxhr = null;
    };
};
