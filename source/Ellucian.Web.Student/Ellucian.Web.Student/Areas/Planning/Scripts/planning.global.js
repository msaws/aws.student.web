﻿/////////////////////////////////////////////////////////////////////////////////////////////
//  This file contains utility functionality that needs to be made available across
//  two or more views within the Planing area of Self Service. Functions placed into this
//  file should be as succint and context-less as possible. These functions will be available
//  in every view in the Planning area, and loaded in the Planning _ViewStart page. Do NOT place
//  anything into this file that needs to be available in other Areas, such as Finance.
/////////////////////////////////////////////////////////////////////////////////////////////

// Determine the value to display for a given Section's credits amount
function courseOrSectionCreditsCeusDisplay(item) {
    var display = "";
    if (item != null) {
        display = ((item.MaximumCredits != null) && item.MaximumCredits > 0 && item.MaximumCredits !== item.MinimumCredits) ? item.MinimumCredits + " to " + item.MaximumCredits : item.MinimumCredits;
        if (display === null) {
            if (item.Ceus != null) {
                display = item.Ceus + " CEUs";
            }
        }
    }
    return display;
}

// Determine if the given course or section item represents a variable credit item
function isCourseOrSectionVariableCredit(item) {
    return (item != null && item.MinimumCredits != null && item.MaximumCredits != null &&
        item.MinimumCredits != item.MaximumCredits);
}