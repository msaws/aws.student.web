﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
var Ellucian = Ellucian || {};
Ellucian.Planning = Ellucian.Planning || {};

Ellucian.Planning.AdvisingViewModel = function AdvisingViewModel(spinnerMessage, baseUrl, planRepository) {
    var self = this;

    // "Inherit" the base view model
    Ellucian.Planning.PlanViewModel.call(self, true, Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, planRepository);

    //this is to keep track of the tab for advising term that was selected
    this.activeAdvisingTermTab = ko.observable(0);

    this.isPageLoading = ko.observable(true);

    this.spinnerMessage = ko.observable(spinnerMessage).subscribeTo("LoadingMessage");

    this.CanAdvisorReviewPlan = ko.observable(false).publishOn("CanAdvisorReviewPlan");
    this.CanAdvisorModifyPlan = ko.observable(false).publishOn("CanAdvisorModifyPlan");
    this.CanAdvisorRegister = ko.observable(false).publishOn("CanAdvisorRegister", true);

    // flags determining which UI pieces are available or showing
    this.canApprove = ko.computed(function () {
        var isProxy = self.isProxyUser();
        return !isProxy || (isProxy && self.CanAdvisorReviewPlan());
    });
    this.canEdit = ko.computed(function () {
        var isProxy = self.isProxyUser();
        return !isProxy || (isProxy && self.CanAdvisorModifyPlan());
    }).publishOn("canEdit"); // publish to canEdit topic, so advisors can add from catalog results, etc

    // showRegister indicates whether or not to display UI elements related to registration (Add, Drop buttons, etc)
    this.showRegister = ko.computed(function () {
        if (self.currentTermCode() === null) return false;
        var currentTerm = ko.utils.arrayFirst(self.DegreePlan().Terms(), function (term) {
            return term.Code() == self.currentTermCode();
        });
        var isProxy = self.isProxyUser();
        return (!isProxy || isProxy && self.CanAdvisorRegister())
            && (currentTerm.AllowRegistration() === true || forceEnableRegistrationButtons === true)
            && currentTerm.IsTermCompleted() === false;
    });

    this.ajaxErrorHandler = function (jqXHR, textStatus, errorThrown) {
        $('#notificationHost').notificationCenter('addNotification', { message: stringActionFailed, type: "error" });
    };

    var updateCompleted = function () {
        self.currentTermCode.valueHasMutated();
        self.isLoading(false);
    };

    this.SubmitReview = function (term) {
        if (self.CanAdvisorReviewPlan() === false) {
            return;
        }

                if (typeof stringUpdatingProtectionMessage === "string") {
                    self.spinnerMessage(stringReviewCompleteSpinner);
                }
                self.isLoading(true);

        planRepository.completeReview(self.DegreePlan().PersonId(), ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
        .then(function (data) {
            self.repositorySuccessHandler(data);

                var notification = {};
                var prompts = [
                    { label: stringArchivePlanNo, action: function () { $("#notificationHost").notificationCenter('removeNotification', notification); } }
                    , { label: stringArchivePlanYes, action: function () { $("#notificationHost").notificationCenter('removeNotification', notification); self.archivePlan(); } }
                ];
                notification.message = stringArchivePlan;
                notification.type = "information";
                notification.prompts = prompts;

                // Wrap the notification update in a timeout to make sure the ajaxSuccessHandler has done it's thing first.
                setTimeout(function () {
                    $('#notificationHost').notificationCenter('addNotification', notification);
                }, 351);
        })
        .catch(function (error) {
            self.ajaxErrorHandler(error);
        })
        .then(updateCompleted, updateCompleted)
    };

    this.SelectAll = function () {
        var currentTerm = self.currentTerm();
        if (self.CanAdvisorReviewPlan() === false || currentTerm.IsTermCompleted() === true || currentTerm.AllCoursesHaveCredit() === true) {
            return;
        }
        for (var i = 0; i < currentTerm.PlannedCourses().length; i++) {
            if (ko.utils.unwrapObservable(currentTerm.PlannedCourses()[i].AcademicHistory.Section) == null && currentTerm.PlannedCourses()[i].Count() == 0) {
                currentTerm.PlannedCourses()[i].selected(true);
            }
        }
    };

    this.SubmitApprovals = function (status) {
        if (self.CanAdvisorReviewPlan() === false) {
            return;
        }

        var currentTerm = self.currentTerm();

        if (!currentTerm.coursesSelected()) {
            return false;
        }

        var courses = [];
        for (var i = 0; i < currentTerm.PlannedCourses().length; i++) {
            if (currentTerm.PlannedCourses()[i].selected() === true) {
                courses.push(currentTerm.PlannedCourses()[i].Id());
            }
        }

                if (typeof stringUpdatingProtectionMessage === "string") {
                    self.spinnerMessage(stringProcessingApprovalSpinner);
                }
                self.isLoading(true);

        planRepository.approveCourses(self.DegreePlan().PersonId(), advisingViewModelInstance.currentTermCode(), courses, status, ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
        .then(function (data) {
            self.repositorySuccessHandler(data);
        })
        .catch(function (error) {
    self.repositoryErrorHandler(error);
})
        .then(updateCompleted, updateCompleted);
    };

    // Add a new advising note
    this.AddAdvisingNoteFromAdvisor = function (note) {

        if (!isNullOrEmpty(note)) {
            var dp = null;
            try {
                if (self !== null) {
                    dp = self.DegreePlan().DegreePlanDto;
                    if (typeof dp === "undefined" || dp === "") {
                        dp = null;
                    }
                }
            } catch (e) {
                dp = null;
            }

                    if (typeof stringAddingAdvisingNote === "string") {
                        self.spinnerMessage(stringAddingAdvisingNote);
                    }
                    self.isLoading(true);

            planRepository.submitNote(self.DegreePlan().PersonId(), note, "ADVISOR", ko.mapping.toJSON(dp))
            .then(function (data) {
                self.repositorySuccessHandler(data);
            })
            .catch(function (error) {
                self.repositoryErrorHandler(error);
            })
            .then(submitNoteCompleted, submitNoteCompleted);
        }
    };

    var submitNoteCompleted = function () {
                    $("#advising-notes-compose-box").val("");
                    $("#advising-notes-compose-box").focus();
        updateCompleted();
    };

    this.archivePlan = function () {

                if (typeof stringAddingAdvisingNote === "string") {
                    self.spinnerMessage(stringArchivingSpinnerMessage);
                }
                self.isLoading(true);

        planRepository.archivePlan(self.DegreePlan().PersonId(), ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
        .then(function (data) {
                if (planArchiveInstance) {
                    planArchiveInstance.archives.unshift(data[0]);
                }

                // reset the notification center, then repopulate with new messages
                setTimeout(function () {
                    $('#notificationHost').notificationCenter('reset');

                    var notification = { message: "An archive has been created.", type: "success" };
                    $('#notificationHost').notificationCenter('addNotification', notification);
                }, 350);
        })
        .catch(function (error) {
            self.repositoryErrorHandler(error);
        })
        .then(updateCompleted, updateCompleted);
    }

    this.searchUrl = ko.observable(baseUrl);

    this.searchAsync = ko.observable(true);

    // "Overloaded" this method in the advising (also exists in plan.view.model.js) view because we have an extra layer of tabs to navigate
    this.ShowScheduleTerm = function (termCode) {

        // Switch to the "Course Plan" tab
        $("#advising-content-nav").tabs("option", "selected", 0);

        self.currentTermCode(termCode);
    };

    // Ajax call to do plan protections - whether overall or course by course
    this.updatePlanProtection = function (protectionType) {

        var currentTerm = ko.utils.arrayFirst(self.DegreePlan().Terms(), function (term) {
            return term.Code() == self.currentTermCode();
        });

        if (!currentTerm.coursesSelected()) {
            return false;
        }

        var courses = [];
        for (var i = 0; i < currentTerm.PlannedCourses().length; i++) {
            if (currentTerm.PlannedCourses()[i].selected() === true) {
                courses.push(currentTerm.PlannedCourses()[i].Id());
            }
        }

                if (typeof stringUpdatingProtectionMessage === "string") {
                    self.spinnerMessage(stringUpdatingProtectionMessage);
                }
                self.isLoading(true);

        planRepository.updateProtection(self.DegreePlan().PersonId(), courses, protectionType, ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
        .then(function (data) {
            self.repositorySuccessHandler(data);
        })
        .catch(function (error) {
            self.repositoryErrorHandler(error);
        })
        .then(updateCompleted, updateCompleted);
    };

   
};
