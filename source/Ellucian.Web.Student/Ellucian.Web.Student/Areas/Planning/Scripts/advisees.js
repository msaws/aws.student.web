﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
var stringFilterWatermark = "Type a name or ID...";

$(document).ready(function () {
    var adviseesViewModelInstance = new Ellucian.Planning.AdviseesViewModel();
    ko.applyBindings(adviseesViewModelInstance, document.getElementById("main"));

    //watermark the search box
    $("#advising-search").watermark(stringFilterWatermark);

    adviseesViewModelInstance.loadMoreAdvisees();

    // We can't add a data-binding to window, so manually add the scroll event handler...
    $(window).scroll(function () {

        // Only take action if there are no pending requests
        if (adviseesViewModelInstance.searchadviseesxhr == null && adviseesViewModelInstance.getadviseesxhr == null && adviseesViewModelInstance.moreAdvisees() == true) {

            // If the user scrolled to within 10 pixels of the bottom, load some more advisees...
            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {

                adviseesViewModelInstance.loadMoreAdvisees();
            }
        }
    });
});