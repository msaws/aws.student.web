﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.


//model for mid term grades (from 1-6 midterms)
var midtermGradingModel = function () {
    ko.validation.init({
        messagesOnModified: true,
        insertMessages: false,
        decorateInputElement: true,
        errorsAsTitle: true
    });
    var self = this;
    this.StudentGrades = ko.observableArray([]);

       this.areThereAnyErrors = ko.computed(function () {
           return ko.utils.arrayFilter(self.StudentGrades(), function (item) {
            return (item.hasErrors() === true)
           }).length;
       });

      

    this.GradeTypes = ko.observableArray([]);

    this.SectionId = ko.observable();

    //add student grade
    this.addStudentGrade = function (studentGrade) {
        this.StudentGrades.push(studentGrade);

    };
    this.hasStudentGrades = ko.computed(function () {
        if (self.StudentGrades() !== null && typeof self.StudentGrades() !== 'undefined') {
            return (self.StudentGrades().length > 0)
        }
        else {
            return false;
        }
    });
};




//Student mid term grade model is created from raw json data that is received from server
//this raw data is mapped to student mid term grade knockout model that will have only midterm grades and
//other final grades will be ignored
function studentMidTermGradeModel(data) {
    ko.mapping.fromJS(data, {
        'ignore': ['FinalGrade','IsGradeVerified','GradeExpireDate','LastDateAttended']
    }, this);
    var self = this;
    baseStudentGradeModel.call(self);

    this.hasErrors = ko.computed(function () {
        if (self.gradeEntryErrors().length > 0)
            return true;
        else
            return false;
    });

    this.Midterm1Grade.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.Midterm1Grade())) {

            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
            self.checkIfNeedToSave();
        }
    });


    this.Midterm2Grade.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.Midterm2Grade())) {

            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
            self.checkIfNeedToSave();
        }
    });

    this.Midterm3Grade.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.Midterm3Grade())) {

            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
            self.checkIfNeedToSave();
        }
    });

    this.Midterm4Grade.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.Midterm4Grade())) {

            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
            self.checkIfNeedToSave();
        }
    });

    this.Midterm5Grade.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.Midterm5Grade())) {

            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
            self.checkIfNeedToSave();
        }
    });

    this.Midterm6Grade.subscribe(function (newValue) {
        if (!isNullOrEmpty(self.Midterm6Grade())) {

            self.isFieldDirty(true);
            self.isSuccessfullyUpdated(null);
            self.checkIfNeedToSave();
        }
    });

    //functions
    this.checkIfNeedToSave = function () {
        if (self.isFieldDirty() === true ) //also check for validity
        {
            //make an ajax call
            self.postStudentGrade();
        }
    };

};


//this is to override toJSON method for studentmidtermGradeModel
//instead of creating another transfer model to server
//this model is itself cleaned and sent
studentMidTermGradeModel.prototype.toJSON = function () {
    return baseStudentGradeModel.prototype.toJSON.call(this);
};

