﻿//Copyright 2015 - 2016 Ellucian Company L.P. and its affiliates.

function facultyTermsViewModel() {
    var self = this;
    this.retrieved = ko.observable(false);
    this.FacultyTerms = ko.observableArray();
}

var facultyTermsInstance = new facultyTermsViewModel();

$(document).ready(function () {

    // bind facultySectionsInstance to the view of the faculty landing page
    ko.applyBindings(facultyTermsInstance, document.getElementById("main"));

    $.ajax({
        url: facultySectionsActionUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, {}, facultyTermsInstance);
                var terms = ko.mapping.fromJS(data, {});
                for (var i = 0; i < terms().length; i++) {
                    facultyTermsInstance.FacultyTerms.push(terms()[i]);
                }
                facultyTermsInstance.retrieved(true);
                $(".faculty-section-table").makeTableResponsive();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve faculty sections", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {

        }
    });


});