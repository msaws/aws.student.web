﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates.
// Define a view model that is subscriber for program requirements model
var Ellucian = Ellucian || {};
// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

//This is subscriber model for program requirments. It is notified when degree-plan is modified either through catalog.result.view.model
//by addition of course/section and also get notified from plan.view.model for removal, addition of sections or terms
Ellucian.Planning = Ellucian.Planning || {};
Ellucian.Planning.programRequirementsSubscriberModel = function () {
    var self = this;
    this.requirementsRepository = Ellucian.Planning.requirementsRepository;

    //subscribed to degreePlanChanged observer that is triggered through degree plan modification events
    this.degreePlanChanged = ko.observable(false).subscribeTo("degreePlanChanged");

    this.degreePlanChanged.subscribe(function (result) {
        if (result === true) {
            self.updateSession(currentUserId);
        }
    });

    //this will trigger requirement repository method to clean session keys
    this.updateSession = function (studentId) {
        self.requirementsRepository.removeSessionKeys(studentId)

    };


};

