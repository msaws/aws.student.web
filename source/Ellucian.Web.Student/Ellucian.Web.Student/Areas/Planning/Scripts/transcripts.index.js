﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
// Viewmodels and mappings for the transcript components are defined in transcripts.view.model.js

var transcriptsInstance = new transcriptsViewModel();

function spinnerModel() {
    var self = this;
    this.isLoading = ko.observable(true);
}
var spinner = new spinnerModel();

$(document).ready(function () {

    // bind transcriptsInstance to the view of the test scores
    ko.applyBindings(transcriptsInstance, document.getElementById("unofficial-transcripts"));
    ko.applyBindings(spinner, document.getElementById('spinner'));

    var ajaxCompleted = 0; // We need to track how many ajax requests have completed, so we can turn off the throbber when they are all done.
    $.ajax({
        url: getTranscriptRestrictionUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                if (data.TranscriptRestrictions.length > 0) {
                    if (data.EnforceTranscriptRestriction === true) {
                        transcriptsInstance.transcriptsAreRestricted(true);

                        for (var i = 0; i < data.TranscriptRestrictions.length; i++) {
                            transcriptsInstance.transcriptRestrictionMessages.push(data.TranscriptRestrictions[i]);
                        }
                    }
                    else {
                        transcriptsInstance.transcriptsAreRestricted(false);
                    }
                }
                else {
                    transcriptsInstance.transcriptsAreRestricted(false);
                }

                if (transcriptsInstance.transcriptsAreRestricted() === true) {
                    ajaxCompleted++;
                } else {
                    $.ajax({
                        url: unofficialTranscriptsActionUrl,
                        type: "GET",
                        contentType: jsonContentType,
                        dataType: "json",
                        success: function (data) {
                            if (!account.handleInvalidSessionResponse(data)) {
                                transcriptsInstance.transcriptGroupings.pushAll(data);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (jqXHR.status != 0) {
                                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve transcript data", type: "error" });
                            }
                        },
                        complete: function (jqXHR, textStatus) {
                            if (ajaxCompleted == 1) {
                                transcriptsInstance.isLoaded(true);
                                spinner.isLoading(false);
                            } else {
                                ajaxCompleted++;
                            }
                        }
                    });
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve transcript data", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
            if (ajaxCompleted == 1) {
                transcriptsInstance.isLoaded(true);
                spinner.isLoading(false);
            } else {
                ajaxCompleted++;
            }
        }
    });
});