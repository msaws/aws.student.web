﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
var testScoreMapping = {
};

function testResultViewModel(data) {
    var self = this;

    this.isLoaded = ko.observable(false);
    this.AdmissionTests = ko.observableArray();
    this.PlacementTests = ko.observableArray();
    this.OtherTests = ko.observableArray();
    this.hasAdmissionTests = ko.computed(function () {
        return (self.AdmissionTests().length > 0)
    });
    this.hasPlacementTests = ko.computed(function () {
        return (self.PlacementTests().length > 0)
    });
    this.hasOtherTests = ko.computed(function () {
        return (self.OtherTests().length > 0)
    });
}


