﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
function planArchiveViewModel(data) {
    var self = this;

    this.isLoaded = ko.observable(false);
    this.archives = ko.observableArray();

    this.hasArchives = ko.computed(function () {
        return self.archives().length > 0;
    });

    this.ReportLinkHandler = function (data) {
        window.location = getPlanArchiveReportUrl + "?archiveId=" + data.Id;
    };
}