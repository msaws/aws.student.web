﻿// forceEnableRegistrationButtons is normally initialized from webconfig, 
// but PrintSchedule skips some processing, so we initialize it manually here.
var forceEnableRegistrationButtons = false;

this.planViewModelInstance = new Ellucian.Planning.PlanViewModel();

$(document).ready(function () {

    if (typeof result !== "undefined" && result !== null && result !== "") {
        // Map the degree plan JSON object to the view model instance
        planViewModelInstance.DegreePlan(ko.mapping.fromJS(result, planMapping));

        // If this is a printable schedule, initialize the viewModel's currentTerm
        // The user already selected which term to print; we don't give them the option
        var printSchedule = document.getElementById("print-schedule-body");
        if (printSchedule !== null) {
            // Select active term on schedule
            if (!isNullOrEmpty(result.TermToShow)) {
                for (var i = 0; i < planViewModelInstance.DegreePlan().Terms().length; i++) {
                    if (planViewModelInstance.DegreePlan().Terms()[i] !== null && planViewModelInstance.DegreePlan().Terms()[i].Code() === result.TermToShow) {
                        planViewModelInstance.currentTermCode(planViewModelInstance.DegreePlan().Terms()[i].Code());
                        break;
                    }
                }
            }
            ko.applyBindings(planViewModelInstance, printSchedule);
            return;
        }
    }
});