﻿// Copyright 2015 - 2016 Ellucian Company L.P. and its affiliates.

function facultyConsentModel() {
    var self = this;
    this.FormattedNameDisplay = ko.observable("");
    this.Term = ko.observable("");
    this.Location = ko.observable("");
    this.Meetings = ko.observableArray();
    this.SectionId = ko.observable("");
    this.retrieved = ko.observable(false);
    };

}

var facultyConsentInstance = new facultyConsentModel();


$(document).ready(function () {

    ko.applyBindings(facultyConsentInstance, document.getElementById("faculty-selection"));
    $.ajax({
        url: facultySectionHeaderActionUrl + "?sectionId=" + sectionId,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, {}, facultyConsentInstance);
                facultyConsentInstance.retrieved(true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToRetrieveSection, type: "error" });
            }
        }
    });
});