﻿// Copyright 2013-2016 Ellucian Company L.P. and its affiliates.
// Define a view model that can be used to display program requirements and program evaluation results
function programRequirementsViewModel(baseUrl, searchAsync) {
    var self = this;

    // "Inherit" the base view model
    BaseViewModel.call(self);

    self.requirementMinHeight = ko.observable();

    // Change desktop structure to tab mobiles structure
    // This should probably be done with a custom knokcout binding
    this.changeToMobile = function () {
        // checks to see if function has already ran by detecting if an ID created by the function has length (if it exists)
        // prevents unecesssary triggering on resize (multiple uls)
        if ($('#program-requirements').length) {
            return;
        }
                
        // removes class program-tabs-desktop so all css associated with it will not be used
        $('#program-tabs').removeClass("program-tabs-desktop");

        // adds classes for tabs
        $('#program-tabs').addClass("programs-nav ui-tabs ui-widget ui-widget-content ui-corner-all");

        // Hide the program section headers (these are replaced by the mobile-nav-tabs list in mobile)
        $('.program-wrapper .program-section-header').hide();

        // Show and style mobile tabs that replace the headers
        $(".mobile-nav-tabs").show();

        $('.program-wrapper').addClass("program-wrapper-tabs");

        $('.program-wrapper-tabs').tabs({
            // First tab (At a Glance) is the default selected tab
            selected: 0,

            // When tabs are shown, if Requirements tab was selected, record the min-height of 
            // #program-list, in case of short requirements
            show: function (event, ui) {
                if (ui.index == 1) {
                    self.requirementMinHeight = $('#program-list').outerHeight();
                }                
            }
        });
        $('div[id ^= "program-requirements"]').addClass('ui-tabs-hide');

        // hide all requirement details
        $('.requirement-details').addClass('requirement-details-hide');
        // hide all backlinks
        $('section.requirement div.backlink').addClass('backlink-hide');
        // show all requirements (only headers remain)
        $('section.requirement').removeClass('requirement-hide');
        // Hide Other Courses
        $('div.requirements-details div.other-courses').hide();
    }

    // Change tabbed mobile structure back to desktop view
    this.changeToDesktop = function () {
        if ($('.program-wrapper .program-section-header').is(':visible')) {
            return;
        }
        // add class program-tabs-desktop
        $('#program-tabs').addClass("program-tabs-desktop");

        // Show the program section headers
        $('.program-wrapper .program-section-header').show();

        // Hide and style mobile tabs that replace the headers
        $(".mobile-nav-tabs").hide();

        // Remove tabs from program-wrapper and both Requirements and At A Glance
        $('.program-wrapper').removeClass('program-wrapper-tabs ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-panel ui-corner-bottom');
        $('section[id ^= "program-requirements"]').removeClass('ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide');
        $('section[id ^= "programs-ataglance"]').removeClass('ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide');

        // Make sure requirements are not hidden (if user changing size in desktop)
        $('section.requirement').removeClass('requirement-hide');

        // Requirement details always show in desktop
        $('.requirement-details, .requirement-animation-wrapper').removeClass('requirement-details-hide');

        // remove the hardcoded styling added by the slider, for specific details and the general details container
        $('.requirement-details, .requirements-details, section.requirement div.backlink').removeAttr('style');

        // remove selected detail ID
        $('#requirement-details-sliding').removeAttr('id');

        // Add class that hides the requirement details backlink--never shows in desktop
        $('section.requirement div.backlink').addClass('backlink-hide');

        // unhide Other Courses
        $('div.requirements-details div.other-courses').show();

        // hide mobile header
        $('.requirement-header-mobile').hide();
    }

    var attachedHandlers = false;

    this.searchUrl = ko.observable(baseUrl);

    this.searchAsync = ko.observable(searchAsync);

    this.isProxyUser = ko.observable(false).subscribeTo("isProxyUser", true);
    this.canEdit = ko.observable(false).subscribeTo("canEdit", true);

    // Post to the "catalogResult" topic - when another view model receives a result, it gets posted 
    // to the "catalogResult" topic, allowing the catalog result view model to display the data.
    this.catalogResult = ko.observable(null).publishOn("catalogResult");

    // catalogResultsDisplayed is used by embedded (asynchronous) course catalogs. When a search result is displayed, 
    // a message (with value of true) is posted to the "catalogResultDisplayed" topic, 
    // allowing the catalog index to hide and the catalog results to show.
    this.catalogResultsDisplayed = ko.observable(false).publishOn("catalogResultDisplayed");

    this.StudentId = ko.observable();

    this.noPrograms = ko.observable(false);

    this.samplePlanLoaded = ko.observable(false).subscribeTo("samplePlanLoaded");

    this.samplePlanLoaded.subscribe(function (result) {
        if (result === true) {
            planRepository.removeSessionKeys(self.StudentId());
            requirementsRepository.removeSessionKeys(self.StudentId());
        }
    });

    // Course link handler.
    this.CourseLinkClick = function (context) {
        var link = self.searchUrl();
        if (groupModel.prototype.isPrototypeOf(context)) {
            link += "?requirement=" + encodeURIComponent(context.RequirementCode()) + "&Subrequirement=" + encodeURIComponent(context.SubrequirementId()) + "&Group=" + encodeURIComponent(context.Id()) + "&requirementText=" + encodeURIComponent(context.SearchText());
        } else if (courseModel.prototype.isPrototypeOf(context)) {
            if (context.Id() == "PlannedCourse") {
                link += "?CourseIds=" + encodeURIComponent(context.CourseId());
            }
            else {
                link += "?CourseIds=" + encodeURIComponent(context.Id());
            }
        } else if (creditModel.prototype.isPrototypeOf(context)) {
            link += "?CourseIds=" + encodeURIComponent(context.CourseId());
        }

        if (!self.searchAsync()) {
            window.location = link;
            return;
        } else {
            $.ajax({
                url: link,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.catalogResult(data);
                        self.catalogResultsDisplayed(true);
                        $("#advising-content-nav").tabs("option", "selected", 3);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0)
                        $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve search results.", type: "error" });
                },
                complete: function (jqXHR, textStatus) {

                }
            });
        }
        return false;

    }

    // Is the current user the student that is being evaluated?  Default to false.
    this.CurrentUserIsStudent = ko.observable(false);

    // Programs in which the student is enrolled
    this.Programs = ko.observableArray();

    // List of all related program codes (strings) associated to this student's active programs
    this.RelatedProgramCodes = ko.observableArray();

    // Related programs for the student's programs - this is the results from the program evaluation.
    this.RelatedPrograms = ko.observableArray();

    // Indicator to show the related program ajax calls are in progress and not yet complete.
    this.relatedProgramsAreLoading = ko.observable(false);

    this.ShowAllRelatedPrograms = ko.observable(false);

    // Limited list of related programs - must hit "more" to see all.
    this.DisplayRelatedPrograms = ko.computed(function () {
        if (self.ShowAllRelatedPrograms()) {
            return self.RelatedPrograms();
        }
        else {
            return self.RelatedPrograms.slice(0, 3);
        }
    });

    this.SetShowAllRelated = function () {
        self.ShowAllRelatedPrograms(true);
    }

    this.Notifications = ko.observableArray();

    // All programs available at institutiuon, for what-if, etc.  Usually loaded via an ajax call to the server.
    this.AllPrograms = ko.observableArray();

    // The degree plan in this view model is usually loaded dynamically via an ajax call to the server.
    this.DegreePlan = ko.observable();

    this.isLoading = ko.observable(false);
    this.LoadingMessage = ko.observable(stringEvaluatingProgramSpinner).syncWith("LoadingMessage", true);

    this.RemoveProgramClickHandler = function () {
        self.Programs.remove(self.CurrentProgram());
        self.CurrentProgram(self.Programs()[self.Programs().length - 1]);
        window.location.hash = self.CurrentProgram().Code();
    }

    this.CurrentProgram = ko.observable();

    this.SelectPreviousProgram = function () {
        var currentProgramIndex = self.Programs.indexOf(self.CurrentProgram());
        self.CurrentProgram(self.Programs()[currentProgramIndex - 1]);
        window.location.hash = self.CurrentProgram().Code();

        // hide currently seelcted detail
        this.hideRequirementDetail();
    }

    this.SelectNextProgram = function () {
        var currentProgramIndex = self.Programs.indexOf(self.CurrentProgram());
        self.CurrentProgram(self.Programs()[currentProgramIndex + 1]);
        window.location.hash = self.CurrentProgram().Code();

        // hide currently selected detail
        this.hideRequirementDetail();
    };

    // Set the window location hash and return the index of the current program.
    this.GetProgramIndexFromHash = function () {

        // Find the program in the list of programs based on the hash code
        var programCodeFromHash = (window.location.hash).substr(1);
        if (programCodeFromHash.length > 0) {
            for (var i = 0; i < self.Programs().length; i++) {
                if (self.Programs()[i].Code() == programCodeFromHash) {
                    window.location.hash = self.Programs()[i].Code();
                    return i;
                }
            }
        }
        // If the has value not present or not found, use the first program
        window.location.hash = self.Programs()[0].Code();
        return 0;
    };

    this.WhatIfDialogIsOpen = ko.observable(false);
    this.OpenWhatIfDialog = function () {
        // If there are related programs for this student and they have not yet been evaluated - kick off the ajax call to evaluate those.
        if (self.RelatedProgramCodes().length > 0 && self.RelatedPrograms().length == 0) {
            self.relatedProgramsAreLoading(true);
            $.ajax({
                url: evaluateRelatedProgramsUrl,
                data: JSON.stringify({ studentId: self.StudentId(), relatedPrograms: self.RelatedProgramCodes() }),
                type: "POST",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        data = $.parseJSON(data);
                        // For each studentProgramModelResult returned (there is a list) build the related program model.
                        for (var r = 0; r < data.length; r++) {
                            var studentProgramRequirementsModel = data[r];
                            var relatedResult = new programModel(studentProgramRequirementsModel.Program);
                            self.RelatedPrograms.push(relatedResult);
                        }
                    }
                    self.relatedProgramsAreLoading(false);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    self.relatedProgramsAreLoading(false);
                    if (jqXHR.status != 0)
                        $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve related program details.", type: "error" });
                }
            });
        }
        $("li.program").show();
        self.WhatIfDialogIsOpen(true);
        $("#programs-filter").val("");
        $("#programs-filternoresults").hide();
        if (self.AllPrograms().length == 0) {
            $("#programs-filternoresults").show();
        }
    }
  

    this.CancelWhatIfDialog = function () {
        self.ShowAllRelatedPrograms(false);
        self.WhatIfDialogIsOpen(false);
    }

    this.SelectedProgramCode = ko.observable();
    this.SelectedProgram = ko.observable();
    this.SelectedProgram.subscribe(function (data) {
        if (data) {
            self.SelectedProgramCode(data.Code());
        }
        else
        {
            self.SelectedProgramCode("");
        }
    });
    this.enableViewProgramButton = ko.pureComputed(function () {
        return !isNullOrEmpty(self.SelectedProgramCode());
    });
    this.Evaluate = function () {

        // If there is already a tab for this program, make it active, but don't re-run the what-if
        for (var i = 0; i < self.Programs().length; i++) {
            if (self.SelectedProgramCode() == self.Programs()[i].Code()) {
                self.CurrentProgram(self.Programs()[i]);
                window.location.hash = self.CurrentProgram().Code();
                self.WhatIfDialogIsOpen(false);
                return;
            }
        }

        // The program isn't loaded, so ask the server to run an evaluation and update the UI accordingly
        self.WhatIfDialogIsOpen(false);
        if (typeof stringEvaluatingProgramSpinner === "string") {
            self.LoadingMessage(stringEvaluatingProgramSpinner);
        }
        self.isLoading(true);
        var jsonData = { program: self.SelectedProgramCode(), isWhatIfEvaluation: true, studentId: self.StudentId() };
        $.ajax({
            url: programEvaluationUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            success: function (data) {
                data = $.parseJSON(data);
                if (!account.handleInvalidSessionResponse(data)) {
                    var result = new programModel(data.Program);
                    self.Programs.push(result);
                    for (var i = 0; i < data.Notifications.length; i++) {
                        var notification = data.Notifications[i];
                        $('#notificationHost').notificationCenter('addNotification', notification);
                    }
                    $("#programs-filter").val("");

                    // wrap this in a timer to allow the tabs to update fully, then set the current tab to the last one (that was just added)
                    setTimeout(function () {
                        self.CurrentProgram(self.Programs()[self.Programs().length - 1]);
                        window.location.hash = self.CurrentProgram().Code();
                    }, 0);
                }
                self.isLoading(false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to evaluate: " + program.Code(), type: "error" });
                self.isLoading(false);
            }
        });
    };

    this.delayedFilter = null;       //delay filter updates as you type
    this.filterDelayMs = 300;        //how long to delay filter updates in milliseconds

    // Wire up the filter search box
    this.setupProgramFilter = function () {
        //note that you cannot simply use jQuery .change() here as it only fires when textbox loses focus
        $(document).on("keyup search", "#programs-filter", function () {
            //reset delay
            window.clearTimeout(self.delayedFilter);
            //run as a delayed function so every quick typing action doesn't trigger a DOM walk/update
            self.delayedFilter = window.setTimeout(function () {
                var f = $("#programs-filter").val();
                //hide all of the programs and then only show the matches
                $("li.program").hide().filter(":containsany(" + f + ")").show();

                if ($("li.program").filter(":visible").length === 0) {
                    $("#programs-filternoresults").show();
                }
                else {
                    $("#programs-filternoresults").hide();
                }

                $("#programs-filter").focus();

                //reset delay
                window.clearTimeout(self.delayedFilter);
            }, self.filterDelayMs);
        });
    }

    this.updateEvaluations = ko.observable(false).syncWith("updateEvaluations");

    this.UpdateEvaluations = function () {
        if (self.updateEvaluations() === true) {
            if (typeof stringEvaluatingProgramSpinner === "string") {
                self.LoadingMessage(stringEvaluatingProgramSpinner);
            }
            self.isLoading(true);
            if (self.Programs().length > 0) {
                for (var i = 0; i < self.Programs().length; i++) {
                    var programCode = self.Programs()[i].Code();
                    var studentId = self.StudentId();
                    $.ajax({
                        url: programEvaluationUrl,
                        data: JSON.stringify({ program: programCode, isWhatIfEvaluation: false, studentId: studentId }),
                        type: "POST",
                        contentType: jsonContentType,
                        dataType: "json",
                        success: function (data) {
                            if (!account.handleInvalidSessionResponse(data)) {
                                data = $.parseJSON(data);
                                self.CurrentUserIsStudent(data.CurrentUserIsStudent);
                                self.StudentId(data.StudentId);
                                var result = new programModel(data.Program);

                                var index = null;
                                for (var j = 0; j < self.Programs().length; j++) {
                                    if (self.Programs()[j].Code() == result.Code()) {
                                        index = j;
                                        break;
                                    }
                                }

                                self.Programs.replace(self.Programs()[index], result);

                                self.CurrentProgram(ko.utils.arrayFirst(self.Programs(), function (program) {
                                    return program.Code() == self.CurrentProgram().Code();
                                }));

                                for (var i = 0; i < data.Notifications.length; i++) {
                                    var notification = ko.toJS(data.Notifications[i]);
                                    $('#notificationHost').notificationCenter('addNotification', notification);
                                }
                                self.updateEvaluations(false);
                                self.isLoading(false);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (jqXHR.status != 0)
                                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve program details.", type: "error" });
                        }
                    });
                }
            }
        }
    };

    // Happens when user clicks "back to requirements" link from mobile view
    this.hideRequirementDetail = function () {
        if (self.isMobile()) {
            // move sliding div offscreen and hide it
            $('#requirement-details-sliding').animate({ 'left': '480px' }, 1000, function () {
                $('#requirement-details-sliding').addClass('requirement-details-hide');
                $('.requirement-selected-container').addClass('requirement-hide').removeClass('requirement-selected-container');

                // hide requirement details
                $('.requirement-details').addClass('requirement-details-hide');

                // hide backlink
                $('section.requirement div.backlink').addClass('backlink-hide');

                // show all requirements (only headers remain)
                $('section.requirement').removeClass('requirement-hide');
                $('.requirement-header').show();

                // fix heights by removing style
                $('.requirements-details').removeAttr('style');
                
                // remove selected detail ID and style (positioning)
                $('#requirement-details-sliding').removeAttr('id style');

                // hide mobile header
                $('.requirement-header-mobile').hide();
            });
        }        
    }

    // Happens when user clicks the requirement in mobile view
    this.showRequirementDetail = function (context, event) {
        if (self.isMobile()) {
            // unhide only the selected requirement
            var currentDOMElement = event.target;
            var selectedSection = $(currentDOMElement).parents('section.requirement');
            $(selectedSection).removeClass('requirement-hide').addClass('requirement-selected-container');

            // unhide details, show mobile header
            $('.requirement-selected-container > .requirement-animation-wrapper > .requirement-details').removeClass('requirement-details-hide');
            $('.requirement-selected-container > .requirement-animation-wrapper > .requirement-header-mobile').show();

            // slide in, with adjustment to place over top of titles, add ID to selected details
            $('.requirement-selected-container > .requirement-animation-wrapper').attr('id', 'requirement-details-sliding');
            $('#requirement-details-sliding').css({ top: 0 + 'px', 'min-height': self.requirementMinHeight + 'px' });

            $('#requirement-details-sliding').animate({ 'left': '0px' }, 1000, function () {
                // fix heights
                var slidingHeight = $('#requirement-details-sliding').outerHeight();
                var backlinkHeight = $('#requirement-details-sliding > .backlink').outerHeight();
                var headingHeight = $('#requirement-details-sliding > .requirement-header-mobile h3').outerHeight();
                var gpaHeight = $('#requirement-details-sliding > .requirement-header-mobile .requirement-gpa').outerHeight();
                var footerHeight = $('footer').outerHeight();
                var heightTotal = (slidingHeight - backlinkHeight - headingHeight - gpaHeight) + footerHeight;
                                
                $('.requirements-details').outerHeight(heightTotal);
            });

            // unhide backlink for selected requirement
            $('#requirement-details-sliding > .backlink').removeClass('backlink-hide');
        }
    }

    this.GetEvaluationNotices = function (programCodeIn) {
        var jsonData = { studentId: self.StudentId(), programCode: programCodeIn };
        $.ajax({
            url: programEvaluationNoticesUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            success: function (data) {
                data = $.parseJSON(data);
                if (!account.handleInvalidSessionResponse(data)) {
                    // Find the program in the list and update the Notices
                    for (var i = 0; i < self.Programs().length; i++) {
                        if (self.Programs()[i].Code() == jsonData.programCode) {
                            // set flag indicating notices have been retrieved
                            self.Programs()[i].RetrievedNotices(true);
                            // If any data is returned, push it onto the notices and flag that it has notices
                            if (data.length > 0) {
                                self.Programs()[i].HasNotices(true);
                                for (var j = 0; j < data.length; j++) {
                                    self.Programs()[i].Notices.push(data[j]);
                                }
                                // Trigger the dialog to open when the notices are retrieved
                                self.Programs()[i].OpenNoticeDialog();
                            }
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to get notices for: " + program.Code(), type: "error" });
                self.isLoading(false);
            }
        });
    };

    //what if modal dialog buttons

    this.whatIfViewProgramButton = {
        id: 'whatifviewprogrambutton',
        title: Ellucian.Programs.Resources.ViewProgramButtonText,
        isPrimary: true,
        enabled: self.enableViewProgramButton,
        callback: self.Evaluate
    };

    this.whatIfCancelButton = {
        id: 'whatifcancelbutton',
        title: Ellucian.Programs.Resources.UiCancelButtonText,
        isPrimary: false,
        callback: self.CancelWhatIfDialog
    };
    
}