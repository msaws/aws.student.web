﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Provides KnockOut binding definitions for custom bindings that are specific to the Planning module.
// This includes bindings for the calendar, accordions, tabs, etc.
///////////////////////////////////////////////////////////////////////////////////////////////////////////


// custom binding for fullCalendar to display as a weekly schedule
ko.bindingHandlers.planningCalendar = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        // in this case, value is set to the currentTerm
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (typeof value === 'function') value = value();

        //default to 8am and then see if we can refine this based on the planned sections
        var firstHour = 8;
        try {
            firstHour = $.fullCalendar.formatDate(value[0].start, 'H');
            for (var i = 1; i < value.length; i++) {
                var eventStart = $.fullCalendar.formatDate(value[i].start, 'H');
                firstHour = eventStart <= firstHour ? eventStart - 1 : firstHour;
            }
        } catch (e) {
            firstHour = 8;
        }
        var clickHandler = allBindingsAccessor().planningCalendarClickHandler || {};
        var detailsClickHandler = allBindingsAccessor().planningCalendarSectionDetailsClickHandler || {};
        var height = allBindingsAccessor().planningCalendarScheduleHeight || null;
        var date = new Date();

        var options = {
            defaultView: 'agendaWeek',
            header: false,
            editable: false,
            theme: true,
            columnFormat: 'ddd',
            allDaySlot: false,
            eventTextColor: "black",
            events: value,
            firstHour: firstHour,
            removeClick: clickHandler,
            detailsClick: detailsClickHandler,
            year: date.getFullYear(),
            month: date.getMonth(),
            date: date.getDate(),
            weekends: true
        };
        if (height != null) options.height = height;

        $(element).fullCalendar(options);
    },
    update: function (element, valueAccessor, allBindingsAccessor) {

        // Remove any existing events
        $(element).fullCalendar('removeEvents');

        // in this case, value is set to the currentTerm
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (typeof value === 'function') value = value();

        for (var i = 0; i < value.length; i++) {
            $(element).fullCalendar( 'renderEvent', value[i]);
        }        
    }
};
