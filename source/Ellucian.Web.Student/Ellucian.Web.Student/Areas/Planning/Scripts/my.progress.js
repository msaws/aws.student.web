﻿
// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.using System;

//viewmodels and mappings are defined in requirements.mappings.js and program.requirements.view.model.js

var planRepository = Ellucian.Planning.planRepository;
var requirementsRepository = Ellucian.Planning.requirementsRepository;

var programRequirementsViewModelInstance = new programRequirementsViewModel(Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl, false);
var loadSamplePlanInstance = new loadSamplePlanViewModel();

function spinnerModel() {
    var self = this;
    this.isLoading = ko.observable(true);
}
spinner = new spinnerModel();

$(document).ready(function () {
    // bind something to the course search form, so the watermark binding can function
    ko.applyBindings({}, document.getElementById("search-form"));

    // bind loadSamplePlanInstance to the load sample dialog
    ko.applyBindings(loadSamplePlanInstance, document.getElementById("load-sample-plan-dialog"));

    // bind programRequirementsViewModelInstance to the view new program dialog
    ko.applyBindings(programRequirementsViewModelInstance, document.getElementById("what-if-dialog"));

    // Start the page loading spinner
    ko.applyBindings(spinner, document.getElementById("spinner"));

    // Get the student's program, plan, and other data so we can render the UI

    var degreePlanPromise = planRepository.get(currentUserId)
        .then(function (results) {
            var degreePlan = results[0];
            // Bind the degree plan to UI
            programRequirementsViewModelInstance.DegreePlan(new DegreePlan(degreePlan));
            loadSamplePlanInstance.DegreePlanId(programRequirementsViewModelInstance.DegreePlan().Id());

            // Update the planningTerms needed for sample plan load with the calculated list in the degree plan
            for (var i = 0; i < degreePlan.AvailableSamplePlanTerms.length; i++) {
                loadSamplePlanInstance.planningTerms.push(new Term(degreePlan.AvailableSamplePlanTerms[i]));
            }
        }).catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" })
        });

    var evaluationsPromise = requirementsRepository.get(currentUserId)
        .then(function (results) {

            var evaluations = results[0];
            var activePrograms = results[1];

                // If there are no evaluations, set the display accordingly
                if (evaluations.length == 0) {
                    programRequirementsViewModelInstance.noPrograms(true);
                } else {

                    // Set the UI based on who is using the system
                    programRequirementsViewModelInstance.CurrentUserIsStudent(evaluations[0].CurrentUserIsStudent);
                    programRequirementsViewModelInstance.StudentId(evaluations[0].StudentId);

                    // Add each evaluation to the local list of evaluations, sorted by title
                    for (var i = 0; i < evaluations.length; i++) {
                        var result = new programModel(evaluations[i].Program);
                        programRequirementsViewModelInstance.Programs.push(result);

                        // If an evaluation has an attached notification, add it to the notification center
                        for (var j = 0; j < evaluations[i].Notifications.length; j++) {
                            var notification = ko.toJS(evaluations[i].Notifications[j]);
                            $('#notificationHost').notificationCenter('addNotification', notification);
                        }
                    }

                    programRequirementsViewModelInstance.Programs.sort(function (left, right) {
                        return left.Title() == right.Title() ? 0 : (left.Title() < right.Title() ? -1 : 1)
                    });
                }

                // If there is at least 1 program - set current program
                if (programRequirementsViewModelInstance.Programs().length > 0) {
                    var programIndex = programRequirementsViewModelInstance.GetProgramIndexFromHash();
                    programRequirementsViewModelInstance.CurrentProgram(programRequirementsViewModelInstance.Programs()[programIndex]);
                }
                // Apply bindings after adding each program to improve performance
                ko.applyBindings(programRequirementsViewModelInstance, document.getElementById('myprogress-main'));

                // A program may have a list of related programs (Studio Art could be related to Art History)
                // Add these list of relatedProgramCodes in the model for later processing.
                for (var i = 0; i < evaluations.length; i++) {
                    if (evaluations[i].RelatedPrograms.length > 0) {
                        for (var t = 0; t < evaluations[i].RelatedPrograms.length; t++) {
                            programRequirementsViewModelInstance.RelatedProgramCodes.push(evaluations[i].RelatedPrograms[t]);
                        }
                    }
                }

                // Bind the list of active programs to the UI
                ko.mapping.fromJS(activePrograms, requirementMapping, programRequirementsViewModelInstance.AllPrograms);
                loadSamplePlanInstance.AllPrograms(programRequirementsViewModelInstance.AllPrograms());
                loadSamplePlanInstance.setupSamplePlansFilter();
        })
    .catch(function (error) {
        $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" })
    });

    Promise.all([degreePlanPromise, evaluationsPromise]).then(function () {
        spinner.isLoading(false);

            // if there is a starting selected program to show, select it in the tab set
            if (selectedProgram) {
                setTimeout(function () { $("#program-tabs ul li a[href^=\#" + selectedProgram.replace(stringTabCodeReplacement, '').replace('.', '') + "]").click(); }, 0);
            }

            // Now that the page is loaded with data, check if we need to adapt the page for mobile.
            programRequirementsViewModelInstance.checkForMobile(window, document);

            loadSamplePlanInstance.StudentPrograms(programRequirementsViewModelInstance.Programs());

    programRequirementsViewModelInstance.setupProgramFilter();
    });
});
