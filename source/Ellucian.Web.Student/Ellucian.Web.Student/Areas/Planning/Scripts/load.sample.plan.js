﻿// Copyright 2013-2014 Ellucian Company L.P. and its affiliates.
// This script drives the display of the "load sample plan" dialog, related ajax calls, and redirection to the degree plan

function loadSamplePlanViewModel() {
    var self = this;
   

    this.StudentPrograms = ko.observableArray();
    this.AllPrograms = ko.observableArray();

    this.SelectedProgramCode = ko.observable();
    this.SelectedProgram = ko.observable(null);
    this.SelectedProgram.subscribe(function (data) {
        if (data) {
            self.SelectedProgramCode(data.Code());
        }
        else
        {
            self.SelectedProgramCode("");
        }
    });
    this.SelectedProgramTitle = ko.computed(function () {
        var programSelected = self.SelectedProgramCode();
        var x = ko.utils.arrayFirst(self.AllPrograms(), function (item) {
            return (item.Code() == programSelected);
        });
        // When this computed item is registered there is no selected program code so x can come back null.
        if (x !== null) {
            return x.Title();
        }
        else {
            return "";
        }

    });
    this.dialogIsOpen = ko.observable(false);
    this.previewIsVisible = ko.observable(false);
    this.previewIsLoading = ko.observable(false);
    this.Terms = ko.observable();
    this.planningTerms = ko.observableArray().subscribeTo("planningTerms", true);
    this.selectedTerm = ko.observable();
    this.DegreePlan = ko.observable();
    this.DegreePlanId = ko.observable();
    this.showPreviewButton = ko.pureComputed(function () {
        return !self.previewIsLoading() && !self.previewIsVisible();
    });

    this.enablePreviewButton = ko.pureComputed(function () {
        return !isNullOrEmpty(self.SelectedProgramCode()) ;
    });
    //this is a topic to publish if sample plan is loaded to reflect on plan.view.model and requirements.view.model subscribers
    this.samplePlanLoaded = ko.observable(false).publishOn("samplePlanLoaded");

    this.CanAdvisorModifyPlan = ko.observable(true);
    this.toggleDialog = function () {
        if (self.dialogIsOpen() == false) {
            $("#sample-plans-filter").val("");
            $("li.sample-plan").hide();
            $("#sample-plan-filternoresults").hide();
            self.dialogIsOpen(true);
        }
        else {
            self.dialogIsOpen(false);
        }
    };

    this.LoadPlan = function () {
        // If no program selected, no-op.
        if (typeof self.DegreePlan() === 'undefined' || self.DegreePlan().length == 0) return;
        self.samplePlanLoaded(true);
        // serialize and post the current model data to the server and then specify a redirect href in the event that this is the advisor
        // instead of the student.
        ko.utils.postJson(loadSamplePlanUrl, { degreePlanJson: ko.toJS(self.DegreePlan), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });

    };

    this.PreviewPlan = function () {
        // If no program selected, no-op.
        if (typeof self.SelectedProgramCode() === 'undefined' || self.SelectedProgramCode().length == 0) return;
        self.previewIsLoading(true);

        $.ajax({
            url: previewSamplePlanUrl + '?program=' + self.SelectedProgramCode() + '&degreePlanId=' + self.DegreePlanId() + '&termCode=' + self.selectedTerm(),
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.previewIsLoading(false);
                    self.previewIsVisible(true);
                    ko.mapping.fromJS(data, {}, self);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 500) {
                    $('#notificationHost').notificationCenter('addNotification', { message: "Unable to generate preview for program: " + self.SelectedProgramTitle(), type: "error" });
                }
                else {
                    $('#notificationHost').notificationCenter('addNotification', { message: "No sample plan available for program: " + self.SelectedProgramTitle() + " and term: " + (self.selectedTerm()?self.selectedTerm():""), type: "error" });
                }
                self.CancelLoadPlan();
            }
        });
    };

    this.PreviewPlanBack = function () {
        self.SelectedProgramCode("");
        self.SelectedProgram(null);
        self.previewIsLoading(false);
        self.previewIsVisible(false);
    };

    this.CancelLoadPlan = function () {
        self.SelectedProgramCode("");
        self.SelectedProgram(null);
        self.previewIsLoading(false);
        self.previewIsVisible(false);
        self.toggleDialog();
    };

    this.delayedFilter = null;       //delay filter updates as you type
    this.filterDelayMs = 300;        //how long to delay filter updates in milliseconds

    // Wire up the filter search box
    this.setupSamplePlansFilter = function () {

        //note that you cannot simply use jQuery .change() here as it only fires when textbox loses focus
        $(document).on("keyup search", "#sample-plans-filter", function () {
            //reset delay
            window.clearTimeout(self.delayedFilter);

            //run as a delayed function so every quick typing action doesn't trigger a DOM walk/update
            self.delayedFilter = window.setTimeout(function () {
                var f = $("#sample-plans-filter").val();

                //hide all of the programs and then only show the matches
                $("li.sample-plan").hide();
                if (f.length > 1) {
                    $("li.sample-plan").filter(":containsany(" + f + ")").show();

                    if ($("li.sample-plan").filter(":visible").length === 0) {
                        $("#sample-plan-filternoresults").show();
                    }
                    else {
                        $("#sample-plan-filternoresults").hide();
                    }
                }
                $("#sample-plans-filter").focus();

                //reset delay
                window.clearTimeout(self.delayedFilter);
            }, self.filterDelayMs);
        });
    }

    this.isProxyUser = ko.observable().subscribeTo("isProxyUser", true);

    //load sample plan dialog buttons
    this.samplePlanCancelButton = {
        id: 'sampleplancancelbutton',
        title:Ellucian.Programs.Resources.UiCancelButtonText,
        isPrimary: false,
        callback: self.CancelLoadPlan
    };
    this.samplePlanPreviewButton = {
        id: 'sampleplanpreviewbutton',
        title: Ellucian.Programs.Resources.UiPreviewSamplePlanButtonText,
        isPrimary: true,
        visible: self.showPreviewButton,
        enabled: self.enablePreviewButton,
        callback: self.PreviewPlan
    };
    this.samplePlanBackButton = {
        id: 'sampleplanbackbutton',
        title: Ellucian.Programs.Resources.UiPreviewSamplePlanBackButtonText,
        isPrimary: false,
        visible: self.previewIsVisible,
        callback: self.PreviewPlanBack
    };
    this.samplePlanLoadButton = {
        id: 'sampleplanloadbutton',
        title: Ellucian.Programs.Resources.UiLoadSamplePlanButtonText,
        isPrimary: true,
        visible: self.previewIsVisible,
        callback: self.LoadPlan
    };

}