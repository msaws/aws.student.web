﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.


//navigation model specifically from grading
//this would have instances of overview, final and midterm grades.
var facultyGradingNavigationModel=function ()
{
   
    self = this;
    BaseViewModel.call(self, 769);
    // Overrides for base view model change functions to remove alerts - do nothing
    self.changeToDesktop = function () { }
    self.changeToMobile = function () { }

    this.areGradesRetrieved = ko.observable(false);
    this.SectionStartDate = ko.observable();
    this.SectionEndDate = ko.observable();
    this.displayGradeOverview = ko.observable(true);
    this.displayFinalGrades = ko.observable(false);
    this.displayMidterm1Grades = ko.observable(false);
    this.displayMidterm2Grades = ko.observable(false);
    this.displayMidterm3Grades = ko.observable(false);
    this.displayMidterm4Grades = ko.observable(false);
    this.displayMidterm5Grades = ko.observable(false);
    this.displayMidterm6Grades = ko.observable(false);
    this.showMidtermGrades = ko.computed(function () {
        return self.displayMidterm1Grades() || self.displayMidterm2Grades() || self.displayMidterm3Grades() || self.displayMidterm4Grades() || self.displayMidterm5Grades() || self.displayMidterm6Grades();
    });

     this.IsTermOpen = ko.observable(false);
    this.NumberOfMidtermGradesToShow = ko.observable(0);
    // keeps track of which midterm grade tab is "active" at any point. Used to show & store entered midterm in appropriate position.
     this.isGradeEntryPerformed = ko.observable(false);
     this.isWaitingForOverview = ko.observable(false);

     this.MobileGradeViews = ko.observableArray();
     this.mobileGradeViewSelected = ko.observable("O");
     this.mobileGradeViewSelected.subscribe(function (newValue) {
         switch (newValue) {
             case "O":
                 self.gradeOverviewTabSelected();
                 break;
             case "F":
                 self.finalGradesTabSelected();
                 break;
             case "M1":
                 self.midterm1GradesTabSelected();
                 break;
             case "M2":
                 self.midterm2GradesTabSelected();
                 break;
             case "M3":
                 self.midterm3GradesTabSelected();
                 break;
             case "M4":
                 self.midterm4GradesTabSelected();
                 break;
             case "M5":
                 self.midterm5GradesTabSelected();
                 break;
             case "M6":
                 self.midterm6GradesTabSelected();
                 break;
             default:
                 self.gradeOverviewTabSelected();
         }
     });

    //to track which tab was selected
     this.gradeOverviewTabSelected = function () {
         //make an ajax call here
         if (self.isGradeEntryPerformed() === true) {
             this.retrieveStudentGrades();
             self.isGradeEntryPerformed(false);
         }
         self.displayGradeOverview(true);
         self.displayFinalGrades(false);
         self.displayMidterm1Grades(false);
         self.displayMidterm2Grades(false);
         self.displayMidterm3Grades(false);
         self.displayMidterm4Grades(false);
         self.displayMidterm5Grades(false);
         self.displayMidterm6Grades(false);
     };
     this.finalGradesTabSelected = function () {
         self.displayGradeOverview(false);
         self.displayFinalGrades(true);
         self.displayMidterm1Grades(false);
         self.displayMidterm2Grades(false);
         self.displayMidterm3Grades(false);
         self.displayMidterm4Grades(false);
         self.displayMidterm5Grades(false);
         self.displayMidterm6Grades(false);

    };
    this.midterm1GradesTabSelected = function () {
        self.displayGradeOverview(false);
        self.displayFinalGrades(false);
        self.displayMidterm1Grades(true);
        self.displayMidterm2Grades(false);
        self.displayMidterm3Grades(false);
        self.displayMidterm4Grades(false);
        self.displayMidterm5Grades(false);
        self.displayMidterm6Grades(false);
    };
    this.midterm2GradesTabSelected = function () {
        self.displayGradeOverview(false);
        self.displayFinalGrades(false);
        self.displayMidterm1Grades(false);
        self.displayMidterm2Grades(true);
        self.displayMidterm3Grades(false);
        self.displayMidterm4Grades(false);
        self.displayMidterm5Grades(false);
        self.displayMidterm6Grades(false);
    };
    this.midterm3GradesTabSelected = function () {
        self.displayGradeOverview(false);
        self.displayFinalGrades(false);
        self.displayMidterm1Grades(false);
        self.displayMidterm2Grades(false);
        self.displayMidterm3Grades(true);
        self.displayMidterm4Grades(false);
        self.displayMidterm5Grades(false);
        self.displayMidterm6Grades(false);
    };
    this.midterm4GradesTabSelected = function () {
        self.displayGradeOverview(false);
        self.displayFinalGrades(false);
        self.displayMidterm1Grades(false);
        self.displayMidterm2Grades(false);
        self.displayMidterm3Grades(false);
        self.displayMidterm4Grades(true);
        self.displayMidterm5Grades(false);
        self.displayMidterm6Grades(false);
     };
    this.midterm5GradesTabSelected = function () {
        self.displayGradeOverview(false);
        self.displayFinalGrades(false);
        self.displayMidterm1Grades(false);
        self.displayMidterm2Grades(false);
        self.displayMidterm3Grades(false);
        self.displayMidterm4Grades(false);
        self.displayMidterm5Grades(true);
        self.displayMidterm6Grades(false);
    };
    this.midterm6GradesTabSelected = function () {
        self.displayGradeOverview(false);
        self.displayFinalGrades(false);
        self.displayMidterm1Grades(false);
        self.displayMidterm2Grades(false);
        self.displayMidterm3Grades(false);
        self.displayMidterm4Grades(false);
        self.displayMidterm5Grades(false);
        self.displayMidterm6Grades(true);
    };

    //mapping of student grades raw data to final grades model
    this.finalGradeMapping = {
        'StudentGrades': {
            create: function (options) {
                self.finalGradingModelInstance.addStudentGrade(new studentGradeModel(options.data, self.SectionStartDate(), self.SectionEndDate(),
                    //this is to pass grade scheme for the student if already have associated grade, this grade scheme is a combination of grade and a field that determines
                    //for the validity of expiration date and LDA
                    //we have to map a single grade text value to this object so that when data is retrieved, it maps to proper grade type in dropdown
                    //Item1 is Letter grade
                     (function () {
                         if (!isNullOrEmpty(options.data.FinalGrade)) {
                             return ko.utils.arrayFirst(self.finalGradingModelInstance.GradeTypes(), function (item) {
                                 return item.Item1() === options.data.FinalGrade;
                             });
                         }
                         else
                             return null;
                     })()
                    ));
            }
        }
    };


    //mapping of student grades raw data to midterm grades model
    this.midtermGradeMapping = {
        'StudentGrades': {
            create: function (options) {
                self.midtermGradingModelInstance.addStudentGrade(new studentMidTermGradeModel(options.data));
                

            }
        }
    };
    //instance for grading overview
    this.overviewGradingModelInstance=new overviewGradingModel();
    //instance for final grades
    this.finalGradingModelInstance = new finalGradingModel();

    //instance for midterm grades
    this.midtermGradingModelInstance = new midtermGradingModel();
    //instance for verify grades
    this.verifyGradingModelInstance = new verifyGradeModel(self, self.finalGradingModelInstance);
    //control notifications for each student
    this.notificationInstance = new notificationModel();

    this.retrieveStudentGrades=function() {
        $.ajax({
            url: facultySectionGradesUrl + "?sectionId=" + sectionId,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                self.isWaitingForOverview(true);
            },
            success: function (data) {
                //map data to overview student grades
                //sort
                ko.mapping.fromJS(data, {}, self.overviewGradingModelInstance);
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: stringUnableToRetrieveSection, type: "error" });
                }
            },
            complete: function (jqXHR, textStatus) {

                self.checkForMobile(window, document);
                $("#faculty-overview-grading-table").makeTableResponsive();
                setTimeout(function () {
                    self.isWaitingForOverview(false);
                }, 200);
            }
        });
    };
};

//prototype added to facultyGradingnavigation model to convert raw data to observable view models
// raw data is distributed across different view models
//hence fields are selectively mapped to other view models as per requirement
facultyGradingNavigationModel.prototype.fromJS = function (data) {
    ko.mapping.fromJS(data, { 'ignore': ['GradeTypes', 'StudentGrades'] }, this);
    ko.mapping.fromJS(data, { 'ignore': ['GradeTypes', 'SectionStartDate', 'SectionEndDate'] }, this.overviewGradingModelInstance);
    ko.mapping.fromJS(data, { 'include': ['GradeTypes', 'CanVerifyGrades', 'SectionId'], 'ignore': ['StudentGrades'] }, this.finalGradingModelInstance);
    ko.mapping.fromJS(data, { 'include': ['GradeTypes', 'SectionId'], 'ignore': ['StudentGrades'] }, this.midtermGradingModelInstance);
    ko.mapping.fromJS(data, this.finalGradeMapping);
    ko.mapping.fromJS(data, this.midtermGradeMapping);
    self.finalGradingModelInstance.StudentGrades(self.finalGradingModelInstance.studentGradesHolder);
    this.areGradesRetrieved(true);
};

////this is to register jquery events.
//happens as soon as fields in final grade entry is triggered for blur event i,e when tab or enter is pressed.
facultyGradingNavigationModel.prototype.registerEventHandlers = function () {
    // to track changes if moved to new field
    $("#faculty-final-grading-table .toTrackChanges").on("blur", function () {
        console.debug("on blur called");
        var context = ko.contextFor(this);
        context.$data.checkIfNeedToSave();
    });
};


//This view model is specific to overview display
var overviewGradingModel = function () {

    var self = this;
    this.StudentGrades = ko.observableArray([]);

    this.hasStudentGrades = ko.pureComputed(function () {
        return (self.StudentGrades().length > 0)
    });

    this.CanVerifyGrades = ko.observable(false); //based upon GRWP verify grades settings
    this.IsTermOpen = ko.observable(false); //based upon if TERM is open on GRWP settings.
    this.NumberOfMidtermGradesToShow = ko.observable(0); // based upon GRWP setting.
    this.InfoMessageToDisplay = ko.observable("");

    this.remainingGradesToBeProcessed = ko.pureComputed(function () {
        var allGradesCount = self.StudentGrades().length;
        if (self.IsTermOpen() === true) {

            if (self.CanVerifyGrades() === true) {
                //count for grades verified
                var count = ko.utils.arrayFilter(self.StudentGrades(), function (item) {
                    return item.IsGradeVerified() === true
                }).length;
                if (count < allGradesCount) {
                    self.InfoMessageToDisplay(VerifyGradeInformationMessage);
                }
                else {
                    self.InfoMessageToDisplay("");
                }
                return allGradesCount-count;
            }
            else {
                //count for final grades
                var count = ko.utils.arrayFilter(self.StudentGrades(), function (item) {
                    return !isNullOrEmpty(item.FinalGrade())
                }).length;

                if (count < allGradesCount) {
                    self.InfoMessageToDisplay(FinalGradeInformationMessage);
                }
                else {
                    self.InfoMessageToDisplay("");
                }
                return allGradesCount-count;
            }

        }
        else {
            self.InfoMessageToDisplay("");
            return 0;
        }


    });


};



//base student model used by student grades collection for final grades and
//student grades collection for mid term grades
var baseStudentGradeModel=function()
{
    var self = this;
    //if any observable is modified
    this.isFieldDirty = ko.observable(false);
    //when ajax is successfully done
    //will use the same flag for verification too
    this.isSuccessfullyUpdated = ko.observable(null);
    //while ajax call is made to show spinner
    //will use the same flag when verification call is made
    this.isWaiting = ko.observable(false);
    //any errors received from server during ajax call
    //this will also have any errors associated with verifiaction
    this.gradeEntryErrors = ko.observableArray([]);

    this.isSuccessfullyUpdated.subscribe(function (newValue) {
        if (newValue === true) {
            //notify 
            facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter(self.StudentId(), null);
            facultyGradingNavigationInstance.isGradeEntryPerformed(true);


        }
        else if (newValue === false) {
            facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter(self.StudentId(), self.gradeEntryErrors());
        }

    });


    this.postStudentGrade = function () {
        //post the data
        var json = { studentGrade: ko.toJSON(self) };
        var isPostSuccess = true;
        $.ajax({
            url: facultySectionStudentGradesUrl,
            data: JSON.stringify(json),
            type: "PUT",
            dataType: "json",
            contentType: jsonContentType,
            beforeSend: function () {
                self.isWaiting(true);
            },
            success: function (data, textStatus, jqXHR) {
                if (data.gradeEntryErrors == null || data.gradeEntryErrors.length == 0) {
                    self.gradeEntryErrors.removeAll();
                    self.isSuccessfullyUpdated(true);

                }
                else {
                    self.gradeEntryErrors.removeAll();
                    self.gradeEntryErrors.push(data.gradeEntryErrors);
                    self.isSuccessfullyUpdated(false);
                    //check if final grade saving happened. this should not run when saving midterm grades
                    if (self.hasOwnProperty('FinalGrade') && self.hasOwnProperty('IsGradeVerified')) {
                        if ((self.IsGradeVerified() === null || typeof self.IsGradeVerified == 'undefined' || self.IsGradeVerified() === false)) {
                            self.finalGradeSelected(null);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.textStatus != 0) {
                    self.gradeEntryErrors.removeAll();
                    self.gradeEntryErrors.push(FinalGradeSaveFailMessage);
                    self.isSuccessfullyUpdated(false);
                    //check if final grade saving happened
                    if (self.hasOwnProperty('FinalGrade') && self.hasOwnProperty('IsGradeVerified')) {
                        if ((self.IsGradeVerified() === null || typeof self.IsGradeVerified == 'undefined' || self.IsGradeVerified() === false)) {
                            self.finalGradeSelected(null);
                        }
                    }
                }
                isPostSuccess = false;
            },
            complete: function () {

                setTimeout(function () {
                    self.isWaiting(false);
                }, 200);

                self.isFieldDirty(false);
            }
        });
    };

}

baseStudentGradeModel.prototype.toJSON = function () {
    var copy = ko.toJS(this); //easy way to get a clean copy
    delete copy.isFieldDirty; //remove an extra properties
    delete copy.isSuccessfullyUpdated;
    delete copy.isWaiting;
    delete copy.__ko_mapping__;
    delete isValid;
    delete errors;
    delete gradeEntryErrors;
    return copy; //return the copy to be serialized
};