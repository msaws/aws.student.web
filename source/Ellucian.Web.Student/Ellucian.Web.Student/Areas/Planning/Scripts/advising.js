﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

var planRepository = Ellucian.Planning.planRepository;
var requirementsRepository = Ellucian.Planning.requirementsRepository;

//viewmodels and mappings for the requirements components are defined in requirements.mappings.js and program.requirements.view.model.js

var advisingViewModelInstance = new Ellucian.Planning.AdvisingViewModel(spinnerMessage, Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, planRepository);

// The program requirements don't interact with the advising portion of the view,
// so we use a separate view model and bind it independent of the advisng stuff
var programRequirementsViewModelInstance = new programRequirementsViewModel(Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, true);

// The catalog doesn't interact with the advising portion of the view,
// so we use a separate view models and bind them independent of the advising stuff
var catalogSearchSubjectsViewModelInstance = new catalogSearchSubjectsViewModel();
var catalogSearchCriteriaModelInstance = new catalogSearchCriteriaModel();
var catalogResultViewModelInstance = new catalogResultViewModel(Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, true, true, planRepository);

var loadSamplePlanInstance = new loadSamplePlanViewModel();

var planArchiveInstance = new planArchiveViewModel();

var testScoreInstance = new testResultViewModel();

var transcriptsInstance = new transcriptsViewModel();

var studentGradesInstance = new studentGradesViewModel();

function pageSpinnerModel() {
    var self = this;
    this.isLoading = ko.observable(true);
    this.start = function () {
        this.isLoading(true);
    }
    this.stop = function () {
        this.isLoading(false);
    }
}
var advisingTimelineSpinner = new pageSpinnerModel();
var advisingProgressSpinner = new pageSpinnerModel();
var advisingCatalogSpinner = new pageSpinnerModel();
var advisingNotesSpinner = new pageSpinnerModel();
var advisingArchiveSpinner = new pageSpinnerModel();
var advisingTestsSpinner = new pageSpinnerModel();
var advisingTranscriptsSpinner = new pageSpinnerModel();
var advisingGradesSpinner = new pageSpinnerModel();

var searchResult = null;
ko.postbox.serializer = ko.mapping.toJSON;
ko.postbox.subscribe("searchResult", function (result) {
    searchResult = result;
});

$(document).ready(function () {

    if (document.getElementById("advising-content-nav")) {

        $("#advising-content-nav").tabs({
            // whenever the advising tab changes, check to see which tab is now selected
            select: function (event, ui) {

                // Assume we're not looking at the schedule, set true later if we are.
                advisingViewModelInstance.isShowingSchedule = false;

                switch (ui.index) {
                    case 0: // The schedule/plan tab is selected
                        $("#dp-plans").scrollLeft();

                        advisingViewModelInstance.isShowingSchedule = true;

                        // Force render the calendar
                        setTimeout(function () { advisingViewModelInstance.currentTermCode.valueHasMutated(); }, 15);

                        break;
                    case 1: // The timeline tab is selected
                        //need a slight delay because this event is handled before the actual UI is switched to the new tab
                        setTimeout(function () { advisingViewModelInstance.ScrollToTerm(false) }, 200);
                        break;
                    case 2: // the progress tab is selected - check to see if the evaluations need to be refreshed
                        setTimeout(function () { programRequirementsViewModelInstance.UpdateEvaluations() }, 200);
                        break;
                    case 3: // The catalog tab is selected
                        getCourseCatalogSubjects();
                        break;
                    case 5: // The Plan Archive tab is selected
                        // Always get the latest archives
                        getDegreePlanArchives();
                        break;
                    case 6: // The Test Scores tab is selected
                        // Only do stuff if the test scores haven't been loaded yet...
                        if (!testScoreInstance.isLoaded()) {
                            // Start the data request
                            getTestScores();
                        }
                        break;
                    case 7: // The Unofficial Transcript tab is selected
                        // Only do stuff if the transcripts haven't been loaded yet...
                        if (!transcriptsInstance.isLoaded()) {
                            // Start the data request
                            getUnofficialTranscripts();
                        }
                        break;
                    case 8: // The grades tab is selected
                        // Only do stuff if the grades haven't been loaded yet...
                        if (!studentGradesInstance.retrieved()) {
                            // Start the data request
                            getStudentGrades();
                        }
                        break;
                    default:
                        // nothing
                }
            }
        });

        // Tabs are hidden by default to prevent flicker; show them after jQuery builds them out
        $("#advising-content-nav").show();

        // Start and bind spinners
        ko.applyBindings(advisingViewModelInstance, document.getElementById("advising-main"));
        advisingTimelineSpinner.start();
        ko.applyBindings(advisingTimelineSpinner, document.getElementById("advising-timeline-spinner"));
        advisingProgressSpinner.start();
        ko.applyBindings(advisingProgressSpinner, document.getElementById("advising-progress-spinner"));
        advisingCatalogSpinner.start();
        ko.applyBindings(advisingCatalogSpinner, document.getElementById("advising-catalog-spinner"));
        advisingNotesSpinner.start();
        ko.applyBindings(advisingNotesSpinner, document.getElementById("advising-notes-spinner"));
        advisingArchiveSpinner.start();
        ko.applyBindings(advisingArchiveSpinner, document.getElementById("plan-archives-spinner"));
        advisingTestsSpinner.start();
        ko.applyBindings(advisingTestsSpinner, document.getElementById("advising-tests-spinner"));
        advisingTranscriptsSpinner.start()
        ko.applyBindings(advisingTranscriptsSpinner, document.getElementById("advising-transcripts-spinner"));
        ko.applyBindings(advisingGradesSpinner, document.getElementById("advising-grades-spinner"));

        // First, map the various view models
        ko.mapping.fromJS(advisingViewModelJson, requirementMapping, advisingViewModelInstance);

        // bind loadSamplePlanInstance to the load sample dialog
        ko.applyBindings(loadSamplePlanInstance, document.getElementById("load-sample-plan-wrapper"));

        // bind programRequirementsViewModelInstance to the view new program dialog
        ko.applyBindings(programRequirementsViewModelInstance, document.getElementById("what-if-dialog"));

        // bind planArchiveInstance to the view of the plan archive
        ko.applyBindings(planArchiveInstance, document.getElementById("plan-archives-data"));

        // bind testScoreInstance to the view of the test scores
        ko.applyBindings(testScoreInstance, document.getElementById("advising-tests-data"));

        // bind transcriptsInstance to the view of the test scores
        ko.applyBindings(transcriptsInstance, document.getElementById("unofficial-transcripts"));

        // bind student grade instance to the view of the student grades
        ko.applyBindings(studentGradesInstance, document.getElementById("advising-grades-data"));

        // Get the student's data so we can render the UI
        var degreePlanPromise = planRepository.get(advisingViewModelJson.Advisee.Id)
            .then(function (results) {
                var degreePlan = results[0];
                // Map the degree plan JSON object to the view model instance
                advisingViewModelInstance.DegreePlan(ko.mapping.fromJS(degreePlan, planMapping));

                // Then set any attributes that drive the display
                var currentTerm = null;
                if (degreePlan.TermToShow.length > 0) {
                    currentTerm = ko.utils.arrayFilter(advisingViewModelInstance.DegreePlan().Terms(), function (term) {
                        return term.Code() == degreePlan.TermToShow;
                    })[0];
                } else {
                    currentTerm = advisingViewModelInstance.DegreePlan.Terms()[0];
                }
                advisingViewModelInstance.currentTermCode(currentTerm.Code());

                //ko.applyBindings(advisingViewModelInstance, document.getElementById("advising-main-data"));
                ko.applyBindings(advisingViewModelInstance, document.getElementById("advising-timeline-data"));
                ko.applyBindings(advisingViewModelInstance, document.getElementById("last-reviewed-info"));
                ko.applyBindings(advisingViewModelInstance, document.getElementById("advising-notes-data"));
                ko.applyBindings(advisingViewModelInstance, document.getElementById("dialogs"));

                // Map the programRequirementsViewModel Instance.
                programRequirementsViewModelInstance.DegreePlan(ko.mapping.fromJS(degreePlan, planMapping));

                loadSamplePlanInstance.DegreePlanId(programRequirementsViewModelInstance.DegreePlan().Id());
                loadSamplePlanInstance.CanAdvisorModifyPlan(advisingViewModelInstance.CanAdvisorModifyPlan());

                // load planningTerms with the AvailableSamplePlanTerms built into the degree plan
                var terms = [];
                for (var i = 0; i < degreePlan.AvailableSamplePlanTerms.length; i++) {
                    terms.push(new Term(degreePlan.AvailableSamplePlanTerms[i]));
                }
                advisingViewModelInstance.planningTerms.pushAll(terms);

            }).catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
            });

        var evaluationsPromise = requirementsRepository.get(advisingViewModelJson.Advisee.Id)
            .then(function (results) {

                var evaluations = results[0];
                var activePrograms = results[1];

                // If there are no evaluations, set the display accordingly
                if (evaluations.length == 0) {
                    programRequirementsViewModelInstance.noPrograms(true);
                    ko.applyBindings(programRequirementsViewModelInstance, document.getElementById("no-programs-notification"));
                } else {
                    // Set the UI based on who is using the system
                    programRequirementsViewModelInstance.CurrentUserIsStudent(evaluations[0].CurrentUserIsStudent);
                    programRequirementsViewModelInstance.StudentId(evaluations[0].StudentId);

                    // Add each evaluation to the local list of evaluations, sorted by title
                    for (var i = 0; i < evaluations.length; i++) {
                        var result = new programModel(evaluations[i].Program);
                        programRequirementsViewModelInstance.Programs.push(result);

                        // If an evaluation has an attached notification, add it to the notification center
                        for (var j = 0; j < evaluations[i].Notifications.length; j++) {
                            var notification = ko.toJS(evaluations[i].Notifications[j]);
                            $('#notificationHost').notificationCenter('addNotification', notification);
                        }
                        // A program may have a list of related programs (Studio Art could be related to Art History)
                        // Add these list of relatedProgramCodes in the model for later processing.
                        if (evaluations[i].RelatedPrograms.length > 0) {
                            programRequirementsViewModelInstance.RelatedProgramCodes.pushAll(evaluations[i].RelatedPrograms);
                        }
                    }
                    programRequirementsViewModelInstance.Programs.sort(function (left, right) {
                        return left.Title() == right.Title() ? 0 : (left.Title() < right.Title() ? -1 : 1)
                    });
                }

               
                // Map the programRequirementsViewModel Instance.
                loadSamplePlanInstance.StudentPrograms(programRequirementsViewModelInstance.Programs());

                // Bind active programs to the UI
                ko.mapping.fromJS(activePrograms, requirementMapping, programRequirementsViewModelInstance.AllPrograms);
                loadSamplePlanInstance.AllPrograms(programRequirementsViewModelInstance.AllPrograms());
                loadSamplePlanInstance.setupSamplePlansFilter();
            }).catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
            });

        Promise.all([degreePlanPromise, evaluationsPromise]).then(function () {

            // Determine whether student is eligible to register.  We want to show the advisor the message, but not actually limit registration.
            advisingViewModelInstance.RefreshRegistrationEligibility(advisingViewModelJson.Advisee.Id);

            // If there is at least 1 program - set current program
            if (programRequirementsViewModelInstance.Programs().length > 0) {
                var programIndex = programRequirementsViewModelInstance.GetProgramIndexFromHash();
                programRequirementsViewModelInstance.CurrentProgram(programRequirementsViewModelInstance.Programs()[programIndex]);
                programRequirementsViewModelInstance.updateEvaluations(false);
                ko.applyBindings(programRequirementsViewModelInstance, document.getElementById("advising-activeadvisee-programs"));
            }

            // Now that the page is loaded with data, check if we need to adapt the page for mobile.
            programRequirementsViewModelInstance.checkForMobile(window, document);

            loadSamplePlanInstance.StudentPrograms(programRequirementsViewModelInstance.Programs());

            ko.applyBindings(advisingViewModelInstance, document.getElementById("user-profile-header"));
            ko.applyBindings(advisingViewModelInstance, document.getElementById("advising-loading"));

            //show any notifications, but only ones that came from the Advising model - do NOT show notifications from the degree audit
            $('#notificationHost').notificationCenter('addNotification', ko.toJS(advisingViewModelInstance.Notification));

            // Set up what if program dialog filtering
            programRequirementsViewModelInstance.setupProgramFilter();

            ko.applyBindings(catalogSearchSubjectsViewModelInstance, document.getElementById("catalog-index"));
            ko.applyBindings(catalogResultViewModelInstance, document.getElementById("catalog-result"));
            ko.applyBindings(catalogResultViewModelInstance, document.getElementById("search-form"));

            //textbox management
            $("#coursecatalog-filter").watermark(" Type a subject...");
            $("#coursecatalog-filter").click(function () { this.select(); });

            // Show the main div, it's hidden until we get most of the bindings completed (this stops partly rendered page elements from displaying)
            advisingTimelineSpinner.stop();
            advisingNotesSpinner.stop();
            advisingProgressSpinner.stop();

            advisingViewModelInstance.isPageLoading(false);
            $("#advising-content-nav").show();
            $("#other-dialogs").show();
            $("#load-sample-plan-wrapper").show();

        });
    }
});

function getCourseCatalogSubjects() {
    $.ajax({
        url: Ellucian.Course.ActionUrls.getSubjectsActionUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {

        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                var subjects = [];
                for (var i = 0; i < data.length; i++) {
                    var subject = new subjectModel(data[i], Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, true);
                    subjects.push(subject);
                }
                catalogSearchSubjectsViewModelInstance.subjects(subjects);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve subject list.", type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            catalogSearchSubjectsViewModelInstance.isLoaded(true);
            advisingCatalogSpinner.stop();
        }
    });
}

function getUnofficialTranscripts() {
    $.ajax({
        url: unofficialTranscriptsActionUrl + "?studentId=" + advisingViewModelJson.Advisee.Id,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                transcriptsInstance.transcriptGroupings.pushAll(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve transcript data", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
            advisingTranscriptsSpinner.stop();
            transcriptsInstance.isLoaded(true);
        }
    });
}

function getTestScores() {
    $.ajax({
        url: testScoresActionUrl + "?studentId=" + advisingViewModelJson.Advisee.Id,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, testScoreMapping, testScoreInstance);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve test scores", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
            advisingTestsSpinner.stop();
            testScoreInstance.isLoaded(true);
        }
    });
}

function getDegreePlanArchives() {
    $.ajax({
        url: getPlanArchivesUrl + "?degreePlanId=" + advisingViewModelInstance.DegreePlan().Id(),
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {
            advisingArchiveSpinner.start();
            planArchiveInstance.isLoaded(false);
            planArchiveInstance.archives.removeAll();
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                planArchiveInstance.archives.pushAll(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve archive", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
            advisingArchiveSpinner.stop();
            planArchiveInstance.isLoaded(true);
        }
    });
}

function getStudentGrades() {
    advisingGradesSpinner.start()
    $.ajax({
        url: getStudentGradeInfoUrl + "?studentId=" + advisingViewModelJson.Advisee.Id,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, gradeMapping, studentGradesInstance);
                // set first term to show true - rest will be collapsed by default.
                if (studentGradesInstance.StudentTermGrades().length > 0) {
                    studentGradesInstance.StudentTermGrades()[0].showTermDetail(true);
                }
                $(".student-grade-table").makeTableResponsive();
                // No need to add grade restriction notications to the notification center in advisor view.
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            advisingGradesSpinner.stop();
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve student grade information", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
            advisingGradesSpinner.stop();
            studentGradesInstance.retrieved(true);
        }
    });
}