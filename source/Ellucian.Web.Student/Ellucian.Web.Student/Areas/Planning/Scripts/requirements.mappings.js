﻿/////////////////////////////////////////////////////////////////////////////////////////////
//  This file contains mappings Requirements objects. These
//  are used anywhere in the UI where a degree audit and/or degree requirements UI is 
//  presented.
/////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
//  Main Requirements Page
var requirementMapping = {
    'Programs': {
        create: function (options) {
            return new programModel(options.data);
        }
    },
    'Requirements': {
        create: function (options) {
            return new requirementModel(options.data);
        }
    },
    'Subrequirements': {
        create: function (options) {
            return new subrequirementModel(options.data);
        }
    },
    'Groups': {
        create: function (options) {
            return new groupModel(options.data);
        }
    },
    'CoursesThatNeedPlanned': {
        create: function (options) {
            return new courseModel(options.data);
        }
    },
    'AppliedAcademicCredits': {
        create: function (options) {
            return new creditModel(options.data);
        }
    },
    'AppliedPlannedCourses': {
        create: function (options) {
            return new courseModel(options.data);
        }
    },
    'NotAppliedAcademicCredits': {
        create: function (options) {
            return new creditModel(options.data);
        }
    },
    'NotAppliedPlannedCredits': {
        create: function (options) {
            return new courseModel(options.data);
        }
    },
    'AcademicCreditsIncludedInGPA': {
        create: function (options) {
            return new creditModel(options.data);
        }
    },
};

function programModel(data) {
    ko.mapping.fromJS(data, requirementMapping, this);

    var self = this;

    this.DescriptionDialogIsOpen = ko.observable(false);
    this.OpenDescriptionDialog = function () {
        self.DescriptionDialogIsOpen(true);
    }
    this.DescriptionDialogOptions = {
        autoOpen: false,
        modal: true,
        width: $(window).width() / 2,
        resizable: false,
        show: { effect: "fade", duration: dialogFadeTimeMs },
        hide: { effect: "fade", duration: dialogFadeTimeMs }
        // Buttons are done inline in the markup for styling purposes
    };

    this.CloseDescriptionDialog = function () {
        self.DescriptionDialogIsOpen(false);
    };

    this.DescriptionDisplay = ko.computed(function () {
        var desc = self.Description();
        var foo = "<p>" + desc.replace(new RegExp("\r\n\r\n", "g"), "</p><p>") + "</p>";
        return foo;
    });

    this.DescriptionShort = ko.computed(function () {
        var desc = self.DescriptionDisplay();
        var wordLimit = 20;
        var finalText = "";
        var split = desc.split(' ');
        var numberOfWords = split.length;
        var i = 0;
        if (numberOfWords > wordLimit) {
            for (i = 0; i < wordLimit; i++) {
                if (finalText.length == 0) {
                    finalText = split[i] + " ";
                }
                else {
                    finalText = finalText + " " + split[i] + " ";
                }
            }
            return finalText;
        }
        else return desc;
    });

    this.TotalCreditsForGraph = ko.computed(function () {
        var tot = self.CompletedCredits() + self.PlannedCredits() + self.InProgressCredits();
        var min = self.MinimumCredits();
        return tot > min ? tot : min;
    });

    this.TotalStudentCredits = ko.computed(function () {
        var tot = self.CompletedCredits() + self.InProgressCredits();
        var min = self.MinimumCredits();
        return tot > min ? min : tot;
    });

    this.TotalStudentProgressPercent = ko.computed(function () {
        if (self.MinimumCredits() === 0) return 0;
        return self.TotalStudentCredits() / self.MinimumCredits() * 100;
    });

    this.TotalInstitutionalCreditsForGraph = ko.computed(function () {
        var tot = self.CompletedInstitutionalCredits() + self.PlannedInstitutionalCredits() + self.InProgressInstitutionalCredits();
        var min = self.MinimumInstitutionalCredits();
        return tot > min ? tot : min;
    });

    this.CompletedCreditsPercent = ko.computed(function () {
        if (self.TotalCreditsForGraph() === 0) return 0;
        return Math.round(self.CompletedCredits() / self.TotalCreditsForGraph() * 100);
    });

    this.CompletedInstitutionalCreditsPercent = ko.computed(function () {
        if (self.TotalInstitutionalCreditsForGraph() === 0) return 0;
        return Math.round( self.CompletedInstitutionalCredits() / self.TotalInstitutionalCreditsForGraph() * 100);
    });

    this.PlannedCreditsPercent = ko.computed(function () {
        if (self.TotalCreditsForGraph() === 0) return 0;
        return Math.round(self.PlannedCredits() / self.TotalCreditsForGraph() * 100);
    });
    this.PlannedInstitutionalCreditsPercent = ko.computed(function () {
        if (self.TotalInstitutionalCreditsForGraph() === 0) return 0;
        return Math.round(self.PlannedInstitutionalCredits() / self.TotalInstitutionalCreditsForGraph() * 100);
    });

    this.InProgressCreditsPercent = ko.computed(function () {
        if (self.TotalCreditsForGraph() === 0) return 0;
        return Math.round(self.InProgressCredits() / self.TotalCreditsForGraph() * 100);
    });
    this.InProgressInstitutionalCreditsPercent = ko.computed(function () {
        if (self.TotalInstitutionalCreditsForGraph() === 0) return 0;
        return Math.round(self.InProgressInstitutionalCredits() / self.TotalInstitutionalCreditsForGraph() * 100);
    });

    // ProgressParts and InstitutionalProgressParts return the parts required for the multi-part progress bars
    this.ProgressParts = function () {
        var completedCredits = self.CompletedCredits();
        var inProgressCredits = self.InProgressCredits();
        var plannedCredits = self.PlannedCredits();
        var parts = [];
        parts.push({ value: self.CompletedCreditsPercent(), text: self.CompletedCredits(), barClass: 'completed-progress', title: self.CompletedCredits() + ' credits completed.' });
        parts.push({ value: self.InProgressCreditsPercent(), text: self.InProgressCredits(), barClass: 'inprogress-progress', title: self.InProgressCredits() + ' credits in progress.' });
        parts.push({ value: self.PlannedCreditsPercent(), text: self.PlannedCredits(), barClass: 'planned-progress', title: self.PlannedCredits() + ' credits planned.' });
        return parts;
    };
    this.InstitutionalProgressParts = function () {
        var parts = [];
        parts.push({ value: self.CompletedInstitutionalCreditsPercent(), text: self.CompletedInstitutionalCredits(), barClass: 'completed-progress', title: self.CompletedInstitutionalCredits() + ' institutional credits completed.' });
        parts.push({ value: self.InProgressInstitutionalCreditsPercent(), text: self.InProgressInstitutionalCredits(), barClass: 'inprogress-progress', title: self.InProgressInstitutionalCredits() + ' institutional credits in progress.' });
        parts.push({ value: self.PlannedInstitutionalCreditsPercent(), text: self.PlannedInstitutionalCredits(), barClass: 'planned-progress', title: self.PlannedInstitutionalCredits() + ' institutional credits planned.' });
        return parts;
    };
    
    // Progress bar, used by Home page, At a Glance and Related Programs (fastest path to completion in View a New Program dialog)
    this.ProgramCompletionProgress = function () {
        var parts = [];
        parts.push({ value: self.CompletedRequirementPercent(), barClass: 'completed-progress' });
        return parts;
    };

    // Links and dialogs to show Evaluation Notices
    // Indicates if ajax call to retrieve notices successfully completed
    this.RetrievedNotices = ko.observable(false);
    // Indicates if retrieval has been done and notices were returned
    this.HasNotices = ko.observable(false);
    // Link to dialog to show program notices
    this.ShowEvaluationNotices = ko.observable(true);
    this.ToggleEvaluationNotices = function () {
        self.ShowEvaluationNotices() == true ? self.ShowEvaluationNotices(false) : self.ShowEvaluationNotices(true);
    }

    this.NoticeDialogIsOpen = ko.observable(false);
    this.OpenNoticeDialog = function () {
        self.NoticeDialogIsOpen(true);
    }
}

function requirementModel(data) {
    ko.mapping.fromJS(data, requirementMapping, this);

    var self = this;

    this.requirementDetailIsDisplayed = ko.observable((function shouldRequirementDetailBeDisplayed() {
        if (detailsCollapsedByDefaultWhenFullyPlanned) {
            return self.CompletionStatus() !== "Completed" && self.CompletionStatus() !== "Waived" && self.PlanningStatus() !== "CompletelyPlanned";
        }
        return self.CompletionStatus() !== "Completed" && self.CompletionStatus() !== "Waived";
    })());
    this.ToggleRequirementDetail = function () {
        self.requirementDetailIsDisplayed(!self.requirementDetailIsDisplayed());
    }
    this.requirementsDetailAriaText = ko.computed(function () {
        return self.requirementDetailIsDisplayed() ? hideRequirementsDetailAriaText : showRequirementsDetailAriaText;
    });
    this.requirementsDetailLinkText = ko.computed(function () {
        return self.requirementDetailIsDisplayed() ? hideRequirementsDetailLinkText : showRequirementsDetailLinkText;
    });

    // Returns the string value of the class to apply to requirement's directive
    this.DirectiveStyle = ko.computed(function () {
        switch (self.CompletionStatus()) {
            case "Waived":
                return "requirement-directive-waived";
            case "Completed":
                return "requirement-directive-complete";
        }
        // No known status, must be incomplete
        return "requirement-directive-incomplete";
    });

    // Returns the string value of the class to apply to requirement's planning directive
    this.PlanningDirectiveStyle = ko.computed(function () {
        return self.PlanningStatus() == "CompletelyPlanned" ? "requirement-directive-planned" : "requirement-directive-remaining";
    });

}

function subrequirementModel(data) {
    ko.mapping.fromJS(data, requirementMapping, this);

    var self = this;

    this.requirementDetailIsDisplayed = ko.observable((function shouldSubRequirementDetailBeDisplayed()
    {
        if (detailsCollapsedByDefaultWhenFullyPlanned) {
            return self.CompletionStatus() !== "Completed" && self.CompletionStatus() !== "Waived" && self.PlanningStatus() !== "CompletelyPlanned";
        }
        return self.CompletionStatus() !== "Completed" && self.CompletionStatus() !== "Waived";
    })());
    
    this.ToggleRequirementDetail = function () {
        self.requirementDetailIsDisplayed(!self.requirementDetailIsDisplayed());
    }
    this.requirementsDetailAriaText = ko.computed(function () {
        return self.requirementDetailIsDisplayed() ? hideRequirementsDetailAriaText : showRequirementsDetailAriaText;
    });
    this.requirementsDetailLinkText = ko.computed(function () {
        return self.requirementDetailIsDisplayed() ? hideRequirementsDetailLinkText : showRequirementsDetailLinkText;
    });

    // Returns the string value of the class to apply to requirement's directive
    this.DirectiveStyle = ko.computed(function () {
        switch (self.CompletionStatus()) {
            case "Waived":
                return "subrequirement-directive-waived";
            case "Completed":
                return "subrequirement-directive-complete";
        }
        // No known status, must be incomplete
        return "subrequirement-directive-incomplete";
    });

    // Returns the string value of the class to apply to subrequirement's planning directive
    this.PlanningDirectiveStyle = ko.computed(function () {
        return self.PlanningStatus() == "CompletelyPlanned" ? "subrequirement-directive-planned" : "subrequirement-directive-incomplete";
    });

    this.GroupStyle = ko.computed(function () {
        return self.Groups().length > 1 ? "groups" : "groups-no-list-style";
    });

    this.CodeText = ko.computed(function () {
        return this.Code().length > 0 ? this.Code() : "Complete the following";
    }, this);
}

function groupModel(data) {
    ko.mapping.fromJS(data, requirementMapping, this);

    var self = this;

    this.groupDetailIsDisplayed = ko.observable((function shouldGroupDetailBeDisplayed()
    {
        if (detailsCollapsedByDefaultWhenFullyPlanned) {
            return self.CompletionStatus() !== "Completed" && self.CompletionStatus() !== "Waived" && self.PlanningStatus() !== "CompletelyPlanned";
        }
        return self.CompletionStatus() !== "Completed" && self.CompletionStatus() !== "Waived";
    })());

    this.ToggleGroupDetail = function () {
        self.groupDetailIsDisplayed(!self.groupDetailIsDisplayed());
    }
    this.requirementsDetailAriaText = ko.computed(function () {
        return self.groupDetailIsDisplayed() ? hideRequirementsDetailAriaText : showRequirementsDetailAriaText;
    });
    this.requirementsDetailLinkText = ko.computed(function () {
        return self.groupDetailIsDisplayed() ? hideRequirementsDetailLinkText : showRequirementsDetailLinkText;
    });

    this.CodeText = ko.computed(function () {
        return this.Code().length > 0 ? this.Code() : "Complete the following";
    }, this);

    // Returns the string value of the class to apply to group's directive
    this.DirectiveStyle = ko.computed(function () {
        var style;
        switch (self.CompletionStatus()) {
            case "Waived":
                style = "group-directive-waived";
                break;
            case "Completed":
                style = "group-directive-complete";
                break;
            default:
                // No known status, must be incomplete
                style = "group-directive-incomplete";
                if (self.PlanningDirective().length == 0) {
                    style += " group-directive-no-left-padding";
                }
                break;
        }

        return style;
    });

    // Returns the string value of the class to apply to group's planning directive
    this.PlanningDirectiveStyle = ko.computed(function () {
        return self.PlanningStatus() == "CompletelyPlanned" ? "group-directive-planned" : "group-directive-incomplete";
    });
}

// Course link handler when course ID is available.
function courseModel(data) {
    ko.mapping.fromJS(data, requirementMapping, this);

    var self = this;
}

// Academic credit link handler when course ID is available.
function creditModel(data) {
    ko.mapping.fromJS(data, requirementMapping, this);
    var self = this;
    this.creditStatusStyle = ko.computed(function () {
        switch (self.DisplayStatus()) {
            case "Attempted":
                return "group-attempted";
            case "Withdrawn":
                return "group-withdrawn";
            case "Completed":
                return "group-completed";
            case "InProgress":
                return "group-inprogress";
            case "Preregistered":
                return "group-preregistered";
            default:
                return "";
        }
    });

    this.creditStatusText = ko.computed(function () {
        switch (self.DisplayStatus()) {
            case "Attempted":
                return creditAttemptedText;
            case "Withdrawn":
                return creditWithdrawnText;
            case "Completed":
                return creditCompletedText;
            case "InProgress":
                return creditInProgressText;
            case "Preregistered":
                return creditPreregisteredText;
            default:
                return "";
        }
    });
}
