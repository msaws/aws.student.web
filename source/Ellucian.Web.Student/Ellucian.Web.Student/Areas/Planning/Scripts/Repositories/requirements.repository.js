﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.Planning = Ellucian.Planning || {};

Ellucian.Planning.requirementsRepository = (function () {

    // The base keys will have student's ID appended (as needed by each repository method)
    // Advisors access many students, so we don't have a single key
    var baseEvaluationsKey = "evaluations";
    var baseActiveProgramsKey = "activeprograms";
    var storage = Ellucian.Storage.session;

    return {

        get: function (studentId) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.requirementsRepositoryMessages.studentIdRequired);
            }


            // If an ID was provided, set the url and storage key appropriately
            var getEvaluationsUrl = Ellucian.Planning.requirementsRepositoryUrls.get + "?studentId=" + studentId;
            var evaluationsKey = baseEvaluationsKey + studentId;
            var activeProgramsKey = baseActiveProgramsKey + studentId;


            var evaluations = storage.getItem(evaluationsKey);
            var activePrograms = storage.getItem(activeProgramsKey);


            if (evaluations !== null && activePrograms !== null) {
                return Promise.resolve([evaluations, activePrograms]);
            } else {
                return new Promise(function (resolve, reject) {
                    var xhr = $.ajax({
                        url: getEvaluationsUrl,
                        type: "GET",
                        contentType: jsonContentType,
                        dataType: "json"
                    })
                    .done(function (data) {
                        if (!account.handleInvalidSessionResponse(data)) {
                            storage.setItem(evaluationsKey, data.Evaluations);
                            storage.setItem(activeProgramsKey, data.ActivePrograms);

                            var evaluations = typeof data.Evaluations === "string" ? JSON.parse(data.Evaluations) : data.Evaluations;
                            resolve([evaluations, data.ActivePrograms]);
                        }
                        reject("Unable to retrieve programs.");
                    })
                    .fail(function () {
                        reject("Unable to retrieve programs.");
                    });
                });
            }
        },

        removeSessionKeys: function (studentId) {
            var evaluationsKey = baseEvaluationsKey + studentId;
            var activeProgramsKey = baseActiveProgramsKey + studentId;
            storage.removeItem(evaluationsKey);
            storage.removeItem(activeProgramsKey);
        }
    }
})();