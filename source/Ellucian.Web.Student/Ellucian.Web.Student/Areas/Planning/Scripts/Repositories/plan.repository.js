﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
var Ellucian = Ellucian || {};
Ellucian.Planning = Ellucian.Planning || {};

Ellucian.Planning.planRepository = (function () {

    // The base keys will have student's ID appended (as needed by each repository method)
    // Advisors access many students, so we don't have a single key
    var baseDegreePlanKey = "degreeplan";
    var baseStudentProgramsKey = "studentprograms";
    var storage = Ellucian.Storage.session;

    var degreePlanKey = "";
    var updateSucceeded = function (data) {
        if (!account.handleInvalidSessionResponse(data)) {
            storage.setItem(degreePlanKey, data);
            return Promise.resolve(data);
        }
        return Promise.reject(Ellucian.Planning.planRepositoryMessages.updateFailed);
    };


    var updateFailed = function (jqXHR) {
        storage.removeItem(degreePlanKey);
        if (jqXHR.status == 409) { // Failed because the local copy of the plan is out-of-date
            return { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
        } else { // Don't know the cause, use a more generic message
            return { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
        }
    };

    return {

        // Get a degree plan
        get: function (studentId) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

           var degreePlanKey = baseDegreePlanKey + studentId;
            var studentProgramsKey = baseStudentProgramsKey + studentId;

            var degreePlan = storage.getItem(degreePlanKey);
            var studentPrograms = storage.getItem(studentProgramsKey);

            // If data is in storage, we're done
            if (degreePlan !== null && studentPrograms !== null) {
                return Promise.resolve([degreePlan, studentPrograms]);
            } else {
                return new Promise(function (resolve, reject) {
                    $.ajax({
                        url: Ellucian.Planning.planRepositoryUrls.get + "?studentId=" + studentId,
                        type: "GET",
                        contentType: jsonContentType,
                        dataType: "json"
                    })
                    .done(function (data) {
                        if (!account.handleInvalidSessionResponse(data)) {
                            storage.setItem(degreePlanKey, data.DegreePlan);
                            storage.setItem(studentProgramsKey, data.StudentPrograms);
                            resolve([data.DegreePlan, data.StudentPrograms]);
                        }
                        reject(Ellucian.Planning.planRepositoryMessages.getFailed);
                    })
                    .fail(function () {
                        reject(Ellucian.Planning.planRepositoryMessages.getFailed);
                    });
                });
            }
        },

        // Registration methods
        register: function (studentId, sectionRegistrations) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // sectionRegistrations must be array
            if (!sectionRegistrations || typeof sectionRegistrations instanceof Array) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                sectionRegistrations: sectionRegistrations, studentId: studentId
            };

            var registrationTimingStart;
            registrationTimingStart = moment();

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.registerSections,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        var registrationTimingEnd = moment();
                        var registrationTimeElapsed = registrationTimingEnd - registrationTimingStart;
                        allTrackUserTiming("Registration", "SectionRegistration", registrationTimeElapsed);
                        resolve(data);
                    }
                    reject(Ellucian.Planning.planRepositoryMessages.updateFailed);
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        completeRegistration: function (studentId, notifications) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                studentId: studentId, notifications: notifications
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.completeRegistration,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function () {
                    reject(Ellucian.Planning.planRepositoryMessages.updateFailed);
                });
            });
        },

        // Methods for Terms
        addTerm: function (studentId, termId, plan) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // termId and plan are required
            if (!termId || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                addTermId: termId, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.addTerm,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        removeTerm: function (studentId, termId, plan) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // termId and plan are required
            if (!termId || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                removeTermId: termId, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.removeTerm,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },

        // Methods for Courses
        addCourse: function (studentId, courseId, termId, credits, plan) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // courseId, termId, credits and plan are required
            if (!courseId || !termId || !credits || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;
            // Remove old plan from storage
            storage.removeItem(degreePlanKey);


            var jsonData = {
                courseId: courseId, termId: termId, credits: credits, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.addCourse,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {

                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        updateCourse: function (studentId, courseId, oldTermId, newTermId, plan) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // courseId, oldTermId, newTermId, and plan are required
            if (!courseId || !oldTermId || !newTermId || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);


            var jsonData = {
                courseId: courseId, oldTerm: oldTermId, newTerm: newTermId, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.updateCourse,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json",
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        removeCourse: function (studentId, courseId, termId, sectionId, plan) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // courseId, termId, and plan are required
            if (!courseId || !termId || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                removeCourseId: courseId, removeCourseTermId: termId, removeCourseSectionId: sectionId, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.removeCourse,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },

        // Methods for Sections
        addSection: function (studentId, courseId, termId, sectionId, gradingType, credits, plan) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // courseId, termId, sectionId, gradingType, credits and plan are required
            if (!courseId || !termId || !sectionId || !gradingType || !credits || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                courseId: courseId, termId: termId, sectionId: sectionId, gradingType: gradingType, credits: credits, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.addSection,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        removeSection: function (studentId, sectionId, plan) {

            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // sectionId and plan are required
            if (!sectionId || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                removeSectionId: sectionId, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.removeSection,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json",

                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },

        // Methods for Advising
        requestReview: function (studentId, plan) {
            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // plan is required
            if (!plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.requestReview,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json",

                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        completeReview: function (studentId, plan) {
            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // plan is required
            if (!plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);
            var jsonData = { degreePlanJson: plan };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.completeReview,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        submitNote: function (studentId, note, type, plan) {
            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // note, type, and plan are required
            if (!note || !type || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                message: note, personType: type, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.submitNote,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        approveCourses: function (studentId, term, courses, status, plan) {
            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // term, courses, status, and plan are required
            if (!term || !courses || !status || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                term: term,
                courses: courses,
                status: status,
                degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.approveCourses,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                    .done(function (data) {
                        resolve(updateSucceeded(data));
                    })
                    .fail(function (jqXHR) {
                        reject(updateFailed(jqXHR));
                    });
            });
        },
        archivePlan: function (studentId, plan) {
            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // plan is required
            if (!plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.archivePlan,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json",

                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        updateProtection: function (studentId, courses, protectionType, plan) {
            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // courses, protectionType, and plan are required
            if (!courses || typeof protectionType !== "boolean" || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                courses: courses, protectedState: protectionType, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.updateProtection,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },

        // Clear courses from specified terms
        clearPlan: function (studentId, terms, plan) {
            // Student ID must be provided.
            if (!studentId || studentId.length === 0) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.studentIdRequired);
            }

            // terms and plan are required
            if (!terms || !plan) {
                return Promise.reject(Ellucian.Planning.planRepositoryMessages.argumentError);
            }

            degreePlanKey = baseDegreePlanKey + studentId;

            // Remove old plan from storage
            storage.removeItem(degreePlanKey);

            var jsonData = {
                clearTermIds: terms, degreePlanJson: plan
            };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.Planning.planRepositoryUrls.clearPlannedCourses,
                    data: JSON.stringify(jsonData),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                })
                .fail(function (jqXHR) {
                    reject(updateFailed(jqXHR));
                });
            });
        },
        removeSessionKeys: function (studentId) {
            var degreePlanKey = baseDegreePlanKey + studentId;
            var studentProgramsKey = baseStudentProgramsKey + studentId;

            var degreePlan = storage.removeItem(degreePlanKey);
            var studentPrograms = storage.removeItem(studentProgramsKey);
        }

    }
})();