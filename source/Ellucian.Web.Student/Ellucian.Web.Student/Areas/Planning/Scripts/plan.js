﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

var planRepository = Ellucian.Planning.planRepository;

var planViewModelInstance = new Ellucian.Planning.PlanViewModel(false, Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl, planRepository);
var loadSamplePlanInstance = new loadSamplePlanViewModel();
//include program requirements subscriber model
var programRequirementsSubscriberModelInstance = new Ellucian.Planning.programRequirementsSubscriberModel();

ko.postbox.serializer = ko.mapping.toJSON;

function retrievePetitionsWaivers() {
    //Get student petitions and faculty consents
    $.ajax({
        url: getStudentPetitionsWaiversActionUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (retrievedData) {
            if (!account.handleInvalidSessionResponse(retrievedData)) {
                ko.mapping.fromJS(retrievedData, planMapping, planViewModelInstance.studentPetitionsWaiversInstance);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve programs", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {
            planViewModelInstance.studentPetitionsWaiversInstance.isLoaded(true);
            $(".student-petition-table").makeTableResponsive();
            $(".student-waiver-table").makeTableResponsive();
        }
    });

}

// Activates knockout bindings
$(document).ready(function () {
    // Start the page loading spinner
    ko.applyBindings(planViewModelInstance, document.getElementById("spinner"));

    // bind something to the course search form, so the watermark binding can function
    ko.applyBindings({}, document.getElementById("search-form"));

    // bind loadSamplePlanInstance to the load sample dialog
    ko.applyBindings(loadSamplePlanInstance, document.getElementById("load-sample-plan-dialog"));

    // Parse out any arguments from the URL
    var urlArguments = getUrlVars();

    // Register notifications provided up front
    if (notification) {
        $('#notificationHost').notificationCenter('addNotification', notification);
        planViewModelInstance.Notification(notification);
    }

    // Get the degree plan
    var degreePlanPromise = planRepository.get(currentUserId)
        .then(function (results) {

            var degreePlan = results[0];
            var studentPrograms = results[1];

            // Map the degree plan JSON object to the view model instance
            planViewModelInstance.DegreePlan(ko.mapping.fromJS(degreePlan, planMapping));

            // if a current term was passed in the URL, use it as the term to show
            if (urlArguments && urlArguments.currentTerm) {
                degreePlan.TermToShow = decodeURIComponent(urlArguments.currentTerm);
            }

            // Select active term on schedule
            if (!isNullOrEmpty(degreePlan.TermToShow)) {
                for (var i = 0; i < planViewModelInstance.DegreePlan().Terms().length; i++) {
                    if (planViewModelInstance.DegreePlan().Terms()[i] !== null && planViewModelInstance.DegreePlan().Terms()[i].Code() === degreePlan.TermToShow) {
                        planViewModelInstance.currentTermCode(planViewModelInstance.DegreePlan().Terms()[i].Code());
                        break;
                    }
                }
            }

            // Bind the view model to the main plan DOM element
            ko.applyBindings(planViewModelInstance, document.getElementById("schedplan-outer"));

            // Set the degree plan id in the sample plan instance
            loadSamplePlanInstance.DegreePlanId(planViewModelInstance.DegreePlan().DegreePlanDto.Id);

            // if a current view was specified in the URL, set the current tab appropriately
            if (urlArguments && urlArguments.currentView) {
                $("#schedplan-tabs").tabs("option", "selected", urlArguments.currentView);
            }

            // Get all terms available for planning
            planViewModelInstance.updatePlanningTerms(degreePlan.AvailableSamplePlanTerms);

            // Map the student programs into the loadSamplePlanInstance for display when loading a sample plan from plan page
            ko.mapping.fromJS(studentPrograms, {}, loadSamplePlanInstance.StudentPrograms);

            // Now that the page is loaded with degree plan data, check if we need to adapt the page for mobile.
            planViewModelInstance.checkForMobile(window, document);

            // Determine whether student is eligible to register, and whether the user has eligibility override
            planViewModelInstance.RefreshRegistrationEligibility();

            // Get all active programs for use in the sample plan instance
            $.ajax({
                url: getActiveProgramsUrl,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        ko.mapping.fromJS(data, requirementMapping, loadSamplePlanInstance.AllPrograms);
                        loadSamplePlanInstance.setupSamplePlansFilter();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve programs", type: "error" });
                    }
                }
            });

            planViewModelInstance.isPageLoading(false);
            $("#other-dialogs").show();
        }).catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" })
        });

    //register to detect which tab is selected; the first time we switch to the timeline, scroll to active term
    $("#schedplan-tabs").bind("tabsselect", function (event, ui) {
        planViewModelInstance.isShowingSchedule = (ui.index === 0);
        var timelineScroll = -1;
        if (planViewModelInstance.isShowingSchedule) {
            timelineScroll = $("#dp-plans").scrollLeft();
        }
        if (!planViewModelInstance.isShowingSchedule) {
            //need a slight delay because this event is handled before the actual UI is switched to the new tab
            setTimeout(function () { planViewModelInstance.ScrollToTerm(false) }, 200);
        } else if (!planViewModelInstance.isShowingSchedule && timelineScroll >= 0) {
            //this is necessary to keep IE9 from always resetting the timeline back to the beginning as you toggle between it and the schedule
            setTimeout(function () { $("#dp-plans").scrollLeft(timelineScroll) }, 50);
        }
        //if petitions and waivers is selected then make an ajax request only first time
        if (ui.index === 3) {
            //retrieve petitions and waivers
            if (!planViewModelInstance.studentPetitionsWaiversInstance.isLoaded()) {
                retrievePetitionsWaivers();
            }
        }
    });
});