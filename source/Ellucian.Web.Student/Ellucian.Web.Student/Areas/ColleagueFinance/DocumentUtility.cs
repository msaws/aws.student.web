﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Infrastructure;

namespace Ellucian.Web.Student.Areas.ColleagueFinance
{
    public static class DocumentUtility
    {
        /// <summary>
        /// Asynchronously get a Purchase Order object.
        /// </summary>
        /// <param name="purchaseOrderId">Purchase Order ID</param>
        /// <param name="serviceClient">Service Client</param>
        /// <param name="logger">ILogger</param>
        /// <returns>Purchase Order in View Model format</returns>
        public static async Task<PurchaseOrderViewModel> GetPurchaseOrderAsync(string purchaseOrderId, string generalLedgerAccountId, ColleagueApiClient serviceClient, ILogger logger)
        {
            try
            {

                // Get the purchase order document, get the AP types so that we can get a description,
                // and get the tax code descriptions.
                var purchaseOrderViewModelTask = serviceClient.GetPurchaseOrderAsync(purchaseOrderId);
                var apTypesTask = ServiceClientCache.GetCachedAccountsPayableTypes(serviceClient);
                var taxesTask = ServiceClientCache.GetCachedAccountsPayableTaxes(serviceClient);
                var generalLedgerTask = serviceClient.GetGeneralLedgerAccountAsync(generalLedgerAccountId);
                await Task.WhenAll(purchaseOrderViewModelTask, apTypesTask, taxesTask, generalLedgerTask);
                var purchaseOrderViewModel = new PurchaseOrderViewModel(purchaseOrderViewModelTask.Result);
                var generalLedgerAccount = generalLedgerTask.Result;
                var apTypes = apTypesTask.Result;
                var taxes = taxesTask.Result;

                // Get the GL number, formatted GL number, and GL number description.
                purchaseOrderViewModel.GeneralLedgerAccountNumberId = generalLedgerAccountId;
                purchaseOrderViewModel.GeneralLedgerAccountNumberDescription = generalLedgerAccount.Description;
                purchaseOrderViewModel.FormattedAccountNumber = generalLedgerAccount.FormattedId;

                // Only resolve the AP Type if the PO has an AP Type.
                if (apTypes != null)
                {
                    if (!string.IsNullOrEmpty(purchaseOrderViewModel.ApType))
                    {
                        purchaseOrderViewModel.ApType = purchaseOrderViewModel.ApType + " " + apTypes.Where(x => x.Code == purchaseOrderViewModel.ApType).FirstOrDefault().Description;
                    }
                }

                // Get the tax code descriptions
                if (taxes != null)
                {
                    if (purchaseOrderViewModel.LineItems != null)
                    {
                        foreach (var lineItem in purchaseOrderViewModel.LineItems)
                        {
                            if (lineItem.TaxDistributions != null)
                            {
                                foreach (var tax in lineItem.TaxDistributions)
                                {
                                    if (!string.IsNullOrEmpty(tax.TaxCode))
                                    {
                                        tax.TaxCodeDescription = taxes.Where(x => x.Code == tax.TaxCode).FirstOrDefault().Description;
                                    }
                                }
                            }
                        }
                    }
                }

                //Get the reference number for the associated requisitions
                if (purchaseOrderViewModel.Requisitions != null)
                {
                    foreach (var reqViewModel in purchaseOrderViewModel.Requisitions)
                    {
                        if (reqViewModel != null)
                        {
                            var poRequisition = await serviceClient.GetRequisitionAsync(reqViewModel.Id);
                            if (poRequisition != null)
                            {
                                reqViewModel.Number = poRequisition.Number;
                            }
                        }
                    }
                }

                return purchaseOrderViewModel;
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Asynchronously get a Voucher object.
        /// </summary>
        /// <param name="voucherId">Voucher ID</param>
        /// <param name="serviceClient">Service Client</param>
        /// <param name="logger">ILogger</param>
        /// <returns>Voucher in View Model format</returns>
        public static async Task<VoucherViewModel> GetVoucherAsync(string voucherId, string generalLedgerAccountId, ColleagueApiClient serviceClient, ILogger logger)
        {
            try
            {
                // Get the voucher document, get the AP types so that we can get a description,
                // and get the tax code descriptions.
                var voucherViewModelTask = serviceClient.GetVoucher2Async(voucherId);
                var apTypesTask = ServiceClientCache.GetCachedAccountsPayableTypes(serviceClient);
                var taxesTask = ServiceClientCache.GetCachedAccountsPayableTaxes(serviceClient);
                var generalLedgerTask = serviceClient.GetGeneralLedgerAccountAsync(generalLedgerAccountId);
                await Task.WhenAll(voucherViewModelTask, apTypesTask, taxesTask, generalLedgerTask);
                var voucherViewModel = new VoucherViewModel(voucherViewModelTask.Result);
                var generalLedgerAccount = generalLedgerTask.Result;
                var apTypes = apTypesTask.Result;
                var taxes = taxesTask.Result;

                // Get the GL number, formatted GL number, and GL number description.
                voucherViewModel.GeneralLedgerAccountNumberId = generalLedgerAccountId;
                voucherViewModel.GeneralLedgerAccountNumberDescription = generalLedgerAccount.Description;
                voucherViewModel.FormattedAccountNumber = generalLedgerAccount.FormattedId;

                // Only resolve the AP Type if the Voucher has an AP Type.
                if (apTypes != null)
                {
                    if (!string.IsNullOrEmpty(voucherViewModel.ApType))
                    {
                        voucherViewModel.ApType = voucherViewModel.ApType + " " + apTypes.Where(x => x.Code == voucherViewModel.ApType).FirstOrDefault().Description;
                    }
                }

                if (taxes != null)
                {
                    if (voucherViewModel.LineItems != null)
                    {
                        foreach (var lineItem in voucherViewModel.LineItems)
                        {
                            if (lineItem.TaxDistributions != null)
                            {
                                foreach (var tax in lineItem.TaxDistributions)
                                {
                                    if (!string.IsNullOrEmpty(tax.TaxCode))
                                    {
                                        tax.TaxCodeDescription = taxes.Where(x => x.Code == tax.TaxCode).FirstOrDefault().Description;
                                    }
                                }
                            }
                        }
                    }
                }

                // Get the reference number for the associated document.
                switch (voucherViewModel.AssociatedDocumentType)
                {
                    case FinanceDocumentType.PurchaseOrder:
                        var voucherPurchaseOrder = await serviceClient.GetPurchaseOrderAsync(voucherViewModel.AssociatedDocument);
                        if (voucherPurchaseOrder != null)
                        {
                            voucherViewModel.AssociatedDocumentReferenceNumber = voucherPurchaseOrder.Number;
                        }
                        break;
                    case FinanceDocumentType.BlanketPurchaseOrder:
                        var voucherBpo = await serviceClient.GetBlanketPurchaseOrderAsync(voucherViewModel.AssociatedDocument);
                        if (voucherBpo != null)
                        {
                            voucherViewModel.AssociatedDocumentReferenceNumber = voucherBpo.Number;
                        }
                        break;
                    case FinanceDocumentType.RecurringVoucher:
                        voucherViewModel.AssociatedDocumentReferenceNumber = voucherViewModel.AssociatedDocument;
                        break;
                }

                return voucherViewModel;
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Asynchronously get a Recurring Voucher object.
        /// </summary>
        /// <param name="recurringVoucherId">Recurring Voucher ID</param>
        /// <param name="serviceClient">Service Client</param>
        /// <param name="logger">ILogger</param>
        /// <returns>Recurring Voucher in View Model format</returns>
        public static async Task<RecurringVoucherViewModel> GetRecurringVoucherAsync(string recurringVoucherId, string generalLedgerAccountId, ColleagueApiClient serviceClient, ILogger logger)
        {
            try
            {
                // Get the recurring voucher document, and the AP types to get the AP type description.
                var recurringVoucherViewModelTask = serviceClient.GetRecurringVoucherAsync(recurringVoucherId);
                var apTypesTask = ServiceClientCache.GetCachedAccountsPayableTypes(serviceClient);
                var generalLedgerTask = serviceClient.GetGeneralLedgerAccountAsync(generalLedgerAccountId);
                await Task.WhenAll(recurringVoucherViewModelTask, apTypesTask, generalLedgerTask);
                var recurringVoucherDto = recurringVoucherViewModelTask.Result;
                var recurringVoucherViewModel = new RecurringVoucherViewModel(recurringVoucherDto);
                var apTypes = apTypesTask.Result;
                var generalLedgerAccount = generalLedgerTask.Result;

                // Get the GL number, formatted GL number, and GL number description.
                recurringVoucherViewModel.GeneralLedgerAccountNumberId = generalLedgerAccountId;
                recurringVoucherViewModel.GeneralLedgerAccountNumberDescription = generalLedgerAccount.Description;
                recurringVoucherViewModel.FormattedAccountNumber = generalLedgerAccount.FormattedId;

                // Only resolve the AP Type if the recurring voucher has an AP Type.
                if (apTypes != null)
                {
                    if (!string.IsNullOrEmpty(recurringVoucherViewModel.ApType))
                    {
                        recurringVoucherViewModel.ApType = recurringVoucherViewModel.ApType + " " + apTypes.Where(x => x.Code == recurringVoucherViewModel.ApType).FirstOrDefault().Description;
                    }
                }

                // Loop through every schedule in the View Model and get the voucher information.
                if (recurringVoucherDto != null)
                {
                    foreach (var scheduleDto in recurringVoucherDto.Schedules)
                    {
                        if (!string.IsNullOrEmpty(scheduleDto.VoucherId))
                        {
                            var voucherDto = await serviceClient.GetVoucher2Async(scheduleDto.VoucherId);
                            var scheduleVM = recurringVoucherViewModel.Schedules.Where(x => x.VoucherId == scheduleDto.VoucherId).FirstOrDefault();

                            if (scheduleVM != null)
                            {
                                scheduleVM.VoucherDueDate = voucherDto.DueDate.HasValue ? voucherDto.DueDate.Value.ToShortDateString() : "";
                                scheduleVM.VoucherStatus = voucherDto.Status.ToString();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(scheduleDto.PurgedVoucherId))
                            {
                                var scheduleVM = recurringVoucherViewModel.Schedules.Where(x => x.PurgedVoucherId == scheduleDto.PurgedVoucherId).FirstOrDefault();

                                if (scheduleVM != null)
                                {
                                    scheduleVM.VoucherDueDate = string.Empty;
                                    scheduleVM.VoucherStatus = VoucherStatus.Purged.ToString();
                                }
                            }
                        }
                    }
                }

                return recurringVoucherViewModel;
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Asynchronously get a Blanket Purchase Order object.
        /// </summary>
        /// <param name="blanketPurchaseOrderId">Blanket Purchase Order ID</param>
        /// <param name="serviceClient">Service Client</param>
        /// <param name="logger">ILogger</param>
        /// <returns>Blanket Purchase Order in View Model format</returns>
        public static async Task<BlanketPurchaseOrderViewModel> GetBlanketPurchaseOrderAsync(string blanketPurchaseOrderId, string generalLedgerAccountId, ColleagueApiClient serviceClient, ILogger logger)
        {
            try
            {
                // Get the blanket purchase order document, and the AP types to get the AP type description.
                var blanketPurchaseOrderViewModelTask = serviceClient.GetBlanketPurchaseOrderAsync(blanketPurchaseOrderId);
                var apTypesTask = ServiceClientCache.GetCachedAccountsPayableTypes(serviceClient);
                var generalLedgerTask = serviceClient.GetGeneralLedgerAccountAsync(generalLedgerAccountId);
                await Task.WhenAll(blanketPurchaseOrderViewModelTask, apTypesTask, generalLedgerTask);
                var blanketPurchaseOrderViewModel = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderViewModelTask.Result);
                var apTypes = apTypesTask.Result;
                var generalLedgerAccount = generalLedgerTask.Result;

                // Get the GL number, formatted GL number, and GL number description.
                blanketPurchaseOrderViewModel.GeneralLedgerAccountNumberId = generalLedgerAccountId;
                blanketPurchaseOrderViewModel.GeneralLedgerAccountNumberDescription = generalLedgerAccount.Description;
                blanketPurchaseOrderViewModel.FormattedAccountNumber = generalLedgerAccount.FormattedId;

                // Only resolve the AP Type if the BPO has an AP Type.
                if (apTypes != null)
                {
                    if (!string.IsNullOrEmpty(blanketPurchaseOrderViewModel.ApType))
                    {
                        blanketPurchaseOrderViewModel.ApType = blanketPurchaseOrderViewModel.ApType + " " + apTypes.Where(x => x.Code == blanketPurchaseOrderViewModel.ApType).FirstOrDefault().Description;
                    }
                }

                // Get the reference number for the associated requisitions
                if (blanketPurchaseOrderViewModel.Requisitions != null)
                {
                    foreach (var reqViewModel in blanketPurchaseOrderViewModel.Requisitions)
                    {
                        if (reqViewModel != null)
                        {
                            var requisition = await serviceClient.GetRequisitionAsync(reqViewModel.Id);
                            if (requisition != null)
                            {
                                reqViewModel.Number = requisition.Number;
                            }
                        }
                    }
                }

                return blanketPurchaseOrderViewModel;
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Asynchronously get a Requisition object.
        /// </summary>
        /// <param name="requisitionId">Requisition ID</param>
        /// <param name="serviceClient">Service Client</param>
        /// <param name="logger">ILogger</param>
        /// <returns>Requisition in View Model format</returns>
        public static async Task<RequisitionViewModel> GetRequisitionAsync(string requisitionId, string generalLedgerAccountId, ColleagueApiClient serviceClient, ILogger logger)
        {
            try
            {
                // Get the requisition document, get the AP types so that we can get a description,
                // and get the tax code descriptions.
                var requisitionViewModelTask = serviceClient.GetRequisitionAsync(requisitionId);
                var apTypesTask = ServiceClientCache.GetCachedAccountsPayableTypes(serviceClient);
                var taxesTask = ServiceClientCache.GetCachedAccountsPayableTaxes(serviceClient);
                var generalLedgerTask = serviceClient.GetGeneralLedgerAccountAsync(generalLedgerAccountId);
                await Task.WhenAll(requisitionViewModelTask, apTypesTask, taxesTask, generalLedgerTask);
                var requisitionViewModel = new RequisitionViewModel(requisitionViewModelTask.Result);
                var apTypes = apTypesTask.Result;
                var taxes = taxesTask.Result;
                var generalLedgerAccount = generalLedgerTask.Result;

                // Get the GL number, formatted GL number, and GL number description.
                requisitionViewModel.GeneralLedgerAccountNumberId = generalLedgerAccountId;
                requisitionViewModel.GeneralLedgerAccountNumberDescription = generalLedgerAccount.Description;
                requisitionViewModel.FormattedAccountNumber = generalLedgerAccount.FormattedId;

                // Only resolve the AP Type if the requisition has an AP Type.
                if (apTypes != null)
                {
                    if (!string.IsNullOrEmpty(requisitionViewModel.ApType))
                    {
                        requisitionViewModel.ApType = requisitionViewModel.ApType + " " + apTypes.Where(x => x.Code == requisitionViewModel.ApType).FirstOrDefault().Description;
                    }
                }

                // Get the tax code descriptions
                if (taxes != null)
                {
                    if (requisitionViewModel.LineItems != null)
                    {
                        foreach (var lineItem in requisitionViewModel.LineItems)
                        {
                            if (lineItem.TaxDistributions != null)
                            {
                                foreach (var tax in lineItem.TaxDistributions)
                                {
                                    if (!string.IsNullOrEmpty(tax.TaxCode))
                                    {
                                        tax.TaxCodeDescription = taxes.Where(x => x.Code == tax.TaxCode).FirstOrDefault().Description;
                                    }
                                }
                            }
                        }
                    }
                }

                // Get the reference number for the associated purchase orders
                if (requisitionViewModel.PurchaseOrders != null)
                {
                    foreach (var poViewModel in requisitionViewModel.PurchaseOrders)
                    {
                        if (poViewModel != null)
                        {
                            var reqPoNumber = await serviceClient.GetPurchaseOrderAsync(poViewModel.Id);
                            if (reqPoNumber != null)
                            {
                                poViewModel.Number = reqPoNumber.Number;
                            }
                        }
                    }
                }

                // Get the reference number for the associated blanket purchase order
                if (requisitionViewModel.BlanketPurchaseOrderId != null)
                {
                    var reqBpoNumber = await serviceClient.GetBlanketPurchaseOrderAsync(requisitionViewModel.BlanketPurchaseOrderId);
                    if (reqBpoNumber != null)
                    {
                        requisitionViewModel.BlanketPurchaseOrderNumber = reqBpoNumber.Number;
                    }
                }

                return requisitionViewModel;
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Asynchronously get a Journal Entry object.
        /// </summary>
        /// <param name="journalEntryId">Journal Entry ID</param>
        /// <param name="serviceClient">Service Client</param>
        /// <param name="logger">ILogger</param>
        /// <returns>Journal Entry in View Model format</returns>
        public static async Task<JournalEntryViewModel> GetJournalEntryAsync(string journalEntryId, string generalLedgerAccountId, ColleagueApiClient serviceClient, ILogger logger)
        {
            try
            {
                var jeDtoTask = serviceClient.GetJournalEntryAsync(journalEntryId);
                var generalLedgerTask = serviceClient.GetGeneralLedgerAccountAsync(generalLedgerAccountId);
                await Task.WhenAll(jeDtoTask, generalLedgerTask);
                var jeViewModel = new JournalEntryViewModel(jeDtoTask.Result);
                var generalLedgerAccount = generalLedgerTask.Result;

                // Get the GL number, formatted GL number, and GL number description.
                jeViewModel.GeneralLedgerAccountNumberId = generalLedgerAccountId;
                jeViewModel.GeneralLedgerAccountNumberDescription = generalLedgerAccount.Description;
                jeViewModel.FormattedAccountNumber = generalLedgerAccount.FormattedId;

                return jeViewModel;
            }
            catch (Exception e)
            {
                // log the error and send back a more generic http status code
                logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}