﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// Javascript representation of a cost center
function costCenterModel(data) {
    var self = this;

    ko.mapping.fromJS(data, costCenterMapping, self);

    // "Inherit" the base cost center model to make use of common computed observables.
    baseCostCenterModel.call(self);

    // costCenterBarGraphData returns the "parts" required for the multi-part bar-graph
    // Only one "part" is required, but many may be used when appropriate
    // value = the % complete for each part
    // text = text to display inside the bar graph
    // barClass = the css styling to apply to the part (so each part can be a different color, etc)
    // title = text used in the tooltip for the bar graph
    self.costCenterBarGraphData = function () {
        var parts = [];
        var budgetBarColor = "bar-graph bar-graph--success";
        if (self.BudgetRemaining() < 0) {
            budgetBarColor = "bar-graph bar-graph--error";
        }
        else if (self.BudgetSpentPercent() >= 85 && self.BudgetSpentPercent() <=  100) {
            budgetBarColor = "bar-graph bar-graph--warning";
        }

        if (self.BudgetSpentPercent) {
            parts.push({ value: self.BudgetSpentPercent(), barClass: budgetBarColor, title: costCenterBarGraphTitle });
        }
        
        return parts;
    }

    self.costCenterRevenueBarGraphData = function () {
        var parts = [];
        var budgetBarColor = "bar-graph bar-graph--hint";
        
        if (self.BudgetSpentPercentRevenue) {
            parts.push({ value: self.BudgetSpentPercentRevenue(), barClass: budgetBarColor, title: costCenterBarGraphTitle });
        }
        
        return parts;
    }
};