﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// Set up viewModel instance
var costCenterDetailViewModelInstance = new costCenterDetailViewModel();

// Register the FilterControl component
ko.components.register('filter-summary', {
    require: 'FilterSummary/_FilterSummary'
});

// Register CsvDownloadLink component
ko.components.register('csv-download-link', {
    require: 'CsvDownloadLink/_CsvDownloadLink'
});

// Once the page has loaded...
$(document).ready(function () {
    ko.applyBindings(costCenterDetailViewModelInstance, document.getElementById("main"));

    costCenterDetailViewModelInstance.IsCostCentersView(false);

    // Execute AJAX request to get all available cost centers for the user
    costCenterDetailViewModelInstance.getMajorComponentsThenCostCenter();
    $(document).mouseup(function (e) {
        closeExportDropDown(e);
    });
});

function closeExportDropDown(clickEvent) {
    var close = true;
    // If the click is within the drop down list don't close it. (And if the click is within the button, don't fire the click event again - let the standard click event handle it.)
    if ($("#download-menu").is(clickEvent.target) || $("#download-menu").has(clickEvent.target).length > 0 ||
        $("#download-dropdown").is(clickEvent.target) || $("#download-dropdown").has(clickEvent.target).length > 0) {
        close = false;
    }

    if (close && $("#download-menu").is(':visible')) {
        $("#download-dropdown").click();
    }
}