﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

// Set up a view model for the list of projects summary view models
function documentViewModel() {
    var self = this;

    // Store the document returned from the API.
    self.Document = ko.observable();

    // Track which tab is selected when in mobile view
    self.TabSelected = ko.observable('O');

    // Determine whether or not we're looking at document line items
    self.LineItemDetailDisplayed = ko.observable(false);

    // Check if Document is defined
    self.isDocumentDefined = ko.computed(function () {
        return typeof self.Document() !== 'undefined';
    }, self);

    // "Inherit" the base view model, specify the mobile screen width
    BaseViewModel.call(self, 769);

    // Change desktop structure to mobile structure
    this.changeToMobile = function () {
        if (self.isMobile() && self.isDocumentDefined()) {
            self.reloadPage(window);
        }
    }

    // Change mobile structure back to desktop view
    this.changeToDesktop = function () {
        if (!self.isMobile() && self.isDocumentDefined()) {
            self.reloadPage(window);
        }
    }

    // Reload the page
    self.reloadPage = function (window) {
        window.location.href = window.location.href;
    }

    // Spinner will show until this is set to true
    self.isLoaded = ko.observable(false);

    // Execute an AJAX requrest to get the specified document.
    self.loadDocument = function (documentUrl, errorMessage) {
        $.ajax({
            url: documentUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.checkForMobile(window, document);

                    // Execute the knockout mapping, which takes the response object and uses the 
                    // explicit mapping and models to update documentViewModelInstance
                    data = { 'Document': data };
                    ko.mapping.fromJS(data, documentMapping, self);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#notificationHost').notificationCenter('addNotification', { message: errorMessage, type: "error" });
            },
            complete: function () {
                // Hide the spinner for the whole page.
                self.isLoaded(true);

                // If we're in mobile view then select the Overview tab
                if (self.isMobile()) {
                    self.selectOverviewTab();
                }
            }
        });
    }

    self.blankSpace = ko.computed(function () {
        var style = "";
        if (self.isMobile()) {
            style = "blank-space";
        }

        return style;
    }, self);

    /*
     * This function is used to manage the interactions with the tabs.
     *
     * Possible values for 'tabSelected' are:
     *   - 'O' - Overview
     *   - 'A' - Approvers
     *   - 'L' - Line Items
     */
    self.clickTab = function (tabClicked) {
        // Hide the back link that's displayed when we look at a specific line item
        self.LineItemDetailDisplayed(false);

        // No matter which tab was clicked issue a "click" event on the back link to
        // dismiss the panel.
        if ($(".panel-wrapper").first().css("display") !== "none") {
            var backLinkId = "#" + $(".panel-wrapper").first().attr("id") + "-back-link";
            $(backLinkId).click();
        }

        // Change to the Overview tab, but only if it's not already selected
        if (tabClicked == 'O' && self.TabSelected() != 'O') {
            self.selectOverviewTab();

            // Re-style the tab to match the back link
            $(".document-tab-line-items").removeClass("line-item-detail-tab-selected");

            // Hide the details information if it was the last thing to be 
            self.clearLineItemDetails();

            // In this scenario the additional details section gets collapsed, but the
            // arrow does not get updated accordingly; update it here.
            $("#additional-details-controller").removeClass("multi-accordion-expanded multi-accordion-collapsed").addClass("arrow-down");
            $("#additional-details-controller .arrow").removeClass("arrow-up").addClass("arrow-down");
        }

        // Change to the Approvers tab, but only if it's not already selected
        if (tabClicked == 'A' && self.TabSelected() != 'A') {
            // Keep track of which tab is selected
            self.TabSelected('A');

            // Re-style the tab to match the back link
            $(".document-tab-line-items").removeClass("line-item-detail-tab-selected");
            $(".document-tab-line-items").addClass("document-tab-selected");

            // Hide the details information if it was the last thing to be 
            self.clearLineItemDetails();
            
            // Style the tabs
            $(".document-tab-overview").removeClass("document-tab-selected");
            $(".document-tab-approvers").addClass("document-tab-selected");
            $(".document-tab-line-items").removeClass("document-tab-selected");

            // Set ARIA attributes so the screen reader knows which tab is selected
            $(".gl-tab-overview").attr("aria-selected", "false");
            $(".gl-tab-approvers").attr("aria-selected", "true");
            $(".gl-tab-line-items").attr("aria-selected", "false");

            // Set ARIA attributes so the screen reader knows which elements are being displayed
            $("#overview-tabpanel").attr("aria-hidden", "true");
            $("#approvers-tabpanel").attr("aria-hidden", "false");
            $("#line-items-tabpanel").attr("aria-hidden", "true");
        }

        // Change to the Approvers tab, but only if it's not already selected
        if (tabClicked == 'L' && self.TabSelected() != 'L') {
            // Keep track of which tab is selected
            self.TabSelected('L');

            // Hide the details information if it was the last thing to be 
            self.clearLineItemDetails();

            // Adjust the headers
            $(".extended-price").removeClass("normal");

            // Style the tabs
            $(".document-tab-overview").removeClass("document-tab-selected");
            $(".document-tab-approvers").removeClass("document-tab-selected");
            $(".document-tab-line-items").addClass("document-tab-selected");

            // Set ARIA attributes so the screen reader knows which tab is selected
            $(".gl-tab-overview").attr("aria-selected", "false");
            $(".gl-tab-approvers").attr("aria-selected", "false");
            $(".gl-tab-line-items").attr("aria-selected", "true");

            // Set ARIA attributes so the screen reader knows which elements are being displayed
            $("#overview-tabpanel").attr("aria-hidden", "true");
            $("#approvers-tabpanel").attr("aria-hidden", "true");
            $("#line-items-tabpanel").attr("aria-hidden", "false");
        }
    }

    self.selectOverviewTab = function () {
        // Keep track of which tab is selected
        self.TabSelected('O');

        // Show the appropriate elements
        $(".document-header.header-right").hide();

        // Style the tabs
        $(".document-tab-overview").addClass("document-tab-selected");
        $(".document-tab-approvers").removeClass("document-tab-selected");
        $(".document-tab-line-items").removeClass("document-tab-selected");

        // Set ARIA attributes so the screen reader knows which tab is selected
        $(".gl-tab-overview").attr("aria-selected", "true");
        $(".gl-tab-approvers").attr("aria-selected", "false");
        $(".gl-tab-line-items").attr("aria-selected", "false");

        // Set ARIA attributes so the screen reader knows which elements are being displayed
        $("#overview-tabpanel").attr("aria-hidden", "false");
        $("#approvers-tabpanel").attr("aria-hidden", "true");
        $("#line-items-tabpanel").attr("aria-hidden", "true");
    }

    self.clearLineItemDetails = function () {
        // Hide the 'back to line items' link
        self.LineItemDetailDisplayed(false);

        // Hide the line item details
        $(".overlay-panel").hide();

        // Restore height of the initial wrapper
        $(".initial-panel-wrapper").height("inherit");

        // Remove the line item details from the DOM
        $("#mobile-line-item-details .line-item-details-row").remove();
    }

    self.isDocumentDefined = ko.computed(function () {
        return typeof self.Document() !== 'undefined';
    }, self);

    self.creditsAmountColor = ko.computed(function () {
        return "itemCreditTotalAmount-red";
    }, self);
}