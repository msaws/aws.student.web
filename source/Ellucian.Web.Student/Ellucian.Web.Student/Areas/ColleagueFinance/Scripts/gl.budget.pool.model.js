﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

// Javascript representation of a GL budget pool
function glBudgetPoolModel(data) {
    var self = this;

    ko.mapping.fromJS(data, costCenterMapping, self);

};