﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

// Initialize the CF namespace object for use in projects accounting
var cfNS = cfNS || {};

// Initialize the ColleagueFinance namespace object for use in ColleagueFinance module.
var Ellucian = Ellucian || {};
Ellucian.ColleagueFinance = Ellucian.ColleagueFinance || {};

// This function is used when the GL Detail view (activity for one GL account) is showing.
// The GL detail view in Projects Accounting DOES use the item code and the item description.
ko.bindingHandlers.toggleGlDetailView = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.toggleView(valueAccessor().glNumber);
        });

        $(element).on('keyup', function (event) {
            var keyCode = event.which || event.keyCode;
            if (keyCode == 13 || keyCode == 32) {
                bindingContext.$root.toggleView(valueAccessor().glNumber);
            }
        });
    }
};

ko.bindingHandlers.linkToGlDetailView = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        if (valueAccessor().glDetailBaseUrl) {
            $(element).click(function () {
                window.document.location = (ko.unwrap(valueAccessor().glDetailBaseUrl) + "/" + valueAccessor().glNumber);
            });

            $(element).on('keyup', function (event) {
                var keyCode = event.which || event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    window.document.location = (ko.unwrap(valueAccessor().glDetailBaseUrl) + "/" + valueAccessor().glNumber);
                }
            });
        }
        else {
            $(element).click(function () {
                window.document.location = (glDetailUrl + "/" + valueAccessor().glNumber);
            });

            $(element).on('keyup', function (event) {
                var keyCode = event.which || event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    window.document.location = (glDetailUrl + "/" + valueAccessor().glNumber);
                }
            });
        }
    }
};

// This knockout binding function is used to set browser memory values for each cost center
// subtotal that is expanded on the cost center page. It is used to keep track of which 
// cost center subtotals have been expanded so that they can be expanded automatically when
// the user returns to the cost center page.
ko.bindingHandlers.updateSubtotalBrowserMemoryList = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // on click, keep track of which line items are expanded via the row ID,
        // and toggle the line item visibility
        $(element).click(function () {
            var rowId = $(this).attr("id");

            // If active line item row has not already been added to the array,
            // add it, otherwise, remove it
            var fromMemory = memory.getItem(valueAccessor().memoryName || "activeCostCenterSubtotal");

            if (fromMemory === null) {
                fromMemory = '';
            }

            if (fromMemory.indexOf(rowId) < 0) {
                var newMemory = '';

                if (fromMemory.length > 0) {
                    newMemory += fromMemory + "," + rowId;
                }
                else {
                    newMemory = rowId;
                }
                memory.setItem(valueAccessor().memoryName || "activeCostCenterSubtotal", newMemory);
            }
            else {
                var activeCostCenterSubtotal = memory.getItem(valueAccessor().memoryName || "activeCostCenterSubtotal");
                var newSubtotalList = '';

                if (activeCostCenterSubtotal !== null && activeCostCenterSubtotal.indexOf(rowId) >= 0) {

                    activeCostCenterSubtotal = activeCostCenterSubtotal.split(",");

                    for (var i = 0; i < activeCostCenterSubtotal.length; i++) {
                        var item = activeCostCenterSubtotal[i];

                        if (rowId != item) {
                            if (newSubtotalList.length > 0) {
                                newSubtotalList += ",";
                            }

                            newSubtotalList += item;
                        }
                    }
                }
                memory.setItem(valueAccessor().memoryName || "activeCostCenterSubtotal", newSubtotalList);
            }
        });
    }
};

Ellucian.ColleagueFinance.initializeActiveRows = function (thisElement) {
    $(thisElement).nextUntil(".collapsible").slideToggle();
    // Toggle the up arrow class (default down .arrow alone for collapsed, .arrow.arrow-up for expanded)
    var replaceArrow = $(thisElement).find('.arrow');
    $(replaceArrow).toggleClass("arrow-up");

    // Toggle the aria-expanded attribute when expanded/collapsed
    if ($(thisElement).find('.expand-details').attr("aria-expanded") == "true") {
        $(thisElement).find('.expand-details').attr("aria-expanded", "false");
    }
    else {
        $(thisElement).find('.expand-details').attr("aria-expanded", "true");
    }
    $(thisElement).removeClass("multi-accordion-collapsed");
    $(thisElement).addClass("multi-accordion-expanded");
}

ko.bindingHandlers.toggleDocumentLineItemGlDistribution = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).click(function () {
            // Only toggle the GL distribution if the account is not masked
            if (!bindingContext.$data.IsAccountMasked()) {
                cfNS.toggleLineItemGlDistribution(this, bindingContext);
            }
        });
    }
};

// The bindingContext variable is only useful for accessing the $root node. This
// function is called from multiple places and as a result the binding context changes
// depending on where it's being called from. As a result, if you try to to reference
// bindingContext.$data here you will get inconsistent results.
cfNS.toggleLineItemGlDistribution = function (thisElement, bindingContext) {
    $(thisElement).parent().next().toggle();

    // Only resize the element if we're in mobile view
    if (bindingContext.$root.isMobile()) {
        // Get the ID of the 'slide panel content' element
        var panelContentId = "#" + $(".panel-wrapper").first().attr("id");

        // Set the line items tab panel height to the sum of the slide panel back link height and the slide panel table height
        var tabPanelId = "#line-items-tabpanel";
        $(tabPanelId)
            .height($(panelContentId).children(".panel-back-link-container").height() + $(panelContentId).children(".panel-table").height());

        // Set the width of the panel after the panel appears to account for the scroll bar, should it appear
        $(panelContentId).css("width", $(tabPanelId).css("width"));
    }

    // Switch the arrow directions (classes) when expanded/collapsed
    var replaceArrow = $(thisElement).children('.arrow');
    $(replaceArrow).toggleClass("arrow-up");

    if ($(thisElement).attr("aria-expanded") == "true") {
        $(thisElement).attr("aria-expanded", "false");
    }
    else {
        $(thisElement).attr("aria-expanded", "true");
    }
}

ko.bindingHandlers.valueOrBlank = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        if (valueAccessor() == "" || valueAccessor() == null) {
            $(element).html("&nbsp;");
        }
        else {
            $(element).text(valueAccessor());
        }
    }
};

ko.bindingHandlers.backToLineItems = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).click(function () {
            cfNS.backToLineItemsFunction(bindingContext);
        });
    }
};

ko.bindingHandlers.selectTab = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).click(function () {
            if (valueAccessor().tab === 'L' && bindingContext.$root.TabSelected() === 'L') {
                cfNS.backToLineItemsFunction(bindingContext);
            }
            else {
                bindingContext.$root.clickTab(valueAccessor().tab);
            }
        });

        $(element).on('keyup', function (event) {
            var keyCode = event.which || event.keyCode;
            if (keyCode == 13 || keyCode == 32) {
                if (valueAccessor().tab === 'L' && bindingContext.$root.TabSelected() === 'L') {
                    cfNS.backToLineItemsFunction(bindingContext);
                }
                else {
                    bindingContext.$root.clickTab(valueAccessor().tab);
                }
            }
        });
    }
};

cfNS.backToLineItemsFunction = function (bindingContext) {
    // This binding can only be executed while in mobile view. So, there is
    // no need to check for mobile using base.view.model.js.

    // Re-style the tab to match the back link
    $(".document-tab-line-items").removeClass("line-item-detail-tab-selected");
    $(".document-tab-line-items").addClass("document-tab-selected");

    // Show and slide overlay
    bindingContext.$root.LineItemDetailDisplayed(false);

    // slide overlay
    $(".overlay-panel").toggleClass("slide", false);

    // show initial panel
    $(".initial-panel").show();

    // restore height
    $(".initial-panel-wrapper").height("inherit");

    $("#mobile-line-item-details .line-item-details-row").remove();
}

ko.bindingHandlers.selectEncumbrancesTab = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.clickEncumbrancesTab();
        });

        $(element).on('keyup', function (event) {
            var keyCode = event.which || event.keyCode;
            if (keyCode == 13 || keyCode == 32) {
                bindingContext.$root.clickEncumbrancesTab();
            }
        });
    }
};

ko.bindingHandlers.selectActualsTab = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.clickActualsTab();
        });

        $(element).on('keyup', function (event) {
            var keyCode = event.which || event.keyCode;
            if (keyCode == 13 || keyCode == 32) {
                bindingContext.$root.clickActualsTab();
            }
        });
    }
};

ko.bindingHandlers.selectBudgetTab = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.clickBudgetTab();
        });

        $(element).on('keyup', function (event) {
            var keyCode = event.which || event.keyCode;
            if (keyCode == 13 || keyCode == 32) {
                bindingContext.$root.clickBudgetTab();
            }
        });
    }
};

// Custom binding to set the visibility of an element.
// Elements when this binding set to true will have a visibility of "visible".
// Elements with this binding set to false will have a visibility of "hidden" or a display of "none", depending on the screen width.
// (They will not appear to the user, but they will still take up space in the page flow unless the user is in mobile view)
// Examples:
//      <span data-bind="visibility: false"></span>
//      <span data-bind="visibility: someObservableBoolean"></span>

ko.bindingHandlers.visibility = (function () {
    function setVisibility(e, valueAccessor) {
        var visible = ko.unwrap(valueAccessor());
        if (visible) {
            $(e).removeClass('hide-content make-invisible');
        }
        else {
            $(e).addClass('hide-content make-invisible');
        }
    }
    return { init: setVisibility, update: setVisibility };
})();