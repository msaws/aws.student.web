﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

// Javascript representation of a GL activity detail.
function glActivityDetailModel(data) {
    var self = this;

    ko.mapping.fromJS(data, glActivityDetailMapping, self);

};