﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.

// Configure a knockout mapping object for the documents
var documentMapping = {
    'Document': {
        create: function (options) {
            return new documentModel(options.data);
        }
    },
    'LineItems': {
        create: function (options) {
            return new documentLineItemModel(options.data);
        }
    },
    'GlDistributions': {
        create: function (options) {
            return new documentLineItemGlDistributionModel(options.data);
        }
    },
    'Approvers': {
        create: function (options) {
            return new documentApproverModel(options.data);
        }
    },
};