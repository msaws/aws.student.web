﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

var arguments = '',
    glDescription = '',
    projectId = '',
    glNumber = '',
    lineItemCode = '',
    lineItemDescription = '';

var parseDocumentUrl = function () {
    arguments = window.location.href.slice(window.location.href.indexOf('?')).substring(1).split("&");
    glDescription = decodeURIComponent(arguments[0]);
    projectId = arguments[1];
    glNumber = decodeURIComponent(arguments[2]);
    lineItemCode = decodeURIComponent(arguments[3]);
    lineItemDescription = decodeURIComponent(arguments[4]);
}