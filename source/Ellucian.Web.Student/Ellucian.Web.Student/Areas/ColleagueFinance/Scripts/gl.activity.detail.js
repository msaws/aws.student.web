﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// Register CsvDownloadLink component
ko.components.register('csv-download-link', {
    require: 'CsvDownloadLink/_CsvDownloadLink'
});

// Set up viewModel instance
var glActivityViewModelInstance = new glActivityDetailViewModel();

// Once the page has loaded...
$(document).ready(function () {
    ko.applyBindings(glActivityViewModelInstance, document.getElementById("main"));

    // Execute AJAX request to get all available gl transactions for the user
    glActivityViewModelInstance.getGlTransactions(glNumber);
});