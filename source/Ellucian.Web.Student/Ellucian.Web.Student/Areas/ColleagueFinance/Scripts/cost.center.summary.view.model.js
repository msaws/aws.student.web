﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

//Select which storage mechanism to use depending on browser.
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();

// Set up a view model for the list of cost centers summary view models
function costCenterSummaryViewModel(hash) {
    var self = this;

    // "Inherit" the base cost center view model to make use of common observables and functions.
    baseCostCenterViewModel.call(self);

    // "Inherit" the base view model, specify the mobile screen width
    BaseViewModel.call(self, 768);

    // Change desktop structure to mobile structure
    self.changeToMobile = function () {
    }

    // Change mobile structure back to desktop view
    self.changeToDesktop = function () {
    }

    // Check for mobile and set the isMobile observable as determined (defined in BaseViewModel.
    self.checkForMobile(window, document);

    self.FiscalYears = [];

    // The list of cost center objects which are displayed.
    self.CostCenters = ko.observableArray();

    // The object data to display on the Object View tab.
    self.ObjectData = ko.observable();

    // Initial value for HasAnyRevenue boolean flag.
    self.HasAnyRevenue = ko.observable(false);

    // Initial value for HasAnyExpense boolean flag.
    self.HasAnyExpense = ko.observable(false);

    // Initial value for HasAnyAsset boolean flag.
    self.HasAnyAsset = ko.observable(false);

    // Initial value for HasAnyLiability boolean flag.
    self.HasAnyLiability = ko.observable(false);

    // Initial value for HasAnyFundBalance boolean flag.
    self.HasAnyFundBalance = ko.observable(false);

    // Used to keep track of the number of cost centers returned.
    self.CostCentersCount = ko.observable(0);

    // Keep track of the number of object codes.
    self.ObjectCodesCount = ko.computed(function () {
        if (self.ObjectData()) {
            return self.ObjectData().ExpenseObjectCodes.length + self.ObjectData().RevenueObjectCodes.length + self.ObjectData().AssetObjectCodes.length + self.ObjectData().LiabilityObjectCodes.length + self.ObjectData().FundBalanceObjectCodes.length;
        }
        else {
            return 0;
        }
    });

    // Display content if there are objects
    self.DisplayObjects = ko.computed(function () {
        return self.ObjectCodesCount() > 0;
    });

    // List of object codes (second level accordians) that are expanded
    self.expandedObjects = ko.observableArray([]);

    // Click handler for second level accordians
    self.toggleObjectAccordianState = function (objectRow) {
        if (self.expandedObjects.indexOf(objectRow.Id) < 0) {
            self.expanding(true);
            setTimeout(function () {
                self.expandedObjects.push(objectRow.Id);
                self.expanding(false);
            }, 15);
        }
        else {
            self.collapsing(true);
            setTimeout(function () {
                self.expandedObjects.remove(objectRow.Id);
                self.collapsing(false);
            }, 15);
        }
    };

    // Helper method to invert boolean observable when setting top level state
    self.invertBooleanObservable = function (observable) {
        observable(!observable());
    }

    // Tell the user that the top level accordian is changing
    self.notifyUserAndExpandCollapse = function (display) {
        // If it is currently shown, hide it
        if (display()) {
            self.collapsing(true);
            setTimeout(function () {
                self.invertBooleanObservable(display);
                self.collapsing(false);
                self.expandedObjects.removeAll();
            }, 15);
        }
            // If it is currently hidden, show it 
        else {
            self.expanding(true);
            setTimeout(function () {
                self.invertBooleanObservable(display);
                self.expanding(false);
            }, 15);
        }
    }

    // Used to show/hide modal spinner message for expanding accordians
    self.expanding = ko.observable(false);

    // Used to show/hide modal spinner message for collapsing accordians
    self.collapsing = ko.observable(false);

    // Track the expansion state of the Asset category
    self.DisplayAssetCodes = ko.observable(false);

    // Toggle accordian (click) event for top level accordian
    self.toggleAssetCodes = function () {
        self.notifyUserAndExpandCollapse(self.DisplayAssetCodes);
    }

    // Track the expansion state of the Liability category
    self.DisplayLiabilityCodes = ko.observable(false);

    // Toggle accordian (click) event for top level accordian
    self.toggleLiabilityCodes = function () {
        self.notifyUserAndExpandCollapse(self.DisplayLiabilityCodes);
    }

    // Track the expansion state of the Fund Balance category
    self.DisplayFundBalanceCodes = ko.observable(false);

    // Toggle accordian (click) event for top level accordian
    self.toggleFundBalanceCodes = function () {
        self.notifyUserAndExpandCollapse(self.DisplayFundBalanceCodes);
    }

    // Track the expansion state of the Revenue category
    self.DisplayRevenueCodes = ko.observable(false);

    // Toggle accordian (click) event for top level accordian
    self.toggleRevenueCodes = function () {
        self.notifyUserAndExpandCollapse(self.DisplayRevenueCodes);
    }

    // Track the expansion state of the Expense category
    self.DisplayExpenseCodes = ko.observable(false);

    // Toggle accordian (click) event for top level accordian
    self.toggleExpenseCodes = function () {
        self.notifyUserAndExpandCollapse(self.DisplayExpenseCodes);
    }

    // Determine Net row visibility based on data (some object data must be exist, the page must be loaded, and the there must be both revenue and expense object codes)
    self.isObjectNetRowVisible = ko.computed(function () {
        return self.ObjectCodesCount() > 0 && self.IsPageLoaded() && self.HasAnyExpense() && self.HasAnyRevenue();
    }, self);


    // Boolean to indicate if we know on which tab the user should land.
    self.isTabKnown = ko.observable(false);

    // Track if we are on the Cost Centers tab or the Object view.
    self.isCostCentersTabActive = ko.observable(!(hash && hash.toLowerCase() === "#objectview"));

    // Set the isTabKnown observable so that we can determine if we need to retrieve the user's tab preference.
    if (hash && (hash.toLowerCase() === "#objectview" || hash.toLowerCase() === "#mycostcenters")) {
        self.isTabKnown(true);
    }
    // Navigate to the Cost Centers tab
    self.costCentersTabClicked = function () {
        self.getFilteredCostCenters();
        self.setCostCentersViewPreference(true);
        self.isCostCentersTabActive(true);
        return false;
    }

    // Navigate to the Object View tab
    self.objectViewTabClicked = function () {
        self.getFilteredObjectData();
        self.setCostCentersViewPreference(false);
        self.isCostCentersTabActive(false);
        return false;
    }

    // Boolean that controls whether accounts with no activity should be displayed.
    self.showActiveAccountsWithNoActivity = ko.observable(true);

    // Boolean that controls whether the Save Criteria button is enabled.
    self.isSaveCriteriaButtonEnabled = ko.pureComputed(function () {
        var changed = false;
        var valid = true;
        for (var i = 0; i < self.MajorComponents().length; i++) {
            var subCriteria = self.MajorComponents()[i].workingCriteria().replace(/\s+/gm, '').replace(/^,+|,+$/gm, '').split(",");
            var wcLength = 0;
            var validationErrors = [];

            // Don't include blank working criteria in the validation and count
            if (subCriteria.length === 1 && subCriteria[0] === "") {
                wcLength = 0;
            }
                // Check for valid working criteria
            else if (self.MajorComponents()[i].validateFilterCriteria(self.MajorComponents()[i].workingCriteria(), validationErrors)) {
                for (var s = 0; s < subCriteria.length; s++) {
                    // Only count the new/unique criteria in the count
                    if (self.MajorComponents()[i].tokenizedFilterCriteria().indexOf(subCriteria[s]) === -1) {
                        wcLength++;
                    }
                    // Determine if the criteria is a change
                    if (self.MajorComponents()[i].appliedFilterCriteria().indexOf(subCriteria[s]) === -1) {
                        changed = true;
                    }
                }
            }
            // Invalidate the "changed" flag if there were any validation errors
            if (validationErrors.length > 0) {
                valid = false;
            }

            // Check number of tokens
            if (self.MajorComponents()[i].tokenizedFilterCriteria().length + wcLength !== 0) {
                changed = true;
            }
        }

        // Check the showActiveAccountsWithNoActivity property
        if (self.showActiveAccountsWithNoActivity() !== ((memory.getItem("show-no-activity") || "true").toLowerCase() === "true")) {
            changed = true;
        }
        // Determin the validity and status of the apply filter button.
        return valid && changed;
    }, self).extend({ deferred: true });

    // Boolean that determines if the showActiveAccountsWithNoActivity accordian is expanded.
    self.aAExpanded = ko.observable(false);

    // Function that changes the state of the showActiveAccountsWithNoActivity accordian flag
    self.toggleAA = function () {
        self.aAExpanded(!self.aAExpanded());
    };

    // List of user-defined saved filter names.
    self.SavedFilterNames = ko.observableArray([]);

    self.savedFilterNamesWithNone = ko.computed(function () {
        var filters = [];
        filters.push({ Key: ko.observable(""), Value: ko.observable(Ellucian.ColleagueFinance.noneLabel) });
        filters = filters.concat(self.SavedFilterNames());
        return filters;
    });

    // Boolean that controls the Saved Filter Criteria DDL. true = expanded; false = collapsed
    self.isSavedFiltersExpanded = ko.observable(false);

    // Boolean to indicate if the filter components need to be populated.
    self.populateFilterComponents = ko.observable(true);

    // Boolean to indicate if the fiscal year dropdown needs to be populated.
    self.PopulateFiscalYearDropDown = ko.observable(true);

    // Boolean to indicate if the dropdown click event needs to be bound.
    self.bindExportClickEvent = ko.observable(true);

    // String that labels the active selection on the saved filter DDL.
    self.selectedSavedFilterLabel = ko.observable(Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel);

    // Default filter observables, computed's, and functions.
    // Boolean that controls the Default Filter Criteria DDL. true = expanded; false = collapsed
    self.isDefaultFiltersExpanded = ko.observable(false);

    // ID of the current default filter.
    self.currentDefaultFilterId = ko.observable("");

    // Name of the current default filter.
    self.currentDefaultFilterName = ko.observable("");

    // Display the name of the current default filter or "None"
    self.currentDefaultFilterLabel = ko.pureComputed(function () {
        if (!self.currentDefaultFilterName() || self.currentDefaultFilterName() == Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel) {
            return Ellucian.ColleagueFinance.noneLabel;
        } else {
            return self.currentDefaultFilterName();
        }
    }, self);

    // ID of the working default filter selection.
    self.workingDefaultFilterId = ko.observable("");

    // Name of the working default filter selection.
    self.workingDefaultFilterName = ko.observable("");

    // Display the name of the current default filter or "None"
    self.workingDefaultFilterLabel = ko.pureComputed(function () {
        if (!self.workingDefaultFilterName() || self.workingDefaultFilterName() == Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel) {
            return Ellucian.ColleagueFinance.noneLabel;
        } else {
            return self.workingDefaultFilterName();
        }
    }, self);

    // Show the default filter dialog
    self.openDefaultFilterDialog = function () {
        // Override the dialog body to allow the dropdown menu to overflow.
        if (!$(".esg-modal-dialog__body").hasClass("overflow-visible")) {
            $(".esg-modal-dialog__body").addClass("overflow-visible");
        }

        // Show the dialog
        self.isDefaultDialogVisible(true);
    };

    // Close the default filter dialog and reset the working default filter observables
    self.closeDefaultFilterDialog = function () {
        // Hide the dialog and reset the dropdown values.
        self.isDefaultDialogVisible(false);
        self.workingDefaultFilterId(self.currentDefaultFilterId());
        self.workingDefaultFilterName(self.currentDefaultFilterName());
    };

    // Expand/collapse the default filters DDL
    self.toggleDefaultFilters = function () {
        self.isDefaultFiltersExpanded(!self.isDefaultFiltersExpanded());
    };
    // Default filter observables, computed's, and functions.

    // String that identifies the active selection on the saved filter DDL.
    self.selectedSavedFilterId = ko.observable("");

    // Boolean to indicate if the user has activated a saved filter.
    self.isSavedFilterSelected = ko.observable(false);

    // Boolean to indicate if the save dialog is shown.
    self.isSaveDialogVisible = ko.observable(false);

    // Boolean to indicate if the default dialog is displayed.
    self.isDefaultDialogVisible = ko.observable(false);

    // String to be used when saving a new filter.
    self.saveFilterName = ko.observable("");

    // Subscribe to saveFilterName changes for validation.
    self.saveFilterName.subscribe(function (newVal) {
        self.validateSaveFilter();
    });

    // Boolean to indicate if the save filter input is valid.
    self.isSaveFilterValid = ko.observable(true);

    self.isSaveFilterNameVisible = ko.pureComputed(function () {
        return self.createOrUpdate() === "create";
    }, self);

    // Boolean to indicate when it is safe to display the bar graph or list view.
    self.isUIViewKnown = ko.observable(false);

    // Boolean to control the bar graph/list view.
    self.isBarGraphViewActive = ko.observable(true);

    self.isFilterChanged = ko.pureComputed(function () {
        var changed = false;
        var valid = true;
        for (var i = 0; i < self.MajorComponents().length; i++) {
            var subCriteria = self.MajorComponents()[i].workingCriteria().replace(/\s+/gm, '').replace(/^,+|,+$/gm, '').split(",");
            var wcLength = 0;
            var validationErrors = [];

            // Don't include blank working criteria in the validation and count
            if (subCriteria.length === 1 && subCriteria[0] === "") {
                wcLength = 0;
            }
                // Check for valid working criteria
            else if (self.MajorComponents()[i].validateFilterCriteria(self.MajorComponents()[i].workingCriteria(), validationErrors)) {
                for (var s = 0; s < subCriteria.length; s++) {
                    // Only count the new/unique criteria in the count
                    if (self.MajorComponents()[i].tokenizedFilterCriteria().indexOf(subCriteria[s]) === -1) {
                        wcLength++;
                    }
                    // Determine if the criteria is a change
                    if (self.MajorComponents()[i].appliedFilterCriteria().indexOf(subCriteria[s]) === -1) {
                        changed = true;
                    }
                }
            }
            // Invalidate the "changed" flag if there were any validation errors
            if (validationErrors.length > 0) {
                valid = false;
            }

            // Check number of tokens
            if (self.MajorComponents()[i].tokenizedFilterCriteria().length + wcLength !== self.MajorComponents()[i].appliedFilterCriteria().length) {
                changed = true;
            }

            // Check to see if there are any new criteria in the tokenized filter criteria array
            for (var j = 0; j < self.MajorComponents()[i].tokenizedFilterCriteria().length; j++) {
                if (self.MajorComponents()[i].appliedFilterCriteria().indexOf(self.MajorComponents()[i].tokenizedFilterCriteria()[j]) === -1) {
                    changed = true;
                }
            }

        }

        // Check the showActiveAccountsWithNoActivity property
        if (self.showActiveAccountsWithNoActivity() !== ((memory.getItem("show-no-activity") || "true").toLowerCase() === "true")) {
            changed = true;
        }
        // Determin the validity and status of the apply filter button.
        return valid && changed;
    }, self).extend({ deferred: true });

    // String for tracking the selected radio button in the save dialog.
    self.createOrUpdate = ko.observable("create");

    // Boolean to indicate if the saving spinner should be displayed.
    self.isFilterSaving = ko.observable(false);

    // Boolean to indicate if the criteria loading spinner should be displayed.
    self.isLoadingSavedFilterCriteria = ko.observable(false);

    // Boolean to indicate if the "deleting filter" spinner should be displayed.
    self.isDeletingSavedFilter = ko.observable(false);

    // Boolean to indicate if the "saving default filter" spinner should be displayed
    self.isSavingDefaultFilter = ko.observable(false);

    // Use the default filter unless browser memory contains some criteria (even blank criteria)
    self.useDefaultFilter = ko.observable(true);

    // When selecting a saved filter criteria, we can force the retrieved criteria to auto-submit (used by default filter)
    self.autoloadCostCenters = ko.observable(false);

    // Used to hide content when in mobile view with the filter menu opened.
    self.filterMenuIsOpen = ko.observable(false);

    // Function to open and close the filter panel.
    self.toggleFilter = function () {
        self.filterMenuIsOpen(!self.filterMenuIsOpen());
        $('html').toggleClass('active-filter');
    };

    // Function to expand/collapse the saved filters DDL
    self.toggleSavedFilters = function () {
        self.isSavedFiltersExpanded(!self.isSavedFiltersExpanded());
    };

    // Function to request the criteria for a filter and set the tokenized filter criteria
    self.selectSavedFilter = function (filter) {
        if (filter && filter.Key && filter.Key() && filter.Key().length > 0) {
            var urlAndArg = getSavedFilterCriteriaUrl + "?filterId=" + encodeURI(filter.Key());
            $.ajax({
                url: urlAndArg,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function () {
                    // Show the drop-down spinner
                    self.isLoadingSavedFilterCriteria(true);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        var savedFilter = ko.mapping.fromJS(data, costCenterMapping);

                        if (savedFilter != null) {
                            if (typeof savedFilter.ComponentCriteria != "undefined" && savedFilter.ComponentCriteria() != null) {
                                for (var i = 0; i < self.MajorComponents().length; i++) {
                                    self.MajorComponents()[i].tokenizedFilterCriteria.removeAll();

                                    for (var s = 0; s < savedFilter.ComponentCriteria().length; s++) {
                                        if (self.MajorComponents()[i].ComponentName().toUpperCase() === savedFilter.ComponentCriteria()[s].ComponentName().toUpperCase()) {
                                            if (typeof savedFilter.ComponentCriteria()[s].IndividualComponentValues !== "undefined" && savedFilter.ComponentCriteria()[s].IndividualComponentValues() != null) {
                                                self.MajorComponents()[i].tokenizedFilterCriteria.pushAll(savedFilter.ComponentCriteria()[s].IndividualComponentValues());
                                            }
                                            if (typeof savedFilter.ComponentCriteria()[s].RangeComponentValues !== "undefined" && savedFilter.ComponentCriteria()[s].RangeComponentValues() != null) {
                                                for (var r = 0; r < savedFilter.ComponentCriteria()[s].RangeComponentValues().length; r++) {
                                                    self.MajorComponents()[i].tokenizedFilterCriteria.push(
                                                        savedFilter.ComponentCriteria()[s].RangeComponentValues()[r].StartValue()
                                                        + "-" + savedFilter.ComponentCriteria()[s].RangeComponentValues()[r].EndValue());
                                                }
                                            }
                                        }
                                    }
                                    self.MajorComponents()[i].tokenizedFilterCriteria.sort();
                                }
                            }

                            self.showActiveAccountsWithNoActivity(savedFilter.IncludeActiveAccountsWithNoActivity());
                            self.isSavedFilterSelected(true);
                            self.selectedSavedFilterLabel(savedFilter.Name());
                            self.selectedSavedFilterId(savedFilter.Id());

                            self.isSavedFiltersExpanded(false);
                        }

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Defualt message for the following errors: 
                    //  - 400: Bad request
                    //  - Unknown status code
                    var message = unableToLoadFilterMessage;

                    // Resource not found
                    if (jqXHR.status == 404) {
                        message = resourceNotFoundText;
                    }

                    $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
                },
                complete: function () {
                    // Hide the drop-down spinner.
                    self.isLoadingSavedFilterCriteria(false);

                    if (self.autoloadCostCenters()) {
                        self.applyFilterToCurrentTab();
                        self.autoloadCostCenters(false);
                    }
                }
            });
        }
    };

    // Function to change the working default filter
    self.selectDefaultFilter = function (filter) {
        if (filter && filter.Key && filter.Value) {
            self.workingDefaultFilterId(filter.Key());
            self.workingDefaultFilterName(filter.Value());
        }
        self.isDefaultFiltersExpanded(false);

    };

    self.openSaveFilterDialog = function () {
        self.triggerAllEnterKeys();
        self.isSaveDialogVisible(true);
    };

    self.closeSaveFilterDialog = function () {
        self.saveFilterName("");
        self.createOrUpdate("create");
        self.isSaveFilterValid(true);
        self.isSaveDialogVisible(false);
    };

    self.validateSaveFilter = function () {
        if (self.isSaveFilterNameVisible()) {
            if (self.saveFilterName().replace(/^\s+|\s+$/g, '').length > 0) {
                self.isSaveFilterValid(true);
                return true;
            }
            else {
                self.isSaveFilterValid(false);
                return false;
            }
        }
        else {
            return true;
        }
    };

    self.saveFilter = function () {
        if (self.validateSaveFilter()) {
            // Build the request (and DO NOT set the active filter)
            var components = self.FormatFilterComponentsForRequest();
            var data = { criteria: components, aawna: self.showActiveAccountsWithNoActivity(), name: self.isSaveFilterNameVisible() ? self.saveFilterName() : self.selectedSavedFilterLabel(), id: self.selectedSavedFilterId(), create: self.createOrUpdate() === "create" };

            $.ajax({
                url: postSaveFilterUrl,
                type: "POST",
                contentType: jsonContentType,
                dataType: "json",
                data: ko.toJSON(data),
                beforeSend: function (data) {
                    // Show the drop-down spinner
                    self.isFilterSaving(true);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        var savedFiltersData = ko.mapping.fromJS(data, costCenterMapping);

                        // Clear and repopulate the saved filters array.
                        self.SavedFilterNames.removeAll();
                        self.SavedFilterNames.pushAll(savedFiltersData());

                        if (self.isSaveFilterNameVisible()) {
                            self.selectedSavedFilterLabel(self.saveFilterName());
                        }
                        self.isSavedFilterSelected(true);
                        $('#notificationHost').notificationCenter('addNotification', { message: filterSavedMessage, type: "success", flash: true });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Defualt message for the following errors: 
                    //  - 400: Bad request
                    //  - Unknown status code
                    var message = unableToSaveFilterMessage;

                    // Resource not found
                    if (jqXHR.status == 404) {
                        message = resourceNotFoundText;
                    }

                    $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
                },
                complete: function () {
                    // Hide the drop-down spinner.
                    self.isFilterSaving(false);
                    self.closeSaveFilterDialog();
                }
            });
        }
    };

    // Update the default filter record using an AJAX call
    self.saveDefaultFilter = function () {
        var saveDefaultFilterUrl = Ellucian.ColleagueFinance.setDefaultFilterAsyncUrl + "?filterKey=" + self.workingDefaultFilterId() + "&filterValue=" + self.workingDefaultFilterName();
        $.ajax({
            url: saveDefaultFilterUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                // Show the loading spinner
                self.isSavingDefaultFilter(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var savedFiltersData = ko.mapping.fromJS(data, {}, costCenterMapping);

                    self.currentDefaultFilterId(savedFiltersData.Key());
                    self.currentDefaultFilterName(savedFiltersData.Value());

                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.ColleagueFinance.defaultFilterSavedMessage, type: "success", flash: true });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = Ellucian.ColleagueFinance.defaultFilterSaveFailedMessage;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                // Auto-close the modal-dialog window
                self.isDefaultDialogVisible(false);
                // Hide the loading spinner
                self.isSavingDefaultFilter(false);
                // Set the working default filter to the current default filter
                self.workingDefaultFilterId(self.currentDefaultFilterId());
                self.workingDefaultFilterName(self.currentDefaultFilterName());
            }
        });
    };

    // Delete the saved filter
    self.deleteFilter = function (filter) {
        if (filter && filter.Key && filter.Key() && filter.Key().length > 0) {
            var urlAndArg = deleteSavedFilterUrl + "?filterId=" + encodeURI(filter.Key());
            $.ajax({
                url: urlAndArg,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function () {
                    // Show the drop-down spinner
                    self.isDeletingSavedFilter(true);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        var savedFiltersData = ko.mapping.fromJS(data, costCenterMapping);

                        // Clear and repopulate the saved filter names array.
                        self.SavedFilterNames.removeAll();
                        self.SavedFilterNames.pushAll(savedFiltersData());

                        if (filter.Value() === self.selectedSavedFilterLabel()) {
                            self.selectedSavedFilterLabel(Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel);
                            self.selectedSavedFilterId("");
                            self.isSavedFilterSelected(false);
                        }

                        if (self.SavedFilterNames().length === 0) {
                            self.isSavedFiltersExpanded(false);
                        }

                        $('#notificationHost').notificationCenter('addNotification', { message: filterDeletedSuccessMessage, type: "success", flash: true });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Defualt message for the following errors: 
                    //  - 400: Bad request
                    //  - Unknown status code
                    var message = Ellucian.ColleagueFinance.filterDeleteFailedMessage;

                    // Resource not found
                    if (jqXHR.status == 404) {
                        message = resourceNotFoundText;
                    }

                    $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
                },
                complete: function () {
                    // Hide the drop-down spinner.
                    self.isDeletingSavedFilter(false);

                    if (self.currentDefaultFilterId() == filter.Key()) {
                        self.currentDefaultFilterId("");
                        self.currentDefaultFilterName("");
                        self.workingDefaultFilterId("");
                        self.workingDefaultFilterName("");
                    }
                }
            });
        }
    };

    self.resetFilters = function () {
        for (var i = 0; i < self.MajorComponents().length; i++) {
            self.MajorComponents()[i].tokenizedFilterCriteria.removeAll();
            self.MajorComponents()[i].tokenizedFilterCriteria.pushAll(self.MajorComponents()[i].resetFilterCriteria());
        }

        self.showActiveAccountsWithNoActivity(true);

        self.selectedSavedFilterLabel(Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel);
        self.selectedSavedFilterId("");
        self.isSavedFilterSelected(false);
    };

    self.getMajorComponentsThenCostCenters = function () {

        var noCostCentersNotification = {
            message: noCostCentersToDisplayText,
            type: "info"
        };

        $.ajax({
            url: getMajorComponentsActionUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                // Show the drop-down spinner
                self.IsFilterUpdating(true);

                // Remove the no cost centers notification.
                $('#notificationHost').notificationCenter('removeNotification', noCostCentersNotification);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var majorComponentData = [];
                    for (var i = 0; i < data.length; i++) {
                        majorComponentData.push(new generalLedgerComponentModel(data[i]));
                    }

                    // Populate the major components for the cost centers filter.
                    if (self.populateFilterComponents()) {
                        self.MajorComponents.pushAll(majorComponentData);
                        self.populateFilterComponents(false);
                    }


                    // Determine if memory storage has values for the major components
                    for (var i = 0; i < self.MajorComponents().length; i++) {
                        var component = self.MajorComponents()[i];
                        var name = component.ComponentName();
                        var memoryValues = memory.getItem(name);
                        if (memoryValues) {
                            self.useDefaultFilter(false);
                            component.tokenizedFilterCriteria.pushAll(memoryValues.split(","));
                        }
                    }
                    var noActivity = memory.getItem("show-no-activity");
                    if (noActivity) {
                        self.useDefaultFilter(false);
                        self.showActiveAccountsWithNoActivity(noActivity.toLowerCase() === "true");
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = unableToLoadCostCentersText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                self.getCostCentersViewPreference();
            }
        });

    }

    self.triggerAllEnterKeys = function () {
        ko.postbox.publish("triggerEnterKey");
    };

    self.getUIPreference = function () {
        $.ajax({
            url: Ellucian.ColleagueFinance.getUIPreferenceUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.isBarGraphViewActive(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                self.isBarGraphViewActive(true)
            },
            complete: function () {
                self.isUIViewKnown(true);
            }
        });
    }

    self.setUIPreference = function (isBar) {

        var setPreferenceFailedNotification = {
            message: Ellucian.ColleagueFinance.setPreferenceFailedMessage,
            type: "error"
        };

        $.ajax({
            url: Ellucian.ColleagueFinance.setUIPreferenceUrl,
            type: "POST",
            data: ko.toJSON({ preferredUi: isBar }),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                // Remove the error notification.
                $('#notificationHost').notificationCenter('removeNotification', setPreferenceFailedNotification);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#notificationHost').notificationCenter('addNotification', setPreferenceFailedNotification);
            }
        });
    }

    // Returns true if default is cost center view and false if default is object view.
    self.getCostCentersViewPreference = function () {
        if (self.isTabKnown()) {
            self.getUIPreference();
            if (self.useDefaultFilter()) {
                // Get the default filter then use that filter's criteria to limit the cost centers that are retrieved.
                self.getDefaultFilterThenCostCenters();
            }
            else {
                // Get the user's default filter information but don't use the default filter to limit cost centers.
                self.getDefaultFilter();
                self.applyFilterToCurrentTab();
            }
        }
        else {
            $.ajax({
                url: Ellucian.ColleagueFinance.getCostCentersViewPreferenceUrl,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.isCostCentersTabActive(data);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    self.isCostCentersTabActive(true)
                },
                complete: function () {
                    self.isTabKnown(true);
                    self.getUIPreference();
                    if (self.useDefaultFilter()) {
                        // Get the default filter then use that filter's criteria to limit the cost centers that are retrieved.
                        self.getDefaultFilterThenCostCenters();
                    }
                    else {
                        // Get the user's default filter information but don't use the default filter to limit cost centers.
                        self.getDefaultFilter();
                        self.applyFilterToCurrentTab();
                    }
                }
            });
        }
    }

    self.setCostCentersViewPreference = function (isCostCenter) {

        var setPreferenceFailedNotification = {
            message: Ellucian.ColleagueFinance.setPreferenceFailedMessage,
            type: "error"
        };

        $.ajax({
            url: Ellucian.ColleagueFinance.setCostCentersViewPreferenceUrl,
            type: "POST",
            data: ko.toJSON({ preferredCostCenterTab: isCostCenter }),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                // Remove the error notification.
                $('#notificationHost').notificationCenter('removeNotification', setPreferenceFailedNotification);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#notificationHost').notificationCenter('addNotification', setPreferenceFailedNotification);
            }
        });
    }

    // Gets the default filter (if one exists) and then (optionally) calls the getFilteredCostCenters method if "alsoGetCostCenters" argument is true
    self.getDefaultFilter = function (alsoGetCostCenters) {
        $.ajax({
            url: Ellucian.ColleagueFinance.getDefaultFilterAsyncUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                // Show the drop-down spinner
                self.isLoadingSavedFilterCriteria(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    // Get saved filter criteria
                    var mappedData = ko.mapping.fromJS(data, costCenterMapping);
                    if (!mappedData.Key() || mappedData.Key() === "") {
                        if (alsoGetCostCenters) {
                            // No filter saved as default - do not use a filter
                            self.applyFilterToCurrentTab();
                        }
                        self.currentDefaultFilterId("");
                        self.currentDefaultFilterName("");
                        self.workingDefaultFilterId("");
                        self.workingDefaultFilterName("");
                    }
                    else {
                        self.currentDefaultFilterId(mappedData.Key());
                        self.currentDefaultFilterName(mappedData.Value());
                        self.workingDefaultFilterId(mappedData.Key());
                        self.workingDefaultFilterName(mappedData.Value());

                        if (alsoGetCostCenters) {
                            self.autoloadCostCenters(true);
                            self.selectSavedFilter(mappedData);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (alsoGetCostCenters) {
                    // Couldn't retrieve the default filter so use no filter
                    self.applyFilterToCurrentTab();
                }
            },
            complete: function () {
                self.isLoadingSavedFilterCriteria(false);
            }
        });
    }

    // Gets the default filter (if one exists) and then calls the getFilteredCostCenters method
    self.getDefaultFilterThenCostCenters = function () {
        self.getDefaultFilter(true);
    }

    // Execute the AJAX request to get cost centers based on the filter criteria that the user has chosen.
    self.getFilteredCostCenters = function () {

        self.triggerAllEnterKeys();

        // Get the selected fiscal year and saved filter from browser memory so it can be used by all the views.
        // Only invoke the code below if the user selected a new fiscal year.
        if (!self.IsPageLoaded()) {
            var fiscalYearFromMemory = memory.getItem("fiscalYear");
            if (fiscalYearFromMemory) {
                self.SelectedFiscalYear(fiscalYearFromMemory);
            }

            var selectedSavedFilterFromMemory = memory.getItem("selectedSavedFilter");
            if (selectedSavedFilterFromMemory) {
                self.selectedSavedFilterLabel(selectedSavedFilterFromMemory);
            }

            var showNoActivityFromMemory = memory.getItem("show-no-activity");
            if (showNoActivityFromMemory) {
                self.showActiveAccountsWithNoActivity(showNoActivityFromMemory.toLowerCase() === "true");
            }
        }

        var noCostCentersNotification = {
            message: noCostCentersToDisplayText,
            type: "info"
        };

        // Save the filter criteria to browser memory when applying filters.
        self.updateMemory();

        // Determine the fiscal year to use from browser memory. If there isn't one in memory
        // the fiscal year will be default to today's date in the API.
        // Initialize the query URL passing the fiscal year as a parameter.

        // Build the request (and set the active filter)
        var components = self.FormatFilterComponentsForRequest(true);

        self.currentFilterActiveAccountsWithNoActivity(self.showActiveAccountsWithNoActivity());

        var data = { criteria: components, aawna: self.showActiveAccountsWithNoActivity(), fiscalYear: self.SelectedFiscalYear() };

        $.ajax({
            url: getFilteredCostCentersActionUrl,
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            data: ko.toJSON(data),
            beforeSend: function (data) {
                // Show the drop-down spinner
                self.IsFilterUpdating(true);

                // Remove the no cost centers notification.
                $('#notificationHost').notificationCenter('removeNotification', noCostCentersNotification);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var costCentersData = ko.mapping.fromJS(data, costCenterMapping);

                    // Only populate the list of fiscal years dropdown the first time the page is loaded.
                    if (self.PopulateFiscalYearDropDown() === true) {
                        self.FiscalYears = data.FiscalYears;
                        if (self.SelectedFiscalYear() === "") {
                            self.SelectedFiscalYear(costCentersData.TodaysFiscalYear());
                        }
                        // Do not populate the fiscal year drop-down unless the page is reloading.
                        self.PopulateFiscalYearDropDown(false);
                    }

                    // Clear and repopulate the cost centers array.
                    self.CostCenters.removeAll();
                    self.CostCenters.pushAll(costCentersData.CostCenters());

                    // Clear and repopulate the saved filter names array.
                    self.SavedFilterNames.removeAll();
                    self.SavedFilterNames.pushAll(costCentersData.SavedFilterNames());

                    self.HasAnyExpense(costCentersData.HasAnyExpense());
                    self.HasAnyRevenue(costCentersData.HasAnyRevenue());

                    self.isFilterCriteriaEntered(true);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = unableToLoadCostCentersText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                // Once the ajax request is completed (regardless of success/error), set isLoaded to true, which
                // disables any spinners/loading icons and other related UI updates.
                self.IsPageLoaded(true);

                // Hide the drop-down spinner.
                self.IsFilterUpdating(false);

                self.CostCentersCount(self.CostCenters().length);

                // always apply "display: block" to ui-progressbar-value to show text on all bar graphs
                $('.ui-progressbar-value').css({ display: "block" });

                // Show a notification if there are no cost centers
                if (self.CostCenters().length == 0) {
                    $('#notificationHost').notificationCenter('addNotification', noCostCentersNotification);
                }
                if (self.bindExportClickEvent()) {
                    $("#download-dropdown").on("click", function () {
                        $(this).toggleClass("esg-is-open");
                        $(this).toggleClass("css-is-open");
                    });
                    $("#saved-filters-dropdown").on("click", self.toggleSavedFilters);
                    self.bindExportClickEvent(false);
                }
            }
        });
    };

    self.applyFilterToCurrentTab = function () {
        if (self.isCostCentersTabActive()) {
            self.getFilteredCostCenters();
        }
        else {
            self.getFilteredObjectData();
        }
    };

    self.getFilteredObjectData = function () {
        self.triggerAllEnterKeys();

        // Get the selected fiscal year and saved filter from browser memory so it can be used by all the views.
        // Only invoke the code below if the user selected a new fiscal year.
        if (!self.IsPageLoaded()) {
            var fiscalYearFromMemory = memory.getItem("fiscalYear");
            if (fiscalYearFromMemory) {
                self.SelectedFiscalYear(fiscalYearFromMemory);
            }

            var selectedSavedFilterFromMemory = memory.getItem("selectedSavedFilter");
            if (selectedSavedFilterFromMemory) {
                self.selectedSavedFilterLabel(selectedSavedFilterFromMemory);
            }

            var showNoActivityFromMemory = memory.getItem("show-no-activity");
            if (showNoActivityFromMemory) {
                self.showActiveAccountsWithNoActivity(showNoActivityFromMemory.toLowerCase() === "true");
            }
        }

        var noObjectCodesNotification = {
            message: Ellucian.ColleagueFinance.noObjectCodesToDisplayText,
            type: "info"
        };

        // Save the filter criteria to browser memory when applying filters.
        self.updateMemory();

        // Determine the fiscal year to use from browser memory. If there isn't one in memory
        // the fiscal year will be default to today's date in the API.
        // Initialize the query URL passing the fiscal year as a parameter.

        // Build the request (and set the active filter)
        var components = self.FormatFilterComponentsForRequest(true);

        self.currentFilterActiveAccountsWithNoActivity(self.showActiveAccountsWithNoActivity());

        var data = { criteria: components, aawna: self.showActiveAccountsWithNoActivity(), fiscalYear: self.SelectedFiscalYear() };

        $.ajax({
            url: Ellucian.ColleagueFinance.getFilteredObjectDataAsyncUrl,
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            data: ko.toJSON(data),
            beforeSend: function () {
                // Clear out the object data and reset the accordians to be collapsed (for performance)
                self.ObjectData(null);
                self.DisplayAssetCodes(false);
                self.DisplayLiabilityCodes(false);
                self.DisplayFundBalanceCodes(false);
                self.DisplayRevenueCodes(false);
                self.DisplayExpenseCodes(false);
                self.expandedObjects.removeAll();

                // Show the filter spinner
                self.IsFilterUpdating(true);

                // Remove the no object codes notification.
                $('#notificationHost').notificationCenter('removeNotification', noObjectCodesNotification);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    // Only populate the list of fiscal years dropdown the first time the page is loaded.
                    if (self.PopulateFiscalYearDropDown() === true) {
                        self.FiscalYears = data.FiscalYears;
                        if (self.SelectedFiscalYear() === "") {
                            self.SelectedFiscalYear(data.TodaysFiscalYear);
                        }
                        // Do not populate the fiscal year drop-down unless the page is reloading.
                        self.PopulateFiscalYearDropDown(false);
                    }

                    self.ObjectData(data.ObjectData);

                    // Clear and repopulate the saved filter names array.
                    var mappedData = ko.mapping.fromJS(data, {});
                    self.SavedFilterNames.removeAll();
                    self.SavedFilterNames.pushAll(mappedData.SavedFilterNames());

                    self.HasAnyExpense(data.ObjectData.HasAnyExpense);
                    self.HasAnyRevenue(data.ObjectData.HasAnyRevenue);
                    self.HasAnyAsset(data.ObjectData.HasAnyAsset);
                    self.HasAnyLiability(data.ObjectData.HasAnyLiability);
                    self.HasAnyFundBalance(data.ObjectData.HasAnyFundBalance);

                    self.isFilterCriteriaEntered(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = Ellucian.ColleagueFinance.unableToLoadObjectDataText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                // Once the ajax request is completed (regardless of success/error), set isLoaded to true, which
                // disables any spinners/loading icons and other related UI updates.
                self.IsPageLoaded(true);

                // Hide the filter spinner.
                self.IsFilterUpdating(false);

                self.initializeObjectRows();

                // Show a notification if there are no object codes
                if (self.ObjectCodesCount() == 0) {
                    $('#notificationHost').notificationCenter('addNotification', noObjectCodesNotification);
                }
                // Determine if we need to bind the click event for the export drop down
                if (self.bindExportClickEvent()) {
                    $("#download-dropdown").on("click", function () {
                        $(this).toggleClass("esg-is-open");
                        $(this).toggleClass("css-is-open");
                    });
                    $("#saved-filters-dropdown").on("click", self.toggleSavedFilters);
                    self.bindExportClickEvent(false);
                }
            }
        });
    }

    // Set the active cost center subtotals in an array
    // Convert the memory item comma-delimited string and convert it into an array
    self.initializeObjectRows = function () {
        var activeObjectRows = memory.getItem("activeObjectRows");

        if (activeObjectRows !== null && activeObjectRows !== "") {
            var activeObjectRowsArray = activeObjectRows.split(",");
            // if there are any cost center subtotals that have been expanded, loop through
            // the list of subtotal IDs, display them as expanded.
            if (activeObjectRowsArray !== null && activeObjectRowsArray.length > 0) {
                // Iterate through array to evaluate which rows need to be expanded
                for (var i = 0; i < activeObjectRowsArray.length; i++) {
                    var rowId = "#" + activeObjectRowsArray[i];

                    // pass in jQuery element of row previously selected, animate parameter is false
                    Ellucian.ColleagueFinance.initializeActiveRows($(rowId));
                }
            }
        }
    }

    // Update browser memory with tokenizedFilterCriteria, showActiveAccountsWithNoActivity state, and selectedSavedFilterLabel.
    self.updateMemory = function () {
        for (var i = 0; i < self.MajorComponents().length; i++) {
            memory.setItem(self.MajorComponents()[i].ComponentName(), self.MajorComponents()[i].tokenizedFilterCriteria());
        }

        memory.setItem('show-no-activity', self.showActiveAccountsWithNoActivity());

        // Store the selected saved filter in memory.
        // If there isn't one selected yet then default it to blank.
        if (typeof self.selectedSavedFilterLabel !== 'undefined' && self.selectedSavedFilterLabel() !== null) {
            memory.setItem("selectedSavedFilter", self.selectedSavedFilterLabel());
        }
    };

    // Display the grid view
    self.toggleGrid = function () {
        self.isBarGraphViewActive(true);
        self.setUIPreference(true);
    }

    // Display the list view
    self.toggleList = function () {
        self.isBarGraphViewActive(false);
        self.setUIPreference(false);
    }

    // Export data for cost centers or object codes
    self.flatExportData = ko.computed(function () {

        if (self.isCostCentersTabActive()) {
            var costCenters = self.CostCenters().map(function buildExportObject(costCenter) {
                return {
                    "UnitId": "'" + costCenter.UnitId() + "'",
                    "Name": costCenter.Name(),
                    "TotalBudgetRevenue": costCenter.TotalBudgetRevenue(),
                    "TotalActualsRevenue": costCenter.TotalActualsRevenue(),
                    "TotalBudget": costCenter.TotalBudget(),
                    "TotalExpensesFormatted": costCenter.TotalExpensesFormatted(),
                    "BudgetRemainingFormatted": costCenter.BudgetRemainingFormatted(),
                    "ListViewBudgetSpentPercent": costCenter.ListViewBudgetSpentPercent()
                };
            });

            return costCenters;
        }
        else {
            var resultData = [];
            if (self.ObjectCodesCount() > 0) {
                function flattenObjects(objects) {
                    for (var i = 0; i < objects.length; i++) {
                        var object = objects[i];

                        for (var k = 0; k < object.GlAccounts.length; k++) {
                            var sequencedGlAccount = object.GlAccounts[k];
                            if (sequencedGlAccount.IsUmbrellaVisible) {
                                resultData.push({
                                    "GlClass": sequencedGlAccount.PrimaryGlAccount.FormattedGlClass,
                                    "GlAccount": sequencedGlAccount.PrimaryGlAccount.FormattedGlAccount,
                                    "Description": sequencedGlAccount.PrimaryGlAccount.Description,
                                    "Budget": sequencedGlAccount.PrimaryGlAccount.Budget,
                                    "Actuals": sequencedGlAccount.PrimaryGlAccount.Actuals,
                                    "Encumbrances": sequencedGlAccount.PrimaryGlAccount.Encumbrances,
                                    "Remaining": sequencedGlAccount.PrimaryGlAccount.BudgetRemaining,
                                    "Processed": sequencedGlAccount.PrimaryGlAccount.PercentProcessed
                                });
                            }
                            else {
                                resultData.push({
                                    "GlClass": sequencedGlAccount.PrimaryGlAccount.FormattedGlClass,
                                    "GlAccount": sequencedGlAccount.PrimaryGlAccount.FormattedGlAccount,
                                    "Description": sequencedGlAccount.PrimaryGlAccount.Description,
                                    "Budget": Ellucian.ColleagueFinance.maskString,
                                    "Actuals": Ellucian.ColleagueFinance.maskString,
                                    "Encumbrances": Ellucian.ColleagueFinance.maskString,
                                    "Remaining": Ellucian.ColleagueFinance.maskString,
                                    "Processed": Ellucian.ColleagueFinance.maskString
                                });
                            }

                            if (sequencedGlAccount.IsPooledAccount) {
                                var poolees = sequencedGlAccount.Poolees;
                                for (var j = 0; j < poolees.length; j++) {
                                    var poolee = poolees[j];
                                    if (sequencedGlAccount.IsUmbrellaVisible || sequencedGlAccount.FormattedGlAccount !== poolee.FormattedGlAccount) {
                                        resultData.push({
                                            "GlClass": poolee.FormattedGlClass,
                                            "GlAccount": poolee.FormattedGlAccount,
                                            "Description": poolee.Description,
                                            "Budget": poolee.Budget,
                                            "Actuals": poolee.Actuals,
                                            "Encumbrances": poolee.Encumbrances,
                                            "Remaining": "",
                                            "Processed": ""
                                        });
                                    }
                                }
                            }
                        }

                    }
                }

                flattenObjects(self.ObjectData().AssetObjectCodes);
                flattenObjects(self.ObjectData().LiabilityObjectCodes);
                flattenObjects(self.ObjectData().FundBalanceObjectCodes);
                flattenObjects(self.ObjectData().RevenueObjectCodes);
                flattenObjects(self.ObjectData().ExpenseObjectCodes);
            }
            return resultData;
        }
    }).extend({ deferred: true });

    self.downloadName = ko.computed(function () {
        if (self.isCostCentersTabActive()) {
            return downloadCostCentersFileName + self.SelectedFiscalYear() + '.csv'
        }
        else {
            return Ellucian.ColleagueFinance.downloadObjectFileName + self.SelectedFiscalYear() + '.csv'
        }
    });

    // The columns to include in the CSV export
    self.csvColumns = ko.computed(function () {
        // Cost Center viewexport
        if (self.isCostCentersTabActive()) {
            var columns = ko.observableArray([
                'UnitId',
                'Name'
            ]);
            if (self.HasAnyRevenue()) {
                columns.pushAll([
                    'TotalBudgetRevenue',
                    'TotalActualsRevenue'
                ]);
            }
            if (self.HasAnyExpense()) {
                columns.pushAll([
                    'TotalBudget',
                    'TotalExpensesFormatted',
                    'BudgetRemainingFormatted',
                    'ListViewBudgetSpentPercent'
                ]);
            }
            return columns();
        }
        else {
            // Object view export
            var objectColumns = [
                'GlClass',
                'GlAccount',
                'Budget',
                'Actuals',
                'Encumbrances',
                'Remaining',
                'Processed'
            ];

            return objectColumns;
        }
    });

    // The replacement headers for each CSV column
    self.csvColumnHeaders = ko.computed(function () {
        if (self.isCostCentersTabActive()) {
            var columns = ko.observableArray([
                Ellucian.ColleagueFinance.costCenterIdHeader,
                Ellucian.ColleagueFinance.costCenterDescriptionHeader
            ]);
            if (self.HasAnyRevenue()) {
                columns.pushAll([
                    Ellucian.ColleagueFinance.listViewHeaderBudgetedRevenue,
                    Ellucian.ColleagueFinance.listViewHeaderActualRevenue
                ]);
            }
            if (self.HasAnyExpense()) {
                columns.pushAll([
                    Ellucian.ColleagueFinance.listViewHeaderBudget,
                    Ellucian.ColleagueFinance.listViewHeaderActualsEncumbrances,
                    Ellucian.ColleagueFinance.remainingExpensesLabel,
                    Ellucian.ColleagueFinance.percentSpentHeaderLabel
                ]);
            }
            return columns();
        }
        else {
            var objctColumns = [
                Ellucian.ColleagueFinance.glClassHeader,
                Ellucian.ColleagueFinance.glAccountHeader,
                Ellucian.ColleagueFinance.budgetHeaderLabel,
                Ellucian.ColleagueFinance.actualsHeaderLabel,
                Ellucian.ColleagueFinance.encumbrancesHeaderLabel,
                Ellucian.ColleagueFinance.remainingHeaderLabel,
                Ellucian.ColleagueFinance.percentSpentReceivedHeaderLabel
            ];
            return objctColumns;
        }
    });

    // Change the spinner's arguments based on which spinner is active
    self.activeSpinner = ko.computed(function () {
        var activeSpinner = { isVisible: self.IsFilterUpdating, message: ko.observable(Ellucian.ColleagueFinance.updatingFilterMessage) };

        if (self.isFilterSaving()) {
            activeSpinner.isVisible = self.isFilterSaving;
            activeSpinner.message(Ellucian.ColleagueFinance.savingFilterMessage);
        }
        else if (self.isLoadingSavedFilterCriteria()) {
            activeSpinner.isVisible = self.isLoadingSavedFilterCriteria;
            activeSpinner.message(Ellucian.ColleagueFinance.gettingSavedFilterMessage);
        }
        else if (self.isDeletingSavedFilter()) {
            activeSpinner.isVisible = self.isDeletingSavedFilter;
            activeSpinner.message(Ellucian.ColleagueFinance.deletingSavedFilterMessage);
        }
        else if (self.isSavingDefaultFilter()) {
            activeSpinner.isVisible = self.isSavingDefaultFilter;
            activeSpinner.message(Ellucian.ColleagueFinance.savingDefaultFilterMessage);
        }
        else if (self.expanding()) {
            activeSpinner.isVisible = self.expanding;
            activeSpinner.message(Ellucian.ColleagueFinance.expandingMessage);
        }
        else if (self.collapsing()) {
            activeSpinner.isVisible = self.collapsing;
            activeSpinner.message(Ellucian.ColleagueFinance.collapsingMessage);
        }

        return activeSpinner
    });
}