﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates

function costCenterDetailViewModel() {
    var self = this;

    // "Inherit" the base cost center view model to make use of common observables and functions.
    baseCostCenterViewModel.call(self);

    // "Inherit" the base view model, specify the mobile screen width
    BaseViewModel.call(self, 769);

    // The cost center being displayed.
    self.CostCenter = ko.observable();

    // Boolean flag to indicate if the export button click binding needs to be created.
    self.bindExportClickEvent = ko.observable(true);

    // Check if cost center is defined.
    self.isCostCenterDefined = ko.computed(function () {
        return typeof self.CostCenter() !== 'undefined' && self.CostCenter() !== null;
    }, self);

    // Determine Net row visibility based on data (a cost center must be defined, the page must be loaded, and the cost center must have both revenue and expense)
    self.isNetRowVisible = ko.computed(function () {
        return self.isCostCenterDefined() && self.IsPageLoaded() && self.CostCenter().HasExpense() && self.CostCenter().HasRevenue();
    }, self);

    // Flattened data for CSV export
    self.FlatGlAccounts = ko.computed(function () {
        var accounts = [];

        function flattenSubtotal(sub) {
            ko.utils.arrayMap(sub.SortedGlAccounts(), function (acct) {
                if (acct.IsPooledAccount()) {
                    if (!acct.PooledAccount.IsUmbrellaVisible()) {
                        acct.PooledAccount.Umbrella.TotalBudget(Ellucian.ColleagueFinance.maskString);
                        acct.PooledAccount.Umbrella.TotalActuals(Ellucian.ColleagueFinance.maskString);
                        acct.PooledAccount.Umbrella.TotalEncumbrances(Ellucian.ColleagueFinance.maskString);
                        acct.PooledAccount.Umbrella.TotalExpensesFormatted(Ellucian.ColleagueFinance.maskString);
                        acct.PooledAccount.Umbrella.BudgetRemainingFormatted(Ellucian.ColleagueFinance.maskString);
                        acct.PooledAccount.Umbrella.ListViewBudgetSpentPercent("");
                    }
                    accounts.push(acct.PooledAccount.Umbrella);

                    ko.utils.arrayMap(acct.PooledAccount.Poolees(), function (poolee) {
                        if (acct.PooledAccount.IsUmbrellaVisible() || acct.PooledAccount.Umbrella.GlAccount() !== poolee.GlAccount()) {
                            poolee.TotalBudget(poolee.nonZeroTotalBudget());
                            poolee.BudgetRemainingFormatted("");
                            poolee.ListViewBudgetSpentPercent("");
                            accounts.push(poolee);
                        }
                    });
                }
                else {
                    accounts.push(acct.NonPooledAccount);
                }
            });
        }

        if (self.isCostCenterDefined()) {
            ko.utils.arrayMap(self.CostCenter().CostCenterRevenueSubtotals(), flattenSubtotal);
            ko.utils.arrayMap(self.CostCenter().CostCenterExpenseSubtotals(), flattenSubtotal);
        }
        return accounts;
    }, self);

    // Array that tells the CSV component what columns to include and in what order
    self.csvColumns = [
        'FormattedGlAccount',
        'Description',
        'TotalBudget',
        'TotalActuals',
        'TotalEncumbrances',
        'BudgetRemainingFormatted',
        'ListViewBudgetSpentPercent'];
    // Array that tells the CSV component how to rename the columns
    self.csvColumnHeaders = [
        Ellucian.ColleagueFinance.glAccountNumberText,
        Ellucian.ColleagueFinance.descriptionHeader,
        Ellucian.ColleagueFinance.budgetHeaderLabel,
        Ellucian.ColleagueFinance.actualsHeaderLabel,
        Ellucian.ColleagueFinance.encumbrancesHeaderLabel,
        Ellucian.ColleagueFinance.remainingHeaderLabel,
        Ellucian.ColleagueFinance.percentSpentReceivedHeaderLabel];

    // We need the major components in order to populate the filter criteria from browser memory.
    self.getMajorComponentsThenCostCenter = function () {

        var noCostCenterNotification = {
            message: noCostCenterToDisplayText,
            type: "info"
        };

        $.ajax({
            url: getMajorComponentsActionUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                // Remove the no cost centers notification.
                $('#notificationHost').notificationCenter('removeNotification', noCostCenterNotification);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var majorComponentData = [];
                    for (var i = 0; i < data.length; i++) {
                        majorComponentData.push(new generalLedgerComponentModel(data[i]));
                    }

                    self.MajorComponents.pushAll(majorComponentData);

                    // Determine if memory storage has values for the major components
                    for (var i = 0; i < self.MajorComponents().length; i++) {
                        var component = self.MajorComponents()[i];
                        var name = component.ComponentName();
                        var memoryValues = memory.getItem(name);
                        if (memoryValues) {
                            component.tokenizedFilterCriteria.pushAll(memoryValues.split(","));
                        }
                    }

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = unableToLoadCostCenterText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                self.getFilteredCostCenter();
            }
        });
    }

    // Execute the AJAX request to get cost centers based on the filter criteria that the user has chosen.
    self.getFilteredCostCenter = function () {
        // Get the selected fiscal year and saved filter from browser memory so it can be used by all the views.
        // Only invoke the code below if the user selected a new fiscal year.
        if (!self.IsPageLoaded()) {
            var fiscalYearFromMemory = memory.getItem("fiscalYear");
            if (fiscalYearFromMemory) {
                self.SelectedFiscalYear(fiscalYearFromMemory);
            }

            var showNoActivityFromMemory = memory.getItem("show-no-activity");
            if (showNoActivityFromMemory) {
                self.currentFilterActiveAccountsWithNoActivity(showNoActivityFromMemory.toLowerCase() === "true");
            }
        }

        var noCostCenterNotification = {
            message: noCostCenterToDisplayText,
            type: "info"
        };

        // Determine the fiscal year to use from browser memory. If there isn't one in memory
        // the fiscal year will be default to today's date in the API.
        // Initialize the query URL passing the fiscal year as a parameter.

        var components = self.FormatFilterComponentsForRequest(true);

        var data = { id: costCenterId, aawna: self.currentFilterActiveAccountsWithNoActivity(), fiscalYear: self.SelectedFiscalYear(), criteria: components };

        $.ajax({
            url: getFilteredCostCenterActionUrl,
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            data: ko.toJSON(data),
            beforeSend: function (data) {
                // Show the drop-down spinner (only if the page isn't loaded yet)
                if (self.IsPageLoaded() === true) {
                    self.IsFilterUpdating(true);
                }

                // Remove the no cost centers notification.
                $('#notificationHost').notificationCenter('removeNotification', noCostCenterNotification);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var costCenterData = ko.mapping.fromJS(data, costCenterMapping);

                    // Only populate the list of fiscal years dropdown the first time the page is loaded.
                    if (self.PopulateFiscalYearDropDown() === true) {
                        self.FiscalYears.pushAll(costCenterData.FiscalYears());
                        if (self.SelectedFiscalYear() === "") {
                            self.SelectedFiscalYear(costCenterData.TodaysFiscalYear());
                        }
                        // Do not populate the fiscal year drop-down unless the page is reloading.
                        self.PopulateFiscalYearDropDown(false);
                    }

                    // Assign the cost center
                    self.CostCenter(costCenterData.CostCenter);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Default message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = unableToLoadCostCenterText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {

                self.IsPageLoaded(true);

                self.IsFilterUpdating(false);

                self.PopulateFiscalYearDropDown(false);

                // Expand the subtotals that were expanded the last time we were on the page.
                self.setCostCenterSubtotals();

                // Show a notification if there are no cost centers
                if (!self.isCostCenterDefined() || self.CostCenter().CostCenterId() === null || self.CostCenter().CostCenterId() === "" || typeof self.CostCenter().CostCenterId() === 'undefined') {
                    $('#notificationHost').notificationCenter('addNotification', noCostCenterNotification);
                }
                if (self.bindExportClickEvent()) {
                    $("#download-dropdown").on("click", function () {
                        $(this).toggleClass("esg-is-open");
                        $(this).toggleClass("css-is-open");
                    });
                    self.bindExportClickEvent(false);
                }
            }
        });
    };

    // Set the active cost center subtotals in an array
    // Convert the memory item comma-delimited string and convert it into an array
    self.setCostCenterSubtotals = function () {
        var activeCostCenterSubtotals = memory.getItem("activeCostCenterSubtotal");

        if (activeCostCenterSubtotals !== null && activeCostCenterSubtotals !== "") {
            var activeCostCenterSubtotalsArray = activeCostCenterSubtotals.split(",");
            // if there are any cost center subtotals that have been expanded, loop through
            // the list of subtotal IDs, display them as expanded.
            if (activeCostCenterSubtotalsArray !== null && activeCostCenterSubtotalsArray.length > 0) {
                // Iterate through array to evaluate which rows need to be expanded
                for (var i = 0; i < activeCostCenterSubtotalsArray.length; i++) {
                    var rowId = "#" + activeCostCenterSubtotalsArray[i];

                    // pass in jQuery element of row previously selected, animate parameter is false
                    Ellucian.ColleagueFinance.initializeActiveRows($(rowId));
                }
            }
        }
    }

    // Change desktop structure to mobile structure
    self.changeToMobile = function () {

    }

    // Change mobile structure back to desktop view
    self.changeToDesktop = function () {

    }

    // Check for mobile and set the isMobile observable as determined (defined in BaseViewModel.
    self.checkForMobile(window, document);

}