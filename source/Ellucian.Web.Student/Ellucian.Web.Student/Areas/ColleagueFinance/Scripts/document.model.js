﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

// Javascript representation of a document
function documentModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects
    ko.mapping.fromJS(data, documentMapping, self);

    // Set up a generic set of items to represent the "line items" section of every document
    self.DocumentItems = ko.computed(function () {
        var items = [];
        
        // Vouchers, purchase orders, requisitions and journal entries line items
        if (self.IsVoucher() || self.IsPurchaseOrder() || self.IsRequisition() || self.IsJournalEntry()) {
            items = self.LineItems();
        }

        // Blanket purchase order GL distributions
        if (self.IsBlanketPurchaseOrder()) {
            items = self.GlDistributions();
        }

        // Recurring voucher schedules
        if (self.IsRecurringVoucher()) {
            items = self.Schedules();
        }

        return items;
    }, self);

    // Build a string that represents the 'back link text' for the mobile sliding panel
    self.documentItemType = ko.computed(function () {
        var linkText = backToText;

        // Vouchers, purchase orders, requisitions and journal entries line items
        if (self.IsJournalEntry()) {
            linkText += jeItemsLabelText;
        }
        else {
            linkText += lineItemsLabelText;
        }

        return linkText;
    }, self);

    // Calculate the link using the ID of the document associated to the transaction.
    // This property is used only for Voucher-type documents and is necessary because
    // a voucher can only have one associated document and we need to figure out the
    // the type.
    self.AssociatedDocumentLink = ko.computed(function () {
        var url = "";
        if (self.IsVoucher()) {
            if (self.IsAssociatedDocumentPurchaseOrder()) {
                url = Ellucian.ColleagueFinance.getPurchaseOrderUrl;
            }
            if (self.IsAssociatedDocumentBlanketPurchaseOrder()) {
                url = Ellucian.ColleagueFinance.getBlanketPurchaseOrderUrl;
            }
            if (self.IsAssociatedDocumentRecurringVoucher()) {
                url = Ellucian.ColleagueFinance.getRecurringVoucherUrl;
            }
            url += '/' + self.AssociatedDocument() + self.CompleteDocumentUrl();
        }
        return url;
    }, self);

    self.AssociatedVoucherLink = function (recordId) {
        return Ellucian.ColleagueFinance.getVoucherUrl + '/' + recordId + self.CompleteDocumentUrl();
    }

    self.AssociatedRecurringVoucherLink = function (recordId) {
        return Ellucian.ColleagueFinance.getRecurringVoucherUrl + '/' + recordId + self.CompleteDocumentUrl();
    }

    self.AssociatedPurchaseOrderLink = function (recordId) {
        return Ellucian.ColleagueFinance.getPurchaseOrderUrl + '/' + recordId + self.CompleteDocumentUrl();
    }

    self.AssociatedRequisitionLink = function (recordId) {
        return Ellucian.ColleagueFinance.getRequisitionUrl + '/' + recordId + self.CompleteDocumentUrl();
    }

    self.AssociatedBlanketPurchaseOrderLink = function (recordId) {
        return Ellucian.ColleagueFinance.getBlanketPurchaseOrderUrl + '/' + recordId + self.CompleteDocumentUrl();
    }

    self.CompleteDocumentUrl = function () {
        var partialUrl = '/' + encodeURIComponent(Ellucian.ColleagueFinance.glNumber);
        if (Ellucian.ColleagueFinance.projectId) {
            partialUrl = '/' + encodeURIComponent(Ellucian.ColleagueFinance.glNumber) + '/' + Ellucian.ColleagueFinance.projectId;
        }
        return partialUrl;
    };
}