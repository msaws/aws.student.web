﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// Set up viewModel instance
var costCenterSummaryViewModelInstance = new costCenterSummaryViewModel(window.location.hash);

// Register CsvDownloadLink component
ko.components.register('csv-download-link', {
    require: 'CsvDownloadLink/_CsvDownloadLink'
});

// Register the FilterControl component
ko.components.register('filter-control', {
    require: 'FilterControl/_FilterControl'
});

// Register the FilterSummary component
ko.components.register('filter-summary', {
    require: 'FilterSummary/_FilterSummary'
});

// Register the YesNoToggle component
ko.components.register('yes-no-toggle', {
    require: 'YesNoToggle/_YesNoToggle'
});

// Once the page has loaded...
$(document).ready(function () {
    ko.applyBindings(costCenterSummaryViewModelInstance, document.getElementById("main"));


    // Execute AJAX request to get cost centers for the user
    costCenterSummaryViewModelInstance.getMajorComponentsThenCostCenters();

    $(window).scroll(function (e) {
        if ($('html').hasClass('active-filter')) {
            var $el = $('#cost-centers-filter-wrapper');
            $el.removeClass("transition");
            var isPositionFixed = ($el.css('position') == 'fixed');

            if ($(this).scrollTop() > $('#dp-monolith').offset().top + 12 && !isPositionFixed && $("html").outerHeight(true) > $("#cost-centers-filter").outerHeight(true) + 300) {
                $el.addClass('fixed');
            }
            if ($(this).scrollTop() < $('#dp-monolith').offset().top + 12 && isPositionFixed) {
                $el.removeClass('fixed');
            }
            $el.addClass("transition");
        }
    });
    $(document).mouseup(function (e) {
        closeExportDropDown(e);
    });
});

function closeExportDropDown(clickEvent) {
    var close = true;
    // If the click is within the drop down list don't close it. (And if the click is within the button, don't fire the click event again - let the standard click event handle it.)
    if ($("#download-menu").is(clickEvent.target) || $("#download-menu").has(clickEvent.target).length > 0 ||
        $("#download-dropdown").is(clickEvent.target) || $("#download-dropdown").has(clickEvent.target).length > 0) {
        close = false;
    }

    if (close && $("#download-menu").is(':visible')) {
        $("#download-dropdown").click();
    }
}