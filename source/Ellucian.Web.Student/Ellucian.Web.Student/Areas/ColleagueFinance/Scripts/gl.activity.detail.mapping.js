﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

// Configure a knockout mapping object for the GL Activity Detail.
var glActivityDetailMapping = {
    'GlAccountActivityDetail': {
        create: function (options) {
            return new glActivityDetailModel(options.data);
        }
    },
    'BudgetTransactions': {
        // The create attribute tells knockout how to create a budget transactions observable .
        create: function (options) {
            return new glTransactionViewModel(options.data);
        }
    },
    'EncumbranceTransactions': {
        // The create attribute tells knockout how to create an encumbrance transactions observable .
        create: function (options) {
            return new glTransactionViewModel(options.data);
        }
    },
    'ActualTransactions': {
        // The create attribute tells knockout how to create a actuals transactions observable .
        create: function (options) {
            return new glTransactionViewModel(options.data);
        }
    },
};