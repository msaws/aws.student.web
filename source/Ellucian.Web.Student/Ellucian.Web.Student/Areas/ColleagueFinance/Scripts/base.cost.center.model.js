﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// Base "class" containing computed observables common to cost centers, subtotals, and GL accounts.
function baseCostCenterModel() {
    var self = this;

    self.remainingBudgetColor = ko.computed(function () {
        if (self.BudgetRemaining) {
            return self.BudgetRemaining() < 0 ? "remaining-red" : "remaining-black";
        }
        else {
            return "";
        }
    }, self);

    self.remainingBudgetRevenueColor = ko.computed(function () {
        if (self.BudgetRemainingRevenue) {
            return self.BudgetRemainingRevenue() < 0 ? "remaining-green" : "remaining-black";
        }
        else {
            return "";
        }
    }, self);

    self.overageColor = ko.computed(function () {
        var style = "";
        if ((self.TotalBudget && self.TotalBudget() == 0) || (self.Overage && self.Overage() >= 100)) {
            style = "zero-budget";
        }

        if (self.Overage && self.Overage() > 0) {
            style = style + " overage";
        }
        return style;
    }, self);


    self.overageRevenueColor = ko.computed(function () {
        var style = "";
        if ((self.TotalBudgetRevenue && self.TotalBudgetRevenue() == 0) || (self.OverageRevenue && self.OverageRevenue() >= 100)) {
            style = "zero-budget";
        }

        if (self.OverageRevenue && self.OverageRevenue() > 0) {
            style = style + " overage-revenue";
        }
        return style;
    }, self);

    // Determine financial health style
    self.financialHealthIndicator = ko.computed(function () {

        var financialHealthIcon = "";

        if (self.HasExpense && !self.HasExpense()) {
            return financialHealthIcon;
        }
        else if (self.BudgetRemaining && self.BudgetRemaining() < 0) {
            financialHealthIcon = "cost-center-icon-red";
        }
        else if (self.BudgetSpentPercent && self.BudgetSpentPercent() >= 85 && self.BudgetSpentPercent() <= 100) {
            financialHealthIcon = "cost-center-icon-yellow";
        }
        else {
            financialHealthIcon = "cost-center-icon-green";
        }
        return financialHealthIcon;
    }, self);
};