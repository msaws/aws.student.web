﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

// Javascript representation of a document line item
function documentLineItemGlDistributionModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects
    ko.mapping.fromJS(data, documentMapping, self);
};