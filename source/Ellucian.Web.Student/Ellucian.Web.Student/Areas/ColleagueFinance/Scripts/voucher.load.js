﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

// Initialize the ColleagueFinance namespace object for use in ColleagueFinance module.
var Ellucian = Ellucian || {};
Ellucian.ColleagueFinance = Ellucian.ColleagueFinance || {};

// Set up viewModel instance
var documentViewModelInstance = new documentViewModel();

// Once the page has loaded...
$(document).ready(function () {

    ko.applyBindings(documentViewModelInstance, document.getElementById("main"));

    // Initialize the query URL
    var voucherQueryUrl = Ellucian.ColleagueFinance.getVoucherAsyncUrl + "?voucherId=" + Ellucian.ColleagueFinance.documentId + "&generalLedgerAccountId=" + Ellucian.ColleagueFinance.glNumber;

    // Get the document
    documentViewModelInstance.loadDocument(voucherQueryUrl, unableToLoadVoucherText);
});