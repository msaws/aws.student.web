﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

// Javascript representation of an approver object
function documentApproverModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects
    ko.mapping.fromJS(data, documentMapping, self);

    // Determine which icon to display for the approver
    self.approvalStatusIcon = ko.computed(function () {
        return self.Date() == "" ? "document-waiting-icon" : "document-approved-icon";
    }, self);

    // Style the approval date "awaiting approval" if there is no date.
    self.awaitingApproval = ko.computed(function () {
        return self.Date() == "" ? "awaiting-approval" : "";
    }, self);
};