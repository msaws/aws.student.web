﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// Configure a knockout mapping object for the cost centers
var costCenterMapping = {
    'CostCenters': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterModel(options.data);
        }
    },
    'CostCenter': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterModel(options.data);
        }
    },
    'CostCenterExpenseSubtotals': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterSubtotalModel(options.data);
        }
    },
    'CostCenterRevenueSubtotals': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterSubtotalModel(options.data);
        }
    },
    'SortedGlAccounts': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterAccountSequenceModel(options.data);
        }
    },
    'NonPooledAccount': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterGlAccountModel(options.data);
        }
    },
    'PooledAccount': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new glBudgetPoolModel(options.data);
        }
    },
    'Umbrella': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterGlAccountModel(options.data);
        }
    },
    'Poolees': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new costCenterGlAccountModel(options.data);
        }
    },
    'MajorComponents': {
        // The create attribute tells knockout how to create a cost center observable 
        create: function (options) {
            return new generalLedgerComponentModel(options.data);
        }
    },
    'SavedFilters': {
        create: function (options) {
            return new savedFilterModel(options.data);
        }
    }
};