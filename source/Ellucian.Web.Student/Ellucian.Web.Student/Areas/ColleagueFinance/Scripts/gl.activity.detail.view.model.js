﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates

//Select which storage mechanism to use depending on browser.
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();


function glActivityDetailViewModel() {
    var self = this;

    // "Inherit" the base view model, specify the mobile screen width
    BaseViewModel.call(self, 769);

    // Keeps track of the activity for a GL account.
    self.GlAccountActivity = ko.observable(null);

    // Used to keep track of which GL Account number we're looking at when in GL Detail mode
    self.GlNumberSelected = ko.observable("");

    // Track which tab is selected when in mobile view.
    self.GlDetailTabSelected = ko.observable("");

    // Keep track of the spinner state
    self.IsGlActivityLoading = ko.observable(false);

    // Flag that indicates if the initial ajax request has completed and data has been loaded into the page.
    self.IsPageLoaded = ko.observable(false);

    // Used to know whether to populate the list of fiscal years drop-down.
    self.PopulateFiscalYearDropDown = ko.observable(true);

    // Hold the selected fiscal year.
    self.SelectedFiscalYear = ko.observable("");

    // New list of fiscal years for a drop-down display to choose from.
    self.FiscalYears = ko.observableArray();

    // Keep track of the DDL state.
    self.isExportDropdownExpanded = ko.observable(false);

    // Css class for the export DDL
    self.exportDropdownCss = ko.computed(function () {
        if (self.isExportDropdownExpanded()) {
            return "esg-is-open css-is-open";
        }
        else {
            return ""
        }
    });

    self.clickTransaction = function (data) {
        if (data.isLinkable()) {
            window.document.location = self.fullDocumentUrl(data);
        }
    };

    self.fullDocumentUrl = function (data) {
        return data.documentLink() + '/' + self.GlAccountActivity().GlAccount();
    };

    // Determine whether or not to show the Budget table.
    self.ShowBudgetTable = ko.computed(function () {
        // If we're not in mobile view then show (return true).
        if (!self.isMobile()) {
            return true;
        }

        // Lastly, if we're in mobile view then determine whether to show the budget, actuals or encumbrances tab
        if (self.isMobile() && self.GlDetailTabSelected() == "B") {
            $(".gl-tab-budget").addClass("gl-detail-tab-selected");
            return true;
        }
        else {
            $(".gl-tab-budget").removeClass("gl-detail-tab-selected");
            return false;
        }
    }, self);

    // Determine whether or not to show the Encumbrances table.
    self.ShowEncumbrancesTable = ko.computed(function () {
        // If we're not in mobile view then show (return true)
        if (!self.isMobile()) {
            return true;
        }

        // Lastly, if we're in mobile view then determine whether to show the budget, actuals or encumbrances tab
        if (self.isMobile() && self.GlDetailTabSelected() == "E") {
            $(".gl-tab-encumbrances").addClass("gl-detail-tab-selected");
            return true;
        }
        else {
            $(".gl-tab-encumbrances").removeClass("gl-detail-tab-selected");
            return false;
        }
    }, self);

    // Determine whether or not to show the Actuals table.
    self.ShowActualsTable = ko.computed(function () {
        // If we're not in mobile view then show (return true).
        if (!self.isMobile()) {
            return true;
        }

        // Lastly, if we're in mobile view then determine whether to show the budget, actuals or encumbrances tab
        if (self.isMobile() && self.GlDetailTabSelected() == "A") {
            $(".gl-tab-actuals").addClass("gl-detail-tab-selected");
            return true;
        }
        else {
            $(".gl-tab-actuals").removeClass("gl-detail-tab-selected");
            return false;
        }
    }, self);

    // Obtain the data for the new fiscal year.
    self.pickFiscalYear = function () {
        // Only invoke the code below if the user selected a new fiscal year.
        if (self.IsPageLoaded() && !self.IsGlActivityLoading()) {

            // Store the selected fiscal year in memory. It can be empty for the default year.
            // If there isn't one selected yet then default it to blank.
            if (typeof self.SelectedFiscalYear() !== 'undefined' && self.SelectedFiscalYear() !== null) {
                memory.setItem("fiscalYear", self.SelectedFiscalYear());
            }
            else {
                memory.setItem("fiscalYear", "");
            }

            self.getGlTransactions(self.GlNumberSelected());
        }
    }

    self.getGlTransactions = function (glNumber) {
        // Make sure the export dropdown is closed because if it's open and the data being loaded has no transactions the export should not be available.
        self.isExportDropdownExpanded(false);

        // Get the selected fiscal year from browser memory so it can be used by all the views.
        // Only invoke the code below if the user selected a new fiscal year.
        if (memory.getItem("fiscalYear") !== null && typeof memory.getItem("fiscalYear") !== 'undefined') {
            self.SelectedFiscalYear(memory.getItem("fiscalYear"));
        }

        self.GlNumberSelected(glNumber);
        // Execute the AJAX call to obtain the transactions for the GL account selected.
        var url = getGlTransactionsAsync + "?glAccount=" + glNumber + "&fiscalYear=" + self.SelectedFiscalYear();
        $.ajax({
            url: url,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                // Show the loading GL activity spinner (only if the page is already loaded)
                if (self.IsPageLoaded() === true) {
                    self.IsGlActivityLoading(true);
                }
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var glAccountActivityData = ko.mapping.fromJS(data, glActivityDetailMapping);

                    // Only populate the list of fiscal years dropdown the first time the page is loaded.
                    if (self.PopulateFiscalYearDropDown()) {
                        self.FiscalYears.pushAll(glAccountActivityData.FiscalYears());
                        if (self.SelectedFiscalYear() === "") {
                            self.SelectedFiscalYear(glAccountActivityData.TodaysFiscalYear());
                        }
                    }

                    // Populate the GL activity.
                    self.GlAccountActivity(glAccountActivityData.GlAccountActivityDetail);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = unableToLoadGlActivityText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                // Remove the spinner for the loading of the GL activity.
                self.IsGlActivityLoading(false);

                self.PopulateFiscalYearDropDown(false);

                self.IsPageLoaded(true);

                self.evaluateTabs();
            }
        });
    }

    // Update browser memory and change the GlDetailTabSelected observable
    self.clickEncumbrancesTab = function () {
        self.GlDetailTabSelected("E");
        memory.setItem("gl-detail-tab", self.GlDetailTabSelected());
    }

    // Update browser memory and change the GlDetailTabSelected observable
    self.clickActualsTab = function () {
        self.GlDetailTabSelected("A");
        memory.setItem("gl-detail-tab", self.GlDetailTabSelected());
    }

    // Update browser memory and change the GlDetailTabSelected observable
    self.clickBudgetTab = function () {
        self.GlDetailTabSelected("B");
        memory.setItem("gl-detail-tab", self.GlDetailTabSelected());
    }

    // Change desktop structure to mobile structure
    self.changeToMobile = function () {
        // Change the colspan of the budget, actuals and encumbrance headers
        $(".actuals-encumbrances-budget-header.gl-details-align-left").attr("colspan", "1");

        // set tab panel accessibility at mobile size only
        $("#encumbrance-panel, #actual-panel, #budget-panel").attr("role", "tabpanel");
    }

    // Change mobile structure back to desktop view
    self.changeToDesktop = function () {
        // Change the colspan of the actuals and encumbrance headers
        $(".actuals-encumbrances-budget-header.gl-details-align-left").attr("colspan", "3");

        // remove tab panel accessibility at desktop size
        $("#encumbrance-panel, #actual-panel, #budget-panel").removeAttr("role aria-hidden");
    }

    // Track if the encumbrances tab is selected
    self.encumbrancesTabSelected = ko.computed(function () {
        return self.GlDetailTabSelected() === "E";
    });

    // Track if the actuals tab is selected
    self.actualsTabSelected = ko.computed(function () {
        return self.GlDetailTabSelected() === "A";
    });

    // Track if the budget tab is selected
    self.budgetTabSelected = ko.computed(function () {
        return self.GlDetailTabSelected() === "B";
    });

    // Get the selected tab from browser memory (or use the default)
    self.evaluateTabs = function () {
        // Determine if there is a tab selected in browser memory and if so, use that value - otherwise default to Encumbrances.
        var tab = memory.getItem("gl-detail-tab");
        if (tab !== null && tab !== "") {
            self.GlDetailTabSelected(tab);
        }
        else {
            self.GlDetailTabSelected("E");
        }
    }

    // Flatten the activity for CSV export
    self.glActivityExportData = ko.computed(function flattenAllActivity() {
        var exportData = [];
        var activity = self.GlAccountActivity();

        function ExportRow(type, documentId, transactionDate, description, amount) {
            var rowData = {
                "Type": type,
                "Document": "'" + documentId + "'",
                "Date": transactionDate,
                "Description": description,
                "Amount": amount
            };
            if (documentId.length === 0) {
                rowData.Document = "";
            }
            return rowData;
        }
        // Flatten and add rows from a list of transactions
        function flattenTransactions(transactions, typeLabel) {
            for (var i = 0; i < transactions.length; i++) {
                var transaction = transactions[i];
                exportData.push(new ExportRow(typeLabel, transaction.ReferenceNumber(), transaction.Date(), transaction.Description(), transaction.Amount()));
            }
        }

        if (activity) {
            // Transaction data (Array)... No pending posting for encumbrances
            flattenTransactions(activity.EncumbranceTransactions(), Ellucian.ColleagueFinance.encumbrancesHeaderLabel);

            // Estimated opening balance (single row - already flat) - Actuals
            if (activity.EstimatedOpeningBalance() != 0) {
                exportData.push(new ExportRow(Ellucian.ColleagueFinance.actualsHeaderLabel, "", "", Ellucian.ColleagueFinance.estimatedOpeningBalanceText, activity.EstimatedOpeningBalanceDisplay()));
            }
            // Closing year amount (single row - already flat) - Actuals
            if (activity.ClosingYearAmount() != 0) {
                exportData.push(new ExportRow(Ellucian.ColleagueFinance.actualsHeaderLabel, "", "", Ellucian.ColleagueFinance.closingYearAmountText, activity.ClosingYearAmountDisplay()));
            }
            // Amounts pending posting (single row - already flat) - Actuals
            if (activity.TotalActualsMemos() != 0) {
                exportData.push(new ExportRow(Ellucian.ColleagueFinance.actualsHeaderLabel, "", "", Ellucian.ColleagueFinance.amountPendingPostingText, activity.TotalActualsMemosDisplay()));
            }
            // Transaction data (Array)...
            flattenTransactions(activity.ActualTransactions(), Ellucian.ColleagueFinance.actualsHeaderLabel);

            // Amounts pending posting (single row - already flat) - Budget
            if (activity.TotalBudgetMemos() != 0) {
                exportData.push(new ExportRow(Ellucian.ColleagueFinance.budgetHeaderLabel, "", "", Ellucian.ColleagueFinance.amountPendingPostingText, activity.TotalBudgetMemosDisplay()));
            }
            // Transaction data (Array)...
            flattenTransactions(activity.BudgetTransactions(), Ellucian.ColleagueFinance.budgetHeaderLabel);
        }

        return exportData;
    }).extend({ deferred: true });

    self.downloadFileName = ko.computed(function () {
        return Ellucian.ColleagueFinance.glDetailExportFileNamePrefix + self.GlNumberSelected() + self.SelectedFiscalYear() + ".csv";
    });

    self.csvColumns = [
        "Type",
        "Document",
        "Date",
        "Description",
        "Amount"
    ];

    self.csvColumnHeaders = [
        Ellucian.ColleagueFinance.transactionTypeHeader,
        Ellucian.ColleagueFinance.documentIdHeader,
        Ellucian.ColleagueFinance.dateHeader,
        Ellucian.ColleagueFinance.descriptionHeader,
        Ellucian.ColleagueFinance.amountHeader
    ];

    self.toggleExportDropdown = function () {
        self.isExportDropdownExpanded(!self.isExportDropdownExpanded());
    };

    // Check for mobile and set the isMobile observable as determined (defined in BaseViewModel).
    self.checkForMobile(window, document);
}