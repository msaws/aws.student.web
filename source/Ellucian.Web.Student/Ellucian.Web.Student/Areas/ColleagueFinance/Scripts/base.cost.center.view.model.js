﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

//Select which storage mechanism to use depending on browser.
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();

// Base "class" containing observables and functions common to both the cost center summary and detail view models.
function baseCostCenterViewModel() {
    var self = this;

    // Flag that indicates if the initial ajax request has completed and data has been loaded into the page.
    self.IsPageLoaded = ko.observable(false);

    // Used for the spinner.
    self.IsFilterUpdating = ko.observable(false);

    // Used to know whether to populate the list of fiscal years drop-down.
    self.PopulateFiscalYearDropDown = ko.observable(true);

    // Hold the selected fiscal year.
    self.SelectedFiscalYear = ko.observable("");

    // Used to determine whether the user is in the cost centers view or the single cost center view.
    self.IsCostCentersView = ko.observable(true);

    self.isFilterCriteriaEntered = ko.observable(false);

    // New list of fiscal years for a drop-down display to choose from.
    self.FiscalYears = ko.observableArray();

    // Obtain the data for the new fiscal year.
    self.pickFiscalYear = function () {
        // Only invoke the code below if the user selected a new fiscal year.
        if (self.IsPageLoaded() && !self.IsFilterUpdating()) {

            // Store the selected fiscal year in memory. It can be empty for the default year.
            // If there isn't one selected yet then default it to blank.
            if (typeof self.SelectedFiscalYear() !== 'undefined' && self.SelectedFiscalYear() !== null) {
                memory.setItem("fiscalYear", self.SelectedFiscalYear());
            }
            else {
                memory.setItem("fiscalYear", "");
            }

            if (self.IsCostCentersView()) {
                // Update the cost centers/object view model with the new fiscal year data.
                self.applyFilterToCurrentTab();
            }
            else {
                // Update the selected cost center with the new fiscal year data.
                self.getFilteredCostCenter();
            }
        }
    }

    // List of major components for the cost centers filter.
    self.MajorComponents = ko.observableArray();

    // Boolean that controls whether accounts with no activity are CURRENTLY being displayed.
    self.currentFilterActiveAccountsWithNoActivity = ko.observable(true);

    // Gather the filter criteria to be displayed as "pills" in the FilterSummary component.
    self.filterSummary = ko.pureComputed(function () {
        var summaries = [];
        for (var i = 0; i < self.MajorComponents().length; i++) {
            if (self.MajorComponents()[i].appliedFilterCriteria().length > 0) {
                summaries.push(self.MajorComponents()[i].ComponentName() + ": " + self.MajorComponents()[i].appliedFilterCriteria().sort().join(", "));
            }
        }

        if (self.currentFilterActiveAccountsWithNoActivity() === false) {
            summaries.push(Ellucian.ColleagueFinance.activeFilterHidingActiveAccountsWithNoActivity);
        }
        return summaries;
    }, self).extend({ deferred: true });

    self.FormatFilterComponentsForRequest = function (updateAppliedFilter) {
        var components = [];

        for (var i = 0; i < self.MajorComponents().length; i++) {
            var component = { ComponentName: self.MajorComponents()[i].ComponentName(), IndividualComponentValues: [], RangeComponentValues: [] };

            if (updateAppliedFilter) {
                self.MajorComponents()[i].appliedFilterCriteria.removeAll();
                self.MajorComponents()[i].appliedFilterCriteria.pushAll(self.MajorComponents()[i].tokenizedFilterCriteria());
            }

            for (var j = 0; j < self.MajorComponents()[i].tokenizedFilterCriteria().length; j++) {
                if (self.MajorComponents()[i].tokenizedFilterCriteria()[j].indexOf("-") > -1) {
                    var se = self.MajorComponents()[i].tokenizedFilterCriteria()[j].replace(/s+/gm, '').split("-");
                    component.RangeComponentValues.push({ StartValue: se[0], EndValue: se[1] });
                }
                else {
                    component.IndividualComponentValues.push(self.MajorComponents()[i].tokenizedFilterCriteria()[j]);
                }
            }

            components.push(component);
        }

        return components;
    };
}