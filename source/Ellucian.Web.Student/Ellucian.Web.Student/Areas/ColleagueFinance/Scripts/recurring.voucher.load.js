﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

// Set up viewModel instance
var documentViewModelInstance = new documentViewModel();

// Once the page has loaded...
$(document).ready(function () {

    ko.applyBindings(documentViewModelInstance, document.getElementById("main"));

    // Initialize the query URL
    var recurringVoucherQueryUrl = Ellucian.ColleagueFinance.getRecurringVoucherAsyncUrl + '?recurringVoucherId=' + Ellucian.ColleagueFinance.documentId + "&generalLedgerAccountId=" + Ellucian.ColleagueFinance.glNumber;

    // Get the document
    documentViewModelInstance.loadDocument(recurringVoucherQueryUrl, unableToLoadRecurringVoucherText);
});