﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// Javascript representation of a cost center subtotal
function costCenterGlAccountModel(data) {
    var self = this;

    ko.mapping.fromJS(data, costCenterMapping, self);

    // "Inherit" the base cost center model to make use of common computed observables.
    baseCostCenterModel.call(self);

    // GL Accounts in cost centers can be either "revenue" or "expense" - not both; use the correct property based on the GL class.
    self.nonZeroTotalBudget = ko.computed(function () {
        if (self.TotalBudget && parseInt(self.TotalBudget().replace(/\D/g, '')) !== 0) {
            return self.TotalBudget();
        }
        else if (self.TotalBudgetRevenue && parseInt(self.TotalBudgetRevenue().replace(/\D/g, '')) !== 0) {
            return self.TotalBudgetRevenue();
        }
        else {
            return "";
        }
    }, self);
};