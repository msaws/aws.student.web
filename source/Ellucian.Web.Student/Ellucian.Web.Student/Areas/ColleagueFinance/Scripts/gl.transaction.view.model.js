﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

// Javascript representation of a GL transaction
function glTransactionViewModel(data) {
    var self = this;

    self.Source = ko.observable("");

    var sourceEnumeration = {
        Encumbrance: "EP",
        Voucher: "PJ",
        JournalEntry: "JE",
        Requisition: "ER"
    }

    // Perform any deeper mapping of child objects
    if (typeof projectMapping !== "undefined") {
        ko.mapping.fromJS(data, projectMapping, self);
    }
    else if (typeof glActivityDetailMapping !== "undefined")
    {
        ko.mapping.fromJS(data, glActivityDetailMapping, self);
    }

    // Determine whether or not to display a link on the page
    // The source is unique for vouchers, requisitions and journal entries; 
    // it is not for encumbrances.
    self.isLinkable = ko.computed(function () {
        var link = false;

        switch (self.Source()) {
            // Voucher
            case sourceEnumeration.Voucher:
                link = true;
                break;

                // Journal Entry
            case sourceEnumeration.JournalEntry:
                link = true;
                break;

                // Requisition
            case sourceEnumeration.Requisition:
                if (self.DocumentId() != null) {
                    link = true;
                }
                break;

                // Encumbrances
            case (sourceEnumeration.Encumbrance):
                // Recurring Voucher
                if (self.ReferenceNumber().substring(0, 2).toUpperCase() == "RV"){
                    link = true;
                }
                // Purchase Order
                else if (self.ReferenceNumber().substring(0, 1).toUpperCase() == "P") {
                    if (self.DocumentId() != null) {
                        link = true;
                    }
                }
                // Blanket Purchase Order
                else if (self.ReferenceNumber().substring(0, 1).toUpperCase() == "B") {
                    if (self.DocumentId() != null) {
                        link = true;
                    }
                }
                break;
        }
        return link;
    }, self);

    // Calculate the link using the ID of the document associated to the transaction.
    self.documentLink = ko.computed(function () {
        var url = "";

        switch (self.Source()){
            // Voucher
            case sourceEnumeration.Voucher:
                url = Ellucian.ColleagueFinance.getVoucherUrl + '/' + self.ReferenceNumber();
                break;

            // Journal Entry
            case sourceEnumeration.JournalEntry:
                url = Ellucian.ColleagueFinance.getJournalEntryUrl + '/' + self.ReferenceNumber();
                break;

            // Requisition
            case sourceEnumeration.Requisition:
                url = Ellucian.ColleagueFinance.getRequisitionUrl + '/' + self.DocumentId();
                break;

            // Encumbrances
            case (sourceEnumeration.Encumbrance):
                // Recurring Voucher
                if (self.ReferenceNumber().substring(0, 2).toUpperCase() == "RV") {
                    url = Ellucian.ColleagueFinance.getRecurringVoucherUrl + '/' + self.ReferenceNumber();
                }
                // Purchase Order
                else if (self.ReferenceNumber().substring(0, 1).toUpperCase() == "P") {
                    url = Ellucian.ColleagueFinance.getPurchaseOrderUrl + '/' + self.DocumentId();
                }
                // Blanket Purchase Order
                else if (self.ReferenceNumber().substring(0, 1).toUpperCase() == "B") {
                    url = Ellucian.ColleagueFinance.getBlanketPurchaseOrderUrl + '/' + self.DocumentId();
                }
                break;
            default:
                url = '#';
                break;
        }
        return url;
    }, self);
};