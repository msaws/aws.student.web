﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

// Javascript representation of a cost center subtotal
function costCenterSubtotalModel(data) {
    var self = this;

    ko.mapping.fromJS(data, costCenterMapping, self);

    // "Inherit" the base cost center model to make use of common computed observables.
    baseCostCenterModel.call(self);
};