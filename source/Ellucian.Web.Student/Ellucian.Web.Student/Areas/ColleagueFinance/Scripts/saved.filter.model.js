﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.


function savedFilterModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects
    ko.mapping.fromJS(data, costCenterMapping, self);

}
