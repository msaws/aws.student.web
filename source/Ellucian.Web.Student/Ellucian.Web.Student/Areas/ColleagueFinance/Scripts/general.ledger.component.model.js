﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

//Select which storage mechanism to use depending on browser.
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();

function generalLedgerComponentModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects
    ko.mapping.fromJS(data, costCenterMapping, self);

    // Observable for tracking changes to the filter (to be used by saved filters; allows changes to be discarded)
    self.resetFilterCriteria = ko.observableArray([]);

    // The array of entered criteria
    self.tokenizedFilterCriteria = ko.observableArray([]);

    // The array of currently applied filter criteria
    self.appliedFilterCriteria = ko.observableArray([]).extend({ deferred: true });

    // Wrapper method for the validateLength function
    self.validateFilterCriteria = function (criteria, errorMessages) {
        var validationResults = self.validateLength(criteria, errorMessages);
        validationResults = self.validateRangeOrder(criteria, errorMessages) && validationResults;
        return validationResults;
    };

    // Used to verify if a criteria is ready to be added (so that the apply filter button can be enabled)
    self.workingCriteria = ko.observable("");

    // Ensure that each part of the user-entered value is the correct length and
    // add validation messages to the "errorMessages" observable if necessary.
    // Returns true/false
    self.validateLength = function (criteria, errorMessages) {
        var validates = true;
        var subCriteria = criteria.replace(/\s+/gm, '').replace(/^,+|,+$/gm, '').split(",");
        for (var s = 0; s < subCriteria.length; s++) {
            var ranges = subCriteria[s].split("-");
            for (var r = 0; r < ranges.length; r++) {
                if (ranges[r].length > self.ComponentLength()) {
                    // Value is too long
                    if (ko.unwrap(errorMessages).indexOf(majorComponentLengthValidationErrorMessage) === -1) {
                        errorMessages.push(majorComponentLengthValidationErrorMessage);
                        validates = false;
                    }
                }
                else if (ranges[r].length < self.ComponentLength()) {
                    // Value is too short
                    if (ko.unwrap(errorMessages).indexOf(majorComponentLengthShortValidationErrorMessage) === -1) {
                        errorMessages.push(majorComponentLengthShortValidationErrorMessage);
                        validates = false;
                    }
                }
            }
        }
        return validates;
    };

    self.validateRangeOrder = function (criteria, errorMessages) {
        var validates = true;
        var subCriteria = criteria.replace(/\s+/gm, '').replace(/^,+|,+$/gm, '').split(",");
        for (var s = 0; s < subCriteria.length; s++) {
            if (subCriteria[s].indexOf("-") !== -1) {
                var ranges = subCriteria[s].split("-");
                if (ranges.length !== 2) {
                    if (ko.unwrap(errorMessages).indexOf(filterInvalidRangeMessage) === -1) {
                        errorMessages.push(filterInvalidRangeMessage);
                        validates = false;
                    }
                }
                else if (ranges[0] > ranges[1]) {
                    if (ko.unwrap(errorMessages).indexOf(filterRangeOrderErrorMessage) === -1) {
                        errorMessages.push(filterRangeOrderErrorMessage);
                        validates = false;
                    }
                }
            }
        }
        return validates;
    };
};