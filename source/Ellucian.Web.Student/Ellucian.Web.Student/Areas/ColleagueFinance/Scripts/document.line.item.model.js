﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.

// Javascript representation of a document line item
function documentLineItemModel(data) {
    var self = this;

    // Initialize these two values since they are not defined in the line item view model
    self.Debit = ko.observable(0);
    self.Credit = ko.observable(0);

    // Perform any deeper mapping of child objects
    ko.mapping.fromJS(data, documentMapping, self);

    self.itemAmountColor = ko.computed(function () {
        var color = "";
            if (self.Credit() > 0) {
                color = "itemAmount-red";
            }
        return color;
    }, self);

    self.itemAmount = ko.computed(function () {
        if (self.Debit() > 0) return self.DebitDisplay();
        else if (self.Credit() > 0) return self.CreditDisplay();
        else return "0";
    });
};