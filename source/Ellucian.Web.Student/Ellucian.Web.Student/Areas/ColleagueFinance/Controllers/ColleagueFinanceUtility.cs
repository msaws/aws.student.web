﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.using System;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Dtos.Base;
using Newtonsoft.Json;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Controllers
{
    public static class ColleagueFinanceUtility
    {
        public static async Task<bool> SetUIPreference(ColleagueApiClient serviceClient, bool preferredUi, string personId, bool isProjectsAccounting = false)
        {
            var dict = new Dictionary<string, dynamic>();
            dict.Add("bar-graph", JsonConvert.SerializeObject(preferredUi));

            string preferenceType = "budget-graph-ui";
            if (isProjectsAccounting)
            {
                preferenceType = "pa-graph-ui";
            }

            var uiPref = await serviceClient.UpdateSelfservicePreferenceAsync(new SelfservicePreference()
            {
                PersonId = personId,
                PreferenceType = preferenceType,
                Id = "",
                Preferences = dict
            });

            return Convert.ToBoolean(uiPref.Preferences["bar-graph"]);
        }

        public static async Task<bool> SetCostCentersViewPreference(ColleagueApiClient serviceClient, bool preferredUi, string personId, bool isProjectsAccounting = false)
        {
            var dict = new Dictionary<string, dynamic>();
            dict.Add("cost-center-tab", JsonConvert.SerializeObject(preferredUi));

            string preferenceType = "cost-center-tab";

            var uiPref = await serviceClient.UpdateSelfservicePreferenceAsync(new SelfservicePreference()
            {
                PersonId = personId,
                PreferenceType = preferenceType,
                Id = "",
                Preferences = dict
            });

            return Convert.ToBoolean(uiPref.Preferences["cost-center-tab"]);
        }
    }
}