﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Newtonsoft.Json;
using slf4net;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class CostCentersController : BaseStudentController
    {
        private const string _SavedFilters = "SavedFilters";
        private const string _DefaultFilterPreferenceType = "bud-def-filter";
        /// <summary>
        /// Creates a new CostCentersController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public CostCentersController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        /// <summary>
        /// Returns the default Index view for Cost Centers, which shows the logged-in user's list of cost centers (if any).
        /// </summary>
        /// <returns>The general ledger user's list Index view.</returns>
        [LinkHelp]
        [PageAuthorize("myCostCenters")]
        public ActionResult Index()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Budget";
            ViewBag.CssAfterOverride = new List<string>();

            return View();
        }

        [LinkHelp]
        [PageAuthorize("costCenterDepartment")]
        public ActionResult CostCenter()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Cost Center";
            ViewBag.CssAfterOverride = new List<string>();

            return View();
        }

        [LinkHelp]
        [PageAuthorize("costCenterDepartment")]
        public ActionResult GlDetail(string id)
        {
            ViewBag.Title = "General Ledger Detail";

            return View("~/Areas/ColleagueFinance/Views/Home/GlDetail.cshtml");
        }


        /// <summary>
        /// Method to be invoked asynchronously to get cost centers filtered by the user.
        /// <param name="criteria">A list of CostCenterComponentQueryCriteria to be filtered on.</param>
        /// <param name="fiscalYear">The fiscal year specified by the user.</param>
        /// </summary>
        /// <returns>Filtered Cost Centers for the fiscal year in JSON format</returns>
        [HttpPost]
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetFilteredCostCentersAsync(IEnumerable<CostCenterComponentQueryCriteria> criteria, bool aawna, string fiscalYear)
        {
            try
            {
                // Get the fiscal year for today.
                var fiscalYearForTodayTask = ServiceClient.GetFiscalYearForTodayAsync();
                var glConfigurationTask = ServiceClientCache.GetCachedGeneralLedgerConfiguration(ServiceClient);
                var fiscalYearsTask = ServiceClientCache.GetCachedFiscalYears(ServiceClient);
                await Task.WhenAll(fiscalYearForTodayTask, glConfigurationTask, fiscalYearsTask);
                var fiscalYears = fiscalYearsTask.Result;
                var fiscalYearForToday = fiscalYearForTodayTask.Result;
                var glConfiguration = glConfigurationTask.Result;

                if (criteria == null)
                {
                    throw new ArgumentNullException("criteria", "criteria cannot be null.");
                }

                if (string.IsNullOrEmpty(fiscalYearForToday))
                {
                    throw new ApplicationException("Fiscal year for today cannot be null.");
                }

                if (glConfiguration == null || glConfiguration.MajorComponents == null)
                {
                    throw new ApplicationException("GL Configuration cannot be null.");
                }

                if (fiscalYears == null || !fiscalYears.Any())
                {
                    throw new ApplicationException("No fiscal years set up.");
                }

                if (string.IsNullOrEmpty(fiscalYear))
                {
                    if (fiscalYears.Any(f => f.Equals(fiscalYearForToday, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        fiscalYear = fiscalYearForToday;
                    }
                    else
                    {
                        fiscalYear = fiscalYears.FirstOrDefault() ?? "";
                    }
                }

                var queryCriteria = BuildCostCenterQueryCriteriaDto(fiscalYear, glConfiguration, criteria, aawna);
                var savedFilterNamesTask = GetSavedFilterNames();
                var costCentersTask = ServiceClient.QueryCostCentersAsync(queryCriteria);
                await Task.WhenAll(costCentersTask, savedFilterNamesTask);

                // Initialize the summary view model to hold all the data
                var costCenterSummaryViewModel = new CostCenterSummaryViewModel(costCentersTask.Result, fiscalYearsTask.Result, fiscalYearForToday);

                var majorComponentDtos = glConfiguration.MajorComponents;
                costCenterSummaryViewModel.SetGeneralLedgerMajorComponents(majorComponentDtos);

                // Order the cost centers in ascending order by the cost center unit id.
                costCenterSummaryViewModel.CostCenters = costCenterSummaryViewModel.CostCenters.OrderBy(x => x.UnitId).ToList();

                // Sort the fiscal years and store them in the view model.
                costCenterSummaryViewModel.FiscalYears = costCenterSummaryViewModel.FiscalYears.OrderByDescending(x => true).ToList();

                // Include the list of saved filter names.
                costCenterSummaryViewModel.SavedFilterNames = savedFilterNamesTask.Result.OrderBy(f => f.Value).ToList();

                return new JsonResult() { Data = costCenterSummaryViewModel, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (HttpRequestFailedException hrfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(hrfe.Message);
                Response.StatusCode = (int)hrfe.StatusCode;
                return Json("HttpRequestFailed");
            }
            catch (ResourceNotFoundException rnfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(rnfe.Message);
                Response.StatusCode = (int)rnfe.StatusCode;
                return Json("ResourceNotFound");
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                Logger.Error(e.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Exception");
            }
        }

        [HttpPost]
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetFilteredObjectDataAsync(IEnumerable<CostCenterComponentQueryCriteria> criteria, bool aawna, string fiscalYear)
        {
            try
            {
                // Get the fiscal year for today.
                var fiscalYearForTodayTask = ServiceClient.GetFiscalYearForTodayAsync();
                var glConfigurationTask = ServiceClientCache.GetCachedGeneralLedgerConfiguration(ServiceClient);
                var fiscalYearsTask = ServiceClientCache.GetCachedFiscalYears(ServiceClient);
                var savedFilterNamesTask = GetSavedFilterNames();
                await Task.WhenAll(fiscalYearForTodayTask, glConfigurationTask, fiscalYearsTask, savedFilterNamesTask);
                var fiscalYears = fiscalYearsTask.Result;
                var fiscalYearForToday = fiscalYearForTodayTask.Result;
                var glConfiguration = glConfigurationTask.Result;
                var savedFilterNames = savedFilterNamesTask.Result;

                if (criteria == null)
                {
                    throw new ArgumentNullException("criteria", "criteria cannot be null.");
                }

                if (string.IsNullOrEmpty(fiscalYearForToday))
                {
                    throw new ApplicationException("Fiscal year for today cannot be null.");
                }

                if (glConfiguration == null || glConfiguration.MajorComponents == null)
                {
                    throw new ApplicationException("GL Configuration cannot be null.");
                }
                var majorComponentsDto = glConfiguration.MajorComponents;

                if (fiscalYears == null || !fiscalYears.Any())
                {
                    throw new ApplicationException("No fiscal years set up.");
                }

                if (string.IsNullOrEmpty(fiscalYear))
                {
                    if (fiscalYears.Any(f => f.Equals(fiscalYearForToday, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        fiscalYear = fiscalYearForToday;
                    }
                    else
                    {
                        fiscalYear = fiscalYears.FirstOrDefault() ?? "";
                    }
                }

                var queryCriteria = BuildCostCenterQueryCriteriaDto(fiscalYear, glConfiguration, criteria, aawna);
                var objectCodes = await ServiceClient.QueryGlObjectCodesAsync(queryCriteria);

                var vm = new ObjectViewModel(objectCodes, fiscalYears, fiscalYearForToday, majorComponentsDto, savedFilterNames);


                return new JsonResult() { Data = vm, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (HttpRequestFailedException hrfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(hrfe.Message);
                Response.StatusCode = (int)hrfe.StatusCode;
                return Json("HttpRequestFailed");
            }
            catch (ResourceNotFoundException rnfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(rnfe.Message);
                Response.StatusCode = (int)rnfe.StatusCode;
                return Json("ResourceNotFound");
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                Logger.Error(e.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Exception");
            }
        }

        /// <summary>
        /// Retrieves the Current User's UI preference for the Budget/Cost Center Summary page
        /// </summary>
        /// <returns>boolean - true if bar graph view is chosen; false if list view is chosen</returns>
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetUIPreference()
        {
            var isBarGraph = true;

            try
            {
                isBarGraph = Convert.ToBoolean((await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, "budget-graph-ui")).Preferences["bar-graph"]);
            }
            catch (Exception)
            {
                // Unable to retrieve the UI preference (may not exist in DB yet) - instead of throwing an error, just use the default (true)
            }

            return new JsonResult() { Data = isBarGraph, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Retrieves whether to take the user to the cost center view or the object view.
        /// </summary>
        /// <returns>boolean - true for cost center view; false for object view. </returns>
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetCostCentersViewPreference()
        {
            var isCostCenterView = true;

            try
            {
                isCostCenterView = Convert.ToBoolean((await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, "cost-center-tab")).Preferences["cost-center-tab"]);
            }
            catch (Exception)
            {
                // Unable to retrieve the UI preference (may not exist in DB yet) - instead of throwing an error, just use the default (true)
            }

            return new JsonResult() { Data = isCostCenterView, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Stores the Current User's Budget/Cost Center Summary UI choice.
        /// </summary>
        /// <param name="preferredUi">boolean: true means bar graph view; false means list view</param>
        /// <returns>the stored preference</returns>
        [HttpPost]
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> SetUIPreference(bool preferredUi)
        {
            var isBarGraph = true;
            try
            {
                isBarGraph = await ColleagueFinanceUtility.SetUIPreference(ServiceClient, preferredUi, CurrentUser.PersonId);
            }
            catch (Exception e)
            {
                // Log the error and send back a generic HTTP status code
                Logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
            return new JsonResult() { Data = isBarGraph, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Stores whether the default will be the cost centers view or the object view.
        /// </summary>
        /// <param name="preferredCostCenterTab">boolean: true means cost center view; false means object view</param>
        /// <returns>The view where to take the user for Budget: cost-center vs object.</returns>
        [HttpPost]
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> SetCostCentersViewPreference(bool preferredCostCenterTab)
        {
            var isCostCenterView = true;
            try
            {
                isCostCenterView = await ColleagueFinanceUtility.SetCostCentersViewPreference(ServiceClient, preferredCostCenterTab, CurrentUser.PersonId);
            }
            catch (Exception e)
            {
                // Log the error and send back a generic HTTP status code
                Logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
            return new JsonResult() { Data = isCostCenterView, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Method to be invoked asynchronously to get Major Components from GL Configuration.
        /// </summary>
        /// <returns>List of General Ledger Component View Model objects in JSON format</returns>
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetMajorComponentsAsync()
        {
            var glConfiguration = await ServiceClient.GetGeneralLedgerConfigurationAsync();

            if (glConfiguration == null || glConfiguration.MajorComponents == null)
            {
                throw new ApplicationException("GL Configuration cannot be null.");
            }

            List<GeneralLedgerComponentViewModel> MajorComponents = new List<GeneralLedgerComponentViewModel>();
            // Add the supplied components to the list.
            foreach (var majorComponentDto in glConfiguration.MajorComponents)
            {
                if (majorComponentDto != null)
                {
                    MajorComponents.Add(new GeneralLedgerComponentViewModel(majorComponentDto));
                }
            }

            return new JsonResult() { Data = MajorComponents, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Gets the saved filter criteria for the given filter name.
        /// </summary>
        /// <param name="filterName">Name of the filter whose criteria is being returned.</param>
        /// <returns>A Saved Filter object in JSON format</returns>
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetSavedFilterCriteriaAsync(string filterId)
        {
            return new JsonResult() { Data = await GetSavedFilterCriteria(filterId), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Add or update a user-defined saved filter
        /// </summary>
        /// <param name="criteria">The filter criteria</param>
        /// <param name="name">The name of the saved filter</param>
        /// <param name="create">Flag to indicate if this is a new filter</param>
        /// <returns>The (potentially) updated list of filter names</returns>
        [HttpPost]
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> AddOrUpdateSavedFilterAsync(List<CostCenterComponentQueryCriteria> criteria, bool aawna, string name, string id, bool create)
        {
            SavedFilter newSavedFilter = new SavedFilter()
            {
                ComponentCriteria = criteria,
                IncludeActiveAccountsWithNoActivity = aawna,
                Name = name,
                Id = id
            };

            var existingFilterNames = await GetSavedFilterNames();
            var nextAvailableKey = existingFilterNames.Select(f => f.Key).LastOrDefault();
            if (nextAvailableKey == null)
            {
                nextAvailableKey = "1";
            }
            else
            {
                nextAvailableKey = (Convert.ToInt32(nextAvailableKey) + 1).ToString();
            }
            List<KeyValuePair<string, string>> updatedFilterNames = existingFilterNames;
            if (create || !existingFilterNames.Any(f => f.Value == name))
            {
                newSavedFilter.Id = nextAvailableKey;
                newSavedFilter.Name = await AddSavedFilterName(name, nextAvailableKey);
                updatedFilterNames = await GetSavedFilterNames();
            }

            await AddOrUpdateSavedFilterCriteriaAsync(newSavedFilter);

            return new JsonResult() { Data = updatedFilterNames.OrderBy(f => f.Value).ToList(), MaxJsonLength = Int32.MaxValue };
        }

        /// <summary>
        /// Debugging action to clear the saved filters.
        /// </summary>
        /// <returns>JsonResult containing an array of the saved filters</returns>
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> DeleteSavedFilterAsync(string filterId)
        {
            if (string.IsNullOrEmpty(filterId))
            {
                filterId = "";
            }
            try
            {
                var defaultFilter = await GetDefaultFilter();
                // If the filterId that is being deleted is also the default, delete the default record as well...
                if (defaultFilter.Key == filterId)
                {
                    var filter = await ServiceClient.DeleteSelfServicePreferenceAsync(CurrentUser.PersonId, _DefaultFilterPreferenceType);
                }
            }
            catch (Exception)
            {
                Logger.Info("Could not delete default filter for user: " + CurrentUser.PersonId);
            }
            try
            {
                await DeleteSavedFilterCriteria(filterId);
            }
            catch (Exception)
            {
                Logger.Info("Could not delete all parts of the saved filter: \"" + filterId + "\" for user: " + CurrentUser.PersonId);
            }
            try
            {
                await DeleteSavedFilterName(filterId);
                var filterNames = await GetSavedFilterNames();
                return new JsonResult() { Data = filterNames.OrderBy(f => f.Value).ToList(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            catch (Exception)
            {
                Logger.Info("Could not delete the name of the saved filter: \"" + filterId + "\" for user: " + CurrentUser.PersonId);
                throw;
            }
        }

        /// <summary>
        /// Get the user's default filter preference from the API
        /// </summary>
        /// <returns>JsonResult containing a key-value pair that represents a filter</returns>
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetDefaultFilterAsync()
        {
            try
            {
                var defaultFilter = await GetDefaultFilter();
                return new JsonResult() { Data = defaultFilter, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Set the user's default filter preference
        /// </summary>
        /// <param name="filterId">String containing the filter ID to save</param>
        /// <returns>JsonResult containing a key-value pair that represents the filter that was saved</returns>
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> SetDefaultFilterAsync(string filterKey, string filterValue)
        {
            try
            {
                if (filterKey == null || filterKey == string.Empty)
                {
                    // Delete the default filter preference
                    var filter = await ServiceClient.DeleteSelfServicePreferenceAsync(CurrentUser.PersonId, _DefaultFilterPreferenceType);
                    return new JsonResult() { Data = new KeyValuePair<string, string>(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                {
                    // Update the default filter preference
                    var dict = new Dictionary<string, dynamic>();
                    dict.Add(_DefaultFilterPreferenceType, JsonConvert.SerializeObject(new KeyValuePair<string, string>(filterKey, filterValue)));

                    var filter = await ServiceClient.UpdateSelfservicePreferenceAsync(new SelfservicePreference()
                    {
                        PersonId = CurrentUser.PersonId,
                        PreferenceType = _DefaultFilterPreferenceType,
                        Id = "",
                        Preferences = dict
                    });
                    var pref = filter.Preferences[_DefaultFilterPreferenceType];
                    var defaultFilter = JsonConvert.DeserializeObject<KeyValuePair<string, string>>(pref);
                    return new JsonResult() { Data = defaultFilter, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Unable to set the default filter preference for user \"" + CurrentUser.PersonId + "\" to \"" + filterKey + "\"");
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }

        #region Private Saved Filter Helper Methods

        /// <summary>
        /// Helper method to get the user-defined saved filter names for the current user.
        /// </summary>
        /// <returns>List of filter names</returns>
        private async Task<List<KeyValuePair<string, string>>> GetSavedFilterNames()
        {
            try
            {
                var filters = await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, "budget-filters");
                var temp = filters.Preferences[_SavedFilters];
                return JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(temp);
            }
            catch (Exception)
            {
                return new List<KeyValuePair<string, string>>();
            }
        }

        /// <summary>
        /// Helper method to get the saved filter criteria for the given filter name.
        /// </summary>
        /// <param name="filterName">Name of the filter whose criteria is being returned.</param>
        /// <returns>A Saved Filter object</returns>
        private async Task<SavedFilter> GetSavedFilterCriteria(string filterId)
        {
            try
            {
                var filters = await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, "budget-filters-" + filterId);
                var temp = filters.Preferences[_SavedFilters];
                return JsonConvert.DeserializeObject<SavedFilter>(temp);
            }
            catch (Exception)
            {
                return new SavedFilter();
            }
        }

        private async Task DeleteSavedFilterName(string filterId)
        {
            try
            {
                var filterNames = await GetSavedFilterNames();
                filterNames = filterNames.Where(f => !f.Key.Equals(filterId, StringComparison.CurrentCulture)).ToList();
                if (filterNames.Any())
                {
                    await UpdateAllSavedFilterNames(filterNames);
                }
                else
                {
                    await ServiceClient.DeleteSelfServicePreferenceAsync(CurrentUser.PersonId, "budget-filters");
                }
            }
            catch (Exception)
            {
                Logger.Error("Could not delete saved filter name: " + filterId);
                throw;
            }
        }

        private async Task DeleteSavedFilterCriteria(string filterId)
        {
            try
            {
                await ServiceClient.DeleteSelfServicePreferenceAsync(CurrentUser.PersonId, "budget-filters-" + filterId);
            }
            catch (Exception)
            {
                Logger.Error("Could not delete saved filter criteria: " + filterId);
                throw;
            }
        }

        /// <summary>
        /// Helper method to add or update a saved filter's criteria.
        /// </summary>
        /// <returns>New or updated filter.</returns>
        private async Task<SavedFilter> AddOrUpdateSavedFilterCriteriaAsync(SavedFilter savedFilter)
        {
            try
            {
                var dict = new Dictionary<string, dynamic>();
                dict.Add(_SavedFilters, JsonConvert.SerializeObject(savedFilter));

                var filters = await ServiceClient.UpdateSelfservicePreferenceAsync(new SelfservicePreference()
                {
                    PersonId = CurrentUser.PersonId,
                    PreferenceType = "budget-filters-" + savedFilter.Id,
                    Id = "",
                    Preferences = dict
                });
                var temp = filters.Preferences[_SavedFilters];
                return JsonConvert.DeserializeObject<SavedFilter>(temp);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Filter \"" + savedFilter.Name + "\" failed to save for person: " + CurrentUser.PersonId);
                throw;
            }

        }

        /// <summary>
        /// Helper method to verify and add a name to the list of user-defined saved filter names.
        /// </summary>
        /// <param name="name">The name to be added</param>
        /// <returns>The updated list of user-defined saved filter names</returns>
        private async Task<string> AddSavedFilterName(string name, string id)
        {
            var existingFilterNames = await GetSavedFilterNames();
            var nextAvailableKey = existingFilterNames.Select(f => f.Key).LastOrDefault();
            if (nextAvailableKey == null)
            {
                nextAvailableKey = "1";
            }
            else
            {
                nextAvailableKey = (Convert.ToInt32(nextAvailableKey) + 1).ToString();
            }
            if (existingFilterNames.Any(f => f.Value.Equals(name, StringComparison.CurrentCultureIgnoreCase)))
            {
                var i = 0;
                do
                {
                    i++;
                }
                while (existingFilterNames.Any(f => f.Value.Equals(name + " (" + i + ")", StringComparison.CurrentCultureIgnoreCase)) && i < 1000);
                if (i >= 1000)
                {
                    throw new IndexOutOfRangeException("Too many filters with the same name.");
                }

                name += " (" + i + ")";
            }
            existingFilterNames.Add(new KeyValuePair<string, string>(nextAvailableKey, name));
            await UpdateAllSavedFilterNames(existingFilterNames);

            return name;
        }

        /// <summary>
        /// Internal method to update the list of saved filter names.
        /// </summary>
        /// <returns>Updated list of filter names.</returns>
        private async Task<List<KeyValuePair<string, string>>> UpdateAllSavedFilterNames(List<KeyValuePair<string, string>> savedFilterNames)
        {
            var dict = new Dictionary<string, dynamic>();
            dict.Add(_SavedFilters, JsonConvert.SerializeObject(savedFilterNames));

            var filters = await ServiceClient.UpdateSelfservicePreferenceAsync(new SelfservicePreference()
            {
                PersonId = CurrentUser.PersonId,
                PreferenceType = "budget-filters",
                Id = "",
                Preferences = dict
            });
            var temp = filters.Preferences[_SavedFilters];
            return JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(temp);
        }

        private async Task<KeyValuePair<string, string>> GetDefaultFilter()
        {
            try
            {
                var filter = await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, _DefaultFilterPreferenceType);
                var pref = filter.Preferences[_DefaultFilterPreferenceType];
                var defaultFilter = JsonConvert.DeserializeObject<KeyValuePair<string, string>>(pref);
                return defaultFilter;
            }
            catch (ResourceNotFoundException)
            {
                return new KeyValuePair<string, string>();
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                return new KeyValuePair<string, string>();
            }
        }

        private CostCenterQueryCriteria BuildCostCenterQueryCriteriaDto(string fiscalYear,
            GeneralLedgerConfiguration glConfiguration,
            IEnumerable<CostCenterComponentQueryCriteria> criteria,
            bool aawna,
            string id = null)
        {
            var queryCriteria = new CostCenterQueryCriteria();
            queryCriteria.ComponentCriteria = new List<CostCenterComponentQueryCriteria>();
            queryCriteria.IncludeActiveAccountsWithNoActivity = aawna;
            queryCriteria.FiscalYear = fiscalYear;
            queryCriteria.Ids = new List<string>();

            if (!string.IsNullOrEmpty(id))
            {
                queryCriteria.Ids.Add(id);
            }

            foreach (var component in criteria)
            {
                var com = glConfiguration.MajorComponents.FirstOrDefault(x => x.ComponentName.Equals(component.ComponentName, StringComparison.CurrentCultureIgnoreCase));
                if (component.IndividualComponentValues != null)
                {
                    foreach (var individual in component.IndividualComponentValues)
                    {
                        if (individual.Length != com.ComponentLength)
                        {
                            throw new ArgumentOutOfRangeException(component.ComponentName, "Incorrect length");
                        }
                    }
                }
                else
                {
                    component.IndividualComponentValues = new List<string>();
                }

                if (component.RangeComponentValues != null)
                {
                    foreach (var range in component.RangeComponentValues)
                    {
                        if (range.StartValue.Length != com.ComponentLength || range.EndValue.Length != com.ComponentLength)
                        {
                            throw new ArgumentOutOfRangeException(component.ComponentName, "Incorrect length");
                        }
                        else if (string.Compare(range.StartValue, range.EndValue) > 0)
                        {
                            throw new ArgumentOutOfRangeException(component.ComponentName, "Range is out of order");
                        }
                    }
                }
                else
                {
                    component.RangeComponentValues = new List<CostCenterComponentRangeQueryCriteria>();
                }

                if (component.RangeComponentValues.Count > 0 || component.IndividualComponentValues.Count > 0)
                {
                    component.ComponentName = component.ComponentName.ToUpper();
                    queryCriteria.ComponentCriteria.Add(component);
                }
            }

            return queryCriteria;
        }

        #endregion

        /// <summary>
        /// Method to be invoked asynchronously to get cost center and apply the filter selected by the user.
        /// <param name="criteria">A list of CostCenterComponentQueryCriteria to be filtered on.</param>
        /// <param name="fiscalYear">The fiscal year specified by the user.</param>
        /// </summary>
        /// <returns>Filtered Cost Centers for the fiscal year in JSON format</returns>
        [HttpPost]
        [PageAuthorize("myCostCenters")]
        public async Task<JsonResult> GetFilteredCostCenterAsync(string id, string fiscalYear, IEnumerable<CostCenterComponentQueryCriteria> criteria, bool aawna)
        {
            try
            {
                // Get the fiscal year for today.
                var fiscalYearForTodayTask = ServiceClient.GetFiscalYearForTodayAsync();
                var glConfigurationTask = ServiceClientCache.GetCachedGeneralLedgerConfiguration(ServiceClient);
                var fiscalYearsTask = ServiceClientCache.GetCachedFiscalYears(ServiceClient);
                await Task.WhenAll(fiscalYearForTodayTask, glConfigurationTask, fiscalYearsTask);
                var fiscalYears = fiscalYearsTask.Result;
                var fiscalYearForToday = fiscalYearForTodayTask.Result;
                var glConfiguration = glConfigurationTask.Result;

                if (string.IsNullOrEmpty(fiscalYearForToday))
                {
                    throw new ApplicationException("Fiscal year for today cannot be null.");
                }

                if (glConfiguration == null || glConfiguration.MajorComponents == null)
                {
                    throw new ApplicationException("GL Configuration cannot be null.");
                }

                if (fiscalYears == null || !fiscalYears.Any())
                {
                    throw new ApplicationException("No fiscal years set up.");
                }

                if (string.IsNullOrEmpty(fiscalYear))
                {
                    if (fiscalYears.Any(f => f.Equals(fiscalYearForToday, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        fiscalYear = fiscalYearForToday;
                    }
                    else
                    {
                        fiscalYear = fiscalYears.FirstOrDefault() ?? "";
                    }
                }

                var queryCriteria = BuildCostCenterQueryCriteriaDto(fiscalYear, glConfiguration, criteria, aawna, id);
                var costCenter = (await ServiceClient.QueryCostCentersAsync(queryCriteria)).FirstOrDefault();

                // Initialize the view model
                var costCenterDetailViewModel = new CostCenterDetailViewModel(costCenter, fiscalYearsTask.Result, fiscalYearForToday);

                // Sort the subtotals by name
                costCenterDetailViewModel.CostCenter.CostCenterExpenseSubtotals = costCenterDetailViewModel.CostCenter.CostCenterExpenseSubtotals.OrderBy(x => x.Name).ToList();

                // Sort the fiscal years and save them in the view model.
                costCenterDetailViewModel.FiscalYears = costCenterDetailViewModel.FiscalYears.OrderByDescending(x => true).ToList();

                var majorComponentDtos = glConfiguration.MajorComponents;
                costCenterDetailViewModel.SetGeneralLedgerMajorComponents(majorComponentDtos);

                return new JsonResult() { Data = costCenterDetailViewModel, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (HttpRequestFailedException hrfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(hrfe.Message);
                throw new HttpException((int)hrfe.StatusCode, hrfe.Message);
            }
            catch (ResourceNotFoundException rnfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(rnfe.Message);
                throw new HttpException((int)rnfe.StatusCode, rnfe.Message);
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                Logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Get GL Account info - including transactions (for GL Detail)
        /// </summary>
        /// <param name="glAccount">The desired GL Account number</param>
        /// <param name="fiscalYear">The desired fiscal year</param>
        /// <returns>A JSON-ified CostCenterGlAccountViewModel object</returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        public async Task<JsonResult> GetGlTransactionsAsync(string glAccount, string fiscalYear)
        {
            try
            {
                var fiscalYearForTodayTask = ServiceClient.GetFiscalYearForTodayAsync();
                var fiscalYearsTask = ServiceClient.GetFiscalYearsAsync();
                await Task.WhenAll(fiscalYearForTodayTask, fiscalYearsTask);
                var fiscalYears = fiscalYearsTask.Result;
                var fiscalYearForToday = fiscalYearForTodayTask.Result;

                if (string.IsNullOrEmpty(fiscalYearForToday))
                {
                    throw new ApplicationException("Fiscal year for today cannot be null.");
                }

                // If the supplied fiscal year is null then use the fiscal year for today's date.
                if (string.IsNullOrEmpty(fiscalYear))
                {
                    if (fiscalYears.Any(f => f.Equals(fiscalYearForToday, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        fiscalYear = fiscalYearForToday;
                    }
                    else
                    {
                        fiscalYear = fiscalYears.FirstOrDefault() ?? "";
                    }
                }

                // Set up the criteria object for the API query.
                GlActivityDetailQueryCriteria criteria = new GlActivityDetailQueryCriteria()
                {
                    Ids = new List<string>(),
                    GlAccount = glAccount,
                    FiscalYear = fiscalYear
                };
                var glAccountDtoTask = ServiceClient.QueryGeneralLedgerActivityDetailsAsync(criteria);
                await Task.WhenAll(glAccountDtoTask);

                // Build the view model from the glAccountDto and the fiscalYears.
                var glActivityDetailVM = new GlActivityDetailViewModel(glAccountDtoTask.Result, fiscalYears, fiscalYearForToday);

                // Sort the transactions in each section by transaction descending date and then by descending document number.
                glActivityDetailVM.GlAccountActivityDetail.EncumbranceTransactions = glActivityDetailVM.GlAccountActivityDetail.EncumbranceTransactions.OrderByDescending(x => DateTime.Parse(x.Date)).ThenByDescending(x => x.ReferenceNumber).ToList();
                glActivityDetailVM.GlAccountActivityDetail.ActualTransactions = glActivityDetailVM.GlAccountActivityDetail.ActualTransactions.OrderByDescending(x => DateTime.Parse(x.Date)).ThenByDescending(x => x.ReferenceNumber).ToList();
                glActivityDetailVM.GlAccountActivityDetail.BudgetTransactions = glActivityDetailVM.GlAccountActivityDetail.BudgetTransactions.OrderByDescending(x => DateTime.Parse(x.Date)).ThenByDescending(x => x.ReferenceNumber).ToList();

                return Json(glActivityDetailVM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                Logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }

        }

        #region Document actions
        /// <summary>
        /// Action method for the Voucher View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceVoucher")]
        public ActionResult Voucher()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Voucher";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/Voucher.cshtml");
        }

        /// <summary>
        /// Action method for the Recurring Voucher View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceRecurringVoucher")]
        public ActionResult RecurringVoucher()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Recurring Voucher";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/RecurringVoucher.cshtml");
        }

        /// <summary>
        /// Action method for the Purchase Order View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinancePurchaseOrder")]
        public ActionResult PurchaseOrder()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Purchase Order";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/PurchaseOrder.cshtml");
        }

        /// <summary>
        /// Action method for the Blanket Purchase Order View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceBlanketPurchaseOrder")]
        public ActionResult BlanketPurchaseOrder()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Blanket Purchase Order";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/BlanketPurchaseOrder.cshtml");
        }

        /// <summary>
        /// Action method for the Requisition View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceRequisition")]
        public ActionResult Requisition()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Requisition";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/Requisition.cshtml");
        }

        /// <summary>
        /// Action method for the Journal Entry View page of Colleague Finance
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceJournalEntry")]
        public ActionResult JournalEntry()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Journal Entry";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/JournalEntry.cshtml");
        }
        #endregion

        #region Document async methods
        /// <summary>
        /// Asynchronously get a Voucher object.
        /// </summary>
        /// <param name="voucherId">Voucher ID</param>
        /// <returns>Voucher in JSON format</returns>
        public async Task<JsonResult> GetVoucherAsync(string voucherId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetVoucherAsync(voucherId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Recurring Voucher object.
        /// </summary>
        /// <param name="recurringVoucherId">Recurring Voucher ID</param>
        /// <returns>Recurring Voucher in JSON format</returns>
        public async Task<JsonResult> GetRecurringVoucherAsync(string recurringVoucherId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetRecurringVoucherAsync(recurringVoucherId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a PurchaseOrder object.
        /// </summary>
        /// <param name="purchaseOrderId">PurchaseOrder ID</param>
        /// <returns>Purchase Order in JSON format</returns>
        public async Task<JsonResult> GetPurchaseOrderAsync(string purchaseOrderId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetPurchaseOrderAsync(purchaseOrderId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a BlanketPurchaseOrder object.
        /// </summary>
        /// <param name="blanketPurchaseOrderId">BlanketPurchaseOrder ID</param>
        /// <returns>BlanketPurchase Order in JSON format</returns>
        public async Task<JsonResult> GetBlanketPurchaseOrderAsync(string blanketPurchaseOrderId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetBlanketPurchaseOrderAsync(blanketPurchaseOrderId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Requisition object.
        /// </summary>
        /// <param name="purchaseOrderId">Requisition ID</param>
        /// <returns>Requisition in JSON format</returns>
        public async Task<JsonResult> GetRequisitionAsync(string requisitionId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetRequisitionAsync(requisitionId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Journal Entry object
        /// </summary>
        /// <param name="journalEntryId">Journal Entry ID</param>
        /// <returns>Journal Entry in JSON format</returns>
        public async Task<JsonResult> GetJournalEntryAsync(string journalEntryId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetJournalEntryAsync(journalEntryId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
