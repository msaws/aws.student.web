﻿using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class BudgetController : BaseStudentController
    {
        /// <summary>
        /// Creates a new BudgetController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public BudgetController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        [LinkHelp]
        [PageAuthorize("costCenterDepartment")]
        public ActionResult GlDetail(string id)
        {
            ViewBag.Title = "General Ledger Detail";

            return View("~/Areas/ColleagueFinance/Views/Home/GlDetail.cshtml");
        }

        #region Document actions

        /// <summary>
        /// Action method for the Voucher View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceVoucher")]
        public ActionResult Voucher()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Voucher";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/Voucher.cshtml");
        }

        /// <summary>
        /// Action method for the Recurring Voucher View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceRecurringVoucher")]
        public ActionResult RecurringVoucher()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Recurring Voucher";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/RecurringVoucher.cshtml");
        }

        /// <summary>
        /// Action method for the Purchase Order View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinancePurchaseOrder")]
        public ActionResult PurchaseOrder()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Purchase Order";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/PurchaseOrder.cshtml");
        }

        /// <summary>
        /// Action method for the Blanket Purchase Order View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceBlanketPurchaseOrder")]
        public ActionResult BlanketPurchaseOrder()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Blanket Purchase Order";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/BlanketPurchaseOrder.cshtml");
        }

        /// <summary>
        /// Action method for the Requisition View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceRequisition")]
        public ActionResult Requisition()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Requisition";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/Requisition.cshtml");
        }

        /// <summary>
        /// Action method for the Journal Entry View page of Colleague Finance
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceJournalEntry")]
        public ActionResult JournalEntry()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Journal Entry";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/JournalEntry.cshtml");
        }

        #endregion

        #region Document async methods

        /// <summary>
        /// Asynchronously get a Voucher object.
        /// </summary>
        /// <param name="voucherId">Voucher ID</param>
        /// <returns>Voucher in JSON format</returns>
        public async Task<JsonResult> GetVoucherAsync(string voucherId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetVoucherAsync(voucherId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Recurring Voucher object.
        /// </summary>
        /// <param name="recurringVoucherId">Recurring Voucher ID</param>
        /// <returns>Recurring Voucher in JSON format</returns>
        public async Task<JsonResult> GetRecurringVoucherAsync(string recurringVoucherId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetRecurringVoucherAsync(recurringVoucherId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a PurchaseOrder object.
        /// </summary>
        /// <param name="purchaseOrderId">PurchaseOrder ID</param>
        /// <returns>Purchase Order in JSON format</returns>
        public async Task<JsonResult> GetPurchaseOrderAsync(string purchaseOrderId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetPurchaseOrderAsync(purchaseOrderId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a BlanketPurchaseOrder object.
        /// </summary>
        /// <param name="blanketPurchaseOrderId">BlanketPurchaseOrder ID</param>
        /// <returns>BlanketPurchase Order in JSON format</returns>
        public async Task<JsonResult> GetBlanketPurchaseOrderAsync(string blanketPurchaseOrderId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetBlanketPurchaseOrderAsync(blanketPurchaseOrderId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Requisition object.
        /// </summary>
        /// <param name="purchaseOrderId">Requisition ID</param>
        /// <returns>Requisition in JSON format</returns>
        public async Task<JsonResult> GetRequisitionAsync(string requisitionId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetRequisitionAsync(requisitionId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Journal Entry object
        /// </summary>
        /// <param name="journalEntryId">Journal Entry ID</param>
        /// <returns>Journal Entry in JSON format</returns>
        public async Task<JsonResult> GetJournalEntryAsync(string journalEntryId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetJournalEntryAsync(journalEntryId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}