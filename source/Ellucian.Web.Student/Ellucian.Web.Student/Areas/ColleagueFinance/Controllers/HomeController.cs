﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using slf4net;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class HomeController : BaseStudentController
    {
        /// <summary>
        /// Creates a new HomeController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public HomeController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        /// <summary>
        /// Action method for the Home page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("financialManagementHome")]
        public ActionResult Index()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Financial Management Overview";
            ViewBag.CssAfterOverride = new List<string>();
            ViewBag.CssAfterOverride.Add("~/Areas/ColleagueFinance/Content/ColleagueFinance.css");

            return View();
        }
    }
}
