﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
namespace Ellucian.Web.Student.Areas.ColleagueFinance
{
    public static class ColleagueFinanceResourceFiles
    {
        public static readonly string ColleagueFinanceHomeResources = "ColleagueFinanceHome";
        public static readonly string ProcurementDocumentsResources = "ProcurementDocuments";
        public static readonly string GeneralLedgerDocumentsResources = "GeneralLedgerDocuments";
    }
}