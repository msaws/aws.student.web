﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System.ComponentModel;
using System.Web.Mvc;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Student.Licensing;

namespace Ellucian.Web.Student.Areas.ColleagueFinance
{
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    public class ColleagueFinanceAreaRegistration : LicenseAreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ColleagueFinance";
            }
        }

        public override void LicensedRegistration(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ColleagueFinance_default",
                "ColleagueFinance/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional, controller = "Home" },
                new string[] { "Ellucian.Web.Student.Areas.ColleagueFinance.Controllers" }
            );
            context.MapRoute(
                name: "DefaultCostCenterGlAccount",
                url: "ColleagueFinance/CostCenters/{action}/{id}/{generalLedgerAccountId}",
                defaults: new { action = "Index", id = string.Empty, generalLedgerAccountId = string.Empty, controller = "CostCenters" },
                namespaces: new string[] { "Ellucian.Web.Student.Areas.ColleagueFinance.Controllers" }
            );
            context.MapRoute(
                name: "DefaultBudgetGlAccount",
                url: "ColleagueFinance/Budget/{action}/{id}/{generalLedgerAccountId}",
                defaults: new { action = "Index", id = string.Empty, generalLedgerAccountId = string.Empty, controller = "Budget" },
                namespaces: new string[] { "Ellucian.Web.Student.Areas.ColleagueFinance.Controllers" }
            );
        }
    }
}
