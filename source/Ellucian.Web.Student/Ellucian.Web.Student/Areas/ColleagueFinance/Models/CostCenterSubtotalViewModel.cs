﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using System.Linq;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a cost center subtotal.
    /// </summary>
    public class CostCenterSubtotalViewModel : BaseCostCenterTotalsViewModel
    {
        /// <summary>
        /// The cost center ID.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The cost center name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// List of pooled and non-pooled general ledger accounts
        /// </summary>
        public List<CostCenterAccountSequenceViewModel> SortedGlAccounts { get; set; }
        
        /// <summary>
        /// Initialize the subtotal view model.
        /// </summary>
        /// <param name="costCenterSubtotalDto">Cost center subtotal DTO.</param>
        public CostCenterSubtotalViewModel(CostCenterSubtotal costCenterSubtotalDto)
            :base(costCenterSubtotalDto)
        {
            if (costCenterSubtotalDto != null)
            {
                this.Id = costCenterSubtotalDto.Id;
                this.Name = costCenterSubtotalDto.Name;
                
                this.SortedGlAccounts = new List<CostCenterAccountSequenceViewModel>();
                if (costCenterSubtotalDto.GlAccounts != null)
                {
                    foreach (var glAccountDto in costCenterSubtotalDto.GlAccounts)
                    {
                        if (glAccountDto != null)
                        {
                            this.SortedGlAccounts.Add(new CostCenterAccountSequenceViewModel(new CostCenterGlAccountViewModel(glAccountDto, this.GlClass)));
                        }
                    }
                }

                if(costCenterSubtotalDto.Pools != null)
                {
                    foreach(var pooledAccount in costCenterSubtotalDto.Pools)
                    {
                        if(pooledAccount != null && pooledAccount.Umbrella != null)
                        {
                            this.SortedGlAccounts.Add(new CostCenterAccountSequenceViewModel(new GlBudgetPoolViewModel(pooledAccount)));
                        }
                    }
                }

                this.SortedGlAccounts = this.SortedGlAccounts.OrderBy(a => a.Id).ToList();
            }
        }
    }
}