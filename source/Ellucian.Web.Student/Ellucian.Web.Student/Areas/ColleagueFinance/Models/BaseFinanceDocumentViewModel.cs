﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This view model represents a generic financial document. This class is intended to be extended
    /// by LedgerEntryDocumentViewModel and AccountsPayablePurchasingDocumentViewModel.
    /// </summary>
    public abstract class BaseFinanceDocumentViewModel
    {

        /// <summary>
        /// ID of the document
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// Not all documents will have a Number, but it needs to be explicitly defined
        /// so we can reliably determine the document type for all documents.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Is the document a voucher?
        /// </summary>
        public bool IsVoucher { get { return this.DocumentType == FinanceDocumentType.Voucher; } }

        /// <summary>
        /// Is the document a purchase order?
        /// </summary>
        public bool IsPurchaseOrder { get { return this.DocumentType == FinanceDocumentType.PurchaseOrder; } }

        /// <summary>
        /// Is the document a blanket purchase order?
        /// </summary>
        public bool IsBlanketPurchaseOrder { get { return this.DocumentType == FinanceDocumentType.BlanketPurchaseOrder; } }

        /// <summary>
        /// Is the document a recurring voucher?
        /// </summary>
        public bool IsRecurringVoucher { get { return this.DocumentType == FinanceDocumentType.RecurringVoucher; } }

        /// <summary>
        /// Is the document a requisition?
        /// </summary>
        public bool IsRequisition { get { return this.DocumentType == FinanceDocumentType.Requisition; } }

        /// <summary>
        /// Is the document a journal entry?
        /// </summary>
        public bool IsJournalEntry { get { return this.DocumentType == FinanceDocumentType.JournalEntry; } }

        /// <summary>
        /// Is the document a procurement document?
        /// </summary>
        public bool IsProcurementDocument
        {
            get
            {
                return this.DocumentType == FinanceDocumentType.Voucher
                    || this.DocumentType == FinanceDocumentType.PurchaseOrder
                    || this.DocumentType == FinanceDocumentType.Requisition
                    || this.DocumentType == FinanceDocumentType.RecurringVoucher
                    || this.DocumentType == FinanceDocumentType.BlanketPurchaseOrder
                    || this.DocumentType == FinanceDocumentType.RecurringVoucher;
            }
        }

        /// <summary>
        /// Is the document a general ledger document?
        /// </summary>
        public bool IsLedgerEntryDocument
        {
            get
            {
                return this.DocumentType == FinanceDocumentType.JournalEntry;
            }
        }

        /// <summary>
        /// Returns a string indicating which type of document we have
        /// </summary>
        private FinanceDocumentType DocumentType
        {
            get
            {
                FinanceDocumentType documentType = FinanceDocumentType.NoValue;
                if (Number.Substring(0, 2).ToUpper() == "RV")
                {
                    documentType = FinanceDocumentType.RecurringVoucher;
                }
                else
                {
                    switch (Number.Substring(0, 1).ToUpper())
                    {
                        case "V":
                            documentType = FinanceDocumentType.Voucher;
                            break;
                        case "P":
                            documentType = FinanceDocumentType.PurchaseOrder;
                            break;
                        case "B":
                            documentType = FinanceDocumentType.BlanketPurchaseOrder;
                            break;
                        case "J":
                            documentType = FinanceDocumentType.JournalEntry;
                            break;
                        default:
                            documentType = FinanceDocumentType.Requisition;
                            // Do nothing
                            break;
                    }
                }

                return documentType;
            }
        }

        /// <summary>
        /// Document comments
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// List of approvers for this document
        /// </summary>
        public List<DocumentApproverViewModel> Approvers { get; set; }

        /// <summary>
        /// General ledger account number associated with the document being viewed.
        /// </summary>
        public string GeneralLedgerAccountNumberId { get; set; }

        /// <summary>
        /// Formatted general ledger account number associated with the document being viewed.
        /// </summary>
        public string FormattedAccountNumber { get; set; }

        /// <summary>
        /// Description of the general ledger account number associated with the document being viewed.
        /// </summary>
        public string GeneralLedgerAccountNumberDescription { get; set; }

        /// <summary>
        /// Create a new base finance view model
        /// </summary>
        /// <param name="documentId">Document ID</param>
        /// <param name="number">Document Number</param>
        public BaseFinanceDocumentViewModel(string documentId, string number)
        {
            this.DocumentId = documentId;
            this.Number = number;
            this.Approvers = new List<DocumentApproverViewModel>();

        }

        /// <summary>
        /// Set the list of approvers for this document
        /// </summary>
        /// <param name="approvers">List of Approver DTOs</param>
        public void SetApprovers(List<Approver> approvers)
        {
            this.Approvers.RemoveAll(x => x is DocumentApproverViewModel);
            if (approvers != null)
            {
                foreach (var approver in approvers)
                {
                    if (approver != null)
                    {
                        this.Approvers.Add(new DocumentApproverViewModel(approver));
                    }
                }
            }
        }
    }
}