﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using System.Linq;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This view model represents a cost center with no underlying subtotals or GL accounts.
    /// </summary>
    public class BaseCostCenterViewModel : BaseCostCenterTotalsViewModel
    {
        /// <summary>
        /// This is the cost center ID.
        /// </summary>
        public string CostCenterId { get; set; }

        /// <summary>
        /// This is the name to be displayed in the markup.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID of the UNIT component.
        /// </summary>
        public string UnitId { get; set; }

        /// <summary>
        /// Boolean to indicate if the Cost Center has any revenue accounts.
        /// </summary>
        public bool HasRevenue { get; private set; }

        /// <summary>
        /// Boolean to indicate if the Cost Center has any expense accounts.
        /// </summary>
        public bool HasExpense { get; private set; }

        /// <summary>
        /// This property hides the base ListViewBudgetSpentPercent by formatting it based on if there are any expenses or not.
        /// </summary>
        public new string ListViewBudgetSpentPercent
        {
            get
            {
                if (this.HasExpense)
                {
                    return base.ListViewBudgetSpentPercent;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property hides the base BudgetRemainingFormatted by formatting it based on if there are any expenses or not.
        /// </summary>
        public new string BudgetRemainingFormatted
        {
            get
            {
                if (this.HasExpense)
                {
                    return base.BudgetRemainingFormatted;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property hides the base TotalExpensesFormatted by formatting it based on if there are any expenses or not.
        /// </summary>
        public new string TotalExpensesFormatted
        {
            get
            {
                if (this.HasExpense)
                {
                    return base.TotalExpensesFormatted;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property hides the base TotalBudget by formatting it based on if there are any expenses or not.
        /// </summary>
        public new string TotalBudget
        {
            get
            {
                if (this.HasExpense)
                {
                    return base.TotalBudget;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property hides the base TotalActuals by formatting it based on if there are any expenses or not.
        /// </summary>
        public new string TotalActuals
        {
            get
            {
                if (this.HasExpense)
                {
                    return base.TotalActuals;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property hides the base TotalEncumbrances by formatting it based on if there are any expenses or not.
        /// </summary>
        public new string TotalEncumbrances
        {
            get
            {
                if (this.HasExpense)
                {
                    return base.TotalEncumbrances;
                }
                else
                {
                    return string.Empty;
                }
            }
        }



        /// <summary>
        /// This property hides the base FinancialHealthTextForScreenReader by formatting it based on if there are any expenses or not.
        /// </summary>
        public new string FinancialHealthTextForScreenReader
        {
            get
            {
                if (this.HasExpense)
                {
                    return base.FinancialHealthTextForScreenReader;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property hides the base TotalActualsRevenue by formatting it based on if there are any revenue or not.
        /// </summary>
        public new string TotalActualsRevenue
        {
            get
            {
                if (this.HasRevenue)
                {
                    return base.TotalActualsRevenue;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property hides the base TotalBudgetRevenue by formatting it based on if there are any revenue or not.
        /// </summary>
        public new string TotalBudgetRevenue
        {
            get
            {
                if (this.HasRevenue)
                {
                    return base.TotalBudgetRevenue;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Initialize the view model.
        /// </summary>
        /// <param name="costCenterDto">Cost center DTO.</param>
        public BaseCostCenterViewModel(CostCenter costCenterDto)
            : base(costCenterDto)
        {
            if (costCenterDto != null)
            {
                this.CostCenterId = costCenterDto.Id;
                this.Name = costCenterDto.Name;
                this.UnitId = costCenterDto.UnitId;

                if (costCenterDto.CostCenterSubtotals != null && costCenterDto.CostCenterSubtotals.Any(ccs => ccs.GlClass == GlClass.Expense))
                {
                    this.HasExpense = true;
                }
                else
                {
                    this.HasExpense = false;
                }

                if (costCenterDto.CostCenterSubtotals != null && costCenterDto.CostCenterSubtotals.Any(ccs => ccs.GlClass == GlClass.Revenue))
                {
                    this.HasRevenue = true;
                }
                else
                {
                    this.HasRevenue = false;
                }
            }
        }
    }
}