﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This view model represents an Accounts Payable/Purchasing document and is intended to be
    /// extended by the various AP/PUR document view models (e.g. Voucher, PO, BPO, etc.).
    /// </summary>
    public abstract class AccountsPayablePurchasingDocumentViewModel : BaseFinanceDocumentViewModel
    {
        /// <summary>
        /// Vendor ID
        /// </summary>
        public string VendorId { get; set; }

        /// <summary>
        /// Vendor name
        /// </summary>
        public string VendorName { get; set; }

        /// <summary>
        /// Overall amount for the document
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// Maintenance date
        /// </summary>
        public string MaintenanceDate { get; set; }

        /// <summary>
        /// Document currency code
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Document date
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Accounts payable type
        /// </summary>
        public string ApType { get; set; }

        /// <summary>
        /// List of line items for this document.
        /// </summary>
        public List<LineItemViewModel> LineItems { get; set; }

        /// <summary>
        /// Create a new accounts payable/purchasing document.
        /// </summary>
        /// <param name="documentId">Document ID</param>
        /// <param name="number">Document Number</param>
        /// <param name="comments">Comments for this document</param>
        /// <param name="approvers">Approvers on this document</param>
        /// <param name="date">Document date</param>
        public AccountsPayablePurchasingDocumentViewModel(string documentId, string number, DateTime date)
            : base(documentId, number)
        {
            this.Date = date.ToShortDateString();
            this.LineItems = new List<LineItemViewModel>();
        }

        /// <summary>
        /// Set the list of line items for this document.
        /// </summary>
        /// <param name="lineItems">List of LineItem DTOs.</param>
        public void SetLineItems(List<LineItem> lineItems)
        {
            this.LineItems.RemoveAll(x => x is LineItemViewModel);
            if (lineItems != null)
            {
                var id = 1;
                foreach (var lineItem in lineItems)
                {
                    if (lineItem != null)
                    {
                        var lineItemVM = new LineItemViewModel(lineItem);

                        // Set the item ID so we can uniquely identify the item.
                        lineItemVM.DocumentItemId = "document-line-item" + id.ToString();
                        this.LineItems.Add(lineItemVM);
                        id++;
                    }
                }
            }
        }
    }
}