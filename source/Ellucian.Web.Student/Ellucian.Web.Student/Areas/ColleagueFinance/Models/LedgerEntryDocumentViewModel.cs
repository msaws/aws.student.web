﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This view model represents a generic ledger entry. This class is intended
    /// to be extended by Journal Entries and Budget Entries
    /// </summary>
    public abstract class LedgerEntryDocumentViewModel : BaseFinanceDocumentViewModel
    {
        /// <summary>
        /// Date of this ledger entry
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Date this ledger entry was entered
        /// </summary>
        public string EnteredDate { get; set; }

        /// <summary>
        /// User who entered this ledger entry
        /// </summary>
        public string EnteredBy { get; set; }

        /// <summary>
        /// List of  items for this ledger entry
        /// </summary>
        public List<ItemViewModel> LineItems { get; set; }

        /// <summary>
        /// Create a new ledger entry
        /// General Ledger entries do not have a sequential ID, so the ID and
        /// number are the same. We need both because of the parent class.
        /// </summary>
        /// <param name="documentId">Ledger entry ID</param>
        /// <param name="number">Ledger entry number</param>
        /// <param name="date">Posting date for this ledger entry</param>
        /// <param name="enteredDate">Entered date for this ledger entry</param>
        /// <param name="enteredBy">User who entered this ledger entry</param>
        public LedgerEntryDocumentViewModel(string documentId, string number, DateTime date, DateTime enteredDate, string enteredBy)
            : base(documentId, number)
        {
            this.Date = date.ToShortDateString();
            this.EnteredDate = enteredDate.ToShortDateString();
            this.EnteredBy = enteredBy;
            this.LineItems = new List<ItemViewModel>();
        }

        /// <summary>
        /// Set the list of items for this ledger entry
        /// </summary>
        /// <param name="items">List of ledger entry item DTOs</param>
        public void SetItems(List<JournalEntryItem> items)
        {
            this.LineItems.RemoveAll(x => x is ItemViewModel);
            if (items != null)
            {
                var id = 1;
                foreach (var item in items)
                {
                    if (item != null)
                    {
                        var itemVM = new ItemViewModel(item);

                        // Set the item ID so we can uniquely identify the item.
                        itemVM.DocumentItemId = "document-line-item" + id.ToString();
                        this.LineItems.Add(itemVM);
                        id++;
                    }
                }
            }
        }
    }
}