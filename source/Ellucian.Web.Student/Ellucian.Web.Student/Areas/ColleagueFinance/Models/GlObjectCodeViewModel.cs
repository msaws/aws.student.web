﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A view model that represents a General Ledger Object Code for the Object View
    /// </summary>
    public class GlObjectCodeViewModel
    {
        /// <summary>
        /// General Ledger Class of the General Ledger Object Code
        /// </summary>
        public GlClass GlClass { get; set; }

        /// <summary>
        /// Id of the General Ledger Object Code
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name of the General Ledger Object Code
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Total budget for the General Ledger Object Code
        /// </summary>
        public decimal _TotalBudget;
        public string TotalBudget
        {
            get
            {
                return this._TotalBudget.ToString("C");
            }
        }

        /// <summary>
        /// Total Actuals for the General Ledger Object Code
        /// </summary>
        public decimal _TotalActuals;
        public string TotalActuals
        {
            get
            {
                return this._TotalActuals.ToString("C");
            }
        }

        /// <summary>
        /// Total Encumbrances for the General Ledger Object Code
        /// </summary>
        public decimal _TotalEncumbrances;
        public string TotalEncumbrances
        {
            get
            {
                return this._TotalEncumbrances.ToString("C");
            }
        }

        /// <summary>
        /// The amount remaining in the budget for the entire object code
        /// </summary>
        public string TotalBudgetRemaining
        {
            get
            {
                return (this._TotalBudget - (this._TotalActuals + this._TotalEncumbrances)).ToString("C");
            }
        }

        /// <summary>
        /// A representation of the amount processed divided by the amount budgeted (in percent format).
        /// </summary>
        public string TotalPercentProcessed
        {
            get
            {
                var percent = 0m;

                if(this._TotalBudget == 0)
                {
                    if((this._TotalActuals + this._TotalEncumbrances) > 0)
                    {
                        percent = 101;
                    }
                    else if((this._TotalActuals + this._TotalEncumbrances) < 0)
                    {
                        percent = -101;
                    }
                }
                else
                {
                    percent = Math.Round(((this._TotalActuals + this._TotalEncumbrances) / this._TotalBudget) * 100m);
                }

                return string.Format("{0:n0}", percent) + " %";
            }
        }

        /// <summary>
        /// The CSS class that is applied to the financial health column of the object view
        /// </summary>
        public string FinancialHealthIndicator
        {
            get
            {
                var indicatorStyle = "";
                if(this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Expense)
                {
                    if(this._TotalActuals + this._TotalEncumbrances > this._TotalBudget)
                    {
                        indicatorStyle = "group-icon-red";
                    }
                    else if ((this._TotalActuals + this._TotalEncumbrances) > this._TotalBudget * 0.85m)
                    {
                        indicatorStyle = "group-icon-yellow";
                    }
                    else
                    {
                        indicatorStyle = "group-icon-green";
                    }
                }
                return indicatorStyle;
            }
        }

        /// <summary>
        /// The off screen text that is applied to the financial health column of the object view
        /// </summary>
        public string FinancialHealthIndicatorText
        {
            get
            {
                var indicatorText = "";
                if (this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Expense)
                {
                    if (this._TotalActuals + this._TotalEncumbrances > this._TotalBudget)
                    {
                        indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
                    }
                    else if ((this._TotalActuals + this._TotalEncumbrances) > this._TotalBudget * 0.85m)
                    {
                        indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
                    }
                    else
                    {
                        indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
                    }
                }
                return indicatorText;
            }
        }

        /// <summary>
        /// The style for color coding certain columns
        /// </summary>
        public string ColorCodeStyle
        {
            get
            {
                var colorStyle = "remaining-black";
                if (this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Expense)
                {
                    if (this._TotalActuals + this._TotalEncumbrances > this._TotalBudget)
                    {
                        colorStyle = "remaining-red";
                    }
                }
                else if (this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Revenue)
                {
                    if (this._TotalActuals + this._TotalEncumbrances > this._TotalBudget)
                    {
                        colorStyle = "remaining-green";
                    }
                }
                return colorStyle;
            }
        }

        /// <summary>
        /// List of General Ledger Accounts within the General Ledger Object Code
        /// </summary>
        public List<GlObjectCodeGlAccountSequenceViewModel> GlAccounts { get; set; }

        public GlObjectCodeViewModel(GlObjectCode objectCodeDto)
        {
            if(objectCodeDto != null)
            {
                this.GlClass = objectCodeDto.GlClass;
                this.Id = objectCodeDto.Id;
                this.Name = objectCodeDto.Name;
                if(this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Revenue || this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Liability || this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.FundBalance)
                {
                    this._TotalBudget = objectCodeDto.TotalBudget * -1;
                    this._TotalActuals = objectCodeDto.TotalActuals * -1;
                    this._TotalEncumbrances = objectCodeDto.TotalEncumbrances * -1;
                }
                else
                {
                    this._TotalBudget = objectCodeDto.TotalBudget;
                    this._TotalActuals = objectCodeDto.TotalActuals;
                    this._TotalEncumbrances = objectCodeDto.TotalEncumbrances;
                }
                this.GlAccounts = new List<GlObjectCodeGlAccountSequenceViewModel>();

                if (objectCodeDto.GlAccounts != null)
                {
                    foreach (var nonPooledAccountDto in objectCodeDto.GlAccounts)
                    {
                        if (nonPooledAccountDto != null)
                        {
                            this.GlAccounts.Add(new GlObjectCodeGlAccountSequenceViewModel(new GlObjectCodeGlAccountViewModel(nonPooledAccountDto, this.GlClass, this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Expense)));
                        }
                    }
                }

                if (objectCodeDto.Pools != null)
                {
                    foreach (var budgetPoolDto in objectCodeDto.Pools)
                    {
                        if (budgetPoolDto != null)
                        {
                            this.GlAccounts.Add(new GlObjectCodeGlAccountSequenceViewModel(new GlObjectCodeBudgetPoolViewModel(budgetPoolDto, this.GlClass, this.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Expense)));
                        }
                    }
                }
                this.GlAccounts = this.GlAccounts.OrderBy(a => a.FormattedGlAccount).ToList();
            }
        }
    }
}