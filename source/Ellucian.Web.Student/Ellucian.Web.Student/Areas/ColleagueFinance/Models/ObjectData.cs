﻿using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    public class ObjectData
    {
        /// <summary>
        /// List of all GL Object Codes.
        /// </summary>
        private List<GlObjectCodeViewModel> GlObjectCodes;

        #region Expense Object Data

        /// <summary>
        /// Subset of GL Object Codes consisting of only expense accounts
        /// </summary>
        public List<GlObjectCodeViewModel> ExpenseObjectCodes
        {
            get
            {
                return this.GlObjectCodes.Where(c => c.GlClass == GlClass.Expense).ToList();
            }
        }

        /// <summary>
        /// The total budget for all expense object codes
        /// </summary>
        private decimal _TotalBudgetExpense
        {
            get
            {
                return this.ExpenseObjectCodes.Sum(c => c._TotalBudget);
            }
        }
        public string TotalBudgetExpense
        {
            get
            {
                return this._TotalBudgetExpense.ToString("C");
            }
        }

        /// <summary>
        /// The total actuals for all expense object codes
        /// </summary>
        private decimal _TotalActualsExpense
        {
            get
            {
                return this.ExpenseObjectCodes.Sum(c => c._TotalActuals);
            }
        }
        public string TotalActualsExpense
        {
            get
            {
                return this._TotalActualsExpense.ToString("C");
            }
        }

        /// <summary>
        /// The total encumbrances for all expense object codes
        /// </summary>
        private decimal _TotalEncumbrancesExpense
        {
            get
            {
                return this.ExpenseObjectCodes.Sum(c => c._TotalEncumbrances);
            }
        }
        public string TotalEncumbrancesExpense
        {
            get
            {
                return this._TotalEncumbrancesExpense.ToString("C");
            }
        }

        /// <summary>
        /// The amount remaining in the budget for the entire object code
        /// </summary>
        public string TotalBudgetRemainingExpense
        {
            get
            {
                return (this._TotalBudgetExpense - (this._TotalActualsExpense + this._TotalEncumbrancesExpense)).ToString("C");
            }
        }

        /// <summary>
        /// A representation of the amount processed divided by the amount budgeted (in percent format).
        /// </summary>
        public string TotalPercentProcessedExpense
        {
            get
            {
                var percent = 0m;

                if (this._TotalBudgetExpense == 0)
                {
                    if ((this._TotalActualsExpense + this._TotalEncumbrancesExpense) > 0)
                    {
                        percent = 101;
                    }
                    else if ((this._TotalActualsExpense + this._TotalEncumbrancesExpense) < 0)
                    {
                        percent = -101;
                    }
                }
                else
                {
                    percent = Math.Round(((this._TotalActualsExpense + this._TotalEncumbrancesExpense) / this._TotalBudgetExpense) * 100m);
                }

                return string.Format("{0:n0}", percent) + " %";
            }
        }

        /// <summary>
        /// The CSS class that is applied to the financial health column of the object view
        /// </summary>
        public string TotalFinancialHealthIndicatorExpense
        {
            get
            {
                var indicatorStyle = "";
                if (this._TotalActualsExpense + this._TotalEncumbrancesExpense > this._TotalBudgetExpense)
                {
                    indicatorStyle = "group-icon-red";
                }
                else if ((this._TotalActualsExpense + this._TotalEncumbrancesExpense) > this._TotalBudgetExpense * 0.85m)
                {
                    indicatorStyle = "group-icon-yellow";
                }
                else
                {
                    indicatorStyle = "group-icon-green";
                }

                return indicatorStyle;
            }
        }

        /// <summary>
        /// The off screen text that is applied to the financial health column of the object view
        /// </summary>
        public string TotalFinancialHealthIndicatorTextExpense
        {
            get
            {
                var indicatorText = "";
                if (this._TotalActualsExpense + this._TotalEncumbrancesExpense > this._TotalBudgetExpense)
                {
                    indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
                }
                else if ((this._TotalActualsExpense + this._TotalEncumbrancesExpense) > this._TotalBudgetExpense * 0.85m)
                {
                    indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
                }
                else
                {
                    indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
                }

                return indicatorText;
            }
        }

        /// <summary>
        /// The style for color coding certain expense columns
        /// </summary>
        public string ColorCodeStyleExpense
        {
            get
            {
                var colorStyle = "remaining-black";
                if (this._TotalActualsExpense + this._TotalEncumbrancesExpense > this._TotalBudgetExpense)
                {
                    colorStyle = "remaining-red";
                }

                return colorStyle;
            }
        }

        #endregion

        #region Revenue Object Data

        /// <summary>
        /// Subset of GL Object Codes consisting of only revenue accounts
        /// </summary>
        public List<GlObjectCodeViewModel> RevenueObjectCodes
        {
            get
            {
                return this.GlObjectCodes.Where(c => c.GlClass == GlClass.Revenue).ToList();
            }
        }

        /// <summary>
        /// The total budget for all revenue object codes
        /// </summary>
        private decimal _TotalBudgetRevenue
        {
            get
            {
                return this.RevenueObjectCodes.Sum(c => c._TotalBudget);
            }
        }
        public string TotalBudgetRevenue
        {
            get
            {
                return this._TotalBudgetRevenue.ToString("C");
            }
        }

        /// <summary>
        /// The total actuals for all revenue object codes
        /// </summary>
        private decimal _TotalActualsRevenue
        {
            get
            {
                return this.RevenueObjectCodes.Sum(c => c._TotalActuals);
            }
        }
        public string TotalActualsRevenue
        {
            get
            {
                return this._TotalActualsRevenue.ToString("C");
            }
        }

        /// <summary>
        /// The total encumbrances for all revenue object codes
        /// </summary>
        private decimal _TotalEncumbrancesRevenue
        {
            get
            {
                return this.RevenueObjectCodes.Sum(c => c._TotalEncumbrances);
            }
        }
        public string TotalEncumbrancesRevenue
        {
            get
            {
                return this._TotalEncumbrancesRevenue.ToString("C");
            }
        }

        /// <summary>
        /// The amount remaining in the revenue budget for the entire object code
        /// </summary>
        public string TotalBudgetRemainingRevenue
        {
            get
            {
                return (this._TotalBudgetRevenue - (this._TotalActualsRevenue + this._TotalEncumbrancesRevenue)).ToString("C");
            }
        }

        /// <summary>
        /// A representation of the amount processed divided by the amount budgeted (in percent format) for revenue.
        /// </summary>
        public string TotalPercentProcessedRevenue
        {
            get
            {
                var percent = 0m;

                if (this._TotalBudgetRevenue == 0)
                {
                    if ((this._TotalActualsRevenue + this._TotalEncumbrancesRevenue) > 0)
                    {
                        percent = 101;
                    }
                    else if ((this._TotalActualsRevenue + this._TotalEncumbrancesRevenue) < 0)
                    {
                        percent = -101;
                    }
                }
                else
                {
                    percent = Math.Round(((this._TotalActualsRevenue + this._TotalEncumbrancesRevenue) / this._TotalBudgetRevenue) * 100m);
                }

                return string.Format("{0:n0}", percent) + " %";
            }
        }

        /// <summary>
        /// The style for color coding certain revenue columns
        /// </summary>
        public string ColorCodeStyleRevenue
        {
            get
            {
                var colorStyle = "remaining-black";
                if (this._TotalActualsRevenue + this._TotalEncumbrancesRevenue > this._TotalBudgetRevenue)
                {
                    colorStyle = "remaining-green";
                }

                return colorStyle;
            }
        }

        #endregion

        #region Asset Object Data

        /// <summary>
        /// Subset of GL Object Codes consisting of only asset accounts
        /// </summary>
        public List<GlObjectCodeViewModel> AssetObjectCodes
        {
            get
            {
                return this.GlObjectCodes.Where(c => c.GlClass == GlClass.Asset).ToList();
            }
        }

        /// <summary>
        /// The total actuals for all asset object codes
        /// </summary>
        private decimal _TotalActualsAsset
        {
            get
            {
                return this.AssetObjectCodes.Sum(c => c._TotalActuals);
            }
        }
        public string TotalActualsAsset
        {
            get
            {
                return this._TotalActualsAsset.ToString("C");
            }
        }

        /// <summary>
        /// The total encumbrances for all asset object codes
        /// </summary>
        private decimal _TotalEncumbrancesAsset
        {
            get
            {
                return this.AssetObjectCodes.Sum(c => c._TotalEncumbrances);
            }
        }
        public string TotalEncumbrancesAsset
        {
            get
            {
                return this._TotalEncumbrancesAsset.ToString("C");
            }
        }

        #endregion

        #region Liability Object Data

        /// <summary>
        /// Subset of GL Object Codes consisting of only liability accounts
        /// </summary>
        public List<GlObjectCodeViewModel> LiabilityObjectCodes
        {
            get
            {
                return this.GlObjectCodes.Where(c => c.GlClass == GlClass.Liability).ToList();
            }
        }

        /// <summary>
        /// The total actuals for all liability object codes
        /// </summary>
        private decimal _TotalActualsLiability
        {
            get
            {
                return this.LiabilityObjectCodes.Sum(c => c._TotalActuals);
            }
        }
        public string TotalActualsLiability
        {
            get
            {
                return this._TotalActualsLiability.ToString("C");
            }
        }

        /// <summary>
        /// The total encumbrances for all liability object codes
        /// </summary>
        private decimal _TotalEncumbrancesLiability
        {
            get
            {
                return this.LiabilityObjectCodes.Sum(c => c._TotalEncumbrances);
            }
        }
        public string TotalEncumbrancesLiability
        {
            get
            {
                return this._TotalEncumbrancesLiability.ToString("C");
            }
        }

        #endregion

        #region Fund Balance Object Data

        /// <summary>
        /// Subset of GL Object Codes consisting of only fund balance accounts
        /// </summary>
        public List<GlObjectCodeViewModel> FundBalanceObjectCodes
        {
            get
            {
                return this.GlObjectCodes.Where(c => c.GlClass == GlClass.FundBalance).ToList();
            }
        }

        /// <summary>
        /// The total actuals for all fund balance object codes
        /// </summary>
        private decimal _TotalActualsFundBalance
        {
            get
            {
                return this.FundBalanceObjectCodes.Sum(c => c._TotalActuals);
            }
        }
        public string TotalActualsFundBalance
        {
            get
            {
                return this._TotalActualsFundBalance.ToString("C");
            }
        }

        /// <summary>
        /// The total encumbrances for all fund balance object codes
        /// </summary>
        private decimal _TotalEncumbrancesFundBalance
        {
            get
            {
                return this.FundBalanceObjectCodes.Sum(c => c._TotalEncumbrances);
            }
        }
        public string TotalEncumbrancesFundBalance
        {
            get
            {
                return this._TotalEncumbrancesFundBalance.ToString("C");
            }
        }

        #endregion

        /// <summary>
        /// Budget for Revenue object codes minus budget for Expense object codes
        /// </summary>
        public string TotalBudgetNet
        {
            get
            {
                return (_TotalBudgetRevenue - _TotalBudgetExpense).ToString("C");
            }
        }

        /// <summary>
        /// Actuals for Revenue object codes minus actuals for Expense object codes
        /// </summary>
        public string TotalActualsNet
        {
            get
            {
                return (_TotalActualsRevenue - _TotalActualsExpense).ToString("C");
            }
        }

        /// <summary>
        /// Boolean to indicate if any Object Code in the list has a revenue account.
        /// </summary>
        public bool HasAnyRevenue
        {
            get
            {
                return this.GlObjectCodes.Any(cc => cc.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Revenue);
            }
        }

        /// <summary>
        /// Boolean to indicate if any Object Code in the list has an expense account.
        /// </summary>
        public bool HasAnyExpense
        {
            get
            {
                return this.GlObjectCodes.Any(cc => cc.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Expense);
            }
        }

        /// <summary>
        /// Boolean to indicate if any Object Code in the list has an asset account.
        /// </summary>
        public bool HasAnyAsset
        {
            get
            {
                return this.GlObjectCodes.Any(cc => cc.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Asset);
            }
        }

        /// <summary>
        /// Boolean to indicate if any Object Code in the list has a liability account.
        /// </summary>
        public bool HasAnyLiability
        {
            get
            {
                return this.GlObjectCodes.Any(cc => cc.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.Liability);
            }
        }

        /// <summary>
        /// Boolean to indicate if any Object Code in the list has a fund balance account.
        /// </summary>
        public bool HasAnyFundBalance
        {
            get
            {
                return this.GlObjectCodes.Any(cc => cc.GlClass == Colleague.Dtos.ColleagueFinance.GlClass.FundBalance);
            }
        }

        public ObjectData(IEnumerable<GlObjectCode> objectCodeDtos)
        {
            this.GlObjectCodes = new List<GlObjectCodeViewModel>();
            if (objectCodeDtos != null)
            {
                foreach (var objectCodeDto in objectCodeDtos)
                {
                    if (objectCodeDto != null)
                    {
                        this.GlObjectCodes.Add(new GlObjectCodeViewModel(objectCodeDto));
                    }
                }
            }
            this.GlObjectCodes = this.GlObjectCodes.OrderBy(c => c.Id).ToList();
        }
    }
}