﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This view model represents a single option in the fiscal year filter.
    /// </summary>
    public class FiscalYearFilterViewModel
    {
        /// <summary>
        /// Value to be used when the option is selected.
        /// </summary>
        public string FiscalYear { get; set; }

        /// <summary>
        /// Description of the option.
        /// </summary>
        public string FiscalYearDescription { get; set; }

        /// <summary>
        /// Initialize the view model.
        /// </summary>
        /// <param name="fiscalYear">Fiscal year.</param>
        public FiscalYearFilterViewModel(string fiscalYear)
        {
            this.FiscalYear = fiscalYear;
            this.FiscalYearDescription = "FY" + fiscalYear;
        }

        /// <summary>
        /// Initialize the fiscal year.
        /// </summary>
        /// <param name="fiscalYear">Fiscal year.</param>
        /// <param name="description">Description of the option.</param>
        public FiscalYearFilterViewModel(string fiscalYear, string description)
            : this(fiscalYear)
        {
            this.FiscalYearDescription = description;
        }
    }
}