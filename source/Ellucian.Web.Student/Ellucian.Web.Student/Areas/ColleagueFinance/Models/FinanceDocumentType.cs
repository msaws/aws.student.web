﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This enumeration represents all of the possible document types available to generic finance documents.
    /// </summary>
    public enum FinanceDocumentType
    {
        /// <summary>
        /// Default value; document has no type
        /// </summary>
        NoValue,

        /// <summary>
        /// This document is a Requisition
        /// </summary>
        Requisition,

        /// <summary>
        /// This document is a Purchase Order
        /// </summary>
        PurchaseOrder,

        /// <summary>
        /// This document is a Blanket Purchase Order
        /// </summary>
        BlanketPurchaseOrder,

        /// <summary>
        /// This document is a Voucher
        /// </summary>
        Voucher,

        /// <summary>
        /// This document is a Recurring Voucher
        /// </summary>
        RecurringVoucher,

        /// <summary>
        /// This document is a Journal Entry
        /// </summary>
        JournalEntry,

        /// <summary>
        /// This document is a Budget Entry
        /// </summary>
        BudgetEntry
    }
}