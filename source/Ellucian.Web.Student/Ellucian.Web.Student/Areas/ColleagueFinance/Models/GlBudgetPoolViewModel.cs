﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A GL Budget Pool view model.
    /// </summary>
    public class GlBudgetPoolViewModel
    {
        /// <summary>
        /// Initializes a GL Budget Pool.
        /// </summary>
        /// <param name="dto">A GlBudgetPool DTO</param>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        public GlBudgetPoolViewModel(GlBudgetPool dto)
        {
            if (dto != null)
            {
                this.IsUmbrellaVisible = dto.IsUmbrellaVisible;
                if (dto.Umbrella != null)
                {
                    if(this.IsUmbrellaVisible)
                    {
                        this.Umbrella = new CostCenterGlAccountViewModel(dto.Umbrella, GlClass.Expense);
                    }
                    else
                    {
                        // We don't want to only rely on client-side masking so zero out any Budget, Actual, and Encumbrance values since the Umbrella data is marked as not visible.
                        dto.Umbrella.Budget = 0;
                        dto.Umbrella.Actuals = 0;
                        dto.Umbrella.Encumbrances = 0;
                        
                        this.Umbrella = new CostCenterGlAccountViewModel(dto.Umbrella, GlClass.Expense);
                    }
                    this.Poolees = new List<CostCenterGlAccountViewModel>();
                    if (dto.Poolees != null)
                    {
                        foreach (var poolee in dto.Poolees)
                        {
                            if (poolee != null)
                            {
                                // If there are direct expenses to the Umbrella account but the Umbrella isn't visible, we don't want to add the umbrella poolee account.
                                if (this.IsUmbrellaVisible || !poolee.GlAccountNumber.Equals(this.Umbrella.GlAccount, StringComparison.OrdinalIgnoreCase))
                                {
                                    // Either the Umbrella is visible, or the poolee we are attemping to add is a different GL Account - go ahead and add the poolee.
                                    this.Poolees.Add(new CostCenterGlAccountViewModel(poolee, GlClass.Expense));
                                }
                            }
                        }

                        this.Poolees = this.Poolees.OrderBy(p => p.GlAccount).ToList();
                    }
                }
            }
        }
        /// <summary>
        /// The umbrella GL account for the budget pool.
        /// </summary>
        public CostCenterGlAccountViewModel Umbrella { get; set; }

        /// <summary>
        /// The list of poolee GL accounts for the budget pool that the user
        /// has access to given their GL account security setup.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        public List<CostCenterGlAccountViewModel> Poolees { get; set; }

        /// <summary>
        /// Whether the umbrella GL account would be visible to the user
        /// given their GL account security setup.
        /// </summary>
        public bool IsUmbrellaVisible { get; set; }
    }
}