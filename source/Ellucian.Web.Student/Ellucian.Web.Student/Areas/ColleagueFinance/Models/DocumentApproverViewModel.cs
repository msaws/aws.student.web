﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents an approver for a Purchasing document
    /// </summary>
    public class DocumentApproverViewModel
    {
        /// <summary>
        /// Approver name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Approval date
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Returns the date if present or a string if not.
        /// </summary>
        public string DateOrWaiting
        {
            get
            {
                if (string.IsNullOrEmpty(Date))
                {
                    return GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "AwaitingApprovalText");
                }
                else
                {
                    return Date;
                }
            }
        }

        /// <summary>
        /// Initialize the view model instance.
        /// </summary>
        /// <param name="approverDto">Approver DTO</param>
        public DocumentApproverViewModel(Approver approverDto)
        {
            this.Name = approverDto.ApprovalName;
            this.Date = approverDto.ApprovalDate.HasValue ? approverDto.ApprovalDate.Value.ToShortDateString() : "";
        }
    }
}