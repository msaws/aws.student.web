﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This view model represents a purchasing document that has a document number. This class is intended
    /// to be extended by BlanketPurchaseOrderNumberViewModel, PurchaseOrderNumberViewModel, and
    /// RequisitionNumberViewModel.
    /// </summary>
    public abstract class PurchasingDocumentNumberViewModel
    {
        /// <summary>
        /// ID of the purchasing document
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// All purchasing documents will have a Number.
        /// </summary>
        public virtual string Number { get; set; }
        
        /// <summary>
        /// Create a new base finance document number view model
        /// </summary>
        /// <param name="id">Document ID</param>
        /// <param name="number">Document Number</param>
        public PurchasingDocumentNumberViewModel(string id)
        {
            this.Id = id;
        }
    }
}