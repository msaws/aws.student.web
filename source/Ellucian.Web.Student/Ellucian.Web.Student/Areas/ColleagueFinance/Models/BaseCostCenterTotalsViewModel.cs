﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class contains various totals properties common to several types of cost center components.
    /// </summary>
    public abstract class BaseCostCenterTotalsViewModel
    {
        /// <summary>
        /// GlClass enum to indicate if the BaseCostCenterTotalsViewModel instance is Expense or Revenue.
        /// </summary>
        public GlClass GlClass { get; set; }

        #region Expense properties

        /// <summary>
        /// The string representation of the total budget (expense) for the cost center.
        /// </summary>
        public string TotalBudget { get { return totalBudget.ToString("C"); } }
        private decimal totalBudget;

        /// <summary>
        /// The string representation of the total actuals (expense) for the cost center.
        /// </summary>
        public string TotalActuals { get { return totalActuals.ToString("C"); } }
        private decimal totalActuals;

        /// <summary>
        /// The string representation of the total encumbrances (expense) for the cost center.
        /// </summary>
        public string TotalEncumbrances { get { return totalEncumbrances.ToString("C"); } }
        private decimal totalEncumbrances;

        /// <summary>
        /// Calculates the total expenses
        /// </summary>
        public decimal TotalExpenses { get { return totalActuals + totalEncumbrances; } }

        /// <summary>
        /// The string representation of the total expenses for the cost center.
        /// </summary>
        public string TotalExpensesFormatted { get { return TotalExpenses.ToString("C"); } }

        /// <summary>
        /// The remaining budget amount for the cost center.
        /// </summary>
        public decimal BudgetRemaining { get { return totalBudget - TotalExpenses; } }

        /// <summary>
        /// Returns the remaining budget formatted for display.
        /// </summary>
        public string BudgetRemainingFormatted { get { return BudgetRemaining.ToString("C"); } }

        /// <summary>
        /// This property returns a string representation of the remaining budget amount.
        /// </summary>
        public string BudgetRemainingString
        {
            get
            {
                string remainingString;
                if (BudgetRemaining >= 0)
                {
                    remainingString = BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText");
                }
                else
                {
                    remainingString = BudgetRemaining.ToString("C");
                }

                return remainingString;
            }
        }

        /// <summary>
        /// Removes the negative sign from the remaining budget (expense) and formats it based on if there is 
        /// any budget remaining so it can be properly read by the screen reader.
        /// </summary>
        public string BudgetRemainingScreenReaderText
        {
            get
            {
                if (BudgetRemaining < 0)
                {
                    var remaining = Math.Abs(BudgetRemaining).ToString("C");
                    return remaining + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterOverbudgetText");
                }
                else
                {
                    return this.BudgetSpentAmount;
                }
            }
        }

        /// <summary>
        /// Calculate the percentage the cost center is over budget. The value returned is used to
        /// fill the bar graph from the right side. For example, if a cost center is 25% over budget,
        /// this property would calculate a 25% fill from the right. The remaining 75% is 
        /// calculated by the BudgetSpentPercent property.
        /// </summary>
        public decimal Overage
        {
            get
            {
                decimal overage = 0;
                if (TotalExpenses > totalBudget)
                {
                    if (totalBudget > 0)
                    {
                        overage = Math.Round((((TotalExpenses - totalBudget) / totalBudget) * 100), 2);

                        if (overage > 100)
                        {
                            overage = 100;
                        }
                    }
                    else
                    {
                        overage = 100;
                    }
                }

                return overage;
            }
        }

        /// <summary>
        /// Calculate the percent that the bar graph is filled from the left side. For example if
        /// a cost center is 25% over budget, this property would calculate a 75% fill from the left.
        /// The remaining 25% is calculated by the Overage property.
        /// </summary>
        public decimal BudgetSpentPercent
        {
            get
            {
                decimal budgetSpentPercent = 0;

                if (totalBudget == 0 && TotalExpenses == 0)
                // If Budget is $0 and expenses are $0 return a 0 percentage
                {
                    budgetSpentPercent = 0;
                }
                else if (TotalExpenses < 0)
                {
                    // Total credit expenses return a zero percentage.
                    budgetSpentPercent = 0;
                }

                else if (TotalExpenses > totalBudget)
                // If expenses are greater than budget return an overspent percentage
                // If expenses are greater than the budget, return a zero percentage if there is zero budget
                // Return the 100 percent minus the overage percent when the budget is greater than zero
                {
                    if (totalBudget == 0)
                    {
                        budgetSpentPercent = 0;
                    }
                    else
                    {
                        budgetSpentPercent = Math.Round(100 - Overage);
                    }
                }
                else
                // Calculate the return percentage
                {
                    budgetSpentPercent = Math.Round(((TotalExpenses / totalBudget) * 100));
                }
                return budgetSpentPercent;
            }
        }

        /// <summary>
        /// Calculates what percentage of the budget has been spent.
        /// </summary>
        public string ListViewBudgetSpentPercent
        {
            get
            {
                var percent = "";
                if (totalBudget == 0)
                {
                    if (TotalExpenses <= 0)
                    {
                        percent = "0 %";
                    }
                    else
                    {
                        percent = "101 %";
                    }
                }
                else
                {
                    percent = Math.Round((TotalExpenses / totalBudget) * 100, MidpointRounding.AwayFromZero).ToString("N0") + " %";
                }

                return percent;
            }
        }

        /// <summary>
        /// Return the text string stating the financial health of the cost center.
        /// </summary>
        public string FinancialHealthTextForScreenReader
        {
            get
            {
                var financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterHealthGreen");
                if (BudgetRemaining < 0)
                {
                    financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterHealthRed");
                }
                else if (BudgetSpentPercent >= 85 && BudgetSpentPercent <= 100)
                {
                    financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterHealthYellow");
                }

                return financialHealthText;
            }
        }

        /// <summary>
        /// Return the text string for labeling the bar graph
        /// </summary>
        public string BudgetSpentAmount
        {
            get
            {
                if (this.TotalExpensesFormatted != null && this.TotalBudget != null)
                {
                    return this.TotalExpensesFormatted + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarText") + this.TotalBudget;
                }
                else
                {
                    return "";
                }
            }
        }

        #endregion

        #region Revenue properties

        /// <summary>
        /// The string representation of the total budget (revenue) for the cost center.
        /// </summary>
        public string TotalBudgetRevenue { get { return totalBudgetRevenue.ToString("C"); } }
        private decimal totalBudgetRevenue;

        /// <summary>
        /// The string representation of the total actuals (revenue) for the cost center.
        /// </summary>
        public string TotalActualsRevenue { get { return totalActualsRevenue.ToString("C"); } }
        private decimal totalActualsRevenue;

        /// <summary>
        /// The string representation of the total encumbrances (revenue) for the cost center.
        /// </summary>
        public string TotalEncumbrancesRevenue { get { return totalEncumbrancesRevenue.ToString("C"); } }
        private decimal totalEncumbrancesRevenue;

        /// <summary>
        /// Calculates the total expenses (revenue)
        /// </summary>
        public decimal TotalExpensesRevenue { get { return totalActualsRevenue + totalEncumbrancesRevenue; } }

        /// <summary>
        /// The string representation of the total actuals and encumbrances (revenue) for the cost center.
        /// </summary>
        public string TotalExpensesRevenueFormatted { get { return TotalExpensesRevenue.ToString("C"); } }

        /// <summary>
        /// The remaining budget amount (revenue) for the cost center.
        /// </summary>
        public decimal BudgetRemainingRevenue { get { return totalBudgetRevenue - TotalExpensesRevenue; } }

        /// <summary>
        /// Returns the remaining budget (revenue) formatted for display.
        /// </summary>
        public string BudgetRemainingRevenueFormatted { get { return BudgetRemainingRevenue.ToString("C"); } }

        /// <summary>
        /// This property returns a string representation of the remaining budget amount (revenue).
        /// </summary>
        public string BudgetRemainingRevenueString
        {
            get
            {
                string remainingString;
                if (BudgetRemainingRevenue >= 0)
                {
                    remainingString = BudgetRemainingRevenue.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText");
                }
                else
                {
                    remainingString = BudgetRemainingRevenue.ToString("C");
                }

                return remainingString;
            }
        }

        /// <summary>
        /// Removes the negative sign from the remaining budget (revenue) and formats it based on if there is 
        /// any budget remaining so it can be properly read by the screen reader.
        /// </summary>
        public string BudgetRemainingScreenReaderTextRevenue
        {
            get
            {
                if (BudgetRemainingRevenue < 0)
                {
                    var remaining = Math.Abs(BudgetRemainingRevenue).ToString("C");
                    return remaining + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterOverbudgetText");
                }
                else
                {
                    return this.BudgetReceivedAmount;
                }
            }
        }

        /// <summary>
        /// Calculate the percentage the cost center is over budget. The value returned is used to
        /// fill the bar graph from the right side. For example, if a cost center is 25% over budget,
        /// this property would calculate a 25% fill from the right. The remaining 75% is 
        /// calculated by the BudgetSpentPercentRevenue property.
        /// </summary>
        public decimal OverageRevenue
        {
            get
            {
                decimal overage = 0;
                if (TotalExpensesRevenue > totalBudgetRevenue)
                {
                    if (totalBudgetRevenue > 0)
                    {
                        overage = Math.Round((((TotalExpensesRevenue - totalBudgetRevenue) / totalBudgetRevenue) * 100), 2);

                        if (overage > 100)
                        {
                            overage = 100;
                        }
                    }
                    else
                    {
                        overage = 100;
                    }
                }

                return overage;
            }
        }

        /// <summary>
        /// Calculate the percent that the bar graph is filled from the left side. For example if
        /// a cost center is 25% over budget, this property would calculate a 75% fill from the left.
        /// The remaining 25% is calculated by the OverageRevenue property.
        /// </summary>
        public decimal BudgetSpentPercentRevenue
        {
            get
            {
                decimal budgetSpentPercent = 0;

                if (totalBudgetRevenue == 0 && TotalExpensesRevenue == 0)
                // If Budget is $0 and expenses are $0 return a 0 percentage
                {
                    budgetSpentPercent = 0;
                }
                else if (TotalExpensesRevenue < 0)
                {
                    // Total credit expenses return a zero percentage.
                    budgetSpentPercent = 0;
                }

                else if (TotalExpensesRevenue > totalBudgetRevenue)
                // If expenses are greater than budget return an overspent percentage
                // If expenses are greater than the budget, return a zero percentage if there is zero budget
                // Return the 100 percent minus the overage percent when the budget is greater than zero
                {
                    if (totalBudgetRevenue == 0)
                    {
                        budgetSpentPercent = 0;
                    }
                    else
                    {
                        budgetSpentPercent = Math.Round(100 - OverageRevenue);
                    }
                }
                else
                // Calculate the return percentage
                {
                    budgetSpentPercent = Math.Round(((TotalExpensesRevenue / totalBudgetRevenue) * 100));
                }
                return budgetSpentPercent;
            }
        }

        /// <summary>
        /// Calculates what percentage of the budget (revenue) has been spent.
        /// </summary>
        public string ListViewBudgetSpentPercentRevenue
        {
            get
            {
                var percent = "";
                if (totalBudgetRevenue == 0)
                {
                    if (TotalExpensesRevenue <= 0)
                    {
                        percent = "0 %";
                    }
                    else
                    {
                        percent = "101 %";
                    }
                }
                else
                {
                    percent = Math.Round((TotalExpensesRevenue / totalBudgetRevenue) * 100, MidpointRounding.AwayFromZero).ToString("N0") + " %";
                }

                return percent;
            }
        }

        /// <summary>
        /// Return the text string for labeling the bar graph
        /// </summary>
        public string BudgetReceivedAmount
        {
            get
            {
                if (this.TotalExpensesRevenueFormatted != null && this.TotalBudgetRevenue != null)
                {
                    return this.TotalExpensesRevenueFormatted + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarText") + this.TotalBudgetRevenue + " " + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetedText");
                }
                else
                {
                    return "";
                }
            }
        }

        #endregion

        #region Net Properties

        public string TotalBudgetNet { get { return (totalBudgetRevenue - totalBudget).ToString("C"); } }

        public string TotalActualsNet { get { return (totalActualsRevenue - totalActuals).ToString("C"); } }

        #endregion

        /// <summary>
        /// Initialize the view model based on a CostCenter DTO.
        /// </summary>
        /// <param name="costCenterDto">a Cost Center DTO</param>
        public BaseCostCenterTotalsViewModel(CostCenter costCenterDto)
        {
            if (costCenterDto != null)
            {
                this.totalBudget = costCenterDto.TotalBudget;
                this.totalActuals = costCenterDto.TotalActuals;
                this.totalEncumbrances = costCenterDto.TotalEncumbrances;

                this.totalBudgetRevenue = costCenterDto.TotalBudgetRevenue * -1;
                this.totalActualsRevenue = costCenterDto.TotalActualsRevenue * -1;
                this.totalEncumbrancesRevenue = costCenterDto.TotalEncumbrancesRevenue * -1;
            }
        }

        /// <summary>
        /// Initialize the view model based on a Cost Center GL Account DTO and a GL Class.
        /// </summary>
        /// <param name="glAccountDto">A Cost Center GL Account</param>
        /// <param name="glClass">The GL Class of the Cost Center GL Account</param>
        public BaseCostCenterTotalsViewModel(CostCenterGlAccount glAccountDto, GlClass glClass)
        {
            if (glAccountDto != null)
            {
                this.GlClass = glClass;
                if (this.GlClass == GlClass.Expense)
                {
                    this.totalBudget = glAccountDto.Budget;
                    this.totalActuals = glAccountDto.Actuals;
                    this.totalEncumbrances = glAccountDto.Encumbrances;
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    this.totalBudgetRevenue = glAccountDto.Budget * -1;
                    this.totalActualsRevenue = glAccountDto.Actuals * -1;
                    this.totalEncumbrancesRevenue = glAccountDto.Encumbrances * -1;
                }
            }
        }

        /// <summary>
        /// Initialize the view model based on a Cost Center Subtotal DTO.
        /// </summary>
        /// <param name="costCenterSubtotalDto">A Cost Center Subtotal DTO</param>
        public BaseCostCenterTotalsViewModel(CostCenterSubtotal costCenterSubtotalDto)
        {
            if (costCenterSubtotalDto != null)
            {
                this.GlClass = costCenterSubtotalDto.GlClass;
                if (this.GlClass == GlClass.Expense)
                {
                    this.totalBudget = costCenterSubtotalDto.TotalBudget;
                    this.totalActuals = costCenterSubtotalDto.TotalActuals;
                    this.totalEncumbrances = costCenterSubtotalDto.TotalEncumbrances;
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    this.totalBudgetRevenue = costCenterSubtotalDto.TotalBudget * -1;
                    this.totalActualsRevenue = costCenterSubtotalDto.TotalActuals * -1;
                    this.totalEncumbrancesRevenue = costCenterSubtotalDto.TotalEncumbrances * -1;
                }
            }
        }
    }
}