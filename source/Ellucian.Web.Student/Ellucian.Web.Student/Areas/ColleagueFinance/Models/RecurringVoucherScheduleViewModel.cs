﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a schedule within a recurring voucher
    /// </summary>
    public class RecurringVoucherScheduleViewModel : DocumentItem
    {
        /// <summary>
        /// Schedule Date
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Schedule Amount
        /// </summary>
        public decimal Amount { get; set;}

        /// <summary>
        /// Formatted schedule amount
        /// </summary>
        public string AmountDisplay { get { return this.Amount.ToString("C"); } }

        /// <summary>
        /// Schedule Tax Amount
        /// </summary>
        public decimal TaxAmount { get; set; }

        /// <summary>
        /// Formatted Tax amount
        /// </summary>
        public string TaxAmountDisplay { get { return this.TaxAmount.ToString("C"); } }

        /// <summary>
        /// Voucher Number associated with the schedule
        /// </summary>
        public string VoucherId { get; set; }

        /// <summary>
        /// ID of purged voucher.
        /// </summary>
        public string PurgedVoucherId { get; set; }

        /// <summary>
        /// Due date of the voucher associated with the schedule
        /// </summary>
        public string VoucherDueDate { get; set; }

        /// <summary>
        /// Status of the voucher associated with the schedule
        /// </summary>
        public string VoucherStatus { get; set; }

        /// <summary>
        /// Create a new recurring voucher schedule view model
        /// </summary>
        /// <param name="scheduleDto">The schedule DTO</param>
        public RecurringVoucherScheduleViewModel(RecurringVoucherSchedule scheduleDto)
        {
            this.Amount = scheduleDto.Amount;
            this.Date = scheduleDto.Date.ToShortDateString();
            this.TaxAmount = scheduleDto.TaxAmount;
            this.VoucherId = scheduleDto.VoucherId;
            this.PurgedVoucherId = scheduleDto.PurgedVoucherId;
            this.VoucherDueDate = string.Empty;
            this.VoucherStatus = string.Empty;
        }
    }
}