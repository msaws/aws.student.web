﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A GL account view model.
    /// </summary>
    public class CostCenterGlAccountViewModel : BaseCostCenterTotalsViewModel
    {
        /// <summary>
        /// The GL account for the cost center.
        /// </summary>
        public string GlAccount { get; set; }

        /// <summary>
        /// The GL account formatted for display.
        /// </summary>
        public string FormattedGlAccount { get; set; }

        /// <summary>
        /// The GL account description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// This property accesses the base ListViewBudgetSpentPercent field for revenue and expense since only one is possible at a time for a given GL account.
        /// </summary>
        public new string ListViewBudgetSpentPercent
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    return base.ListViewBudgetSpentPercent;
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    return base.ListViewBudgetSpentPercentRevenue;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property accesses the base BudgetRemainingFormatted field for revenue and expense since only one is possible at a time for a given GL account.
        /// </summary>
        public new string BudgetRemainingFormatted
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    return base.BudgetRemainingFormatted;
                }
                else if(this.GlClass == GlClass.Revenue)
                {
                    return base.BudgetRemainingRevenueFormatted;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property accesses the base TotalExpensesFormatted field for revenue and expense since only one is possible at a time for a given GL account.
        /// </summary>
        public new string TotalExpensesFormatted
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    return base.TotalExpensesFormatted;
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    return base.TotalExpensesRevenueFormatted;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property accesses the base TotalBudget field for revenue and expense since only one is possible at a time for a given GL account.
        /// </summary>
        public new string TotalBudget
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    return base.TotalBudget;
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    return base.TotalBudgetRevenue;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property accesses the base TotalActuals field for revenue and expense since only one is possible at a time for a given GL account.
        /// </summary>
        public new string TotalActuals
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    return base.TotalActuals;
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    return base.TotalActualsRevenue;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property accesses the base TotalEncumbrances field for revenue and expense since only one is possible at a time for a given GL account.
        /// </summary>
        public new string TotalEncumbrances
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    return base.TotalEncumbrances;
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    return base.TotalEncumbrancesRevenue;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Initializes the GL account view model with an explicit GL Class.
        /// </summary>
        /// <param name="glAccountDto">A GL account DTO.</param>
        /// <param name="glClass">A GL class enumeration</param>
        public CostCenterGlAccountViewModel(CostCenterGlAccount glAccountDto, GlClass glClass)
            :base(glAccountDto, glClass)
        {
            if (glAccountDto != null)
            {
                this.GlAccount = glAccountDto.GlAccountNumber;
                this.FormattedGlAccount = glAccountDto.FormattedGlAccount;
                this.Description = glAccountDto.Description;
            }
        }
    }
}