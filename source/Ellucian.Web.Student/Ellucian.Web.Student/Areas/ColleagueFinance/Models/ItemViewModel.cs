﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a Ledger Entry Item
    /// </summary>
    public class ItemViewModel : DocumentItem
    {
        /// <summary>
        /// GL Account description
        /// </summary>
        public string GlAccountDescription { get; set; }

        /// <summary>
        /// GL account number
        /// </summary>
        public string GlAccount { get; set; }

        /// <summary>
        /// GL account formatted for display
        /// </summary>
        public string FormattedGlAccount { get; set; }

        /// <summary>
        /// Project Number
        /// </summary>
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Project line item code
        /// </summary>
        public string ProjectLineItemCode { get; set; }

        /// <summary>
        /// Item description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Item debit amount
        /// </summary>
        public decimal? Debit { get; set; }

        /// <summary>
        /// Item debit display amount
        /// </summary>
        public string DebitDisplay
        {
            get
            {
                var display = string.Empty;
                if (Debit.HasValue)
                {
                    display = Debit.Value.ToString("C");
                }
                return display;
            }
        }

        /// <summary>
        /// Item credit amount
        /// </summary>
        public decimal? Credit { get; set; }

        /// <summary>
        /// Item credit display amount
        /// </summary>
        public string CreditDisplay
        {
            get
            {
                var display = string.Empty;
                if (Credit.HasValue)
                {
                    display = Credit.Value.ToString("C");
                }
                return display;
            }
        }


        /// <summary>
        /// Create a new journal entry item
        /// </summary>
        /// <param name="journalEntryItemDto">Journal Entry Item DTO</param>
        public ItemViewModel(JournalEntryItem journalEntryItemDto)
        {
            this.GlAccountDescription = journalEntryItemDto.GlAccountDescription ?? "";
            // Limit the GL number description to 60.
            if (this.GlAccountDescription.Length > 60)
            {
                this.GlAccountDescription = this.GlAccountDescription.Substring(0, 60);
            }
            this.GlAccount = journalEntryItemDto.GlAccount;
            this.FormattedGlAccount = journalEntryItemDto.FormattedGlAccount;
            this.ProjectNumber = journalEntryItemDto.ProjectNumber;
            this.ProjectLineItemCode = journalEntryItemDto.ProjectLineItemCode;
            this.Description = journalEntryItemDto.Description;
            this.Debit = journalEntryItemDto.Debit;
            this.Credit = journalEntryItemDto.Credit;
            
        }
    }
}