﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    public class CostCenterAccountSequenceViewModel
    {
        /// <summary>
        /// A GL account that can be sequenced by its GL account number
        /// </summary>
        public CostCenterGlAccountViewModel NonPooledAccount { get; private set; }

        /// <summary>
        /// A pooled account that can be sequenced by its Umbrella's GL account number
        /// </summary>
        public GlBudgetPoolViewModel PooledAccount { get; private set; }

        /// <summary>
        /// A flag to indicate if the account is a pooled account
        /// </summary>
        public bool IsPooledAccount { get; private set; }

        /// <summary>
        /// The GL account that is used for sequencing
        /// </summary>
        public string Id { get; private set; }

        public CostCenterAccountSequenceViewModel(CostCenterGlAccountViewModel glAccount)
        {
            if(glAccount == null)
            {
                throw new ArgumentNullException("glAccount");
            }

            this.NonPooledAccount = glAccount;
            this.Id = glAccount.GlAccount;
            this.IsPooledAccount = false;
        }

        public CostCenterAccountSequenceViewModel(GlBudgetPoolViewModel pool)
        {
            if (pool == null)
            {
                throw new ArgumentNullException("pool");
            }
            if (pool.Umbrella == null)
            {
                throw new ArgumentNullException("pool.Umbrella");
            }

            this.PooledAccount = pool;
            this.Id = pool.Umbrella.GlAccount;
            this.IsPooledAccount = true;
        }
    }
}