﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    ///  This class represents a GL distribution within a blanket purchase order
    /// </summary>
    public class BlanketPurchaseOrderGlDistributionViewModel : DocumentItem
    {
        /// <summary>
        /// This is the GL account for the GL distribution
        /// </summary>
        public string GlAccount { get; set; }

        /// <summary>
        /// This is the GL account for the GL distribution formatted for display
        /// </summary>
        public string FormattedGlAccount { get; set; }

        /// <summary>
        /// GL Account description
        /// </summary>
        public string GlAccountDescription { get; set; }

        /// <summary>
        /// This is the project number for the GL distribution
        /// </summary>
        public string ProjectNumber { get; set; }

        /// <summary>
        /// This is the project line item item code for the GL distribution
        /// </summary>
        public string ProjectLineItemCode { get; set; }

        /// <summary>
        /// This is the private encumbered amount for the GL distribution
        /// </summary>
        private decimal encumberedAmount;

        /// <summary>
        /// This is the GL encumbered amount for the GL distribution
        /// </summary>
        public string EncumberedAmount { get { return this.encumberedAmount.ToString("C"); } }

        private decimal expensedAmount;

        /// <summary>
        /// This is the GL expensed amount for the GL distribution
        /// </summary>
        public string ExpensedAmount { get { return this.expensedAmount.ToString("C"); } }

        /// <summary>
        /// The remaining amount is the difference between the encumbered and expense amounts.
        /// </summary>
        public string RemainingAmount { get { return (this.encumberedAmount - this.expensedAmount).ToString("C"); } }

        /// <summary>
        /// Create a new BPO GL Distribution view model.
        /// </summary>
        /// <param name="glDistibutionDto">GL Distribution DTO</param>
        public BlanketPurchaseOrderGlDistributionViewModel(BlanketPurchaseOrderGlDistribution glDistibutionDto)
        {
            this.GlAccountDescription = glDistibutionDto.Description;
            // Limit the GL number description to 60.
            if (this.GlAccountDescription.Length > 60)
            {
                this.GlAccountDescription = this.GlAccountDescription.Substring(0, 60);
            }
            this.GlAccount = glDistibutionDto.GlAccount;
            this.FormattedGlAccount = glDistibutionDto.FormattedGlAccount;
            this.ProjectNumber = glDistibutionDto.ProjectNumber;
            this.ProjectLineItemCode = glDistibutionDto.ProjectLineItemCode;
            this.encumberedAmount = glDistibutionDto.EncumberedAmount;
            this.expensedAmount = glDistibutionDto.ExpensedAmount;
        }
    }
}