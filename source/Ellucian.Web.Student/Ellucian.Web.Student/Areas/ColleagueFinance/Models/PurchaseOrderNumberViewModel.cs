﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This is a purchase order number document
    /// </summary>
    public class PurchaseOrderNumberViewModel : PurchasingDocumentNumberViewModel
    {
        /// <summary>
        /// Purchase order internal ID
        /// </summary>
        public override string Id { get; set; }

        /// <summary>
        /// Purchase order number that the document is known by
        /// </summary>
        public override string Number { get; set; }

        /// <summary>
        /// Create a new purchase number view model
        /// </summary>
        /// <param name="id">purchase order ID</param>
        public PurchaseOrderNumberViewModel(string id) : base(id)
        {
            this.Id = id;
        }
    }
}
