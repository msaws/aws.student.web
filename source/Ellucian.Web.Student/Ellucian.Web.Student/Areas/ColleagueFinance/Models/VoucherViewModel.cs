﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
	/// <summary>
	/// This class represents a Voucher document.
	/// </summary>
	public class VoucherViewModel : AccountsPayablePurchasingDocumentViewModel
	{
		/// <summary>
		/// Voucher due date
		/// </summary>
		public string DueDate { get; set; }

		/// <summary>
		/// Voucher invoice number
		/// </summary>
		public string InvoiceNumber { get; set; }
		
		/// <summary>
		/// Voucher invoice date
		/// </summary>
		public string InvoiceDate { get; set; }

		/// <summary>
		/// Voucher check number
		/// </summary>
		public string CheckNumber { get; set; }

		/// <summary>
		/// Voucher check date
		/// </summary>
		public string CheckDate { get; set; }

		private readonly string purchaseOrderId;

		private readonly string blanketPurchaseOrderId;

		private readonly string recurringVoucherId;

		/// <summary>
		/// Return the document ID associated to this voucher
		/// </summary>
		public string AssociatedDocument
		{
			get
			{
				string document = "";
				if (!string.IsNullOrEmpty(purchaseOrderId))
				{
					document = purchaseOrderId;
				}

				if (!string.IsNullOrEmpty(blanketPurchaseOrderId))
				{
					document = blanketPurchaseOrderId;
				}

				if (!string.IsNullOrEmpty(recurringVoucherId))
				{
					document = recurringVoucherId;
				}

				return document;
			}
		}

		/// <summary>
		/// Reference number for the associated PO, BPO, or RCV.
		/// </summary>
		public string AssociatedDocumentReferenceNumber { get; set; }

		/// <summary>
		/// Determine the type of the associated document
		/// </summary>
		public FinanceDocumentType AssociatedDocumentType
		{
			get
			{
				FinanceDocumentType documentType = FinanceDocumentType.NoValue;
				if (!string.IsNullOrEmpty(purchaseOrderId))
				{
					documentType = FinanceDocumentType.PurchaseOrder;
				}

				if (!string.IsNullOrEmpty(blanketPurchaseOrderId))
				{
					documentType = FinanceDocumentType.BlanketPurchaseOrder;
				}

				if (!string.IsNullOrEmpty(recurringVoucherId))
				{
					documentType = FinanceDocumentType.RecurringVoucher;
				}

				return documentType;
			}
		}

		/// <summary>
		/// Is the associated document a purchase order?
		/// </summary>
		public bool IsAssociatedDocumentPurchaseOrder { get { return this.AssociatedDocumentType == FinanceDocumentType.PurchaseOrder; } }

		/// <summary>
		/// Is the associated document a blanket purchase order?
		/// </summary>
		public bool IsAssociatedDocumentBlanketPurchaseOrder { get { return this.AssociatedDocumentType == FinanceDocumentType.BlanketPurchaseOrder; } }

        /// <summary>
        /// Is the associated document a recurring voucher?
        /// </summary>
        public bool IsAssociatedDocumentRecurringVoucher { get { return this.AssociatedDocumentType == FinanceDocumentType.RecurringVoucher; } }

		/// <summary>
		/// Determines if the associated document for this voucher is linkable.
		/// 
		/// This property will eventually be phased out as we add the capability
		/// to view more documents.
		/// </summary>
		public bool IsAssociatedDocumentLinkable
		{
			get
			{
				if (!string.IsNullOrEmpty(purchaseOrderId) ||
                    !string.IsNullOrEmpty(blanketPurchaseOrderId) ||
                    !string.IsNullOrEmpty(recurringVoucherId))
				{
					return true;
				}
                else
                {
                    return false;
                }
			}
		}

		/// <summary>
		/// Voucher status
		/// </summary>
		public VoucherStatus Status { get; set; }

		/// <summary>
		/// Return the status description
		/// </summary>
		public string StatusDescription
		{
			get
			{
				string description = "";
				switch (Status)
				{
					case VoucherStatus.Cancelled:
						description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "VoucherStatusCancelled");
						break;
					case VoucherStatus.InProgress:
						description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "VoucherStatusInProgress");
						break;
					case VoucherStatus.NotApproved:
						description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "VoucherStatusNotApproved");
						break;
					case VoucherStatus.Outstanding:
						description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "VoucherStatusOutstanding");
						break;
					case VoucherStatus.Paid:
						description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "VoucherStatusPaid");
						break;
					case VoucherStatus.Reconciled:
						description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "VoucherStatusReconciled");
						break;
					case VoucherStatus.Voided:
						description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "VoucherStatusVoided");
						break;
				}

				return description;
			}
		}

		/// <summary>
		/// Create a new Voucher. We forward the voucher ID  to the base class as
		/// the document number so we can reliably determine the document type.
		/// </summary>
		/// <param name="voucherDto">Voucher DTO</param>
		public VoucherViewModel(Voucher2 voucherDto)
			:base(voucherDto.VoucherId, voucherDto.VoucherId, voucherDto.Date)
		{
			this.DueDate = voucherDto.DueDate.HasValue ? voucherDto.DueDate.Value.ToShortDateString() : "";
			this.InvoiceNumber = voucherDto.InvoiceNumber;
			this.InvoiceDate = voucherDto.InvoiceDate.HasValue ? voucherDto.InvoiceDate.Value.ToShortDateString() : "";
			this.CheckNumber = voucherDto.CheckNumber;
			this.CheckDate = voucherDto.CheckDate.HasValue ? voucherDto.CheckDate.Value.ToShortDateString() : "";
			this.purchaseOrderId = voucherDto.PurchaseOrderId;
			this.blanketPurchaseOrderId = voucherDto.BlanketPurchaseOrderId;
			this.recurringVoucherId = voucherDto.RecurringVoucherId;
			this.Status = voucherDto.Status;

			// These properties belong to the base class, but are initialized at the document level
			this.ApType = voucherDto.ApType;
			this.Amount = voucherDto.Amount.ToString("C");
			this.CurrencyCode = voucherDto.CurrencyCode;
			this.MaintenanceDate = voucherDto.MaintenanceDate.HasValue ? voucherDto.MaintenanceDate.Value.ToShortDateString() : "";
			this.Comments = voucherDto.Comments;
			this.VendorId = voucherDto.VendorId;
			this.VendorName = voucherDto.VendorName ?? "";

			// Initialize the approvers and line items lists by calling the set method on the base class
			this.SetApprovers(voucherDto.Approvers);
			this.SetLineItems(voucherDto.LineItems);
		}
	}
}