﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This is a Requisition document
    /// </summary>
    public class RequisitionViewModel : AccountsPayablePurchasingDocumentViewModel
    {
        /// <summary>
        /// Requisition status
        /// </summary>
        public RequisitionStatus Status { get; set; }

        /// <summary>
        /// Requisition status description
        /// </summary>
        public string StatusDescription
        {
            get
            {
                string description = "";
                switch (Status)
                {
                    case RequisitionStatus.InProgress:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RequisitionStatusInProgress");
                        break;
                    case RequisitionStatus.NotApproved:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RequisitionStatusNotApproved");
                        break;
                    case RequisitionStatus.Outstanding:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RequisitionStatusOutstanding");
                        break;
                    case RequisitionStatus.PoCreated:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RequisitionStatusPoCreated");
                        break;
                }
                return description;
            }
        }

        /// <summary>
        /// Requisition status date
        /// </summary>
        public string StatusDate { get; set; }

        /// <summary>
        /// Requisition desired date
        /// </summary>
        public string DesiredDate { get; set; }

        /// <summary>
        /// Requisition initiator name
        /// </summary>
        public string InitiatorName { get; set; }

        /// <summary>
        /// Requisition requestor name
        /// </summary>
        public string RequestorName { get; set; }

        /// <summary>
        /// Requisition internal comments
        /// </summary>
        public string InternalComments { get; set; }

        /// <summary>
        /// List of purchase orders associated with the requisition
        /// </summary>
        public List<PurchaseOrderNumberViewModel> PurchaseOrders { get; set; }

        /// <summary>
        /// Blanket purchase order ID associated with the requisition
        /// </summary>
        public string BlanketPurchaseOrderId { get; set; }

        /// <summary>
        /// Blanket purchase order number associated with the requisition
        /// </summary>
        public string BlanketPurchaseOrderNumber { get; set; }


        /// <summary>
        /// Create a new requisition view model
        /// </summary>
        /// <param name="requisitionDto">requisition DTO</param>
        public RequisitionViewModel(Requisition requisitionDto)
            : base(requisitionDto.Id, requisitionDto.Number, requisitionDto.Date)
        {
            this.Status = requisitionDto.Status;
            this.StatusDate = requisitionDto.StatusDate.ToShortDateString();
            this.DesiredDate = requisitionDto.DesiredDate.HasValue ? requisitionDto.DesiredDate.Value.ToShortDateString() : "";
            this.InitiatorName = requisitionDto.InitiatorName ?? "";
            this.RequestorName = requisitionDto.RequestorName ?? "";
            this.InternalComments = requisitionDto.InternalComments;

            // These properties belong to the base class, but are initialized at the document level
            this.Amount = requisitionDto.Amount.ToString("C");
            this.ApType = requisitionDto.ApType;

            this.BlanketPurchaseOrderId = requisitionDto.BlanketPurchaseOrder;
            this.Comments = requisitionDto.Comments;
            this.CurrencyCode = requisitionDto.CurrencyCode;
            this.MaintenanceDate = requisitionDto.MaintenanceDate.HasValue ? requisitionDto.MaintenanceDate.Value.ToShortDateString() : "";
            this.VendorId = requisitionDto.VendorId;
            this.VendorName = requisitionDto.VendorName ?? "";
            
            // Initialize the approver and line item lists by calling the set methods on the base class
            this.SetApprovers(requisitionDto.Approvers);
            this.SetLineItems(requisitionDto.LineItems);

            // Convert the list of purchase order IDs into a list of purchase order number view models
            this.PurchaseOrders = new List<PurchaseOrderNumberViewModel>();
            if (requisitionDto.PurchaseOrders != null)
            {
                foreach (var po in requisitionDto.PurchaseOrders)
                {
                    var purchaseOrder = new PurchaseOrderNumberViewModel(po);
                    this.PurchaseOrders.Add(purchaseOrder);
                }
            }

        }
    }
}