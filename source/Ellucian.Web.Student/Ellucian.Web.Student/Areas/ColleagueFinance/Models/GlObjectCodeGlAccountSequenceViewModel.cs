﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A wrapper to support ordering General Ledger Object Code Accounts
    /// </summary>
    public class GlObjectCodeGlAccountSequenceViewModel
    {
        /// <summary>
        /// The formatted General Ledger Account number for this sequenced General Ledger Account
        /// </summary>
        public string FormattedGlAccount
        {
            get
            {
                return this.PrimaryGlAccount.FormattedGlAccount;
            }
        }

        /// <summary>
        /// A boolean to determine if this sequenced General Ledger Account is a pool or not
        /// </summary>
        public bool IsPooledAccount { get; private set; }

        /// <summary>
        /// Account on which to be sequenced (could be the umbrella)
        /// </summary>
        public GlObjectCodeGlAccountViewModel PrimaryGlAccount { get; private set; }

        /// <summary>
        /// A boolean to determine if the primary account is visible (always true for Non-Pooled Accounts)
        /// </summary>
        public bool IsUmbrellaVisible { get; private set; }

        /// <summary>
        /// List of poolee accounts (for a sequenced budget pool)
        /// </summary>
        public List<GlObjectCodeGlAccountViewModel> Poolees { get; private set; }

        public GlObjectCodeGlAccountSequenceViewModel(GlObjectCodeGlAccountViewModel nonPooledAccount)
        {
            if(nonPooledAccount != null)
            {
                this.IsPooledAccount = false;
                this.IsUmbrellaVisible = true;
                this.PrimaryGlAccount = nonPooledAccount;

                // No poolees for non-pooled accounts
                this.Poolees = new List<GlObjectCodeGlAccountViewModel>();
            }
        }

        public GlObjectCodeGlAccountSequenceViewModel(GlObjectCodeBudgetPoolViewModel pooledAccount)
        {
            if(pooledAccount != null)
            {
                this.IsPooledAccount = true;
                this.IsUmbrellaVisible = pooledAccount.IsUmbrellaVisible;
                this.PrimaryGlAccount = pooledAccount.Umbrella;

                this.Poolees = pooledAccount.Poolees;
            }
        }
    }
}