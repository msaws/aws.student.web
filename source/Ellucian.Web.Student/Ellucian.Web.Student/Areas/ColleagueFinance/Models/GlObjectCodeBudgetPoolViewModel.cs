﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A view model that represents a budget pool for the Object View
    /// </summary>
    public class GlObjectCodeBudgetPoolViewModel
    {
        /// <summary>
        /// Boolean to determine if the Umbrella is visible
        /// </summary>
        public bool IsUmbrellaVisible { get; private set; }

        /// <summary>
        /// The umbrella account for the budget pool
        /// </summary>
        public GlObjectCodeGlAccountViewModel Umbrella { get; private set; }

        /// <summary>
        /// List of poolee accounts for the budget pool
        /// </summary>
        public List<GlObjectCodeGlAccountViewModel> Poolees { get; private set; }

        public GlObjectCodeBudgetPoolViewModel(GlObjectCodeBudgetPool budgetPoolDto, GlClass glClass, bool includeFinancialHealthIndicator = false)
        {
            if(budgetPoolDto != null)
            {
                this.IsUmbrellaVisible = budgetPoolDto.IsUmbrellaVisible;
                this.Umbrella = new GlObjectCodeGlAccountViewModel(budgetPoolDto.Umbrella, glClass, includeFinancialHealthIndicator && this.IsUmbrellaVisible);
                this.Poolees = new List<GlObjectCodeGlAccountViewModel>();

                if (!this.IsUmbrellaVisible)
                {
                    this.Umbrella.ZeroOutBudgetActualsEncumbrances();
                }

                if(budgetPoolDto.Poolees != null)
                {
                    foreach (var poolee in budgetPoolDto.Poolees)
                    {
                        if(poolee != null)
                        {
                            if (this.IsUmbrellaVisible || !poolee.GlAccountNumber.Equals(this.Umbrella.GlAccountNumber, StringComparison.OrdinalIgnoreCase))
                            this.Poolees.Add(new GlObjectCodeGlAccountViewModel(poolee, glClass));
                        }
                    }
                }
                
                this.Poolees = this.Poolees.OrderBy(a => a.FormattedGlAccount).ToList();
            }
        }
    }
}