﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.ColleagueFinance.Controllers;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This is a Journal Entry
    /// </summary>
    public class JournalEntryViewModel : LedgerEntryDocumentViewModel
    {
        /// <summary>
        /// Journal Entry status
        /// </summary>
        public JournalEntryStatus Status { get; set; }

        /// <summary>
        /// The journal entry status description
        /// </summary>
        public string StatusDescription
        {
            get
            {
                string description = "";
                switch (Status)
                {
                    case JournalEntryStatus.Complete:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryStatusComplete");
                        break;
                    case JournalEntryStatus.NotApproved:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryStatusNotApproved");
                        break;
                    case JournalEntryStatus.Unfinished:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryStatusUnfinished");
                        break;
                }
                return description;
            }
        }

        /// <summary>
        /// The journal entry type
        /// </summary>
        public JournalEntryType Type { get; set; }

        /// <summary>
        /// The journal entry type description
        /// </summary>
        public string TypeDescription
        {
            get
            {
                string type = "";
                switch (Type)
                {
                    case JournalEntryType.General:
                        type = GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryTypeGeneral");
                        break;
                    case JournalEntryType.OpeningBalance:
                        type = GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryTypeOpeningBalance");
                        break;
                }
                return type;
            }
        }

        /// <summary>
        /// The journal entry author
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// The journal entry total debits
        /// </summary>
        public string TotalDebits { get; set; }

        /// <summary>
        /// The journal entry total credits
        /// </summary>
        public string TotalCredits { get; set; }

        /// <summary>
        /// Indicates the journal entry was automatically reversed
        /// </summary>
        public bool AutomaticReversal { get; set; }

        /// <summary>
        /// Contains Yes or No as to whether the journal entry has an automatic reversal
        /// </summary>
        public string AutomaticReversalDescription
        {
            get
            {
                string desc = "No";
                if (AutomaticReversal)
                {
                    desc = "Yes";
                }
                return desc;
            }
        }
        /// <summary>
        /// Create a new journal entry
        /// </summary>
        /// <param name="journalEntryDto">The Journal Entry DTO</param>
        public JournalEntryViewModel(JournalEntry journalEntryDto)
            : base(journalEntryDto.Id, journalEntryDto.Id, journalEntryDto.Date, journalEntryDto.EnteredDate, journalEntryDto.EnteredByName)
        {
            this.Comments = journalEntryDto.Comments;
            this.TotalCredits = journalEntryDto.TotalCredits.ToString("C");
            this.TotalDebits = journalEntryDto.TotalDebits.ToString("C");
            this.Status = journalEntryDto.Status;
            this.Type = journalEntryDto.Type;
            this.Author = journalEntryDto.Author;
            this.AutomaticReversal = journalEntryDto.AutomaticReversal;

            // Initialize the approvers list by calling the set method on the base class
            this.SetApprovers(journalEntryDto.Approvers);

            // Initialize the items list by calling the set method on the base class
            this.SetItems(journalEntryDto.Items);
        }
    }
}