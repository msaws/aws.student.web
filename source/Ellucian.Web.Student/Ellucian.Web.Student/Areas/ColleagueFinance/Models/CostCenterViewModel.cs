﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using System.Linq;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents data that is displayed or used in the markup.
    /// </summary>
    public class CostCenterViewModel : BaseCostCenterTotalsViewModel
    {
        /// <summary>
        /// This is the cost center ID.
        /// </summary>
        public string CostCenterId { get; set; }

        /// <summary>
        /// This is the name to be displayed in the markup.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID of the UNIT component.
        /// </summary>
        public string UnitId { get; set; }

        /// <summary>
        /// List of cost center subtotals (expense).
        /// </summary>
        public List<CostCenterSubtotalViewModel> CostCenterExpenseSubtotals { get; set; }

        /// <summary>
        /// List of cost center subtotals (revenue).
        /// </summary>
        public List<CostCenterSubtotalViewModel> CostCenterRevenueSubtotals { get; set; }

        /// <summary>
        /// Boolean to indicate if the Cost Center has any revenue accounts.
        /// </summary>
        public bool HasRevenue { get; private set; }

        /// <summary>
        /// Boolean to indicate if the Cost Center has any expense accounts.
        /// </summary>
        public bool HasExpense { get; private set; }

        /// <summary>
        /// Initializes the cost center view model using the DTO as an argument.
        /// </summary>
        /// <param name="costCenterDto">A Cost Center DTO.</param>
        public CostCenterViewModel(CostCenter costCenterDto)
            :base(costCenterDto)
        {
            if (costCenterDto != null)
            {
                this.CostCenterId = costCenterDto.Id;
                this.Name = costCenterDto.Name;
                this.UnitId = costCenterDto.UnitId;
                this.HasExpense = false;
                this.HasRevenue = false;

                // Initialize the GL accounts
                this.CostCenterExpenseSubtotals = new List<CostCenterSubtotalViewModel>();
                this.CostCenterRevenueSubtotals = new List<CostCenterSubtotalViewModel>();
                if (costCenterDto.CostCenterSubtotals != null)
                {
                    foreach (var subtotal in costCenterDto.CostCenterSubtotals)
                    {
                        if (subtotal != null)
                        {
                            if (subtotal.GlClass == GlClass.Expense)
                            {
                                this.CostCenterExpenseSubtotals.Add(new CostCenterSubtotalViewModel(subtotal));
                                this.HasExpense = true;
                            }
                            else if (subtotal.GlClass == GlClass.Revenue)
                            {
                                this.CostCenterRevenueSubtotals.Add(new CostCenterSubtotalViewModel(subtotal));
                                this.HasRevenue = true;
                            }
                        }
                    }
                }

                this.CostCenterExpenseSubtotals = this.CostCenterExpenseSubtotals.OrderBy(a => a.Name).ToList();
                this.CostCenterRevenueSubtotals = this.CostCenterRevenueSubtotals.OrderBy(b => b.Name).ToList();
            }
        }
    }
}