﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a recurring voucher document
    /// </summary>
    public class RecurringVoucherViewModel : AccountsPayablePurchasingDocumentViewModel
    {
        /// <summary>
        /// Recurring Voucher status date
        /// </summary>
        public string StatusDate { get; set; }

        /// <summary>
        /// Recurring Voucher status
        /// </summary>
        public RecurringVoucherStatus Status { get; set; }

        /// <summary>
        /// Return the status description
        /// </summary>
        public string StatusDescription
        {
            get
            {
                string description = "";
                switch (Status)
                {
                    case RecurringVoucherStatus.Cancelled:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RecurringVoucherStatusCancelled");
                        break;
                    case RecurringVoucherStatus.Closed:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RecurringVoucherStatusInProgress");
                        break;
                    case RecurringVoucherStatus.NotApproved:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RecurringVoucherStatusNotApproved");
                        break;
                    case RecurringVoucherStatus.Outstanding:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RecurringVoucherStatusOutstanding");
                        break;
                    case RecurringVoucherStatus.Voided:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RecurringVoucherStatusVoided");
                        break;
                }

                return description;
            }
        }

        /// <summary>
        /// Recurring Voucher invoice number
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Recurring Voucher invoice date
        /// </summary>
        public string InvoiceDate { get; set; }

        /// <summary>
        /// Schedules associated to this recurring voucher.
        /// </summary>
        public List<RecurringVoucherScheduleViewModel> Schedules { get; set; }

        /// <summary>
        /// Calculates the total schedule amount for this recurring voucher.
        /// </summary>
        public string TotalScheduleAmount { get { return this.Schedules.Sum(x => x.Amount).ToString("C"); } }

        /// <summary>
        /// Calculates the total tax amount for this recurring voucher.
        /// </summary>
        public string TotalScheduleTaxAmount { get { return this.Schedules.Sum(x => x.TaxAmount).ToString("C"); } }

        /// <summary>
        /// The total amount in local currency
        /// </summary>
        public string TotalScheduleAmountInLocalCurrency { get; set; }

        /// <summary>
        /// The total tax amount in local currency
        /// </summary>
        public string TotalScheduleTaxAmountInLocalCurrency { get; set; }

        /// <summary>
        /// Create a new recurring voucher. We forward the recurring voucher ID  to the base
        /// class as the document number so we can reliably determine the document type.
        /// </summary>
        /// <param name="recurringVoucherDto">Recurring Voucher DTO</param>
        public RecurringVoucherViewModel(RecurringVoucher recurringVoucherDto)
            : base(recurringVoucherDto.RecurringVoucherId, recurringVoucherDto.RecurringVoucherId, recurringVoucherDto.Date)
        {
            this.InvoiceNumber = recurringVoucherDto.InvoiceNumber;
            this.InvoiceDate = recurringVoucherDto.InvoiceDate.ToShortDateString();
            this.Status = recurringVoucherDto.Status;
            this.StatusDate = recurringVoucherDto.StatusDate.ToShortDateString();

            // These properties belong to the base class, but are initialized at the document level
            this.ApType = recurringVoucherDto.ApType;
            this.Amount = recurringVoucherDto.Amount.ToString("C");
            this.MaintenanceDate = recurringVoucherDto.MaintenanceDate.HasValue ? recurringVoucherDto.MaintenanceDate.Value.ToShortDateString() : "";
            this.Comments = recurringVoucherDto.Comments;
            this.VendorId = recurringVoucherDto.VendorId;
            this.VendorName = recurringVoucherDto.VendorName ?? "";
            this.CurrencyCode = recurringVoucherDto.CurrencyCode;
            this.TotalScheduleAmountInLocalCurrency = recurringVoucherDto.TotalScheduleAmountInLocalCurrency.HasValue ? recurringVoucherDto.TotalScheduleAmountInLocalCurrency.Value.ToString("C") : null;
            this.TotalScheduleTaxAmountInLocalCurrency = recurringVoucherDto.TotalScheduleTaxAmountInLocalCurrency.HasValue ? recurringVoucherDto.TotalScheduleTaxAmountInLocalCurrency.Value.ToString("C") : null;

            // Initialize the approvers list by calling the set method on the base class
            this.SetApprovers(recurringVoucherDto.Approvers);

            // Add the list of schedules to the recurring voucher view model
            this.Schedules = new List<RecurringVoucherScheduleViewModel>();
            if (recurringVoucherDto.Schedules != null)
            {
                int id = 1;
                foreach (var schedule in recurringVoucherDto.Schedules)
                {
                    if (schedule != null)
                    {
                        var scheduleVM = new RecurringVoucherScheduleViewModel(schedule);

                        // Set the item ID so we can uniquely identify the item.
                        scheduleVM.DocumentItemId = "document-line-item" + id.ToString();
                        this.Schedules.Add(scheduleVM);
                        id++;
                    }
                }
            }
        }
    }
}