﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A GL account activity detail view model.
    /// </summary>
    public class GlAccountActivityDetailViewModel
    {
        /// <summary>
        /// The GL account number.
        /// </summary>
        public string GlAccount { get; set; }

        /// <summary>
        /// The GL account formatted for display.
        /// </summary>
        public string FormattedGlAccount { get; set; }

        /// <summary>
        /// The GL account description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The string representation of the total budget for the GL account.
        /// </summary>
        public string TotalBudget { get { return (totalBudget + TotalBudgetMemos).ToString("C"); } }
        protected decimal totalBudget;

        /// <summary>
        /// The budget pending posting for this GL account.
        /// </summary>
        public decimal TotalBudgetMemos { get; private set; }

        /// <summary>
        /// Return the total budget memo amount in formatted mode for display.
        /// </summary>
        public string TotalBudgetMemosDisplay { get { return TotalBudgetMemos.ToString("C"); } }

        /// <summary>
        /// The string representation of the total expenses for the cost center.
        /// </summary>
        public string TotalEncumbrances { get { return totalEncumbrances.ToString("C"); } }
        protected decimal totalEncumbrances;

        /// <summary>
        /// The string representation of the total actuals for the GL account:
        /// total actuals posted plus actuals pending posting.
        /// </summary>
        public string TotalActuals { get { return (totalActuals + TotalActualsMemos + EstimatedOpeningBalance + ClosingYearAmount).ToString("C"); } }
        protected decimal totalActuals;

        /// <summary>
        /// The actuals pending posting for this GL account.
        /// </summary>
        public decimal TotalActualsMemos { get; private set; }

        /// <summary>
        /// Return the total actuals memo amount in formatted mode for display.
        /// </summary>
        public string TotalActualsMemosDisplay { get { return TotalActualsMemos.ToString("C"); } }

        /// <summary>
        /// The estimated opening balance for a GL account in an open year that follows another open year.
        /// </summary>
        public decimal EstimatedOpeningBalance { get; private set; }

        /// <summary>
        /// The estimated opening balance as a formatted string.
        /// </summary>
        public string EstimatedOpeningBalanceDisplay { get { return EstimatedOpeningBalance.ToString("C"); } }

        /// <summary>
        /// The closing year amount for a fund balance GL account in a closed year.
        /// </summary>
        public decimal ClosingYearAmount { get; private set; }

        /// <summary>
        /// The closing year amount as a formatted string.
        /// </summary>
        public string ClosingYearAmountDisplay { get { return ClosingYearAmount.ToString("C"); } }

        /// <summary>
        /// List of budget transactions.
        /// </summary>
        public List<GlTransactionViewModel> BudgetTransactions;

        /// <summary>
        /// List of encumbrance transactions.
        /// </summary>
        public List<GlTransactionViewModel> EncumbranceTransactions;

        /// <summary>
        /// List of actual transactions.
        /// </summary>
        public List<GlTransactionViewModel> ActualTransactions;

        /// <summary>
        /// The Unit Id of the cost center
        /// </summary>
        public string UnitId { get; set; }

        /// <summary>
        /// The Name of the cost center Unit Id
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Cost Center Id
        /// </summary>
        public string CostCenterId { get; set; }

        /// <summary>
        /// Initializes the GL account view model.
        /// </summary>
        /// <param name="glAccountActivityDetailDto">A GL account DTO.</param>
        public GlAccountActivityDetailViewModel(GlAccountActivityDetail glAccountActivityDetailDto)
        {
            if (glAccountActivityDetailDto != null)
            {
                this.GlAccount = glAccountActivityDetailDto.GlAccountNumber;
                this.FormattedGlAccount = glAccountActivityDetailDto.FormattedGlAccount;
                this.Description = glAccountActivityDetailDto.Description;
                this.totalBudget = glAccountActivityDetailDto.Budget;
                this.TotalBudgetMemos = glAccountActivityDetailDto.MemoBudget;
                this.totalActuals = glAccountActivityDetailDto.Actuals;
                this.TotalActualsMemos = glAccountActivityDetailDto.MemoActuals;
                this.totalEncumbrances = glAccountActivityDetailDto.Encumbrances;
                this.EstimatedOpeningBalance = glAccountActivityDetailDto.EstimatedOpeningBalance;
                this.ClosingYearAmount = glAccountActivityDetailDto.ClosingYearAmount;
                this.UnitId = glAccountActivityDetailDto.UnitId;
                this.Name = glAccountActivityDetailDto.Name;
                this.CostCenterId = glAccountActivityDetailDto.CostCenterId;

                this.BudgetTransactions = new List<GlTransactionViewModel>();
                this.ActualTransactions = new List<GlTransactionViewModel>();
                this.EncumbranceTransactions = new List<GlTransactionViewModel>();

                // Add the budget transactions to the view model.
                if (glAccountActivityDetailDto.BudgetTransactions != null)
                {
                    foreach (var budgetTransaction in glAccountActivityDetailDto.BudgetTransactions)
                    {
                        this.BudgetTransactions.Add(new GlTransactionViewModel(budgetTransaction));
                    }
                }

                // Add the encumbrance transactions to the view model.
                if (glAccountActivityDetailDto.EncumbranceTransactions != null)
                {
                    foreach (var encumbranceTransaction in glAccountActivityDetailDto.EncumbranceTransactions)
                    {
                        this.EncumbranceTransactions.Add(new GlTransactionViewModel(encumbranceTransaction));
                    }
                }

                // Add the actuals transactions to the view model.
                if (glAccountActivityDetailDto.ActualsTransactions != null)
                {
                    foreach (var actualTransaction in glAccountActivityDetailDto.ActualsTransactions)
                    {
                        this.ActualTransactions.Add(new GlTransactionViewModel(actualTransaction));
                    }
                }
            }
        }
    }
}