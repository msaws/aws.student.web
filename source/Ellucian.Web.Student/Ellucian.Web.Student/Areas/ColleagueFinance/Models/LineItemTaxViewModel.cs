﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a tax distribution.
    /// </summary>
    public class LineItemTaxViewModel
    {
        /// <summary>
        /// Tax code
        /// </summary>
        public string TaxCode { get; set; }

        /// <summary>
        /// Tax code description
        /// </summary>
        public string TaxCodeDescription { get; set; }

        /// <summary>
        /// Tax amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Format and display the tax amount.
        /// </summary>
        public string AmountDisplay
        {
            get
            {
                return Amount.ToString("C");
            }
        }

        /// <summary>
        /// Create a new tax object for the line item.
        /// </summary>
        /// <param name="lineItemTaxDto">Line item tax DTO</param>
        public LineItemTaxViewModel(LineItemTax lineItemTaxDto)
        {
            this.TaxCode = lineItemTaxDto.TaxCode;
            this.TaxCodeDescription = "";
            this.Amount = lineItemTaxDto.TaxAmount;
        }
    }
}