﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// Represents the list of cost centers assigned to the user that will be displayed.
    /// </summary>
    public class CostCenterSummaryViewModel
    {
        /// <summary>
        /// A list of cost center view models.
        /// </summary>
        public List<BaseCostCenterViewModel> CostCenters { get; set; }

        /// <summary>
        /// List of General Ledger fiscal years for the drop-down.
        /// </summary>
        public List<FiscalYearFilterViewModel> FiscalYears { get; set; }

        /// <summary>
        /// The current fiscal year.
        /// </summary>
        public string TodaysFiscalYear { get; set; }

        /// <summary>
        /// List of GL components for the filter.
        /// </summary>
        public List<GeneralLedgerComponentViewModel> MajorComponents { get; set; }

        /// <summary>
        /// List of available, user defined saved filters.
        /// </summary>
        public List<SavedFilter> SavedFilters { get; set; }

        /// <summary>
        /// List of available, user-defined saved filter names.
        /// </summary>
        public List<KeyValuePair<string,string>> SavedFilterNames { get; set; }

        /// <summary>
        /// Boolean to indicate if any Cost Center in the list has a revenue account.
        /// </summary>
        public bool HasAnyRevenue
        {
            get
            {
                return this.CostCenters.Any(cc => cc.HasRevenue);
            }
        }

        /// <summary>
        /// Boolean to indicate if any Cost Center in the list has an expense account.
        /// </summary>
        public bool HasAnyExpense
        {
            get
            {
                return this.CostCenters.Any(cc => cc.HasExpense);
            }
        }

        /// <summary>
        /// Initializes the cost centers model properties.
        /// </summary>
        public CostCenterSummaryViewModel(IEnumerable<CostCenter> costCenterDtos, IEnumerable<string> fiscalYears, string todaysFiscalYear)
        {
            this.CostCenters = new List<BaseCostCenterViewModel>();

            this.TodaysFiscalYear = todaysFiscalYear ?? "";

            if (costCenterDtos != null)
            {
                foreach (var costCenterDto in costCenterDtos)
                {
                    if (costCenterDto != null)
                        CostCenters.Add(new BaseCostCenterViewModel(costCenterDto));
                }
            }

            this.FiscalYears = new List<FiscalYearFilterViewModel>();
            if (fiscalYears != null)
            {
                for (int i = 0; i < fiscalYears.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(fiscalYears.ElementAt(i)))
                        FiscalYears.Add(new FiscalYearFilterViewModel(fiscalYears.ElementAt(i)));
                }
            }

            // Initialize the major components.
            this.MajorComponents = new List<GeneralLedgerComponentViewModel>();
        }

        /// <summary>
        /// Takes the supplied list of major component DTOs and adds them to the list of major component view models.
        /// </summary>
        /// <param name="majorComponentDtos"></param>
        public void SetGeneralLedgerMajorComponents(IEnumerable<GeneralLedgerComponent> majorComponentDtos)
        {
            // First, remove all view models.
            this.MajorComponents.RemoveAll(x => true);

            // Now, add the supplied components to the list.
            if (majorComponentDtos != null)
            {
                foreach (var majorComponentDto in majorComponentDtos)
                {
                    if (majorComponentDto != null)
                    {
                        this.MajorComponents.Add(new GeneralLedgerComponentViewModel(majorComponentDto));
                    }
                }
            }
        }
    }
}

