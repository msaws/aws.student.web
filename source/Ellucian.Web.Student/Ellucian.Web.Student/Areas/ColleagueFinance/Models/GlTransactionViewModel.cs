﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a GL Transaction.
    /// </summary>
    public class GlTransactionViewModel
    {
        /// <summary>
        /// Document ID for the transaction.
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// Reference number for the document.
        /// </summary>
        public string ReferenceNumber { get; set; }

        /// <summary>
        /// Source for the transaction.
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Returns the formatted transaction date.
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Transaction description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Private decimal variable to be used by the public Amount property.
        /// </summary>
        private decimal amount;

        /// <summary>
        /// Return the formatted transaction amount.
        /// </summary>
        public string Amount
        {
            get
            {
                return amount.ToString("C");
            }
        }

        /// <summary>
        /// Initialize the attributes for the GL transaction VM.
        /// </summary>
        /// <param name="glTransactionDto">GL transaction DTO</param>
        public GlTransactionViewModel(GlTransaction glTransactionDto)
        {
            this.DocumentId = glTransactionDto.DocumentId;
            this.ReferenceNumber = glTransactionDto.ReferenceNumber;
            this.Source = glTransactionDto.Source;
            this.Date = glTransactionDto.TransactionDate.ToShortDateString();
            this.Description = glTransactionDto.Description;
            this.amount = glTransactionDto.Amount;
        }
    }
}