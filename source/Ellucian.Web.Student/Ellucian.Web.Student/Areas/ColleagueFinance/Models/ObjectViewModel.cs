﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// Represents all of the data needed to render the Object View tab.
    /// </summary>
    public class ObjectViewModel
    {
        /// <summary>
        /// Contains the Object data for all object codes the user has access to.
        /// </summary>
        public ObjectData ObjectData { get; private set;}

        /// <summary>
        /// List of General Ledger fiscal years for the drop-down.
        /// </summary>
        public List<FiscalYearFilterViewModel> FiscalYears { get; private set; }

        /// <summary>
        /// The current fiscal year.
        /// </summary>
        public string TodaysFiscalYear { get; private set; }

        /// <summary>
        /// List of GL components for the filter.
        /// </summary>
        public List<GeneralLedgerComponentViewModel> MajorComponents { get; private set; }

        /// <summary>
        /// List of available, user-defined saved filter names.
        /// </summary>
        public List<KeyValuePair<string, string>> SavedFilterNames { get; private set; }

        public ObjectViewModel(IEnumerable<GlObjectCode> objectCodeDtos, IEnumerable<string> fiscalYears, string todaysFiscalYear, 
            IEnumerable<GeneralLedgerComponent> majorComponentDtos, List<KeyValuePair<string, string>> savedFilterNames)
        {
            this.TodaysFiscalYear = todaysFiscalYear ?? "";

            if(objectCodeDtos != null)
            {
                this.ObjectData = new ObjectData(objectCodeDtos);
            }

            this.FiscalYears = new List<FiscalYearFilterViewModel>();
            if (fiscalYears != null)
            {
                for (int i = 0; i < fiscalYears.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(fiscalYears.ElementAt(i)))
                        this.FiscalYears.Add(new FiscalYearFilterViewModel(fiscalYears.ElementAt(i)));
                }
            }
            this.FiscalYears = this.FiscalYears.OrderByDescending(x => true).ToList();

            
            this.MajorComponents = new List<GeneralLedgerComponentViewModel>();
            if (majorComponentDtos != null)
            {
                foreach (var majorComponentDto in majorComponentDtos)
                {
                    if (majorComponentDto != null)
                    {
                        this.MajorComponents.Add(new GeneralLedgerComponentViewModel(majorComponentDto));
                    }
                }
            }


            this.SavedFilterNames = new List<KeyValuePair<string, string>>();
            if(savedFilterNames != null)
            {
                this.SavedFilterNames = savedFilterNames.OrderBy(f => f.Value).ToList();
            }
        }
    }
}