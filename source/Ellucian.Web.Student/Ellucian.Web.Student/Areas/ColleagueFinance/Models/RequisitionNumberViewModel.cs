﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This is a Requisition document
    /// </summary>
    public class RequisitionNumberViewModel : PurchasingDocumentNumberViewModel
    {
        /// <summary>
        /// Requisition internal ID
        /// </summary>
        public override string Id { get; set; }

        /// <summary>
        /// Requisition number that the document is known by
        /// </summary>
        public override string Number { get; set; }

        /// <summary>
        /// Create a new requisition number view model
        /// </summary>
        /// <param name="id">requisition ID</param>
        public RequisitionNumberViewModel(string id) : base(id)
        {
            this.Id = id;
        }
    }
}
