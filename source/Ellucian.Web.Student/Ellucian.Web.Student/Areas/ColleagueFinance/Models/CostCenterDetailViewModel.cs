﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// View model for a single cost center.
    /// </summary>
    public class CostCenterDetailViewModel
    {
        /// <summary>
        /// Cost center for the view model.
        /// </summary>
        public CostCenterViewModel CostCenter { get; set; }

        /// <summary>
        /// List of General Ledger fiscal years for the drop-down.
        /// </summary>
        public List<FiscalYearFilterViewModel> FiscalYears { get; set; }

        /// <summary>
        /// List of GL components for the filter.
        /// </summary>
        public List<GeneralLedgerComponentViewModel> MajorComponents { get; set; }

        /// <summary>
        /// The current fiscal year.
        /// </summary>
        public string TodaysFiscalYear { get; set; }

        /// <summary>
        /// Initialize the view model.
        /// </summary>
        /// <param name="costCenterDto">Cost center DTO.</param>
        public CostCenterDetailViewModel(CostCenter costCenterDto, IEnumerable<string> fiscalYears, string todaysFiscalYear)
        {
            this.CostCenter = new CostCenterViewModel(costCenterDto);

            this.TodaysFiscalYear = todaysFiscalYear ?? "";

            this.FiscalYears = new List<FiscalYearFilterViewModel>();
            if (fiscalYears != null)
            {
                for (int i = 0; i < fiscalYears.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(fiscalYears.ElementAt(i)))
                        FiscalYears.Add(new FiscalYearFilterViewModel(fiscalYears.ElementAt(i)));
                }
            }

            // Initialize the major components.
            this.MajorComponents = new List<GeneralLedgerComponentViewModel>();
        }

        /// <summary>
        /// Takes the supplied list of major component DTOs and adds them to the list of major component view models.
        /// </summary>
        /// <param name="majorComponentDtos"></param>
        public void SetGeneralLedgerMajorComponents(IEnumerable<GeneralLedgerComponent> majorComponentDtos)
        {
            // First, remove all view models.
            this.MajorComponents.Clear();

            // Now, add the supplied components to the list.
            if (majorComponentDtos != null)
            {
                foreach (var majorComponentDto in majorComponentDtos)
                {
                    if (majorComponentDto != null)
                    {
                        this.MajorComponents.Add(new GeneralLedgerComponentViewModel(majorComponentDto));
                    }
                }
            }
        }
    }
}