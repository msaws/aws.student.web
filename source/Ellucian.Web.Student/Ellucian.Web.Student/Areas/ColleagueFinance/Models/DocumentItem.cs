﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class holds properties common to all document "items" (line items, schedules, GL distributions, etc.)
    /// </summary>
    public abstract class DocumentItem
    {
        /// <summary>
        /// Generated ID used to uniquely identify the item.
        /// </summary>
        public string DocumentItemId { get; set; }
    }
}