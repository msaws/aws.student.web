﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a Purchase Order document.
    /// </summary>
    public class PurchaseOrderViewModel : AccountsPayablePurchasingDocumentViewModel
    {
        /// <summary>
        /// Purchase order status
        /// </summary>
        public PurchaseOrderStatus Status { get; set; }

        /// <summary>
        /// Return the status description
        /// </summary>
        public string StatusDescription
        {
            get
            {
                string description = "";
                switch (Status)
                {
                    case PurchaseOrderStatus.Accepted:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusAccepted");
                        break;
                    case PurchaseOrderStatus.Backordered:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusBackordered");
                        break;
                    case PurchaseOrderStatus.Closed:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusClosed");
                        break;
                    case PurchaseOrderStatus.InProgress:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusInProgress");
                        break;
                    case PurchaseOrderStatus.Invoiced:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusInvoiced");
                        break;
                    case PurchaseOrderStatus.NotApproved:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusNotApproved");
                        break;
                    case PurchaseOrderStatus.Outstanding:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusOutstanding");
                        break;
                    case PurchaseOrderStatus.Paid:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusPaid");
                        break;
                    case PurchaseOrderStatus.Reconciled:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusReconciled");
                        break;
                    case PurchaseOrderStatus.Voided:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusVoided");
                        break;
                }

                return description;
            }
        }

        /// <summary>
        /// Purchase order status date
        /// </summary>
        public string StatusDate { get; set; }

        /// <summary>
        /// Purchase order delivery date
        /// </summary>
        public string DeliveryDate { get; set; }

        /// <summary>
        /// Purchase order initiator name
        /// </summary>
        public string InitiatorName { get; set; }

        /// <summary>
        /// Purchase order requestor name
        /// </summary>
        public string RequestorName { get; set; }

        /// <summary>
        /// Purchase order ship to code
        /// </summary>
        public string ShipToCode { get; set; }

        /// <summary>
        /// Purchase order internal comments
        /// </summary>
        public string InternalComments { get; set; }

        /// <summary>
        /// List of requisitions associated to the purchase order
        /// </summary>
        public List<RequisitionNumberViewModel> Requisitions { get; set; }

        /// <summary>
        /// List of vouchers associated to the purchase order
        /// </summary>
        public List<string> Vouchers { get; set; }

        /// <summary>
        /// Create a new purchase order.
        /// </summary>
        /// <param name="purchaseOrderDto">Purchase order DTO</param>
        public PurchaseOrderViewModel(PurchaseOrder purchaseOrderDto)
            :base(purchaseOrderDto.Id, purchaseOrderDto.Number, purchaseOrderDto.Date)
        {
            this.Status = purchaseOrderDto.Status;
            this.StatusDate = purchaseOrderDto.StatusDate.ToShortDateString();
            this.DeliveryDate = purchaseOrderDto.DeliveryDate.HasValue ? purchaseOrderDto.DeliveryDate.Value.ToShortDateString() : "";
            this.InitiatorName = purchaseOrderDto.InitiatorName ?? "";
            this.RequestorName = purchaseOrderDto.RequestorName ?? "";
            this.ShipToCode = purchaseOrderDto.ShipToCodeName;
            this.Comments = purchaseOrderDto.Comments;
            this.InternalComments = purchaseOrderDto.InternalComments;

            // These properties belong to the base class, but are initialized at the document level
            this.Amount = purchaseOrderDto.Amount.ToString("C");
            this.MaintenanceDate = purchaseOrderDto.MaintenanceDate.HasValue ? purchaseOrderDto.MaintenanceDate.Value.ToShortDateString() : "";
            this.CurrencyCode = purchaseOrderDto.CurrencyCode;
            this.ApType = purchaseOrderDto.ApType;
            this.VendorId = purchaseOrderDto.VendorId;
            this.VendorName = purchaseOrderDto.VendorName ?? "";

            // Initialize the approvers and line items lists by calling the set method on the base class
            this.SetApprovers(purchaseOrderDto.Approvers);
            this.SetLineItems(purchaseOrderDto.LineItems);

            // Convert the list of requisition dtos into a list of requisition number view models
            this.Requisitions = new List<RequisitionNumberViewModel>();
            if (purchaseOrderDto.Requisitions != null)
            {
                foreach (var req in purchaseOrderDto.Requisitions)
                {
                    var requisition = new RequisitionNumberViewModel(req);
                    this.Requisitions.Add(requisition);
                }
            }

            // Add the list of vouchers dtos to the purchase order view model
            this.Vouchers = new List<string>();
            if (purchaseOrderDto.Vouchers != null)
            {
                this.Vouchers = purchaseOrderDto.Vouchers;
            }
        }
    }
}