﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A view model that represents a single General Ledger Account for the Object View
    /// </summary>
    public class GlObjectCodeGlAccountViewModel
    {
        /// <summary>
        /// General Ledger Account Number for this General Ledger Account
        /// </summary>
        public string GlAccountNumber { get; private set; }

        /// <summary>
        /// Formatted General Ledger Account Number for this General Ledger Account
        /// </summary>
        public string FormattedGlAccount { get; private set; }

        /// <summary>
        /// Description of this General Ledger Account - from Colleague
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Budget amount for this General Ledger Account
        /// </summary>
        public decimal _Budget;
        public string Budget
        {
            get
            {
                if (this.PoolType == GlBudgetPoolType.Poolee && _Budget == 0)
                {
                    return "";
                }
                else
                {
                    return this._Budget.ToString("C");
                }
            }
        }

        /// <summary>
        /// Actuals amount for this General Ledger Account
        /// </summary>
        private decimal _Actuals;
        public string Actuals
        {
            get
            {
                return this._Actuals.ToString("C");
            }
        }

        /// <summary>
        /// Encumbrances amount for this General Ledger Account
        /// </summary>
        private decimal _Encumbrances;
        public string Encumbrances
        {
            get
            {
                return this._Encumbrances.ToString("C");
            }
        }

        /// <summary>
        /// The amount remaining in the budget
        /// </summary>
        public string BudgetRemaining
        {
            get
            {
                return (this._Budget - (this._Actuals + this._Encumbrances)).ToString("C");
            }
        }

        /// <summary>
        /// A representation of the amount processed divided by the amount budgeted (in percent format).
        /// </summary>
        public string PercentProcessed
        {
            get
            {
                var percent = 0m;

                if (this._Budget == 0)
                {
                    if ((this._Actuals + this._Encumbrances) > 0)
                    {
                        percent = 101;
                    }
                    else if ((this._Actuals + this._Encumbrances) < 0)
                    {
                        percent = -101;
                    }
                }
                else
                {
                    percent = Math.Round(((this._Actuals + this._Encumbrances) / this._Budget) * 100m);
                }

                return string.Format("{0:n0}", percent) + " %";
            }
        }

        /// <summary>
        /// The CSS class that is applied to the financial health column of the object view
        /// </summary>
        public string FinancialHealthIndicator
        {
            get
            {
                var indicatorStyle = "";
                if (this._IncludeFinancialHealthIndicator)
                {
                    if (this._Actuals + this._Encumbrances > this._Budget)
                    {
                        indicatorStyle = "group-icon-red";
                    }
                    else if ((this._Actuals + this._Encumbrances) > this._Budget * 0.85m)
                    {
                        indicatorStyle = "group-icon-yellow";
                    }
                    else
                    {
                        indicatorStyle = "group-icon-green";
                    }
                }
                return indicatorStyle;
            }
        }

        /// <summary>
        /// The off screen text that is applied to the financial health column of the object view
        /// </summary>
        public string FinancialHealthIndicatorText
        {
            get
            {
                var indicatorText = "";
                if (this._IncludeFinancialHealthIndicator)
                {
                    if (this._Actuals + this._Encumbrances > this._Budget)
                    {
                        indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
                    }
                    else if ((this._Actuals + this._Encumbrances) > this._Budget * 0.85m)
                    {
                        indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
                    }
                    else
                    {
                        indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
                    }
                }
                return indicatorText;
            }
        }

        /// <summary>
        /// Controls the financial health indicator visibility
        /// </summary>
        private bool _IncludeFinancialHealthIndicator;

        /// <summary>
        /// The style for color coding certain columns
        /// </summary>
        public string ColorCodeStyle
        {
            get
            {
                var colorStyle = "remaining-black";
                if (this._GlClass == GlClass.Expense)
                {
                    if (this._Actuals + this._Encumbrances > this._Budget)
                    {
                        colorStyle = "remaining-red";
                    }
                }
                else if (this._GlClass == GlClass.Revenue)
                {
                    if (this._Actuals + this._Encumbrances > this._Budget)
                    {
                        colorStyle = "remaining-green";
                    }
                }

                return colorStyle;
            }
        }

        /// <summary>
        /// The GL account GL Class.
        /// </summary>
        private GlClass _GlClass;

        /// <summary>
        /// The GL Class formatted for display.
        /// </summary>
        public string FormattedGlClass
        {
            get
            {
                var glClass = "Unknown";
                switch (_GlClass)
                {
                    case Colleague.Dtos.ColleagueFinance.GlClass.Asset:
                        glClass = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "AssetLabel");
                        break;
                    case Colleague.Dtos.ColleagueFinance.GlClass.Liability:
                        glClass = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "LiabilityLabel");
                        break;
                    case Colleague.Dtos.ColleagueFinance.GlClass.FundBalance:
                        glClass = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "FundBalanceLabel");
                        break;
                    case Colleague.Dtos.ColleagueFinance.GlClass.Revenue:
                        glClass = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "RevenueLabel");
                        break;
                    case Colleague.Dtos.ColleagueFinance.GlClass.Expense:
                        glClass = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ExpenseLabel");
                        break;

                }
                return glClass;
            }
        }


        /// <summary>
        /// Enumerated type of General Ledger Account that this is (Umbrella, Poolee, or Non-Pooled)
        /// </summary>
        private GlBudgetPoolType PoolType;

        public string PoolTypeCssClass
        {
            get
            {
                if (this.PoolType == GlBudgetPoolType.Umbrella)
                {
                    return "umbrella";
                }
                else if (this.PoolType == GlBudgetPoolType.Poolee)
                {
                    return "poolee";
                }
                else
                {
                    return "non-pooled-account";
                }
            }
        }

        public GlObjectCodeGlAccountViewModel(GlObjectCodeGlAccount accountDto, GlClass glClass, bool includeFinancialHealthIndicator = false)
        {
            if (accountDto != null)
            {
                this._GlClass = glClass;
                if (glClass == GlClass.Revenue || glClass == GlClass.Liability || glClass == GlClass.FundBalance)
                {
                    this._Actuals = accountDto.Actuals * -1;
                    this._Encumbrances = accountDto.Encumbrances * -1;
                    this._Budget = accountDto.Budget * -1;
                }
                else
                {
                    this._Actuals = accountDto.Actuals;
                    this._Encumbrances = accountDto.Encumbrances;
                    this._Budget = accountDto.Budget;
                }

                this.Description = accountDto.Description;
                this.GlAccountNumber = accountDto.GlAccountNumber;
                this.FormattedGlAccount = accountDto.FormattedGlAccount;
                this.PoolType = accountDto.PoolType;
                this._IncludeFinancialHealthIndicator = includeFinancialHealthIndicator;
            }
        }

        public void ZeroOutBudgetActualsEncumbrances()
        {
            this._Budget = 0;
            this._Actuals = 0;
            this._Encumbrances = 0;
        }
    }
}