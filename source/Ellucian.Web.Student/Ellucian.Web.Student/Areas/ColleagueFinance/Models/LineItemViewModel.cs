﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a document line item.
    /// </summary>
    public class LineItemViewModel : DocumentItem
    {
        /// <summary>
        /// Line item description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Line item quantity
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Line item price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Format and display the price
        /// </summary>
        public string PriceDisplay
        {
            get
            {
                return Price.ToString("C");
            }
        }

        /// <summary>
        /// Extended price for the line item
        /// </summary>
        public decimal ExtendedPrice { get; set; }

        /// <summary>
        /// Format and display the extended price
        /// </summary>
        public string ExtendedPriceDisplay
        {
            get
            {
                return ExtendedPrice.ToString("C");
            }
        }

        /// <summary>
        /// Unit of issue for the line item
        /// </summary>
        public string UnitOfIssue { get; set; }

        /// <summary>
        /// Line item expected delivery date
        /// </summary>
        public string ExpectedDeliveryDate { get; set; }

        /// <summary>
        /// Line item desired date
        /// </summary>
        public string DesiredDate { get; set; }

        /// <summary>
        /// Line item vendor item number
        /// </summary>
        public string VendorItem { get; set; }

        /// <summary>
        /// Line item invoice number
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Line item tax form.
        /// </summary>
        public string TaxForm { get; set; }

        /// <summary>
        /// Tax form code for the line item
        /// </summary>
        public string TaxFormCode { get; set; }

        /// <summary>
        /// Tax form location for the line item
        /// </summary>
        public string TaxFormLocation { get; set; }

        /// <summary>
        /// Line item comments
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// List of GL distributions
        /// </summary>
        public List<LineItemGlDistributionViewModel> GlDistributions { get; set; }

        /// <summary>
        /// List of tax distributions
        /// </summary>
        public List<LineItemTaxViewModel> TaxDistributions { get; set; }

        /// <summary>
        /// Create a new line item.
        /// </summary>
        /// <param name="lineItemDto">Line Item DTO</param>
        public LineItemViewModel(LineItem lineItemDto)
        {
            this.Description = lineItemDto.Description;
            this.Quantity = lineItemDto.Quantity;
            this.Price = lineItemDto.Price;
            this.ExtendedPrice = lineItemDto.ExtendedPrice;
            this.UnitOfIssue = lineItemDto.UnitOfIssue; 
            this.ExpectedDeliveryDate = lineItemDto.ExpectedDeliveryDate.HasValue ? lineItemDto.ExpectedDeliveryDate.Value.ToShortDateString() : "";
            this.DesiredDate = lineItemDto.DesiredDate.HasValue ? lineItemDto.DesiredDate.Value.ToShortDateString() : "";
            this.VendorItem = lineItemDto.VendorPart;
            this.InvoiceNumber = lineItemDto.InvoiceNumber;
            this.TaxForm = lineItemDto.TaxForm;
            this.TaxFormCode = lineItemDto.TaxFormCode;
            this.TaxFormLocation = lineItemDto.TaxFormLocation;
            this.Comments = lineItemDto.Comments;

            this.GlDistributions = new List<LineItemGlDistributionViewModel>();
            if (lineItemDto.GlDistributions != null)
            {
                foreach (var glDistribution in lineItemDto.GlDistributions)
                {
                    this.GlDistributions.Add(new LineItemGlDistributionViewModel(glDistribution));
                }
            }

            this.TaxDistributions = new List<LineItemTaxViewModel>();
            if (lineItemDto.LineItemTaxes != null)
            {
                foreach (var taxDistribution in lineItemDto.LineItemTaxes)
                {
                    this.TaxDistributions.Add(new LineItemTaxViewModel(taxDistribution));
                }
            }
        }
    }
}