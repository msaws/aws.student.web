﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// View model for a GL Account activity detail.
    /// </summary>
    public class GlActivityDetailViewModel
    {
        /// <summary>
        /// Object containing General Ledger account activity detail.
        /// </summary>
        public GlAccountActivityDetailViewModel GlAccountActivityDetail { get; set; }

        /// <summary>
        /// List of General Ledger fiscal years for the drop-down.
        /// </summary>
        public List<FiscalYearFilterViewModel> FiscalYears { get; set; }

        /// <summary>
        /// The current fiscal year.
        /// </summary>
        public string TodaysFiscalYear { get; set; }

        /// <summary>
        /// Initialize the GL Activity Detail view model.
        /// </summary>
        /// <param name="glAccountActivityDetailDto">A GL account activity detail DTO.</param>
        /// <param name="fiscalYears">List of fiscal years for display.</param>
        public GlActivityDetailViewModel(GlAccountActivityDetail glAccountActivityDetailDto, IEnumerable<string> fiscalYears, string todaysFiscalYear)
        {
            GlAccountActivityDetail = new GlAccountActivityDetailViewModel(glAccountActivityDetailDto);

            this.TodaysFiscalYear = todaysFiscalYear ?? "";

            this.FiscalYears = new List<FiscalYearFilterViewModel>();
            if (fiscalYears != null)
            {
                for (int i = 0; i < fiscalYears.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(fiscalYears.ElementAt(i)))
                        FiscalYears.Add(new FiscalYearFilterViewModel(fiscalYears.ElementAt(i)));
                }
            }
        }
    }
}