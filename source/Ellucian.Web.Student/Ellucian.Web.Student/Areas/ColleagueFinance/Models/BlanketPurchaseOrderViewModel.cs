﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System.Diagnostics.CodeAnalysis;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a Blanket Purchase Order document.
    /// </summary>
    public class BlanketPurchaseOrderViewModel : AccountsPayablePurchasingDocumentViewModel
    {
        /// <summary>
        /// Blanket Purchase order status
        /// </summary>
        public BlanketPurchaseOrderStatus Status { get; set; }

        /// <summary>
        /// Return the status description
        /// </summary>
        public string StatusDescription
        {
            get
            {
                string description = "";
                switch (Status)
                {
                    case BlanketPurchaseOrderStatus.Closed:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusClosed");
                        break;
                    case BlanketPurchaseOrderStatus.InProgress:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusInProgress");
                        break;
                    case BlanketPurchaseOrderStatus.NotApproved:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusNotApproved");
                        break;
                    case BlanketPurchaseOrderStatus.Outstanding:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusOutstanding");
                        break;
                    case BlanketPurchaseOrderStatus.Voided:
                        description = GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusVoided");
                        break;
                }

                return description;
            }
        }

        /// <summary>
        /// Blanket Purchase order status date
        /// </summary>
        public string StatusDate { get; set; }

        /// <summary>
        /// Blanket Purchase order description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Blanket Purchase order expiration date
        /// </summary>
        public string ExpirationDate { get; set; }

        /// <summary>
        /// Blanket Purchase order initiator name
        /// </summary>
        public string InitiatorName { get; set; }

        /// <summary>
        /// Purchase order internal comments
        /// </summary>
        public string InternalComments { get; set; }

        /// <summary>
        /// List of requisitions associated to the blanket purchase order
        /// </summary>
        public List<RequisitionNumberViewModel> Requisitions { get; set; }

        /// <summary>
        /// List of vouchers associated to the blanket purchase order
        /// </summary>
        public List<string> Vouchers { get; set; }

        /// <summary>
        /// List of GlDistributions for this BPO.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        public List<BlanketPurchaseOrderGlDistributionViewModel> GlDistributions { get; set; }

        /// <summary>
        /// Create a new blanket purchase order.
        /// </summary>
        /// <param name="purchaseOrderDto">Purchase order DTO</param>
        public BlanketPurchaseOrderViewModel(BlanketPurchaseOrder blanketPurchaseOrderDto)
            : base(blanketPurchaseOrderDto.Id, blanketPurchaseOrderDto.Number, blanketPurchaseOrderDto.Date)
        {
            this.Status = blanketPurchaseOrderDto.Status;
            this.StatusDate = blanketPurchaseOrderDto.StatusDate.ToShortDateString();
            this.ExpirationDate = blanketPurchaseOrderDto.ExpirationDate.HasValue ? blanketPurchaseOrderDto.ExpirationDate.Value.ToShortDateString() : "";
            this.InitiatorName = blanketPurchaseOrderDto.InitiatorName ?? "";
            this.Description = blanketPurchaseOrderDto.Description;
            this.InternalComments = blanketPurchaseOrderDto.InternalComments;

            // These properties belong to the base class, but are initialized at the document level
            this.Amount = blanketPurchaseOrderDto.Amount.ToString("C");
            this.Comments = blanketPurchaseOrderDto.Comments;
            this.MaintenanceDate = blanketPurchaseOrderDto.MaintenanceDate.HasValue ? blanketPurchaseOrderDto.MaintenanceDate.Value.ToShortDateString() : "";
            this.CurrencyCode = blanketPurchaseOrderDto.CurrencyCode;
            this.ApType = blanketPurchaseOrderDto.ApType;
            this.VendorId = blanketPurchaseOrderDto.VendorId;
            this.VendorName = blanketPurchaseOrderDto.VendorName ?? "";

            // Initialize the approvers by calling the set method on the base class
            this.SetApprovers(blanketPurchaseOrderDto.Approvers);

            // Convert the list of requisition dtos into a list of requisition number view models
            this.Requisitions = new List<RequisitionNumberViewModel>();
            if (blanketPurchaseOrderDto.Requisitions != null)
            {
                foreach (var req in blanketPurchaseOrderDto.Requisitions)
                {
                    if (!string.IsNullOrEmpty(req))
                    {
                        var requisition = new RequisitionNumberViewModel(req);
                        this.Requisitions.Add(requisition);
                    }
                }
            }

            // Add the list of vouchers IDs to the blanket purchase order view model
            this.Vouchers = new List<string>();
            if (blanketPurchaseOrderDto.Vouchers != null)
            {
                foreach (var voucher in blanketPurchaseOrderDto.Vouchers)
                {
                    if (!string.IsNullOrEmpty(voucher))
                    {
                        this.Vouchers.Add(voucher);
                    }
                }
            }

            // Initialize the list of GL Distributions
            this.GlDistributions = new List<BlanketPurchaseOrderGlDistributionViewModel>();
            if (blanketPurchaseOrderDto.GlDistributions != null)
            {
                int id = 1;
                blanketPurchaseOrderDto.GlDistributions.ForEach(glDistrDto =>
                {
                    if (glDistrDto != null)
                    {
                        var glDistributionVM = new BlanketPurchaseOrderGlDistributionViewModel(glDistrDto);

                        // Set the item ID so we can uniquely identify the item.
                        glDistributionVM.DocumentItemId = "document-line-item" + id.ToString();
                        this.GlDistributions.Add(glDistributionVM);
                        id++;
                    }
                });
            }
        }
    }
}
