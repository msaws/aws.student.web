﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// GL component view model
    /// </summary>
    public class GeneralLedgerComponentViewModel
    {
        /// <summary>
        /// Name of the GL component
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// Maximum length of the GL component
        /// </summary>
        public string ComponentLength { get; set; }

        /// <summary>
        /// Start position of the GL component
        /// </summary>
        public int StartPosition { get; set; }

        /// <summary>
        /// Initialize the GL component view model
        /// </summary>
        public GeneralLedgerComponentViewModel(GeneralLedgerComponent glComponentDto)
        {
            if (glComponentDto != null)
            {
                // Change the component name to be "pascal" or "title" case.
                this.ComponentName = glComponentDto.ComponentName.First().ToString().ToUpper() + glComponentDto.ComponentName.Substring(1).ToLower();

                this.ComponentLength = glComponentDto.ComponentLength.ToString();
                this.StartPosition = glComponentDto.StartPosition;
            }
        }
    }
}