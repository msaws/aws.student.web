﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// A user-defined filter
    /// </summary>
    public class SavedFilter
    {
        /// <summary>
        /// Saved filter's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Saved filter's ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The saved filter's criteria
        /// </summary>
        public List<CostCenterComponentQueryCriteria> ComponentCriteria { get; set; }

        /// <summary>
        /// The boolean flag to determine what accounts to include.
        /// </summary>
        public bool IncludeActiveAccountsWithNoActivity { get; set; }
    }
}
