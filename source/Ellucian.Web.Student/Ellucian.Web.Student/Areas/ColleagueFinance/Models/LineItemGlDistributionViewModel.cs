﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class represents a GL Distribution for a transaction line item.
    /// </summary>
    public class LineItemGlDistributionViewModel
    {
        /// <summary>
        /// GL account number
        /// </summary>
        public string GlAccount { get; set; }

        /// <summary>
        /// GL account formatted for display
        /// </summary>
        public string FormattedGlAccount { get; set; }

        /// <summary>
        /// Project Number
        /// </summary>
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Project line item code
        /// </summary>
        public string ProjectLineItemCode { get; set; }

        /// <summary>
        /// GL quantity
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// GL amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Format and display the distribution amount.
        /// </summary>
        public string AmountDisplay
        {
            get
            {
                return Amount.ToString("C");
            }
        }

        /// <summary>
        /// Does the user have access to this GL distribution?
        /// </summary>
        public bool IsAccountMasked
        {
            get
            {
                return GlAccount == null;
            }
        }

        /// <summary>
        /// Create a new GL distribution for the line item.
        /// </summary>
        /// <param name="glDistributionDto">GL Distribution DTO</param>
        public LineItemGlDistributionViewModel(LineItemGlDistribution glDistributionDto)
        {
            this.GlAccount = glDistributionDto.GlAccount;
            this.FormattedGlAccount = glDistributionDto.FormattedGlAccount;
            this.ProjectNumber = glDistributionDto.ProjectNumber;
            this.ProjectLineItemCode = glDistributionDto.ProjectLineItemCode;
            this.Quantity = glDistributionDto.Quantity;
            this.Amount = glDistributionDto.Amount;
        }
    }
}