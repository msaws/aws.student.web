﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This is a Blanket Purchase Order document that only contains
    /// the record key and number.
    /// </summary>
    public class BlanketPurchaseOrderNumberViewModel : PurchasingDocumentNumberViewModel
    {
        /// <summary>
        ///  Blanket Purchase Order internal ID
        /// </summary>
        public override string Id { get; set; }

        /// <summary>
        /// Blanket Purchase Order number that the document is known by
        /// </summary>
        public override string Number { get; set; }

        /// <summary>
        /// Create a new blanket purchase order number view model
        /// </summary>
        /// <param name="id">Blanket Purchase Order ID</param>
        public BlanketPurchaseOrderNumberViewModel(string id) : base(id)
        {
            this.Id = id;
        }
    }
}
