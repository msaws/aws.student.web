﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
memory.setItem("newPage", 1);

var utility = new financialAidUtility();

var awardsViewModelInstance = new awardsViewModel();

$(document).ready(function () {
    ko.applyBindings(awardsViewModelInstance, document.getElementById("main"));
    getAwardsData();
});

function getAwardsData() {
    var url = utility.buildFinAidUrl(getAwardsActionUrl, memory);

    // Get the student's awards
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {

                ko.mapping.fromJS(data, awardsViewModelMapping, awardsViewModelInstance);

                var awardYears = awardsViewModelInstance.AwardYears();
                var selected = memory.getItem("yearCode");
                for (var i = 0; i < awardYears.length; i++) {
                    if (awardYears[i].Code() == selected) {
                        awardsViewModelInstance.selectedYear(awardsViewModelInstance.AwardYears()[i]);
                        break;
                    }
                }
                awardsViewModelInstance.showUI(true);
                memory.setItem("newPage", 0);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Awards retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if (jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if (jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faAwardsUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            // Accordion content must be rendered before its controller because their IDs are dynamic;
            // this will move accordion content beneath its controller for proper accordion show/hide.
            rearrangeMultiAccordionElements($(".multi-accordion-content"));

            awardsViewModelInstance.checkForMobile(window, document);
        }
    });
}
