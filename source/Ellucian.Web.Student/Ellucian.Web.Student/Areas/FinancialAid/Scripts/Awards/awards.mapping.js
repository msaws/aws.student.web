﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
var Ellucian = Ellucian || {};
Ellucian.AwardsViewModelMapping = Ellucian.AwardsViewModelMapping || {};

var awardsViewModelMapping = {

    'StudentUnsubLoans': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel(options);
        }
    },

    'StudentSubLoans': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel(options);
        }
    },

    'StudentGradPlusLoans': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel(options);
        }
    },

    'StudentAwards': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentAwardCollectionModel(options.data);
        }
    },

    'StudentAwardsForYear': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentAwardModel(options);
        }
    },

    'StudentWorkAwardsForYear': {
        create: function(options) {
            return new Ellucian.AwardsViewModelMapping.studentAwardModel(options);
        }
    },

    'OtherLoans': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentAwardCollectionModel(options.data);
        }
    },

    'Loans': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentAwardModel(options);
        }
    },

    'LoanPeriods': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.loanPeriodsModel(options.data);
        }
    },

    'StudentAwardPeriods': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.studentAwardPeriodModel(options);
        }
    },

    'LoanRequirements': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.loanRequirementsModel(options.data);
        }
    },

    'Configuration': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.StudentFinancialAidConfiguration(options.data);
        }
    },

    'AwardYearConfigurations': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.AwardYearConfiguration(options.data);
        }
    },

    'AwardPercentageCharts': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.percentageChartCollectionModel(options.data);
        }
    },
    'PercentageChartModels': {
        create: function(options){
            return new Ellucian.AwardsViewModelMapping.percentageByAwardTypeChartModel(options.data);
        }
    },
    'SAPStatus': {
        create: function (options) {
            return new Ellucian.AwardsViewModelMapping.sapStatusModel(options.data);
        }
    }
}

Ellucian.AwardsViewModelMapping.studentAwardModel = function(options) {
    var data = options.data;
    var parent = options.parent;
    
    var self = this;
    //Flag, does not change
    self.isLoan = false;

    self.hasInputErrors = ko.observable(false);
    self.inputErrorMessage = ko.observable();
    
    self.awardUpdated = ko.observable(false);

    ko.mapping.fromJS(data, awardsViewModelMapping, this);

    self.accordionIsOpen = ko.observable(false);
    self.flipAccordion = function () {
        if (self.accordionIsOpen()) {
            self.accordionIsOpen(false);

        } else {
            self.accordionIsOpen(true);
        }
        return false;
    };

    self.totalAmount = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.StudentAwardPeriods(), function (period) {
            var amt = parseANumber(period.AwardAmount());
            total = total + amt;
        });

        return total;
    });    

    //#region These fields are used for loans only when allowYearLevelUpdatesOnly flag is set to true
    //Use this whenever the flag allowYearLevelUpdatesOnly is set to true and
    //it is allowed to change loan amounts
    self.totalLoanChangeAmount = ko.observable().extend({
        numeric: {
            precision: 0,
            min: 0,
            valCondition: function () { return true; },            
            updateOnError: true

        },
        rateLimit: {
            timeout: 2000,
            method: "notifyWhenChangesStop"
        }
    });

    //Display totalAmount with two decimal places when input box is out of focus, if there is error
    //return 0 to be consistent with other input boxes behavior
    self.loanChangeAmountWithPrecision = ko.computed(function () {
        if (!self.hasInputErrors()) {
            var amount = parseANumber(ko.utils.unwrapObservable(self.totalAmount()));            
        }
        else { amount = 0; }
        return Globalize.format(amount, "n2");
    });

    //Flag to indicate whether the current award's loan amount input box has focus or not
    self.isLoanAmountInFocus = ko.observable(false);
    //#endregion

    //Flag that indicates if this award is a direct or grad plus type loan
    self.isDirectOrGradPlusLoan = ko.computed(function () {
        if (self.LoanType() === 'SubsidizedLoan' || self.LoanType() === 'UnsubsidizedLoan' ||
            self.LoanType() === 'GraduatePlusLoan') return true;
        else return false;
    });

    self.totalAwardPeriodAmount = function (awardPeriod) {

        var matchingStudentAwardPeriods = ko.utils.arrayFilter(self.StudentAwardPeriods(), function (studentAwardPeriod) {
            return (studentAwardPeriod.Code() === awardPeriod.Code());
        });

        var total = 0;
        ko.utils.arrayForEach(matchingStudentAwardPeriods, function (studentAwardPeriod) {
            var amt = parseANumber(studentAwardPeriod.AwardAmount());
            if (amt === null) { amt = 0; }
            total += amt;
        });

        return total;
    }

    self.awardRows = function () {
        return self.accordionIsOpen() ? 5 : 2;
    }

    /*Flag to indicate if a an award is on hold or in review, or amount is not gt zero:
        a) Award is pending but the status is not modifiable;
        b) total award amount is $0, the status is modifiable and award section is expanded
    */
    self.isAwardOnHoldOrInReview = ko.computed(function () {
        if ((self.AwardStatus.Category() === 'Pending' || self.AwardStatus.Category() === 'Estimated') && !self.IsStatusModifiable()
            || ((self.totalAmount() === 0) && self.IsStatusModifiable() && self.accordionIsOpen())) {
            return true;
        }
        else return false;
    });
    
    //Flag indicating whether the notification should bevisible or not
    self.isInfoNotificationVisible = ko.computed(function () {
        if (!(self.LoanType() === 'UnsubsidizedLoan' && parent.isAnySubLoanPending()) &&
            (self.AwardStatus.Category() === 'Pending'
            || self.AwardStatus.Category() === 'Estimated'
            || (self.isDirectOrGradPlusLoan() && self.IsAmountModifiable()))) return true;
        return false;
    });

    //Info notification to display on award level
    self.infoNotification = ko.computed(function () {
        if (!(self.LoanType() === 'UnsubsidizedLoan' && parent.isAnySubLoanPending())) {
            if (((self.AwardStatus.Category() === 'Pending' || self.AwardStatus.Category() === 'Estimated')
                || (self.isDirectOrGradPlusLoan() && self.AwardStatus.Category() === 'Accepted'))
                && self.IsAwardInReview() && !self.IsStatusModifiable())
                return faAwardsSingleAwardInReviewMessage;
            else if ((self.AwardStatus.Category() === 'Pending' || self.AwardStatus.Category() === 'Estimated')
                && !self.IsAwardInReview() && !self.IsStatusModifiable() && !self.IsAmountModifiable())
                return faAwardsSingleAwardPlacedOnHoldMessage;
            else if ((self.totalAmount() === 0 && !self.isDirectOrGradPlusLoan() && self.IsStatusModifiable()) ||
                (self.isDirectOrGradPlusLoan() && parent.totalAmount() === 0 && !parent.allowDeclineZeroAcceptedLoans()))
                return faAwardsTotalAwardAmountEqualsZeroAcceptMessage;
            else if (self.isDirectOrGradPlusLoan() && parent.suppressMaxLoanLimit()){
                if (self.AwardStatus.Category() === 'Pending' || self.AwardStatus.Category() === 'Estimated') return faAwardsLoanActionNoMaxLimitsMessage;
                else {
                    if (parent.allowDeclineZeroAcceptedLoans()) return faAwardsNonPendingLoanActionNoMaxLimitsZeroAllowedMessage;
                    else return faAwardsNonPendingLoanActionNoMaxLimitsMessage;
                }
            }                
            else if (self.isDirectOrGradPlusLoan()){
                if (self.AwardStatus.Category() === 'Pending' || self.AwardStatus.Category() === 'Estimated') return faAwardsLoanActionMessage;
                else {
                    if (parent.allowDeclineZeroAcceptedLoans()) return faAwardsNonPendingLoanActionZeroAllowedMessage;
                    else return faAwardsNonPendingLoanActionMessage;
                }
            }
                
            else return faAwardsAwardActionMessage;
        }
    });

    //Description(full title) of the award for accessibility purposes
    self.offScreenDescription = ko.computed(function () {
        return self.Description();
    });

    //Message for the loan input field based on the loan type
    self.awardLoanInputMessage = ko.computed(function () {
        if (self.LoanType() === 'SubsidizedLoan') return faAwardsSubsidizedLoansInputMessage;
        else if (self.LoanType() === 'UnsubsidizedLoan') return faAwardsUnubsidizedLoansInputMessage;
        else if (self.LoanType() === 'GraduatePlusLoan') return faAwardsGradPlusLoansInputMessage;
    });

    //Disables/enables reject button
    self.disableRejectButton = ko.computed(function () {
        if (!self.IsStatusModifiable()) return true;
        return false;
    });

    //Disables/enables accept button
    self.disableAcceptButton = ko.computed(function () {
        if (!self.IsStatusModifiable()
            || (self.totalAmount() === 0)) return true;
        else return false;
    });
    
}

Ellucian.AwardsViewModelMapping.studentLoanCollectionModel = function(options) {

    var self = this;

    var parent = options.parent;
    var data = options.data;

    self.Loans = ko.observableArray();

    self.hasInputErrors = ko.observable(false);

    //Flag, does not change
    self.isLoan = true;

    self.loanCollectionUpdated = ko.observable(false);    

    ko.mapping.fromJS(data, awardsViewModelMapping, this);

    self.accordionIsOpen = ko.observable(false);
    self.flipAccordion = function () {
        if (self.accordionIsOpen()) {
            self.accordionIsOpen(false);
        } else {
            self.accordionIsOpen(true);
        }
        return false;
    };

    //Get the count of subLoans for year
    //mcd - when the accordion is open, we're adding three extra rows to the number of subloan rows
    //when the accoridon is closed, the collection is only 1 row
    self.collectionRows = ko.computed(function () {
        if (self.accordionIsOpen()) {
            return self.Loans().length + 4;
        }

        return 1;
    });

    //Total amount for all loans in the group across periods
    self.totalAmount = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.Loans(), function (loan) {
            total += loan.totalAmount();
        });

        return total;
    });

    self.totalAwardPeriodAmount = function (period) {
        var total = 0;
        ko.utils.arrayForEach(self.Loans(), function (loan) {
            var amt = loan.totalAwardPeriodAmount(period);
            if (amt === null) { amt = 0; }
            total += amt;
        });
        return total;
    }

    self.hasPendingLoans = ko.computed(function () {
        var pendingLoan = null;
        ko.utils.arrayFirst(self.Loans(), function (loan) {
            var category = ko.utils.unwrapObservable(loan.AwardStatus.Category());
            if (category === "Pending" || category === "Estimated") {
                pendingLoan = loan;
            }
        });

        if (pendingLoan !== null) return true;
        else return false;
    });

    //flag indicating whether loan limits should be suppressed for the year
    self.suppressMaxLoanLimit = ko.computed(function () {
        var yearConfig = ko.utils.unwrapObservable(parent.yearConfiguration());
        if (yearConfig !== null && yearConfig.SuppressMaximumLoanLimits()) return true;
        return false;
    });

    //Check if the loan is not greater/less than allowed amount
    self.passesLimitationCriteria = ko.computed(function () {

        //if there are no pending loans or no amounts are modifiable, then we don't care if the amounts
        //do not meet limitations
        if (!self.hasPendingLoans() && !self.IsStatusModifiable() && !self.IsAnyAmountModifiable()) return true;

        //If flag to suppress max loan limits is false - take limits into consideration
        if (!self.suppressMaxLoanLimit() && (self.totalAmount() > self.MaximumAmount())) {
            $("#aria-announcements").text(MaximumAmountWarningForScreenReaders.format(self.MaximumAmount().toFixed(2)));
            return false;
        }

        $("#aria-announcements").empty();
        return true;
    });

    self.checkboxActivator = function (awardPeriod, event) {
        var checkbox = event.target;

        ko.utils.arrayForEach(self.Loans(), function (loan) {
            //If loan is accepted, user unclicking the period, should not change the accepted amount
            if (loan.AwardStatus.Category() == "Pending" || loan.AwardStatus.Category() == "Estimated" || loan.IsStatusModifiable() === true) {
                ko.utils.arrayForEach(loan.StudentAwardPeriods(), function (studentAwardPeriod) {
                    //Even if the whole loan is pending, some of the award periods' amounts might be accepted, check for that as well
                    if ((studentAwardPeriod.Description() == awardPeriod.Description()) && (studentAwardPeriod.AwardStatus.Category() == "Pending" || studentAwardPeriod.AwardStatus.Category() == "Estimated" || studentAwardPeriod.IsStatusModifiable() === true)) {
                        studentAwardPeriod.applyAwardToPeriod(studentAwardPeriod, event);
                    }
                });
            }
        });

        return true;
    }

    //Computed boolean that returns true if mulptiple loans in the loan collection are pending but
    //cannot be accepted/rejected
    self.multipleUntouchableAwards = ko.computed(function () {
        var count = 0;
        if (self.hasPendingLoans() || self.IsLoanCollectionInReview()) {
            ko.utils.arrayForEach(self.Loans(), function (loan) {
                if ((loan.AwardStatus.Category() == "Pending" || loan.AwardStatus.Category() == "Estimated" || loan.IsAwardInReview()) && loan.IsStatusModifiable() === false) {
                    count += 1;
                }
            });
        }
        if (count > 1) { return true };
        return false;
    });

    //Flag to indicate if any of the loans belonging to this loan collection is on hold by the FA office(FSOP exclusion for accept/reject)
    self.isAnyLoanOnHold = ko.computed(function () {
        var onHoldLoan = ko.utils.arrayFirst(self.Loans(), function (loan) {
            return ((loan.StatusDescription() === 'Pending' || loan.StatusDescription() === 'Estimated') && !loan.IsStatusModifiable());
        });

        return onHoldLoan === null ? false : true;
    });

    /*Flag to indicate whether the loan collection is on hold, in review, or the total of all subloans is not gt zero:
        a) Loan collection has pending loans but the status is not modifiable;
        b) There is at least one loan that is on hold;
        c) loan collection is in review;
        d) total amount equals $0, accordion is expanded and either the loan collection status is modifiable or 
           some of the loan amount(s) is/are modifiable
    */
    self.isLoanCollectionOnHoldOrInReview = ko.computed(function () {
        if (((self.StatusDescription() === 'Pending' || self.StatusDescription() === 'Estimated') && !self.IsStatusModifiable())
            || self.isAnyLoanOnHold()
            || self.IsLoanCollectionInReview()
            || (self.totalAmount() === 0 && self.accordionIsOpen()
                && (self.IsStatusModifiable() || self.IsAnyAmountModifiable())
                && !self.allowDeclineZeroAcceptedLoans())) {
            return true;
        }
        else return false;
    });

    //Description(full title) of the loan collection for accessibility purposes
    self.offScreenDescription = ko.computed(function () {
        if (self.LoanCollectionType() === 'SubsidizedLoan') return faAwardsSubsidizedLoansTitle;
        else if (self.LoanCollectionType() === 'UnsubsidizedLoan') return faAwardsUnsubsidizedLoansTitle;
        else if (self.LoanCollectionType() === 'GraduatePlusLoan') return faAwardsGradPlusLoansTitle;
        else return " ";
    });

    //Flag indicating if an action can be taken on the loan collection
    self.isLoanCollectionActionable = ko.computed(function () {
        if (self.LoanCollectionType() === 'UnsubsidizedLoan' && parent.isSubLoanPending()) return false;
        return true;
    });

    //Disables/enables submitLoanAmountChange button
    self.disableSubmitLoanAmountChangeButton = ko.pureComputed(function () {
        if (!(self.isLoanCollectionActionable())
            || !(self.passesLimitationCriteria())
            || !(self.IsAnyAmountModifiable() && self.areAnyPeriodsActive())
            || self.hasInputErrors()
            || (self.totalAmount() === 0 && !parent.yearConfiguration().AllowDeclineZeroOfAcceptedLoans())
            || parent.isAdmin()
            || parent.IsProxyView()) return true;

        return false;
    });

    //Disables/enables reject button
    self.disableRejectButton = ko.pureComputed(function () {
        if (!self.isLoanCollectionActionable()
            || !(self.IsStatusModifiable() && self.areAnyPeriodsActive())
            || self.hasInputErrors()) return true;
        return false;
    });

    //Function returns true/false based on whether there are any award periods 
    //in the collection that are active
    self.areAnyPeriodsActive = ko.pureComputed(function () {
        var actionablePeriod = ko.utils.arrayFirst(self.Loans(), function (loan) {
            return ko.utils.arrayFirst(loan.StudentAwardPeriods(), function (studentAwardPeriod) {
                if (studentAwardPeriod.IsActive()) {
                    return true;
                }
            });
        });

        return actionablePeriod !== null;
    });

    //Disables/enables accept button
    self.disableAcceptButton = ko.pureComputed(function () {
        if (!self.isLoanCollectionActionable()
            || self.hasInputErrors() 
            || !self.passesLimitationCriteria() 
            || !self.IsStatusModifiable() 
            || (self.totalAmount() === 0)
            || (self.IsStatusModifiable() && self.IsAccepted())) return true;
        else return false;
    });

    self.isLoanCollectionWarningVisible = ko.computed(function () {
        if (!parent.yearConfiguration().SuppressMaximumLoanLimits() || !self.isLoanCollectionActionable()) return true;
        return false;
    });

    self.isAnySubLoanPending = ko.computed(function () {
        return parent.isSubLoanPending();
    });

    self.isSubmitLoanAmountChangeButtonVisible = ko.pureComputed(function () {
        return ((!self.IsStatusModifiable() && self.IsAnyAmountModifiable())
            || self.IsStatusModifiable() && self.IsAccepted());
    });

    self.allowDeclineZeroAcceptedLoans = ko.pureComputed(function () {
        var yearConfig = ko.utils.unwrapObservable(parent.yearConfiguration());
        if (yearConfig !== null && yearConfig.AllowDeclineZeroOfAcceptedLoans()) return true;
        return false;
    });

    self.showErrorIcon = ko.pureComputed(function () {
        return (!self.accordionIsOpen() &&
            (!self.passesLimitationCriteria()
            || self.hasInputErrors()
            || (self.totalAmount() === 0 && self.IsStatusModifiable() && !self.allowDeclineZeroAcceptedLoans())));
    });
}

//Need this for mapping in the ajax request
//since it did not work when the object was an "Object"
Ellucian.AwardsViewModelMapping.studentAwardCollectionModel = function(data) {
    var self = this;
    ko.mapping.fromJS(data, awardsViewModelMapping, this);    
}

Ellucian.AwardsViewModelMapping.studentAwardPeriodModel = function(options) {

    var self = this;
    var data = options.data;
    var parent = options.parent;

    self.loanChangeAmount = ko.observable().extend({
        numeric: {
            precision: 0,
            min: 0,
            valCondition: function () { return true; },
            updateOnError: true

        },
        rateLimit: {
            timeout: 2000,
            method: "notifyWhenChangesStop"
        }
    });

    //Display loanChangeAmount with two decimal places when input box is out of focus
    self.loanChangeAmountWithPrecision = ko.computed(function () {
        var amount = parseANumber(ko.utils.unwrapObservable(self.AwardAmount()));
        return Globalize.format(amount, "n2");
    });

    //Flag to indicate whether the current award period's loan amount input box has focus or not
    self.isLoanAmountInFocus = ko.observable(false);    

    ko.mapping.fromJS(data, awardsViewModelMapping, this);

    Initialize();

    function Initialize() {
        self.originalAmount = ko.observable(self.AwardAmount());
        self.originalIsActive = ko.observable(self.IsActive());
        self.loanChangeAmount(self.AwardAmount());

        if (!self.IsActive()) {
            self.AwardAmount(0);
            self.loanChangeAmount(0);
        }
    }

    //this function will need some work.
    //we might need this property in the C# view model 
    //so we can incorporate the loan limits API
    //mcd 12/4/13 - replaced editable with IsModifiable from ViewModel in HTML...may still need editable function
    self.editable = function (award) {
        return false
    };

    //Handles Apply/Unapply events when user checks/unchecks that an award should be applied to an award period
    self.applyAwardToPeriod = function (period, event) {
        var checkbox = event.target;
        //if the awardPeriod is StatusModifable only!
        if (period.IsStatusModifiable()) {
            //if the user unchecked the box to unapply the award period to the award
            //zero out the award amount
            if (!checkbox.checked) {
                self.IsActive(false);
                self.AwardAmount(0);
                self.loanChangeAmount(0);
            }

                //if the user checks or rechecks the box to apply the award period to the award
                //set the award amount back to the original amount
            else if (checkbox.checked) {
                self.IsActive(true);
                self.AwardAmount(self.originalAmount());
                self.loanChangeAmount(self.AwardAmount());
            }
        }

        return true;
    }

    //Message for the loan input field based on the loan type
    self.awardPeriodLoanInputMessage = ko.computed(function () {
        if (parent.LoanType() === 'SubsidizedLoan') return faAwardsSubsidizedLoansInputMessage;
        else if (parent.LoanType() === 'UnsubsidizedLoan') return faAwardsUnubsidizedLoansInputMessage;
        else if (parent.LoanType() === 'GraduatePlusLoan') return faAwardsGradPlusLoansInputMessage;
    });
}

Ellucian.AwardsViewModelMapping.loanRequirementsModel = function(data) {
    ko.mapping.fromJS(data, awardsViewModelMapping, this);

    var self = this;
    //Calculate direct loan styles
    self.directLoanMpnStatusStyle = ko.computed(function () {
        if (self.ActiveDirectLoanMpnStatus() === "Incomplete" || self.ActiveDirectLoanMpnStatus() === null)
            return "action-incomplete";
        else if (self.ActiveDirectLoanMpnStatus() === "Complete")
            return "action-completed";
    });

    self.directLoanEntranceStatusStyle = ko.computed(function () {
        if (self.DirectLoanEntranceInterviewStatus() === "Incomplete" || self.DirectLoanEntranceInterviewStatus() === null)
            return "action-incomplete";
        else if (self.DirectLoanEntranceInterviewStatus() === "Complete")
            return "action-completed";
    });
    
    //Calculate Grad PLUS loan styles
    self.gradPlusLoanMpnStatusStyle = ko.computed(function () {
        if (self.ActiveGraduatePlusMpnStatus() === "Incomplete" || self.ActiveGraduatePlusMpnStatus() === null)
            return "action-incomplete";
        else if (self.ActiveGraduatePlusMpnStatus() === "Complete")
            return "action-completed";
    });

    self.gradPlusLoanEntranceStatusStyle = ko.computed(function () {
        if (self.GraduatePlusEntranceInterviewStatus() === "Incomplete" || self.GraduatePlusEntranceInterviewStatus() === null)
            return "action-incomplete";
        else if (self.GraduatePlusEntranceInterviewStatus() === "Complete")
            return "action-completed";
    });
}

Ellucian.AwardsViewModelMapping.loanPeriodsModel = function(data) {
    ko.mapping.fromJS(data, awardsViewModelMapping, this);

    var self = this;

    self.originalIsActive = ko.observable(self.IsActive());
}

Ellucian.AwardsViewModelMapping.StudentFinancialAidConfiguration = function(data) {
    var self = this;

    self.MissingConfigurationMessages = ko.observableArray();

    ko.mapping.fromJS(data, awardsViewModelMapping, self);
}

Ellucian.AwardsViewModelMapping.AwardYearConfiguration = function(data) {
    var self = this;

    ko.mapping.fromJS(data, awardsViewModelMapping, self);
}

Ellucian.AwardsViewModelMapping.percentageChartCollectionModel = function(data)
{
    var self = this;
    ko.mapping.fromJS(data, awardsViewModelMapping, self);
}

Ellucian.AwardsViewModelMapping.percentageByAwardTypeChartModel = function(data)
{
    var self = this;
    ko.mapping.fromJS(data, awardsViewModelMapping, self);

    self.awardsSubsectionHeader = ko.computed(function () {
        if (self.AwardCategoryType() === 'Award') {
            return faAwardsDontPayBackHeader;
        }
        else if (self.AwardCategoryType() === 'Work') {
            return faAwardsWorkForHeader;
        }
        else return faAwardsPayBackHeader;
    });

    self.awardsSubsectionMessage = ko.computed(function () {
        if (self.AwardCategoryType() === 'Award') {
            return faAwardsDontPayBackMessage;
        }
        else if (self.AwardCategoryType() === 'Work') {
            return faAwardsWorkForMessage;
        }
        else return faAwardsPayBackMessage;
    });
}

Ellucian.AwardsViewModelMapping.sapStatusModel = function(data) {
    var self = this;
    ko.mapping.fromJS(data, awardsViewModelMapping, self);

    self.sapStatusMessage = ko.computed(function () {
        var status = self.AcademicProgressStatus.Description();
        var highlightedText = '<span id="highlighted-text">' + faSharedAcademicProgressMessageHighlightedText + '</span>';
        if (self.AcademicProgressStatus.Category() === 'Unsatisfactory' || self.AcademicProgressStatus.Category() === 'Warning') {
            return faSharedUnsatisfactoryAcademicProgressMessage.format(highlightedText, status);
        }
        else return faSharedSatisfactoryAcademicProgressMessage.format(highlightedText, status);
    });
}

function parseANumber(value) {
    value = value || 0;
    var parsedNumber = (isNaN(value)) ? Globalize.parseFloat(value) : parseFloat(value);
    return parsedNumber;
}


