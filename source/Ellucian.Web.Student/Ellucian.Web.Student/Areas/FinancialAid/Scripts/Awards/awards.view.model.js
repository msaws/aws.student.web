﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
function awardsViewModel() {

    var self = this;

    // Is there an ajax request being processed that should block the page?
    self.isLoading = ko.observable(false);

    // Spinner message
    self.SpinnerMessage = ko.observable(faAwardsSubmittingChangesMessage);

    //Flag set to true in my.awards.js after data is loaded into this object
    self.showUI = ko.observable(false);

    //Observable to toggle enabling/disabling control elements on the page
    self.acceptRejectInProgress = ko.observable(false);
    
    //"Inherit" the Year Selector. Initial year set in my.awards.js after data is loaded
    yearSelector.call(self);

    BaseViewModel.call(self, 900);

    sharedDataViewModel.call(self);

    self.changeToMobile = function () { }

    self.changeToDesktop = function () { }

    //The full set of awards across all years
    self.StudentAwards = ko.observableArray();

    //The full set of subsidized loans across all years
    self.StudentSubLoans = ko.observableArray();

    //The full set of unsubsidized loans across all years
    self.StudentUnsubLoans = ko.observableArray();

    //The full set of other loans across all years
    self.OtherLoans = ko.observableArray();

    //The full set of grad plus loans across all years
    self.StudentGradPlusLoans = ko.observableArray();

    //Loan Summary
    self.LoanRequirements = ko.observable();

    //Student/Applicant data
    self.Person = ko.observable();
    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();

    //Indicates whether the user is self or a proxy or admin
    self.IsUserSelf = ko.observable();
    
    //Award prerequisites
    self.AwardPrerequisites = ko.observableArray();

    //Configuration object containing list of AwardYearConfigurations
    //specific configuration for award year is computed in year.selector.view.model.js
    self.Configuration = ko.observable();

    //List of Award percentage chart collections
    self.AwardPercentageCharts = ko.observableArray();

    self.AwardLetterData = ko.observableArray();

    self.loanAmountChangeOccurred = ko.observable(false);

    self.awardChangeOccurred = ko.observable(false);

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(awardsAdminActionUrl);
    
    // Flag indicating if student has satisfied prerequisites for the selected year
    self.arePrerequisitesSatisfiedForSelectedYear = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return true;

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var matchingPrerequisites = ko.utils.arrayFirst(self.AwardPrerequisites(), function (prereq) {
            return (prereq.AwardYearCode() == selectedYearCode);
        });

        return matchingPrerequisites !== null ? ko.utils.unwrapObservable(matchingPrerequisites.AreAllPrerequisitesSatisfied()) : true;
    });

    //Flag indicating whether checklist items were assigned for the year
    self.areChecklistItemsAssigned = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return false;

        return ko.utils.unwrapObservable(self.selectedYear().AreChecklistItemsAssigned);
    });

    //Calculate StudentAwards for the current year
    self.selectedAwardsForYear = ko.computed(function () {
        //selectedYear comes from inherited YearSelector object

        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return [];

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var matchingAwards = ko.utils.arrayFirst(self.StudentAwards(), function (studentAwardCollection) {
            return (studentAwardCollection.AwardYearCode() == selectedYearCode);
        });

        return (matchingAwards !== null) ? ko.utils.unwrapObservable(matchingAwards).StudentAwardsForYear() : [];
    });

    self.hasAwardsForYear = ko.computed(function () {
        if (self.selectedAwardsForYear().length > 0) return true;
        else return false;
    });

    //Calculate StudentFWSAwards for the current year
    self.selectedWorkAwardsForYear = ko.computed(function () {
        //selectedYear comes from inherited YearSelector object

        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return [];

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var matchingAwards = ko.utils.arrayFirst(self.StudentAwards(), function (studentAwardCollection) {
            return (studentAwardCollection.AwardYearCode() == selectedYearCode);
        });

        return (matchingAwards !== null) ? ko.utils.unwrapObservable(matchingAwards).StudentWorkAwardsForYear() : [];
    });

    self.hasWorkAwardsForYear = ko.computed(function () {
        if (self.selectedWorkAwardsForYear().length > 0) return true;
        else return false;
    });

    //Calculate OtherLoans for the current year
    self.selectedOtherLoansForYear = ko.computed(function () {
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return [];

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var matchingOtherLoans = ko.utils.arrayFirst(self.OtherLoans(), function (studentAwardCollection) {
            return (studentAwardCollection.AwardYearCode() == selectedYearCode);
        });

        return (matchingOtherLoans !== null) ? ko.utils.unwrapObservable(matchingOtherLoans.StudentAwardsForYear()) : [];
    });

    self.hasOtherLoansForYear = ko.computed(function () {
        if (self.selectedOtherLoansForYear().length > 0) return true;
        else return false;
    });

    self.selectedSubLoansModelForYear = ko.computed(function () {

        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return true;

        var selectedYearCode = self.selectedYear().Code();
        var matchingSubLoansModel = ko.utils.arrayFirst(self.StudentSubLoans(), function (subLoanCollection) {
            return (subLoanCollection.AwardYearCode() == selectedYearCode);
        });

        return matchingSubLoansModel !== null ? ko.utils.unwrapObservable(matchingSubLoansModel) : true;

    });

    self.hasSubLoanForYear = ko.computed(function () {
        if (self.selectedSubLoansModelForYear() !== true) return true;
        else return false;
    });

    //Flag indicating whether any of the subloans for the year is pending
    self.isSubLoanPending = ko.computed(function () {
        if (self.hasSubLoanForYear()) {
            var loans = self.selectedSubLoansModelForYear().Loans();
            
            for (var i = 0; i < loans.length; i++) {
                if (loans[i].AwardStatus.Category() === "Estimated" || loans[i].AwardStatus.Category() === "Pending") return true;
            }
        }
        return false;
    });

    //Calculate unsubsidized loans for the current year
    self.selectedUnsubLoansModelForYear = ko.computed(function () {
        //selectedYear comes from inherited YearSelector object

        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return true;

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());

        var matchingUnsubLoansModel = ko.utils.arrayFirst(self.StudentUnsubLoans(), function (unsubLoanCollection) {
            return (unsubLoanCollection.AwardYearCode() == selectedYearCode);
        });

        return matchingUnsubLoansModel !== null ? ko.utils.unwrapObservable(matchingUnsubLoansModel) : true;
    });

    self.hasUnsubLoanForYear = ko.computed(function () {
        if (self.selectedUnsubLoansModelForYear() !== true) return true;
        else return false;
    });

    //Calculate grad plus loans for the current year
    self.selectedGradPlusLoansModelForYear = ko.computed(function () {
        //selectedYear comes from inherited YearSelector object

        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return true;

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());

        var matchingGradPlusLoansModel = ko.utils.arrayFirst(self.StudentGradPlusLoans(), function (gradPlusLoanCollection) {
            return (gradPlusLoanCollection.AwardYearCode() == selectedYearCode);
        });

        return matchingGradPlusLoansModel !== null ? ko.utils.unwrapObservable(matchingGradPlusLoansModel) : true;
    });

    self.selectedAwardLetterInfoForYear = ko.pureComputed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return null;
        var selectedYearCode = self.selectedYear().Code();
        var matchingLetterData = ko.utils.arrayFirst(self.AwardLetterData(), function (letterData) {
            return (letterData.AwardYearCode() === selectedYearCode);
        });

        return matchingLetterData !== null ? ko.utils.unwrapObservable(matchingLetterData) : null;
    });

    self.hasGradPlusLoansForYear = ko.computed(function () {
        if (self.selectedGradPlusLoansModelForYear() !== true) return true;
        else return false;
    });

    self.hasAnyAwardTypesForYear = ko.computed(function () {
        if (self.hasAwardsForYear() || self.hasWorkAwardsForYear() || self.hasOtherLoansForYear() || self.hasSubLoanForYear() || self.hasUnsubLoanForYear() || self.hasGradPlusLoansForYear())
            return true;
        return false;
    });

    self.hasAnyLoanTypeForYear = ko.computed(function () {
        if (self.hasOtherLoansForYear() || self.hasSubLoanForYear() || self.hasUnsubLoanForYear() || self.hasGradPlusLoansForYear())
            return true;
        return false;
    });

    self.hasDirectLoanRequirements = ko.computed(function () {
        if (self.hasSubLoanForYear()) {
            if (self.selectedSubLoansModelForYear().StatusDescription() != "Rejected" && self.selectedSubLoansModelForYear().StatusDescription() != "Denied") {
                return true;
            }
        }
        if (self.hasUnsubLoanForYear()) {
            if (self.selectedUnsubLoansModelForYear().StatusDescription() != "Rejected" && self.selectedUnsubLoansModelForYear().StatusDescription() != "Denied") {
                return true;
            }
        }
        return false;
    });

    self.hasGradPlusLoanRequirements = ko.computed(function () {
        if (self.hasGradPlusLoansForYear()) {
            if (self.selectedGradPlusLoansModelForYear().StatusDescription() != "Rejected" && self.selectedGradPlusLoansModelForYear().StatusDescription() != "Denied") {
                return true;
            }
        }
        return false;
    });

    //Observable to indicate whether updates are allowed on yearly or period levels
    self.allowYearLevelUpdatesOnly = ko.computed(function () {
        var configuration = self.yearConfiguration();
        if (configuration === null || typeof configuration === 'undefined') return false;
        return configuration.AllowAnnualAwardUpdatesOnly();
    });

    //Awards percentage chart for the year
    self.awardsPercentageChartForYear = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return null;

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var matchingChartCollection = ko.utils.arrayFirst(self.AwardPercentageCharts(), function (chartCollection) {
            return (chartCollection.AwardYearCode() == selectedYearCode);
        });
        
        return matchingChartCollection !== null ? ko.utils.arrayFirst(matchingChartCollection.PercentageChartModels(), function (chart) {
            return (chart.AwardCategoryType() === "Award");
        }) : null;
    });

    self.hasAwardsPercentageChartForYear = ko.computed(function () {
        if (self.awardsPercentageChartForYear()) return true;
        else return false;
    });

    //Work awards percentage chart for the year
    self.workAwardsPercentageChartForYear = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return null;

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var matchingChartCollection = ko.utils.arrayFirst(self.AwardPercentageCharts(), function (chartCollection) {
            return (chartCollection.AwardYearCode() == selectedYearCode);
        });

        return matchingChartCollection !== null ? ko.utils.arrayFirst(matchingChartCollection.PercentageChartModels(), function (chart) {
            return (chart.AwardCategoryType() === "Work");
        }) : null;
    });

    self.hasWorkAwardsPercentageChartForYear = ko.computed(function () {
        if (self.workAwardsPercentageChartForYear()) return true;
        else return false;
    });

    //Loans percentage chart for the year
    self.loansPercentageChartForYear = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return null;

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var matchingChartCollection = ko.utils.arrayFirst(self.AwardPercentageCharts(), function (chartCollection) {
            return (chartCollection.AwardYearCode() == selectedYearCode);
        });

        return matchingChartCollection !== null ? ko.utils.arrayFirst(matchingChartCollection.PercentageChartModels(), function (chart) {
            return (chart.AwardCategoryType() === "Loan");
        }) : null;
    });

    self.hasLoansPercentageChartForYear = ko.computed(function () {
        if (self.loansPercentageChartForYear()) return true;
        else return false;
    });

    //Calculate the total amount for scholarships and grants
    self.awardsTotalAmountForYear = ko.computed(function () {
        var total = 0;
        if (self.hasAwardsForYear()) {
            var awardsForYear = ko.utils.unwrapObservable(self.selectedAwardsForYear());
            ko.utils.arrayForEach(awardsForYear, function (award) {
                total += award.totalAmount();
            });
        }
        return total;
    });

    //Calculate the total amount for work awards
    self.workAwardsTotalAmountForYear = ko.computed(function () {
        var total = 0;
        if (self.hasWorkAwardsForYear()) {
            var workAwardsForYear = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
            ko.utils.arrayForEach(workAwardsForYear, function (award) {
                total += award.totalAmount();
            });
        }
        return total;
    });

    //Calculate the total amount for all loans
    self.loansTotalAmountForYear = ko.computed(function () {
        var total = 0;
        if (self.hasAnyLoanTypeForYear()) {
            var otherLoansForYear = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());
            ko.utils.arrayForEach(otherLoansForYear, function (award) {
                total += award.totalAmount();
            });
            total += self.totalLoanAmount();
        }
        return total;
    });

    //distinct Award Periods for the Year. Sorted by startDate in the c# view model.
    self.distinctAwardPeriodsForYear = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return true;
        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());
        var awardYear = ko.utils.arrayFirst(ko.utils.unwrapObservable(self.AwardYears()), function (year) {
            return (year.Code() == selectedYearCode);
        });
        //gives time for the observable to be computed
        if (awardYear === null || typeof awardYear === 'undefined') return true;
        return awardYear.DistinctAwardPeriods;
    });

    //calculates the total award period amount
    self.totalAwardPeriodAmount = function (period) {
        var awards = ko.utils.unwrapObservable(self.selectedAwardsForYear());
        var workAwards = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
        var subLoanCollection = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear());
        var unsubLoanCollection = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
        var gradPlusLoanCollection = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());
        var otherLoanCollection = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());

        var total = 0;
        ko.utils.arrayForEach(awards, function (studentAward) {
            var amt = studentAward.totalAwardPeriodAmount(period);
            if (amt === null) { amt = 0; }
            total += amt;
        });

        ko.utils.arrayForEach(workAwards, function (studentAward) {
            var amt = studentAward.totalAwardPeriodAmount(period);
            if (amt === null) { amt = 0; }
            total += amt;
        });

        //Other loans are treated as award object
        ko.utils.arrayForEach(otherLoanCollection, function (loan) {
            var amt = loan.totalAwardPeriodAmount(period);
            if (amt === null) { amt = 0; }
            total += amt;
        });

        if (self.hasSubLoanForYear()) {
            total += subLoanCollection.totalAwardPeriodAmount(period);
        }

        if (self.hasUnsubLoanForYear()) {
            total += unsubLoanCollection.totalAwardPeriodAmount(period);
        }

        if (self.hasGradPlusLoansForYear()) {
            total += gradPlusLoanCollection.totalAwardPeriodAmount(period);
        }

        return total;

    }

    //check if the student has any unaccepted awards/loans
    self.hasUnacceptedAwards = ko.computed(function () {
        var selectedYearAwards = ko.utils.unwrapObservable(self.selectedAwardsForYear());
        var selectedYearFWSAwards = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
        var selectedYearSubLoanCollection = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear());
        var selectedYearUnsubLoanCollection = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
        var selectedYearGradPlusLoanCollection = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());
        var selectedYearOtherLoanCollection = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());

        var unacceptedStudentAwards = [];
        ko.utils.arrayFirst(selectedYearAwards, function (award) {
            var category = ko.utils.unwrapObservable(award.AwardStatus.Category());
            if (category === "Pending" || category === "Estimated") {
                unacceptedStudentAwards.push(award);
            }
        });

        ko.utils.arrayFirst(selectedYearFWSAwards, function (award) {
            var category = ko.utils.unwrapObservable(award.AwardStatus.Category());
            if (category === "Pending" || category === "Estimated") {
                unacceptedStudentAwards.push(award);
            }
        });

        //Other loans are treated as award objects
        ko.utils.arrayFirst(selectedYearOtherLoanCollection, function (loan) {
            var category = ko.utils.unwrapObservable(loan.AwardStatus.Category());
            if (category === "Pending" || category == "Estimated") {
                unacceptedStudentAwards.push(loan);
            }
        });

        //Check if there are unaccepted among awards - we can stop early
        if (unacceptedStudentAwards.length > 0) return true;

        if (self.hasSubLoanForYear()) {
            if (selectedYearSubLoanCollection.hasPendingLoans()) return true;
        }

        if (self.hasUnsubLoanForYear()) {
            if (selectedYearUnsubLoanCollection.hasPendingLoans()) return true;
        }

        if (self.hasGradPlusLoansForYear()) {
            if (selectedYearGradPlusLoanCollection.hasPendingLoans()) return true;
        }

        else return false;

    });

    /*Flag that indicates if there are any errors that could prevent acceptance of awards/loans
        Potential errors:
            1. Any of the loans in associated loan collections exceed the maximum loan amount
            2. Any of the loan collections total amount equals zero while a loan(s) can still be modified
              status/amount-wise
            3. Any of the individual awards/some loans(not grouped in loan collections) has a total amount of zero 
               and is status modifiable
    */
    self.hasAwardsWithErrors = ko.computed(function () {
        if (self.hasSubLoanForYear()) {
            var subLoanCollection = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear());            
            if (!subLoanCollection.passesLimitationCriteria()) return true;
            if (subLoanCollection.totalAmount() === 0 && (subLoanCollection.IsStatusModifiable() || subLoanCollection.IsAnyAmountModifiable())) return true;
        }

        if (self.hasUnsubLoanForYear()) {
            var unsubLoanCollection = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
            if (!unsubLoanCollection.passesLimitationCriteria()) return true;
            if (unsubLoanCollection.totalAmount() === 0 && (unsubLoanCollection.IsStatusModifiable() || unsubLoanCollection.IsAnyAmountModifiable())) return true;
        }

        if (self.hasGradPlusLoansForYear()) {
            var gradPlusLoanCollection = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());
            if (!gradPlusLoanCollection.passesLimitationCriteria()) return true;
            if (gradPlusLoanCollection.totalAmount() === 0 && (gradPlusLoanCollection.IsStatusModifiable() || gradPlusLoanCollection.IsAnyAmountModifiable())) return true;
        }

        if (self.hasAwardsForYear()) {
            var awardsForYear = ko.utils.unwrapObservable(self.selectedAwardsForYear());
            var awardWithZeroAmount = ko.utils.arrayFirst(awardsForYear, function (award) {
                return (award.totalAmount() === 0 && award.IsStatusModifiable());
            });
            if (awardWithZeroAmount !== null) return true;
        }

        if (self.hasWorkAwardsForYear()) {
            var workAwardsForYear = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
            var awardWithZeroAmount = ko.utils.arrayFirst(workAwardsForYear, function (award) {
                return (award.totalAmount() === 0 && award.IsStatusModifiable());
            });
            if (awardWithZeroAmount !== null) return true;
        }

        if (self.hasOtherLoansForYear()) {
            var otherLoansForYear = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());
            var otherLoanWithZeroAmount = ko.utils.arrayFirst(otherLoansForYear, function (loan) {
                return (loan.totalAmount() === 0 && loan.IsStatusModifiable());
            });
            if (otherLoanWithZeroAmount !== null) return true;
        }

        return false;
    });

    //Calculates if any of the awards/loans for the year have IsStatusModifiable flag set to true
    self.isAnyStatusModifiable = ko.computed(function () {
        var selectedAwardsForYear = ko.utils.unwrapObservable(self.selectedAwardsForYear());
        var selectedFWSAwardForYear = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
        var selectedOtherLoansForYear = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());
        var selectedYearSubLoanCollection = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear());
        var selectedYearUnsubLoanCollection = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
        var selectedYearGradPlusLoanCollection = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());

        if (self.hasSubLoanForYear()) { if (selectedYearSubLoanCollection.IsStatusModifiable()) { return true; } }
        if (self.hasUnsubLoanForYear()) { if (selectedYearUnsubLoanCollection.IsStatusModifiable()) { return true; } }
        if (self.hasGradPlusLoansForYear()) { if (selectedYearGradPlusLoanCollection.IsStatusModifiable()) { return true; } }

        awardWithStatusModifiable = ko.utils.arrayFilter(selectedAwardsForYear, function (award) {
            return award.IsStatusModifiable() === true;
        });
        if (awardWithStatusModifiable !== null && awardWithStatusModifiable.length > 0) return true;

        workAwardsWithStatusModifiable = ko.utils.arrayFilter(selectedFWSAwardForYear, function (award) {
            return award.IsStatusModifiable() === true;
        });
        if (workAwardsWithStatusModifiable !== null && workAwardsWithStatusModifiable.length > 0) return true;

        loansWithStatusModifiable = ko.utils.arrayFilter(selectedOtherLoansForYear, function (loan) {
            return loan.IsStatusModifiable() === true;
        });
        if (loansWithStatusModifiable !== null && loansWithStatusModifiable.length > 0) return true;

        return false;
    });

    //Holds the number of columns in the award table for the year
    //Equals the number of award periods in the selected year plus 3 columns
    //for Award, Status, and Total Amount
    self.tableColumnCount = ko.computed(function () {
        if (self.distinctAwardPeriodsForYear() === null || typeof self.distinctAwardPeriodsForYear() === 'undefined' || self.distinctAwardPeriodsForYear() === true) return true;
        var periods = ko.utils.unwrapObservable(self.distinctAwardPeriodsForYear());
        return periods.length + 3;
    });

    self.totalLoanAmount = ko.computed(function () {
        var subLoanCollection = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear());
        var unsubLoanCollection = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
        var gradPlusLoanCollection = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());
        var total = 0;

        if (subLoanCollection !== true) {
            total += subLoanCollection.totalAmount();
        }
        if (unsubLoanCollection !== true) {
            total += unsubLoanCollection.totalAmount();
        }
        if (gradPlusLoanCollection !== true) {
            total += gradPlusLoanCollection.totalAmount();
        }

        return total;
    });

    //Total amount to display in the Total section of the awards table
    self.totalAmount = ko.computed(function () {
        var awardsForYear = ko.utils.unwrapObservable(self.selectedAwardsForYear());
        var workAwardsForYear = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
        var subLoanCollection = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear());
        var unsubLoanCollection = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
        var gradPlusLoanCollection = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());
        var otherLoanCollection = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());
        var total = 0;

        ko.utils.arrayForEach(awardsForYear, function (award) {
            total += award.totalAmount();
        });

        ko.utils.arrayForEach(workAwardsForYear, function (award) {
            total += award.totalAmount();
        });

        //Other loans are accounted for as awards
        ko.utils.arrayForEach(otherLoanCollection, function (loan) {
            total += loan.totalAmount();
        });

        if (subLoanCollection !== true) {
            total += subLoanCollection.totalAmount();
        }
        if (unsubLoanCollection !== true) {
            total += unsubLoanCollection.totalAmount();
        }
        if (gradPlusLoanCollection !== true) {
            total += gradPlusLoanCollection.totalAmount();
        }

        return total;
    });

    //Indicates the state of the accordion
    self.totalAccordionIsOpen = ko.observable(false);

    //functions for the total row accordion since the total row isn't actually a StudentAward object    
    self.flipTotalAccordion = function () {
        if (self.totalAccordionIsOpen()) {
            self.totalAccordionIsOpen(false);
        } else {
            self.totalAccordionIsOpen(true);
        }
        return false;
    }

    self.totalAccordionRows = ko.computed(function () {
        return (self.totalAccordionIsOpen()) ? 4 : 2;
    });

    self.awardPeriodColumnWidth = ko.computed(function () {
        var width;
        switch (self.tableColumnCount()) {
            case 3:
                width = 0.00;
                break;
            default:
                width = (55 / (self.tableColumnCount() - 3));
        }
        return width.toString() + "%";
    });

    self.currencySymbol = ko.computed(function () {
        return Globalize.culture().numberFormat.currency.symbol;
    });

    //When the user clicks the cancel button, we want to reset the award
    //back to its original state
    self.cancelHandler = function (award, event) {
        if (award.isLoan) {
            //If any values were "erased" on click in one of the input boxes, the focus will stay in the box 
            //and will not react to reset - so repopulate the input box if empty
            $(".period-loan-amount").each(function () {
                if (this.value === ' ') this.value = ko.dataFor(this).loanChangeAmount();
            });
            var loans = award.Loans();
            for (var i = 0; i < loans.length ; i++) {
                var periods = ko.utils.unwrapObservable(loans[i].StudentAwardPeriods);
                for (var k = 0; k < periods.length; k++) {
                    periods[k].loanChangeAmount.extend({ rateLimit: { timeout: 0 } });
                    if (periods[k].AwardStatus.Category() === "Rejected" ||
                        periods[k].AwardStatus.Category() === "Denied") {
                        periods[k].AwardAmount(0);
                    }
                    else {
                        periods[k].AwardAmount(periods[k].originalAmount());
                    }
                    periods[k].loanChangeAmount(periods[k].AwardAmount());
                    periods[k].IsActive(periods[k].originalIsActive());
                    periods[k].loanChangeAmount.extend({ rateLimit: { timeout: 1000, method: "notifyWhenChangesStop" } });
                }
            }
            /* loanPeriod.IsActive(different from each loan's IsActive attr) is 
            what makes a checkbox checked/unchecked for loans */

            //Obsolete after intro of action panel??
            /*var loanPeriods = ko.utils.unwrapObservable(award.LoanPeriods());
            for (var i = 0; i < loanPeriods.length; i++) {
                loanPeriods[i].IsActive(loanPeriods[i].originalIsActive());
            }*/
            
        }
        else {
            var periods = ko.utils.unwrapObservable(award.StudentAwardPeriods);
            for (var i = 0; i < periods.length; i++) {
                periods[i].AwardAmount(periods[i].originalAmount());
                periods[i].IsActive(periods[i].originalIsActive());
            }
        }
    }

    //Update a loan collection
    self.acceptRejectLoanCollectionHandler = function (loanCollection, event) {

        if (loanCollection === null || typeof loanCollection === 'undefined') return false;
        
        var awardYear = loanCollection.AwardYearCode();

        //Current year student awards configuration
        var configuration = self.yearConfiguration();

        //Default value is specified in Value attribute in View.
        //Note, the default values are defined in the resource file
        var clickedButton = event.currentTarget.name;

        var message = "";
        switch (clickedButton) {
            case "Accept":
                message = faAwardsAcceptSpinnerMessage;                
                break;
            case "Change":
                message = faAwardsSubmittingChangesMessage;                
                break;
            case "Reject":
                message = faAwardsRejectSpinnerMessage;                
                break;
            default:
                break;
        }

        if (clickedButton === "Accept" || clickedButton === "Change") {
            if (!loanCollection.passesLimitationCriteria()) return false;
        }

        var jsonData = { 'loanCollection': ko.toJSON(loanCollection), 'action': ko.toJSON(clickedButton) };

        self.acceptRejectInProgress(true);

        var isErrorThrown = false;

        $.ajax({
            url: updateLoanCollectionActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                if (typeof message === 'string') {
                    //$("#loadingdialog-text").text(message);
                    self.SpinnerMessage(message);
                }
                self.isLoading(true);
            },
            success: function (data) {
                /*The reason we are looping through all loan categories is 
                that we need to update loan limitations for each */
                if (!account.handleInvalidSessionResponse(data)) {
                    //self.updateStudentLoansForYear(awardYear, data);
                    self.updateAwardsFromViewModel(awardYear, data);
                    loanCollection.loanCollectionUpdated(true);
                }

                var loanType = loanCollection.LoanCollectionType() == "UnsubsidizedLoan" ? faAwardsUnsubsidizedLoanType :
                    loanCollection.LoanCollectionType() == "SubsidizedLoan" ? faAwardsSubsidizedLoanType : faAwardsGradPlusLoanType;
                if (clickedButton == "Accept" && !loanCollection.IsLoanCollectionInReview())
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsLoanAcceptedMessage.format(loanType || ''), type: "success", flash: true });
                else if (clickedButton == "Change" && !configuration.IsLoanAmountChangeReviewRequired())
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsLoanAmountSuccessfullyChanged.format(loanType || ''), type: "success", flash: true });
                else if ((clickedButton == "Reject" && configuration.IsDeclinedStatusChangeReviewRequired()) || (((clickedButton == "Accept" && loanCollection.IsLoanCollectionInReview()) || clickedButton == "Change") && configuration.IsLoanAmountChangeReviewRequired()))
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsLoanPlacedInReviewMessage.format(loanType || ''), type: "success", flash: true });
                else if (clickedButton == "Reject" && !configuration.IsDeclinedStatusChangeReviewRequired())
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsLoanDeclinedMessage.format(loanType || ''), type: "success", flash: true });
            },

            error: function (jqXHR, textStatus, errorThrown) {
                isErrorThrown = true;
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsErrorUpdatingAwardMessage, type: "error", flash: true });
            },

            complete: function (jqXHR, textStatus) {
                // Accordion content must be rendered before its controller because their IDs are dynamic;
                // this will move accordion content beneath its controller for proper accordion show/hide.
                rearrangeMultiAccordionElements($(".multi-accordion-content"));
                self.acceptRejectInProgress(false);
                self.isLoading(false);

                if (!isErrorThrown) {
                    loanCollection.flipAccordion();
                }

                if (clickedButton === "Change") self.loanAmountChangeOccurred(true);
                else self.awardChangeOccurred(true);
                self.displayPopUpDialog();
            }
        });

        return false;
    }

    //Update a separate award/other loan
    self.acceptRejectHandler = function (award, event) {

        if (award === null || typeof award === 'undefined') return true;

        //Default value is specified in Value attribute in View.
        //Note, the default values are defined in the resource file
        var clickedButton = event.currentTarget.name;

        var message = "";
        switch (clickedButton) {
            case "Accept":
                message = faAwardsAcceptSpinnerMessage;
                break;
            case "Reject":
                message = faAwardsRejectSpinnerMessage;
                break;
            default:
                break;
        }

        //Current year student awards configuration
        var configuration = self.yearConfiguration();

        var jsonData = { 'award': ko.toJSON(award), 'action': ko.toJSON(clickedButton) };

        self.acceptRejectInProgress(true);

        var isErrorThrown = false;

        $.ajax({
            url: acceptStudentAwardActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                if (typeof message === 'string') {
                    //$("#loadingdialog-text").text(message);
                    self.SpinnerMessage(message);
                }
                self.isLoading(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {

                    self.updateAwardsFromViewModel(self.selectedYear().Code(), data);
                    award.awardUpdated(true);
                }

                var awardDescription = award.Description() != null ? award.Description() : "Award";
                if (clickedButton == "Accept")
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsAwardAcceptedMessage.format(awardDescription || ''), type: "success", flash: true });
                else if (clickedButton == "Reject" && !configuration.IsDeclinedStatusChangeReviewRequired())
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsAwardDeclinedMessage.format(awardDescription || ''), type: "success", flash: true });
                else if (clickedButton == "Reject" && configuration.IsDeclinedStatusChangeReviewRequired())
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsAwardPlacedInReviewMessage.format(awardDescription || ''), type: "success", flash: true });
            },

            error: function (jqXHR, textStatus, errorThrown) {
                isErrorThrown = true;
                console.debug("Award acceptance failed: " + jqXHR.status + " " + jqXHR.responseText);
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsErrorUpdatingAwardMessage, type: "error", flash: true });
            },

            complete: function (jqXHR, textStatus) {
                // Accordion content must be rendered before its controller because their IDs are dynamic;
                // this will move accordion content beneath its controller for proper accordion show/hide.
                rearrangeMultiAccordionElements($(".multi-accordion-content"));
                self.acceptRejectInProgress(false);
                self.isLoading(false);
                if (!isErrorThrown) {
                    award.flipAccordion();
                }

                self.awardChangeOccurred(true);
                self.displayPopUpDialog();
            }

        });

    }

    //Accept/reject all pending awards and loans
    self.acceptRejectAllHandler = function (na, event) {

        var clickedButton = event.currentTarget.name;
        if ((clickedButton == "Accept") && (self.hasAwardsWithErrors())) return true;

        var message = "";
        switch (clickedButton) {
            case "Accept":
                message = faAwardsAcceptAllSpinnerMessage;
                break;
            case "Reject":
                message = faAwardsRejectAllSpinnerMessage;
                break;
            default:
                break;
        }

        self.acceptRejectInProgress(true);

        //Current year student awards configuration
        var configuration = self.yearConfiguration();

        var subLoanCollection = null;
        var unsubLoanCollection = null;
        var gradPlusLoanCollection = null;

        var isSubPending = false;
        var isUnsubPending = false;
        var isGradPlusPending = false;

        var isErrorThrown = false;

        //Get all loans for the year
        if (self.hasSubLoanForYear()) {
            subLoanCollection = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear);
        }


        if (self.hasUnsubLoanForYear()) {
            unsubLoanCollection = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
        }


        if (self.hasGradPlusLoansForYear()) {
            gradPlusLoanCollection = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());
        }

        var unacceptedAwardsAndLoans = [];

        //separate list of just the pending awards (need them for after ajax success)
        var pendingAwards = [];

        //Push all loans into unacceptedAwards array
        if (subLoanCollection != null) {
            if (subLoanCollection.hasPendingLoans()) {
                isSubPending = true;
                ko.utils.arrayForEach(subLoanCollection.Loans(), function (subLoan) {
                    if (subLoan.AwardStatus.Category() == "Pending" || subLoan.AwardStatus.Category() == "Estimated") {
                        unacceptedAwardsAndLoans.push(subLoan);
                    }
                });
            }
        }

        if (unsubLoanCollection != null) {
            if (unsubLoanCollection.hasPendingLoans()) {
                isUnsubPending = true;
                ko.utils.arrayForEach(unsubLoanCollection.Loans(), function (unsubLoan) {
                    if (unsubLoan.AwardStatus.Category() == "Pending" || unsubLoan.AwardStatus.Category() == "Estimated") {
                        unacceptedAwardsAndLoans.push(unsubLoan);
                    }
                });
            }

        }

        if (gradPlusLoanCollection != null) {
            if (gradPlusLoanCollection.hasPendingLoans()) {
                isGradPlusPending = true;
                ko.utils.arrayForEach(gradPlusLoanCollection.Loans(), function (gradPlusLoan) {
                    if (gradPlusLoan.AwardStatus.Category() == "Pending" || gradPlusLoan.AwardStatus.Category() == "Estimated") {
                        unacceptedAwardsAndLoans.push(gradPlusLoan);
                    }
                });
            }
        }

        var selectedYearAwards = ko.utils.unwrapObservable(self.selectedAwardsForYear());
        var selectedYearFWSAwards = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
        var otherLoans = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());

        //Push all pending awards and other loans into unacceptedAwards array
        ko.utils.arrayFilter(selectedYearAwards, function (award) {
            var category = ko.utils.unwrapObservable(award.AwardStatus.Category());
            if (category === "Pending" || category === "Estimated") {
                unacceptedAwardsAndLoans.push(award);
                pendingAwards.push(award);
            };
        });

        //FWS awards
        ko.utils.arrayFilter(selectedYearFWSAwards, function (award) {
            var category = ko.utils.unwrapObservable(award.AwardStatus.Category());
            if (category === "Pending" || category === "Estimated") {
                unacceptedAwardsAndLoans.push(award);
                pendingAwards.push(award);
            };
        });

        //Other loans are treated as award objects
        ko.utils.arrayFilter(otherLoans, function (loan) {
            var category = ko.utils.unwrapObservable(loan.AwardStatus.Category());
            if (category === "Pending" || category === "Estimated") {
                unacceptedAwardsAndLoans.push(loan);
                pendingAwards.push(loan);
            };
        });

        var jsonData = { 'awardsAndLoans': ko.toJSON(unacceptedAwardsAndLoans), 'action': ko.toJSON(clickedButton) };
        var awardYear = unacceptedAwardsAndLoans[0].AwardYearId();

        $.ajax({
            url: updateAllAwardsAndLoansActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                if (typeof message === 'string') {
                    //$("#loadingdialog-text").text(message);
                    self.SpinnerMessage(message);
                }
                self.isLoading(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    //data is the AwardsViewModel containing this year's data

                    self.updateAwardsFromViewModel(awardYear, data);
                }

                //Update the updated observables so appropriate statuses get styled differently
                if (isSubPending) {
                    subLoanCollection.loanCollectionUpdated(true);
                }
                    // mapping does not go into loanCollectionModel in this method, so update this observable manually
                else if (!isSubPending && subLoanCollection != null) {
                    subLoanCollection.loanCollectionUpdated(false);
                }
                if (isUnsubPending) {
                    unsubLoanCollection.loanCollectionUpdated(true);
                }
                    // mapping does not go into loanCollectionModel in this method, so update this observable manually
                else if (!isUnsubPending && unsubLoanCollection != null) {
                    unsubLoanCollection.loanCollectionUpdated(false);
                }
                if (isGradPlusPending) {
                    gradPlusLoanCollection.loanCollectionUpdated(true);
                }
                    // mapping does not go into loanCollectionModel in this method, so update this observable manually
                else if (!isGradPlusPending && gradPlusLoanCollection != null) {
                    gradPlusLoanCollection.loanCollectionUpdated(false);
                }

                //Need to get this again, since the structure is a bit different than that of loanCollections
                //and thus behaves differently
                var selectedAwards = ko.utils.unwrapObservable(self.selectedAwardsForYear());
                var selectedFWSAwards = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
                var selectedOtherLoans = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());

                //Use the codes from the pendingAwards list to get awards that were pending to bold out their statuses
                for (var j = 0; j < pendingAwards.length; j++) {
                    for (var i = 0; i < selectedAwards.length; i++) {
                        if (selectedAwards[i].Code() == pendingAwards[j].Code()) {
                            selectedAwards[i].awardUpdated(true);
                        }
                    }
                    for (var i = 0; i < selectedFWSAwards.length; i++) {
                        if (selectedFWSAwards[i].Code() == pendingAwards[j].Code()) {
                            selectedFWSAwards[i].awardUpdated(true);
                        }
                    }
                    for (var k = 0; k < selectedOtherLoans.length; k++) {
                        if (selectedOtherLoans[k].Code() == pendingAwards[j].Code()) {
                            selectedOtherLoans[k].awardUpdated(true);
                        }
                    }
                }
                if (clickedButton == "Accept")
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsAcceptAllSuccessMessage, type: "success", flash: true });
                else if (clickedButton == "Reject" && !configuration.IsDeclinedStatusChangeReviewRequired())
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsDeclineAllSuccessMessage, type: "success", flash: true });
                else if (clickedButton == "Reject" && configuration.IsDeclinedStatusChangeReviewRequired())
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsMultipleAwardsPlacedInReviewMessage, type: "success", flash: true });
            },

            error: function (jqXHR, textStatus, errorThrown) {
                isErrorThrown = true;
                console.debug("Award acceptance failed: " + jqXHR.status + " " + jqXHR.responseText);
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardsAcceptDeclineAllFailureMessage, type: "error", flash: true });
            },

            complete: function (jqXHR, textStatus) {
                // Accordion content must be rendered before its controller because their IDs are dynamic;
                // this will move accordion content beneath its controller for proper accordion show/hide.
                rearrangeMultiAccordionElements($(".multi-accordion-content"));
                self.acceptRejectInProgress(false);
                self.isLoading(false);
                if (!isErrorThrown) {
                    self.flipTotalAccordion();
                }

                self.awardChangeOccurred(true);
                self.displayPopUpDialog();
            }

        });
    }

    //Updates awards and loans for the selected year 
    //if the ajax request to accept/decline/change loan amount is successful
    self.updateAwardsFromViewModel = function (awardYear, data) {
        self.isLoading(false);
        var selectedAwardsAndOtherLoansForYear =
            self.selectedAwardsForYear().
            concat(self.selectedWorkAwardsForYear()).
            concat(self.selectedOtherLoansForYear());

        data.StudentAwards.
            flatMap(function (studentAwardCollection) {
                return studentAwardCollection.StudentAwardsForYear;
            }).
            concat(data.StudentAwards.
                flatMap(function (studentAwardCollection) {
                    return studentAwardCollection.StudentWorkAwardsForYear;
                })).
            concat(data.OtherLoans.
                flatMap(function (studentAwardCollection) {
                    return studentAwardCollection.StudentAwardsForYear;
                })
            ).
            forEach(function (studentAwardData) {
                selectedAwardsAndOtherLoansForYear.
                    filter(function (selectedAward) {
                        return selectedAward.Code() === studentAwardData.Code;
                    }).
                    forEach(function (selectedAward) {
                        ko.mapping.fromJS(studentAwardData, selectedAward);
                    });
            });

        var loanCollectionsForYear =
             self.StudentSubLoans().
             concat(self.StudentUnsubLoans()).
             concat(self.StudentGradPlusLoans()).
                 filter(function (loanCollection) {
                     return loanCollection.AwardYearCode() === awardYear;
                 });

        data.StudentSubLoans.
        concat(data.StudentUnsubLoans).
        concat(data.StudentGradPlusLoans).
            filter(function (loanCollectionData) {
                return loanCollectionData.AwardYearCode === awardYear;
            }).
            forEach(function (loanCollectionData) {
                loanCollectionsForYear.
                    filter(function (selectedLoanCollection) {
                        return selectedLoanCollection.LoanCollectionType() === loanCollectionData.LoanCollectionType;
                    }).
                    forEach(function (selectedLoanCollection) {
                        ko.mapping.fromJS(loanCollectionData, selectedLoanCollection);
                    });
            });
    }

    //Observable for tracking errors in user input (all loans)
    self.hasErrors = ko.observable(false);

    //Check validation errors when new amount is entered (award period level), capture appropriate message
    //We are updating the award amount property if there are no errors
    self.processAmountChange = function (parents, loanChangeAmount, awardPeriod) {
        var loanCollection = parents[2];
        var loan = parents[1];
        if (loanChangeAmount.hasError() === true) {
            loan.hasInputErrors(true);
            loanCollection.hasInputErrors(true);
            loan.inputErrorMessage(ko.utils.unwrapObservable(loanChangeAmount.validationMessage));
            self.hasErrors(true);
        }

        else {
            loan.hasInputErrors(false);
            loanCollection.hasInputErrors(false);
            self.hasErrors(false);
            awardPeriod.AwardAmount(parseANumber(loanChangeAmount()));
        }
        var element = document.getElementById(awardPeriod.AwardId() + '_' + awardPeriod.Code());
        window.setTimeout(function () {
           element.focus();
        }, 0);
        
    }

    /*Function that's being triggered on input event whenever totalLoanAmount is changed (when
    * allowYearLevelUpdatesOnly flag is set to true); it checks for input errors and distributes the new 
    * amount among awards' active/modifiable periods
    */
    self.processTotalAmountChange = function (loanCollection, award, totalLoanChangeAmount) {
        var message;
        if (totalLoanChangeAmount.hasError() === true) {
            message = ko.utils.unwrapObservable(totalLoanChangeAmount.validationMessage);
            self.setError(award, loanCollection, message);            
        }
        else {
            award.hasInputErrors(false);
            loanCollection.hasInputErrors(false);
            self.hasErrors(false);

            //Parse the newly entered amount - if it is > 999 - it might contain special numeric separatotrs
            var newAmount = isNaN(totalLoanChangeAmount()) ? Globalize.parseFloat(totalLoanChangeAmount())
                : parseFloat(totalLoanChangeAmount());

            //Get only those periods that are active - not rejected/denied, not dummy, and IsAmountModifiable
            var activePeriods = ko.utils.arrayFilter(award.StudentAwardPeriods(), function (period) {
                return (period.IsActive() && period.IsAmountModifiable());
            });

            //Get inactive/unmodifiable periods
            var restOfPeriods = ko.utils.arrayFilter(award.StudentAwardPeriods(), function (period) {
                return (!period.IsActive() || !period.IsAmountModifiable());
            });

            //Calculate the total amount that cannot be touched - inactive/unmodifiable periods amounts summed
            var totalUntouchableAmount = 0;
            for (var i = 0; i < restOfPeriods.length ; i++) {
                totalUntouchableAmount += restOfPeriods[i].AwardAmount();
            }
            //If the new amount is less than the sum of untouchable periods - let the user know
            //they should enter an amount greater than or equal to the untouchable amount
            if (totalUntouchableAmount > newAmount) {
                message = faAwardsInsufficientAmountMessage.format(self.currencySymbol(), Globalize.format(totalUntouchableAmount, "n2"));
                totalLoanChangeAmount.hasError(true);
                self.setError(award, loanCollection, message);
            }
            else {
                //Now subtract the inactive/unmodifiable periods amount from new amount
                newAmount -= totalUntouchableAmount;

                //Count of active periods
                var periodCount = activePeriods.length;

                //Calculate the integer amount to distribute amoung active periods
                var amountPerPeriod = Math.round(newAmount / periodCount);

                //Calcuate the difference between the total rounded split amount and the requested amount. 
                //This will be subtracted from the last award period.
                var roundingDifference = (amountPerPeriod * periodCount) - newAmount;

                //Update all active/modifiable award periods with the new award amount
                for (var i = 0; i < periodCount; i++) {
                    activePeriods[i].AwardAmount(amountPerPeriod);
                }

                //The amount for the last award period might be a bit different from the rest due to division
                if (periodCount > 0)
                    activePeriods[periodCount - 1].AwardAmount(amountPerPeriod - roundingDifference);
            }
        }        
        //var element = document.getElementById(award.Code()+"Total");
        //window.setTimeout(function () {
        //    element.focus();
        //}, 0);
        
    }

    //Helper function to set error observables and error message
    self.setError = function (award, loanCollection, message) {
        award.hasInputErrors(true);
        loanCollection.hasInputErrors(true);
        award.inputErrorMessage(message);
        self.hasErrors(true);
    }

    //Options for the jqAccordion class. Note jqAccordion is a custom binding in knockout.bindings.js
    self.awardAccordionOptions = { collapsible: true, active: false, autoHeight: false };

    //Computed observable to disable/enable acceptAll/rejectAll buttons
    self.disableBulkAction = ko.computed(function () {
        return (!self.hasUnacceptedAwards()
            || self.acceptRejectInProgress()
            || self.hasErrors()
            || (self.yearConfiguration() && !self.yearConfiguration().AreAwardingChangesAllowed())
            || self.isAdmin()
            || self.IsProxyView());
    });

    /*************  Award letter message related functions  ************/
    self.isAwardLetterForYearAccepted = ko.pureComputed(function () {
        if (self.selectedAwardLetterInfoForYear() === null || typeof self.selectedAwardLetterInfoForYear() === 'undefined') return false;
        else return self.selectedAwardLetterInfoForYear().IsAwardLetterAccepted();
    });
        
    self.isAwardLetterForYearAvailable = ko.pureComputed(function () {
        if (self.selectedAwardLetterInfoForYear() === null || typeof self.selectedAwardLetterInfoForYear() === 'undefined') return false;
        else return self.selectedAwardLetterInfoForYear().IsAwardLetterAvailable();
    });

    self.allPendingAwardPeriodsForYear = ko.pureComputed(function () {
       var allPendingAwardPeriods = [];
        if (self.hasAwardsForYear()) {
            self.getPendingAwardPeriods(self.selectedAwardsForYear(), allPendingAwardPeriods);
        }
        if (self.hasWorkAwardsForYear()) {
            self.getPendingAwardPeriods(self.selectedWorkAwardsForYear(), allPendingAwardPeriods);
        }
        if (self.hasOtherLoansForYear()) {
            self.getPendingAwardPeriods(self.selectedOtherLoansForYear(), allPendingAwardPeriods);
        }
        if (self.hasSubLoanForYear()) {
            self.getPendingAwardPeriods(self.selectedSubLoansModelForYear().Loans(), allPendingAwardPeriods);
        }
        if (self.hasUnsubLoanForYear()) {
            self.getPendingAwardPeriods(self.selectedUnsubLoansModelForYear().Loans(), allPendingAwardPeriods);
        }
        if (self.hasGradPlusLoansForYear()) {
            self.getPendingAwardPeriods(self.selectedGradPlusLoansModelForYear().Loans(), allPendingAwardPeriods);
        }
        return allPendingAwardPeriods;
    });

    self.getPendingAwardPeriods = function (awards, awardPeriodContainer) {
        awards
        .flatMap(function (award) { return award.StudentAwardPeriods() })
        .forEach(function (period) {
            if (period.AwardStatus.Category() === "Pending" || period.AwardStatus.Category() === "Estimated") {
                awardPeriodContainer.push(period);
            }
        });
    }

    self.areAllPendingAwardsIgnoredFromEval = ko.pureComputed(function () {
        var firstNonIgnored = null;
        var pendingPeriods = ko.utils.unwrapObservable(self.allPendingAwardPeriodsForYear());
        if (pendingPeriods !== null && typeof pendingPeriods !== 'undefined' && pendingPeriods.length > 0) {
            firstNonIgnored = ko.utils.arrayFirst(pendingPeriods, function (period) {
                return !period.IsIgnoredOnAwardLetter();
    });
            return (firstNonIgnored === null);
        }
        return false;
    });

    self.areAnyAwardsViewableOnAwardLetter = ko.pureComputed(function () {

            //unwrap and store some of the current observables in variables
            var selectedYearAwards = ko.utils.unwrapObservable(self.selectedAwardsForYear());
            var selectedYearFWSAwards = ko.utils.unwrapObservable(self.selectedWorkAwardsForYear());
            var selectedYearSubLoans = ko.utils.unwrapObservable(self.selectedSubLoansModelForYear());
            var selectedYearUnsubLoans = ko.utils.unwrapObservable(self.selectedUnsubLoansModelForYear());
            var selectedYearGradPlusLoans = ko.utils.unwrapObservable(self.selectedGradPlusLoansModelForYear());
            var selectedYearOtherLoanCollection = ko.utils.unwrapObservable(self.selectedOtherLoansForYear());

        if (self.checkAwardsViewabilityOnAwardLetter(selectedYearAwards)) return true;

        if (self.checkAwardsViewabilityOnAwardLetter(selectedYearFWSAwards)) return true;

            //Other loans are treated as award objects
        if (self.checkAwardsViewabilityOnAwardLetter(selectedYearOtherLoanCollection)) return true;

            if (self.hasSubLoanForYear()) {
            if (self.checkAwardsViewabilityOnAwardLetter(selectedYearSubLoans.Loans())) return true;
            }

            if (self.hasUnsubLoanForYear()) {
            if (self.checkAwardsViewabilityOnAwardLetter(selectedYearUnsubLoans.Loans())) return true;
                    }

        if (self.hasGradPlusLoansForYear()) {
            if (self.checkAwardsViewabilityOnAwardLetter(selectedYearGradPlusLoans.Loans())) return true;
            }

                            return false;
                });

    //Determines if there are any viewable on award letter awards in the provided award collection
    self.checkAwardsViewabilityOnAwardLetter = function(awardCollection){
        var firstViewable = ko.utils.arrayFirst(awardCollection, function (award) {
            for (var i = 0; i < award.StudentAwardPeriods().length; i++) {
                if (award.StudentAwardPeriods()[i].IsViewableOnAwardLetter()) {
                    return true;
            }
        }
    });
        return (firstViewable !== null && typeof firstViewable !== 'undefined');
    };

    self.isAwardLetterReadyToBeSigned = ko.pureComputed(function () {
        if (self.isAwardLetterForYearAvailable() && self.areAnyAwardsViewableOnAwardLetter() && self.IsUserSelf()) {
            if (self.hasUnacceptedAwards() && self.areAllPendingAwardsIgnoredFromEval()) return true;
            else if (!self.hasUnacceptedAwards()) return true;            
            else return false;
        }
        else return false;
    });

    //Flag to keep track of pop up dialog occurence
    self.firstPopUpDialogDisplay = ko.observable(true);
    self.displayAwardLetterReadyPopUpDialog = ko.observable(false);
    self.displayPopUpDialog = function () {
        //if this is the first time the change is occurring, check for conditions and display a popup
        if (self.isAwardLetterReadyToBeSigned() && self.firstPopUpDialogDisplay()) {
            if (self.loanAmountChangeOccurred() && !self.yearConfiguration().IsLoanAmountChangeReviewRequired() && !self.displayAwardLetterReadyMessage()) self.displayAwardLetterReadyPopUpDialog(true);
            else if (self.awardChangeOccurred() && self.selectedAwardLetterInfoForYear().DoesAnyAwardRequireAction()) self.displayAwardLetterReadyPopUpDialog(true);
            else self.displayAwardLetterReadyPopUpDialog(false);
        }
        else self.displayAwardLetterReadyPopUpDialog(false);
    };

    self.displayAwardLetterReadyMessage = ko.pureComputed(function () {
        return (self.isAwardLetterReadyToBeSigned() && !self.isAwardLetterForYearAccepted() && !self.displayAwardLetterReadyPopUpDialog());
    });

    self.closeAwardLetterReadyPopUpDialog = function () {
        self.firstPopUpDialogDisplay(false);    //pop up was displayed - set observable to false so we don't display it again
        self.awardChangeOccurred(false);
        self.loanAmountChangeOccurred(false);
        self.displayAwardLetterReadyPopUpDialog(false);
    };

    self.awardLetterReadyDialogOptions = ko.observable({
        autoOpen: false, modal: true, minWidth: $(window).width() / 4.5,
        show: { effect: "fade", duration: dialogFadeTimeMs }, hide: { effect: "fade", duration: dialogFadeTimeMs }, resizable: false
    });
    
} //end of awardsViewModel

/****These two functions (clearInputField and resetInputField are outside of the
   awardsViewModel since the similar functions  inside the binding did not behave as expected,
   so we are using html tag properties onfocus and onblur to trigger these functions*****/

//Clear input field when user clicks in it for easier amount entry
function clearInputField(target) {
    ko.dataFor(target).isLoanAmountInFocus(true);
    target.value = ' ';    
    return false;
}

//Resets input field to its original value if no amount was entered and the user clicks away
function resetInputField(target) {
    if (target.value === ' ' || target.value === '') {
        target.value = ko.dataFor(target).loanChangeAmountWithPrecision();
    }
    ko.dataFor(target).isLoanAmountInFocus(false);
    return false;
}

