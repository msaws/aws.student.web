﻿/*Copyright 2014-2015 Ellucian Company L.P. and its affiliates */

// Knockout binding for a javascript chart
//===================================================================
// Summary
//===================================================================
// Creates a chart with the provided parameters, uses Chart.js
//===================================================================
// Options
//===================================================================
// - chartType: the type of chart to display. 
//              Currently only two types are supported: pie, doughnut
//              Default: pie
//===================================================================
// Mark-Up examples
//===================================================================
// This will create a doughnut type chart:
//       <canvas data-bind="chart: $data, chartType: 'Doughnut'">
//
// This will create a pie type chart:
//      <canvas data-bind="chart: $data>
//===================================================================
ko.bindingHandlers.chart = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        //Instantiate the Chart class on the canvas we want to draw on. 
        ///This causes Chart.js to handle display scaling.
        var chartContext = $(element).get(0).getContext("2d");
        chart = new Chart(chartContext);
    },

    update: function (element, valueAccessor, allBindingsAccessor) {
        var chartModel = valueAccessor();
        var chartType = allBindingsAccessor().chartType || 'pie';
        if (chartModel === null || typeof chartModel === 'undefined' || chartModel === true) return true;

        var unwrapChartModel = ko.toJS(valueAccessor());

        var data = unwrapChartModel.ChartData;
        var options = unwrapChartModel.ChartOptions;
        if (chartType.toLowerCase() === "doughnut")
        {
            chart.Doughnut(data, options);
        }
        else chart.Pie(data, options);     
    },

    chart: null
};

/*
Extends knockout's built in 'with' binding by setting a timing delay. Essentially a copy of the 'with' binding code with a few tweaks.
Written for Knockout 2.2.1.
Can definitely be re-written and simplified with later versions of knockout.

Bindings:
    delayWith: main parameter - the object that you want to use as the context for binding descendant elements
    switch: optional - A boolean value to determine whether the delay should occur only on add or only on remove - True for add, False for remove. 
        Do not include this binding if you always want to apply the delay
    duration: optional - A string or number representing time in milliseconds indicating how long the delay should last. Default is 500ms
*/
ko.bindingHandlers.delayWith = {
    withIfDomDataKey: '__ko_withIfBindingData',

    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        ko.utils.domData.set(element, ko.bindingHandlers.delayWith.withIfDomDataKey, {});
        return { 'controlsDescendantBindings': true };
    },

    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

        var withIfData = ko.utils.domData.get(element, ko.bindingHandlers.delayWith.withIfDomDataKey),
            dataValue = ko.utils.unwrapObservable(valueAccessor()),
            shouldDisplay = !!dataValue,
            isFirstRender = !withIfData.savedNodes,
            delayDuration = allBindings()['duration'] || 500,
            switchExists = typeof (allBindings()['switch']) !== 'undefined';

        if (isFirstRender) {
            withIfData.savedNodes = ko.bindingHandlers.delayWith.cloneNodes(ko.virtualElements.childNodes(element), true /* shouldCleanNodes */);
        }

        var shouldDisplayFunction = function () {
            if (!isFirstRender) {
                ko.virtualElements.setDomNodeChildren(element, ko.bindingHandlers.delayWith.cloneNodes(withIfData.savedNodes));
            }
            ko.applyBindingsToDescendants(ko.bindingHandlers.delayWith.makeContextCallback(bindingContext, dataValue), element);
        }

        var shouldNotDisplayFunction = function () {
            ko.virtualElements.emptyNode(element);
        }

        if (shouldDisplay) {
            if (switchExists && allBindings()['switch']) {
                window.setTimeout(shouldDisplayFunction, delayDuration);
            } else {
                shouldDisplayFunction();
            }
        } else {
            if (switchExists && !allBindings()['switch']) {
                window.setTimeout(shouldNotDisplayFunction, delayDuration);
            } else {
                shouldNotDisplayFunction();
            }
        }

        withIfData.didDisplayOnLastUpdate = shouldDisplay;
    },

    makeContextCallback: function(context, value) {
        return context['createChildContext'](value);
    },

    cloneNodes: function (nodesArray, shouldCleanNodes) {
        for (var i = 0, j = nodesArray.length, newNodesArray = []; i < j; i++) {
            var clonedNode = nodesArray[i].cloneNode(true);
            newNodesArray.push(shouldCleanNodes ? ko.cleanNode(clonedNode) : clonedNode);
        }
        return newNodesArray;
    }
};

ko.bindingHandlers.checkedInArray = {
    init: function (element, valueAccessor) {
        ko.utils.registerEventHandler(element, "click", function () {
            var options = ko.utils.unwrapObservable(valueAccessor()),
                array = options.array, // don't unwrap array because we want to update the observable array itself
                value = ko.utils.unwrapObservable(options.value),
                checked = element.checked;
            ko.bindingHandlers.checkedInArray.addOrRemoveItem(array, value, checked);
        });
    },

    update: function (element, valueAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor()),
            array = ko.utils.unwrapObservable(options.array),
            value = ko.utils.unwrapObservable(options.value);

        element.checked = ko.utils.arrayIndexOf(array, value) >= 0;
    },

    addOrRemoveItem: function (array, value, included) {
        var existingEntryIndex = array.indexOf ? array.indexOf(value) : ko.utils.arrayIndexOf(array, value);
        if (existingEntryIndex < 0) {
            if (included)
                array.push(value);
        } else {
            if (!included)
                array.splice(existingEntryIndex, 1);
        }
    }
};

/* 
This was copied from immediate.payments.js
numeric : {
    precision: int, precision value; default is 2
    zeroNull: bool, true - empty value equates with zero; false - empty values are replaced with zero; default is false
    maxValue: decimal, maximum value; default is no maximum
    maxMessage: string, custom error message to display when the max value is exceeded (only used if maxValue is specified)
    minValue: decimal, minimum value; default is no minimum
    minMessage: string, custom error message to display when the min value is exceeded (only used if minValue is specified)
    nanMessage: string, custom error message to display if the user-entered value is not a number
    valCondition: function, bool result of function indicates whether or not to validate the target
    observable: ko.observable, if you have an underlying observable to update with the same value of the target observable
    updateOnError: bool, true - update the target (and observable) even if there is an error; default is false
}
*/
ko.extenders.numeric = function (target, arguments) {
    // Process the arguments passed in
    //var precision = arguments.precision || 2;
    var precision = arguments.precision;
    if (isNaN(precision)) {
        precision = 2;
    }

    // Process the argument to use an empty/null value in place of zero
    var zeroNull = arguments.zeroNull || false;
    var zeroFormat = (zeroNull) ? "" : 0;

    // Get maximum value, if any, and its error message
    var maxValue = arguments.max;
    if (maxValue && isNaN(maxValue)) {
        maxValue = undefined;
    }
    if (maxValue) {
        var maxMessage = arguments.maxMessage || "You must enter a value less than {0}".format(maxValue);
    }

    // Get minimum value, if any, and its error message
    var minValue = arguments.min;
    var minMessage = "";
    if (isNaN(minValue)) {
        minValue = undefined;
    } else {
        minMessage = arguments.minMessage || "You must enter a value greater than or equal to {0}.".format(minValue);
    }

    // Get the "not a number" message and the validation condition
    var nanMessage = arguments.nanMessage || "Please enter a valid number.";
    var valCondition = arguments.valCondition;

    var underlyingObservable = arguments.observable;
    var updateOnError = arguments.updateOnError;

    //create a writeable computed observable to intercept writes to our observable
    var result = ko.computed({
        read: function() {
            return Globalize.format(target(), "n" + precision);
        },
        write: function (newValue) {
            //add some sub-observables to our observable if they don't exist
            if (!result.hasError) {
                result.hasError = ko.observable(false);
            }
            if (!result.validationMessage) {
                result.validationMessage = ko.observable("");
            }

            // Get the current value, and turn the new value into a numeric
            var current = target(),
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = parseANumber(newValue);
            // Get the current error and message values and initialize new values
            var hasError = result.hasError(),
                newError = false;
            var valMessage = result.validationMessage(),
                newMessage = "";

            // Only validate when a new value is available (i.e. not null) and the validation condition is true, if one was specified
            if (newValue && valCondition && typeof valCondition === "function" && valCondition()) {
                // Validate the data
                if (isNaN(newValueAsNum)) {
                    // Not a number
                    newError = true;
                    newMessage = nanMessage;
                } else if (minValue != "undefined" && newValueAsNum < minValue) {
                    // Value is less than the minimum
                    newError = true;
                    newMessage = minMessage;
                } else if (maxValue && newValueAsNum > maxValue) {
                    // Value is greater than the maximum
                    newError = true;
                    newMessage = maxMessage;
                }
            }

            if (updateOnError || !newError) {

                // Make sure we have a number before determining the value to write out
                if (isNaN(newValueAsNum)) {
                    newValueAsNum = 0;
                }
                var valueToWrite = (newValueAsNum === 0) ?
                    zeroFormat :
                    Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;

                // Only write the new value if it changed
                if (valueToWrite !== current) {
                    target(valueToWrite);
                    if (underlyingObservable && ko.isObservable(underlyingObservable))
                        underlyingObservable(valueToWrite);
                } else {
                    // If the rounded value is the same, but a different value was written, force a notification for the current field
                    if (newValueAsNum !== current) {
                        target.notifySubscribers(valueToWrite);
                        if (underlyingObservable && ko.isObservable(underlyingObservable))
                            underlyingObservable.notifySubscribers(valueToWrite);
                    }
                }
            }
            // If the error or message values have changed, save the new values. Because these are part of
            // a dependent (computed) observable, we have to notify the subscribers manually.
            if (newError !== hasError) {
                result.hasError(newError);
                result.hasError.notifySubscribers(newError);
            }
            if (newMessage !== valMessage) {
                result.validationMessage(newMessage);
                result.validationMessage.notifySubscribers(newMessage);
            }
        }
    });

    // Initialize the computed observable with the current value to make sure it is rounded appropriately
    result(target());
    // Return the new computed observable
    return result;
};