﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
var outsideAwardsViewModelMapping = {    
    'OutsideAwards': {
        create: function (options) {
            return new outsideAwardItemModel(options.data);
        }
    },    
    'AwardYears': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Code);
        }
    }
}

function outsideAwardItemModel(data) {
    var self = this;
    ko.mapping.fromJS(data, {}, this);

    self.AwardAmount = ko.observable(data.AwardAmount).extend({
        numeric: {
            precision: 2,
            min: 1,
            max: 99999.99,
            valCondition: function () { return true },
            updateOnError: true
        }
    });
}