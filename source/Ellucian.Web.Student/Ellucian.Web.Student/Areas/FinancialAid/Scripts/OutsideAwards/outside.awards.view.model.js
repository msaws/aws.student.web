﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
var outsideAwardsViewModel = function() {

    var self = this;

    //Array of existing outside awards for the year
    self.OutsideAwards = ko.observableArray();

    //Array of predetermined award types (scholarship, grant, loan)
    self.OutsideAwardTypes = ko.observableArray();

    //Flag to indicate whether to display the outside awards view or not
    self.displayOutsideAwardsView = ko.observable(false);

    //Flag to toggle display of the outside award input form
    self.displayOutsideAwardInputForm = ko.observable(false);

    //Flag to toggle display of the remove outside award form
    self.displayAwardRemoveDialog = ko.observable(false);

    //Flag to indicate whether current action is 'edit'
    self.editOutsideAward = ko.observable(false);

    //Flag to indicate whether any update was made to the existing award
    self.isModified = ko.observable(false);

    //Flag indicating whether there are any input errors
    self.hasErrors = ko.observable(false);

    //Flag that toggles the page data display
    self.showUI = ko.observable();

    //Observable for the student info
    self.Student = ko.observable();

    //Observable for the flag whether student is self or not
    self.IsUserSelf = ko.observable();

    //'Inherit' year selector and base view models
    yearSelector.call(self);
    BaseViewModel.call(self);

    self.changeToDesktop = function () { }
    self.changeToMobile = function () { }

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(outsideAwardsAdminActionUrl);
    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();
    
    //Outside award "object" to create/update
    self.outsideAward = ko.observable({
        Id: ko.observable(""),
        AwardName: ko.observable(""),
        AwardType: ko.observable(),
        AwardAmount: ko.observable("").extend({
            //required: true,
            numeric: {
                precision: 2,
                min: 1,
                max: 99999.99,
                valCondition: function () { return true },
                updateOnError: true
            }
        }),
        AwardFundingSource: ko.observable(""),
        AwardYearCode: ko.observable(),
        StudentId: ko.observable()
    });

    //Function to create a new outside award
    self.createOutsideAward = function () {
        self.outsideAward().AwardYearCode(self.selectedYear().Code());
        self.outsideAward().StudentId(self.Student().Id());
        var jsonData = { 'newOutsideAward': ko.toJSON(self.outsideAward()) };
        $.ajax({
            url: createOutsideAwardActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) { },
            success: function (data) {
                var outsideAward = {
                    Id: ko.observable(data.Id),
                    AwardName: ko.observable(data.AwardName),
                    AwardType: ko.observable(data.AwardType),
                    AwardAmount: ko.observable(data.AwardAmount),
                    AwardFundingSource: ko.observable(data.AwardFundingSource),
                    AwardYearCode: ko.observable(data.AwardYearCode),
                    StudentId: ko.observable(data.StudentId)
                }
                self.OutsideAwards.push(outsideAward);               
            },
            error: function (jqXHR) {
                if (jqXHR.status !== 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: faOutsideAwardsUnableToCreateMessage, type: "error", flash: true });
                }
            },
            complete: function () {
                self.displayOutsideAwardInputForm(false);
                self.clearInputValues();
                self.outsideAward().AwardAmount.extend({ rateLimit: 0 });
                $("#outside-awards-table").makeTableResponsive();
            }
        });
    }

    //Function triggered by Add an award button
    self.addOutsideAward = function () {
        self.clearInputValues();
        self.displayOutsideAwardInputForm(true);
        self.outsideAward().AwardAmount.extend({ rateLimit: { timeout: 2000, method: "notifyWhenChangesStop" } });
    }

    //Function to cancel add/update award action
    self.cancelOutsideAwardAction = function () {
        self.displayOutsideAwardInputForm(false);
        self.outsideAward().AwardAmount.extend({ rateLimit: 0 });
        if (self.editOutsideAward()) {
            self.editOutsideAward(false);
            self.originalOutsideAward("");
            self.isModified(false);
        }
    }

    //Function to clear new outside award fields
    self.clearInputValues = function () {        
        document.getElementById('award-amount-field').value="";
        self.outsideAward().AwardAmount("");
        self.outsideAward().AwardFundingSource("");
        self.outsideAward().AwardName("");
        self.outsideAward().AwardType("Scholarship");
    }

    //Function to validate award amount input
    self.processAmountChange = function (awardAmount, vm, event) {
        if (awardAmount.hasError()) {
            self.hasErrors(true);
        }
        else {            
            if (self.editOutsideAward()) {
                if ((self.originalOutsideAward().AwardAmount() !== self.outsideAward().AwardAmount()) && !self.isModified()) {
                    self.isModified(true);
                }
            }
            self.hasErrors(false);
        }
    }

    //Function to clear award amount input field on focus
    self.clearInputField = function (data, event) {
        var target = event.target;
        target.value = ' ';
        return false;
    }

    //Remove/delete functionality related observables and functions
    self.removeOutsideAwardMessage = ko.observable();
    self.outsideAwardToRemoveId = ko.observable();
    self.showAwardRemoveDialog = function (award, event) {
        self.displayAwardRemoveDialog(true);
        self.removeOutsideAwardMessage(faOutsideAwardsRemoveAwardConfirmationText.format(award.AwardName()));
        self.outsideAwardToRemoveId(award.Id());
    };

    //Cancel remove action and reset the observables
    self.cancelRemoveOutsideAward = function () {
        self.displayAwardRemoveDialog(false);
        self.removeOutsideAwardMessage("");
        self.outsideAwardToRemoveId("");
    }

    //Function to remove an outside award
    self.deleteOutsideAward = function (vm, event) {
        var jsonData = { 'outsideAwardId': ko.toJSON(self.outsideAwardToRemoveId()), 'studentId': ko.toJSON(self.Student().Id()) };
        $.ajax({
            url: deleteOutsideAwardActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) { },
            success: function (data) {
                self.OutsideAwards.remove(function (award) { return award.Id() === self.outsideAwardToRemoveId(); });                
            },
            error: function (jqXHR) {
                if (jqXHR.status !== 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: faOutsideAwardsUnableToDeleteMessage, type: "error", flash: true });
                }
            },
            complete: function () {
                self.displayAwardRemoveDialog(false);
            }
        });
    }

    //Function to handle add/update outside award events
    self.handleOutsideAwardEvent = function (vm, event) {
        var clickedButton = event.currentTarget.name;
        switch (clickedButton) {
            case "add":
                self.createOutsideAward();
                break;
            case "update":
                self.updateOutsideAward();
                break;
        }        
    }

    //Add/update button disabler flag
    self.disableOutsideAwardAction = ko.pureComputed(function () {
        if (self.hasErrors()) return true;
        else if (self.outsideAward().AwardName() === "" || self.outsideAward().AwardAmount() == 0 || self.outsideAward().AwardAmount() === ""
            || self.outsideAward().AwardFundingSource() === "") return true;
        else if (self.editOutsideAward() && !self.isModified()) return true;
        else return false;
    });

    //Label for add/update award button
    self.outsideAwardActionButtonLabel = ko.pureComputed(function () {
        if (self.editOutsideAward()) {
            return faOutsideAwardsUpdateButtonLabel;
        }
        else return faOutsideAwardsAddButtonLabel;
    });

    //Name for add/update award button
    self.outsideAwardActionButtonName = ko.pureComputed(function () {
        if (self.editOutsideAward()) {
            return "update";
        }
        else return "add";
    });

    //Holds the original award values to track updates
    self.originalOutsideAward = ko.observable();

    //Award edit observables/functions
    self.showAwardEditDialog = function (award, event) {
        self.displayOutsideAwardInputForm(true);
        self.editOutsideAward(true);
        self.outsideAward().Id(award.Id());
        self.outsideAward().AwardName(award.AwardName());
        self.outsideAward().AwardType(award.AwardType());
        self.outsideAward().AwardAmount(award.AwardAmount());
        self.outsideAward().AwardFundingSource(award.AwardFundingSource());
        self.outsideAward().AwardYearCode(award.AwardYearCode());
        self.outsideAward().StudentId(award.StudentId());

        self.originalOutsideAward(award);
        self.outsideAward().AwardAmount.extend({ rateLimit: { timeout: 2000, method: "notifyWhenChangesStop" } });
    };

    //Function to update outside award via ajax request
    self.updateOutsideAward = function () {
        var jsonData = { 'outsideAwardToUpdate': ko.toJSON(self.outsideAward()) };
        $.ajax({
            url: updateOutsideAwardActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) { },
            success: function (data) {
                var outsideAward = ko.utils.arrayFirst(self.OutsideAwards(), function (award) {
                    return (award.Id() === data.Id);
                });
                if (outsideAward) {
                    outsideAward.AwardAmount(data.AwardAmount), outsideAward.AwardName(data.AwardName),
                    outsideAward.AwardType(data.AwardType), outsideAward.AwardFundingSource(data.AwardFundingSource)
                }                
            },
            error: function (jqXHR) {
                if (jqXHR.status !== 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: faOutsideAwardsUnableToUpdateMessage, type: "error", flash: true });
                }
            },
            complete: function () {
                self.displayOutsideAwardInputForm(false);
                self.editOutsideAward(false);
                self.isModified(false);
                self.outsideAward().AwardAmount.extend({ rateLimit: 0 });
                $("#outside-awards-table").makeTableResponsive();
            }
        });
    };

    //Function that is triggered when the user makes changes to the existing award
    //and compares the original values with the new ones; AwardAmount change is 
    //handled in processAmounChange func
    self.processAwardUpdate = function (vm, event) {
        //Only if event is triggered by the user, track updates
        if (event && event.originalEvent && self.editOutsideAward()) {
            if (self.originalOutsideAward().AwardName() !== self.outsideAward().AwardName()) {
                self.isModified(true);
            }
            else if (self.originalOutsideAward().AwardType() !== self.outsideAward().AwardType()) {
                self.isModified(true);
            }
            else if (self.originalOutsideAward().AwardFundingSource() !== self.outsideAward().AwardFundingSource()) {
                self.isModified(true);
            }
        }
    }

    /*********       dialog options        **********/
    //Dialog displayed for adding/updating award
    self.outsideAwardDialogOptions = ko.observable({
        autoOpen: false, modal: true, minWidth: $(window).width() / 4.0,
        show: { effect: "fade", duration: dialogFadeTimeMs }, hide: { effect: "fade", duration: dialogFadeTimeMs }, resizable: false
    });

    //Dialog displayed for removing award
    self.removeOutsideAwardDialogOptions = ko.observable({
        autoOpen: false, modal: true, minWidth: $(window).width() / 4.0,
        show: { effect: "fade", duration: dialogFadeTimeMs }, hide: { effect: "fade", duration: dialogFadeTimeMs }, resizable: false
    });    
    /*-----------------------------------------------*/
}