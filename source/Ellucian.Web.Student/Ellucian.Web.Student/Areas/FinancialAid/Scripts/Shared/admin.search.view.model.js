﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function adminSearchViewModel() {
    var self = this;

    searchViewModel.call(self);

    // Function to retrieve admin search view model
    self.getFinancialAidAdminSearchViewModel = function (controllerName) {
        $.ajax({
            url: getFinancialAidAdminSearchViewModelUrl + "?controllerName=" + controllerName,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                self.SpinnerText(adminLoadingText);
                self.isLoading(true);
                self.showUI(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, {}, self);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: unableToLoadAdminSearch, type: "error", flash: true });
                }
            },
            complete: function () {
                self.checkForMobile(window, document);
                $("table").makeTableResponsive();
                self.isLoading(false);
                self.showUI(true);
            }
        });
    };

    // Function to execute a search
    self.executeSearch = function () {

        //Convert the model to a JSON string
        var json = { searchQueryJson: ko.toJSON(self.SearchString()) };

        //Post the data
        $.ajax({
            url: searchForStudentOrApplicantAsyncUrl,
            data: JSON.stringify(json),
            type: "POST",
            dataType: "json",
            contentType: jsonContentType,
            beforeSend: function (data) {
                self.searching(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.SearchResults(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.textStatus != 0) {                                                 //comes from SiteSources
                    $('#notificationHost').notificationCenter('addNotification', { message: searchFailedMessage.format(self.SearchString()), type: "error", flash: true });
                    self.SearchResults(null);
                    self.ErrorOccurred(true);                    
                    self.ErrorMessage(searchErrorMessage.format(self.SearchString()));
                }
            },
            complete: function () {
                $("table").makeTableResponsive();
                self.searching(false);
            }
        });
    }

    // Function to process a search result selection
    self.selectResult = function (data) {
        if (data && self.Select().Area() && self.Select().Controller && self.Select().Action() && !data.HasPrivacyRestriction) {
            openUrl(baseUrl + self.Select().Area() + '/' + self.Select().Controller() + '/' + self.Select().Action() + '/' + data.Id);
        }
    }
}