﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
var yearSelector = function () {

    //Select which storage mechanism to use depending on browser
    var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
    
    var self = this;

    //Flag to indicate if the current user is an administrator
    self.isAdmin = ko.observable();

    self.AwardYears = ko.observableArray();

    self.hasAwardYears = ko.computed(function () {
        var awardYears = self.AwardYears();
        return (awardYears.length > 0);
    });

    self.showYearSelector = ko.computed(function () {
        return self.hasAwardYears() && !self.HasPrivacyRestriction();
    });

    self.selectedYear = ko.observable({
        Code: ko.observable(),
        Description: ko.observable(),
        ArePrerequisitesSatisfied: ko.observable(),
        Counselor: ko.observable()
    });

    self.yearConfiguration = ko.pureComputed(function () {
        if (typeof self.Configuration === 'undefined' || typeof self.Configuration() === 'undefined' || self.Configuration() === null) return null;

        return ko.utils.arrayFirst(self.Configuration().AwardYearConfigurations(), function (config) {
            return config.AwardYearCode() === self.selectedYear().Code();
        });
    });

    self.hasConfiguration = ko.pureComputed(function () {
        return self.yearConfiguration() != null;
    });
    
    self.selectionChanged = function (data) {        
        //User selected another year
        if (memory.getItem("newPage") == 0) {
            if (typeof data.selectedYear() !== 'undefined') {
                memory.setItem("yearCode", data.selectedYear().Code());
                memory.setItem("yearDesc", data.selectedYear().Description());
            }
            
            //Request a loan page
            var selectedLoanRequest = data.selectedLoanRequestForYear;
            if (selectedLoanRequest !== null && typeof selectedLoanRequest !== 'undefined' && selectedLoanRequest() !== null)
            {
                selectedLoanRequest().cancelRequest();                
            }

            //documents page
            if (typeof documentsViewModelInstance !== 'undefined' && documentsViewModelInstance !== null)
            {
                if (documentsViewModelInstance.isMobile())
                {
                    documentsViewModelInstance.changeToMobile();
                }
            }

            //awards page
            if (typeof awardsViewModelInstance !== 'undefined' && awardsViewModelInstance !== null)
            {
                // Accordion content must be rendered before its controller because their IDs are dynamic;
                // this will move accordion content beneath its controller for proper accordion show/hide.
                rearrangeMultiAccordionElements($(".multi-accordion-content"));                
            }

            //award letter page
            if (typeof awardLetterViewModelInstance !== 'undefined' && awardLetterViewModelInstance !== null) {
                //award year has changed - send an ajax request to get award letter data or nullify 
                //StudentAwardLetter based on prereqs for the year
                var getAwardLetter = data.selectedYear().ArePrerequisitesSatisfied();                
                var selectedYearCode = data.selectedYear().Code();
                utility.getAwardLetterData(data, selectedYearCode, getAwardLetter);                
            }

            //outside awards page
            if (typeof outsideAwardsViewModelInstance !== 'undefined' && outsideAwardsViewModelInstance !== null) {
                //award year has changed - send an ajax request to get outside awards data for the year
                var selectedYearCode = data.selectedYear().Code();
                utility.getOutsideAwardsData(data, selectedYearCode);
            }

            //home page
            if (typeof homeViewModelInstance !== 'undefined' && homeViewModelInstance !== null) {
                if (data.isMobile()) {
                    $('.home-wrapper').tabs({ selected: 0 });
                }
            }
        }
    };
}

//Rearranges multi accordion elements to appear in the right order: 
//controller -> controlled panel
function rearrangeMultiAccordionElements(multiAccordionArray) {
    multiAccordionArray.each(function () {
        $(this).next().after($(this));
    });
}
