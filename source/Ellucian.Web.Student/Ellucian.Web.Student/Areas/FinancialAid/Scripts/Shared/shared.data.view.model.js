﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
var sharedDataViewModel = function () {

    var self = this;

    //SAPStatus observable
    self.SAPStatus = ko.observable();

    //Flag to indicate if current user is Proxy or not
    self.IsProxyView = ko.observable(false);

    //Flag that indicates whether the sap status is sat or unsat
    self.isAcademicProgressSatisfactory = ko.computed(function () {
        var sapStatus = ko.utils.unwrapObservable(self.SAPStatus());
        if (sapStatus === null || typeof (sapStatus) === 'undefined') return true;
        else {
            var progressStatus = ko.utils.unwrapObservable(sapStatus.AcademicProgressStatus);

            if ((progressStatus !== null && typeof(progressStatus != 'undefined'))
                && (progressStatus.Category() === 'Unsatisfactory' || progressStatus.Category() === 'Warning')) return false;
            else return true;
        }
    });
}