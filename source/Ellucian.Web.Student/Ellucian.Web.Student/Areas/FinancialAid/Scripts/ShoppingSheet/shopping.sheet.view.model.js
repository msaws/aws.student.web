﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
function shoppingSheetViewModel() {

    var self = this;

    // "Inherit" the base view model
    BaseViewModel.call(self, 768);

    //Flag set to true in shopping.sheet.js after data is loaded into this object
    self.showUI = ko.observable(false);

    //Student object observable
    self.Student = ko.observable();

    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();

    //List of Shopping sheets
    self.StudentShoppingSheets = ko.observableArray();

    self.Configuration = ko.observable();
    
    //"Inherit" the Year Selector. Initial year set in shopping.sheet.js after data is loaded
    yearSelector.call(self);

    //Get the shopping sheet for the selected year
    self.selectedShoppingSheetForYear = ko.computed(function () {
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return null;
        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());

        return ko.utils.arrayFirst(self.StudentShoppingSheets(), function (shoppingSheet) {
            return shoppingSheet.AwardYearCode() === selectedYearCode;
        }) || null;
    });

    self.changeToMobile = function () {
        // shopping sheet doesn't really work on a mobile device so help direct users to larger devices
        alert(faShoppingSheetBestViewedOnLargerDeviceMessage);
    };

    self.changeToDesktop = function () { //nothing to do here
    };

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(shoppingSheetAdminActionUrl);
}