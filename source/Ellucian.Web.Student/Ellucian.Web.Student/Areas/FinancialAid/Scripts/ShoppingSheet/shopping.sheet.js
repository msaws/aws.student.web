﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
var shoppingSheetViewModelInstance = new shoppingSheetViewModel();

//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
memory.setItem("newPage", 1);

//Build url
var utility = new financialAidUtility();

$(document).ready(function () {
    ko.applyBindings(shoppingSheetViewModelInstance, document.getElementById("main"));    
    getShoppingSheetData();
});

function getShoppingSheetData() {
    var url = utility.buildFinAidUrl(getShoppingSheetDataActionUrl, memory);

    // Get the student's shopping sheet data
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {

                ko.mapping.fromJS(data, shoppingSheetMapping, shoppingSheetViewModelInstance);
                var awardYears = shoppingSheetViewModelInstance.AwardYears();
                var selected = memory.getItem("yearCode");
                for (var i = 0; i < awardYears.length; i++) {
                    if (awardYears[i].Code() == selected) {
                        shoppingSheetViewModelInstance.selectedYear(shoppingSheetViewModelInstance.AwardYears()[i]);
                        break;
                    }
                }
                shoppingSheetViewModelInstance.showUI(true);

                // Force display of "view on desktop for best experience" message when viewing on mobile device
                shoppingSheetViewModelInstance.checkForMobile(window, document);

                memory.setItem("newPage", 0);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Federal Shopping Sheet data retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if (jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if (jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faShoppingSheetUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}