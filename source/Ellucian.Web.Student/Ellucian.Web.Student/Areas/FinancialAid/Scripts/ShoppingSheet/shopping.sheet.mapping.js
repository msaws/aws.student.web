﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
var shoppingSheetMapping = {

    'StudentShoppingSheets': {
        create: function (options) {
            return new shoppingSheetModel(options.data, options.parent);
        }
    },

    'ShoppingSheetConfiguration': {
        create: function (options) {
            return new shoppingSheetConfigurationModel(options.data);   
        }
    }
}

function shoppingSheetModel(data, shoppingSheetViewModelParent) {
    var self = this;

    ko.mapping.fromJS(data, shoppingSheetMapping, this);

    self.shortYearLabel = ko.computed(function() {
        return (self.AwardYearLabel()) ? self.AwardYearLabel() : self.AwardYearCode() ? self.AwardYearCode() : " ";
    })
    self.costsForYearLabel = ko.computed(function () {     
        return faShoppingSheetCostsForYearLabel.format(self.shortYearLabel());
    });

    self.costOfAttendanceGlossaryText = ko.computed(function () {
        return faShoppingSheetCostOfAttendanceGlossaryText.format(self.shortYearLabel());
    });
    self.netCostGlossaryText = ko.computed(function () {
        return faShoppingSheetNetCostsGlossaryText.format(self.shortYearLabel());
    });

    self.configuration = ko.pureComputed(function () {
        return (shoppingSheetViewModelParent.yearConfiguration() != null) ? shoppingSheetViewModelParent.yearConfiguration().ShoppingSheetConfiguration : null;
    });

}

function shoppingSheetConfigurationModel(data) {
    var self = this;

    ko.mapping.fromJS(data, shoppingSheetMapping, this);

    
    self.graduationRateLevel = ko.computed(function () {
        if (self.GraduationRateLevel() === "Low")
            return "hbar-pointer-low";
        else if (self.GraduationRateLevel() === "Medium")
            return "hbar-pointer-medium";
        else if (self.GraduationRateLevel() === "High")
            return "hbar-pointer-high";
    });

    self.loanDefaultRateLevel = ko.computed(function () {
        if(self.LoanDefaultRateLevel() === "Low")
            return "ldr-left-bar-lower";
        else if (self.LoanDefaultRateLevel() === "Medium")
            return "ldr-left-bar-equal";
        else if (self.LoanDefaultRateLevel() === "High")
            return "ldr-left-bar-higher";
    });

    self.nationalLoanDefaultRateLevel = ko.computed(function () {
        if (self.NationalLoanDefaultRateLevel() === "Low")
            return "ldr-right-bar-lower";
        else if (self.NationalLoanDefaultRateLevel() === "Medium")
            return "ldr-right-bar-equal";
        else if (self.NationalLoanDefaultRateLevel() === "High")
            return "ldr-right-bar-higher";
    });

    /**********   Repayment rate chart style properties   ***********/
    /*
    -30px is the default value for when national repayment rate is higher or equal to the institution rate;
    80 is the default height of the repayment rate bar;
    bottom - bar_average bottom;
    top - vbar_pointer top
    height - bar_body height;
    margin-top - bar_body margin top
    */
    self.vbarPointerTop = ko.computed(function () {
        var top = -30;
        if (self.NationalRepaymentRateLevel() === "Low") {
            var bottom = Math.round(self.NationalRepaymentRateAverage() / self.InstitutionRepaymentRate() * 80);
            top = 50 - bottom;
        }
        return top + "px";
    });

    self.barAverageBottom = ko.computed(function () {
        var bottom = 80;
        if (self.NationalRepaymentRateLevel() === "Low") {
            bottom = Math.round(self.NationalRepaymentRateAverage() / self.InstitutionRepaymentRate() * 80);
        }
        return bottom + "px";
    });

    self.barBodyHeight = ko.computed(function () {
        var height = 80;
        if (self.InstitutionRepaymentRateLevel() === "Low") {
            height = Math.round(self.InstitutionRepaymentRate() / self.NationalRepaymentRateAverage() * 80);
        }
        return height + "px";
    });

    self.barBodyMarginTop = ko.computed(function () {
        if (self.InstitutionRepaymentRateLevel() === "Low") {
            var height = Math.round(self.InstitutionRepaymentRate() / self.NationalRepaymentRateAverage() * 80);
            return (80 - height) + "px";
        }
    });
}