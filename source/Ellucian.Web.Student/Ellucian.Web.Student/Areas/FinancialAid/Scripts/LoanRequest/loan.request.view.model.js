﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates

function loanRequestViewModel() {

    var self = this;

    // Is there an ajax request being processed that should block the page?
    self.isLoading = ko.observable(false);    

    self.Configuration = ko.observable();

    //Flag set to true in loan.request.js after data is loaded into this object
    self.showUI = ko.observable(false);

    //Observable to toggle enabling/disabling control elements on the page
    self.loanRequestSubmissionInProgress = ko.observable();

    //"Inherit" the Year Selector. Initial year set in loan.request.js after data is loaded
    yearSelector.call(self);

    BaseViewModel.call(self);
    
    self.changeToMobile = function () {}

    self.changeToDesktop = function () {}

    //Student/Applicant data
    self.Person = ko.observable();
    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();

    //List of loan request objects
    self.LoanRequests = ko.observableArray();

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(loanRequestAdminActionUrl);

    //Get pending loan for the year if any and set the boolean respectively
    self.selectedLoanRequestForYear = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return null;
        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());

        return ko.utils.arrayFirst(self.LoanRequests(), function (loanRequest) {
            return (loanRequest.AwardYearCode() == selectedYearCode);
        }) || null;
    });

    // Gets the currency symbol for the local culture settings
    self.currencySymbol = ko.computed(function () {
        return Globalize.culture().numberFormat.currency.symbol;
    });


    //Create a new loan request
    self.createNewLoanRequest = function (loanRequest) {

        var jsonData = { 'request': ko.toJSON(loanRequest) };
        self.loanRequestSubmissionInProgress(true);

        $.ajax({
            url: createNewLoanRequestActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                self.isLoading(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    for (var i = 0; i < self.LoanRequests().length; i++) {
                        if (self.LoanRequests()[i].AwardYearCode() == loanRequest.AwardYearCode()) {
                            var newLoanRequest = self.LoanRequests()[i];
                            ko.mapping.fromJS(data, newLoanRequest);
                            break;
                        }
                    }
                }
                $('#notificationHost').notificationCenter('addNotification', { message: faLoanRequestSuccessMessage, type: "success", flash: true });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                self.isLoading(false);
                console.debug("Loan request submission failed: " + jqXHR.status + " " + jqXHR.responseText);
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faLoanRequestUnableToSubmitLoanRequestMessage, type: "error" });
            },

            complete: function (jqXHR, textStatus) {
                self.isLoading(false);
                self.loanRequestSubmissionInProgress(false);
                self.selectedLoanRequestForYear().requestSubmitted(true).currentWorkFlowStep("Confirmation");
            }
        });
    }

    //Sets selectedRequestOption to "customAmount"
    self.selectCustomRequestOption = function ()
    {
        if (!self.IsProxyView()) self.selectedLoanRequestForYear().selectedRequestOption('customAmount');
    }

} //end of loanRequestViewModel
