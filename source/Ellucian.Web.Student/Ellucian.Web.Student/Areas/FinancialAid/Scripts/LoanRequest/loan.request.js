﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
memory.setItem("newPage", 1);

var utility = new financialAidUtility();

var loanRequestViewModelInstance = new loanRequestViewModel();

$(document).ready(function () {
    ko.applyBindings(loanRequestViewModelInstance, document.getElementById("main"));
    getLoanRequestData();
});

function getLoanRequestData() {

    var url = utility.buildFinAidUrl(getLoanRequestActionUrl, memory);

    // Get the student's loan request information
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {

                ko.mapping.fromJS(data, loanRequestViewModelMapping, loanRequestViewModelInstance);
                var awardYears = loanRequestViewModelInstance.AwardYears();
                var selected = memory.getItem("yearCode");
                for (var i = 0; i < awardYears.length; i++) {
                    if (awardYears[i].Code() == selected) {
                        loanRequestViewModelInstance.selectedYear(loanRequestViewModelInstance.AwardYears()[i]);
                        break;
                    }
                }
                loanRequestViewModelInstance.showUI(true);
                memory.setItem("newPage", 0);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Awards retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if (jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if (jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faLoanRequestUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            loanRequestViewModelInstance.checkForMobile(window, document);
        }
    });
}