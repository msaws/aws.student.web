﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
var loanRequestViewModelMapping = {

    'LoanRequests': {
        copy: ["IsRequestPending"],
        create: function (options) {
            return new loanRequestModel(options.data);
        }
    },

    'LoanRequestAwardPeriods': {
        key: function(data) {
            return ko.utils.unwrapObservable(data.Code);
        },
        create: function (options) {
            return new loanRequestAwardPeriodModel(options.data);
        }
    },

    'AssignedToCounselor': {
        create: function (options) {
            if (options.data != null)
                return new loanCounselorModel(options.data);
        }
    }
}

function loanCounselorModel(data) {
    var self = this;

    ko.mapping.fromJS(data, loanRequestViewModelMapping, this);

    self.contactMessage = ko.computed(function () {
        var phone = (self.PhoneNumber()) ? "<a href=\'tel:"
                + ko.utils.unwrapObservable(self.PhoneNumber()).replace(/\D/g, '')
                + "\'>"
                + self.PhoneNumber()
                + "</a>"
                + " " : " ";
        var extension = (self.Extension()) ? faLoanRequestExtensionText.format(self.Extension()) : "";
        var email = (self.EmailAddress()) ? " (<a href=\'mailto:"
                + self.EmailAddress()
                + "\'>"
                + self.EmailAddress()
                + "</a>) " : " ";
        return faLoanRequestCounselorMessage.format(phone, extension, email);
    });
}

function loanRequestAwardPeriodModel(data) {

    var self = this;

    ko.mapping.fromJS(data, loanRequestViewModelMapping, this);

}


function loanRequestModel(data) {

    var origData = data;

    var self = this;

    self.MaximumRequestAmount = ko.observable();

    ko.mapping.fromJS(data, loanRequestViewModelMapping, this);

    //changed to customRequestAmount from loanRequestAmount
    self.customRequestAmount = ko.observable().extend({
        numeric: {
            precision: 0,
            zeroNull: true,
            min: 1,
            minMessage: "You must enter an amount greater than or equal to {0}".format(formatAsCurrency(1, 0)),
            max: self.MaximumRequestAmount.peek(),
            maxMessage: "You must enter an amount less than or equal to {0}".format(formatAsCurrency(self.MaximumRequestAmount.peek(), 0)),
            valCondition: function () { return true; },
            updateOnError: true
        }
    });

    //Message for the unmet cost request option
    self.unmetCostRequestAmountMessage = ko.computed(function () {
        var amount = '<span class=\"bold\">' +
            formatAsCurrency(parseANumber(self.UnmetCost()), 2) +
            '</span>';
        return faLoanRequestUnmetCostRequestAmountMessage.format(amount);       
    });

    //initialize the default option. if unmet cost exists, the default option is unmet cost
    self.selectedRequestOption = ko.observable();
    self.selectedRequestOption.extend({ notify: 'always' });
    if (self.UnmetCost() > 0) self.selectedRequestOption("unmetCost");
    else self.selectedRequestOption("customAmount");

    self.selectedRequestAmount = ko.computed(function () {
        if (self.selectedRequestOption() === "unmetCost") {
            self.clearCustomInput();
            return self.UnmetCost();
        }
        return parseANumber(self.customRequestAmount());
    });

    self.clearCustomInput = function () {
        self.customRequestAmount(0);
        document.getElementById('customAmountInput').value = "";
    }

    self.showCostEquation = ko.observable(false);

    self.showHideCostEquation = function () {
        if (self.showCostEquation())
            self.showCostEquation(false);
        else
            self.showCostEquation(true);
    };

    self.selectedAwardPeriods = ko.observableArray();
    self.selectedAwardPeriods.pushAll(self.LoanRequestAwardPeriods());

    self.currentWorkFlowStep = ko.observable((self.IsRequestPending()) ? "Confirmation" : "EnterLoanAmount");
    self.currentWorkFlowStep.extend({ notify: 'always' })
    self.nextStep = function (request) {
        switch (self.currentWorkFlowStep()) {
            case "EnterLoanAmount":
                self.currentWorkFlowStep("SelectPeriods");                
                break;
            case "SelectPeriods":
                self.currentWorkFlowStep("ReviewAndSubmit");
                break;                            
        }
    }

    self.prevStep = function () {
        switch (self.currentWorkFlowStep()) {
            case "SelectPeriods":
                self.currentWorkFlowStep("EnterLoanAmount");
                break;
            case "ReviewAndSubmit":
                self.currentWorkFlowStep("SelectPeriods");
                break;
        }
    }

    self.cancelRequest = function () {
        if (self.UnmetCost() > 0) self.selectedRequestOption("unmetCost");
        else self.selectedRequestOption("customAmount");

        var origRequestPeriods = ko.utils.arrayMap(origData.LoanRequestAwardPeriods, function (periodData) {
            return new loanRequestAwardPeriodModel(periodData);
        });
        self.selectedAwardPeriods.removeAll();
        self.selectedAwardPeriods.pushAll(origRequestPeriods);
        self.LoanRequestAwardPeriods(origRequestPeriods);

        self.clearCustomInput();
        self.Comments(null);
        if (self.IsRequestPending()) self.currentWorkFlowStep("Confirmation")
        else self.currentWorkFlowStep("EnterLoanAmount");
    }

    self.requestSubmitted = ko.observable(false);

    //Distribute selected request amount evenly among award periods.
    //only if the request is not pending (i.e. when the student hasn't submitted the request yet)
    self.distributeAwardPeriodAmounts = ko.computed(function () {

        if (!self.IsRequestPending()) {
            var periodCount = self.selectedAwardPeriods().length;
            //self.selectedAwardPeriods.sortByDateProperty("StartDate");
            self.selectedAwardPeriods.sort(function (a, b) {
                return (new Date(a.StartDate()) - new Date(b.StartDate()));
            });
            //Calculate the integer amount of selected period based on requested amount
            var amountPerPeriod = Math.round(self.selectedRequestAmount() / periodCount);

            //Calcuate the difference between the total rounded split amount and the requested amount. 
            //This will be subtracted from the last award period.
            var roundingDifference = (amountPerPeriod * periodCount) - self.selectedRequestAmount();

            for (var i = 0; i < self.LoanRequestAwardPeriods().length; i++) {

                var selectedIndex = self.selectedAwardPeriods.indexOf(self.LoanRequestAwardPeriods()[i]);
                if (selectedIndex >= 0) {
                    self.LoanRequestAwardPeriods()[i].AwardAmount(amountPerPeriod);
                    self.selectedAwardPeriods()[selectedIndex].AwardAmount(amountPerPeriod);
                } else {
                    self.LoanRequestAwardPeriods()[i].AwardAmount(0);
                }
            }

            if (periodCount > 0)
                self.selectedAwardPeriods()[periodCount - 1].AwardAmount(amountPerPeriod - roundingDifference);

            self.TotalRequestAmount(self.selectedRequestAmount());
        }

        return true;
    });

    self.formatWorkFlowIndicator = ko.computed(function () {

        // Original remove was not working, so edited.
        // Need a better solution here. Function is firing twice, hence the need for the removal.
        // Better to check if the step indicator arrows exist, and add them if not.
        $('.step-chevron').remove();

        /*if (!$.browser.msie) {*/
            $(".step-indicator > li .description-wrapper").not(":last").append("<div class='step-chevron'></div>");
        /*} else if ($.browser.msie && parseInt($.browser.version, 10) > 7) {
            $(".step-indicator > li .description-wrapper").not(":last").append("<div class='step-chevron'><div class='step-chevron'></div></div>");
        }*/

        $('#workflowIndicator li').removeClass("current done future");
        $('#step-icon-current, #step-icon-future, #step-icon-done').remove();

        switch (self.currentWorkFlowStep()) {
            case "EnterLoanAmount":
                $('#wfiEnterLoanAmount').addClass("current");
                $('#wfiSelectPeriods, #wfiReviewAndSubmit, #wfiConfirmation').addClass("future");
                break;

            case "SelectPeriods":
                $('#wfiEnterLoanAmount').addClass("done");
                $('#wfiSelectPeriods').addClass('current');
                $('#wfiReviewAndSubmit, #wfiConfirmation').addClass("future");
                break;

            case "ReviewAndSubmit":
                $('#wfiEnterLoanAmount, #wfiSelectPeriods').addClass("done");
                $('#wfiReviewAndSubmit').addClass('current');
                $('#wfiConfirmation').addClass("future");
                break;

            case "Confirmation":
                $('#wfiEnterLoanAmount, #wfiSelectPeriods, #wfiReviewAndSubmit, #wfiConfirmation').addClass("done");
                break;
        }

        $('.loan-step.offScreen').remove();
        $('.icon-circle').attr('class', 'icon-circle');

        $('#workflowIndicator li.current .icon-circle').addClass("icon-play-circle");
        $('<span class="loan-step offScreen">' + currentStep + '</span>').appendTo($('#workflowIndicator li.current .description'));

        $('#workflowIndicator li.done .icon-circle').addClass("icon-success");
        $('<span class="loan-step offScreen">' + completeStep + '</span>').appendTo($('#workflowIndicator li.done .description'));;

        $('#workflowIndicator li.future .icon-circle').addClass("icon-circle-gray icon-circle-outline");
        $('<span class="loan-step offScreen">' + incompleteStep + '</span>').appendTo($('#workflowIndicator li.future .description'));
    });       
}

function parseANumber(value) {
    value = value || 0;
    return (isNaN(value)) ? Globalize.parseFloat(value) : parseFloat(value);
}

function formatAsCurrency(amount, precision) {
    precision = precision || 2;
    if (isNaN(precision)) { precision = 2; }
    return Globalize.format(amount, "C" + precision);
}

