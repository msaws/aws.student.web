﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
function homeViewModel() {  

    var self = this;

    //Flag set to true in home.js after data is loaded into this object
    self.showUI = ko.observable(false);

    //"Inherit" the Year Selector. Initial year set in home.js after data is loaded
    yearSelector.call(self);

    // "Inherit" the base view model
    BaseViewModel.call(self, 769);

    //Shared data view model
    sharedDataViewModel.call(self);

    // Change desktop structure to tab mobiles structure
    // This should probably be done with a custom knockout binding
    this.changeToMobile = function () {

        // jQuery changes when screen hits mobile (769px and below)
        $(".mobile-nav-tabs").show();

        //$('.home-wrapper').addClass("home-wrapper-tabs");

        $('.home-wrapper').tabs({ selected: 0 }).addClass(" ui-tabs ui-widget ui-widget-content ui-corner-all");

        $('#counselor-container, #resources-container, #loans-container, #pell-leu-container').addClass('ui-tabs-hide');
    }

    // Change tabbed mobile structure back to desktop view
    this.changeToDesktop = function () {
        // Hide and style mobile tabs
        $(".mobile-nav-tabs").hide();

        // Remove tab classes
        $('.home-wrapper').removeClass('ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-panel ui-corner-bottom');
        $('#checklist-container, #counselor-container, #resources-container, #loans-container, #pell-leu-container').removeClass('ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide');
    }

    //List of checklists for all fa years
    self.StudentChecklists = ko.observableArray();

    //List of loans a student has so far
    self.StudentLoansSummary = ko.observable();

    //List of AverageAwardPackages
    self.AverageAwardPackages = ko.observableArray();

    //List of form links
    self.FormLinks = ko.observableArray();

    //List of useful links
    self.HelpfulLinks = ko.observableArray();

    //Person (Student or Applicant) 
    self.Person = ko.observable();
    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();

    self.Configuration = ko.observable();

    //PellLEU percentage observable
    self.PellLifetimeEligibilityUsedPercentage = ko.observable();

    //Student Account Summary observable
    self.StudentAccountSummary = ko.observable();

    //Flag indicating whether the current view is admin only(user is admin and not self)
    self.IsAdminView = ko.observable();

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(homeAdminActionUrl);
    
    //Flag indicating whether checklist items were assigned for the year
    self.areChecklistItemsAssigned = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return false;

        return ko.utils.unwrapObservable(self.selectedYear().AreChecklistItemsAssigned);
    });

    //Get checklist for selected year
    self.selectedChecklistForYear = ko.computed(function () {
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return true;
        if (self.StudentChecklists() === null || typeof self.StudentChecklists() === 'undefined') return true;
        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());

        var matchingChecklist = ko.utils.arrayFirst(self.StudentChecklists(), function (checklist) {
            return (checklist.AwardYearCode() == selectedYearCode);
        });

        return matchingChecklist !== null ? matchingChecklist : true;
    });

    //Get average award package for the selected year
    self.selectedAverageAwardPackageForYear = ko.computed(function () {
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return true;
        if (self.AverageAwardPackages() === null || typeof self.AverageAwardPackages() === 'undefined') return true;
        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());

        var matchingAverageAwardPackage = ko.utils.arrayFirst(self.AverageAwardPackages(), function (awardPackage) {
            return (awardPackage.AwardYearCode() == selectedYearCode);
        });

        return matchingAverageAwardPackage;
    });

    // Check if student has any loan debt
    self.studentHasLoanHistory = ko.computed(function () {
        var summary = ko.utils.unwrapObservable(self.StudentLoansSummary());
        if (summary === null || typeof summary === 'undefined') return false;

        return (summary.AggregateTotalLoanAmount() !== 0);
    });

    //Check if useful links were received
    self.areHelpfulLinksReceived = ko.computed(function () {
        var helpfulLinks = ko.utils.unwrapObservable(self.HelpfulLinks());
        if (helpfulLinks === null || typeof helpfulLinks === 'undefined') return false;

        return (helpfulLinks.length > 0);
    });

    //Check if form links were received
    self.areFormLinksReceived = ko.computed(function () {
        var formLinks = ko.utils.unwrapObservable(self.FormLinks());
        if (formLinks === null || typeof formLinks === 'undefined') return false;

        return (formLinks.length > 0);
    });

    //Boolean to indicate whether any links were received
    self.areLinksReceived = ko.computed(function () {
        return (self.areHelpfulLinksReceived() || self.areFormLinksReceived());
    });

    //Observable to toggle display of no checklist items assigned to student     
    self.showNoChecklistItemsMessage = ko.computed(function () {
        if (self.hasAwardYears()
            && !(ko.utils.unwrapObservable(self.selectedYear().AreChecklistItemsAssigned))) {
            return true;
        }
        return false;
    });

    //Observable to toggle display of notifications at the top of the fa home page
    self.showNotification = ko.computed(function () {
        if(self.showUI()){
            if (self.showNoChecklistItemsMessage()) return true;
            else if (self.Configuration().IsConfigurationComplete() && !self.hasAwardYears()) return true;            
            else return false;
        }
        return false;
    });

    //Observable to hold the specific notification message to show at the top of the fa home page
    self.notificationMessage = ko.computed(function () {
        if (self.showUI()) {
            if (self.showNoChecklistItemsMessage() && (self.isAdmin() || self.IsProxyView())) return faHomeNoChecklistAssignedMessage;
            else if (self.showNoChecklistItemsMessage() && !(self.isAdmin() || self.IsProxyView())) return faHomeNoChecklistAssignedStudentMessage;
            else if (self.Configuration().IsConfigurationComplete() && !self.hasAwardYears()) return faHomeNoFinAidDataMessage;
        }
    });

    //Indicates whether to display PellLEU
    self.displayPellLEUPercentage = ko.pureComputed(function () {
        if (self.yearConfiguration() === null || typeof self.yearConfiguration() === 'undefined') return false;
        var displayPercentage = ko.utils.unwrapObservable(self.yearConfiguration().DisplayPellLifetimeEligibilityUsedPercentage());

        //The configuration must be set to display pell used percentage and pell used percentage cannot be null/undefined 
        if (displayPercentage && (self.PellLifetimeEligibilityUsedPercentage() !== null && typeof self.PellLifetimeEligibilityUsedPercentage() !== 'undefined')) return true;
        else return false;
    });

    //Flag indicating whether the student account summary should be displayed to the student
    self.displayStudentAccountSummary = ko.pureComputed(function () {
        if (self.yearConfiguration() === null || typeof self.yearConfiguration() === 'undefined') return false;
        else if (!self.yearConfiguration().SuppressStudentAccountSummaryDisplay() && self.StudentAccountSummary() !== null && typeof self.StudentAccountSummary() !== 'undefined') return true;
        else return false;
    });

    //Flag indicating whether the average award package should be displayed to the student
    self.displayAverageAwardPackage = ko.pureComputed(function () {
        if (self.yearConfiguration() === null || typeof self.yearConfiguration() === 'undefined') return false;
        else if (!self.yearConfiguration().SuppressAverageAwardPackageDisplay()) return true;
        else return false;
    });

    //Style for the home-steps depending on the number of available "bubbles"
    self.homeStepStyle = ko.pureComputed(function () {
        if (!self.displayStudentAccountSummary() && !self.displayAverageAwardPackage()) return "one-home-steps-width";
        else if ((self.displayStudentAccountSummary() && !self.displayAverageAwardPackage())
        || (!self.displayStudentAccountSummary() && self.displayAverageAwardPackage())) return "two-home-steps-width";
        return "three-home-steps-width";
    });
    
}