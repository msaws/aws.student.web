﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
memory.setItem("newPage", 1);

var utility = new financialAidUtility();

var homeViewModelInstance = new homeViewModel();

$(document).ready(function () {
    ko.applyBindings(homeViewModelInstance, document.getElementById("main"));
    getHomeData();
});

function getHomeData(){
    var url = utility.buildFinAidUrl(getHomeDataActionUrl, memory);
    // Get the home page info
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {

                ko.mapping.fromJS(data, homeViewModelMapping, homeViewModelInstance);
                var awardYears = homeViewModelInstance.AwardYears();
                var selected = memory.getItem("yearCode");
                for (var i = 0; i < awardYears.length; i++) {
                    if (awardYears[i].Code() == selected) {
                        homeViewModelInstance.selectedYear(homeViewModelInstance.AwardYears()[i]);
                        break;
                    }
                }
                memory.setItem("newPage", 0);
                homeViewModelInstance.showUI(true);               
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Home page information retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if(jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if(jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faHomeUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            /*checkForMobile(window, document) is invoked as part of constructor when on mobile device and
            isMobile is set to true but need to call changeToMobile() again after all elements
            are loaded*/
            if (homeViewModelInstance.isMobile()) {
                homeViewModelInstance.changeToMobile();
            }
                /*isMobile is set to false at init. If on desktop, checkForMobile(window, document)
                is not invoked when initializing, need to check if we have a minimized 
                browser window*/
            else if (!homeViewModelInstance.isMobile()) {
                homeViewModelInstance.checkForMobile(window, document);
            }

        }
    });
}