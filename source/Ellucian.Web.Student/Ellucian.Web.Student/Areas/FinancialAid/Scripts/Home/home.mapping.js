﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
var homeViewModelMapping = {
    'ChecklistItems': {
        create: function (options) {
            return new CheckListItemsModel(options.data);
        }
    },

    'ActiveChecklistItem': {
        create: function (options) {
            return new activeItemDataModel(options.data);
        }
    },

    'Configuration': {
        create: function (options) {
            return new StudentFinancialAidConfiguration(options.data, options.parent);
        }
    },

    'AwardYearConfigurations': {
        create: function (options) {
            return new AwardYearConfiguration(options.data);
        }
    },

    'SAPStatus': {
        create: function (options) {
            return new sapStatusModel(options.data);
        }
    },

    'StudentAccountSummary': {
        create: function (options) {
            return new studentAccountSummaryModel(options.data);
        }
    }

}

function CheckListItemsModel(data) {
    ko.mapping.fromJS(data, homeViewModelMapping, this);

    var self = this;

    /* Custom mapping for each checklist item to have associated css group
    (based on the status of the item) */
    self.actionStyle = ko.computed(function () {
        if (self.Status() === "Incomplete")
            return "action-incomplete";
        else if (self.Status() === "Complete")
            return "action-completed";
        else if (self.Status() === "NotAvailable")
            return "action-notAvailable";
        else if (self.Status() === "InProgress")
            return "action-progress";

    });

    /*Returns the css group the link belongs to:
    if item requires action - activeChecklistItemLink, and vice versa*/
    self.linkStyle = ko.computed(function () {
        if (self.IsActiveItem()) {
            return "activeChecklistItemLink";
        }
        else return "inactiveChecklistItemLink";
    });
    
}

function activeItemDataModel(data) {
    var self = this;

    ko.mapping.fromJS(data, homeViewModelMapping, self);

    /* Returns the css group the current Bubble belongs to.
    It is linked to the currently active checklist item */
    self.stepStyle = ko.computed(function () {
        var item = ko.utils.unwrapObservable(self.Type());
        if (item === "FAFSA" || item === "Profile")
            return "group-application";
        else if (item === "Documents")
            return "group-documents";
        else if (item === "Application")
            return "group-faApplication";
        else if (item === "Awards")
            return "group-faAwards";
        else if (item === "AwardLetter")
            return "group-awardLetter";
        else if (item === "MPN")
            return "group-mpn";
        else if (item === "Interview")
            return "group-interview";
        else if (item === "Complete")
            return "group-complete"
    });
}

function StudentFinancialAidConfiguration(data, homeViewModelParent) {
    var self = this;

    self.MissingConfigurationMessages = ko.observableArray();

    ko.mapping.fromJS(data, homeViewModelMapping, self);
}

function AwardYearConfiguration(data) {
    var self = this;
    
    ko.mapping.fromJS(data, homeViewModelMapping, self);
}

function sapStatusModel(data)
{
    var self = this;
    ko.mapping.fromJS(data, homeViewModelMapping, self);

    self.sapStatusMessage = ko.computed(function () {
        var status = self.AcademicProgressStatus.Description();
        var highlightedText = '<span id="highlighted-text">' + faSharedAcademicProgressMessageHighlightedText + '</span>';
        if (self.AcademicProgressStatus.Category() === 'Unsatisfactory' || self.AcademicProgressStatus.Category() === 'Warning') {
            return faSharedUnsatisfactoryAcademicProgressMessage.format(highlightedText, status);
        }
        else return faSharedSatisfactoryAcademicProgressMessage.format(highlightedText, status);
    });
}

function studentAccountSummaryModel(data) {
    var self = this;
    ko.mapping.fromJS(data, homeViewModelMapping, self);

    self.studentAccountSummaryAmountDueAndDate = ko.pureComputed(function () {
        return faHomeStudentAccountSummaryAmountDueMessage.format(new Date().toLocaleDateString());
    });
}