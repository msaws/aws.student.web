// Tooltip with close icon

$(document).ready(function () {
    // target = element clicked (a) - currently false
    // tooltip = div - currently false
    // title = attribute of a - currently false
    // tip = text contained within title
    var target = false,
        tooltip = false,
        title = false,
        tip = "";

    // listen for click on element in body containing rel=tooltip
    $('body').on('click', '[rel~=tooltip]', function (event) {
        // if a tooltip is already present on the page, remove and reset
        if ($('.tooltip-text').length > 0) {
            return false;
        }
        // assign variables
        target = $(this);
        tip = target.attr('title');
        tooltip = $('<div id="tooltip" tabindex="-1" role="dialog"></div>');

        // text of tooltip (a title)
        tooltipText = $('<div class="tooltip-text" tabindex="0"></div>');

        // close icon
        tooltipClose = $('<span class="close-tooltip"></span>');

        // if no tip or tip is empty, don't do anything (return false)
        if (!tip || tip == '')
            return false;

        // remove title from a and assign to tooltip
        target.removeAttr('title');
        tooltip.css('opacity', 0)
               .html(tip)
               .appendTo('body');

        // remove tooltip
        var removeTooltip = function () {
            tooltip.animate({ top: '-=10', opacity: 0 }, 50, function () {
                $(this).remove();
            });

            // reassigns tooltip text to a title
            target.attr('title', tip);

            // change focus back to a (for accessibility)
            target.focus();

        };

        // initialize tootlip
        var initTooltip = function () {
            // place title text inside tooltip
            tooltip.wrapInner(tooltipText);

            // add tooltip close icon before tooltip text
            $('.tooltip-text').before(tooltipClose);

            // timeout needed to ensure tooltip exists before focus is changed
            setTimeout(function () {
                $('.tooltip-text').focus();
            }, 300);
            

            // will need to rework this later and change to respond to base.view.model when page is made responsive
            // if-elses control positioning of tooltip
            if ($(window).width() < tooltip.outerWidth() * 1.5)
                tooltip.css('max-width', $(window).width() / 2);
            else
                tooltip.css('max-width', 340);

            var posTop = target.offset().top + (target.outerHeight() / 2) - (tooltip.outerHeight() / 2) - 10,
            posLeft = target.offset().left + (target.outerWidth() / 2) + 30;

            if (posLeft < 0) {
                posLeft = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass('left');
            }
            else
                tooltip.removeClass('left');

            if (posLeft + tooltip.outerWidth() > $(window).width()) {
                posLeft = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass('right');
            }
            else
                tooltip.removeClass('right');

            if (posTop < 0) {
                var posTop = target.offset().top + target.outerHeight();
                tooltip.addClass('top');
            }
            else
                tooltip.removeClass('top');

            tooltip.css({ left: posLeft, top: posTop })
                   .animate({ top: '+=10', opacity: 1 }, 50);
            return false;
        };

        // call initialize tooltip function
        initTooltip();

        // added to prevent page from jumping to top
        event.preventDefault();

        // currently broken, need to change this to use base.view.model for responsive
        // $(window).resize(initTooltip);

        // remove tooltip when "close" icon clicked
        $('.close-tooltip').bind('click', removeTooltip);
        
        // remove tooltip when ESC pressed
        $(document).keydown(function (e) {
            if (e.keyCode == 27) {
                removeTooltip();
            }
        });

    });
});
