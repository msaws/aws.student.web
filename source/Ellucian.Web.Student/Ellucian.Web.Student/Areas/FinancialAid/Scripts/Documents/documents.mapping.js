﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
var documentsViewModelMapping = {
    'StudentDocumentCollections': {
        create: function (options) {
            return new studentDocumentCollectionModel(options.data);
        },
    },
    'IncompleteDocuments': {
        create: function (options) {
            return new studentDocumentModel(options.data);
        }
    },
    'CompleteDocuments': {
        create: function (options) {
            return new studentDocumentModel(options.data);
        }
    }
};

function studentDocumentCollectionModel(data) {
    ko.mapping.fromJS(data, documentsViewModelMapping, this);

    var self = this;
}

function studentDocumentModel(data) {
    ko.mapping.fromJS(data, documentsViewModelMapping, this);    

    var self = this;
    
    self.statusStyle = ko.computed(function () {
        var status = self.Status();
        var statusDesc = self.StatusDisplay();
        if (status === "Received")
            return "document-received";
        else if (status === "Waived")
            return "document-waived";
        else if (status === "Incomplete" || status === ""){
            if(statusDesc === "Overdue"){
                return "document-overdue";
            }
            else{
                return "document-incomplete";
            }
        }        
    });
}
