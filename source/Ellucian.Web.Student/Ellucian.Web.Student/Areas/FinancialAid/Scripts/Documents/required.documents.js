﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
memory.setItem("newPage", 1);

var utility = new financialAidUtility();

var documentsViewModelInstance = new documentsViewModel();

//Save or retrieve student's id if in admin view
var url = utility.buildFinAidUrl(getDocumentsActionUrl, memory);


$(document).ready(function () {    
    ko.applyBindings(documentsViewModelInstance, document.getElementById("main"));
    getDocumentsData();
});

function getDocumentsData() {
    var url = utility.buildFinAidUrl(getDocumentsActionUrl, memory);

    // Get the student's documents
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {

                ko.mapping.fromJS(data, documentsViewModelMapping, documentsViewModelInstance);

                var awardYears = documentsViewModelInstance.AwardYears();
                var selected = memory.getItem("yearCode");

                for (var i = 0; i < awardYears.length; i++) {
                    if (awardYears[i].Code() === selected) {
                        documentsViewModelInstance.selectedYear(documentsViewModelInstance.AwardYears()[i]);
                        break;
                    }
                }
                documentsViewModelInstance.showUI(true);
                memory.setItem("newPage", 0);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Documents retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if (jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if (jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faDocumentsUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            documentsViewModelInstance.checkForMobile(window, document);
        }
    });
}