﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
function documentsViewModel() {

    var self = this;

    // We're done loading, so display the UI
    self.showUI = ko.observable(false);

    // "Inherit" the year selector
    yearSelector.call(self);

    // "Inherit" the base view model
    BaseViewModel.call(self);

    // Change desktop structure to tab mobiles structure
    // This should probably be done with a custom knokcout binding
    self.changeToMobile = function () {
        // jQuery changes when screen hits mobile (480px and below)
        // currently all handed by CSS, but may change in the futer

        //move each document title from div.document-details to div.document-header
        $('.document').each(function (document) {
            var detailsTitle = $('.document-details .document-title', this).remove();

            //if there's not a document-title class already in .document-header, then
            //prepend docTitle to the div.document-header
            var headerTitle = $('.document-header .document-title', this)
            if (!headerTitle.length) {
                detailsTitle.prependTo($('.document-header', this));
            }
        });

        // move the year selector label offscreen (not hiding)
        $('.year-select-text').addClass('offScreen');

        $('.document-title').attr('aria-role', 'button');
    }

    // Change tabbed mobile structure back to desktop view
    self.changeToDesktop = function () {
        //move each document title from the document-header div to the document-details div
        $('.document').each(function (document) {
            var headerTitle = $('.document-header .document-title', this).remove();

            //if there's not a document-title class already in .document-details, then
            //prepend docTitle to the div.document-details
            var detailsTitle = $('.document-details .document-title', this);
            if (!detailsTitle.length) {
                headerTitle.prependTo($('.document-details', this));
            }
        });

        // move the year selector label offscreen (not hiding)
        $('.year-select-text').removeClass('offScreen');

        $('.document-title').removeAttr('aria-role');

        self.deactivateDocument();
    }

    //Person (Student/Applicant) who owns this document data.
    self.Person = ko.observable();
    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();

    // List of student document collections. One Collection Per Year
    self.StudentDocumentCollections = ko.observableArray();

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(documentsAdminActionUrl);
    
    //Selected year's document collection
    self.selectedDocumentCollection = ko.computed(function () {
        //selectedYear comes from inherited YearSelector object
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return null;

        var selectedYearCode = ko.utils.unwrapObservable(self.selectedYear().Code());

        return ko.utils.arrayFirst(self.StudentDocumentCollections(), function (documentCollection) {
            return documentCollection.AwardYearCode() === selectedYearCode;
        }) || null;
    });

    //Check if there are any documents for the selectedYear
    self.documentsExistForYear = ko.computed(function () {
        return (self.selectedDocumentCollection()) ? self.selectedDocumentCollection().DocumentsExist() : false;
    });

    self.activeDocument = ko.observable();
    self.activateDocument = function (selectedDocument) {
        if (self.isMobile()) {
            self.activeDocument(selectedDocument);

            // show overlay
            $(".overlay-panel").show();

            // specific to Required Documents
            $("#data-container-overlay .document-details").show();
            $("#data-container-overlay .document-header").toggleClass("mobile-document-padding");
            var detailsTitle = $('#data-container-overlay .document-title').remove();
            detailsTitle.prependTo($('#data-container-overlay .document-header'));

            // slide overlay
            $(".overlay-panel").toggleClass("slide", true);

            // hide initial panel when overlay is active, after animation is complete
            $(".overlay-panel").one(transitionEvent(),
                function (event) {
                    $(".initial-panel").hide();
                });

            // adjust panel height
            var newHeight = $(".overlay-panel-details").height();
            $(".initial-panel-wrapper").height(newHeight);            
        }
        //returning true so that click events on anchors contained in selectedDocument bubble to the default
        //action of following the hyperlink. See knockout doc on click binding.
        return true;
    }
    self.deactivateDocument = function () {
        // slide overlay
        $(".overlay-panel").toggleClass("slide", false);

        // hide overlay when initial panel is active, after animation is complete
        $(".overlay-panel").one(transitionEvent(),
            function (event) {
                $(".overlay-panel").hide();
            });

        // show intial panel
        $(".initial-panel").show();

        // specific to Required Documents
        self.activeDocument(null);

        // restore height
        $(".initial-panel-wrapper").height("inherit");
    }   

};



