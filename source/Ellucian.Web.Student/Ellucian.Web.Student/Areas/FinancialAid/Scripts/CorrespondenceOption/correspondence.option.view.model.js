﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
function correspondenceOptionViewModel() {

    var self = this;
    
    //Flag set to true in my.awards.js after data is loaded into this object
    self.showUI = ko.observable(false);

    // Is there an ajax request being processed that should block the page?
    self.isLoading = ko.observable(false);

    //Student object observable
    self.Person = ko.observable();
    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();

    //Correspondence observable data
    self.CorrespondenceOptionData = ko.observable();

    //Indicates whether the current user is proxy or not
    self.IsProxyView = ko.observable(false);

    //Indicates whether current user is admin or not
    self.isAdmin = ko.observable(false);

    //Flag to indicate whether or not the checkbox state has changed
    //Used to disable the submit button if there was no change from original state
    self.isCheckboxSelectionChanged = ko.observable(false);

    // Spinner message
    self.SpinnerMessage = ko.observable();

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(correspondenceOptionAdminActionUrl);

    //Trigerred when a user clicks on the correspondence option checkbox
    //Updates IsCorrespondenceOptionChecked observable
    self.changeSelection = function (option, event) {
        var isChecked = event.target.checked;
        var data = ko.utils.unwrapObservable(self.CorrespondenceOptionData());

        self.isCheckboxSelectionChanged(false);

        if (data.isOriginalOptionChecked() !== isChecked) {
            data.IsCorrespondenceOptionChecked(isChecked);
            self.isCheckboxSelectionChanged(true);
        }                
        return true;
    }
    
    //Triggered when a user clicks on submit button
    self.submitSelection = function (optionData) {
        var jsonData = { 'optionData': ko.toJSON(optionData) };

        $.ajax({
            url: updateCorrespondenceOptionActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {                
                self.isLoading(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    optionData.IsCorrespondenceOptionChecked(data.IsCorrespondenceOptionChecked);
                    optionData.isOriginalOptionChecked(optionData.IsCorrespondenceOptionChecked());                    
                    self.isCheckboxSelectionChanged(false);
                }
                $('#notificationHost').notificationCenter('addNotification', { message: faCorrespondenceOptionUpdateSuccessMessage, type: "success", flash: true });
            },

            error: function (jqXHR, textStatus, errorThrown) {
                self.isLoading(false);
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faCorrespondenceOptionUnableToSubmitChangeMessage, type: "error" });
            },

            complete: function (jqXHR, textStatus) {
                self.isLoading(false);
            }
        });
    }
}