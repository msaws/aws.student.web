﻿//Copyright 2014 Ellucian Company L.P. and its affiliates
var correspondenceOptionViewModelMapping = {
    'CorrespondenceOptionData': {
        create: function (options) {
            return new correspondenceOptionDataModel(options.data);
        }
    }
}

function correspondenceOptionDataModel(data) {
    ko.mapping.fromJS(data, correspondenceOptionViewModelMapping, this);

    var self = this;    

    self.isOriginalOptionChecked = ko.observable(data.IsCorrespondenceOptionChecked);
}