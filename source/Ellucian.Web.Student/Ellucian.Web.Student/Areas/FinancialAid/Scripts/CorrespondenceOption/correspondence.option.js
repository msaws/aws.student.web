﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
var utility = new financialAidUtility();

//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();

var correspondenceOptionViewModelInstance = new correspondenceOptionViewModel();

$(document).ready(function () {
    ko.applyBindings(correspondenceOptionViewModelInstance, document.getElementById("main"));
    getCorrespondenceOptionData();
});

function getCorrespondenceOptionData() {
    var url = utility.buildFinAidUrl(getCorrespondenceOptionActionUrl, memory);

    // Get the correspondence option data
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {

                ko.mapping.fromJS(data, correspondenceOptionViewModelMapping, correspondenceOptionViewModelInstance);
                correspondenceOptionViewModelInstance.showUI(true);

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Correpondence option data retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);            
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if (jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if (jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faCorrespondenceOptionUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {

        }
    });
}