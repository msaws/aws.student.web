﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates

var awardLetterViewModelMapping = {        
    'AwardLetterHistoryItems': {
        create: function (options) {
            return new awardLetterHistoryItemModel(options.data);
        }
    },
    'StudentAwardLetter': {
        create: function (options) {
            return new studentAwardLetterModel(options);
        }
    },
    'AwardYears': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Code);
        }
    }
};

function studentAwardLetterModel(options) {

    var self = this;
    var data = options.data;
    var parent = options.parent;

    ko.mapping.fromJS(data, {}, this);

    //Assign a pending awards message on the award letter based on the different parameters
    self.letterWithPendingAwardsMessage = ko.computed(function () {        
        if (self.AreAnyAwardsPending()) {
            if (!self.AcceptedDate() && parent.isMobile())
                return faAwardLetterActionRequiredMobileMessage;
            else if (!self.AcceptedDate() && !parent.isAwardPackageChecklistItemRemoved())
                return faAwardLetterAwardsActionRequired;
            else if(self.AcceptedDate())
                return faAwardLetterPendingAwardsMessage;
        }
    });

    //Flag indicating whether to display letterWithPendingAwardsMessage
    self.showLetterWithPendingAwardsMessage = ko.computed(function () {
        return self.AreAnyAwardsPending();
    });

    //Flag to indicate whether to display award letter notice (about accepting award letter)
    self.showAwardLetterNotice = ko.computed(function () {
        return (!self.AreAnyAwardsPending() && !self.AcceptedDate());
    });

    //Css style assigned to the notification based on awards status
    self.letterNoticeStyle = ko.computed(function () {        
        if (self.AreAnyAwardsPending() && (!parent.isAwardPackageChecklistItemRemoved() || parent.isMobile())) return 'letter-pending';
        else if(self.AcceptedDate() && (!self.AreAnyAwardsPending() || parent.isAwardPackageChecklistItemRemoved())) return 'letter-accepted';
        else if(parent.isAwardPackageChecklistItemRemoved() && !self.AcceptedDate() && self.AreAnyAwardsPending() && !parent.isMobile()) return 'letter-no-message';
    });

    //Get the letter greeting
    self.awardLetterGreeting = ko.computed(function () {
        var studentName = self.StudentName(); 
        return faAwardLetterGreeting.format(studentName);
    });

    //Get and insert the date of acceptance and format the accepted letter message
    self.letterIsAcceptedMessage = ko.computed(function () {
        return faAwardLetterAcceptedWithDateMessage.format(self.AcceptedDateDisplay());
    });
}

function awardLetterHistoryItemModel(data) {

    var self = this;

    ko.mapping.fromJS(data, {}, this);

    //Flag indicating whether the history item is visible to the user
    self.isItemVisible = ko.observable(true);
}