﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
function awardLetterViewModel() {

    var self = this;

    // "Inherit" the base view model
    BaseViewModel.call(self, 768);
    //"Inherit" the Year Selector. Initial year set in award.letter.js after data is loaded
    yearSelector.call(self);
    
    // Change to mobile
    self.changeToMobile = function () {}

    // Change to dektop
    self.changeToDesktop = function () {}

    //Flag set to true in award.letter.js after data is loaded into this object
    self.showUI = ko.observable(false);

    //Student object observable
    self.Student = ko.observable();

    //Student Award Letter for the selected year
    self.StudentAwardLetter = ko.observable();

    //Set of award letter history items for the selected year
    self.AwardLetterHistoryItems = ko.observableArray();

    //Control status of the award package checklist item for the selected year
    self.AwardPackageChecklistItemControlStatus = ko.observable();    

    //Used to disable all control elements on the page while
    //the process of accepting an award letter
    self.acceptAwardLetterInProgress = ko.observable(false);

    // Is there an ajax request being processed that should block the page?
    self.isLoading = ko.observable(false);

    //Flag indicating whether to display all of the award letter history items
    self.showAllAwardLetterHistoryItems = ko.observable(false);

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(awardLetterAdminActionUrl);

    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();
    
    //Flag indicating whether checklist items were assigned for the year
    self.areChecklistItemsAssigned = ko.computed(function () {
        //make sure the selectedYear has a value
        if (self.selectedYear() === null || typeof self.selectedYear() === 'undefined') return false;

        return ko.utils.unwrapObservable(self.selectedYear().AreChecklistItemsAssigned);
    });

    //Flag to indicate whether award package checklist item was removed from checklist
    self.isAwardPackageChecklistItemRemoved = ko.computed(function () {
        if (self.AwardPackageChecklistItemControlStatus() === null || typeof self.AwardPackageChecklistItemControlStatus() === 'undefined') {
            return false;
        }
        if (self.AwardPackageChecklistItemControlStatus().toUpperCase() === "REMOVEDFROMCHECKLIST") return true;
        return false;
    });

    //flag indicating whether there are any award letter history records for the year
    self.areAnyAwardLetterHistoryRecords = ko.computed(function () {
        if (self.AwardLetterHistoryItems() === null || typeof self.AwardLetterHistoryItems() === 'undefined') return false;
        else if (self.AwardLetterHistoryItems().length > 0) return true;
        return false;
    });

    //Award letter history items to display based on the value of the showAllAwardLetterHistoryItems flag
    self.awardLetterHistoryItemsToDisplay = ko.computed(function () {
        if (self.AwardLetterHistoryItems() === null || typeof self.AwardLetterHistoryItems() === 'undefined') return [];
        var awardLetterHistoryItems = self.AwardLetterHistoryItems();
        //Only display 3 history records initially
        if (!self.showAllAwardLetterHistoryItems()) {
            for (var i = 3; i < awardLetterHistoryItems.length; i++) {
                awardLetterHistoryItems[i].isItemVisible(false);
            }
        }
        else if (self.showAllAwardLetterHistoryItems()) {
            for (var i = 3; i < awardLetterHistoryItems.length; i++) {
                awardLetterHistoryItems[i].isItemVisible(true);
            }
        }
        return ko.utils.unwrapObservable(self.AwardLetterHistoryItems());
    });

    //Sets the flag to view all award letter history items to true
    self.toggleAwardLetterHistoryView = function () {
        self.showAllAwardLetterHistoryItems(true);
    }

    //Css classes for the amount-report section
    self.amountReportSectionStyle = ko.computed(function () {
        var style = "section-wrapper clear-group";
        if (self.areAnyAwardLetterHistoryRecords()) style += " column-six";
        return style;
    });
    
    //Checks if pdf is returned without errors and opens a new page with pdf if so
    self.redirectToUrl = function (record, e) {
        e.preventDefault();

        if (self.Student() === null || typeof self.Student() === 'undefined') return "";
        if (getAwardLetterReportActionUrl === null || typeof getAwardLetterReportActionUrl === 'undefined') return "";

        var studentId = ko.utils.unwrapObservable(self.Student().Id());
        var awardLetterId = ko.utils.unwrapObservable(record.AwardLetterRecordId());        

        var url = getAwardLetterReportActionUrl + "?studentId=" + studentId + "&awardLetterId=" + awardLetterId;
        
        $.ajax({
            url: url,            
            type: "GET",
            contentType: 'application/pdf',            
            success: function (data) {                
                window.open(url);
            },
            error: function (jqXHR, textStatus, errorThrown) {                
                if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardLetterUnableToRetrieveReport, type: "error", flash: true });                    
                }
            }
        });        
    }
    
    //Triggered when Accept" award letter button is clicked, updates the accepted date
    self.awardLetterButtonHandler = function (awardLetter, event) {
        if (awardLetter === null || typeof awardLetter === 'undefined') return true;

        var clickedButton = event.currentTarget.name;

        var jsonData = { 'awardLetter': ko.toJSON(awardLetter) };

        self.acceptAwardLetterInProgress(true);

        $.ajax({
            url: updateAwardLetterActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {                
                self.isLoading(true);
            },
            success: function (data) {
                //data is a StudentAwardLetter model
                //update the IsLetterAccepted and AcceptedDate observables
                if (!account.handleInvalidSessionResponse(data)) {
                    awardLetter.IsLetterAccepted(data.IsLetterAccepted);
                    awardLetter.AcceptedDate(data.AcceptedDate);
                    awardLetter.AcceptedDateDisplay(data.AcceptedDateDisplay);

                    if (clickedButton == "AcceptButton")
                        $('#notificationHost').notificationCenter('addNotification', { message: faAwardLetterSignedMessage, type: "success", flash: true });                    
                }
            },

            error: function (jqXHR, textStatus, errorThrown) {
                self.isLoading(false);                
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardLetterErrorUpdatingMessage, type: "error" });
            },

            complete: function (jqXHR, textStatus) {
                self.acceptAwardLetterInProgress(false);
                self.isLoading(false);
            }
        });
    }    

    //Enables award letter accept button if "verify" checkbox is checked
    self.awardLetterAcceptButtonEnabler = ko.computed(function () {
        if (self.StudentAwardLetter() === null || typeof self.StudentAwardLetter() === 'undefined') return false;       

        var awardLetter = self.StudentAwardLetter();
        if (awardLetter.AreAnyAwardsPending())
            return false;
        if (awardLetter.IsLetterAccepted() && !awardLetter.AcceptedDate()) //IsLetterAccepted is the Verify Checkbox
            return true;

        return false;
    });

    //By clicking on a cancel link, the verification checkbox must be unchecked
    self.revertChanges = function () {
        
        var awardLetter = self.StudentAwardLetter();
        awardLetter.IsLetterAccepted(false);
        
        return true;
    }
}