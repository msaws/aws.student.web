﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
var utility = new financialAidUtility();

//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
memory.setItem("newPage", 1);

var awardLetterViewModelInstance = new awardLetterViewModel();

$(document).ready(function () {
    ko.applyBindings(awardLetterViewModelInstance, document.getElementById("main"));
    getAwardLetter();        
});

function getAwardLetter() {
    //Build url    
    var url = utility.buildFinAidUrl(getGeneralLetterDataActionUrl, memory);

    // Get the student's award view model data
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, {}, awardLetterViewModelInstance);

                var awardYears = awardLetterViewModelInstance.AwardYears();
                var selected = memory.getItem("yearCode");
                for (var i = 0; i < awardYears.length; i++) {
                    if (awardYears[i].Code() == selected) {
                        awardLetterViewModelInstance.selectedYear(awardLetterViewModelInstance.AwardYears()[i]);
                        break;
                    }
                }
                //If prerequisites are satisfied for the selected year, get award letter data
                if (awardLetterViewModelInstance.selectedYear().ArePrerequisitesSatisfied()) {
                    var selectedYearCode = awardLetterViewModelInstance.selectedYear().Code();
                    utility.getAwardLetterData(data, selectedYearCode, true);
                }
                else {
                    awardLetterViewModelInstance.showUI(true);
                    memory.setItem("newPage", 0);
                }

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Award letter retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if (jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if (jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faAwardLetterUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            awardLetterViewModelInstance.checkForMobile(window, document);
        }
    });
}