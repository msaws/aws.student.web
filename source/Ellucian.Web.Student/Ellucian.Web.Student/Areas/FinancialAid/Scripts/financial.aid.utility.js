﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates

function financialAidUtility() {
    /* Function that accepts the base url and memory object
    * (local storage or similar) and if the current view is Admin either
    * stores the studentId in the memory or retrives one from the memory 
    */
    this.buildFinAidUrl = function (url, memory) {
        var urlVars = getUrlVars();

        if (urlVars && urlVars[0].contains("Admin")) {
            var index = urlVars[0].indexOf("Admin") + 6;
            var studentId = urlVars[0].substring(index);
            if (studentId) {
                memory.setItem("studentId", studentId);
            }
            else {
                studentId = memory.getItem("studentId");
                if (studentId === null || studentId === "") memory.setItem("studentId", " ");
                url += "/" + studentId;
            }
        }

        return url;
    }

    /* Call to retrieve award letter data for the specified year. 
    * StudentAwardLetter and related information get retrieved based on the getLetterData flag value
    * This function is used by award.letter.js and year.selector.view.model.js
    */
    this.getAwardLetterData = function(viewModelData, selectedYearCode, getLetterData) {

        var url = this.buildFinAidUrl(getAwardLetterDataActionUrl, memory);

        var jsonData = { 'awardLetterViewModel': ko.toJSON(viewModelData), 'selectedYearCode': ko.toJSON(selectedYearCode), 'getLetterData': ko.toJSON(getLetterData) };

        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(jsonData),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function(){
                awardLetterViewModelInstance.showUI(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, awardLetterViewModelMapping, awardLetterViewModelInstance);
                    awardLetterViewModelInstance.showUI(true);
                    awardLetterViewModelInstance.showAllAwardLetterHistoryItems(false);
                    memory.setItem("newPage", 0);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {                
                if (jqXHR.status == 403)
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardLetterNotAuthorizedMessage, type: "error" });
                else if (jqXHR.status == 400)
                    $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
                else if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faAwardLetterUnableToLoadMessage, type: "error" });
            }
        });
    }

   /* Call to retrieve outside awards data for the specified year.    
   * This function is used by outside.awards.js and year.selector.view.model.js
   */
    this.getOutsideAwardsData = function (viewModelData, selectedYearCode) {

        var url = this.buildFinAidUrl(getOutsideAwardsActionUrl, memory);

        var jsonData = { 'outsideAwardsViewModel': ko.toJSON(viewModelData), 'awardYearCode': ko.toJSON(selectedYearCode) };

        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(jsonData),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                outsideAwardsViewModelInstance.showUI(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, outsideAwardsViewModelMapping, outsideAwardsViewModelInstance);
                    outsideAwardsViewModelInstance.showUI(true);
                    outsideAwardsViewModelInstance.displayOutsideAwardsView(true);
                    memory.setItem("newPage", 0);
                    $("#outside-awards-table").makeTableResponsive();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 403)
                    $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
                else if (jqXHR.status == 400)
                    $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
                else if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: faOutsideAwardsUnableToLoadMessage, type: "error" });
            }
        });
    }
}