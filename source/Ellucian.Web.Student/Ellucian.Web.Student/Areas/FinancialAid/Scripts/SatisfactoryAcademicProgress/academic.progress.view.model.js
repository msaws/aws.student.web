﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
function academicProgressViewModel()
{
    var self = this;

    self.showUI = ko.observable();
    BaseViewModel.call(self, 1023);

    //SAPHistory items list
    self.SAPHistoryItems = ko.observableArray();

    //SAP related links list
    self.SAPLinks = ko.observableArray();

    //Latest academic progress evaluation
    self.LatestAcademicProgressEvaluation = ko.observable();

    //Person object
    self.Person = ko.observable();
    //For admin view
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.PrivacyMessage = ko.observable();

    //FA Counselor object
    self.FinancialAidCounselor = ko.observable();

    //Flag to indicate if sap page is to be displayed
    self.IsAcademicProgressActive = ko.observable();

    //Flag to indicate whether the current view is the 
    //detail view of a sap history evaluation item or not
    self.isSAPHistoryItemView = ko.observable(false);    

    //Observable to hold current sap history item data
    self.historyItemData = ko.observable(null);

    //Flag to indicate whether to display all history items or not
    self.showAllSAPHistoryItems = ko.observable(false);

    self.changeToMobile = function () {}

    self.changeToDesktop = function () { }

    //Returns url for user change in admin view
    self.changeUserUrl = ko.observable(academicProgressAdminActionUrl);

    //Determine if there is sap data available
    self.isAcademicProgressDataAvailable = ko.computed(function () {
        var latestEvaluation = ko.utils.unwrapObservable(self.LatestAcademicProgressEvaluation());
        if (latestEvaluation !== null && typeof latestEvaluation !== 'undefined'
            && latestEvaluation.SAPStatus !== null && typeof latestEvaluation.SAPStatus !== 'undefined'
            && self.IsAcademicProgressActive()) return true;
        else return false;
    });

    //Column style is being determined depending on the existence of SAP detail items
    self.sapSectionColumnStyle = ko.computed(function () {
        var latestEvaluation = ko.utils.unwrapObservable(self.LatestAcademicProgressEvaluation());
        if (latestEvaluation !== null && typeof latestEvaluation !== 'undefined'){
            if (latestEvaluation.SAPDetailItems() === null || latestEvaluation.SAPDetailItems().length === 0)
            return "sap-section-full";
        }
        return "sap-section";
    })

    //Pull academic progress status out for easier access
    self.academicProgressStatusCategory = ko.computed(function () {
        var sapStatus = null;
        if (self.isAcademicProgressDataAvailable()) {
            sapStatus = ko.utils.unwrapObservable(self.LatestAcademicProgressEvaluation().SAPStatus);
        }
        return sapStatus !== null ? ko.utils.unwrapObservable(sapStatus.AcademicProgressStatus.Category()) : null;
    });

    //Flag to indicate whether the academic progress is sat or not
    self.isAcademicProgressSatisfactory = ko.computed(function () {
        if (self.academicProgressStatusCategory() === null || typeof self.academicProgressStatusCategory() === 'undefined'
            || self.academicProgressStatusCategory() === 'Satisfactory') return true;
        else return false;
    });

    //Function to toggle the observable indicating whether the current view
    //is sap history item one or not
    self.toggleView = function (data, event) {
        if (self.isSAPHistoryItemView()) {
            self.isSAPHistoryItemView(false);

        }
        else {
            self.historyItemData(data);
            self.isSAPHistoryItemView(true);
            rearrangeMultiAccordionElements($("." + data.AcademicProgressEvaluationId() + "-multi-accordion-content"));
        }
    }; 
    
    //List of history items to display: based on the state of the showAllHistoryItems
    //flag this list will either have all history items visible or just the first four
    self.sapHistoryItemsToDisplay = ko.computed(function () {
        //hardcoding 4 - we want to see only 4 history items initially
        if (!self.showAllSAPHistoryItems()) {
            for (var i = 4; i < self.SAPHistoryItems().length; i++) {
                self.SAPHistoryItems()[i].isItemVisible(false);
            }
        }
        else if (self.showAllSAPHistoryItems()) {
            for (var i = 4; i < self.SAPHistoryItems().length; i++) {
                self.SAPHistoryItems()[i].isItemVisible(true);
            }
        }        
        return ko.utils.unwrapObservable(self.SAPHistoryItems());
    });

    //Function to flip the value of the showAllSAPHistoryItems flag
    self.toggleSAPHistoryView = function () {
        self.showAllSAPHistoryItems(true);           
    }    
}

//Rearranges multi accordion elements to appear in the right order: 
//controller -> controlled panel
function rearrangeMultiAccordionElements(multiAccordionArray) {
    multiAccordionArray.each(function () {
        $(this).next().after($(this));
    });
}