﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();

var academicProgressViewModelInstance = new academicProgressViewModel();

$(document).ready(function () {
    ko.applyBindings(academicProgressViewModelInstance, document.getElementById("main"));
    getAcademicProgress();

});

function getAcademicProgress() {
    //Build url
    var utility = new financialAidUtility();
    var url = utility.buildFinAidUrl(getAcademicProgressUrl, memory);

    // Get the student's academic progress information
    $.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {

                ko.mapping.fromJS(data, academicProgressViewModelMapping, academicProgressViewModelInstance);
                academicProgressViewModelInstance.showUI(true);

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.debug("Academic progress retrieval failed: " + jqXHR.status + " " + jqXHR.responseText);            
            if (jqXHR.status == 403)
                $('#notificationHost').notificationCenter('addNotification', { message: faNotAuthorizedMessage, type: "error" });
            else if (jqXHR.status == 400)
                $('#notificationHost').notificationCenter('addNotification', { message: faMissingStudentIdMessage, type: "error" });
            else if (jqXHR.status == 404)
                $('#notificationHost').notificationCenter('addNotification', { message: faInvalidStudentIdMessage, type: "error" });
            else if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: faAcademicProgressUnableToLoadMessage, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            academicProgressViewModelInstance.checkForMobile(window, document);
            rearrangeElements($('.multi-accordion-content'));

        }
    });
}

//Rearranges multi accordion elements to appear in the right order: 
//controller -> controlled panel
function rearrangeElements(elementArray) {
    elementArray.each(function () {
        $(this).next().after($(this));
    });
}