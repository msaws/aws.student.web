﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
var academicProgressViewModelMapping = {
    'SAPStatus': { 
        create: function (options) {
            return new sapStatusModel(options.data);
        }
    },

    'SAPDetailItems': {
        create: function (options) {
            return new sapDetailItemModel(options.data);
        }
    },
    'AcademicProgressStatus': {
        create: function (options) {
            return new academicProgressStatusModel(options.data);
        }
    },
    'LatestAcademicProgressEvaluation': {
        create: function (options) {
            return new academicProgressEvaluationModel(options);
        }
    },
    'SAPHistoryItems': {
        create: function (options) {
            return new academicProgressEvaluationModel(options);
        }
    }
}

function sapStatusModel(data) {
    var self = this;
    ko.mapping.fromJS(data, academicProgressViewModelMapping, self);

    //Construct the evaluation period value: there will always be at least end term or end date
    self.evaluationPeriod = ko.computed(function () {
        var period = (self.PeriodStartTerm() && self.PeriodEndTerm()) ?
            self.PeriodStartTerm() + ' - ' + self.PeriodEndTerm() :                                                      //both start and end terms are present
            (!self.PeriodStartTerm() && self.PeriodEndTerm()) ?                                                          //no start term but end term is present
            faAcademicProgressEvaluationMissingStartTermDateLabel.format(self.PeriodEndTerm()) :
            (self.PeriodStartDate() && self.PeriodEndDate()) ?                                                           //both start and end dates are present
            Globalize.format(self.PeriodStartDate(), "d") + ' - ' + Globalize.format(self.PeriodEndDate(), "d") :
            faAcademicProgressEvaluationMissingStartTermDateLabel.format(Globalize.format(self.PeriodEndDate(), "d"));   //no start date but end date is present
        return period;
    });

    //Get the css style that is based on the status category (now do by code)
    self.academicProgressStatusStyle = ko.computed(function () {
        var status = ko.utils.unwrapObservable(self.AcademicProgressStatus);
        if (status !== null) {
            if (status.Category() === 'Warning') return "warning";
            else if (status.Category() === 'Unsatisfactory') return "error";
            else if (status.Category() === 'Satisfactory') return "check";
        }
    });

    //Get the statusExplanation header by inserting the status value into 
    //the status explanation header string
    self.statusExplanationHeader = ko.computed(function () {
        var status = ko.utils.unwrapObservable(self.AcademicProgressStatus);
        var statusDescription = status !== null ?
            status.Description() : '';

        return faAcademicProgressStatusExplanationHeader.format(statusDescription);
    });

    //Get the academic progress review date label
    self.academicProgressEvaluationDateLabel = ko.computed(function () {
        var date = self.EvaluationDate() !== null ? Globalize.format(self.EvaluationDate(), "d") : '';
        return faAcademicProgressEvaluationDateLabel.format(date);
    });
}

function sapDetailItemModel(data) {
    var self = this;
    ko.mapping.fromJS(data, academicProgressViewModelMapping, self);
}

function academicProgressStatusModel(data) {
    var self = this;
    ko.mapping.fromJS(data, academicProgressViewModelMapping, self);
}

function academicProgressEvaluationModel(options) {
    var self = this;
    var data = options.data;
    var parent = options.parent;

    ko.mapping.fromJS(data, academicProgressViewModelMapping, self);

    self.evaluationPeriod = ko.computed(function () {
        var sapStatus = ko.utils.unwrapObservable(self.SAPStatus);        
        
        var period = (sapStatus.PeriodStartTerm() && sapStatus.PeriodEndTerm()) ?
            sapStatus.PeriodStartTerm() + ' - ' + sapStatus.PeriodEndTerm() :
            (!sapStatus.PeriodStartTerm() && sapStatus.PeriodEndTerm()) ?
            faAcademicProgressEvaluationMissingStartTermDateLabel.format(sapStatus.PeriodEndTerm()) :
            (sapStatus.PeriodStartDate() && sapStatus.PeriodEndDate()) ?
            Globalize.format(sapStatus.PeriodStartDate(), "d") + ' - ' + Globalize.format(sapStatus.PeriodEndDate(), "d") :
            faAcademicProgressEvaluationMissingStartTermDateLabel.format(Globalize.format(sapStatus.PeriodEndDate(), "d"));
        return period;
    });

    //This is only used for SAP history items. 
    //Header for the SAP History item container
    self.sapHistoryHeader = ko.computed(function () {
        var period = self.evaluationPeriod();
        return faAcademicProgressEvaluationSAPHistoryItemHeader.format(period);
    });

    //Used for SAP History items only
    self.isItemVisible = ko.observable(true);

    self.multiAccordionContentClassName = ko.computed(function () {
        if (parent.isSAPHistoryItemView() === true || parent.showAllSAPHistoryItems() === true) {
            return self.AcademicProgressEvaluationId() + '-multi-accordion-content multi-accordion-content';
        }
        else {
            return 'multi-accordion-content';
        }
    });

    //Sap history item label to use for description property of panel binding
    self.sapHistoryItemLabel = ko.computed(function () {
        var period = self.evaluationPeriod();
        return faAcademicProgressEvaluationViewHistoryItemLabel.format(period);
    });
}