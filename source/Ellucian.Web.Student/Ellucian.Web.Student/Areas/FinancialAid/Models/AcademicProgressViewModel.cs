﻿/*Copyright 2015-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// AcademicProgressViewModel class
    /// </summary>
    public class AcademicProgressViewModel : FinancialAidAdminModel
    {
        /// <summary>
        /// Latest Academic Progress Evaluation
        /// </summary>
        public AcademicProgressEvaluation LatestAcademicProgressEvaluation { get; set; }

        /// <summary>
        /// List of SAP History academic evaluations
        /// </summary>
        public List<AcademicProgressEvaluation> SAPHistoryItems { get; set; }

        /// <summary>
        ///List of helpful sap related links 
        /// </summary>
        public List<Colleague.Dtos.FinancialAid.Link> SAPLinks { get; set; }

        /// <summary>
        /// Person data describing the student
        /// </summary>
        public FAStudent Person { get; set; }

        /// <summary>
        /// Financial Aid Counselor for the student with the most recent year's data
        /// </summary>
        public Counselor FinancialAidCounselor { get; set; }

        /// <summary>
        /// Indicates whether to display data on the webpage or not.
        /// </summary>
        public bool IsAcademicProgressActive { get; set; }

        /// <summary>
        /// Indicates whether the current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }

        /// <summary>
        /// AcademicProgressViewModel constructor
        /// </summary>
        /// <param name="personData">person data(student/applicant)</param>
        /// <param name="academicProgressEvaluationsData">list of academic progress evaluations</param>
        /// <param name="academicProgressStatusesData">list of academic progress evaluation statuses</param>
        /// <param name="financialAidCounselorData">financial aid counselor data</param>
        /// <param name="counselorsPhoneNumbers">set of counselor phone numbers</param>
        /// <param name="studentAwardYearsData">list of student fa years</param>
        /// <param name="financialAidOfficesData">list of financial aid offices</param>
        /// <param name="linksData">list of links</param>
        /// <param name="academicProgramsData">list of academic programs</param>
        /// <param name="academicProgressAppealCodesData">list of appeal codes</param>
        /// <param name="appealsCounselorsData">list of fa counselors</param>
        /// <param name="currentUser">current user</param>
        public AcademicProgressViewModel(Colleague.Dtos.Base.Person personData,
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluationsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatusesData,
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode> academicProgressAppealCodesData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidCounselor> appealsCounselorsData,
            Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData,            
            IEnumerable<Colleague.Dtos.Base.PhoneNumber> counselorsPhoneNumbers,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficesData,
            IEnumerable<Colleague.Dtos.FinancialAid.Link> linksData,
            IEnumerable<Colleague.Dtos.Student.AcademicProgram> academicProgramsData,
            ICurrentUser currentUser)
        {

            SAPHistoryItems = new List<AcademicProgressEvaluation>();
            SAPLinks = new List<Colleague.Dtos.FinancialAid.Link>();

            if (personData == null)
            {
                Person = new FAStudent();
            }
            else
            {
                var personType = personData.GetType();
                if (personType == typeof(Colleague.Dtos.Student.Student))
                {
                    Person = new FAStudent(personData);
                }
                else
                {
                    var applicant = (Colleague.Dtos.Student.Applicant)personData;
                    Person = new FAStudent(applicant);
                }
            }

            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();

            Colleague.Dtos.FinancialAid.StudentAwardYear2 mostRecentAwardYear = null;
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> activeStudentAwardYears = new List<StudentAwardYear2>();

            if (financialAidOfficesData == null)
            {
                financialAidOfficesData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }
            var configurationDtos = financialAidOfficesData.SelectMany(o => o.Configurations).ToList();

            activeStudentAwardYears = studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos);

            mostRecentAwardYear = (activeStudentAwardYears != null && activeStudentAwardYears.Any()) ? activeStudentAwardYears.OrderByDescending(say => say.Code).First() : null;

            //Get most recent student's FA Office
            FinancialAidOffice3 financialAidOffice = null;
            if (mostRecentAwardYear != null)
            {
                financialAidOffice = financialAidOfficesData.FirstOrDefault(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);
            }

            if (financialAidOffice == null ||
                financialAidOffice.AcademicProgressConfiguration == null ||
                !financialAidOffice.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive)
            {
                IsAcademicProgressActive = false;
            }
            else
            {
                IsAcademicProgressActive = true;
                try
                {
                    var counselorPhoneNumber = financialAidCounselorData != null && counselorsPhoneNumbers != null ? counselorsPhoneNumbers.FirstOrDefault(cpn => cpn.PersonId == financialAidCounselorData.Id) : null;
                    FinancialAidCounselor = new Counselor(financialAidCounselorData, counselorPhoneNumber, mostRecentAwardYear, financialAidOfficesData);
                }
                catch (Exception) { }

                if (linksData != null && linksData.Any())
                {
                    SAPLinks.AddRange(linksData.Where(l => l.LinkType == LinkTypes.SatisfactoryAcademicProgress));
                }

                //Filter out evaluations to use by type
                academicProgressEvaluationsData = academicProgressEvaluationsData != null && academicProgressEvaluationsData.Any() && financialAidOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay.Any() ?
                    academicProgressEvaluationsData.Where(e => financialAidOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay.Any(pt => pt.ToUpper() == e.AcademicProgressTypeCode.ToUpper())).ToList() :
                    academicProgressEvaluationsData;

                var latestAcademicProgressEvaluation = new AcademicProgressEvaluation(academicProgressEvaluationsData, academicProgressStatusesData,
                    academicProgressAppealCodesData, appealsCounselorsData, counselorsPhoneNumbers, academicProgramsData, financialAidOffice, mostRecentAwardYear);

                LatestAcademicProgressEvaluation = latestAcademicProgressEvaluation.SAPStatus != null ? latestAcademicProgressEvaluation : null;

                if (LatestAcademicProgressEvaluation != null && LatestAcademicProgressEvaluation.SAPAppeal != null && LatestAcademicProgressEvaluation.SAPAppeal.AppealAdvisor == null)
                {
                    LatestAcademicProgressEvaluation.SAPAppeal.AppealAdvisor = FinancialAidCounselor;
                }

                //Create SAP History items only if the SAP History flag is on
                if (financialAidOffice.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressHistoryActive && academicProgressEvaluationsData != null
                    && LatestAcademicProgressEvaluation != null && LatestAcademicProgressEvaluation.SAPStatus != null)
                {
                    //Statuses that have category of "Do not display"
                    var doNotDisplayStatuses = (academicProgressStatusesData != null && academicProgressStatusesData.Any()) ? academicProgressStatusesData.Where(aps => aps.Category == AcademicProgressStatusCategory.DoNotDisplay) :
                        new List<AcademicProgressStatus>();

                    //Exclude the most recent evaluation as well as evaluations in "Do not display" status category
                    var sameTypePastEvaluations = academicProgressEvaluationsData.Where(ape => ape.EvaluationDateTime != LatestAcademicProgressEvaluation.SAPStatus.EvaluationDate 
                        && doNotDisplayStatuses.All(dds => dds.Code != ape.StatusCode)
                        && ape.AcademicProgressTypeCode.ToUpper() == LatestAcademicProgressEvaluation.AcademicProgressEvaluationType.ToUpper());

                    //Sort all evaluations in descending order and get only the specified number from that list
                    if (sameTypePastEvaluations != null && sameTypePastEvaluations.Any())
                    {
                        sameTypePastEvaluations = sameTypePastEvaluations.OrderByDescending(pe => pe.EvaluationDateTime).Take(financialAidOffice.AcademicProgressConfiguration.NumberOfAcademicProgressHistoryRecordsToDisplay).ToList();
                    }

                    foreach (var eval in sameTypePastEvaluations)
                    {
                        var pastEvaluation = new AcademicProgressEvaluation(eval, academicProgressStatusesData, academicProgressAppealCodesData, 
                            appealsCounselorsData, counselorsPhoneNumbers, academicProgramsData, financialAidOffice, mostRecentAwardYear);
                        if (pastEvaluation != null && pastEvaluation.SAPStatus != null)
                        {
                            if (pastEvaluation.SAPAppeal != null && pastEvaluation.SAPAppeal.AppealAdvisor == null)
                            {
                                pastEvaluation.SAPAppeal.AppealAdvisor = FinancialAidCounselor;
                            }
                            SAPHistoryItems.Add(pastEvaluation);
                        }

                    }
                }
            }
        }
    }
}