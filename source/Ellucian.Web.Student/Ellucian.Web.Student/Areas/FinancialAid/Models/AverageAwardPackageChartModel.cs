﻿/*Copyright 2014-2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Linq;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class AverageAwardPackageChartModel : ChartModel
    {
        public int AverageGrantAmount { get; set; }
        public int AverageLoanAmount { get; set; }
        public int AverageScholarshipAmount { get; set; }
        public string AwardYearCode { get; set; } 

        /// <summary>
        /// Flag to indicate if link to detailed average award package 
        /// information was passed
        /// </summary>
        public bool IsAverageAwardPackageInfoLinkPresent { get; set; }

        public AverageAwardPackageChartModel(int averageGrantAmount, int averageLoanAmount, int averageScholarshipAmount, string awardYearCode)
        {
            // The Repository throws an error for negative amounts and we only want to suppress the table when all values = zero.
            
            if (averageGrantAmount < 0)
            {
                throw new ApplicationException("Average Grant Amount cannot be a negative amount.");
            }

            if (averageLoanAmount < 0)
            {
                throw new ApplicationException("Average Loan Amount cannot be a negative amount.");
            }

            if (averageScholarshipAmount < 0)
            {
                throw new ApplicationException("Average Scholarship Amount cannot be a negative amount.");
            }

            // When one of the table elements is zero the chart handles it.
            if ( (averageGrantAmount == 0) && (averageLoanAmount == 0) && (averageScholarshipAmount == 0) )
            {
                throw new ApplicationException("All average package amounts are zero or less. Cannot create chart model");
            }

            if (string.IsNullOrEmpty(awardYearCode))
            {
                throw new ApplicationException("Award year is null or empty. Cannot create a chart model.");
            }

            AverageGrantAmount = averageGrantAmount;
            AverageLoanAmount = averageLoanAmount;
            AverageScholarshipAmount = averageScholarshipAmount;
            AwardYearCode = awardYearCode;

            bool hasMultipleNonZeroAverages = (averageGrantAmount > 0 && averageLoanAmount > 0) || (averageGrantAmount > 0 && averageScholarshipAmount > 0) || (averageScholarshipAmount > 0 && averageLoanAmount > 0) ? true : false;

            ChartData = new List<Data>()
            {
                new Data() {value = AverageGrantAmount, color = "#88ddfd"},
                new Data() {value = AverageLoanAmount, color = "#6aca3f"},
                new Data() {value = AverageScholarshipAmount, color = "#fc361d"}
            };

            ChartOptions = new Options()
            {
                segmentShowStroke = hasMultipleNonZeroAverages,
                segmentStrokeColor = "#f7f7f7",
                segmentStrokeWidth = 2,
                animation = false
            };
        }        

    }
}