﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using Ellucian.Colleague.Dtos.FinancialAid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// SAP Status class
    /// </summary>
    public class SAPStatus
    {
        /// <summary>
        /// A pair of start and end terms for evaluation period,
        /// might be null
        /// </summary>
        public string PeriodStartTerm { get; set; }
        public string PeriodEndTerm { get; set; }

        /// <summary>
        /// A pair of start and end dates for the evaluation period,
        /// might be null
        /// </summary>
        public DateTimeOffset? PeriodStartDate { get; set; }
        public DateTimeOffset? PeriodEndDate { get; set; }

        /// <summary>
        /// Date the academic progress was evaluated
        /// </summary>
        public DateTimeOffset? EvaluationDate { get; set; }

        /// <summary>
        /// Academic Progress Status: Code, Description, Explanation, Category
        /// </summary>
        public AcademicProgressStatus AcademicProgressStatus { get; set; }

        /// <summary>
        /// Student academic program at the time of evaluation
        /// </summary>
        public string StudentAcademicProgram { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SAPStatus()
        {

        }

        /// <summary>
        /// Constructor that accepts most recent evaluation and academic progress status dtos
        /// </summary>
        /// <param name="academicProgressEvaluation">most recent academic progress evaluation</param>
        /// <param name="academicProgressStatus">academic progress status dto corresponding to that evaluation</param>
        /// <param name="studentAcademicProgram">academic program student is in</param>
        public SAPStatus(Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicProgressEvaluation,
            Colleague.Dtos.FinancialAid.AcademicProgressStatus academicProgressStatus, 
            Colleague.Dtos.Student.AcademicProgram studentAcademicProgram)
        {
            if (academicProgressEvaluation == null)
            {
                throw new ArgumentNullException("academicProgressEvaluation");
            }
            if (academicProgressStatus == null)
            {
                throw new ArgumentNullException("academicProgressStatus");
            }

            PeriodEndDate = academicProgressEvaluation.EvaluationPeriodEndDate;
            PeriodEndTerm = academicProgressEvaluation.EvaluationPeriodEndTerm;
            PeriodStartDate = academicProgressEvaluation.EvaluationPeriodStartDate;
            PeriodStartTerm = academicProgressEvaluation.EvaluationPeriodStartTerm;

            //Force adherence to carriage returns/new lines on explanation
            if (academicProgressStatus != null && !string.IsNullOrEmpty(academicProgressStatus.Explanation))
            {
                academicProgressStatus.Explanation = academicProgressStatus.Explanation.Replace("" + Environment.NewLine, "<br />");
            }
            AcademicProgressStatus = academicProgressStatus;
            
            EvaluationDate = academicProgressEvaluation.EvaluationDateTime;
            StudentAcademicProgram = studentAcademicProgram != null ? studentAcademicProgram.Description : null;
        }
        
    }
}