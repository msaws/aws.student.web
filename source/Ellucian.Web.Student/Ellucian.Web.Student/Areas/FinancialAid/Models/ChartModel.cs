﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// General ChartModel class
    /// </summary>
    public abstract class ChartModel
    {
        public List<Data> ChartData { get; set; }

        public Options ChartOptions { get; set; }

        public class Data
        {
            public int value;
            public string color;
        }

        public class Options
        {
            public bool segmentShowStroke { get; set; }
            public string segmentStrokeColor { get; set; }
            public int segmentStrokeWidth { get; set; }
            public bool animation { get; set; }
            public int animationSteps { get; set; }
            public string animationEasing { get; set; }
            public bool animateRotate { get; set; }
            public bool animateScale { get; set; }
            public object onAnimationComplete { get; set; }
            public int percentageInnerCutout { get; set; }
        }
    }
}