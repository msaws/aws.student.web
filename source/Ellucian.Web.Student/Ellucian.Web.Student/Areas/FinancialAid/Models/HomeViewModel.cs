﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Security;


namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Home View Model contains information for checklist items and
    /// other items on the home page of the FA_HUB
    /// </summary>
    public class HomeViewModel : FinancialAidAdminModel
    {
        /// <summary>
        /// List of fa award years for drop down
        /// </summary>
        public IEnumerable<AwardYear> AwardYears { get; set; }

        /// <summary>
        /// List of checklists for each fa year
        /// </summary>
        public List<StudentChecklist> StudentChecklists { get; set; }

        /// <summary>
        /// List of loans a student has
        /// </summary>
        public StudentLoansSummary StudentLoansSummary { get; set; }

        /// <summary>
        /// Average award Package set across all active award years to display to the student
        /// </summary>
        public List<AverageAwardPackageChartModel> AverageAwardPackages { get; set; }

        /// <summary>
        /// List of form links to display on home page
        /// </summary>
        public List<Colleague.Dtos.FinancialAid.Link> FormLinks { get; set; }

        /// <summary>
        /// List of useful links to display on home page
        /// </summary>
        public List<Colleague.Dtos.FinancialAid.Link> HelpfulLinks { get; set; }

        /// <summary>
        /// Student or Applicant Person data.
        /// </summary>
        public Colleague.Dtos.Base.Person Person { get; set; }

        /// <summary>
        /// Indicates whether the current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }

        /// <summary>
        /// Indicates whether the current user is admin (and not self)
        /// </summary>
        public bool IsAdminView { get; set; }

        /// <summary>
        /// Financial Aid Self Service Configuration 
        /// </summary>
        public StudentFinancialAidConfiguration Configuration { get; set; }

        /// <summary>
        /// SAP Status data
        /// </summary>
        public SAPStatus SAPStatus { get; set; }

        /// <summary>
        /// Student Pell LEU Amount
        /// </summary>
        public decimal? PellLifetimeEligibilityUsedPercentage { get; set; }

        /// <summary>
        /// Student account summary
        /// </summary>
        public StudentFinanceAccountSummary StudentAccountSummary { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="studentAwardYearsData">List of all Fa years on student's record</param>
        /// <param name="studentDocumentsData">List of student documents for all award years</param>
        /// <param name="studentAwardsData">List of student awards across years</param>
        /// <param name="awardsData">List of Financial Aid Awards</param>
        /// <param name="awardStatusesData">List of possible award statuses</param>
        /// <param name="studentAwardLettersData">List of student award letters across award years</param>
        /// <param name="studentFaApplicationsData">List of student Application items' booleans for completeness/incompleteness</param>
        /// <param name="studentLoanSummaryData">List of student's MPN and Entrance Interview Dates</param>
        /// <param name="averageAwardPackageData">Student's average award package, could be null.</param>
        /// <param name="communicationCodesData">List of communication codes</param>
        /// <param name="counselorPhoneNumbers">List of counselor phone numbers</param>
        /// <param name="financialAidChecklistItemsData">List of generic financial aid checklist items</param>
        /// <param name="financialAidCounselorData">Financial Aid counselor related info</param>
        /// <param name="financialAidOfficeData">List of financial aid offices</param>
        /// <param name="ipedsInstitutionData">List of IpedInstitution objects</param>
        /// <param name="linksData">List of links</param>
        /// <param name="officeCodesData">List of office codes</param>
        /// <param name="personData">Information about person(student or applicant)</param>
        /// <param name="studentChecklistsData">List of student checklists across fa years on student's file</param>
        /// <param name="academicProgressEvaluationsData">List of academic progress evaluations</param>
        /// <param name="academicProgressStatusesData">List of academic progress statuses</param>
        /// <param name="studentNsldsData">student NSLDS related data</param>
        /// <param name="studentAccountDueData">student account due data</param>
        /// <param name="currentUser">Current user</param>
        /// <param name="isUserAdminOnly">flag indicating whether user is admin only</param>
        public HomeViewModel(Colleague.Dtos.Base.Person personData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentsData,
            IEnumerable<Colleague.Dtos.Base.CommunicationCode2> communicationCodesData,
            IEnumerable<Colleague.Dtos.Base.OfficeCode> officeCodesData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> studentAwardsData,
            IEnumerable<Colleague.Dtos.FinancialAid.Award2> awardsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardStatus> awardStatusesData,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardLetter2> studentAwardLettersData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidApplication2> studentFaApplicationsData,
            Colleague.Dtos.FinancialAid.StudentLoanSummary studentLoanSummaryData,
            IEnumerable<Colleague.Dtos.FinancialAid.IpedsInstitution> ipedsInstitutionData,
            IEnumerable<Colleague.Dtos.FinancialAid.AverageAwardPackage> averageAwardPackageData,
            IEnumerable<Colleague.Dtos.FinancialAid.Link> linksData,
            Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist> studentChecklistsData,
            IEnumerable<Colleague.Dtos.FinancialAid.ChecklistItem> financialAidChecklistItemsData,
            Colleague.Dtos.Base.PhoneNumber counselorPhoneNumbers,
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluationsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatusesData,
            Colleague.Dtos.FinancialAid.StudentNsldsInformation studentNsldsData,
            Ellucian.Colleague.Dtos.Finance.AccountDue.AccountDue studentAccountDueData, 
            ICurrentUser currentUser, bool isUserAdminOnly)
        {
            Person = personData;
            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;

            //If there is a proxy subject - the current user is a proxy
            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();
            IsAdminView = isUserAdminOnly;

            var awardYears = new List<AwardYear>();
            StudentChecklists = new List<StudentChecklist>();
            StudentLoansSummary = new StudentLoansSummary();
            FormLinks = new List<Colleague.Dtos.FinancialAid.Link>();
            HelpfulLinks = new List<Colleague.Dtos.FinancialAid.Link>();
            AverageAwardPackages = new List<AverageAwardPackageChartModel>();

            if (financialAidOfficeData == null)
            {
                financialAidOfficeData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }

            var configurationDtos = financialAidOfficeData.SelectMany(o => o.Configurations).ToList();
            Configuration = new StudentFinancialAidConfiguration(studentAwardYearsData, configurationDtos);

            var activeStudentAwardYearDtos = studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos).OrderByDescending(say => say.Code);

            var financialAidOffice = (financialAidOfficeData.Count() > 0 && activeStudentAwardYearDtos.Count() > 0) ? financialAidOfficeData.FirstOrDefault(fao => fao.Id == activeStudentAwardYearDtos.First().FinancialAidOfficeId) : null;

            if (financialAidOffice != null && financialAidOffice.AcademicProgressConfiguration != null && financialAidOffice.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive)
            {
                //Filter out evaluations by type
                academicProgressEvaluationsData = academicProgressEvaluationsData != null && academicProgressEvaluationsData.Any() && financialAidOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay.Any() ?
                    academicProgressEvaluationsData.Where(e => financialAidOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay.Any(pt => pt.ToUpper() == e.AcademicProgressTypeCode.ToUpper())).ToList() :
                    academicProgressEvaluationsData;
                SAPStatus = SAPUtility.GetLatestSAPStatus(academicProgressEvaluationsData, academicProgressStatusesData, null);
            }

            #region Links
            if (linksData != null)
            {
                foreach (var link in linksData)
                {
                    if (link.LinkType == LinkTypes.Form)
                    {
                        FormLinks.Add(link);
                    }
                    else if(link.LinkType != LinkTypes.SatisfactoryAcademicProgress)
                    {
                        HelpfulLinks.Add(link);
                    }
                }
                HelpfulLinks = HelpfulLinks.OrderBy(ul => ul.LinkType, new LinkTypeComparer()).ToList();
            }
            #endregion

            #region AverageAwardPackage
            if (averageAwardPackageData != null)
            {
                foreach (var averageAwardPackage in averageAwardPackageData)
                {
                    try
                    {
                        var averageAwardPackageChartModel = new AverageAwardPackageChartModel(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount,
                                                                                    averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);
                        //Temporary:
                        averageAwardPackageChartModel.IsAverageAwardPackageInfoLinkPresent = false;
                        AverageAwardPackages.Add(averageAwardPackageChartModel);
                    }

                    catch (Exception) { /*Just continue with the rest of aaps*/ }

                }
            }
            #endregion

            #region StudentLoanSummary
            if (studentLoanSummaryData != null)
            {
                StudentLoansSummary = new Models.StudentLoansSummary(studentLoanSummaryData, ipedsInstitutionData);
            }
            #endregion

            #region StudentChecklist
            
            foreach (var activeStudentAwardYearDto in activeStudentAwardYearDtos)
            {
                var configurationDto = configurationDtos.GetConfigurationDtoOrDefault(activeStudentAwardYearDto);

                //Create student checklist for each year
                var studentChecklistForYear = new StudentChecklist(activeStudentAwardYearDto);
                //var outstandingDocuments = false;

                //Get list of Fa Applications' booleans for the current year
                var faApplicationsForYear = (studentFaApplicationsData != null) ?
                    studentFaApplicationsData.Where(sfa => sfa.AwardYear == activeStudentAwardYearDto.Code)
                    : new List<FinancialAidApplication2>();

                //Get checklist for the year
                var studentChecklistDataForYear = studentChecklistsData.FirstOrDefault(sc => sc.AwardYear == activeStudentAwardYearDto.Code);
                bool areChecklistItemsAssigned = (studentChecklistDataForYear != null && studentChecklistDataForYear.ChecklistItems != null && studentChecklistDataForYear.ChecklistItems.Count != 0);

                //Add award year to the list of award years
                var studentAwardYear = new AwardYear(activeStudentAwardYearDto);
                studentAwardYear.AreChecklistItemsAssigned = areChecklistItemsAssigned;
                try
                {
                    studentAwardYear.Counselor = new Counselor(financialAidCounselorData, counselorPhoneNumbers, activeStudentAwardYearDto, financialAidOfficeData);
                }
                catch (Exception) {/*Doesn't matter if a counselor is created or not*/ }
                awardYears.Add(studentAwardYear);


                if (areChecklistItemsAssigned && financialAidChecklistItemsData != null && configurationDto != null)
                {
                    //Create a flag to indicate if there is an item that's both required and incomplete
                    var isAnyItemRequiredAndIncomplete = false;

                    #region Profile ChecklistItem

                    var profileInfo = linksData.FirstOrDefault(l => l.LinkType == LinkTypes.PROFILE);
                    var profileTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.PROFILE);

                    var studentProfileItem = profileTypeChecklistItem != null ? studentChecklistDataForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == profileTypeChecklistItem.ChecklistItemCode) : null;
                    
                    if (studentProfileItem != null)
                    {
                        var profileChecklistItem = new ChecklistItem()
                        {
                            Type = ChecklistItemType.Profile,
                            Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ProfileDescription"),
                            Link = profileInfo != null ? profileInfo.LinkUrl : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ProfileLink"),
                            Target = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ExternalLinkTarget"),
                            Title = profileInfo != null ? profileInfo.Title : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ProfileTitle"),
                            Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ProfileSummary"),
                            ControlStatus = studentProfileItem != null ? studentProfileItem.ControlStatus : ChecklistItemControlStatus.CompletionRequired
                        };

                        //does the student need a profile for the year?
                        if (configurationDto.IsProfileActive &&
                            profileChecklistItem.ControlStatus != ChecklistItemControlStatus.RemovedFromChecklist)
                        {
                            if (faApplicationsForYear.Any(app => app.GetType() == typeof(ProfileApplication)))
                            {
                                profileChecklistItem.Status = ChecklistItemStatus.Complete;
                                profileChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                profileChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                profileChecklistItem.IsActiveItem = true;
                                if (profileChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired)
                                {
                                    isAnyItemRequiredAndIncomplete = true;
                                }
                            }
                            studentChecklistForYear.ChecklistItems.Add(profileChecklistItem);
                        }
                    }
                    #endregion

                    #region FAFSA ChecklistItem

                    var fafsaInfo = linksData.FirstOrDefault(l => l.LinkType == LinkTypes.FAFSA);

                    var fafsaTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.FAFSA);
                    var studentFafsaItem = fafsaTypeChecklistItem != null ? studentChecklistDataForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == fafsaTypeChecklistItem.ChecklistItemCode) : null;

                    if (studentFafsaItem != null)
                    {
                        var fafsaChecklistItem = new ChecklistItem()
                            {
                                Type = ChecklistItemType.FAFSA,
                                Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAFSADescription"),
                                Link = fafsaInfo != null ? fafsaInfo.LinkUrl : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAFSALink"),
                                Target = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ExternalLinkTarget"),
                                Title = fafsaInfo != null ? fafsaInfo.Title : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAFSATitle"),
                                Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAFSASummary"),
                                ControlStatus = studentFafsaItem != null ? studentFafsaItem.ControlStatus : ChecklistItemControlStatus.CompletionRequired
                            };

                        if (fafsaChecklistItem.ControlStatus != ChecklistItemControlStatus.RemovedFromChecklist)
                        {
                            if (isAnyItemRequiredAndIncomplete)
                            {
                                fafsaChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                                fafsaChecklistItem.IsActiveItem = false;

                            }
                            else if (faApplicationsForYear.Any(app => app.GetType() == typeof(Fafsa)))
                            {
                                fafsaChecklistItem.Status = ChecklistItemStatus.Complete;
                                fafsaChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                fafsaChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                fafsaChecklistItem.IsActiveItem = true;
                                if (fafsaChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired)
                                {
                                    isAnyItemRequiredAndIncomplete = true;
                                }
                            }

                            studentChecklistForYear.ChecklistItems.Add(fafsaChecklistItem);
                        }
                    }

                    #endregion

                    #region Documents ChecklistItem

                    var studentDocumentCollection = new StudentDocumentCollection(activeStudentAwardYearDto, studentDocumentsData, communicationCodesData, officeCodesData, configurationDto);

                    var documentTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.CompletedDocuments);
                    var studentDocumentsItem = documentTypeChecklistItem != null ? studentChecklistDataForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == documentTypeChecklistItem.ChecklistItemCode) : null;

                    if (studentDocumentsItem != null)
                    {
                        //Create documents checklist item
                        var documentsChecklistItem = new ChecklistItem()
                        {
                            Type = ChecklistItemType.Documents,
                            Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DocumentsDescription"),
                            Link = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DocumentsLink"),
                            Title = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DocumentsTitle"),
                            Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DocumentsSummary"),
                            ControlStatus = studentDocumentsItem != null ? studentDocumentsItem.ControlStatus : ChecklistItemControlStatus.CompletionRequired,
                            IsReadOnly = IsProxyView
                        };

                        if (documentsChecklistItem.ControlStatus != ChecklistItemControlStatus.RemovedFromChecklist)
                        {
                            //Calculate documents item's status
                            if (isAnyItemRequiredAndIncomplete)
                            {
                                documentsChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                                documentsChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                if (studentDocumentCollection.HasOutstandingDocuments)
                                {
                                    documentsChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                    documentsChecklistItem.IsActiveItem = true;
                                    if (documentsChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired)
                                    {
                                        isAnyItemRequiredAndIncomplete = true;
                                    }
                                }
                                else
                                {
                                    documentsChecklistItem.Status = ChecklistItemStatus.Complete;
                                    documentsChecklistItem.IsActiveItem = false;
                                }

                            }

                            studentChecklistForYear.ChecklistItems.Add(documentsChecklistItem);
                        }
                    }

                    #endregion

                    #region Application Reviewed ChecklistItem

                    var applicationReviewTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.ApplicationReview);
                    var studentApplicationReviewedItem = applicationReviewTypeChecklistItem != null ? studentChecklistDataForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == applicationReviewTypeChecklistItem.ChecklistItemCode)
                                                        : null;

                    if (studentApplicationReviewedItem != null)
                    {
                        //Create fa Application reviewed item
                        var faApplicationChecklistItem = new ChecklistItem()
                            {
                                Type = ChecklistItemType.Application,
                                Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAApplicationDescription"),
                                Link = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAApplicationLink"),
                                Target = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ExternalLinkTarget"),
                                Title = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAApplicationTitle"),
                                Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAApplicationSummary"),
                                ControlStatus = studentApplicationReviewedItem != null ? studentApplicationReviewedItem.ControlStatus : ChecklistItemControlStatus.CompletionRequired
                            };

                        if (faApplicationChecklistItem.ControlStatus != ChecklistItemControlStatus.RemovedFromChecklist)
                        {
                            //Calculate fa Application item's status
                            if (isAnyItemRequiredAndIncomplete)
                            {
                                faApplicationChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                                faApplicationChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                if (activeStudentAwardYearDto.IsApplicationReviewed)
                                {
                                    faApplicationChecklistItem.Status = ChecklistItemStatus.Complete;
                                    faApplicationChecklistItem.IsActiveItem = false;
                                }
                                else
                                {
                                    faApplicationChecklistItem.Status = ChecklistItemStatus.InProgress;
                                    faApplicationChecklistItem.IsActiveItem = true;
                                    if (faApplicationChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired)
                                    {
                                        isAnyItemRequiredAndIncomplete = true;
                                    }
                                }
                            }

                            studentChecklistForYear.ChecklistItems.Add(faApplicationChecklistItem);
                        }
                    }

                    #endregion

                    #region MyAwards ChecklistItem

                    var areAllAwardsAccepted = true;

                    //Get studentAwards for the year     
                    var studentAwardsForYear = studentAwardsData.ToList().FindAll(sa => sa.AwardYearId == activeStudentAwardYearDto.Code);

                    //Get awardStatuses' codes that correspond "pending" or "estimated" status
                    var pendingStatusCodes = awardStatusesData.ToList().FindAll(ast =>
                        ast.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Pending ||
                        ast.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Estimated);

                    //Check if any awards for the year have a pending status
                    foreach (var award in studentAwardsForYear)
                    {
                        if (award.StudentAwardPeriods.Any(sap => pendingStatusCodes.Any(ast => ast.Code == sap.AwardStatusId)))
                        {
                            areAllAwardsAccepted = false;
                            break;
                        }
                    }

                    var awardPackageTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.ReviewAwardPackage);
                    var studentAwardsPackageItem = awardPackageTypeChecklistItem != null ? studentChecklistDataForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == awardPackageTypeChecklistItem.ChecklistItemCode) : null;

                    if (studentAwardsPackageItem != null)
                    {
                        //Create awards item
                        var faAwardsChecklistItem = new ChecklistItem()
                        {
                            Type = ChecklistItemType.Awards,
                            Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardsDecription"),
                            Link = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardsLink"),
                            Title = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardsTitle"),
                            Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardsSummary"),
                            ControlStatus = studentAwardsPackageItem != null ? studentAwardsPackageItem.ControlStatus : ChecklistItemControlStatus.CompletionRequired,
                            IsReadOnly = IsProxyView
                        };

                        if (configurationDto.IsAwardingActive &&
                            faAwardsChecklistItem.ControlStatus != ChecklistItemControlStatus.RemovedFromChecklist)
                        {
                            //Calculate faAwardsChecklistItem's status
                            if (isAnyItemRequiredAndIncomplete)
                            {
                                faAwardsChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                                faAwardsChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                if (!areAllAwardsAccepted)
                                {
                                    faAwardsChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                    faAwardsChecklistItem.IsActiveItem = true;
                                }
                                else
                                {
                                    faAwardsChecklistItem.Status = ChecklistItemStatus.Complete;
                                    faAwardsChecklistItem.IsActiveItem = false;
                                }
                            }
                            studentChecklistForYear.ChecklistItems.Add(faAwardsChecklistItem);
                        }
                    }

                    #endregion

                    #region DirectLoan Interview ChecklistItem

                    var isDirectLoanPresent = false;

                    //Determine if there are sub/unsub loans, set the flag
                    var directLoanAwardIds = awardsData.Where(
                        a => a.LoanType.HasValue &&
                            (a.LoanType.Value == LoanType.SubsidizedLoan || a.LoanType.Value == LoanType.UnsubsidizedLoan)
                            ).Select(a => a.Code);
                    var directLoanStudentAwards = studentAwardsForYear.Where(sa => directLoanAwardIds.Contains(sa.AwardId));

                    //if there are any direct loan studentAward objects and the calculated award status for all those direct loan
                    //student awards is not rejected, then the direct loan is present, and direct loan specific checklist items
                    //can be added
                    if (directLoanStudentAwards.Count() > 0)
                    {
                        var directLoanPeriods = directLoanStudentAwards.SelectMany(dl => dl.StudentAwardPeriods);
                        var directLoanStatus = AwardStatusEvaluator.CalculateAwardStatus(directLoanPeriods, awardStatusesData);
                        if (directLoanStatus != null && directLoanStatus.Category != AwardStatusCategory.Rejected && directLoanStatus.Category != AwardStatusCategory.Denied)
                        {
                            isDirectLoanPresent = true;
                        }
                    }

                    //DL entrance interview 
                    var directLoanEntranceInterviewDate = (studentLoanSummaryData != null) ?
                        studentLoanSummaryData.DirectLoanEntranceInterviewDate :
                        null;

                    //Get the Entrance interview info
                    var entranceInterviewInfo = linksData.FirstOrDefault(l => l.LinkType == LinkTypes.EntranceInterview);

                    var directLoanInterviewChecklistItem = new ChecklistItem()
                    {
                        Type = ChecklistItemType.Interview,
                        Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanEntranceInterviewDescription"),
                        Link = entranceInterviewInfo != null ? entranceInterviewInfo.LinkUrl : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanEntranceInterviewLink"),
                        Target = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ExternalLinkTarget"),
                        Title = entranceInterviewInfo != null ? entranceInterviewInfo.Title : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanEntranceInterviewTitle"),
                        Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanEntranceInterviewSummary"),
                        ControlStatus = ChecklistItemControlStatus.CompletionRequired
                    };

                    //only calc the checklist item status if the student has a direct loan award
                    if (configurationDto.IsAwardingActive &&
                        isDirectLoanPresent)
                    {

                        if (isAnyItemRequiredAndIncomplete)
                        {
                            directLoanInterviewChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                            directLoanInterviewChecklistItem.IsActiveItem = false;
                        }
                        else
                        {
                            if (directLoanEntranceInterviewDate.HasValue)
                            {
                                directLoanInterviewChecklistItem.Status = ChecklistItemStatus.Complete;
                                directLoanInterviewChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                directLoanInterviewChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                directLoanInterviewChecklistItem.IsActiveItem = true;
                            }
                        }

                        studentChecklistForYear.ChecklistItems.Add(directLoanInterviewChecklistItem);
                    }

                    #endregion

                    #region GradPlus Interview ChecklistItem

                    var isGradPlusLoanPresent = false;

                    //Determine if there is a grad plus loan on student's record, set the flag
                    var gradPlusAwardIds = awardsData.Where(
                        a => a.LoanType.HasValue &&
                            (a.LoanType.Value == LoanType.GraduatePlusLoan)
                        ).Select(a => a.Code);
                    var gradPlusStudentAwards = studentAwardsForYear.Where(sa => gradPlusAwardIds.Contains(sa.AwardId));

                    //if there are any grad plus studentAward objects and the calculated award status for all those grad plus
                    //student awards is not rejected, then the grad plus loan is present, and grad plus specific checklist items
                    //can be added
                    if (gradPlusStudentAwards.Count() > 0)
                    {
                        var gradPlusPeriods = gradPlusStudentAwards.SelectMany(gp => gp.StudentAwardPeriods);
                        var gradPlusStatus = AwardStatusEvaluator.CalculateAwardStatus(gradPlusPeriods, awardStatusesData);
                        if (gradPlusStatus != null && gradPlusStatus.Category != AwardStatusCategory.Rejected && gradPlusStatus.Category != AwardStatusCategory.Denied)
                        {
                            isGradPlusLoanPresent = true;
                        }
                    }

                    //grad plus entrance interview

                    var gradPlusInterviewDate = (studentLoanSummaryData != null) ?
                        studentLoanSummaryData.GraduatePlusLoanEntranceInterviewDate :
                        null;

                    var gradPlusInterviewChecklistItem = new ChecklistItem()
                    {
                        Type = ChecklistItemType.Interview,
                        Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanEntranceInterviewDescription"),
                        Link = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanEntranceInterviewLink"),
                        Target = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ExternalLinkTarget"),
                        Title = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanEntranceInterviewTitle"),
                        Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanEntranceInterviewSummary"),
                        ControlStatus = ChecklistItemControlStatus.CompletionRequired
                    };
                    //only calc the grad plus interview status if the student has a grad plus award
                    if (configurationDto.IsAwardingActive &&
                        isGradPlusLoanPresent)
                    {
                        if (isAnyItemRequiredAndIncomplete)
                        {
                            gradPlusInterviewChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                            gradPlusInterviewChecklistItem.IsActiveItem = false;
                        }
                        else
                        {
                            if (gradPlusInterviewDate.HasValue)
                            {
                                gradPlusInterviewChecklistItem.Status = ChecklistItemStatus.Complete;
                                gradPlusInterviewChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                gradPlusInterviewChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                gradPlusInterviewChecklistItem.IsActiveItem = true;
                            }
                        }

                        studentChecklistForYear.ChecklistItems.Add(gradPlusInterviewChecklistItem);
                    }
                    #endregion

                    #region DirectLoan MPN ChecklistItem


                    //Direct Loan MPN checklist item
                    //Get direct loan mpn expiration date
                    var directLoanMpnExpirationDate = (studentLoanSummaryData != null) ?
                        studentLoanSummaryData.DirectLoanMpnExpirationDate :
                        null;

                    //Get MPN info
                    var mpnInfo = linksData.FirstOrDefault(l => l.LinkType == LinkTypes.MPN);

                    var directLoanMpnChecklistItem = new ChecklistItem()
                    {
                        Type = ChecklistItemType.MPN,
                        Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanMpnDescription"),
                        Link = mpnInfo != null ? mpnInfo.LinkUrl : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanMpnLink"),
                        Target = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ExternalLinkTarget"),
                        Title = mpnInfo != null ? mpnInfo.Title : GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanMpnTitle"),
                        Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "DirectLoanMpnSummary"),
                        ControlStatus = ChecklistItemControlStatus.CompletionRequired
                    };

                    //only calc the checklist status if the student has a direct loan award
                    if (configurationDto.IsAwardingActive &&
                        isDirectLoanPresent)
                    {
                        //Calculate direct loan mpn status - conditions might change
                        if (isAnyItemRequiredAndIncomplete)
                        {
                            directLoanMpnChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                            directLoanMpnChecklistItem.IsActiveItem = false;
                        }
                        else
                        {
                            if ((directLoanMpnExpirationDate == null) || (directLoanMpnExpirationDate < DateTime.Today))
                            {
                                directLoanMpnChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                directLoanMpnChecklistItem.IsActiveItem = true;
                            }
                            else
                            {
                                directLoanMpnChecklistItem.Status = ChecklistItemStatus.Complete;
                                directLoanMpnChecklistItem.ExpirationDate = directLoanMpnExpirationDate.Value.ToShortDateString();
                                directLoanMpnChecklistItem.IsActiveItem = false;
                            }
                        }

                        studentChecklistForYear.ChecklistItems.Add(directLoanMpnChecklistItem);
                    }
                    #endregion

                    #region GradPlus MPN Checklist item


                    //Get plus loan mpn expiration date
                    var plusLoanMpnExpirationDate = (studentLoanSummaryData != null) ?
                        studentLoanSummaryData.PlusLoanMpnExpirationDate :
                        null;

                    var plusLoanMpnChecklistItem = new ChecklistItem()
                    {
                        Type = ChecklistItemType.MPN,
                        Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanMpnDescription"),
                        Link = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanMpnLink"),
                        Target = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ExternalLinkTarget"),
                        Title = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanMpnTitle"),
                        Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "PlusLoanMpnSummary"),
                        ControlStatus = ChecklistItemControlStatus.CompletionRequired
                    };

                    //only calc the checklist item status and add it to the checklist if there's a gradplus loan present in the student awards
                    if (configurationDto.IsAwardingActive &&
                        isGradPlusLoanPresent)
                    {
                        //Calculate plus loan mpn status - conditions might change
                        if (isAnyItemRequiredAndIncomplete)
                        {
                            plusLoanMpnChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                            plusLoanMpnChecklistItem.IsActiveItem = false;
                        }
                        else
                        {
                            if ((plusLoanMpnExpirationDate == null) || (plusLoanMpnExpirationDate < DateTime.Today))
                            {
                                plusLoanMpnChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                plusLoanMpnChecklistItem.IsActiveItem = true;
                            }
                            else
                            {
                                plusLoanMpnChecklistItem.Status = ChecklistItemStatus.Complete;
                                plusLoanMpnChecklistItem.ExpirationDate = plusLoanMpnExpirationDate.Value.ToShortDateString();
                                plusLoanMpnChecklistItem.IsActiveItem = false;
                            }
                        }

                        studentChecklistForYear.ChecklistItems.Add(plusLoanMpnChecklistItem);
                    }
                    #endregion

                    #region AwardLetter ChecklistItem

                    //Get studentAwardLetter for the year
                    var studentAwardLetterForYear = studentAwardLettersData.FirstOrDefault(al => al.AwardLetterYear == activeStudentAwardYearDto.Code);

                    var awardLetterTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.ReviewAwardLetter);
                    var studentAwardLetterItem = awardLetterTypeChecklistItem != null ? studentChecklistDataForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == awardLetterTypeChecklistItem.ChecklistItemCode) : null;

                    if (studentAwardLetterItem != null)
                    {
                        //Create faAwardLetter item
                        var faAwardLetterChecklistItem = new ChecklistItem()
                        {
                            Type = ChecklistItemType.AwardLetter,
                            Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardLetterDescription"),
                            Link = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardLetterLink"),
                            Title = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardLetterTitle"),
                            Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "FAAwardLetterSummary"),
                            ControlStatus = studentAwardLetterItem != null ? studentAwardLetterItem.ControlStatus : ChecklistItemControlStatus.CompletionRequired,
                            IsReadOnly = IsProxyView
                        };

                        if (configurationDto.IsAwardLetterActive &&
                            faAwardLetterChecklistItem.ControlStatus != ChecklistItemControlStatus.RemovedFromChecklist)
                        {
                            //Calculate faAwardLetterChecklistItem's status 
                            if (isAnyItemRequiredAndIncomplete)
                            {
                                faAwardLetterChecklistItem.Status = ChecklistItemStatus.NotAvailable;
                                faAwardLetterChecklistItem.IsActiveItem = false;
                            }
                            else
                            {
                                if ((studentAwardLetterForYear != null) && (studentAwardLetterForYear.AcceptedDate.HasValue))
                                {
                                    faAwardLetterChecklistItem.Status = ChecklistItemStatus.Complete;
                                    faAwardLetterChecklistItem.IsActiveItem = false;
                                }
                                else
                                {
                                    faAwardLetterChecklistItem.Status = ChecklistItemStatus.Incomplete;
                                    faAwardLetterChecklistItem.IsActiveItem = true;
                                }
                            }

                            studentChecklistForYear.ChecklistItems.Add(faAwardLetterChecklistItem);
                        }
                    }


                    #endregion

                    #region ActiveChecklistItem

                    //Find and assign the current active checklist item for the year - may have to change as multiple items may be active at once
                    var activeChecklistItem = studentChecklistForYear.ChecklistItems.FirstOrDefault(i => i.IsActiveItem);
                    if (activeChecklistItem != null)
                    {
                        studentChecklistForYear.ActiveChecklistItem = activeChecklistItem;
                    }
                    else
                    {
                        studentChecklistForYear.ActiveChecklistItem = new ChecklistItem()
                        {
                            Type = ChecklistItemType.Complete,
                            Description = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ChecklistCompleteDescription"),
                            Link = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ChecklistCompleteLink"),
                            Title = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ChecklistCompleteTitle"),
                            Summary = GlobalResources.GetString(FinancialAidResourceFiles.ChecklistResources, "ChecklistCompleteSummary"),
                            Status = ChecklistItemStatus.Complete,
                            IsActiveItem = true
                        };
                    }

                    #endregion


                    StudentChecklists.Add(studentChecklistForYear);
                }
            }

            AwardYears = awardYears;
            //.OrderByDescending(ay => ay.Code);

            #endregion

            #region PellLEUAmount

            PellLifetimeEligibilityUsedPercentage = studentNsldsData != null ? studentNsldsData.PellLifetimeEligibilityUsedPercentage : null;

            #endregion

            #region StudentAccountSummary

            if (studentAccountDueData != null)
            {
                StudentAccountSummary = new StudentFinanceAccountSummary(studentAccountDueData);
            }

            #endregion
        }
    }
}
