﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// SAPAppeal class - part of AcademicProgressEvaluation object
    /// </summary>
    public class SAPAppeal
    {
        /// <summary>
        /// Appeal status - may change its type to be a custom object
        /// depending on the DTO property
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Latest Appeal date
        /// </summary>
        public DateTimeOffset AppealDate { get; set; }
        /// <summary>
        /// Advisor for the given appeal
        /// </summary>
        public Counselor AppealAdvisor { get; set; }

        /// <summary>
        /// SAPAppeal constructor
        /// <param name="academicProgressAppealCodesData">List of appeal codes</param>
        /// <param name="academicProgressEvaluationData">List of academic progress evaluations</param>
        /// <param name="appealsCounselorsData">List of counselors</param>
        /// <param name="counselorsPhoneNumbersData">List of counselors' phone numbers</param>
        /// <param name="financialAidOfficeData">List of financial aid offices</param>
        /// <param name="mostRecentAwardYearData">most recent award year on student's record</param>
        /// </summary>
        public SAPAppeal(Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicProgressEvaluationData, 
                         IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode> academicProgressAppealCodesData,
                         IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidCounselor> appealsCounselorsData,
                         IEnumerable<Colleague.Dtos.Base.PhoneNumber> counselorsPhoneNumbersData,
                         Colleague.Dtos.FinancialAid.StudentAwardYear2 mostRecentAwardYearData,
                         Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOfficeData)
        {
            if (academicProgressEvaluationData == null)
            {
                throw new ArgumentNullException("academicProgressEvaluationData");
            }

            if (academicProgressAppealCodesData == null || !academicProgressAppealCodesData.Any())
            {
                throw new ArgumentNullException("academicProgressAppealCodesData cannot be empty");
            }

            var mostRecentAppeal = academicProgressEvaluationData.ResultAppeals != null && academicProgressEvaluationData.ResultAppeals.Any() ? 
                    academicProgressEvaluationData.ResultAppeals.First() : null;
            if (mostRecentAppeal != null)
            {
                var appealStatus = academicProgressAppealCodesData.FirstOrDefault(pac => pac.Code == mostRecentAppeal.AppealStatusCode);                    
                
                Status = appealStatus != null ? appealStatus.Description : null;
                AppealDate = mostRecentAppeal.AppealDate;

                //Get the appeal counselor and their phone number
                var appealCounselor = appealsCounselorsData != null && appealsCounselorsData.Any() ? 
                    appealsCounselorsData.FirstOrDefault(ac => ac.Id == mostRecentAppeal.AppealCounselorId) : null;

                var appealCounselorPhoneNumber = counselorsPhoneNumbersData != null && counselorsPhoneNumbersData.Any() ? 
                    counselorsPhoneNumbersData.FirstOrDefault(p => p.PersonId == mostRecentAppeal.AppealCounselorId) : null;
                try
                {
                    AppealAdvisor = new Counselor(appealCounselor, appealCounselorPhoneNumber, mostRecentAwardYearData, 
                        new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>() {financialAidOfficeData}, true);
                }
                catch (Exception) { }
            }
        }
    }
}