﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// SAP Detail items types
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SAPDetailItemType
    {
        /// <summary>
        /// Value type - default
        /// </summary>
        Value,
        /// <summary>
        /// Rate type
        /// </summary>
        Rate,        
        /// <summary>
        /// GPA type
        /// </summary>
        GPA
    }
}