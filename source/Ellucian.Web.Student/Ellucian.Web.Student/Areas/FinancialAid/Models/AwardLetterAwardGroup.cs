﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Award letter award group class - contains information about award category groups
    /// such as name, sequence number, and a list of awards that belong to the group
    /// </summary>
    public class AwardLetterAwardGroup
    {
        /// <summary>
        /// Group name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Group number
        /// </summary>
        public int SequenceNumber { get; set; }

        /// <summary>
        /// List of award letter awards that belong to the group
        /// </summary>
        public List<AwardLetterStudentAward> AwardLetterStudentAwards { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AwardLetterAwardGroup()
        {
            AwardLetterStudentAwards = new List<AwardLetterStudentAward>();
        }

        /// <summary>
        /// Constructor that accepts an awardLetterDTO
        /// </summary>
        /// <param name="awardLetterDto">AwardLetter DTO</param>
        public AwardLetterAwardGroup(AwardLetter2 awardLetterDto)
        {
            if (awardLetterDto == null)
            {
                throw new ArgumentNullException("awardLetterDto");
            }
            AwardLetterStudentAwards = new List<AwardLetterStudentAward>();

            //Get all of the award periods for the year
            var awardPeriodsForYear = awardLetterDto.AwardLetterAnnualAwards.Any() ? awardLetterDto.AwardLetterAnnualAwards.SelectMany(a => a.AwardLetterAwardPeriods).ToList() : new List<AwardLetterAwardPeriod>();

            //Get all of the distinct award period column numbers
            var distinctAwardPeriodColumnsForYear = awardPeriodsForYear.Any() ? awardPeriodsForYear.Select(ap => ap.ColumnNumber).Distinct().OrderBy(cn => cn).ToList() : new List<int>();

            var totalRow = new AwardLetterStudentAward() { Description = GlobalResources.GetString(FinancialAidResourceFiles.AwardLetterResources, "TotalRowHeader") };
            //Add "award periods" to the total row - the sums of all of the awards for each award period
            foreach (var columnNumber in distinctAwardPeriodColumnsForYear)
            {
                var columnGroupPeriods = awardPeriodsForYear.Where(ap => ap.ColumnNumber == columnNumber).ToList();
                var totalAmountForPeriod = columnGroupPeriods.Any() ? columnGroupPeriods.Sum(p => p.AwardPeriodAmount) : null;
                totalRow.StudentAwardPeriods.Add(new StudentAwardPeriod() { AwardAmount = totalAmountForPeriod });
            }

            totalRow.TotalAwardAmount = awardPeriodsForYear.Any() ? awardPeriodsForYear.Sum(ap => ap.AwardPeriodAmount)
                : awardLetterDto.AwardLetterAnnualAwards.Sum(a => a.AnnualAwardAmount);

            AwardLetterStudentAwards.Add(totalRow);
        }

        /// <summary>
        /// Award letter award group constructor
        /// </summary>
        /// <param name="name">group name</param>
        /// <param name="sequenceNumber">group number</param>
        /// <param name="groupAwards">awards that belong to the group</param>    
        /// <param name="awardPeriodsForYear">all award periods for the current award letter</param>
        public AwardLetterAwardGroup(string name, int sequenceNumber, 
            IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.AwardLetterAnnualAward> groupAwards,
            IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.AwardLetterAwardPeriod> awardPeriodsForYear)
        {
            if (groupAwards == null || !groupAwards.Any())
            {
                throw new ArgumentNullException("groupAwards");
            }

            Name = name;
            SequenceNumber = sequenceNumber;
            AwardLetterStudentAwards = new List<AwardLetterStudentAward>();

            var distinctAwardPeriods = awardPeriodsForYear != null ? awardPeriodsForYear.Select(ap => ap.ColumnNumber).Distinct().OrderBy(cn => cn).ToList() : new List<int>();
            
            foreach (var award in groupAwards)
            {
                var awardLetterAward = new AwardLetterStudentAward()
                {
                    Code = award.AwardId,
                    Description = award.AwardDescription
                };

                foreach (var period in distinctAwardPeriods)
                {
                    var awardPeriod = award.AwardLetterAwardPeriods.FirstOrDefault(ap => ap.ColumnNumber == period);
                    if (awardPeriod == null)
                    {
                        //Add a dummy award period if the current award does not have specified award period
                        awardLetterAward.StudentAwardPeriods.Add(new StudentAwardPeriod() { AwardAmount = null });
                    }
                    else
                    {
                        awardLetterAward.StudentAwardPeriods.Add(new StudentAwardPeriod() { AwardAmount = awardPeriod.AwardPeriodAmount});
                    }
                }
                    
                //If any award periods - calculate the sum, otherwise use AnnualAwardAmount                    
                awardLetterAward.TotalAwardAmount = award.AwardLetterAwardPeriods.Any() ? award.AwardLetterAwardPeriods.Sum(ap => ap.AwardPeriodAmount) 
                    : award.AnnualAwardAmount;

                AwardLetterStudentAwards.Add(awardLetterAward);
            }
            
        }        
    }
}