﻿//Copyright 2014 Ellucian Company L.P. and its affiliates

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Enumeration of checklist items types for the FA HUB Home page
    /// </summary>

    public enum ChecklistItemType
    {
        Profile,
        FAFSA,
        Documents,
        Application,
        Awards,
        MPN,
        Interview,
        AwardLetter,
        Complete
    }
}
