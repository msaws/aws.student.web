﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// OutsideAwardsViewModel class
    /// </summary>
    public class OutsideAwardsViewModel : FinancialAidAdminModel
    {
        /// <summary>
        /// Existing outside awards for selected year
        /// </summary>
        public List<OutsideAward> OutsideAwards { get; set; }

        /// <summary>
        /// Predetermined award types:
        /// Scholarship, grant, or loan
        /// </summary>
        public List<string> OutsideAwardTypes;
        
        /// <summary>
        /// Student object
        /// </summary>
        public FAStudent Student { get; set; }

        /// <summary>
        /// List of all active student award years
        /// </summary>
        public IEnumerable<AwardYear> AwardYears { get; set; }

        /// <summary>
        /// Indicates whether the current user is self or not
        /// </summary>
        public bool IsUserSelf { get; set; }
        
        /// <summary>
        /// OutsideAwardsViewModel constructor
        /// </summary>
        /// <param name="personData">person data</param>
        /// <param name="studentAwardYearsData">active student award years for the person</param>
        /// <param name="financialAidOfficeData">list of financial aid offices</param>
        /// <param name="financialAidCounselorData">financial aid counselor data for the person</param>
        public OutsideAwardsViewModel(Person personData, 
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeData,
            Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData,
            bool isUserSelf)
        {
            OutsideAwards = new List<OutsideAward>();
            OutsideAwardTypes = new List<string>();

            IsUserSelf = isUserSelf;

            if (personData == null) { personData = new Ellucian.Colleague.Dtos.Student.Student(); }
            Student = new FAStudent()
            {
                Id = personData.Id,
                PreferredName = personData.PreferredName
            };

            if (financialAidOfficeData == null) { financialAidOfficeData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>(); }
            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            var configurationDtos = financialAidOfficeData.SelectMany(o => o.Configurations).ToList();

            AwardYears = studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos)
                .Select(activeStudentAwardYearDto =>
                {
                    var awardYear = new AwardYear(activeStudentAwardYearDto);
                    try
                    {
                        awardYear.Counselor = new Counselor(financialAidCounselorData, null, activeStudentAwardYearDto, financialAidOfficeData);
                    }
                    catch (Exception) { }
                    return awardYear;
                })
                .OrderByDescending(awardYear => awardYear.Code);
        }

        /// <summary>
        /// Updates OutsideAwardsViewModel with year specific outside awards data
        /// </summary>
        /// <param name="outsideAwardsData">existing outside awards for the year</param>
        internal void UpdateOutsideAwardsViewModel(IEnumerable<Colleague.Dtos.FinancialAid.OutsideAward> outsideAwardsData)
        {
            //Remove existing OutsideAwards if the list is not empty
            if (OutsideAwards.Any())
            {
                OutsideAwards.Clear();
            }
            //If any dtos received, assign new OutsideAward objects to the OutsideAwards
            if (outsideAwardsData != null)
            {
                OutsideAwards = outsideAwardsData.ToList();
            }
            //If there are no OutsideAwardTypes, add predetermined values
            if (OutsideAwardTypes == null || !OutsideAwardTypes.Any())
            {
                OutsideAwardTypes = new List<string>() { "Scholarship", "Grant", "Loan" };
            }
        }
    }
}