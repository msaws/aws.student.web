﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class Counselor
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        public string EmailAddress { get; set; }
        /// <summary>
        /// Flag to indicate whether the properties were set based on the information received from counselor dto
        /// or financial aid office dto
        /// </summary>
        public bool IsCounselorInfo { get; set; }

        public Counselor() { }

        public Counselor(
            Colleague.Dtos.FinancialAid.FinancialAidCounselor counselorDto,
            Colleague.Dtos.Base.PhoneNumber counselorPhoneNumberDto,
            Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeDtoList)
        {
            SetCounselorPropertiesFromDtos(counselorDto, counselorPhoneNumberDto, studentAwardYearDto, financialAidOfficeDtoList, false);
        }

        /// <summary>
        /// Constructor for a SAP AppealCounselor
        /// </summary>
        /// <param name="counselorDto">counselor data</param>
        /// <param name="counselorPhoneNumberDto">counselor phone numbers</param>
        /// <param name="studentAwardYearDto">studentAwardYear data</param>
        /// <param name="financialAidOfficeDtoList">List of FA offices</param>
        /// <param name="isAppealCounselor">Flag to indicate whether counselor object is being created for an appeal counselor</param>
       public Counselor(
           Colleague.Dtos.FinancialAid.FinancialAidCounselor counselorDto,
           Colleague.Dtos.Base.PhoneNumber counselorPhoneNumberDto,
           Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto,
           IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeDtoList,
           bool isAppealCounselor)
        {
            SetCounselorPropertiesFromDtos(counselorDto, counselorPhoneNumberDto, studentAwardYearDto, financialAidOfficeDtoList, isAppealCounselor);
        }

        /// <summary>
        /// Create a Counselor object based on the contact information from both a FinancialAidCounselor DTO and a FinancialAidOffice DTO.
        /// Both DTOs contain contact information that the student can use.
        /// 
        /// Pass in the student's most recent StudentAwardYear DTO in order to get the FinancialAidOfficeId of the FinancialAidOffice DTO to use
        /// as part of the counselor contact. If the student has no FinancialAidOfficeId or if the most recent StudentAwardYear Dto is null,
        /// then the first FinancialAidOffice in the list is used.
        /// </summary>
        /// <param name="counselorDto">A FinancialAidCounselor Dto to get contact information from.</param>
        /// <param name="studentAwardYearDtoList">A list of the student's StudentAwardYear Dtos.</param>
        /// <param name="financialAidOfficeDtoList">A list of FinancialAidOffice DTOs to get contact information from.</param>
        /// <exception cref="ArgumentNullException">Thrown if both the counselorDto and financialAidOfficeDtoList are null or empty</exception>
        public Counselor(Colleague.Dtos.FinancialAid.FinancialAidCounselor counselorDto,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearDtoList,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeDtoList,
            Colleague.Dtos.Base.PhoneNumber counselorPhoneNumbers)
        {
            var mostRecentStudentAwardYearDto = (studentAwardYearDtoList != null && studentAwardYearDtoList.Count() > 0) ?
                    studentAwardYearDtoList.OrderByDescending(y => y.Code).First() :
                    null;

            SetCounselorPropertiesFromDtos(counselorDto, counselorPhoneNumbers, mostRecentStudentAwardYearDto, financialAidOfficeDtoList, false);
        }

        /// <summary>
        /// Sets counselor properties either with the default contact info for the fa office or
        /// assigned fa counselor
        /// </summary>
        /// <param name="counselorDto">counselor dto containing assigned counselor info</param>
        /// <param name="counselorPhoneNumberDto">set of counselor phone numbers</param>
        /// <param name="studentAwardYearDto">student award year info</param>
        /// <param name="financialAidOfficeDtoList">set of financial aid office objects</param>
        /// <param name="isAppealCounselor">flag to indicate whether the counselor to be created is for sap appeals</param>
        public void SetCounselorPropertiesFromDtos(
            Colleague.Dtos.FinancialAid.FinancialAidCounselor counselorDto,
            Colleague.Dtos.Base.PhoneNumber counselorPhoneNumberDto,
            Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeDtoList,
            bool isAppealCounselor)
        {
            if (financialAidOfficeDtoList == null)
            {
                throw new ArgumentNullException("financialAidOfficeDtoList is required in order to get a phone number");
            }
            if (studentAwardYearDto == null)
            {
                throw new ArgumentNullException("studentAwardYearDto");
            }

            //its okay if counselorDto is null.

            var officeDto = financialAidOfficeDtoList.FirstOrDefault(o => o.Id == studentAwardYearDto.FinancialAidOfficeId);
            if (officeDto == null)
            {
                throw new ApplicationException(string.Format("Office {0} does not exist. Cannot get phone number or type", studentAwardYearDto.FinancialAidOfficeId));
            }

            var configurationDto = (officeDto.Configurations == null) ? null :
                officeDto.Configurations.GetConfigurationDtoOrDefault(studentAwardYearDto);

            bool useDefaultCounselorInfo = configurationDto != null ? configurationDto.UseDefaultContact : false;

            //Counselor info is present, flag to use default info is false => use counselor info
            if (counselorDto != null && !useDefaultCounselorInfo)
            {
                Name = counselorDto.Name;
                EmailAddress = !string.IsNullOrEmpty(counselorDto.EmailAddress) ? counselorDto.EmailAddress 
                    : officeDto.EmailAddress;

                var counselorPhoneByType = (configurationDto == null || counselorPhoneNumberDto == null || counselorPhoneNumberDto.PhoneNumbers == null) ? null :
                    counselorPhoneNumberDto.PhoneNumbers.FirstOrDefault(pn => pn.TypeCode == configurationDto.CounselorPhoneType);

                PhoneNumber = (counselorPhoneByType != null) ? counselorPhoneByType.Number : officeDto.PhoneNumber;
                Extension = (counselorPhoneByType != null) ? counselorPhoneByType.Extension : string.Empty;
                IsCounselorInfo = true;
            }
            //counselor info is absent and we are trying to create appeals counselor => throw exception
            else if (counselorDto == null && isAppealCounselor)
            {
                throw new ArgumentException("CounselorDto is null when creating sap appeal counselor");
            }
            //Use default info
            else
            {
                Name = officeDto.DirectorName;
                EmailAddress = officeDto.EmailAddress;
                PhoneNumber = officeDto.PhoneNumber;
            }

            ////mcd - conversation with Michelle that if there's no Name or (no Phone Number and no Email), throw an exception
            if (string.IsNullOrEmpty(this.Name) || (string.IsNullOrEmpty(this.PhoneNumber) && string.IsNullOrEmpty(this.EmailAddress)))
            {
                throw new ApplicationException("Counselor must have at least a name and some form of contact, phone number or email");
            }

        }


        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var counselor = obj as Counselor;

            if (counselor.Name == this.Name &&
                counselor.PhoneNumber == this.PhoneNumber &&
                counselor.EmailAddress == this.EmailAddress)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ PhoneNumber.GetHashCode() ^ EmailAddress.GetHashCode();
        }
    }
}