﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// New Student Loan Request class
    /// </summary>
    public class StudentLoanRequest
    {
        /// <summary>
        /// Year of the pending request
        /// </summary>
        public string AwardYearCode { get; set; }
        /// <summary>
        /// The date the request was submitted
        /// </summary>
        public string RequestDate { get; set; }
        /// <summary>
        /// Requested amount
        /// </summary>
        public int TotalRequestAmount { get; set; }
        /// <summary>
        /// FA Counselor request was assigned to
        /// </summary>
        public Counselor AssignedToCounselor { get; set; }
        /// <summary>
        /// boolean to indicate whether there is a pending loan request for the year
        /// </summary>
        public bool IsRequestPending { get; set; }
        /// <summary>
        /// Student's id
        /// </summary>
        public string StudentId { get; set; }
        /// <summary>
        /// List of Award periods with corresponding amounts for the loan request
        /// </summary>
        public List<StudentLoanPeriod> LoanRequestAwardPeriods { get; set; }

        /// <summary>
        /// Estimated cost of attendance for the year
        /// </summary>
        public int EstimatedCostOfAttendance { get; set; }

        /// <summary>
        /// Total awarded amount for the year, includes all awards except those that are denied or rejected
        /// </summary>
        public int TotalAwardedAmount { get; set; }

        /// <summary>
        /// Unmet cost for the year
        /// </summary>
        public int UnmetCost
        {
            get { return EstimatedCostOfAttendance - TotalAwardedAmount; }
        }

        /// <summary>
        /// Maximum request amount is the UnmetCost, or, if UnmetCost is 0, $99,999.
        /// </summary>
        public int MaximumRequestAmount
        {
            get { return UnmetCost > 0 ? UnmetCost : maxAmountIfNoUnmetCost; }
        }
        private const int maxAmountIfNoUnmetCost = 99999;

        /// <summary>
        /// Comments for students to be able to add to the loan request
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Constructor that accepts a pending loan request dto and
        /// creates a Student loan request object with received data
        /// </summary>
        /// <param name="loanRequest">pending loan request on student's record for the year</param>
        public StudentLoanRequest(Colleague.Dtos.FinancialAid.LoanRequest loanRequest)
        {
            AwardYearCode = loanRequest.AwardYear;
            RequestDate = loanRequest.RequestDate.ToShortDateString();
            TotalRequestAmount = loanRequest.TotalRequestAmount;
            IsRequestPending = true;
            StudentId = loanRequest.StudentId;
            LoanRequestAwardPeriods = loanRequest.LoanRequestPeriods
                .Select(requestPeriodDto => new StudentLoanPeriod() { Code = requestPeriodDto.Code, AwardAmount = requestPeriodDto.LoanAmount }).ToList();
        }

        /// <summary>
        /// Constructor that accepts an award year and student id
        /// as arguments and creates a student loan request object that will
        /// potentially be used for a new loan request by the student. Used when 
        /// there is no pending loan request for the award year
        /// </summary>
        ///<param name="studentAwardYearDto">StudentAwardYear2 DTO</param>
        ///<param name="studentId">student ID</param>
        ///<param name="defaultAwardPeriodsList">list of StudentDefaultAwardPeriod DTOs</param>
        ///<param name="awardPeriodDtoList">list of AwardPeriod DTOs</param>
        public StudentLoanRequest(Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto, string studentId, 
            IEnumerable<Colleague.Dtos.FinancialAid.StudentDefaultAwardPeriod> defaultAwardPeriodsList, 
            IEnumerable<Colleague.Dtos.FinancialAid.AwardPeriod> awardPeriodDtoList)
        {
            AwardYearCode = studentAwardYearDto.Code;
            IsRequestPending = false;
            StudentId = studentId;

            //Get award periods based on received default periods for the year
            var awardPeriods = defaultAwardPeriodsList
                .Where(dapl => dapl.AwardYear == studentAwardYearDto.Code)
                .SelectMany(dapl =>
                    dapl.DefaultAwardPeriods
                    .Select(defaulAwardPeriod =>
                        awardPeriodDtoList.FirstOrDefault(awardPeriod => awardPeriod.Code == defaulAwardPeriod)
                    )
                )
                .Where(awardPeriodDto => awardPeriodDto != null)
                .Distinct(new FunctionEqualityComparer<AwardPeriod>((x, y) => x.Code == y.Code, (awardPeriod) => awardPeriod.Code.GetHashCode()))
                .OrderBy(awardPeriodDto => awardPeriodDto.StartDate).ToList();


            LoanRequestAwardPeriods = awardPeriods
                .Select(awardPeriodDto => new StudentLoanPeriod(awardPeriodDto)).ToList();

            EstimatedCostOfAttendance = studentAwardYearDto.EstimatedCostOfAttendance.HasValue ? (int)studentAwardYearDto.EstimatedCostOfAttendance.Value : 0;
            TotalAwardedAmount = (int)Math.Round(studentAwardYearDto.TotalAwardedAmount);
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public StudentLoanRequest()
        {
            LoanRequestAwardPeriods = new List<StudentLoanPeriod>();
        }

        /// <summary>
        /// Compares two StudentLoanRequest objects
        /// </summary>
        /// <param name="obj">StudentLoanRequest object to compare to</param>
        /// <returns>true/false</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var studentLoanRequest = obj as StudentLoanRequest;

            return studentLoanRequest.AwardYearCode == this.AwardYearCode &&
                studentLoanRequest.StudentId == this.StudentId &&
                studentLoanRequest.RequestDate == this.RequestDate;
        }

        /// <summary>
        /// Gets the hashcode of a StudentLoanRequest object
        /// </summary>
        /// <returns>object's hashcode</returns>
        public override int GetHashCode()
        {
            return AwardYearCode.GetHashCode() ^ StudentId.GetHashCode() ^ RequestDate.GetHashCode();
        }
        
    }


}
