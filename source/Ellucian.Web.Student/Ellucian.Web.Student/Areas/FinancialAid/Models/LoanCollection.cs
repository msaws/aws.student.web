﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// A loan collection class 
    /// </summary>
    public class LoanCollection
    {
        private const string UnknownStatusDescription = "Unknown";

        public string AwardYearCode { get; set; }

        public string StudentId { get; set; }

        //List of loans: sub/ unsub separate
        public List<StudentAward> Loans { get; set; }

        /// <summary>
        /// Total amount for this loan collection
        /// </summary>
        public decimal LoansTotalAmount 
        {
            get
            {
                return Loans.Count > 0 ?
                    Loans.SelectMany(sa => sa.StudentAwardPeriods.Where(sap => sap.AwardStatus.Category != Colleague.Dtos.FinancialAid.AwardStatusCategory.Denied
                        && sap.AwardStatus.Category != Colleague.Dtos.FinancialAid.AwardStatusCategory.Rejected)).Sum(sap => (sap.OriginalAwardAmount.HasValue ? sap.OriginalAwardAmount.Value : 0)) : 0;
            }
        }

        /// <summary>
        /// boolean flag to indicate if the whole loan category 
        /// can be accepted/rejected
        /// </summary>
        public bool IsStatusModifiable { get; set; }
        /// <summary>
        /// Flag that indicates whether any of the loans in the category has a pending change
        /// request and is under review by FA staff
        /// </summary>
        public bool IsLoanCollectionInReview { get; set; }

        /// <summary>
        /// flag indicates if any student award period amount can be modified
        /// </summary>
        public bool IsAnyAmountModifiable
        {
            get
            {
                if (Loans != null)
                {
                    return Loans.Where(l => l.StudentAwardPeriods != null).SelectMany(l => l.StudentAwardPeriods).Where(p => p != null).Any(p => p.IsAmountModifiable);
                }
                return false;
            }
        }

        /// <summary>
        /// The status of the collection
        /// </summary>
        public string StatusDescription
        {
            get
            {

                if (Loans != null && Loans.Count() > 0)
                {
                    string status = null;                    
                    var allStudentAwardPeriods = Loans.Where(l => l.StudentAwardPeriods != null).SelectMany(l => l.StudentAwardPeriods).Where(p => p != null);

                    //if any of the loans in the category are under review, set the overall status to "In Review", this precedes "Complete" since changes to loans
                    //might be allowed even if all loans have been accepted/rejected
                    if (Loans.Any(l => l.IsAwardInReview))
                    {
                        status = GlobalResources.GetString(FinancialAidResourceFiles.MyAwardsResources, "InReviewStatusDescription");                        
                    }
                    //If we have all award period statuses either accepted or rejected (mixed) for DIFFERENT loans in the same loan category,
                    //we want to show that loan collection's status description as "Completed" 
                    else if (allStudentAwardPeriods != null && 
                        allStudentAwardPeriods.Any(sap => sap.AwardStatus.Category == AwardStatusCategory.Rejected ||sap.AwardStatus.Category == AwardStatusCategory.Denied) && 
                        allStudentAwardPeriods.Any(sap => sap.AwardStatus.Category == AwardStatusCategory.Accepted) && 
                        allStudentAwardPeriods.All(sap => sap.AwardStatus.Category == AwardStatusCategory.Accepted || 
                            sap.AwardStatus.Category == AwardStatusCategory.Denied || sap.AwardStatus.Category == AwardStatusCategory.Rejected) &&
                        allStudentAwardPeriods.Any(sap => sap.AwardId != allStudentAwardPeriods.First().AwardId))
                    {
                        status = "Completed";
                    }                        
                    else
                    {
                        var awardStatus = AwardStatusEvaluator.CalculateAwardStatus(allStudentAwardPeriods);
                        status =  awardStatus != null ? awardStatus.Category.ToString() : UnknownStatusDescription;
                    }

                    return status;
                }
                return UnknownStatusDescription;
            }
        }

        public int MaximumAmount { get; set; }

        public List<LoanPeriod> LoanPeriods { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public LoanType? LoanCollectionType
        {
            get
            {
                if (Loans == null || Loans.Count() < 1)
                {
                    return new Nullable<LoanType>();
                }

                return Loans.First().LoanType;
            }
        }

        /// <summary>
        /// Flag indicating whether the loan collection has the collective status of "Accepted"
        /// </summary>
        public bool IsAccepted
        {
            get
            {
                return (AwardStatusEvaluator.CalculateAwardStatus(Loans.SelectMany(l => l.StudentAwardPeriods))).Category == AwardStatusCategory.Accepted;
            }
        }

        public LoanCollection()
        {
            Loans = new List<StudentAward>();
        }
    }


}