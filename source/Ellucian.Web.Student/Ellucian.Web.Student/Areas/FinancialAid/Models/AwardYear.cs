﻿/*Copyright 2014-2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// An award year class that has extended attributes: distinct award
    /// periods list
    /// </summary>
    public class AwardYear
    {
        public string Code { get; set; }
        public string Description { get; set; }

        //List of distinct award periods for the year
        //public List<AwardPeriod> DistinctAwardPeriods { get; set; }

        private readonly List<AwardPeriod> _DistinctAwardPeriods;
        /// <summary>
        /// This is a list of unique award periods for the award year. This list is sorted by the award period's start date.
        /// </summary>
        public ReadOnlyCollection<AwardPeriod> DistinctAwardPeriods { get; private set; }

        /// <summary>
        /// Flag that indicates whether previous checklist items/requirements were
        /// necessary to be completed and completed if so
        /// </summary>
        public bool ArePrerequisitesSatisfied { get; set; }

        /// <summary>
        /// Flag that indicates whether there are checklist items for the current year for the student;
        /// currently is used just on Home, My Awards and Award Letter pages in counselor mode view - please make
        /// changes to this comment if that changes
        /// </summary>
        public bool AreChecklistItemsAssigned { get; set; }

        /// <summary>
        /// The FinancialAidCounselor for the year.
        /// </summary>
        public Counselor Counselor { get; set; }

        /// <summary>
        /// Method attempts to add an award period to the list of distinct award periods. If the award period
        /// does not exist in the list, it is added. The DistinctAwardPeriods list is resorted after each successful Add.
        /// If the award period already exists in the list, it is not added.
        /// </summary>
        /// <param name="awardPeriod">The award period to attempt to add</param>
        /// <returns>True if the award period was added. False, if not.</returns>
        public bool AddDistinctAwardPeriod(AwardPeriod awardPeriod)
        {
            if (awardPeriod == null) return false;

            if (!_DistinctAwardPeriods.Any(p => p.Code == awardPeriod.Code))
            {
                _DistinctAwardPeriods.Add(awardPeriod);
                _DistinctAwardPeriods.Sort((dap1, dap2) => DateTime.Compare(dap1.StartDate, dap2.StartDate));

                return true;
            }
            return false;
        }

        public AwardYear()
        {
            _DistinctAwardPeriods = new List<AwardPeriod>();
            DistinctAwardPeriods = _DistinctAwardPeriods.AsReadOnly();
        }

        public AwardYear(Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto)
        {
            _DistinctAwardPeriods = new List<AwardPeriod>();
            InitializeWithStudentAwardYearDto(studentAwardYearDto);
        }

        public AwardYear(Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto,
            Func<bool> prerequisiteCalc, bool areChecklistItemsAssigned)
        {
            _DistinctAwardPeriods = new List<AwardPeriod>();
            InitializeWithStudentAwardYearDto(studentAwardYearDto);

            ArePrerequisitesSatisfied = prerequisiteCalc.Invoke();
            AreChecklistItemsAssigned = areChecklistItemsAssigned;
        }

        private void InitializeWithStudentAwardYearDto(Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto)
        {
            if (studentAwardYearDto == null)
            {
                throw new ArgumentNullException("studentAwardYearDto");
            }

            Code = studentAwardYearDto.Code;
            Description = !string.IsNullOrEmpty(studentAwardYearDto.Description) ?
                studentAwardYearDto.Description :
                studentAwardYearDto.Code;

            DistinctAwardPeriods = _DistinctAwardPeriods.AsReadOnly();
        }
    }
}