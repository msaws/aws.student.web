﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class FAStudent
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public IEnumerable<string> PreferredAddress { get; set; }
        public string PreferredEmailAddress { get; set; }
        public string PreferredName { get; set; }
        public string FullName { get; set; }

        public FAStudent()
        {
            
        }

        /// <summary>
        /// Constructor with applicant object argument
        /// </summary>
        /// <param name="applicant"></param>
        public FAStudent(Colleague.Dtos.Student.Applicant applicant)
        {
            Id = applicant.Id;
            FirstName = applicant.FirstName;
            LastName = applicant.LastName;
            MiddleName = applicant.MiddleName;
            PreferredAddress = applicant.PreferredAddress;
            PreferredEmailAddress = applicant.PreferredEmailAddress;
            PreferredName = applicant.PreferredName;
        }

        /// <summary>
        /// Constructor with a student object argument
        /// </summary>
        /// <param name="student"></param>
        public FAStudent(Colleague.Dtos.Student.Student student)
        {
            Id = student.Id;
            FirstName = student.FirstName;
            LastName = student.LastName;
            MiddleName = student.MiddleName;
            PreferredAddress = student.PreferredAddress;
            PreferredEmailAddress = student.PreferredEmailAddress;
            PreferredName = student.PreferredName;
        }

        /// <summary>
        /// Constructor with a person object argument
        /// </summary>
        /// <param name="person">person dto</param>
        public FAStudent(Colleague.Dtos.Base.Person person)
        {
            Id = person.Id;
            FirstName = person.FirstName;
            LastName = person.LastName;
            MiddleName = person.MiddleName;
            PreferredAddress = person.PreferredAddress;
            PreferredName = person.PreferredName;
        }

    }
}
