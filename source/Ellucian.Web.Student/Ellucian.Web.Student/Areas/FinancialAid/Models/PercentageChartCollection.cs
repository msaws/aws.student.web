﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// PercentageChartCollection
    /// </summary>
    public class PercentageChartCollection
    {
        /// <summary>
        /// Award year code
        /// </summary>
        public string AwardYearCode { get; set; }
        /// <summary>
        /// Collection of PercentageChartModels by Award type
        /// We want to make sure that the numbers in all members of the collection
        /// add up
        /// </summary>
        public ReadOnlyCollection<PercentageByAwardTypeChartModel> PercentageChartModels { get; private set; }
        private readonly List<PercentageByAwardTypeChartModel> percentageChartModels;

        /// <summary>
        /// PercentageChartCollection Constructor
        /// </summary>
        /// <param name="awardYear">award year code</param>
        public PercentageChartCollection(string awardYear)
        {
            AwardYearCode = awardYear;
            percentageChartModels = new List<PercentageByAwardTypeChartModel>();
            PercentageChartModels = percentageChartModels.AsReadOnly();
        }

        /// <summary>
        /// Creates percentage charts for all applicable award categories:
        /// awards, work awards, loans
        /// </summary>
        /// <param name="awardsTotal"></param>
        /// <param name="workAwardsTotal"></param>
        /// <param name="loansTotal"></param>
        public void CreatePercentageCharts(decimal awardsTotal, decimal workAwardsTotal, decimal loansTotal)
        {
            var totalAwarded = awardsTotal + workAwardsTotal + loansTotal;

            try
            {
                percentageChartModels.Add(new PercentageByAwardTypeChartModel(totalAwarded, awardsTotal, AwardCategoryType.Award));
            }
            catch (Exception ex) { throw new ApplicationException("Could not create awards percantage chart", ex.InnerException); }

            try
            {
                percentageChartModels.Add(new PercentageByAwardTypeChartModel(totalAwarded, workAwardsTotal, AwardCategoryType.Work));
            }
            catch (Exception ex) { throw new ApplicationException("Could not create work awards percantage chart", ex.InnerException); }


            try
            {
                percentageChartModels.Add(new PercentageByAwardTypeChartModel(totalAwarded, loansTotal, AwardCategoryType.Loan));
            }
            catch (Exception ex) { throw new ApplicationException("Could not create loans percantage chart", ex.InnerException); }


        }
    }
}