﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using Ellucian.Colleague.Dtos.FinancialAid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Comparer class compares academic progress property type enumerations to determine
    /// the order in which to display Academic Progress Properties
    /// </summary>
    public class AcademicProgressPropertyTypeDisplayComparer : IComparer<Colleague.Dtos.FinancialAid.AcademicProgressPropertyType>
    {

        /// <summary>
        /// Compares two AcademicProgressPropertyType enums and returns an indication of their
        /// relative values in the display order
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(AcademicProgressPropertyType x, AcademicProgressPropertyType y)
        {
            return GetSortValue(x).CompareTo(GetSortValue(y));
        }

        private int GetSortValue(AcademicProgressPropertyType type)
        {
            switch(type)
            {
                case AcademicProgressPropertyType.MaximumProgramCredits:
                    return 1;
                case AcademicProgressPropertyType.EvaluationPeriodAttemptedCredits:
                    return 2;
                case AcademicProgressPropertyType.EvaluationPeriodCompletedCredits:
                    return 3;
                case AcademicProgressPropertyType.EvaluationPeriodOverallGpa:
                    return 4;
                case AcademicProgressPropertyType.EvaluationPeriodRateOfCompletion:
                    return 5;
                case AcademicProgressPropertyType.CumulativeAttemptedCredits:
                    return 6;
                case AcademicProgressPropertyType.CumulativeCompletedCredits:
                    return 7;
                case AcademicProgressPropertyType.CumulativeOverallGpa:
                    return 8;
                case AcademicProgressPropertyType.CumulativeRateOfCompletion:
                    return 9;
                case AcademicProgressPropertyType.CumulativeAttemptedCreditsExcludingRemedial:
                    return 10;                
                case AcademicProgressPropertyType.CumulativeCompletedCreditsExcludingRemedial:
                    return 11;                               
                case AcademicProgressPropertyType.CumulativeRateOfCompletionExcludingRemedial:
                    return 12;
                default:
                    return 13;                
            }
        }
    }
}