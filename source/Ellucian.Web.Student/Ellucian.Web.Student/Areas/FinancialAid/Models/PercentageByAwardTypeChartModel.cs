﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.FinancialAid;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// PercentageByAwardTypeChartModel class
    /// </summary>
    public class PercentageByAwardTypeChartModel : ChartModel
    {
        public decimal PercentageForAwardType { get; set; }
        public decimal PercentageForAllOtherAwards { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AwardCategoryType AwardCategoryType { get; set; }

        /// <summary>
        /// PercentageByAwardTypeChartModel constructor
        /// </summary>
        /// <param name="totalAmountAwardedForYear">total amount awarded for the year</param>
        /// <param name="totalAmountForAwardType">total amount awarded for the award type</param>
        /// <param name="awardCategoryType">award category type</param>
        public PercentageByAwardTypeChartModel(decimal totalAmountAwardedForYear, decimal totalAmountForAwardType, AwardCategoryType? awardCategoryType)
        {
            if (totalAmountAwardedForYear < 0)
            {
                throw new ApplicationException("totalAmountAwardedForYear is less than 0. Cannot create chart model");
            }

            var percentageForAwardType = (totalAmountForAwardType > 0 && totalAmountAwardedForYear > 0) ? Math.Round(((totalAmountForAwardType / totalAmountAwardedForYear) * 100), 0) : 0;
            PercentageForAwardType = percentageForAwardType == 1 ? (percentageForAwardType / 100) : percentageForAwardType;
            PercentageForAllOtherAwards = 100 - percentageForAwardType;

            AwardCategoryType = awardCategoryType.HasValue ? awardCategoryType.Value : AwardCategoryType.Award;

            ChartData = new List<Data>()
            {
                new Data 
                {
                    value = (int)percentageForAwardType, 
                    color = AwardCategoryType == Models.AwardCategoryType.Award ? "#3B5998" :
                            AwardCategoryType == Models.AwardCategoryType.Loan ? "#CC3366" :
                            "#009999"
                },

                new Data 
                {
                    value = (int)PercentageForAllOtherAwards,
                    color = "#A6A6A6"
                }
            };

            ChartOptions = new Options()
            {
                animation = false,
                percentageInnerCutout = 73
            };
        }
    }
}