﻿//Copyright 2014 Ellucian Company L.P. and its affiliates
using System;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// A summary of loan data for a student
    /// </summary>
    public class LoanRequirements
    {
        public string StudentId { get; set; }

        /// <summary>
        /// Whether or not the student completed the entrance interview for sub/unsub loans
        /// </summary>
        public string DirectLoanEntranceInterviewStatus { get; set; }

        /// <summary>
        /// SubEntranceInterviewDate
        /// </summary>
        public string DirectLoanEntranceInterviewDate { get; set; }

        /// <summary>
        /// Whether or not the student has an active MPN for sub/unsub loans
        /// </summary>
        public string ActiveDirectLoanMpnStatus { get; set; }

        /// <summary>
        /// ActiveMpnDate for sub/unsub loans
        /// </summary>
        public string ActiveDirectLoanMpnExpirationDate { get; set; }

        /// <summary>
        /// Status of the grad plus entrance interview
        /// </summary>
        public string GraduatePlusEntranceInterviewStatus { get; set; }

        /// <summary>
        /// Date of the grad plus entrance interview
        /// </summary>
        public string GraduatePlusEntranceInterviewDate { get; set; }

        /// <summary>
        /// Status of the grad plus MPN
        /// </summary>
        public string ActiveGraduatePlusMpnStatus { get; set; }

        /// <summary>
        /// Expiration Date of the grad plus MPN
        /// </summary>
        public string ActiveGraduatePlusMpnExpirationDate { get; set; }


        public LoanRequirements(Colleague.Dtos.FinancialAid.StudentLoanSummary loanSummaryDto)
        {
            StudentId = loanSummaryDto.StudentId;

            DirectLoanEntranceInterviewStatus = (loanSummaryDto.DirectLoanEntranceInterviewDate.HasValue) ? "Complete" : "Incomplete";

            //Format the date
            if (loanSummaryDto.DirectLoanEntranceInterviewDate.HasValue)
            {
                DirectLoanEntranceInterviewDate = loanSummaryDto.DirectLoanEntranceInterviewDate.Value.ToShortDateString();
            }

            ActiveDirectLoanMpnStatus = (loanSummaryDto.DirectLoanMpnExpirationDate.HasValue && loanSummaryDto.DirectLoanMpnExpirationDate.Value >= DateTime.Today) ? "Complete" : "Incomplete";

            //Format the date
            if (loanSummaryDto.DirectLoanMpnExpirationDate.HasValue)
            {
                ActiveDirectLoanMpnExpirationDate = loanSummaryDto.DirectLoanMpnExpirationDate.Value.ToShortDateString();
            }

            GraduatePlusEntranceInterviewStatus = (loanSummaryDto.GraduatePlusLoanEntranceInterviewDate.HasValue) ? "Complete" : "Incomplete";

            //Format the date
            if (loanSummaryDto.GraduatePlusLoanEntranceInterviewDate.HasValue)
            {
                GraduatePlusEntranceInterviewDate = loanSummaryDto.GraduatePlusLoanEntranceInterviewDate.Value.ToShortDateString();
            }

            ActiveGraduatePlusMpnStatus = (loanSummaryDto.PlusLoanMpnExpirationDate.HasValue && loanSummaryDto.PlusLoanMpnExpirationDate.Value >= DateTime.Today) ? "Complete" : "Incomplete";

            //Format the date
            if (loanSummaryDto.PlusLoanMpnExpirationDate.HasValue)
            {
                ActiveGraduatePlusMpnExpirationDate = loanSummaryDto.PlusLoanMpnExpirationDate.Value.ToShortDateString();
            }


        }
    }

}
