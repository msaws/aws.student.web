﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Student Shopping Sheet class
    /// </summary>
    public class StudentShoppingSheet
    {
        private const string notApplicable = "N/A";

        public string AwardYearCode { get; set; }
        //Used for the Estimated costs section header
        public string AwardYearLabel { get; set; }
        public string CurrentDate { get { return DateTime.Now.ToShortDateString(); } }
        public FAOffice FinancialAidOffice { get; set; }

        //Cost of attendance - strings because display "n/a" if received value is null
        public string BooksAndSupplies { get; set; }
        public string HousingAndMeals { get; set; }
        public string OtherCosts { get; set; }
        public string Transportation { get; set; }
        public string TuitionAndFees { get; set; }

        public string TotalEstimatedCost { get; set; }

        //Grans and scholarships - strings because display "n/a" if received value is null
        public string OtherGrants { get; set; }
        public string SchoolGrants { get; set; }
        public string StateGrants { get; set; }
        public string FederalPellGrant { get; set; }

        public string TotalGrantsAndScholarships { get; set; }

        //Loan options - ints because display 0 if received value is zero
        public int PerkinsLoans { get; set; }
        public int SubsidizedLoans { get; set; }
        public int UnsubsidizedLoans { get; set; }

        //FamilyContribution - string because displays a blank value if null
        public string FamilyContribution { get; set; }

        //NetCosts - always has a value
        public int NetCosts { get; set; }

        //Work options - string because display "n/a" if received value is null
        public string WorkStudy { get; set; }

        //Custom messages
        public IEnumerable<string> CustomMessagesSet1 { get; set; }
        public IEnumerable<string> CustomMessagesSet2 { get; set; }

        /// <summary>
        /// Flag indicating whether the current shopping sheet is for a year prior to 2017
        /// </summary>
        public bool IsPrior2017
        {
            get
            {
                int yearCode;
                if (!string.IsNullOrEmpty(AwardYearCode) && Int32.TryParse(AwardYearCode, out yearCode)
                    && yearCode < 2017)
                {
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Student Shopping Sheet constructor
        /// </summary>
        /// <param name="awardYearDto">award year to create shopping sheet for</param>
        /// <param name="shoppingSheetDtos">a list of student shopping sheet dtos</param>
        /// <param name="financialAidOfficeDtos">list of financial aid offices</param>
        /// <param name="institutionDto">institution dto - might be null(not required)</param>
        public StudentShoppingSheet(Colleague.Dtos.FinancialAid.StudentAwardYear2 awardYearDto,
            IEnumerable<Colleague.Dtos.FinancialAid.ShoppingSheet> shoppingSheetDtos,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeDtos,
            Colleague.Dtos.Base.Institution institutionDto)
        {
            if (awardYearDto == null || shoppingSheetDtos == null || financialAidOfficeDtos == null)
            {
                throw new ArgumentNullException("awardYearDto, shoppingSheetDtos, and financialAidOfficeDtos are required");
            }

            var shoppingSheetDtoForYear = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == awardYearDto.Code && ss.StudentId == awardYearDto.StudentId);
            if (shoppingSheetDtoForYear == null)
            {
                throw new Exception(string.Format("No shopping sheet exists for student {0} for {1} year", awardYearDto.StudentId, awardYearDto.Code));
            }

            var financialAidOfficeForYear = financialAidOfficeDtos.FirstOrDefault(fao => fao.Id == awardYearDto.FinancialAidOfficeId);
            if (financialAidOfficeForYear == null)
            {
                throw new Exception(string.Format("Could not find financial aid office data for student {0} for {1} year", awardYearDto.StudentId, awardYearDto.Code));
            }

            var financialAidConfigurationForYear = financialAidOfficeForYear.Configurations.FirstOrDefault(c => c.AwardYear == awardYearDto.Code);
            if (financialAidConfigurationForYear == null || !financialAidConfigurationForYear.IsShoppingSheetActive)
            {
                throw new Exception(string.Format("ShoppingSheet is not active for student {0}, {1} year, {2} office", awardYearDto.StudentId, awardYearDto.Code, financialAidOfficeForYear.Id));
            }

            AwardYearCode = awardYearDto.Code;

            //Construct the AwardYearLabel (YYYY-YY)
            string year = string.Empty;
            int nextYear;

            //Length of a year code = 4
            if (AwardYearCode.Length == 4 && Int32.TryParse(AwardYearCode, out nextYear))
            {
                //take the last two digits
                year = AwardYearCode.Substring(2);
                if (Int32.TryParse(year, out nextYear))
                {
                    nextYear++;
                }
                AwardYearLabel = AwardYearCode + "-" + nextYear.ToString();
            }

            else
            {
                AwardYearLabel = GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "DefaultCostsLabel");
            }

            if (shoppingSheetDtoForYear.CustomMessages != null)
            {
                CustomMessagesSet1 = shoppingSheetDtoForYear.CustomMessages.Take(3);
                CustomMessagesSet2 = shoppingSheetDtoForYear.CustomMessages.Skip(3).Take(3);
            }

            string institutionName = institutionDto != null ? institutionDto.FinancialAidInstitutionName : string.Empty;

            try { FinancialAidOffice = new FAOffice(financialAidOfficeForYear, institutionName); }
            catch (Exception ex) { throw new Exception("Could not create Financial aid office object", ex); }

            TotalEstimatedCost = shoppingSheetDtoForYear.TotalEstimatedCost.HasValue ? ((int)shoppingSheetDtoForYear.TotalEstimatedCost).ToString("C0", CultureInfo.CurrentCulture) : notApplicable;
            FamilyContribution = shoppingSheetDtoForYear.FamilyContribution.HasValue ? ((int)shoppingSheetDtoForYear.FamilyContribution).ToString("C0", CultureInfo.CurrentCulture) : string.Empty;
            NetCosts = shoppingSheetDtoForYear.NetCosts;
            TotalGrantsAndScholarships = shoppingSheetDtoForYear.TotalGrantsAndScholarships.HasValue ? ((int)shoppingSheetDtoForYear.TotalGrantsAndScholarships).ToString("C0", CultureInfo.CurrentCulture) : notApplicable;

            //Set the costs
            var costs = shoppingSheetDtoForYear.Costs;
            BooksAndSupplies = costs.Any(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.BooksAndSupplies) ?
                costs.Where(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.BooksAndSupplies).Sum(c => c.Cost).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;
            HousingAndMeals = costs.Any(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.HousingAndMeals) ?
                costs.Where(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.HousingAndMeals).Sum(c => c.Cost).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;

            OtherCosts = costs.Any(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.OtherCosts) ?
                costs.Where(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.OtherCosts).Sum(c => c.Cost).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;

            Transportation = costs.Any(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.Transportation) ?
               costs.Where(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.Transportation).Sum(c => c.Cost).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;

            TuitionAndFees = costs.Any(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.TuitionAndFees) ?
                costs.Where(c => c.BudgetGroup == Colleague.Dtos.FinancialAid.ShoppingSheetBudgetGroup.TuitionAndFees).Sum(c => c.Cost).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;

            //Set grants and scholarships
            var grants = shoppingSheetDtoForYear.GrantsAndScholarships;
            OtherGrants = grants.Any(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.OtherGrants) ?
                grants.Where(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.OtherGrants).Sum(g => g.Amount).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;
            SchoolGrants = grants.Any(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.SchoolGrants) ?
                grants.Where(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.SchoolGrants).Sum(g => g.Amount).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;
            StateGrants = grants.Any(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.StateGrants) ?
                grants.Where(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.StateGrants).Sum(g => g.Amount).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;
            FederalPellGrant = grants.Any(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.PellGrants) ?
                grants.Where(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.PellGrants).Sum(g => g.Amount).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;

            //Set loan options
            var loans = shoppingSheetDtoForYear.LoanOptions;
            PerkinsLoans = loans.Where(l => l.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.PerkinsLoans).Sum(l => l.Amount);
            SubsidizedLoans = loans.Where(l => l.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.SubsidizedLoans).Sum(l => l.Amount);
            UnsubsidizedLoans = loans.Where(l => l.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.UnsubsidizedLoans).Sum(l => l.Amount);

            //Set work study
            var workStudy = shoppingSheetDtoForYear.WorkOptions;
            WorkStudy = workStudy.Any(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.WorkStudy) ?
                workStudy.Where(g => g.AwardGroup == Colleague.Dtos.FinancialAid.ShoppingSheetAwardGroup.WorkStudy).Sum(g => g.Amount).ToString("N0", CultureInfo.CurrentCulture) : notApplicable;

        }
    }
}
