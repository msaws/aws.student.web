﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;



namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Represents a single award assigned to a student for a particular year.
    /// </summary>
    public class StudentAward
    {
        //public Colleague.Dtos.FinancialAid.AwardYear AwardYear { get; set; }
        public string AwardYearId { get; set; }
        public string StudentId { get; set; }

        //Flatten the student award by eliminating the award object and directly using its attrs
        public string Code { get; set; }
        public string Description { get; set; }
        public string Explanation { get; set; }

        //Award category: Work, Award, Loan
        [JsonConverter(typeof(StringEnumConverter))]
        public AwardCategoryType AwardCategoryType { get; set; }

        //This can be different than the description property of the AwardStatus. In case
        // there are any pending change requests for the associated award
        public string StatusDescription { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public LoanType? LoanType { get; set; }

        public AwardStatus AwardStatus
        {
            get
            {
                return AwardStatusEvaluator.CalculateAwardStatus(StudentAwardPeriods);
            }

        }
        public List<StudentAwardPeriod> StudentAwardPeriods { get; set; }
        public bool IsEligible { get; set; }

        /// <summary>
        /// IsAmountModifiable flag
        /// </summary>
        public bool IsAmountModifiable { get; set; }
        /// <summary>
        /// IsStatusModifiable flag
        /// </summary>
        public bool IsStatusModifiable { get; set; }
        /// <summary>
        /// Flag that indicates whether award has any pending change request
        /// and is being reviewed by FA staff
        /// </summary>
        public bool IsAwardInReview { get; set; }


        public StudentAward()
        {
            //AwardYear = new Colleague.Dtos.FinancialAid.AwardYear();
            //AwardStatus = new AwardStatus();
            StudentAwardPeriods = new List<StudentAwardPeriod>();
        }

        public StudentAward(AwardYear awardYearModel,
            Colleague.Dtos.FinancialAid.StudentAward studentAwardDto,
            Colleague.Dtos.FinancialAid.Award2 awardDto,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardPeriod> allAwardPeriodsDtoList,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardStatus> allAwardStatusesDtoList)
        {
            AwardYearId = studentAwardDto.AwardYearId;
            StudentId = studentAwardDto.StudentId;
            Code = awardDto.Code;
            Description = awardDto.Description;
            Explanation = awardDto.Explanation;

            LoanType = awardDto.LoanType;
            IsEligible = studentAwardDto.IsEligible;
            IsAmountModifiable = studentAwardDto.IsAmountModifiable;
            IsStatusModifiable = false;
            StudentAwardPeriods = new List<StudentAwardPeriod>();
            foreach (var studentAwardPeriodDto in studentAwardDto.StudentAwardPeriods)
            {
                //If award period status is modifiable, set the whole award status modifiability to true
                if (studentAwardPeriodDto.IsStatusModifiable && !IsStatusModifiable)
                {
                    IsStatusModifiable = studentAwardPeriodDto.IsStatusModifiable;
                }
                var awardPeriod = allAwardPeriodsDtoList.FirstOrDefault(p => p.Code == studentAwardPeriodDto.AwardPeriodId);
                var awardStatus = allAwardStatusesDtoList.FirstOrDefault(s => s.Code == studentAwardPeriodDto.AwardStatusId);
                StudentAwardPeriods.Add(new StudentAwardPeriod(studentAwardPeriodDto, awardPeriod, awardStatus));

                awardYearModel.AddDistinctAwardPeriod(awardPeriod);
            }

            StudentAwardPeriods.Sort((s1, s2) => DateTime.Compare(s1.StartDate, s2.StartDate));
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var studentAward = obj as StudentAward;

            if (studentAward.AwardYearId == this.AwardYearId &&
                studentAward.StudentId == this.StudentId &&
                studentAward.Code == this.Code)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return AwardYearId.GetHashCode() ^ StudentId.GetHashCode() ^ Code.GetHashCode();
        }
    }
}
