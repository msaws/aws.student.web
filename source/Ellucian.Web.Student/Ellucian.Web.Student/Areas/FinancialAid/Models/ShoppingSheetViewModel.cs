﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Shopping sheet view model
    /// </summary>
    public class ShoppingSheetViewModel : FinancialAidAdminModel
    {
        public FAStudent Student { get; set; }

        public IEnumerable<AwardYear> AwardYears { get; set; }

        public string FinancialAidOfficeName { get; set; }

        public List<StudentShoppingSheet> StudentShoppingSheets { get; set; }

        public StudentFinancialAidConfiguration Configuration { get; set; }

        /// <summary>
        /// Indicates whether current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }
        
        /// <summary>
        /// ShoppingSheetViewModel constructor
        /// </summary>
        /// <param name="personData">person data(student/applicant)</param>
        /// <param name="financialAidCounselorData">financial aid coundelor data</param>
        /// <param name="studentAwardYearsData">set of student award years</param>
        /// <param name="financialAidOfficeData">set of financial aid offices</param>
        /// <param name="institutionData">institution data - might be null</param>
        /// <param name="shoppingSheetData">set of student shopping sheets</param>
        /// <param name="currentUser">current system user</param>
        public ShoppingSheetViewModel(Ellucian.Colleague.Dtos.Base.Person personData,
            Ellucian.Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData,
            IEnumerable<StudentAwardYear2> studentAwardYearsData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeData,
            IEnumerable<Colleague.Dtos.FinancialAid.ShoppingSheet> shoppingSheetData,
            Colleague.Dtos.Base.Institution institutionData,
            ICurrentUser currentUser)
        {
            StudentShoppingSheets = new List<StudentShoppingSheet>();

            if (personData == null) { personData = new Ellucian.Colleague.Dtos.Student.Student(); }

            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();
            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            var awardYears = new List<AwardYear>();

            if (financialAidOfficeData == null)
            {
                financialAidOfficeData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }

            var configurationDtos = financialAidOfficeData.SelectMany(o => o.Configurations).ToList();
            Configuration = new StudentFinancialAidConfiguration(studentAwardYearsData, configurationDtos);           

            if (financialAidOfficeData.Count() > 1)
            {
                institutionData = null;
            }

            foreach (var activeStudentAwardYearDto in studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos))
            {
                var awardYearModel = new AwardYear(activeStudentAwardYearDto);
                try
                {
                    awardYearModel.Counselor = new Counselor(financialAidCounselorData, null, activeStudentAwardYearDto, financialAidOfficeData);
                }
                catch (Exception) {/*Don't care if a counselor isn't created */}
                awardYears.Add(awardYearModel);

                try
                {
                    StudentShoppingSheets.Add(new StudentShoppingSheet(activeStudentAwardYearDto, shoppingSheetData, financialAidOfficeData, institutionData));
                }
                catch (Exception) { }
            }

            AwardYears = awardYears.OrderByDescending(ay => ay.Code);

            Student = new FAStudent(personData);

        }
    }
}
