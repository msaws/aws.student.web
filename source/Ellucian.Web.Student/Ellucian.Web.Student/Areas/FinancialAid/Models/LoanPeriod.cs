﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.FinancialAid;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Loan period object (for loan subcategory)
    /// </summary>
    public class LoanPeriod
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }

        //Calculated based on statuses for a particular period of all loans in this subcategory
        public bool IsActive { get; set; }

        //Calculated based on same name booleans for a particular period of
        //all the loans in the subcategory
        public bool IsStatusModifiable { get; set; }
    }

}
