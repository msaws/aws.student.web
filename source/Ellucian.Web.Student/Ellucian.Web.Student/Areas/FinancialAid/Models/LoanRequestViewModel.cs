﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// View model that encapsulates a student's loan request and related items to be
    /// displayed on the LoanRequest page
    /// </summary>
    public class LoanRequestViewModel : FinancialAidAdminModel
    {

        /// <summary>
        /// Set of AwardYears for which the student has data
        /// </summary>
        public IEnumerable<AwardYear> AwardYears { get; set; }

        /// <summary>
        /// Person data describing the student
        /// </summary>
        public FAStudent Person { get; set; }

        /// <summary>
        /// List of pending loan requests across award years
        /// </summary>
        public List<StudentLoanRequest> LoanRequests { get; set; }

        /// <summary>
        /// Configuration options
        /// </summary>
        public StudentFinancialAidConfiguration Configuration { get; set; }

        /// <summary>
        /// Indicates whether current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }

        /// <summary>
        /// LoanRequestViewModel constructor
        /// </summary>
        /// <param name="personData">person (student/applicant) data</param>
        /// <param name="studentAwardYearsData">set of student award years</param>
        /// <param name="studentDocumentsData">set of student documents</param>
        /// <param name="communicationCodesData">set of reference communication codes data</param>
        /// <param name="officeCodesData">set of reference office codes data</param>
        /// <param name="studentFaApplicationsData">set of student applications (fafsa, profile)</param>
        /// <param name="financialAidCounselorData">fa counselor data</param>
        /// <param name="financialAidOfficeData">set of financial aid offices set ups</param>
        /// <param name="pendingStudentLoanRequestModels">list of currently pending student loan requests</param>
        /// <param name="counselorPhoneNumbers">set of counselor phone numbers</param>
        /// <param name="currentUser">current user</param>
        public LoanRequestViewModel(Colleague.Dtos.Base.Person personData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentsData,
            IEnumerable<Colleague.Dtos.Base.CommunicationCode2> communicationCodesData,
            IEnumerable<Colleague.Dtos.Base.OfficeCode> officeCodesData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidApplication2> studentFaApplicationsData,
            Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeData,
            IEnumerable<StudentLoanRequest> pendingStudentLoanRequestModels,
            Colleague.Dtos.Base.PhoneNumber counselorPhoneNumbers,
            ICurrentUser currentUser)
        {
            if (personData == null) { personData = new Ellucian.Colleague.Dtos.Student.Student(); }
            //Create a Person based on the type of user (student/ applicant)
            var personType = personData.GetType();
            if (personType == typeof(Colleague.Dtos.Student.Student))
            {
                var student = (Colleague.Dtos.Student.Student)personData;
                Person = new FAStudent(student);
            }
            else
            {
                var applicant = (Colleague.Dtos.Student.Applicant)personData;
                Person = new FAStudent(applicant);
            }

            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();

            if (financialAidOfficeData == null)
            {
                financialAidOfficeData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }
            var configurationDtos = financialAidOfficeData.SelectMany(o => o.Configurations).ToList();
            Configuration = new StudentFinancialAidConfiguration(studentAwardYearsData, configurationDtos);            

            LoanRequests = pendingStudentLoanRequestModels.ToList();

            AwardYears = studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos)
                .Select(activeStudentAwardYearDto =>
                    {
                        var awardYear = new AwardYear(activeStudentAwardYearDto);
                        try
                        {
                            awardYear.Counselor = new Counselor(financialAidCounselorData, counselorPhoneNumbers, activeStudentAwardYearDto, financialAidOfficeData);
                        }
                        catch (Exception) { }
                        return awardYear;
                    })
                .OrderByDescending(awardYear => awardYear.Code);
        }
    }
}
