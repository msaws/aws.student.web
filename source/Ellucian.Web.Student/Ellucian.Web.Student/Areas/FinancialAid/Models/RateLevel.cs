﻿//Copyright 2015 Ellucian Company L.P. and its affiliates

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Rate level enum - used for such properties as
    /// graduationRateLevel, LoanDefaultLevel, etc. 
    /// (ShoppingSheetConfiguration)
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RateLevel
    {
        /// <summary>
        /// Low rate level
        /// </summary>
        Low,
        /// <summary>
        /// Medium/equal rate level
        /// </summary>
        Medium,
        /// <summary>
        /// High rate level
        /// </summary>
        High
    }
}