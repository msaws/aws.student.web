﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.FinancialAid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class AwardLetterInfo
    {
        /// <summary>
        /// Award year code of this award letter
        /// </summary>
        public string AwardYearCode { get; set; }
        /// <summary>
        /// Flag to indicate whether the award letter for the year is available:
        /// IsAwardLetterActive + Checklist item's statuses
        /// </summary>
        public bool IsAwardLetterAvailable { get; set; }
        /// <summary>
        /// Flag to indicate whether the latest award letter for the year 
        /// has been accepted
        /// </summary>
        public bool IsAwardLetterAccepted { get; set; }        
        /// <summary>
        /// Flag that indicates whether there are any awards that need to be taken care of 
        /// before the award letter can be signed
        /// </summary>
        public bool DoesAnyAwardRequireAction { get; set; }

        
        /// <summary>
        /// AwardLetterInfo constructor
        /// </summary>
        /// <param name="awardYearModel"></param>
        /// <param name="configurationForTheYear"></param>
        /// <param name="studentAwardsForYear"></param>
        /// <param name="awardStatuses"></param>
        /// <param name="awardLetterForYear"></param>
        public AwardLetterInfo(AwardYear awardYearModel, 
            FinancialAidConfiguration3 configurationForTheYear, 
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> studentAwardsForYear,
            IEnumerable<AwardStatus> awardStatuses,
            AwardLetter2 awardLetterForYear)
        {
            if (awardYearModel == null) { throw new ArgumentNullException("awardYearModel"); }

            if (string.IsNullOrEmpty(awardYearModel.Code)) { throw new ArgumentException("awardYearModel code cannot be null or empty"); }

            AwardYearCode = awardYearModel.Code;

            IsAwardLetterAvailable = (configurationForTheYear != null ? configurationForTheYear.IsAwardLetterActive : false) && awardYearModel.ArePrerequisitesSatisfied;
            IsAwardLetterAccepted = awardLetterForYear != null ? awardLetterForYear.AcceptedDate.HasValue : false;

            var pendingAwardPeriodsForYear = GetPendingAwardPeriodsForYear(studentAwardsForYear, awardStatuses);
            DoesAnyAwardRequireAction = pendingAwardPeriodsForYear.Any(pap => !pap.IsIgnoredOnAwardLetter);
        }

        /// <summary>
        /// Gets all of the pending award periods from the student awards for the year
        /// </summary>
        /// <param name="studentAwardsForYear"></param>
        /// <param name="awardStatuses"></param>
        /// <returns></returns>
        private IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardPeriod> GetPendingAwardPeriodsForYear(IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> studentAwardsForYear, IEnumerable<AwardStatus> awardStatuses)
        {
            var pendingAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>();
            if (studentAwardsForYear != null && studentAwardsForYear.Any() && awardStatuses != null && awardStatuses.Any())
            {
                //all possible pending award status codes
                var pendingAwardStatuses = awardStatuses.Where(s => s.Category == AwardStatusCategory.Estimated || s.Category == AwardStatusCategory.Pending).Select(s => s.Code);
                //all student award periods with the pending statuses for the year
                pendingAwardPeriods = studentAwardsForYear.SelectMany(sa => sa.StudentAwardPeriods)
                    .Where(sap => pendingAwardStatuses.Contains(sap.AwardStatusId)).ToList();
                
            }
            return pendingAwardPeriods;
        }
    }
}