﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Student Award Letter class
    /// </summary>
    public class StudentAwardLetter
    {
        /// <summary>
        /// Award letter award year code
        /// </summary>
        public string AwardYearCode { get; set; }
        /// <summary>
        /// Award letter award year description
        /// </summary>
        public string AwardYearDescription { get; set; }
        /// <summary>
        /// Student id
        /// </summary>
        public string StudentId { get; set; }
        /// <summary>
        /// Award letter history record id - needed for getting the pdf report
        /// </summary>
        public string AwardLetterRecordId { get; set; }
        /// <summary>
        /// Student name
        /// </summary>
        public string StudentName { get; set; }
        /// <summary>
        /// Accepted date for display formatted as a short string
        /// </summary>
        public string AcceptedDateDisplay { get { return (AcceptedDate.HasValue) ? AcceptedDate.Value.ToShortDateString() : string.Empty; } }
        /// <summary>
        /// Accepted date
        /// </summary>
        public DateTime? AcceptedDate { get; set; }
        /// <summary>
        /// Today's date
        /// </summary>
        public string CurrentDate { get { return DateTime.Now.ToShortDateString(); } }
        /// <summary>
        /// Award letter opening paragraph
        /// </summary>
        public string OpeningParagraph { get; set; }
        /// <summary>
        /// Award letter closing paragraph
        /// </summary>
        public string ClosingParagraph { get; set; }
        /// <summary>
        /// Flag indicating whether to display the office contact address
        /// </summary>
        public bool IsContactBlockActive { get; set; }
        /// <summary>
        /// Office contact address
        /// </summary>
        public List<string> ContactAddress { get; set; }
        /// <summary>
        /// Student address
        /// </summary>
        public List<string> StudentAddress { get; set; }
        /// <summary>
        /// Flag indicating whether to display Need, Budget, EFC amounts on the award letter
        /// </summary>
        public bool IsNeedBlockActive { get; set; }
        /// <summary>
        /// Budget amount
        /// </summary>
        public int BudgetAmount { get; set; }
        /// <summary>
        /// EFC amount
        /// </summary>
        public int EstimatedFamilyContributionAmount { get; set; }
        /// <summary>
        /// Need amount
        /// </summary>
        public int NeedAmount { get; set; }
        /// <summary>
        /// Flag indicating whether to display housing code on the award letter
        /// </summary>
        public bool IsHousingCodeActive { get; set; }
        /// <summary>
        /// Housing code
        /// </summary>
        public string HousingCode { get; set; }

        /// <summary>
        /// Total amount of all the awards included on the award letter
        /// </summary>
        public decimal? AwardLetterAwardsTotal { get; set; }

        /// <summary>
        /// List of award groups such as "scholarhips and grants, loans, etc. each of which contains
        /// a list of awards that belong to the group
        /// </summary>
        public List<AwardLetterAwardGroup> StudentAwardGroups { get; set; }

        /// <summary>
        /// Column header title for the awards column
        /// </summary>
        public string AwardColumnHeader { get; set; }
        /// <summary>
        /// Column header title for the total column
        /// </summary>
        public string TotalColumnHeader { get; set; }
        /// <summary>
        /// List of column titles for award periods 
        /// </summary>
        public List<string> AwardPeriodColumnHeaders { get; set; }        
        
        /// <summary>
        /// Flag to indicate whether the award letter is accepted
        /// </summary>
        public bool IsLetterAccepted { get { return (AcceptedDate.HasValue); } }

        /// <summary>
        /// Flag to indicate whether there are any awards for the current year for the student 
        /// (including those that might be excluded from award letter display)
        /// </summary>
        public bool AreAwardsPresent { get; set; }

        /// <summary>
        /// Flag to indicate whether award letter contains any awards to display to the student:
        /// there can be none if there are no award periods for the current year or there are awards/award periods
        /// that were excluded from being viewable on award letter
        /// </summary>
        public bool AreAnyAwardsToDisplay { get; set; }        

        /// <summary>
        /// Flag to indicate whether any of the award letter awards are pending (excluding ignored awards)
        /// </summary>
        public bool AreAnyAwardsPending { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public StudentAwardLetter()
        {
            ContactAddress = new List<string>();
            StudentAddress = new List<string>();
            AwardPeriodColumnHeaders = new List<string>();
        }        

        /// <summary>
        /// StudentAwardLetter constructor
        /// </summary>
        /// <param name="studentAwardYearDto">student award year DTO</param>
        /// <param name="studentAwardLetterDto">student award letter DTO</param>        
        /// <param name="studentAwardDtos">set of student award DTOs for the selected year</param>
        /// <param name="awardStatusDtos">set of reference award status DTOs</param>
        /// <param name="awardLetterConfigurationDtos">set of award letter configuration DTOs</param>
        public StudentAwardLetter(StudentAwardYear2 studentAwardYearDto,
            AwardLetter2 studentAwardLetterDto,            
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> studentAwardDtos,
            IEnumerable<AwardStatus> awardStatusDtos,
            IEnumerable<AwardLetterConfiguration> awardLetterConfigurationDtos,
            FinancialAidConfiguration3 currentConfiguration)
        {
            if (studentAwardYearDto == null || studentAwardLetterDto == null)
            {
                throw new ArgumentNullException("studentAwardYearDto and studentAwardLetterDto arguments are required");
            }

            if (awardLetterConfigurationDtos == null || !awardLetterConfigurationDtos.Any())
            {
                throw new ArgumentNullException("awardLetterConfigurationDtos");
            }            

            //Get award letter configuration by the configuration id
            var awardLetterConfigurationForYear = awardLetterConfigurationDtos.FirstOrDefault(c => c.Id == studentAwardLetterDto.AwardLetterParameterId);
            if (awardLetterConfigurationForYear == null)
            {
                throw new Exception(string.Format("No award letter parameters for {0} year {1} award letter", studentAwardYearDto.Code, studentAwardLetterDto.Id));
            }
            
            StudentId = studentAwardLetterDto.StudentId;
            StudentName = studentAwardLetterDto.StudentName;
            StudentAddress = studentAwardLetterDto.StudentAddress != null ? studentAwardLetterDto.StudentAddress.Select(a => a.AddressLine).ToList() : new List<string>();
            
            AwardYearCode = studentAwardYearDto.Code;
            AwardYearDescription = studentAwardYearDto.Description;            
            AwardLetterRecordId = studentAwardLetterDto.Id;

            AcceptedDate = studentAwardLetterDto.AcceptedDate;

            OpeningParagraph = studentAwardLetterDto.OpeningParagraph.Replace(Environment.NewLine, "<br />");
            ClosingParagraph = studentAwardLetterDto.ClosingParagraph.Replace(Environment.NewLine, "<br />");
            ContactAddress = studentAwardLetterDto.ContactAddress != null ? studentAwardLetterDto.ContactAddress.Select(a => a.AddressLine).ToList() : new List<string>();            

            EstimatedFamilyContributionAmount = studentAwardLetterDto.EstimatedFamilyContributionAmount;
            BudgetAmount = studentAwardLetterDto.BudgetAmount;
            NeedAmount = studentAwardLetterDto.NeedAmount;
            HousingCode = studentAwardLetterDto.HousingCode == Colleague.Dtos.FinancialAid.HousingCode.OnCampus ? GlobalResources.GetString(FinancialAidResourceFiles.AwardLetterResources, "OnCampusHousing") :
                          studentAwardLetterDto.HousingCode == Colleague.Dtos.FinancialAid.HousingCode.OffCampus ? GlobalResources.GetString(FinancialAidResourceFiles.AwardLetterResources, "OffCampusHousing") :
                          studentAwardLetterDto.HousingCode == Colleague.Dtos.FinancialAid.HousingCode.WithParent ? GlobalResources.GetString(FinancialAidResourceFiles.AwardLetterResources, "WithParentHousing") :
                          string.Empty;
            
            IsContactBlockActive = awardLetterConfigurationForYear.IsContactBlockActive;
            IsNeedBlockActive = awardLetterConfigurationForYear.IsNeedBlockActive;            
            IsHousingCodeActive = awardLetterConfigurationForYear.IsHousingBlockActive;
            

            AwardColumnHeader = awardLetterConfigurationForYear.AwardTableTitle;
            TotalColumnHeader = awardLetterConfigurationForYear.AwardTotalTitle;

            var awardLetterPeriodsForYear = studentAwardLetterDto.AwardLetterAnnualAwards.SelectMany(a => a.AwardLetterAwardPeriods).ToList();
            AwardPeriodColumnHeaders = awardLetterPeriodsForYear.Any() ? awardLetterPeriodsForYear.OrderBy(ap => ap.ColumnNumber).Select(ap => ap.ColumnName).Distinct().ToList() : new List<string>();

            StudentAwardGroups = new List<AwardLetterAwardGroup>();
            //Get all distinct awardLetterAwardGroup numbers
            var allDistinctAwardGroups = studentAwardLetterDto.AwardLetterAnnualAwards.Select(a => a.GroupNumber).Distinct();
            foreach (var group in allDistinctAwardGroups)
            {
                var groupAwards = studentAwardLetterDto.AwardLetterAnnualAwards.Where(a => a.GroupNumber == group);
                string groupName = groupAwards.First().GroupName;
                try
                {
                    StudentAwardGroups.Add(new AwardLetterAwardGroup(groupName, group, groupAwards, awardLetterPeriodsForYear));
                }
                catch { }
            }

            //Add an "AwardLetterGroup" for the total row in the award letter awards table
            try
            {
                StudentAwardGroups.Add(new AwardLetterAwardGroup(studentAwardLetterDto));
            }
            catch { }

            if (studentAwardDtos != null && studentAwardDtos.Any())
            {
                AreAwardsPresent = true;

                //Award periods of all student awards for the year - some of these might not be included on the award letter
                var awardPeriodsForYear = studentAwardDtos.SelectMany(s => s.StudentAwardPeriods);
                if (awardPeriodsForYear.Any())
                {
                    AwardStatus awardStatusOfAwardPeriodsForYear = null;
                    if (awardStatusDtos != null && awardStatusDtos.Any())
                    {
                        awardStatusOfAwardPeriodsForYear = AwardStatusEvaluator.CalculateAwardStatus(awardPeriodsForYear, awardStatusDtos, true);                        
                    }

                    AreAnyAwardsPending = awardStatusOfAwardPeriodsForYear != null ?
                        (awardStatusOfAwardPeriodsForYear.Category == AwardStatusCategory.Pending ||
                        awardStatusOfAwardPeriodsForYear.Category == AwardStatusCategory.Estimated)
                        : false;
                }
            }

            AreAnyAwardsToDisplay = studentAwardLetterDto.AwardLetterAnnualAwards.Any();

            //Sum award periods' amounts if any award periods - otherwise sum the annualAwardAmounts            
            AwardLetterAwardsTotal = awardLetterPeriodsForYear.Any() ? awardLetterPeriodsForYear.Sum(ap => ap.AwardPeriodAmount)
                : studentAwardLetterDto.AwardLetterAnnualAwards.Sum(a => a.AnnualAwardAmount);
        }       
    }
}
