﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// View model that encapsulates a student's required documents data and related items to be
    /// displayed on the Required Documents page
    /// </summary>
    public class DocumentsViewModel : FinancialAidAdminModel
    {
        /// <summary>
        ///List of student document collections. One Collection Per Year
        /// </summary>
        public List<StudentDocumentCollection> StudentDocumentCollections { get; set; }

        /// <summary>
        /// Set of award years
        /// </summary>
        public List<AwardYear> AwardYears { get; set; }

        public Colleague.Dtos.Base.Person Person { get; set; }

        /// <summary>
        /// Indicates whether the current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="personData">person(student/applicant) data</param>
        /// <param name="financialAidCounselorData">student fa counselor data</param>
        /// <param name="studentDocumentsData">set of student documents</param>
        /// <param name="communicationCodesData">set of reference communication codes</param>
        /// <param name="officeCodesData">set of reference office codes</param>
        /// <param name="studentAwardYearsData">set of student award years</param>
        /// <param name="financialAidOfficeData">set of fa offices</param>
        /// <param name="currentUser">current system user</param>
        public DocumentsViewModel(Colleague.Dtos.Base.Person personData,
            Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentsData,
            IEnumerable<Colleague.Dtos.Base.CommunicationCode2> communicationCodesData,
            IEnumerable<Colleague.Dtos.Base.OfficeCode> officeCodesData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeData,
            ICurrentUser currentUser)
        {
            //Student/Applicant demographic data
            Person = personData;
            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();

            if (studentAwardYearsData == null)
            {
                studentAwardYearsData = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            }

            //Set of student documents
            StudentDocumentCollections = new List<StudentDocumentCollection>();

            //Set of award years in the drop down box
            AwardYears = new List<AwardYear>();

            if (financialAidOfficeData == null)
            {
                financialAidOfficeData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }

            var configurationDtos = financialAidOfficeData.SelectMany(o => o.Configurations).ToList();

            foreach (var activeStudentAwardYearDto in studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos))
            {
                //Get the office configuration for the year
                var configurationForTheYear = configurationDtos.GetConfigurationDtoOrDefault(activeStudentAwardYearDto);

                var awardYear = new AwardYear(activeStudentAwardYearDto);
                try
                {
                    awardYear.Counselor = new Counselor(financialAidCounselorData, null, activeStudentAwardYearDto, financialAidOfficeData);
                }
                catch (Exception) { /* Doesn't matter if there's an exception */ }
                AwardYears.Add(awardYear);

                StudentDocumentCollections.Add(new StudentDocumentCollection(activeStudentAwardYearDto, studentDocumentsData, communicationCodesData, officeCodesData, configurationForTheYear));
            }

            AwardYears = AwardYears.OrderByDescending(ay => ay.Code).ToList();
        }
    }
}
