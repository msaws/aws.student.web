﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// SAP Credits item class for items that are displayed in SAP Credits section:
    /// Completed credits, attempted credits, etc.
    /// </summary>
    public class SAPDetailItem
    {
        /// <summary>
        /// Code of the credits item
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Description of the item
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Item explanation
        /// </summary>
        public string Explanation { get; set; }
        /// <summary>
        /// Item value
        /// </summary>
        public decimal Value { get; set; }
        
        public SAPDetailItemType ItemType { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SAPDetailItem()
        {
        }

       
    }
    
}