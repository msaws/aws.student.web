﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Student checklist class
    /// </summary>
    public class StudentChecklist 
    {    
        /// <summary>
        /// Award year code
        /// </summary>
        public string AwardYearCode { get; set; }        

        /// <summary>
        /// List of checklist items
        /// </summary>
        public List<ChecklistItem> ChecklistItems { get; set; }

        /// <summary>
        /// Active(current) checklist item
        /// </summary>
        public ChecklistItem ActiveChecklistItem { get; set; }        

        /// <summary>
        /// Constructor accepting studentAwardYearDto as an argument
        /// </summary>
        /// <param name="studentAwardYearDto">student award year dto</param>
        public StudentChecklist(Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto)
        {
            if (studentAwardYearDto == null)
            {
                throw new ArgumentNullException("studentAwardYearDto");
            }
            AwardYearCode = studentAwardYearDto.Code;
            ChecklistItems = new List<ChecklistItem>();
            ActiveChecklistItem = new ChecklistItem();
        }
       
    }
}
