﻿/*Copyright 2017 Ellucian Company L.P. and its affiliates*/
using Ellucian.Web.Student.Models.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class FinancialAidAdminModel : PrivacyRestrictionModel
    {
        public string PersonId { get; set; }

        public string PersonName { get; set; }
    }
}
