﻿//Copyright 2014 Ellucian Company L.P. and its affiliates

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Checklist item statuses enum 
    /// </summary>
    public enum ChecklistItemStatus
    {
        Complete,
        Incomplete,
        NotAvailable,
        InProgress
    }
}
