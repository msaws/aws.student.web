﻿/*Copyright 2014-2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Object containing a list of loans a student has so far as well as their total amount
    /// </summary>
    public class StudentLoansSummary
    {
        /// <summary>
        /// Sum of loan amounts from listed institutions
        /// </summary>
        public int TotalLoanAmountBorrowed
        {
            get
            {
                if (LoanHistory == null || !LoanHistory.Any())
                {
                    return 0;
                }

                return LoanHistory.Sum(h => h.TotalLoanAmount);
            }
        }

        /// <summary>
        /// Aggregate amount can be different from TotalLoanAmountBorrowed:
        /// while TotalLoanAmountBorrowed is the sum of all loans from
        /// known institutions, aggregate amount is the total amount of 
        /// all loans student ever took
        /// </summary>
        public int AggregateTotalLoanAmount { get; set; }

        /// <summary>
        /// Difference between AggregateTotalLoanAmount and TotalLoanAmountBorrowed
        /// amounts
        /// </summary>
        public int OtherLoansAmount {
            get
            {
                if (AggregateTotalLoanAmount == 0 || (AggregateTotalLoanAmount < TotalLoanAmountBorrowed)) return 0;
                else
                {
                    return (AggregateTotalLoanAmount - TotalLoanAmountBorrowed);
                }
            }
        }
        

        /// <summary>
        /// Loast of LoanDetail items each of which insludes institution name
        /// and amount borrowed at that institution
        /// </summary>
        public List<LoanDetails> LoanHistory { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public StudentLoansSummary()
        {
            LoanHistory = new List<LoanDetails>();
        }

        /// <summary>
        /// Constructor that accepts studentLoanSummary DTO and 
        /// ipedsInstitutionsList
        /// </summary>
        /// <param name="studentLoanSummaryDto">studentLoanSummary DTO vontaining data about loan history</param>
        /// <param name="ipedsInstitutionDtoList">list of ipedsInstitution DTOs</param>
        public StudentLoansSummary(Colleague.Dtos.FinancialAid.StudentLoanSummary studentLoanSummaryDto,
            IEnumerable<Colleague.Dtos.FinancialAid.IpedsInstitution> ipedsInstitutionDtoList)
        {
            LoanHistory = new List<LoanDetails>();

            if (studentLoanSummaryDto != null){
                AggregateTotalLoanAmount = studentLoanSummaryDto.StudentLoanCombinedTotalAmount;

                if (studentLoanSummaryDto.StudentLoanHistory != null &&
                studentLoanSummaryDto.StudentLoanHistory.Any())
                {
                    foreach (var studentLoanHistoryDto in studentLoanSummaryDto.StudentLoanHistory)
                    {
                        var ipedsInstitution = ipedsInstitutionDtoList != null ? ipedsInstitutionDtoList
                            .FirstOrDefault(i => i.OpeId.TrimStart('0') == studentLoanHistoryDto.OpeId.TrimStart('0'))
                            : null;

                        LoanHistory.Add(new LoanDetails(studentLoanHistoryDto, ipedsInstitution));
                    }
                }

            }
        }
    }
}
