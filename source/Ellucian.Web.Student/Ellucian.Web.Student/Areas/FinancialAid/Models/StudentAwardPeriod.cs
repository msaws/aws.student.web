﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates*/
using System;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class StudentAwardPeriod
    {

        public string AwardId { get; set; }
        public string AwardYearId { get; set; }

        //Flatten StudentAwardPeriod by directly using AwardPeriod's attrs
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }

        public AwardStatus AwardStatus { get; set; }
        public decimal? OriginalAwardAmount { get; set; }
        public decimal? AwardAmount { get; set; }
        public bool IsTransmitted { get; set; }
        public bool IsActive { get; set; }
        public bool IsFrozen { get; set; }
        public bool IsAmountModifiable { get; set; }
        public bool IsStatusModifiable { get; set; }
        public bool IsIgnoredOnAwardLetter { get; set; }
        public bool IsViewableOnAwardLetter { get; set; }

        /// <summary>
        /// If true, the award period doesn't exist for the student and was added to view model in order to fill out the award table
        /// If false, the award period is part of the student's award records.
        /// </summary>
        public bool IsDummy { get; set; }

        public StudentAwardPeriod()
        {
            AwardStatus = new AwardStatus();
            IsActive = true;
        }

        public StudentAwardPeriod(Colleague.Dtos.FinancialAid.StudentAwardPeriod studentAwardPeriodDto, 
            AwardPeriod awardPeriodDto, 
            Colleague.Dtos.FinancialAid.AwardStatus awardStatusDto)
        {
            AwardId = studentAwardPeriodDto.AwardId;
            AwardYearId = studentAwardPeriodDto.AwardYearId;

            Code = awardPeriodDto.Code;
            Description = awardPeriodDto.Description;
            StartDate = awardPeriodDto.StartDate;
            
            AwardStatus = awardStatusDto;

            OriginalAwardAmount = studentAwardPeriodDto.AwardAmount;
            AwardAmount = (studentAwardPeriodDto.AwardAmount.HasValue) ? studentAwardPeriodDto.AwardAmount.Value : 0;
            IsActive = (awardStatusDto.Category != AwardStatusCategory.Rejected && awardStatusDto.Category != AwardStatusCategory.Denied);
            IsTransmitted = studentAwardPeriodDto.IsTransmitted;
            IsFrozen = studentAwardPeriodDto.IsFrozen;
            IsAmountModifiable = studentAwardPeriodDto.IsAmountModifiable;
            IsStatusModifiable = studentAwardPeriodDto.IsStatusModifiable;
            IsIgnoredOnAwardLetter = studentAwardPeriodDto.IsIgnoredOnAwardLetter;
            IsViewableOnAwardLetter = studentAwardPeriodDto.IsViewableOnAwardLetterAndShoppingSheet;
            IsDummy = false;
        }

        public static StudentAwardPeriod CreateDummy(string awardId, string awardYearId, Colleague.Dtos.FinancialAid.AwardPeriod awardPeriodDto, Colleague.Dtos.FinancialAid.AwardStatus awardStatusDto)
        {
            return new StudentAwardPeriod()
            {
                AwardId = awardId,
                AwardYearId = awardYearId,
                AwardAmount = 0,
                Code = awardPeriodDto.Code,
                Description = awardPeriodDto.Description,
                StartDate = awardPeriodDto.StartDate,
                AwardStatus = awardStatusDto,
                IsActive = false,
                IsFrozen = false,
                IsAmountModifiable = false,
                IsTransmitted = false,
                IsDummy = true
            };
        }


    }
}
