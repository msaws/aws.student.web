﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Security;
using System.Text.RegularExpressions;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// View model that encapsulates a student's award data and related items to be
    /// displayed on the MyAwards page
    /// </summary>
    public class AwardsViewModel : FinancialAidAdminModel
    {

        /// <summary>
        /// The student's awards across all years
        /// </summary>
        public List<StudentAwardCollection> StudentAwards { get; set; }

        /// <summary>
        /// The student's subsizdized loans across all years
        /// </summary>
        public List<LoanCollection> StudentSubLoans { get; set; }

        /// <summary>
        /// The student's unsubsidized loans across all years
        /// </summary>
        public List<LoanCollection> StudentUnsubLoans { get; set; }

        /// <summary>
        /// The student's grad plus loans across all years
        /// </summary>
        public List<LoanCollection> StudentGradPlusLoans { get; set; }

        /// <summary>
        /// The student's other loans across all years
        /// </summary>
        public List<StudentAwardCollection> OtherLoans { get; set; }

        /// <summary>
        /// List of Award prerequisites by award year that need to be
        /// completed before we can display awards for the year
        /// </summary>
        public List<AwardPrerequisite> AwardPrerequisites { get; set; }

        /// <summary>
        /// List of Award percentage chart collections across all years
        /// </summary>
        public List<PercentageChartCollection> AwardPercentageCharts { get; set; }

        /// <summary>
        /// Loan Summary object 
        /// </summary>
        public LoanRequirements LoanRequirements { get; set; }

        /// <summary>
        /// Set of AwardYears for which the student has data
        /// </summary>
        public IEnumerable<AwardYear> AwardYears { get; set; }

        /// <summary>
        /// Person data describing the student
        /// </summary>
        public FAStudent Person { get; set; }

        /// <summary>
        /// The student/applicant's financial aid counselor
        /// </summary>
        // public Counselor Counselor { get; set; }

        /// <summary>
        /// SAP Status
        /// </summary>
        public SAPStatus SAPStatus { get; set; }

        /// <summary>
        /// List of pending loan requests across award years
        /// </summary>
        public List<StudentLoanRequest> LoanRequests { get; set; }

        public StudentFinancialAidConfiguration Configuration { get; set; }

        /// <summary>
        /// Indicates whether the current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }

        /// <summary>
        /// List of AwardLetterInfo objects for each year
        /// </summary>
        public List<AwardLetterInfo> AwardLetterData { get; set; }

        /// <summary>
        /// Indicates whether the current user is self. Alternatives are they are proxy
        /// for a student or fa counselor looking up a student other than self
        /// </summary>
        public bool IsUserSelf { get; set; }

        public List<StudentAward> GetAllStudentAwards()
        {
            var allStudentAwardList = new List<StudentAward>();
            allStudentAwardList.AddRange(StudentAwards.SelectMany(sa => sa.StudentAwardsForYear));
            allStudentAwardList.AddRange(StudentAwards.SelectMany(sa => sa.StudentWorkAwardsForYear));
            allStudentAwardList.AddRange(OtherLoans.SelectMany(ol => ol.StudentAwardsForYear));
            allStudentAwardList.AddRange(StudentSubLoans.SelectMany(ssl => ssl.Loans));
            allStudentAwardList.AddRange(StudentUnsubLoans.SelectMany(sul => sul.Loans));
            allStudentAwardList.AddRange(StudentGradPlusLoans.SelectMany(sgpl => sgpl.Loans));
            return allStudentAwardList;
        }

        /// <summary>
        /// Get a list of all loan collections
        /// </summary>
        /// <returns>a list of loan collection objects</returns>
        public List<LoanCollection> GetAllStudentLoanCollections()
        {
            var allLoanCollections = new List<LoanCollection>();
            allLoanCollections.AddRange(StudentSubLoans);
            allLoanCollections.AddRange(StudentUnsubLoans);
            allLoanCollections.AddRange(StudentGradPlusLoans);
            return allLoanCollections;
        }

        /// <summary>
        /// Awards View model constructor
        /// </summary>
        /// <param name="personData">person(student/applicant) data</param>
        /// <param name="studentAwardsData">set of student awards</param>
        /// <param name="studentLoanLimitationsData">student loan limitations data</param>
        /// <param name="studentLoanSummaryData">student loan summary data</param>
        /// <param name="awardsData">set of reference awards</param>
        /// <param name="awardPeriodsData">set of reference award periods</param>
        /// <param name="awardStatusesData">set of reference award statuses</param>
        /// <param name="studentAwardYearsData">set of student award years</param>
        /// <param name="studentDocumentsData">set of student documents</param>
        /// <param name="studentAwardLetterData">List of student award letters</param>
        /// <param name="communicationCodesData">set of reference communication codes</param>
        /// <param name="officeCodesData">set of reference office codes</param>
        /// <param name="studentFaApplicationsData">set of student fa applications</param>
        /// <param name="financialAidCounselorData">student fa counselor data</param>
        /// <param name="financialAidOfficeData">set of fa offices</param>
        /// <param name="pendingStudentLoanRequestsData">set of student pending loan requests</param>
        /// <param name="awardPackageChangeRequestData">set of student award package change requests</param>
        /// <param name="studentChecklistsData">set of student checklists</param>
        /// <param name="financialAidChecklistItemsData">set of reference fa checklists</param>
        /// <param name="academicProgressEvaluationsData">set of student academic progress evaluations</param>
        /// <param name="academicProgressStatusesData">set of reference academic progress statuses</param>
        /// <param name="currentUser">current user</param>        
        /// <param name="isUserSelf">boolean indicating whether user os self</param>
        public AwardsViewModel(Colleague.Dtos.Base.Person personData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> studentAwardsData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentLoanLimitation> studentLoanLimitationsData,
            Colleague.Dtos.FinancialAid.StudentLoanSummary studentLoanSummaryData,
            IEnumerable<Colleague.Dtos.FinancialAid.Award2> awardsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardPeriod> awardPeriodsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardStatus> awardStatusesData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardLetter2> studentAwardLetterData,
            IEnumerable<Colleague.Dtos.Base.CommunicationCode2> communicationCodesData,
            IEnumerable<Colleague.Dtos.Base.OfficeCode> officeCodesData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidApplication2> studentFaApplicationsData,
            Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeData,
            List<StudentLoanRequest> pendingStudentLoanRequestsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest> awardPackageChangeRequestData,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist> studentChecklistsData,
            IEnumerable<Colleague.Dtos.FinancialAid.ChecklistItem> financialAidChecklistItemsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluationsData,
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatusesData,
            ICurrentUser currentUser, bool isUserSelf)
        {
            if (personData == null) { personData = new Ellucian.Colleague.Dtos.Student.Student(); }
            //Create a Person based on the type of user (student/ applicant)
            var personType = personData.GetType();
            if (personType == typeof(Colleague.Dtos.Student.Student))
            {
                var student = (Colleague.Dtos.Student.Student)personData;
                Person = new FAStudent(student);
            }
            else
            {
                var applicant = (Colleague.Dtos.Student.Applicant)personData;
                Person = new FAStudent(applicant);
            }

            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();
            IsUserSelf = isUserSelf;

            LoanRequests = pendingStudentLoanRequestsData;

            StudentAwards = new List<StudentAwardCollection>();
            StudentSubLoans = new List<LoanCollection>();
            StudentUnsubLoans = new List<LoanCollection>();
            StudentGradPlusLoans = new List<LoanCollection>();
            OtherLoans = new List<StudentAwardCollection>();
            AwardPercentageCharts = new List<PercentageChartCollection>();
            AwardLetterData = new List<AwardLetterInfo>();

            if (financialAidOfficeData == null)
            {
                financialAidOfficeData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }

            var configurationDtos = financialAidOfficeData.SelectMany(o => o.Configurations).ToList();
            Configuration = new StudentFinancialAidConfiguration(studentAwardYearsData, configurationDtos);

            var activeStudentAwardYearDtos = studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos).OrderByDescending(say => say.Code);

            var financialAidOffice = (financialAidOfficeData.Count() > 0 && activeStudentAwardYearDtos.Count() > 0) ? financialAidOfficeData.FirstOrDefault(fao => fao.Id == activeStudentAwardYearDtos.First().FinancialAidOfficeId) : null;

            if (financialAidOffice != null && financialAidOffice.AcademicProgressConfiguration != null && financialAidOffice.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive)
            {
                //Filter out evaluations by type
                academicProgressEvaluationsData = academicProgressEvaluationsData != null && academicProgressEvaluationsData.Any() && financialAidOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay.Any() ?
                    academicProgressEvaluationsData.Where(e => financialAidOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay.Any(pt => pt.ToUpper() == e.AcademicProgressTypeCode.ToUpper())).ToList() :
                    academicProgressEvaluationsData; 
                SAPStatus = SAPUtility.GetLatestSAPStatus(academicProgressEvaluationsData, academicProgressStatusesData, null);
            }

            if (studentLoanSummaryData != null) { LoanRequirements = new LoanRequirements(studentLoanSummaryData); }
            AwardPrerequisites = new List<AwardPrerequisite>();

            //Set a flag to indicate if there are any awardPackageChangeRequests
            var awardPackageChangeRequestsExist = awardPackageChangeRequestData != null && awardPackageChangeRequestData.Count() != 0;

            //temporary storage for awardYears
            var awardYears = new List<AwardYear>();
            
            #region award years
            //Populate the list of fa years for student
            foreach (var activeStudentAwardYearDto in activeStudentAwardYearDtos)
            {
                //Get checklist for the year
                var studentChecklistForYear = studentChecklistsData.FirstOrDefault(sc => sc.AwardYear == activeStudentAwardYearDto.Code);
                bool areChecklistItemsAssigned = (studentChecklistForYear != null && studentChecklistForYear.ChecklistItems != null && studentChecklistForYear.ChecklistItems.Count != 0);

                //Get the office configuration for the year
                var configurationForTheYear = financialAidOfficeData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == activeStudentAwardYearDto.FinancialAidOfficeId && c.AwardYear == activeStudentAwardYearDto.Code);                

                // Add all award years to list
                var awardYearModel = new AwardYear(activeStudentAwardYearDto, () =>
                    {
                        if (areChecklistItemsAssigned && financialAidChecklistItemsData != null)
                        {
                            var studentDocumentCollection = new StudentDocumentCollection(activeStudentAwardYearDto, studentDocumentsData, communicationCodesData, officeCodesData, configurationForTheYear);

                            var documentTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.CompletedDocuments);
                            var documentChecklistItem = documentTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == documentTypeChecklistItem.ChecklistItemCode)
                                : null;
                            var isDocumentsCompletionRequired = documentChecklistItem != null && documentChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired;

                            if (studentDocumentCollection.HasOutstandingDocuments && isDocumentsCompletionRequired)
                            {
                                return false;
                            }

                            var applicationsForYear = studentFaApplicationsData.Where(fa => fa.AwardYear == activeStudentAwardYearDto.Code);

                            //Get checklist items to check control statuses
                            var profileTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.PROFILE);
                            var profileChecklistItem = profileTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == profileTypeChecklistItem.ChecklistItemCode)
                                : null;

                            var fafsaTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.FAFSA);
                            var fafsaChecklistItem = fafsaTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == fafsaTypeChecklistItem.ChecklistItemCode)
                                : null;

                            var applicationReviewTypeChecklistItem = financialAidChecklistItemsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.ApplicationReview);
                            var applicationReviewedChecklistItem = applicationReviewTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() ==
                                applicationReviewTypeChecklistItem.ChecklistItemCode) : null;

                            var isProfileCompletionRequired = (configurationForTheYear.IsProfileActive) ?
                                (profileChecklistItem != null && profileChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired) :
                                false;

                            var isFafsaCompletionRequired = fafsaChecklistItem != null && fafsaChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired;
                            var isApplicationReviewCompletionRequired = applicationReviewedChecklistItem != null && applicationReviewedChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired;

                            return
                                (applicationsForYear.Any(app => app.GetType() == typeof(ProfileApplication)) || !isProfileCompletionRequired) &&
                                (applicationsForYear.Any(app => app.GetType() == typeof(Fafsa)) || !isFafsaCompletionRequired) &&
                                (activeStudentAwardYearDto.IsApplicationReviewed || !isApplicationReviewCompletionRequired);
                        }
                        else
                        {
                            return false;
                        }
                    }, areChecklistItemsAssigned);
                
                try
                {
                    awardYearModel.Counselor = new Counselor(financialAidCounselorData, null, activeStudentAwardYearDto, financialAidOfficeData);
                }
                catch (Exception) {/*Don't care if a counselor isn't created */}
                awardYears.Add(awardYearModel);

                // Create a prerequisites list for each year
                AwardPrerequisite prerequisite = new AwardPrerequisite()
                {
                    AwardYearCode = activeStudentAwardYearDto.Code,
                    AreAllPrerequisitesSatisfied = awardYearModel.ArePrerequisitesSatisfied
                };

                AwardPrerequisites.Add(prerequisite);

                //Add PercentageChartCollection to AwardPercentageCharts
                AwardPercentageCharts.Add(new PercentageChartCollection(activeStudentAwardYearDto.Code));

                //Get the award letter and awards for the year
                var awardLetterForYear = studentAwardLetterData.FirstOrDefault(al => al.AwardLetterYear == activeStudentAwardYearDto.Code);
                var studentAwardsForYear = studentAwardsData.Where(sa => sa.AwardYearId == activeStudentAwardYearDto.Code);
                try
                {
                    AwardLetterData.Add(new AwardLetterInfo(awardYearModel, configurationForTheYear, studentAwardsForYear, awardStatusesData, awardLetterForYear));
                }
                catch { }
            }
            #endregion

            #region awards
            foreach (var studentAward in studentAwardsData)
            {
                //Retreive the AwardYear object based on this studentAward's AwardYear
                var awardYear = awardYears.FirstOrDefault(ay => ay.Code == studentAward.AwardYearId);

                if (awardYear != null)
                {
                    //Retrieve the Award object based on this studentAward's AwardId
                    var award = awardsData.FirstOrDefault(a => a.Code == studentAward.AwardId);

                    //Instantiate a new StudentAwardModel
                    var studentAwardModel = new StudentAward();
                    studentAwardModel.IsStatusModifiable = false;

                    //Assign AwardCategory
                    studentAwardModel.AwardCategoryType = AssignAwardCategoryType(award); 

                    //Create new ViewModel StudentAwardPeriod objects from the StudentAwardPeriod DTO
                    //Add each new object to the studentAwardModel
                    foreach (var studentAwardPeriodDto in studentAward.StudentAwardPeriods)
                    {
                        //If award period status is modifiable, set the whole award status modifiability to true
                        if (studentAwardPeriodDto.IsStatusModifiable && !studentAwardModel.IsStatusModifiable)
                        {
                            studentAwardModel.IsStatusModifiable = studentAwardPeriodDto.IsStatusModifiable;
                        }

                        //Retrieve the AwardPeriod object based on this studentAwardPeriod's AwardPeriodId
                        var awardPeriodDto = awardPeriodsData.FirstOrDefault(ap => ap.Code == studentAwardPeriodDto.AwardPeriodId);

                        //Update DistinctAwardPeriods of awardYear object
                        awardYear.AddDistinctAwardPeriod(awardPeriodDto);

                        //Retrieve the AwardStatus object based on this studentAwardPeriod's AwardStatusId
                        var awardStatusDto = awardStatusesData.FirstOrDefault(s => s.Code == studentAwardPeriodDto.AwardStatusId);

                        studentAwardModel.StudentAwardPeriods.Add(new StudentAwardPeriod(studentAwardPeriodDto, awardPeriodDto, awardStatusDto));
                    }

                    //Set the other attributes of the StudentAward view model
                    studentAwardModel.AwardYearId = awardYear.Code;
                    studentAwardModel.StudentId = studentAward.StudentId;
                    studentAwardModel.Code = award.Code;
                    studentAwardModel.Description = award.Description;
                    studentAwardModel.Explanation = FormatAwardExplanation(award.Explanation);
                    studentAwardModel.LoanType = award.LoanType;

                    studentAwardModel.IsEligible = studentAward.IsEligible;
                    studentAwardModel.IsAmountModifiable = studentAward.IsAmountModifiable;

                    //Get the associated awardPackageChangeRequest if any
                    AwardPackageChangeRequest awardPackageChangeRequest = null;
                    if (awardPackageChangeRequestsExist)
                    {
                        awardPackageChangeRequest = awardPackageChangeRequestData.FirstOrDefault(apcr => (apcr.AwardPeriodChangeRequests.Any(awpcr => awpcr.Status == AwardPackageChangeRequestStatus.Pending))
                            && (apcr.AwardId == studentAward.AwardId) && (apcr.AwardYearId == studentAward.AwardYearId));
                    }

                    //Calculate and assign statusDescription and make necessary changes to other applicable properties
                    CalculateChangeRequestRelatedProperties(awardPackageChangeRequest, studentAwardModel, awardStatusesData);

                    //Retrieve the loanlimitations object for the year.
                    //If one does not exist, create one with all of the loan limits set to 0
                    var loanLimitsForYear = studentLoanLimitationsData.FirstOrDefault(l => l.AwardYear == awardYear.Code) != null
                                                ? studentLoanLimitationsData.First(l => l.AwardYear == awardYear.Code)
                                                : new StudentLoanLimitation() { AwardYear = awardYear.Code, StudentId = studentAward.StudentId, SubsidizedMaximumAmount = 0, UnsubsidizedMaximumAmount = 0 };

                    //Finally, add the completed studentAwardModel to the StudentAwards, StudentSubLoans or StudentUnsubLoans attribute
                    if (studentAwardModel.LoanType == LoanType.SubsidizedLoan)
                    {
                        AddStudentAwardModelToLoanCollection(StudentSubLoans, studentAwardModel, loanLimitsForYear);
                    }
                    else if (studentAwardModel.LoanType == LoanType.UnsubsidizedLoan)
                    {
                        AddStudentAwardModelToLoanCollection(StudentUnsubLoans, studentAwardModel, loanLimitsForYear);
                    }
                    else if (studentAwardModel.LoanType == LoanType.GraduatePlusLoan)
                    {
                        //Grad plus loans go into the GradPlusLoans StudentAwardCollection object
                        AddStudentAwardModelToLoanCollection(StudentGradPlusLoans, studentAwardModel, loanLimitsForYear);
                    }
                    else if (studentAwardModel.LoanType == LoanType.OtherLoan)
                    {
                        AddStudentAwardModelToStudentAwardCollection(OtherLoans, studentAwardModel, award);
                    }
                    else
                    {
                        AddStudentAwardModelToStudentAwardCollection(StudentAwards, studentAwardModel, award);
                    }
                }
            }
            #endregion

            foreach (var year in awardYears)
            {
                //Sort the periods by start date in awardYears
                //year.DistinctAwardPeriods.Sort((dap1, dap2) => DateTime.Compare(dap1.StartDate, dap2.StartDate));

                AddEmptyPeriodsToAwardCollection(StudentAwards, year, awardStatusesData);
                AddEmptyPeriodsToLoanCollection(StudentGradPlusLoans, year, awardStatusesData);
                AddEmptyPeriodsToAwardCollection(OtherLoans, year, awardStatusesData);
                AddEmptyPeriodsToLoanCollection(StudentSubLoans, year, awardStatusesData);
                AddEmptyPeriodsToLoanCollection(StudentUnsubLoans, year, awardStatusesData);
            }

            //Create charts for all available award categories(award, work award, and loan) for each year
            foreach (var chartCollection in AwardPercentageCharts)
            {
                decimal studentAwardsTotal = 0, studentWorkAwardsTotal = 0, studentLoansTotal = 0;
                var studentAwardsForYear = StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == chartCollection.AwardYearCode);
                if (studentAwardsForYear != null)
                {
                    studentAwardsTotal = studentAwardsForYear.StudentAwardsForYearTotalAmount;
                    studentWorkAwardsTotal = studentAwardsForYear.StudentWorkAwardsForYearTotalAmount;
                }
                var studentSubLoansForYear = StudentSubLoans.FirstOrDefault(ssl => ssl.AwardYearCode == chartCollection.AwardYearCode);
                var studentSubLoansTotal = studentSubLoansForYear != null ? studentSubLoansForYear.LoansTotalAmount : 0;

                var studentUnsubLoansForYear = StudentUnsubLoans.FirstOrDefault(sul => sul.AwardYearCode == chartCollection.AwardYearCode);
                var studentUnsubLoansTotal = studentUnsubLoansForYear != null ? studentUnsubLoansForYear.LoansTotalAmount : 0;

                var studentGradPlusLoansForYear = StudentGradPlusLoans.FirstOrDefault(sgpl => sgpl.AwardYearCode == chartCollection.AwardYearCode);
                var studentGradPlusLoansTotal = studentGradPlusLoansForYear != null ? studentGradPlusLoansForYear.LoansTotalAmount : 0;

                var studentOtherLoansForYear = OtherLoans.FirstOrDefault(ol => ol.AwardYearCode == chartCollection.AwardYearCode);
                var studentOtherLoansTotal = studentOtherLoansForYear != null ? studentOtherLoansForYear.StudentAwardsForYearTotalAmount : 0;

                studentLoansTotal = studentSubLoansTotal + studentUnsubLoansTotal + studentGradPlusLoansTotal + studentOtherLoansTotal;
                try
                {
                    chartCollection.CreatePercentageCharts(studentAwardsTotal, studentWorkAwardsTotal, studentLoansTotal);
                }
                catch (Exception) { }
            }

            //Assign awardYears to the IENumerable AwardYears, sort by descending (already sorted before looping through all active years)
            AwardYears = awardYears;                
        }

        /// <summary>
        /// Format award explanation: adds target _blank for any links within 
        /// the explanation to open in a new tab
        /// </summary>
        /// <param name="explanation">award explanation</param>
        /// <returns>formatted award explanation</returns>
        private string FormatAwardExplanation(string explanation)
        {
            string formattedExplanation = explanation;
            string stringToInsert = " target=_blank";
            int insertedStringLength = 0;

            //If the explanation exists and it contains href, add target _blank to the link
            if (!string.IsNullOrEmpty(explanation) && explanation.Contains("<a href="))
            {
                var urls = Regex.Matches(explanation, @"href=[""']([^""'])+[""']");
                foreach (Match url in urls)
                {
                    foreach (Capture capture in url.Captures)
                    {
                        int endOfUrlIndex = capture.Index + capture.Length + insertedStringLength;
                        explanation = explanation.Substring(0, endOfUrlIndex) + stringToInsert + explanation.Substring(endOfUrlIndex);
                        insertedStringLength += stringToInsert.Length;
                    }
                }                
            }
            return explanation;
        }

        /// <summary>
        /// Returns AwardCategoryType to be assigned to studentAwardModel awardCategory property
        /// </summary>
        /// <param name="award">award that we get awardCategoryType from</param>
        /// <returns>AwardCategory type </returns>
        private AwardCategoryType AssignAwardCategoryType(Award2 award)
        {
            var awardCategoryType = award.AwardCategoryType;
            switch (awardCategoryType)
            {
                case Colleague.Dtos.FinancialAid.AwardCategoryType.Grant:
                    return AwardCategoryType.Award;
                case Colleague.Dtos.FinancialAid.AwardCategoryType.Scholarship:
                    return AwardCategoryType.Award;
                case Colleague.Dtos.FinancialAid.AwardCategoryType.Loan:
                    return AwardCategoryType.Loan;
                case Colleague.Dtos.FinancialAid.AwardCategoryType.Work:
                    return AwardCategoryType.Work;
                default:
                    if (award.LoanType == null)
                    {
                        return AwardCategoryType.Award;
                    }
                    else return AwardCategoryType.Loan;
            }
        }

        /// <summary>
        /// Calculates studentAward's status description along with other applicable properties
        /// </summary>
        /// <param name="awardPackageChangeRequest">award package change request</param>
        /// <param name="awardStatusesData">award statuses data</param>
        /// <param name="studentAwardModel">student award model to make changes to</param>
        private void CalculateChangeRequestRelatedProperties(AwardPackageChangeRequest awardPackageChangeRequest, StudentAward studentAwardModel, IEnumerable<Colleague.Dtos.FinancialAid.AwardStatus> awardStatusesData)
        {
            //Return "In Review" status description if there is a pending change request
            if (awardPackageChangeRequest != null && (awardPackageChangeRequest.AwardPeriodChangeRequests.Any(apcr => apcr.Status == AwardPackageChangeRequestStatus.Pending)))
            {
                //Get awardStatuses that correspond to decline action
                var declineAwardStatuses = awardStatusesData.Where(aws => aws.Category == AwardStatusCategory.Denied || aws.Category == AwardStatusCategory.Rejected);

                //This is already assigned, so reassign it
                studentAwardModel.IsStatusModifiable = false;
                studentAwardModel.IsAwardInReview = true;

                foreach (var period in studentAwardModel.StudentAwardPeriods)
                {
                    period.IsAmountModifiable = false;
                    period.IsStatusModifiable = false;

                    //Get corresponding award period change request if any
                    var correspondingAwardPeriodChangeRequest = awardPackageChangeRequest.AwardPeriodChangeRequests.FirstOrDefault(apcr => apcr.AwardPeriodId == period.Code);
                    if (correspondingAwardPeriodChangeRequest != null)
                    {
                        if (correspondingAwardPeriodChangeRequest.NewAmount.HasValue)
                        {
                            period.AwardAmount = correspondingAwardPeriodChangeRequest.NewAmount.Value;
                        }
                        if (correspondingAwardPeriodChangeRequest.NewAwardStatusId != null && declineAwardStatuses.Any(daws => daws.Code.ToUpper() == correspondingAwardPeriodChangeRequest.NewAwardStatusId.ToUpper()))
                        {
                            period.AwardAmount = 0;
                        }
                    }

                }

                studentAwardModel.StatusDescription = GlobalResources.GetString(FinancialAidResourceFiles.MyAwardsResources, "InReviewStatusDescription");
            }
            //Otherwise, statusDescription equals AwardStatusDto description property
            else
            {
                studentAwardModel.StatusDescription = studentAwardModel.AwardStatus.Description;
            }

        }

        /// <summary>
        /// Adds a student award to the appropriate loan collection. Creates a new loan collection 
        /// if none exists of type for the year
        /// </summary>
        /// <param name="loanCollections">set of loan collections(sub, unsub, grad plus)</param>
        /// <param name="studentAwardModelToAdd">award to add</param>
        /// <param name="loanLimitsForYear">loan limitations for the year(max limits)</param>        
        private void AddStudentAwardModelToLoanCollection(List<LoanCollection> loanCollections, StudentAward studentAwardModelToAdd, StudentLoanLimitation loanLimitsForYear)
        {
            //if the year is not yet in the list of loan collections
            if (!loanCollections.Any(lc => lc.AwardYearCode == studentAwardModelToAdd.AwardYearId))
            {
                //Create temporary storage for period description/ isActive values pairs for each period
                var activeLoanPeriods = new List<LoanPeriod>();
                foreach (var period in studentAwardModelToAdd.StudentAwardPeriods)
                {
                    //TODO: Olga - create a constructor not to reuse this block again later
                    activeLoanPeriods.Add(new LoanPeriod()
                    {
                        Code = period.Code,
                        Description = period.Description,
                        StartDate = period.StartDate,
                        IsActive = period.IsActive,
                        IsStatusModifiable = period.IsStatusModifiable

                    });
                }

                var loanType = studentAwardModelToAdd.LoanType;
                //TODO: Olga - create a constructor not to reuse this block again later - see if would be possible/better?
                loanCollections.Add(new LoanCollection()
                {
                    AwardYearCode = studentAwardModelToAdd.AwardYearId,
                    StudentId = studentAwardModelToAdd.StudentId,
                    IsStatusModifiable = studentAwardModelToAdd.IsStatusModifiable,
                    MaximumAmount = loanType == LoanType.GraduatePlusLoan ? loanLimitsForYear.GradPlusMaximumAmount :
                        loanType == LoanType.SubsidizedLoan ? loanLimitsForYear.SubsidizedMaximumAmount : loanLimitsForYear.UnsubsidizedMaximumAmount,
                    Loans = new List<StudentAward>() { studentAwardModelToAdd },
                    //List of period descriptions with corresponding IsActive values
                    LoanPeriods = activeLoanPeriods,
                    IsLoanCollectionInReview = studentAwardModelToAdd.IsAwardInReview
                });
            }
            else
            {
                var loanCollection = loanCollections.FirstOrDefault(lc => lc.AwardYearCode == studentAwardModelToAdd.AwardYearId);

                foreach (var period in studentAwardModelToAdd.StudentAwardPeriods)
                {
                    var loanPeriod = loanCollection.LoanPeriods.FirstOrDefault(lp => lp.Code == period.Code);
                    if (loanPeriod != null)
                    {
                        //Make sure if the period IsActive then the value of corresponding period in ActiveLoanPeriods is also active                 
                        if (period.IsActive)
                        {
                            loanPeriod.IsActive = true;
                        }

                        //If at least one of the loan's for the period have IsStatusModifiable set to true, 
                        //the whole award period should be assigned the true value
                        if (period.IsStatusModifiable && !loanPeriod.IsStatusModifiable)
                        {
                            loanPeriod.IsStatusModifiable = period.IsStatusModifiable;
                        }
                    }
                    //Create a new loan period
                    else
                    {
                        loanCollection.LoanPeriods.Add(new LoanPeriod()
                        {
                            Code = period.Code,
                            Description = period.Description,
                            StartDate = period.StartDate,
                            IsActive = period.IsActive,
                            IsStatusModifiable = period.IsStatusModifiable
                        });
                    }
                }

                //Set the flag IsStatusModifiable for the whole loan collection to true if at least one of the loans
                //has that flag set to true
                if (!loanCollection.IsStatusModifiable && studentAwardModelToAdd.IsStatusModifiable)
                {
                    loanCollection.IsStatusModifiable = studentAwardModelToAdd.IsStatusModifiable;
                }
                //Set the flag IsLoanCollectionInReview to true if at least one of the loans has IsAwardInReview
                //flag set to true
                if (!loanCollection.IsLoanCollectionInReview && studentAwardModelToAdd.IsAwardInReview)
                {
                    loanCollection.IsLoanCollectionInReview = studentAwardModelToAdd.IsAwardInReview;
                }
                loanCollection.Loans.Add(studentAwardModelToAdd);
            }
        }

        private void AddStudentAwardModelToStudentAwardCollection(List<StudentAwardCollection> studentAwardCollections, StudentAward studentAwardModelToAdd, Award2 award)
        {
            //Check if the studentAwardCollection has the studentAwardModelToAdd's year, create new if necessary
            if (!studentAwardCollections.Any(sa => sa.AwardYearCode == studentAwardModelToAdd.AwardYearId))
            {
                var newStudentAwardCollection = new StudentAwardCollection()
                {
                    AwardYearCode = studentAwardModelToAdd.AwardYearId,
                    StudentAwardsForYear = new List<StudentAward>(),
                    StudentWorkAwardsForYear = new List<StudentAward>()
                };

                AddStudentAwardModel(studentAwardModelToAdd, award, newStudentAwardCollection);
                studentAwardCollections.Add(newStudentAwardCollection);
            }
            else
            {
                var studentAwardCollection = studentAwardCollections.First(sa => sa.AwardYearCode == studentAwardModelToAdd.AwardYearId);
                AddStudentAwardModel(studentAwardModelToAdd, award, studentAwardCollection);
            }
        }

        /// <summary>
        /// Adds studentAwardModel to either StudentAwardsForYear or StudentFWSAwardsForYear
        /// list depending on the category of the associated award
        /// </summary>
        /// <param name="studentAwardModelToAdd">studentAwardModel to add</param>
        /// <param name="award">associated award</param>
        /// <param name="studentAwardCollection">studentAwardCollection that studentAwardModel becomes part of</param>
        private void AddStudentAwardModel(StudentAward studentAwardModelToAdd, Award2 award, StudentAwardCollection studentAwardCollection)
        {
            if (award.AwardCategoryType == Colleague.Dtos.FinancialAid.AwardCategoryType.Work)
            {
                studentAwardCollection.StudentWorkAwardsForYear.Add(studentAwardModelToAdd);
            }
            else
            {
                studentAwardCollection.StudentAwardsForYear.Add(studentAwardModelToAdd);
            }
        }

        private void AddEmptyPeriodsToLoanCollection(List<LoanCollection> loanCollectionList, AwardYear year, IEnumerable<AwardStatus> awardStatusesData)
        {
            if (loanCollectionList.Count() != 0)
            {
                var loansForYear = loanCollectionList.FirstOrDefault(lc => lc.AwardYearCode == year.Code);
                if (loansForYear != null)
                {
                    var activeLoanPeriods = new List<LoanPeriod>();
                    var dummyAwardStatus = new AwardStatus() { Code = "R", Description = "Rejected", Category = AwardStatusCategory.Rejected };

                    //Add "empty" awardPeriods to all unsubLoans and resort them
                    foreach (var loan in loansForYear.Loans)
                    {
                        var emptyPeriods = year.DistinctAwardPeriods.Where(dap => !loan.StudentAwardPeriods.Any(sap => sap.Code == dap.Code));
                        if (emptyPeriods.Count() != 0)
                        {
                            foreach (var period in emptyPeriods)
                            {
                                loan.StudentAwardPeriods.Add(StudentAwardPeriod.CreateDummy(loan.Code, year.Code, period, dummyAwardStatus));

                                activeLoanPeriods.Add(new LoanPeriod()
                                {
                                    Code = period.Code,
                                    Description = period.Description,
                                    StartDate = period.StartDate,
                                    IsActive = false

                                });
                            }
                        }
                        loan.StudentAwardPeriods.Sort((dap1, dap2) => DateTime.Compare(dap1.StartDate, dap2.StartDate));
                    }
                    //We need to add distinct collective periods to LoanPeriods attr of each LoanCollection as well
                    foreach (var activePeriod in activeLoanPeriods)
                    {
                        if (!loansForYear.LoanPeriods.Any(lp => lp.Code == activePeriod.Code))
                        {
                            loansForYear.LoanPeriods.Add(activePeriod);
                        }
                    }
                    loansForYear.LoanPeriods.Sort((lp1, lp2) => DateTime.Compare(lp1.StartDate, lp2.StartDate));
                }
            }
        }

        private void AddEmptyPeriodsToAwardCollection(List<StudentAwardCollection> studentAwardCollectionList, AwardYear year, IEnumerable<AwardStatus> awardStatusesData)
        {
            if (studentAwardCollectionList.Count() > 0) {

                var awardCollectionForYear = studentAwardCollectionList.FirstOrDefault(ol => ol.AwardYearCode == year.Code);

                if (awardCollectionForYear != null)
                {
                    var dummyAwardStatus = new AwardStatus() { Code = "R", Description = "Rejected", Category = AwardStatusCategory.Rejected };
                    
                    //Add "Empty award periods to all awards in the collection and re-sort them
                    foreach (var award in awardCollectionForYear.StudentAwardsForYear)
                    {
                        AddEmptyPeriodsToAward(year, dummyAwardStatus, award);
                    }
                    //Add dummy periods to all work study awards
                    foreach (var award in awardCollectionForYear.StudentWorkAwardsForYear)
                    {
                        AddEmptyPeriodsToAward(year, dummyAwardStatus, award);
                    }
                }
            }
        }

        /// <summary>
        /// Adds empty award periods to an award and resorts them
        /// </summary>
        /// <param name="year">award year the award belongs to</param>
        /// <param name="dummyAwardStatus">award status for the dummy award period</param>
        /// <param name="award">award that award periods are added to</param>
        private static void AddEmptyPeriodsToAward(AwardYear year, AwardStatus dummyAwardStatus, StudentAward award)
        {
            var emptyPeriods = year.DistinctAwardPeriods.Where(dap => !award.StudentAwardPeriods.Any(sap => sap.Code == dap.Code));
            if (emptyPeriods.Any())
            {
                foreach (var period in emptyPeriods)
                {
                    award.StudentAwardPeriods.Add(StudentAwardPeriod.CreateDummy(award.Code, year.Code, period, dummyAwardStatus));
                }
            }
            award.StudentAwardPeriods.Sort((dap1, dap2) => DateTime.Compare(dap1.StartDate, dap2.StartDate));
        }

        /// <summary>
        /// Add dummy studentAwardPeriods to each of the awards as well as loan collections if those were updated
        /// </summary>
        /// <param name="dummyStudentAwardPeriods">dummy student award periods to add</param>
        public void AddDummyStudentAwardPeriods(IEnumerable<StudentAwardPeriod> dummyStudentAwardPeriods)
        {
            var allLoanCollections = GetAllStudentLoanCollections();
            foreach (var dummyStudentAwardPeriod in dummyStudentAwardPeriods)
            {
                var year = AwardYears.FirstOrDefault(y => y.Code == dummyStudentAwardPeriod.AwardYearId);
                if (year != null)
                {
                    var dummyAwardPeriod = new AwardPeriod() { Code = dummyStudentAwardPeriod.Code, Description = dummyStudentAwardPeriod.Description, StartDate = dummyStudentAwardPeriod.StartDate };
                    var dummyAwardStatus = new AwardStatus() { Code = "R", Description = "Rejected", Category = AwardStatusCategory.Rejected };
                    year.AddDistinctAwardPeriod(dummyAwardPeriod);

                    var studentAward = GetAllStudentAwards().FirstOrDefault(a => a.Code == dummyStudentAwardPeriod.AwardId);
                    if (studentAward != null)
                    {
                        if (studentAward.StudentAwardPeriods.FirstOrDefault(p => p.Code == dummyStudentAwardPeriod.Code) == null)
                        {
                            studentAward.StudentAwardPeriods.Add(StudentAwardPeriod.CreateDummy(studentAward.Code, year.Code, dummyAwardPeriod, dummyAwardStatus));
                        }

                        studentAward.StudentAwardPeriods.Sort((dap1, dap2) => DateTime.Compare(dap1.StartDate, dap2.StartDate));
                    }

                    //Get all loan collections for the year and add LoanPeriods to ones that were updated (count > 0)
                    var loanCollectionsForYear = allLoanCollections.FindAll(slc => slc.AwardYearCode == year.Code && slc.Loans.Count() != 0);
                    foreach (var loanCollection in loanCollectionsForYear)
                    {
                        if (!loanCollection.LoanPeriods.Any(lp => lp.Code == dummyAwardPeriod.Code))
                        {
                            loanCollection.LoanPeriods.Add(new LoanPeriod()
                            {
                                Code = dummyAwardPeriod.Code,
                                Description = dummyAwardPeriod.Description,
                                IsActive = false,
                                IsStatusModifiable = false,
                                StartDate = dummyAwardPeriod.StartDate
                            });
                        }
                    }
                }
            }
            //Sort all loan periods of collections that were updated (count != 0)
            foreach (var loanCollection in allLoanCollections)
            {
                if (loanCollection.Loans.Count() != 0)
                {
                    loanCollection.LoanPeriods.Sort((lp1, lp2) => DateTime.Compare(lp1.StartDate, lp2.StartDate));
                }
            }
        }
    }
}
