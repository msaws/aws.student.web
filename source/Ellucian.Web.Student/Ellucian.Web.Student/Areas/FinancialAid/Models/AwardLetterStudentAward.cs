﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Represents a single award assigned to a student for a particular year extends
    /// StudentAward
    /// </summary>
    public class AwardLetterStudentAward : StudentAward
    {

        public decimal? TotalAwardAmount { get; set; }
        

        public AwardLetterStudentAward()
            : base()
        {

        }
    }
}