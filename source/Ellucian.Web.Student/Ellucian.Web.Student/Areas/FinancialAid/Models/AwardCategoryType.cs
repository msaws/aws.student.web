﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// AwardCategoryType enumeration
    /// </summary>
    public enum AwardCategoryType
    {
        /// <summary>
        /// All awards that are not loans or work awards
        /// fall into this category
        /// </summary>
        Award,
        /// <summary>
        /// All loans fall into this category
        /// </summary>
        Loan,
        /// <summary>
        /// Work awards fall into this category
        /// </summary>
        Work
    }
}