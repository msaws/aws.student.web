﻿//Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class LinkTypeComparer : IComparer<LinkTypes>
    {
        protected IList<LinkTypes> orderedTypes {get; set;}
        public LinkTypeComparer() {        
            orderedTypes = new List<LinkTypes>() 
            {
                LinkTypes.PROFILE,
                LinkTypes.FAFSA,
                LinkTypes.EntranceInterview,
                LinkTypes.MPN,
                LinkTypes.PLUS,
                LinkTypes.NSLDS,
                LinkTypes.Forecaster,
                LinkTypes.User
            };
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="typeA"></param>
        /// <param name="typeB"></param>
        /// <returns></returns>
        public int Compare(LinkTypes typeA, LinkTypes typeB)
        {
            var indexA = orderedTypes.IndexOf(typeA);
            var indexB = orderedTypes.IndexOf(typeB);

            return indexA.CompareTo(indexB);
        }
    }
}

