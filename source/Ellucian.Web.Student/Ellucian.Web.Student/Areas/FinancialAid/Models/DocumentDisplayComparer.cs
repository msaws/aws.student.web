﻿//Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class DocumentDisplayComparer : IComparer<StudentDocument>
    {
        //-1, x is less than y
        //0, x is the same as y
        // 1, x is greater than y
        //Overdue by asending date
        //Incomplete by ascending date
        //Complete by most recent date (descending)
        public int Compare(StudentDocument x, StudentDocument y)
        {
            if (x.StatusDisplay == "Overdue")
            {
                if (y.StatusDisplay == "Overdue")
                {
                    return x.Date.GetValueOrDefault().CompareTo(y.Date.GetValueOrDefault());
                }
                else
                {
                    return -1;
                }
            }
            else if (x.StatusDisplay == "Incomplete")
            {
                if (y.StatusDisplay == "Overdue")
                {
                    return 1;
                }
                else if (y.StatusDisplay == "Incomplete")
                {
                    if (x.Date.HasValue && !y.Date.HasValue)
                    {
                        return -1;
                    }
                    else if (!x.Date.HasValue && y.Date.HasValue)
                    {
                        return 1;
                    }
                    else
                    {
                        x.Date.GetValueOrDefault().CompareTo(y.Date.GetValueOrDefault());
                    }
                }
                else
                {
                    return -1;
                }
            }
            else if (y.StatusDisplay == "Overdue" || y.StatusDisplay == "Incomplete")
            {
                return 1;
            }

            return -x.Date.GetValueOrDefault().CompareTo(y.Date.GetValueOrDefault());
        }
    }
}
