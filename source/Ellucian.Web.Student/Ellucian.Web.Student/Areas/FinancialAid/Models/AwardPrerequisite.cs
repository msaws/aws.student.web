﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class AwardPrerequisite 
    {
        public string AwardYearCode { get; set; }
        public bool AreAllPrerequisitesSatisfied { get; set; }
    }
}
