﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class AwardLetterHistoryItem
    {
        public string AwardLetterRecordId { get; set; }

        public DateTimeOffset CreatedDate { get; set; }
    }
}
