﻿/*Copyright 2014-2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class LoanDetails
    {
        public string SchoolName { get; set; }
        public int TotalLoanAmount { get; set; }

        public LoanDetails(Colleague.Dtos.FinancialAid.StudentLoanHistory studentLoanHistoryDto, Colleague.Dtos.FinancialAid.IpedsInstitution ipedsInstitutionDto)
        {
            if (studentLoanHistoryDto == null)
            {
                throw new ArgumentNullException("studentLoanHistoryDto");
            }
            SchoolName = (ipedsInstitutionDto != null && !string.IsNullOrEmpty(ipedsInstitutionDto.Name)) ?
                ipedsInstitutionDto.Name :
                studentLoanHistoryDto.OpeId;

            TotalLoanAmount = studentLoanHistoryDto.TotalLoanAmount;
        }
    }

}
