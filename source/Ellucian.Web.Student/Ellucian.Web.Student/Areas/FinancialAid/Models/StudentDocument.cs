﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Student Document object
    /// </summary>
    public class StudentDocument
    {
        /// <summary>
        /// Document Code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Document Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Document Explanation
        /// </summary>
        public string Explanation { get; set; }
        /// <summary>
        /// Document Instance text
        /// </summary>
        public string Instance { get; set; }

        /// <summary>
        /// Document Date (can be due date or status date)
        /// </summary>
        public DateTimeOffset? Date;
        //Flag indicating whether the Date property is the due date or not
        public bool IsDueDate;

        /// <summary>
        /// Document Status: Waived, Received, Incomplete
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public Colleague.Dtos.FinancialAid.DocumentStatus Status { get; set; }
        /// <summary>
        /// Document Status Description to display
        /// </summary>
        public string StatusDisplay { get; set; }
        /// <summary>
        /// List of hyperlinks for the document
        /// </summary>
        public List<Colleague.Dtos.Base.CommunicationCodeHyperlink> Hyperlinks { get; set; }

        /// <summary>
        /// Flag indicating whether the document is outstanding(incomplete)
        /// </summary>
        public bool IsOutstanding
        {
            get
            {
                return (Status == Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete);
            }
        }

        /// <summary>
        /// StudentDocument constructor
        /// </summary>
        /// <param name="studentDocumentDto">StudentDocument DTO, required</param>
        /// <param name="communicationCodeDtoList">List of CommunicationCode DTOs, required</param>
        /// <param name="suppressInstance">If true, the Instance attribute of the StudentDocument will be always be empty. If false, the Instance attribute could have data</param>
        public StudentDocument(Colleague.Dtos.FinancialAid.StudentDocument studentDocumentDto,
            IEnumerable<Colleague.Dtos.Base.CommunicationCode2> communicationCodeDtoList,
            bool suppressInstance, bool useDocumentStatusDescription)
        {
            if (studentDocumentDto == null) throw new ArgumentNullException("studentDocumentDto");
            if (communicationCodeDtoList == null) throw new ArgumentNullException("communicationCodeDtoList");

            var commCodeDto = communicationCodeDtoList.First(c => c.Code == studentDocumentDto.Code);

            Code = studentDocumentDto.Code;
            Description = commCodeDto.Description;
            Explanation = FormatAndAssignDocumentExplanation(commCodeDto.Explanation);  
            Instance = (!suppressInstance) ? studentDocumentDto.Instance : string.Empty;

            Status = studentDocumentDto.Status;
            StatusDisplay = CalculateStatusDisplay(studentDocumentDto, useDocumentStatusDescription);

            Hyperlinks = commCodeDto.Hyperlinks == null ? new List<Colleague.Dtos.Base.CommunicationCodeHyperlink>() :
                commCodeDto.Hyperlinks.ToList();

            if (studentDocumentDto.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Received ||
                studentDocumentDto.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Waived)
            {
                Date = studentDocumentDto.StatusDate;
                IsDueDate = false;
            }
            else
            {
                Date = studentDocumentDto.DueDate;
                IsDueDate = true;
            }
        }

        /// <summary>
        /// Helper method to format explanation text with hard returns
        /// </summary>
        /// <param name="explanation">explanation string</param>
        /// <returns>formatted explanation string</returns>
        private string FormatAndAssignDocumentExplanation(string explanation)
        {
            if (string.IsNullOrEmpty(explanation)) { return string.Empty; }
            else
            {
                return explanation.Replace(Environment.NewLine, "<br />")                                  
                                  .Replace("\r", "<br />")
                                  .Replace("\n", "<br />");
            }
        }

        /// <summary>
        /// Calculates the document status to display to web user
        /// </summary>
        /// <param name="studentDocumentDto">current document dto</param>
        /// <param name="useDocumentStatusDescription">flag indicating whether to use status description for display to web user</param>
        /// <returns></returns>
        private static string CalculateStatusDisplay(Colleague.Dtos.FinancialAid.StudentDocument studentDocumentDto, bool useDocumentStatusDescription)
        {
            string statusDescription;
            //Document status is Incomplete and we are past the due date - assign "Overdue" status description
            if(studentDocumentDto.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete 
                && studentDocumentDto.DueDate.HasValue 
                && studentDocumentDto.DueDate.Value < DateTime.Now)
            {
                statusDescription = "Overdue";
            }
            //Flag to use status descriptions is set to "yes" and description is not empty - use the description
            else if (useDocumentStatusDescription && !string.IsNullOrEmpty(studentDocumentDto.StatusDescription))
            {
                statusDescription = studentDocumentDto.StatusDescription;
            }
            //return status value
            else
            {
                statusDescription = studentDocumentDto.Status.ToString();
            }                                    
            return statusDescription;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var studentDocument = obj as StudentDocument;

            if (studentDocument.Code == this.Code &&
                studentDocument.Instance == this.Instance &&
                studentDocument.Date == this.Date &&
                studentDocument.Status == this.Status)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode() ^ (string.IsNullOrEmpty(Instance) ? 0 : Instance.GetHashCode()) 
                ^ (Date == null ? 0 : Date.GetHashCode()) ^ Status.GetHashCode();
        }
    }
}
