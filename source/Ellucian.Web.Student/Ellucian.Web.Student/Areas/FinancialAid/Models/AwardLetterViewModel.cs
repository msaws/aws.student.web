﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Colleague.Dtos.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// AwardLetterViewModel class containing information on
    /// student award letter, award letter history items for selected year
    /// as well as student and active student award years
    /// </summary>
    public class AwardLetterViewModel : FinancialAidAdminModel
    {
        /// <summary>
        /// Student object
        /// </summary>
        public FAStudent Student { get; set; }

        /// <summary>
        /// Student award letter for the selected year
        /// </summary>
        public StudentAwardLetter StudentAwardLetter { get; set; }

        /// <summary>
        /// Control status of the award package checklist item for
        /// the selected year
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ChecklistItemControlStatus? AwardPackageChecklistItemControlStatus { get; set; }

        /// <summary>
        /// List of all active student award years
        /// </summary>
        public List<AwardYear> AwardYears { get; set; }

        /// <summary>
        /// List of award letter history records for the selected year
        /// </summary>
        public List<AwardLetterHistoryItem> AwardLetterHistoryItems { get; set; }

        /// <summary>
        /// Indicates whether the current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }

        /// <summary>
        /// AwardLetterViewModel constructor
        /// </summary>
        /// <param name="studentAwardYearsData">set of student award year DTOs</param>
        /// <param name="studentApplicationData">set of student application DTOs includes fafsa and profile</param>
        /// <param name="studentDocumentsData">set of student document DTOs</param>
        /// <param name="communicationCodesData">set of communication code DTOs</param>
        /// <param name="officeCodesData">set of office code DTOs</param>
        /// <param name="personData">person DTO</param>
        /// <param name="financialAidCounselorData">set of financial aid counselor DTOs</param>
        /// <param name="financialAidOfficesData">set of financial aid office DTOs</param>
        /// <param name="studentChecklistsData">set of student checklist DTOs</param>
        /// <param name="financialAidChecklistsData">set of reference financial aid checklist DTOs</param>
        /// <param name="currentUser">current user</param>
        public AwardLetterViewModel(IEnumerable<StudentAwardYear2> studentAwardYearsData,
            IEnumerable<FinancialAidApplication2> studentApplicationData, IEnumerable<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentsData,
            IEnumerable<CommunicationCode2> communicationCodesData, IEnumerable<OfficeCode> officeCodesData,
            Person personData, FinancialAidCounselor financialAidCounselorData,
            IEnumerable<FinancialAidOffice3> financialAidOfficesData, IEnumerable<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist> studentChecklistsData,
            IEnumerable<Colleague.Dtos.FinancialAid.ChecklistItem> financialAidChecklistsData,
            ICurrentUser currentUser)
        {

            #region Student
            if (personData == null) { personData = new Ellucian.Colleague.Dtos.Student.Student(); }

            Student = new FAStudent()
            {
                Id = personData.Id,
                FirstName = personData.FirstName,
                LastName = personData.LastName,
                MiddleName = personData.MiddleName,
                PreferredAddress = personData.PreferredAddress,
                PreferredName = personData.PreferredName,
                FullName = personData.FirstName + ' ' + personData.MiddleName + ' ' + personData.LastName
            };
            #endregion

            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();

            var awardYears = new List<AwardYear>();
            AwardLetterHistoryItems = new List<AwardLetterHistoryItem>();

            if (financialAidOfficesData == null)
            {
                financialAidOfficesData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }
            var configurationDtos = financialAidOfficesData.SelectMany(o => o.Configurations).ToList();

            foreach (var activeStudentAwardYearDto in studentAwardYearsData.GetActiveStudentAwardYearDtos(configurationDtos))
            {
                //Get the office configuration for the year
                var configurationForTheYear = configurationDtos.GetConfigurationDtoOrDefault(activeStudentAwardYearDto);


                //Get checklist for the year
                var studentChecklistForYear = studentChecklistsData.FirstOrDefault(sc => sc.AwardYear == activeStudentAwardYearDto.Code);
                bool areChecklistItemsAssigned = (studentChecklistForYear != null && studentChecklistForYear.ChecklistItems != null && studentChecklistForYear.ChecklistItems.Count != 0);

                #region awardYearModel
                var awardYearModel = new AwardYear(activeStudentAwardYearDto, () =>
                {
                    if (configurationForTheYear.IsAwardLetterActive == false)
                    {
                        return false;
                    }

                    if (areChecklistItemsAssigned && financialAidChecklistsData != null)
                    {
                        //check prerequisites for viewing the award letter - applications and documents must be complete
                        var studentDocumentsCollection = new StudentDocumentCollection(activeStudentAwardYearDto, studentDocumentsData, communicationCodesData, officeCodesData, configurationForTheYear);

                        var documentTypeChecklistItem = financialAidChecklistsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.CompletedDocuments);
                        var documentChecklistItem = documentTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == documentTypeChecklistItem.ChecklistItemCode)
                            : null;
                        var isDocumentsCompletionRequired = documentChecklistItem != null && documentChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired;

                        if (studentDocumentsCollection.HasOutstandingDocuments && isDocumentsCompletionRequired)
                        {
                            return false;
                        }

                        var applicationsForYear = studentApplicationData.Where(fa => fa.AwardYear == activeStudentAwardYearDto.Code);

                        //Get checklist items to check control statuses
                        var profileTypeChecklistItem = financialAidChecklistsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.PROFILE);
                        var profileChecklistItem = profileTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == profileTypeChecklistItem.ChecklistItemCode)
                            : null;

                        var fafsaTypeChecklistItem = financialAidChecklistsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.FAFSA);
                        var fafsaChecklistItem = fafsaTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == fafsaTypeChecklistItem.ChecklistItemCode)
                            : null;

                        var applicationReviewTypeChecklistItem = financialAidChecklistsData.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.ApplicationReview);
                        var applicationReviewedChecklistItem = applicationReviewTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() ==
                            applicationReviewTypeChecklistItem.ChecklistItemCode) : null;

                        var isProfileCompletionRequired = (configurationForTheYear != null && configurationForTheYear.IsProfileActive) ?
                                (profileChecklistItem != null && profileChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired) :
                                false;

                        var isFafsaCompletionRequired = fafsaChecklistItem != null && fafsaChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired;
                        var isApplicationReviewCompletionRequired = applicationReviewedChecklistItem != null && applicationReviewedChecklistItem.ControlStatus == ChecklistItemControlStatus.CompletionRequired;

                        return
                            (applicationsForYear.Any(app => app.GetType() == typeof(ProfileApplication)) || !isProfileCompletionRequired) &&
                            (applicationsForYear.Any(app => app.GetType() == typeof(Fafsa)) || !isFafsaCompletionRequired) &&
                            (activeStudentAwardYearDto.IsApplicationReviewed || !isApplicationReviewCompletionRequired);
                    }
                    else
                    {
                        return false;
                    }

                }, areChecklistItemsAssigned);
                #endregion

                #region Counselor
                try
                {
                    awardYearModel.Counselor = new Counselor(financialAidCounselorData, null, activeStudentAwardYearDto, financialAidOfficesData);
                }
                catch (Exception) {/*Don't care if a counselor isn't created */}
                #endregion

                awardYears.Add(awardYearModel);
            }

            AwardYears = awardYears.OrderByDescending(ay => ay.Code).ToList();
        }

        /// <summary>
        /// Updates view model with received data for award letter, award letter history records, 
        /// award package control status
        /// </summary>
        /// <param name="awardStatusDtos">set of award status DTOs</param>
        /// <param name="awardLetterConfigurationDtos">set of award letter configuration DTOs</param>
        /// <param name="financialAidOfficeDtos">set of financial aid office DTOs</param>
        /// <param name="financialAidChecklistDtos">set of financial aid checklist DTOs</param>        
        /// <param name="studentAwardLetterDto">student award letter DTO</param>
        /// <param name="studentAwardDtos">set of student award DTOs for the selected year</param>
        /// <param name="studentAwardYearDto">student award year DTO for the selected year</param>
        /// <param name="studentChecklistDtos">set of student checklist DTOs</param>
        internal void UpdateViewModel(IEnumerable<AwardStatus> awardStatusDtos, List<AwardLetterConfiguration> awardLetterConfigurationDtos,
            IEnumerable<FinancialAidOffice3> financialAidOfficeDtos, IEnumerable<Colleague.Dtos.FinancialAid.ChecklistItem> financialAidChecklistDtos,
            AwardLetter2 studentAwardLetterDto, IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> studentAwardDtos,
            StudentAwardYear2 studentAwardYearDto, IEnumerable<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist> studentChecklistDtos)
        {

            if (financialAidOfficeDtos == null)
            {
                financialAidOfficeDtos = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            }
            var configurationDtos = financialAidOfficeDtos.SelectMany(o => o.Configurations).ToList();
            var configurationForTheYear = configurationDtos.GetConfigurationDtoOrDefault(studentAwardYearDto);

            StudentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationDtos, configurationForTheYear);

            #region AwardLetterHistoryItems            

            //We are updating so remove all award letter history items if any
            if (AwardLetterHistoryItems.Any())
            {
                AwardLetterHistoryItems.Clear();
            }
            GetAwardLetterHistoryItems(studentAwardYearDto, configurationForTheYear, studentAwardLetterDto);

            #endregion

            #region AwardsPackageChecklistItemControlStatus

            //Get checklist for the year
            var studentChecklistForYear = studentChecklistDtos.FirstOrDefault(sc => sc.AwardYear == studentAwardYearDto.Code);
            bool areChecklistItemsAssigned = (studentChecklistForYear != null && studentChecklistForYear.ChecklistItems != null && studentChecklistForYear.ChecklistItems.Any());

            //Get awards checklist item and its control status
            if (areChecklistItemsAssigned && financialAidChecklistDtos != null)
            {
                var awardsPackageTypeChecklistItem = financialAidChecklistDtos.FirstOrDefault(ci => ci.ChecklistItemType == Colleague.Dtos.FinancialAid.ChecklistItemType.ReviewAwardPackage);
                var awardsChecklistItem = awardsPackageTypeChecklistItem != null ? studentChecklistForYear.ChecklistItems.FirstOrDefault(sci => sci.Code.ToUpper() == awardsPackageTypeChecklistItem.ChecklistItemCode) : null;

                AwardPackageChecklistItemControlStatus = (configurationForTheYear != null && configurationForTheYear.IsAwardingActive && awardsChecklistItem != null) ?
                    awardsChecklistItem.ControlStatus :
                    (ChecklistItemControlStatus?)null;
            }
            else
            {
                AwardPackageChecklistItemControlStatus = null;
            }
            #endregion
        }

        /// <summary>
        /// Helper method to build a list of award letter history items for the selected year
        /// </summary>
        /// <param name="activeStudentAwardYearDto">student award year DTO for the selected year</param>
        /// <param name="configurationForTheYear">financial aid configuration DTO for the selected year</param>
        private void GetAwardLetterHistoryItems(StudentAwardYear2 activeStudentAwardYearDto, FinancialAidConfiguration3 configurationForTheYear, AwardLetter2 studentAwardLetterDto)
        {
            if (activeStudentAwardYearDto.AwardLetterHistoryItemsForYear.Any() && configurationForTheYear != null && configurationForTheYear.IsAwardLetterHistoryActive)
            {
                //These come sorted in descending order by date and time from the API, exclude the current award letter record
                var historyItemsForYear = activeStudentAwardYearDto.AwardLetterHistoryItemsForYear
                    .Where(hi => hi.Id != studentAwardLetterDto.Id);
                AwardLetterHistoryItems.AddRange(
                        historyItemsForYear
                        .Select(item => new AwardLetterHistoryItem()
                        {
                            AwardLetterRecordId = item.Id,
                            CreatedDate = item.CreatedDate
                        }));

            }
        }
    }
}