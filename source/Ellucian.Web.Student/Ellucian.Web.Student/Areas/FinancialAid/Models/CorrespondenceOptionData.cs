﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Class that encapsulates the Corepondence option data
    /// on students file
    /// </summary>
    public class CorrespondenceOptionData 
    {
        /// <summary>
        /// A flag to indicate whether the correspondence option box is checked or not
        /// The 'meaning' of the check is specific to the school:
        /// while some may choose to specify electronic data delivery with a checked box,
        /// others may choose to indicate paper delivery
        /// </summary>
        public bool IsCorrespondenceOptionChecked { get; set; }

        /// <summary>
        /// The message to be displayed next to the correspondence option checkbox
        /// </summary>
        public string OptionMessage { get; set; }

        /// <summary>
        /// Award year code through which the paper copy option flag
        /// is going to be updated
        /// </summary>
        public string AwardYearCode { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CorrespondenceOptionData()
        {
            IsCorrespondenceOptionChecked = false;
            OptionMessage = string.Empty;
            AwardYearCode = string.Empty;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data">Correspondence option data</param>
        public CorrespondenceOptionData(Colleague.Dtos.FinancialAid.StudentAwardYear2 latestStudentAwardYearData, 
            Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOfficeData)
        {
            if (latestStudentAwardYearData == null)
            {
                throw new ArgumentNullException("latestStudentAwardYearData");
            }
            if (financialAidOfficeData == null)
            {
                throw new ArgumentNullException("financialAidOfficeData");
            }
            if (financialAidOfficeData.Configurations == null || financialAidOfficeData.Configurations.Count == 0)
            {
                throw new ApplicationException(string.Format("No configurations were found for {0} office", financialAidOfficeData.Id)); 
            }

            var currentConfiguration = financialAidOfficeData.Configurations.FirstOrDefault(c => c.AwardYear == latestStudentAwardYearData.Code);
            if (currentConfiguration == null)
            {
                throw new ApplicationException(string.Format("No current configuration was found for {0} office {1} year", financialAidOfficeData.Id, latestStudentAwardYearData.Code));
            }

            AwardYearCode = latestStudentAwardYearData.Code;
            IsCorrespondenceOptionChecked = latestStudentAwardYearData.IsPaperCopyOptionSelected;
            OptionMessage = !string.IsNullOrEmpty(currentConfiguration.PaperCopyOptionText) ? currentConfiguration.PaperCopyOptionText : GlobalResources.GetString(FinancialAidResourceFiles.CorrespondenceOptionResources, "ElectronicConsentCheckboxLabel");
        }
    }
}
