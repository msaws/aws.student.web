﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public static class AwardStatusEvaluator
    {
        /// <summary>
        /// Calculate an "annual" award status.
        /// If all of the award period's awardStatusCategories are equal, just return the first status.
        /// If they aren't all equal,
        ///     if any of the awardStatusCategories is Pending, return that first Pending AwardStatus
        ///     if none of the awardStatusCategories is Pending, then there is some combination of Accepted or Rejected - since
        ///     at least one of statuses is Accepted, return the Accepted AwardStatus
        /// </summary>
        /// <param name="periodsList">List of StudentAwardPeriods to evaluated</param>
        /// <returns>An AwardStatus that indicates if a student needs to take action on an award.</returns>
        public static AwardStatus CalculateAwardStatus(IEnumerable<StudentAwardPeriod> periodsList)
        {
            if (periodsList == null || periodsList.Count() == 0) return null;

            //Build a list of AwardStatus objects based on the Status Codes of the StudentAwardPeriods in periodsList
            var periodStatusList = periodsList.Where(p => p != null).Select(p => p.AwardStatus);

            return CalculateAwardStatus(periodStatusList);
        }

        /// <summary>
        /// Calculate an "annual" award status. If the parameter excludeIgnored is true, the award periods with ignored
        /// flags are excluded from evaluation
        /// </summary>
        /// <param name="periodsList">all student award periods for the year</param>
        /// <param name="allAwardStatuses">all reference award statuses</param>
        /// <param name="excludeIgnored">optional flag to exclude ignored student award period</param>
        /// <returns>Overall award status</returns>
        public static AwardStatus CalculateAwardStatus(IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardPeriod> periodsList, IEnumerable<Colleague.Dtos.FinancialAid.AwardStatus> allAwardStatuses, 
            bool excludeIgnored = false)
        {
            var periodStatusCodeList = excludeIgnored ? periodsList.Where(p => p != null && !p.IsIgnoredOnAwardLetter).Select(p => p.AwardStatusId)
                : periodsList.Where(p => p != null).Select(p => p.AwardStatusId);

            var awardStatuses = allAwardStatuses.Where(a => a != null && periodStatusCodeList.Contains(a.Code));

            return CalculateAwardStatus(awardStatuses);
        }

        /// <summary>
        /// if ALL award statuses have the same category - return the first status in the list
        /// if ALL statuses are rejected/denied - return the first rejected or denied status in the list
        /// if there is ANY Pending or Estimated category - return the first pending or estimated status in the list        
        /// else, there is some combination of accepted/rejected/denied statuses - return the first accepted status
        /// </summary>
        /// <param name="awardStatuses">List of award statuses with which to calculat a total Award Status</param>
        /// <returns>An Award Status that describes the state of all the status in the input argument</returns>
        private static AwardStatus CalculateAwardStatus(IEnumerable<AwardStatus> awardStatuses)
        {
            if (awardStatuses != null && awardStatuses.Any())
            {
                //if all the period's awardStatus Categories are equal, 
                //return the first award status
                var firstStatus = awardStatuses.First();
                if (awardStatuses.All(ps => ps.Category == firstStatus.Category))
                {
                    return firstStatus;
                }

                //if all of the period's statuses are rejected or denied, return a Rejected Status
                else if (awardStatuses.All(ps => ps.Category == AwardStatusCategory.Rejected || ps.Category == AwardStatusCategory.Denied))
                {
                    return awardStatuses.FirstOrDefault(ps => ps.Category == AwardStatusCategory.Rejected || ps.Category == AwardStatusCategory.Denied);
                }

                //if any of the period's awardStatusCategories are Pending or Estimated, return a Pending AwardStatus
                else if (awardStatuses.Any(ps => ps.Category == AwardStatusCategory.Pending || ps.Category == AwardStatusCategory.Estimated))
                {
                    return awardStatuses.FirstOrDefault(ps => ps.Category == AwardStatusCategory.Pending || ps.Category == AwardStatusCategory.Estimated);
                }

                else
                {
                    //at this point, there is some combination of Accepted and Rejected/Denied categories. Since at least
                    //one of the statuses is Accepted, return the Accepted AwardStatus
                    return awardStatuses.FirstOrDefault(ps => ps.Category == AwardStatusCategory.Accepted);
                }
            }
            else return null;
        }
    }
}