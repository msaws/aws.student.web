﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Student Document Collection object
    /// </summary>
    public class StudentDocumentCollection
    {
        public string AwardYearCode { get; set; }

        /// <summary>
        /// Documents is private so this object has a list of all documents, but we expose Incomplet and Complete Documents
        /// to the UI.
        /// </summary>
        private List<StudentDocument> Documents { get; set; }

        public List<StudentDocument> IncompleteDocuments
        {
            get
            {
                return Documents.Where(d => d.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete).OrderBy(d => d, new DocumentDisplayComparer()).ToList();
            }
        }

        public List<StudentDocument> CompleteDocuments
        {
            get
            {
                return Documents.Where(d =>
                    d.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Received ||
                    d.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Waived).OrderBy(d => d, new DocumentDisplayComparer()).ToList();
            }
        }

        public bool DocumentsExist
        {
            get
            {
                return Documents.Count() > 0;
            }
        }

        /// <summary>
        /// Helper method to get all the documents.
        /// Purposfully not a class attribute to avoid exposing all document data to UI
        /// </summary>
        /// <returns></returns>
        public List<StudentDocument> GetStudentDocuments()
        {
            return Documents;
        }

        public bool HasOutstandingDocuments
        {
            get
            {
                return Documents.Any(d => d.IsOutstanding);
            }
        }

        /// <summary>
        /// Constructor creates an empty StudentDocumentCollection
        /// </summary>
        public StudentDocumentCollection()
        {
            Documents = new List<StudentDocument>();
        }

        /// <summary>
        /// Constructor builds a StudentDocumentCollection for the given award year.
        /// It's easiest to pass in all of the StudentDocument DTOs, all of the communicationCode DTOs, and
        /// all of the OfficeCode DTOs
        /// </summary>
        /// <param name="studentAwardYearDto">A single StudentAwardYear2 DTO</param>
        /// <param name="studentDocumentDtoList">StudentDocument DTOs.</param>
        /// <param name="communicationCodeDtoList">CommunicationCode DTOs</param>
        /// <param name="officeCodeDtoList">OfficeCode DTOs</param>
        /// <param name="currentConfiguration">current fa configuration for the office/year</param>
        public StudentDocumentCollection(Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto, IEnumerable<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentDtoList,
            IEnumerable<Colleague.Dtos.Base.CommunicationCode2> communicationCodeDtoList, IEnumerable<Colleague.Dtos.Base.OfficeCode> officeCodeDtoList, 
            Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 currentConfiguration)
        {
            if (studentAwardYearDto == null) throw new ArgumentNullException("studentAwardYearDto");

            AwardYearCode = studentAwardYearDto.Code;

            Documents = new List<StudentDocument>();

            if (studentDocumentDtoList != null &&
                studentDocumentDtoList.Count() > 0 &&
                communicationCodeDtoList != null &&
                officeCodeDtoList != null)
            {
                //extract the financial aid office codes
                var financialAidOfficeCodes = officeCodeDtoList.Where(o => o.Type == Colleague.Dtos.Base.OfficeCodeType.FinancialAid);

                //extract the communication codes assigned to the FA office 
                //and that are assigned to the specific award year (or no award year), and that
                //are viewable by a student (on CCWP form)
                var financialAidCommunicationCodeDtoList = communicationCodeDtoList.Where(c =>
                    (c.AwardYear == AwardYearCode || string.IsNullOrEmpty(c.AwardYear)) &&
                    financialAidOfficeCodes.Select(o => o.Code).Contains(c.OfficeCodeId) && c.IsStudentViewable);

                //extract the student documents contained in the finAidCommCodes 
                var documentDtoList = studentDocumentDtoList.Where(d => financialAidCommunicationCodeDtoList.Select(c => c.Code).Contains(d.Code));
                
                var suppressInstance = currentConfiguration != null ? currentConfiguration.SuppressInstanceData : false;
                var useDocumentStatusDescription = currentConfiguration != null ? currentConfiguration.UseDocumentStatusDescription : false;
                foreach (var documentDto in documentDtoList)
                {
                    try
                    {
                        Documents.Add(new StudentDocument(documentDto, financialAidCommunicationCodeDtoList, suppressInstance, useDocumentStatusDescription));
                    }
                    catch (Exception) { }
                }
            }
        }

    }
}
