﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class AwardLetterHistoryCollection
    {
        public string AwardYearCode { get; set; }

        public IEnumerable<AwardLetterHistoryItem> AwardLetterHistoryItems { get; set; }
    }
}
