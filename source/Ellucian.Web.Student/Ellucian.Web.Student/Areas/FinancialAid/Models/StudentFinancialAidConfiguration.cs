﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class StudentFinancialAidConfiguration
    {

        /// <summary>
        /// Indicates whether the configuration is complete for a student
        /// A configuration is complete when there's at least one configuration where IsSelfServiceActive=true
        /// for the student's award year/office combinations.
        /// This helps onboard clients to FA self-service
        /// </summary>
        public bool IsConfigurationComplete { get; private set; }

        /// <summary>
        /// A list of messages that indicate what is missing from the configuration. 
        /// Should only be displayed to Admin Counselor users.
        /// This helps onboard clients to FA Self-Service
        /// </summary>
        public List<string> MissingConfigurationMessages { get; set; }

        /// <summary>
        /// A list of AwardYearConfigurations specific to this student's configuration.
        /// </summary>
        public List<AwardYearConfiguration> AwardYearConfigurations { get; set; }        

        /// <summary>
        /// Default constructor
        /// </summary>
        public StudentFinancialAidConfiguration()
        {
            IsConfigurationComplete = false;
            MissingConfigurationMessages = new List<string>();
            AwardYearConfigurations = new List<AwardYearConfiguration>();
        }

        /// <summary>
        /// Creates the FinancialAidConfiguration and calculates whether the configuration is complete.
        /// </summary>
        /// <param name="studentAwardYearDtos"></param>
        /// <param name="configurationDtos"></param>
        public StudentFinancialAidConfiguration(IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearDtos, IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3> configurationDtos)
        {
            MissingConfigurationMessages = new List<string>();
            AwardYearConfigurations = new List<AwardYearConfiguration>();
            SetIsConfigurationComplete(studentAwardYearDtos, configurationDtos);

            if (studentAwardYearDtos != null && configurationDtos != null)
            {
                foreach (var awardYear in studentAwardYearDtos)
                {
                    var config = configurationDtos.GetConfigurationDtoOrDefault(awardYear);
                    if (config != null)
                    {
                        AwardYearConfigurations.Add(new AwardYearConfiguration(config));
                    }
                }
            }
        }

        /// <summary>
        /// Calculates and sets IsConfigurationComplete.
        /// A configuration is complete when there's at least one configuration where IsSelfServiceActive=true
        /// for the student's award year/office combinations.
        /// </summary>
        /// <param name="studentAwardYearDtos"></param>
        /// <param name="configurationDtos"></param>
        public void SetIsConfigurationComplete(IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearDtos, IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3> configurationDtos)
        {
            MissingConfigurationMessages = new List<string>();
            IsConfigurationComplete = true;

            //no configurations are setup at all
            if (configurationDtos == null || configurationDtos.Count() == 0)
            {
                IsConfigurationComplete = false;
                MissingConfigurationMessages.Add("No configurations exist.");
                return;
            }

            if (studentAwardYearDtos == null || studentAwardYearDtos.Count() == 0)
            {
                //some configurations exist but don't know if configuration specific to the student is complete or not. 
                //because the student may have have no financial aid data.
                //Assume configuration is complete
                IsConfigurationComplete = true;
                return;
            }

            //do a join between the studentAwardYears and the configurations matching their AwardYear and Office Id, returning the configurations that match
            var yearOfficeBasedConfigurations =
                studentAwardYearDtos.Join(configurationDtos,
                    year => new Tuple<string, string>(year.Code, year.FinancialAidOfficeId),
                    config => new Tuple<string, string>(config.AwardYear, config.OfficeId),
                    (year, config) => config);

            if (yearOfficeBasedConfigurations == null || yearOfficeBasedConfigurations.Count() == 0)
            {
                IsConfigurationComplete = false;
                MissingConfigurationMessages.Add(string.Format("No configurations exist for any of the student's awardYear & office combinations: {0}", string.Join(", ", studentAwardYearDtos.Select(y => string.Format("{0}*{1}", y.Code, y.FinancialAidOfficeId)))));
                return;
            }

            var activeStudentAwardYears = studentAwardYearDtos.GetActiveStudentAwardYearDtos(configurationDtos);
            if (activeStudentAwardYears == null || activeStudentAwardYears.Count() == 0)
            {
                IsConfigurationComplete = false;
                MissingConfigurationMessages.Add(string.Format("Self-Service is deactivated for all student's configurations: {0}", string.Join(", ", studentAwardYearDtos.Select(y => string.Format("{0}*{1}", y.Code, y.FinancialAidOfficeId)))));
                return;
            }

            return;
        }
    }
}