﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.FinancialAid;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Ellucian.Web.Student.Utility;
using System.Globalization;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// ShoppingSheetConfiguration class
    /// </summary>
    public class ShoppingSheetConfiguration
    {
        public string CustomMessageRuleTableId { get; set; }
        /// <summary>
        /// The EfcOption allows schools who use the College Board PROFILE application
        /// to display the Estimated Family Contribution on the shopping sheet as calculated
        /// by the College Board instead of the Department of Education.
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ShoppingSheetEfcOption EfcOption { get; set; }

        public decimal? GraduationRate { get; set; }
        public decimal? LoanDefaultRate { get; set; }
        public string MedianBorrowingAmount { get; set; }
        public string MedianMonthlyPaymentAmount { get; set; }
        public decimal? NationalLoanDefaultRate { get; set; }
        public decimal? InstitutionRepaymentRate { get; set; }
        public decimal? NationalRepaymentRateAverage { get; set; }

        /// <summary>
        /// Graduation rate message to display on the shopping sheet scorecard
        /// which changes based on the office type
        /// </summary>
        public string GraduationRateDetail { get { return AssignGraduationRateDetail(); } }
        /// <summary>
        /// Loan default rate message depends on whether both LoanDefaultRate and
        /// NationalLoanDefaultRate are set or not
        /// </summary>
        public string LoanDefaultRateDetail { get; set; }
        /// <summary>
        /// Median Borrowing message depends on whether or not there is data about
        /// median borrowing 
        /// </summary>
        public string MedianBorrowingDetail { get; set; }
        /// <summary>
        /// Loan Repayment Rate message depends on whether or not there is corresponding data
        /// </summary>
        public string RepaymentRateDetail { get; set; }
        /// <summary>
        /// This type is used by the FA Shopping Sheet to configure the scorecard graphics
        /// on the right side of the shopping sheet.
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ShoppingSheetOfficeType OfficeType { get; set; }

        /// <summary>
        /// Rate levels to be reflected on the shopping sheet scorecard
        /// </summary>        
        public RateLevel? GraduationRateLevel
        {
            get
            {
                if (!lowMediumGraduationRateBoundary.HasValue || !mediumHighGraduationRateBoundary.HasValue || !GraduationRate.HasValue) return null;
                if (GraduationRate.Value < lowMediumGraduationRateBoundary.Value) return RateLevel.Low;
                else if (GraduationRate.Value < mediumHighGraduationRateBoundary.Value) return RateLevel.Medium;
                else return RateLevel.High;
            }
        }

        /// <summary>
        /// Institutional Loan Default Rate Level is calculated relative to the NationalLoanDefaultRateLevel
        /// </summary>
        public RateLevel? LoanDefaultRateLevel
        {
            get
            {
                return (LoanDefaultRate.HasValue && NationalLoanDefaultRate.HasValue) ?
                    (LoanDefaultRate < NationalLoanDefaultRate ? RateLevel.Low :
                    LoanDefaultRate > NationalLoanDefaultRate ? RateLevel.High : RateLevel.Medium) :
                    (RateLevel?)null;
            }
        }

        /// <summary>
        /// National Loan Default Rate Level is calculated relative to the institutional loan default rate level
        /// </summary>
        public RateLevel? NationalLoanDefaultRateLevel
        {
            get
            {
                return (LoanDefaultRate.HasValue && NationalLoanDefaultRate.HasValue) ?
                    (NationalLoanDefaultRate < LoanDefaultRate ? RateLevel.Low :
                    NationalLoanDefaultRate > LoanDefaultRate ? RateLevel.High : RateLevel.Medium) :
                    (RateLevel?)null;
            }
        }
        /// <summary>
        /// Institutional Repayment Rate Level is calculated relative to the National Repayment Rate Level
        /// </summary>
        public RateLevel? InstitutionRepaymentRateLevel
        {
            get
            {
                return (InstitutionRepaymentRate.HasValue && NationalRepaymentRateAverage.HasValue) ?
                    (InstitutionRepaymentRate < NationalRepaymentRateAverage ? RateLevel.Low :
                    InstitutionRepaymentRate > NationalRepaymentRateAverage ? RateLevel.High : (RateLevel?)null) :
                    (RateLevel?)null;
            }
        }
        /// <summary>
        /// National Repayment Rate Level is calculated relative to the Institution Repayment Rate Level
        /// </summary>
        public RateLevel? NationalRepaymentRateLevel
        {
            get
            {
                return (NationalRepaymentRateAverage.HasValue && InstitutionRepaymentRate.HasValue) ?
                    (NationalRepaymentRateAverage < InstitutionRepaymentRate ? RateLevel.Low :
                    NationalRepaymentRateAverage > InstitutionRepaymentRate ? RateLevel.High : (RateLevel?)null) :
                    (RateLevel?)null;
            }
        }

        private decimal? lowMediumGraduationRateBoundary;
        private decimal? mediumHighGraduationRateBoundary;

        /// <summary>
        /// Default constructor
        /// </summary>
        public ShoppingSheetConfiguration() { }

        public ShoppingSheetConfiguration(Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration shoppingSheetConfigurationDto, string officeName)
        {
            if (shoppingSheetConfigurationDto == null)
            {
                throw new ArgumentNullException("shoppingSheetConfiguration");
            }

            CustomMessageRuleTableId = shoppingSheetConfigurationDto.CustomMessageRuleTableId;
            EfcOption = shoppingSheetConfigurationDto.EfcOption;

            //If the rate is less than or equal to 1,the percentages are presented as 10%, 20%, etc. - thus the division
            GraduationRate = shoppingSheetConfigurationDto.GraduationRate <= 1 ? shoppingSheetConfigurationDto.GraduationRate / 100 : shoppingSheetConfigurationDto.GraduationRate;
            LoanDefaultRate = shoppingSheetConfigurationDto.LoanDefaultRate <= 1 ? shoppingSheetConfigurationDto.LoanDefaultRate / 100 : shoppingSheetConfigurationDto.LoanDefaultRate;
            NationalLoanDefaultRate = shoppingSheetConfigurationDto.NationalLoanDefaultRate <= 1 ? shoppingSheetConfigurationDto.NationalLoanDefaultRate / 100 : shoppingSheetConfigurationDto.NationalLoanDefaultRate;

            InstitutionRepaymentRate = shoppingSheetConfigurationDto.InstitutionRepaymentRate <= 1 ? shoppingSheetConfigurationDto.InstitutionRepaymentRate / 100 : shoppingSheetConfigurationDto.InstitutionRepaymentRate;
            NationalRepaymentRateAverage = shoppingSheetConfigurationDto.NationalRepaymentRateAverage <= 1 ? shoppingSheetConfigurationDto.NationalRepaymentRateAverage / 100 : shoppingSheetConfigurationDto.NationalRepaymentRateAverage;

            MedianBorrowingAmount = shoppingSheetConfigurationDto.MedianBorrowingAmount.HasValue ? ((int)shoppingSheetConfigurationDto.MedianBorrowingAmount).ToString("C0", CultureInfo.CurrentCulture) : string.Empty;
            MedianMonthlyPaymentAmount = shoppingSheetConfigurationDto.MedianMonthlyPaymentAmount.HasValue ? ((decimal)shoppingSheetConfigurationDto.MedianMonthlyPaymentAmount).ToString("C0", CultureInfo.CurrentCulture) : string.Empty;

            OfficeType = shoppingSheetConfigurationDto.OfficeType;

            lowMediumGraduationRateBoundary = shoppingSheetConfigurationDto.LowToMediumBoundary;
            mediumHighGraduationRateBoundary = shoppingSheetConfigurationDto.MediumToHighBoundary;

            LoanDefaultRateDetail = (shoppingSheetConfigurationDto.LoanDefaultRate.HasValue && shoppingSheetConfigurationDto.NationalLoanDefaultRate.HasValue) ?
                GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "LoanDefaultRateDetail") : GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "LoanDefaultRateNoDataMessage");

            RepaymentRateDetail = (InstitutionRepaymentRate.HasValue && NationalRepaymentRateAverage.HasValue) ?
                GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "LoanRepaymentRateDetail") : GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "LoanRepaymentRateNoDataMessage");

            MedianBorrowingDetail = AssignMedianBorrowingDetail(officeName);
        }

        /// <summary>
        /// Assigns the text to display on the shopping sheet scorecard for the
        /// Median Borrowing section
        /// </summary>
        /// <param name="financialAidConfigurationDto"></param>
        /// <returns></returns>
        private string AssignMedianBorrowingDetail(string officeName)
        {
            if (!string.IsNullOrEmpty(MedianBorrowingAmount) && !string.IsNullOrEmpty(MedianMonthlyPaymentAmount))
            {
                if (string.IsNullOrEmpty(officeName))
                {
                    officeName = "this institution";
                }
                try { return string.Format(GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "MedianBorrowingDetail"), officeName, MedianBorrowingAmount, MedianMonthlyPaymentAmount); }
                catch (Exception) { return string.Empty; }
            }
            else
            {
                return GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "MedianBorrowingNoDataMessage");
            }
        }

        /// <summary>
        /// Assigns the text to display on the shopping sheet scorecard
        /// for the Graduation Rate based on the office type
        /// </summary>
        /// <returns></returns>
        private string AssignGraduationRateDetail()
        {
            switch (OfficeType)
            {
                case ShoppingSheetOfficeType.AssociateDegreeGranting:
                    return GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "AssociatesDegreeGradRateMessage");
                case ShoppingSheetOfficeType.BachelorDegreeGranting:
                    return GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "BachelorsDegreeGradRateMessage");
                case ShoppingSheetOfficeType.CertificateGranting:
                    return GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "CertificateGrantingGradRateMessage");
                case ShoppingSheetOfficeType.GraduateDegreeGranting:
                    return GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "GraduateDegreeGradRateMessage");
                case ShoppingSheetOfficeType.NonDegreeGranting:
                    return GlobalResources.GetString(FinancialAidResourceFiles.ShoppingSheetResources, "NonDegreeGrantingGradRateMessage");
                default:
                    return null;
            }
        }
    }
}
