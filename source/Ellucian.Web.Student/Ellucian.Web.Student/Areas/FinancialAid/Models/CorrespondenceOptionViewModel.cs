﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Correspondence Option view model that encapsulates data to be
    /// shown on the Correspondence Option page
    /// </summary>
    public class CorrespondenceOptionViewModel : FinancialAidAdminModel
    {
        /// <summary>
        /// Person data describing the student
        /// </summary>
        public Colleague.Dtos.Base.Person Person { get; set; }

        /// <summary>
        /// Object that holds the Correspondence Option data
        /// </summary>
        public CorrespondenceOptionData CorrespondenceOptionData { get; set; }

        /// <summary>
        /// Indicates whether the current user is proxy or not
        /// </summary>
        public bool IsProxyView { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="personData">person object containing data about a student/applicant</param>
        /// <param name="studentAwardYearsData">set of student award years</param>
        /// <param name="financialAidOfficesData">set of fa offices</param>
        /// <param name="currentUser">current system user</param>
        public CorrespondenceOptionViewModel(Colleague.Dtos.Base.Person personData,
            List<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData,
            List<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficesData,
            ICurrentUser currentUser)
        {
            //Student/Applicant demographic data
            Person = personData;
            PersonId = personData != null ? personData.Id : null;
            PersonName = personData != null ? personData.PreferredName : null;
            PrivacyStatusCode = personData != null ? personData.PrivacyStatusCode : null;
            IsProxyView = currentUser != null && currentUser.ProxySubjects.Any();

            var configurationDtos = (financialAidOfficesData != null && financialAidOfficesData.Count != 0) ? 
                financialAidOfficesData.SelectMany(o => o.Configurations).ToList() : 
                new List<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3>();

            //Sort student award years in descending order
            var allStudentAwardYearsSortedByDesc = (studentAwardYearsData != null && studentAwardYearsData.Count != 0) ? 
                studentAwardYearsData.ToList().OrderByDescending(ay => ay.Code) : 
                new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>().AsEnumerable();

            var latestStudentAwardYearWithConfiguration = GetMostRecentAwardYearWithConfiguration(allStudentAwardYearsSortedByDesc, configurationDtos);            

            try
            {
                var financialAidOffice = financialAidOfficesData.FirstOrDefault(fao => fao.Id == latestStudentAwardYearWithConfiguration.FinancialAidOfficeId);
                CorrespondenceOptionData = new CorrespondenceOptionData(latestStudentAwardYearWithConfiguration, financialAidOffice);
            }
            catch (Exception) { /*Don't want to rethrow any exceptions */}

        }

        /// <summary>
        /// Returns the most recent award year with non-null configuration on student's record
        /// </summary>
        /// <param name="allStudentAwardYearsSortedByDesc">all student award years sorted in descending order</param>
        /// <param name="configurationDtos">list of configuration dtos</param>
        /// <returns>StudentAwardYear2 DTO</returns>
        private Colleague.Dtos.FinancialAid.StudentAwardYear2 GetMostRecentAwardYearWithConfiguration(IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> allStudentAwardYearsSortedByDesc, 
            List<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3> configurationDtos)
        {
            foreach (var year in allStudentAwardYearsSortedByDesc)
            {
                if (ConfigurationUtility.GetConfigurationDtoOrDefault(configurationDtos, year) != null)
                {
                    return year;
                }
            }
            return null;
        }

    }
}
