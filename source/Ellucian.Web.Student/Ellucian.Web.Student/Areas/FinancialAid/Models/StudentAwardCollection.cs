﻿/*Copyright 2014-2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class StudentAwardCollection
    {
        /// <summary>
        /// Award Year code
        /// </summary>
        public string AwardYearCode { get; set; }
        /// <summary>
        /// List of Student awards for the year
        /// </summary>
        public List<StudentAward> StudentAwardsForYear { get; set; }
        /// <summary>
        /// List of work awards for the year
        /// </summary>
        public List<StudentAward> StudentWorkAwardsForYear { get; set; }
        /// <summary>
        /// Total amount of all student awards for the year
        /// (if award is declined, we don't add the amount to the total)
        /// </summary>
        public decimal StudentAwardsForYearTotalAmount 
        {
            get
            {
                return StudentAwardsForYear != null && StudentAwardsForYear.Count > 0 ?
                    StudentAwardsForYear.SelectMany(sa => sa.StudentAwardPeriods.Where(sap => sap.AwardStatus.Category != Colleague.Dtos.FinancialAid.AwardStatusCategory.Denied 
                        && sap.AwardStatus.Category != Colleague.Dtos.FinancialAid.AwardStatusCategory.Rejected)).Sum(sap => (sap.OriginalAwardAmount.HasValue ? sap.OriginalAwardAmount.Value : 0)) : 0;                
            }
        }
        /// <summary>
        /// Total amount of all work awards for the year
        /// (if award is declined, we don't add the amount to the total)
        /// </summary>
        public decimal StudentWorkAwardsForYearTotalAmount 
        { 
            get
            {
                return StudentWorkAwardsForYear != null && StudentWorkAwardsForYear.Count > 0 ?
                   StudentWorkAwardsForYear.SelectMany(sa => sa.StudentAwardPeriods.Where(sap => sap.AwardStatus.Category != Colleague.Dtos.FinancialAid.AwardStatusCategory.Denied
                        && sap.AwardStatus.Category != Colleague.Dtos.FinancialAid.AwardStatusCategory.Rejected)).Sum(sap => (sap.OriginalAwardAmount.HasValue ? sap.OriginalAwardAmount.Value : 0)) : 0;
            }
        }
                
        public StudentAwardCollection()
        {
            
        }

    }
}
