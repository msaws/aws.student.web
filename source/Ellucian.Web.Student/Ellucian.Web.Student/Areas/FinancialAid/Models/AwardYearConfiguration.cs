﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    public class AwardYearConfiguration
    {
        /// <summary>
        /// Award year code of the configuration
        /// </summary>
        public string AwardYearCode { get; set; }

        /// <summary>
        /// Flag to indicate whether declined awards need to be placed in holding bin for review
        /// </summary>
        public bool IsDeclinedStatusChangeReviewRequired { get; set; }

        /// <summary>
        /// Flag that indicates whether loans with amount change need to be placed in the holding bin
        /// for review
        /// </summary>
        public bool IsLoanAmountChangeReviewRequired { get; set; }

        /// <summary>
        /// boolean to indicate whether a loan request can be made for the year
        /// </summary>
        public bool IsLoanRequestEnabled { get; set; }

        /// <summary>
        /// boolean to indicate whether awarding changes can be made for the year
        /// </summary>
        public bool AreAwardingChangesAllowed { get; set; }

        /// <summary>
        /// flag to indicate whether a user is allowed to update awards on annual
        /// or award period level
        /// </summary>
        public bool AllowAnnualAwardUpdatesOnly { get; set; }

        /// <summary>
        /// Flag to indicate whether to suppress maximum loan limits for the office/year;
        /// suppresses all maximum amounts if set to true (sub, unsub, grad)
        /// </summary>
        public bool SuppressMaximumLoanLimits { get; set; }

        /// <summary>
        /// Flag indicating whether the Pell LEU amount should be displayed for the current year
        /// </summary>
        public bool DisplayPellLifetimeEligibilityUsedPercentage { get; set; }

        /// <summary>
        /// Flag to indicate whether the Student Account Summary information display 
        /// should be suppressed or not
        /// </summary>
        public bool SuppressStudentAccountSummaryDisplay { get; set; }

        /// <summary>
        /// Flag to indicate whether to suppress the average award package information display
        /// </summary>
        public bool SuppressAverageAwardPackageDisplay { get; set; }

        /// <summary>
        /// Configuration object for the shopping sheet
        /// </summary>

        public ShoppingSheetConfiguration ShoppingSheetConfiguration { get; set; }

        /// <summary>
        /// Flag to indicate if students are allowed to decline or zero out already accepted loans
        /// </summary>
        public bool AllowDeclineZeroOfAcceptedLoans { get; set; }

        public AwardYearConfiguration() { }


        public AwardYearConfiguration(Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 configurationDto)
        {
            CreateAwardYearConfiguration(null, configurationDto);
        }

        public AwardYearConfiguration(Colleague.Dtos.FinancialAid.FinancialAidOffice3 officeDto, Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 configurationDto)
        {
            CreateAwardYearConfiguration(officeDto, configurationDto);
        }

        public void CreateAwardYearConfiguration(Colleague.Dtos.FinancialAid.FinancialAidOffice3 officeDto, Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 configurationDto)
        {
            if (configurationDto == null)
            {
                throw new ArgumentNullException("configurationDto");
            }

            AwardYearCode = configurationDto.AwardYear;
            IsDeclinedStatusChangeReviewRequired = configurationDto.IsDeclinedStatusChangeRequestRequired;
            IsLoanAmountChangeReviewRequired = configurationDto.IsLoanAmountChangeRequestRequired;
            IsLoanRequestEnabled = configurationDto.AreLoanRequestsAllowed;
            AreAwardingChangesAllowed = configurationDto.AreAwardChangesAllowed;
            AllowAnnualAwardUpdatesOnly = configurationDto.AllowAnnualAwardUpdatesOnly;
            SuppressMaximumLoanLimits = configurationDto.SuppressMaximumLoanLimits;
            DisplayPellLifetimeEligibilityUsedPercentage = configurationDto.DisplayPellLifetimeEarningsUsed;
            SuppressStudentAccountSummaryDisplay = configurationDto.SuppressAccountSummaryDisplay;
            SuppressAverageAwardPackageDisplay = configurationDto.SuppressAverageAwardPackageDisplay;
            AllowDeclineZeroOfAcceptedLoans = configurationDto.AllowDeclineZeroOfAcceptedLoans;

            if (configurationDto.ShoppingSheetConfiguration != null)
            {
                var officeName = officeDto != null ? officeDto.Name : string.Empty;
                ShoppingSheetConfiguration = new ShoppingSheetConfiguration(configurationDto.ShoppingSheetConfiguration, officeName);
            }
            else
            {
                ShoppingSheetConfiguration = new ShoppingSheetConfiguration();
            }
        }
    }
}