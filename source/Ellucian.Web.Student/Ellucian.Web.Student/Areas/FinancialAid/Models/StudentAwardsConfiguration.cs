﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Student awards configuration class
    /// </summary>
    public class StudentAwardsConfiguration
    {
        /// <summary>
        /// Award year code of the configuration
        /// </summary>
        public string AwardYearCode { get; set; }
        /// <summary>
        /// Flag to indicate whether declined awards need to be placed in holding bin for review
        /// </summary>
        public bool IsDeclinedStatusChangeReviewRequired { get; set; }
        /// <summary>
        /// Flag that indicates whether loans with amount change need to be placed in the holding bin
        /// for review
        /// </summary>
        public bool IsLoanAmountChangeReviewRequired { get; set; }
    }
}