﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Financial Aid office class
    /// </summary>
    public class FAOffice
    {
        /// <summary>
        /// Name to be displayed at the top of the shopping sheet
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Name to be displayed in the contact block
        /// </summary>
        public string ContactName { get; set; }
        public List<string> Address { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="financialAidOfficeDto">financial aid office dto</param>
        /// <param name="awardYearCode">award year code for which creating FA office</param>
        /// <param name="institutionName">institution name - not required</param>
        public FAOffice(Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOfficeDto, string institutionName)
        {
            if (financialAidOfficeDto == null)
            {
                throw new ArgumentNullException("financialAidOfficeDto");
            }

            Name = !string.IsNullOrEmpty(institutionName) ? institutionName : financialAidOfficeDto.Name;
            Address = financialAidOfficeDto.AddressLabel;
            PhoneNumber = financialAidOfficeDto.PhoneNumber;
            EmailAddress = financialAidOfficeDto.EmailAddress;
            ContactName = financialAidOfficeDto.Name;
        }
    }
}
