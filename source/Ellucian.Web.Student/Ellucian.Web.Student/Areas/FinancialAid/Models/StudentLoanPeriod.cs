﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliatesusing System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Student Loan period class
    /// </summary>
    public class StudentLoanPeriod
    {
        /// <summary>
        /// Period code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Period description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Period amount
        /// </summary>
        public int AwardAmount { get; set; }

        /// <summary>
        /// The Start Date of the award period. Used for sorting.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Boolean to indicate if loan period is selected or not
        /// </summary>
        public bool IsActive { get; set; }

        public StudentLoanPeriod()
        {

        }

        /// <summary>
        /// Constructor that accepts awardPeriod DTO
        /// </summary>
        /// <param name="awardPeriodDto">awardPeriod DTO to create loanPeriod object from</param>
        public StudentLoanPeriod(Colleague.Dtos.FinancialAid.AwardPeriod awardPeriodDto)
        {
            Code = awardPeriodDto.Code;
            Description = awardPeriodDto.Description;
            StartDate = awardPeriodDto.StartDate;
            IsActive = true;
        }

        /// <summary>
        /// Compares two loanPeriod objects and returns true/false
        /// </summary>
        /// <param name="obj">loanPeriod object to compare to</param>
        /// <returns>true/false</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var studentLoanPeriod = obj as StudentLoanPeriod;

            return (studentLoanPeriod.Code == this.Code);
        }

        /// <summary>
        /// Gets the hashcode of a studentLoanPeriod object based on its Code
        /// </summary>
        /// <returns>objects hash code</returns>
        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }
    }
}
