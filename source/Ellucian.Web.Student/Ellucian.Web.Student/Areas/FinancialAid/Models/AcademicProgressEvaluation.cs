﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// SAP evaluation class
    /// </summary>
    public class AcademicProgressEvaluation
    {
        /// <summary>
        /// SAP Status object for the current evaluation
        /// </summary>
        public SAPStatus SAPStatus { get; set; }
        /// <summary>
        /// List of SAP Detail items for the current evaluation
        /// </summary>
        public List<SAPDetailItem> SAPDetailItems { get; set; }
        /// <summary>
        /// Most recent SAP Appeal(status) (if any) for the current evaluation
        /// </summary>
        public SAPAppeal SAPAppeal { get; set; }

        /// <summary>
        /// Academic Progress evaluation id 
        /// </summary>
        public string AcademicProgressEvaluationId { get; set; }

        /// <summary>
        /// Academic progress evaluation type
        /// </summary>
        public string AcademicProgressEvaluationType { get; set; }
        
        /// <summary>
        /// Constructor for most recent AcademicProgressEvaluation
        /// </summary>
        /// <param name="academicProgressEvaluationsData">List of academicProgressEvaluation DTOs</param>
        /// <param name="academicProgressStatusesData">List of AcademicProgressStatuse DTOs</param>
        /// <param name="academicProgramsData">List of AcademicProgram DTOs</param>
        /// <param name="financialAidOfficeData">FinancialAidOffice DTO for most recent award year</param>
        /// <param name="academicProgressAppealCodesData">List of appeal codes</param>
        /// <param name="appealsCounselorsData">List of fa counselors</param>
        /// <param name="counselorPhoneNumbersData">List of counselors' phone numbers</param>
        /// <param name="mostRecentAwardYear">most recent award year on student's record</param>
        public AcademicProgressEvaluation(IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluationsData,
                                          IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatusesData,                                          
                                          IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode> academicProgressAppealCodesData,
                                          IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidCounselor> appealsCounselorsData,
                                          IEnumerable<Colleague.Dtos.Base.PhoneNumber> counselorPhoneNumbersData,
                                          IEnumerable<Colleague.Dtos.Student.AcademicProgram> academicProgramsData,
                                          Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOfficeData,
                                          Colleague.Dtos.FinancialAid.StudentAwardYear2 mostRecentAwardYear)
        {
            Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicProgressEvaluation = null;

            //Create and populate SAPDetails list
            SAPDetailItems = new List<SAPDetailItem>();
            
            //Get latest SAPStatus
            SAPStatus = SAPUtility.GetLatestSAPStatus(academicProgressEvaluationsData, academicProgressStatusesData, academicProgramsData);
            
            //Get most recent academic progress evaluation
            if (academicProgressEvaluationsData != null && SAPStatus != null)
            {
                academicProgressEvaluation = academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault(aped => aped.StatusCode == SAPStatus.AcademicProgressStatus.Code);
            }

            if (academicProgressEvaluation != null)
            {
                AcademicProgressEvaluationId = academicProgressEvaluation.Id;
                AcademicProgressEvaluationType = academicProgressEvaluation.AcademicProgressTypeCode;

                CreateSAPDetailItems(financialAidOfficeData, academicProgressEvaluation);

                if (academicProgressEvaluation.ResultAppeals != null && academicProgressEvaluation.ResultAppeals.Any())
                {
                    try
                    {
                        //Create SAPAppeal if data received
                        SAPAppeal = new SAPAppeal(academicProgressEvaluation, academicProgressAppealCodesData, appealsCounselorsData, counselorPhoneNumbersData, mostRecentAwardYear, financialAidOfficeData);
                    }
                    catch { }
                }            
            }
            
        }

        /// <summary>
        /// Constructor for AcademicProgressEvaluation items in SAPHistoryItems list
        /// </summary>
        /// <param name="academicProgressEvaluationData">Single AcademicProgressEvaluation DTO</param>
        /// <param name="academicProgressStatusesData">List of AcademicProgressStatus DTOs</param>
        /// <param name="academicProgramsData">List of AcademicProhram DTOs</param>
        /// <param name="financialAidOfficeData">FinancialAidOffice DTO for most recent award year</param>
        /// <param name="academicProgressAppealCodesData">List of appeal codes</param>
        /// <param name="appealsCounselorsData">List of fa counselors</param>
        /// <param name="counselorPhoneNumbersData">List of counselors' phone numbers</param>
        /// <param name="mostRecentAwardYear">most recent award year on student's record</param>
        public AcademicProgressEvaluation(Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicProgressEvaluationData,
                                          IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatusesData,                                          
                                          IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode> academicProgressAppealCodesData,
                                          IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidCounselor> appealsCounselorsData,
                                          IEnumerable<Colleague.Dtos.Base.PhoneNumber> counselorPhoneNumbersData,
                                          IEnumerable<Colleague.Dtos.Student.AcademicProgram> academicProgramsData,
                                          Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOfficeData,
                                          Colleague.Dtos.FinancialAid.StudentAwardYear2 mostRecentAwardYear)
        {
            Colleague.Dtos.FinancialAid.AcademicProgressStatus academicProgressStatus = null;
            Colleague.Dtos.Student.AcademicProgram academicProgram = null;

            //Create and populate SAPDetails list
            SAPDetailItems = new List<SAPDetailItem>();

            //Get status and program for the academic progress evaluation
            if (academicProgressEvaluationData != null)
            {
                academicProgressStatus = academicProgressStatusesData != null ?
                                    academicProgressStatusesData.FirstOrDefault(aps => aps.Code == academicProgressEvaluationData.StatusCode) : null;
                academicProgram = (academicProgramsData != null && academicProgressEvaluationData.ProgramDetail != null) ?
                        academicProgramsData.FirstOrDefault(ap => ap.Code == academicProgressEvaluationData.ProgramDetail.ProgramCode) : null;

                AcademicProgressEvaluationId = academicProgressEvaluationData.Id;
                AcademicProgressEvaluationType = academicProgressEvaluationData.AcademicProgressTypeCode;

                if (academicProgressEvaluationData.ResultAppeals != null && academicProgressEvaluationData.ResultAppeals.Any())
                {
                    try
                    {
                        //Create SAPAppeal and SAP Status if data received
                        SAPAppeal = new SAPAppeal(academicProgressEvaluationData, academicProgressAppealCodesData, appealsCounselorsData, counselorPhoneNumbersData, mostRecentAwardYear, financialAidOfficeData);                        
                    }
                    catch { }
                }

                try
                {
                    SAPStatus = new SAPStatus(academicProgressEvaluationData, academicProgressStatus, academicProgram);
                }
                catch { }
                
                CreateSAPDetailItems(financialAidOfficeData, academicProgressEvaluationData);
            }
            
        }

        /// <summary>
        /// Iterates over all available sap detail properties from FinancialAidOffice DTO and calls a private method <see cref="AddAcademicProgressDetailItem"/> to create
        /// each of the SAPDetailItem objects
        /// </summary>
        /// <param name="financialAidOfficeData">Financial Aid office data</param>
        /// <param name="academicEvaluation">academic progress evaluation</param>
        private void CreateSAPDetailItems(Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOfficeData, Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicEvaluation)
        {
            if (academicEvaluation != null && academicEvaluation.Detail != null && academicEvaluation.ProgramDetail != null
                && financialAidOfficeData != null && financialAidOfficeData.AcademicProgressConfiguration != null && financialAidOfficeData.AcademicProgressConfiguration.DetailPropertyConfigurations != null
               )
            {
                foreach (var propertyConfig in financialAidOfficeData.AcademicProgressConfiguration.DetailPropertyConfigurations.Where(c => !c.IsHidden).OrderBy(c => c.Type, new AcademicProgressPropertyTypeDisplayComparer()))
                {

                    switch (propertyConfig.Type)
                    {
                        case AcademicProgressPropertyType.CumulativeAttemptedCredits:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.CumulativeAttemptedCredits, SAPDetailItemType.Value);
                            break;
                        case AcademicProgressPropertyType.CumulativeAttemptedCreditsExcludingRemedial:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.CumulativeAttemptedCreditsExcludingRemedial, SAPDetailItemType.Value);
                            break;
                        case AcademicProgressPropertyType.CumulativeCompletedCredits:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.CumulativeCompletedCredits, SAPDetailItemType.Value);
                            break;
                        case AcademicProgressPropertyType.CumulativeCompletedCreditsExcludingRemedial:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.CumulativeCompletedCreditsExcludingRemedial, SAPDetailItemType.Value);
                            break;
                        case AcademicProgressPropertyType.CumulativeOverallGpa:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.CumulativeOverallGpa, SAPDetailItemType.GPA);
                            break;
                        case AcademicProgressPropertyType.CumulativeRateOfCompletion:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.CumulativeRateOfCompletion, SAPDetailItemType.Rate);
                            break;
                        case AcademicProgressPropertyType.CumulativeRateOfCompletionExcludingRemedial:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.CumulativeRateOfCompletionExcludingRemedial, SAPDetailItemType.Rate);
                            break;
                        case AcademicProgressPropertyType.EvaluationPeriodAttemptedCredits:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.EvaluationPeriodAttemptedCredits, SAPDetailItemType.Value);
                            break;
                        case AcademicProgressPropertyType.EvaluationPeriodCompletedCredits:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.EvaluationPeriodCompletedCredits, SAPDetailItemType.Value);
                            break;
                        case AcademicProgressPropertyType.EvaluationPeriodOverallGpa:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.EvaluationPeriodOverallGpa, SAPDetailItemType.GPA);
                            break;
                        case AcademicProgressPropertyType.EvaluationPeriodRateOfCompletion:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.Detail.EvaluationPeriodRateOfCompletion, SAPDetailItemType.Rate);
                            break;
                        case AcademicProgressPropertyType.MaximumProgramCredits:
                            AddAcademicProgressDetailItem(propertyConfig, academicEvaluation.ProgramDetail.ProgramMaxCredits ?? academicEvaluation.ProgramDetail.ProgramMinCredits ?? 0, SAPDetailItemType.Value);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Creates instances of SAPDetailItem object and adds them to SAPDetailItems list
        /// </summary>
        /// <param name="propertyConfig">AcademicProgressPropertyConfiguration: contains Code, Description, Explanation</param>
        /// <param name="value">Value for the current SAPDetailItem: comes from associated value of current academicEvaluation DTO</param>
        /// <param name="itemType">Item type <see cref="SAPDetailItemType"/></param>
        private void AddAcademicProgressDetailItem(Colleague.Dtos.FinancialAid.AcademicProgressPropertyConfiguration propertyConfig, decimal value, SAPDetailItemType itemType)
        {
            SAPDetailItems.Add(new SAPDetailItem()
            {
                Code = propertyConfig.Type.ToString(),
                Description = propertyConfig.GetAcademicProgressPropertyLabelOrDefault(),
                Value = value,
                ItemType = itemType,
                Explanation = propertyConfig.Description
            });
        }
        
    }
}