﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Class that holds general information about student account summary
    /// </summary>
    public class StudentFinanceAccountSummary
    {
        /// <summary>
        /// Amount that is due on student's account
        /// </summary>
        public decimal AmountDue { get { return amountDue; } }
        private readonly decimal amountDue;
        /// <summary>
        /// Amount that is overdue on student's account
        /// </summary>
        public decimal AmountOverdue { get { return amountOverdue; } }
        private readonly decimal amountOverdue;
        /// <summary>
        /// Total amount due: due + overdue
        /// </summary>
        public decimal TotalAmountDue
        {
            get
            {
                return AmountDue + AmountOverdue;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="studentAccountDue">AccountDue object containing due amounts information</param>
        public StudentFinanceAccountSummary(Colleague.Dtos.Finance.AccountDue.AccountDue studentAccountDue)
        {
            if (studentAccountDue != null)
            {
                //General, invoice, and payment plan items extend accountReceivableDueItem - concat them together
                var accountReceivableDueItems = studentAccountDue.AccountTerms.Where(t => t.GeneralItems != null && t.GeneralItems.Any()).SelectMany(t => t.GeneralItems)
                    .Concat(studentAccountDue.AccountTerms.Where(t => t.InvoiceItems != null && t.InvoiceItems.Any()).SelectMany(t => t.InvoiceItems))
                    .Concat(studentAccountDue.AccountTerms.Where(t => t.PaymentPlanItems != null && t.PaymentPlanItems.Any()).SelectMany(t => t.PaymentPlanItems)).ToList();
                //Deposit due items 
                var depositDueItems = studentAccountDue.AccountTerms.Where(t => t.DepositDueItems != null && t.DepositDueItems.Any()).SelectMany(t => t.DepositDueItems).ToList();

                if (accountReceivableDueItems.Any())
                {
                    foreach (var item in accountReceivableDueItems)
                    {
                        if (item.DueDate.HasValue && item.DueDate.Value < DateTime.Today)
                        {
                            amountOverdue += item.AmountDue.HasValue ? item.AmountDue.Value : 0;
                        }
                        else
                        {
                            amountDue += item.AmountDue.HasValue ? item.AmountDue.Value : 0;
                        }
                    }
                }

                if (depositDueItems.Any())
                {
                    foreach (var item in depositDueItems)
                    {
                        if (item.DueDate < DateTime.Today)
                        {
                            amountOverdue += item.AmountDue;
                        }
                        else
                        {
                            amountDue += item.AmountDue;
                        }
                    }
                }
                
            }
        }

    }
}