﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Areas.FinancialAid.Models
{
    /// <summary>
    /// Item that appears in FA home page checklist
    /// </summary>
    public class ChecklistItem
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ChecklistItemType? Type { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Target { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ChecklistItemStatus Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public string DueDate { get; set; }
        public string ExpirationDate { get; set; }

        public string Title { get; set; }
        public string Summary { get; set; }

        public bool IsActiveItem { get; set; }

        /// <summary>
        /// Flag to indicate whether a user has access to the internal page
        /// linked to the checklist item or not
        /// </summary>
        public bool IsReadOnly { get; set; }
        /// <summary>
        /// Status that determines whether the item is to be displayed, skipped or not displayed
        /// on the checklist
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ChecklistItemControlStatus ControlStatus { get; set; }
    }
}
