﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Filters;
using Newtonsoft.Json;
using slf4net;
using System.IO;
using System.Net;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Mvc.Menu;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// AwardLetterController class for FinancialAid Hub
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class AwardLetterController : BaseFinancialAidController
    {
        private readonly string reportLogoPath;

        /// <summary>
        /// Creates a new AwardLetterController instance
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public AwardLetterController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {
            reportLogoPath = (settings.ReportLogoPath != null) ?
               settings.ReportLogoPath.PrependTilde() : string.Empty;
        }

        /// <summary>
        /// Action method for the Award Letter page.
        /// </summary>
        /// <returns>AwardLetter view</returns>
        [LinkHelp]
        [PageAuthorize("faAwardLetter")]
        public ActionResult Index()
        {
            ViewBag.Title = "AwardLetter";
            ViewBag.AbsoluteReportLogoPath = UrlHelper.GenerateContentUrl(reportLogoPath, this.HttpContext);

            return View("AwardLetter");
        }

        [LinkHelp]
        [PageAuthorize("faAwardLetterAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Award Letter";
            ViewBag.AbsoluteReportLogoPath = UrlHelper.GenerateContentUrl(reportLogoPath, this.HttpContext);
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("AwardLetter");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("AwardLetter");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Gets student's information necessary to create student award years, counselor, student objects
        /// and passes that to the View model
        /// </summary>
        /// <param name="studentId">student's id for whom to retrieve the data</param>
        /// <returns>awardLetterViewModel</returns>
        [PageAuthorize("faAwardLetter")]
        public async Task<String> GetGeneralAwardLetterDataAsync(string id)
        {
            var studentId = id;

            //Get the person data
            var person = GetPersonData(id, ref studentId);

            var counselor = GetFinancialAidCounselor(person);            

            var officeCodes = new List<Colleague.Dtos.Base.OfficeCode>();
            var communicationCodes = new List<Colleague.Dtos.Base.CommunicationCode2>();
            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            
            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var studentApplications = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>();
            var studentDocuments = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
            
            var studentChecklists = new List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist>();
            var financialAidChecklistItems = new List<Colleague.Dtos.FinancialAid.ChecklistItem>();

            try { officeCodes = ServiceClient.GetOfficeCodes().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { communicationCodes = ServiceClient.GetCommunicationCodes2().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }            

            try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentApplications.AddRange(ServiceClient.GetStudentFafsas(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentApplications.AddRange(ServiceClient.GetProfileApplications(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentDocuments = (await ServiceClient.GetStudentDocumentsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }            

            try { studentChecklists = (await ServiceClient.GetAllStudentChecklistsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidChecklistItems = ServiceClient.GetFinancialAidChecklistItems().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            var awardLetterViewModel = new AwardLetterViewModel(studentAwardYears, studentApplications,
                studentDocuments, communicationCodes, officeCodes, person, counselor, financialAidOffices,
                studentChecklists, financialAidChecklistItems, CurrentUser);

            await SetPrivacyPropertiesForAdminModel(awardLetterViewModel);

            return JsonConvert.SerializeObject(awardLetterViewModel);
        }

        /// <summary>
        /// Gets AwardLetter data for the specified student award year
        /// </summary>
        /// <param name="awardLetterViewModel">award letter view model to update with award letter data</param>
        /// <param name="selectedYearCode">selected student award year</param>
        /// <param name="getLetterData">flag indicating whether to get award letter data or not</param>
        /// <returns>awardLetterViewModel</returns>
        [PageAuthorize("faAwardLetter")]
        public async Task<String> GetAwardLetterDataAsync(string awardLetterViewModel, string selectedYearCode, bool getLetterData)
        {
            var studentAwardLetterViewModel = JsonConvert.DeserializeObject<AwardLetterViewModel>(awardLetterViewModel);
            var selectedYear = JsonConvert.DeserializeObject<string>(selectedYearCode);

            var student = studentAwardLetterViewModel.Student;
                        
            //If set to yes, get the data necessary to build award letter, award letter history records list, and 
            //award package control status for the selected year
            if (getLetterData)
            {
                var studentAwardLetter = new Colleague.Dtos.FinancialAid.AwardLetter2();
                var studentAwardYearDto = new Colleague.Dtos.FinancialAid.StudentAwardYear2();
                var awardStatuses = new List<Colleague.Dtos.FinancialAid.AwardStatus>();

                var awardLetterConfigurations = new List<Colleague.Dtos.FinancialAid.AwardLetterConfiguration>();
                var studentAwards = new List<Colleague.Dtos.FinancialAid.StudentAward>();
                var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
                var studentChecklists = new List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist>();
                var financialAidChecklistItems = new List<Colleague.Dtos.FinancialAid.ChecklistItem>();

                try { studentAwardYearDto = ServiceClient.GetStudentAwardYear2(student.Id, selectedYear); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                try { awardStatuses = ServiceClient.GetAwardStatuses().ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                try { awardLetterConfigurations = (await ServiceClient.GetAwardLetterConfigurationsAsync()).ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                try { studentAwards = ServiceClient.GetStudentAwards(student.Id, selectedYear).ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                try { studentChecklists = (await ServiceClient.GetAllStudentChecklistsAsync(student.Id)).ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                try { financialAidChecklistItems = ServiceClient.GetFinancialAidChecklistItems().ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                //Add a flag to see if exception was thrown when trying to retrieve student award letter
                bool studentAwardLetterExceptionThrown = false;
                try { studentAwardLetter = await ServiceClient.GetAwardLetter3Async(student.Id, selectedYear); }
                catch (Exception e) { 
                    Logger.Info(e, e.Message);
                    studentAwardLetterExceptionThrown = true;                    
                }
                //something went wrong with the award letter retrieval - call the reset method - no need
                //to call UpdateViewModel
                if (studentAwardLetterExceptionThrown)
                {
                    ResetAwardLetter(studentAwardLetterViewModel);
                }
                else
                {
                    studentAwardLetterViewModel.UpdateViewModel(awardStatuses, awardLetterConfigurations, financialAidOffices, financialAidChecklistItems,
                    studentAwardLetter, studentAwards, studentAwardYearDto, studentChecklists);
                }
                
            }
                //If flag is set to false, nullify/empty the properties
            else
            {
                ResetAwardLetter(studentAwardLetterViewModel);
            }
            return JsonConvert.SerializeObject(studentAwardLetterViewModel);
        }

        /// <summary>
        /// Resets award letter specific properties of the studentAwardLetterViewModel
        /// to have no data
        /// </summary>
        /// <param name="studentAwardLetterViewModel">student award letter view model</param>
        private static void ResetAwardLetter(AwardLetterViewModel studentAwardLetterViewModel)
        {
            studentAwardLetterViewModel.StudentAwardLetter = null;
            studentAwardLetterViewModel.AwardPackageChecklistItemControlStatus = null;
            studentAwardLetterViewModel.AwardLetterHistoryItems = new List<AwardLetterHistoryItem>();
        }        

        /// <summary>
        /// Update the accepted date for a student's award letter.
        /// Accepting the letter will update the AcceptedDate with today's date. 
        /// </summary>
        /// <param name="awardLetter">A JSON the StudentAwardLetter to update</param>
        /// <returns>A JSON representaiton of the updated StudentAwardLetter</returns>
        [PageAuthorize("faAwardLetter")]
        public async Task<string> UpdateAwardLetterAsync(string awardLetter)
        {
            var studentAwardLetterModel = JsonConvert.DeserializeObject<StudentAwardLetter>(awardLetter);

            var awardLetterDto = new Colleague.Dtos.FinancialAid.AwardLetter2();
            awardLetterDto.Id = studentAwardLetterModel.AwardLetterRecordId;
            awardLetterDto.StudentId = studentAwardLetterModel.StudentId;
            awardLetterDto.AwardLetterYear = studentAwardLetterModel.AwardYearCode;

            var updatedAwardLetter = await ServiceClient.UpdateAwardLetter2(awardLetterDto.StudentId, awardLetterDto.AwardLetterYear, awardLetterDto);
            if (updatedAwardLetter != null)
            {
                studentAwardLetterModel.AcceptedDate = updatedAwardLetter.AcceptedDate;                
            }

            return JsonConvert.SerializeObject(studentAwardLetterModel);
        }

        /// <summary>
        /// Get a FileResult object that represents a PDF Award Letter report. This report will be opened
        /// within the browser, as opposed to downloaded to the user's machine.
        /// </summary>
        /// <param name="studentId">The Colleague PERSON id of the student for whom to get the award letter report.</param>
        /// <param name="awardLetterId">The award letter id for which to get the award letter report.</param>
        /// <returns>A FileResult containing the Award Letter Report pdf file.</returns>
        [PageAuthorize("faAwardLetter")]
        public async Task<FileResult> GetAwardLetterReportAsync(string studentId, string awardLetterId)
        {
            if (string.IsNullOrEmpty(studentId)) studentId = GetEffectivePersonId();

            if (string.IsNullOrEmpty(awardLetterId))
            {
                Logger.Error("award letter id cannot be null or empty.");
                return null;
            }
            
            try
            {
                var fileName = string.Empty;
                var report = await ServiceClient.GetAwardLetterReport3Async(studentId, awardLetterId);

                MemoryStream memoryStream = new MemoryStream(report.FileContent);

                return File(memoryStream, "application/Pdf");

            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return null;
            }
        }
    }
}
