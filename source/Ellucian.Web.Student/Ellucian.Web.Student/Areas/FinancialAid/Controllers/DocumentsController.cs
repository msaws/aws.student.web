﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Filters;
using Newtonsoft.Json;
using slf4net;
using Ellucian.Web.Mvc.Menu;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// DocumentsController class for FinancialAid Hub
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class DocumentsController : BaseFinancialAidController
    {
        /// <summary>
        /// Creates a new DocumentsController instance
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public DocumentsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {

        }

        /// <summary>
        /// Action method for the RequiredDocuments page.
        /// </summary>
        /// <returns>Documents view</returns>
        [LinkHelp]
        [PageAuthorize("faDocuments")]
        public ActionResult Index()
        {
            ViewBag.Title = "Required Documents";
 
            return View("Documents");
        }

        [LinkHelp]
        [PageAuthorize("faDocumentsAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Required Documents";
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("Documents");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("Documents");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Gets student's required documents information and passes it to 
        /// DocumentsViewModel
        /// </summary>
        /// <param name="id"></param>
        /// <returns>documentsViewModel</returns>
        [PageAuthorize("faDocuments")]
        public async Task<String> GetDocumentsViewModelAsync(string id)
        {
            var studentId = id;

            //Get the person data
            var person = GetPersonData(id, ref studentId);

            var counselor = GetFinancialAidCounselor(person);

            var officeCodes = new List<Colleague.Dtos.Base.OfficeCode>();
            var communicationCodes = new List<Colleague.Dtos.Base.CommunicationCode2>();
            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var studentDocuments = new List<Colleague.Dtos.FinancialAid.StudentDocument>();            

            try { officeCodes = ServiceClient.GetOfficeCodes().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { communicationCodes = ServiceClient.GetCommunicationCodes2().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentDocuments = (await ServiceClient.GetStudentDocumentsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }
            

            var documentsViewModel = new DocumentsViewModel(person, counselor, studentDocuments, communicationCodes, officeCodes, studentAwardYears, financialAidOffices, CurrentUser);

            await SetPrivacyPropertiesForAdminModel(documentsViewModel);

            return JsonConvert.SerializeObject(documentsViewModel);
        }
    }
}
