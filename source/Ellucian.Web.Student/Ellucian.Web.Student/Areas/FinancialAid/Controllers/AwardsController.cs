﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Filters;
using Newtonsoft.Json;
using slf4net;
using Ellucian.Web.Mvc.Menu;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// AwardsController class for FinancialAid Hub
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class AwardsController : BaseFinancialAidController
    {
        /// <summary>
        /// Creates a new AwardsController instance
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public AwardsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {

        }

        /// <summary>
        /// Action method for the MyAwards page.
        /// </summary>
        /// <returns>Awards view</returns>
        [LinkHelp]
        [PageAuthorize("faAwards")]
        public ActionResult Index()
        {
            ViewBag.Title = "My Awards";
            
            return View("Awards");
        }

        [LinkHelp]
        [PageAuthorize("faAwardsAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin My Awards";
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("Awards");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("Awards");
                return View("Admin", model);
            }
        }

        [PageAuthorize("faAwards")]
        public async Task<string> GetAwardsViewModelAsync(string id)
        {
            var studentId = id;

            //Get the person data
            var person = GetPersonData(id, ref studentId);

            /* Determine if the current user is the student/applicant.
             * The alternatives could be the current user is an fa counselor or proxy
             * retrieving student/applicant info
             */
            bool isUserSelf = IsUserSelf(person);
           
            var counselor = GetFinancialAidCounselor(person);

            var awards = ServiceClient.GetAwards2();
            var awardStatuses = ServiceClient.GetAwardStatuses();
            var awardPeriods = ServiceClient.GetAwardPeriods();

            var officeCodes = new List<Colleague.Dtos.Base.OfficeCode>();
            var communicationCodes = new List<Colleague.Dtos.Base.CommunicationCode2>();
            var awardYears = new List<Colleague.Dtos.FinancialAid.AwardYear>();
            var studentFaApplications = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>();
            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentLoanSummary = new Colleague.Dtos.FinancialAid.StudentLoanSummary();
            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var studentDocuments = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
            var studentAwards = new List<Colleague.Dtos.FinancialAid.StudentAward>();
            var studentLoanLimitations = new List<Colleague.Dtos.FinancialAid.StudentLoanLimitation>();
            var studentPendingLoanRequests = new List<StudentLoanRequest>();
            var awardPackageChangeRequests = new List<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest>();
            var studentChecklists = new List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist>();
            var financialAidChecklistItems = new List<Colleague.Dtos.FinancialAid.ChecklistItem>();
            var academicProgressEvaluations = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            var academicProgressStatuses = new List<Colleague.Dtos.FinancialAid.AcademicProgressStatus>();
            var studentAwardLetters = new List<Colleague.Dtos.FinancialAid.AwardLetter2>();

            try { officeCodes = ServiceClient.GetOfficeCodes().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { communicationCodes = ServiceClient.GetCommunicationCodes2().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { awardYears = ServiceClient.GetAwardYears().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetStudentFafsas(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetProfileApplications(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentLoanSummary = await ServiceClient.GetStudentLoanSummaryAsync(studentId); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentDocuments = (await ServiceClient.GetStudentDocumentsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwards = ServiceClient.GetStudentAwards(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentLoanLimitations = (await ServiceClient.GetStudentLoanLimitationsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { awardPackageChangeRequests = ServiceClient.GetAwardPackageChangeRequests(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentChecklists = (await ServiceClient.GetAllStudentChecklistsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidChecklistItems = ServiceClient.GetFinancialAidChecklistItems().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { academicProgressEvaluations = (await ServiceClient.GetStudentAcademicProgressEvaluations2Async(studentId)).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { academicProgressStatuses = (await ServiceClient.GetAcademicProgressStatusesAsync()).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { studentAwardLetters = (await ServiceClient.GetAwardLetters3Async(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            var awardsViewModel = new AwardsViewModel(person, studentAwards, studentLoanLimitations, studentLoanSummary, awards, awardPeriods, awardStatuses,
                studentAwardYears, studentDocuments, studentAwardLetters, communicationCodes, officeCodes, studentFaApplications, counselor, financialAidOffices, studentPendingLoanRequests,
                awardPackageChangeRequests, studentChecklists, financialAidChecklistItems, academicProgressEvaluations, academicProgressStatuses, CurrentUser, isUserSelf);

            await SetPrivacyPropertiesForAdminModel(awardsViewModel);

            return JsonConvert.SerializeObject(awardsViewModel);
        }

        /// <summary>
        /// Updates a loan collection
        /// </summary>
        /// <param name="loanCollection">loan collection (sub, unsub, gradPlus, etc.)</param>
        /// <param name="action">accept/reject</param>
        /// <returns>serialized updated awards model</returns>
        [PageAuthorize("faAwards")]
        public async Task<string> UpdateLoanCollectionAsync(string loanCollection, string action)
        {
            var loanCollectionModel = JsonConvert.DeserializeObject<LoanCollection>(loanCollection);
            var actionTaken = JsonConvert.DeserializeObject<string>(action);

            var studentId = loanCollectionModel.StudentId;
            var awardYear = loanCollectionModel.AwardYearCode;

            var updatedAwardsViewModel = await UpdateAwardsViewModelAsync(studentId, awardYear, actionTaken, loanCollectionModel.Loans);

            return JsonConvert.SerializeObject(updatedAwardsViewModel);

        }


        /// <summary>
        /// Updates a single award/other loan
        /// </summary>
        /// <param name="award">award/other loan to update</param>
        /// <param name="action">accept/reject</param>
        /// <returns>updated award/loan</returns>
        [PageAuthorize("faAwards")]
        public async Task<string> UpdateStudentAwardAsync(string award, string action)
        {
            var studentAwardModel = JsonConvert.DeserializeObject<StudentAward>(award);
            var actionTaken = JsonConvert.DeserializeObject<string>(action);

            //get dummy award periods to add back to the view model            

            var studentId = studentAwardModel.StudentId;
            var awardYear = studentAwardModel.AwardYearId;

            var updatedAwardsViewModel = await UpdateAwardsViewModelAsync(studentId, awardYear, actionTaken, new List<StudentAward>() { studentAwardModel });

            return JsonConvert.SerializeObject(updatedAwardsViewModel);
        }

        /// <summary>
        /// Updates all pending awards and loans
        /// </summary>
        /// <param name="awardsAndLoans">a list of pending awards and loans</param>
        /// <param name="action">accept/reject</param>
        /// <returns>updated awards view model</returns>
        [PageAuthorize("faAwards")]
        public async Task<string> UpdateAllAwardsAndLoansAsync(string awardsAndLoans, string action)
        {

            var awardsAndLoansModels = JsonConvert.DeserializeObject<List<StudentAward>>(awardsAndLoans);
            var actionTaken = JsonConvert.DeserializeObject<string>(action);

            var studentId = awardsAndLoansModels.First().StudentId;
            var awardYear = awardsAndLoansModels.First().AwardYearId;

            var updatedAwardsViewModel = await UpdateAwardsViewModelAsync(studentId, awardYear, actionTaken, awardsAndLoansModels);

            return JsonConvert.SerializeObject(updatedAwardsViewModel);
        }

        /// <summary>
        /// Builds award package change requests for declined awards
        /// </summary>
        /// <param name="studentAwards">student awards that were declined</param>
        /// <param name="declinedStatusCode"></param>
        /// <returns>A List of award package change requests to decline awards</returns>
        private static IEnumerable<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest> BuildDeclinedStatusChangeRequests(IEnumerable<StudentAward> studentAwards, string declinedStatusCode)
        {
            var declinedChangeRequestList = new List<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest>();
            foreach (var studentAward in studentAwards.Where(sa => sa.IsStatusModifiable && !sa.IsAwardInReview))
            {
                studentAward.StudentAwardPeriods.RemoveAll(sap => sap.IsDummy);
                var periods = studentAward.StudentAwardPeriods;

                if(!studentAward.StudentAwardPeriods.Any(p => p.AwardStatus.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Estimated
                    || p.AwardStatus.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Pending))
                {
                    periods = studentAward.StudentAwardPeriods.Where(p => p.IsActive).ToList();
                }

                var changeRequest = new Colleague.Dtos.FinancialAid.AwardPackageChangeRequest()
                    {
                        StudentId = studentAward.StudentId,
                        AwardYearId = studentAward.AwardYearId,
                        AwardId = studentAward.Code,
                        AwardPeriodChangeRequests = periods.Where(p => p.IsStatusModifiable)
                        .Select(period =>
                            new Colleague.Dtos.FinancialAid.AwardPeriodChangeRequest()
                            {
                                AwardPeriodId = period.Code,
                                NewAwardStatusId = declinedStatusCode
                            })
                    };
                if (changeRequest.AwardPeriodChangeRequests.Count() > 0)
                {
                    declinedChangeRequestList.Add(changeRequest);
                }
            }
            return declinedChangeRequestList;
        }

        /// <summary>
        /// Builds a list of loan amount change requests
        /// </summary>
        /// <param name="studentAwards">student awards that loan amount change was requested for</param>
        /// <param name="statusCode"></param>
        /// <returns>A list of award package change requests with loan amount changes</returns>
        private static IEnumerable<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest> BuildLoanAmountChangeRequests(IEnumerable<StudentAward> studentAwards, string statusCode)
        {
            var amountChangeRequestList = new List<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest>();
            foreach (var studentAward in studentAwards.Where(sa => sa.IsAmountModifiable && !sa.IsAwardInReview))
            {
                studentAward.StudentAwardPeriods.RemoveAll(sap => sap.IsDummy);

                var changeRequest = new Colleague.Dtos.FinancialAid.AwardPackageChangeRequest()
                {
                    StudentId = studentAward.StudentId,
                    AwardYearId = studentAward.AwardYearId,
                    AwardId = studentAward.Code,
                    AwardPeriodChangeRequests = studentAward.StudentAwardPeriods
                        .Where(p => p.IsAmountModifiable)
                        .Select(period =>
                    new Colleague.Dtos.FinancialAid.AwardPeriodChangeRequest()
                    {
                        AwardPeriodId = period.Code,
                        NewAmount = period.AwardAmount,
                        NewAwardStatusId = statusCode
                    })
                };
                if (changeRequest.AwardPeriodChangeRequests.Count() > 0)
                {
                    amountChangeRequestList.Add(changeRequest);
                }
            }
            return amountChangeRequestList;
        }


        /// <summary>
        /// Builds a list of studentAwardDtos that is passed to the UpdateAwards ServiceClient method
        /// </summary>
        /// <param name="studentAwards">list of awards and/or loans</param>
        /// <param name="awardStatusCode">accept/reject</param>
        /// <param name="actionTaken">Accept/Reject</param>
        /// <param name="outgoingUpdate">Flag to indicate whether the method is invoked on awards that will be sent to the API for an update</param>
        /// <returns>a list of studentAwardDtos</returns>
        private static IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> BuildStudentAwardDtoList(IEnumerable<StudentAward> studentAwards, 
            string awardStatusCode, string actionTaken, bool outgoingUpdate = false)
        {
            var studentAwardDtoList = new List<Colleague.Dtos.FinancialAid.StudentAward>();
            foreach (var studentAward in studentAwards)
            {
                var studentAwardDto = new Colleague.Dtos.FinancialAid.StudentAward();
                studentAwardDto.AwardYearId = studentAward.AwardYearId;
                studentAwardDto.StudentId = studentAward.StudentId;
                studentAwardDto.AwardId = studentAward.Code;
                studentAwardDto.StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>();
                studentAwardDto.IsEligible = studentAward.IsEligible;
                studentAwardDto.IsAmountModifiable = studentAward.IsAmountModifiable;

                studentAward.StudentAwardPeriods.RemoveAll(sap => sap.IsDummy);

                BuildStudentAwardPeriodDtoList(awardStatusCode, studentAward, studentAwardDto, actionTaken, outgoingUpdate);

                studentAwardDtoList.Add(studentAwardDto);
            }
            return studentAwardDtoList;
        }

        /// <summary>
        /// Creates a list of student Award Period Dtos and adds them
        /// to the student award DTO to be sent for an update
        /// </summary>
        /// <param name="newAwardStatusCode">accept/reject</param>
        /// <param name="award">award/loan that is being altered</param>
        /// <param name="actionTaken"> Accept/Reject</param>
        /// <param name="outgoingUpdate">Flag to indicate whether the method is invoked on awards that will be sent to the API for an update</param>
        /// <param name="studentAwardDto">award/loan Dto that is being created</param>
        private static void BuildStudentAwardPeriodDtoList(string newAwardStatusCode, StudentAward award, Colleague.Dtos.FinancialAid.StudentAward studentAwardDto, 
            string actionTaken, bool outgoingUpdate = false)
        {
            var periods = award.StudentAwardPeriods;
            //Pass only the selected award periods if attempting to reject non-pending awards that will be sent to the API for an update
            if (outgoingUpdate && (!periods.Any(p => p.AwardStatus.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Estimated 
                || p.AwardStatus.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Pending))
                && !string.IsNullOrEmpty(actionTaken) && actionTaken.ToUpper() == "REJECT")
            {
                periods = award.StudentAwardPeriods.Where(ap => ap.IsActive).ToList();
            }
            foreach (var studentAwardPeriodModel in periods)
            {
                var studentAwardPeriodDto = new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                {
                    AwardYearId = studentAwardDto.AwardYearId,
                    StudentId = studentAwardDto.StudentId,
                    AwardId = studentAwardDto.AwardId,
                    AwardPeriodId = studentAwardPeriodModel.Code,
                    AwardAmount = AssignAwardPeriodAwardAmount(award, actionTaken, studentAwardPeriodModel),
                    AwardStatusId = !string.IsNullOrEmpty(newAwardStatusCode) && studentAwardPeriodModel.IsStatusModifiable ? newAwardStatusCode : studentAwardPeriodModel.AwardStatus.Code,
                    IsTransmitted = studentAwardPeriodModel.IsTransmitted,
                    IsFrozen = studentAwardPeriodModel.IsFrozen,
                    IsAmountModifiable = studentAwardPeriodModel.IsAmountModifiable,
                    IsStatusModifiable = studentAwardPeriodModel.IsStatusModifiable,
                    IsIgnoredOnAwardLetter = studentAwardPeriodModel.IsIgnoredOnAwardLetter
                };

                studentAwardDto.StudentAwardPeriods.Add(studentAwardPeriodDto);
            }
        }

        /// <summary>
        /// Assigns award amount to the passed in award period based on update action
        /// and a set of award period properties
        /// </summary>
        /// <param name="award">award award period is part of</param>
        /// <param name="actionTaken">update action taken on the award</param>
        /// <param name="studentAwardPeriodModel">student award period that we are calculating award amount for</param>
        /// <returns>award amount</returns>
        private static decimal? AssignAwardPeriodAwardAmount(StudentAward award, string actionTaken, StudentAwardPeriod studentAwardPeriodModel)
        {
            //Return the original award period amount if user is rejecting the award, award status is not modifiable and original amount has value
            if ((actionTaken.ToUpper() == "REJECT" && !award.IsStatusModifiable) && studentAwardPeriodModel.OriginalAwardAmount.HasValue)
            {
                return studentAwardPeriodModel.OriginalAwardAmount.Value;
            }
            /* Return studentAwardPeriodModel award amount(which can be a new amount the user entered) in case the update action is "Accept" or "Change",
                and the original amount is not null or current award amount is not zero
                case 1: original amount is not null - we are accepting the award either at the original amount(if the AwardAmount was changed, it will be equal to the original amount) or new amount;
                    if it were null, we want to pass null back(see else clause);
                case 2: studentAwardPeriodModel.AwardAmount != 0 - we are accepting the award award amount of which hs changed - thus, return the new amount
             */
            else if (actionTaken.ToUpper() != "REJECT" && (studentAwardPeriodModel.OriginalAwardAmount.HasValue || studentAwardPeriodModel.AwardAmount != 0))
            {
                return studentAwardPeriodModel.AwardAmount;
            }
            //Otherwise return null
            else
            {
                return new Nullable<decimal>();
            }
        }

        /// <summary>
        /// Get the status code to use when accepting or rejecting based on the configuration or, if not specified in the configuration,
        /// the first matching status with the right status category.
        /// </summary>
        /// <param name="userAction">The action button the user clicked (Accept or Reject). If the user clicked a different button, that is ignored here.</param>
        /// <param name="configuration">The configuration associated to the award year and studentId the user is operating on.</param>
        /// <param name="awardStatuses">The award status from the API</param>
        /// <returns></returns>
        private string GetAwardStatusCodeFromUserAction(string userAction, Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 configuration, IEnumerable<Colleague.Dtos.FinancialAid.AwardStatus> awardStatuses)
        {
            if (string.IsNullOrEmpty(userAction))
            {
                return string.Empty;
            }
            if (userAction.ToUpper() == "ACCEPT")
            {
                if (configuration != null && !string.IsNullOrEmpty(configuration.AcceptedAwardStatusCode))
                {
                    return configuration.AcceptedAwardStatusCode;
                }
                else
                {
                    var status = (awardStatuses != null) ? awardStatuses.FirstOrDefault(s => s.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Accepted) : null;
                    if (status != null)
                    {
                        return status.Code;
                    }
                    else
                    {
                        var message = string.Format("No Accepted Award Status code exists to accept the award(s)");
                        Logger.Error(message);
                        throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }

            }
            if (userAction.ToUpper() == "REJECT")
            {
                if (configuration != null && !string.IsNullOrEmpty(configuration.RejectedAwardStatusCode))
                {
                    return configuration.RejectedAwardStatusCode;
                }
                else
                {
                    var status = (awardStatuses != null) ? awardStatuses.FirstOrDefault(s => s.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Rejected || s.Category == Colleague.Dtos.FinancialAid.AwardStatusCategory.Denied) : null;
                    if (status != null)
                    {
                        return status.Code;
                    }
                    else
                    {
                        var message = string.Format("No Rejected Award Status code exists to accept the award(s)");
                        Logger.Error(message);
                        throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Updates the awards view model
        /// </summary>
        /// <param name="studentId">student id</param>
        /// <param name="awardYear">award year</param>
        /// <param name="actionTaken">action taken for updated awards</param>
        /// <param name="updatedStudentAwards">a list of awards that were updated</param>
        /// <returns>An updated AwardsViewModel</returns>
        private async Task<AwardsViewModel> UpdateAwardsViewModelAsync(string studentId, string awardYear, string actionTaken, IEnumerable<StudentAward> updatedStudentAwards)
        {
            var dummyStudentAwardPeriodModels = new List<StudentAwardPeriod>(updatedStudentAwards.SelectMany(a => a.StudentAwardPeriods).Where(sap => sap.IsDummy));

            var studentAwardYear = ServiceClient.GetStudentAwardYear2(studentId, awardYear);
            var configuration = (await ServiceClient.GetFinancialAidOffices3Async()).SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYear.FinancialAidOfficeId && c.AwardYear == awardYear);
            var awardStatuses = ServiceClient.GetAwardStatuses();

            var awardStatusCode = GetAwardStatusCodeFromUserAction(actionTaken, configuration, awardStatuses);

            Colleague.Dtos.FinancialAid.StudentAwardPackage updatedStudentAwardPackage = new Colleague.Dtos.FinancialAid.StudentAwardPackage();
            var updatedStudentAwardDtos = new List<Colleague.Dtos.FinancialAid.StudentAward>();

            //To hold all the awards we don't need to PUT
            var nonUpdatedAwards = new List<StudentAward>();

            #region ReviewDeclinedAwards
            //Award decline requests need to be placed in the holding bin
            if (configuration.IsDeclinedStatusChangeRequestRequired && actionTaken.ToUpper() == "REJECT")
            {
                updatedStudentAwardPackage.StudentAwards = BuildStudentAwardDtoList(updatedStudentAwards, string.Empty, actionTaken);
                //Get only the awards with modifiable status
                var awardsToDecline = updatedStudentAwards.Where(usa => usa.IsStatusModifiable);

                //Create award package change requests
                var declinedChangeRequests = BuildDeclinedStatusChangeRequests(awardsToDecline, awardStatusCode);
                foreach (var changeRequest in declinedChangeRequests)
                {
                    await ServiceClient.CreateAwardPackageChangeRequestAsync(studentId, changeRequest);
                }
                updatedStudentAwardDtos.AddRange(updatedStudentAwardPackage.StudentAwards);
            }
            #endregion
            #region ReviewAmountChanges
            //Loan amount change requests need to be placed in the holding bin
            else if (configuration.IsLoanAmountChangeRequestRequired && actionTaken.ToUpper() != "REJECT")
            {
                var amountChangeAwards = updatedStudentAwards.Where(sa => sa.LoanType.HasValue && sa.IsAmountModifiable && sa.StudentAwardPeriods.Any(p => p.OriginalAwardAmount.HasValue && p.OriginalAwardAmount.Value != p.AwardAmount));
                nonUpdatedAwards.AddRange(amountChangeAwards);

                var unmodifiableAwards = updatedStudentAwards.Where(sa => !sa.IsAmountModifiable && !sa.IsStatusModifiable);
                nonUpdatedAwards.AddRange(unmodifiableAwards);
                var nonUpdatedAwardDtos = BuildStudentAwardDtoList(nonUpdatedAwards, awardStatusCode, actionTaken);

                var allOtherAwards = updatedStudentAwards.Except(nonUpdatedAwards).ToList();
                var studentAwardDtos = BuildStudentAwardDtoList(allOtherAwards, awardStatusCode, actionTaken);

                //Put only those awards that are ok to be updated without being put in the holding bin
                updatedStudentAwardPackage =
                ServiceClient.PutStudentAwardPackage(studentId, awardYear,
                new Colleague.Dtos.FinancialAid.StudentAwardPackage() { StudentAwards = studentAwardDtos });

                if (amountChangeAwards.Count() > 0)
                {
                    var amountChangeRequests = BuildLoanAmountChangeRequests(amountChangeAwards, awardStatusCode);
                    foreach (var changeRequest in amountChangeRequests)
                    {
                        await ServiceClient.CreateAwardPackageChangeRequestAsync(studentId, changeRequest);
                    }
                }
                updatedStudentAwardDtos.AddRange(updatedStudentAwardPackage.StudentAwards);
                updatedStudentAwardDtos.AddRange(nonUpdatedAwardDtos);
            }
            #endregion
            #region No review required
            else
            {
                //Put awards that can and cannot be modified into two separate lists
                var unmodifiableAwards = updatedStudentAwards.Where(sa => !sa.IsStatusModifiable && !sa.IsAmountModifiable);
                var modifiableAwards = updatedStudentAwards.Except(unmodifiableAwards);

                var studentAwardDtos = BuildStudentAwardDtoList(modifiableAwards, awardStatusCode, actionTaken, true);

                updatedStudentAwardPackage =
                ServiceClient.PutStudentAwardPackage(studentId, awardYear,
                new Colleague.Dtos.FinancialAid.StudentAwardPackage() { StudentAwards = studentAwardDtos });

                updatedStudentAwardDtos.AddRange(updatedStudentAwardPackage.StudentAwards);
                updatedStudentAwardDtos.AddRange(BuildStudentAwardDtoList(unmodifiableAwards, awardStatusCode, actionTaken));
            }
            #endregion
            var updatedAwardsViewModel = await GetUpdatedAwardsViewModelAsync(studentId, awardYear, updatedStudentAwardDtos);
            updatedAwardsViewModel.AddDummyStudentAwardPeriods(dummyStudentAwardPeriodModels);
            return updatedAwardsViewModel;
        }

        /// <summary>
        /// Gets all necessary pieces of data to build an updated awards view model and 
        /// builds the awardsViewModel
        /// </summary>
        /// <param name="studentId">student id</param>
        /// <param name="studentAwardDtoList">list of studentAwardDtos</param>
        /// <returns>updated AwardsViewModel</returns>
        private async Task<AwardsViewModel> GetUpdatedAwardsViewModelAsync(string studentId, string awardYear, IEnumerable<Colleague.Dtos.FinancialAid.StudentAward> updatedStudentAwards)
        {
            var person = GetFinancialAidPerson(studentId);

            /* Determine if the current user is the student/applicant.
             * The alternatives could be the current user is an fa counselor or proxy
             * retrieving student/applicant info
             */
            bool isUserSelf = false;
            bool isProxy = CurrentUser != null && CurrentUser.ProxySubjects.Any();
            if (person != null && person.Id.Equals(GetEffectivePersonId()) && !isProxy)
            {
                isUserSelf = true;
            }

            if (person == null)
            {
                throw new HttpException(403, "Only Students and Applicants may access the Financial Aid Hub");
            }

            var awards = ServiceClient.GetAwards2();
            var awardStatuses = ServiceClient.GetAwardStatuses();
            var awardPeriods = ServiceClient.GetAwardPeriods();

            var officeCodes = new List<Colleague.Dtos.Base.OfficeCode>();
            var communicationCodes = new List<Colleague.Dtos.Base.CommunicationCode2>();
            var studentFaApplications = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>();
            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentLoanSummary = new Colleague.Dtos.FinancialAid.StudentLoanSummary();
            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var studentDocuments = new List<Colleague.Dtos.FinancialAid.StudentDocument>();

            var studentLoanLimitations = new List<Colleague.Dtos.FinancialAid.StudentLoanLimitation>();
            var studentPendingLoanRequests = new List<StudentLoanRequest>();
            var awardPackageChangeRequests = new List<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest>();
            var studentChecklists = new List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist>();
            var financialAidChecklistItems = new List<Colleague.Dtos.FinancialAid.ChecklistItem>();
            var academicProgressEvaluations = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            var academicProgressStatuses = new List<Colleague.Dtos.FinancialAid.AcademicProgressStatus>();
            var studentAwardLetters = new List<Colleague.Dtos.FinancialAid.AwardLetter2>();

            try { officeCodes = ServiceClient.GetOfficeCodes().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { communicationCodes = ServiceClient.GetCommunicationCodes2().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetStudentFafsas(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetProfileApplications(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentLoanSummary = await ServiceClient.GetStudentLoanSummaryAsync(studentId); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>() { ServiceClient.GetStudentAwardYear2(studentId, awardYear) }; }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentDocuments = (await ServiceClient.GetStudentDocumentsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentLoanLimitations = (await ServiceClient.GetStudentLoanLimitationsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { awardPackageChangeRequests = ServiceClient.GetAwardPackageChangeRequests(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentChecklists = (await ServiceClient.GetAllStudentChecklistsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidChecklistItems = ServiceClient.GetFinancialAidChecklistItems().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { academicProgressEvaluations = (await ServiceClient.GetStudentAcademicProgressEvaluations2Async(studentId)).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { academicProgressStatuses = (await ServiceClient.GetAcademicProgressStatusesAsync()).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { studentAwardLetters = (await ServiceClient.GetAwardLetters3Async(studentId)).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            //send override dummy award period ids
            var updatedAwardsViewModel = new AwardsViewModel(person, updatedStudentAwards, studentLoanLimitations, studentLoanSummary,
                awards, awardPeriods, awardStatuses, studentAwardYears, studentDocuments, studentAwardLetters, communicationCodes, officeCodes, studentFaApplications, null,
                financialAidOffices, studentPendingLoanRequests, awardPackageChangeRequests, studentChecklists, financialAidChecklistItems, academicProgressEvaluations, 
                academicProgressStatuses, CurrentUser, isUserSelf);

            await SetPrivacyPropertiesForAdminModel(updatedAwardsViewModel);

            return updatedAwardsViewModel;
        }
    }
}
