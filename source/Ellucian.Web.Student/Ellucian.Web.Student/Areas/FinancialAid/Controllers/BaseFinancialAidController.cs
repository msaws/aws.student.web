﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Infrastructure;
using slf4net;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Mvc.Menu;
using System.Web;
using System.Net;
using Ellucian.Web.Mvc.Filter;
using System.Web.Mvc;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    public class BaseFinancialAidController : BaseStudentController
    {
        private readonly ISiteService siteService;


        protected BaseFinancialAidController(Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger)
        {
            this.siteService = siteService;
         }

        /// <summary>
        /// Generic error page to be returned whenever an error occurs
        /// </summary>
        /// <returns>Error view</returns>
        [LinkHelp]
        public ActionResult Error()
        {
           return View("~/Views/Shared/Error");
        }

        /// <summary>
        /// Checks the CurrentUser's Roles to see if the list contains the Financial Aid Counselor Role
        /// </summary>
        /// <returns>True, if the current user is an admin Financial Aid Counselor. False, otherwise.</returns>
        protected bool IsCurrentUserAdmin()
        {
            var counselorRoles = siteService.Get(CurrentUser).Submenus.Where(m => m.Id == "financial-aid-admin").SelectMany(m => m.AllowedRoles);
            return CurrentUser.Roles.Any(r => counselorRoles.Contains(r, StringComparer.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Queries the API for the given student's Person data.
        /// This method tries to get a Student DTO first. If that fails, this method
        /// tries to get an Applicant DTO. If that fails, this method returns null.
        /// </summary>
        /// <param name="personId">The Id of the Person</param>
        /// <returns>A Person DTO, or null.</returns>
        protected Colleague.Dtos.Base.Person GetFinancialAidPerson(string personId)
        {
            try
            {
                var student = ServiceClient.GetStudent(personId);
                return student;
            }
            catch (ResourceNotFoundException rnfe)
            {
                Logger.Error(rnfe, string.Format("Person with ID {0} is not a Student", personId));
            }

            try
            {
                var applicant = ServiceClient.GetApplicant(personId);
                return applicant;
            }
            catch (ResourceNotFoundException rnfe)
            {
                Logger.Error(rnfe, string.Format("Person with ID {0} is not an Applicant", personId));
            }

            return null;
        }

        protected Colleague.Dtos.FinancialAid.FinancialAidCounselor GetFinancialAidCounselor(Colleague.Dtos.Base.Person person)
        {
            var counselorId = "";
            if (person.GetType() == typeof(Colleague.Dtos.Student.Student) || person.GetType() == typeof(Colleague.Dtos.Student.Applicant))
            {
                counselorId = (person.GetType() == typeof(Colleague.Dtos.Student.Student))
                    ? ((Colleague.Dtos.Student.Student)person).FinancialAidCounselorId
                    : ((Colleague.Dtos.Student.Applicant)person).FinancialAidCounselorId;
            }


            Colleague.Dtos.FinancialAid.FinancialAidCounselor counselor = null;
            try
            {
                counselor = ServiceClient.GetFinancialAidCounselor(counselorId);
            }
            catch (Exception e) { Logger.Error(e, e.Message); }

            return counselor;
        }

        /// <summary>
        /// If there is no studentId passed, gets the effective person id(current user or proxy subject) and attempts to get
        /// student/applicant information for that id. If no data was returned, throws an appropriate exception
        /// </summary>
        /// <param name="id">id passed to the initial call to get viewModel info</param>
        /// <param name="studentId">student id we will get info for</param>
        /// <returns>person dto containing info about person with associated student id</returns>
        protected Colleague.Dtos.Base.Person GetPersonData(string id, ref string studentId)
        {
            //Id is null? try to get effective person id - counselor can be a student as well
            if (string.IsNullOrEmpty(studentId))
            {
                studentId = GetEffectivePersonId();
            }

            //Attempt to get student/applicant data
            var person = GetFinancialAidPerson(studentId);

            if (person == null)
            {
                //Current user is admin, id was provided and does not equal counselor's person id => must be invalid
                if (IsCurrentUserAdmin() && !string.IsNullOrEmpty(id) && !id.Equals(GetEffectivePersonId()))
                {
                    throw new HttpException((int)HttpStatusCode.NotFound, string.Format("Provided id {0} is invalid.", id));
                }
                //Current user is admin but the id was not provided and admin is not student/applicant themselves
                else if (IsCurrentUserAdmin() && string.IsNullOrEmpty(id))
                {
                    throw new HttpException((int)HttpStatusCode.BadRequest, "StudentId is required in request.");
                }
                //Current user is not a student or applicant but is attempting to get fa data for the specified id
                else
                {
                    throw new HttpException((int)HttpStatusCode.Forbidden, "Only Students and Applicants may access the Financial Aid Hub");
                }
            }
            return person;
        }

        /// <summary>
        /// This method checks if the given personId is a Student or Applicant, as known to the API.
        /// </summary>
        /// <param name="personId">The Colleague PERSON id.</param>
        /// <returns>True, if a Student or Applicant DTO is successfully retrieved. False, otherwise.</returns>
        protected bool IsStudentOrApplicant(string personId)
        {
            try
            {
                ServiceClient.GetStudent(personId);
                return true;
            }
            catch (ResourceNotFoundException rnfe)
            {
                Logger.Error(rnfe, string.Format("Person with ID {0} is not a Student", personId));
            }

            try
            {
                ServiceClient.GetApplicant(personId);
                return true;
            }
            catch (ResourceNotFoundException rnfe)
            {
                Logger.Error(rnfe, string.Format("Person with ID {0} is not an Applicant", personId));
            }

            return false;
        }

        /// <summary>
        /// Determines if the current user is self or not - can be admin, proxy, student/applicant(self), 
        /// or admin and student/applicant(self)
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        protected bool IsUserSelf(Colleague.Dtos.Base.Person person)
        {
            bool isUserSelf = false;
            bool isProxy = CurrentUser != null && CurrentUser.ProxySubjects.Any();
            if (person != null && person.Id.Equals(GetEffectivePersonId()) && !isProxy)
            {
                isUserSelf = true;
            }
            return isUserSelf;
        }

        /// <summary>
        /// Sets the privacy properties of a <see cref="FinancialAidAdminModel"/> object.
        /// </summary>
        public async Task SetPrivacyPropertiesForAdminModel(FinancialAidAdminModel model)
        {
            if (model == null)
            {
                return;
            }

            // If no privacy code, the data does not have a privacy restriction or associated message
            if (string.IsNullOrEmpty(model.PrivacyStatusCode))
            {
                model.HasPrivacyRestriction = false;
                model.PrivacyMessage = null;
                return;
            }
            else
            {
                // Users have access to their own data and person proxy has access to person's data
                if (CurrentUser.IsPerson(model.PersonId) || model.PersonId == GetEffectivePersonId())
                {
                    model.HasPrivacyRestriction = false;
                    model.PrivacyMessage = null;
                }
                else
                {
                    // Administrators must have access to the privacy code in order to view the data;
                    // access the admin user's staff record (if available) to determine their privacy access privileges
                    Staff staff = null;
                    try
                    {
                        staff = await ServiceClient.GetStaffAsync(CurrentUser.PersonId);
                    }
                    catch
                    {
                        staff = new Staff(CurrentUser.PersonId, string.Empty) { PrivacyCodes = new List<string>() };
                    }

                    // Data is private if admin user does have access to the privacy code
                    model.HasPrivacyRestriction = !staff.PrivacyCodes.Contains(model.PrivacyStatusCode);
                    if (model.HasPrivacyRestriction)
                    {
                        // Use the record denial message from PID5 if the user does not have access to the data
                        PrivacyConfiguration privacyConfig = await ServiceClient.GetPrivacyConfigurationAsync();
                        model.PrivacyMessage = privacyConfig != null ? privacyConfig.RecordDenialMessage : string.Empty;
                    }
                    else
                    {
                        // Use the privacy warning message from PID5 if the user has access to the data
                        model.PrivacyMessage = await ServiceClient.GetCachedPrivacyMessageAsync(model.PrivacyStatusCode);
                    }
                }
                return;
            }
        }
    }
}