﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Newtonsoft.Json;
using slf4net;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// OutsideAwardsController to handle outside awards data
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class OutsideAwardsController : BaseFinancialAidController
    {
        public OutsideAwardsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {

        }

        /// <summary>
        /// Action method for the Outside Awards page.
        /// </summary>
        /// <returns>OutsideAwards view</returns>
        [LinkHelp]
        [PageAuthorize("faOutsideAwards")]
        public ActionResult Index()
        {
            ViewBag.Title = "Outside Awards";
            return View("OutsideAwards");
        }

        /// <summary>
        /// Action method for the Admin Outside Awards view
        /// </summary>
        /// <param name="id">student id</param>
        /// <returns>OutsideAwards view</returns>
        [LinkHelp]
        [PageAuthorize("faOutsideAwardsAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Outside Awards";
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("OutsideAwards");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("OutsideAwards");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Gets OutsideAwardsViewModel with general data (no outside awards data)
        /// </summary>
        /// <param name="id">studentId</param>
        /// <returns>outside awards view model json string</returns>
        [PageAuthorize("faHome")]
        public async Task<string> GetOutsideAwardsViewModelAsync(string id) 
        {
            var studentId = id;
            
            var person = GetPersonData(id, ref studentId);

            var counselor = GetFinancialAidCounselor(person);

            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            OutsideAwardsViewModel outsideAwardsViewModel = new OutsideAwardsViewModel(person, studentAwardYears, financialAidOffices, counselor, IsUserSelf(person));

            await SetPrivacyPropertiesForAdminModel(outsideAwardsViewModel);

            return JsonConvert.SerializeObject(outsideAwardsViewModel);
        }

        /// <summary>
        /// Gets outside awards for the specified year for the current student
        /// </summary>
        /// <param name="outsideAwardsViewModel">existing OutsideAwardsViewModel with general data</param>
        /// <param name="awardYearCode">award year to retrieve awards for</param>
        /// <returns>updates outside awards view model json string</returns>
        [PageAuthorize("faHome")]
        public async Task<string> GetOutsideAwardsAsync(string outsideAwardsViewModel, string awardYearCode)
        {
            if (string.IsNullOrEmpty(awardYearCode))
            {
                throw new ArgumentNullException("awardYearCode");
            }
            
            var awardYear = JsonConvert.DeserializeObject<string>(awardYearCode);            
           
            var studentOutsideAwardsViewModel = JsonConvert.DeserializeObject<OutsideAwardsViewModel>(outsideAwardsViewModel);
            
            var studentId = studentOutsideAwardsViewModel.Student.Id;

            List<Colleague.Dtos.FinancialAid.OutsideAward> outsideAwardDtos = new List<Colleague.Dtos.FinancialAid.OutsideAward>();
            try
            {
                outsideAwardDtos = (await ServiceClient.GetOutsideAwardsAsync(studentId, awardYear)).ToList();
            }
            catch (Exception e) { Logger.Info(e, e.Message); }

            studentOutsideAwardsViewModel.UpdateOutsideAwardsViewModel(outsideAwardDtos);
            return JsonConvert.SerializeObject(studentOutsideAwardsViewModel);            
        }

        /// <summary>
        /// Creates a new outside award
        /// </summary>
        /// <param name="newOutsideAward">new outside award in json string format</param>
        /// <returns>json string representation of the created outside award</returns>
        [PageAuthorize("faHome")]
        public async Task<string> CreateOutsideAwardAsync(string newOutsideAward)
        {
            var outsideAward = JsonConvert.DeserializeObject<Ellucian.Colleague.Dtos.FinancialAid.OutsideAward>(newOutsideAward);

            var createdOutsideAward = await ServiceClient.CreateOutsideAwardAsync(outsideAward);

            return JsonConvert.SerializeObject(createdOutsideAward);            
        }

        /// <summary>
        /// Deletes the outside award record for the specified id
        /// </summary>
        /// <param name="studentId">student id award belongs to</param>
        /// <param name="outsideAwardId">outside award record id</param>
        /// <returns></returns>
        [PageAuthorize("faHome")]
        public async Task DeleteOutsideAwardAsync(string studentId, string outsideAwardId)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException("studentId is required");
            }
            if (string.IsNullOrEmpty(outsideAwardId))
            {
                throw new ArgumentNullException("outsideAwardId is required");
            }

            string sid = JsonConvert.DeserializeObject<string>(studentId);
            string id = JsonConvert.DeserializeObject<string>(outsideAwardId);

            await ServiceClient.DeleteOutsideAwardAsync(sid, id);            
        }

        /// <summary>
        /// Updates existing outside award 
        /// </summary>
        /// <param name="outsideAwardToUpdate">outside award to update in json string format</param>
        /// <returns>updated outside award in json string format</returns>
        [PageAuthorize("faHome")]
        public async Task<string> UpdateOutsideAwardAsync(string outsideAwardToUpdate)
        {
            var outsideAward = JsonConvert.DeserializeObject<Ellucian.Colleague.Dtos.FinancialAid.OutsideAward>(outsideAwardToUpdate);

            var updatedOutsideAward = await ServiceClient.UpdateOutsideAwardAsync(outsideAward);

            return JsonConvert.SerializeObject(updatedOutsideAward);            
        }
    }
}