﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Filters;
using Newtonsoft.Json;
using slf4net;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Mvc.Menu;
using System.Threading.Tasks;
using System.Net;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Licensing;
using Ellucian.Web.Student.Models.Person;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class HomeController : BaseFinancialAidController
    {

        private readonly ISiteService siteService;
        
        /// <summary>
        /// Creates a new Home Controller instance
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public HomeController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {
            this.siteService = siteService;
        }

        /// <summary>
        /// Action method for the HomePage of FinancialAid Hub
        /// </summary>
        /// <returns>Index View</returns>
        [LinkHelp]
        [PageAuthorize("faHome")]
        public ActionResult Index()
        {
            ViewBag.Title = "Financial Aid";

            return View();
        }

        [LinkHelp]
        [PageAuthorize("faHomeAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Home";            
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("Index");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("Home");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Gets Home View Model information
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns>Home View Model</returns>
        [PageAuthorize("faHome")]
        public async Task<string> GetHomeViewModelAsync(string id)
        {
            var studentId = id;

            //Get the person data
            var person = GetPersonData(id, ref studentId);
            
            /* Determine if the current user is the student/applicant.
             * The alternatives could be the current user is an fa counselor or proxy
             * retrieving student/applicant info
             */
            bool isUserSelf = IsUserSelf(person);
            bool isUserAdminOnly = IsCurrentUserAdmin() && !isUserSelf;

            var counselor = GetFinancialAidCounselor(person);

            var awards = ServiceClient.GetAwards2();
            var awardStatuses = ServiceClient.GetAwardStatuses();

            var officeCodes = new List<Colleague.Dtos.Base.OfficeCode>();
            var communicationCodes = new List<Colleague.Dtos.Base.CommunicationCode2>();
            var usefulLinks = new List<Colleague.Dtos.FinancialAid.Link>();
            var studentFaApplications = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>();
            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentLoanSummary = new Colleague.Dtos.FinancialAid.StudentLoanSummary();
            var ipedsInstitutions = new List<Colleague.Dtos.FinancialAid.IpedsInstitution>();
            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var studentDocuments = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
            var studentAwards = new List<Colleague.Dtos.FinancialAid.StudentAward>();
            var averagePackages = new List<Colleague.Dtos.FinancialAid.AverageAwardPackage>();
            var studentAwardLetters = new List<Colleague.Dtos.FinancialAid.AwardLetter2>();
            var studentChecklists = new List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist>();
            var financialAidChecklistItems = new List<Colleague.Dtos.FinancialAid.ChecklistItem>();
            var counselorPhoneNumbers = new Colleague.Dtos.Base.PhoneNumber();
            var academicProgressEvaluations = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            var academicProgressStatuses = new List<Colleague.Dtos.FinancialAid.AcademicProgressStatus>();
            Ellucian.Colleague.Dtos.Finance.AccountDue.AccountDue studentAccountDue = null;
            Colleague.Dtos.FinancialAid.StudentNsldsInformation studentNsldsInformation = null;

            try { officeCodes = ServiceClient.GetOfficeCodes().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { communicationCodes = ServiceClient.GetCommunicationCodes2().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { usefulLinks = ServiceClient.GetLinks().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetStudentFafsas(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetProfileApplications(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentLoanSummary = await ServiceClient.GetStudentLoanSummaryAsync(studentId); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            if (studentLoanSummary != null &&
                studentLoanSummary.StudentLoanHistory != null &&
                studentLoanSummary.StudentLoanHistory.Count() > 0)
            {
                try
                {
                    ipedsInstitutions = (await ServiceClient.GetIpedsInstitutionsAsync(studentLoanSummary.StudentLoanHistory.Select(h => h.OpeId))).ToList();
                }
                catch (Exception e) { Logger.Info(e, e.Message); }
            }

            try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentDocuments = (await ServiceClient.GetStudentDocumentsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwards = ServiceClient.GetStudentAwards(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { averagePackages = ServiceClient.GetAverageAwardPackages(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); averagePackages = null; }

            try { studentAwardLetters = (await ServiceClient.GetAwardLetters3Async(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentChecklists = await GetStudentChecklistsAsync(financialAidOffices, studentAwardYears, studentId, isUserSelf); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidChecklistItems = ServiceClient.GetFinancialAidChecklistItems().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { counselorPhoneNumbers = ServiceClient.GetPersonPhones(counselor.Id); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { academicProgressEvaluations = (await ServiceClient.GetStudentAcademicProgressEvaluations2Async(studentId)).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { academicProgressStatuses = (await ServiceClient.GetAcademicProgressStatusesAsync()).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            if (studentChecklists.Any() && !studentAwardYears.Any())
            {
                try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); }

                try { averagePackages = ServiceClient.GetAverageAwardPackages(studentId).ToList(); }
                catch (Exception e) { Logger.Info(e, e.Message); averagePackages = null; }

            }

            try { studentNsldsInformation = await ServiceClient.GetStudentNsldsInformationAsync(studentId); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            var site = siteService.Get(CurrentUser);
            try
            {
                if (SiteAccessUtility.IsInformationAccessible(site, "Finance",  new List<string>(){"acctSummary"}))
                {
                    studentAccountDue = ServiceClient.GetPaymentsDueByTermForStudent(studentId);
                }
            }
            catch (Exception e) { Logger.Info(e, e.Message); }

            var homeViewModel = new HomeViewModel(
                person,
                studentAwardYears,
                studentDocuments,
                communicationCodes,
                officeCodes,
                studentAwards,
                awards,
                awardStatuses,
                studentAwardLetters,
                studentFaApplications,
                studentLoanSummary,
                ipedsInstitutions,
                averagePackages,
                usefulLinks,
                counselor,
                financialAidOffices,
                studentChecklists,
                financialAidChecklistItems,
                counselorPhoneNumbers,
                academicProgressEvaluations,
                academicProgressStatuses,
                studentNsldsInformation,
                studentAccountDue,
                CurrentUser,
                isUserAdminOnly);

            var checklistItems = homeViewModel.StudentChecklists.SelectMany(c => c.ChecklistItems);
            
            foreach (var item in checklistItems)
            {
                var pageId = string.Empty;
                if (item.Type == ChecklistItemType.Documents)
                {
                    pageId = isUserAdminOnly ? "faDocumentsAdmin" : "faDocuments";
                }
                else if (item.Type == ChecklistItemType.Awards)
                {
                    pageId = isUserAdminOnly ? "faAwardsAdmin" : "faAwards";
                }
                else if (item.Type == ChecklistItemType.AwardLetter)
                {
                    pageId = isUserAdminOnly ? "faAwardLetterAdmin" : "faAwardLetter";
                }

                if (!string.IsNullOrEmpty(pageId))
                {
                    var page = site.Pages.FirstOrDefault(p => p.Id == pageId);
                    string action = string.Empty;
                    string sId = string.Empty;
                    if (isUserAdminOnly)
                    {
                        action = page.Action;
                        sId = studentId;
                    }
                    item.Link = (page != null) ? Url.Action(action, page.Controller, new { id = sId }) : "#";
                }
            }

            await SetPrivacyPropertiesForAdminModel(homeViewModel);

            return JsonConvert.SerializeObject(homeViewModel);
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="financialAidOffices"></param>
        /// <param name="studentAwardYears"></param>
        /// <param name="dfltStudentId"></param>
        /// <returns></returns>
        private async Task<List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist>> GetStudentChecklistsAsync(IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOffices, 
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYears, string dfltStudentId, bool isUserSelf)
        {
            var configurations = financialAidOffices.SelectMany(o => o.Configurations);
            var activeStudentAwardYears = studentAwardYears.GetActiveStudentAwardYearDtos(configurations);
            
            //Default office is a required field - it should always exist
            var dfltOfficeRecord = financialAidOffices.Where(f => f.IsDefault == true).First();

            var dfltYear = dfltOfficeRecord.Configurations.OrderByDescending(c => c.AwardYear).FirstOrDefault();
            var dfltOfficeCode = dfltOfficeRecord.Id;
            var dfltCreateFlag = dfltYear != null ? dfltOfficeRecord.Configurations.FirstOrDefault(c => c.AwardYear == dfltYear.AwardYear).CreateChecklistItemsForNewStudent : false;
            var studentChecklists = new List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist>();

            if (activeStudentAwardYears == null || activeStudentAwardYears.Count() == 0)
            {
                // if the student has no activeStudentAwardYears and the Create Checklist Flag is yes we want to create
                // the checklist items
                if (dfltCreateFlag == true)
                {
                    //If user is student/applicant for whom retrieving data, create a checklist for the year
                    if (isUserSelf)
                    {
                        studentChecklists.Add(await ServiceClient.CreateStudentFinancialAidChecklistAsync(dfltStudentId, dfltYear != null ? dfltYear.AwardYear : null));
                    }                    
                }
                else 
                {
                    Logger.Info("No active student award years");
                    
                }
                return studentChecklists;
            }
            var studentId = activeStudentAwardYears.First().StudentId;

            try { studentChecklists = (await ServiceClient.GetAllStudentChecklistsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            //Check if we have studentFinancialAidChecklist for each of the active years in studentAwardYears list
            foreach (var studentAwardYear in activeStudentAwardYears)
            {
                var checklistForYear = studentChecklists.FirstOrDefault(sc => sc.AwardYear == studentAwardYear.Code);

                if (checklistForYear == null || checklistForYear.ChecklistItems == null || checklistForYear.ChecklistItems.Count == 0)
                {
                    //If checklist for the year is not null, we must have received a checklist with no checklist items, remove that one from the list
                    if (checklistForYear != null)
                    {
                        var index = studentChecklists.FindIndex(sc => sc.AwardYear == studentAwardYear.Code);
                        studentChecklists.RemoveAt(index);
                    }
                    //If user is student/applicant for whom retrieving data, create a checklist for the year
                    if (isUserSelf)
                    {
                        try
                        {
                            studentChecklists.Add(await ServiceClient.CreateStudentFinancialAidChecklistAsync(studentId, studentAwardYear.Code));
                        }
                        catch (Exception e) { Logger.Info(e, e.Message); }
                    }
                }
            }

            return studentChecklists;
        }
    }
}