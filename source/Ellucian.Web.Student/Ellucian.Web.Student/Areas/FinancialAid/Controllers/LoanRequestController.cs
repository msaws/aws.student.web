﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Filters;
using Newtonsoft.Json;
using slf4net;
using Ellucian.Web.Mvc.Menu;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// LoanRequestController class for FinancialAid Hub
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class LoanRequestController : BaseFinancialAidController
    {
        /// <summary>
        /// Creates a new LoanRequestController instance
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public LoanRequestController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {

        }

        /// <summary>
        /// Action method for the RequestANewLoan page.
        /// </summary>
        /// <returns>Request a New Loan view</returns>
        [LinkHelp]
        [PageAuthorize("faLoanRequest")]
        public ActionResult Index()
        {
            ViewBag.Title = "Request a New Loan";

            return View("LoanRequest");
        }

        [LinkHelp]
        [PageAuthorize("faLoanRequestAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Request a New Loan";
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id; 
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("LoanRequest");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("LoanRequest");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Gets all loan request related data for the student, creates a LoanRequestViewModel, and
        /// returns the serialized object
        /// </summary>
        /// <param name="id">student id</param>
        /// <returns>serialized LoanRequestViewModel</returns>
        [PageAuthorize("faLoanRequest")]
        public async Task<String> GetLoanRequestViewModelAsync(string id)
        {
            var studentId = id;

            //Get the person data
            var person = GetPersonData(id, ref studentId);

            var counselor = GetFinancialAidCounselor(person);

            var officeCodes = new List<Colleague.Dtos.Base.OfficeCode>();
            var communicationCodes = new List<Colleague.Dtos.Base.CommunicationCode2>();
            var awardYears = new List<Colleague.Dtos.FinancialAid.AwardYear>();
            var studentFaApplications = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>();
            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var counselorPhoneNumbers = new Colleague.Dtos.Base.PhoneNumber();

            var studentDocuments = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
            var studentPendingLoanRequests = new List<StudentLoanRequest>();            

            try { officeCodes = ServiceClient.GetOfficeCodes().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { communicationCodes = ServiceClient.GetCommunicationCodes2().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { awardYears = ServiceClient.GetAwardYears().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetStudentFafsas(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentFaApplications.AddRange(ServiceClient.GetProfileApplications(studentId)); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { counselorPhoneNumbers = ServiceClient.GetPersonPhones(counselor.Id); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentDocuments = (await ServiceClient.GetStudentDocumentsAsync(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }
            

            var loanRequestViewModel = new LoanRequestViewModel(
                person,
                studentAwardYears,
                studentDocuments,
                communicationCodes,
                officeCodes,
                studentFaApplications,
                counselor,
                financialAidOffices,
                //Build model here because we may need to get extra counselor data from ServiceClient
                await BuildStudentLoanRequestModels(studentId, studentAwardYears, counselor, financialAidOffices, counselorPhoneNumbers),
                counselorPhoneNumbers,
                CurrentUser);

            await SetPrivacyPropertiesForAdminModel(loanRequestViewModel);

            return JsonConvert.SerializeObject(loanRequestViewModel);
        }

        /// <summary>
        /// Creates a new loan as per student's/applicant's request
        /// </summary>
        /// <param name="request">stringified StudentLoanRequest object</param>
        /// <returns>Newly created loan request</returns>
        [PageAuthorize("faLoanRequest")]
        public async Task<string> CreateNewLoanRequestAsync(string request)
        {
            if (string.IsNullOrEmpty(request))
            {
                throw new ArgumentNullException("request");
            }

            var loanRequestModel = JsonConvert.DeserializeObject<StudentLoanRequest>(request);
            if (loanRequestModel.TotalRequestAmount <= 0)
            {
                throw new ArgumentException("Loan request total amount must be greater than 0.", "request");
            }
            if (loanRequestModel.TotalRequestAmount != loanRequestModel.LoanRequestAwardPeriods.Sum(p => p.AwardAmount))
            {
                Logger.Error("Loan Request Award Period amounts do not sum to the total requested amount");
            }

            var loanRequestDto = new Colleague.Dtos.FinancialAid.LoanRequest()
            {
                AwardYear = loanRequestModel.AwardYearCode,
                StudentId = loanRequestModel.StudentId,
                TotalRequestAmount = loanRequestModel.LoanRequestAwardPeriods.Sum(p => p.AwardAmount),
                LoanRequestPeriods = loanRequestModel.LoanRequestAwardPeriods.Where(p => p.AwardAmount > 0).Select(periodModel =>
                    new Colleague.Dtos.FinancialAid.LoanRequestPeriod() { Code = periodModel.Code, LoanAmount = periodModel.AwardAmount }).ToList(),
                StudentComments = loanRequestModel.Comments
            };

            var newRequest = ServiceClient.CreateLoanRequest(loanRequestDto);

            var newLoanRequest = new StudentLoanRequest(newRequest);

            var studentId = newRequest.StudentId;

            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentAwardYearDto = new Colleague.Dtos.FinancialAid.StudentAwardYear2();
            var counselorPhoneNumbers = new Colleague.Dtos.Base.PhoneNumber();

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentAwardYearDto = ServiceClient.GetStudentAwardYear2(studentId, loanRequestModel.AwardYearCode); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            var person = GetFinancialAidPerson(studentId);
            if (person == null)
            {
                throw new HttpException(403, "Only Students and Applicants may access the Financial Aid Hub");
            }

            var counselor = GetFinancialAidCounselor(person);

            var assignedToId = newRequest.AssignedToId;

            try { counselorPhoneNumbers = ServiceClient.GetPersonPhones(counselor.Id); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try
            {
                var financialAidCounselorData = counselor;
                if ((financialAidCounselorData == null) ||
                    (financialAidCounselorData != null && assignedToId != financialAidCounselorData.Id))
                {
                    financialAidCounselorData = ServiceClient.GetFinancialAidCounselor(assignedToId);
                }

                newLoanRequest.AssignedToCounselor = new Counselor(financialAidCounselorData, counselorPhoneNumbers, studentAwardYearDto, financialAidOffices);
            }
            catch (Exception e) { Logger.Info(e, e.Message); }

            return JsonConvert.SerializeObject(newLoanRequest);
        }

        /// <summary>
        /// Builds a list of StudentLoanRequest objects for all student years
        /// </summary>
        /// <param name="studentId">student id </param>
        /// <param name="studentAwardYearDtoList">studentAwardYearDto list</param>
        /// <param name="counselorDto">counselor dto</param>
        /// <param name="officeDtoList">office dto</param>
        /// <param name="counselorPhoneNumbers">set of counselor phone numbers</param>
        /// <returns></returns>
        private async Task<IEnumerable<StudentLoanRequest>> BuildStudentLoanRequestModels(
            string studentId,
            IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearDtoList,
            Colleague.Dtos.FinancialAid.FinancialAidCounselor counselorDto,
            IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidOffice3> officeDtoList,
            Colleague.Dtos.Base.PhoneNumber counselorPhoneNumbers)
        {
            //Get default awards periods: these will be either the SA.TERMS from Colleague or 
            //terms from an Attendance Pattern determined from the rules
            var defaultAwardPeriodsList = new List<Colleague.Dtos.FinancialAid.StudentDefaultAwardPeriod>();
            try { defaultAwardPeriodsList = (await ServiceClient.GetStudentDefaultAwardPeriods(studentId)).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            var awardPeriodDtoList = ServiceClient.GetAwardPeriods();

            var studentLoanRequestList = new List<StudentLoanRequest>();
            foreach (var studentAwardYearDto in studentAwardYearDtoList)
            {

                Colleague.Dtos.FinancialAid.LoanRequest loanRequestDto = null;
                //There should be a pending loan for the current year
                if (!string.IsNullOrEmpty(studentAwardYearDto.PendingLoanRequestId))
                {
                    try
                    {
                        loanRequestDto = ServiceClient.GetLoanRequest(studentAwardYearDto.PendingLoanRequestId);
                    }
                    catch (Exception e) { Logger.Info(e, e.Message); }

                    //Loan request received
                    if (loanRequestDto != null)
                    {
                        var assignedToId = loanRequestDto.AssignedToId;
                        var loanRequest = new StudentLoanRequest(loanRequestDto);

                        try
                        {

                            if ((counselorDto == null) ||
                                (counselorDto != null && assignedToId != counselorDto.Id))
                            {
                                counselorDto = ServiceClient.GetFinancialAidCounselor(assignedToId);
                            }

                            loanRequest.AssignedToCounselor = new Counselor(counselorDto, counselorPhoneNumbers, studentAwardYearDto, officeDtoList);
                        }
                        catch (Exception e) { Logger.Info(e, e.Message); }

                        studentLoanRequestList.Add(loanRequest);
                    }
                }

                //No pending loan for current year was found
                if (loanRequestDto == null)
                {

                    var loanRequest = new StudentLoanRequest(studentAwardYearDto, studentAwardYearDto.StudentId, defaultAwardPeriodsList, awardPeriodDtoList);
                    studentLoanRequestList.Add(loanRequest);
                }
            }

            return studentLoanRequestList;
        }
    }
}
