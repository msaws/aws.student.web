﻿/*Copyright 2015-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Models;
using slf4net;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Newtonsoft.Json;
using Ellucian.Web.Mvc.Menu;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class SatisfactoryAcademicProgressController : BaseFinancialAidController
    {
        /// <summary>
        /// Creates a new SatisfactoryAcademicProgress Controller instance
        /// </summary>                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public SatisfactoryAcademicProgressController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {

        }

        /// <summary>
        /// Action method for the SatisfactoryAcademicProgress Page of FinancialAid Hub
        /// </summary>
        /// <returns>SatisfactoryAcademicProgress view</returns>
        [LinkHelp]
        [PageAuthorize("faSatisfactoryAcademicProgress")]
        public ActionResult Index()
        {
            ViewBag.Title = "Satisfactory Academic Progress";

            return View("SatisfactoryAcademicProgress");
        }

        /// <summary>
        /// Action method for the Admin view of SatisfactoryAcademicProgress Page
        /// </summary>
        /// <param name="id">student id</param>
        /// <returns>SatisfactoryAcademicProgress Page Admin view</returns>
        [LinkHelp]
        [PageAuthorize("faSatisfactoryAcademicProgressAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Satisfactory Academic Progress";
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("SatisfactoryAcademicProgress");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("SatisfactoryAcademicProgress");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Gets student's academic progress information
        /// </summary>
        /// <param name="id">student id</param>
        /// <returns>AcademicProgressViewModel</returns>
        [PageAuthorize("faSatisfactoryAcademicProgress")]
        public async Task<string> GetAcademicProgressViewModelAsync(string id)
        {
            var studentId = id;

            //Get the person data
            var person = GetPersonData(id, ref studentId);

            var counselor = GetFinancialAidCounselor(person);

            var academicProgressEvaluations = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            var academicProgressStatuses = new List<Colleague.Dtos.FinancialAid.AcademicProgressStatus>();
            var counselorsPhoneNumbers = new List<Colleague.Dtos.Base.PhoneNumber>();
            var studentAwardsYearsData = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var financialAidOfficeData = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var helpfulLinks = new List<Colleague.Dtos.FinancialAid.Link>();
            var academicPrograms = new List<Colleague.Dtos.Student.AcademicProgram>();
            var academicProgressAppealCodes = new List<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode>();
            var appealsCounselors = new List<Colleague.Dtos.FinancialAid.FinancialAidCounselor>();

            try { academicProgressEvaluations = (await ServiceClient.GetStudentAcademicProgressEvaluations2Async(studentId)).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            var mostRecentAppeals = academicProgressEvaluations.Any() ? academicProgressEvaluations.SelectMany(ape => ape.ResultAppeals)
                .GroupBy(ap => ap.AcademicProgressEvaluationId).Select(ap => ap.First()).ToList() : null;
            var counselorIds = mostRecentAppeals != null ? mostRecentAppeals.Select(mra => mra.AppealCounselorId).ToList() : new List<string>();

            //Add the "general" counselor id to the list for batch phone number retrieval
            if (counselor != null)
            {
                counselorIds.Add(counselor.Id);
            }           

            try
            {
                appealsCounselors = (await ServiceClient.GetFinancialAidCounselorsByIdAsync(new Colleague.Dtos.FinancialAid.FinancialAidCounselorQueryCriteria() {FinancialAidCounselorIds = counselorIds})).ToList();
            }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { academicProgressStatuses = (await ServiceClient.GetAcademicProgressStatusesAsync()).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try
            {
                counselorsPhoneNumbers = (await ServiceClient.GetPersonPhonesByIdsAsync(counselorIds)).ToList();
            }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { studentAwardsYearsData = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { financialAidOfficeData = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception ex) { Logger.Info(ex, ex.Message); }

            try { helpfulLinks = ServiceClient.GetLinks().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { academicPrograms = ServiceClient.GetAcademicPrograms().ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { academicProgressAppealCodes = (await ServiceClient.GetAcademicProgressAppealCodesAsync()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            var academicProgressViewModel = new AcademicProgressViewModel(person, academicProgressEvaluations, academicProgressStatuses, academicProgressAppealCodes, appealsCounselors, counselor, 
                counselorsPhoneNumbers, studentAwardsYearsData, financialAidOfficeData, helpfulLinks, academicPrograms, CurrentUser);

            await SetPrivacyPropertiesForAdminModel(academicProgressViewModel);

            return JsonConvert.SerializeObject(academicProgressViewModel);
        }
    }
}