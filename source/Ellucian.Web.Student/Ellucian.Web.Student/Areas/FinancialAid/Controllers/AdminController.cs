﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Mvc.Models;
using slf4net;
using Ellucian.Web.Adapters;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Models.Person;
using System.Net;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// The AdminController class provides various actions for Financial Aid Counselors 
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class AdminController : BaseStudentController
    {
        private static readonly object _lock = new object();
        private readonly Settings _settings;
        private readonly ILogger _logger;
        private readonly IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Creates a new instance of the <see cref="AdminController"/> class.
        /// </summary>
        /// <param name="adapterRegistry"><see cref="IAdapterRegistry"/></param>
        /// <param name="settings"><see cref="Settings"/></param>
        /// <param name="logger"><see cref="ILogger"/></param>
        public AdminController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _adapterRegistry = adapterRegistry;
            _settings = settings;
            _logger = logger;
        }

        /// <summary>
        /// AJAX method to search for persons for the specified search criteria
        /// </summary>
        /// <param name="searchQueryJson">Criteria to search for persons</param>
        /// <returns>JSON-formatted collection of persons who meet the search criteria</returns>
        [HttpPost]
        public async Task<ActionResult> SearchForStudentOrApplicantAsync(string searchQueryJson)
        {
            try
            {
                var query = JsonConvert.DeserializeObject<string>(searchQueryJson);
                if (string.IsNullOrEmpty(query))
                {
                    string errorMessage = GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchNoCriteriaGivenError");
                    Logger.Error(errorMessage);
                    throw new ArgumentNullException("No search string provided.");
                }
                else
                {
                    var matches = new List<Person>();
                    try
                    {
                        matches = (await ServiceClient.QueryFinancialAidPersonsAsync(new FinancialAidPersonQueryCriteria() { FinancialAidPersonQueryKeyword = query })).ToList();
                        var resultsList = new List<PersonSearchResultModel>();
                        foreach(var match in matches){
                            var result = new PersonSearchResultModel(match.PreferredName, match.Id, match.PrivacyStatusCode);
                            await SetPrivacyPropertiesForAdminModel(result);
                            resultsList.Add(result);
                        }
                        return Json(resultsList, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.ToString());
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// AJAX method to get financial aid counseling search view model in JSON format
        /// </summary>
        /// <param name="controllerName">Name of the origin ASP.NET MVC controller for search context</param>
        /// <returns>JSON-formatted PersonSearchModel</returns>
        [HttpGet]
        public JsonResult GetFinancialAidAdminSearchViewModel(string controllerName)
        {
            try
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel(controllerName);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}