﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Filters;
using Newtonsoft.Json;
using slf4net;
using System.IO;
using System.Net;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Mvc.Menu;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// ShoppingSheetController class for FinancialAid module
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class ShoppingSheetController : BaseFinancialAidController
    {
        private readonly string reportLogoPath;

        /// <summary>
        /// Creates a new ShoppingSheetController instance
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public ShoppingSheetController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {
            reportLogoPath = (settings.ReportLogoPath != null) ?
                settings.ReportLogoPath.PrependTilde() : string.Empty;
        }

        /// <summary>
        /// Action method for the Federal Shopping Sheet page.
        /// </summary>
        /// <returns>Shopping Sheet view</returns>
        [LinkHelp]
        [PageAuthorize("faShoppingSheet")]
        public ActionResult Index()
        {
            ViewBag.Title = "ShoppingSheet";
            ViewBag.AbsoluteReportLogoPath = UrlHelper.GenerateContentUrl(reportLogoPath, this.HttpContext);

            return View("ShoppingSheet");
        }

        /// <summary>
        /// Action method for the Federal Shopping Sheet page
        /// admin view
        /// </summary>
        /// <param name="id">student's id</param>
        /// <returns>Shopping Sheet view</returns>
        [LinkHelp]
        [PageAuthorize("faShoppingSheetAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Shopping Sheet";
            ViewBag.AbsoluteReportLogoPath = UrlHelper.GenerateContentUrl(reportLogoPath, this.HttpContext);

            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;

            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
            return View("ShoppingSheet");
        }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("ShoppingSheet");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Get the shopping sheet model that contains shopping sheet related data
        /// </summary>
        /// <param name="studentId">student id</param>
        /// <returns>shopping sheet view model</returns>
        [PageAuthorize("faShoppingSheet")]
        public async Task<string> GetShoppingSheetViewModelAsync(string id)
        {
            var studentId = id;

            //Get the person data
            var person = GetPersonData(id, ref studentId);

            var counselor = GetFinancialAidCounselor(person);

            var studentAwardYears = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var financialAidOffices = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();
            var studentShoppingSheets = new List<Colleague.Dtos.FinancialAid.ShoppingSheet>();
            Colleague.Dtos.Base.Institution institution = null;
            

            try { studentAwardYears = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOffices = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { institution = ServiceClient.GetInstitutions().FirstOrDefault(i => i.IsHostInstitution); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { studentShoppingSheets = ServiceClient.GetStudentShoppingSheets(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }           

            var shoppingSheetViewModel = new ShoppingSheetViewModel(person, counselor, studentAwardYears, financialAidOffices, studentShoppingSheets, institution, CurrentUser);

            await SetPrivacyPropertiesForAdminModel(shoppingSheetViewModel);

            return JsonConvert.SerializeObject(shoppingSheetViewModel);
        }
    }
}
