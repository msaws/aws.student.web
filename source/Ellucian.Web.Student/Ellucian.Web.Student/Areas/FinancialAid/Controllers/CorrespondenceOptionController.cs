﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.FinancialAid.Controllers
{
    /// <summary>
    /// CorrespondenceOptionController class for FinancialAid Hub
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class CorrespondenceOptionController : BaseFinancialAidController
    {
        /// <summary>
        /// Creates a new correspondenceOptionController instance
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public CorrespondenceOptionController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger, siteService)
        {

        }

        /// <summary>
        /// Action method for the Correspondence Option page.
        /// </summary>
        /// <returns>CorrespondenceOption view</returns>
        [LinkHelp]
        [PageAuthorize("faCorrespondenceOption")]
        public ActionResult Index()
        {
            ViewBag.Title = "CorrespondenceOption";
 
            return View("CorrespondenceOption");
        }

        /// <summary>
        /// Action method for the Correspondence option page in
        /// admin view
        /// </summary>
        /// <param name="id">student's id</param>
        /// <returns>admin view of the Correspondence Option page</returns>
        [LinkHelp]
        [PageAuthorize("faCorrespondenceOptionAdmin")]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = "Admin Correspondence Option";
            ViewBag.IsAdmin = true;
            ViewBag.StudentId = id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("CorrespondenceOption");
            }
            else
            {
                PersonSearchModel model = FinancialAidAdminUtility.CreatePersonSearchModel("CorrespondenceOption");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Get the CorrespondenceOptionViewModel
        /// </summary>
        /// <param name="id">person id</param>
        /// <returns>string containing the consent data</returns>
        [PageAuthorize("faCorrespondenceOption")]
        public async Task<string> GetCorrespondenceOptionViewModelAsync(string id)
        {
            var studentId = id;
            var allStudentAwardYearDtos = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var financialAidOfficeDtos = new List<Colleague.Dtos.FinancialAid.FinancialAidOffice3>();

            //Get the person data
            var person = GetPersonData(id, ref studentId);

            try { allStudentAwardYearDtos = ServiceClient.GetStudentAwardYears2(studentId).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }

            try { financialAidOfficeDtos = (await ServiceClient.GetFinancialAidOffices3Async()).ToList(); }
            catch (Exception e) { Logger.Info(e, e.Message); }            

            var correspondenceOptionViewModel = new CorrespondenceOptionViewModel(person, allStudentAwardYearDtos, financialAidOfficeDtos, CurrentUser);

            await SetPrivacyPropertiesForAdminModel(correspondenceOptionViewModel);

            return JsonConvert.SerializeObject(correspondenceOptionViewModel);
        }

        /// <summary>
        /// Updates the correspondence option selected by a student
        /// </summary>
        /// <param name="optionData">stringified correspondence option flag</param>
        /// <returns>updated correspondence option flag</returns>
        [PageAuthorize("faCorrespondenceOption")]
        public string UpdateCorrespondenceOptionFlag(string optionData)
        {
            if (string.IsNullOrEmpty(optionData))
            {
                string message = "Cannot update correspondence option flag if option data is null";
                Logger.Error(message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return message;
            }
            try
            {
                var studentId = GetEffectivePersonId();
                var optionDataViewModel = JsonConvert.DeserializeObject<CorrespondenceOptionData>(optionData);
                if (optionDataViewModel == null)
                {
                    string message = "Cannot update correspondence option flag if option data is null";
                    Logger.Error(message);
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return message;
                }
                var studentAwardYearDto = new Colleague.Dtos.FinancialAid.StudentAwardYear2()
                {
                    Code = optionDataViewModel.AwardYearCode,
                    IsPaperCopyOptionSelected = optionDataViewModel.IsCorrespondenceOptionChecked,
                    StudentId = studentId
                };

                var updatedStudentAwardYearDto = ServiceClient.UpdateStudentAwardYear2(studentId, studentAwardYearDto);
                optionDataViewModel.IsCorrespondenceOptionChecked = updatedStudentAwardYearDto.IsPaperCopyOptionSelected;
                return JsonConvert.SerializeObject(optionDataViewModel);
            }
            catch (Exception e) 
            { 
                Logger.Error(e, e.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "Something went wrong. Cannot update correspondence option flag"; 
            }
        }
    }
}
