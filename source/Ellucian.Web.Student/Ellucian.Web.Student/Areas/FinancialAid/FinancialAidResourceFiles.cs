﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid
{
    public static class FinancialAidResourceFiles
    {
        public static readonly string AwardLetterResources = "AwardLetter";
        public static readonly string ChecklistResources = "Checklist";
        public static readonly string CorrespondenceOptionResources = "CorrespondenceOption";
        public static readonly string FinancialAidHomeResources = "FinancialAidHome";
        public static readonly string LoanRequestResources = "LoanRequest";
        public static readonly string MyAwardsResources = "MyAwards";
        public static readonly string RequiredDocumentsResources = "RequiredDocuments";
        public static readonly string ShoppingSheetResources = "ShoppingSheet";
    }
}