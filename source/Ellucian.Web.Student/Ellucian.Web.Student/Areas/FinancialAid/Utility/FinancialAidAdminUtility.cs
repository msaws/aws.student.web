﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Models.Search;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Utility
{
    public class FinancialAidAdminUtility
    {
        /// <summary>
        /// Creates a <see cref="PersonSearchModel"/> for a Financial Aid Counseling view
        /// </summary>
        /// <param name="controllerName">Name of the origin ASP.NET MVC controller</param>
        /// <returns>A <see cref="PersonSearchModel"/> for a Financial Aid Counseling view</returns>
        public static PersonSearchModel CreatePersonSearchModel(string controllerName)
        {
            if (string.IsNullOrEmpty(controllerName))
            {
                throw new ArgumentNullException("controllerName", "An ASP.NET MVC controller name must be supplied to build the person search model.");
            }

            return new PersonSearchModel(GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchSearchPrompt"),
                GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchBeforeSearchText"),
                "_PersonSearchResults",
                new SearchActionModel("Admin", controllerName, "FinancialAid"),
                new List<SearchActionModel>() { new SearchActionModel("SearchForStudentOrApplicantAsync", "Admin", "FinancialAid") })
            {
                ErrorMessage = null,
                ErrorOccurred = false,
                PlaceholderText = GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchPlaceholderText"),
                PointOfOriginControllerName = controllerName,
                Search = new SearchActionModel("SearchForStudentOrApplicantAsync", "Admin", "FinancialAid"),
                SearchFieldLabel = GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchSearchFieldLabel"),
                SearchString = null,
                SearchResults = null,
                SearchSubmitLabel = GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchSearchSubmitLabel"),
                Select = new SearchActionModel("Admin", controllerName, "FinancialAid"),
                SpinnerAlternateText = GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchSpinnerAlternateText"),
                SpinnerText = GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchSpinnerText")
            };
        }
    }
}