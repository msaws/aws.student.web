﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.FinancialAid.Utility
{
    /// <summary>
    /// Utility class that provides methods for interacting with SAP related
    /// objects
    /// </summary>
    public static class SAPUtility
    {
        public static SAPStatus GetLatestSAPStatus(IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluationsData, 
            IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatusesData, 
            IEnumerable<Colleague.Dtos.Student.AcademicProgram> academicProgramsData)
        {
            Ellucian.Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 mostRecentEvaluation = null;
            AcademicProgressStatus academicProgressStatus = null;
            AcademicProgram studentAcademicProgram = null;
            SAPStatus sapStatus = null;
  
            //Get most recent academicEvaluation and its academicProgressStatus
            if (academicProgressEvaluationsData != null && academicProgressStatusesData != null)
            {
                var ignoreStatuses = academicProgressStatusesData.Where(apsd => apsd.Category == AcademicProgressStatusCategory.DoNotDisplay);
                mostRecentEvaluation = academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault(aped => ignoreStatuses.All(stat => aped.StatusCode != stat.Code));
                academicProgressStatus = mostRecentEvaluation != null ? academicProgressStatusesData.FirstOrDefault(aps => aps.Code == mostRecentEvaluation.StatusCode) : null;
            }           

            //Create SAPStatus object
            if (mostRecentEvaluation != null && academicProgressStatus != null)
            {
                //Get the current student academic program
                studentAcademicProgram = (academicProgramsData != null && mostRecentEvaluation.ProgramDetail != null) ? 
                    academicProgramsData.FirstOrDefault(ap => ap.Code == mostRecentEvaluation.ProgramDetail.ProgramCode) : null;

                try
                {
                    sapStatus = new SAPStatus(mostRecentEvaluation, academicProgressStatus, studentAcademicProgram);
                }
                catch (Exception) { }
            }

            return sapStatus;
        }

        public static string GetAcademicProgressPropertyLabelOrDefault(this Colleague.Dtos.FinancialAid.AcademicProgressPropertyConfiguration propertyConfig)
        {
            return !string.IsNullOrEmpty(propertyConfig.Label) ? propertyConfig.Label :
                GlobalResources.GetString("SatisfactoryAcademicProgress", "DefaultLabel" + propertyConfig.Type.ToString());
        }
    }
}