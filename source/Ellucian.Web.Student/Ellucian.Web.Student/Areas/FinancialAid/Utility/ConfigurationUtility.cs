﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.FinancialAid.Utility
{
    /// <summary>
    /// Class provides utility methods for interacting with Configuration objects
    /// </summary>
    public static class ConfigurationUtility
    {
        /// <summary>
        /// Return the active student award years from a list of student award years based on the year's equivalent configuration
        /// </summary>
        /// <param name="studentAwardYears"></param>
        /// <param name="configurations"></param>
        /// <returns></returns>
        public static IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> GetActiveStudentAwardYearDtos(this IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYears, IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3> configurations)
        {
            if (configurations == null || configurations.Count() == 0)
            {
                return new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            }
            if (studentAwardYears == null || studentAwardYears.Count() == 0)
            {
                return new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            }

            return studentAwardYears.Where(y =>
            {
                var config = configurations.GetConfigurationDtoOrDefault(y);
                return (config != null) ? config.IsSelfServiceActive : false;
            }).ToList();
        }

        /// <summary>
        /// Get the Configuration that matches the input StudentAwardYear's year and officeId
        /// </summary>
        /// <param name="configurations"></param>
        /// <param name="studentAwardYear"></param>
        /// <returns>The matching configuration or null</returns>
        public static Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 GetConfigurationDtoOrDefault(this IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3> configurations, Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYear)
        {
            if (configurations == null)
            {
                return null;
            }
            if (studentAwardYear == null)
            {
                return null;
            }

            return configurations.FirstOrDefault(c => c.AwardYear == studentAwardYear.Code && c.OfficeId == studentAwardYear.FinancialAidOfficeId);
        }
    }
}