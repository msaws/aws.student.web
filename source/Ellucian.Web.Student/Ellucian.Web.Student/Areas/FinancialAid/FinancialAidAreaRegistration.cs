﻿//Copyright 2014 Ellucian Company L.P. and its affiliates
using System.Web.Mvc;
using System.ComponentModel;
using Ellucian.Web.Student.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Mvc.Licensing;

namespace Ellucian.Web.Student.Areas.FinancialAid
{
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.FinancialAid)]
    public class FinancialAidAreaRegistration : LicenseAreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FinancialAid";
            }
        }

        public override void LicensedRegistration(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FinancialAid_default",
                "FinancialAid/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "Ellucian.Web.Student.Areas.FinancialAid.Controllers" }
            );
        }
    }
}
