﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.HumanResources.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class HomeController : BaseStudentController
    {
        public HomeController(Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        [LinkHelp]
        [PageAuthorize("employeeHome")]
        public async Task<ActionResult> Index()
        {
            try
            {
                var userPermissions = await GetCurrentUserPermissionsAsync();
                var w2Permission = userPermissions.Contains("VIEW.W2");
                var form1095CPermission = userPermissions.Contains("VIEW.1095C");
                var form1098Permission = userPermissions.Contains("VIEW.1098");

                var allPermissions = await GetAllDefinedPermissions();
                var w2Defined = allPermissions.Contains("VIEW.W2");
                var form1095CDefined = allPermissions.Contains("VIEW.1095C");
                var form1098Defined = allPermissions.Contains("VIEW.1098");

                var permissionsAssigned = w2Defined || form1095CDefined || form1098Defined;

                ViewBag.W2 = permissionsAssigned ? w2Permission : true;
                ViewBag.Form1095C = permissionsAssigned ? form1095CPermission : true;
                ViewBag.Form1098 = permissionsAssigned ? form1098Permission : true;
            }
            catch (Exception)
            {
                ViewBag.W2 = true;
                ViewBag.Form1095C = true;
                ViewBag.Form1098 = true;
            }

            ViewBag.Title = "Employee Self Service Home";
            ViewBag.CssAfterOverride = new List<string>();

            return View("Index");
        }

        private async Task<IEnumerable<string>> GetCurrentUserPermissionsAsync()
        {
            var roles = await ServiceClient.GetRolesAsync();
            var userPermissions = roles
                .Where(r => CurrentUser.Roles.Contains(r.Title))
                .SelectMany(r =>
                    r.Permissions.Select(p => p.Code));

            return userPermissions;
        }

        private async Task<IEnumerable<string>> GetAllDefinedPermissions()
        {
            var roles = await ServiceClient.GetRolesAsync();
            var userPermissions = roles
                .SelectMany(r =>
                    r.Permissions.Select(p => p.Code));

            return userPermissions;
        }
    }
}