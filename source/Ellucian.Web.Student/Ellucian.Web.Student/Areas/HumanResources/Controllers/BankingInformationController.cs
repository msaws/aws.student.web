﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.HumanResources.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.HumanResources.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class BankingInformationController : BaseStudentController
    {

        public BankingInformationController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, ISiteService siteService)
            : base(settings, logger)
        {

        }

        //[PageAuthorize("bankingInformation")]
        //public ActionResult Home()
        //{
        //    ViewBag.Title = "2 Banking Information ";
        //    ViewBag.CssAfterOverride = new List<string>();

        //    return View("BankingInformation2");
        //}

        [LinkHelp]
        [PageAuthorize("bankingInformation")]
        public ActionResult Index()
        {
            ViewBag.Title = "Banking Information";
            ViewBag.CssAfterOverride = new List<string>();

            return View("BankingInformation");
        }

        public async Task<JsonResult> GetBankingInformationViewModelAsync(string payeeId, string addressId)
        {
            if (string.IsNullOrEmpty(payeeId))
            {
                payeeId = CurrentUser.PersonId;
            }



            //if there's an error retrieving the configuration, then just stop.
            BankingInformationConfiguration configuration = null;
            try
            {
                configuration = await ServiceClient.GetBankingInformationConfigurationAsync();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Unable to get bankingInformationConfiguration from ServiceClient");
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "Unable to get bankingInformationConfiguration from ServiceClient"), JsonRequestBehavior.AllowGet);
            }

            //build tasks to get data
            var payrollDepositDirectivesTask = ServiceClient.GetPayrollDepositDirectivesAsync();
            var payableDepositDirectivesTask = ServiceClient.GetPayableDepositDirectivesAsync();
            var userPermissionsTask = GetCurrentUserPermissionsAsync();

            try
            {
                await Task.WhenAll(payrollDepositDirectivesTask, payableDepositDirectivesTask, userPermissionsTask);
            }
            catch (Exception e)
            {
                var exceptionMessage = e.Message;
                if (e is AggregateException)
                {
                    var ae = e as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                Logger.Error(e.ToString());
            }
            try
            {
                var payrollDepositDirectives = payrollDepositDirectivesTask.Status == TaskStatus.RanToCompletion ? payrollDepositDirectivesTask.Result : null;
                var payableDepositDirectives = payableDepositDirectivesTask.Status == TaskStatus.RanToCompletion ? payableDepositDirectivesTask.Result : null;
                var userPermissions = userPermissionsTask.Status == TaskStatus.RanToCompletion ? userPermissionsTask.Result : null;

                //create tasks with default results 
                var addressTask = Task.FromResult<IEnumerable<Address>>(new List<Address>());
                var banksTask = Task.FromResult<IEnumerable<Bank>>(new List<Bank>());

                if (userPermissions.Contains(BankingInformationPermissionCodes.EditVendorBankingInformation, StringComparer.CurrentCultureIgnoreCase))
                {
                    // if the user has vendor permissions, get all addresses
                    addressTask = ServiceClient.GetPersonAddressesAsync(payeeId);
                }

                if (payableDepositDirectives != null && payableDepositDirectives.Any())
                {
                    //create the banks tasks
                    banksTask = GetBanksAsync(
                        payableDepositDirectives
                            .Where(a => string.IsNullOrEmpty(a.BankName))
                            .Select(a => !string.IsNullOrEmpty(a.RoutingId) ? a.RoutingId : string.Format("{0}-{1}", a.InstitutionId, a.BranchNumber)));
                }

                try
                {
                    await Task.WhenAll(addressTask, banksTask);
                }
                catch (Exception e)
                {
                    var exceptionMessage = e.Message;
                    if (e is AggregateException)
                    {
                        var ae = e as AggregateException;
                        exceptionMessage = ae.LogAggregateExceptions(Logger);
                    }
                    Logger.Error(e.ToString());
                }

                var userAddresses = addressTask.Status == TaskStatus.RanToCompletion ? addressTask.Result : new List<Address>();
                var banks = banksTask.Status == TaskStatus.RanToCompletion ? banksTask.Result : new List<Bank>();

                var bankingInformationViewModel = new BankingInformationViewModel(
                    payeeId, payrollDepositDirectives,
                    payableDepositDirectives,
                    addressId,
                    configuration,
                    banks,
                    userPermissionCodes: userPermissions,
                    payableDepositAddressDtos: userAddresses
                );

                return Json(new StandardJsonResult(bankingInformationViewModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unknown exception occurred getting and building the banking information view model";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<IEnumerable<string>> GetCurrentUserPermissionsAsync()
        {
            var roles = await ServiceClient.GetRolesAsync();
            var userPermissions = roles
                .Where(r => CurrentUser.Roles.Contains(r.Title))
                .SelectMany(r =>
                    r.Permissions.Select(p => p.Code));

            return userPermissions;
        }

        #region PAYROLL DEPOSITS
        [HttpPost]
        public async Task<JsonResult> CreatePayrollDepositAsync(string payrollDepositData)
        {
            try
            {
                if (string.IsNullOrEmpty(payrollDepositData))
                {
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "payrollDepositData Argument is required"), JsonRequestBehavior.AllowGet);
                }

                var payrollDepositViewModelToCreate = JsonConvert.DeserializeObject<PayrollDepositViewModel>(payrollDepositData);
                PayrollDepositDirective newPayrollDepositDirective = null;
                PayrollDepositViewModel newPayrollDepositViewModel = null;

                // TODO: need to check config and determine if payroll is active...
                if (payrollDepositViewModelToCreate != null)
                {
                    var payrollDepositDirectivesDto = payrollDepositViewModelToCreate.ConvertModelToPayrollDepositDirectivesDto();
                    var payrollDepositDirectiveTask = ServiceClient.CreatePayrollDepositDirectiveAsync(payrollDepositDirectivesDto, payrollDepositViewModelToCreate.SecurityToken);

                    try
                    {
                        newPayrollDepositDirective = await payrollDepositDirectiveTask;
                    }
                    catch (Exception e)
                    {
                        var message = "Unable to create payrollDepositDirective from ServiceClient";
                        Logger.Error(e, message);
                        //throw new HttpException((int)HttpStatusCode.BadRequest, "Unable to update payrollDepositDirectives from ServiceClient");
                        return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
                    }
                }

                newPayrollDepositViewModel = new PayrollDepositViewModel(newPayrollDepositDirective);

                return Json(new StandardJsonResult(newPayrollDepositViewModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unknown exception occurred creating the payroll directive";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPut]
        public async Task<JsonResult> UpdatePayrollDepositAsync(string payrollDepositData, string bankingAuthenticationToken)
        {
            if (string.IsNullOrEmpty(payrollDepositData))
            {
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "payrollDepositData Argument is required"), JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(bankingAuthenticationToken))
            {
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "bankingAuthenticaitonToken Argument is required"), JsonRequestBehavior.AllowGet);
            }

            try
            {
                IEnumerable<PayrollDepositViewModel> payrollDepositViewModelToUpdate;
                BankingAuthenticationToken tokenDto;

                payrollDepositViewModelToUpdate = JsonConvert.DeserializeObject<IEnumerable<PayrollDepositViewModel>>(payrollDepositData);
                tokenDto = JsonConvert.DeserializeObject<BankingAuthenticationToken>(bankingAuthenticationToken);

                IEnumerable<PayrollDepositDirective> updatedPayrollDepositDirectives = null;
                IEnumerable<PayrollDepositViewModel> updatedPayrollDepositViewModels = null;

                // TODO: need to check config and determine if payroll is active...
                if (payrollDepositViewModelToUpdate != null && payrollDepositViewModelToUpdate.Any())
                {
                    var payrollDepositDirectivesDtos = payrollDepositViewModelToUpdate.Select(p => p.ConvertModelToPayrollDepositDirectivesDto());
                    var payrollDepositDirectiveTask = ServiceClient.UpdatePayrollDepositDirectivesAsync(payrollDepositDirectivesDtos, tokenDto);

                    try
                    {
                        updatedPayrollDepositDirectives = await payrollDepositDirectiveTask;
                    }
                    catch (Exception e)
                    {
                        var message = "Unable to update payrollDepositDirectives from ServiceClient";
                        Logger.Error(e, message);
                        return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
                    }
                }

                updatedPayrollDepositViewModels = updatedPayrollDepositDirectives.Select(d => new PayrollDepositViewModel(d));

                return Json(new StandardJsonResult(updatedPayrollDepositViewModels), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unknown exception occurred updating the payroll directive";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpDelete]
        public async Task<JsonResult> DeletePayrollDepositAsync(string payrollDepositData)
        {
            if (string.IsNullOrEmpty(payrollDepositData))
            {
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "payrollDepositData Argument is required"), JsonRequestBehavior.AllowGet);
            }

            try
            {
                var payrollDepositViewModel = JsonConvert.DeserializeObject<PayrollDepositViewModel>(payrollDepositData);

                try
                {
                    await ServiceClient.DeletePayrollDepositDirectiveAsync(payrollDepositViewModel.Id, payrollDepositViewModel.SecurityToken);
                }
                catch (Exception e)
                {
                    var message = "Unable to delete payroll deposit";
                    Logger.Error(e, message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
                }

                return Json(new StandardJsonResult(new object { }), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unknown exception occurred deleting the payroll directive";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region PAYABLE DEPOSITS
        [HttpPost]
        public async Task<JsonResult> CreatePayableDepositAsync(string payableDepositData)
        {
            if (string.IsNullOrEmpty(payableDepositData))
            {
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "payableDepositData Argument is required"), JsonRequestBehavior.AllowGet);
            }
            try
            {
                var payableDepositViewModelToCreate = JsonConvert.DeserializeObject<PayableDepositViewModel>(payableDepositData);
                PayableDepositDirective newPayableDepositDirective = null;
                PayableDepositViewModel newPayableDepositViewModel = null;

                // TODO: need to check config and determine if is active...
                if (payableDepositViewModelToCreate != null)
                {
                    var payableDepositDirectivesDto = payableDepositViewModelToCreate.ConvertModelToPayableDepositDirectiveDtos();
                    var payableDepositDirectiveTask = ServiceClient.CreatePayableDepositDirectiveAsync(payableDepositDirectivesDto, payableDepositViewModelToCreate.SecurityToken);

                    try
                    {
                        newPayableDepositDirective = await payableDepositDirectiveTask;
                    }
                    catch (Exception e)
                    {
                        var message = "Unable to create payable deposit directive from Service Client";
                        Logger.Error(e, message);
                        return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);

                    }
                }

                newPayableDepositViewModel = new PayableDepositViewModel(newPayableDepositDirective);

                return Json(new StandardJsonResult(newPayableDepositViewModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unknown exception occurred creating the payable directive";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPut]
        public async Task<JsonResult> EditPayableDepositAsync(string payableDepositData)
        {
            if (string.IsNullOrEmpty(payableDepositData))
            {
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "payableDepositData Argument is required"), JsonRequestBehavior.AllowGet);
            }

            try
            {
                var payableDepositViewModelToUpdate = JsonConvert.DeserializeObject<PayableDepositViewModel>(payableDepositData);
                PayableDepositDirective updatedPayableDepositDirective = null;
                PayableDepositViewModel updatedPayableDepositViewModel = null;

                // TODO: need to check config and determine if is active...
                if (payableDepositViewModelToUpdate != null)
                {
                    var payableDepositDirectivesDto = payableDepositViewModelToUpdate.ConvertModelToPayableDepositDirectiveDtos();
                    var payableDepositDirectiveTask = ServiceClient.UpdatePayableDepositDirectiveAsync(payableDepositDirectivesDto, payableDepositViewModelToUpdate.SecurityToken);

                    try
                    {
                        updatedPayableDepositDirective = await payableDepositDirectiveTask;
                    }
                    catch (Exception e)
                    {
                        var message = "Unable to update payable deposit directive from Service Client";
                        Logger.Error(e, message);
                        return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
                    }
                }

                updatedPayableDepositViewModel = new PayableDepositViewModel(updatedPayableDepositDirective);

                return Json(new StandardJsonResult(updatedPayableDepositViewModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unknown exception occurred updating the payable directive";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpDelete]
        public async Task<JsonResult> DeletePayableDepositAsync(string payableDepositData)
        {
            if (string.IsNullOrEmpty(payableDepositData))
            {
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "payableDepositData Argument is required"), JsonRequestBehavior.AllowGet);
            }

            try
            {
                var payableDepositViewModel = JsonConvert.DeserializeObject<PayableDepositViewModel>(payableDepositData);

                //if the viewModel id is not 0 (means its an existing deposit) and startdate is greater than today (means 
                //its a future deposit, then ok to delete
                if (payableDepositViewModel.Id != "0" && payableDepositViewModel.StartDate > DateTime.Today)
                {
                    await ServiceClient.DeletePayableDepositDirectiveAsync(payableDepositViewModel.Id, payableDepositViewModel.SecurityToken);
                }
                else
                {
                    //throw new HttpException((int)HttpStatusCode.BadRequest, "Cannot delete new, active, or past deposits. Only existing, future deposits can be deleted");

                    var message = "Cannot delete new, active, or past deposits. Only existing, future deposits can be deleted";
                    Logger.Error(message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
                }
                return Json(new StandardJsonResult(new object { }), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Error occurred deleting the payable directive";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpGet]
        public async Task<JsonResult> GetBankAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "id Argument is required to getBank"), JsonRequestBehavior.AllowGet);
            }

            try
            {
                var bank = await ServiceClient.GetBankAsync(id);
                return Json(new StandardJsonResult(bank), JsonRequestBehavior.AllowGet);
            }
            catch (ResourceNotFoundException rnfe)
            {
                var message = string.Format("Bank with id {0} is not known to Colleague API", id);
                Logger.Error(rnfe, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.NotFound, message: message), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unable to get bank from ServiceClient";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public async Task<IEnumerable<Bank>> GetBanksAsync(IEnumerable<string> ids)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }

            var tasks = ids.Distinct().Select(id => ServiceClient.GetBankAsync(id)).ToList();
            IEnumerable<Bank> banks = null;
            if (tasks.Any())
            {
                try
                {
                    banks = await Task.WhenAll(tasks);
                }
                catch (Exception)
                {
                    banks = tasks.Where(t => t.Status == TaskStatus.RanToCompletion).Select(t => t.Result).ToList();
                }
            }

            return banks;
        }

        [HttpGet]
        public async Task<JsonResult> AuthenticatePayrollDepositDirectiveAsync(string directiveId, string accountId)
        {
            try
            {
                BankingAuthenticationToken token;
                if (string.IsNullOrEmpty(directiveId) && string.IsNullOrEmpty(accountId))
                {
                    token = await ServiceClient.AuthenticatePayrollDepositDirectiveAsync();
                }
                else
                {
                    token = await ServiceClient.AuthenticatePayrollDepositDirectiveAsync(directiveId, accountId);
                }

                return Json(new StandardJsonResult(token), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unable to get payroll deposit change authorization";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public async Task<JsonResult> AuthenticatePayableDepositDirectiveAsync(string directiveId, string accountId, string addressId)
        {
            try
            {

                var token = await ServiceClient.AuthenticatePayableDepositDirectiveAsync(directiveId, accountId, addressId);

                return Json(new StandardJsonResult(token), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Unable to get payable deposit change authorization";
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }


    }
}