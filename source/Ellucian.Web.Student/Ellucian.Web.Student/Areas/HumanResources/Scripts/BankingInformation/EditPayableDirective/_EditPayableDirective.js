﻿var markup = require("./_EditPayableDirective.html"),
    actions = require("BankingInformation/banking.information.actions"),
    resources = require("Site/resource.manager");

/*
 * params: {
 *  dataModel: bankingInformationViewModel - C# server model,
 *  id: id of the directive to edit
 * }
 */ 
function EditPayableDirectiveViewModel(params) {
    var self = this;
   

    var allPayableDirectives = ko.computed(function () {
        var unwrappedActiveRefundDeposit = ko.unwrap(params.dataModel.ActiveRefundDeposit);
        return (unwrappedActiveRefundDeposit ? [unwrappedActiveRefundDeposit] : [])
            .concat(params.dataModel.FutureRefundDeposits())
            .concat(params.dataModel.PastRefundDeposits())
    });

    self.hasAllCanadianAccounts = ko.computed(function () {
        var hasUsDirective = allPayableDirectives().some(function (directive) {
            return directive.RoutingId() ? true : false;
        });
        return !hasUsDirective;
    });

    var editingDirective = ko.computed(function () {
        var directive = allPayableDirectives().find(function (directive) {
            return directive.Id() === params.id;
        });
        return directive;
    });

    self.invalidId = ko.computed(function () {
        return (!editingDirective() || (editingDirective().EndDate() && editingDirective().EndDate() < Date.Today()));
    })

    function editPayableDirectiveViewModel(directive) {
        var viewModel = this;

        viewModel.Id = directive.Id;
        viewModel.RoutingId = directive.RoutingId;
        viewModel.IsVerified = directive.IsVerified;
        viewModel.InstitutionId = directive.InstitutionId;
        viewModel.EndDate = directive.EndDate;
        viewModel.Configuration = params.dataModel.Configuration;
        viewModel.SecurityToken = directive.SecurityToken;
        viewModel.AccountIdLastFour = directive.AccountIdLastFour;

        viewModel.displayBankName = ko.computed(function () {
            if (directive.Nickname()) {
                return directive.Nickname();
            }
            return directive.BankName();

        });

        viewModel.displayBankAccountLastFour = ko.computed(function () {
            return "..." + directive.AccountIdLastFour();
        });

        viewModel.isSwitchedOn = ko.observable(directive.IsElectronicPaymentRequested());

        viewModel.startDateBacker = ko.observable(directive.StartDate() ? directive.StartDate() : Date.Today());
        viewModel.startDateErrorMessage = ko.observable();

        viewModel.startDateIsReadOnly = ko.computed(function () {
            return Date.Compare(viewModel.startDateBacker(), Date.Today()) <= 0;

        });
        viewModel.startDateNeedsInput = ko.computed(function () {
            return !viewModel.startDateIsReadOnly() && (!viewModel.startDateBacker() || viewModel.startDateBacker() === "");
        });

        viewModel.accountDetailsModel = {
            accountIdLast4: ko.observable(directive.AccountIdLastFour()).extend({ notify: 'always' }),
            nickname: ko.observable(directive.Nickname()),
            bankName: ko.observable(directive.BankName()),
            routingId: ko.observable(directive.RoutingId()),
            institutionId: ko.observable(directive.InstitutionId()),
            branchNumber: ko.observable(directive.BranchNumber()),
            newAccountId: ko.observable(null),
            accountType: ko.observable(directive.Type()),
            isVerified: ko.observable(directive.IsVerified())
        }
        function resetAccountDetails() {
            viewModel.accountDetailsModel
                .accountIdLast4(directive.AccountIdLastFour())
                .nickname(directive.Nickname())
                .bankName(directive.BankName())
                .routingId(directive.RoutingId())
                .institutionId(directive.InstitutionId())
                .branchNumber(directive.BranchNumber())
                .newAccountId(null)
                .accountType(directive.Type())
                .isVerified(directive.IsVerified())
        }

       
        viewModel.isBankAccountDetailsInputValid = ko.observable(true);
        viewModel.showEditBankAccountDetails = ko.observable(false);
        viewModel.editDirectiveBankAccount = function () {
            viewModel.showEditBankAccountDetails(true);
        }
        viewModel.cancelEditBankAccountDetails = function () {
            viewModel.showEditBankAccountDetails(false);
            resetAccountDetails()
        }
        viewModel.isSavingBankAccountDetailsInProgress = ko.observable(false);
        viewModel.saveBankAccountDetails = function () {
            if (!viewModel.isBankAccountDetailsInputValid()) {
                return Promise.resolve();
            }
            viewModel.showEditBankAccountDetails(false);
            viewModel.isSavingBankAccountDetailsInProgress(true);

            var model = viewModel.accountDetailsModel;
            directive.AccountIdLastFour(model.accountIdLast4());
            directive.Nickname(model.nickname());
            directive.BankName(model.bankName());
            directive.RoutingId(model.routingId());
            directive.InstitutionId(model.institutionId());
            directive.BranchNumber(model.branchNumber());
            directive.NewAccountId(model.newAccountId());
            directive.Type(model.accountType());

            var unmappedObject = ko.mapping.toJS(directive);

            return actions.updatePayableDepositDirectiveAsync(unmappedObject).then(function () {
                viewModel.isSavingBankAccountDetailsInProgress(false);
            }).catch(function (err) {
                console.error(err);
                $('#notificationHost').notificationCenter('addNotification', { message: updatePayableErrorMessage(), type: 'error', flash: true });
                resetAccountDetails();
                viewModel.isSavingBankAccountDetailsInProgress(false);
            });
        }
        var updatePayableErrorMessage = resources.getObservable('BankingInformation.UpdateBankingInformationErrorMessage');


        viewModel.hasInputErrors = ko.computed(function () {
            return viewModel.startDateErrorMessage() ? true : false;
        });
        viewModel.saveEnabled = ko.computed(function () {
            if (viewModel.startDateIsReadOnly()) return true;
            return !viewModel.hasInputErrors();
        });
        viewModel.isSaveDirectiveInProgress = ko.observable(false);
        viewModel.saveAsync = function () {
            viewModel.isSaveDirectiveInProgress(true);
           
            directive.IsElectronicPaymentRequested(viewModel.isSwitchedOn());
            directive.StartDate(viewModel.startDateBacker());

            var unmappedDirective = ko.mapping.toJS(directive);

            return actions.updatePayableDepositDirectiveAsync(unmappedDirective).then(function () {
                viewModel.isSaveDirectiveInProgress(false);
                return;
            }).catch(function (err) {
                console.error(err);
                $('#notificationHost').notificationCenter('addNotification', { message: updatePayableErrorMessage(), type: 'error', flash: true });
                viewModel.isSaveDirectiveInProgress(false);
                return;
            }).then(function() {
                viewModel.back();
                return;
            });

        }

        viewModel.deleteButtonEnabled = ko.computed(function () {
            return Date.Compare(directive.StartDate(), Date.Today()) == 1;
        });

        viewModel.isDeleteInProgress = ko.observable(false);

        viewModel.deleteAsync = function () {
            viewModel.isDeleteInProgress(true);

            var unmappedObject = ko.mapping.toJS(directive);
            return actions.deletePayableDepositDirectiveAsync(unmappedObject).then(function () {
                viewModel.isDeleteInProgress(false);
               
                return;
            }).catch(function (err) {
                console.error(err);
                $('#notificationHost').notificationCenter('addNotification', { message: deletePayableErrorMessage(), type: 'error', flash: true });

                viewModel.isDeleteInProgress(false);
                return;
            }).then(function() {
                viewModel.back();
                return;
            });
        }

        var deletePayableErrorMessage = resources.getObservable('BankingInformation.DeletingDepositError');

        viewModel.back = function () {
            window.history.back();
        }

        viewModel.actionInProgressMessage = ko.computed(function () {
            if (viewModel.isDeleteInProgress()) {
                return resources.getObservable("BankingInformation.DeletingDepositLoadingMessage")();
            }
            else if (viewModel.isSaveDirectiveInProgress()) {
                return resources.getObservable("BankingInformation.EditDepositDetailsModalSavingLabel")();
            }
            else if (viewModel.isSavingBankAccountDetailsInProgress()) {
                return resources.getObservable("BankingInformation.EditAccountDetailsModalSavingLabel")();
            }
            return null;
        })
        viewModel.actionInProgress = ko.computed(function () {
            return viewModel.isDeleteInProgress() ||
                viewModel.isSaveDirectiveInProgress() ||
                viewModel.isSavingBankAccountDetailsInProgress();
        });

        viewModel.dispose = function () {
            viewModel.startDateNeedsInput.dispose();
            viewModel.startDateIsReadOnly.dispose();
            viewModel.actionInProgress.dispose();
            viewModel.actionInProgressMessage.dispose();
            viewModel.deleteButtonEnabled.dispose();
            viewModel.saveEnabled.dispose();
            viewModel.hasInputErrors.dispose();
            viewModel.displayBankAccountLastFour.dispose();
            viewModel.displayBankName.dispose();

        }
    }

    self.editingDirectiveViewModel = null;

    if (!self.invalidId()) {
        self.editingDirectiveViewModel = new editPayableDirectiveViewModel(editingDirective())
    }

    self.dispose = function () {
        if (self.editingDirectiveViewModel) {
            self.editingDirectiveViewModel.dispose();
        }

        self.invalidId.dispose();
        editingDirective.dispose();
        self.hasAllCanadianAccounts.dispose();
        allPayableDirectives.dispose();
    }
   
}

module.exports = { viewModel: EditPayableDirectiveViewModel, template: markup };