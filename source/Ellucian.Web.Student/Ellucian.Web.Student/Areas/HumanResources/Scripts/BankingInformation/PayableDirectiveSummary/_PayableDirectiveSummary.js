﻿var markup = require("./_PayableDirectiveSummary.html");

/*
 * params: {
 *  dataModel: PayableDepositViewModel - C# server model,
 *  hasAllCanadianAccounts: computed indicating whether all directives deposit to canadian bank accounts,
 *  type: active || future || past
 * }
 */
function PayableDirectiveSummaryViewModel(params) {
    var self = this;

    self.hasAllCanadianAccounts = params.hasAllCanadianAccounts;

    self.BankName = params.dataModel.BankName;
    self.IsVerified = params.dataModel.IsVerified;
    self.RoutingId = params.dataModel.RoutingId;
    self.InstitutionId = params.dataModel.InstitutionId;
    self.isPastDirective = params.type === 'past';

    self.editPayableDepositDirective = function () {
        if (!self.isPastDirective) {
            var addressHash = "";
            if (params.dataModel.AddressId()) {
                addressHash = "&addressId={0}".format(params.dataModel.AddressId());
            }
            var payableHash = "#payable={0}".format(params.dataModel.Id());

            window.location.hash = payableHash + addressHash;
        }
        return;
    }

    self.displayBankName = ko.computed(function () {
        var bankName = params.dataModel.Nickname() ? params.dataModel.Nickname() : params.dataModel.BankName();
        if (!bankName) {
            var bankId = params.dataModel.RoutingId() ?
                params.dataModel.RoutingId() :
                "{0}-{1}".format(params.dataModel.InstitutionId(), params.dataModel.BranchNumber());
            return bankId;
        }
        return bankName;
    });

    self.thirdColumnDisplayByType = ko.computed(function () {
        switch (params.type) {
            case "active":
                return '\xa0';
            case "future":
                return Globalize.format(params.dataModel.StartDate(), 'd');
            case "past":
                return Globalize.format(params.dataModel.EndDate(), 'd');
        }
    });

}

module.exports = { viewModel: PayableDirectiveSummaryViewModel, template: markup };