﻿var markup = require("./_ActivePayableDirective.html"),
    resources = require("Site/resource.manager");

/*
 * params: {
 *  dataModel - bankingInformationViewModel - C# server model,
 *  hasAllCanadianAccounts - computed that indicates whether all deposits go to Canadian banks
 * }
 */
function ActivePayableDirectiveViewModel(params) {
    var self = this;

    self.dataModel = params.dataModel;

    self.ActiveRefundDeposit = ko.utils.unwrapObservable(self.dataModel.ActiveRefundDeposit);
    self.hasAllCanadianAccounts = params.hasAllCanadianAccounts;

    self.viewAllPayableHref = ko.pureComputed(function () {
        var addressHash = "";
        if (params.dataModel.AddressId()) {
            addressHash = "&addressId={0}".format(params.dataModel.AddressId());
        }
        return "#viewAllPayable" + addressHash;        
    });

    self.hasActivePayableDeposit = ko.computed(function () {
        return self.ActiveRefundDeposit !== null ? true : false;
    });

    self.noActivePayableDepositMessage = ko.computed(function () {
        return resources.getObservable('BankingInformation.NoActivePayableDepositMessage')() + " " +
            resources.getObservable('BankingInformation.NoActivePayableDepositEntireMessage')();
    });
}

module.exports = { viewModel: ActivePayableDirectiveViewModel, template: markup };