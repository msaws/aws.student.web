﻿var markup = require("./_EditViewComponent.html"),
    actions = require("BankingInformation/banking.information.actions"),
    resources = require("Site/resource.manager");

/*
 * params: {
 *  type: payroll || payable || new
 *  id: id of the directive being edited. null if new
 *  addressId: id of address. null if no address (default view).
 * }
 */
function EditViewComponentViewModel(params) {
    var self = this;

    self.addressId = params.addressId || null;
    self.isLoading = ko.observable(true);
    self.loadingMessage = resources.getObservable("BankingInformation.LoadingBankingInformationMessage");

    self.loadError = ko.observable(false);
    self.loadErrorMessage = resources.getObservable('BankingInformation.LoadBankingInformationErrorMessage');

    self.type = params.type;

    actions.getBankingInformationViewModelAsync("", self.addressId).then(function (data) {
        self.bankingInformationDataModel = ko.mapping.fromJS(data);
        
        self.loadError(false).isLoading(false);
    }).catch(function (err) {
        self.loadError(true).isLoading(false);
        //no need to push a notification here
        console.error(err);
    });


    self.bankingInformationDataModel = {};
    self.id = params.id;

    self.back = function () {
        window.history.back();
    }
   
}

module.exports = { viewModel: EditViewComponentViewModel, template: markup };