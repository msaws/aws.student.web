﻿// Examines the target against a provided regular expression
ko.extenders.formatCheck = function (target, options) {
    target.hasFormatError = ko.observable();
    target.errorMessage = ko.observable();

    function success() {
        target.hasFormatError(false);
        target.errorMessage(null);
    }

    function error(msg) {
        target.hasFormatError(true);
        target.errorMessage(msg);
    }

    var pattern = new RegExp(options.pattern);
    var message = options.message;

    function validateFormat(input) {
        if (input != null && input != '') {
            if (!input.match(pattern)) {
                return success();
            }
            else {
                return error(ko.utils.unwrapObservable(message));
            }
        } else {
            return success();
        }
        
    }
    validateFormat(target());
    target.subscribe(validateFormat);

    return target;
}
