﻿var markup = require("./_AllPayableDirectivesCollection.html");

/*
 * params: {
 *  dataModel: bankingInformationViewModel - c# server model
 * }
 */
function AllPayableDirectivesCollectionViewModel(params) {
    var self = this;

    self.ActiveRefundDeposit = ko.unwrap(params.dataModel.ActiveRefundDeposit);
    self.FutureRefundDeposits = params.dataModel.FutureRefundDeposits;
    self.PastRefundDeposits = params.dataModel.PastRefundDeposits;


    var allPayableDirectives = ko.computed(function () {
        return (self.ActiveRefundDeposit ? [self.ActiveRefundDeposit] : [])
            .concat(params.dataModel.PastRefundDeposits())
            .concat(params.dataModel.FutureRefundDeposits());
    });

    self.hasAllCanadianAccounts = ko.computed(function () {
        var hasUsDirective = allPayableDirectives().some(function (directive) {
            return directive.RoutingId() ? true : false;
        });
        return !hasUsDirective;
    });
}

module.exports = { viewModel: AllPayableDirectivesCollectionViewModel, template: markup };