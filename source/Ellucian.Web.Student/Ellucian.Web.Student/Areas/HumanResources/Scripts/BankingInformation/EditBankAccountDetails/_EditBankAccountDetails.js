﻿var markup = require("./_EditBankAccountDetails.html"),
    resources = require("Site/resource.manager"),
    actions = require("BankingInformation/banking.information.actions");

require("BankingInformation/format.check.extender");
require("BankingInformation/length.check.extender");
require("BankingInformation/routing.number.check.extender");
require("./_EditBankAccountDetails.scss");

require("Site/Components/Dropdown/_Dropdown");


/*
 * params: {
 *  
 *  dataModel: an object with observable attributes that looks like this - 
 *             mimic's the C# server Payroll or PayableDepositViewModel, but should not be the actual data model
 *             the parent component will have a reference to these observables, which it can use on save
 *          {
 *              accountIdLast4: observable,
 *              nickname: observable,
 *              bankName: observable,
 *              routingId: observable,
 *              institutionId: observable,
 *              branchNumber: observable,
 *              newAccountId: observable,
 *              accountType: observable,
 *              isVerified: observable,
 *          },
 *  configuration: bankingInformationConfiguration object - c# server model,
 *  isValidInput: observable that will be updated by this component when all the inputs are valid,
 *  isAdd: indicates whether this is an add or an edit,
 *  isPayroll: indicates whether this bank account is for a payroll deposit (could be both)
 *  isPayable: indicates whether this bank account is for a payable deposit (could be both)
 * }
 */
function EditBankAccountDetailsViewModel(params) {
    var self = this;

    self.isAdd = params.isAdd;
    self.isPayroll = params.isPayroll;
    self.isPayable = params.isPayable;



    self.usCheckImageAltText = resources.getObservable('BankingInformation.UsCheckImageAltText');
    self.caCheckImageAltText = resources.getObservable('BankingInformation.CanadaCheckImageAltText');

    self.displayBankAccountLastFour = ko.computed(function () {
        return "..." + params.dataModel.accountIdLast4();
    });

    self.displayBankName = ko.computed(function () {
        if (params.dataModel.nickname()) {
            return params.dataModel.nickname();
        }
        return params.dataModel.bankName();

    });

    self.nicknameInput = ko.computed({
        read: function () { return params.dataModel.nickname() },
        write: function (value) { value && value != '' ? params.dataModel.nickname(value) : params.dataModel.nickname('') }
    }).extend({
        formatCheck: { pattern: "([<>])", message: resources.getObservable('BankingInformation.ErrorInvalidCharactersMessage') },
        lengthCheck: { maxLength: 50, maxLengthMessage: resources.getObservable('BankingInformation.ErrorNicknameMaxLengthMessage') },
    });
    self.nicknameHasError = ko.computed(function () {
        return self.nicknameInput.hasFormatError() || self.nicknameInput.hasLengthError();
    });
    self.nicknameErrorMessage = ko.computed(function () {
        if (self.nicknameInput.hasFormatError()) {
            return self.nicknameInput.errorMessage();
        }
        else if (self.nicknameInput.hasLengthError()) {
            return self.nicknameInput.lengthErrorMessage();
        }
        return null;
    });
    self.nicknamePlaceholder = resources.getObservable('BankingInformation.AccountDetailsInputNicknamePlaceholder');
    self.isEditingNickname = ko.observable(self.isAdd);
    var nicknameEditMessage = resources.getObservable('BankingInformation.EditingNicknameMessage');
    self.toggleNicknameEdit = function () {
        self.isEditingNickname(!self.isEditingNickname());
        if (self.isEditingNickname()) {
            $('#aria-announcements').text(nicknameEditMessage());
        }
    }

    self.countries = ko.observableArray(['Canada', 'United States']);

    self.bankCountry = ko.observable(
        (function () {
            if (params.dataModel.routingId() && params.dataModel.routingId().length > 0) {
                return self.countries()[1];
            }
            else if (params.dataModel.branchNumber() && params.dataModel.branchNumber().length > 0) {
                return self.countries()[0];
            }
            return Globalize.culture().name == 'en-US' ? self.countries()[1] : self.countries()[0];
        })()
    );

    self.showUnitedStatesData = ko.pureComputed(function () {
        if (self.isAdd) {
            return self.bankCountry() == self.countries()[1];
        }
        return params.dataModel.routingId.peek();
        //return (self.bankCountry() == self.countries()[1] && self.isAdd) || (params.dataModel.routingId() && !self.isAdd)
    });

    self.makeCountryInputFluid = function () {
        $("edit-bank-account-details .esg-button-group").addClass("esg-button-group--fluid");
    }

    self.showHideCheckImage = function () {
        console.info("implement show hide check image");
    }

    self.accountEditsDisabled = ko.computed(function () {
        if (self.isAdd) {
            return false;
        }
        return params.dataModel.isVerified() || params.dataModel.institutionId();
    })

    self.routingIdInput = ko.computed({
        read: function () {
            return params.dataModel.routingId();
        },
        write: function (value) {
            if (value === "") {
                value = null;
            }
            return params.dataModel.routingId(value);
        }
    }).extend({
        rateLimit: { timeout: 1000, method: "notifyWhenChangesStop" },
        routingNumberCheck: {
            numericOnlyMessage: resources.getObservable('BankingInformation.ErrorRoutingNumberNumericValuesOnly'),
            firstTwoDigitsMessage: resources.getObservable('BankingInformation.ErrorRoutingNumberInvalidMessage'),
            incorrectLengthMessage: resources.getObservable('BankingInformation.ErrorRoutingNumberIncorrectLengthMessage'),
            invalidNumberMessage: resources.getObservable('BankingInformation.ErrorRoutingNumberInvalidMessage')
        },
    });
    self.isEditingRoutingId = ko.observable(self.isAdd);
    var routingIdEditMessage = resources.getObservable('BankingInformation.EditingRoutingIdMessage');
    self.toggleRoutingIdEdit = function () {
        self.isEditingRoutingId(!self.isEditingRoutingId());
        if (self.isEditingRoutingId()) {
            $('#aria-announcements').text(routingIdEditMessage());
        }
    }

    self.routingIdNeedsInput = ko.pureComputed(function () {
        return self.bankCountry() === "United States" && (params.dataModel.routingId() === null || params.dataModel.routingId() === "");
    });


    self.resolvedBankName = ko.computed({
        read: function () {
            return params.dataModel.bankName();
        },
        write: function (value) {
            params.dataModel.bankName(value);
            return
        }
    });
        
    self.resolvedBankNameError = ko.observable(false);
    self.resolvedBankNameErrorMessage = ko.observable(null);

    var errorBankDoesNotExistMessage = resources.getObservable('BankingInformation.ErrorBankDoesNotExistMessage');
    var errorUnableToResolveBankMessage = resources.getObservable('BankingInformation.ErrorUnableToResolveBankMessage');

    //function that gets the bank asynchronsly and updates the resolvedBankName observable
    self.resolveRoutingId = function (value) {
        //parent.checkAgainstAchDirectoryPermissions(false);
        if (self.routingIdInput.hasFormatError() || !value) {
            self.resolvedBankName(null);
            return Promise.resolve({});
        } else {

            var bankPromise = actions.getBankAsync(value);
            self.resolvedBankNameError(false);
            bankPromise.then(function (bank) {
                self.resolvedBankName(bank.Name);
                return bank;
            }).catch(function (responseObject) {
                self.resolvedBankNameError(true);
                if (responseObject.StatusCode === 404) {
                    self.resolvedBankName(null);
                    self.resolvedBankNameErrorMessage(errorBankDoesNotExistMessage());
                    console.error(responseObject.Message + "\n" + errorBankDoesNotExistMessage())
                }
                else {
                    self.resolvedBankName(null);
                    console.warn(responseObject.Message + "\n" + errorUnableToResolveBankMessage());
                }
                return;
            });

            return bankPromise;
        }
    }
    //subscribe to the routingIdInput. when it changes, call the resolveRoutingId function
    self.routingIdInputSubscription = self.routingIdInput.subscribe(self.resolveRoutingId);

    self.hasUsBankAddPermissionErrorMessage = ko.observable("Unable to confirm the routing number. Please contact your Payroll office to add this bank");
    self.hasCaBankAddPermissionErrorMessage = ko.observable("Unable to confirm the institution id and branch number. Please contact your Payroll office to add this bank");
    self.hasBankAddPermissionError = ko.computed(function () {
        return self.resolvedBankNameError() && params.isPayroll;
        
    });

    self.routingIdHasError = ko.computed(function () {
        return self.routingIdInput.hasFormatError() || self.hasBankAddPermissionError();
    });

    self.routingIdErrorMessage = ko.computed(function () {
        if (self.routingIdInput.hasFormatError()) {
            return self.routingIdInput.errorMessage();
        }
        else if (self.hasBankAddPermissionError()) {
            return self.hasUsBankAddPermissionErrorMessage();
        }
        return null;
    });

    self.branchNumberInput = ko.computed({
        read: function () {
            return params.dataModel.branchNumber();
        },
        write: function (value) {
            params.dataModel.branchNumber(value);
        }
    }).extend({
        rateLimit: { timeout: 1000, method: "notifyWhenChangesStop" },
        simpleNumericValidation: { invalidMessage: resources.getObservable('BankingInformation.ErrorBranchNumberNumericOnlyMessage') },
        lengthCheck: {
            exactLength: 5,
            exactLengthMessage: resources.getObservable('BankingInformation.ErrorBranchNumberIncorrectLengthMessage')
        }
    });
    self.branchNumberNeedsInput = ko.pureComputed(function () {
        return self.bankCountry() === "Canada" && (params.dataModel.branchNumber() === null || params.dataModel.branchNumber() === "");
    });
    self.branchNumberHasError = ko.computed(function () {
        return self.branchNumberInput.hasError() || self.branchNumberInput.hasLengthError() || self.hasBankAddPermissionError();
    });
    self.branchNumberErrorMessage = ko.computed(function () {
        if (self.branchNumberInput.hasError()) {
            return self.branchNumberInput.errorMessage();
        }
        else if (self.branchNumberInput.hasLengthError()) {
            return self.branchNumberInput.lengthErrorMessage();
        }
        else if (self.hasBankAddPermissionError()) {
            return self.hasCaBankAddPermissionErrorMessage();
        }
        return null;
    })

    self.institutionIdInput = ko.computed({
        read: function () {
            return params.dataModel.institutionId();
        },
        write: function (value) {
            params.dataModel.institutionId(value);
        }
    }).extend({
        rateLimit: { timeout: 750, method: "notifyWhenChangesStop" },
        simpleNumericValidation: { invalidMessage: resources.getObservable('BankingInformation.ErrorInstitutionIdNumericOnlyMessage') },
        lengthCheck: {
            exactLength: 3,
            exactLengthMessage: resources.getObservable('BankingInformation.ErrorInstitutionIdIncorrectLengthMessage')
        }
    });

    self.institutionIdNeedsInput = ko.pureComputed(function () {
        return self.bankCountry() === "Canada" && (params.dataModel.institutionId() === null || params.dataModel.institutionId() === "");
    });
    self.institutionIdHasError = ko.computed(function () {
        return self.institutionIdInput.hasError() || self.institutionIdInput.hasLengthError();
    });
    self.institutionIdErrorMessage = ko.computed(function () {
        if (self.institutionIdInput.hasError()) {
            return self.institutionIdInput.errorMessage();
        }
        else if (self.institutionIdInput.hasLengthError()) {
            return self.institutionIdInput.lengthErrorMessage();
        }
        else if (self.hasBankAddPermissionError()) {
            return self.hasCaBankAddPermissionErrorMessage();
        }
        return null;
    });


    self.resolveInstitutionId = function (value) {
        
        if (self.institutionIdInput.hasError() || self.institutionIdInput.hasLengthError() || !value) {
            self.resolvedBankName(null);
            return Promise.resolve({});
       
        } else if (self.branchNumberNeedsInput()) {
            self.resolvedBankName(null);
            return Promise.resolve({});
        }
        else {
            var id = "{0}-{1}".format(value, params.dataModel.branchNumber());
            var bankPromise = actions.getBankAsync(id);
            bankPromise.then(function (bank) {
                self.resolvedBankNameError(false);
                self.resolvedBankName(bank.Name);
                return bank;
            }).catch(function (responseObject) {

                self.resolvedBankNameError(true);
                if (responseObject.StatusCode === 404) {
                    self.resolvedBankName(null);
                    self.resolvedBankNameErrorMessage(errorBankDoesNotExistMessage());
                    console.error(responseObject.Message + "\n" + errorBankDoesNotExistMessage())
                }
                else {
                    self.resolvedBankName(null);
                    console.warn(responseObject.Message + "\n" + errorUnableToResolveBankMessage());
                }
                return;
            });

            return bankPromise;
        }
    }
    //subscribe to the institutionIdInput. when it changes, call the resolveRoutingId function
    self.institutionIdInputSubscription = self.institutionIdInput.subscribe(self.resolveInstitutionId);

    self.accountIdBacker = ko.observable(null);
    self.accountIdInput = ko.computed({
        read: function () {
            return self.accountIdBacker();
        },
        write: function (value) {
            value = removeWhiteSpace(value);
            self.accountIdBacker(value);
            params.dataModel.newAccountId(value);
        }
    }).extend({
        lengthCheck: { maxLength: 17, maxLengthMessage: resources.getObservable('BankingInformation.ErrorAccountIdMaxLengthMessage') },
        formatCheck: { pattern: "([<>])", message: resources.getObservable('BankingInformation.ErrorInvalidCharactersMessage') },
    });

    self.accountIdNeedsInput = ko.pureComputed(function () {
        if (!self.accountIdBacker() || self.accountIdBacker().length === 0) {
            if (params.isAdd) {
                return true;
            }
            if (self.accountIdReentryInput() && self.accountIdReentryInput().length > 0) {
                return true;
            }
        }
        return false;
    });
    self.accountIdHasError = ko.computed(function () {
        return self.accountIdInput.hasFormatError() || self.accountIdInput.hasLengthError();
    });
    self.accountIdErrorMessage = ko.computed(function () {
        if (self.accountIdInput.hasFormatError()) {
            return self.accountIdInput.errorMessage();
        }
        else if (self.accountIdInput.hasLengthError()) {
            return self.accountIdInput.lengthErrorMessage();
        }
        return null;
    })

    self.accountIdReentryInput = ko.observable(null).extend({
        rateLimit: { timeout: 1000, method: "notifyWhenChangesStop" },
        lengthCheck: { maxLength: 17, maxLengthMessage: resources.getObservable('BankingInformation.ErrorAccountIdMaxLengthMessage') },
        formatCheck: { pattern: "([<>])", message: resources.getObservable('BankingInformation.ErrorInvalidCharactersMessage') },
    });



    params.dataModel.newAccountId.subscribe(function (newValue) {
        //need a subscribe to update local vars when data is reset
        if (!newValue) {
        self.accountIdBacker(newValue).accountIdReentryInput(newValue);
        }
    })

    self.accountIdReentryNeedsInput = ko.computed(function () {
        if (!self.accountIdReentryInput() || self.accountIdReentryInput().length === 0) {
            if (params.isAdd) {
                return true;
            }
            if (self.accountIdBacker() && self.accountIdBacker().length > 0) {
                return true;
            }
        }
        return false;
    })

    self.isAccountIdMatch = ko.computed(function () {
        if (self.accountIdBacker() && self.accountIdReentryInput()) {

            self.accountIdReentryInput(removeWhiteSpace(self.accountIdReentryInput())); // remove any whitespace from the backer; the id input is already handled in the computed

            if (self.accountIdBacker() === self.accountIdReentryInput()) {
                params.dataModel.accountIdLast4(self.accountIdBacker().slice(-4));
                return true;
            }
            return false;
        }
        return true;
    });
    
    self.accountIdReentryHasError = ko.computed(function () {
        return self.accountIdReentryInput.hasFormatError() || self.accountIdReentryInput.hasLengthError() || !self.isAccountIdMatch();
    })
    self.accountIdReentryErrorMessage = ko.computed(function () {
        if (self.accountIdReentryInput.hasFormatError()) {
            return self.accountIdReentryInput.errorMessage();
        }
        else if (self.accountIdReentryInput.hasLengthError()) {
            return self.accountIdReentryInput.lengthErrorMessage();
        }
        else if (!self.isAccountIdMatch()) {
            return resources.getObservable('BankingInformation.BankAccountNumberMismatchErrorMessage')();
        }
        return null;
    });

    var accountNumberEditMessage = resources.getObservable('BankingInformation.EditingAccountNumberMessage');
    self.isEditingAccountNumber = ko.observable(self.isAdd);
    self.toggleAccountNumberEdit = function () {
        self.isEditingAccountNumber(!self.isEditingAccountNumber());
        if (self.isEditingAccountNumber()) {
            $('#aria-announcements').text(accountNumberEditMessage());
        }
    }


    function removeWhiteSpace(value) {
        if (typeof value === 'string'){
            while (value.contains(' '))
                value = value.replace(' ', '');
        }
        return value;
    }

    //self.accountTypeBacker = ko.observable(self.Type());
    var accountType = function (id, name) {
        this.id = id;
        this.name = name;
    }
    self.accountTypes = ko.observableArray([new accountType(0, "Checking"), new accountType(1, "Savings")]);
    self.accountTypeInput = ko.computed({
        read: function () {
            return params.dataModel.accountType() === 0 ? self.accountTypes()[0] : self.accountTypes()[1]
        },
        write: function (value) {
            return params.dataModel.accountType(value.id)
        },
    });
    self.isEditingAccountType = ko.observable(self.isAdd);
    var accountTypeEditMessage = resources.getObservable('BankingInformation.EditingAccountTypeMessage');
    self.toggleEditAccountType = function () {
        self.isEditingAccountType(!self.isEditingAccountType());
        if (self.isEditingAccountType()) {
            $('#aria-announcements').text(accountTypeEditMessage());
        }
    }

    self.termsAndConditionsTitle = resources.getObservable('BankingInformation.AddAccountTermsAndConditionsLabel');
    self.termsAndConditionsParagraph = params.configuration.AddEditAccountTermsAndConditions;

    self.isTermsAccepted = ko.observable(!params.isAdd);

    self.hasInvalidInput = ko.computed(function () {
        var hasInvalidInput = self.hasBankAddPermissionError() ||
         self.nicknameInput.hasFormatError() ||
         self.nicknameInput.hasLengthError() ||
         (self.bankCountry() === 'United States' &&
            (self.routingIdInput.hasFormatError() ||
            self.routingIdNeedsInput())) ||
        (self.bankCountry() == "Canada" &&
            (self.branchNumberInput.hasError() ||
            self.branchNumberInput.hasLengthError() ||
            self.branchNumberNeedsInput() ||
            self.institutionIdInput.hasError() ||
            self.institutionIdInput.hasLengthError() ||
            self.institutionIdNeedsInput())) ||
        (!self.resolvedBankName() && params.isPayroll) ||
         self.accountIdInput.hasFormatError() ||
         self.accountIdInput.hasLengthError() ||
         self.accountIdNeedsInput() ||
         self.accountIdReentryInput.hasFormatError() ||
         self.accountIdReentryInput.hasLengthError() ||
         self.accountIdReentryNeedsInput() ||
         !self.isAccountIdMatch() ||
         (!self.isTermsAccepted() && params.isAdd);

        params.isValidInput(!hasInvalidInput);
        return hasInvalidInput;
    });

 
    $(".edit-icon-container .esg-icon__container").hover(function () {
        $(this).toggleClass("esg-icon__container--fill");
    });

}

module.exports = { viewModel: EditBankAccountDetailsViewModel, template: markup };