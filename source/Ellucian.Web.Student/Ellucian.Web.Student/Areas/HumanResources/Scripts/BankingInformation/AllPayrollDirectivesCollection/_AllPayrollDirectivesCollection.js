﻿var markup = require("./_AllPayrollDirectivesCollection.html");

/*
 * params: {
 *  dataModel: bankingInformationViewModel - c# server model
 * }
 */
function AllPayrollDirectivesCollectionViewModel(params) {
    var self = this;


    self.ActivePayrollDeposits = params.dataModel.ActivePayrollDeposits;
    self.FuturePayrollDeposits = params.dataModel.FuturePayrollDeposits;
    self.PastPayrollDeposits = params.dataModel.PastPayrollDeposits;

    var allPayrollDirectives = ko.computed(function () {
        return params.dataModel.ActivePayrollDeposits()
            .concat(params.dataModel.PastPayrollDeposits())
            .concat(params.dataModel.FuturePayrollDeposits());
    });

    self.hasAllCanadianAccounts = ko.computed(function () {
        var hasUsDirective = allPayrollDirectives().some(function (directive) {
            return directive.RoutingId() ? true : false;
        });
        return !hasUsDirective;
    });
}

module.exports = { viewModel: AllPayrollDirectivesCollectionViewModel, template: markup };