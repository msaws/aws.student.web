﻿var markup = require("./_PayableDirectiveSwitch.html");

/*
 * params: {
 *  directive: the directive being added/edited,
 *  isSwitchedOn: an observable that gets updated when the user flips the switch
 *  hasAllCanadianAccounts: computed indicating whether all accounts are canadian or not
 *  startDateObservable: an observable that gets updated with the user's start date input,
 *  isStartDateReadOnly: a computed that indicates whether the start date is read-only,
 *  startDateError: an observable that gets updated with a message when the user's input has some error
 * }
 */
function PayableDirectiveSwitchViewModel(params) {
    var self = this;

    self.isSwitchedOn = params.isSwitchedOn;
    self.hasAllCanadianAccounts = params.hasAllCanadianAccounts;

    self.Id = params.directive.Id;
    self.RoutingId = params.directive.RoutingId;
    self.IsVerified = params.directive.IsVerified;
    self.InstitutionId = params.directive.InstitutionId;
    self.EndDate = params.directive.EndDate;

    self.isVerificationVisible = ko.computed(function () {
        return (params.directive.RoutingId() || params.directive.InstitutionId());
    });

    self.startDateBacker = params.startDateObservable;
    self.startDateInput = ko.computed({
        read: function () {
            return Globalize.format(self.startDateBacker(), "d");
        },
        write: function (value) {
            var parsedDate = Globalize.parseDate(value, "d");
            (parsedDate) ? self.startDateBacker(parsedDate) : self.startDateBacker(value);
        },
    }).extend({
        dateCheck: {
            minDate: Date.Today()
        }
    });

    self.startDateInput.errorMessage.subscribe(function (newValue) {
        params.startDateError(newValue);
    });

    self.startDateIsReadOnly = ko.utils.unwrapObservable(params.isStartDateReadOnly);

    self.startDateNeedsInput = ko.pureComputed(function () {
        return !self.startDateIsReadOnly && (!self.startDateBacker() || self.startDateBacker() === "");
    });

}

module.exports = { viewModel: PayableDirectiveSwitchViewModel, template: markup };