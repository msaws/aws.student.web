﻿//TODO: move all error string to resource file
//document this:
//  target
//      bank
//      hasBank
//      hasFormatError
//      errorMessage
//  options
//      message?
//      dataFunction
ko.extenders.routingNumberCheck = function (target, options) {

    target.hasFormatError = ko.observable();
    target.errorMessage = ko.observable();

    var numericValuesOnlyMessage = options.numericOnlyMessage || "Only numeric values are allowed";
    var firstTwoDigitsMessage = options.firstTwoDigitsMessage || "The first two numbers entered are invalid";
    var incorrectLengthMessage = options.incorrectLengthMessage || "The routing number must be nine digits";
    var invalidMessage = options.invalidNumberMessage || "The routing number is invalid";


    var originalValue = target();
    function success() {
        target.hasFormatError(false);
        target.errorMessage(null);
    }

    function error(message) {
        target.hasFormatError(true);
        target.errorMessage(message);
    }


    function validateRoutingNumber(input) {
        if (!input) {
            success()
            return;
        }
        if (input) {

            // remove any whitespace or hyphen without creating error
            if (input.indexOf(" ") > -1 || input.indexOf("-") > -1) {
                while ((input.indexOf(" ") > -1 || input.indexOf("-") > -1)) {
                    input = input.replace(" ", "");
                    input = input.replace("-", "");
                }
                // update the field
                return target(input);
            }

            // verify all numeric values
            for (var i = 0; i < input.length; i++) {
                if (isNaN(input.charAt(i) / 1)) {

                    return error(ko.utils.unwrapObservable(numericValuesOnlyMessage));
                }
            }

            // verify first two
            var firstTwo = parseInt(input.substring(0, 2), 10);
            if (
                (firstTwo < 0) ||
                (firstTwo > 12 && firstTwo < 21) ||
                (firstTwo > 32 && firstTwo < 61) ||
                (firstTwo > 72 && firstTwo != 80)
                ) {

                return error(ko.utils.unwrapObservable(firstTwoDigitsMessage));
            }

            // verify length - can it be less than nine?
            if (input.length != 9) {
                return error(ko.utils.unwrapObservable(incorrectLengthMessage));
            }

            // verify checkSum
            if (
                   (3 * (1 * input.charAt(0) + 1 * input.charAt(3) + 1 * input.charAt(6)) +
                    7 * (1 * input.charAt(1) + 1 * input.charAt(4) + 1 * input.charAt(7)) +
                        (1 * input.charAt(2) + 1 * input.charAt(5) + 1 * input.charAt(8)))
                 % 10 != 0
            ) {
                return error(ko.utils.unwrapObservable(invalidMessage));
            }

            return success();
        }
    }


    validateRoutingNumber(target());
    target.subscribe(validateRoutingNumber);

    return target;
}