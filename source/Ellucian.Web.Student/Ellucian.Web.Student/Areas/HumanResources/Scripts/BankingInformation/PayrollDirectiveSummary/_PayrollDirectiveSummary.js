﻿var markup = require("./_PayrollDirectiveSummary.html");

/*
 * params: {
 *  dataModel: a PayrollDepositViewModel - C# server model,
 *  hasAllCanadianAccounts: a computed that indicates if all the payroll directives are canadian,
 *  index: the index of the directive in the list of directives,
 *  type: active || future || past
 * }
 */
function PayrollDirectiveSummaryViewModel(params) {
    var self = this;

    //self.dataModel = params.dataModel;

    //self.BankName = self.dataModel.BankName;
    self.IsVerified = params.dataModel.IsVerified;
    self.RoutingId = params.dataModel.RoutingId;
    self.InstitutionId = params.dataModel.InstitutionId;
    self.DepositAmount = params.dataModel.DepositAmount;
    self.DepositOrder = params.dataModel.DepositOrder;
    self.StartDate = params.dataModel.StartDate;
    self.EndDate = params.dataModel.EndDate;

    self.hasAllCanadianAccounts = params.hasAllCanadianAccounts;

    self.isPastDirective = params.type === 'past';

    self.displayBankName = ko.computed(function () {
        var bankName = params.dataModel.Nickname() ? params.dataModel.Nickname() : params.dataModel.BankName();
        return bankName;
    });

    // deposit order using index of array
    self.depositOrderIndex = ko.computed(function () {
        if (self.DepositOrder() === 999) {
            return 999;
        }
        return params.index() + 1;

    });

    // displays last for deposit order of 999
    self.displayDepositOrderAsLast = ko.computed(function () {
        if (self.DepositOrder() == 999) {
            return "Last";
        }
        return self.depositOrderIndex();
    });

    self.fourthColumnDataByType = ko.computed(function () {
        switch (params.type) {
            case "active":
                return self.displayDepositOrderAsLast();
            case "future":
                return Globalize.format(self.StartDate(), 'd');
            case "past":
                return Globalize.format(self.EndDate(), 'd');
        }
    })

    self.editPayrollDepositDirective = function () {
        if (!self.isPastDirective) {
            window.location.hash = "#payroll={0}".format(params.dataModel.Id());
        }
        return;
    }
}

module.exports = { viewModel: PayrollDirectiveSummaryViewModel, template: markup };