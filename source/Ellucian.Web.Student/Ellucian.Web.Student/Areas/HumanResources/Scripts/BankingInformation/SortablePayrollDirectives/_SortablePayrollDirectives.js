﻿var markup = require("./_SortablePayrollDirectives.html"),
    resources = require("Site/resource.manager");

require("BankingInformation/jquery.ui.sortable.list.binding");

/*
 * params: { 
*   directives,
*   editingDirectiveId,
*   editingDirectiveAmountOption,
*   numberNonRemainderDirectives
 * }
 */
function SortablePayrollDirectivesViewModel(params) {
    var self = this;


    self.sortableOptions = {
        items: ".sortable-list-item",
        containment: "parent"
    };

    self.sortableItems = ko.observableArray(
        params.directives().sort(function (a, b) {
            if (a.DepositOrder() < b.DepositOrder()) return -1;
            if (a.DepositOrder() > b.DepositOrder()) return 1;
            return 0;
        }).map(function (directive, index) {
            if (directive.DepositOrder() !== 999) {
                directive.DepositOrder(index + 1);
            }
            return new sortableItem(directive, index);
        })
    );

    self.sortableItems.subscribe(function (newVal) {
        newVal.forEach(function (si, index) {
            si.index(index);
        });
    });


    function sortableItem(directive, index) {

        var item = this;
        
        item.index = ko.observable(index);

        

        item.id = directive.Id();

        item.displayBankName = ko.computed(function () {
            return directive.Nickname() ? directive.Nickname() : directive.BankName();
        });



        item.isEditingDirective = directive.Id() === params.editingDirectiveId;

        item.isRemainder = ko.computed(function () {
            if (directive.DepositOrder() === 999) {
                return true;
            }
            else if (item.isEditingDirective && params.editingDirectiveAmountOption() !== 'isSpecific') {
                return true;
            }
            return false;
        });

        item.isLocked = ko.computed(function () {
            return item.isRemainder() || params.numberNonRemainderDirectives() === 1;
        });

        item.sortableCssClass = ko.computed(function () {
            return item.isLocked() ? 'unsortable-list-item' : 'sortable-list-item';
        });
        item.newDirectiveCssClass = ko.computed(function () {
            return !directive.Id() ? 'new-sortable-item' : "";
        })

        item.priorityDisplay = ko.computed(function () {
            return item.isRemainder() ? resources.getObservable('BankingInformation.BalanceAccountText')() : item.index() + 1;
        });

        item.isMoveUpButtonVisible = ko.computed(function () {
            return item.index() > 0;
        });
        item.isMoveDownButtonVisible = ko.computed(function () {
            return item.index() < params.numberNonRemainderDirectives() - 1;
        });

        item.moveUp = function () {
            
            self.sortableItems.remove(item);
            self.sortableItems.splice(item.index() - 1, 0, item);
            //sortableList.splice(newPosition, 0, item);
        }

        item.moveDown = function () {
            self.sortableItems.remove(item);
            self.sortableItems.splice(item.index() + 1, 0, item);

        }

        item.index.subscribe(function (newIndex) {
            if (!item.isLocked()) {
                directive.DepositOrder(newIndex + 1);
            }
        });
    }



    
}

module.exports = { viewModel: SortablePayrollDirectivesViewModel, template: markup };
