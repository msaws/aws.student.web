﻿var markup = require("./_CreateNewDirective.html"),
    resources = require("Site/resource.manager"),
    actions = require("BankingInformation/banking.information.actions");

/**
 * params: {
 *  dataModel: bankingInformationViewModel - C# server model
 * }
 */
function CreateNewDirectiveViewModel(params) {
    var self = this;

    self.IsVendor = params.dataModel.IsVendor;
    self.Configuration = params.dataModel.Configuration;
    self.IsPayableActive = params.dataModel.IsPayableActive;
    self.IsPayrollActive = params.dataModel.IsPayrollActive;

    var allPayrollDirectives = ko.computed(function () {
        return params.dataModel.ActivePayrollDeposits()
           .concat(params.dataModel.PastPayrollDeposits())
           .concat(params.dataModel.FuturePayrollDeposits());
    });
    var unwrappedRefundDeposit = ko.unwrap(params.dataModel.ActiveRefundDeposit);
    var allPayableDirectives = ko.computed(function () {
        return (unwrappedRefundDeposit ? [unwrappedRefundDeposit] : [])
            .concat(params.dataModel.FutureRefundDeposits())
            .concat(params.dataModel.PastRefundDeposits());
    });
    var allDepositDirectives = ko.computed(function () {
        return allPayrollDirectives().concat(allPayableDirectives());
    });

    self.hasAllCanadianAccounts = ko.computed(function () {
        var hasUsDirective = allDepositDirectives().some(function (directive) {
            return directive.RoutingId() ? true : false;
        });
        return !hasUsDirective;
    });

    var currentActiveRemainderDirective = ko.computed(function () {
        var activeRemainder = params.dataModel.ActivePayrollDeposits().find(function (directive) {
            return directive.DepositOrder() === 999;
        });
        return activeRemainder;
    });
    var pastRemainderDirective = ko.computed(function () {
        var pastRemainder = params.dataModel.PastPayrollDeposits().find(function (directive) {
            return directive.DepositOrder() === 999;
        });
        return pastRemainder;
    });
    var futureRemainderDirective = ko.computed(function () {
        var futureRemainder = params.dataModel.FuturePayrollDeposits().find(function (directive) {
            return directive.DepositOrder() === 999;
        });
        return futureRemainder;
    })

    var activeAndFutureNonRemainders = ko.computed(function () {
        var nonRemainders = params.dataModel.ActivePayrollDeposits().concat(params.dataModel.FuturePayrollDeposits()).filter(function (directive) {
            return directive.DepositOrder() !== 999;
        });
        return nonRemainders;
    });
    var activeAndFutureRemainders = ko.computed(function () {
        var remainders = params.dataModel.ActivePayrollDeposits().concat(params.dataModel.FuturePayrollDeposits()).filter(function (directive) {
            return directive.DepositOrder() === 999;
        });
        return remainders;
    })

    var payableDirective = {
        Id: null,
        PayeeId: params.dataModel.PayeeId(),
        RoutingId: null,
        InstitutionId: null,
        BranchNumber: null,
        BankName: null,
        NewAccountId: null,
        AccountIdLastFour: null,
        Nickname: "New Account",
        Type: 0,
        SecurityToken: null,
        AddressId: params.dataModel.AddressId(),
        StartDate: Date.Today(),
        EndDate: null,
        IsElectronicPaymentRequested: true,
        IsVerified: false,
        AddDateTime: new Date(),
        ChangeDateTime: new Date(),
        AddOperator: params.dataModel.PayeeId,
        ChangeOperator: params.dataModel.PayeeId,
    }

    var payrollDirective = {
        Id: null,
        PersonId: params.dataModel.PayeeId(),
        RoutingId: null,
        InstitutionId: null,
        BranchNumber: null,
        BankName: null,
        NewAccountId: null,
        AccountIdLastFour: null,
        Nickname: "New Account",
        Type: 0,
        SecurityToken: null,
        DepositAmount: null,
        StartDate: Date.Today(),
        EndDate: null,
        OriginalDepositOrder: 998,
        DepositOrder: 998,
        IsVerified: false,
        AddDateTime: new Date(),
        ChangeDateTime: new Date(),
        AddOperator: params.dataModel.PayeeId(),
        ChangeOperator: params.dataModel.PayeeId(),
    }


    self.payableDirectiveModel = ko.mapping.fromJS(payableDirective);
    self.payrollDirectiveModel = ko.mapping.fromJS(payrollDirective);

    //step auth stuff
    self.securityToken = ko.observable(null);


    self.directiveTypeToVerify = null;

    var directiveToVerify = ko.computed(function () {
        if (allDepositDirectives().length == 0) {
            return null;
        }
        var payroll = self.IsPayrollActive();
        var payable = self.IsPayableActive();
        //activeRemainder if it exists,
        //highest prioirty active payroll, if active exist
        //active refund, if it exists
        //pastRemainder, if it exists,
        //most recently ended past payroll, if past exist
        //most recently ended past refund, if past exist
        //futureRemainder, if it exists,
        //next future payroll, if future exist,
        //next future refund, it future exist

        if (currentActiveRemainderDirective() && payroll) {
            self.directiveTypeToVerify = "payroll";
            return currentActiveRemainderDirective();
        }
        else if (params.dataModel.ActivePayrollDeposits().length > 0 && payroll) {
            self.directiveTypeToVerify = "payroll";
            return params.dataModel.ActivePayrollDeposits()[0];
        }
        else if (unwrappedRefundDeposit && payable) {
            self.directiveTypeToVerify = "payable";
            return unwrappedRefundDeposit;
        }
        else if (pastRemainderDirective() && payroll) {
            self.directiveTypeToVerify = "payroll";
            return pastRemainderDirective();
        }
        else if (params.dataModel.PastPayrollDeposits().length > 0 && payroll) {
            self.directiveTypeToVerify = "payroll";
            return params.dataModel.PastPayrollDeposits()[0];
        }
        else if (params.dataModel.PastRefundDeposits().length > 0 && payable) {
            self.directiveTypeToVerify = "payable";
            return params.dataModel.PastRefundDeposits()[0];
        }
        else if (futureRemainderDirective() && payroll) {
            self.directiveTypeToVerify = "payroll";
            return futureRemainderDirective();
        }
        else if (params.dataModel.FuturePayrollDeposits().length > 0 && payroll) {
            self.directiveTypeToVerify = "payroll";
            return params.dataModel.FuturePayrollDeposits()[0];
        }
        else {
            self.directiveTypeToVerify = "payable";
            return params.dataModel.FutureRefundDeposits()[0];
        }
    });
    self.directiveIdToVerify = directiveToVerify() ? directiveToVerify().Id() : null;
    self.authenticationDirectiveLastFour = directiveToVerify() ? directiveToVerify().AccountIdLastFour() : null;
    self.authenticationDisplayHint = ko.computed(function () {
        var hint = null;
        if (directiveToVerify()) {
            if (directiveToVerify().Nickname()) {
                hint = directiveToVerify().Nickname();
            }
            else if (directiveToVerify().BankName()) {
                hint = directiveToVerify().BankName();
            }
            else {
                hint = directiveToVerify().RoutingId() ? directiveToVerify().RoutingId() : "{0}-{1}".format(directiveToVerify().InstitutionId(), directiveToVerify().BranchNumber());
            }

            return hint;
        }
        return null;
    });


    if (!directiveToVerify()) {
        authenticateNewUser();
    }

    function authenticateNewUser() {
        var action = null;
        if (self.IsPayrollActive()) {
            action = actions.getPayrollAuthenticationAsync;
        } else if (self.IsPayableActive()) {
            action = actions.getPayableAuthenticationAsync;
        }

        if (!directiveToVerify() && action != null) {
            return action("", "", params.dataModel.AddressId()).then(function (tokenObj) {
                self.securityToken(tokenObj);
            }).catch(function (err) {
                $('#notificationHost').notificationCenter('addNotification', { message: authenticationErrorMessage(), type: 'error' });
                console.error(err);
                window.history.back();
            });
        }
        return Promise.resolve();
    }
    var authenticationErrorMessage = resources.getObservable('BankingInformation.AuthenticationErrorMessage');

    //payable switch stuff

    self.isPayableSwitchedOn = ko.observable(false);
    self.payableStartDateError = ko.observable(null);

    //payroll switch stuff
    self.isPayrollSwitchedOn = ko.observable(false);
    self.isPayrollSwitchVisible = ko.computed(function () {
        return true;
    });
    self.payrollDepositAmountOption = ko.observable();
    self.isPayrollRemainderDeposit = ko.computed(function () {
        return (self.payrollDirectiveModel.DepositOrder() === 999 || self.payrollDepositAmountOption() == 'isRemainder' || self.payrollDepositAmountOption() == 'isBalance');
    });


    self.payrollStartDateError = ko.observable(null);

    self.isPayrollEndDateVisible = ko.computed(function () {
        return !params.dataModel.Configuration.IsRemainderAccountRequired() || !self.isPayrollRemainderDeposit();
    });
    self.payrollEndDateError = ko.observable(null);

    self.isFuturePayrollRemainderAndCurrentRemainderExists = ko.computed(function () {
        var exists = self.Configuration.IsRemainderAccountRequired() &&
                self.payrollDirectiveModel.OriginalDepositOrder() === 999 &&
                Date.Compare(self.payrollDirectiveModel.StartDate(), Date.Today()) == 1 &&
                currentActiveRemainderDirective();
        return exists;
    });

    self.payrollEffectiveDateMessage = ko.computed(function () {
        if (self.Configuration.PayrollEffectiveDateMessage() && self.isPayrollSwitchedOn()) {
            return self.Configuration.PayrollEffectiveDateMessage();
        }
        return null;
    });
    self.willChangeEndDateOfCurrentRemainderMessage = ko.computed(function () {
        if (self.isFuturePayrollRemainderAndCurrentRemainderExists()) {
            return resources.getObservable('BankingInformation.WillChangeEndDateOfCurrentRemainderMessage')();
        }
        return null;
    });


    //amount option stuff here
    //initialize amount option
    if (params.dataModel.ActivePayrollDeposits().length === 0) {
        self.payrollDepositAmountOption("isBalance");
    } else if (self.Configuration.IsRemainderAccountRequired() && !currentActiveRemainderDirective()) {
        self.payrollDepositAmountOption("isRemainder");
    } else {
        self.payrollDepositAmountOption("isSpecific");
    }

    self.specificAmountIsVisible = ko.computed(function () {
        var isVisible = (self.Configuration.IsRemainderAccountRequired() && currentActiveRemainderDirective()) ||
            !self.Configuration.IsRemainderAccountRequired();
        return isVisible;
    });
    self.specificDepositAmountError = ko.observable(null);
    self.showRemainingBalanceOption = ko.computed(function () {
        return params.dataModel.ActivePayrollDeposits().length > 0;
    });
    self.depositAmountNeedsInput = ko.computed(function () {
        var needsInput = self.isPayrollSwitchedOn() &&
            self.payrollDepositAmountOption() === 'isSpecific' &&
            (!self.payrollDirectiveModel.DepositAmount() || self.payrollDirectiveModel.DepositAmount() === '');
        return needsInput;
    });

    //sortable stuff here
    self.sortablePayrollDeposits = ko.computed(function () {
        return params.dataModel.ActivePayrollDeposits().concat([self.payrollDirectiveModel]);
    });
    self.editingDirectiveId = self.payrollDirectiveModel.Id();
    self.numberActiveNonRemainderDirectives = ko.computed(function () {
        var nonRemainderDirectives = self.sortablePayrollDeposits().filter(function (directive) {
            return directive.DepositOrder() !== 999;
        });
        return nonRemainderDirectives.length;
    });

    //button bar stuff
    self.hasInputErrors = ko.computed(function () {
        var hasPayableErrors = false,
            hasPayrollErrors = false;
        if (self.isPayableSwitchedOn()) {
            hasPayableErrors = self.payableStartDateError() || !self.payableDirectiveModel.StartDate() ? true : false;
        }

        if (self.isPayrollSwitchedOn()) {
            hasPayrollErrors = self.payrollStartDateError() ||
                !self.payrollDirectiveModel.StartDate() ||
                self.depositAmountNeedsInput() ||
                self.specificDepositAmountError() ? true : false;
        }

        return hasPayableErrors || hasPayrollErrors;
    });

    self.isNextStepEnabled = ko.computed(function () {
        var isEnabled = !self.hasInputErrors() && (self.isPayableSwitchedOn() || self.isPayrollSwitchedOn());
        return isEnabled;
    });

    self.accountDetailsModel = {
        accountIdLast4: ko.observable(null),
        nickname: ko.observable("New Account"),
        bankName: ko.observable(null),
        routingId: ko.observable(null),
        institutionId: ko.observable(null),
        branchNumber: ko.observable(null),
        newAccountId: ko.observable(null),
        accountType: ko.observable(0),
        isVerified: ko.observable(false)
    }

    self.isBankAccountDetailsInputValid = ko.observable(false);

    self.showEditBankAccountDetails = ko.observable(false);
    self.nextStep = function () {
        self.showEditBankAccountDetails(true);
    }

    self.hideEditBankAccountDetails = function () {
        self.showEditBankAccountDetails(false);
    }

    
    var depositsToDelete = ko.observableArray([]);
    var deletedDepositIds = ko.computed(function() {
        return depositsToDelete().map(function (dep) {
            return dep.Id();
        });
    });
    self.savingNewDirectiveMessage = resources.getObservable("BankingInformation.CreatingNewDepositsMessage");
    self.isSaveNewDirectiveInProgress = ko.observable(false);
    self.saveNewDirective = function () {
        if (!self.isBankAccountDetailsInputValid()) {
            return Promise.resolve();
        }
        self.isSaveNewDirectiveInProgress(true);
        self.showEditBankAccountDetails(false);

        if (self.isPayableSwitchedOn()) {
            updateModelWithBankAccountDetails(self.payableDirectiveModel);
            self.payableDirectiveModel.SecurityToken(self.securityToken());
        }
        if (self.isPayrollSwitchedOn()) {
            updateModelWithBankAccountDetails(self.payrollDirectiveModel);
            updatePayrollDataModel(self.payrollDirectiveModel);
            self.payrollDirectiveModel.SecurityToken(self.securityToken());
        }

        var createErrorMessage = resources.getObservable("BankingInformation.CreateDepositError");

        var payableAction = self.isPayableSwitchedOn() ?
            actions.createPayableDepositDirectiveAsync(ko.mapping.toJS(self.payableDirectiveModel)) :
            Promise.resolve(null);

        //first create the payable
        return payableAction.then(function (payableModel) {
            if (payableModel !== null) {
                self.payableDirectiveModel.Id(payableModel.Id);
            }

            //delete deposits first
            if (self.isPayrollSwitchedOn()) {
                var tasks = depositsToDelete().map(function (dep) {
                    return actions.deletePayrollDepositDirectiveAsync(ko.mapping.toJS(dep));
                });
                return Promise.all(tasks);
            }
            return Promise.resolve(null);
        }).then(function (deleteTasks) {

            //then update the existing deposits next, 
            //particularly to end any remainders so as not to create an overlap situation
            if (self.isPayrollSwitchedOn()) {
                var directivesToUpdate = params.dataModel.ActivePayrollDeposits()
                    .concat(params.dataModel.FuturePayrollDeposits()) //concat the Active+Future directives
                    .filter(function (dep) {
                        return deletedDepositIds().indexOf(dep.Id()) < 0; //filter out the ones that will be deleted
                    });
                if (directivesToUpdate.length > 0) {
                    return actions.updatePayrollDepositDirectivesAsync(ko.mapping.toJS(directivesToUpdate), ko.mapping.toJS(self.securityToken));
                }
            }
            return Promise.resolve(null);

        }).then(function () {
            //finally, create the new deposit
            if (self.isPayrollSwitchedOn()) {
                return actions.createPayrollDepositDirectiveAsync(ko.mapping.toJS(self.payrollDirectiveModel))
            }
            return Promise.resolve(null);

        }).then(function (payrollModel) {
            if (payrollModel !== null) {
                self.payrollDirectiveModel.Id(payrollModel.Id);
            }
            return;
        }).catch(function (err) {
            console.error(err);
            $('#notificationHost').notificationCenter('addNotification', { message: createErrorMessage(), type: 'error', flash: true });

            var payableDeleteAction = self.payableDirectiveModel.Id() ?
                actions.deletePayableDepositDirectiveAsync(ko.mapping.toJS(self.payableDirectiveModel)) :
                Promise.resolve();
            var payrollDeleteAction = self.payrollDirectiveModel.Id() ?
                actions.deletePayrollDepositDirectiveAsync(ko.mapping.toJS(self.payrollDirectiveModel)) :
                Promise.resolve();

            return Promise.all([payableDeleteAction, payrollDeleteAction]);

        }).then(function () {
            self.isSaveNewDirectiveInProgress(false);
            window.history.back();
        });
    }

    var updateModelWithBankAccountDetails = function (directiveModel) {
        var model = self.accountDetailsModel;
        directiveModel
            .AccountIdLastFour(model.accountIdLast4())
            .Nickname(model.nickname())
            .BankName(model.bankName())
            .RoutingId(model.routingId())
            .InstitutionId(model.institutionId())
            .BranchNumber(model.branchNumber())
            .NewAccountId(model.newAccountId())
            .Type(model.accountType())
            .IsVerified(model.isVerified());
    }

    function updatePayrollDataModel(payrollDirectiveModel) {

        if (self.payrollDepositAmountOption() == 'isBalance') {

            //if isBalance is selected, change the enddate of all accounts that end after the new one starts to the new start date - 1 day;
            //loop through all the remainders and make sure no remainder dates overlap, including modifying the end date of the new one if necessary

            payrollDirectiveModel.DepositOrder(999);
            payrollDirectiveModel.DepositAmount(null);

            // end all accounts besides the new one past the new account start date except any remainder
            activeAndFutureNonRemainders().forEach(function (dpst) {
                var newEndDate = new Date(payrollDirectiveModel.StartDate()); // need to create a new date for each iteration or else changes will accumulate

                //if this dpst has no end date (or the 
                if (!dpst.EndDate() || (Date.Compare(dpst.EndDate(), newEndDate) > 1)) {

                    //if the start date of the deposit is less than the newEndDate (which is the start date of the new directive)
                    if (Date.Compare(dpst.StartDate(), newEndDate) < 0) {
                        //use the start date minus 1
                        newEndDate = newEndDate.addDays(-1);
                    }
                    else if (Date.Compare(dpst.StartDate(), newEndDate) >= 0 &&//if the deposit starts on the same day as or starts later than as the new balance startdate
                        (!payrollDirectiveModel.EndDate() || Date.Compare(payrollDirectiveModel.EndDate(), dpst.StartDate()) >= 0)) { //and the new balance doesn't end, or it ends after this deposit starts

                        //delete it                      

                        dpst.SecurityToken(self.securityToken());
                        depositsToDelete.push(dpst);

                    }

                    //otherwise, just use the Start Date of the new directive, so that the Start and End dates of this
                    //dpst are the same. (Prevents having an end date less than the start date).

                    dpst.EndDate(newEndDate);
                }
            });




            //now do remainders
            activeAndFutureRemainders().forEach(function (remainder) {
                var startDate = new Date(payrollDirectiveModel.StartDate());
                updateRemainderDates(remainder, startDate, payrollDirectiveModel);
            });
        }
        else if (self.payrollDepositAmountOption() == 'isRemainder') {
            //if isRemainder is selected
            //loop through all the remainders and make sure no remainder dates overlap, including modifying the end date of the new one if necessary


            payrollDirectiveModel.DepositOrder(999);
            payrollDirectiveModel.DepositAmount(null);

            activeAndFutureRemainders().forEach(function (remainder) {
                var startDate = new Date(payrollDirectiveModel.StartDate());
                updateRemainderDates(remainder, startDate, payrollDirectiveModel);
            });
        }

    }

    function updateRemainderDates(otherRemainder, startDateUpdate, payrollDirective) {

        //if the new directive start date is greater than the other remainder's start date      
        //startDateUpdate > otherRemainder.StartDate
        if (Date.Compare(startDateUpdate, otherRemainder.StartDate()) === 1) {

            //update the other remainder's end date if:
            if (self.Configuration.IsRemainderAccountRequired() || //remainder account is required
                !otherRemainder.EndDate() || //there's no end date on the other remainder
                Date.Compare(startDateUpdate, otherRemainder.EndDate()) < 1) { //the start date of the new remainder is before the end date of the existing remainder

                //set the other remainder end date to the start date-1        
                otherRemainder.EndDate(startDateUpdate.addDays(-1));
            } //else, remainder accounts aren't required and the end date is already before the new start date
        }
        else {

            // new remainder is current and other is future, or they share startDate
            // startDateUpdate <= otherRemainder.StartDate
            //if the new payroll directive doesn't have an end date, delete the future remainder
            if (!payrollDirective.EndDate()) {
                otherRemainder.SecurityToken(self.securityToken());
                depositsToDelete.push(otherRemainder);

            } else if 
                
                (self.Configuration.IsRemainderAccountRequired() || //remainder account is required, or             
                Date.Compare(payrollDirective.EndDate(), otherRemainder.StartDate()) >= 0) { //the end date the user entered is after the other remainder's start date

                otherRemainder.StartDate((new Date(payrollDirective.EndDate())).addDays(1)); //move the start of the other remainder to be a day after the new one.

                if (otherRemainder.EndDate() && Date.Compare(otherRemainder.StartDate(), otherRemainder.EndDate()) > 0) {
                    otherRemainder.SecurityToken(self.securityToken());
                    depositsToDelete.push(otherRemainder);
                }

            } //else remainder accounts aren't required and the new end date is set before the next remainder's start date
        }
    }

    self.cancel = function () {
        window.history.back();
    }


    self.dispose = function () {       
        deletedDepositIds.dispose();
        self.isNextStepEnabled.dispose();
        self.hasInputErrors.dispose();
        self.numberActiveNonRemainderDirectives.dispose();
        self.sortablePayrollDeposits.dispose();
        self.depositAmountNeedsInput.dispose();
        self.showRemainingBalanceOption.dispose();
        self.specificAmountIsVisible.dispose();
        self.willChangeEndDateOfCurrentRemainderMessage.dispose();
        self.payrollEffectiveDateMessage.dispose();
        self.isFuturePayrollRemainderAndCurrentRemainderExists.dispose();
        self.isPayrollEndDateVisible.dispose();
        self.isPayrollRemainderDeposit.dispose();
        self.isPayrollSwitchVisible.dispose();
        self.authenticationDisplayHint.dispose();
        activeAndFutureRemainders.dispose();
        activeAndFutureNonRemainders.dispose();
        futureRemainderDirective.dispose();
        pastRemainderDirective.dispose();
        currentActiveRemainderDirective.dispose();
        self.hasAllCanadianAccounts.dispose();
        allDepositDirectives.dispose();
        allPayableDirectives.dispose();
        allPayrollDirectives.dispose();
    }

}

module.exports = { viewModel: CreateNewDirectiveViewModel, template: markup };