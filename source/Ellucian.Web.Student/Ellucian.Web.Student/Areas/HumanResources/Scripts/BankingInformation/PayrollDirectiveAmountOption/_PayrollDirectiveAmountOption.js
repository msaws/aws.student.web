﻿var markup = require("./_PayrollDirectiveAmountOption.html");

/*
 * params: {
 *  depositAmountOption: observable that will be updated with the user's choice of deposit amount option,
 *  showSpecificAmountOption: computed that indicates whether the specific amount option is available,
 *  specificDepositAmount: observable that will be updated with the user's specific deposit amount,
 *  specificDepositAmountError: observable that will be updated if the user's entry is invalid
 *  showRemainingBalanceOption: computed that indicates wehther the remaining balance option is available,
 *  
 * }
 */
function PayrollDirectiveAmountOptionViewModel(params) {
    var self = this;

    self.depositAmountOption = params.depositAmountOption;

    self.showSpecificAmountOption = params.showSpecificAmountOption;
    self.showRemainingBalanceOption = params.showRemainingBalanceOption;

    self.depositAmountBacker = params.specificDepositAmount;
    self.depositAmountInput = ko.computed({
        read: function () {
            return Globalize.format(self.depositAmountBacker(), 'C');
        },
        write: function (value) {
            var parsedValue = Globalize.parseFloat(value);
            if (!isNaN(parsedValue)) {
                var roundedVal = Math.round(parsedValue * 100) / 100;
                if (self.depositAmountBacker() === roundedVal) {
                    //setting to null to register data change.
                    self.depositAmountBacker(null);
                }
                self.depositAmountBacker(roundedVal);
                return;
            }
            self.depositAmountBacker(value);
            return;
        },

    }).extend({
        simpleNumericValidation: { minValue: 0, invalidMessage: "Please enter a valid amount" }
    });

    self.depositAmountInput.errorMessage.subscribe(function (newValue) {
        params.specificDepositAmountError(newValue);
    })

    self.depositAmountNeedsInput = ko.computed(function () {
        var needsInput = self.depositAmountOption() === 'isSpecific' && (!self.depositAmountInput() || self.depositAmountInput() === '');
        return needsInput;
    });

    self.isAmountErrorVisible = ko.computed(function () {
        return (self.depositAmountOption() === 'isSpecific') && self.depositAmountInput.hasError()
    });
}

module.exports = { viewModel: PayrollDirectiveAmountOptionViewModel, template: markup };