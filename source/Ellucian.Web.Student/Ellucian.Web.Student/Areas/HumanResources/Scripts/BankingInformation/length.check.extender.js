﻿
ko.extenders.lengthCheck = function (target, options) {
    target.hasLengthError = ko.observable();
    target.lengthErrorMessage = ko.observable();


    var minLength = options.minLength || null;
    var minLengthMessage = options.minLengthMessage || "Input must be greater than " + options.minLength + " characters";
    var maxLength = options.maxLength || null;
    var maxLengthMessage = options.maxLengthMessage || "Input must be less than " + options.maxLength + " characters";
    var exactLength = options.exactLength || null;
    var exactLengthMessage = options.exactLengthMessage || "Input must be " + options.exactLength + " characters";
    var charToRemove = options.removeChar || null;

    function success() {
        target.hasLengthError(false);
        target.lengthErrorMessage(null);
    }

    function error(message) {
        target.hasLengthError(true);
        target.lengthErrorMessage(message);
    }

    function validateLength(input) {

        if (ko.utils.unwrapObservable(charToRemove)) {
            input = input.replace(ko.utils.unwrapObservable(charToRemove), "");
            target(input);
        }

        if (input === null || input === "") {
            return success();
        }
        if (ko.utils.unwrapObservable(exactLength) !== null) {
            if (input.length === ko.utils.unwrapObservable(exactLength)) {
                success();
            } else {
                return error(ko.utils.unwrapObservable(exactLengthMessage))
            }
        }

        if (ko.utils.unwrapObservable(minLength) !== null) {
            if (input.length >= ko.utils.unwrapObservable(minLength)) {
                success();
            } else {
                return error(ko.utils.unwrapObservable(minLengthMessage));
            }
        }

        if (ko.utils.unwrapObservable(maxLength) !== null) {
            if (input.length <= ko.utils.unwrapObservable(maxLength)) {
                success();
            } else {
                return error(ko.utils.unwrapObservable(maxLengthMessage));
            }
        }
        return success();
    }
    validateLength(target());
    target.subscribe(validateLength);

    return target;
}
