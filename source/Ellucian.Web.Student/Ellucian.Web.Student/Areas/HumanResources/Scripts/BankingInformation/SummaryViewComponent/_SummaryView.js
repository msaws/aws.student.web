﻿var markup = require("./_SummaryView.html");
var resources = require("Site/resource.manager");
var actions = require("BankingInformation/banking.information.actions");

/*
 * params: {
 *  type: null || "payroll" || "payable"
 *  addressId : null || "1231"
 * }
 */
function SummaryViewViewModel(params) {
    var self = this;

    self.type = params.type;

    self.isLoading = ko.observable(true);
    self.loadingMessage = resources.getObservable("BankingInformation.LoadingBankingInformationMessage");

    self.loadError = ko.observable(false);
    self.loadErrorMessage = resources.getObservable('BankingInformation.LoadBankingInformationErrorMessage');

    self.loadDataPromise = actions.getBankingInformationViewModelAsync("", params.addressId).then(function (data) {
        self.dataModel = ko.mapping.fromJS(data);
        
        self.loadError(false).isLoading(false);
    }).catch(function (err) {
        self.loadError(true).isLoading(false);
        console.error(err);
        //no need to flash a notifcation
    });

    self.dataModel = {};


    self.back = function () {
        window.history.back();
        return false;
    }
}

module.exports = { viewModel: SummaryViewViewModel, template: markup };
