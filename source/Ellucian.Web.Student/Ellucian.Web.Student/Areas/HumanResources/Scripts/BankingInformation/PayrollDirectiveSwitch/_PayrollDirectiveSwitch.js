﻿var markup = require("./_PayrollDirectiveSwitch.html");

/*
 * params: {
 *  isVendor: whether or not this is a vendor workflow
 *  isSwitchedOn: observable that gets updated when the user flips the switch,
 *  isSwitchVisible: computed that indicates whether the switch is visible - in some cases the switch is required to be on
 *  directive: the payroll directive being edited/created,
 *  hasAllCanadianAccounts: computed indicating whether all accounts are canadian or not
 *  startDateObservable: an observable that gets updated with the user's start date input,
 *  isStartDateReadOnly: a computed that indicates whether the start date is read-only,
 *  startDateError: an observable that gets updated with a message when the user's input has some error
 *  isEndDateVisible: a computed that indicates whether the end date (input or read-only) is visible,
 *  endDateObservable: an observable that gets updated with the user's end date input,
 *  endDateError: an observable that gets updated with a message when the user's input has some error,
 *  cannotDeactivateRemainderMessage: a computed that shows a message if user cannot end this directive,
 *  effectiveDateMessage: a computed that shows a message if there's a message to display about the start date,
 *  willChangeEndDateOfCurrentRemainderMessage: a computed that shows a message if the current directive will change the end date of the current remainder
 * }
 */
function PayrollDirectiveSwitchViewModel(params) {
    var self = this;

    self.isVendor = ko.utils.unwrapObservable(params.isVendor);
    self.isSwitchedOn = params.isSwitchedOn;
    self.isSwitchVisible = params.isSwitchVisible;

    self.hasAllCanadianAccounts = params.hasAllCanadianAccounts;

    self.IsVerified = params.directive.IsVerified;
    self.RoutingId = params.directive.RoutingId;
    self.InstitutionId = params.directive.InstitutionId;
    self.StartDate = params.directive.StartDate;
    self.cannotDeactivateRemainderMessage = params.cannotDeactivateRemainderMessage;
    self.effectiveDateMessage = params.effectiveDateMessage;
    self.willChangeEndDateOfCurrentRemainderMessage = params.willChangeEndDateOfCurrentRemainderMessage;

    self.isAdd = ko.computed(function () {
        return params.directive.Id() ? false : true;
    });

    self.startDateBacker = params.startDateObservable;
    self.startDateInput = ko.computed({
        read: function () {
            return Globalize.format(self.startDateBacker(), "d");
        },
        write: function (value) {
            var parsedDate = Globalize.parseDate(value, "d");
            (parsedDate) ? self.startDateBacker(parsedDate) : self.startDateBacker(value);
        },
    }).extend({
        dateCheck: {
            minDate: Date.Today(),

        }
    });

    self.startDateInput.errorMessage.subscribe(function (newValue) {
        params.startDateError(newValue);
    });


    self.startDateIsReadOnly = ko.utils.unwrapObservable(params.isStartDateReadOnly);

    self.startDateNeedsInput = ko.pureComputed(function () {
        return !self.startDateIsReadOnly && (!self.startDateBacker() || self.startDateBacker() === "");
    });

    self.isEndDateVisible = params.isEndDateVisible;
    self.endDateBacker = params.endDateObservable;
    self.endDateOption = ko.observable(params.endDateObservable() ? "endDate" : "noEndDate");
    self.endDateOption.subscribe(function (newVal) {
        if (newVal == "noEndDate") {
            self.endDateBacker(null);
        }
    })

    

    self.endDateInput = ko.computed({
        read: function () {
            return (self.endDateBacker()) ? Globalize.format(self.endDateBacker(), "d") : "";
        },
        write: function (value) {
            var parsedDate = Globalize.parseDate(value, "d");
            (parsedDate) ? self.endDateBacker(parsedDate) : self.endDateBacker(value);
        }
    }).extend(
    {
        dateCheck:
        {
            //min date is yesterday if startDate is before yesterday. if startdate is yesterday, today, or future, min date is startDate + 1
            minDate: Date.Compare(params.directive.StartDate(), Date.Today()) < 0 ? Date.Today() : params.directive.StartDate()
        }
    });

    self.endDateInput.errorMessage.subscribe(function (newValue) {
        params.endDateError(newValue);
    });

    self.isDateErrorVisible = ko.computed(function () {
        return (self.endDateOption() === 'endDate') && self.endDateInput.hasFormatError()
    });
}

module.exports = { viewModel: PayrollDirectiveSwitchViewModel, template: markup };