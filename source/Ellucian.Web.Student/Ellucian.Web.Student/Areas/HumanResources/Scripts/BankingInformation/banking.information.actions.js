﻿var resources = require("Site/resource.manager");

var webRequests = {
    getBankingInformationViewModelAsync: function (payeeId, addressId) {
        //return Promise.resolve(testData);
        return GetBankingInformationViewModelAsync(payeeId, addressId);
    },
    updatePayrollDepositDirectivesAsync: function (payrollDepositDirectives, securityToken) {
        if(!payrollDepositDirectives)
            throw new Error('payrollDepositDirective required to update payrollDepositDirectives');
        if (!securityToken) {
            throw new Error("security token required to update payrollDepositDirectives")
        }
        // additional validation

        var data = JSON.stringify(payrollDepositDirectives);
        var token = JSON.stringify(securityToken);
        return UpdatePayrollDepositDirectivesAsync(data, token);
    },
    updatePayableDepositDirectiveAsync: function(payableDepositDirective) { 
        if(!payableDepositDirective)
            throw new Error('payableDepositDirective required to update payableDepositDirective');
        // additional validation

        var data = JSON.stringify(payableDepositDirective);

        return UpdatePayableDepositDirectiveAsync(data);
    },    
    createPayrollDepositDirectiveAsync: function (payrollDepositDirective) { 
        if(!payrollDepositDirective)
            throw new Error('payrollDepositDirective required to create payrollDepositDirective');
        // additional validation

        var data = JSON.stringify(payrollDepositDirective);

        return CreatePayrollDepositDirectiveAsync(data);
    },
    createPayableDepositDirectiveAsync: function (payableDepositDirective) {
        if(!payableDepositDirective)
            throw new Error('payableDepositDirective required to create payableDepositDirective');
        // additional validation

        var data = JSON.stringify(payableDepositDirective);

        return CreatePayableDepositDirectiveAsync(data);
    },
    deletePayrollDepositDirectiveAsync: function (payrollDepositDirective) {
        if (!payrollDepositDirective)
            throw new Error('Id of payrollDepositDirective required to delete');
        // additional validation
        var data = JSON.stringify(payrollDepositDirective);
        return DeletePayrollDepositDirectiveAsync(data);
    },
    deletePayableDepositDirectiveAsync: function (payableDepositDirective) {
        if (!payableDepositDirective)
            throw new Error('Id of payableDepositDirective required to delete');
        // additional validation
        var data = JSON.stringify(payableDepositDirective);
        return DeletePayableDepositDirectiveAsync(data);
    },
    getBankAsync: function(identifier){        
        if(!identifier)
            throw new Error('Identifier of bank required to find bank');
        // additional validation
        return GetBankAsync(identifier);
    },
    getPayrollAuthenticationAsync: function (directiveId, accountId) {
        return GetPayrollAuthenticationAsync(directiveId, accountId);
    },
    getPayableAuthenticationAsync: function (directiveId, accountId, addressId) {
        return GetPayableAuthenticationAsync(directiveId, accountId, addressId);
    }
};

function handleResponse(responseObj) {
    if (responseObj.StatusCode === 200) {
        return responseObj.Model;
    }
    else {
        throw responseObj;
    }
}

var loadErrorMessage = resources.getObservable('BankingInformation.LoadBankingInformationErrorMessage');
function GetBankingInformationViewModelAsync(payeeId, addressId) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.getBankingInformationActionUrl,
        type: 'GET',
        data: { payeeId: payeeId, addressId: addressId },
        contentType: jsonContentType,
        dataType: "json"
    })).then(handleResponse);
};


var updatePayrollErrorMessage = resources.getObservable('BankingInformation.UpdateBankingInformationErrorMessage')
function UpdatePayrollDepositDirectivesAsync(directive, token) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.updatePayrollDepositActionUrl,
        type: 'PUT',
        data: JSON.stringify({ payrollDepositData: directive, bankingAuthenticationToken: token }),
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

var updatePayableErrorMessage = resources.getObservable('BankingInformation.UpdateBankingInformationErrorMessage');
function UpdatePayableDepositDirectiveAsync(directive) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.editPayableDepositActionUrl,
        type: 'PUT',
        data: JSON.stringify({ payableDepositData: directive }),
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};
    
var createPayrollErrorMessage = resources.getObservable('BankingInformation.CreateDepositError');
function CreatePayrollDepositDirectiveAsync(directive) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.createPayrollDepositActionUrl,
        type: 'POST',
        data: JSON.stringify({ payrollDepositData: directive }),
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

var createPayableErrorMessage = resources.getObservable('BankingInformation.CreateDepositError');
function CreatePayableDepositDirectiveAsync(directive) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.createPayableDepositActionUrl,
        type: 'POST',
        data: JSON.stringify({ payableDepositData: directive }),
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

var deletePayrollErrorMessage = resources.getObservable('BankingInformation.DeletingDepositError');
function DeletePayrollDepositDirectiveAsync(directive) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.deletePayrollDepositActionUrl,
        type: 'DELETE',
        data: JSON.stringify({ payrollDepositData: directive }),
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

var deletePayableErrorMessage = resources.getObservable('BankingInformation.DeletingDepositError');
function DeletePayableDepositDirectiveAsync(directive) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.deletePayableDepositActionUrl,
        type: 'DELETE',
        data: JSON.stringify({ payableDepositData: directive }),
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

var getBankErrorMessage = resources.getObservable('BankingInformation.ErrorBankDoesNotExistMessage');
function GetBankAsync(identifier) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.getBankAsyncActionUrl,
        type: 'GET',
        data: { id: identifier },
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

var payrollAuthenticationError = resources.getObservable('BankingInformation.AuthenticationErrorMessage');
function GetPayrollAuthenticationAsync(directiveId, accountId) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.authenticatePayrollDepositActionUrl,
        type: 'GET',
        data: { directiveId: directiveId, accountId: accountId },
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

var payableAuthenticationError = resources.getObservable('BankingInformation.AuthenticationErrorMessage');
function GetPayableAuthenticationAsync(directiveId, accountId, addressId) {
    return Promise.resolve($.ajax({
        url: window.Ellucian.BankingInformation.ActionUrls.authenticatePayableDepositActionUrl,
        type: 'GET',
        data: { directiveId: directiveId, accountId: accountId, addressId: addressId },
        contentType: jsonContentType,
        dataType: 'json'
    })).then(handleResponse);
};

module.exports = webRequests;








