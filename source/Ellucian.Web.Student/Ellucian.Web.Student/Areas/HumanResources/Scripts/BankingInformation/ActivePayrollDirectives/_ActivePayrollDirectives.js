﻿var markup = require("./_ActivePayrollDirectives.html"),
    resources = require("Site/resource.manager");

/*
 * params: {
 *  dataModel: the BankingInformationDataModel - c# server model,
 *  hasAllCanadianAccounts - computed that determines if all accounts are canadian,
 * }
 */
function ActivePayrollDirectivesViewModel(params) {
    var self = this;

    self.dataModel = params.dataModel;

    self.type = params.type;

    self.ActivePayrollDeposits = self.dataModel.ActivePayrollDeposits;

    self.hasAllCanadianAccounts = params.hasAllCanadianAccounts;

    self.viewAllPayrollHref = ko.pureComputed(function () {


        return "#viewAllPayroll"
    })

    self.hasActivePayrollDeposits = ko.computed(function () {
        return (self.ActivePayrollDeposits() && self.ActivePayrollDeposits().length > 0);
    });

    self.noActivePayrollDepositsMessage = ko.computed(function () {
        return resources.getObservable('BankingInformation.NoActivePayrollDepositsMessage')() + " " +
            resources.getObservable('BankingInformation.NoActivePayrollNoRemainderMessage')();
    });


    self.editPayrollDepositDirective = function () {
        console.info("implement edit payroll deposit directive");
    }

    self.activePayrollRemainderDeposit = ko.computed(function () {
        return ko.utils.arrayFirst(self.ActivePayrollDeposits(), function (onePayrollDeposit) {            
            return onePayrollDeposit.DepositOrder() === 999;
        });
    })

    self.hasActivePayrollRemainderDeposit = ko.computed(function () {
        return self.activePayrollRemainderDeposit() != null;
    });

    self.noPayrollRemainderDepositMessage = resources.getObservable('BankingInformation.NoPayrollRemainderDepositMessage');

    //self.headerLabel = ko.computed(function () {
    //    var summaryHeader = resources.getObservable("BankingInformation.PayrollDepositsHeader"),
    //        activeHeader = resoureces.getObservable("BankingInformation.ActiveAccountsColumnHeader"),
    //        futureHeader = resoureces.getObservable("BankingInformation.FutureAccountsColumnHeader"),
    //        pastHeader = resources.getObservable("BankingInformation.PastAccountsColumnHeader");

    //    switch (self.type) {
    //        case "summary":
    //            return summaryHeader();
    //        case "active":
    //            return activeHeader();
    //        case "future":
    //            return futureHeader();
    //        case "past":
    //            return pastHeader();
    //    }

    //});
}

module.exports = { viewModel: ActivePayrollDirectivesViewModel, template: markup };