﻿var markup = require("./_ActiveAccounts.html"),
    resources = require('Site/resource.manager');

/*
 * params: {
 *  dataModel: a BankingInformationViewModel 
 * }
 */
function ActiveAccountsViewModel(params) {
    var self = this;

    self.dataModel = params.dataModel;

    self.selectedAddressId = ko.observable(ko.utils.unwrapObservable(params.dataModel.AddressId));
    self.loadVendorDeposits = function () {
        if (self.selectedAddressId()) {
            window.location.hash = "#addressId={0}".format(self.selectedAddressId());
        }
        else {
            window.location.hash = "#";
        }
    }

    self.addAccount = function () {
        var addressHash = "";
        if (self.selectedAddressId()) {
            addressHash = "&addressId={0}".format(self.selectedAddressId());
        }
        window.location.hash = "#new" + addressHash;
    }

    

    self.IsPayrollActive = params.dataModel.IsPayrollActive;
    self.IsPayableActive = params.dataModel.IsPayableActive;
    self.IsVendor = params.dataModel.IsVendor;
    self.Configuration = params.dataModel.Configuration;
    self.Addresses = params.dataModel.Addresses;

    self.absentTermsAndConditionsMessage = resources.getObservable('BankingInformation.GetConfigurationErrorMessage');

    var allDepositDirectives = ko.computed(function () {
        var unwrappedActiveRefundDeposit = ko.unwrap(params.dataModel.ActiveRefundDeposit);
        return params.dataModel.ActivePayrollDeposits()
            .concat(params.dataModel.PastPayrollDeposits())
            .concat(params.dataModel.FuturePayrollDeposits())
            .concat(unwrappedActiveRefundDeposit ? [unwrappedActiveRefundDeposit] : [])
            .concat(params.dataModel.FutureRefundDeposits())
            .concat(params.dataModel.PastRefundDeposits());
    });

    self.hasAllCanadianAccounts = ko.computed(function () {
        var hasUsDirective = allDepositDirectives().some(function (directive) {
            return directive.RoutingId() ? true : false;
        });
        return !hasUsDirective;
    });

}

module.exports = { viewModel: ActiveAccountsViewModel, template: markup };