﻿var markup = require("./_EditPayrollDirective.html"),
    resources = require('Site/resource.manager'),
    actions = require("BankingInformation/banking.information.actions");


/*
 * params: {
 *  dataModel: bankingInformationViewModel - c# server model,
 *  id: id of the payroll directive to edit,
 *  isAdd: whether this is an add workflow
 * }
 */
function EditPayrollDirectiveViewModel(params) {
    var self = this;

    params.isAdd = false;

    var allPayrollDirectives = ko.computed(function () {
        return params.dataModel.ActivePayrollDeposits()
            .concat(params.dataModel.FuturePayrollDeposits())
            .concat(params.dataModel.PastPayrollDeposits())
    });

    self.numberActiveNonRemainderDirectives = ko.computed(function () {
        var nonRemainderDirectives = params.dataModel.ActivePayrollDeposits().filter(function (directive) {
            return directive.DepositOrder() !== 999;
        });
        return nonRemainderDirectives.length;
    })

    var currentActiveRemainderDirective = ko.computed(function () {
        var activeRemainder = params.dataModel.ActivePayrollDeposits().find(function (directive) {
            return directive.DepositOrder() === 999;
        });
        return activeRemainder;
    });

    self.futurePayrollRemainder = ko.computed(function () {
        var futureRemainder = params.dataModel.FuturePayrollDeposits().find(function (directive) {
            return directive.DepositOrder() === 999;
        });
        return futureRemainder;
    });

    self.hasAllCanadianAccounts = ko.computed(function () {
        var hasUsDirective = allPayrollDirectives().some(function (directive) {
            return directive.RoutingId() ? true : false;
        });
        return !hasUsDirective;
    });

    var editingDirective = ko.computed(function () {
        var edit = allPayrollDirectives().find(function (directive) {
            return directive.Id() === params.id;
        });
        return edit;
    });

    self.invalidId = ko.computed(function () {
        return (!editingDirective() || (editingDirective().EndDate() && editingDirective().EndDate() < Date.Today()));
    });

    function editPayrollDirectiveViewModel(directive) {
        var viewModel = this;


        viewModel.isAdd = params.isAdd;
        viewModel.IsVendor = params.dataModel.IsVendor;
        viewModel.Id = directive.Id;
        viewModel.RoutingId = directive.RoutingId;
        viewModel.AccountIdLastFour = directive.AccountIdLastFour;
        viewModel.IsVerified = directive.IsVerified;
        viewModel.InstitutionId = directive.InstitutionId;
        viewModel.DepositAmount = directive.DepositAmount;
        viewModel.DepositOrder = directive.DepositOrder;
        viewModel.OriginalDepositOrder = directive.OriginalDepositOrder;
        viewModel.StartDate = directive.StartDate;
        viewModel.EndDate = directive.EndDate;
        viewModel.SecurityToken = directive.SecurityToken;
        viewModel.Configuration = params.dataModel.Configuration;
        viewModel.ActivePayrollDeposits = params.dataModel.ActivePayrollDeposits;




        viewModel.displayBankName = ko.computed(function () {
            if (directive.Nickname()) {
                return directive.Nickname();
            }
            return directive.BankName();
        });

        viewModel.displayBankAccountLastFour = ko.computed(function () {
            return "..." + directive.AccountIdLastFour();
        });

        viewModel.isActive = ko.pureComputed(function () {
            var todayDateOnly = Date.Today();
            return viewModel.StartDate().getTime() <= todayDateOnly.getTime() && (!viewModel.EndDate() || viewModel.EndDate().getTime() >= todayDateOnly.getTime());
        });
        viewModel.isFuture = ko.pureComputed(function () {
            return Date.Compare(viewModel.StartDate(), Date.Today()) == 1;
        });

        viewModel.isSwitchedOn = ko.observable(viewModel.isActive() || viewModel.isFuture());
        viewModel.isSwitchedOnSubscription = viewModel.isSwitchedOn.subscribe(function (value) {
            if (value === true && viewModel.RoutingId()) {
                console.log("resolve the bank name here? not sure why");
            }
        });

        viewModel.isSwitchVisible = ko.pureComputed(function () {

            return viewModel.deleteButtonEnabled() || !viewModel.isPayrollRemainderRequired() || !viewModel.isRemainderDeposit() || params.isAdd;

        });

        viewModel.deleteButtonEnabled = ko.pureComputed(function () {
            return !params.isAdd && viewModel.isFuture();
        });

        viewModel.isPayrollRemainderRequired = ko.pureComputed(function () {
            return viewModel.Configuration.IsRemainderAccountRequired();
        });

        viewModel.depositAmountOption = ko.observable(null);
        viewModel.isRemainderDeposit = ko.computed(function () {
            return (viewModel.DepositOrder() === 999 || viewModel.depositAmountOption() == 'isRemainder' || viewModel.depositAmountOption() == 'isBalance');
        });

        //initialize the deposit amount option
        if (viewModel.isRemainderDeposit() && params.dataModel.ActivePayrollDeposits().length === 1) {
            viewModel.depositAmountOption("isBalance");
        } else if (viewModel.isRemainderDeposit()) {
            viewModel.depositAmountOption("isRemainder");
        } else {
            viewModel.depositAmountOption("isSpecific");
        }


        viewModel.startDateBacker = ko.observable(viewModel.StartDate() ? viewModel.StartDate() : Date.Today());


        viewModel.startDateIsReadOnly = ko.pureComputed(function () {
            return !params.isAdd && Date.Compare(viewModel.startDateBacker(), Date.Today()) <= 0;
        });
        viewModel.startDateError = ko.observable(null);
        viewModel.startDateNeedsInput = ko.pureComputed(function () {
            return !viewModel.startDateIsReadOnly() && (!viewModel.startDateBacker() || viewModel.startDateBacker() === "");
        });

        viewModel.endDateOption = ko.observable(viewModel.EndDate() ? "endDate" : "noEndDate");

        viewModel.endDateBacker = ko.observable(viewModel.EndDate());

        viewModel.isEndDateVisible = ko.computed(function () {
            return !viewModel.isPayrollRemainderRequired() || !viewModel.isRemainderDeposit();
        })

        viewModel.endDateOptionUpdate = ko.computed(function () {
            if (viewModel.endDateOption() === 'noEndDate')
                viewModel.endDateBacker(null);
        });

        viewModel.endDateError = ko.observable(null);

        viewModel.isDateErrorVisible = ko.computed(function () {
            return (viewModel.endDateOption() === 'endDate') && viewModel.endDateError();
        });

        viewModel.cannotDeactivatePayrollRemainderDepositMessage = ko.computed(function () {
            if (viewModel.isRemainderDeposit() && viewModel.isPayrollRemainderRequired() && viewModel.isActive()) {
                return resources.getObservable('BankingInformation.CannotDeactivePayrollRemainderDepositMessage')();
            }
            return null;
        });


        viewModel.isFutureAndCurrentRemainderExists = ko.computed(function () {
            var exists = viewModel.isPayrollRemainderRequired() &&
                viewModel.OriginalDepositOrder() === 999 &&
                viewModel.isFuture() &&
                currentActiveRemainderDirective();
            return exists;
        });


        viewModel.willChangeEndDateOfCurrentRemainderMessage = ko.computed(function () {
            if (viewModel.isFutureAndCurrentRemainderExists()) {
                return resources.getObservable('BankingInformation.WillChangeEndDateOfCurrentRemainderMessage')();
            }
            return null;
        });


        viewModel.specificAmountIsVisible = ko.computed(function () {
            var isVisible = !viewModel.isPayrollRemainderRequired() ||
                viewModel.OriginalDepositOrder() !== 999 ||
                (currentActiveRemainderDirective() && viewModel.OriginalDepositOrder() !== 999);
            return isVisible;
        });

        viewModel.depositAmountBacker = ko.observable(viewModel.DepositAmount());

        viewModel.specificDepositAmountError = ko.observable(null);

        viewModel.depositAmountNeedsInput = ko.computed(function () {
            return viewModel.depositAmountOption() === 'isSpecific' && (!viewModel.depositAmountBacker() || viewModel.depositAmountBacker() === '');

        });



        viewModel.isRemainingBalanceOptionVisible = ko.computed(function () {
            return params.dataModel.ActivePayrollDeposits().length > 1 && viewModel.OriginalDepositOrder !== 999;
        });


        viewModel.accountDetailsModel = {
            accountIdLast4: ko.observable(directive.AccountIdLastFour()).extend({ notify: 'always' }),
            nickname: ko.observable(directive.Nickname()),
            bankName: ko.observable(directive.BankName()),
            routingId: ko.observable(directive.RoutingId()),
            institutionId: ko.observable(directive.InstitutionId()),
            branchNumber: ko.observable(directive.BranchNumber()),
            newAccountId: ko.observable(null),
            accountType: ko.observable(directive.Type()),
            isVerified: ko.observable(directive.IsVerified())
        }
        function resetAccountDetails() {
            viewModel.accountDetailsModel
                .accountIdLast4(directive.AccountIdLastFour())
                .nickname(directive.Nickname())
                .bankName(directive.BankName())
                .routingId(directive.RoutingId())
                .institutionId(directive.InstitutionId())
                .branchNumber(directive.BranchNumber())
                .newAccountId(null)
                .accountType(directive.Type())
                .isVerified(directive.IsVerified())
        }


        viewModel.isBankAccountDetailsInputValid = ko.observable(true);

        viewModel.showEditBankAccountDetails = ko.observable(false);
        viewModel.editDirectiveBankAccount = function () {
            viewModel.showEditBankAccountDetails(true);
        }
        viewModel.cancelEditBankAccountDetails = function () {
            viewModel.showEditBankAccountDetails(false);
            resetAccountDetails()
        }
        viewModel.isSavingBankAccountDetailsInProgress = ko.observable(false);
        viewModel.saveBankAccountDetails = function () {
            if (!viewModel.isBankAccountDetailsInputValid()) {
                return Promise.resolve();
            }
            viewModel.showEditBankAccountDetails(false);
            viewModel.isSavingBankAccountDetailsInProgress(true);


            var model = viewModel.accountDetailsModel;
            directive.AccountIdLastFour(model.accountIdLast4());
            directive.Nickname(model.nickname());
            directive.BankName(model.bankName());
            directive.RoutingId(model.routingId());
            directive.InstitutionId(model.institutionId());
            directive.BranchNumber(model.branchNumber());
            directive.NewAccountId(model.newAccountId());
            directive.Type(model.accountType());

            var unmappedObject = ko.mapping.toJS(directive);

            return actions.updatePayrollDepositDirectivesAsync([unmappedObject], unmappedObject.SecurityToken).then(function () {
                viewModel.isSavingBankAccountDetailsInProgress(false);
                return;
            }).catch(function (err) {
                console.error(err);
                $('#notificationHost').notificationCenter('addNotification', { message: updatePayrollErrorMessage(), type: 'error', flash: true });

                resetAccountDetails();
                viewModel.isSavingBankAccountDetailsInProgress(false);
                return;
            });
        }
        var updatePayrollErrorMessage = resources.getObservable('BankingInformation.UpdateBankingInformationErrorMessage');


        viewModel.hasInputErrors = ko.computed(function () {
            return viewModel.endDateError() ||
                   (viewModel.startDateError() && (viewModel.isAdd || viewModel.isFuture())) ||
                   viewModel.specificDepositAmountError() ||
                   viewModel.depositAmountNeedsInput() ||
                   (!viewModel.startDateBacker() || viewModel.startDateBacker() === '');
        });



        viewModel.saveEnabled = ko.computed(function () {
            //if (viewModel.startDateIsReadOnly()) return true;
            return !viewModel.hasInputErrors();
        });

        var depositsToDelete = ko.observableArray([]);
        var deletedDepositIds = ko.computed(function () {
            return depositsToDelete().map(function (dep) {
                return dep.Id();
            });
        });

        viewModel.isSaveDirectiveInProgress = ko.observable(false);
        viewModel.saveAsync = function () {
            viewModel.isSaveDirectiveInProgress(true);

            viewModel.updateDataModel();

            var directivesToUpdate = params.dataModel.ActivePayrollDeposits()
                .concat(params.dataModel.FuturePayrollDeposits())
                .filter(function (dep) { //filter out ones that have been deleted
                    return deletedDepositIds().indexOf(dep.Id()) < 0; //dep.Id not found in deleted deposits
                });
            if (directivesToUpdate.indexOf(directive) < 0) {
                directivesToUpdate.push(directive);
            }
            var unmappedDirectives = ko.mapping.toJS(directivesToUpdate);
            var unmappedSecurityToken = ko.mapping.toJS(directive.SecurityToken);

            var deleteTasks = depositsToDelete().map(function (dep) {
                return actions.deletePayrollDepositDirectiveAsync(ko.mapping.toJS(dep));
            });

            return Promise.all(deleteTasks).then(function () {//delete first
                return actions.updatePayrollDepositDirectivesAsync(unmappedDirectives, unmappedSecurityToken); //then update
            }).catch(function (err) {
                console.error(err);
                $('#notificationHost').notificationCenter('addNotification', { message: updatePayrollErrorMessage(), type: 'error', flash: true });

            }).then(function () {
                viewModel.isSaveDirectiveInProgress(false);
                viewModel.back();
                return;
            });
        }

        viewModel.updateDataModel = function () {

            // start date updates
            directive.StartDate(viewModel.startDateBacker());

            // end date updates
            if (!viewModel.isSwitchedOn()) {
                var newEndDate = (directive.StartDate().getTime() === Date.Today().getTime()) ? Date.Today() : Date.TodayDateOffset(-1);
                directive.EndDate(newEndDate);

                //if the deposit in context is a future remainder deposit and an active remainder deposit exists
                //set the end date of the active remainder deposit to null
                if (viewModel.isFutureAndCurrentRemainderExists()) {
                    currentActiveRemainderDirective.EndDate(null);
                }
            }
            else {
                directive.EndDate(viewModel.endDateBacker());
            }

            // balance updates 

            var startDateUpdate = directive.StartDate()
            //var startDateUpdate = new Date((Date.Compare(directive.StartDate(), Date.Today()) == 1) ? directive.StartDate().valueOf() : Date.Today().valueOf());
            var otherRemainders = ko.utils.arrayFilter(params.dataModel.ActivePayrollDeposits().concat(params.dataModel.FuturePayrollDeposits()), function (dpst) {
                return dpst.DepositOrder() === 999 && dpst.Id() !== directive.Id();
            });

            //only do depositAmountOption saves if the deposit is switched on.
            if (viewModel.isSwitchedOn()) {
                if (viewModel.depositAmountOption() === 'isBalance') {


                    // end all accounts besides the new one past the new account start date except any remainder
                    params.dataModel.ActivePayrollDeposits().concat(params.dataModel.FuturePayrollDeposits()).forEach(function (dpst) {
                        var thisStartDateUpdate = new Date(startDateUpdate); // need to create a new date for each iteration or else changes will accumulate
                        // filter out this payroll deposit and the remainders from the comparisons
                        if (dpst.Id() !== directive.Id() && dpst.DepositOrder() !== 999) {

                            if ((!directive.EndDate() || Date.Compare(directive.EndDate(), dpst.StartDate()) >= 0) &&
                                (!dpst.EndDate() || Date.Compare(dpst.EndDate(), startDateUpdate) >= 0)) {
                                dpst.EndDate(thisStartDateUpdate.addDays(-1));

                                if (Date.Compare(dpst.StartDate(), dpst.EndDate()) > 0) {
                                    dpst.SecurityToken(viewModel.SecurityToken());
                                    depositsToDelete.push(dpst);
                                }
                            }

                        }
                    });
                    // now handle the remainder
                    if (otherRemainders) {
                        ko.utils.arrayForEach(otherRemainders, function (remainder) {
                            updateRemainderDates(remainder, startDateUpdate)
                        }); // extracted this to function because we need to do for 'isBalance' and 'isRemainder'
                    }
                    else if (viewModel.isPayrollRemainderRequired()) {
                        directive.EndDate(null);
                    }

                    // ensure the deposit holds appropriate remainder properties   
                    directive.DepositAmount(null);
                    directive.DepositOrder(999);
                }
                else if (viewModel.depositAmountOption() === 'isRemainder') {

                    directive.DepositAmount(null);
                    directive.DepositOrder(999);

                    if (otherRemainders) {
                        ko.utils.arrayForEach(otherRemainders, function (remainder) {
                            updateRemainderDates(remainder, startDateUpdate);
                        });
                    }
                    else if (viewModel.isPayrollRemainderRequired()) {
                        directive.EndDate(null);
                    }
                    // ensure the deposit holds appropriate remainder properties  
                    
                }
                else { // here there is a deposit amount change
                    directive.DepositAmount(viewModel.depositAmountBacker());
                    // now we check if this was a future remainder deposit that is ending; if so, 
                    // and if remainder accounts are required, we extend any current remainder accounts end date to this
                    if (viewModel.isPayrollRemainderRequired() && directive.OriginalDepositOrder() === 999 && viewModel.isFuture()) {
                        //var currentRemainder = ko.utils.arrayFirst(root.ActivePayrollDeposits(), function (dpst) { return dpst.DepositOrder() === 999 });
                        if (currentActiveRemainderDirective()) {
                            currentActiveRemainderDirective().EndDate(null);//new Date.DateOffset(self.payrollDeposit().StartDate(), -1));
                        }
                    }
                }
            }
        }

        // handles start and end date saves between new and existing remainder accounts
        // startDateUpdate is the startDate of the remainder in context
        var updateRemainderDates = function (otherRemainder, startDateUpdate) {

            // if remainder being edited starts after the other remainder        
            //startDateUpdate > otherRemainder.StartDate
            if (Date.Compare(startDateUpdate, otherRemainder.StartDate()) == 1) {


                if (viewModel.isPayrollRemainderRequired() || //payroll remainder is required
                    !otherRemainder.EndDate() || // other remainder has no endate
                    Date.Compare(startDateUpdate, otherRemainder.EndDate()) < 1) {

                    //we need to ensure the remainders do not overlap, so set the other Remainder's enddate to the day before this remainder's start date
                    otherRemainder.EndDate((new Date(startDateUpdate)).addDays(-1));
                } //else, remainder accounts aren't required and the end date is already before the new start date
            }

            else {

                //the remainder being edited starts on or before the other remainder

                if (!directive.EndDate()) {
                    otherRemainder.SecurityToken(viewModel.SecurityToken());
                    depositsToDelete.push(otherRemainder);
                }
                else if (viewModel.isPayrollRemainderRequired() || //remainder is required, which means dates must be contiguous, or
                    Date.Compare(directive.EndDate(), otherRemainder.StartDate()) == 1) {// the end date the user entered is after the otherRemainder's Start Date

                    otherRemainder.StartDate((new Date(directive.EndDate())).addDays(1));

                    if (otherRemainder.EndDate() && Date.Compare(otherRemainder.StartDate(), otherRemainder.EndDate()) > 0) {
                        otherRemainder.SecurityToken(self.securityToken());
                        depositsToDelete.push(otherRemainder);
                    }
                }
                
            }
        }

        viewModel.deleteButtonEnabled = ko.computed(function () {
            return Date.Compare(directive.StartDate(), Date.Today()) == 1;
        });

        viewModel.isDeleteInProgress = ko.observable(false);
        viewModel.deleteAsync = function () {
            viewModel.isDeleteInProgress(true);

            var unmappedObject = ko.mapping.toJS(directive);
            return actions.deletePayrollDepositDirectiveAsync(unmappedObject).then(function () {
                viewModel.isDeleteInProgress(false);
                return;
            }).catch(function (err) {
                console.error(err);
                $('#notificationHost').notificationCenter('addNotification', { message: deletePayrollErrorMessage(), type: 'error', flash: true });

                viewModel.isDeleteInProgress(false);
                return;
            }).then(function () {
                viewModel.back();
                return;
            });
        }
        var deletePayrollErrorMessage = resources.getObservable('BankingInformation.DeletingDepositError');

        viewModel.back = function () {
            window.history.back();
        }


        viewModel.actionInProgressMessage = ko.computed(function () {
            if (viewModel.isDeleteInProgress()) {
                return resources.getObservable("BankingInformation.DeletingDepositLoadingMessage")();
            }
            else if (viewModel.isSaveDirectiveInProgress()) {
                return resources.getObservable("BankingInformation.EditDepositDetailsModalSavingLabel")();
            }
            else if (viewModel.isSavingBankAccountDetailsInProgress()) {
                return resources.getObservable("BankingInformation.EditAccountDetailsModalSavingLabel")();
            }
            return null;
        })
        viewModel.actionInProgress = ko.computed(function () {
            return viewModel.isDeleteInProgress() ||
                viewModel.isSaveDirectiveInProgress() ||
                viewModel.isSavingBankAccountDetailsInProgress();
        });
    }

    self.editingDirectiveViewModel = null;

    if (!self.invalidId()) {
        self.editingDirectiveViewModel = new editPayrollDirectiveViewModel(editingDirective())
    } 
}

module.exports = { viewModel: EditPayrollDirectiveViewModel, template: markup };