﻿var bankingInformationViewModel = require('./banking.information.application.view.model');

var viewModelInstance = new bankingInformationViewModel();

__webpack_public_path__ = window.Ellucian.appUrl;

function hashChangeHandler() {

    var editRegex = /#(payroll|payable)=([^\n&]*)/i;
    var createRegex = /#new/i
    var viewAllPayableRegex = /#viewAllPayable/i;

    var addressRegex = /addressId=([^\n&]*)/i;
    var addressId = null;
    if (addressRegex.test(window.location.hash)) {
        addressId = addressRegex.exec(window.location.hash)[1]; //extract out the addressId
    } 


    if (window.location.hash === "#viewAllPayroll") { //view all payroll
        viewModelInstance.currentViewComponentName("summary-view");
        viewModelInstance.currentViewComponentParams({
            type: "payroll",
            addressId: null
        });
    }
    else if (viewAllPayableRegex.test(window.location.hash)) { //view all payable
        var result = viewAllPayableRegex.exec(window.location.hash);

        viewModelInstance.currentViewComponentName("summary-view");
        viewModelInstance.currentViewComponentParams({
            type: "payable",
            addressId: addressId
        });
    }
    else if (createRegex.test(window.location.hash)) { //create new
        var result = createRegex.exec(window.location.hash);

        viewModelInstance.currentViewComponentName("edit-view");
        viewModelInstance.currentViewComponentParams({
            type: null,
            id: null,
            addressId: addressId
        })
    }
    else if (editRegex.test(window.location.hash)) { //edit payroll or payable
        var result = editRegex.exec(window.location.hash);

        viewModelInstance.currentViewComponentName("edit-view");
        viewModelInstance.currentViewComponentParams({
            type: result[1],
            id: result[2],
            addressId: addressId,
        });
    }
    else {

        viewModelInstance.currentViewComponentName("summary-view");
        viewModelInstance.currentViewComponentParams({
            type: null,
            addressId: addressId
        });
    }
}

$(window).on('hashchange', hashChangeHandler);

function loadApplication() {
    ko.applyBindings(viewModelInstance, document.getElementById('banking-information-application'));

    hashChangeHandler();

    viewModelInstance.checkForMobile(window, document);
}

$(document).ready(function () {
    
    ko.components.register('summary-view', require('BankingInformation/SummaryViewComponent/_SummaryView'));
    ko.components.register('active-accounts-collection', require("BankingInformation/ActiveAccountsCollection/_ActiveAccounts"));
    ko.components.register('active-payroll-directives', require("BankingInformation/ActivePayrollDirectives/_ActivePayrollDirectives"));
    ko.components.register('active-payable-directive', require("BankingInformation/ActivePayableDirective/_ActivePayableDirective"));
    ko.components.register('payroll-directive-summary', require("BankingInformation/PayrollDirectiveSummary/_PayrollDirectiveSummary"));
    ko.components.register('payable-directive-summary', require("BankingInformation/PayableDirectiveSummary/_PayableDirectiveSummary"));
    ko.components.register("all-payroll-directives-collection", require("BankingInformation/AllPayrollDirectivesCollection/_AllPayrollDirectivesCollection"));
    ko.components.register("all-payable-directives-collection", require("BankingInformation/AllPayableDirectivesCollection/_AllPayableDirectivesCollection"));

    ko.components.register("edit-view", require("BankingInformation/EditViewComponent/_EditViewComponent"));
    ko.components.register('step-up-authentication', require("BankingInformation/StepUpAuthentication/_StepUpAuthentication"));
    ko.components.register('edit-payable-directive', require("BankingInformation/EditPayableDirective/_EditPayableDirective"));
    ko.components.register('payable-directive-switch', require("BankingInformation/PayableDirectiveSwitch/_PayableDirectiveSwitch"));
    ko.components.register('edit-payroll-directive', require("BankingInformation/EditPayrollDirective/_EditPayrollDirective"));
    ko.components.register('payroll-directive-switch', require("BankingInformation/PayrollDirectiveSwitch/_PayrollDirectiveSwitch"));
    ko.components.register('payroll-directive-amount-option', require("BankingInformation/PayrollDirectiveAmountOption/_PayrollDirectiveAmountOption"));
    ko.components.register('sortable-payroll-directives', require("BankingInformation/SortablePayrollDirectives/_SortablePayrollDirectives"));
    ko.components.register("create-new-directive", require("BankingInformation/CreateNewDirective/_CreateNewDirective"));
    ko.components.register('edit-bank-account-details', require("BankingInformation/EditBankAccountDetails/_EditBankAccountDetails"));

    loadApplication();
});

if (__DEV__) {
    if (module.hot) {
        module.hot.accept();
    }
}
