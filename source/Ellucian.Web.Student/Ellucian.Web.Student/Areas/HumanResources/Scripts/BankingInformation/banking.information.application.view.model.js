﻿
function bankingInformationViewModel() {
    var self = this;

    BaseViewModel.call(self, 768);

    self.changeToMobile = function () {
        // This method should be overridden in any class that uses BaseViewModel
    };

    self.changeToDesktop = function () {
        // This method should be overridden in any class that uses BaseViewModel
    };



    self.currentViewComponentName = ko.observable();
    self.currentViewComponentParams = ko.observable();
}

module.exports = bankingInformationViewModel;