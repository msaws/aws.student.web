﻿/**
jquery UI Sortable binding

Summary:
    The sortable binding applies the jquery UI sortable widget to a list of data items. It was written for use with a foreach binding,
    bound to an observable array. When the user sorts the elements in the browser, this binding updates the underlying observable array
    with the new positions of the items, using a custom update function. When you change the observable array programatically, knockout
    updates the DOM like you would expect. However, the transitions are not animated.

Usage:
    jqSortableList accepts an object as it's valueAccessor. This object has one property
        options - An object containing jqueryUI Sortable widget options.

    jqSortableList should be used alongside a foreach binding bound to an observable array. If it is not boudn to an observable array, the sortable widget will
        not be applied.

Example:
    <script>
        ko.applyBindings({
            myItems: ko.observableArray(["item1", "item2", "item3"]),
            sortableOptions: { containment: "parent" }
        });
    </script>
    <ul data-bind="foreach: myItems, jqSortableList: { options: sortableOptions }">
        <li data-bind="text: $data"></li>
    </ul>
    
Also see the BankingInformation markup for a real-world example.

Potential enhancements:
  sortableList could be retrieved from foreach: sortableList, foreach: { data: sortableList } and <!-- ko foreach: sortableList -->
  add option for button selectors, add move up/down methods to this binding, bind click events on those buttons to the up/down methods - thinking
    about how BankingInformation has up and down buttons to provide for keyboard accessibility
*/
ko.bindingHandlers.jqSortableList = {
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var options = ko.utils.unwrapObservable(valueAccessor().options);
        var sortableList = allBindings.get('foreach');

        if (ko.isObservable(sortableList)) {
            var observableUpdate = function (event, ui) {

                //retrieve our actual data item
                var item = ko.dataFor(ui.item[0]);

                //figure out its new position
                var newPosition = ko.utils.arrayIndexOf(ui.item.parent().children(), ui.item[0]);

                //remove the item from the list and add it back in the right spot
                if (newPosition >= 0) {
                    sortableList.remove(item);
                    sortableList.splice(newPosition, 0, item);
                }
                ui.item.remove();
            }

            //handle disposal
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).sortable("destroy");
            });

            options.update = options.update || observableUpdate;
            $(element).sortable(options);
        }
    }
};