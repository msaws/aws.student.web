﻿var markup = require("./_StepUpAuthentication.html"),
    actions = require("BankingInformation/banking.information.actions"),
    resources = require("Site/resource.manager");

/*
 * params: {
 *  securityTokenObservable: an observable which this component will update when it receives successful authentication
 *  bankName: the name of the bank to display in the input label,
 *  accountIdLastFour: the last four digits of the account number of the directive we're authenticating
 *  directiveId: id of the directive we're authenticating,
 *  directiveType: payroll || payable,
 * }
 */
function StepUpAuthenticationViewModel(params) {
    var self = this;

    self.isVisible = ko.computed({
        read: function () {
            if (!params.securityTokenObservable()) {
                return true;
            }
            return false;
        },
        write: function (val) {
            //do nothing...this is just to prevent errors with the modal.
            //the user must cancel or verify the authentication to remove the modal.
        }
    });

    
    self.displayBankAccountLastFour = ko.computed(function () {
        return resources.getObservable('BankingInformation.AuthenticationAccountNumberLastFourLabel', [ko.utils.unwrapObservable(params.accountIdLastFour)])();
    });

    self.bankNameLabel = ko.utils.unwrapObservable(params.bankName);


    self.authenticationValue = ko.observable(null);

    self.cancelAuthentication = function () {
        params.securityTokenObservable(null);
        self.isAuthenticationInProgress(false);
        window.location.hash = "#"
    }

    self.authenticationFailed = ko.observable(false);
    self.isAuthenticationInProgress = ko.observable(false);
    self.authenticationInProgressMessage = resources.getObservable("BankingInformation.VerificationInProgressMessage");

    self.getAuthentication = function () {
        if (self.authenticationValue() && !self.isAuthenticationInProgress()) {
            self.isAuthenticationInProgress(true);
            self.authenticationValue(removeWhiteSpace(self.authenticationValue()));
            var action = ko.utils.unwrapObservable(params.directiveType) === "payroll" ? actions.getPayrollAuthenticationAsync : actions.getPayableAuthenticationAsync;

            return action(ko.utils.unwrapObservable(params.directiveId), self.authenticationValue()).then(function (tokenObj) {
                self.authenticationFailed(false);
                params.securityTokenObservable(tokenObj);
                $('#notificationHost').notificationCenter('addNotification', { message: authenticationSuccess(), type: 'success', flash: true });
                return;
            }).catch(function(err) {
                console.error(err);
                $('#notificationHost').notificationCenter('addNotification', { message: authenticationError(), type: 'error', flash: true });
                params.securityTokenObservable(null);
                self.authenticationValue(null);
                self.authenticationFailed(true);
                return;
            }).then(function () {
                self.isAuthenticationInProgress(false);
                return;
            });
        }
    }

    self.authenticationHasFocus = ko.observable(false);

    self.verifyMask = function (s, e) {
        if (window.getComputedStyle(e.target, null).getPropertyValue('font-family').indexOf('mask') === -1) {
            console.error('cannot accept input without masked font');
            return self.cancelAuthentication();
        }
        return;
    }

    var authenticationError = resources.getObservable('BankingInformation.AuthenticationErrorMessage');
    var authenticationSuccess = resources.getObservable("BankingInformation.AuthenticationSuccessMessage");

    self.authenticationModalDialogTitle = resources.getObservable('BankingInformation.AuthenticationModalDialogTitle');
    self.authenticationModalCancelButtonLabel = resources.getObservable('BankingInformation.AuthenticationModalCancelButtonLabel');
    self.authenticationModalPrimaryButtonLabel = resources.getObservable("BankingInformation.AuthenticationModalPrimaryButtonLabel");

    function removeWhiteSpace(value) {
        if (typeof value === 'string') {
            while (value.contains(' '))
                value = value.replace(' ', '');
        }
        return value;
    }

    document.addEventListener('keydown', submitOnEnter, false); // something is preventing the listener from being added to the element
    function submitOnEnter(e) {
        if(e.target.id === 'directive-authentication-input' && e.which === 13){
            self.getAuthentication();
        }
    }
    self.dispose = function () {
        document.removeEventListener('keydown', submitOnEnter, false);
    }
}

module.exports = { viewModel: StepUpAuthenticationViewModel, template: markup };