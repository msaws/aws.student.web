﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.HumanResources
{
    public static class HumanResourcesResourceFiles
    {
        public static readonly string BankingInformationResources = "BankingInformation";
        public static readonly string TaxFormsResources = "TaxForms";
    }
}
