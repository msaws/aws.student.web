﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;

namespace Ellucian.Web.Student.Areas.HumanResources.Models
{
    /// <summary>
    /// View Model for BankingInformation Page DepositAccounts
    /// </summary>
    public class PayrollDepositViewModel
    {
        public string Id;
        public string PersonId;
        public string RoutingId;
        public string InstitutionId;
        public string BranchNumber;
        public string BankName;
        public string NewAccountId;
        public string AccountIdLastFour;
        public string Nickname;
        public BankAccountType Type;
        public BankingAuthenticationToken SecurityToken;        
        public decimal? DepositAmount;
        public DateTime StartDate;
        public DateTime? EndDate;
        public int OriginalDepositOrder;
        public int DepositOrder;
        public DateTimeOffset AddDateTime;
        public DateTimeOffset ChangeDateTime;
        public string AddOperator;
        public string ChangeOperator;
        public bool IsVerified;

        /// <summary>
        /// Instantiate a DepositAccountViewModel for active accounts using a PayrollDepositDirective DTO
        /// </summary>
        /// <param name="payrollDepositAccount"></param>
        public PayrollDepositViewModel(Colleague.Dtos.HumanResources.PayrollDepositDirective payrollDepositDirective, int? depositOrder = null)
        {

            if (payrollDepositDirective == null)
            {
                throw new ArgumentNullException("payrollDepositDirective");
            }

            Id = payrollDepositDirective.Id;
            PersonId = payrollDepositDirective.PersonId;
            RoutingId = payrollDepositDirective.RoutingId;
            InstitutionId = payrollDepositDirective.InstitutionId;
            BranchNumber = payrollDepositDirective.BranchNumber;
            BankName = payrollDepositDirective.BankName;
            NewAccountId = payrollDepositDirective.NewAccountId;
            AccountIdLastFour = payrollDepositDirective.AccountIdLastFour;
            Nickname = payrollDepositDirective.Nickname;
            Type = payrollDepositDirective.BankAccountType;
            SecurityToken = null;            
            DepositAmount = payrollDepositDirective.DepositAmount;
            StartDate = payrollDepositDirective.StartDate;
            EndDate = payrollDepositDirective.EndDate;
            OriginalDepositOrder = payrollDepositDirective.Priority;
            DepositOrder = (payrollDepositDirective.Priority < 999) ? depositOrder ?? payrollDepositDirective.Priority : 999;
            AddDateTime = payrollDepositDirective.Timestamp.AddDateTime;
            ChangeDateTime = payrollDepositDirective.Timestamp.ChangeDateTime;
            AddOperator = payrollDepositDirective.Timestamp.AddOperator;
            ChangeOperator = payrollDepositDirective.Timestamp.ChangeOperator;
            IsVerified = payrollDepositDirective.IsVerified;
        }

        /// <summary>
        /// Instantiate an empty DepositAccountViewModel
        /// </summary>
        public PayrollDepositViewModel()
        {

        }

        public PayrollDepositDirective ConvertModelToPayrollDepositDirectivesDto()
        {
            return new PayrollDepositDirective()
                {
                    AccountIdLastFour = this.AccountIdLastFour,
                    BankAccountType = this.Type,
                    BankName = this.BankName,
                    BranchNumber = this.BranchNumber,
                    DepositAmount = this.DepositAmount,
                    EndDate = this.EndDate,
                    Id = this.Id,
                    InstitutionId = this.InstitutionId,
                    IsVerified = this.IsVerified,
                    NewAccountId = this.NewAccountId,
                    Nickname = this.Nickname,
                    PersonId = this.PersonId,
                    Priority = this.DepositOrder,
                    RoutingId = this.RoutingId,
                    StartDate = this.StartDate,
                    Timestamp = new Timestamp()
                    {
                        AddDateTime = this.AddDateTime,
                        AddOperator = this.AddOperator,
                        ChangeDateTime = this.ChangeDateTime,
                        ChangeOperator = this.ChangeOperator
                    }
                };
        }
    }
}