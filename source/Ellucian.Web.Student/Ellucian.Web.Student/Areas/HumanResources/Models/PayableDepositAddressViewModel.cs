﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.HumanResources.Models
{
    /// <summary>
    /// Stripped down address model for Payable Deposits
    /// </summary>
    public class PayableDepositAddressViewModel
    {
        /// <summary>
        /// Db Id of the Address. Empty string if this is the default address
        /// </summary>
        public string Id;

        /// <summary>
        /// Address Lines
        /// </summary>
        public List<string> AddressLines;

        /// <summary>
        /// Create an object based on the data in addressDto argurment
        /// </summary>
        /// <param name="addressDto"></param>
        public PayableDepositAddressViewModel(Address addressDto)
        {
            if (addressDto == null)
            {
                throw new ArgumentNullException("addressDto");
            }
  
            Id = string.IsNullOrWhiteSpace(addressDto.AddressId) ? null : addressDto.AddressId.Trim();
            AddressLines = addressDto.AddressLines != null ? addressDto.AddressLines : new List<string>();
        }    

        /// <summary>
        /// Create a default object. Sets the AddressId to an empty string
        /// </summary>
        public PayableDepositAddressViewModel()
        {
            Id = null;
            AddressLines = new List<string>() { "Default Account" };
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var addressViewModel = obj as PayableDepositAddressViewModel;

            return addressViewModel.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {       
            return string.Format("{0} - {1}", Id, string.Join(",", AddressLines));
        }
    }
}
