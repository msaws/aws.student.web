﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.HumanResources.Models
{
    /// <summary>
    /// Top Level View Model for the BankingInformation page
    /// </summary>
    public class BankingInformationViewModel
    {

        #region FIELDS

        /// <summary>
        /// The Id of the current user or designated payee
        /// </summary>
        public string PayeeId { get; set; }

        /// <summary>
        /// True if the current user is a vendor
        /// </summary>
        public bool IsVendor { get; set; }

        /// <summary>
        /// The address id of the refund deposits. For people, this is always null.
        /// For vendors, this could have value.
        /// </summary>
        public string AddressId { get; set; }

        /// <summary>
        /// Configuration elements for a Banking Information view model
        /// </summary>
        public BankingInformationConfiguration Configuration;

        /// <summary>
        /// Indicates whether Payroll is active and should be displayed to users.        
        /// </summary>
        public bool IsPayrollActive { get; set; }

        /// <summary>
        /// Indicates whether Payables are active and should be displayed to users.
        /// </summary>
        public bool IsPayableActive { get; set; }

        /// <summary>
        /// List of Active PayrollDeposits
        /// </summary>
        public List<PayrollDepositViewModel> ActivePayrollDeposits;
        /// <summary>
        /// List of Past (now inactive) PayrollDeposits
        /// </summary>
        public List<PayrollDepositViewModel> PastPayrollDeposits;
        /// <summary>
        /// List of Future (not yet active) PayrollDeposits
        /// </summary>
        public List<PayrollDepositViewModel> FuturePayrollDeposits;

        /// <summary>
        /// Active Refund Deposit
        /// </summary>
        public PayableDepositViewModel ActiveRefundDeposit;
        /// <summary>
        /// Past (now inactive) Refund Deposit
        /// </summary>
        public List<PayableDepositViewModel> PastRefundDeposits;
        /// <summary>
        /// Future (not yet active)  Refund Deposit
        /// </summary>
        public List<PayableDepositViewModel> FutureRefundDeposits;

        public List<PayableDepositAddressViewModel> Addresses;

        #endregion

        public BankingInformationViewModel(
            string payeeId,
            IEnumerable<PayrollDepositDirective> payrollDepositDirectives,
            IEnumerable<PayableDepositDirective> payableDepositDirectives,
            string addressId,
            Colleague.Dtos.Base.BankingInformationConfiguration configuration,
            IEnumerable<Bank> banks,
            IEnumerable<string> userPermissionCodes = null,
            IEnumerable<Address> payableDepositAddressDtos = null, // can pass in either Dtos or models. Dtos will take precedent and models will be ignored if you pass in both
            IEnumerable<PayableDepositAddressViewModel> payableDepositAddressViewModels = null)
        {
            if (string.IsNullOrEmpty(payeeId))
            {
                throw new ArgumentNullException("payeeId");
            }
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            PayeeId = payeeId;

            Configuration = configuration;

            //Set Active Functionality bools
            //payroll is active when directDeposit is enabled, user does NOT have the vendor permission, user has the payroll permission, and user has non-null directDeposits
            IsPayrollActive = configuration.IsDirectDepositEnabled &&
                !userPermissionCodes.HasEditVendorPermission() &&
                userPermissionCodes.HasEditPayrollPermission() &&
                payrollDepositDirectives != null;

            IsPayableActive = configuration.IsPayableDepositEnabled &&
                (userPermissionCodes.HasEditVendorPermission() || userPermissionCodes.HasEditPayablePermission());

            IsVendor = userPermissionCodes.HasEditVendorPermission();

            //Instantiate class members
            ActivePayrollDeposits = new List<PayrollDepositViewModel>();
            PastPayrollDeposits = new List<PayrollDepositViewModel>();
            FuturePayrollDeposits = new List<PayrollDepositViewModel>();

            PastRefundDeposits = new List<PayableDepositViewModel>();
            FutureRefundDeposits = new List<PayableDepositViewModel>();

            Addresses = new List<PayableDepositAddressViewModel>();

            if (string.IsNullOrWhiteSpace(addressId))
            {
                addressId = null;
            }

            AddressId = addressId;

            //Check if user is vendor - if so, set Vendor type attributes
            if (IsVendor)
            {
                //populate Addresses with Dtos or with existing Models
                if (payableDepositAddressDtos != null)
                {
                    Addresses = payableDepositAddressDtos.Select(addrDto => new PayableDepositAddressViewModel(addrDto)).ToList();
                }
                else if (payableDepositAddressViewModels != null)
                {
                    Addresses = payableDepositAddressViewModels.ToList();
                }
            }

            //There will always be  a default address
            Addresses.Insert(0, new PayableDepositAddressViewModel());

            #region PAYROLL DEPOSITS
            if (IsPayrollActive && payrollDepositDirectives != null)
            {
                var allPayrollDepositAccounts = payrollDepositDirectives
                    .Where(d => d != null).ToList();

                var index = 0;  // Index to override deposit order
                ActivePayrollDeposits = allPayrollDepositAccounts
                    .Where(p => p != null && p.StartDate <= DateTime.Today && (!p.EndDate.HasValue || (p.EndDate >= DateTime.Today)))
                    .OrderBy(p => p.Priority).ThenBy(p => p.StartDate).ThenByDescending(p => p.DepositAmount)
                    .Select(payrollDeposit => new PayrollDepositViewModel(payrollDeposit, ++index))
                    .OrderBy(vm => vm.DepositOrder).ToList();

                PastPayrollDeposits = allPayrollDepositAccounts
                        .Where(p => p != null && p.EndDate.HasValue && (p.EndDate < DateTime.Today))
                        .Select(payrollDeposit => new PayrollDepositViewModel(payrollDeposit))
                    .OrderByDescending(vm => vm.EndDate).ToList();

                FuturePayrollDeposits = allPayrollDepositAccounts
                        .Where(p => p != null && p.StartDate > DateTime.Today)
                        .Select(payrollDeposit => new PayrollDepositViewModel(payrollDeposit))
                    .OrderBy(vm => vm.StartDate).ToList();
            }
            #endregion

            #region REFUND DEPOSITS
            if (IsPayableActive && payableDepositDirectives != null)
            {
                var allPayableDepositAccounts = payableDepositDirectives.
                    Where(d => d != null).ToList();

                foreach (var account in allPayableDepositAccounts)
                {
                    if (!string.IsNullOrWhiteSpace(account.RoutingId))
                    {
                        var knownBank = banks.FirstOrDefault(b => b.RoutingId == account.RoutingId);
                        account.BankName = (knownBank != null) ? knownBank.Name : account.RoutingId;
                    }
                    else if (!string.IsNullOrWhiteSpace(account.InstitutionId) && !string.IsNullOrWhiteSpace(account.BranchNumber))
                    {
                        var knownBank = banks.FirstOrDefault(b => b.InstitutionId == account.InstitutionId && b.BranchTransitNumber == account.BranchNumber);
                        account.BankName = (knownBank != null) ? knownBank.Name : account.InstitutionId + "-" + account.BranchNumber;
                    }
                }

                ActiveRefundDeposit = allPayableDepositAccounts
                    .Where(p => p != null && p.AddressId == AddressId && p.StartDate <= DateTime.Today && (!p.EndDate.HasValue || p.EndDate >= DateTime.Today))
                    .Select(payableDeposit => new PayableDepositViewModel(payableDeposit))
                    .OrderByDescending(vm => vm.AddDateTime)
                    .FirstOrDefault();

                if (ActiveRefundDeposit != null && !ActiveRefundDeposit.IsElectronicPaymentRequested)
                {
                    ActiveRefundDeposit = null;
                }


                PastRefundDeposits = allPayableDepositAccounts
                        .Where(p => p != null && p.AddressId == AddressId && p.IsElectronicPaymentRequested && (p.EndDate.HasValue && (p.EndDate < DateTime.Today || (p.EndDate == DateTime.Today && (ActiveRefundDeposit == null || p.Id != ActiveRefundDeposit.Id)))))
                        .Select(payableDeposit => new PayableDepositViewModel(payableDeposit))
                    .OrderByDescending(vm => vm.EndDate).ToList();

                FutureRefundDeposits = allPayableDepositAccounts
                        .Where(p => p != null && p.AddressId == AddressId && p.IsElectronicPaymentRequested && p.StartDate > DateTime.Today)
                        .Select(payableDeposit => new PayableDepositViewModel(payableDeposit))
                    .OrderBy(vm => vm.StartDate).ToList();
            }
            #endregion
        }

        /// <summary>
        /// Instantiate an empty BankingInformationViewModel instance
        /// </summary>
        public BankingInformationViewModel()
        {

        }


        #region CONVERSION FUNCTIONS

        public List<PayrollDepositDirective> ConvertModelToPayrollDepositDirectivesDto()
        {
            var payrollDepositViewModels = ActivePayrollDeposits.Concat(PastPayrollDeposits).Concat(FuturePayrollDeposits);
            return payrollDepositViewModels.Select(pd =>
                new PayrollDepositDirective()
                {
                    AccountIdLastFour = string.IsNullOrEmpty(pd.NewAccountId) ? pd.AccountIdLastFour : pd.NewAccountId.Substring((pd.NewAccountId.Length >= 4 ? pd.NewAccountId.Length - 4 : 0)),
                    BankAccountType = pd.Type,
                    BankName = pd.BankName,
                    BranchNumber = pd.BranchNumber,
                    DepositAmount = pd.DepositAmount,
                    EndDate = pd.EndDate,
                    Id = pd.Id,
                    InstitutionId = pd.InstitutionId,
                    IsVerified = pd.IsVerified,
                    NewAccountId = pd.NewAccountId,
                    Nickname = pd.Nickname,
                    PersonId = pd.PersonId,
                    Priority = pd.DepositOrder,
                    RoutingId = pd.RoutingId,
                    StartDate = pd.StartDate,
                    Timestamp = new Timestamp()
                    {
                        AddDateTime = pd.AddDateTime,
                        AddOperator = pd.AddOperator,
                        ChangeDateTime = pd.ChangeDateTime,
                        ChangeOperator = pd.ChangeOperator
                    }
                }
                ).ToList();
        }

        public List<PayableDepositDirective> ConvertModelToPayableDepositDirectiveDtos()
        {
            var payableDepositViewModels = PastRefundDeposits.Concat(new List<PayableDepositViewModel>() { ActiveRefundDeposit }).Concat(FutureRefundDeposits);
            return payableDepositViewModels
                .Where(pd => pd != null)
                .Select(pd => new PayableDepositDirective()
                {
                     AccountIdLastFour = pd.AccountIdLastFour,
                     AddressId = pd.AddressId,
                     BankAccountType = pd.Type,
                     BankName = pd.BankName,
                     BranchNumber = pd.BranchNumber,
                     EndDate = pd.EndDate,
                     Id = pd.Id,
                     InstitutionId = pd.InstitutionId,
                     IsElectronicPaymentRequested = pd.IsElectronicPaymentRequested,
                     IsVerified = pd.IsVerified,
                     NewAccountId = pd.NewAccountId,
                     Nickname = pd.Nickname,
                     PayeeId = pd.PayeeId,
                     RoutingId = pd.RoutingId,
                     StartDate = pd.StartDate,
                     Timestamp = new Timestamp()
                     {
                         AddDateTime = pd.AddDateTime,
                         AddOperator = pd.AddOperator,
                         ChangeDateTime = pd.ChangeDateTime,
                         ChangeOperator = pd.ChangeOperator
                     }
                 }).ToList();
        }


        #endregion
    }
}