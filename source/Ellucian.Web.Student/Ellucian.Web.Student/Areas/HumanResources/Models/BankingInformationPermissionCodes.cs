﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.HumanResources.Models
{
    public static class BankingInformationPermissionCodes
    {
        /// <summary>
        /// Permission to view and edit banking information as a vendor
        /// </summary>
        public const string EditVendorBankingInformation = "EDIT.VENDOR.BANKING.INFORMATION";

        /// <summary>
        /// Permission to view and edit payroll direct deposits
        /// </summary>
        public const string EditPayrollBankingInformation = "EDIT.PAYROLL.BANKING.INFORMATION";

        /// <summary>
        /// Permission to view and edit payable deposits
        /// </summary>
        public const string EditEchecksBankingInformation = "EDIT.ECHECKS.BANKING.INFORMATION";

        /// <summary>
        /// Helper extension to determine if the list of user permission codes contains the EditVendorBankingInformation code
        /// </summary>
        /// <param name="userPermissionCodes"></param>
        /// <returns></returns>
        public static bool HasEditVendorPermission(this IEnumerable<string> userPermissionCodes)
        {
            return userPermissionCodes != null && userPermissionCodes.Contains(EditVendorBankingInformation, StringComparer.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Helper extension to determine if the list of user permission codes contains the EditPayrollBankingInformation code
        /// </summary>
        /// <param name="userPermissionCodes"></param>
        /// <returns></returns>
        public static bool HasEditPayrollPermission(this IEnumerable<string> userPermissionCodes)
        {
            return userPermissionCodes != null && userPermissionCodes.Contains(EditPayrollBankingInformation, StringComparer.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Helper extension to determine if the list of user permission codes contains the EditEchecksBankingInformation code
        /// </summary>
        /// <param name="userPermissionCodes"></param>
        /// <returns></returns>
        public static bool HasEditPayablePermission(this IEnumerable<string> userPermissionCodes)
        {
            return userPermissionCodes != null && userPermissionCodes.Contains(EditEchecksBankingInformation, StringComparer.CurrentCultureIgnoreCase);
        }
    }
}