﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Areas.HumanResources.Models
{
    /// <summary>
    /// View Model for BankingInformation Page PayableDeposits
    /// </summary>
    public class PayableDepositViewModel
    {
        public string Id;
        public string PayeeId;
        public string RoutingId;
        public string InstitutionId;
        public string BranchNumber;
        public string BankName;
        public string NewAccountId;
        public string AccountIdLastFour;
        public string Nickname;
        public BankAccountType Type;
        public BankingAuthenticationToken SecurityToken;        
        public string AddressId;
        public DateTime StartDate;
        public DateTime? EndDate;
        public bool IsElectronicPaymentRequested;
        public DateTimeOffset AddDateTime;
        public DateTimeOffset ChangeDateTime;
        public string AddOperator;
        public string ChangeOperator;
        public bool IsVerified;

        /// <summary>
        /// Instantiate a PayableDepositViewModel using a Base PayableDepositDirective DTO
        /// </summary>
        /// <param name="depositAccount"></param>
        /// <param name="payableDeposit"></param>
        public PayableDepositViewModel(Colleague.Dtos.Base.PayableDepositDirective payableDepositDirective)
        {
            if (payableDepositDirective == null)
            {
                throw new ArgumentNullException("payableDepositDirective");
            }

            Id = payableDepositDirective.Id;
            PayeeId = payableDepositDirective.PayeeId;
            RoutingId = payableDepositDirective.RoutingId;
            InstitutionId = payableDepositDirective.InstitutionId;
            BranchNumber = payableDepositDirective.BranchNumber;
            BankName = payableDepositDirective.BankName;
            NewAccountId = payableDepositDirective.NewAccountId;
            AccountIdLastFour = payableDepositDirective.AccountIdLastFour;
            Nickname = payableDepositDirective.Nickname;
            Type = payableDepositDirective.BankAccountType;
            SecurityToken = null;            
            AddressId = string.IsNullOrWhiteSpace(payableDepositDirective.AddressId) ? null : payableDepositDirective.AddressId.Trim();
            StartDate = payableDepositDirective.StartDate;  // Have the startDate available to display for past / future accounts
            EndDate = payableDepositDirective.EndDate;
            IsElectronicPaymentRequested = payableDepositDirective.IsElectronicPaymentRequested;
            AddDateTime = payableDepositDirective.Timestamp.AddDateTime;
            ChangeDateTime = payableDepositDirective.Timestamp.ChangeDateTime;
            AddOperator = payableDepositDirective.Timestamp.AddOperator;
            ChangeOperator = payableDepositDirective.Timestamp.ChangeOperator;
            IsVerified = payableDepositDirective.IsVerified;
        }

        /// <summary>
        /// Instantiate an empty PayableDepositViewModel
        /// </summary>
        public PayableDepositViewModel()
        {

        }

        public PayableDepositDirective ConvertModelToPayableDepositDirectiveDtos()
        {
            return new PayableDepositDirective()
            {
                AccountIdLastFour = this.AccountIdLastFour,
                AddressId = this.AddressId,
                BankAccountType = this.Type,
                BankName = this.BankName,
                BranchNumber = this.BranchNumber,
                EndDate = this.EndDate,
                Id = this.Id,
                InstitutionId = this.InstitutionId,
                IsElectronicPaymentRequested = this.IsElectronicPaymentRequested,
                IsVerified = this.IsVerified,
                NewAccountId = this.NewAccountId,
                Nickname = this.Nickname,
                PayeeId = this.PayeeId,
                RoutingId = this.RoutingId,
                StartDate = this.StartDate,
                Timestamp = new Timestamp()
                {
                    AddDateTime = this.AddDateTime,
                    AddOperator = this.AddOperator,
                    ChangeDateTime = this.ChangeDateTime,
                    ChangeOperator = this.ChangeOperator
                }
            };
        }
    }
}