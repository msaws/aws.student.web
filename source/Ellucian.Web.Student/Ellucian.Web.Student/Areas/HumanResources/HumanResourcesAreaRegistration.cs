﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Student.Licensing;
using System.ComponentModel;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.HumanResources
{
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class HumanResourcesAreaRegistration : LicenseAreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HumanResources";
            }
        }

        public override void LicensedRegistration(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HumanResources_default",
                "HumanResources/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "Ellucian.Web.Student.Areas.HumanResources.Controllers" });
        }
    }
}
