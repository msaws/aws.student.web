﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Controllers;
using Ellucian.Web.Student.Filters;
using slf4net;

namespace Ellucian.Web.Student.Areas.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class TaxInformationController : BaseTaxInformationController
    {
        /// <summary>
        /// Creates a new HomeController instance.
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public TaxInformationController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(adapterRegistry, settings, logger)
        {
        }
    }
}