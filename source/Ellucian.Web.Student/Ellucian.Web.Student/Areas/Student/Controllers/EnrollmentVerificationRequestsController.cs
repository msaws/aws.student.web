﻿//Copyright 2016 Ellucain Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using Ellucian.Web.Student.Areas.Student.Models.StudentRequests;
using Ellucian.Web.Student.Areas.Student.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class EnrollmentVerificationRequestsController : BaseStudentController
    {
        private StudentHelper _studentHelper;
        /// <summary>
        /// Creates an instance of EnrollmentVerificationRequestsController
        /// </summary>
        /// <param name="adapterRegistry">adapter registry helper; injected by the framework</param>
        /// <param name="settings">settings helper; injected by the framework</param>
        /// <param name="logger">logging helper; injected by the framework</param>
        public EnrollmentVerificationRequestsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, StudentHelper studentHelper)
            : base(settings, logger)
        {
            _studentHelper = studentHelper;
        }

        /// <summary>
        /// Index page for enrollment verification landing page - where you can view existing requests and add new ones.
        /// </summary>
        /// <returns>A View of the existing enrollment verification requests</returns>
        [LinkHelp]
        [PageAuthorize("enrollmentVerificationHome")]
        [ActionName("Index")]
        public async Task<ActionResult> IndexAsync(string Notify = null)
        {
            string emailAddress = string.Empty;
            ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.RequestsResources, "AreaDescription");
            ViewBag.CssAfterOverride = new List<string>();
            ViewBag.Notification = Notify;
            if (Notify != null && Notify != string.Empty)
            {
                var studentTask = ServiceClient.GetStudentAsync(GetEffectivePersonId());
                var studentRequestConfigurationTask = ServiceClient.GetStudentRequestConfigurationAsync();
                await Task.WhenAll(studentTask, studentRequestConfigurationTask);
                try
                {
                    emailAddress = _studentHelper.GetEmailAddress(studentTask.Result, studentRequestConfigurationTask.Result, StudentRequestType.EnrollmentVerificationRequest);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }

            }
            ViewBag.EmailAddress = emailAddress;
            return View();
        }

        /// <summary>
        /// Get any existing enrollment verification requests for the student
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <returns>The enrollment request data</returns>
        [PageAuthorize("enrollmentVerificationHome")]
        public async Task<JsonResult> GetExistingEnrollmentVerificationsAsync(string studentId)
        {
            if (string.IsNullOrEmpty(studentId)) studentId = GetEffectivePersonId();
            var studentEnrollmentRequests = await ServiceClient.GetStudentEnrollmentRequestsAsync(studentId);
            List<StudentRequest> studentRequestDtos = studentEnrollmentRequests.ConvertAll(x => (StudentRequest)x);

            // Get InvoicePayments for these requests so we can determine paid status
            var invoicePayments = new List<InvoicePayment>();
            var invoiceIds = studentEnrollmentRequests.Where(r => !string.IsNullOrEmpty(r.InvoiceNumber)).Select(x => x.InvoiceNumber).Distinct().ToList();
            if (invoiceIds != null && invoiceIds.Any())
            {
                invoicePayments = (await ServiceClient.QueryInvoicePaymentsAsync(new InvoiceQueryCriteria() { InvoiceIds = invoiceIds })).ToList();
            }
            var financeConfigurationTask = ServiceClient.GetCachedFinanceConfiguration();
            var countriesTask = ServiceClient.GetCachedCountriesAsync();
            var requestConfigurationTask = ServiceClient.GetStudentRequestConfigurationAsync();
            try
            {
                await Task.WhenAll(financeConfigurationTask, countriesTask, requestConfigurationTask);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(StudentResourceFiles.RequestsResources, "GetExistingRequestsFailureMessage"));
            }
            var financeConfiguration = financeConfigurationTask.Result;
            var countries = countriesTask.Result;
            var requestConfiguration = requestConfigurationTask.Result;
            // In the case of enrollment verification there are no restrictions to check. Pass null.
            var studentEnrollmentRequestModel = new StudentRequestsModel(studentRequestDtos, countries, null, invoicePayments, financeConfiguration, requestConfiguration.EnrollmentRequestRequireImmediatePayment);
            return Json(studentEnrollmentRequestModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// New enrollment verification entry form
        /// </summary>
        /// <param name="Notify"></param>
        /// <returns></returns>
        [LinkHelp(HelpIdOverride = "EnrollmentVerificationRequest")]
        [PageAuthorize("enrollmentVerificationRequest")]
        public ActionResult EnrollmentVerificationRequest()
        {

            try
            {
                ViewBag.Title = "Enrollment Verification  Requests";
                return View();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw ex;
            }
        }

        /// <summary>
        /// ajax get request to retrieve the enrollment request data (dropdown values and other initial setup values) 
        /// <returns>EnrollmentVerificationRequestViewModel</returns>
        [PageAuthorize("enrollmentVerificationRequest")]
        public async Task<JsonResult> GetEnrollmentVerificationRequestsInfoAsync()
        {
            try
            {

                var countriesTask = ServiceClient.GetCachedCountriesAsync();
                var statesTask = ServiceClient.GetCachedStatesAsync();
                //retrieve student configuration to get charge paid and if require immediate payment
                var studentRequestsConfigurationTask = ServiceClient.GetStudentRequestConfigurationAsync();
                try
                {
                    await Task.WhenAll(countriesTask, statesTask, studentRequestsConfigurationTask);
                }
                catch (Exception ex)
                {
                    var exceptionMessage = ex.Message;
                    if (ex is AggregateException)
                    {
                        AggregateException ae = ex as AggregateException;
                        exceptionMessage = ae.LogAggregateExceptions(Logger);
                    }
                    throw;
                }
                var countries = countriesTask.Result;
                var states = statesTask.Result;
                var studentRequestsConfiguration = studentRequestsConfigurationTask.Result;

                EnrollmentVerificationRequestViewModel enrollmentRequest = new EnrollmentVerificationRequestViewModel(countries, states);
                enrollmentRequest.RequireImmediatePayment = studentRequestsConfiguration.EnrollmentRequestRequireImmediatePayment;
                return Json(enrollmentRequest, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(StudentResourceFiles.RequestsResources, "GetInformationFailureMessage"));
            }


        }


        /// <summary>
        /// Creates a new enrollment verification request. Form Post with enrollment request fields. 
        /// </summary>
        /// <param name="newEnrollmentRequest">The new student enrollment verification request</param>
        /// <returns></returns>
        [PageAuthorize("enrollmentVerificationRequest")]
        public async Task<ActionResult> CreateEnrollmentVerificationRequestAsync([ModelBinder(typeof(StudentRequestJsonBinder))]  EnrollmentVerificationRequestModel newEnrollmentRequest)
        {

            if (newEnrollmentRequest == null)
            {
                Logger.Error("NewEnrollmentVerificationRequest is null or empty, cannot complete enrollment verification request");
                throw new ArgumentNullException("newEnrollmentRequest");
            }
            try
            {

                //Build StudentEnrollmentRequest Dto
                StudentEnrollmentRequest requestDto = new StudentEnrollmentRequest();
                requestDto.StudentId = GetEffectivePersonId();
                requestDto.RecipientName = newEnrollmentRequest.Recipient;
                requestDto.MailToAddressLines = newEnrollmentRequest.MailToAddressLines;
                requestDto.HoldRequest = newEnrollmentRequest.HoldRequestCode;
                requestDto.MailToCity = newEnrollmentRequest.MailToCity;
                requestDto.MailToCountry = newEnrollmentRequest.MailToCountry;
                requestDto.MailToPostalCode = newEnrollmentRequest.MailToPostalCode;
                requestDto.MailToState = newEnrollmentRequest.MailToState;
                requestDto.NumberOfCopies = newEnrollmentRequest.NumberOfCopies;
                requestDto.Comments = newEnrollmentRequest.Comments;
                var response = await ServiceClient.AddStudentEnrollmentRequestAsync(requestDto);
                if (response != null && !string.IsNullOrEmpty(response.Id) && newEnrollmentRequest.RequireImmediatePayment && !string.IsNullOrEmpty(response.InvoiceNumber))
                {
                    //redirect to payment page
                    return RedirectToAction("EnrollmentRequestProceedToPayment", "EnrollmentVerificationRequests", new
                    {
                        area = "Student",
                        RequestId = response.Id
                    });
                }
                return RedirectToAction("Index", "EnrollmentVerificationRequests", new
                {
                    area = "Student",
                    Notify = StudentRequestNotificationType.EnrollmentRequestSubmitSuccess
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return RedirectToAction("Index", "EnrollmentVerificationRequests", new
                {
                    area = "Student",
                    Notify = StudentRequestNotificationType.EnrollmentRequestSubmitFailure
                });
            }

        }

        /// <summary>
        /// this is a get request.
        /// to return proceed to payment view that will have payment methods to select from.
        /// this will take request id for which the payment is yet to be made.
        /// </summary>
        /// <param name="RequestId">RequestId</param>
        /// <returns></returns>
        [LinkHelp(HelpIdOverride = "EnrollmentVerificationRequestPayment")]
        [ActionName("EnrollmentRequestProceedToPayment")]
        [PageAuthorize("enrollmentVerificationHome", "enrollmentVerificationRequest")]
        public async Task<ActionResult> EnrollmentRequestProceedToPaymentAsync(string RequestId = null)
        {
            ViewBag.Title = "Enrollment Verification Requests Payment";
            if (RequestId == null)
            {
                throw new ArgumentNullException("RequestId");
            }
            try
            {

                StudentEnrollmentRequest enrollment = await ServiceClient.GetStudentEnrollmentRequestAsync(RequestId);
                var studentId = GetEffectivePersonId();
                if (enrollment.StudentId != studentId)
                {
                    var message = string.Format("Request Id {0} does not belong to user {1}", RequestId, studentId);
                    throw new Exception(message);
                }
                // We should ONLY allow them to make a payment on a request that has an invoice number
                // If we have gotten here and the request doesn't have an invoice it is an error.
                if (string.IsNullOrEmpty(enrollment.InvoiceNumber))
                {
                    var message = string.Format("Request Id {0} does not have associated invoice number", RequestId);
                    throw new Exception(message);
                }
                // Get the invoice so we can get the amount of the invoice
                var invoicePayment = (await ServiceClient.QueryInvoicePaymentsAsync(new InvoiceQueryCriteria() { InvoiceIds = new List<string>() { enrollment.InvoiceNumber } })).FirstOrDefault();                
                if (invoicePayment == null)
                {
                    var message = string.Format("Unable to retrieve request invoice information", RequestId);
                    throw new Exception(message);
                }
                if ((invoicePayment.Amount - invoicePayment.AmountPaid) <= 0)
                {
                    //redirect to payment page because there is nothing to pay here
                    return RedirectToAction("Index", "EnrollmentRequests", new
                    {
                        area = "Student",
                        //Notify = StudentRequestNotificationType.EnrollmentRequestSubmitSuccess
                    });
                }
                int numberOfCopies = enrollment != null && enrollment.NumberOfCopies.HasValue ? enrollment.NumberOfCopies.Value : 1;
                StudentRequestPaymentModel paymentModel = new StudentRequestPaymentModel(enrollment.InvoiceNumber, numberOfCopies);

                var financeConfigTask = ServiceClient.GetCachedFinanceConfiguration();
                var feeDetailsTask = ServiceClient.GetStudentRequestFeeAsync(studentId, enrollment.Id);
                await Task.WhenAll(financeConfigTask, feeDetailsTask);
                FinanceConfiguration financeConfiguration = financeConfigTask.Result;
                StudentRequestFee feeDetails = feeDetailsTask.Result;
                if (feeDetails == null)
                {
                    throw new Exception("Fee details retrieved for enrollment verification request is null");
                }
                paymentModel.SetPaymentDetails(financeConfiguration.PaymentMethods, feeDetails, invoicePayment);
                return View("EnrollmentRequestsProceedToPayment", paymentModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return RedirectToAction("Index", "EnrollmentVerificationRequests", new
                {
                    area = "Student",
                    Notify = StudentRequestNotificationType.EnrollmentRequestPaymentFailure
                });
            }
        }

        /// <summary>
        /// ajax post call to pass payment model to create Make A Payment model which finance Payments controller payment review action needs.
        /// </summary>
        /// <param name="paymentModelJson"></param>
        /// <returns></returns>
        [HttpPost]
        [PageAuthorize("enrollmentVerificationHome", "enrollmentVerificationRequest")]
        public async Task<JsonResult> CreateEnrollmentVerificationRequestPaymentAsync(string paymentModelJson)
        {
            if (paymentModelJson == null)
            {
                Logger.Error("enrollment request payment model is null");
                throw new ArgumentNullException("paymentModelJson");
            }
            var studentRequestPaymentModel = JsonConvert.DeserializeObject<StudentRequestPaymentModel>(paymentModelJson);
            if (studentRequestPaymentModel == null)
                    {
                Logger.Error("enrollment request payment model was not deserialized");
                throw new Exception("enrollment request payment model desrialized exception");
                    }
            try
            {
                MakeAPaymentModel makeAPaymentModel = await _studentHelper.BuildMakeAPaymentModelAsync(studentRequestPaymentModel, GetEffectivePersonId(), ServiceClient);
                    Response.StatusCode = (int)HttpStatusCode.Accepted;
                return Json(new { MakeAPaymentModel = makeAPaymentModel }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }

        }

    }
}