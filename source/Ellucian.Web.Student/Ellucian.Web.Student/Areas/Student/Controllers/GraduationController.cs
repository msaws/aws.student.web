﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Models.UserProfile;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Areas.Student.Models.Graduation;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.IO;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using System.Web.Mvc;
using Ellucian.Web.Student.Areas.Student.Utility;

namespace Ellucian.Web.Student.Areas.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class GraduationController : BaseStudentController
    {
        private StudentHelper _studentHelper;
        /// <summary>
        /// Creates a new GraduationController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public GraduationController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, StudentHelper studentHelper)
            : base(settings, logger)
        {
            _studentHelper = studentHelper;
        }

        /// <summary>
        /// Index page for viewing programs you can apply to.
        /// </summary>
        /// <returns>A View of the potential graduation programs for a student</returns>
        [LinkHelp]
        [PageAuthorize("graduationHome")]
        public ActionResult Index(string Notify = "")
        {
            ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "AreaDescription");
            ViewBag.CssAfterOverride = new List<string>();
            ViewBag.Notification = Notify;
            return View();
        }
        /// <summary>
        /// Index page for viewing and entering a graduation application.
        /// </summary>
        /// <param name="programCode">Program code for the application</param>
        /// <param name="applicationExists">Indicates if application was already submitted. Used to indicate add or update</param>
        /// <returns>A View of the graduation application process for a student</returns>
        [LinkHelp]
        [PageAuthorize("graduationApplication")]
        public ActionResult GraduationApplication(string programCode, bool? applicationExists)
        {
            ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "AreaDescription");
            ViewBag.CssAfterOverride = new List<string>();
            ViewBag.ProgramCode = programCode;
            ViewBag.ApplicationExists = applicationExists;
            return View();
        }

        /// <summary>
        /// Index page for confirming a graduation application submission
        /// </summary>
        /// <returns>A View of the graduation application confirmation</returns>
        [LinkHelp]
        [PageAuthorize("graduationSubmission")]
        [ActionName("SubmissionStatus")]
        public async Task<ActionResult> SubmissionStatusAsync(HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            try
            {
                // Retrieve the student's email 
                var studentId = GetEffectivePersonId();
                var student = await ServiceClient.GetStudentAsync(studentId);
                var graduationConfiguration = await ServiceClient.GetGraduationConfigurationAsync();
                var emailAddress = string.Empty;
                if (!string.IsNullOrEmpty(graduationConfiguration.EmailGradNotifyPara) && statusCode == HttpStatusCode.OK)
                {
                    var preferredEmailType = graduationConfiguration.DefaultWebEmailType;
                    var emailAddressObject = !string.IsNullOrEmpty(preferredEmailType) ? student.EmailAddresses.Where(p => p.TypeCode == preferredEmailType).FirstOrDefault() : null;
                    if (emailAddressObject == null)
                    {
                        emailAddress = student.PreferredEmailAddress;
                    }
                    else
                    {
                        emailAddress = emailAddressObject.Value;
                    }
                }
                ViewBag.email = emailAddress;

                ViewBag.CssAfterOverride = new List<string>();
                if (statusCode == HttpStatusCode.OK)
                {
                    ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "SucceedDescription");
                    ViewBag.Message = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "SucceedMessage");
                    ViewBag.Success = true;
                }
                else if (statusCode == HttpStatusCode.Conflict)
                {
                    ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "ConflictDescription");
                    ViewBag.Message = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "ConflictMessage");
                    ViewBag.Success = null;
                }
                else
                {
                    ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "FailDescription");
                    ViewBag.Message = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "FailMessage");
                    ViewBag.Success = null;
                }
            }
            catch (Exception)
            {
                ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "FailDescription");
                ViewBag.Message = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "FailMessage");
                ViewBag.Success = null;
            }
            return View("SubmissionStatus");
        }

        /// <summary>
        /// Get the previous graduation application data
        /// </summary>
        /// <returns>The graduation data</returns>
        [PageAuthorize("graduationHome")]
        public async Task<JsonResult> GetPreviousApplicationInformationAsync(string studentId)
        {
            if (string.IsNullOrEmpty(studentId)) studentId = GetEffectivePersonId();
            var currentUser = CurrentUser;
            var graduationConfigurationTask = ServiceClient.GetCachedGraduationConfigurationAsync();
            var graduationApplicationsTask = ServiceClient.RetrieveGraduationApplicationsAsync(studentId);
            var studentTask = ServiceClient.GetStudentAsync(studentId);
            var programsTask = ServiceClient.GetCachedProgramsAsync();
            var majorsTask = ServiceClient.GetCachedMajorsAsync();
            var allTermsTask = ServiceClient.GetCachedTermsAsync();
            var studentProgramsTask = ServiceClient.GetStudentPrograms2Async(studentId, true);
            try
            {
                await Task.WhenAll(graduationConfigurationTask, graduationApplicationsTask, studentTask, programsTask, majorsTask, allTermsTask, studentProgramsTask);
            }
            catch (Exception e)
            {
                throw e;
            }

            var student = studentTask.Result;
            var programs = programsTask.Result;
            var majors = majorsTask.Result;
            var allTerms = allTermsTask.Result;
            var configuration = graduationConfigurationTask.Result;
            var studentPrograms = studentProgramsTask.Result;
            var graduationApplications = graduationApplicationsTask.Result;
               FinanceConfiguration financeConfiguration = null;
               if (configuration.RequireImmediatePayment)
               {
                   financeConfiguration = await ServiceClient.GetCachedFinanceConfiguration();
               }
               var graduationModel = new PreviousApplicationsModel(student, programs, majors, allTerms, configuration, studentPrograms, graduationApplications, currentUser, financeConfiguration);
            return Json(graduationModel, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Retrieves information to create, view or edit a graduation application 
        /// </summary>
        /// <param name="programCode">Program code of the application</param>
        /// <returns>The graduation information data needed to view (or edit) a new application</returns>
        [PageAuthorize("graduationApplication")]
        public async Task<JsonResult> GetStudentGraduationInformationAsync(string programCode, bool? applicationExists)
        {
            try
            {
                if (string.IsNullOrEmpty(programCode) || !applicationExists.HasValue)
                {
                    throw new ArgumentNullException("programCode", "You must supply a program code for the application.");
                }
                var studentId = GetEffectivePersonId();
                // First see if an application exists for this student and program.  If so, include it in the full model.
                GraduationApplication graduationApplication = null;
                if (applicationExists.Value)
                {
                    try
                    {
                        graduationApplication = await ServiceClient.RetrieveGraduationApplicationAsync(studentId, programCode);
                    }
                    catch
                    {
                        graduationApplication = null;
                        throw;
                    }
                }
                var graduationModel = await BuildGraduationModelAsync(studentId, programCode, graduationApplication);
                return Json(graduationModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(ex,ex.ToString());
                var exceptionMessage = ex.Message;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(exceptionMessage, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task RequestPreferredAddressAsync(string submissionJson, GraduationApplication graduationApplicationDto)
        {
            // Get the current user's system ID, use it to get the user's basic demographic and configuration data.
            var personId = CurrentUser.PersonId;

            // Bypass the cache to ensure latest data, which can be necessary for updates to succeed
            var profile = await ServiceClient.GetProfileAsync(personId, false);

            var config = await ServiceClient.GetUserProfileConfigurationAsync();
            var wbAddressType = config.UpdatableAddressTypes[0];

            var addr = profile.Addresses.Where(a => a.TypeCode.Split(',').Contains(wbAddressType) ).FirstOrDefault();

            if (addr != null)
            {
                addr.State = graduationApplicationDto.MailDiplomaToState;
                addr.City = graduationApplicationDto.MailDiplomaToCity;
                addr.AddressLines = graduationApplicationDto.MailDiplomaToAddressLines;
                addr.PostalCode = graduationApplicationDto.MailDiplomaToPostalCode;
                addr.Country = graduationApplicationDto.MailDiplomaToCountry;
            }
            else
            {
                profile.Addresses.Add(new Address()
                {
                    AddressLines = graduationApplicationDto.MailDiplomaToAddressLines,
                    City = graduationApplicationDto.MailDiplomaToCity,
                    State = graduationApplicationDto.MailDiplomaToState,
                    Country = graduationApplicationDto.MailDiplomaToCountry,
                    PostalCode = graduationApplicationDto.MailDiplomaToPostalCode,
                    TypeCode = wbAddressType
                });
            }

            await ServiceClient.UpdateProfileAsync(profile as Profile);
        }

        /// <summary>
        /// Add the graduation application data
        /// </summary>
        /// <param name="aubmissionJson">The Application to be submitted</param>
        /// <returns>A JSON object with the status code </returns>
        [PageAuthorize("graduationApplication")]
        public async Task<JsonResult> AddGraduationApplicationAsync(string graduationApplicationJson, string graduationPaymentJson, bool addressChangeRequested)
        {
            string invoiceId = string.Empty;
            MakeAPaymentModel paymentModel = null;
            try
            {
                GraduationApplication graduationApplicationDto = JsonConvert.DeserializeObject<GraduationApplication>(graduationApplicationJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
                if (graduationApplicationDto == null) { throw new ArgumentNullException("graduationApplication", "You need to have an application"); }
                GraduationPaymentModel graduationPaymentModel = JsonConvert.DeserializeObject<GraduationPaymentModel>(graduationPaymentJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
                var returnCode = HttpStatusCode.OK;
                try
                {
                    graduationApplicationDto.StudentId = GetEffectivePersonId();
                    //fill my dto with ids - studentid
                    var addedApplicationDto = await ServiceClient.CreateGraduationApplicationAsync(graduationApplicationDto);

                    if (addressChangeRequested)
                    {
                        try
                        {
                            await RequestPreferredAddressAsync(graduationApplicationJson, graduationApplicationDto);
                        }
                        catch (HttpRequestFailedException)
                        {
                            // Return a 406 if we catch a User Profile Update problem.
                            returnCode = HttpStatusCode.NotAcceptable;
                        }
                    }

                    //get the invoice id 
                    if (graduationPaymentModel.RequireImmediatePayment)
                    {
                        if (string.IsNullOrEmpty(addedApplicationDto.InvoiceNumber))
                        {
                            Logger.Error(string.Format("Graduation Application is create for studentId{0} and programCode{1} but associated invoice number didn't exist, hence payment workflow was not followed", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode));
                            return Json(new { MakeAPaymentModel = paymentModel, StatusCode = returnCode }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            graduationPaymentModel.InvoiceId = addedApplicationDto.InvoiceNumber;
                            graduationPaymentModel.ProgramCode = addedApplicationDto.ProgramCode;
                            paymentModel = await MakeAGraduationPaymentAsync(graduationPaymentModel);
                        }
                    }

                }
                catch (HttpRequestFailedException)
                {
                    Logger.Error(string.Format("Creation of graduation application failed for studentId{0} and programCode{1}", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode));
                    return Json(new { MakeAPaymentModel = paymentModel, StatusCode=HttpStatusCode.Forbidden }, JsonRequestBehavior.AllowGet);
                }
                
                return Json(new { MakeAPaymentModel = paymentModel, StatusCode = returnCode }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                // Throw a 404
                Logger.Error(exception, exception.ToString());
               return Json(new { MakeAPaymentModel = paymentModel, StatusCode = HttpStatusCode.BadRequest }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Update the graduation application data
        /// </summary>
        /// <param name="aubmissionJson">The Application to be submitted</param>
        /// <returns>A JSON object with the status code </returns>
        [PageAuthorize("graduationApplication")]
        public async Task<HttpStatusCodeResult> UpdateGraduationApplicationAsync(string submissionJson, bool addressChangeRequested)
        {
            try
            {
                GraduationApplication graduationApplicationDto = JsonConvert.DeserializeObject<GraduationApplication>(submissionJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
                if (graduationApplicationDto == null) { throw new ArgumentNullException("graduationApplication", "You need to have an application"); }
                try
                {
                    graduationApplicationDto.StudentId = GetEffectivePersonId();
                    //fill my dto with ids - studentid
                    await ServiceClient.UpdateGraduationApplicationAsync(graduationApplicationDto);
                }
                catch (HttpRequestFailedException ex)
                {
                    return new HttpStatusCodeResult(ex.StatusCode);
                }

                if (addressChangeRequested)
                {
                    try
                    {
                        await RequestPreferredAddressAsync(submissionJson, graduationApplicationDto);
                    }
                    catch (HttpRequestFailedException)
                    {
                        // Return a 406 if we catch a User Profile Update problem.
                        return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable);
                    }
                }
                return new HttpStatusCodeResult(HttpStatusCode.OK);

            }
            catch (Exception exception)
            {
                // Throw a 404
                Logger.Error(exception,exception.ToString());
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
        }

        /// <summary>
        /// Builds the full GraduationModel needed to add, view or edit a graduation application. This model contains all the configuration options
        /// </summary>
        /// <param name="studentId">Student Id making the request</param>
        /// <param name="programCode">Program Code for the application</param>
        /// <param name="graduationApplication">(Optional) In the case of viewing or editing this is used to build the application detail model.</param>
        /// <returns></returns>
        private async Task<GraduationModel> BuildGraduationModelAsync(string studentId, string programCode, GraduationApplication graduationApplication = null)
        {
            if (string.IsNullOrEmpty(programCode))
            {
                throw new ArgumentNullException("programCode", "You must supply a program code for the application.");
            }
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException("studentId", "You must supply a studentId for the application.");
            }
            var graduationConfigurationTask = ServiceClient.GetCachedGraduationConfigurationAsync();
            var capSizeTask = ServiceClient.GetCachedCapSizesAsync();
            var gownSizeTask = ServiceClient.GetCachedGownSizesAsync();
            var commencementSitesTask = ServiceClient.GetCachedCommencementSitesAsync();
            var countriesTask = ServiceClient.GetCachedCountriesAsync();
            var statesTask = ServiceClient.GetCachedStatesAsync();
            var studentTask = ServiceClient.GetStudentAsync(studentId);
            var programsTask = ServiceClient.GetCachedProgramsAsync();
            var majorsTask = ServiceClient.GetCachedMajorsAsync();
            var minorsTask = ServiceClient.GetCachedMinorsAsync();
            var specializationsTask = ServiceClient.GetCachedSpecializationsAsync();
            var allTermsTask = ServiceClient.GetCachedTermsAsync();
            var studentProgramTask = ServiceClient.GetStudentPrograms2Async(studentId, true);
            try
            {
                await Task.WhenAll(graduationConfigurationTask, capSizeTask, gownSizeTask, commencementSitesTask, countriesTask, statesTask, studentTask, programsTask, majorsTask, minorsTask, specializationsTask, allTermsTask, studentProgramTask);
            }
            catch (Exception exception)
            {
                // Log the aggregate errors - there could be multiples -- and then throw a general exception.
                var exceptionMessage = exception.Message;
                if (exception is AggregateException)
                {
                    AggregateException ae = exception as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                Logger.Error(exception.ToString());
                throw new Exception();
            }


            var student = studentTask.Result;
            var program = programsTask.Result.Where(x => x.Code == programCode).FirstOrDefault(); ;
            var majors = majorsTask.Result;
            var minors = minorsTask.Result;
            var specializations = specializationsTask.Result;
            var allTerms = allTermsTask.Result;
            var graduationConfiguration = graduationConfigurationTask.Result;
            var studentPrograms = studentProgramTask.Result;
            var capSizes = capSizeTask.Result;
            var gownSizes = gownSizeTask.Result;
            var commencementSites = commencementSitesTask.Result;
            var countries = countriesTask.Result;
            var states = statesTask.Result;
            var userprofileConfig = await ServiceClient.GetUserProfileConfigurationAsync();
            var roles = await ServiceClient.GetCachedRolesAsync();
            GraduationApplicationFee graduationFees = null;
            FinanceConfiguration financeConfiguration =null;
            if (graduationConfiguration!=null && graduationConfiguration.RequireImmediatePayment)
            {
                try
                {
                //retrieve payment methods
                    financeConfiguration = await ServiceClient.GetCachedFinanceConfiguration();
                //retrieve graduation application fee
                    graduationFees = await ServiceClient.GetGraduationApplicationFeeAsync(studentId, programCode);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            if (Logger.IsInfoEnabled)
            {
                if (graduationFees != null)
                {
                    if (graduationFees.Amount == null || (graduationFees.Amount != null && graduationFees.Amount <= 0))
                    {
                        Logger.Info("Cannot process payment as graduation fees is not provided");
                    }
                    if (string.IsNullOrEmpty(graduationFees.PaymentDistributionCode))
                    {
                        Logger.Info("Cannot process payment as payment distribution code is not provided");
                    }
                }
                if (graduationConfiguration != null && !graduationConfiguration.RequireImmediatePayment)
                {
                    Logger.Info("Cannot process payment as require immediate payment flag is not set");
                }
            }

         
            List<PrimaryLocationModel> primaryLocations = await BuildPrimaryLocationAsync(studentId, programCode, studentPrograms, program.Locations, student.StudentHomeLocations);

            return new GraduationModel(student, program, majors, minors, specializations, allTerms, graduationConfiguration, studentPrograms, capSizes, gownSizes, commencementSites, countries, states, financeConfiguration, graduationFees, primaryLocations,userprofileConfig, CurrentUser, roles,graduationApplication);
        }

        private async Task<List<PrimaryLocationModel>> BuildPrimaryLocationAsync(string studentId, string programCode, IEnumerable<StudentProgram2> studentPrograms, IEnumerable<string> programAllLocations,  List<StudentHomeLocation> studentHomeLocations)
        {
            try
            {
                List<PrimaryLocationModel> primaryLocations = new List<PrimaryLocationModel>();
                StudentHomeLocation activeLocation = null;
                string studentProgramLocation = string.Empty;

                if (studentPrograms != null && studentPrograms.Count() > 0)
                {
                    var studentProgram = studentPrograms.FirstOrDefault(p => p.StudentId == studentId && p.ProgramCode == programCode);
                    if (studentProgram != null)
                    {
                        studentProgramLocation = studentProgram.Location;
                    }
                }
                List<string> programLocations = programAllLocations != null ? programAllLocations.ToList<string>() : new List<string>();
                var locations = await ServiceClient.GetCachedLocationsAsync();
                //find active location from studentHomeLocations
                try
                {
                    activeLocation = studentHomeLocations != null ? studentHomeLocations.SingleOrDefault(x => x.StartDate.HasValue && x.StartDate != default(DateTime) && DateTime.Today >= x.StartDate && ((x.EndDate!=null && x.EndDate!=default(DateTime) && DateTime.Today<=x.EndDate)||(x.EndDate==null))) : null;
                    //if enddate is provided should be in rtange
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, string.Format("Could not retrieve studentHomeLocations from SPRO for person Id {0} for Date {1}, could be multiple existing", CurrentUser.PersonId, DateTime.Today));
                }
                if (activeLocation != null)
                {
                    if ((!string.IsNullOrEmpty(studentProgramLocation) && !string.Equals(activeLocation.Location, studentProgramLocation, StringComparison.OrdinalIgnoreCase))|| string.IsNullOrEmpty(studentProgramLocation))
                    {
                        primaryLocations.Add(new PrimaryLocationModel() { Code = activeLocation.Location, Description = locations != null && locations.Any() ? locations.FirstOrDefault(l => l.Code == activeLocation.Location).Description : "", IsSelected = !string.IsNullOrEmpty(studentProgramLocation) ? false : true });
                    }
                        programLocations.Remove(activeLocation.Location);
                }
                if (!string.IsNullOrEmpty(studentProgramLocation))
                {
                    primaryLocations.Add(new PrimaryLocationModel() { Code = studentProgramLocation, Description = locations != null && locations.Any() ? locations.FirstOrDefault(l => l.Code == studentProgramLocation).Description : "", IsSelected = true });
                        programLocations.Remove(studentProgramLocation);
                }

                //add unique programs into collection
                if (programLocations.Any())
                {
                    primaryLocations.AddRange(programLocations.Select(p => new PrimaryLocationModel() { Code = p, Description = locations != null && locations.Any() ? locations.FirstOrDefault(l => l.Code == p).Description : "", IsSelected = false }));
                }
                return primaryLocations;
            }
            catch (Exception ex)
            {
                Logger.Error(ex,ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<MakeAPaymentModel> MakeAGraduationPaymentAsync(GraduationPaymentModel graduationPaymentModel)
        {
            try
            {
                MakeAPaymentModel makeAPaymentModel = null;
                string invoiceId = graduationPaymentModel.InvoiceId;
                var studentId = GetEffectivePersonId();
                AccountHolder accountHolder = ServiceClient.GetAccountHolder2(studentId);
                FinanceConfiguration config = await ServiceClient.GetCachedFinanceConfiguration();
                FinanceConfiguration configurationCopy = _studentHelper.OverrideFinanceConfigurationOptions(config);

                IEnumerable<Invoice> invoices = ServiceClient.GetInvoices(new List<string>() { graduationPaymentModel.InvoiceId });
                if (invoices != null && invoices.Any())
                {
                    InvoiceDue invoicesForTerms = new InvoiceDue(graduationPaymentModel.DistributionCode, null, invoices.ToList<Invoice>());
                    // Build the make a payment term model
                    makeAPaymentModel = MakeAPaymentModel.Build(invoicesForTerms, configurationCopy, accountHolder, CurrentUser.PersonId);
                    makeAPaymentModel.SelectedPaymentMethod = graduationPaymentModel.PaymentMethodSelected;
                }
                else
                {
                    throw new Exception(string.Format("No invoice details found for the invoice # {0}", graduationPaymentModel.InvoiceId));
                }
                return makeAPaymentModel;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                throw;
            }
        }

    }
}