﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using Ellucian.Web.Student.Areas.Student.Models.StudentRequests;
using Ellucian.Web.Student.Areas.Student.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class TranscriptRequestsController : BaseStudentController
    {
        private StudentHelper _studentHelper;
        /// <summary>
        /// Creates an instance of TranscriptRequestsController
        /// </summary>
        /// <param name="adapterRegistry">adapter registry helper; injected by the framework</param>
        /// <param name="settings">settings helper; injected by the framework</param>
        /// <param name="logger">logging helper; injected by the framework</param>
        public TranscriptRequestsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger, StudentHelper studentHelper)
            : base(settings, logger)
        {
            _studentHelper = studentHelper;
        }

        /// <summary>
        /// Returns TranscriptRequests view
        /// </summary>
        /// <param name="Notify"></param>
        /// <returns></returns>
        [LinkHelp]
        [PageAuthorize("transcriptsHome")]
        public async Task<ActionResult> Index(StudentRequestNotificationType? Notify = null)
        {
            try
            {
                string emailAddress = string.Empty;
                ViewBag.Title = "Transcript Requests";
                ViewBag.Notification = Notify;
                if (Notify != null )
                {
                    var studentTask = ServiceClient.GetStudentAsync(GetEffectivePersonId());
                    var studentRequestConfigurationTask = ServiceClient.GetStudentRequestConfigurationAsync();
                    await Task.WhenAll(studentTask, studentRequestConfigurationTask);
                    try
                    {
                        emailAddress = _studentHelper.GetEmailAddress(studentTask.Result, studentRequestConfigurationTask.Result, StudentRequestType.TranscriptRequest);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }

                }
                ViewBag.EmailAddress = emailAddress;
                return View();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw ex;
            }
        }

        /// <summary>
        /// Get any existing transcript requests for the student
        /// </summary>
        /// <param name="studentId">Id of student</param>
        /// <returns>The transcript request data</returns>
        [PageAuthorize("transcriptsHome")]
        public async Task<JsonResult> GetExistingTranscriptRequestsAsync(string studentId)
        {
            if (string.IsNullOrEmpty(studentId)) studentId = GetEffectivePersonId();
            var studentTranscriptRequests = await ServiceClient.GetStudentTranscriptRequestsAsync(studentId);
            List<StudentRequest> studentRequestDtos = studentTranscriptRequests.ConvertAll(x => (StudentRequest)x);


            // Get InvoicePayments for these requests so we can determine paid status
            var invoicePayments = new List<InvoicePayment>();
            var invoiceIds = studentTranscriptRequests.Where(r => !string.IsNullOrEmpty(r.InvoiceNumber)).Select(x => x.InvoiceNumber).Distinct().ToList();
            if (invoiceIds != null && invoiceIds.Any())
            {
                invoicePayments = (await ServiceClient.QueryInvoicePaymentsAsync(new InvoiceQueryCriteria() { InvoiceIds = invoiceIds })).ToList();
            }
            var financeConfigurationTask = ServiceClient.GetCachedFinanceConfiguration();
            var transcriptAccessTask = ServiceClient.GetTranscriptRestrictions2Async(studentId);
            var countriesTask = ServiceClient.GetCachedCountriesAsync();
            var requestConfigurationTask = ServiceClient.GetStudentRequestConfigurationAsync();
            try
            {
                await Task.WhenAll(financeConfigurationTask, transcriptAccessTask, countriesTask, requestConfigurationTask);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(StudentResourceFiles.RequestsResources, "GetExistingRequestsFailureMessage"));
            }
            var financeConfiguration = financeConfigurationTask.Result;
            var transcriptAccess = transcriptAccessTask.Result;
            var countries = countriesTask.Result;
            var requestConfiguration = requestConfigurationTask.Result;
            var studentTranscriptRequestsModel = new StudentRequestsModel(studentRequestDtos, countries, transcriptAccess, invoicePayments, financeConfiguration, requestConfiguration.TranscriptRequestRequireImmediatePayment);
            return Json(studentTranscriptRequestsModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// New transcript request entry form
        /// </summary>
        /// <param name="Notify"></param>
        /// <returns>A view for entering a transcript request</returns>
        [LinkHelp(HelpIdOverride = "TranscriptRequest")]
        [PageAuthorize("transcriptRequest")]
        public ActionResult TranscriptRequest()
        {

            try
            {
                ViewBag.Title = "Transcript Requests";
                return View();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw ex;
            }
        }

        /// <summary>
        /// ajax get request to retrieve the transcript request data (dropdown values and other initial setup values) 
        /// <returns>TranscriptRequestViewModel</returns>
        [PageAuthorize("transcriptRequest")]
        public async Task<JsonResult> GetTranscriptRequestsInfoAsync()
        {
            try
            {

                var countriesTask = ServiceClient.GetCachedCountriesAsync();
                var statesTask = ServiceClient.GetCachedStatesAsync();
                var holdRequestTypesTask = ServiceClient.GetHoldRequestTypesAsync();
                var studentProgramTask = ServiceClient.GetStudentPrograms2Async(GetEffectivePersonId(), false);
                var transcriptGroupingsTask = ServiceClient.GetSelectableTranscriptGroupingsAsync();
                var programTask = ServiceClient.GetCachedProgramsAsync();
                var studentRequestsConfigurationTask = ServiceClient.GetStudentRequestConfigurationAsync();
                try
                {
                    await Task.WhenAll(countriesTask, statesTask, holdRequestTypesTask, studentProgramTask, transcriptGroupingsTask, programTask,studentRequestsConfigurationTask);
                }
                catch (Exception ex)
                {
                    var exceptionMessage = ex.Message;
                    if (ex is AggregateException)
                    {
                        AggregateException ae = ex as AggregateException;
                        exceptionMessage = ae.LogAggregateExceptions(Logger);
                    }
                    throw;
                }
                var countries = countriesTask.Result;
                var states = statesTask.Result;
                var holdRequestTypes = holdRequestTypesTask.Result;
                var studentPrograms = studentProgramTask.Result;
                var transcriptGroupings = transcriptGroupingsTask.Result;
                var programs = programTask.Result;
                var studentRequestsConfiguration = studentRequestsConfigurationTask.Result;
               

                TranscriptRequestsViewModel transcriptRequest = new TranscriptRequestsViewModel(countries, states, holdRequestTypes);
                transcriptRequest.UpdateTranscriptTypes(transcriptGroupings, studentPrograms, programs);
                transcriptRequest.RequireImmediatePayment = studentRequestsConfiguration.TranscriptRequestRequireImmediatePayment;
                return Json(transcriptRequest, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(StudentResourceFiles.RequestsResources, "GetInformationFailureMessage"));
            }

        }

        /// <summary>
        /// Creates a new transcript request. Form Post with transcript request fields 
        /// </summary>
        /// <param name="newTranscriptRequest">The new student transcript request</param>
        /// <returns></returns>
        [HttpPost]
        [PageAuthorize("transcriptRequest")]
        public async Task<ActionResult> CreateTranscriptRequestAsync([ModelBinder(typeof(StudentRequestJsonBinder))]  TranscriptRequestModel newTranscriptRequest)
        {
            if (newTranscriptRequest==null)
            {
                Logger.Error("NewTranscriptRequest is null or empty, cannot complete transcript request");
                throw new ArgumentNullException("newTranscriptRequest");
            }
            try
            {

                //Build StudentTranscriptRequestDto
                StudentTranscriptRequest requestDto = new StudentTranscriptRequest();
                requestDto.StudentId = GetEffectivePersonId();
                requestDto.RecipientName = newTranscriptRequest.Recipient;
                requestDto.MailToAddressLines = newTranscriptRequest.MailToAddressLines;
                requestDto.HoldRequest = newTranscriptRequest.HoldRequestCode;
                requestDto.MailToCity = newTranscriptRequest.MailToCity;
                requestDto.MailToCountry = newTranscriptRequest.MailToCountry;
                requestDto.MailToPostalCode = newTranscriptRequest.MailToPostalCode;
                requestDto.MailToState = newTranscriptRequest.MailToState;
                requestDto.NumberOfCopies = newTranscriptRequest.NumberOfCopies;
                requestDto.Comments = newTranscriptRequest.Comments;
                requestDto.TranscriptGrouping = newTranscriptRequest.TranscriptGrouping;
                var response = await ServiceClient.AddStudentTranscriptRequestAsync(requestDto);
                if (response != null && !string.IsNullOrEmpty(response.Id) &&  newTranscriptRequest.RequireImmediatePayment && !string.IsNullOrEmpty(response.InvoiceNumber))
                {
                    //redirect to payment page
                    return RedirectToAction("TranscriptRequestProceedToPayment", "TranscriptRequests", new
                    {
                        area = "Student",
                        RequestId = response.Id
                    });
                }
                return RedirectToAction("Index", "TranscriptRequests", new
                {
                    area = "Student",
                    Notify =StudentRequestNotificationType.TranscriptRequestSubmitSuccess
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return RedirectToAction("Index", "TranscriptRequests", new
                {
                    area = "Student",
                    Notify = StudentRequestNotificationType.TranscriptRequestSubmitFailure
                });
            }
           
        }

        /// <summary>
        /// this is a get request.
        /// to return proceed to payment view that will have payment methods to select from.
        /// this will take request id for which the payment is yet to be made.
        /// </summary>
        /// <param name="RequestId">RequestId</param>
        /// <returns></returns>
        [ActionName("TranscriptRequestProceedToPayment")]
        [LinkHelp(HelpIdOverride = "TranscriptRequestPayment")]
        [PageAuthorize("transcriptRequest", "transcriptsHome")]
        public async Task<ActionResult> TranscriptRequestProceedToPaymentAsync(string RequestId = null)
        {
           ViewBag.Title = "Transcript Requests Payment";

           if (RequestId == null)
           {
               throw new ArgumentNullException("RequestId");
           }
           try
           {

               StudentTranscriptRequest transcript = await ServiceClient.GetStudentTranscriptRequestAsync(RequestId);

               if (transcript == null)
               {
                   throw new Exception("tanscript is null");
               }
               var studentId = GetEffectivePersonId();
               if (transcript.StudentId != studentId)
               {
                   var message = string.Format("Request Id {0} does not belong to user {1}", RequestId, studentId);
                   throw new Exception(message);
               }
               // We should ONLY allow them to make a payment on a request that has an invoice number
               // If we have gotten here and the request doesn't have an invoice it is an error.
               if (string.IsNullOrEmpty(transcript.InvoiceNumber))
               {
                   var message = string.Format("Request Id {0} does not have associated invoice number", RequestId);
                   throw new Exception(message);
               }
               // Get the invoice so we can get the amount of the invoice
               var invoicePayment = (await ServiceClient.QueryInvoicePaymentsAsync(new InvoiceQueryCriteria() { InvoiceIds = new List<string>() { transcript.InvoiceNumber } })).FirstOrDefault();
               if (invoicePayment == null)
               {
                   var message = string.Format("Unable to retrieve request invoice information", RequestId);
                   throw new Exception(message);
               }
               if ((invoicePayment.Amount - invoicePayment.AmountPaid) <= 0)
               {
                   //redirect to payment page because there is nothing to pay here
                   return RedirectToAction("Index", "TranscriptRequests", new
                   {
                       area = "Student",
                       //Notify = StudentRequestNotificationType.TranscriptRequestSubmitSuccess
                   });
               }

               
               int numberOfCopies = transcript != null && transcript.NumberOfCopies.HasValue ? transcript.NumberOfCopies.Value : 1;
               StudentRequestPaymentModel paymentModel = new StudentRequestPaymentModel(transcript.InvoiceNumber, numberOfCopies);

               // Get the request fee so we have the distribution code and the payment methods from the finance configuration
               var financeConfigTask = ServiceClient.GetCachedFinanceConfiguration();
               var feeDetailsTask = ServiceClient.GetStudentRequestFeeAsync(studentId, transcript.Id);
               await Task.WhenAll(financeConfigTask, feeDetailsTask);
               FinanceConfiguration financeConfiguration = financeConfigTask.Result;
               StudentRequestFee feeDetails = feeDetailsTask.Result;
               if (feeDetails == null)
               {
                   throw new Exception("Fee details retrieved for transcript request is null");
               }

               paymentModel.SetPaymentDetails(financeConfiguration.PaymentMethods, feeDetails, invoicePayment);
               return View("TranscriptRequestsProceedToPayment", paymentModel);
           }
           catch (Exception ex)
           {
               Logger.Error(ex.Message, ex);
               return RedirectToAction("Index", "TranscriptRequests", new
               {
                   area = "Student",
                   Notify = StudentRequestNotificationType.TranscriptRequestPaymentFailure
               });
           }
        }

        /// <summary>
        /// ajax post call to pass payment model to create Make A Payment model which finance Payments controller payment review action needs.
        /// </summary>
        /// <param name="paymentModelJson"></param>
        /// <returns></returns>
        [HttpPost]
        [PageAuthorize("transcriptRequest", "transcriptsHome")]
        public async Task<JsonResult> CreateTranscriptRequestPaymentAsync(string  paymentModelJson)
        {
            if (paymentModelJson == null)
            {
                Logger.Error("transcript request payment model is null");
                throw new ArgumentNullException("paymentModelJson");
            }
            var studentRequestPaymentModel = JsonConvert.DeserializeObject<StudentRequestPaymentModel>(paymentModelJson);
            if (studentRequestPaymentModel == null)
            {
                Logger.Error("transcript request payment model was not deserialized");
                throw new Exception("transcript request payment model desrialized exception");
            }
            try
            {
                MakeAPaymentModel makeAPaymentModel = await _studentHelper.BuildMakeAPaymentModelAsync(studentRequestPaymentModel, GetEffectivePersonId(), ServiceClient);
                Response.StatusCode = (int)HttpStatusCode.Accepted;
                return Json(new { MakeAPaymentModel = makeAPaymentModel }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }

        }
    }
}












































