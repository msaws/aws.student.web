﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Student.Models.Grades;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using slf4net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class GradesController : BaseStudentController
    {

        /// <summary>
        /// Creates a new GradesController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public GradesController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        /// <summary>
        /// Index page for student grades
        /// </summary>
        /// <returns>A View of the grade information for a student</returns>
        [LinkHelp]
        [PageAuthorize("gradesHome")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(StudentResourceFiles.StudentGradesResources, "PageDescription");
            ViewBag.CssAfterOverride = new List<string>();
            return View();
        }
        

        /// <summary>
        /// Get the student grade data
        /// </summary>
        /// <param name="studentId">Id of the student for which grades are requested</param>
        /// <returns>A JSON of the grade data</returns>
        [PageAuthorize("gradesHome", "advisees")]
        public async Task<JsonResult> GetStudentGradeInformationAsync(string studentId)
        {
            if (string.IsNullOrEmpty(studentId)) studentId = GetEffectivePersonId();
            var currentUser = CurrentUser;

            var studentAcademicHistoryTask = ServiceClient.GetAcademicHistory3Async(studentId, false, false);
            var allTermsTask = ServiceClient.GetCachedTermsAsync();
            var gradeConfigurationTask = ServiceClient.GetFacultyGradingConfigurationAsync();
            var gradesTask = ServiceClient.GetGradesAsync();

            try
            {
                await Task.WhenAll(studentAcademicHistoryTask, allTermsTask, gradeConfigurationTask, gradesTask );
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                Logger.Error(ex.ToString());
                throw new Exception();

            }
            var academicHistory = studentAcademicHistoryTask.Result;
            var allTerms = allTermsTask.Result;
            var configuration = gradeConfigurationTask.Result;
            var grades = gradesTask.Result;

            var studentGradesModel = new StudentGradesModel(academicHistory, allTerms, configuration, grades);
            return Json(studentGradesModel, JsonRequestBehavior.AllowGet);
        }


        

    }
}