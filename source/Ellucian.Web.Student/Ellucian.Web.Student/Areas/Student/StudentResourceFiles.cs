﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
namespace Ellucian.Web.Student.Areas.Student
{
    public static class StudentResourceFiles
    {
        public static readonly string GraduationResources = "Graduation";
        public static readonly string RequestsResources = "Requests";
        public static readonly string TranscriptRequestsResources = "TranscriptRequests";
        public static readonly string StudentGradesResources = "StudentGrades";
    }
}