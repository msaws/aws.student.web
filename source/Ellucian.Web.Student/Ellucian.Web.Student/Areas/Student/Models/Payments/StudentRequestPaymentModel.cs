﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.Payments
{
    public class StudentRequestPaymentModel
    {
        //related to payments
        public bool RequireImmediatePayment { get; set; }
        public decimal AmountDue { get; set; }
        public string PaymentDistributionCode { get; set; }
        public string InvoiceNumber { get; set; }
        public IEnumerable<AvailablePaymentMethod> PaymentMethods { get; set; }
        public int NumberOfCopies { get; set; }
        public AvailablePaymentMethod PaymentMethodSelected {get;set;}
    
        public StudentRequestPaymentModel(string invoiceNumber, int NumberOfCopies = 1)
            : this()
        {
            this.NumberOfCopies = NumberOfCopies;
            this.InvoiceNumber = invoiceNumber;
        }

        public StudentRequestPaymentModel()
        {
            AmountDue = 0;
            PaymentMethods = new List<AvailablePaymentMethod>();

        }

        public void SetPaymentDetails(List<AvailablePaymentMethod> paymentMethods, StudentRequestFee feeDetails, InvoicePayment invoicePayment)
        {
            if (paymentMethods != null)
            {
                PaymentMethods = paymentMethods;
            }
            AmountDue = (invoicePayment.Amount - invoicePayment.AmountPaid) > 0 ? (invoicePayment.Amount - invoicePayment.AmountPaid) : 0;
            if (feeDetails != null)
            {
                PaymentDistributionCode = !string.IsNullOrEmpty(feeDetails.PaymentDistributionCode) ? feeDetails.PaymentDistributionCode : string.Empty;
            }
        }
    }
}