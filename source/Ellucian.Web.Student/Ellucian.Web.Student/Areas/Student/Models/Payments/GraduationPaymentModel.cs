﻿using Ellucian.Colleague.Dtos.Finance.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.Payments
{
    public class GraduationPaymentModel
    {
        //related to payments
        public List<AvailablePaymentMethod> PaymentMethods { get; set; }
        public bool RequireImmediatePayment { get; set; }
        public decimal? GraduationFeesAmount { get; set; }
        public string DistributionCode { get; set; }
        public string InvoiceId { get; set; }
        public string ProgramCode { get; set; }
        public AvailablePaymentMethod PaymentMethodSelected {get;set;}
        public GraduationPaymentModel()
        { }
    }
}