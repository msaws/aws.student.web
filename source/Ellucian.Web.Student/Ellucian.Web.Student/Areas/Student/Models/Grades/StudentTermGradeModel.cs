﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Areas.Student.Models.Grades
{
    public class StudentTermGradeModel
    {
        /// <summary>
        /// Student Grade Models associated to this term
        /// </summary>
        public List<StudentGradeModel> StudentGrades { get; set; }

        /// <summary>
        /// Name of the term
        /// </summary>
        public string TermName;
        /// <summary>
        /// Year of the term
        /// </summary>
        public int TermYear { get; set; }
        /// <summary>
        /// Order of the term within its year
        /// </summary>
        public int TermOrder { get; set; }

        /// <summary>
        /// Gets or sets the completed GPA of this term.
        /// </summary>
        public decimal? CompletedGpa { get; set; }

        /// <summary>
        /// Gets a formatted string which prints the GPA and a label.
        /// </summary>
        public string CompletedGpaDisplay
        {
            get
            {
                return (CompletedGpa == null) ? string.Empty : GlobalResources.GetString(GlobalResourceFiles.StudentResources, "TermGPALabel") + " " + ( CreditsFormatter.FormattedGpaString((decimal)CompletedGpa));
            }
        }

        public StudentTermGradeModel()
        {
            StudentGrades = new List<StudentGradeModel>();
        }
    }
}