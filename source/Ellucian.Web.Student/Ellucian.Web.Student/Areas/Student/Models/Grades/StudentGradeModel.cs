﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Student.Models.Grades
{
    public class StudentGradeModel
    {
        // Display information
        public List<string> FormattedCourseNameDisplay { get; private set; }
        public string CourseNameSort { get; private set; }
        public string Title { get; private set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string FinalGrade { get; set; }
        public string FinalGradeExpirationDate { get; set; }
        public string CreditsCeus { get; set; }
        public string Midterm1Grade { get; set; }
        public string Midterm2Grade { get; set; }
        public string Midterm3Grade { get; set; }
        public string Midterm4Grade { get; set; }
        public string Midterm5Grade { get; set; }
        public string Midterm6Grade { get; set; }
        /// <summary>
        /// Gets or sets a formatted date display string for this section.
        /// </summary>
        public string DatesDisplay
        {
            get
            {
                string dateDisplay = string.Empty;
                if (StartDate.HasValue)
                {
                    if (EndDate.HasValue)
                    {
                        dateDisplay = StartDate.Value.ToShortDateString() + " - " + EndDate.Value.ToShortDateString();
                    }
                    else
                    {
                        dateDisplay = StartDate.Value.ToShortDateString();
                    }
                }
                return dateDisplay;
            }
        }

        public List<string> FinalGradeDisplay
        {
            get
            {
                if (string.IsNullOrEmpty(FinalGradeExpirationDate))
                {
                    return new List<String>() { FinalGrade };
                }
                else if (!string.IsNullOrEmpty(FinalGrade) && !string.IsNullOrEmpty(FinalGradeExpirationDate))
                {
                    return new List<String>() { FinalGrade, GlobalResources.GetString(GlobalResourceFiles.StudentResources, "GradeExpiresOn") + FinalGradeExpirationDate };
                }
                else
                {
                    return new List<String>();
                }
            }
        }

        public StudentGradeModel(Colleague.Dtos.Student.AcademicCredit2 academicCredit, IEnumerable<Colleague.Dtos.Student.Grade> grades)
        {
            if (academicCredit != null)
            {
                this.CourseNameSort = academicCredit.CourseName;
                this.FormattedCourseNameDisplay = new List<string>();
                if (!string.IsNullOrEmpty(academicCredit.SectionNumber))
                {
                    this.CourseNameSort += GlobalResources.GetString(GlobalResourceFiles.StudentResources, "UiCourseDelimiter") + academicCredit.SectionNumber;
                }
                Title = academicCredit.Title;
                StartDate = academicCredit.StartDate;
                EndDate = academicCredit.EndDate;

                FormattedCourseNameDisplay.Add(this.CourseNameSort);
                FormattedCourseNameDisplay.Add(DatesDisplay);

                var grade = grades.Where(g => g.Id == academicCredit.VerifiedGradeId).FirstOrDefault();
                FinalGrade = grade != null ? grade.LetterGrade : string.Empty;
                FinalGradeExpirationDate = !string.IsNullOrEmpty(academicCredit.VerifiedGradeId) && academicCredit.FinalGradeExpirationDate.HasValue ? academicCredit.FinalGradeExpirationDate.Value.ToShortDateString() : string.Empty;
                CreditsCeus = CreditsFormatter.FormattedCreditsCeusString(academicCredit.Credit, academicCredit.ContinuingEducationUnits);
                
                if (academicCredit.MidTermGrades != null)
                {
                    foreach (var midtermGrade in academicCredit.MidTermGrades)
                    {
                        if (midtermGrade != null)
                        {
                            var midgrade = grades.Where(g => g.Id == midtermGrade.GradeId).FirstOrDefault();
                            if (midgrade != null)
                            {
                                switch (midtermGrade.Position)
                                {
                                    case 1:
                                        Midterm1Grade = midgrade.LetterGrade;
                                        break;
                                    case 2:
                                        Midterm2Grade = midgrade.LetterGrade;
                                        break;
                                    case 3:
                                        Midterm3Grade = midgrade.LetterGrade;
                                        break;
                                    case 4:
                                        Midterm4Grade = midgrade.LetterGrade;
                                        break;
                                    case 5:
                                        Midterm5Grade = midgrade.LetterGrade;
                                        break;
                                    case 6:
                                        Midterm6Grade = midgrade.LetterGrade;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                    }
                }

            }


        }
        public StudentGradeModel()
        {
            FormattedCourseNameDisplay = new List<string>();
        }

    }


}