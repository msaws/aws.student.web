﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Student.Models.Grades
{
    public class StudentGradesModel
    {
        /// <summary>
        /// Each object is a term with a list of student grades.
        /// </summary>
        public List<StudentTermGradeModel> StudentTermGrades { get; set; }
        /// <summary>
        /// Number of midterm grades to show
        /// </summary>
        public int NumberOfMidtermGradesToShow { get; set; }
        /// <summary>
        /// Contains any notification messages that should be shown in notification center - typically an indication that student is restricted from seeing final grades.
        /// </summary>
        public List<Notification> Notifications { get; set; }

        public StudentGradesModel(AcademicHistory3 academicHistory, IEnumerable<Term> allTerms, FacultyGradingConfiguration configuration, IEnumerable<Grade> grades)
        {
            NumberOfMidtermGradesToShow = configuration != null ? configuration.NumberOfMidtermGrades : 0;
            StudentTermGrades = new List<StudentTermGradeModel>();
            Notifications = new List<Notification>();
            if (academicHistory != null)
            {

                if (academicHistory.GradeRestriction != null && academicHistory.GradeRestriction.IsRestricted && academicHistory.GradeRestriction.Reasons != null)
                {
                    foreach (string reason in academicHistory.GradeRestriction.Reasons)
                    {
                        if (!string.IsNullOrEmpty(reason))
                        {
                            Notifications.Add(new Notification(reason, NotificationType.Warning, false));
                        }
                    }
                }
                
                // First process all ther academic credits grouped into an academic term
                foreach (var academicTerm in academicHistory.AcademicTerms)
                {
                    var termModel = new StudentTermGradeModel();
                    
                    if (academicTerm.AcademicCredits != null && academicTerm.AcademicCredits.Any())
                    {
                        foreach (var academicCredit in academicTerm.AcademicCredits)
                        {
                            // Right now we are following the WebAdvisor paradigm and only displaying academic credits that are New, Add or Equivalancies

                            if ((academicCredit.Status == "New" || academicCredit.Status == "Add" 
                                || academicCredit.Status == "TransferOrNonCourse") && !string.IsNullOrEmpty(academicCredit.SectionId))
                            {
                                termModel.StudentGrades.Add(new StudentGradeModel(academicCredit, grades));
                            }    
                        }
                    }

                    // If there is a term with no items to show disregard it.
                    if (termModel.StudentGrades.Count() > 0)
                    {
                        // Sort the resulting list by course name display.
                        termModel.StudentGrades = termModel.StudentGrades.OrderBy(g => g.CourseNameSort).ToList();
                        // Fill out other term properties
                        if (academicHistory.GradeRestriction != null && !academicHistory.GradeRestriction.IsRestricted)
                        {
                            termModel.CompletedGpa = academicTerm.GradePointAverage;
                        }
                        
                        // There shouldn't be a term without a termId (or valid term) in this list. They should be in the non-term separate list below.  If there is we are ignoring it.
                        if (!string.IsNullOrEmpty(academicTerm.TermId))
                        {
                            var term = allTerms.Where(t => t.Code == academicTerm.TermId).FirstOrDefault();
                            if (term != null)
                            {
                                termModel.TermName = term.Description;
                                termModel.TermYear = term.ReportingYear;
                                termModel.TermOrder = term.Sequence;
                            }
                        }

                        StudentTermGrades.Add(termModel);
                    }

                }
                // Sort the most recent terms first
                StudentTermGrades = StudentTermGrades.OrderByDescending(t => t.TermYear).ThenByDescending(st => st.TermOrder).ToList();

                // Next: Add any Non-term  academic credits to the bottom of the list.
                if (academicHistory.NonTermAcademicCredits != null && academicHistory.NonTermAcademicCredits.Any())
                {
                    var termModel = new StudentTermGradeModel();
                    
                    foreach (var academicCredit in academicHistory.NonTermAcademicCredits)
                    {
                        // Right now we are following the WebAdvisor paradigm and only displaying academic credits that are New, Add or Equivalancies
                        if ((academicCredit.Status.ToUpper() == "NEW" || academicCredit.Status.ToUpper() == "ADD"
                            || academicCredit.Status.ToUpper() == "TRANSFERORNONCOURSE") && !string.IsNullOrEmpty(academicCredit.SectionId))
                        {
                            termModel.StudentGrades.Add(new StudentGradeModel(academicCredit, grades));
                        }
                    }
                    if (termModel.StudentGrades.Count() > 0)
                    {
                        termModel.TermName = GlobalResources.GetString(GlobalResourceFiles.StudentResources, "NonTermSectionHeaderComplete");
                        StudentTermGrades.Add(termModel);
                        // sort the nonterm grades by start date and then by name
                        termModel.StudentGrades = termModel.StudentGrades.OrderBy(s => s.StartDate).ThenBy(sg => sg.CourseNameSort).ToList();
                    }

                }
            }
        }

        public StudentGradesModel()
        {
            StudentTermGrades = new List<StudentTermGradeModel>();
            Notifications = new List<Notification>();
        }
    }
}