﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Globalization;
using System.Resources;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
namespace Ellucian.Web.Student.Areas.Student.Models.Graduation
{
     public class GraduationModel
     {

          /// <summary>
          /// The data that is student specific
          /// </summary>
          public string FullName { get; set; }
          public List<string> PreferredAddress { get; set; }
          public string NumberofGuestsLabel { get; set; }
          /// <summary>
          /// The values that fill the drop downs
          /// </summary>
          public List<ProgramModel> Programs { get; set; }
          public List<GraduationTermModel> GraduationTerms { get; set; }
          public List<CapSize> CapSizes { get; set; }
          public List<GownSize> GownSizes { get; set; }
          public List<CommencementSite> CommencementSites { get; set; }
          public List<Country> Countries { get; set; }
          public List<State> States { get; set; }
          public List<MilitaryStatusModel> MilitaryStatuses { get; set; }
          public List<PrimaryLocationModel> PrimaryLocations { get; set; }



          public string PhoneticSpellingLink { get; set; }
          public string CapAndGownLink { get; set; }
          public string CapAndGownSizingLink { get; set; }

          /// <summary>
          /// Graduation term description for the existing application 
          /// This is needed to because the term may no longer be in the list of terms in GAWP
          /// </summary>
          public string GraduationTermDisplay { get; set; }

          /// <summary>
          /// The following are the  show-question/require question boolean pairs
          /// </summary>
          public bool ShowDiplomaName { get; set; }
          public bool RequireDiplomaName { get; set; }

          public bool ShowPhoneticSpelling { get; set; }
          public bool RequirePhoneticSpelling { get; set; }

          public bool ShowAttendCommencement { get; set; }
          public bool RequireAttendCommencement { get; set; }

          public bool ShowNumberGuests { get; set; }
          public bool RequireNumberGuests { get; set; }

          public bool ShowNameInProgram { get; set; }
          public bool RequireNameInProgram { get; set; }

          public bool ShowHometown { get; set; }
          public bool RequireHometown { get; set; }

          public bool ShowCapSize { get; set; }
          public bool RequireCapSize { get; set; }

          public bool ShowGownSize { get; set; }
          public bool RequireGownSize { get; set; }

          public bool ShowCommencementLocation { get; set; }
          public bool RequireCommencementLocation { get; set; }

          public bool ShowPickUpDiploma { get; set; }
          public bool RequirePickUpDiploma { get; set; }

          public bool ShowMilitaryStatus { get; set; }
          public bool RequireMilitaryStatus { get; set; }

          public bool ShowSpecialAccommodations { get; set; }
          public bool RequireSpecialAccommodations { get; set; }

          public bool ShowPrimaryLocation { get; set; }
          public bool RequirePrimaryLocation { get; set; }

          public bool OverrideCapAndGownDisplay { get; set; }

          /// <summary>
          /// The maximum number of guests a student is allowed to bring to commencement
          /// </summary>
          public int? MaximumCommencementGuests { get; set; }

          /// <summary>
          /// This is to control whether to show or hide New preferred address question .
          /// </summary>
          public bool ShowNewPreferredAddressQuestion { get; set; }

          /// <summary>
          /// Has the Academic Credentials record been updated with the information from the Graduates file in Colleague
          /// The importance being that once the record has been copied to Academic Credentials it should no longer be 
          /// changeable.
          /// </summary>
          public bool IsAcademicCredentialsUpdated { get; set; }

         /// <summary>
         /// this indicates if diploma address is same as preferred address.
         /// </summary>
          public bool IsDiplomaAddressSameAsPreferred { get; set; }

          /// <summary>
          /// Specific Graduation Application being viewed or edited. (Not included when applying for a new program)
          /// </summary>
          public GraduationApplication GraduationApplication { get; set; }

          public GraduationPaymentModel GraduationPaymentModel { get; set; }

          /// <summary>
          /// Holds all of the information needed to fill in or view a graduation application when displayed to a student.
          /// </summary>
          public GraduationModel(Ellucian.Colleague.Dtos.Student.Student student,
                                  Program graduatingProgram,
                                  IEnumerable<Major> majors,
                                  IEnumerable<Minor> minors,
                                  IEnumerable<Specialization> specialization,
                                  IEnumerable<Term> allTerms,
                                  Ellucian.Colleague.Dtos.Student.GraduationConfiguration configuration,
                                  IEnumerable<StudentProgram2> studentPrograms,
                                  IEnumerable<CapSize> capSizes,
                                  IEnumerable<GownSize> gownSizes,
                                  IEnumerable<CommencementSite> commencementSites,
                                  IEnumerable<Country> countries,
                                  IEnumerable<State> states,
                                  FinanceConfiguration financeConfiguration,
                                  GraduationApplicationFee graduationFees,
                                  List<PrimaryLocationModel> primaryLocations,
                                  UserProfileConfiguration userProfileConfig,
                                  ICurrentUser currentUser,
                                  IEnumerable<Role> roles,
                                  GraduationApplication application = null

                               )
          {
               if (student == null)
                    throw new ArgumentNullException("student", "You must supply a student.");
               if (studentPrograms == null)
                    throw new ArgumentNullException("studentPrograms", "You must supply student programs.");
               if (graduatingProgram == null)
                    throw new ArgumentNullException("programs", "You must supply at least one program to initiate a graduation application.");
               if (majors == null || !majors.Any())
                    throw new ArgumentNullException("majors", "You must supply at least one major");
               if (configuration == null)
                    throw new ArgumentNullException("configuration", "You must supply some configuration");
               if (countries == null || !countries.Any())
                    throw new ArgumentNullException("majors", "You must supply at least one country");
               if (states == null || !states.Any())
                    throw new ArgumentNullException("configuration", "You must supply atleast one state");
               if (configuration != null && configuration.RequireImmediatePayment && graduationFees == null)
               {
                    throw new ArgumentNullException("graduationFees", "Graduation fees must be supplied");
               }
               if (configuration != null && configuration.RequireImmediatePayment && financeConfiguration == null)
               {
                    throw new ArgumentNullException("financeConfiguration", "Finance configuration must be supplied");
               }
               if (graduationFees != null && (graduationFees.Amount != null && graduationFees.Amount.Value > 0) && configuration.RequireImmediatePayment && financeConfiguration != null && (financeConfiguration.PaymentMethods == null || !financeConfiguration.PaymentMethods.Any()))//amount > 0 and configuration has immediatepayment set to true
               {
                    throw new ArgumentNullException("payment methods", "You must supply atleast one payment method");
               }

               /// <summary>
               /// Get the graduation terms
               /// </summary>
               this.GraduationTerms = new List<GraduationTermModel>();
               if (configuration.GraduationTerms != null && configuration.GraduationTerms.Count > 0)
               {
                    foreach (var term in configuration.GraduationTerms)
                    {
                         if (term != null)
                         {
                              var addTerm = allTerms.Where(p => p.Code == term).FirstOrDefault();
                              if (addTerm != null)
                              {
                                  this.GraduationTerms.Add(new GraduationTermModel() { Code = addTerm.Code, Description = addTerm.Description });
                              }
                              
                         }
                    }
               }
               if (application != null)
               {
                    this.GraduationApplication = application;
                    if ((configuration.GraduationTerms == null || (configuration.GraduationTerms != null && configuration.GraduationTerms.Count() == 0)) || (configuration.GraduationTerms != null && !configuration.GraduationTerms.Contains(application.GraduationTerm)))
                    {
                         this.GraduationApplication.DiplomaName = HttpUtility.HtmlEncode(application.DiplomaName);
                         this.GraduationApplication.PhoneticSpellingOfName = HttpUtility.HtmlEncode(application.PhoneticSpellingOfName);
                         this.GraduationApplication.Hometown = HttpUtility.HtmlEncode(application.Hometown);
                         this.GraduationApplication.SpecialAccommodations = HttpUtility.HtmlEncode(application.SpecialAccommodations);
                         this.GraduationApplication.MailDiplomaToCity = HttpUtility.HtmlEncode(application.MailDiplomaToCity);
                         this.GraduationApplication.MailDiplomaToPostalCode = HttpUtility.HtmlEncode(application.MailDiplomaToPostalCode);
                         this.GraduationApplication.MailDiplomaToAddressLines = application.MailDiplomaToAddressLines.Select(addr => HttpUtility.HtmlEncode(addr)).ToList<string>();
                    }
                    if (!string.IsNullOrEmpty(application.GraduationTerm))
                    {
                         this.GraduationTermDisplay = allTerms.Where(p => p.Code == application.GraduationTerm).FirstOrDefault().Description;
                    }
                    else
                    {
                         this.GraduationTermDisplay = string.Empty;
                    }

                    this.IsAcademicCredentialsUpdated = application.AcadCredentialsUpdated;
                    this.IsDiplomaAddressSameAsPreferred = application.IsDiplomaAddressSameAsPreferred;
               }
               else
               {
                   this.IsAcademicCredentialsUpdated = false;
               }

               /// <summary>
               /// The user's full name
               /// </summary>
               this.FullName = NameHelper.PersonDisplayName(student, NameFormats.FirstMiddleLast);

               /// <summary>
               /// The user's preferred address
               /// </summary>
               this.PreferredAddress = (student.PreferredAddress != null) ? student.PreferredAddress.ToList<string>() : new List<string>();

               /// <summary>
               /// The maximum number of guests allowed
               /// </summary>
               this.MaximumCommencementGuests = configuration.MaximumCommencementGuests;

               /// <summary>
               /// The non person specific information needed to fill the dropdowns
               /// </summary>

               this.CapSizes = capSizes.ToList();
               this.GownSizes = gownSizes.ToList();
               this.CommencementSites = commencementSites.ToList();
               this.PrimaryLocations = primaryLocations.ToList();
               // Filter the countries in the validation table to only those that have IsNotInUse false and then sort them by description
               this.Countries = countries.Where(ct => !ct.IsNotInUse).OrderBy(c => c.Description).ToList();
               this.States = states.OrderBy(c => c.Description).ToList();
               // Populate the Military Status model with a code and a description (Needed because the codes don't have spaces)
               this.MilitaryStatuses = new List<MilitaryStatusModel>();
               this.MilitaryStatuses.Add(new MilitaryStatusModel() { Code = (int)GraduateMilitaryStatus.ActiveMilitary, Description = "Active Military" });
               this.MilitaryStatuses.Add(new MilitaryStatusModel() { Code = (int)GraduateMilitaryStatus.Veteran, Description = "Veteran" });
               this.MilitaryStatuses.Add(new MilitaryStatusModel() { Code = (int)GraduateMilitaryStatus.NotApplicable, Description = "Not Applicable" });

               this.NumberofGuestsLabel = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "NumGuest");
               try
               {
                   if (string.IsNullOrEmpty(NumberofGuestsLabel)) { NumberofGuestsLabel = "Max {0}"; }
                   this.NumberofGuestsLabel = string.Format(this.NumberofGuestsLabel, this.MaximumCommencementGuests);
               }
               catch (Exception) {  }
               /// <summary>
               /// The student's program(s)
               /// </summary>


               var Majors = graduatingProgram.Majors;
               var Minors = graduatingProgram.Minors;
               var Specializations = graduatingProgram.Specializations;
               var Ccds = graduatingProgram.Ccds;

               // Find the graduating program in the student's list of programs to determine if ther are any additional requirements etc. 
               var studentProgram = studentPrograms.Where(p => p.ProgramCode == graduatingProgram.Code).FirstOrDefault();

               if (studentProgram != null && studentProgram.AdditionalRequirements != null)
               {
                    /// <summary>
                    /// Getting any of the student's non program majors/minors/ect
                    /// </summary>
                    foreach (var req in studentProgram.AdditionalRequirements)
                    {
                         // Additional Requirements frequently have an associated major, minor, specialization, or certificate,
                         // so add those to the program summary (user doesn't care that these are "additions", only that the extra is listed
                         if (req.Type == AwardType.Major)
                         {
                              Majors = Majors.Union(new List<string>() { req.AwardName });
                         }
                         if (req.Type == AwardType.Minor)
                         {
                              Minors = Minors.Union(new List<string>() { req.AwardName });
                         }
                         if (req.Type == AwardType.Specialization)
                         {
                              Specializations = Specializations.Union(new List<string>() { req.AwardName });
                         }
                         if (req.Type == AwardType.Ccd)
                         {
                              Ccds = Ccds.Union(new List<string>() { req.AwardName });
                         }
                    }
               }
               this.Programs = new List<ProgramModel>();
               this.Programs.Add(new ProgramModel()
               {
                    Code = graduatingProgram.Code,
                    Title = graduatingProgram.Title,
                    Degree = graduatingProgram.Degree,
                    Majors = Majors != null ? Majors.ToList<string>() : new List<string>(),
                    Minors = Minors != null ? Minors.ToList<string>() : new List<string>(),
                    Specializations = Specializations != null ? Specializations.ToList<string>() : new List<string>(),
                    Ccds = Ccds != null ? Ccds.ToList<string>() : new List<string>()
               });


               /// <summary>
               /// Throw an error if the student doesn't have any programs
               /// </summary>

               if (!this.Programs.Any()) throw new ArgumentNullException("Programs", "We couldn't find your programs");

               this.OverrideCapAndGownDisplay = configuration.OverrideCapAndGownDisplay;

               char[] charSeparators = new char[] { ' ' };
               this.CapAndGownLink = configuration.CapAndGownLink;
               this.CapAndGownSizingLink = configuration.CapAndGownSizingLink;
               this.PhoneticSpellingLink = configuration.PhoneticSpellingLink;

               this.ShowNewPreferredAddressQuestion = false;
               /// <summary>
               /// Determine what questions are shown and required
               /// Only questions that are shown can be required
               /// </summary>
               List<GraduationQuestion> questions = configuration.ApplicationQuestions;
               foreach (var question in questions)
               {
                    switch ((GraduationQuestionType)question.Type)
                    {
                         case GraduationQuestionType.DiplomaName:
                              this.ShowDiplomaName = true;
                              this.RequireDiplomaName = question.IsRequired;
                              break;
                         case GraduationQuestionType.PhoneticSpelling:
                              this.ShowPhoneticSpelling = true;
                              this.RequirePhoneticSpelling = question.IsRequired;
                              break;
                         case GraduationQuestionType.AttendCommencement:
                              this.ShowAttendCommencement = true;
                              this.RequireAttendCommencement = question.IsRequired;
                              break;
                         case GraduationQuestionType.NumberGuests:
                              this.ShowNumberGuests = true;
                              this.RequireNumberGuests = question.IsRequired;
                              break;
                         case GraduationQuestionType.NameInProgram:
                              this.ShowNameInProgram = true;
                              this.RequireNameInProgram = question.IsRequired;
                              break;
                         case GraduationQuestionType.Hometown:
                              this.ShowHometown = true;
                              this.RequireHometown = question.IsRequired;
                              break;
                         case GraduationQuestionType.CapSize:
                              this.ShowCapSize = true;
                              this.RequireCapSize = question.IsRequired;
                              break;
                         case GraduationQuestionType.GownSize:
                              this.ShowGownSize = true;
                              this.RequireGownSize = question.IsRequired;
                              break;
                         case GraduationQuestionType.CommencementLocation:
                              this.ShowCommencementLocation = true;
                              this.RequireCommencementLocation = question.IsRequired;
                              break;
                         case GraduationQuestionType.PickUpDiploma:
                              this.ShowPickUpDiploma = true;
                              this.RequirePickUpDiploma = question.IsRequired;
                              break;
                         case GraduationQuestionType.MilitaryStatus:
                              this.ShowMilitaryStatus = true;
                              this.RequireMilitaryStatus = question.IsRequired;
                              break;
                         case GraduationQuestionType.SpecialAccommodations:
                              this.ShowSpecialAccommodations = true;
                              this.RequireSpecialAccommodations = question.IsRequired;
                              break;
                         case GraduationQuestionType.PrimaryLocation:
                              this.ShowPrimaryLocation = true;
                              this.RequirePrimaryLocation = question.IsRequired;
                              break;
                         case GraduationQuestionType.RequestAddressChange:
                              // This case only gets run if the user determines in GAPQ that they want to SHOW the New Preferred Address Question
                              ///make new preferred address question visible only if addressesAreUpdatable flag is set to true & can update without permission flag is true or user have permission to update its address
                              if (userProfileConfig != null && userProfileConfig.AddressesAreUpdatable)
                              {
                                   if (!userProfileConfig.CanUpdateAddressWithoutPermission)
                                   {
                                        //check for permissions
                                        var updateAddressPermissionCode = "UPDATE.OWN.ADDRESS";
                                        IEnumerable<string> roleTitlesWithAddressUpdatePermissions = roles != null ?
                                    roles.Where(r => r.Permissions.Where(p => p.Code == updateAddressPermissionCode).Any())
                                    .Select(r => r.Title) : new List<string>();

                                        //retirve current user roles that have "UPDATE.OWN.ADDRESS" permission
                                        this.ShowNewPreferredAddressQuestion = currentUser.Roles != null && currentUser.Roles.Intersect(roleTitlesWithAddressUpdatePermissions).Any();
                                   }
                                   else
                                   {
                                        this.ShowNewPreferredAddressQuestion = true;
                                   }
                              }
                              break;
                    }
               }


               //for payment
               this.GraduationPaymentModel = new GraduationPaymentModel()
               {
                    PaymentMethods = financeConfiguration != null ? financeConfiguration.PaymentMethods : null,
                    RequireImmediatePayment = configuration != null ? configuration.RequireImmediatePayment : false,
                    GraduationFeesAmount = graduationFees != null ? graduationFees.Amount ?? 0 : 0,
                    DistributionCode = graduationFees != null ? graduationFees.PaymentDistributionCode : null
               };
          }
     }


}