﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

namespace Ellucian.Web.Student.Areas.Student.Models.Graduation
{
    public class MilitaryStatusModel
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}