﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.Graduation
{
    public class PrimaryLocationModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
       // public  LocationType LocationType  { get; set; }
    public bool IsSelected {get;set;}
    }
}

//public enum LocationType
//{
//    SACP,
//    PROG,
//    SPRO
//}