﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;

namespace Ellucian.Web.Student.Areas.Student.Models.Graduation
{
    public class ProgramModel
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public bool Selected { get; set; }
        public string Degree { get; set; }
        public List<string> Majors { get; set; }
        public List<string> Minors { get; set; }
        public List<string> Specializations { get; set; }
        public List<string> Ccds { get; set; }
    }
}