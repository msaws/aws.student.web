﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web.Mvc;
namespace Ellucian.Web.Student.Areas.Student.Models.Graduation
{
    public class PreviousApplicationsModel
    {
        public PreviousApplicationsModel(Ellucian.Colleague.Dtos.Student.Student student,
                               IEnumerable<Program> programs,
                               IEnumerable<Major> majors,
                               IEnumerable<Term> allTerms,
                               Ellucian.Colleague.Dtos.Student.GraduationConfiguration configuration,
                               IEnumerable<StudentProgram2> studentPrograms,
                               IEnumerable<GraduationApplication> graduationApplications,
                               ICurrentUser currentUser,
                               FinanceConfiguration financeConfiguration)
        {
            if (student == null)
                throw new ArgumentNullException("student", "You must supply a student.");
            if (studentPrograms == null)
                throw new ArgumentNullException("studentProgram", "You must supply a student program.");
            if (programs == null || !programs.Any())
                throw new ArgumentNullException("programs", "You must supply at least one active program");
            if (allTerms == null || !allTerms.Any())
                throw new ArgumentNullException("allTerms", "You must supply at least one term");
            if (majors == null || !majors.Any())
                throw new ArgumentNullException("majors", "You must supply at least one major");
            if (graduationApplications == null)
            {
                graduationApplications = new List<GraduationApplication>();
            }

            this.Programs = new List<ProgramModel>();
            if (student.ProgramIds == null || student.ProgramIds.Count == 0)
                throw new ArgumentException("Student must have an active program");

            //sorts the avaliable programs so that the programs that don't have an application are on top
            var submitorder = student.ProgramIds.Where(p => !graduationApplications.Any(q => q.ProgramCode == p));
            submitorder = submitorder.Concat(student.ProgramIds.Where(p => graduationApplications.Any(q => q.ProgramCode == p)));

            foreach (var programID in submitorder)
            {
                var program = programs.Where(p => p.Code == programID).FirstOrDefault();
                if (program!=null && program.IsGraduationAllowed)
                {
                    try
                    {
                        var prog = studentPrograms.Where(p => p.ProgramCode == programID).FirstOrDefault();
                        var application = graduationApplications.Where(p => p.ProgramCode == programID).FirstOrDefault();
                            var Majors = program.Majors;
                            if (prog != null)
                            {
                                /// <summary>
                                /// Getting any of the student's non program majors/minors/ect
                                /// </summary>
                                foreach (var req in prog.AdditionalRequirements)
                                {
                                    // Additional Requirements frequently have an associated major, minor, specialization, or certificate,
                                    // so add those to the program summary (user doesn't care that these are "additions", only that the extra is listed
                                    if (req.Type == AwardType.Major)
                                    {
                                        Majors = Majors.Union(new List<string>() { req.AwardName });
                                    }
                                }


                            string AppTerm = null;
                            string SubmitDate = null;
                            bool isGraduationApplicationSubmitted = false;
                            var SubmitString = GlobalResources.GetString(StudentResourceFiles.GraduationResources, "ApplicationExists");
                            if (SubmitString == null) { SubmitString = "Submitted {0}"; }
                            if (application != null)
                            {
                                isGraduationApplicationSubmitted = true;
                                if (application.GraduationTerm != null)
                                {
                                    if (configuration.GraduationTerms!=null && configuration.GraduationTerms.Contains(application.GraduationTerm))
                                    {
                                        var addTerm = allTerms.Where(p => p.Code == application.GraduationTerm).FirstOrDefault();
                                        if (addTerm != null)
                                        {
                                            AppTerm = addTerm.Description;
                                        }
                                    }
                                }

                                if (application.SubmittedDate != null)
                                    SubmitDate = application.SubmittedDate.Value.DateTime.ToShortDateString();
                            }
                            this.Programs.Add(new ProgramModel()
                            {
                                Code = program.Code,
                                Title = program.Title,
                                Degree = program.Degree,
                                Majors = Majors != null ? Majors.ToList<string>() : new List<string>(),
                                Term = AppTerm,
                                IsGraduationApplicationSubmitted=isGraduationApplicationSubmitted,
                                SubmittedString = string.Format(SubmitString, SubmitDate)
                            });

                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
            
            /// <summary>
            /// Inserts the links into the text
            /// </summary>
            char[] charSeparators = new char[] { ' ' };
            this.ApplyForDifferentProgramLink = configuration.ApplyForDifferentProgramLink;
            this.CommencementInformationLink = configuration.CommencementInformationLink;

            ISiteService SiteService = DependencyResolver.Current.GetService<ISiteService>();
            if (SiteService != null)
            {
                var siteServiceResults = SiteService.Get(currentUser);
                var academicPlanningMenu = siteServiceResults.Submenus.FirstOrDefault(m => m.Id == "academic-planning");
                this.ShowMyProgressLink = academicPlanningMenu != null && !academicPlanningMenu.Hidden && siteServiceResults.Pages.FirstOrDefault(p => p.Id == "myprogress" && !p.Hidden) != null;
            }
            // Determine if new applications are being accepted.  This would require at least one term open for graduation application in the configuration list of terms and,
            // if immediate payments are required that eCommerce is currently open.
            if (configuration.GraduationTerms != null && configuration.GraduationTerms.Count > 0)
            {
                if (configuration.RequireImmediatePayment && (financeConfiguration == null || (financeConfiguration != null && !financeConfiguration.ECommercePaymentsAllowed)))
                {
                    // When the client requires immediate payment but eCommerce payments are not currently allowed, do not accept new applications.
                    AcceptingNewApplications = false;
                }
                else
                {
                    AcceptingNewApplications = true;
                }
                
            }
            else
            {
                AcceptingNewApplications = false;
            }


        }
        public List<ProgramModel> Programs { get; set; }
        public bool AcceptingNewApplications { get; set; }

        /// <summary>
        /// The Links that the colleges can define
        /// </summary>
        public string CommencementInformationLink { get; set; }
        public string ApplyForDifferentProgramLink { get; set; }
        public bool ShowMyProgressLink { get; set; }

        public class ProgramModel
        {
            public string Code { get; set; }
            public string Title { get; set; }
            public string Term { get; set; }
            public bool Selected { get; set; }
            public string Degree { get; set; }
            public List<string> Majors { get; set; }
            public string SubmittedString { get; set; }
            public bool IsGraduationApplicationSubmitted { get; set; }
        }
        public class GraduationTermModel
        {
            public string Code { get; set; }
            public string Description { get; set; }
            public bool Selected { get; set; }
        }
    }

}