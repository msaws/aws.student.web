﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.StudentRequests
{
    /// <summary>
    /// Model that contains a list of request objects for a student. These can actually be either transcript requests or enrollment verification requests
    /// given the similarity of the data and how showing the list of requests for either is really the same.
    /// </summary>
    public class StudentRequestsModel
    {
        public List<StudentRequestModel> Requests { get; set; }

        public List<string> RequestRestrictionMessages { get; set; }


        public StudentRequestsModel(List<StudentRequest> studentRequestDtos, IEnumerable<Country> countryDtos, TranscriptAccess transcriptAccess, List<InvoicePayment> invoicePayments, FinanceConfiguration financeConfiguration, bool immediatePayment)
        {
            Requests = new List<StudentRequestModel>();
            RequestRestrictionMessages = new List<string>();

            //Gets the resx
            ResourceManager MyResourceClass = new ResourceManager("Resources.Requests", System.Reflection.Assembly.Load("App_GlobalResources"));

            if (studentRequestDtos != null && studentRequestDtos.Any())
            {
                // Sort the DTOS by descending request date.

                foreach (var request in studentRequestDtos.OrderByDescending(sr => sr.RequestDate))
                {
                    List<string> recipientAddress = new List<string>();
                    if (request.MailToAddressLines != null && request.MailToAddressLines.Any())
                    {
                        recipientAddress = request.MailToAddressLines;
                    }
                    if (!string.IsNullOrEmpty(request.MailToCity) && !string.IsNullOrEmpty(request.MailToState) && !string.IsNullOrEmpty(request.MailToPostalCode))
                    {
                        recipientAddress.Add(request.MailToCity + ", " + request.MailToState + " " + request.MailToPostalCode);
                    }
                    if (!string.IsNullOrEmpty(request.MailToCountry))
                    {
                        var country = countryDtos.Where(c => c.Code == request.MailToCountry).FirstOrDefault();
                        if (country != null && !string.IsNullOrEmpty(country.Description))
                        {
                            recipientAddress.Add(country.Description);
                        }
                    }
                    var studentRequest = new StudentRequestModel()
                    {
                        RequestId = request.Id,
                        RecipientName = request.RecipientName,
                        RequestDate = request.RequestDate.HasValue ? request.RequestDate.Value.ToShortDateString() : string.Empty,
                        CompletedDate = request.CompletedDate.HasValue ? request.CompletedDate.Value.ToShortDateString() : MyResourceClass.GetString("PendingRequestText"),
                        MailToAddress = recipientAddress,
                        NumberOfCopies = request.NumberOfCopies.ToString()
                    };
                    studentRequest.PaymentStatus = StudentRequestPaymentStatus.None.ToString().ToLower();
                    if (!string.IsNullOrEmpty(request.InvoiceNumber))
                    {
                        var invoice = invoicePayments.Where(i => i.Id == request.InvoiceNumber).FirstOrDefault();
                        // Determine payment status for the item

                        studentRequest.ShowPayNow = false;
                        if (invoice != null)
                        {
                            if ((invoice.Amount - invoice.AmountPaid) > 0)
                            {
                                studentRequest.PaymentStatus = StudentRequestPaymentStatus.Unpaid.ToString().ToLower();

                                // Since there is amount amount due for the invoice show a Pay Now for this item as long as the institution has e-commerce turned on, self-service payments are allowed 
                                // and require immediate payment is allowed for this type of request.

                                if (financeConfiguration != null && financeConfiguration.ECommercePaymentsAllowed && financeConfiguration.SelfServicePaymentsAllowed && immediatePayment)
                                {
                                    studentRequest.ShowPayNow = true;
                                }
                            }
                            else
                            {
                                studentRequest.PaymentStatus = StudentRequestPaymentStatus.Paid.ToString().ToLower();
                            }
                        }
                    }
                    Requests.Add(studentRequest);

                }

                if (transcriptAccess != null && transcriptAccess.TranscriptRestrictions != null && transcriptAccess.TranscriptRestrictions.Any())
                {
                    foreach (var restriction in transcriptAccess.TranscriptRestrictions)
                    {
                        if (!string.IsNullOrEmpty(restriction.Description))
                        {
                            RequestRestrictionMessages.Add(restriction.Description);
                        }
                    }
                }

            }


        }



    }
}