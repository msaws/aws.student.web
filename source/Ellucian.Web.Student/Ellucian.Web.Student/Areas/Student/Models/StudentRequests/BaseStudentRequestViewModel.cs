﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.StudentRequests
{
    public abstract class BaseStudentRequestViewModel
    {

        public IEnumerable<Country> Countries { get; set; }
        public IEnumerable<State> States { get; set; }
        public bool RequireImmediatePayment { get; set; }

        public BaseStudentRequestViewModel()
        {

        }
        public BaseStudentRequestViewModel(IEnumerable<Country> countries, IEnumerable<State> states)
            : this()
        {
            Countries = countries != null ? countries.OrderBy(c => c.Description).ToList() : new List<Country>();
            States = states != null ? states.OrderBy(st => st.Description).ToList() : new List<State>();
        }
  
    }
}