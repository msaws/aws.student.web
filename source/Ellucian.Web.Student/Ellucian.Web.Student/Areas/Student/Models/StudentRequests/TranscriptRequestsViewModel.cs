﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using Ellucian.Colleague.Dtos.Student.Requirements;

namespace Ellucian.Web.Student.Areas.Student.Models.StudentRequests
{
    public class TranscriptRequestsViewModel:BaseStudentRequestViewModel
    {
        /// <summary>
        /// Data for dropdowns
        /// </summary>
        public IEnumerable<TranscriptGrouping> TranscriptTypes { get; private set; }
        public IEnumerable<HoldRequestType> TranscriptHoldTypes { get; private set; }

        /// <summary>
        /// TranscriptRequestsViewModel constructor
        /// </summary>
        /// <param name="countries"></param>
        /// <param name="states"></param>
        /// <param name="holdTypes"></param>
        /// <param name="transcriptGroupings"></param>
        public TranscriptRequestsViewModel(IEnumerable<Country> countries,
                                IEnumerable<State> states,
                                IEnumerable<HoldRequestType> holdTypes
                                ):base(countries,states)
        {

            TranscriptHoldTypes = (holdTypes != null && holdTypes.Any()) ? holdTypes : null;
        }

        public void UpdateTranscriptTypes(IEnumerable<TranscriptGrouping> selectableTranscriptGroupings, IEnumerable<StudentProgram2> studentPrograms, IEnumerable<Colleague.Dtos.Student.Requirements.Program> programs)
        {
           if(studentPrograms==null || programs==null || selectableTranscriptGroupings ==null )
           {
               return;
           }
            //retrieve official transcript grouping from programs by joining with student programs
            var transcripts=studentPrograms.Where(sp=>sp.ProgramStatusProcessingCode==StudentProgramStatusProcessingType.Active || sp.ProgramStatusProcessingCode==StudentProgramStatusProcessingType.Graduated || sp.ProgramStatusProcessingCode==StudentProgramStatusProcessingType.Withdrawn)
            .Join<StudentProgram2, Program, string, string>(programs, sp => sp.ProgramCode, p => p.Code, (sp, p) => p.OfficialTranscriptGrouping).Where(p=>!string.IsNullOrEmpty(p)).Distinct<string>();
            if(transcripts!=null && transcripts.Any())
            {
                //retrieve transcript types by joining selectable transcript grouping with program transcript groupings.
              TranscriptTypes=  transcripts.Join<string, TranscriptGrouping, string, TranscriptGrouping>(selectableTranscriptGroupings, t => t, tg => tg.Id, (t, tg) => tg);
            }
            else
            {
                TranscriptTypes = selectableTranscriptGroupings;
            }


        }
    }
}