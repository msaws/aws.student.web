﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.StudentRequests
{
     //Carries basic information about either an enrollment request or a transcript request to show on the
     //landing page. 
    public class StudentRequestModel
    {
        public string RequestId { get; set; }
        public string RequestDate { get; set; }
        public string CompletedDate { get; set; }
        public string RecipientName { get; set; }
        public List<string> MailToAddress { get; set; }
        public string NumberOfCopies { get; set; }
        /// <summary>
        /// Status of the payment on the request - user definable texts for paid, unpaid, or not applicable
        /// </summary>
        public string PaymentStatus { get; set; }

        /// <summary>
        /// Indicates whether the Pay Now button should display to the user.  This should only be true if
        ///    1) The student has an outstanding amount due
        ///    2) The institution has e-commerce turned on for Make a Payment
        ///    3) The institution is allowing immediate payment for the appropriate type of student requests 
        /// </summary>
        public bool ShowPayNow { get; set; }

        public string PaymentStatusNotation
        {
            get
            {
                var notationText = string.Empty;
                if (PaymentStatus == "paid")
                    notationText = GlobalResources.GetString(StudentResourceFiles.RequestsResources, "PaymentStatusPaid");
                if (PaymentStatus == "unpaid")
                    notationText = GlobalResources.GetString(StudentResourceFiles.RequestsResources, "PaymentStatusUnpaid");
                if (PaymentStatus == "none")
                    notationText = GlobalResources.GetString(StudentResourceFiles.RequestsResources, "PaymentStatusNotApplicable");
                return notationText;
            }
        }
        public StudentRequestModel()
        {
            MailToAddress = new List<string>();
        }

    }

    public enum StudentRequestPaymentStatus
    {
        None,
        Paid,
        Unpaid
    }
}