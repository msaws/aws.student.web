﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using System;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.StudentRequests
{
    /// <summary>
    /// Request model class. Can be used for multiple types of requests, e.g.
    /// Transcript Request, Enrollment Verification Request
    /// </summary>
    public abstract class BaseStudentRequestModel
    {
        /// <summary>
        /// Recipient of the requested document
        /// </summary>
        [Required]
        public string Recipient { get; set; }

        /// <summary>
        /// Address lines where the requested document to be sent
        /// </summary>
       [Required]
        public List<string> MailToAddressLines { get; set; }

        /// <summary>
        /// City where the requested document to be sent
        /// </summary>
        public string MailToCity { get; set; }

        /// <summary>
        /// State where the requested document to be sent
        /// </summary>
        public string MailToState { get; set; }

        /// <summary>
        /// Country where the requested document to be sent
        /// </summary>
        public string MailToCountry { get; set; }

        /// <summary>
        /// Postal code where the requested document to be sent
        /// </summary>
        public string MailToPostalCode { get; set; }

        /// <summary>
        /// Number of copies requested
        /// </summary>
        [Required]
        public int? NumberOfCopies { get; set; }

        /// <summary>
        /// Comments regarding the request
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Hold request code
        /// </summary>
        public string HoldRequestCode { get; set; }

        /// <summary>
        /// Requires immediate payment
        /// </summary>
        public bool RequireImmediatePayment { get; set; }

    }
}