﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using System;
using System.Collections.Generic;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.StudentRequests
{

     public class EnrollmentVerificationRequestViewModel:BaseStudentRequestViewModel
     {
         
          public EnrollmentVerificationRequestViewModel(IEnumerable<Country> countries, IEnumerable<State> states):base(countries,states)
          {
          }

     
     }
}