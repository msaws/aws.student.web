﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Models.StudentRequests
{
    /// <summary>
    /// TranscriptRequestModel class. Extends on abstract RequestModel class
    /// </summary>
    public class TranscriptRequestModel : BaseStudentRequestModel {

        public string TranscriptGrouping { get; set;}

        public TranscriptRequestModel()
        {

        }
    }
}