﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

//Transcript request view model used to hold data for 
//display on Transcript Requests page; inherits properties from 
//requestViewModel (request.view.model.js)
function transcriptRequestsViewModel() {

    var self = this;
    
    //"Inherit" the base view model for mobile observables
    BaseViewModel.call(self, 769);
    self.changeToMobile = function () { }
    self.changeToDesktop = function () { }

    //"Inherit" the request view model for common student request properties
    baseStudentRequestViewModel.call(self);
    
    //Observable array of TranscriptTypes(TranscriptGroupings)
    self.TranscriptTypes = ko.observableArray();

    //Observable to hold currently selected TranscriptType(grouping)
    self.TranscriptType = ko.observable();

    //subscribe to function when it is single value in transcript types
    self.TranscriptTypes.subscribe(function (value) {
        if (value !== null && typeof value !== 'undefined' && value.length == 1)
            self.TranscriptType(self.TranscriptTypes()[0]);
    });

    //Observable array of transcript request hold types
    self.TranscriptHoldTypes = ko.observableArray();

    //Observable to hold currently selected request hold type
    self.TranscriptHoldType = ko.observable();

    //Observable to indicate whether the request is in the process of 
    //being submitted
    self.isSubmissionInProgress = ko.observable(false);

    //Function that triggers creation of a new transcript request
    self.submitTranscriptRequest = function () {

        try
        {
            self.isSubmissionInProgress(true);
            setTimeout(function () { }, 2000);

            ko.validation.group(self, { deep: true });
            //The viewmodel is valid, initiate an ajax request
            if (self.isValid()) {
                self.cleanupAddressInfoBeforeSubmit();
                var request = new transcriptRequestCollectorModel(self);
                ko.utils.postJson(createNewTranscriptRequestActionUrl, { newTranscriptRequest: ko.mapping.toJS(request), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
            }
                //required data is missing, highlight errors
            else {
                self.isSubmissionInProgress(false);
                self.hasInputErrors(true);
                $(".required-input").first().focus();
            }
        }
        catch (e) {
           self.isSubmissionInProgress(false);
            console.error(e.message);
            $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.TranscriptRequests.unableToSubmitNewTranscriptRequestMessage, type: "error" });
        }
    };

}