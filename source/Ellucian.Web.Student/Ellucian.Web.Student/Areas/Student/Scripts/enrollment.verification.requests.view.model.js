﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function enrollmentVerificationRequestsViewModel() {

     var self = this;

     //"Inherit" the request view model for common student request properties
     baseStudentRequestViewModel.call(self);
     self.isUpdating = ko.observable(false);

     self.submit = function () {
         try
         {
             self.isUpdating(true);
             var errors = ko.validation.group(self, { deep: true });
             if (self.isValid()) {
                 self.cleanupAddressInfoBeforeSubmit();
                 var model = new enrollmentVerificationRequestCollectorModel(self);
                 ko.utils.postJson(createNewEnrollmentVerificationRequestUrl, { newEnrollmentRequest: ko.mapping.toJS(model), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
             }
             else {
                 self.isUpdating(false);
                 self.hasInputErrors(true);
                 $(".required-input").first().focus();
             }
         }
         catch (e) {
             self.isUpdating(false);
             console.error(e.message);
             $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.RequestsF.enrollmentVerificationRequestSubmissionFailure, type: "error" });
         }
     };
}