﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
ko.observable.fn.ContentsChanged = function (callback) {
     var oldValue;
     this.subscribe(function (_oldValue) {
          oldValue = _oldValue;
     }, this, 'beforeChange');

     this.subscribe(function (newValue) {
          callback(newValue, oldValue);
     });
};

function GraduationViewModel() {
     var self = this;

     ko.validation.init({
          messagesOnModified: true,
          insertMessages: false,
          decorateInputElement: false
     });

     self.isLoaded = ko.observable(false);
     self.isUpdating = ko.observable(false);
     self.viewOnlyMode = ko.observable(false);
     self.editOnlyMode = ko.observable(false);
     self.IsAcademicCredentialsUpdated = ko.observable(false);
     self.IsDiplomaAddressSameAsPreferred = ko.observable(false);
     self.isFormDirty = ko.observable(false);
     self.bindingComplete = ko.observable(false);
     self.hasInputErrors = ko.observable(false);
     self.FullName = ko.observable();
     self.Programs = ko.observableArray();
     self.GraduationTerms = ko.observableArray();
     self.CapSizes = ko.observableArray();
     self.GownSizes = ko.observableArray();
     self.CommencementSites = ko.observableArray();
     self.MilitaryStatuses = ko.observableArray();
     self.MaximumCommencementGuests = ko.observable();
     self.NumberofGuestsLabel = ko.observable();
     self.GraduationPaymentModel = ko.observable(new GraduationPaymentModel());
     self.PreferredAddress = ko.observableArray();
     self.PrimaryLocations = ko.observableArray();
     self.existingAddress = ko.observable(false);
     self.AddressInfo = ko.observable(new AddressModel());
     self.isNewPreferredAddress = ko.observable(false);
     self.hasAddressChanged = ko.observable(false);

     //links
     self.PhoneticSpellingLink = ko.observable();
     self.CapAndGownLink = ko.observable();
     self.CapAndGownSizingLink = ko.observable();

     //Graduation Term
     self.GraduationTermSelected = ko.observable("").extend({ required: true });
     self.GraduationTerm = ko.observable();
     self.GraduationTermDisplay = ko.observable("");

     //The diploma name
     self.ShowDiplomaName = ko.observable(false);
     self.RequireDiplomaName = ko.observable(false);
     self.DiplomaName = ko.observable("").extend({
          required: {
               onlyIf: function () { return self.RequireDiplomaName() && self.ShowDiplomaName() }
          }
     });
     self.DiplomaName.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     //The phonetic spelling
     self.ShowPhoneticSpelling = ko.observable(false);
     self.RequirePhoneticSpelling = ko.observable(false);
     self.PhoneticSpellingOfName = ko.observable("").extend({
          required: {
               onlyIf: function () { return self.RequirePhoneticSpelling() && self.ShowPhoneticSpelling() }
          }
     });
     self.PhoneticSpellingOfName.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     //Hometown
     self.ShowHometown = ko.observable(false);
     self.RequireHometown = ko.observable(false);
     self.Hometown = ko.observable("").extend({
          required: {
               onlyIf: function () { return self.RequireHometown() && self.ShowHometown() }
          }
     });
     self.Hometown.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     //Commencement Details read fields

     //commencementLocation
     self.ShowCommencementLocation = ko.observable(false);
     self.RequireCommencementLocation = ko.observable(false);
     self.CommencementSiteSelected = ko.observable("").extend({ required: { onlyIf: function () { return self.RequireCommencementLocation() && self.ShowCommencementLocation() } } });
     self.CommencementSiteSelected.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     //Attend Commencement
     self.ShowAttendCommencement = ko.observable(false);
     self.RequireAttendCommencement = ko.observable(false);
     self.AttendingCommencement = ko.observable(true);
     self.AttendingCommencement.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     //Include name in program
     self.ShowNameInProgram = ko.observable(false);
     self.RequireNameInProgram = ko.observable(false);
     self.IncludeNameInProgram = ko.observable(true);
     self.IncludeNameInProgram.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     //Number of guests
     self.ShowNumberGuests = ko.observable(false);
     self.RequireNumberGuests = ko.observable(false);
     self.ShowGuests = ko.computed(function () {
          if (self.ShowNumberGuests() && (self.AttendingCommencement() || !self.ShowAttendCommencement())) {
               return true;
          }
          else {
               return false;
          }
     });
     self.NumberOfGuests = ko.observable("0").extend({
          required: {
               onlyIf: function () {
                    return self.RequireNumberGuests() && self.ShowGuests()
               }
          },
          min: 0,
          validation: [{
               validator: function (value) {

                    if (self.ShowGuests()) {
                         try {
                              if (self.NumberOfGuests() > 0 && self.NumberOfGuests() % 1 != 0)
                                   return false;
                              return self.NumberOfGuests() <= self.MaximumCommencementGuests();
                         } catch (e) {
                              return false;
                         }
                    }
                    else
                         return true;
               },
               message: "Number is out of range"
          }]
     });
     self.NumberOfGuests.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     //cap size
     self.ShowCapSize = ko.observable(false);
     self.RequireCapSize = ko.observable(false);
     self.CapSizeSelected = ko.observable("").extend({ required: { onlyIf: function () { return self.RequireCapSize() && self.ShowCapSize() && self.ShowCapAndGown() } } });
     self.CapSizeSelected.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });
     //gown size
     self.ShowGownSize = ko.observable(false);
     self.RequireGownSize = ko.observable(false);
     self.GownSizeSelected = ko.observable("").extend({ required: { onlyIf: function () { return self.RequireGownSize() && self.ShowGownSize() && self.ShowCapAndGown() } } });
     self.OverrideCapAndGownDisplay = ko.observable(false);
     self.ShowCapAndGown = ko.computed(function () {
         if ((self.OverrideCapAndGownDisplay() || self.AttendingCommencement() || !self.ShowAttendCommencement()) && (self.CapAndGownLink() || self.ShowCapSize() || self.ShowGownSize())) {
               return true;
          }
          else {
               return false;
          }
     }, this);
     self.GownSizeSelected.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });
     //pick up diploma
     self.ShowPickUpDiploma = ko.observable(false);
     self.RequirePickUpDiploma = ko.observable(false);
     self.WillPickupDiploma = ko.observable(true);
     self.ShowPickUpDiplomaAddress = ko.computed(function () {
          if (!self.ShowPickUpDiploma() || (self.ShowPickUpDiploma() && !self.WillPickupDiploma())) {
               return true;
          }
          else {
               return false;
          }
     });
     self.WillPickupDiploma.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

    

     //Address
     self.existingAddress.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });
     //Require address
     self.RequireAddress = ko.computed(function () {
          return !self.existingAddress() && (!self.WillPickupDiploma() || !self.ShowPickUpDiploma());
     }, this);

     //Detect if Address has been changed
     self.AddressInfo().Street1.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().Street2.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().Street3.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().Street4.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().City.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().State.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().PostalCode.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().Country.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });
     self.AddressInfo().internationalSelected.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
              self.hasAddressChanged(true);
          }
     });


     // Formats input address to be displayed correctly
     self.DisplayedAddress = ko.computed(function () {
          var string = "";
          if (self.existingAddress() != true) {
               string = string.concat(self.AddressInfo().Street1() + "<br\>");
               if (self.AddressInfo().Street2() != "") {
                    string = string.concat(self.AddressInfo().Street2() + "<br\>");
               }
               if (self.AddressInfo().Street3() != "") {
                    string = string.concat(self.AddressInfo().Street3() + "<br\>");
               }
               if (self.AddressInfo().City() != "" || self.AddressInfo().State() != null || self.AddressInfo().PostalCode() != "") {
                    string = string.concat(self.AddressInfo().City());
                    if (self.AddressInfo().State() != null) {
                         string = (self.AddressInfo().City() != "") ? string.concat(", " + self.AddressInfo().State().Code + " ") : string.concat(self.AddressInfo().State().Code + " ");
                    }
                    string = string.concat(self.AddressInfo().PostalCode() + "<br\>");
               }
               if (self.AddressInfo().Country() != null) {
                    string = string.concat(self.AddressInfo().Country().Description);
               }
          }
          return string;
     }, this);

     self.validateAddress = ko.computed(function () {
          self.AddressInfo().validate(!self.existingAddress() && self.ShowPickUpDiplomaAddress());
     }, this);


     // Military Status
     self.ShowMilitaryStatus = ko.observable(false);
     self.RequireMilitaryStatus = ko.observable(false);
     self.MilitaryStatusSelected = ko.observable("").extend({ required: { onlyIf: function () { return self.RequireMilitaryStatus() && self.ShowMilitaryStatus() } } });
     self.MilitaryStatusSelected.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     // Primary Location
     self.ShowPrimaryLocation = ko.observable(false);
     self.RequirePrimaryLocation = ko.observable(false);
     self.PrimaryLocationSelected = ko.observable().extend({ required: { onlyIf: function () { return self.RequirePrimaryLocation() && self.ShowPrimaryLocation() } } });
     self.PrimaryLocationSelected.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });
     self.multipleLocations = ko.computed(function () {
          if (self.PrimaryLocations().length == 1 && self.PrimaryLocations()[0].IsSelected() == true) {
               self.PrimaryLocationSelected(self.PrimaryLocations()[0]);
               return false;
          }
          return (self.PrimaryLocations().length > 1) ? true : false;
     }, this);
     self.activeLocation = ko.computed(function () {
          if (self.PrimaryLocations().length > 0) {
               var pl = ko.utils.arrayFirst(self.PrimaryLocations(), function (item) {
                    return item.IsSelected() === true;
               });
               if (pl !== null && pl !== typeof 'undefined') {
                    self.PrimaryLocationSelected(pl);
                    return true;
               }
               else {
                    return false;
               }
          }
          return false;
     }, this);

     // Special Accommodations
     self.ShowSpecialAccommodations = ko.observable(false);
     self.RequireSpecialAccommodations = ko.observable(false);
     self.SpecialAccommodations = ko.observable("").extend({
          required: {
               onlyIf: function () { return self.RequireSpecialAccommodations() && self.ShowSpecialAccommodations() && (self.AttendingCommencement() || !self.ShowAttendCommencement()) }
          }
     });
     self.DisplaySpecialAccommodationsField = ko.computed(function () {
          if (self.ShowSpecialAccommodations() && (self.AttendingCommencement() || !self.ShowAttendCommencement())) {
               return true;
          }
          else {
               return false;
          }
     }, this);
     self.SpecialAccommodations.ContentsChanged(function (newValue, oldValue) {
          if (oldValue != newValue && self.bindingComplete()) {
               self.isFormDirty(true);
          }
     });

     self.ShowNewPreferredAddressQuestion = ko.observable(false);

     self.updateApp = function () {
         if (self.readyToSubmit() == true) {
               var program = gradView.Programs()[0];
               var view = new GraduationAddViewModel(program.Code(), self);
               var json = {
                   submissionJson: ko.toJSON(view),
                   addressChangeRequested: self.isNewPreferredAddress()
               };

               //put the data
               $.ajax({
                    url: Ellucian.Student.Graduation.ActionUrls.updateGraduationApplicationUrl,
                    data: JSON.stringify(json),
                    type: "PUT",
                    dataType: "json",
                    contentType: jsonContentType,
                    beforeSend: function (data) {
                         self.isUpdating(true);
                    },
                    error: function () {
                         self.isUpdating(false);
                    },
                    complete: function (xhrObject) {
                         self.isUpdating(false);
                         if (xhrObject.status == 200) {
                              window.location = Ellucian.Student.Graduation.ActionUrls.graduationInfoUrl + '?Notify=editSuccess';
                         }
                         else if (xhrObject.status == 406) {
                             // 406 from the GradApp means the Graduation Application was updated but the User Profile Preferred Address threw an exception.
                             window.location = Ellucian.Student.Graduation.ActionUrls.graduationInfoUrl + '?Notify=editAddressError';
                         }
                         else {
                             window.location = Ellucian.Student.Graduation.ActionUrls.graduationInfoUrl + '?Notify=editFailure';
                         }

                    }
               });
          }
     };

     self.readyToSubmit = function () {
          try {
               var errors = ko.validation.group(self, { deep: true });
               if (self.isValid()) {
                   //clean the new address data if existingAddress radio button is selected or when address section is not shown(will happen when diploma is picked)
                    if (self.existingAddress() || !self.ShowPickUpDiplomaAddress()) {
                         self.AddressInfo().Street1(null);
                         self.AddressInfo().Street2(null);
                         self.AddressInfo().Street3(null);
                         self.AddressInfo().Street4(null);
                         self.AddressInfo().City(null);
                         self.AddressInfo().State(null);
                         self.AddressInfo().PostalCode(null);
                         self.AddressInfo().Country(null);
                    }
                   //clean the data for address selection between international and US/Canada.
                    if (!self.AddressInfo().internationalSelected() && self.AddressInfo().State() !== null) {
                        var c = ko.utils.arrayFirst(self.AddressInfo().Countries(), function (item) {
                            return item.Code === self.AddressInfo().State().CountryCode;
                        });
                        self.AddressInfo().Country(c);
                        self.AddressInfo().Street3(null);
                        self.AddressInfo().Street4(null);
                    }
                    else {
                        self.AddressInfo().City(null);
                        self.AddressInfo().State(null);
                        self.AddressInfo().PostalCode(null);
                    }
                   //this checkbox have dependent controls. that is, if it is selected, other fields appear. In a scenario when a user select this checkbox, enter values for
                   //dependent fields but before submit make a No selection that in turn hides these dependent boxes, we want to send null values for that.
                   //we could have immediately made null by subscribing them to the changes in checkbox observable but we want to retain values until submitted.
                    if (self.AttendingCommencement() !== null && self.AttendingCommencement() === false) {

                        //only nullify when #of guests is shown otherwise in submit it will default to 0 and in edit it will remain the same value as was 
                        //entered in back office. We don't want hidden fields to be editted hence should retain original values.
                        if (self.ShowNumberGuests() === true) {
                            self.NumberOfGuests("0");
                        }
                        if (self.ShowSpecialAccommodations() === true) {
                            self.SpecialAccommodations(null);
                        }
                    }
                   
                    if (!self.OverrideCapAndGownDisplay() && self.AttendingCommencement() !== null && self.AttendingCommencement() === false)
                    {
                        if (self.ShowCapSize() === true) {
                            self.CapSizeSelected(null);
                        }
                        if (self.ShowGownSize() === true) {
                            self.GownSizeSelected(null);
                        }
                    }
                   //these boolean values or checkboxes are set to null when not shown because they are initially assigned true value during declaration. 
                   //so if not reset, the values will go as true even if it is hidden. This is only when new form is submitted, for edit mode we don't want to nullify hidden
                   //boolean values.
                    if (!self.ShowAttendCommencement() && !self.editOnlyMode()) {
                         self.AttendingCommencement(null);
                    }
                   
                    if (!self.ShowNameInProgram() && !self.editOnlyMode()) {
                         self.IncludeNameInProgram(null);
                    }

                    if (!self.ShowPickUpDiploma() && !self.editOnlyMode()) {
                        self.WillPickupDiploma(null);
                    }
                    //this is when NumberOfGuests is not required and left blank, we will default it to 0.
                    if (isNullOrEmpty(self.NumberOfGuests())) {
                        self.NumberOfGuests("0");
                    }

                    return true;
               }
               else {
                    self.hasInputErrors(true);
                    $(".required-input").first().focus();
               }
          }
          catch (e) {
               console.error(e.message);
               $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve graduation information", type: "error" });
          }

     };

     self.submitApp = function () {
          if (self.readyToSubmit() == true) {
               var program = gradView.Programs()[0];
               var view = new GraduationAddViewModel(program.Code(), self);
               var json = {
                   graduationApplicationJson: ko.toJSON(view),
                   graduationPaymentJson: ko.toJSON(self.GraduationPaymentModel),
                   addressChangeRequested: self.isNewPreferredAddress()
               };

               //post the data
               $.ajax({
                    url: Ellucian.Student.Graduation.ActionUrls.addStudentGraduationInfoUrl,
                    data: JSON.stringify(json),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType,
                    beforeSend: function (data) {
                         self.isUpdating(true);
                    },
                    success: function (jsonObject) {
                         self.isUpdating(false);
                         if (jsonObject.StatusCode == 200) {
                              if (self.GraduationPaymentModel().ShowPaymentDetails() && jsonObject.MakeAPaymentModel !== null) {
                                  self.showPayment(jsonObject);
                              }
                              else {
                                  window.location = Ellucian.Student.Graduation.ActionUrls.submitGraduationApplicationUrl + "?statusCode=" + jsonObject.StatusCode;
                              }
                         }
                         else if (jsonObject.StatusCode == 406) {
                             // 406 from the GradApp means the Graduation Application was updated but the User Profile Preferred Address threw an exception.
                             if (self.GraduationPaymentModel().ShowPaymentDetails() && jsonObject.MakeAPaymentModel !== null) {
                                 self.showPayment(jsonObject);
                             } else {
                                 window.location = Ellucian.Student.Graduation.ActionUrls.graduationInfoUrl + '?Notify=editAddressError';
                              }
                         }
                         
                         else {
                             window.location = Ellucian.Student.Graduation.ActionUrls.submitGraduationApplicationUrl + "?statusCode=" + jsonObject.StatusCode;
                         }
                    },
                    error: function (jsonObject) {
                         self.isUpdating(false);
                         window.location = Ellucian.Student.Graduation.ActionUrls.submitGraduationApplicationUrl + "?statusCode=" + jsonObject.StatusCode;
                    }
               });
          }
     };

     //Function that is triggered if ShowPaymentDetails flag is set to true on submitApp
     self.showPayment = function (jsonObject) {
         var makeAPaymentModel = jsonObject.MakeAPaymentModel;
         makeAPaymentModel.AlertMessage = sanitizeFormData(makeAPaymentModel.AlertMessage);
         ko.utils.postJson(mapPaymentReviewActionUrl, { model: makeAPaymentModel, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
     };

     self.pickupDiplomaClicked = function () {
          $("#address-section-id").attr("aria-live", "polite");
          $("#address-section-id").attr("role", "region");
          return true;
     };

     self.newAddressClicked = function () {
          if (self.existingAddress() !== null && typeof self.existingAddress() != 'undefined' && !self.existingAddress()) {
               $("#address-entry-component-id").attr("aria-live", "assertive");
               $("#address-entry-component-id").attr("role", "group");
               $("#address-entry-component-id").attr("aria-atomic", "true");
          }
          self.hasAddressChanged(true);
          return true;
     };
};