﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

//initialize the Viewmodel
var gradInfo = new GraduationInfoViewModel();

$(document).ready(function () {
     ko.applyBindings(gradInfo, document.getElementById('graduation-info'));
     //Loads the data into the form
     getGraduationInfo();

});

function getGraduationInfo() {
     $.ajax({
          url: Ellucian.Student.Graduation.ActionUrls.getPreviousGraduationInfoUrl,
          type: "GET",
          contentType: jsonContentType,
          dataType: "json",
          success: function (data) {
               if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, {}, gradInfo);
               }
          },
          error: function (jqXHR, textStatus, errorThrown) {
               if (jqXHR.status != 0) {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Graduation.gradAppGetFailure, type: "error" });
               }

          },
          complete: function (jqXHR, textStatus) {
               gradInfo.isLoaded(true);
               $("#graduation-table").makeTableResponsive();
               if (notify === "editSuccess") {
                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Graduation.gradAppUpdateSuccess, type: "success" });
               }
               if (notify === "editFailure") {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Graduation.gradAppUpdateFailure, type: "error" });
               }
               if (notify === "editAddressError")
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Graduation.gradAppProfileUpdateError, type: "error" });
          }
     });
}