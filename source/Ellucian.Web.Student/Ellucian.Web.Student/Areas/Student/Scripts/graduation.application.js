﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

//initialize the Viewmodel
var gradView = new GraduationViewModel();

$(document).ready(function () {
     try {

         ko.components.register('address-entry', {
             require: 'AddressEntry/_AddressEntry'
         });

         ko.components.register('yes-no-toggle', {
             require: 'YesNoToggle/_YesNoToggle'
         });



          ko.applyBindings(gradView, document.getElementById('graduation-form'));
          //Loads the data into the form
          $.ajax({
               url: Ellucian.Student.Graduation.ActionUrls.getStudentGraduationInfoUrl,
               type: "GET",
               contentType: jsonContentType,
               dataType: "json",
               success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                         try {
                              ko.mapping.fromJS(data, { 'ignore': ["GraduationApplication"] }, gradView);
                              gradView.DiplomaName(data.FullName);
                              gradView.existingAddress(data.PreferredAddress.length > 0);
                              gradView.AddressInfo().States(data.States);
                              gradView.AddressInfo().Countries(data.Countries);
                              if (data.GraduationApplication != null) {
                                   loadApplication(data);
                              }
                              x = $("#program option").length
                              if (x > 5) x = 5;
                              $("#program").attr("size", x);
                              //fits the program select box to the number of options*/
                         }
                         catch (e) {
                              console.error(e.message);
                              $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve graduation information", type: "error" });
                         }
                    }
               },
               error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                         $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve graduation information", type: "error" });
                    }
               },
               complete: function (jqXHR, textStatus) {
                    gradView.isLoaded(true);
               }
          });
     }
     catch (e) {
          console.error(e.message);
          $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve graduation information", type: "error" });
     }

});

loadApplication = function (data) {
     try {
          ko.mapping.fromJS(data.GraduationApplication, {}, gradView);
          with (data.GraduationApplication) {
              if (MailDiplomaToAddressLines.length > 0) {
                  if (gradView.IsDiplomaAddressSameAsPreferred()) {
                      gradView.existingAddress(true);
                  }
                  else {
                      gradView.existingAddress(false);

                      gradView.AddressInfo().Street1(data.GraduationApplication.MailDiplomaToAddressLines[0]);
                      if (MailDiplomaToAddressLines.length > 1) { gradView.AddressInfo().Street2(data.GraduationApplication.MailDiplomaToAddressLines[1]) };
                      if (MailDiplomaToAddressLines.length > 2) { gradView.AddressInfo().Street3(data.GraduationApplication.MailDiplomaToAddressLines[2]) };
                      if (MailDiplomaToAddressLines.length > 3) { gradView.AddressInfo().Street4(data.GraduationApplication.MailDiplomaToAddressLines[3]) };
                      gradView.AddressInfo().City(data.GraduationApplication.MailDiplomaToCity);
                      gradView.AddressInfo().PostalCode(data.GraduationApplication.MailDiplomaToPostalCode);


                      if (data.GraduationApplication.MailDiplomaToAddressLines.length > 2 || data.GraduationApplication.MailDiplomaToState == "") {
                          gradView.AddressInfo().internationalSelected(true);
                      }
                  }
               };


               //Each following if statement sets the drop down boxes to the option selected in the application
               if (GraduationTerm !== null && typeof GraduationTerm !== 'undefined'
                                     && gradView.GraduationTerms() !== null && typeof gradView.GraduationTerms() !== 'undefined') {
                    var gt = ko.utils.arrayFirst(gradView.GraduationTerms(), function (item) {
                         return item.Code() === GraduationTerm;
                    });
                    gradView.GraduationTermSelected(gt);
               }
               if (CommencementLocation !== null && typeof CommencementLocation !== 'undefined'
                                     && gradView.CommencementSites() !== null && typeof gradView.CommencementSites() !== 'undefined') {
                    var cs = ko.utils.arrayFirst(gradView.CommencementSites(), function (item) {
                         return item.Code() === CommencementLocation;
                    });
                    gradView.CommencementSiteSelected(cs);
               }
               if (MailDiplomaToState !== null && typeof MailDiplomaToState !== 'undefined'
                                     && gradView.AddressInfo().States() !== null && typeof gradView.AddressInfo().States() !== 'undefined') {
                    var s = ko.utils.arrayFirst(gradView.AddressInfo().States(), function (item) {
                         return item.Code === MailDiplomaToState;
                    });
                    gradView.AddressInfo().State(s);
               }
               if (MailDiplomaToCountry !== null && typeof MailDiplomaToCountry !== 'undefined'
                                     && gradView.AddressInfo().Countries() !== null && typeof gradView.AddressInfo().Countries() !== 'undefined') {
                    var c = ko.utils.arrayFirst(gradView.AddressInfo().Countries(), function (item) {
                         return item.Code === MailDiplomaToCountry;
                    });
                    gradView.AddressInfo().Country(c);
               }
               if (CapSize !== null && typeof CapSize !== 'undefined'
                                     && gradView.CapSizes() !== null && typeof gradView.CapSizes() !== 'undefined') {
                    var cs = ko.utils.arrayFirst(gradView.CapSizes(), function (item) {
                         return item.Code() === CapSize;
                    });
                    gradView.CapSizeSelected(cs);
               }
               if (GownSize !== null && typeof GownSize !== 'undefined'
                                     && gradView.GownSizes() !== null && typeof gradView.GownSizes() !== 'undefined') {
                    var gs = ko.utils.arrayFirst(gradView.GownSizes(), function (item) {
                         return item.Code() === GownSize;
                    });
                    gradView.GownSizeSelected(gs);
               }
               if (MilitaryStatus !== null && typeof MilitaryStatus !== 'undefined'
                                && gradView.MilitaryStatuses() !== null && typeof gradView.MilitaryStatuses() !== 'undefined') {
                    var gs = ko.utils.arrayFirst(gradView.MilitaryStatuses(), function (item) {
                         return item.Code() === MilitaryStatus;
                    });
                    gradView.MilitaryStatusSelected(gs);
               }
               if (PrimaryLocation !== null && typeof PrimaryLocation !== 'undefined' && PrimaryLocation !== ""
                                               && gradView.PrimaryLocations() !== null && typeof gradView.PrimaryLocations() !== 'undefined') {
                    var pl = ko.utils.arrayFirst(gradView.PrimaryLocations(), function (item) {
                         return item.Code() === PrimaryLocation;
                    });
                    gradView.PrimaryLocationSelected(pl);
               }
          }
          //set viewOnlyMode to true AFTER everything has been loaded
          if (gradView.GraduationTermSelected() != null && gradView.GraduationTermSelected() != 'undefined' && gradView.IsAcademicCredentialsUpdated() === false) {
               gradView.viewOnlyMode(false);
               gradView.editOnlyMode(true);
          }
          else {
               if (gradView.SpecialAccommodations() !== null && gradView.SpecialAccommodations() !== 'undefined' && gradView.SpecialAccommodations().length > 0) {
                    gradView.SpecialAccommodations(gradView.SpecialAccommodations().replace(new RegExp('\r?\n', 'g'), '<br />'));
               }
               gradView.viewOnlyMode(true);
               gradView.editOnlyMode(false);
          }
          gradView.bindingComplete(true);
     }
     catch (e) {
          console.error(e.message);
          $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve graduation information", type: "error" });
          gradView.viewOnlyMode(true);
          gradView.editOnlyMode(false);
     }
}

