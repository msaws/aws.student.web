﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function baseStudentRequestCollectorModel(collector) {
     var self = this;
    try
    {
        self.Recipient = encodeURIComponent(collector.Recipient().trim());
        //  self.Recipient = collector.Recipient().trim().replace(/</g,"&lt;").replace(/>/g,"&gt;");
        //Obtain Address Information
        self.MailToAddressLines = [];
        if (collector.AddressInfo().Street1() !== null && typeof collector.AddressInfo().Street1() !== 'undefined') {
            self.MailToAddressLines.push(encodeURIComponent(collector.AddressInfo().Street1()));
        }
        if (collector.AddressInfo().Street2() !== null && typeof collector.AddressInfo().Street2() !== 'undefined') {
            self.MailToAddressLines.push(encodeURIComponent(collector.AddressInfo().Street2()));
        }
        if (collector.AddressInfo().Street3() !== null && typeof collector.AddressInfo().Street3() !== 'undefined') {
            self.MailToAddressLines.push(encodeURIComponent(collector.AddressInfo().Street3()));
        }
        if (collector.AddressInfo().Street4() !== null && typeof collector.AddressInfo().Street4() !== 'undefined') {
            self.MailToAddressLines.push(encodeURIComponent(collector.AddressInfo().Street4()));
        }
        if (typeof collector.AddressInfo().City() !== 'undefined' && collector.AddressInfo().City() !== null) {
            self.MailToCity = encodeURIComponent(collector.AddressInfo().City().trim());
        }
        if (typeof collector.AddressInfo().PostalCode() !== 'undefined' && collector.AddressInfo().PostalCode() !== null) {
            self.MailToPostalCode = encodeURIComponent(collector.AddressInfo().PostalCode().trim());
        }
        if (typeof collector.AddressInfo().State() !== 'undefined' && collector.AddressInfo().State() !== null && collector.AddressInfo().State() != "") {
            self.MailToState = collector.AddressInfo().State().Code;
        }
        if (typeof collector.AddressInfo().Country() !== 'undefined' && collector.AddressInfo().Country() !== null && collector.AddressInfo().Country() != "") {
            self.MailToCountry = collector.AddressInfo().Country().Code;
        }
          
        self.NumberOfCopies = collector.NumberOfCopiesSelected();
        if (typeof collector.Comments() !== 'undefined' && collector.Comments() !== null) {
            self.Comments =encodeURIComponent( collector.Comments().trim());
        }

        self.RequireImmediatePayment = collector.RequireImmediatePayment();
       
     }
     catch(exception){
          //let them know that you did something wrong
        //  self.success = false;
     }

}
