﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

//initialize the Viewmodel
var requestInfo = new TranscriptRequestsViewModel();

$(document).ready(function () {
    ko.applyBindings(requestInfo, document.getElementById('enrollment-request-info'));
     //Loads the data into the form
    getTranscriptRequestInfo();

});

function getTranscriptRequestInfo() {
     $.ajax({
         url: getExistingTranscriptRequestsUrl,
          type: "GET",
          contentType: jsonContentType,
          dataType: "json",
          success: function (data) {
               if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, {}, requestInfo);
               }
          },
          error: function (jqXHR, textStatus, errorThrown) {
               if (jqXHR.status !== 0) {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.TranscriptRequests.transcriptInfoGetFailure, type: "error" });
               }

          },
          complete: function (jqXHR, textStatus) {
               requestInfo.isLoaded(true);
               $("#student-requests-table").makeTableResponsive();
               if (notify === "TranscriptRequestSubmitFailure") {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.TranscriptRequests.unableToSubmitNewTranscriptRequestMessage, type: "error" });
               }
               if (notify === "TranscriptRequestPaymentFailure") {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.TranscriptRequests.unableToSubmitTranscriptRequestPayment, type: "error" });
               }
               if (notify === "TranscriptRequestSubmitSuccess") {
                   var emailStatement = !isNullOrEmpty(emailAddress) ? " " + Ellucian.Student.Requests.requestEmailConfirmation + " " + emailAddress : "";
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.TranscriptRequests.newTranscriptRequestSuccessMessage + emailStatement, type: "success" });
               }
               notify = "";
          }
     });
}