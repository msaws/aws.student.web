﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function TranscriptRequestPaymentModel()
    {
    var self = this;
    //"Inherit" the payment model 
    BaseStudentRequestPaymentModel.call(self);
    self.isUpdating = ko.observable(false);
    //Function that triggers creation of a new transcript request
    self.submitTranscriptRequestPayment = function () {
        ko.validation.group(self, { deep: true });
        if (self.isValid()) {
            var jsonData = { 'paymentModelJson': ko.toJSON(self) };
            //post the data
            $.ajax({
                url: createTranscriptRequestPaymentUrl,
                data: JSON.stringify(jsonData),
                type: "POST",
                dataType: "json",
                contentType: jsonContentType,
                beforeSend: function (data) {
                    self.isUpdating(true);
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status == 202) {
                        // check for payment details 
                        if (data.MakeAPaymentModel !== null) {
                            self.showPayment(data);
                        }

                        else {
                            window.location = newTranscriptRequestFormUrl + '?Notify=TranscriptRequestPaymentFailure';
                        }
                    }
                },
                error: function (jqXHR, textStatus,errorThrown) {
                    self.isUpdating(false);
                    // redirect to main page and show negative message
                    window.location = newTranscriptRequestFormUrl + '?Notify=TranscriptRequestPaymentFailure';
                }
            });
        }

            //required data is missing, highlight errors
        else {
            self.hasInputErrors(true);
            $(".required-input").first().focus();
        }
    };

}