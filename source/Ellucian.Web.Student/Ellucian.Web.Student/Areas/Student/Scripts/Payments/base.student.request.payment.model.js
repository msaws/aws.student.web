﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function BaseStudentRequestPaymentModel()
    {
    var self = this;
    ko.validation.init({
        messagesOnModified: true,
        insertMessages: false,
        decorateInputElement: false
    });

    self.hasInputErrors = ko.observable(false);
   // ko.mapping.fromJS(modelData, {}, self);
    self.PaymentMethodSelected = ko.observable(null);
    self.PaymentDistributionCode = ko.observable(null);
    self.NumberOfCopies = ko.observable(1); 
    self.RequireImmediatePayment = ko.observable(false);
    self.PaymentMethods = ko.observableArray();
    self.InvoiceNumber = ko.observable();

    self.PaymentMethodSelected = ko.observable(null).extend({ required: true });

    self.AmountDue = ko.observable();

    //Function that is triggered if ShowPaymentDetails flag is set to true on submitApp
    self.showPayment = function (jsonObject) {
        var makeAPaymentModel = jsonObject.MakeAPaymentModel;
        makeAPaymentModel.AlertMessage = sanitizeFormData(makeAPaymentModel.AlertMessage);
        ko.utils.postJson(mapPaymentReviewActionUrl, { model: makeAPaymentModel, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
    };
}