﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function GraduationPaymentModel() {
    var self = this;
    this.PaymentMethodSelected = ko.observable("");
    this.DistributionCode = ko.observable();
    this.InvoiceId = ko.observable();
    this.PaymentMethods = ko.observableArray();
    this.RequireImmediatePayment = ko.observable(false);
    this.GraduationFeesAmount = ko.observable(0);
    this.ProgramCode = ko.observable("");

    this.PaymentMethodSelected = ko.observable("").extend(
        {
            required: {
                onlyIf: function () {
                    return self.RequireImmediatePayment() && self.GraduationFeesAmount() > 0 && !gradView.viewOnlyMode() && !gradView.editOnlyMode()
                }
            }
        });


    this.ShowPaymentDetails = ko.computed(function () {
        if (self.RequireImmediatePayment() && (self.GraduationFeesAmount() > 0) && !isNullOrEmpty(self.DistributionCode())) {
            return true;
        }
        else {
            return false;
        }
    });
}