﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

var transcriptRequestPaymentModelInstance = new TranscriptRequestPaymentModel();

$(document).ready(function () {
    ko.mapping.fromJS(jsonData, {}, transcriptRequestPaymentModelInstance);
    ko.applyBindings(transcriptRequestPaymentModelInstance, document.getElementById("main"));
   
});

