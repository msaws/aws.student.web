﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

var enrollmentRequestPaymentModelInstance = new EnrollmentRequestPaymentModel();

$(document).ready(function () {
    ko.mapping.fromJS(jsonData, {}, enrollmentRequestPaymentModelInstance);
    ko.applyBindings(enrollmentRequestPaymentModelInstance, document.getElementById("main"));
   
});

