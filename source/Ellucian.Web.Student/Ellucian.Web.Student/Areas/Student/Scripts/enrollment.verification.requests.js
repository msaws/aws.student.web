﻿//Copyright 2016 Ellucian COmpany L.P. and its affiliates

var enrollmentVerificationRequestsViewModelInstance = new enrollmentVerificationRequestsViewModel();
$(document).ready(function () {
     ko.components.register('address-entry', {
          require: 'AddressEntry/_AddressEntry'
     });
     getEnrollmentVerificationRequestsInfo();
     ko.applyBindings(enrollmentVerificationRequestsViewModelInstance, document.getElementById("main"));
});

function getEnrollmentVerificationRequestsInfo() {
     $.ajax({
          url: getEnrollmentVerificationRequestsInfoUrl,
          type: "GET",
          contentType: jsonContentType,
          dataType: "json",
          success: function (data) {
               if (!account.handleInvalidSessionResponse(data)) {
                    try {
                        ko.mapping.fromJS(data, {}, enrollmentVerificationRequestsViewModelInstance);
                         enrollmentVerificationRequestsViewModelInstance.AddressInfo().States(data.States);
                         enrollmentVerificationRequestsViewModelInstance.AddressInfo().Countries(data.Countries);
                    }
                    catch (e) {
                         console.error(e.message);
                         $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Requests.enrollmentVerificationInfoGetFailure, type: "error" });
                    }
               }
          },
          error: function (jqXHR, textStatus, errorThrown) {
              if (jqXHR.status != 0) {
                  $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Requests.enrollmentVerificationInfoGetFailure, type: "error" });
              }
          },
          complete: function (jqXHR, textStatus) {
              enrollmentVerificationRequestsViewModelInstance.isLoaded(true);
          }

          
     });
}