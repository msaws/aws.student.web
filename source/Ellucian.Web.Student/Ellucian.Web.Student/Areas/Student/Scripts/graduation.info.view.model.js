﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function GraduationInfoViewModel() {
    var self = this;
    self.isLoaded = ko.observable(false);
    self.ShowMyProgressLink = ko.observable();
    self.Programs = ko.observableArray();
    self.AcceptingNewApplications = ko.observable(false);

    //links
    self.CommencementInformationLink = ko.observable();
    self.ApplyForDifferentProgramLink = ko.observable();
    //click handler
    self.programLinkClick = function (context) {
        window.location = Ellucian.Student.Graduation.ActionUrls.graduationApplicationUrl + "?programCode=" + encodeURIComponent(context.Code()) + "&applicationExists=" + context.IsGraduationApplicationSubmitted();
        return;
    };
};