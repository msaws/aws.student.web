﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

//Request view model => holds general student request's data
//for display
function baseStudentRequestViewModel() {

    var self = this;

    ko.validation.init({
        messagesOnModified: true,
        insertMessages: false,
        decorateInputElement: false
    });

    //Is this a transcript request?
    self.isTranscriptRequest = ko.observable(false);
    self.hasInputErrors = ko.observable(false);

    //spinner to show until loaded
    self.isLoaded = ko.observable(false);

    //Observable to hold address info - value is an AddressModel object
    //from components library
    self.AddressInfo = ko.observable(new AddressModel());

    self.Recipient = ko.observable("").extend({ required: true });
    self.MaximumNumberOfCopies = ko.observable(9);
    self.NumberOfCopiesSelected = ko.observable(1);
    self.NumberOfCopiesSelected.extend({
        required: true,
        min: 1,
        max: 9,
        validation: [{
            validator: function (value) {
                try {
                    if ((self.NumberOfCopiesSelected() > 0) && (self.NumberOfCopiesSelected() % 1) != 0)
                        return false;
                    return self.NumberOfCopiesSelected() <= self.MaximumNumberOfCopies();
                } catch (e) {
                    return false;
                }
            },
            message: "Number is out of range"
        }]

    });

    self.Comments = ko.observable();
    self.RequireImmediatePayment = ko.observable(false);
 //   self.PaymentModel = ko.observable(new StudentRequestPaymentModel(self.NumberOfCopiesSelected));//passing observable numberOfCopies
   // self.PaymentMethods = ko.observableArray();


    self.ShowPaymentDetails = ko.computed(function () {
        if (self.RequireImmediatePayment() ) {
            return true;
        }
        else {
            return false;
        }
    });


    
   

    //Cleans address data that is not required for intenational addresses before submitting the request
    self.cleanupAddressInfoBeforeSubmit = function () {
        if (!self.AddressInfo().internationalSelected() && self.AddressInfo().State() !== null) {
            var c = ko.utils.arrayFirst(self.AddressInfo().Countries(), function (item) {
                return item.Code === self.AddressInfo().State().CountryCode;
            });
            self.AddressInfo().Country(c);
            self.AddressInfo().Street3(null);
            self.AddressInfo().Street4(null);
        }
        else {
            self.AddressInfo().City(null);
            self.AddressInfo().State(null);
            self.AddressInfo().PostalCode(null);
        }
    }
}