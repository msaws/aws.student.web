﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
function StatusViewModel() {
    var self = this;
    self.dummy = ko.observable(false);
};
var statusInfo = new StatusViewModel();

$(document).ready(function () {
    ko.applyBindings(statusInfo, document.getElementById('graduation-status'));
});


