﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
function GraduationAddViewModel(ProgramCode, collector) {
     var self = this;
     try {
          self.ProgramCode = ProgramCode;
          self.GraduationTerm = collector.GraduationTermSelected().Code();
          if (!isNullOrEmpty(collector.DiplomaName()))
               self.DiplomaName = collector.DiplomaName().trim();
          if (!isNullOrEmpty(collector.PhoneticSpellingOfName()))
               self.PhoneticSpellingOfName = collector.PhoneticSpellingOfName().trim();
          if (!isNullOrEmpty(collector.Hometown()))
               self.Hometown = collector.Hometown().trim();

          //Commencement Details
          if (typeof collector.CommencementSiteSelected() !== 'undefined' && collector.CommencementSiteSelected() !== null && collector.CommencementSiteSelected() !== "")
               self.CommencementLocation = collector.CommencementSiteSelected().Code();
          self.AttendingCommencement = collector.AttendingCommencement();
          self.WillPickupDiploma = collector.WillPickupDiploma();
          self.IncludeNameInProgram = collector.IncludeNameInProgram();
          self.NumberOfGuests = collector.NumberOfGuests();

          //Address given, null if preferred address
          self.MailDiplomaToAddressLines = [];
          if (collector.AddressInfo().Street1() !== null && typeof collector.AddressInfo().Street1() !== 'undefined' && collector.AddressInfo().Street1()!=="") {
               self.MailDiplomaToAddressLines.push(collector.AddressInfo().Street1());
          }
          if (collector.AddressInfo().Street2() !== null && typeof collector.AddressInfo().Street2() !== 'undefined' && collector.AddressInfo().Street2()!="") {
               self.MailDiplomaToAddressLines.push(collector.AddressInfo().Street2());
          }
          if (collector.AddressInfo().Street3() !== null && typeof collector.AddressInfo().Street3() !== 'undefined' && collector.AddressInfo().Street3()!="") {
               self.MailDiplomaToAddressLines.push(collector.AddressInfo().Street3());
          }
          if (collector.AddressInfo().Street4() !== null && typeof collector.AddressInfo().Street4() !== 'undefined' && collector.AddressInfo().Street4()!="") {
               self.MailDiplomaToAddressLines.push(collector.AddressInfo().Street4());
          }
          self.MailDiplomaToCity = collector.AddressInfo().City();
          self.MailDiplomaToPostalCode = collector.AddressInfo().PostalCode();
          if (typeof collector.AddressInfo().State() !== 'undefined' && collector.AddressInfo().State() !== null && collector.AddressInfo().State() !== "")
               self.MailDiplomaToState = collector.AddressInfo().State().Code;
          if (typeof collector.AddressInfo().Country() !== 'undefined' && collector.AddressInfo().Country() !== null && collector.AddressInfo().Country() !== "")
               self.MailDiplomaToCountry = collector.AddressInfo().Country().Code;

          //Cap and gown size chosen
          if (typeof collector.CapSizeSelected() !== 'undefined' && collector.CapSizeSelected() !== null && collector.CapSizeSelected() !== "")
               self.CapSize = collector.CapSizeSelected().Code();
          if (typeof collector.GownSizeSelected() !== 'undefined' && collector.GownSizeSelected() !== null && collector.GownSizeSelected() !== "")
               self.GownSize = collector.GownSizeSelected().Code();

          // Military status and accommodations
          if (typeof collector.MilitaryStatusSelected() !== 'undefined' && collector.MilitaryStatusSelected() !== null && collector.MilitaryStatusSelected() !== "")
               self.MilitaryStatus = collector.MilitaryStatusSelected().Code();
          self.SpecialAccommodations = collector.SpecialAccommodations();

          // Primary Location
          if (typeof collector.PrimaryLocationSelected() !== 'undefined' && collector.PrimaryLocationSelected() !== null && collector.PrimaryLocationSelected() !== "")
               self.PrimaryLocation = collector.PrimaryLocationSelected().Code();

     }
     catch (exception) {
          //let them know that they passed in something wrong
          self.success = false;
     }
}