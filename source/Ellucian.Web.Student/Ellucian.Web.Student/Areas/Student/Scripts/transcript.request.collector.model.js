﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function transcriptRequestCollectorModel(data) {

    var self = this;
    //"Inherit" studentRequestModel
    baseStudentRequestCollectorModel.call(self, data);

    if(typeof data.TranscriptType() !== 'undefined' && data.TranscriptType() !== null) self.TranscriptGrouping = data.TranscriptType().Id();
    if (typeof data.TranscriptHoldType() !== 'undefined' && data.TranscriptHoldType() !== null) self.HoldRequestCode = data.TranscriptHoldType().Code();
}