﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

var transcriptRequestsViewModelInstance = new transcriptRequestsViewModel();

$(document).ready(function () {
    ko.components.register('address-entry', {
        require: 'AddressEntry/_AddressEntry'
    });
    getTranscriptRequestsInfo();
    ko.applyBindings(transcriptRequestsViewModelInstance, document.getElementById("main"));
   
});

//Gets transcript requests info
function getTranscriptRequestsInfo() {
    $.ajax({
        url: getTranscriptRequestsInfoUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                try {
                    ko.mapping.fromJS(data, {}, transcriptRequestsViewModelInstance);
                    transcriptRequestsViewModelInstance.AddressInfo().States(data.States);
                    transcriptRequestsViewModelInstance.AddressInfo().Countries(data.Countries);
                    transcriptRequestsViewModelInstance.isTranscriptRequest(true);
                }
                catch (e) {
                    console.error(e.message);
                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.TranscriptRequests.transcriptInfoGetFailure, type: "error" });
                }                
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.TranscriptRequests.transcriptInfoGetFailure, type: "error" });
            }

        },
        complete: function (jqXHR, textStatus)
        {
            transcriptRequestsViewModelInstance.checkForMobile(window, document);
            transcriptRequestsViewModelInstance.isLoaded(true);
        }
    });
}