﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.

function studentGradesViewModel() {
    var self = this;
    this.retrieved = ko.observable(false);
    this.StudentTermGrades = ko.observableArray();
    this.NumberOfMidtermGradesToShow = ko.observable(0);
    this.Notifications = ko.observableArray();
}