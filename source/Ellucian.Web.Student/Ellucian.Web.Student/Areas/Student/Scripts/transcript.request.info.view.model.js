﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
function TranscriptRequestsViewModel() {
    var self = this;
    self.isLoaded = ko.observable(false);
    self.Requests = ko.observableArray();
    self.RequestRestrictionMessages = ko.observableArray();

};