﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.

var studentGradingInstance = new studentGradesViewModel();

$(document).ready(function () {

    // bind studentGradesViewModelInstance to the view of the student grades page
    ko.applyBindings(studentGradingInstance, document.getElementById("student-grades"));
    getStudentGradeInfo();

});

function getStudentGradeInfo() {
    $.ajax({
        url: getStudentGradeInfoUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, gradeMapping, studentGradingInstance);
                // set first term to show true - rest will be collapsed by default.
                if (studentGradingInstance.StudentTermGrades().length > 0) {
                    studentGradingInstance.StudentTermGrades()[0].showTermDetail(true);
                }
                studentGradingInstance.retrieved(true);
                $(".student-grade-table").makeTableResponsive();
                // If grade report has any attached notifications, add them to the notification center
                for (var j = 0; j < studentGradingInstance.Notifications().length; j++) {
                    var notification = ko.toJS(studentGradingInstance.Notifications()[j]);
                    $('#notificationHost').notificationCenter('addNotification', notification);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                studentGradingInstance.retrieved(true);
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve student grades", type: "error" });
            }
        },
        complete: function (jqXHR, textStatus) {

        }
    });
}