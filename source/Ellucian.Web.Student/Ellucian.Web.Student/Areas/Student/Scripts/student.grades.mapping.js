﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
// planMapping is used to map the JSON returned by the UI server to 
// objects used in the knockoutJS data binding.
var gradeMapping = {
    'StudentTermGrades': {
        create: function (options) {
            return new StudentTermGrades(options.data);
        }
    },
    'StudentGrades': {
        create: function (options) {
            return new StudentGrades(options.data);
        }
    }
};

function StudentTermGrades(data) {
    ko.mapping.fromJS(data, gradeMapping, this);

    var self = this;

    this.showTermDetail = ko.observable(false);

    this.esgCollapsedClass = ko.computed(function () {
        if (self.showTermDetail()) {
            return "esg-is-open";
        }
        return "esg-is-collapsed";
    });

    this.svgIconClass = ko.computed(function () {
        if (self.showTermDetail()) {
            return "esg-icon--up";
        }
        return "esg-icon--down";
    });

    this.ToggleTermDetail = function () {
        self.showTermDetail(!self.showTermDetail());
    }
};

function StudentGrades(data) {
    ko.mapping.fromJS(data, {}, this);
    var self = this;
};