﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

//initialize the Viewmodel
var requestInfo = new StudentRequestsViewModel();

$(document).ready(function () {
    ko.applyBindings(requestInfo, document.getElementById('enrollment-request-info'));
     //Loads the data into the form
    getEnrollmentRequestInfo();

});

function getEnrollmentRequestInfo() {
     $.ajax({
         url: getExistingEnrollmentRequestsUrl,
          type: "GET",
          contentType: jsonContentType,
          dataType: "json",
          success: function (data) {
               if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, {}, requestInfo);
               }
          },
          error: function (jqXHR, textStatus, errorThrown) {
               if (jqXHR.status !== 0) {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Requests.enrollmentVerificationInfoGetFailure, type: "error" });
               }

          },
          complete: function (jqXHR, textStatus) {
               requestInfo.isLoaded(true);
               $("#enrollment-requests-table").makeTableResponsive();
               if (notify === "EnrollmentRequestSubmitFailure") {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Requests.enrollmentVerificationRequestSubmissionFailure, type: "error" });
               }
               if (notify === "EnrollmentRequestPaymentFailure") {
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Requests.unableToSubmitEnrollmentRequestPayment, type: "error" });
               }
               if (notify === "EnrollmentRequestSubmitSuccess") {
                   var emailStatement = !isNullOrEmpty(emailAddress) ? " " + Ellucian.Student.Requests.requestEmailConfirmation + " " + emailAddress : "";
                   $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Student.Requests.enrollmentVerificationRequestSubmissionSuccess + emailStatement, type: "success" });
               }
               notify = "";
          }
     });
}