﻿using Ellucian.Colleague.Dtos.Finance.Configuration;
//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using System;
using System.Threading.Tasks;
namespace Ellucian.Web.Student.Areas.Student.Utility
{
    public interface IStudentHelper
    {

        /// <summary>
        /// to generate payment model
        /// </summary>
        /// <param name="studentRequestPaymentModel"></param>
        /// <param name="studentId"></param>
        /// <param name="ServiceClient"></param>
        /// <returns></returns>
        Task<Ellucian.Web.Student.Areas.Finance.Models.Payments.MakeAPaymentModel> BuildMakeAPaymentModelAsync(StudentRequestPaymentModel studentRequestPaymentModel, string studentId, Ellucian.Colleague.Api.Client.ColleagueApiClient ServiceClient);
        /// <summary>
        /// to retrieve email address
        /// </summary>
        /// <param name="student"></param>
        /// <param name="studentRequestConfiguration"></param>
        /// <param name="requestType"></param>
        /// <returns></returns>
        string GetEmailAddress(Ellucian.Colleague.Dtos.Student.Student student, Ellucian.Colleague.Dtos.Student.StudentRequestConfiguration studentRequestConfiguration, StudentRequestType requestType);
        /// <summary>
        /// Make a copy of the finance configuration before modifying it.
        /// </summary>
        /// <param name="config">Finance Configuration Cache Reference object</param>
        /// <returns></returns>
        FinanceConfiguration OverrideFinanceConfigurationOptions(FinanceConfiguration config);
    }
}
