﻿//Copyright 2016 Ellucian Company L.P. and its affiliatesusing System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Utility
{
    public enum StudentRequestType
    {
        TranscriptRequest,
        EnrollmentVerificationRequest
    }
}