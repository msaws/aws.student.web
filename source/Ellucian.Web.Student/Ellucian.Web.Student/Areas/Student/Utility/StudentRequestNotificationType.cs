﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Utility
{
     [JsonConverter(typeof(StringEnumConverter))]
    public enum StudentRequestNotificationType
    {
        TranscriptRequestSubmitSuccess,
        TranscriptRequestSubmitFailure,
        TranscriptRequestPaymentSuccess,
        TranscriptRequestPaymentFailure,
        EnrollmentRequestSubmitSuccess,
        EnrollmentRequestSubmitFailure,
        EnrollmentRequestPaymentSuccess,
        EnrollmentRequestPaymentFailure

    }
}