﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

using Ellucian.Web.Student.Areas.Student.Models.StudentRequests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Ellucian.Web.Student.Areas.Student.Utility
{
    /// <summary>
    /// Custom JSON data model binder
    /// this will bind student request JSON data to model
    /// </summary>
    public class StudentRequestJsonBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            BaseStudentRequestModel request = null;
            var stringified = controllerContext.HttpContext.Request[bindingContext.ModelName];
            if (string.IsNullOrEmpty(stringified))
                return null;
            if (bindingContext.ModelType == typeof(TranscriptRequestModel))
            {
                request = serializer.Deserialize(stringified, bindingContext.ModelType) as TranscriptRequestModel;

            }
            else if (bindingContext.ModelType == typeof(EnrollmentVerificationRequestModel))
            {
                request = serializer.Deserialize(stringified, bindingContext.ModelType) as EnrollmentVerificationRequestModel;
            }
            else
            {
                return serializer.Deserialize(stringified, bindingContext.ModelType);
            }
                request.Recipient = HttpUtility.UrlDecode(request.Recipient);
                request.Comments = HttpUtility.UrlDecode(request.Comments);
            if (request.MailToAddressLines != null)
            {
                request.MailToAddressLines = request.MailToAddressLines.Select(a => HttpUtility.UrlDecode(a)).ToList<string>();
            }
                request.MailToCity = HttpUtility.UrlDecode(request.MailToCity);
                request.MailToPostalCode = HttpUtility.UrlDecode(request.MailToPostalCode);
            return request;
        }
    }
}