﻿//Copyright 2017 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Areas.Student.Models.Payments;
using Ellucian.Web.Student.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Ellucian.Web.Student.Areas.Student.Utility
{
    public class StudentHelper : Ellucian.Web.Student.Areas.Student.Utility.IStudentHelper
    {


        public string GetEmailAddress(Colleague.Dtos.Student.Student student, Colleague.Dtos.Student.StudentRequestConfiguration studentRequestConfiguration, StudentRequestType requestType)
        {
            var emailAddress = string.Empty;
            if (student == null || studentRequestConfiguration == null)
            {
                return string.Empty;
            }
            bool toSendEmailConfirmation = requestType.Equals(StudentRequestType.TranscriptRequest) ? studentRequestConfiguration.SendTranscriptRequestConfirmation : requestType.Equals(StudentRequestType.EnrollmentVerificationRequest) ? studentRequestConfiguration.SendEnrollmentRequestConfirmation : false;
            if (toSendEmailConfirmation)
            {
                var preferredEmailType = studentRequestConfiguration.DefaultWebEmailType;
                var emailAddressObject = !string.IsNullOrEmpty(preferredEmailType) && student.EmailAddresses != null ? student.EmailAddresses.Where(p => p.TypeCode == preferredEmailType).FirstOrDefault() : null;
                if (emailAddressObject == null)
                {
                    emailAddress = student.PreferredEmailAddress;
                }
                else
                {
                    emailAddress = emailAddressObject.Value;
                }
            }
            return emailAddress;
        }
        /// <summary>
        /// Make changes to finance configuration copy to account for the graduation, transcript request and enrollment request payments.
        /// </summary>
        /// <param name="config">Original Configuration</param>
        /// <returns></returns>
        public FinanceConfiguration OverrideFinanceConfigurationOptions(FinanceConfiguration config)
        {

            string configurationString = JsonConvert.SerializeObject(config);
            var configClone = JsonConvert.DeserializeObject<FinanceConfiguration>(configurationString);
            configClone.PartialAccountPaymentsAllowed = false;
            configClone.PartialDepositPaymentsAllowed = false;
            configClone.PartialPlanPaymentsAllowed = PartialPlanPayments.Denied;
            configClone.PaymentDisplay = PaymentDisplay.DisplayByTerm;
            return configClone;
        }

        public async Task<MakeAPaymentModel> BuildMakeAPaymentModelAsync(StudentRequestPaymentModel studentRequestPaymentModel, string studentId, Colleague.Api.Client.ColleagueApiClient ServiceClient)
        {
            try
            {
                MakeAPaymentModel makeAPaymentModel = null;
                string invoiceId = studentRequestPaymentModel.InvoiceNumber;
                AccountHolder accountHolder = ServiceClient.GetAccountHolder2(studentId);
                FinanceConfiguration config = await ServiceClient.GetCachedFinanceConfiguration();
                var configurationCopy = OverrideFinanceConfigurationOptions(config);
                var activeRestrictions = new List<PersonRestriction>();
                IEnumerable<Invoice> invoices = ServiceClient.GetInvoices(new List<string>() { studentRequestPaymentModel.InvoiceNumber });
                if (invoices != null && invoices.Any())
                {
                    InvoiceDue invoicesForTerms = new InvoiceDue(studentRequestPaymentModel.PaymentDistributionCode, null, invoices.ToList<Invoice>());
                    // Build the make a payment term model
                    makeAPaymentModel = MakeAPaymentModel.Build(invoicesForTerms, configurationCopy, accountHolder, studentId);
                    makeAPaymentModel.SelectedPaymentMethod = studentRequestPaymentModel.PaymentMethodSelected;
                }
                else
                {
                    throw new Exception(string.Format("No invoice details found for the invoice # {0}", studentRequestPaymentModel.InvoiceNumber));
                }
                return makeAPaymentModel;
            }
            catch
            {
                throw;
            }
        }
    }
}