﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Student.Licensing;
using System.ComponentModel;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.TimeManagement
{
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.TimeManagement)]
    public class TimeManagementAreaRegistration : LicenseAreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TimeManagement";
            }
        }

        public override void LicensedRegistration(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TimeManagement_default",
                "TimeManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "Ellucian.Web.Student.Areas.TimeManagement.Controllers" }
            );
        }
    }
}