﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.TimeManagement.Controllers
{

    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public abstract class SharedController : BaseStudentController
    {
        public SharedController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        [HttpPost]
        [PageAuthorize("timeSheet", "timeApproval")]
        public async Task<ActionResult> SubmitTimecardStatusAsync(string timecardStatusJson)
        {

            int? newStatus = null;
            try
            {
                if (string.IsNullOrWhiteSpace(timecardStatusJson))
                {
                    var message = "TimecardStatusJson can not be null, empty, or whitespace";
                    Logger.Error(message);
                    throw new ArgumentNullException("timecardStatusJson");
                }

                TimecardStatus timecardStatus = JsonConvert.DeserializeObject<TimecardStatus>(timecardStatusJson);
                timecardStatus.ActionerId = GetAuthenticatedPersonId();
                timecardStatus.Timestamp = new Timestamp();

                var response = await ServiceClient.CreateTimecardStatusAsync(timecardStatus);
                newStatus = (int)response.ActionType;
            }
            catch (Exception exception)
            {
                Logger.Error(exception, exception.Message);
                var errorMessage = exception.Message + Environment.NewLine + exception.StackTrace;
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: errorMessage), JsonRequestBehavior.AllowGet);
            }

            return Json(new StandardJsonResult(newStatus), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [PageAuthorize("timeSheet", "timeApproval")]
        public async Task<ActionResult> SubmitTimecardStatusesAsync(string timecardStatusesJson)
        {
            var returnCode = HttpStatusCode.OK;
            var errorMessage = "";
            int? newStatus = null;
            try
            {
                if (string.IsNullOrWhiteSpace(timecardStatusesJson)) throw new ArgumentNullException("timecardStatusJson");

                List<TimecardStatus> timecardStatuses = JsonConvert.DeserializeObject<List<TimecardStatus>>(timecardStatusesJson);

                var ActionerId = GetAuthenticatedPersonId();
                foreach (var status in timecardStatuses)
                {
                    status.ActionerId = ActionerId;
                }
                var responses = await ServiceClient.CreateTimecardStatusesAsync(timecardStatuses);
                newStatus = (int)responses.ElementAt(0).ActionType;
                foreach (var response in responses)
                {
                    if ((int)response.ActionType < newStatus) newStatus = (int)response.ActionType;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception, exception.Message);
                returnCode = HttpStatusCode.BadRequest;
                errorMessage = exception.Message + Environment.NewLine + exception.StackTrace;
            }

            return Json(new StandardJsonResult(newStatus, returnCode, errorMessage), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Post a new Comment to the Api
        /// </summary>
        /// <param name="commentModelJson"></param>
        /// <returns></returns>
        [HttpPost]
        [PageAuthorize("timeSheet", "timeApproval")]
        public async Task<JsonResult> CreateTimeEntryCommentsAsync(string commentModelJson)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(commentModelJson))
                {
                    var message = "commentModelJson can not be null, empty, or white space";
                    Logger.Error(message);
                    throw new ArgumentNullException("commentsEntryJson");
                }

                var commentModel = JsonConvert.DeserializeObject<CommentModel>(commentModelJson);
                var commentsDto = new TimeEntryComments()
                {
                    EmployeeId = commentModel.EmployeeId,
                    TimecardId = commentModel.TimecardId,
                    TimecardStatusId = commentModel.TimecardStatusId,
                    PositionId = commentModel.PositionId,
                    PayCycleId = commentModel.PayCycleId,
                    PayPeriodEndDate = commentModel.PayPeriodEndDate,
                    Comments = commentModel.CommentText,
                };

                var createdCommentsDto = await ServiceClient.CreateTimeEntryCommentsAsync(commentsDto);

                if (createdCommentsDto == null)
                {
                    var message = "createdCommentsDto can not be null";
                    Logger.Error(message);
                    throw new ApplicationException("Null response body POSTing comment to API");
                }

                var newCommentModel = new CommentModel(createdCommentsDto);

                return Json(new StandardJsonResult(newCommentModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = e.Message + Environment.NewLine + e.StackTrace;
                Logger.Error(e, message);
                return Json(new StandardJsonResult( statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }
        }
    }
}