﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.TimeManagement.Controllers
{

     [Authorize]
     [OutputCache(CacheProfile = "NoCache")]
     public class TimeApprovalController : SharedController
     {
          /// <summary>
          /// Creates an instance of TimeApprovalController
          /// </summary>
          /// <param name="adapterRegistry">adapter registry helper; injected by the framework</param>
          /// <param name="settings">settings helper; injected by the framework</param>
          /// <param name="logger">logging helper; injected by the framework</param>
          public TimeApprovalController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
               : base(adapterRegistry, settings, logger)
          {

          }

          [LinkHelp]
          [PageAuthorize("timeApproval")]
          public ActionResult Index()
          {
               ViewBag.Title = "Time Approval";
               ViewBag.CssAfterOverride = new List<string>();


               return View("~/Areas/TimeManagement/Views/TimeApproval/TimeApproval.cshtml");
          }

          [HttpGet]
          [PageAuthorize("timeApproval")]
          public async Task<JsonResult> GetSummaryApprovalDataAsync()
          {
               try
               {
                    // get necessary information
                    var Timecards2Task = ServiceClient.GetTimecards2Async();
                    var timecardStatusesTask = ServiceClient.GetLatestTimecardStatuses();
                    var positionsTask = ServiceClient.GetPositionsAsync();
                    var personPositionsTask = ServiceClient.GetPersonPositionsAsync();
                    var personPositionWagesTask = ServiceClient.GetPersonPositionWagesAsync();
                    var payCyclesTask = ServiceClient.GetPayCyclesAsync();
                    var commentsTask = ServiceClient.GetTimeEntryCommentsAsync();
                    var personEmploymentStatusesTask = ServiceClient.GetPersonEmploymentStatusesAsync();
                    var humanResourcesDemographicsTask = ServiceClient.GetHumanResourceDemographicsAsync();

                    await Task.WhenAll(Timecards2Task, timecardStatusesTask, positionsTask, personPositionsTask, personPositionWagesTask, payCyclesTask, commentsTask, personEmploymentStatusesTask, humanResourcesDemographicsTask);

                    var timecards = Timecards2Task.Result;
                    var timecardStatuses = timecardStatusesTask.Result;
                    var positions = positionsTask.Result.ToList();
                    var personPositions = personPositionsTask.Result.ToList();
                    var personPositionWages = personPositionWagesTask.Result.ToList();
                    var payCycles = payCyclesTask.Result;
                    var comments = commentsTask.Result;
                    var personEmploymentStatuses = personEmploymentStatusesTask.Result;
                    var humanResourcesDemographicsDtos = humanResourcesDemographicsTask.Result;

                    var employeeDictionary = humanResourcesDemographicsDtos.ToDictionary((hrd) => hrd.Id);

                    var supervisorPositionQuery = new PositionQuery(CurrentUser.PersonId, personPositions, positions, personPositionWages, payCycles, true);
                    var approverQuery = new ApproverQuery(timecards, timecardStatuses, payCycles, comments);

                    var timeApprovalModel = new ApprovalSummaryModel(supervisorPositionQuery, approverQuery, positions, personPositions, employeeDictionary, personEmploymentStatuses);

                    var payCycleDict = payCycles.ToDictionary(p => p.Id);
                    var overtimeTasks = timeApprovalModel.PayPeriodModels
                        .SelectMany(pm =>
                            {
                                 PayCycle payCycle = null;
                                 //var normalizePayPeriodStartDate = ;
                                 //var normalizePayPeriodEndDate = ;

                                 //if the paycycle object for the given id exists (which it should)
                                 //and the normalized
                                 if (payCycleDict.TryGetValue(pm.PayCycleId, out payCycle))
                                 {
                                      return pm.EmployeeModels.SelectMany(em =>
                                          TimeManagementUtilities.Weeks(
                                              pm.PeriodStartDate.NormalizeToStartDayOfWeek(payCycle.WorkWeekStartDay),
                                              pm.PeriodEndDate.NormalizeToEndDateOfWeek((DayOfWeek)(int)payCycle.WorkWeekStartDay + 6 % 7))
                                          .Where(w => w.Last() <= pm.PeriodEndDate) //only include overtime for weeks that end in this pay period. overtime is paid in the pay period in which the week ends
                                          .Select(w =>
                                          ServiceClient.CalculateOvertime(new OvertimeQueryCriteria()
                                          {
                                               PersonId = em.EmployeeId,
                                               PayCycleId = pm.PayCycleId,
                                               EndDate = w.Last(),
                                               StartDate = w.First()
                                          })));
                                 }
                                 else
                                 {
                                      return null;
                                 }
                            })
                            .Where(task => task != null).ToList();


                    await Task.WhenAll(overtimeTasks);

                    var taskGrouping = overtimeTasks.GroupBy(t => new Tuple<DateTime, string>(t.Result.EndDate, t.Result.PayCycleId));

                    foreach (var taskGroup in taskGrouping)
                    {
                         var payPeriodModel = timeApprovalModel.PayPeriodModels.FirstOrDefault(p => p.PeriodEndDate >= taskGroup.Key.Item1 && p.PeriodStartDate <= taskGroup.Key.Item1 && p.PayCycleId == taskGroup.Key.Item2);
                         if (payPeriodModel != null)
                         {
                              foreach (var task in taskGroup)
                              {
                                   var employeeModel = payPeriodModel.EmployeeModels.FirstOrDefault(e => e.EmployeeId == task.Result.PersonId);
                                   if (employeeModel != null)
                                   {
                                        employeeModel.OvertimeHours += (decimal)task.Result.Thresholds.Sum(t => t.TotalOvertime.TotalHours);
                                   }
                              }
                         }
                    }


                    return Json(new StandardJsonResult(timeApprovalModel), JsonRequestBehavior.AllowGet);
               }
               catch (Exception e)
               {
                    var message = "Something went wrong retrieving data or creating models: " + e.Message + Environment.NewLine + e.StackTrace;
                    Logger.Error(e, message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
               }

          }

          [HttpGet]
          [PageAuthorize("timeApproval")]
          public async Task<JsonResult> GetDetailApprovalDataAsync(string personId, string payCycleId, string payPeriodEndDate)
          {
               if (string.IsNullOrWhiteSpace(personId))
               {
                    var message = "personId is required";
                    Logger.Error(message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
               }
               if (string.IsNullOrWhiteSpace(payCycleId))
               {
                    var message = "payCycleId is required";
                    Logger.Error(message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
               }
               if (string.IsNullOrWhiteSpace(payPeriodEndDate))
               {
                    var message = "payPeriodEndDate is required";
                    Logger.Error(message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
               }

               DateTime periodEndDate;
               if (!DateTime.TryParse(payPeriodEndDate, out periodEndDate))
               {
                    var message = "unable to parse payPeriodEndDate";
                    Logger.Error(message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
               }

               try
               {


                    var positionQuery = await ServiceClient.GetPositionQueryAsync(GetEffectivePersonId(), personId, false);
                    //get relevant PayPeriod
                    var payCycle = positionQuery.GetPayCycle(payCycleId);

                    if (payCycle == null)
                    {
                         var message = "positionQuery.GetPayCycle returned null";
                         Logger.Error(message);
                         throw new ApplicationException(message);
                    }
                    var payPeriod = payCycle.PayPeriods.FirstOrDefault(x => x.EndDate == periodEndDate);

                    if (payPeriod == null)
                    {
                         var message = "payCycle.PayPeriods was unable to return a valid PayPeriod";
                         Logger.Error(message);
                         throw new ApplicationException(message);
                    }

                    // get pertinent information   
                    var supervisorQueryTask = ServiceClient.GetPositionQueryAsync(GetEffectivePersonId(), GetEffectivePersonId(), true);
                    var timecardQueryTask = ServiceClient.GetTimecardQueryAsync(personId, payPeriod.StartDate, payPeriod.EndDate);
                    var earningsTypesTask = ServiceClient.GetEarningsTypesAsync();
                    var personEmploymentStatusesTask = ServiceClient.GetCachedPersonEmploymentStatusesAsync(GetEffectivePersonId(), personId);
                    var humanResourceDemographicsDtosTask = ServiceClient.GetHumanResourceDemographicsAsync();
                    var departmentsTask = ServiceClient.GetDepartmentsAsync();
                    var locationsTask = ServiceClient.GetLocationsAsync();
                    await Task.WhenAll(supervisorQueryTask, timecardQueryTask, earningsTypesTask, personEmploymentStatusesTask, humanResourceDemographicsDtosTask, departmentsTask, locationsTask);

                    var supervisorPositionQuery = supervisorQueryTask.Result;
                    var timecardQuery = timecardQueryTask.Result;
                    var earningsTypes = earningsTypesTask.Result;
                    var departments = departmentsTask.Result;
                    var locations = locationsTask.Result;
                    var personEmployementStatuses = personEmploymentStatusesTask.Result;
                    var humanResourceDemographicsDtos = humanResourceDemographicsDtosTask.Result;

                    // create a dictionary of the Names and Ids for the employees you supervise
                    var employeeDictionary = humanResourceDemographicsDtos.ToDictionary((hrd) => hrd.Id);

                    var model = new ApprovalDetailModel(supervisorPositionQuery, payCycleId, periodEndDate, positionQuery, timecardQuery, earningsTypes, personEmployementStatuses, employeeDictionary, departments, locations);


                    return Json(new StandardJsonResult(model), JsonRequestBehavior.AllowGet);
               }
               catch (Exception e)
               {
                   var message = "Something went wrong retrieving data or creating the ApprovalDetailModel: " + e.Message + Environment.NewLine + e.StackTrace;
                    Logger.Error(e, message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
               }
          }


          [HttpPost]
          [PageAuthorize("timeApproval")]
          public async Task<JsonResult> PostOvertimeQueryAsync(string overtimeQueryCriteriaJson)
          {
               if (string.IsNullOrEmpty(overtimeQueryCriteriaJson))
               {
                    var message = "overtimeQueryCriteriaJson is required";
                    Logger.Error(message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message));
               }


               try
               {
                    var overtimeQueryCriteria = JsonConvert.DeserializeObject<OvertimeQueryCriteria>(overtimeQueryCriteriaJson);

                    var overtimeCalculation = await ServiceClient.CalculateOvertime(overtimeQueryCriteria);

                    if (overtimeCalculation == null)
                    {
                         var message = "Unable to perform overtime calculation";
                         Logger.Error(message);
                         throw new ApplicationException(message);
                    }

                    var overtimeCalculationResultModel = new OvertimeCalculationResultModel(overtimeQueryCriteria.PayCycleId, overtimeCalculation.Thresholds);


                    return Json(new StandardJsonResult(overtimeCalculationResultModel), JsonRequestBehavior.AllowGet);
               }
               catch (Exception e)
               {
                   var message = "Something went wrong creating the OvertimeCalculationResultModel: " + e.Message + Environment.NewLine + e.StackTrace; ;
                    Logger.Error(e, message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
               }
          }

     }
}