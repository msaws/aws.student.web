﻿using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using Ellucian.Web.Student.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Controllers
{

    /// <summary>
    /// A set of extension methods on the ColleagueApiClient that implements caching and/or
    /// builds complex data structures from multiple API sources
    /// </summary>
    public static class SharedHelpers
    {
        public static async Task<IEnumerable<PersonEmploymentStatus>> GetCachedPersonEmploymentStatusesAsync(this ColleagueApiClient serviceClient, string currentUserId, string personId)
        {
            return (await ServiceClientCache.GetOrAddToCacheAsync<IEnumerable<PersonEmploymentStatus>>(currentUserId + "-PersonEmploymentStatuses", async () =>
            {
                return await serviceClient.GetPersonEmploymentStatusesAsync();
            })).Where(pes => pes.PersonId == personId);
        }


        public static async Task<PositionQuery> GetPositionQueryAsync(this ColleagueApiClient ServiceClient, string currentUserId, string personId, bool isSupervisor)
        {
            var personPositionsTask = ServiceClient.GetCachedPersonPositionsAsync(currentUserId);
            var personWagesTask = ServiceClient.GetCachedPersonPositionWagesAsync(currentUserId);
            var positionsTask = ServiceClient.GetPositionsAsync();
            var payCyclesTask = ServiceClient.GetPayCyclesAsync();
            await Task.WhenAll(personPositionsTask, personWagesTask, positionsTask, payCyclesTask);

            return new PositionQuery(
                personId,
                personPositionsTask.Result,
                positionsTask.Result,
                personWagesTask.Result,
                payCyclesTask.Result,
                isSupervisor);
        }

        public static async Task<IEnumerable<PersonPosition>> GetCachedPersonPositionsAsync(this ColleagueApiClient serviceClient, string currentUserId)
        {
            return await ServiceClientCache.GetOrAddToCacheAsync<IEnumerable<PersonPosition>>(currentUserId + "-PersonPositions", async () =>
                {
                    return await serviceClient.GetPersonPositionsAsync();
                });
        }

        public static async Task<IEnumerable<PersonPositionWage>> GetCachedPersonPositionWagesAsync(this ColleagueApiClient serviceClient, string currentUserId)
        {
            return await ServiceClientCache.GetOrAddToCacheAsync<IEnumerable<PersonPositionWage>>(currentUserId + "-PersonPositionsWage", async () =>
            {
                return await serviceClient.GetPersonPositionWagesAsync();
            });
        }

        /// <summary>
        /// Extension method of ColleagueApiClient gets all the data needed to build a TimecardQueryDataCoordinator
        /// </summary>
        /// <param name="ServiceClient"></param>
        /// <param name="personId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static async Task<TimecardQueryDataCoordinator> GetTimecardQueryAsync(this ColleagueApiClient ServiceClient, string personId, DateTime? startDate = null, DateTime? endDate = null)
        {
            var taskList = new List<Task>();
            var Timecards2Task = ServiceClient.GetTimecards2Async();
            var statusesTask = ServiceClient.GetLatestTimecardStatuses();
            var commentsTask = ServiceClient.GetTimeEntryCommentsAsync();
            var timecardHistoriesTask = startDate.HasValue && endDate.HasValue ?
                ServiceClient.GetTimecardHistories2Async(startDate.Value.AddDays(-1), endDate.Value.AddDays(1)) :
                Task.FromResult<IEnumerable<TimecardHistory2>>(new List<TimecardHistory2>());


            await Task.WhenAll(Timecards2Task, statusesTask, commentsTask, timecardHistoriesTask);



            var timecards = Timecards2Task.Result;
            var statuses = statusesTask.Result;
            var comments = commentsTask.Result;
            var timecardHistories = timecardHistoriesTask.Result;

            return new TimecardQueryDataCoordinator(personId, timecards, timecardHistories, statuses, comments);
        }
    }
}