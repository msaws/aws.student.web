﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Student.Models;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.TimeManagement.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class TimeSheetController : SharedController
    {
        /// <summary>
        /// Creates an instance of TimeSheetController
        /// </summary>
        /// <param name="adapterRegistry">adapter registry helper; injected by the framework</param>
        /// <param name="settings">settings helper; injected by the framework</param>
        /// <param name="logger">logging helper; injected by the framework</param>
        public TimeSheetController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(adapterRegistry, settings, logger)
        {

        }

        [LinkHelp]
        [PageAuthorize("timeSheet")]
        public ActionResult Index()
        {
            ViewBag.Title = "Time Sheet";
            ViewBag.CssAfterOverride = new List<string>();

            //Stephen says this is bad -- will be fixing the bundles on the next changeset
            //ViewBag.CssAfterOverride.Add("~/Areas/TimeManagement/Content/TimeManagement.css");

            return View("~/Areas/TimeManagement/Views/TimeSheet/TimeSheet.cshtml");
        }


        /// <summary>
        /// Gets TimeSheet information
        /// </summary>
        /// <returns>Json of an object containing a TimecardCollectionModel, an HTTPStatusCode, and an potential error message</returns>
        [PageAuthorize("timeSheet")]
        [HttpGet]
        public async Task<JsonResult> GetTimeSheetDataAsync(string startDate, string endDate, string payCycleId)
        {

            DateTime start;
            DateTime end;
            if (!DateTime.TryParse(startDate, out start))
            {
                var message = string.Format("Unable to parse start date {0}", startDate);
                Logger.Error(message);
                return Json(new { StatusCode = HttpStatusCode.BadRequest, Message = message }, JsonRequestBehavior.AllowGet);
            }
            if (!DateTime.TryParse(endDate, out end))
            {
                var message = string.Format("Unable to parse end date {0}", endDate);
                Logger.Error(message);
                return Json(new { StatusCode = HttpStatusCode.BadRequest, Message = message }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                // get pertinent information   
                var positionQueryTask = ServiceClient.GetPositionQueryAsync(CurrentUser.PersonId, GetEffectivePersonId(), false);
                var timecardQueryTask = ServiceClient.GetTimecardQueryAsync(GetEffectivePersonId(), start, end);
                var earningsTypesTask = ServiceClient.GetEarningsTypesAsync();
                var personEmploymentStatusesTask = ServiceClient.GetCachedPersonEmploymentStatusesAsync(CurrentUser.PersonId, GetEffectivePersonId());
                var humanResourcesDemographicsTask = ServiceClient.GetHumanResourceDemographicsAsync();
                var departmentsTask = ServiceClient.GetDepartmentsAsync();
                var locationsTask = ServiceClient.GetLocationsAsync();
                await Task.WhenAll(positionQueryTask, timecardQueryTask, earningsTypesTask, personEmploymentStatusesTask, humanResourcesDemographicsTask, departmentsTask, locationsTask);

                var positionQuery = positionQueryTask.Result;
                var timecardQuery = timecardQueryTask.Result;
                var earningsTypes = earningsTypesTask.Result;
                var departments = departmentsTask.Result;
                var locations = locationsTask.Result;
                var personEmployementStatuses = personEmploymentStatusesTask.Result;
                var humanResourcesDemographicsDtos = humanResourcesDemographicsTask.Result;
                var employeeDictionary = humanResourcesDemographicsDtos.ToDictionary((hrd) => hrd.Id);
                // get the personEmployementStatuses from the cache

                //since the start and end dates come in from the url, we have to verify that those dates
                //actually correspond to time periods so unknowing or malicious users don't create timecards for 
                //crazy date ranges
                var timePeriodCollectionModel = new TimePeriodCollectionModel(payCycleId, timecardQuery, positionQuery, personEmployementStatuses);
                if (timePeriodCollectionModel.GetTimePeriodModel(start, end) == null)
                {
                    var message = string.Format("Start Date {0} and End Date {1} are invalid based on prepared time periods", start, end);
                    Logger.Error(message);
                    return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
                }

                var timecardCollectionModel = new TimecardCollectionModel(GetEffectivePersonId(), payCycleId, start, end, employeeDictionary, positionQuery, timecardQuery, personEmployementStatuses, earningsTypes, departments, locations);

                timecardCollectionModel.AddTimePeriodContext(timePeriodCollectionModel.GetPreviousTimePeriodModel(start, end), timePeriodCollectionModel.GetNextTimePeriodModel(start, end));

                return Json(new { Model = timecardCollectionModel, StatusCode = HttpStatusCode.OK, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Something went wrong retrieving data from the server or creating the timecard collection model: " + e.Message;
                Logger.Error(e, message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: message), JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Updates a timecard
        /// </summary>
        /// <returns>A json object of a TimecardModel containing the received TimeCard object</returns>
        [HttpPut]
        [PageAuthorize("timeSheet")]
        public async Task<JsonResult> UpdateTimecardAsync(string timecardModelJson)
        {
            try
            {
                TimecardModel timecardModel = JsonConvert.DeserializeObject<TimecardModel>(timecardModelJson);
                Timecard2 timecard2Dto = null;

                // convert the TimeCardModel to a TimeCardDto
                timecard2Dto = timecardModel.ConvertModelToDto();

                var timeframeStartDate = timecardModel.WorkEntryList[0].DailyTimeEntryGroupList[0].DateWorked;

                // Service Call to update timecard
                var updatedTimecard2DtoTask =  ServiceClient.UpdateTimecard2Async(timecard2Dto);

                await updatedTimecard2DtoTask;

                return Json(new StandardJsonResult(statuscode: HttpStatusCode.NoContent), JsonRequestBehavior.AllowGet);

            }
            catch (Exception exception)
            {
                // Throw a 400
                Logger.Error(exception, exception.Message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: exception.Message), JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Updates a timecard
        /// </summary>
        /// <returns>A json object of a TimecardModel containing the received TimeCard object</returns>
        [HttpPost]
        [PageAuthorize("timeSheet")]
        public async Task<JsonResult> CreateTimecardAsync(string timecardModelJson)
        {
            try
            {
                TimecardModel timecardModel = JsonConvert.DeserializeObject<TimecardModel>(timecardModelJson);
                Timecard2 timecard2Dto = null;

                // convert the TimeCardModel to a TimeCardDto
                timecard2Dto = timecardModel.ConvertModelToDto();

                var timeframeStartDate = timecardModel.WorkEntryList[0].DailyTimeEntryGroupList[0].DateWorked;
     
                // Service Call to create timecard
                var createdTimecard = await ServiceClient.CreateTimecards2Async(timecard2Dto);

                var obj = new 
                {
                    Id = createdTimecard.Id,
                    PeriodStartDate = createdTimecard.PeriodStartDate,
                    PeriodEndDate = createdTimecard.PeriodEndDate,
                    PayCycleId = createdTimecard.PayCycleId,
                    PositionId = createdTimecard.PositionId,
                };

                return Json(new StandardJsonResult(obj), JsonRequestBehavior.AllowGet);

            }
            catch (Exception exception)
            {
                // Throw a 400
                Logger.Error(exception, exception.Message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: exception.Message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [PageAuthorize("timeSheet")]
        public async Task<JsonResult> CreateTimecardsInPayPeriod(string periodStartDate, string periodEndDate, string payCycleId, string positionId)
        {
            DateTime start;
            DateTime end;
            if (!DateTime.TryParse(periodStartDate, out start))
            {
                var message = string.Format("Unable to parse start date {0}", periodStartDate);
                Logger.Error(message);
                return Json(new { StatusCode = HttpStatusCode.BadRequest, Message = message }, JsonRequestBehavior.AllowGet);
            }
            if (!DateTime.TryParse(periodEndDate, out end))
            {
                var message = string.Format("Unable to parse end date {0}", periodEndDate);
                Logger.Error(message);
                return Json(new { StatusCode = HttpStatusCode.BadRequest, Message = message }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                //get data to see if we need to create additional timecards in the pay peirod                    
                var positionQueryTask = ServiceClient.GetPositionQueryAsync(CurrentUser.PersonId, GetEffectivePersonId(), false);
                var timecardQueryTask = ServiceClient.GetTimecardQueryAsync(GetEffectivePersonId(), start, end);
                var personEmploymentStatusesTask = ServiceClient.GetCachedPersonEmploymentStatusesAsync(CurrentUser.PersonId, GetEffectivePersonId());
                var earningsTypesTask = ServiceClient.GetEarningsTypesAsync();
                var humanResourcesDemographicsTask = ServiceClient.GetHumanResourceDemographicsAsync();
                var departmentsTask = ServiceClient.GetDepartmentsAsync();
                var locationsTask = ServiceClient.GetLocationsAsync();

                await Task.WhenAll(positionQueryTask, timecardQueryTask, personEmploymentStatusesTask, earningsTypesTask, humanResourcesDemographicsTask, departmentsTask, locationsTask);

                var positionQuery = positionQueryTask.Result;
                var timecardQuery = timecardQueryTask.Result;
                var personEmploymentStatuses = personEmploymentStatusesTask.Result;
                var earningsTypes = earningsTypesTask.Result;
                var departments = departmentsTask.Result;
                var locations = locationsTask.Result;
                var humanResourcesDemographicsDtos = humanResourcesDemographicsTask.Result;
                var employeeDictionary = humanResourcesDemographicsDtos.ToDictionary((hrd) => hrd.Id);

                var timePeriodCollectionModel = new TimePeriodCollectionModel(payCycleId, timecardQuery, positionQuery, personEmploymentStatuses);
                var timePeriodModelsInTimecardPayPeriod = timePeriodCollectionModel.GetTimePeriodModels(payCycleId, positionId, start, end);
                var createTimecard2Tasks = new List<Task<Timecard2>>();
                foreach (var timePeriod in timePeriodModelsInTimecardPayPeriod)
                {
                    var timecardCollection = new TimecardCollectionModel(GetEffectivePersonId(), payCycleId, timePeriod.StartDate, timePeriod.EndDate, employeeDictionary, positionQuery, timecardQuery, personEmploymentStatuses, earningsTypes, departments, locations);
                    var timecardsToCreateTasks = timecardCollection.Timecards
                        .Where(t => string.IsNullOrEmpty(t.TimecardId) && t.PositionId == positionId)
                        .Select(t => ServiceClient.CreateTimecards2Async(t.ConvertModelToDto()));
                    createTimecard2Tasks.AddRange(timecardsToCreateTasks);
                }

                await Task.WhenAll(createTimecard2Tasks);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Unable to create timecards to fill out the pay period");
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: "Unable to create timecards to fill out pay period"), JsonRequestBehavior.AllowGet);
            }

            return Json(new StandardJsonResult(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [PageAuthorize("timeSheet")]
        public async Task<JsonResult> GetTimePeriodPayCyclesAsync()
        {
            try
            {
                var positionQuery = await ServiceClient.GetPositionQueryAsync(CurrentUser.PersonId, GetEffectivePersonId(), false);
                var payCycles = positionQuery
                    .Where(pq => pq.PreparedPayPeriods.Any() || pq.HistoricalPayPeriods.Any())
                    .Select(pq => pq.PayCycle)
                    .Select(pc => new PayCycleModel(pc.Id, pc.Description))
                    .Distinct();

                return Json(new StandardJsonResult(payCycles), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                return Json(new StandardJsonResult(statuscode: HttpStatusCode.BadRequest, message: e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [PageAuthorize("timeSheet")]
        public async Task<JsonResult> GetTimePeriodDataAsync(string payCycleId)
        {
            try
            {
                // get pertinent information   
                var positionQueryTask = ServiceClient.GetPositionQueryAsync(CurrentUser.PersonId, GetEffectivePersonId(), false);
                var timecardQueryTask = ServiceClient.GetTimecardQueryAsync(GetEffectivePersonId());
                var personEmploymentStatusesTask = ServiceClient.GetCachedPersonEmploymentStatusesAsync(CurrentUser.PersonId, GetEffectivePersonId());
                await Task.WhenAll(positionQueryTask, timecardQueryTask, personEmploymentStatusesTask);
                var timePeriodCollectionModel = new TimePeriodCollectionModel(payCycleId, timecardQueryTask.Result, positionQueryTask.Result, personEmploymentStatusesTask.Result);

                return Json(new StandardJsonResult(timePeriodCollectionModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var message = "Something went wrong with retrieving data or creating the TimePeriodCollectionModel: " + e.Message;
                Logger.Error(e, message);
                return Json(new { StatusCode = HttpStatusCode.BadRequest, Message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [PageAuthorize("timeSheet")]
        public async Task<JsonResult> GetOvertimeCalculationAsync(string overtimeQueryCriteriaJson)
        {
            try
            {
                var overtimeQueryCriteria = JsonConvert.DeserializeObject<OvertimeQueryCriteria>(overtimeQueryCriteriaJson);
                overtimeQueryCriteria.PersonId = string.IsNullOrWhiteSpace(overtimeQueryCriteria.PersonId) ? base.CurrentUser.PersonId : overtimeQueryCriteria.PersonId;

                var overtimeCalculation = await ServiceClient.CalculateOvertime(overtimeQueryCriteria);

                if (overtimeCalculation == null)
                    throw new ApplicationException("Unable to perform overtime calculation");

                var overtimeCalculationResultModel = new OvertimeCalculationResultModel(overtimeQueryCriteria.PayCycleId, overtimeCalculation.Thresholds);

                return Json(new { Model = overtimeCalculationResultModel, StatusCode = HttpStatusCode.OK, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e, e.ToString());
                return Json(new { StatusCode = HttpStatusCode.BadRequest, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
