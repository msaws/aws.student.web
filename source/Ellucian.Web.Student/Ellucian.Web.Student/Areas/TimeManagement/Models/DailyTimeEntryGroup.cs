﻿using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class DailyTimeEntryGroup
    {
        public string Id { get; set; }
        public DateTime DateWorked { get; set; }
        public bool IsReadOnly { get; private set; }
        public List<DailyTimeEntry> DailyTimeEntryList { get; set; }

        public DailyTimeEntryGroup()
        {
            DailyTimeEntryList = new List<DailyTimeEntry>();
        }

        public DailyTimeEntryGroup(string id, DateTime dateWorked, IEnumerable<TimeEntry2> timeEntry2Dtos, bool isGroupReadOnly, bool isGroupHistorical)
        {
            DailyTimeEntryList = new List<DailyTimeEntry>();
            Id = id;
            DateWorked = DateTime.SpecifyKind(dateWorked, DateTimeKind.Unspecified);
            IsReadOnly = isGroupReadOnly;
            DailyTimeEntryList.AddRange(timeEntry2Dtos.Select(te => new DailyTimeEntry(te, isGroupReadOnly, isGroupHistorical)));
        }

        public DailyTimeEntryGroup(string id, DateTime dateWorked, bool isGroupReadOnly)
        {
            DailyTimeEntryList = new List<DailyTimeEntry>();
            Id = id;
            DateWorked = DateTime.SpecifyKind(dateWorked, DateTimeKind.Unspecified);
            IsReadOnly = isGroupReadOnly;
            DailyTimeEntryList.Add(new DailyTimeEntry(dateWorked, isGroupReadOnly));
        }
    }
}