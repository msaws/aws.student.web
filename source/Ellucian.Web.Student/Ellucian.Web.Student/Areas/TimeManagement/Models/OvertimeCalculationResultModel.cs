﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class OvertimeCalculationResultModel
    {
        /// <summary>
        /// Identifier of the associated paycycle
        /// </summary>
        public string PayCycleId { get; private set; }
        /// <summary>
        /// Total overtime for the context timecard collection
        /// </summary>
        public decimal TotalOvertime { get; private set; }

        /// <summary>
        ///  Creates overtimeCalculationResultModel from payCycleId and list of calculation result thresholds
        /// </summary>
        /// <param name="payCycleId"></param>
        /// <param name="thresholds"></param>
        public OvertimeCalculationResultModel(string payCycleId, List<OvertimeThresholdResult> thresholds)
        {
            if (string.IsNullOrWhiteSpace(payCycleId))
                throw new ArgumentNullException("payCycleId");

            this.PayCycleId = payCycleId;

            var tempOvertime = 0.00m;
            if(thresholds != null)
            {
                foreach (var threshold in thresholds)
                {
                    tempOvertime += (decimal)threshold.TotalOvertime.TotalHours;
            }

        }
            this.TotalOvertime = tempOvertime;
        }
    }
}