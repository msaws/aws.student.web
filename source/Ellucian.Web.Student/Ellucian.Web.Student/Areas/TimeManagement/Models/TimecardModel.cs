﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class TimecardModel : IComparable<TimecardModel>, IComparable, IComparer<TimecardModel>
    {
        public string ModelId
        {
            get
            {
                if (PositionId == null) return this.GetHashCode().ToString();
                return (PositionId.GetHashCode() ^
                    PayCycleId.GetHashCode() ^
                    StartDate.GetHashCode() ^
                    EndDate.GetHashCode()).ToString();
            }
        }
        public string TimecardId { get; set; }
        public string EmployeeId { get; set; }
        public string PositionTitle { get; set; }
        public string PositionId { get; set; }
        private string PositionDepartment { get; set; }
        private string PositionLocation { get; set; }
        private Dictionary<string, HumanResourceDemographics> EmployeeDictionary { get; set; }
        public string SubtitleText { get; set; }
        public string TitleText { get; set; }
        public bool IsAssociatedPositionPrimary { get; set; }
        public bool IsForHistoricalPayPeriod { get; set; }
        public string SupervisorId { get; set; }
        public List<string> PositionLevelSupervisorIds { get; set; }
        public string SupervisorPositionId { get; set; }
        public string SupervisorName { get; set; }
        public string PayCycleId { get; set; }
        public TimecardType DailyTimeEntryType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PeriodStartDate { get; set; }
        public DateTime PeriodEndDate { get; set; }
        public DateTimeOffset? EmployeeSubmitDeadline { get; set; }
        public DateTimeOffset? SupervisorApproveDeadline { get; set; }
        public List<WorkEntry> WorkEntryList { get; set; }
        public List<EarningsType> EarningsTypes { get; set; }
        public Timestamp Timestamp { get; set; }
        public int? TimecardStatus { get; set; }
        public DateTimeOffset? TimecardStatusActionDateTime { get; set; }
        public List<CommentModel> Comments { get; set; }

        /// <summary>
        /// Returns true if the timecard has all the required attributes. 
        /// Can help to prevent timecards created from bad data
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(EmployeeId) && !string.IsNullOrEmpty(PositionId) && !string.IsNullOrEmpty(PayCycleId) &&
                EmployeeSubmitDeadline.HasValue && SupervisorApproveDeadline.HasValue;
        }

        //default constructor
        public TimecardModel()
        {
            WorkEntryList = new List<WorkEntry>();
            EarningsTypes = new List<EarningsType>();
        }


        // constructor that accepts arguments
        public TimecardModel(TimecardQueryResultContext timecardQueryResultContext, PositionQueryResultItem positionQueryResult, IEnumerable<EarningsType> earningsTypes, IEnumerable<PersonEmploymentStatus> employmentStatuses, IEnumerable<Department> departments, IEnumerable<Location> locations, Dictionary<string, HumanResourceDemographics> employeeDictionary, DateTime timeframeStartDate)
        {
            var timecardDto = timecardQueryResultContext.Current.Timecard2;
            var timecardStatus = timecardQueryResultContext.Current.Status;

            var joiner = timecardQueryResultContext.Current.TimecardJoiner;
            var personPositionWages = positionQueryResult.GetPersonPositionWages(timeframeStartDate, timeframeStartDate.AddDays(6));

            if (personPositionWages.Any())
            {
                TimecardId = timecardDto.Id;
                EmployeeId = joiner.EmployeeId;
                PositionTitle = positionQueryResult.PositionTitle;
                PositionId = positionQueryResult.PositionId;

                //department is required on position, but still need to check for null
                var department = departments.FirstOrDefault(d => d.Code == positionQueryResult.Position.PositionDept);
                if (department != null)
                {
                    PositionDepartment = department.Description;
                }

                // location not required on position
                var posLocation = locations.FirstOrDefault(l => l.Code == positionQueryResult.Position.PositionLocation);
                if (posLocation != null)
                {
                    PositionLocation = posLocation.Description;
                }

                PayCycleId = joiner.PayCycleId;
                StartDate = joiner.StartDate;
                EndDate = joiner.EndDate;
                DailyTimeEntryType = positionQueryResult.PositionTimeType;
                SupervisorId = positionQueryResult.PersonPosition.SupervisorId;
                PositionLevelSupervisorIds = positionQueryResult.PersonPosition.PositionLevelSupervisorIds;
                SupervisorPositionId = positionQueryResult.Position.SupervisorPositionId;
                PeriodStartDate = joiner.PeriodStartDate;
                PeriodEndDate = joiner.PeriodEndDate;
                EmployeeSubmitDeadline = positionQueryResult.GetPayPeriod(PeriodEndDate).EmployeeTimecardCutoffDateTime;
                SupervisorApproveDeadline = positionQueryResult.GetPayPeriod(PeriodEndDate).SupervisorTimecardCutoffDateTime;
                IsForHistoricalPayPeriod = positionQueryResult.GetPayPeriod(PeriodEndDate).Status == PayPeriodStatus.History;
                TitleText = PositionId + " • " + PositionTitle;
                SubtitleText = PositionDepartment;

                var supervisorId = SupervisorId;
                if (string.IsNullOrWhiteSpace(supervisorId))
                {
                    if (PositionLevelSupervisorIds.Any())
                    {
                        supervisorId = PositionLevelSupervisorIds[0];
                    }
                }

                if (!string.IsNullOrWhiteSpace(supervisorId))
                {
                    SupervisorName = TimeManagementUtilities.FormatEmployeeName(supervisorId, employeeDictionary);
                }

                if (!String.IsNullOrWhiteSpace(SupervisorName))
                {
                    SubtitleText = string.Concat(SupervisorName, " • ", SubtitleText);
                }

                if (!String.IsNullOrWhiteSpace(PositionLocation))
                {
                    SubtitleText = string.Concat(SubtitleText, " • ", PositionLocation);
                }

                // set the list of earnings types sent to the timecardModel to the regular earnings type defined for the person's wage record. this
                // TODO: is temporary; we will be adding more earnings types to the list as the project progresses...
                var regularEarnTypesFromWages = personPositionWages.Select(pw => pw.RegularWorkEarningsTypeId);
                EarningsTypes = earningsTypes.Where(et => regularEarnTypesFromWages.Contains(et.Id)).ToList();
                WorkEntryList = new List<WorkEntry>();
                Comments = timecardQueryResultContext.Current.Comments
                    .Select(dto => new CommentModel(dto))
                    .Distinct()
                    .OrderBy(c => c.CommentDateTime)
                    .ToList();

                UpdateModelWithDtoData(timecardQueryResultContext, positionQueryResult, timeframeStartDate);

                if (timecardStatus == null)
                {
                    TimecardStatus = null;
                    TimecardStatusActionDateTime = null;
                }
                else
                {
                    TimecardStatus = (int)timecardStatus.ActionType;
                    TimecardStatusActionDateTime = timecardStatus.Timestamp.AddDateTime;
                }

                IsAssociatedPositionPrimary = employmentStatuses.Any(s =>
                    s.PrimaryPositionId == PositionId &&
                    s.StartDate <= EndDate &&
                    (!s.EndDate.HasValue || s.EndDate.Value >= StartDate));
            }
        }

        public void UpdateModelWithDtoData(
            //Timecard updatedTimecardDto, 
           TimecardQueryResultContext timecardQueryResultContext,
            PositionQueryResultItem positionQueryResultItem,
            DateTime timeframeStartDate)
        {
            TimecardId = timecardQueryResultContext.Current.Timecard2.Id;

            var currentIsHistorical = timecardQueryResultContext.Current.IsHistorical;
            // Make sure the timeCardModel.WorkEntryList is empty
            WorkEntryList.Clear();

            // collect historical TimeEntries
            var historicalTimeEntries = new List<TimeEntry2>();

            if (timecardQueryResultContext.Previous != null &&
                timecardQueryResultContext.Previous.PayPeriodComparer != timecardQueryResultContext.Current.PayPeriodComparer)
            {
                //previous historical entries will come from other pay periods. if previous is in the same pay period, 
                //we dont' want to view them as "historical" - likely, the "previous" comes from a "current" timecard with the same position id
                historicalTimeEntries.AddRange(timecardQueryResultContext.Previous.Timecard2.TimeEntries);
            }
            if (timecardQueryResultContext.Next != null &&
                timecardQueryResultContext.Next.PayPeriodComparer != timecardQueryResultContext.Current.PayPeriodComparer)
            {
                //next "historical" entries will come from other pay periods. if next is in the same pay period,
                //we don't want to view them as "historical" - likely, the "next" comes from a "current" timecard with the same position id.
                historicalTimeEntries.AddRange(timecardQueryResultContext.Next.Timecard2.TimeEntries);
            }


            if (currentIsHistorical)
            {
                historicalTimeEntries.AddRange(timecardQueryResultContext.Current.Timecard2.TimeEntries);
            }


            var updatedWorkEntryGrouping = currentIsHistorical ?
                new List<IGrouping<string, TimeEntry2>>() :
                timecardQueryResultContext.Current.Timecard2.TimeEntries.GroupBy(te => te.EarningsTypeId);

            var historicalWorkEntryGrouping = historicalTimeEntries.GroupBy(teh => teh.EarningsTypeId);

            var personPositionWages = positionQueryResultItem.GetPersonPositionWages(timeframeStartDate, timeframeStartDate.AddDays(6));
            var regularEarningsTypeIds = personPositionWages.Select(pw => pw.RegularWorkEarningsTypeId).Distinct();

            foreach (var regularEarningsTypeId in regularEarningsTypeIds)
            {
                var workEntryGroup = updatedWorkEntryGrouping.FirstOrDefault(g => g.Key == regularEarningsTypeId);
                var historicalWorkEntryGroup = historicalWorkEntryGrouping.FirstOrDefault(g => g.Key == regularEarningsTypeId);

                var workEntryList = new List<Colleague.Dtos.TimeManagement.TimeEntry2>();
                var historicalWorkEntryList = new List<TimeEntry2>();

                if (workEntryGroup != null)
                {
                    workEntryList = workEntryGroup.ToList();
                }
                if (historicalWorkEntryGroup != null)
                {
                    historicalWorkEntryList = historicalWorkEntryGroup.ToList();
                }

                var newWorkEntry = new WorkEntry(positionQueryResultItem, WorkEntryList.Count().ToString(), regularEarningsTypeId, timeframeStartDate, StartDate, EndDate, workEntryList, historicalWorkEntryList);
                WorkEntryList.Add(newWorkEntry);
            }
        }

        /// <summary>
        /// Helper function that converts a this object to a TimeCardDto
        /// </summary>
        /// <returns>Returns a Timecard Dto</returns>
        public Timecard2 ConvertModelToDto()
        {
            // create a Timecard
            Timecard2 timecard2Dto = new Timecard2()
            {
                EmployeeId = EmployeeId,
                Id = TimecardId,
                PayCycleId = PayCycleId,
                PeriodEndDate = PeriodEndDate,
                PeriodStartDate = PeriodStartDate,
                StartDate = StartDate,
                EndDate = EndDate,
                PositionId = PositionId,
                TimeEntries = new List<Colleague.Dtos.TimeManagement.TimeEntry2>()
            };

            // fill timeCardDto with time data from timeCardModelJson
            foreach (var workEntry in WorkEntryList)
            {
                foreach (var dailyGroup in workEntry.DailyTimeEntryGroupList)
                {
                    foreach (var entry in dailyGroup.DailyTimeEntryList.Where(d => d.HoursWorked.HasValue && !d.IsHistorical))
                    {
                        //create TimeEntry
                        var timeEntry = new Ellucian.Colleague.Dtos.TimeManagement.TimeEntry2()
                        {
                            //add info to the TimeEntry  
                            EarningsTypeId = workEntry.EarningsTypeId,
                            Id = entry.Id,
                            TimecardId = this.TimecardId,
                            WorkedDate = entry.DateWorked.Date,
                            InDateTime = entry.TimeStart,
                            OutDateTime = entry.TimeEnd,
                            //convert hours worked to ticks (constant found within TimeSpan)
                            WorkedTime = new TimeSpan(Convert.ToInt64(entry.HoursWorked.Value * TimeSpan.TicksPerHour)),

                        };

                        // add timeEntry to the timeCardDto's list of time entries
                        timecard2Dto.TimeEntries.Add(timeEntry);
                    }
                }
            }

            //return the Timecard
            return timecard2Dto;
        }

        //primary position comes first.
        //everything else is alpha order
        public int CompareTo(TimecardModel other)
        {
            if (other == null) return -1;

            if (this.IsAssociatedPositionPrimary) return -1;

            return String.Compare(this.PositionId, other.PositionId);
        }

        public int Compare(TimecardModel x, TimecardModel y)
        {
            return x.CompareTo(y);
        }

        public int CompareTo(object obj)
        {
            return this.CompareTo((TimecardModel)obj);
        }
    }
}