﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class TimePeriodCollectionModel
    {
        public List<TimePeriodModel> TimePeriodModels;
        public string PayCycleId;

        public TimePeriodCollectionModel(string payCycleId, TimecardQueryDataCoordinator timecardQuery, PositionQuery positionQuery, IEnumerable<PersonEmploymentStatus> personEmploymentStatuses)
        {
            TimePeriodModels = new List<TimePeriodModel>();

            PayCycleId = payCycleId;


            //flatten out the prepared pay periods across all positions
            var validPayPeriods = positionQuery
                .Where(pq => pq.PayCycle.Id == payCycleId)
                .SelectMany(p => p.PreparedPayPeriods)
                .Where(pp => pp.EmployeeTimecardCutoffDateTime.HasValue && pp.SupervisorTimecardCutoffDateTime.HasValue)
                .Concat( // concatenate historical items
                    positionQuery
                        .Where(pq => pq.PayCycle.Id == payCycleId)
                        .SelectMany(p =>
                            p.HistoricalPayPeriods
                                .Where(pp => timecardQuery.GetOrderedResultsForPayPeriod(p.PayCycle.Id, pp.EndDate).Any(t => !t.IsHistorical))
                                .Where(pp => pp.EmployeeTimecardCutoffDateTime.HasValue && pp.SupervisorTimecardCutoffDateTime.HasValue)
                ))
                .OrderBy(pp => pp.StartDate);


            //retrieve a PayCycle object
            var payCycle = positionQuery.GetPayCycle(payCycleId);

            if (validPayPeriods.Any() && payCycle != null)
            {
                

                //find the earliest and latest pay periods
                var earliestStartDate = TimeManagementUtilities.GetMaxDate(validPayPeriods.First().StartDate, positionQuery.EarliestPersonPositionStartDate);
                var latestEndDate = TimeManagementUtilities.GetMinDate(validPayPeriods.OrderBy(d => d.EndDate).Last().EndDate, positionQuery.LastestPersonPositionEndDate);

                var startDayOfWorkWeek = payCycle.WorkWeekStartDay;
                var endDayOfWorkWeek = (DayOfWeek)(((int)startDayOfWorkWeek + 6) % 7);
       
                //normalize the startdate of this collection to start on the specified startDayOfWorkWeek
                var collectionStartDate = earliestStartDate.NormalizeToStartDayOfWeek(startDayOfWorkWeek);

                //normalize the enddate of this collection to end on the end day of workweek
                var collectionEndDate = latestEndDate.NormalizeToEndDateOfWeek(endDayOfWorkWeek);

                //build out weeks between the start and end dates of this collection
                var weeks = TimeManagementUtilities.Weeks(collectionStartDate, collectionEndDate);

                foreach (var week in weeks)
                {
                    //determine if there are any prepared pay periods that overlap this week
                    var preppedPayPeriodsContainingWeek = validPayPeriods.Where(a =>
                        a.StartDate <= week.Last() && week.First() <= a.EndDate).ToList();

                    if (preppedPayPeriodsContainingWeek.Any())
                    {
                        //find all the start/end dates of the prepped pay periods that contain this week
                        var preppedStartDates = preppedPayPeriodsContainingWeek.Select(p => p.StartDate);
                        var preppedEndDates = preppedPayPeriodsContainingWeek.Select(p => p.EndDate);

                        //find the earliest start/latest end date of the prepped start/end dates
                        var startDateLimit = preppedStartDates.OrderBy(d => d).FirstOrDefault();
                        var endDateLimit = preppedEndDates.OrderBy(d => d).LastOrDefault();

                        //find the days of this week that match any prepped start and end date
                        var preppedStartDatesInWeek = week.Where(day => preppedStartDates.Contains(day)).OrderBy(d => d);
                        var preppedEndDatesInWeek = week.Where(day => preppedEndDates.Contains(day)).OrderBy(d => d);

                        //since pay periods overlap, we want to break apart weeks in which any pay period ends/starts in the middle of that week
                        var subWeeks = new List<List<DateTime>>();
                        var subWeek = new List<DateTime>();
                        foreach (var day in week)
                        {

                            if ((preppedStartDatesInWeek.Contains(day)) && subWeek.Any())
                            {
                                subWeeks.Add(subWeek);
                                subWeek = new List<DateTime>();
                                subWeek.Add(day);
                            }
                            else if (preppedEndDatesInWeek.Contains(day) || day == week.Last())
                            {
                                subWeek.Add(day);
                                subWeeks.Add(subWeek);
                                subWeek = new List<DateTime>();
                            }
                            else
                            {
                                subWeek.Add(day);
                            }
                        }

                        //filter out partial weeks that are outside of pay period start/end ranges that contain the week
                        var filteredSubWeeks = subWeeks.Where(w => w.Any(day =>
                            preppedPayPeriodsContainingWeek.Any(payPeriod => day >= payPeriod.StartDate && day <= payPeriod.EndDate)));

                        //build out the timeperiods for each partial week in this week
                        foreach (var partialWeek in filteredSubWeeks)
                        {
                            var startDate = partialWeek.First() < startDateLimit ? startDateLimit : partialWeek.First();
                            var endDate = partialWeek.Last() > endDateLimit ? endDateLimit : partialWeek.Last();
                            var periodEndDate = partialWeek.Last() < endDateLimit ? endDateLimit : partialWeek.Last();

                            var positionsForPartialWeek = positionQuery.Get(startDate, endDate, PayCycleId);
                            // filter positionsForPartialWeek with IsMigratedForThisDate
                            var migratedPositionsForPartialWeek = positionsForPartialWeek.Where(x => x.IsMigratedForThisDate(startDate));
                            if (migratedPositionsForPartialWeek.Any())
                            {

                                var existingTimePeriod = TimePeriodModels.FindTimePeriodForDate(startDate, endDate);
                                if (existingTimePeriod == null)
                                {
                                    var associatedPayPeriod = preppedPayPeriodsContainingWeek.First(pp => pp.EndDate >= endDate);
                                    existingTimePeriod = new TimePeriodModel(startDate, endDate)
                                    {                                                                   // apply the cutoff date to the correct week
                                        EmployeeSubmitDeadline = associatedPayPeriod.EmployeeTimecardCutoffDateTime,
                                        PayPeriodStartDate = associatedPayPeriod.StartDate,
                                        PayPeriodEndDate = associatedPayPeriod.EndDate,
                                        SupervisorApproveDeadline = preppedPayPeriodsContainingWeek.First(pp => pp.EndDate >= endDate).SupervisorTimecardCutoffDateTime,
                                        IsForHistoricalPayPeriod = preppedPayPeriodsContainingWeek.All(pp => pp.Status == PayPeriodStatus.History)
                                    };
                                    TimePeriodModels.Add(existingTimePeriod);
                                }


                                foreach (var positionQueryResult in migratedPositionsForPartialWeek)
                                {
                                    var isPrimary = personEmploymentStatuses.Any(pes =>
                                        pes.PrimaryPositionId == positionQueryResult.PositionId &&
                                        pes.StartDate <= (positionQueryResult.PersonPosition.EndDate ?? DateTime.MaxValue) &&
                                        (!pes.EndDate.HasValue || pes.EndDate.Value >= positionQueryResult.PersonPosition.StartDate));

                                    existingTimePeriod.AddTimecardPositionStatus(
                                        new TimecardPositionStatusModel(positionQueryResult.PositionId, positionQueryResult.PositionTitle, positionQueryResult.PayCycle.Id, isPrimary, null));
                                }

                                var timecardQueryResults = timecardQuery
                                    .GetResultsForDateRange(positionQuery, startDate, endDate, PayCycleId)
                                    .ToList();
                                foreach (var timecardQueryResult in timecardQueryResults)
                                {
                                    existingTimePeriod.AddTimecardHours(timecardQueryResult.Timecard2);

                                    //in the edge case where there are two timecards in this week timeframe with the same positionId,
                                    //we need to try to assign, based on the ordering/index of the data, the right status to the right timecard.
                                    //in the normal case where each timecard in the timeframe has a separate positionId,
                                    //this code always assigns the right status to the right timecard
                                    var positionStatuses = existingTimePeriod.TimecardPositionStatuses.Where(tp => tp.PositionId == timecardQueryResult.Timecard2.PositionId).ToList();
                                    if (positionStatuses.Any())
                                    {
                                        var index = timecardQueryResults.IndexOf(timecardQueryResult);
                                        if (index >= positionStatuses.Count())
                                        {
                                            index = 0;
                                        }
                                        var timecardPositionStatus = positionStatuses[index];

                                        timecardPositionStatus.TimecardId = timecardQueryResult.Timecard2.Id;
                                        timecardPositionStatus.CurrentStatus = (int?)timecardQueryResult.StatusActionType;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            TimePeriodModels = TimePeriodModels.OrderBy(tp => tp.StartDate).ToList();
        }

        public TimePeriodCollectionModel()
        {
            TimePeriodModels = new List<TimePeriodModel>();
        }

        public TimePeriodModel GetTimePeriodModel(DateTime startDate, DateTime endDate)
        {
            return TimePeriodModels.FirstOrDefault(tp => tp.StartDate == startDate && tp.EndDate == endDate);
        }

        public TimePeriodModel GetNextTimePeriodModel(DateTime startDate, DateTime endDate)
        {
            var model = GetTimePeriodModel(startDate, endDate);
            if (model == null) 
            {
                return null;
            }
            var currentPeriodIndex = TimePeriodModels.IndexOf(model);
            if (currentPeriodIndex < 0 || currentPeriodIndex == TimePeriodModels.Count - 1)
            {
                return null;
            }

            return TimePeriodModels[currentPeriodIndex + 1];
        }

        public TimePeriodModel GetPreviousTimePeriodModel(DateTime startDate, DateTime endDate)
        {
            var model = GetTimePeriodModel(startDate, endDate);
            if (model == null)
            {
                return null;
            }
            var currentPeriodIndex = TimePeriodModels.IndexOf(model);
            if (currentPeriodIndex < 0 || currentPeriodIndex == 0)
            {
                return null;
            }

            return TimePeriodModels[currentPeriodIndex - 1];
        }

        /// <summary>
        /// Gets timePeriodModels in a pay period for a given position id
        /// </summary>
        /// <param name="payCyleId"></param>
        /// <param name="payPeriodStartDate"></param>
        /// <param name="payPeriodEndDate"></param>
        /// <returns></returns>
        public IEnumerable<TimePeriodModel> GetTimePeriodModels(string payCyleId, string positionId, DateTime payPeriodStartDate, DateTime payPeriodEndDate)
        {
            return TimePeriodModels.Where(tp =>
                tp.StartDate <= payPeriodEndDate &&
                tp.EndDate >= payPeriodStartDate &&
                tp.TimecardPositionStatuses.Any(tps =>
                    tps.PayCycleId == payCyleId &&
                    tps.PositionId == positionId));
        }


    }
}