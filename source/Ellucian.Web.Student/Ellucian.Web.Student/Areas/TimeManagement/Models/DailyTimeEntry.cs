﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.TimeManagement;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class DailyTimeEntry : IComparable<DailyTimeEntry>, IComparable, IComparer<DailyTimeEntry>
    {
        public string Id { get; set; }
        public DateTime DateWorked { get; set; }
        public decimal? HoursWorked { get; set; }
        public DateTime? TimeStart { get; set; }
        public DateTime? TimeEnd { get; set; }
        public bool IsReadOnly { get; private set; }
        public bool IsHistorical { get; private set; }

        //MessageChannel is a generic list for front end components to
        //pass messages via the data model.                
        public List<string> MessageChannel { get; set; }

         /// <summary>
         /// For use when building an existing DailyTimeEntry
         /// </summary>
         /// <param name="timeEntryDto"></param>
         /// <param name="isReadOnly"></param>
         /// <param name="isHistorical"></param>
        public DailyTimeEntry(Colleague.Dtos.TimeManagement.TimeEntry2 timeEntryDto, bool isReadOnly, bool isHistorical)
        {            
            Id = timeEntryDto.Id;
            DateWorked = timeEntryDto.WorkedDate.Date;
            if (timeEntryDto.WorkedTime.HasValue)
            {
                HoursWorked = (decimal?)timeEntryDto.WorkedTime.Value.TotalHours;
            }
            else
            {   //if either inDateTime or OutDatetime has a value, set hours worked  to 0.
                //if both are null, hours worked is null
                HoursWorked = (timeEntryDto.InDateTime.HasValue || timeEntryDto.OutDateTime.HasValue) ? 0 : (decimal?)null; 
            }
            //HoursWorked = timeEntryDto.WorkedTime.HasValue ? (decimal?)timeEntryDto.WorkedTime.Value.TotalHours : null;
            TimeStart = timeEntryDto.InDateTime;
            TimeEnd = timeEntryDto.OutDateTime;
            IsReadOnly = isReadOnly;
            IsHistorical = isHistorical;
            MessageChannel = new List<string>();
        }


        /// <summary>
        /// For use when building a non-existent DailyTimeEntry
        /// </summary>
        /// <param name="dateWorked"></param>
        /// <param name="isReadOnly"></param>
        public DailyTimeEntry(DateTime dateWorked, bool isReadOnly)
        {
            Id = "";
            DateWorked = DateTime.SpecifyKind(dateWorked.Date, DateTimeKind.Unspecified);
            HoursWorked = null;
            TimeStart = null;
            TimeEnd = null;
            IsReadOnly = isReadOnly;
            IsHistorical = false;
            MessageChannel = new List<string>();
        }

        public DailyTimeEntry()
        {
            MessageChannel = new List<string>();
        }

        public int CompareTo(DailyTimeEntry other)
        {

            //ideally we want to compare start times.

            var compareThisTime = this.TimeStart ?? this.TimeEnd;
            var toOtherTime = other.TimeStart ?? other.TimeEnd;

            if (!compareThisTime.HasValue || !toOtherTime.HasValue) 
            {
                //if either of the times don't have value, then we want the null time to be after the
                //non-null time (the Nullable.Compare default is that the null time is before the non-null time)
                //if they're both null, -0 is 0;
                return -Nullable.Compare(compareThisTime, toOtherTime);;
            }
            else 
            {
                
                return Nullable.Compare(compareThisTime, toOtherTime);;
            }
        }

        public int CompareTo(object obj)
        {
            return CompareTo((DailyTimeEntry)obj);
        }

        public int Compare(DailyTimeEntry x, DailyTimeEntry y)
        {
            return x.CompareTo(y);
        }
    }
}