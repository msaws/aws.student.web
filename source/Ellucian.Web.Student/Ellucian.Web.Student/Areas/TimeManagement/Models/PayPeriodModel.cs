﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    [Serializable]
    public class PayPeriodModel
    {
        public DateTime PeriodStartDate;
        public DateTime PeriodEndDate;
        public DateTimeOffset SupervisorCutoffDateTime;
        public bool IsForHistoricalPayPeriod;
        public string PayCycleId;
        public string PayCycleDescription;
        public List<EmployeeModel> EmployeeModels;


        public PayPeriodModel(PositionQuery supervisorPositionQuery,
            ApproverQueryPayPeriodResult approverQueryResultItem,
            List<Position> positions,
            List<PersonPosition> personPositions,
            Dictionary<string, HumanResourceDemographics> employeeDictionary,
            IEnumerable<PersonEmploymentStatus> personEmploymentStatuses)
        {
            PeriodStartDate = approverQueryResultItem.PeriodStartDate;
            PeriodEndDate = approverQueryResultItem.PeriodEndDate;
            SupervisorCutoffDateTime = approverQueryResultItem.SupervisorTimecardCutoffDateTime.Value;
            IsForHistoricalPayPeriod = approverQueryResultItem.IsForHistoricalPayPeriod;
            PayCycleId = approverQueryResultItem.PayCycle.Id;
            PayCycleDescription = approverQueryResultItem.PayCycle.Description;
            EmployeeModels = new List<EmployeeModel>();

            // Use the data given to create EmployeeModels
            foreach (var timecardsForPerson in approverQueryResultItem.TimecardsForPerson)
            {
                // pass in the supervisorId, list of timecards for an employee, and the list of Positions and PersonPositions
                var employeeModel = new EmployeeModel(approverQueryResultItem.PeriodStartDate,
                    approverQueryResultItem.PeriodEndDate,
                    supervisorPositionQuery,
                    timecardsForPerson,
                    positions,
                    personPositions,
                    employeeDictionary,
                    personEmploymentStatuses.Where(p => p.PersonId == timecardsForPerson.EmployeeId).ToList());

                //add the employee if any of the employee's positions is supervised by the current user
                if (employeeModel.Positions.Any(p => p.IsSupervised))
                {
                    EmployeeModels.Add(employeeModel);
                }

            }
            //sort EmployeeModels
            EmployeeModels.OrderBy(x => x.EmployeeName);
        }
    }
}