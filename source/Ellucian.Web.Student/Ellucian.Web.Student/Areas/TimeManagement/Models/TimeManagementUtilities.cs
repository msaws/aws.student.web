﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public static class TimeManagementUtilities
    {

        /// <summary>
        /// Build a 2-dimensional List of Weeks for the given start and end dates
        /// Each entry in the list represents a Week in the date range
        /// Each entry in the list is a list of DateTime objects representing the days of the week.
        /// 
        /// </summary>
        /// <param name="payPeriod">the payPeriod to convert to Weeks</param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<DateTime>> Weeks(DateTime startDate, DateTime endDate)
        {
            var returnWeeks = new List<IEnumerable<DateTime>>();
            var weekDates = new List<DateTime>(7);
            for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
            {
                weekDates.Add(i);
                if ((weekDates.Count == 7) || (i == endDate))
                {
                    returnWeeks.Add(weekDates.ToArray());
                    weekDates = new List<DateTime>(7);
                }
            }

            return returnWeeks;
        }

        /// <summary>
        /// Build a 2-dimensional List of Weeks in the given PayPeriod.
        /// Each entry in the list represents a Week in the pay period.
        /// Each entry in the list is a list of DateTime objects representing the days of the week.
        /// 
        /// </summary>
        /// <param name="this">the payPeriod to convert to Weeks</param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<DateTime>> Weeks(this PayPeriod @this)
        {
            return Weeks(@this.StartDate, @this.EndDate);
        }

        /// <summary>
        /// Finds a timecard for a position for a given date
        /// </summary>
        /// <param name="timecards">List of timecards to search from</param>
        /// <param name="positionQueryResult">Position to search for</param>
        /// <param name="Date">Date that the returned timecard must envelope</param>
        /// <returns></returns>
        public static Timecard2 FindTimecardDtoForPositionForDate(this IEnumerable<Timecard2> @this, PositionQueryResultItem positionQueryResult, DateTime Date)
        {
            return @this.FirstOrDefault(x =>
                x.StartDate <= Date &&
                x.EndDate >= Date &&
                x.PositionId == positionQueryResult.PositionId);
        }


        /// <summary>
        /// Finds a time period for a specific date range.
        /// </summary>
        /// <param name="timePeriodCollection"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static TimePeriodModel FindTimePeriodForDate(this IEnumerable<TimePeriodModel> @this, DateTime startDate, DateTime endDate)
        {
            return @this.FirstOrDefault(x => x.StartDate == startDate && x.EndDate == endDate);
        }

        /// <summary>
        /// Creates a new timecard for a given position
        /// </summary>
        /// <param name="position"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public static Timecard2 CreateNewTimecardForPosition(PositionQueryResultItem position, String personId, DateTime startDate, DateTime endDate)
        {

            if (position == null) throw new ArgumentNullException("position can not be null");
            if (String.IsNullOrWhiteSpace(personId)) throw new ArgumentNullException("personId is required to create a Timecard");

            Timecard2 newTimecard2 = new Timecard2();
            // add data to newTimecard
            newTimecard2.EmployeeId = personId;
            newTimecard2.PositionId = position.PositionId;
            newTimecard2.PayCycleId = position.PayCycle.Id;
            newTimecard2.Id = "";


            // TODO: revisit when adding support for non-week based pay periods like monthly and semi-monthly
            var payPeriod = position.GetPayPeriod(endDate);
            newTimecard2.PeriodStartDate = payPeriod.StartDate;
            newTimecard2.PeriodEndDate = payPeriod.EndDate;

            newTimecard2.StartDate = startDate;
            newTimecard2.EndDate = endDate;
            newTimecard2.TimeEntries = new List<Colleague.Dtos.TimeManagement.TimeEntry2>();

            return newTimecard2;
        }

        public static DateTime GetMinDate(params DateTime[] array)
        {
            return array.OrderBy(d => d).First();
        }

        public static DateTime GetMaxDate(params DateTime[] array)
        {
            return array.OrderBy(d => d).Last();
        }

        /// <summary>
        /// Formats the name of the specified human resource in Last, First MI format. 
        /// Method uses best case scenario if name or portions of name do not exist.
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="nameDictionary"></param>
        /// <returns></returns>
        public static string FormatEmployeeName(string employeeId, Dictionary<string, HumanResourceDemographics> nameDictionary)
        {
            var employeeName = "";
            HumanResourceDemographics thisHumanResource;
            
            // if there is no HumanResourceDemographics in the dictionary for the given Id, set the EmployeeName as the Id
            if (!nameDictionary.TryGetValue(employeeId, out thisHumanResource))
            {
                return employeeId;
            }
            else
            {
                // if the employee does NOT have a middle name
                if (string.IsNullOrWhiteSpace(thisHumanResource.MiddleName))
                {

                    var lastName = thisHumanResource.LastName;
                    if (!string.IsNullOrWhiteSpace(thisHumanResource.FirstName))
                    {
                        employeeName = lastName + ", " + thisHumanResource.FirstName;
                    }
                    else
                    {
                        employeeName = lastName;
                    }
                }
                // if the employee has a middle name
                else
                {
                    var lastName = thisHumanResource.LastName;
                    if (!string.IsNullOrWhiteSpace(thisHumanResource.FirstName))
                    {
                        employeeName = lastName + ", " + thisHumanResource.FirstName + " " + thisHumanResource.MiddleName[0] + ".";
                    }
                    else
                    {
                        employeeName = lastName + " " + thisHumanResource.MiddleName[0] + ".";
                    }
                }
            }
            return employeeName;
        }

        /// <summary>
        /// Computes a mod n; - euclidean style (aka positive remainder)
        /// .NET's % operator actually computes a remainder. So we do a double mod in order
        /// to always compute a positive remainder, which is necessary for circular arrays such
        /// as the DayOfWeek enum.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="n"></param>
        /// <example>Mod(5, 7) = 5</example>
        /// <example>Mod(9, 7) = 2</example>
        /// <example>Mod(-3, 7) = 4 (whereas -3 % 7 = -3, which is OutOfBounds when dealing with arrays and enums</example>
        /// <returns></returns>
        public static int Mod(int a, int n)
        {
            var result = ((a % n) + n) % n;
            return result;
        }

        public static DateTime NormalizeToStartDayOfWeek(this DateTime @this, DayOfWeek startDayOfWeek)
        {
            return @this.DayOfWeek == startDayOfWeek ?
                @this :
                @this.AddDays(-(TimeManagementUtilities.Mod(@this.DayOfWeek - startDayOfWeek, 7)));      
        }

        public static DateTime NormalizeToEndDateOfWeek(this DateTime @this, DayOfWeek endDayOfWeek)
        {
            return @this.DayOfWeek == endDayOfWeek ?
                    @this :
                    @this.AddDays(TimeManagementUtilities.Mod(@this.DayOfWeek - endDayOfWeek, 7));
        }

        public static Timecard2 ConvertToTimecard(this TimecardHistory2 @this)
        {
            return new Timecard2()
            {
                Id = @this.Id,
                EmployeeId = @this.EmployeeId,
                EndDate = @this.EndDate,
                PayCycleId = @this.PayCycleId,
                PeriodEndDate = @this.PeriodEndDate,
                PeriodStartDate = @this.PeriodStartDate,
                PositionId = @this.PositionId,
                StartDate = @this.StartDate,
                Timestamp = @this.Timestamp,
                TimeEntries = @this.TimeEntryHistories.Select(h => new TimeEntry2()
                {
                    Id = h.Id,
                    EarningsTypeId = h.EarningsTypeId,
                    InDateTime = h.InDateTime,
                    OutDateTime = h.OutDateTime,
                    PersonLeaveId = h.PersonLeaveId,
                    ProjectId = h.ProjectId,
                    TimecardId = h.TimecardHistoryId,
                    Timestamp = h.Timestamp,
                    WorkedDate = h.WorkedDate,
                    WorkedTime = h.WorkedTime
                }).ToList()
            };
        }

        /// <summary>
        /// Insert the item into a sorted list only if the item does not already exist using the default comparer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <param name="item"></param>
        public static int AddSortedDistinctOnly<T>(this List<T> @this, T item) where T : IComparable<T>
        {
            if (@this.Count == 0)
            {
                @this.Add(item);
                return 0;
            }
            if (@this[@this.Count - 1].CompareTo(item) <= 0)
            {
                @this.Add(item);
                return @this.Count - 1;
            }
            if (@this[0].CompareTo(item) >= 0)
            {
                @this.Insert(0, item);
                return 0;
            }
            int index = @this.BinarySearch(item);
            if (index < 0)
            {
                index = ~index;
                @this.Insert(index, item);
                return index;
            }
            return -1; //if index is positive, then this item was already found in the array            
        }
    }
}