﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class TimecardPositionStatusModel
    {
        public string TimecardId { get; set; }
        public string PayCycleId { get; set; }
        public string PositionId { get; set; }
        public string PositionTitle { get; set; }
        public bool IsPositionPrimary { get; set; }
        public int? CurrentStatus;

        public TimecardPositionStatusModel(string positionId, string positionTitle, string payCycleId, bool isPositionPrimary, int? currentStatus)
        {
            this.PositionId = positionId;
            this.PositionTitle = positionTitle;
            this.PayCycleId = payCycleId;
            this.CurrentStatus = currentStatus;
            this.IsPositionPrimary = isPositionPrimary;
        }

        public TimecardPositionStatusModel(PositionQueryResultItem positionQueryResult)
        {
            this.PositionId = positionQueryResult.PositionId;
            this.PositionTitle = positionQueryResult.PositionTitle;
        }
    }
}