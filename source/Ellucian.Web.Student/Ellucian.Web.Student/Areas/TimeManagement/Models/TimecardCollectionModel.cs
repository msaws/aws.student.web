﻿using Ellucian.Colleague.Dtos.Base;
//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class TimecardCollectionModel
    {
        /// <summary>
        /// This is the start date of the week that's displayed to the user.
        /// We can derive it by looking at the earliest DateWorked of any timecard row
        /// </summary>
        public DateTime EffectiveStartDate { get; set; }

        /// <summary>
        /// This is the true start date of timecard collection. Any days displayed to the user prior to this date 
        /// are read only.
        /// </summary>
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PayCycleId { get; set; }


        public DateTime? NextStartDate { get; set; }
        public DateTime? NextEndDate { get; set; }
        public DateTime? NextPayPeriodStartDate { get; set; }
        public DateTime? NextPayPeriodEndDate { get; set; }

        public DateTime? PreviousStartDate { get; set; }
        public DateTime? PreviousEndDate { get; set; }
        public DateTime? PreviousPayPeriodStartDate { get; set; }
        public DateTime? PreviousPayPeriodEndDate { get; set; }

        private Dictionary<string, HumanResourceDemographics> EmployeeDictionary { get; set; }
        public string EmployeeId { get; set; }


        public string EmployeeName
        {
            get
            {
                return TimeManagementUtilities.FormatEmployeeName(EmployeeId, EmployeeDictionary);
            }
        }

        public ReadOnlyCollection<TimecardModel> Timecards
        {
            get
            {
                Sort();
                return this.timecards.AsReadOnly();
            }
        }
        private List<TimecardModel> timecards;

        public void Sort()
        {
            timecards.Sort();
            timecards.ForEach(t => 
                t.WorkEntryList.ForEach(w => 
                    w.DailyTimeEntryGroupList.ForEach(g => 
                        g.DailyTimeEntryList.Sort())));
        }


        public void AddTimecard(TimecardModel timecard)
        {
            if (timecard.IsValid())
            {
                this.timecards.Add(timecard);
            }
        }

        public bool Has(string timecardId)
        {
            return timecards.Any(t => t.TimecardId == timecardId);
        }

        public TimecardCollectionModel()
        {
            timecards = new List<TimecardModel>();
        }
        public TimecardCollectionModel(string employeeId, string payCycleId, DateTime startDate, DateTime endDate, Dictionary<string, HumanResourceDemographics> employeeDictionary)
        {
            EmployeeId = employeeId;
            PayCycleId = payCycleId;
            timecards = new List<TimecardModel>();
            StartDate = startDate;
            EndDate = endDate;
            EmployeeDictionary = employeeDictionary;
        }

        public TimecardCollectionModel(
            string employeeId, 
            string payCycleId, 
            DateTime startDate, 
            DateTime endDate, 
            Dictionary<string, HumanResourceDemographics> employeeDictionary, 
            PositionQuery positionQuery,
            TimecardQueryDataCoordinator timecardQuery, 
            IEnumerable<PersonEmploymentStatus> personEmploymentStatusDtos,
            IEnumerable<EarningsType> earningsTypeDtos,
            IEnumerable<Department> departmentDtos,
            IEnumerable<Location> locationDtos) 
        {
            EmployeeId = employeeId;
            PayCycleId = payCycleId;
            StartDate = startDate;
            EndDate = endDate;
            EmployeeDictionary = employeeDictionary;
            timecards = new List<TimecardModel>();

            var payCycle = positionQuery.GetPayCycle(payCycleId);

            EffectiveStartDate = startDate.NormalizeToStartDayOfWeek(payCycle.WorkWeekStartDay);

            var migratedPositionQueryResults = positionQuery
                .Get(EffectiveStartDate, EffectiveStartDate.AddDays(6), payCycleId)
                .Where(pqr => pqr.IsMigratedForThisDate(startDate));


            foreach (var positionQueryResult in migratedPositionQueryResults) 
            {
                var adjustedStartDate = TimeManagementUtilities.GetMaxDate(startDate, positionQueryResult.PersonPosition.StartDate);
                var adjustedEndDate = TimeManagementUtilities.GetMinDate(endDate, positionQueryResult.PersonPosition.EndDate ?? DateTime.MaxValue);

                var timecardQueryResultContext = timecardQuery.FindTimecardQueryResultContext(positionQueryResult, adjustedStartDate, adjustedEndDate);
                

                
                if (timecardQueryResultContext == null)
                {
                    var newTimecardDto = TimeManagementUtilities.CreateNewTimecardForPosition(positionQueryResult, employeeId, adjustedStartDate, adjustedEndDate);
                    timecardQueryResultContext = timecardQuery.InsertTimecard(newTimecardDto);                      
                }

                var timecardModel = new TimecardModel(timecardQueryResultContext, positionQueryResult, earningsTypeDtos, personEmploymentStatusDtos, departmentDtos, locationDtos, employeeDictionary, EffectiveStartDate);
                timecards.Add(timecardModel);
            }
        }

        public void AddTimePeriodContext(TimePeriodModel previousTimePeriod, TimePeriodModel nextTimePeriod)
        {
            if (previousTimePeriod != null)
            {
                PreviousStartDate = previousTimePeriod.StartDate;
                PreviousEndDate = previousTimePeriod.EndDate;
                PreviousPayPeriodStartDate = previousTimePeriod.PayPeriodStartDate;
                PreviousPayPeriodEndDate = previousTimePeriod.PayPeriodEndDate;
            }

            if (nextTimePeriod != null)
            {
                NextStartDate = nextTimePeriod.StartDate;
                NextEndDate = nextTimePeriod.EndDate;
                NextPayPeriodStartDate = nextTimePeriod.PayPeriodStartDate;
                NextPayPeriodEndDate = nextTimePeriod.PayPeriodEndDate;
            }
        }
    }
}