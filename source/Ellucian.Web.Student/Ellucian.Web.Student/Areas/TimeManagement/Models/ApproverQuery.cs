﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class ApproverQuery : IQueryable<ApproverQueryPayPeriodResult>
    {
        private List<ApproverQueryPayPeriodResult> list;

        public ApproverQuery(IEnumerable<Timecard2> Timecards2,
            IEnumerable<TimecardStatus> timecardStatuses,
            IEnumerable<PayCycle> payCycles,
            IEnumerable<TimeEntryComments> comments)
        {
            list = new List<ApproverQueryPayPeriodResult>();

            var timecardQuery = new TimecardQueryDataCoordinator(string.Empty, Timecards2, new List<TimecardHistory2>(), timecardStatuses, comments);

            foreach (var payCycleId in timecardQuery.PayCycleIds)
            {
                var payCycle = payCycles.FirstOrDefault(p => p.Id == payCycleId);
                if (payCycle != null)
                {
                    var payPeriodDates = timecardQuery[payCycleId].Select(t => new Tuple<DateTime, DateTime>(t.PayPeriodComparer.PeriodStartDate, t.PayPeriodComparer.PeriodEndDate)).Distinct();
                    foreach (var dateSet in payPeriodDates)
                    {
                        var normalizedStartDate = dateSet.Item1.NormalizeToStartDayOfWeek(payCycle.WorkWeekStartDay);
                       // var normalizedEndDate = dateSet.Item2; //dateSet.Item2.NormalizeToEndDateOfWeek((DayOfWeek)((int)payCycle.WorkWeekStartDay + 6 % 7));

                        var resultsInNormalizedPayPeriod = timecardQuery
                            .GetResultsForDateRange(normalizedStartDate, dateSet.Item2, payCycleId);

                        if (resultsInNormalizedPayPeriod.Any())
                        {
                            list.Add(new ApproverQueryPayPeriodResult(resultsInNormalizedPayPeriod, payCycle, dateSet.Item1, dateSet.Item2));
                        }

                    }
                }
            }

            list = list
                .Where(qr => qr.EmployeeTimecardCutoffDateTime.HasValue &&
                        qr.SupervisorTimecardCutoffDateTime.HasValue &&
                        qr.TimecardsForPerson.Any())
                .OrderByDescending(x => x.PeriodEndDate)
                .ToList();


        }

        public ApproverQueryPayPeriodResult this[int index]
        {
            get
            {
                return list[index];
            }

            set
            {
                list[index] = value;
            }
        }

        public int Count()
        {
            return list.Count();
        }



        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public Type ElementType
        {
            get { return typeof(ApproverQueryPayPeriodResult); }
        }

        public System.Linq.Expressions.Expression Expression
        {
            get { return list.AsQueryable().Expression; }
        }

        public IQueryProvider Provider
        {
            get { return list.AsQueryable().Provider; }
        }

        IEnumerator<ApproverQueryPayPeriodResult> IEnumerable<ApproverQueryPayPeriodResult>.GetEnumerator()
        {
            return list.GetEnumerator();
        }
    }

    // A list of TimecardsForPersonItems for a specific PayPeriod
    public class ApproverQueryPayPeriodResult
    {
        public DateTime PeriodStartDate;
        public DateTime PeriodEndDate;
        public DateTimeOffset? SupervisorTimecardCutoffDateTime;
        public DateTimeOffset? EmployeeTimecardCutoffDateTime;
        public PayCycle PayCycle;
        public List<ApproverQueryEmployeeResult> TimecardsForPerson;
        public bool IsForHistoricalPayPeriod;

        /// <summary>
        /// Represents timecards in a pay period (including split weeks from previous and next periods) for all employees supervised by the current user
        /// </summary>
        /// <param name="timecardsForPeriod"></param>
        /// <param name="payCycle"></param>
        public ApproverQueryPayPeriodResult(IEnumerable<TimecardQueryResultItem> timecardsForPeriod, PayCycle payCycle, DateTime payPeriodStartDate, DateTime payPeriodEndDate)
        {
            TimecardsForPerson = new List<ApproverQueryEmployeeResult>();

            PayCycle = payCycle;

            if (PayCycle != null)
            {
                // sort TimecardQueryResultItems by employee
                var timecardsByPerson = from tc in timecardsForPeriod
                                        group tc by tc.TimecardJoiner.EmployeeId into timecardsForEmployee
                                        select timecardsForEmployee;

                TimecardsForPerson = timecardsByPerson.Select(t => new ApproverQueryEmployeeResult(t)).ToList();

                // set the PeriodEndDate & PeriodStartDate
                PeriodEndDate = payPeriodEndDate;
                PeriodStartDate = payPeriodStartDate;

                SupervisorTimecardCutoffDateTime =
                            payCycle.PayPeriods.FirstOrDefault(x => x.EndDate == PeriodEndDate && x.StartDate == PeriodStartDate) != null ?
                            payCycle.PayPeriods.FirstOrDefault(x => x.EndDate == PeriodEndDate && x.StartDate == PeriodStartDate).SupervisorTimecardCutoffDateTime :
                            new DateTimeOffset();

                EmployeeTimecardCutoffDateTime =
                    payCycle.PayPeriods.Where(x => x.EndDate == PeriodEndDate && x.StartDate == PeriodStartDate).FirstOrDefault() != null ?
                    payCycle.PayPeriods.Where(x => x.EndDate == PeriodEndDate && x.StartDate == PeriodStartDate).FirstOrDefault().EmployeeTimecardCutoffDateTime :
                    new DateTimeOffset();

                IsForHistoricalPayPeriod = payCycle.PayPeriods.Where(x => x.EndDate == PeriodEndDate && x.StartDate == PeriodStartDate).FirstOrDefault() != null ?
                    payCycle.PayPeriods.Where(x => x.EndDate == PeriodEndDate && x.StartDate == PeriodStartDate).FirstOrDefault().Status == PayPeriodStatus.History :
                    false;

            }
        }

    }

    // A list of TimecardQueryResultItems for a specific Employee
    public class ApproverQueryEmployeeResult
    {
        public string EmployeeId;
        public List<TimecardQueryResultItem> Timecards;

        public ApproverQueryEmployeeResult(IEnumerable<TimecardQueryResultItem> timecards)
        {

            Timecards = new List<TimecardQueryResultItem>();
            if (timecards.Any())
            {
                Timecards.AddRange(timecards);
                EmployeeId = Timecards[0].Timecard2.EmployeeId;

            }
        }
    }

}