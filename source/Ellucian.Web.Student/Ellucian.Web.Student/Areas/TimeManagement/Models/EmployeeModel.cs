﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    [Serializable]
    public class EmployeeModel
    {
        public string EmployeeId;
        public string EmployeeName;
        //public StatusAction? Status;
        public List<PositionModel> Positions;
        public decimal OvertimeHours;


        public EmployeeModel(DateTime periodStartDate,
            DateTime periodEndDate,
            PositionQuery supervisorPositionQuery,
            ApproverQueryEmployeeResult timecardsForPersonItem,
            IEnumerable<Position> positions,
            IEnumerable<PersonPosition> personPositions,
            Dictionary<string, HumanResourceDemographics> employeeDictionary,
            IEnumerable<PersonEmploymentStatus> personEmploymentStatuses)
        {
            EmployeeId = timecardsForPersonItem.EmployeeId;
            Positions = new List<PositionModel>();
            OvertimeHours = 0;
            EmployeeName = TimeManagementUtilities.FormatEmployeeName(EmployeeId, employeeDictionary);

            // find the employee's person positions for the given pay period date range
            var employeePersonPositions = personPositions.Where(pp => pp.PersonId == EmployeeId &&
                    pp.StartDate.Date <= periodEndDate.Date &&
                    (pp.EndDate.HasValue ? pp.EndDate.Value.Date : DateTime.MaxValue) >= periodStartDate.Date);

            //select the position ids of the employee's person positions
            //because the supervisor has access to his/her supervisee data, these are a portion of the position ids supervised by the current user
            var supervisorPositionIdsFromPersonPositions = employeePersonPositions
                    .Where(x => x.SupervisorId == supervisorPositionQuery.PersonId)
                    .Select(y => y.PositionId);

            //select the position ids from the subset of the employee's positions that are position-supervised by the supervisor's position
            var supervisorPositionIdsFromPositions = positions
                .Where(p => employeePersonPositions.Any(ppos => p.Id == ppos.PositionId)) // filter positions to subset of employee's positions
                .Where(p => supervisorPositionQuery.Any(qr => qr.PositionId == p.SupervisorPositionId)) // filter to positions that are position-supervised by supervisor's position
                .Select(p => p.Id);

            var supervisedPositionIds = supervisorPositionIdsFromPersonPositions.Union(supervisorPositionIdsFromPositions);
    

            // for each position the employee has, build a PositionModel
            var distinctPositionIds = employeePersonPositions.Select(p => p.PositionId).Distinct();
            foreach (var positionId in distinctPositionIds)
            {
                var positionTitle = positions.Where(x => x.Id == positionId).FirstOrDefault().Title;
                var isSupervisorBool = supervisedPositionIds.Contains(positionId);
                var timecardsForPosition = timecardsForPersonItem.Timecards.Where(x => x.Timecard2.PositionId == positionId).ToList();
                if (timecardsForPosition.Any())
                {
                    var isPrimaryPosition = personEmploymentStatuses.Any(s =>
                        s.PersonId == EmployeeId &&
                        s.PrimaryPositionId == positionId &&
                        s.StartDate <= periodEndDate &&
                        (!s.EndDate.HasValue || s.EndDate.Value >= periodStartDate));

                    Positions.Add(new PositionModel(positionId, positionTitle, isSupervisorBool, isPrimaryPosition, timecardsForPosition));
                }
            }

            Positions = Positions.OrderByDescending(tc => tc.IsPrimary).ThenBy(tc => tc.Title).ToList();


        }
    }

}