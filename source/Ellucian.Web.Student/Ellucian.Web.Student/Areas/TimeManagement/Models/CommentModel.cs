﻿using Ellucian.Colleague.Dtos.TimeManagement;
//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    /// <summary>
    /// User comments for use in timecard or timecardStatus
    /// </summary>
    public class CommentModel
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string TimecardId { get; set; }
        public string TimecardStatusId { get; set; }
        public string PositionId { get; set; }
        public string PayCycleId { get; set; }
        public DateTime PayPeriodEndDate { get; set; }
        public DateTimeOffset CommentDateTime { get; set; }
        public string CommentAuthorName { get; set; }
        public string CommentText { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CommentModel()
        {

        }

        public CommentModel(TimeEntryComments timeEntryCommentsDto)
        {
            Id = timeEntryCommentsDto.Id;
            EmployeeId = timeEntryCommentsDto.EmployeeId;
            TimecardId = timeEntryCommentsDto.TimecardId;
            TimecardStatusId = timeEntryCommentsDto.TimecardStatusId;
            PositionId = timeEntryCommentsDto.PositionId;
            PayCycleId = timeEntryCommentsDto.PayCycleId;
            PayPeriodEndDate = timeEntryCommentsDto.PayPeriodEndDate;
            CommentDateTime = timeEntryCommentsDto.CommentsTimestamp.AddDateTime;
            CommentAuthorName = timeEntryCommentsDto.CommentAuthorName;
            CommentText = timeEntryCommentsDto.Comments;

        }


        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var commentModel = obj as CommentModel;

            if (string.IsNullOrEmpty(this.Id) && string.IsNullOrEmpty(commentModel.Id))
            {
                return this.EmployeeId == commentModel.EmployeeId &&
                    this.PayCycleId == commentModel.PayCycleId &&
                    this.PayPeriodEndDate == commentModel.PayPeriodEndDate &&
                    this.PositionId == commentModel.PositionId &&
                    this.TimecardId == commentModel.TimecardId &&
                    this.TimecardStatusId == commentModel.TimecardStatusId;
            }
            else
            {
                return this.Id == commentModel.Id;
            }
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}