﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery
{
    /// <summary>
    /// This class contains the correlated timecard data.
    /// </summary>
    public class TimecardQueryResultItem : IComparer<TimecardQueryResultItem>, IComparable<TimecardQueryResultItem>
    {
        private Timecard2 timecard2;
        private TimecardHistory2 timecardHistory2;

        /// <summary>
        /// The Status of the Timecard. Can be null
        /// </summary>
        public TimecardStatus Status { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public StatusAction? StatusActionType 
        { 
            get 
            { 
                return Status == null ? null : new Nullable<StatusAction>(Status.ActionType); 
            } 
        }

        public IEnumerable<TimeEntryComments> Comments { get; private set; }

        public bool IsHistorical
        {
            get
            {
                return timecard2 == null && timecardHistory2 != null;
            }
        }

        public Timecard2 Timecard2
        {
            get
            {
                if (timecard2 != null)
                {
                    return timecard2;
                }
                else
                {
                    return timecardHistory2.ConvertToTimecard();
                }
            }
        }

        /// <summary>
        /// Constructor builds a TimecardQueryResult that represents a period of time for which 
        /// an employee entered time worked in Self Service. The time worked is stored on either the timecard or the timecard history. 
        /// If the timecard is not null, it contains the time data of record. If the timecard is null, then the timecard history contains the data of record.
        /// 
        /// The timecard and timecardHistory arguments cannot both be null
        /// </summary>
        /// <param name="timecard2">Can be null</param>
        /// <param name="history2">Can be null</param>
        /// <param name="timecardStatus">Can be null</param>
        /// <param name="comments">Can be null</param>
        public TimecardQueryResultItem(Timecard2 timecard2, TimecardHistory2 history2, TimecardStatus timecardStatus, IEnumerable<TimeEntryComments> comments)
        {
            if (timecard2 == null && history2 == null)
            {
                throw new ArgumentNullException();
            }

            this.timecard2 = timecard2;
            timecardHistory2 = history2;

            Status = timecardStatus;
            Comments = comments != null ? comments : new List<TimeEntryComments>();

            if (timecard2 == null)
            {
                Status = new TimecardStatus() { ActionType = history2.StatusAction, HistoryId = history2.Id, Timestamp = history2.Timestamp, ActionerId = "" };
            }

            TimecardJoiner = timecard2 != null ? new TimecardComparer(timecard2) : new TimecardComparer(history2);
            PayPeriodComparer = new PositionPayPeriodComparer(TimecardJoiner);
        }

        /// <summary>
        /// Computes the TimecardId. Returns null if timecard is null
        /// </summary>
        public string TimecardId
        {
            get
            {
                return (this.timecard2 != null) ? this.timecard2.Id : null;
            }
        }

        /// <summary>
        /// Computes the TimecardHistoryId, Returns null if timecardHistory is null
        /// </summary>
        public string TimecardHistoryId
        {
            get
            {
                return (this.timecardHistory2 != null ? this.timecardHistory2.Id : null);
            }
        }



        /// <summary>
        /// Represets the attributes that allow timecardQueryResult items to be grouped by PayPeriod
        /// </summary>
        public PositionPayPeriodComparer PayPeriodComparer { get; private set; }

        /// <summary>
        /// Represets the attributes that correlate a Timecard with its History record. 
        /// </summary>
        public TimecardComparer TimecardJoiner { get; private set; }

        /// <summary>
        /// Two TimecardQueryResults are equal when the Ids of their Timecards match and when the Ids of their TimecardHistories match.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null) || obj.GetType() != GetType()) //check for null or different types
            {
                return false;
            }
            if (ReferenceEquals(obj, this)) //same object as this
            {
                return true;
            }           

            var thing = obj as TimecardQueryResultItem;

            return TimecardId == thing.TimecardId &&
                TimecardHistoryId == thing.TimecardHistoryId &&
                TimecardJoiner == thing.TimecardJoiner;
        }

        public static bool operator ==(TimecardQueryResultItem x, TimecardQueryResultItem y)
        {
            if (ReferenceEquals(x, y)) //handles if both are null or same object
            {
                return true;
            }
            if (ReferenceEquals(x, null))
            {
                return false;
            }
                
            return x.Equals(y);
        }

        public static bool operator !=(TimecardQueryResultItem x, TimecardQueryResultItem y)
        {
            return !(x == y);
        }

        public override int GetHashCode()
        {
            return (TimecardId == null ? 0 : TimecardId.GetHashCode()) ^
                (TimecardHistoryId == null ? 0 : TimecardHistoryId.GetHashCode()) ^
                TimecardJoiner.GetHashCode();
        }

        public int Compare(TimecardQueryResultItem x, TimecardQueryResultItem y)
        {
            if (x.Equals(y))
            {
                return 0;
            }
            else if (x.PayPeriodComparer.Equals(y.PayPeriodComparer))
            {
                return x.TimecardJoiner.CompareTo(y.TimecardJoiner);
            }
            else
            {
                return x.PayPeriodComparer.CompareTo(y.PayPeriodComparer);
            }
        }

        public int CompareTo(TimecardQueryResultItem other)
        {
            return this.Compare(this, other);
        }
    } 
}