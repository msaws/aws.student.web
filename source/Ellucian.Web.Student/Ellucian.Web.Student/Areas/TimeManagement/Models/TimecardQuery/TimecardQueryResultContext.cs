﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery
{
    public class TimecardQueryResultContext
    {
        public TimecardQueryResultItem Previous { get; private set; }
        public TimecardQueryResultItem Current { get; private set; }
        public TimecardQueryResultItem Next { get; private set; }

        public TimecardQueryResultContext(TimecardQueryResultItem prev, TimecardQueryResultItem curr, TimecardQueryResultItem next)
        {
            Previous = prev;
            Current = curr;
            Next = next;
        }
    }
}