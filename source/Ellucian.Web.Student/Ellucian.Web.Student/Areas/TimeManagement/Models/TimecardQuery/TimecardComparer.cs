﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery
{
    public class TimecardComparer : IComparer<TimecardComparer>, IComparable<TimecardComparer>
    {
        public string EmployeeId { get; private set; }
        public string PositionId { get; private set; }
        public string PayCycleId { get; private set; }
        public DateTime PeriodStartDate { get; private set; }
        public DateTime PeriodEndDate { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }

        public TimecardComparer(Timecard2 timecard2)
        {
            this.EmployeeId = timecard2.EmployeeId;
            this.PositionId = timecard2.PositionId;
            this.PayCycleId = timecard2.PayCycleId;
            this.PeriodStartDate = timecard2.PeriodStartDate;
            this.PeriodEndDate = timecard2.PeriodEndDate;
            this.StartDate = timecard2.StartDate;
            this.EndDate = timecard2.EndDate;
        }

        public TimecardComparer(TimecardHistory2 timecardHistory)
        {
            this.EmployeeId = timecardHistory.EmployeeId;
            this.PositionId = timecardHistory.PositionId;
            this.PayCycleId = timecardHistory.PayCycleId;
            this.PeriodStartDate = timecardHistory.PeriodStartDate;
            this.PeriodEndDate = timecardHistory.PeriodEndDate;
            this.StartDate = timecardHistory.StartDate;
            this.EndDate = timecardHistory.EndDate;
        }

        public static bool operator ==(TimecardComparer x, TimecardComparer y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }
            if (ReferenceEquals(x, null))
            {
                return false;
            }
            return x.Equals(y);
        }
        public static bool operator !=(TimecardComparer x, TimecardComparer y)
        {
            return !(x == y);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }
            if (ReferenceEquals(obj, this)) //same object as this
            {
                return true;
            }

            var joiner = obj as TimecardComparer;

            return EmployeeId == joiner.EmployeeId &&
                PositionId == joiner.PositionId &&
                PayCycleId == joiner.PayCycleId &&
                PeriodStartDate == joiner.PeriodStartDate &&
                PeriodEndDate == joiner.PeriodEndDate &&
                StartDate == joiner.StartDate &&
                EndDate == joiner.EndDate;
        }

        public override int GetHashCode()
        {
            return EmployeeId.GetHashCode() ^
                PositionId.GetHashCode() ^
                PayCycleId.GetHashCode() ^
                PeriodStartDate.GetHashCode() ^
                PeriodEndDate.GetHashCode() ^
                StartDate.GetHashCode() ^
                EndDate.GetHashCode();
        }

        public int Compare(TimecardComparer x, TimecardComparer y)
        {
            if (x.Equals(y))
            {
                return 0;
            }
            else if (x.PositionId != y.PositionId)
            {
                return x.PositionId.CompareTo(y.PositionId);
            }
            else if (x.PayCycleId != y.PayCycleId)
            {
                return x.PayCycleId.CompareTo(y.PayCycleId);
            }
            else if (x.StartDate < y.StartDate)
            {
                return -1;
            }
            else if (x.StartDate > y.StartDate)
            {
                return 1;
            }
            else
            {
                if (x.EndDate < y.EndDate)
                {
                    return -1;
                }
                else if (x.EndDate > y.EndDate)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int CompareTo(TimecardComparer other)
        {
            return this.Compare(this, other);
        }

    }

}