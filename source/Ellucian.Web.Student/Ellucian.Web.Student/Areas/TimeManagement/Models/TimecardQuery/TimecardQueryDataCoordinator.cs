﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery
{
    /// <summary>
    /// DataCoordinator correlates all the timecard data into TimecardQueryResultItems, which are grouped by PayCycle.
    /// Use lookup syntax to get results for a particular payCycle.
    /// 
    /// The number of results equals the number of timecards + the number of the most recent histories with no associated timecard.
    /// </summary>
    public class TimecardQueryDataCoordinator
    {
        private string personId;
        private IDictionary<string, List<TimecardQueryResultItem>> dictionary;

        /// <summary>
        /// For the given data, build a payCycleId key'd lookup.
        /// The results are timecards + the most recent histories with no associated timecard.
        /// 
        /// The query algorithm is: 
        /// 1. Find the most recent timecard history objects 
        /// 2. Join the most recent status and all related comments to each timecard.
        /// 3. Left Outer Join timecards to related timecard history - meaning return timecards with and without history
        /// 4. Left Outer Join timecard history to related timecards - meaning return timecard history with and without related timecards (indicates a timecard has been "paid")
        /// 5. Set Union of step 3 and 4 are the results.
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="timecard2Dtos"></param>
        /// <param name="timecardHistoryDtos"></param>
        /// <param name="timecardStatuses"></param>
        /// <param name="comments"></param>
        public TimecardQueryDataCoordinator(
            string personId,
            IEnumerable<Timecard2> timecard2Dtos,
            IEnumerable<TimecardHistory2> timecardHistoryDtos,
            IEnumerable<TimecardStatus> timecardStatuses,
            IEnumerable<TimeEntryComments> comments)
        {

            if (timecard2Dtos == null)
            {
                timecard2Dtos = new List<Timecard2>();
            }
            if (timecardHistoryDtos == null)
            {
                timecardHistoryDtos = new List<TimecardHistory2>();
            }
            if (timecardStatuses == null)
            {
                timecardStatuses = new List<TimecardStatus>();
            }
            if (comments == null)
            {
                comments = new List<TimeEntryComments>();
            }
            
            this.personId = personId;
            var employeeTimecards = string.IsNullOrEmpty(personId) ? timecard2Dtos :
                timecard2Dtos.Where(td => td.EmployeeId == personId);
             // filter out all timecardHistories that don't have a Paid status
            var employeetimecardHistory = string.IsNullOrEmpty(personId) ? timecardHistoryDtos.Where(th => th.StatusAction == StatusAction.Paid) : 
                timecardHistoryDtos.Where(th => th.EmployeeId == personId && th.StatusAction == StatusAction.Paid);

            
            var latestHistories = (from timecardHistory in employeetimecardHistory                                  
                                  group timecardHistory by new TimecardComparer(timecardHistory) into historyGroup
                                  select historyGroup.OrderByDescending(history => history.Timestamp.AddDateTime))
                                  .Select(historyGroup => historyGroup.FirstOrDefault())
                                  .Where(history => history != null).ToList();

            var timecardJoin =
                from timecard in employeeTimecards
                    join status in timecardStatuses on timecard.Id equals status.TimecardId into statusGroup                                            
                    join comment in comments on timecard.EmployeeId equals comment.EmployeeId into commentGroup             
                select new 
                {
                    Timecard = timecard, 
                    Status = statusGroup.OrderByDescending(s => s.Timestamp.AddDateTime).FirstOrDefault(), 
                    Comments = commentGroup
                        .Where(c => c.TimecardId == timecard.Id || 
                            (string.IsNullOrEmpty(c.TimecardId) && c.PositionId == timecard.PositionId && c.PayCycleId == timecard.PayCycleId && c.PayPeriodEndDate == timecard.PeriodEndDate))
                        .OrderBy(c => c.CommentsTimestamp.AddDateTime)
                };


            var timecardLeftOuterJoin = 
                from association in timecardJoin
                join timecardHistory in latestHistories on new TimecardComparer(association.Timecard) equals new TimecardComparer(timecardHistory) into historyGroup
                from history in historyGroup.DefaultIfEmpty()
                select new TimecardQueryResultItem(association.Timecard, history, association.Status, association.Comments);


            var timecardHistoryLeftOuterJoin = 
                from timecardHistory in latestHistories
                    join association in timecardJoin on new TimecardComparer(timecardHistory) equals new TimecardComparer(association.Timecard) into associationGroup                
                from assocation in associationGroup.DefaultIfEmpty()
                select new TimecardQueryResultItem(
                    assocation == null ? null : assocation.Timecard, 
                    timecardHistory, 
                    assocation == null ? null : assocation.Status, 
                    assocation == null ? null : assocation.Comments);



            dictionary =
                timecardLeftOuterJoin
                    .Union(timecardHistoryLeftOuterJoin)
                    .ToLookup(t => t.PayPeriodComparer.PayCycleId)
                    .Select(l => new KeyValuePair<string, List<TimecardQueryResultItem>>(l.Key, l.OrderBy(qt => qt).ToList()))
                    .ToDictionary(kv => kv.Key, v => v.Value);        
        }


        public IEnumerable<TimecardQueryResultItem> this[string payCycleId]
        {
            get
            {
                    if (!dictionary.ContainsKey(payCycleId))
                    {
                         return new List<TimecardQueryResultItem>();
                    }
                return dictionary[payCycleId];
            }      
        }

        public IEnumerable<string> PayCycleIds
        {
            get
            {
                return dictionary.Keys;
            }
        }

        public IEnumerable<TimecardQueryResultItem> GetOrderedResultsForPayPeriod(string payCycleId, DateTime payPeriodEndDate)
        {
            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("payCycleId");
            }
            if (!dictionary.ContainsKey(payCycleId))
            {
                    return new List<TimecardQueryResultItem>();
            }

            return dictionary[payCycleId]
                .Where(result => result.PayPeriodComparer.PeriodEndDate == payPeriodEndDate)
                .OrderBy(result => result)
                .ToList();
        }

        public IEnumerable<TimecardQueryResultItem> GetResultsForDateRange(PositionQuery positionQuery, DateTime startDate, DateTime endDate, string payCycleId)
        {
               if (!dictionary.ContainsKey(payCycleId))
               {
                    return new List<TimecardQueryResultItem>();
               }
            var validPositions = positionQuery.Select(p => p.PositionId);

            return dictionary[payCycleId].Where(qr =>
                //qr.Timecard != null &&
                validPositions.Contains(qr.TimecardJoiner.PositionId) &&
                qr.TimecardJoiner.PayCycleId == payCycleId &&
                qr.TimecardJoiner.StartDate.Date <= endDate.Date &&
                qr.TimecardJoiner.EndDate.Date >= startDate.Date);
        }

        public IEnumerable<TimecardQueryResultItem> GetResultsForDateRange(DateTime startDate, DateTime endDate, string payCycleId)
        {
            if (!dictionary.ContainsKey(payCycleId))
            {
                return new List<TimecardQueryResultItem>();
            }

            return dictionary[payCycleId].Where(qr =>
                qr.TimecardJoiner.PayCycleId == payCycleId &&
                qr.TimecardJoiner.StartDate <= endDate &&
                qr.TimecardJoiner.EndDate >= startDate);
        }

        public TimecardQueryResultItem FindTimecardQueryResult(PositionQueryResultItem positionQueryResultItem, DateTime startDate, DateTime endDate)
        {
               // forces the method to return null if the payCycleId isn't a key in the dictionary
               if (!dictionary.ContainsKey(positionQueryResultItem.PayCycle.Id))
               {
                    return null;
               }
            return dictionary[positionQueryResultItem.PayCycle.Id].FirstOrDefault(qr =>
                qr.TimecardJoiner.PositionId == positionQueryResultItem.PositionId &&
                qr.TimecardJoiner.StartDate.Date == startDate.Date &&
                qr.TimecardJoiner.EndDate.Date == endDate.Date);
        }

        public TimecardQueryResultContext FindTimecardQueryResultContext(PositionQueryResultItem positionQueryResultItem, DateTime startDate, DateTime endDate)
        {
            var result = FindTimecardQueryResult(positionQueryResultItem, startDate, endDate);
            return result == null ? null : GetContext(result);
        }

        public TimecardQueryResultContext GetContext(TimecardQueryResultItem timecardQueryResult) 
        {
            var payCycleId = timecardQueryResult.TimecardJoiner.PayCycleId;
               if (!dictionary.ContainsKey(payCycleId))
               {
                    return null;
               }
            var index = dictionary[payCycleId].BinarySearch(timecardQueryResult);
            if (index < 0) 
            {
                return null;
            }

            var context = buildContext(payCycleId, index);
            return context; 
        }

        public TimecardQueryResultContext InsertTimecard(Timecard2 timecardDto)
        {
               // if the dictionary doesn't contain the key, add it
               if (!dictionary.ContainsKey(timecardDto.PayCycleId))
               {
                    dictionary.Add(timecardDto.PayCycleId, new List<TimecardQueryResultItem>());

               }
            if (dictionary[timecardDto.PayCycleId] != null) 
            {
                var newQueryResult = new TimecardQueryResultItem(timecardDto, null, null, new List<Colleague.Dtos.TimeManagement.TimeEntryComments>());

                var index = dictionary[timecardDto.PayCycleId].AddSortedDistinctOnly(newQueryResult);
                if (index >= 0)
                {

                    var context = buildContext(timecardDto.PayCycleId, index);
                    return context;
                }                
            }
            return null;
        }

        //move some of this to the constructor of TQRC
        private TimecardQueryResultContext buildContext(string payCycleId, int currentIndex)
        {
               if (!dictionary.ContainsKey(payCycleId))
               {
                    return null;
               }
            if (dictionary[payCycleId] != null)
            {
                TimecardQueryResultItem current = dictionary[payCycleId][currentIndex];
                TimecardQueryResultItem next = null;
                TimecardQueryResultItem previous = null;

                if (currentIndex > 0)
                {
                    var potentialPrevious = dictionary[payCycleId][currentIndex - 1];
                    if (potentialPrevious.TimecardJoiner.PositionId == current.TimecardJoiner.PositionId)
                    {
                        previous = potentialPrevious;
                    }
                }
                if (currentIndex < dictionary[payCycleId].Count - 1)
                {
                    var potentialNext = dictionary[payCycleId][currentIndex + 1];
                    if (potentialNext.TimecardJoiner.PositionId == current.TimecardJoiner.PositionId)
                    {
                        next = potentialNext;
                    }
                }
                return new TimecardQueryResultContext(previous, current, next);

            }
            return null;
        }
 
    }


    


}