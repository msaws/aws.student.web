﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery
{


    public class PositionPayPeriodComparer : IComparable<PositionPayPeriodComparer>, IComparer<PositionPayPeriodComparer>
    {
        public string EmployeeId
        {
            get
            {
                return joiner != null ? joiner.EmployeeId : employeeId;
            }
        }
        private string employeeId;

        public string PositionId
        {
            get
            {
                return joiner != null ? joiner.PositionId : positionId;
            }
        }
        private string positionId;

        public string PayCycleId
        {
            get
            {
                return joiner != null ? joiner.PayCycleId : payCycleId;
            }
        }
        private string payCycleId;

        public DateTime PeriodStartDate
        {
            get
            {
                return joiner != null ? joiner.PeriodStartDate : periodStartDate;
            }
        }
        private DateTime periodStartDate;

        public DateTime PeriodEndDate
        {
            get
            {
                return joiner != null ? joiner.PeriodEndDate : PeriodEndDate;
            }
        }
        private DateTime periodEndDate;

        private TimecardComparer joiner;

        public PositionPayPeriodComparer(string employeeId, string positionId, string payCycleId, DateTime periodStartDate, DateTime periodEndDate)
        {
            this.employeeId = employeeId;
            this.positionId = positionId;
            this.payCycleId = payCycleId;
            this.periodStartDate = periodStartDate;
            this.periodEndDate = periodEndDate;
        }

        public PositionPayPeriodComparer(TimecardComparer joiner)
        {
            this.joiner = joiner;
        }

        public static bool operator ==(PositionPayPeriodComparer x, PositionPayPeriodComparer y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }
            if (ReferenceEquals(x, null))
            {
                return false;
            }
            return x.Equals(y);
        }
        public static bool operator !=(PositionPayPeriodComparer x, PositionPayPeriodComparer y)
        {
            return !(x == y);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }
            if (ReferenceEquals(obj, this)) //same object as this
            {
                return true;
            }

            var joiner = obj as PositionPayPeriodComparer;

            return EmployeeId == joiner.EmployeeId &&
                PositionId == joiner.PositionId &&
                PayCycleId == joiner.PayCycleId &&
                PeriodStartDate == joiner.PeriodStartDate &&
                PeriodEndDate == joiner.PeriodEndDate;
        }

        public override int GetHashCode()
        {
            return EmployeeId.GetHashCode() ^ PositionId.GetHashCode() ^ PayCycleId.GetHashCode() ^ PeriodStartDate.GetHashCode() ^ PeriodEndDate.GetHashCode();
        }

        public int Compare(PositionPayPeriodComparer x, PositionPayPeriodComparer y)
        {
            if (x.Equals(y))
            {
                return 0;
            }
            else if (x.PositionId != y.PositionId)
            {
                return x.PositionId.CompareTo(y.PositionId);
            }
            else if (x.PayCycleId != y.PayCycleId)
            {
                return x.PayCycleId.CompareTo(y.PositionId);
            }
            else if (x.PeriodStartDate < y.PeriodStartDate)
            {
                return -1;
            }
            else if (x.PeriodStartDate > y.PeriodStartDate)
            {
                return 1;
            }
            else
            {
                if (x.PeriodEndDate < y.PeriodEndDate)
                {
                    return -1;
                }
                else if (x.PeriodEndDate > y.PeriodEndDate)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int CompareTo(PositionPayPeriodComparer other)
        {
            return this.Compare(this, other);
        }


    }
}