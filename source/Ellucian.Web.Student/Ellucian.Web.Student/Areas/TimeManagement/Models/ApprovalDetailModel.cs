﻿using Ellucian.Colleague.Dtos.Base;
//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    //represents a set of timecard collections in a pay period
    public class ApprovalDetailModel
    {
        public string SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public List<string> SupervisorPositionIds { get; set; }
        public string SuperviseeId { get; set; }
        public string SuperviseeName { get; set; }
        public DateTimeOffset? SupervisorCutoffDateTime
        {
            get
            {
                return TimecardCollections
                    .SelectMany(c => c.Timecards
                        .Where(t => t.SupervisorApproveDeadline.HasValue)
                        .Select(t => t.SupervisorApproveDeadline.Value))
                    .OrderBy(d => d).FirstOrDefault();

            }
        }
        public List<TimecardCollectionModel> TimecardCollections { get; set; }

        public ApprovalDetailModel(PositionQuery supervisorPositionQuery, string payCycleId, DateTime payPeriodEndDate, PositionQuery positionQuery, TimecardQueryDataCoordinator timecardQuery, IEnumerable<EarningsType> earningsTypes, IEnumerable<PersonEmploymentStatus> employmentStatuses, Dictionary<string, HumanResourceDemographics> employeeDictionary, IEnumerable<Department> departments, IEnumerable<Location> locations)
        {

            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("payCycleId");
            }
            if (positionQuery == null)
            {
                throw new ArgumentNullException("positionQuery");
            }
            if (timecardQuery == null)
            {
                throw new ArgumentNullException("timecardQuery");
            }
            if (earningsTypes == null || !earningsTypes.Any())
            {
                throw new ArgumentNullException("earningstypes");
            }

            TimecardCollections = new List<TimecardCollectionModel>();

            //if the person doesn't have any employement statuses, then something went wrong on the client side
            //maybe the end user modified the URL. Don't return any timecard collections. Browser will report a message
            if (employmentStatuses != null && employmentStatuses.Any())
            {
                SupervisorId = supervisorPositionQuery.PersonId;
                SupervisorPositionIds = supervisorPositionQuery.Select(q => q.PersonPosition.PositionId).ToList();
                SuperviseeId = employmentStatuses.First().PersonId;

                

                var payPeriodTimecards = timecardQuery.GetOrderedResultsForPayPeriod(payCycleId, payPeriodEndDate);
                if (payPeriodTimecards.Any())
                {
                    var payCycle = positionQuery.GetPayCycle(payCycleId);

                    var payPeriodStartDate = payPeriodTimecards.First().TimecardJoiner.PeriodStartDate;
                    var payPeriodStartDateNormalized = payPeriodStartDate.NormalizeToStartDayOfWeek(payCycle.WorkWeekStartDay);
                    var payPeriodEndDateNormalized = payPeriodEndDate.NormalizeToEndDateOfWeek((DayOfWeek)(((int)payCycle.WorkWeekStartDay + 6) % 7));
                    var weeksInPayPeriod = TimeManagementUtilities.Weeks(payPeriodStartDateNormalized, payPeriodEndDateNormalized);

                    foreach (var week in weeksInPayPeriod)
                    {
                        var timeframeStartDate = week.First();
                        var timeframeEndDate = week.Last();

                        var timecardsInWeek = payPeriodTimecards.Where(tq => tq.TimecardJoiner.StartDate <= timeframeEndDate && tq.TimecardJoiner.EndDate >= timeframeStartDate);
                        if (timecardsInWeek.Any())
                        {
                            var startDate = timecardsInWeek.Min(t => t.TimecardJoiner.StartDate);
                            var endDate = timecardsInWeek.Max(t => t.TimecardJoiner.EndDate);
                            var collection = new TimecardCollectionModel(SuperviseeId, payCycle.Id, startDate, endDate, employeeDictionary, positionQuery, timecardQuery, employmentStatuses, earningsTypes, departments, locations);

                            TimecardCollections.Add(collection);
                        }
                    }
                }
            }

            SupervisorName = TimeManagementUtilities.FormatEmployeeName(SupervisorId, employeeDictionary);
            SuperviseeName = TimeManagementUtilities.FormatEmployeeName(SuperviseeId, employeeDictionary);
        }
    }
}