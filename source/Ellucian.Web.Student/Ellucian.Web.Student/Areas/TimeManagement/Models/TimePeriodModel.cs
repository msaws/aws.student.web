﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using System.Collections.ObjectModel;
namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class TimePeriodModel
    {
        public DateTime PayPeriodStartDate { get; set; }
        public DateTime PayPeriodEndDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double TotalHours { get { return totalHours; } }
        private double totalHours;
        public DateTimeOffset? EmployeeSubmitDeadline { get; set; }
        public DateTimeOffset? SupervisorApproveDeadline { get; set; }
        public bool IsForHistoricalPayPeriod { get; set; }

        public ReadOnlyCollection<TimecardPositionStatusModel> TimecardPositionStatuses
        {
            get
            {
                return timecardPositionStatuses
                    .OrderByDescending(tps => tps.IsPositionPrimary).ThenBy(tps => tps.PositionTitle).ToList().AsReadOnly();
            }
        }
        private List<TimecardPositionStatusModel> timecardPositionStatuses;

        public TimePeriodModel(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
            totalHours = 0;
            timecardPositionStatuses = new List<TimecardPositionStatusModel>();
        }

        public TimePeriodModel() { }

        public void AddTimecardHours(Timecard2 timecardDto)
        {
            totalHours += timecardDto.TimeEntries.Sum(te => te.WorkedTime.HasValue ? te.WorkedTime.Value.TotalHours : 0);
        }

        public void AddTimecardHours(IEnumerable<Timecard2> timecardDtos)
        {
            totalHours += timecardDtos.SelectMany(t => t.TimeEntries).Sum(te => te.WorkedTime.HasValue ? te.WorkedTime.Value.TotalHours : 0);
        }

        public void AddTimecardPositionStatus(TimecardPositionStatusModel model)
        {
            timecardPositionStatuses.Add(model);
        }
    }

}