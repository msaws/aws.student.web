﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class PositionQuery : IQueryable<PositionQueryResultItem>
    {
        private List<PositionQueryResultItem> list;
        private string personId;

        /// <summary>
        /// Create a PositionQuery object that correlates all the given data to PositionQueryResultItems.
        /// This query will spit out one PositionQueryResultItem for each of the employee's valid PersonPositions
        /// Generating a PositionQuery for a Supervisor ignores certain criteria that are normally applied to employee data.
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="personPositions"></param>
        /// <param name="positions"></param>
        /// <param name="personWages"></param>
        /// <param name="payCycles"></param>
        /// <param name="isSupervisor"></param>
        public PositionQuery(string personId,
            IEnumerable<PersonPosition> personPositions,
            IEnumerable<Position> positions,
            IEnumerable<PersonPositionWage> personWages,
            IEnumerable<PayCycle> payCycles,
            bool isSupervisor)
        {
            list = new List<PositionQueryResultItem>();

            // filter out all inactive positions
            var activePersonPositions =
                 from p in personPositions
                 where p.PersonId == personId
                 select p;

            var activePersonWages =
                from w in personWages
                where w.PersonId == personId
                select w;

            //var availablePayCycles =
            //    from pc in payCycles
            //    select pc;

            var availablePositions =
                from pos in positions
                where isSupervisor || (!pos.IsExempt && !pos.IsSalary && (pos.TimecardType == TimecardType.Summary || pos.TimecardType == TimecardType.Detailed))
                select pos;

            var query =
                 from personPosition in activePersonPositions
                 join personWage in activePersonWages on personPosition.Id equals personWage.PersonPositionId into wageGroup
                 where wageGroup.Any()
                 join payCycle in payCycles on wageGroup.First().PayCycleId equals payCycle.Id
                 join position in availablePositions on personPosition.PositionId equals position.Id
                 select new PositionQueryResultItem(personPosition, position, wageGroup, payCycle);

            this.personId = personId;
            list = isSupervisor ? query.ToList() : query.Where(qr =>
                !string.IsNullOrEmpty(qr.PersonPosition.SupervisorId) ||
                qr.PersonPosition.PositionLevelSupervisorIds.Any()).ToList();

            // add PositionQueryResultItem if the person has a Non-Employee Position
            if (isSupervisor)
            {
                var nonEmployeePositions = personPositions.Where(x => x.NonEmployeePosition == true);
                foreach (var pPos in nonEmployeePositions)
                {
                    var position = positions.FirstOrDefault(pos => pos.Id == pPos.PositionId);
                    if (position != null)
                    {
                        list.Add(new PositionQueryResultItem(pPos, position));
                    }
                }
            }

        }

        /// <summary>
        /// Get a PositionQueryResultItem for a given index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public PositionQueryResultItem this[int index]
        {
            get
            {
                return list[index];
            }

            set
            {
                list[index] = value;
            }
        }


        public string PersonId
        {
            get { return personId; }
        }

        public int Count()
        {
            return list.Count();
        }

        public PositionQueryResultItem Get(TimecardQueryResultItem tqri)
        {
            return Get(tqri.TimecardJoiner.StartDate, tqri.TimecardJoiner.EndDate, tqri.TimecardJoiner.PayCycleId).FirstOrDefault(pq =>
                pq.PositionId == tqri.TimecardJoiner.PositionId);
        }

        public IEnumerable<PositionQueryResultItem> Get(DateTime startDate, DateTime endDate, string payCycleId)
        {
            return list.Where(p =>
                    p.PayCycle.Id == payCycleId &&
                    p.PersonPosition.StartDate <= endDate &&
                    (!p.PersonPosition.EndDate.HasValue || p.PersonPosition.EndDate.Value >= startDate) &&
                    p.GetPersonPositionWages(startDate, endDate).Any());
        }



        public DateTime EarliestPersonPositionStartDate
        {
            get
            {
                return list.Select(r => r.PersonPosition.StartDate).OrderBy(d => d).First();
            }
        }

        public DateTime LastestPersonPositionEndDate
        {
            get
            {
                return list.Any(r => !r.PersonPosition.EndDate.HasValue) ? DateTime.MaxValue :
                    list.Select(r => r.PersonPosition.EndDate.Value).OrderBy(d => d).Last();
            }
        }

        public PayCycle GetPayCycle(string payCycleId)
        {
            var queryResultWithPayCycle = list.FirstOrDefault(pq => pq.PayCycle.Id == payCycleId);
            return queryResultWithPayCycle == null ? null : queryResultWithPayCycle.PayCycle;

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public Type ElementType
        {
            get { return typeof(PositionQueryResultItem); }
        }

        public System.Linq.Expressions.Expression Expression
        {
            get { return list.AsQueryable<PositionQueryResultItem>().Expression; }
        }

        public IQueryProvider Provider
        {
            get { return list.AsQueryable<PositionQueryResultItem>().Provider; }
        }

        IEnumerator<PositionQueryResultItem> IEnumerable<PositionQueryResultItem>.GetEnumerator()
        {
            return list.GetEnumerator();
        }
    }

    public class PositionQueryResultItem
    {
        public string PositionId
        {
            get
            {
                return Position.Id;
            }

        }
        public string PositionTitle
        {
            get
            {
                return Position.Title;
            }
        }
        public TimecardType PositionTimeType
        {
            get
            {
                return Position.TimecardType;
            }
        }

        public PersonPosition PersonPosition { get; set; }
        public Position Position { get; set; }
        public IEnumerable<PersonPositionWage> PersonWages { get; set; }
        public PayCycle PayCycle { get; set; }


        public PositionQueryResultItem(PersonPosition pPos, Position pos, IEnumerable<PersonPositionWage> pPosWage, PayCycle payCycle)
        {
            PersonPosition = pPos;
            Position = pos;
            PersonWages = pPosWage;
            PayCycle = payCycle;
        }

        /// <summary>
        /// This constructor should ONLY be used in the case of a PersonPosition being a Non-Employee Position. 
        /// As a reminder, the Non-Employee Position does not have a corresponding PERPOS record.
        /// </summary>
        /// <param name="pPos"></param>
        /// <param name="pos"></param>
        public PositionQueryResultItem(PersonPosition pPos, Position pos)
        {
            PersonPosition = pPos;
            Position = pos;
        }


        public PersonPositionWage GetPersonPositionWage(DateTime forThisDate, string forThisEarningsType)
        {
            return PersonWages.FirstOrDefault(pw =>
                pw.StartDate.Date <= forThisDate.Date &&
                (!pw.EndDate.HasValue || pw.EndDate.Value.Date >= forThisDate.Date) &&
                pw.RegularWorkEarningsTypeId == forThisEarningsType);
        }

        public IEnumerable<PersonPositionWage> GetPersonPositionWages(DateTime forThisStartDate, DateTime forThisEndDate)
        {
            return PersonWages.Where(pw => pw.StartDate.Date <= forThisEndDate.Date && (!pw.EndDate.HasValue || pw.EndDate.Value.Date >= forThisStartDate.Date));
        }

        public PayPeriod GetPayPeriod(DateTime forThisDate)
        {
            var result = PayCycle.PayPeriods.FirstOrDefault(r => r.StartDate <= forThisDate && r.EndDate >= forThisDate);
            if (result == null)
            {
                throw new ArgumentOutOfRangeException(string.Format("No PayPeriods in PayCycleId {0} match date {1}", PayCycle.Id, forThisDate));
            }
            return result;
        }

        public IEnumerable<PayPeriod> PreparedPayPeriods
        {
            get
            {
                return PayCycle.PayPeriods
                    .Where(pp => (pp.Status == PayPeriodStatus.Calculation || pp.Status == PayPeriodStatus.Generated || pp.Status == PayPeriodStatus.Prepared) &&
                        pp.StartDate <= (PersonPosition.EndDate ?? DateTime.MaxValue) &&
                        pp.EndDate >= PersonPosition.StartDate);
            }
        }

        public IEnumerable<PayPeriod> HistoricalPayPeriods
        {
            get
            {
                return PayCycle.PayPeriods
                    .Where(pp => pp.Status == PayPeriodStatus.History &&
                        pp.StartDate <= (PersonPosition.EndDate ?? DateTime.MaxValue) &&
                        pp.EndDate >= PersonPosition.StartDate);
            }
        }

        public bool IsMigratedForThisDate(DateTime date)
        {

            DateTime migrationStartDate = new DateTime();
            if (this.PersonPosition.MigrationDate.HasValue)
            {

                var payPeriodContainingMigrationDate = GetPayPeriod(this.PersonPosition.MigrationDate.Value);
                if (payPeriodContainingMigrationDate != null)
                {
                    migrationStartDate = payPeriodContainingMigrationDate.StartDate;
                }
                else
                {
                    var message = string.Format("Unable to find a pay period in pay cycle {0} for migration date {1}", PayCycle.Id, this.PersonPosition.MigrationDate.Value);
                    throw new ApplicationException(message);
                }


                return (migrationStartDate <= date);

            }
            else
            {
                // If a position has no MigrationDate && does have EMPTIME.DTL data, return false
                return !this.PersonPosition.LastWebTimeEntryPayPeriodEndDate.HasValue;
                //if (this.PersonPosition.LastWebTimeEntryPayPeriodEndDate != null)
                //{
                //    return false;
                //}
                //// If a position has no MigrationDate && no EMPTIME.DTL data, return true
                //else
                //{
                //    return true;
                //}

            }

        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var resultItem = obj as PositionQueryResultItem;
            return resultItem.PersonPosition.Id == PersonPosition.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }

}