﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    [Serializable]
    public class PositionModel
    {
        public string Id;
        public string Title;
        public bool IsSupervised;
        public bool IsPrimary;
        public decimal TotalWorkHours;

        public List<StatusAction?> TimecardStatuses;
        public List<string> TimecardIds;
        public List<CommentModel> Comments;



        public PositionModel(string id, string title, bool isSupervised, bool isPrimary, List<TimecardQueryResultItem> timecardsForPositionForPeriod)
        {
            if (!timecardsForPositionForPeriod.Any())
            {
                throw new ArgumentException("timecardQueryResults are require to build PositionModel", "timecardsForPositionForPeriod");
            }
            Id = id;
            Title = title;
            IsSupervised = isSupervised;
            IsPrimary = isPrimary;
            TimecardStatuses = new List<StatusAction?>();
            TimecardIds = new List<string>();
            TotalWorkHours = 0;

            foreach (var tc in timecardsForPositionForPeriod)
            {

                TimecardStatuses.Add(tc.StatusActionType);
                TimecardIds.Add(tc.Timecard2.Id);

                var timecardHoursSum = (decimal)tc.Timecard2.TimeEntries
                    .Where(te => te.WorkedTime.HasValue)
                    .Sum(te => te.WorkedTime.Value.TotalHours);

                TotalWorkHours = TotalWorkHours + timecardHoursSum;
            }

            Comments = timecardsForPositionForPeriod
                .SelectMany(qr => qr.Comments.Select(c => new CommentModel(c)))
                .Distinct()
                .OrderBy(x => x.CommentDateTime)
                .ToList();
        }
    }
}