﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class WorkEntry
    {
        public string Id { get; set; }
        public string EarningsTypeId { get; set; }
        public List<DailyTimeEntryGroup> DailyTimeEntryGroupList { get; set; }

        public WorkEntry()
        {
            DailyTimeEntryGroupList = new List<DailyTimeEntryGroup>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="positionQueryResult"></param>
        /// <param name="id"></param>
        /// <param name="earningsTypeId"></param>
        /// <param name="timeframeStartDate"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="timeEntryDtos"></param>
        public WorkEntry(
            PositionQueryResultItem positionQueryResult,
            string id,
            string earningsTypeId,
            DateTime timeframeStartDate,
            DateTime startDate,
            DateTime endDate,
            IEnumerable<Colleague.Dtos.TimeManagement.TimeEntry2> timeEntryDtos,
            IEnumerable<Colleague.Dtos.TimeManagement.TimeEntry2> historicalTimeEntryDtos)
        {
            DailyTimeEntryGroupList = new List<DailyTimeEntryGroup>();

            Id = id;
            EarningsTypeId = earningsTypeId;


            for (DateTime i = timeframeStartDate.Date; i <= timeframeStartDate.AddDays(6); i = i.AddDays(1))
            {
                var personPositionWageForDate = positionQueryResult.GetPersonPositionWage(i, earningsTypeId);
                var isReadOnly =
                    i < startDate.Date ||
                    i > endDate.Date ||
                    i < positionQueryResult.PersonPosition.StartDate.Date ||
                    i > (positionQueryResult.PersonPosition.EndDate.HasValue ? positionQueryResult.PersonPosition.EndDate.Value.Date : DateTime.MaxValue) ||
                    personPositionWageForDate == null;

                var workedDateTimeEntryDtos = timeEntryDtos.Where(te => te.WorkedDate.Date == i.Date);
                var workedDateHistoricalTimeEntryDtos = historicalTimeEntryDtos.Where(te => te.WorkedDate.Date == i.Date);
                var groupId = i.GetHashCode() ^ Id.GetHashCode();

                if (workedDateTimeEntryDtos.Any())
                {
                    DailyTimeEntryGroupList.Add(new DailyTimeEntryGroup(groupId.ToString(), i.Date, workedDateTimeEntryDtos, isReadOnly, false));
                }
                else if (workedDateHistoricalTimeEntryDtos.Any())
                {
                    DailyTimeEntryGroupList.Add(new DailyTimeEntryGroup(groupId.ToString(), i.Date, workedDateHistoricalTimeEntryDtos, true, true));
                }
                else
                {
                    DailyTimeEntryGroupList.Add(new DailyTimeEntryGroup(groupId.ToString(), i.Date, isReadOnly));
                }
            }
        }
    }
}