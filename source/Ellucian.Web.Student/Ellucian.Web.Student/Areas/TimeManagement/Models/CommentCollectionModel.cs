﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class CommentCollectionModel
    {
        public string PositionId { get; set; }
        public string PositionTitle { get; set; }
        public string PayCycleId { get; set; }
        public string PayPeriodEndDate { get; set; }
        public List<CommentModel> Comments { get; set; }

        public CommentCollectionModel()
        {

        }
    }
}