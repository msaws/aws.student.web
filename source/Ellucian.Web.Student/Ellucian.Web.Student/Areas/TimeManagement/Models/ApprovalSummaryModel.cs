﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    public class ApprovalSummaryModel
    {
        public string SupervisorId;
        public string SupervisorName;
        public List<PayPeriodModel> PayPeriodModels;

        public ApprovalSummaryModel(PositionQuery supervisorPositionQuery,
            ApproverQuery approverQuery,
            List<Position> positions,
            List<PersonPosition> personPositions,
            Dictionary<string, HumanResourceDemographics> employeeDictionary,
            IEnumerable<PersonEmploymentStatus> personEmploymentStatuses)
        {
            SupervisorId = supervisorPositionQuery.PersonId;

            HumanResourceDemographics supervisorInfo;
            if (!employeeDictionary.TryGetValue(SupervisorId, out supervisorInfo))
            {
                SupervisorName = SupervisorId;
            }
            else
            {
                SupervisorName = string.Format("{0} {1}", supervisorInfo.FirstName, supervisorInfo.LastName);
            }

            PayPeriodModels = new List<PayPeriodModel>();

            foreach (var approverQueryResult in approverQuery)
            {
                if (approverQueryResult.EmployeeTimecardCutoffDateTime.HasValue && approverQueryResult.SupervisorTimecardCutoffDateTime.HasValue)
                {
                    var payPeriodModel = new PayPeriodModel(supervisorPositionQuery, approverQueryResult, positions, personPositions, employeeDictionary, personEmploymentStatuses);
                    if (payPeriodModel.EmployeeModels.Any())
                    {
                        PayPeriodModels.Add(payPeriodModel);
                    }
                }
            }

        }
    }
}