﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.    
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.TimeManagement.Models
{
    /// <summary>
    /// PayCycle
    /// </summary>
    [Serializable]
    public class PayCycleModel
    {
        /// <summary>
        /// Identifier of this payCycle
        /// </summary>
        public string Id { get; private set; }
        /// <summary>
        /// Name or description of this payCycle
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// PayCycle model constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="description"></param>
        public PayCycleModel(string id, string description) 
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException("id");
            else if (string.IsNullOrWhiteSpace(description))
                throw new ArgumentNullException("description");

            this.Id = id;
            this.Description = description;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var payCycle = obj as PayCycleModel;
            return payCycle.Id == this.Id;
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}