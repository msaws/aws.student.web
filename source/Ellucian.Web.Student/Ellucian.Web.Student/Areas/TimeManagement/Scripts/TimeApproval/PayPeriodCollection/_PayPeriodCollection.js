﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.

var markup = require("./_PayPeriodCollection.html");
var resources = require('Site/resource.manager');
var mapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");

require('TimeManagement/TimeApproval/EmployeeTimecardSummary/_EmployeeTimecardSummary');

require('./_PayPeriodCollection.scss');

// dataModel is a single PayPeriodModel
/*
 * parent component(s): SubmittedTimeSheets
 * params: {
 *  dataModel: a PayPeriodModel (server) model)
 *  supervisorName: the name of the supervisor
 * }
 */
function PayPeriodCollection(params) {
    var self = this;

    if (!params) {
        console.warn("No parameters passed into PayPeriodCollection component");
        params = {};
    }
    if (!params.dataModel) {
        console.warn("dataModel is a required parameter");
    }
    if (!params.supervisorName || !ko.isObservable(params.supervisorName)) {
        console.warn("supervisorName is required to be observable");
    }

    self.supervisorName = params.supervisorName();

    ko.mapping.fromJS(params.dataModel, mapping, self);

    self.timecardDueDateTimeDisplay = ko.pureComputed(function () {
        return 'Due by: ' + Globalize.format(self.SupervisorCutoffDateTime(), 'd') + ' '
                          + Globalize.format(self.SupervisorCutoffDateTime(), 't');
    });



    self.periodEndDateDisplay = ko.pureComputed(function () {
        return Globalize.format(self.PeriodEndDate(), 'd');
    });
}

module.exports = { viewModel: PayPeriodCollection, template: markup };

if (!ko.components.isRegistered('pay-period-collection')) {
    ko.components.register('pay-period-collection', module.exports);
}
