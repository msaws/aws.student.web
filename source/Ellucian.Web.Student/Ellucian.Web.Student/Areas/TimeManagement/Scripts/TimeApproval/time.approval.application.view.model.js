﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
//known dependencies that can't be injected
/*
 * BaseViewModel
 * ko
 */

function TimeApprovalApplicationViewModel() {
    var self = this;

    BaseViewModel.call(self, 480);

    this.changeToMobile = function () {
        // This method should be overridden in any class that uses BaseViewModel
        //alert("Changing to mobile (override me)...");
    };

    this.changeToDesktop = function () {
        // This method should be overridden in any class that uses BaseViewModel
        //alert("Changing to desktop (override me)...");
    };

    self.currentViewComponentName = ko.observable(null);
    self.currentViewComponentParams = ko.observable(null);
}

module.exports = TimeApprovalApplicationViewModel;