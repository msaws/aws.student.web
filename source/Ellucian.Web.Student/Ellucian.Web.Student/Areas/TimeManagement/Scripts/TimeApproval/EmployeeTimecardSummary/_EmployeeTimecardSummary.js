﻿
//Copyright 2016 Ellucian Company L.P. and its affiliates.
var markup = require("./_EmployeeTimecardSummary.html");
var actions = require("TimeManagement/TimeApproval/time.approval.actions");
var resources = require('Site/resource.manager');
var mapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");
var status = require('TimeManagement/Modules/submission.status');

require('TimeManagement/Components/CommentsContainer/_CommentsContainer')
require('TimeManagement/TimeApproval/RejectionDialog/_RejectionDialog')


require("./_EmployeeTimecardSummary.scss");
/*
 * parent component(s): PayPeriodCollection
 * params: {
 *  dataModel: see EmployeeModel on server
 *  startDate: the start date of the pay period
 *  endDate: the end date of the pay period
 *  payCycleId: the payCycleId of the payPeriod,
 *  supervisorName: the name of the supervisor
 *  supervisorCutoffDateTime: the date and time when the supervisor's approval is due
 *  isForHistoricalPayPeriod: whether this summary belongs to a historical pay period
 * }
 */
function EmployeeTimecardSummary(params) {

    var self = this;

    self.supervisorName = params.supervisorName;

    ko.mapping.fromJS(params.dataModel, mapping, self);

    self.employeeNameDisplay = ko.pureComputed(function () {
        return self.EmployeeName() + " - " + self.EmployeeId();
    });
    self.startDate = Globalize.format(params.startDate(), "d");
    self.endDate = Globalize.format(params.endDate(), "d");
    self.isUpdatingStatus = ko.observable(false);
    self.submitLoaderText = ko.observable();

    self.totalHoursSum = ko.pureComputed(function () {
        return self.Positions().reduce(function (totalHours, positionModel) {
            return totalHours + positionModel.TotalWorkHours();
        }, 0);
    });

    self.regularWorkHoursSum = ko.pureComputed(function () {
        return self.totalHoursSum() - self.OvertimeHours();
    })

    var isBeforeSupervisorCutoff = ko.pureComputed(function () {
        return Date.Compare(new Date(), ko.unwrap(params.supervisorCutoffDateTime)) < 0;
    });

    var isForHistoricalPayPeriod = ko.unwrap(params.isForHistoricalPayPeriod);

    self.aggregateStatus = ko.pureComputed(function () {
        return status.aggregate(
            self.Positions().map(function (pos) { return pos.TimecardStatuses(); }).reduce(function (a, b) { return a.concat(b); })
        )
    });

    self.enableApproveButton = ko.pureComputed(function () {
        return (isBeforeSupervisorCutoff() || isForHistoricalPayPeriod) && (self.aggregateStatus().isAllTimeSubmitted || self.aggregateStatus().isAllTimeRejected);
    });

    self.enableUnapproveButton = ko.pureComputed(function () {
        return (isBeforeSupervisorCutoff() || isForHistoricalPayPeriod) && self.aggregateStatus().isAllTimeApproved;
    });

    self.enableRejectButton = ko.pureComputed(function () {
        return (isBeforeSupervisorCutoff() || isForHistoricalPayPeriod) && (self.aggregateStatus().isAllTimeSubmitted || self.aggregateStatus().isAllTimeRejected);
    });

    self.enableUnrejectButton = ko.pureComputed(function () {
        return (isBeforeSupervisorCutoff() || isForHistoricalPayPeriod) && self.aggregateStatus().isAllTimeRejected;
    });

    self.statusLabel = ko.pureComputed(function () {
        return self.aggregateStatus().statusLabel();
    });
    self.statusIconClass = ko.pureComputed(function () {
        return self.aggregateStatus().statusClass;
    })

    // displays a message in case of an error updating the status
    self.isReady = ko.observable(false);
    $(document).ready(function () { return setTimeout(function () { return self.isReady(true); }, 1000); });

    // displays a message in case of an error updating the status
    // note: tried to use the 'slide' ko binding, 
    // but could not find good way to prevent load flash
    var toggleSaveError = function () {
        $('.time-approval-save-error').slideDown();
        return setTimeout(function () {
            return $('.time-approval-save-error').slideUp();
        }, 10000);
    }

    ////loader text labels
    //var unapproveLoadingMessage = ko.pureComputed(function () {
    //    return resources.getObservable("TimeApproval.TimecardStatusUnapprove")();
    //});
    //var unrejectLoadingMessage = ko.pureComputed(function () {
    //    return resources.getObservable("TimeApproval.TimecardStatusUnreject")();
    //});
    //var approveLoadingMessage = ko.pureComputed(function () {
    //    return resources.getObservable("TimeApproval.TimecardStatusApprove")();
    //});
    //var rejectLoadingMessage = ko.pureComputed(function () {
    //    return resources.getObservable("TimeApproval.TimecardStatusReject")();
    //});

    var statusLoadingMessages = ko.computed(function () {
        return {
            unapprove: resources.getObservable("TimeApproval.TimecardStatusUnapprove")(),
            unreject: resources.getObservable("TimeApproval.TimecardStatusUnreject")(),
            approve: resources.getObservable("TimeApproval.TimecardStatusApprove")(),
            reject: resources.getObservable("TimeApproval.TimecardStatusReject")()
        }
    });

    self.unapproveTimecard = function () {
        self.submitLoaderText(statusLoadingMessages().unapprove)
            .isUpdatingStatus(true);

        var nextStatus = status.submitted.code;
        var startTime = Date.now();

        return actions.SubmitTimecardStatusesAsync(nextStatus, unpaidSupervisedTimecardIds()).then(function (sts) {
            self.Positions().forEach(function (pos) {
                return pos.TimecardStatuses([sts]);
            });
            return;
        }).then(function () {
            toggleLoader(Date.now());
            return;
        }).catch(function (err) {
            console.error(err);
            toggleSaveError();
            toggleLoader(Date.now());
            return;
        });
    }

    self.unrejectTimecard = function () {
        self.submitLoaderText(statusLoadingMessages().unreject)
            .isUpdatingStatus(true);

        var nextStatus = status.submitted.code;
        var startTime = Date.now();


        return actions.SubmitTimecardStatusesAsync(nextStatus, unpaidSupervisedTimecardIds()).then(function (sts) {
            self.Positions().forEach(function (pos) {
                return pos.TimecardStatuses([sts]);
            });
            return;
        }).then(function () {
            toggleLoader(Date.now());
            return;
        }).catch(function (err) {
            console.error(err);
            toggleSaveError();
            toggleLoader(Date.now());
            return;
        });
    }

    self.ApproveTimecard = function () {
        self.submitLoaderText(statusLoadingMessages().approve)
            .isUpdatingStatus(true);

        var nextStatus = status.approved.code;
        var startTime = Date.now();

        return actions.SubmitTimecardStatusesAsync(nextStatus, unpaidSupervisedTimecardIds()).then(function (sts) {
            self.Positions().forEach(function (pos) {
                return pos.TimecardStatuses([sts]);
            });
            return;
        }).then(function () {
            toggleLoader(Date.now());
            return;
        }).catch(function (err) {
            console.error(err);
            toggleSaveError();
            toggleLoader(Date.now());
            return;
        });
    }

    self.rejectionDataModel = ko.observable({
        supervisorName: self.supervisorName,
        employeeName: self.EmployeeName(),
        employeeId: self.EmployeeId(),
        startDate: self.startDate,
        endDate: self.endDate,
        PayPeriod: self.startDate + ' - ' + self.endDate,
    });

    self.rejectTimecard = function (rejectionComment) {
        self.submitLoaderText(statusLoadingMessages().reject)
            .isUpdatingStatus(true);

        var nextStatus = status.rejected.code;
        var startTime = Date.now();

        return actions.SubmitTimecardStatusesAsync(nextStatus, unpaidSupervisedTimecardIds())
            .then(function (sts) {
                self.Positions().forEach(function (pos) { return pos.TimecardStatuses([sts]); })
                return self.aggregateStatus();
            }).then(function () {

                //apply the rejection comment to each position at the pay period level
                var commentPromises = self.Positions().map(function (position) {
                    return self.newCommentHandler({
                        CommentText: rejectionComment,
                        CommentAuthorName: self.supervisorName,
                        CommentDateTime: ko.observable(new Date()),
                        PositionId: position.Id(),
                        EmployeeId: self.EmployeeId.peek(),
                        PayCycleId: params.payCycleId.peek(),
                        PayPeriodEndDate: params.endDate.peek()
                    });
                });

                return Promise.all(commentPromises);
            }).then(function () {
                toggleLoader(Date.now());
                return;
            }).catch(function (err) {
                toggleLoader(Date.now());
                toggleSaveError();
                return console.error(err);
            });
    }
    var toggleLoader = function (startTime) {
        var endTime = Date.now();
        if (endTime - startTime > 2000) {
            self.isUpdatingStatus(false);
        } else {
            setTimeout(function () {
                self.isUpdatingStatus(false);
            }, (1000 - (endTime - startTime)));
        }
    }

    var unpaidSupervisedTimecardIds = ko.pureComputed(function () {
        return self.Positions()
            .filter(function (positionModel) {
                return positionModel.IsSupervised();
            })
            .flatMap(function (positionModel) {
                return positionModel.TimecardIds().filter(function (id, index) {
                    var timecardStatus = status.get(positionModel.TimecardStatuses()[index]);
                    return timecardStatus != status.paid;
                })
            });
    });



    self.detailLink = ko.pureComputed(function () {
        return "#pid={0}&pcy={1}&ppend={2}&i=0".format(self.EmployeeId(), params.payCycleId(), self.endDate);
    });

    self.detailLinkText = self.startDate + ' - ' + self.endDate;    

    self.detailLinkLabel = ko.pureComputed(function () {
        return 'Timecard detail for ' + self.employeeNameDisplay() +
            ' between ' + self.startDate + ' and ' + self.endDate +
            '; Timecard status - ' + self.statusLabel();
    });

    self.positionCommentsDataModel = ko.computed({
        read: function () {
            return {
                positionCommentsArray: self.Positions().map(function (positionModel) {
                    return {
                        PositionId: positionModel.Id(),
                        PositionTitle: positionModel.Title(),
                        Comments: positionModel.Comments(),
                        PayPeriodEndDate: params.endDate.peek(),
                        PayCycleId: params.payCycleId.peek()
                    };
                })
            };
        },
        write: function (newComment) {
            var associatedPosition = self.Positions().find(function (positionModel) {
                return positionModel.Id() === newComment.PositionId;
            });
            if (associatedPosition) {
                associatedPosition.Comments.push(newComment);
            }
        }
    });

    self.newCommentHandler = function (comment) {
        if (comment) {
            // verify any missing data that is available here
            if (!comment.PayCycleId) {
                comment.PayCycleId = params.payCycleId.peek();
            }
            if (!comment.PayPeriodEndDate) {
                comment.PayPeriodEndDate = params.endDate.peek();
            }
            if (!comment.EmployeeId) {
                comment.EmployeeId = self.EmployeeId();
            }
            // perform save
            return actions.CreateTimeEntryCommentsAsync(comment).then(function (responseCommentJSON) {
                // add the response value to the appropriate data model
                return self.positionCommentsDataModel(responseCommentJSON);
            }).catch(function (err) {
                console.error(err);
            });
        }
    }



    self.dispose = function () {
        self.positionCommentsDataModel.dispose();
    }

}

module.exports = { viewModel: EmployeeTimecardSummary, template: markup };


if (!ko.components.isRegistered('employee-timecard-summary')) {
    ko.components.register('employee-timecard-summary', module.exports);
}
