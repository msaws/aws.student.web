﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
var markup = require("./_SummaryViewComponent.html");
var actions = require("TimeManagement/TimeApproval/time.approval.actions");
var resources = require("Site/resource.manager");
var dataMapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");

require('TimeManagement/TimeApproval/SubmittedTimeSheets/_SubmittedTimeSheets');

/**
 * parent component(s): none
 * params: none
 */
function TimeApprovalSummaryViewComponent() {
    var self = this;
    self.PayPeriodModels = ko.observable();

    //data access from actions here
    // -----
    actions.GetSummaryApprovalDataAsync().then(function (summaryViewJSON) {
        ko.mapping.fromJS(summaryViewJSON, dataMapping, self);
        self.isLoaded(true);
    }).catch(function (err) {
        console.error(err);
        self.isLoaded(true).loadError(true);
    });

    self.isLoaded = ko.observable(false);
    self.loadError = ko.observable(false);

    self.approvalSummaryNotificationMessage = function () {
        if (self.loadError())
            return 'TimeApproval.SomethingWentWrongError';
        else if (self.PayPeriodModels() && !self.PayPeriodModels().length)
            return 'TimeApproval.NoEmployeeSummaryDataMessage';
        else {
            return '';
        }
    };

    self.isApprovalSummaryNotification = ko.pureComputed(function () {
        return self.approvalSummaryNotificationMessage().length;
    });

}

module.exports = { viewModel: TimeApprovalSummaryViewComponent, template: markup };

if (!ko.components.isRegistered('summary-view-component')) {
    ko.components.register('summary-view-component', module.exports);
}

