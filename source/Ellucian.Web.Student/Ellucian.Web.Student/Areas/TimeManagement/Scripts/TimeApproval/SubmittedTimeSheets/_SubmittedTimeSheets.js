﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
var markup = require("./_SubmittedTimeSheets.html");
var resources = require("Site/resource.manager");
var dataMapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");

require('TimeManagement/TimeApproval/PayPeriodCollection/_PayPeriodCollection');

/*
 * parent component(s): SummaryViewComponent
 * params = {
 *  dataModel: SummaryViewComponent datamodel - see TimeApprovalModel on server
 * }
 */
function SubmittedTimeSheets(params) {
    var self = this;
    self.dataModel = ko.utils.unwrapObservable(params.dataModel);
}

module.exports = { viewModel: SubmittedTimeSheets, template: markup };

if (!ko.components.isRegistered('submitted-time-sheets')) {
    ko.components.register('submitted-time-sheets', module.exports);
}
