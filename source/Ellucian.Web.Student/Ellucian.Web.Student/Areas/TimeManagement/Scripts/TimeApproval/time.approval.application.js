﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
var ApplicationViewModel = require('TimeManagement/TimeApproval/time.approval.application.view.model');

require("TimeManagementContent/TimeManagement.scss")

__webpack_public_path__ = window.Ellucian.appUrl;

var approvalViewModelInstance = new ApplicationViewModel();

function hashChangeHandler() {
    var detailViewRegex = /#pid=(.*)&pcy=(.*)&ppend=([^\n&]*)(?:&i=)?([^\n&]*)/i;

    if (window.location.hash === '' || window.location.hash === '#') {
        //do summary view component
        approvalViewModelInstance.currentViewComponentName('summary-view-component');

    }

    else if (detailViewRegex.test(window.location.hash)) {
        //do approval detail view component

        var result = detailViewRegex.exec(window.location.hash);

        approvalViewModelInstance.currentViewComponentName("detail-view-component");
        approvalViewModelInstance.currentViewComponentParams({
            personId: result[1],
            payCycleId: result[2],
            payPeriodEndDate: result[3],
            timePeriodIndex: result[4], // index value of this work week in the pay period
        });
    }
    return;
}

$(window).on("hashchange", hashChangeHandler);

function loadApplication() {
    ko.applyBindings(approvalViewModelInstance, document.getElementById("time-approval-application"));

    approvalViewModelInstance.checkForMobile(window, document);

    hashChangeHandler();
}

$(document).ready(function () {

    require('TimeManagement/TimeApproval/SummaryViewComponent/_SummaryViewComponent');
    require('TimeManagement/TimeApproval/DetailViewComponent/_DetailViewComponent');

    loadApplication();
})

if (__DEV__) {
    if (module.hot) {
        module.hot.accept();
    }
}

module.exports = {};
