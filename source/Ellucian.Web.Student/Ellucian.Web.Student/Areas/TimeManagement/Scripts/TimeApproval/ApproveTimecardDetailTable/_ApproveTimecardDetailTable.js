﻿//Copyright 2016 - 2017 Ellucian Company L.P. and its affiliates.
var markup = require("./_ApproveTimecardDetailTable.html");
var mapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");
var actions = require("TimeManagement/TimeApproval/time.approval.actions");
var status = require('TimeManagement/Modules/submission.status');
var resources = require('Site/resource.manager');
var moment = require("moment");

require('TimeManagement/Components/TimecardHeader/_TimecardHeader');

require('./_ApproveTimecardDetailTable.scss');

/**
 * parent component(s): TimecardCollectionCarousel
 * params: {
 *  dataModel: TimecardCollection (server) model,
 *  supervisorId: the supervisorId (probably the current user),
 *  supervisorPositionIds: the list of the supervisor's positionIds
 * }
 */
function ApproveTimecardDetailTable(params) {
	var self = this;

	ko.mapping.fromJS(ko.utils.unwrapObservable(params.dataModel), mapping, self);

	self.daysInWeek = ko.pureComputed(function () {
		var days = [];

		for (var i = self.EffectiveStartDate() ; i <= Date.DateOffset(self.EffectiveStartDate(), 6) ; i = Date.DateOffset(i, 1)) {
			days.push(i);
		}
		return days;
	});

    //self.headerContentMarkup = $.parseHTML(headerContentMarkup);
	self.headerComponentMarkup = $.parseHTML("<timecard-header params='timecardModel: timecard'></timecard-header>");


	self.dailyTimeEntryType = function (timecard) {
		var timeEntryType = {};
		timeEntryType.isSummary = timeEntryType.isDetail = false;

		switch (timecard.DailyTimeEntryType()) {
			case 1:
				timeEntryType.isSummary = true;
				break;
			case 2:
				timeEntryType.isDetail = true;
				break;
			default:
				throw new Error(timecard.DailyTimeEntryType() +
					' is not valid; a daily time entry type of 1 (summary) or 2 (detail) must be provided'
				);
		}
		return timeEntryType;
	};

	self.dailyDetailTimeEntryStartTimePlaceholder = resources.getObservable('TimeApproval.DailyDetailTimeEntryStartTimePlaceholder');
	self.dailyDetailTimeEntryEndTimePlaceholder = resources.getObservable('TimeApproval.DailyDetailTimeEntryEndTimePlaceholder');

	self.formattedTimeStart = function (dailyTimeEntry) {
		var formattedTimeStart = {};
		formattedTimeStart = {
			formattedTime: dailyTimeEntry.TimeStart() ? moment(dailyTimeEntry.TimeStart()).format('h:mm A') : self.dailyDetailTimeEntryStartTimePlaceholder(),
			isMissing: !dailyTimeEntry.TimeStart()
		}
		return formattedTimeStart;
	};

	self.formattedTimeEnd = function (dailyTimeEntry) {
		var formattedTimeEnd = {};
		formattedTimeEnd = {
			formattedTime: dailyTimeEntry.TimeEnd() ? moment(dailyTimeEntry.TimeEnd()).format('h:mm A') : self.dailyDetailTimeEntryEndTimePlaceholder(),
			isMissing: !dailyTimeEntry.TimeEnd()
	}
		return formattedTimeEnd;
	};

	self.totalHours = function (timecard) {
		if (timecard) {
			return totalTimecardHours(timecard);
		} else {
			return self.Timecards().reduce(function (collectionHours, timecard) {
				return collectionHours + totalTimecardHours(timecard);
			}, 0);
		}
	}
	function totalTimecardHours(timecard) {
		return timecard.WorkEntryList().reduce(function (timecardHours, workEntry) {
			return timecardHours + self.totalWorkEntryHours(workEntry);
		}, 0);
	}

	self.totalWorkEntryHours = function (workEntry) {
		return workEntry.DailyTimeEntryGroupList().reduce(function (dailyGroupHours, dailyGroup) {
			return dailyGroupHours + dailyGroup.DailyTimeEntryList().reduce(function (dailyHours, dailyEntry) {
				return dailyHours + dailyEntry.HoursWorked();
			}, 0);
		}, 0);
	}

	self.totalDayHours = function (index, timecard) {
		if (timecard) {
			return totalTimecardDayHours(index, timecard);
		} else {
			return self.Timecards().reduce(function (collectionHours, timecard) {
				return collectionHours + totalTimecardDayHours(index, timecard);
			}, 0);
		}
	}

	self.isOutsideTimecardDates = function (index) {
		var currentDate = addDays(self.EffectiveStartDate(), index());
		return currentDate < self.StartDate() || currentDate > self.EndDate();
	}

	function addDays(date, days) {
		var result = new Date(date);
		result.setDate(result.getDate() + days);
		return result;
	}

	function totalTimecardDayHours(index, timecard) {
		return timecard.WorkEntryList().reduce(function (workEntryHours, workEntry) {
			return workEntryHours + workEntry.DailyTimeEntryGroupList()[index()].DailyTimeEntryList().reduce(function (dailyHours, dailyTimeEntry) {
				return dailyHours + dailyTimeEntry.HoursWorked();
			}, 0);
		}, 0);
	}

	self.workEntryDescription = function (timecard, workEntry) {
		var earnType = timecard.EarningsTypes().find(function (type) {
			return type.Id() === workEntry.EarningsTypeId();
		});

		if (earnType) {
			return earnType.Description();
		}
	}

	self.dailyTimeHoursWorked = function (dailyTimeEntryGroup) {
		return dailyTimeEntryGroup.DailyTimeEntryList().reduce(function (dailyHours, dailyTimeEntry) {
			return dailyHours + (dailyTimeEntry.HoursWorked() ? dailyTimeEntry.HoursWorked() : 0);
		}, 0);
	}

	self.overtimeHours = ko.observable(0);
	actions.CalculateOvertimeAsync(self.EmployeeId(), self.daysInWeek()[0], self.daysInWeek()[6], self.PayCycleId()).then(function (overtimeModel) {
		self.overtimeHours(overtimeModel.TotalOvertime);
		return;
	});

	self.totalRegularWorkHours = ko.pureComputed(function () {
		return self.totalHours() - self.overtimeHours();
	});

	self.nonManagedPositionLabel = resources.getObservable('TimeApproval.TimecardTableNonManagedPositionLabel');

	self.timecardHeaderTitle = function (timecard) {
	    var header = timecard.PositionTitle();
	    if (!self.isTimecardSupervised(timecard)) {
	        header += " "+ self.nonManagedPositionLabel();
	    }
	    return header;
	}

	self.isTimecardSupervised = function (timecard) {
		return timecard.SupervisorId() === params.supervisorId() ||
			((timecard.SupervisorId() === null || timecard.SupervisorId() === "") &&
			  params.supervisorPositionIds().indexOf(timecard.SupervisorPositionId()) >= 0);
	}

	//get the statusObject based on the TimecardStatus value
	self.timecardStatusObject = function (timecard) {
		return status.get(timecard.TimecardStatus());
	}

	//has timecardStatus is true when the status does not equal none
	self.hasTimecardStatus = function (timecard) {
		return self.timecardStatusObject(timecard) !== status.none;
	}

	self.weeklyTotalTitle = resources.getObservable('TimeApproval.TimecardCollectionWeeklyTotalFooterLabel');

}

module.exports = { viewModel: ApproveTimecardDetailTable, template: markup };

if (!ko.components.isRegistered('timecard-detail-table')) {
    ko.components.register('timecard-detail-table', module.exports);
}
