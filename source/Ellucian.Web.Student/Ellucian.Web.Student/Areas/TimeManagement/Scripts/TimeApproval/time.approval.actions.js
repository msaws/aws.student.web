﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
var exports = {};

function getSummaryApprovalDataAsync() {
    return Promise.resolve($.ajax({
        url: Ellucian.TimeApproval.ActionUrls.getSummaryApprovalDataUrl,
        type: "GET",
        contentType: jsonContentType,
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {
            return responseObj.Model;
        } else {
            throw new Error(responseObj.Message);
        }
    });
}
exports.GetSummaryApprovalDataAsync = getSummaryApprovalDataAsync;

function submitTimecardStatusesAsync(statuses) {
    var payload = {
        "timecardStatusesJson": ko.toJSON(statuses)
    }

    return Promise.resolve($.ajax({
        url: Ellucian.TimeApproval.ActionUrls.submitTimecardStatusesUrl,
        type: "POST",
        contentType: jsonContentType,
        dataType: "json",
        data: JSON.stringify(payload)
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {
            // note: bhr - we are not displayng a message to the user in case of success;
            // the model coming back is only the 'timecard status code' (as opposed to the response status code)
            return responseObj.Model;
        }
        else {
            throw new Error(responseObj.Message);
        }
    });
}
exports.SubmitTimecardStatusesAsync = function (newStatusCode, timecardIds) {
    if (!timecardIds && timecardIds.length === 0) {
        console.warn("SubmitTimecardStatusesAsync requires at least one timecardId");
    }

    var statuses = [];
    for (var i = 0; i < timecardIds.length; i++) {

        var status = {
            "ActionType": newStatusCode,
            "TimecardId": timecardIds[i]
        };
        statuses.push(status);
    }
    return submitTimecardStatusesAsync(statuses);
};

function getDetailApprovalDataAsync(personId, payCycleId, payPeriodEndDate) {

    var url = Ellucian.TimeApproval.ActionUrls.getDetailApprovalDataUrl + "?" +
        "personId=" + personId +
        "&payCycleId=" + payCycleId +
        "&payPeriodEndDate=" + payPeriodEndDate;

    return Promise.resolve($.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {
            return responseObj.Model;
        } else {
            throw new Error(responseObj.Message);
        }
    })
}
exports.GetDetailApprovalDataAsync = function (personId, payCycleId, payPeriodEndDate) {
    if (!personId || !payCycleId || !payPeriodEndDate) {
        console.warn("personId, payCycleId and payPeriodEndDate are required to get detail approval data");
    }
    return getDetailApprovalDataAsync(personId, payCycleId, payPeriodEndDate);
}

function postOvertimeQueryAsync(overtimeQueryCriteria) {
    return Promise.resolve($.ajax({
        url: Ellucian.TimeApproval.ActionUrls.postOvertimeQueryUrl,
        type: 'POST',
        contentType: jsonContentType,
        data: JSON.stringify({ 'overtimeQueryCriteriaJson': ko.toJSON(overtimeQueryCriteria) }),
        dataType: 'json'
    })).then(function (response) {
        if (response.StatusCode === 200) {
            return response.Model;
        } else {
            throw new Error(response.Message);
        }
    });
}
exports.CalculateOvertimeAsync = function (personId, startDate, endDate, payCycleId) {
    if (!personId || !startDate || !endDate || !payCycleId)
        console.warn('Calculating overtime requires personId, startDate, endDate, and paycycleId');
    var overtimeQueryCriteria = { PersonId: personId, PaycycleId: payCycleId, StartDate: startDate, EndDate: endDate };
    return postOvertimeQueryAsync(overtimeQueryCriteria);
}

function createTimeEntryCommentsAsync(newComment) {
    return Promise.resolve($.ajax({
        url: Ellucian.TimeApproval.ActionUrls.createTimeEntryCommentsUrl,
        type: 'POST',
        contentType: jsonContentType,
        data: JSON.stringify({ 'commentModelJson': ko.toJSON(newComment) }),
        dataType: 'json'
    })).then(function (response) {
        if (response.StatusCode === 200) {
            return response.Model;
        } else {
            throw new Error(response.Message);
        }
    })
}
exports.CreateTimeEntryCommentsAsync = function (newComment) {
    if (!newComment)
        console.warn("comment object is required to post");
    return createTimeEntryCommentsAsync(newComment);
}

module.exports = exports;