﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
module.exports = {
    "PayPeriodModels": {
        key: function (data) {
            var key = ko.unwrap(data.PeriodEndDate) + "*" + ko.unwrap(data.PayCycleId);
            return key;
        }
    },

    "EmployeeModels": {
        key: function (data) {
            return ko.utils.unwrapObservable(data.EmployeeId);
        }
    },
    "Comments": {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        }
    }
}