﻿var markup = require("./_DetailViewComponent.html");
var actions = require("TimeManagement/TimeApproval/time.approval.actions");
var mapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");
var resources = require("Site/resource.manager");

require('TimeManagement/TimeApproval/TimecardCollectionCarousel/_TimecardCollectionCarousel');


//potentially use these so that when timePeriodIndex changes, we can just
//grab the data from the cache instead of hitting the server
var dataCache = {};
var lastParams = {};

function getDataFromServerOrCacheAsync(params) {

    //if (dataCache == {} ||
    //    lastParams.personId !== params.personId ||
    //    lastParams.payCycleId != params.payCycleId ||
    //    lastParams.payPeriodEndDate !== params.payPeriodEndDate)
    //{
    return actions.GetDetailApprovalDataAsync(params.personId, params.payCycleId, params.payPeriodEndDate).then(function (model) {
        dataCache = Object.assign({}, model);
        lastParams = Object.assign({}, params);
        return model;
    });
    //}
    //else {
    //    console.log("from cache");
    //    //may not do this once we implement animcation
    //    return Promise.resolve({
    //        then: function (onFulfill, onReject) {
    //            setTimeout(function () {
    //                onFulfill(dataCache);
    //            }, 666);
    //        }
    //    });
    //}
}

/*
 * parent components(s): none
 * params: {
 *  personId: id of the person whose timecards we're looking at
 *  payCycleId: the paycycleId of the timecards we're looking at
 *  payPeriodEndDate: the end date of the pay period we're looking at
 *  timePeriodIndex: the index of the week we're looking at in the collection array.   
 * }
 */
function DetailViewComponent(params) {
    var self = this;

    if (!params.personId || !params.payCycleId || !params.payPeriodEndDate) {
        console.warn("PersonId, PayCycleId and PayPeriodEndDate are required parameters of DetailViewComponent");
    }

    self.dataModel = ko.observable(null);

    getDataFromServerOrCacheAsync(params).then(function (model) {
        self.dataModel(model).isLoaded(true).loadError(false);
        return;
    }).catch(function (err) {
        console.error(err);
        self.isLoaded(true).loadError(true);
        return;
    });

    self.selectedIndex = params.timePeriodIndex;


    self.isLoaded = ko.observable(false);
    self.loadError = ko.observable(false);

    self.dispose = function () {
        //nothing to do {

    }
}

module.exports = { viewModel: DetailViewComponent, template: markup };

if (!ko.components.isRegistered('detail-view-component')) {
    ko.components.register('detail-view-component', module.exports);
}

