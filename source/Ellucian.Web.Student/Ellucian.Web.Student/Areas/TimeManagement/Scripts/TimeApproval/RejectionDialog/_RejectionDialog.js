﻿var markup = require("./_RejectionDialog.html");
var dataMapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");
var resources = require("Site/resource.manager");

/*
 * params = {
 *  dataModel: object with the following structure 
 *  {
 *      supervisorName: name of the person doing the rejection,
 *      employeeName: name of the employee owning the timecard being rejected,
 *      employeeId: id of the employee,
 *      startDate: start date of the time period (pay period or week, generally) in which the timecards will be rejected
 *      endDate: end date of the time period (pay period or week, generally) in which the timecards will be rejected     
 *  },
 *  isRejectDialogEnabled: observable - returns true or false, enabling or disabling the button to open the reject dialog, respectively
 *  rejectHandler: function - should be invoked when the reject button is clicked, arguments are:
 *                      arg0 - the text of the comment the user entered
 * }
 */
function RejectionDialog(params) {
    var self = this;

    if (typeof params.dataModel == 'function') {
        ko.mapping.fromJS(params.dataModel(), dataMapping, self); // the summary view sends in the dataModel as a function
    } else {
        ko.mapping.fromJS(params.dataModel, dataMapping, self); // the detail view does not
    }

    if (!params.isRejectDialogEnabled || !ko.isObservable(params.isRejectDialogEnabled)) {
        console.warn("isRejectDialogEnabled param is required to be observable");
    }

    self.enableRejectDialogButton = params.isRejectDialogEnabled;


    self.rejectTimecard = function () {
        if (self.rejectionComment()) {
            self.toggleDialog();
            params.rejectHandler(self.rejectionComment());
            self.rejectionComment(null);
        }
    }

    self.timePeriodDisplay = ko.pureComputed(function () {
        return Globalize.format(self.startDate(), "d") + " - " + Globalize.format(self.endDate(), "d");
    })

    self.rejectionComment = ko.observable(null);

    self.isRejectionComment = ko.pureComputed(function () {
        return self.rejectionComment() && self.rejectionComment().length > 0;
    });

    self.rejectionDialogIsOpen = ko.observable(false);
    self.toggleDialog = function () {
        if (self.rejectionDialogIsOpen()) {
            self.rejectionDialogIsOpen(false);
        } else {
            self.rejectionDialogIsOpen(true);
        }
    }

    self.rejectionCommentsPlaceholder = resources.getObservable('TimeApproval.RejectionCommentsPlaceholder');

    var rejectButtonText = resources.getObservable('TimeApproval.TimeApprovalRejectLabel');
    self.rejectionDialogOptions = {
        title: resources.getObservable('TimeApproval.TimeApprovalRejectModalHeader'),
        buttons: [{
            title: rejectButtonText,
            callback: self.rejectTimecard,
            isPrimary: true,
            enabled: self.isRejectionComment,
            visible: true
        }]
    }
}

module.exports = { viewModel: RejectionDialog, template: markup };

if (!ko.components.isRegistered('rejection-dialog')) {
    ko.components.register('rejection-dialog', module.exports);
}
