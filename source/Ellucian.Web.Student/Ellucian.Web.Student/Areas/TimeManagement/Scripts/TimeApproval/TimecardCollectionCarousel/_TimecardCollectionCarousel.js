﻿var markup = require("./_TimecardCollectionCarousel.html");
var mapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");
var resources = require("Site/resource.manager");
var actions = require("TimeManagement/TimeApproval/time.approval.actions");
var status = require('TimeManagement/Modules/submission.status');

//child components
require('TimeManagement/TimeApproval/ApproveTimecardDetailTable/_ApproveTimecardDetailTable');


//sass
require("./_TimecardCollectionCarousel.scss");


/**
 * parent component(s): DetailViewComponent
 * params: {
 *  index: the selectedIndex of the timecard collection
 *  dataModel: ApproverDetailModel (server) models
 * }
 */
function TimecardCollectionCarousel(params) {
    var self = this;

    if (!params.index) {
        console.warn("index parameter is null. assuming 0");
    }

    ko.mapping.fromJS(params.dataModel(), mapping, self);

    self.normalizedIndex = ko.pureComputed(function () {
        if (!params.index) return 0;
        else if (params.index >= self.TimecardCollections().length) return self.TimecardCollections().length;
        else if (params.index < 0) return 0;
        else return parseInt(params.index);


    });

    self.isAvailablePreviousWeek = ko.pureComputed(function () {
        return self.normalizedIndex() > 0;
    });

    self.isAvailableNextWeek = ko.pureComputed(function () {
        return self.normalizedIndex() < self.TimecardCollections().length - 1;
    });

    self.forwardLink = ko.pureComputed(function () {
        var nextIndex = (self.normalizedIndex() == self.TimecardCollections().length - 1) ?
            0 :
            parseInt(self.normalizedIndex()) + 1;


        var newHash = window.location.hash.replace(/i=(\d*)/i, "i=" + nextIndex);

        return newHash;
    });
    self.backwardLink = ko.pureComputed(function () {
        var prevIndex = (self.normalizedIndex() === 0) ?
            self.TimecardCollections().length - 1 :
            parseInt(self.normalizedIndex()) - 1;

        var newHash = window.location.hash.replace(/i=(\d*)/i, "i=" + prevIndex);
        return newHash;
    });

    self.selectedCollection = ko.pureComputed(function () {
        if (self.TimecardCollections().length === 0) return null;
        return self.TimecardCollections()[self.normalizedIndex()];
    });

    self.selectedCollectionSupervisedTimecards = ko.pureComputed(function () {
        if (!self.selectedCollection()) return [];
        return self.selectedCollection().Timecards().filter(function (timecard) {
            return timecard.SupervisorId() === self.SupervisorId() ||
                ((timecard.SupervisorId() === null || timecard.SupervisorId() === "") &&
                  self.SupervisorPositionIds().indexOf(timecard.SupervisorPositionId()) >= 0);
        });
    })

    self.selectedCollectionCommentsModel = ko.computed(function () {
        if (!self.selectedCollection()) return [];

        return {
            positionCommentsArray: self.selectedCollection().Timecards()
                .map(function (timecard) {
                    return {
                        PositionId: timecard.PositionId(),
                        PositionTitle: timecard.PositionTitle(),
                        TimecardId: timecard.TimecardId(),
                        Comments: timecard.Comments(),
                        PayPeriodEndDate: timecard.PeriodEndDate(),
                        PayCycleId: timecard.PayCycleId()
                    };
                })
        };

    });

    self.newCommentHandler = function (comment) {
        if (comment) {

            comment.EmployeeId = self.SuperviseeId();

            return actions.CreateTimeEntryCommentsAsync(comment).then(function (responseCommentJSON) {
                var timecardToUpdate = self.selectedCollectionSupervisedTimecards().find(function (timecard) {
                    return timecard.TimecardId() === responseCommentJSON.TimecardId
                });
                if (timecardToUpdate) {
                    var mappedComments = ko.mapping.fromJS(responseCommentJSON, mapping);
                    timecardToUpdate.Comments.push(mappedComments);
                }
                return responseCommentJSON;
            }).catch(function (err) {
                console.error(err);
            });
        }
    }


    self.selectedCollectionTotalHours = ko.pureComputed(function () {
        if (!self.selectedCollection()) return 0;
        return self.selectedCollection().Timecards().reduce(function (collectionHours, timecard) {
            return collectionHours + timecard.WorkEntryList().reduce(function (workEntryHours, workEntry) {
                return workEntryHours + workEntry.DailyTimeEntryGroupList().reduce(function (dailyGroupHours, dailyGroup) {
                    return dailyGroupHours + dailyGroup.DailyTimeEntryList().reduce(function (dailyHours, dailyEntry) {
                        return dailyHours + dailyEntry.HoursWorked();
                    }, 0);
                }, 0);
            }, 0);
        }, 0);
    });

    self.aggregateStatus = ko.pureComputed(function () {
        return status.aggregate(
            self.selectedCollectionSupervisedTimecards().map(function (timecard) { return timecard.TimecardStatus(); })
        )
    });

    self.selectedCollectionStatusLabel = ko.pureComputed(function () {
        return {
            text: self.aggregateStatus().statusLabel(),
            style: self.aggregateStatus().statusClass
        }
    });


    self.newComments = ko.observable(null);


    self.weekLabel = ko.pureComputed(function () {
        if (!self.selectedCollection()) return "";
        var weekLabelResourceObservable = resources.getObservable("TimeApproval.TimecardCollectionCarouselWeekLabel");
        var weekContent = Globalize.format(self.selectedCollection().StartDate(), "d") + " - " + Globalize.format(self.selectedCollection().EndDate(), "d");

        return weekLabelResourceObservable() + " " + weekContent;
    });

    //if true, status buttons are enabled
    //if false, status buttons are disabled
    self.isBeforeCutoffDate = ko.pureComputed(function () {
        return Date.Compare(new Date(), self.SupervisorCutoffDateTime()) < 0;
    });

    self.isForHistoricalPayPeriod = self.TimecardCollections().some(function (tcc) {
        return tcc.Timecards().some(function (tc) {
            return tc.IsForHistoricalPayPeriod();
        });
    });

    self.enableApproveButton = ko.pureComputed(function () {
        return (self.isBeforeCutoffDate() || self.isForHistoricalPayPeriod) && status.compare(self.aggregateStatus(), status.unsubmitted) > 0 && status.compare(status.paid, self.aggregateStatus()) > 0;
    });

    self.enableUnapproveButton = ko.pureComputed(function () {
        return (self.isBeforeCutoffDate() || self.isForHistoricalPayPeriod) && self.aggregateStatus().isAllTimeApproved;
    });

    self.enableRejectDialog = ko.pureComputed(function () {
        return (self.isBeforeCutoffDate() || self.isForHistoricalPayPeriod) && status.compare(self.aggregateStatus(), status.unsubmitted) > 0 && status.compare(status.paid, self.aggregateStatus()) > 0;
    });

    self.enableUnrejectButton = ko.pureComputed(function () {
        return (self.isBeforeCutoffDate() || self.isForHistoricalPayPeriod) && self.aggregateStatus().isAllTimeRejected;
    });

    self.approveAction = function () {
        submitStatusChangeForSelectedCollectionAsync(status.approved.code);
    }
    self.unapproveAction = function () {
        submitStatusChangeForSelectedCollectionAsync(status.submitted.code);
    }
    self.unrejectAction = function () {
        submitStatusChangeForSelectedCollectionAsync(status.submitted.code);
    }
    self.rejectAction = function (rejectComment) {
        submitStatusChangeForSelectedCollectionAsync(status.rejected.code).then(function () {

            //map each timecard to a newCommentHandler promise. then wait for them all to finish
            var commentPromises = self.selectedCollectionSupervisedTimecards().map(function (timecard) {
                return self.newCommentHandler({
                    PositionId: timecard.PositionId(),
                    PayPeriodEndDate: timecard.PeriodEndDate(),
                    PayCycleId: timecard.PayCycleId(),
                    TimecardId: timecard.TimecardId(),
                    CommentText: rejectComment
                });
            });

            return Promise.all(commentPromises);
        }).catch(function (err) {
            console.error(err);
        })
    }
    //self.returnAction = function () {
    //    submitStatusChangeForSelectedCollectionAsync(status.submitted.code);
    //}

    // displays a message in case of an error updating the status
    // note: tried to use the 'slide' ko binding, 
    // but could not find good way to prevent load flash
    var flashSaveError = function () {
        self.saveErrorIsVisible(true);
        setTimeout(function () {
            self.saveErrorIsVisible(false);
        }, 10000);
    }
    self.saveErrorIsVisible = ko.observable(false);
    self.statusSaveErrorMessage = resources.getObservable('TimeApproval.StatusSaveErrorMessage');

    function submitStatusChangeForSelectedCollectionAsync(newStatusCode) {
        self.newStatusCode(newStatusCode);
        self.updatingStatusMessage();
        self.isUpdatingStatus(true);
        var startTime = Date.now();

        var timecardIds = self.selectedCollectionSupervisedTimecards()
            .filter(function (timecard) {
                var currentStatus = status.get(timecard.TimecardStatus());
                return currentStatus != status.paid;
            })
            .map(function (timecard) {
                return timecard.TimecardId();
            });

        return actions.SubmitTimecardStatusesAsync(newStatusCode, timecardIds).then(function (newStatus) {
            self.selectedCollectionSupervisedTimecards().forEach(function (timecard) {
                timecard.TimecardStatus(newStatus);
                timecard.TimecardStatusActionDateTime(new Date());
            });
        }).then(function () {
            var endTime = Date.now();
            if (endTime - startTime > 2000) {
                self.isUpdatingStatus(false);
            } else {
                setTimeout(function () {
                    self.isUpdatingStatus(false);
                }, (2000 - (endTime - startTime)));
            }
            return;
        }).catch(function (err) {
            console.error(err);
            self.isUpdatingStatus(false);
            flashSaveError();
        })
    }

    // initialize status code to 0
    self.newStatusCode = ko.observable(0);
    // gets the text of the submit button from the resource manager
    self.submitButtonText = ko.computed(function () {
        switch (self.newStatusCode()) {
            //submit
            case 0:
                // use the unreject label if that is what we're doing
                if (self.enableUnrejectButton()) {
                    return resources.getObservable('TimeApproval.TimecardStatusUnreject');
                }
                if (self.enableUnapproveButton()) {
                    return resources.getObservable('TimeApproval.TimecardStatusUnapprove');
                }
                return resources.getObservable('TimeApproval.TimecardStatusSubmit');
                // unsubmit
            case 1:
                return resources.getObservable('TimeApproval.TimecardStatusUnsubmit');
                // reject
            case 2:
                return resources.getObservable('TimeApproval.TimecardStatusReject');
                // approve
            case 3:
                return resources.getObservable('TimeApproval.TimecardStatusApprove');
        }
    });


    // the display name of the supervisor in the comments dialog box
    self.supervisorCommentName = ko.computed(function () {
        var ary = self.SupervisorName().split(" ");
        switch (ary.length) {
            case 1:
                return ary[0];
                break;
            case 2:
            case 3:
                return (ary[1] + " " + ary[0]).replace(",", "");
                break;
            default:
                return self.SupervisorName();
                break;
        }

    });
    self.isUpdatingStatus = ko.observable(false);
    //self.updatingStatusMessage = resources.getObservable('TimeApproval.UpdatingStatusMessage');
    self.updatingStatusMessage = ko.observable("");

    self.rejectionDataModel = {
        supervisorName: self.SupervisorName(),
        employeeName: self.SuperviseeName(),
        employeeId: self.SuperviseeId(),
        startDate: self.selectedCollection().StartDate(),
        endDate: self.selectedCollection().EndDate(),
    };
}

module.exports = { viewModel: TimecardCollectionCarousel, template: markup };

//ko.components.register('timecard-collection-carousel', require('TimeManagement/TimeApproval/TimecardCollectionCarousel/_TimecardCollectionCarousel'));
if (!ko.components.isRegistered('timecard-collection-carousel')) {
    ko.components.register('timecard-collection-carousel', module.exports);
}

