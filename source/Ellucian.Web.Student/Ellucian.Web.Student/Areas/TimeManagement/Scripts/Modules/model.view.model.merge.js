﻿const dataMapping = require("TimeManagement/TimeSheet/time.sheet.data.mapping");

//mapped model is an object run through ko.mapping.fromJS,
//viewmodel is the object which will receive the properties of mappedModel (or the values)
let mapModelToSelf = function (mappedModel, viewModel) {
    // construct the data model from parent observables
    for (let key in mappedModel) {
        if (mappedModel.hasOwnProperty(key)) {

            if (ko.isObservable(viewModel[key])) {

                if (Array.isArray(viewModel[key]())) {
                    
                    //array length difference
                    //let diff = viewModel[key]().length - mappedModel[key]().length; 
                    //if (diff > 0) {
                    //    //if viewmodel length is greater than the mapped model length, 
                    //    //cut the viewmodel down to size
                    //    viewModel[key]().splice(diff);
                    //}
                    let maxIndex = Math.max(mappedModel[key]().length, viewModel[key]().length);


                    //for (var i = 0; i < mappedModel[key]().length; i++) {
                    for (let i = 0; i < maxIndex; i++) {
                        //for each item in the mapped model,
                        if (i < viewModel[key]().length) {

                            //merge each element in the mappedModel array into the viewModel element
                            mapModelToSelf(mappedModel[key]()[i], viewModel[key]()[i])
                            
                        }
                        else {
                            //the mapped model array is longer than the viewModel array, so push the 
                            //mapped model item into the viewmodel array
                            viewModel[key].push(mappedModel[key]()[i]);
                        }

                        if (i >= mappedModel[key]().length) {
                            //the viewModel array is longer than the mappedModel array, so update the mappedModel
                            mappedModel[key].push(viewModel[key]()[i]);
                        }
                        
                    }                                        
                }
                else {
                   // not an array, so update the viewModel property with the mappedModel value
                    viewModel[key](mappedModel[key]());
                }

            }
            else {
                //property doesn't exist on the view model so set it.
                viewModel[key] = mappedModel[key];
            }

        }
    }

}

module.exports = mapModelToSelf;