﻿
//  SUBMISSION.STATUS (module)
//		- Provides the correct status associated to a timecard based on an input integer value or string identifier
//		- Determines the appropriate aggregate status and available actions for a set of timecards (belonging to a pay period)
//		- Provides utility functions for operating on statuses (comparisons, sequences, etc)
//      REQUIREMENTS
//      - Resources
var resources = require('Site/resource.manager');

// status object
//  Arguments:
//      code - the integer identifier of the status -- corresponse with server enum value
//      name - the name of the status -- corresponds with database name
//      statusClass - the ESG class applied to this status
//      statusLabel - the resource file text to be applied to this status wrapped in an observable
function status(code, name, statusClass, statusLabel) {
     this.code = code;
     this.name = name;
     this.statusClass = statusClass || '';
     this.statusLabel = statusLabel || ko.observable('');
}
// aggregate status object
function groupStatus(statuses) {

     var _statuses = [];
     // exposed properties of the goup statuses
     // each of these represent an outcome derived from the available set of codes
     this.isSomeTimeWithoutStatus = false;
     this.isAllTimeWithoutStatus = false;
     this.isSomeTimeSubmitted = false;
     this.isAllTimeSubmitted = false;
     this.isSomeTimeUnsubmitted = false;
     this.isAllTimeUnsubmitted = false;
     this.isSomeTimeRejected = false;
     this.isAllTimeRejected = false;
     this.isSomeTimeApproved = false;
     this.isAllTimeApproved = false;
     this.isSomeTimePaid = false;
     this.isAllTimePaid = false;

     // construct all statuses
     // handle a non-argument
     if (typeof statuses != 'undefined' && statuses != null) {
          // handle anything that is not an array
          if (!Array.isArray(statuses)) {
               _statuses.push(getStatus(statuses));
          }   // handle a proper array 
          else {
               for (var i = 0; i < statuses.length; i++) {
                    _statuses.push(getStatus(statuses[i]));
               }
          }
          if (_statuses.length) {
               // set our exposed values
               // note on 'every' function: determines if all elements meet a given criteria; returns boolean
               // note on 'some' function: determines if any elements meet a given criteria; returns boolean        
               if (_statuses.every(function (sts) { return sts.code === _paid.code; })) {
                    this.isAllTimePaid = true;
               } else if (_statuses.some(function (sts) { return sts.code === _paid.code;; })) {
                    this.isSomeTimePaid = true;
               }

               if (_statuses.every(function (sts) { return sts.code === _approved.code; })) {
                    this.isAllTimeApproved = true;
               } else if (_statuses.some(function (sts) { return sts.code === _approved.code; })) {
                    this.isSomeTimeApproved = true;
               }

               if (_statuses.every(function (sts) { return sts.code === _rejected.code; })) {
                    this.isAllTimeRejected = true;
               } else if (_statuses.some(function (sts) { return sts.code === _rejected.code; })) {
                    this.isSomeTimeRejected = true;
               }

               if (_statuses.every(function (sts) { return sts.code === _unsubmitted.code; })) {
                    this.isAllTimeUnsubmitted = true;
               } else if (_statuses.some(function (sts) { return sts.code === _unsubmitted.code; })) {
                    this.isSomeTimeUnsubmitted = true;
               }

               if (_statuses.every(function (sts) { return sts.code === _submitted.code; })) {
                    this.isAllTimeSubmitted = true;
               } else if (_statuses.some(function (sts) { return sts.code === _submitted.code; })) {
                    this.isSomeTimeSubmitted = true;
               }

               if (_statuses.every(function (sts) { return typeof sts.code == 'undefined' || sts.code == null || sts.code === _none.code; })) {
                    this.isAllTimeWithoutStatus = true;
               } else if (_statuses.some(function (sts) { return typeof sts.code == 'undefined' || sts.code == null || sts.code === _none.code; })) {
                    this.isSomeTimeWithoutStatus = true;
               }
          }
     }

     // set the style and label based on lowest partial actionable, then lowest partial
     var statusClassToSet = null;
     var statusLabelToSet = null;

     if (this.isSomeTimeSubmitted && !this.isSomeTimeRejected && !this.isSomeTimeApproved) {
          statusClassToSet = 'esg-label esg-label--pending';
          statusLabelToSet = resources.getObservable('TimeStatus.PartiallySubmittedLabel');
     } else if (this.isSomeTimeUnsubmitted) {
          statusClassToSet = 'esg-label esg-label--default'; // these should display when some time is not/unsubmitted and some time is approved/rejected/paid
          statusLabelToSet = resources.getObservable('TimeStatus.UnsubmittedLabel');
     } else if (this.isSomeTimeWithoutStatus) {
          statusClassToSet = 'esg-label esg-label--default';
          statusLabelToSet = resources.getObservable('TimeStatus.NotSubmittedLabel');
     } else if (this.isSomeTimeRejected) {
          statusClassToSet = 'esg-label esg-label--pending';
          statusLabelToSet = resources.getObservable('TimeStatus.PartiallyRejectedLabel');
     } else if (this.isSomeTimeApproved) {
          statusClassToSet = 'esg-label esg-label--pending';
          statusLabelToSet = resources.getObservable('TimeStatus.PartiallyApprovedLabel');
     }

     var agStat = findAggregateStatus(_statuses);
     this.code = agStat.code;
     this.name = agStat.name;
     this.statusClass = statusClassToSet || agStat.statusClass;
     this.statusLabel = statusLabelToSet || agStat.statusLabel;
}

// status definitions
var availableStatuses = [
	new status(0, 'Submitted', 'esg-label esg-label--draft', resources.getObservable('TimeStatus.SubmittedLabel')),
	new status(1, 'Unsubmitted', 'esg-label esg-label--default', resources.getObservable('TimeStatus.UnsubmittedLabel')),
	new status(2, 'Rejected', 'esg-label esg-label--error', resources.getObservable('TimeStatus.RejectedLabel')),
	new status(3, 'Approved', 'esg-label esg-label--success', resources.getObservable('TimeStatus.ApprovedLabel')),
	new status(4, 'Paid', 'esg-label esg-label--success', resources.getObservable('TimeStatus.PaidLabel')),
    new status(-1, 'none', 'esg-label esg-label--default', resources.getObservable('TimeStatus.NotSubmittedLabel'))
];

// local constants
var _submitted = availableStatuses[0],
    _unsubmitted = availableStatuses[1],
    _rejected = availableStatuses[2],
    _approved = availableStatuses[3],
    _paid = availableStatuses[4],
    _none = availableStatuses[5];

// maximum available status
//var maxStatus = availableStatuses[availableStatuses.length -1];

//// minimum available status
//var minStatus = availableStatuses[0];

// finds the correct status given a single-value argument
function getStatus(arg) {

     // if an actual 'status' has been supplied, get the code
     if (typeof arg == 'object') {
          arg = arg && typeof arg.code != 'undefined' && arg.code != null ? arg.code : arg;
     }

     // no action has been done on timecard to acheive a status
     if (typeof arg == 'undefined' || arg == null)
          return _none;

     // if the argument provided corresponds to the status code
     if (typeof arg === 'number' || Number(arg)) {
          switch (Number(arg)) {
               case _submitted.code:
                    return _submitted;
               case _unsubmitted.code:
                    return _unsubmitted;
               case _rejected.code:
                    return _rejected;
               case _approved.code:
                    return _approved;
               case _paid.code:
                    return _paid;
               case _none.code:
                    return _none;
               default:
                    throw new Error('Invalid status code provided');
          }
          // if the argument provided corresponds to the status name
     } else if (typeof arg === 'string') {
          switch (arg.toLowerCase().trim()) {
               case _submitted.name.toLowerCase():
                    return _submitted;
               case _unsubmitted.name.toLowerCase():
                    return _unsubmitted;
               case _rejected.name.toLowerCase():
                    return _rejected;
               case _approved.name.toLowerCase():
                    return _approved;
               case _paid.name.toLowerCase():
                    return _paid;
               case _none.name.toLowerCase():
                    return _none;
               default:
                    throw new Error('Invalid status name provided');
          }
          // there was no valid argument provided
     } else {
          throw new Error('Unable to determine status from argument provided');
     }
}

// compares two statuses based on the code value
function compareTwoStatuses(statusA, statusB) {
     statusA = getStatus(statusA);
     statusB = getStatus(statusB);

     if (statusA.code === 1 && (statusB.code === 0 || statusB.Code > 1)) // the code of '1' is effectively not submitted
          return -1;
     if (statusB.code === 1 && (statusA.code === 0 || statusA.Code > 1)) // the code of '1' is effectively not submitted
          return 1;
     else if (statusA.code > statusB.code)
          return 1;
     else if (statusA.code < statusB.code)
          return -1;
     else
          return 0;
}

// finds the next status in the status chain
// note: may not be useful
//function findNextStatus(currentStatus){
//    currentStatus = getStatus(currentStatus);
//	if(compareTwoStatuses(currentStatus,maxStatus) == 0) // we will keep the maximum status as the cieling
//		return currentStatus;
//	else 
//		return getStatus(currentStatus.code + 1);
//}

// finds the previous status in the status chain
// note: may not be useful
//function findPreviousStatus(currentStatus){
//    currentStatus = getStatus(currentStatus);
//	if(compareTwoStatuses(currentStatus,minStatus) == 0) // we will keep the minimum status as the floor
//		return currentStatus;
//	else 
//		return getStatus(currentStatus.code - 1);
//}


// finds the base status from a group of statuses
function findAggregateStatus(statuses) {
     // handle anything that is not an array
     if (!Array.isArray(statuses)) {
          return getStatus(statuses);
     }
     // handle an empty array
     if (!statuses.length) {
          return _none;
     }
     // otherwise...
     // for now let us just set the 'lowest' value in the group as the aggregate
     var baseStatus = _paid;
     for (var i = 0; i < statuses.length; i++) {
          var thisStatus = getStatus(statuses[i]);
          if (compareTwoStatuses(thisStatus, baseStatus) == -1)
               baseStatus = thisStatus;
     }

     return baseStatus;
}

module.exports = {
     // 'enum' like values 
     submitted: _submitted,
     unsubmitted: _unsubmitted,
     rejected: _rejected,
     approved: _approved,
     paid: _paid,
     none: _none,
     // utilities
     get: getStatus,  // gets a status object given a single argument consisting of an integer or string as a status 'code', a string as a status 'name', or another identifiable status object
     aggregate: function (group) { return new groupStatus(group); },  // gets a single status for which a group of timecards can be represented by or defaulted to, using a single argument of an (mixed) array of integers, strings, objects which represent the status
     compare: compareTwoStatuses,  // compares two statuses, returning an integer 1 if the first is greater, an integer -1 if the second is greater, and an integer 0 if they are equal

     // note: these following export functions are not needed at this time
     //next: findNextStatus,
     //previous: findPreviousStatus,	
}