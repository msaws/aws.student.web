﻿
/*
    This module is represents a day of the time sheet collection, 
    where the data projected into a columnar structure.

    The values listed for this day should be time entries

*/

/*
    Dependencies
*/

let moment = require('moment'),
    errorDefinitions = require('TimeManagement/TimeSheet/time.sheet.error.definitions');

/*
 * dailyTimeEntries should be an array of DailyTimeEntry (server) models
 */
function TimeSheetDayModule(dailyTimeEntries) {

    let self = this;

    let dailyEntries = dailyTimeEntries;

    self.date = dailyEntries[0].DateWorked();

    /**
     * The section does total day hours error checking
     */
    //calculate the total hours from the given dailyTimeEntries for the day
    self.totalDayHours = ko.computed(function() {
        let totalTime = dailyEntries.reduce(function(total, current) {
            return total += current.HoursWorked();
        }, 0);
        return totalTime;
    });

    //set up a function to push/remove errors across the message channel
    function updateTotalDayHoursErrorState(newValue) {
        let errorKey = errorDefinitions.keys.totalDayTimeError;
        if (newValue > 24) {
            dailyEntries.forEach(function (entry) {
                if (entry.MessageChannel().indexOf(errorKey) < 0) {
                    entry.MessageChannel.push(errorKey);
                }
            });
        } else if (newValue <= 24) {
            dailyEntries.forEach(function (entry) {
                entry.MessageChannel.removeAll([errorKey]);
            });
        }
    }

    //initialize the errors on load
    updateTotalDayHoursErrorState(self.totalDayHours());
    let totalHoursErrorSubscription = self.totalDayHours.subscribe(updateTotalDayHoursErrorState); //subscribe to total day hour changes
     

    /*
     * This section does time overlap checking
     */
    //first sort the dailyEntries and project into range objects.
    let sortedRanges = ko.computed(function() {
        let sorted = dailyEntries.filter(function(entry) {
            let start = entry.TimeStart();
            let end = entry.TimeEnd();
            return start && end;
        })
        .map(function (entry) {
            
            return new range(entry, entry.TimeStart(), entry.TimeEnd());
        })
        .sort(function(a, b) {
            if (a.start.isBefore(b.start)) return -1;
            else if (a.start.isAfter(b.start)) return 1;
            else return 0;
        });
        return sorted;
    });

    //then reduce the sorted ranges to a single object
    //containing a list of overlapping ranges
    let overlapResult = ko.computed(function () {
        

        let finalResult = sortedRanges().reduce(function (result, current, idx, arr) {
            if (idx === 0) { return result; }
            let previous = arr[idx - 1];

            let overlap = previous.end.isAfter(current.start);

            if (overlap) {
                result.overlap = true;

                result.ranges.push({
                    previous: previous,
                    current: current
                });
            }
            return result;
        }, { overlap: false, ranges: [] });

        return finalResult;
    });
    

    

    function updateOverlapErrorState(newResult) {
        let errorKey = errorDefinitions.keys.overlapError;
        dailyEntries.forEach(function (entry) { //this changed...hook up to QA DB to test lalitha's scenario
            entry.MessageChannel.removeAll([errorKey]);
        });

        if (newResult.overlap) {
            newResult.ranges.forEach(function (resultRange) {
                if (resultRange.previous.MessageChannel.indexOf(errorKey) < 0) {
                    resultRange.previous.MessageChannel.push(errorKey);
                }
                if (resultRange.current.MessageChannel.indexOf(errorKey) < 0) {
                    resultRange.current.MessageChannel.push(errorKey);
                }
            });
        }
    }

    //check error state on module construction
    updateOverlapErrorState(overlapResult());
    let resultSubscription = overlapResult.subscribe(updateOverlapErrorState); //update error state when overlapResult changes


    /*
     * This section does missing entry checking. Needs to be here instead of DailyDetailTimeEntry
     * because each day's worth of viewmodels are disposed of when the day is switched on mobile, and 
     * the entire timecard needs to know about any missingTime, not just missing time on the selectedDay.     
     * In effect, when DailyDetailTimeEntry is disposed, its channel messages are removed, and this puts them back in.
     */
    let missingEntry = ko.computed(function () {
        dailyEntries.forEach(function (entry) {
            let start = entry.TimeStart(),
                end = entry.TimeEnd(),
                errorKey = errorDefinitions.keys.missingTimeWarning;

            if ((!start && end) || (start && !end)) {
                if (entry.MessageChannel().indexOf(errorKey) < 0) { //this changed.
                    entry.MessageChannel.push(errorKey);
                }
            }
            else {
                entry.MessageChannel.removeAll([errorKey]);
            }
        });
    });

    /*
     * Recommend that calling modules use this dispose function when getting rid of any timeSheetDay object. This
     * is not an knockout-component and the dispose function will not be called automatically.
     */
    self.dispose = function () {
        totalHoursErrorSubscription.dispose();
        self.totalDayHours.dispose();
        sortedRanges.dispose();
        overlapResult.dispose();
        resultSubscription.dispose();
        missingEntry.dispose();
        
    }
}


function range(entry, start, end) {
    this.start = moment(start);
    this.end = moment(end);
    this.MessageChannel = entry.MessageChannel;
}

module.exports = TimeSheetDayModule;