﻿var markup = require('./_DailyTimeEntryRow.html'),
    mapper = require("TimeManagement/Modules/model.view.model.merge"),
    resources = require('Site/resource.manager');

require("./_DailyTimeEntryRow.scss");

require('TimeManagement/TimeSheet/DailyTimeEntryGroup/_DailyTimeEntryGroup');


/*
 * parent component(s): Timecard
 * params = {
 *  workEntryModel: the JSON data model, mapped to observables, (this might be JSON mapped to observables)
 *  earningsTypeModels: [{earningsTypeModel}] - array of available earnings types to select (mapped to observables)
 *  isReadOnly: observable - indicating whether the daily time entry row can be modified by the user
 *  isMobileObservable: observable - whether the view is mobile sized
 *  daySelectedObservable: observable -  the Date selected in a mobile view
 *  
 *  onWorkTypeChange: Function: callback invoked when the earningsType is updated - not required,
 *                      Args: newValue - the new EarningsTypeId value
 *  onDailyTimeEntryChange: Function: callback invoked when any of this Work Type's DailyTimeEntry's is changed - not required
 *                      Args: workTypeId - the earningsTypeId of this model
 *                            dailyTimeEntryModel - the DailyTimeEntryModel that was modified
 *  dailyTimeEntryType: an integer value representing whether time entry is summary or detailed.
 * }
 */
function DailyTimeEntryRowViewModel(params) {

    var self = this;

    //argument verification
    if (params.onDailyTimeEntryChange && typeof params.onDailyTimeEntryChange !== 'function') {
        console.warn("params.onDailyTimeEntryChange must be a function");
    }
    if (params.onWorkTypeChange && typeof params.onWorkTypeChange !== 'function') {
        console.warn("params.onWorkTypeChange must be a function");
    }
    if (params.isReadOnly && !ko.isObservable(params.isReadOnly)) {
        console.warn("params.isReadOnly must be an observable");
    }


    mapper(params.workEntryModel, self);

    //initialize selected work type by finding the object in params.earningsTypeModels that has an Id that matches
    //this data model's EarningsTypeId;

    self.selectedWorkType = ko.observable((function () {
        if (self.EarningsTypeId() === null || self.EarningsTypeId() === "")
            return null;

        return params.earningsTypeModels.find(function (earningsTypeModel) {
            return earningsTypeModel.Id() === self.EarningsTypeId();
        });
    })());

    self.isReadOnly = params.isReadOnly ? params.isReadOnly : ko.observable(false);

    //initialize available work types to be the earningsTypeModels
    self.availableWorkTypes = ko.observableArray(params.earningsTypeModels);

    //compute the DailyTimeEntry model with the same day as the day selected
    self.selectedDailyTimeEntryGroup = ko.pureComputed(function () {
        if (!params.daySelectedObservable()) return null;
        return self.DailyTimeEntryGroupList.peek().find(function (dailyTimeEntryGroupModel) {
            return Date.Compare(dailyTimeEntryGroupModel.DateWorked.peek(), params.daySelectedObservable()) === 0;
        });
    });

    //compute the total hours worked for all the dailyTimeEntry models
    self.totalTime = ko.pureComputed(function () {
        return self.DailyTimeEntryGroupList().reduce(function (total, group) {
            return total += group.DailyTimeEntryList().reduce(function (total, dailyTimeEntry) {
                return total += (dailyTimeEntry.HoursWorked() ? dailyTimeEntry.HoursWorked() : 0);
            }, 0);
        }, 0);
    });

    //handler function to be executed when a dailyTimeEntry object changes
    //returns a Promise
    self.dailyTimeEntryChangeHandler = function (dailyTimeEntryGroupModel) {
        if (params.onDailyTimeEntryChange instanceof Function) {
            return params.onDailyTimeEntryChange(self.EarningsTypeId(), dailyTimeEntryGroupModel);
        }
        return Promise.resolve();
    }

        /*
         * subscribe to selectedWorkType. when the view changes, update the model and invoke the onWorkTypeChange callback
         */
    var workTypeChangeSubscription = self.selectedWorkType.subscribe(function (newEarningsTypeModel) {
        if (typeof newEarningsTypeModel !== 'undefined' && newEarningsTypeModel !== null)
            self.EarningsTypeId(newEarningsTypeModel.Id());
    else
        self.EarningsTypeId(null);

    if (params.onWorkTypeChange)
        params.onWorkTypeChange(self);
    });

        /*
        **  maps the the time entry type (as an integer value) to it's appropriate description
        **  1 -> Summary
        **  2 -> Detail
        **  this uses an object constructor to set the value from the argument provided to this module
        */
    self.dailyTimeEntryType = new function () {

        this.isSummary = this.isDetail = false;

        switch (params.dailyTimeEntryType) {
            case 1:
                this.isSummary = true;
                break;
            case 2:
                this.isDetail = true;
                break;
            default:
                throw new Error(params.dailyTimeEntryType +
                    ' is not valid; a daily time entry type of 1 (summary) or 2 (detail) must be provided'
                );
        }
    };
}


module.exports = { viewModel: DailyTimeEntryRowViewModel, template: markup };

if (!ko.components.isRegistered('daily-time-entry-row')) {
    ko.components.register('daily-time-entry-row', module.exports);
}
