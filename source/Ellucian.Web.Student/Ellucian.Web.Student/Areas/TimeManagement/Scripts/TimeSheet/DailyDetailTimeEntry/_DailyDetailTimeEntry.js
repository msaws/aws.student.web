﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
var markup = require('./_DailyDetailTimeEntry.html'),
    mapper = require("TimeManagement/Modules/model.view.model.merge"),
    errorDefinitions = require('TimeManagement/TimeSheet/time.sheet.error.definitions'),
    resources = require('Site/resource.manager'),
    moment = require("moment");


require('Site/Timepicker/timepicker.binding'),
require('./_DailyDetailTimeEntry.scss');



/*
 * parent component(s): DailyTimeEntryRow
 * params = {
 *  dailyTimeEntryModel: the JSON data model, mapped to observables,
 *  isReadOnly - observable - indicating whether the daily time entry can be modified by the user
 *  isMobile - observable - indicates whether the screen size is mobile size. controls timepicker options,
 *  onTimeWorkedChange - function - not required - callback is invoked when user changes a time value. argument is this viewmodel,
 *  index - integer - the index value of this timeEntry in/out in the list
 * }
 */
function DailyDetailTimeEntryViewModel(params) {
    var self = this;
    
    /*
    ** argument verification
    */
    if (!params) {
        throw new Error('params are required');
    }
    if (!ko.isObservable(params.isReadOnly)) {
        throw new Error('params.isReadOnly must be an observable');
    }

    if (!params.dailyTimeEntryModel) {
        throw new Error('params.dailyTimeEntryModel is required to construct _DailyDetailTimeEntry');
    }

    if (!ko.isObservable(params.isMobile)) {
        throw new Error('params.isMobile is required and must be an observable');
    }


    mapper(params.dailyTimeEntryModel, self);
    
    /*
    * params.isReadOnly is passed down as a param from parent components
    * self.IsReadOnly comes from the server
    */
    self.isReadOnly = ko.pureComputed(function () {        
        return params.isReadOnly() || self.IsReadOnly();
    });


    /*
     * One computed for each error
     */
    self.hasOverlapError = ko.computed(function () {
        return self.MessageChannel().indexOf(errorDefinitions.keys.overlapError) >= 0;
    });
    self.hasTotalTimeError = ko.computed(function () {
        return self.MessageChannel().indexOf(errorDefinitions.keys.totalDayTimeError) >= 0;
    })
    self.hasTimeOutOfOrderError = ko.computed(function () {
        return self.MessageChannel().indexOf(errorDefinitions.keys.timeOutOfOrderError) >= 0;
    })



    /*
    ** function calculates a summary error from each individual error
    */
    self.hasInputError = ko.computed(function () {

        if (self.isReadOnly()) return false;

        if (self.hasTimeOutOfOrderError()) return true;

        if (self.hasTotalTimeError()) return true;

        if (self.hasOverlapError()) return true;  

        return false;
    });


    /*
    ** get the resource constants
    */
    self.dailyDetailTimeEntryStartTimePlaceholder = resources.getObservable('TimeSheet.DailyDetailTimeEntryStartTimePlaceholder');
    self.dailyDetailTimeEntryEndTimePlaceholder = resources.getObservable('TimeSheet.DailyDetailTimeEntryEndTimePlaceholder');

    /*
     * When TimeStart or TimeEnd changes, 
     *  do format, 
     *  check for errors, 
     *  if no errors, 
     *      if timeEnd != null
     *          update hours worked
     *      save
     */

    /*
    ** create observables to back up the passed-down server objects
    */
    let timeStartBacker = ko.observable(self.TimeStart() ? moment(self.TimeStart()).format() : null);
    let timeEndBacker = ko.observable(self.TimeEnd() ? moment(self.TimeEnd()).format() : null);
    let hoursWorkedBacker = ko.observable(self.HoursWorked()).extend({ notify: "always" });

    self.formattedTimeStart = ko.computed({
        read: function () {
            return timeStartBacker() ? moment(timeStartBacker()).toDate() : null;
        },
        write: function (val) {
            if (val) {
                let dateTime = self.setTimeToDate(self.DateWorked(), val);
                return timeStartBacker(moment(dateTime).format());
            }
            else {
                timeStartBacker(null);
            }
        }
    });
    self.formattedTimeEnd = ko.computed({
        read: function () {
            return timeEndBacker() ? moment(timeEndBacker()).toDate() : null;
        },
        write: function (val) {
            if (val) {
                let dateTime = self.setTimeToDate(self.DateWorked(), val);
                return timeEndBacker(moment(dateTime).format());
            }
            else {
                timeEndBacker(null);
            }
        }
    });


    var handleTimeChange = ko.computed(function () {
        let timeStart = timeStartBacker();
        let timeEnd = timeEndBacker();

        if (!timeStart || timeStart == "Invalid date" || !timeEnd || timeEnd == "Invalid date") {

            hoursWorkedBacker(0);

        } else {

            let startDateTime = moment(timeStart);
            let endDateTime = moment(timeEnd);

            //push the error key into message channel, if the error condition is true
            //otherwise, remove it.
            let errorKey = errorDefinitions.keys.timeOutOfOrderError;

            if (endDateTime.isBefore(startDateTime)) {
                if (self.MessageChannel.peek().indexOf(errorKey) < 0) {
                    self.MessageChannel.push(errorKey);
                }
            }
            else {
                self.MessageChannel.removeAll([errorKey]);
                hoursWorkedBacker(endDateTime.diff(startDateTime, "hours", true)); //true as third arg does floating point precision
            }
        }

    });

    var hoursWorkedSubscription = hoursWorkedBacker.subscribe(function (newVal) {
        //hoursWorkedBacker always notifies so something changed.

        let isChange = false;
        let dataTimeStart = self.TimeStart() ? moment(self.TimeStart()).format() : null;
        let dataTimeEnd = self.TimeEnd() ? moment(self.TimeEnd()).format() : null;
        if (dataTimeStart !== timeStartBacker()) {            
            isChange = true;
        }
        if (dataTimeEnd !== timeEndBacker()) {
            isChange = true;
        }
        if (isChange) {

            if (!timeStartBacker() && !timeEndBacker()) {
                self.Id(null); //both timeStart and timeEnd are null, so this is a delete
                self.HoursWorked(null);
                self.TimeStart(null);
                self.TimeEnd(null);
            } else {
                //this is a change or an add
                self.TimeStart(timeStartBacker());
                self.TimeEnd(timeEndBacker());
                self.HoursWorked(newVal);
            }
            
            //only invoke the callback if there are no errors
            if (params.onTimeWorkedChange instanceof Function && !self.hasInputError()) {
                params.onTimeWorkedChange(self);
            }
        }
        
    })


    /*
     * create a set of timepicker options shared by both the start time input and the end time input
     */
    var sharedTimepickerOptions = ko.computed(function () {
        return {
            step: 15,
            stopScrollPropagation: true,
            timeFormat: function (date) {
                return Globalize.format(date, 't');
            },
            maxTime: "11:55 PM",
            showOn: params.isMobile() ? [] : ['focus', 'click'],/* do not show for mobile */
        }
    });

    /*
     * set up separate options for the start and end inputs. merge the specific options with the shared options
     */
    self.startTimepickerOptions = ko.pureComputed(function () {
        return Object.assign({}, sharedTimepickerOptions())
    });

    self.endTimepickerOptions = ko.pureComputed(function () {
        return Object.assign({
            showDuration: timeStartBacker() ? true : false,
            durationTime: function () {            
                return timeStartBacker() ? moment(timeStartBacker()).toDate() : null;
            },
            scrollDefault: timeStartBacker() ? moment(timeStartBacker()).toDate() : null,
        
        }, sharedTimepickerOptions());
    });





    /*
    ** function to determine a partially complete time entry
    ** returns the name of the affected input type or empty string
    */
    self.missingTimeEntryIndicator = ko.computed(function () {
        if (!self.hasInputError()) {

            if (timeStartBacker() === null && timeEndBacker() != null) {
                return 'timeStart';
            }
            if (timeStartBacker() != null && timeEndBacker() === null) {
                return 'timeEnd';
            }
        }
        return '';
    });

    //given two Date objects, function merges the date portion of date
    //and the time portion of time into a new Date object
    self.setTimeToDate = function (date, time) {
        if (!date || !time)
            return null;
        try {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), 0, 0);
        } 
        catch (err) {
            return null;
        }

    }

    self.timeStartId = Math.random().toString().split('.')[1];
    self.timeEndId = Math.random().toString().split('.')[1];

    var date = self.DateWorked();
    var formattedDate = Globalize.format(date, 'ddd') + ' ' + Globalize.format(date, 'M/d');

    self.timeStartLabel = ko.pureComputed(function () {
        let time = self.formattedTimeStart() ? moment(self.formattedTimeStart()).format('h mm a') : 'none';
        return formattedDate + ' Time Start number ' + (params.index() + 1) + ', ' + time;
    });

    self.timeEndLabel = ko.pureComputed(function () {
        let time = self.formattedTimeEnd() ? moment(self.formattedTimeEnd()).format('h mm a') : 'none';
        return formattedDate + ' Time End number ' + (params.index() + 1) + ', ' + time;
    });

    self.setInputAriaLabel = function (model, event) {        
        if (event.which === 38 || event.which === 40) {
            let proposedTime = $('.ui-timepicker-selected:last').text();
            $('#' + event.target.id).attr('aria-label', 'change to ' + proposedTime);
        } else {
            $('#' + event.target.id).attr('aria-label','');
        }
        //if (event.which === 38 || event.which === 40) {
        //    let proposedTime = $('.ui-timepicker-selected').text();
        //    $('label[for=' + event.target.id + ']').text(proposedTime)
        //    console.log($('label[for=' + event.target.id + ']'));
        //}
    }

    self.removeInputAriaLabel = function (model, event) {
        $('#' + event.target.id).removeAttr('aria-label');
        //if (event.target.id === self.timeStartId) {
        //    $('label[for=' + event.target.id + ']').text(self.timeStartLabel());
        //} else if (event.target.id === self.timeEndId) {
        //    $('label[for=' + event.target.id + ']').text(self.timeEndLabel());
        //}
    }

    //$('#' + self.timeStartId + ', ' + '#' + self.timeEndId).on('keyup.dailyDetailTimeEntry',
    //    function (e) {
    //        console.log(e);
    //        if (e.which === 38 || e.which === 40) {
    //            let proposedTime = $('.ui-timepicker-selected').text();
    //            $('label[for=' + this.id + ']').text(proposedTime)
    //        } else if (this.id === self.timeStartId) {
    //            $('label[for=' + this.id + ']').text(self.timeStartLabel());
    //        } else if (this.id === self.timeEndId) {
    //            $('label[for=' + this.id + ']').text(self.timeEndLabel());
    //        }
    //    });


    self.dispose = function () {
        
        self.hasInputError.dispose();
        self.hasTotalTimeError.dispose();
        self.hasTimeOutOfOrderError.dispose();        
        self.hasOverlapError.dispose();
        self.MessageChannel.removeAll();

        self.formattedTimeEnd.dispose();
        self.formattedTimeStart.dispose();
        self.missingTimeEntryIndicator.dispose();

        handleTimeChange.dispose();
        hoursWorkedSubscription.dispose();        
        sharedTimepickerOptions.dispose();

    };
}


module.exports = { viewModel: DailyDetailTimeEntryViewModel, template: markup };

if (!ko.components.isRegistered('daily-detail-time-entry')) {
    ko.components.register('daily-detail-time-entry', module.exports);
}