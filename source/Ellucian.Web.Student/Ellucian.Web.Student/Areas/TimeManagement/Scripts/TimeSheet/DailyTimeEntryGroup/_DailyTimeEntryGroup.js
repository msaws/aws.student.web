﻿const markup = require("./_DailyTimeEntryGroup.html"),
    mapper = require("TimeManagement/Modules/model.view.model.merge");

require("./_DailyTimeEntryGroup.scss");

require('TimeManagement/TimeSheet/DailyTimeEntry/_DailyTimeEntry');
require('TimeManagement/TimeSheet/DailyDetailTimeEntry/_DailyDetailTimeEntry');

/*
 * params = {
 *  dataModel: DailyTimeEntryGroup data model (from server c#) mapped to KO
 *  dailyTimeEntryType: object - { isSummary: bool, isDetail: bool},
 *  isReadOnly : observable - indicating whether the daily time entry group can be modified by the user,
 *  onDailyTimeEntryChange: Function: callback invoked when any of the dailyTimeEntry or dailyDetailTimeEntry changes
 *                          Args: dailyTimeEntryModel - the data model that was updated
 * }
 */
function DailyTimeEntryGroupViewModel(params) {
    let self = this;

    // construct the data model from parent observables
    //for (var key in params.dataModel) {
    //    self[key] = params.dataModel[key];
    //}
        
    mapper(params.dataModel, self);

    self.dailyTimeEntryType = params.dailyTimeEntryType;

    // params.isReadOnly is based on the timecard's status
    // IsReadOnly from the server model is based on Date Worked in relation to the pay period start/end dates
    // if either of these is true, then set self.isReadOnly to true
    self.isReadOnly = ko.pureComputed(function () {
        return params.isReadOnly() || self.IsReadOnly();
    });

    self.dailyTimeEntryChangeHandler = function (dailyEntryModel) {
        if (params.onDailyTimeEntryChange instanceof Function) {

            if (self.dailyTimeEntryType.isDetail) {
                if (self.DailyTimeEntryList().length > 1) {
                    for (var i = 0; i < self.DailyTimeEntryList().length; i++) {
                        if (!self.DailyTimeEntryList()[i].TimeStart() && !self.DailyTimeEntryList()[i].TimeEnd()) {
                            self.DailyTimeEntryList.splice(i, 1);
                        }
                    }
                }
            }
            return params.onDailyTimeEntryChange(self);
        }
        return Promise.resolve();
    }

    self.deleteEnabled = ko.pureComputed(function () {
        return self.dailyTimeEntryType.isDetail && !self.isReadOnly() && self.DailyTimeEntryList().length > 1;
    });

    self.deleteEntry = function (data, event) {
        let removedEntry = self.DailyTimeEntryList.pop();
        if (removedEntry.TimeStart() || removedEntry.TimeEnd()) {
            return self.dailyTimeEntryChangeHandler();
        }
        return Promise.resolve();
    }

    self.addEnabled = ko.pureComputed(function () {
        if (self.dailyTimeEntryType.isSummary || self.isReadOnly()) {
            return false;
        }
        let lastEntry = self.DailyTimeEntryList()[self.DailyTimeEntryList().length - 1];
        return lastEntry.TimeStart() && lastEntry.TimeEnd();
    });
    self.addEntry = function (data, event) {

        let newEntry = {
            Id: ko.observable(null),
            DateWorked: ko.observable(self.DateWorked()),
            HoursWorked: ko.observable(null),
            TimeStart: ko.observable(null),
            TimeEnd: ko.observable(null),
            IsReadOnly: ko.observable(false),
            IsHistorical: ko.observable(false),
            MessageChannel: ko.observableArray([])
        };

        self.DailyTimeEntryList.push(newEntry);
        
        
    }
}


module.exports = { viewModel: DailyTimeEntryGroupViewModel, template: markup };

if (!ko.components.isRegistered('daily-time-entry-group')) {
    ko.components.register('daily-time-entry-group', module.exports);
}