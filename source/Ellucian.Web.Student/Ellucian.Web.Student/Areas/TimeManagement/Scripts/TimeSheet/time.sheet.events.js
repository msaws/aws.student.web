﻿module.exports = {

    /*
     * Published by the totals component when its grand total is updated. The body of event
     * is the total hours. 
     */
    TIMESHEET_TOTALHOURSUPDATE: "timesheet:totalhoursupdate",

    /*
     * Published when an actual timecard update resolves successfully.
     * Publishes an object that identifies the specific timecards being saved in the following format
     * {
     *  startDate: the effective start date of the week for the timesheet
     *  endDate: the effective end date of the week for the timesheet
     *  payCycleId: the id of the paycycle of the timesheet
     * }
     */
    TIMESHEET_SAVECOMPLETE: "timesheet:savecomplete",

    /*
     * Published when any messages are added or removed.
     * Publishes an array of error objects with the following format:
     * {
     *      id: messageId from the error defintions file
     *      type: type of message (error, warning) from the error definitions file   
     *      message: message from the error definitions file,
     *      date: the date in which the error/warning exits,
     *      timecardId: id of the timecard where the error/warning exists,
     *      workEntryId: id of the work entry where the error/warning exists,
     *      groupId: id of the group where the error/warning exists,
     *      entryId: id of the entry where the error/warning exists
     *      
     * }
     */
    TIMESHEET_MESSAGES: "timesheet:messages"
     
}
