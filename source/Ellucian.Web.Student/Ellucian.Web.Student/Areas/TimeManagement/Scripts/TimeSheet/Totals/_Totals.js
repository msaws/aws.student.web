﻿var markup = require('./_Totals.html'),
    actions = require('TimeManagement/TimeSheet/time.sheet.actions'),
    timeSheetEvents = require('TimeManagement/TimeSheet/time.sheet.events'),
    resources = require('Site/resource.manager');

require("./_Totals.scss");

/**
 * Component represents all the different totals in timecard collection
 * 
 *   this component is responsible for displaying the accumulated time of an employee in a given work week
 *   it will display daily totals (by earning type? or all-together?) of (regular, vaction, sick, etc...?) hours
 *   it will display daily totals for overtime hours, by earning type
 *   it will display gross weekly totals
 * 
 * parent component(s): TimecardCollection
 *
 * params: {
 *  timeSheetDays - observable - array of TimeSheetDay objects, one for each day of the week
 *  startDate - start date of the week
 *  enddate - end date of the week
 *  payCycleId - identifier of the context pay cycle
 *  days - observable array of days in the week
 *  selectedDay - observable that indicates the selected day in mobile
 * }
 */
function TotalsViewModel(params) {

    if (!ko.isObservable(params.timeSheetDays) || !Array.isArray(params.timeSheetDays())) {
        throw new Error("timeSheetDays parameter is required, must be an observable, and value must be an array");
    }
    if (!params.startDate || !params.endDate)
        throw new Error("Start and End dates are required");
    if (!params.payCycleId)
        throw new Error("paycycleId required");
    if (!params.days)
        throw new Error("days array is required");
    if (!params.selectedDay || !ko.isObservable(params.selectedDay))
        throw new Error("selectedDay is required to be an observable");

    var self = this;

    self.selectedDay = params.selectedDay;
    self.totalOvertimeHours = ko.observable(0);

    self.weeklyTotalsTitle = resources.getObservable('TimeSheet.WeeklyTotalHeaderLabel');

    self.dailyTotalHours = ko.pureComputed(function () {

        let dayInfoArray = params.timeSheetDays().map(function(dayModule) {
            return {
                dayString: Globalize.format(dayModule.date, "dddd") + " " + Globalize.format(dayModule.date, "M/d"),
                dayHours: dayModule.totalDayHours(),
                isOutsideTimecardDates: Date.Compare(params.startDate(), dayModule.date) == 1 || Date.Compare(dayModule.date, params.endDate()) == 1
            };
        });
        return dayInfoArray;
    });

    // total weekly hours of all timecards
    self.weeklyTotalHours = ko.pureComputed(function () {
        return self.dailyTotalHours().reduce(function (total, daily) {
            return total + daily.dayHours;
        }, 0);
    }).publishOn(timeSheetEvents.TIMESHEET_TOTALHOURSUPDATE);

    self.weeklyRegularHours = ko.pureComputed(function () {
        return self.weeklyTotalHours() - self.totalOvertimeHours();
    })

    self.selectedDayTotalHours = ko.pureComputed(function () {
        if (self.selectedDayIndex() < 0) return null;
        let dayHours = params.timeSheetDays()[self.selectedDayIndex()].totalDayHours();
        return dayHours;
    });


    let saveCompleteSubscription = ko.postbox.subscribe(timeSheetEvents.TIMESHEET_SAVECOMPLETE, function (timesheetIdentifier) {
        //check the identifiers of the timesheet that was saved. only calculate overtime if the 
        //timesheet that was saved matches the identifiers of this totals component
        // -- reason -- 
        //  components from other weeks are not being disposed. they're still sitting out in memory
        //  still subscribed to this event. I found this seeing four OT Calc requests in the network tab in DevTools.
        //  we need to investigate the disposal of components. maybe use an observable to subscribe to the event, and then
        //  unsubscribe in dispose (assuming the object is actually disposed).
        if (Date.Compare(timesheetIdentifier.startDate, params.days()[0]) === 0 &&
            Date.Compare(timesheetIdentifier.endDate, params.days()[6]) === 0 &&
            timesheetIdentifier.payCycleId === params.payCycleId())
        {
            calculateOvertimeAsync();
        }


    });


    function calculateOvertimeAsync() {

        return actions.CalculateOvertimeAsync(params.days()[0], params.days()[6], params.payCycleId())
            .then(function (overtimeCalculationResultModel) {
                self.totalOvertimeHours(overtimeCalculationResultModel.TotalOvertime);
                return;
            }).catch(function (err) {
                console.error(err);
            });
    };
    calculateOvertimeAsync(); //get overtime to initialize


    //helper to get the name of the day of the date based on the Global format string
    self.getDayName = function (date) {
        return Globalize.format(date, "dddd");
    }


    //computes the index of the selectedDay in the days array
    self.selectedDayIndex = ko.pureComputed(function () {
        if (!self.selectedDay())
            return -1;
        return params.days.peek()
            .map(function (day) { return day.getTime() })
            .indexOf(self.selectedDay().getTime());
    });

    $("#weekly-total-body").makeTableResponsive();

    self.mobileDailyTotalSummaryHeaderLabel = resources.getObservable('TimeSheet.MobileDailyTotalSummaryHeaderLabel');
    self.mobileDailyTotalSummaryTotalLabel = resources.getObservable('TimeSheet.MobileDailyTotalSummaryTotalLabel');
    self.weeklyRegularHoursTotalLabel = resources.getObservable('TimeSheet.WeeklyRegularHoursTotalLabel');
    self.weeklyOvertimeHoursTotalLabel = resources.getObservable('TimeSheet.WeeklyOvertimeHoursTotalLabel');

    self.dispose = function () {
        self.weeklyTotalHours.stopPublishingOn(timeSheetEvents.TIMESHEET_TOTALHOURSUPDATE);
        saveCompleteSubscription.dispose();
    }
}


module.exports = { viewModel: TotalsViewModel, template: markup };


if (!ko.components.isRegistered('totals')) {
    ko.components.register('totals', module.exports);
}