﻿const resources = require("Site/resource.manager");

//these constants allow us to avoid magic strings
const errorDefinitionkeys = {
    overlapError: 'overlapError',
    totalDayTimeError: 'totalDayTimeError',
    timeOutOfOrderError: 'timeOutOfOrderError',
    missingTimeWarning: 'missingTimeWarning'
}

const errorDefinitionTypes = {
    error: 'error',
    warning: 'warning'
}

//'class' definition
function errorDefintion(type, message) {
    this.type = type;
    this.message = message;
}

//this provides the public API and the errorDefintiions instances
function DefinitionModule() {

    let errorMap = {};

    function init() {
        addError(errorDefinitionkeys.overlapError,
            new errorDefintion(errorDefinitionTypes.error, resources.getObservable('TimeSheet.DailyTimeOverlapError')));

        addError(errorDefinitionkeys.totalDayTimeError,
            new errorDefintion(errorDefinitionTypes.error, resources.getObservable('TimeSheet.TimeSheetMaxHoursError')));

        addError(errorDefinitionkeys.timeOutOfOrderError,
            new errorDefintion(errorDefinitionTypes.error, resources.getObservable("TimeSheet.DailyDetailTimeEntryStartAfterEndError")));

        addError(errorDefinitionkeys.missingTimeWarning,
            new errorDefintion(errorDefinitionTypes.warning, resources.getObservable('TimeSheet.DailyDetailTimeEntryStartOrEndMissingError')));
    }

    function addError(key, definition) {
        errorMap[key] = definition;
        return errorMap[key];
    }

    this.getError = function(key) {
        return errorMap[key];
    }

    init();

}

//instantiate the module instance
let instance = new DefinitionModule();

module.exports = {
    keys: errorDefinitionkeys,
    types: errorDefinitionTypes,
    get: instance.getError
};