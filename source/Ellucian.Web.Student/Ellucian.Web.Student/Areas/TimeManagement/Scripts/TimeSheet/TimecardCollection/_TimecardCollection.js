﻿/* Copyright 2016-2017 Ellucian Company L.P. and its affiliates. */
var markup = require('./_TimecardCollection.html'),
    mapper = require("TimeManagement/Modules/model.view.model.merge"),
    autoSaveEvents = require('Site/Components/AutoSave/_AutoSaveEvents'),
    timeSheetEvents = require("TimeManagement/TimeSheet/time.sheet.events"),
    resources = require('Site/resource.manager'),
    errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");
timeSheetDay = require('TimeManagement/TimeSheet/time.sheet.day'),
moment = require('moment');

require('./_TimecardCollection.scss');

require('TimeManagement/TimeSheet/TimecardCollectionActionBar/_ActionBar');
require('TimeManagement/Components/TimecardHeader/_TimecardHeader');
require('TimeManagement/TimeSheet/Timecard/_Timecard');
require('TimeManagement/TimeSheet/Totals/_Totals');



/*
* parent component(s): TimeSheetViewComponent
* params: {
*  dataModel: the timecard collection data model, mapped to observables
* }
*/
function TimecardCollectionViewModel(params) {
    let self = this;


    mapper(params.dataModel, self);

    let storage;
    try {
        storage = window.localStorage;
    }
    catch (err) {
        console.log(err);
    }
    try {
        if (window.Ellucian && window.Ellucian.Storage && window.Ellucian.Storage.local) {
            storage = window.Ellucian.Storage.local;
        }
    }
    catch (err) {
        console.log(err);
    }

    self.headerComponentMarkup = $.parseHTML("<timecard-header params='timecardModel: timecard'></timecard-header>");

    self.employeeNameDisplay = ko.pureComputed(function () {
        var name = self.EmployeeName().split(',');
        return (name[1] ? name[1].trim() + ' ' : '') + name[0];
    });

    //computes an array of dates between the start and enddates
    self.availableDays = ko.pureComputed(function () {
        var days = [];
        for (var i = self.StartDate() ; i <= self.EndDate() ; i = Date.DateOffset(i, 1)) {
            days.push(i);
        }
        return days;
    });


    self.daysInWeek = ko.pureComputed(function () {
        var days = [];

        for (var i = self.EffectiveStartDate() ; i <= Date.DateOffset(self.EffectiveStartDate(), 6) ; i = Date.DateOffset(i, 1)) {
            days.push(i);
        }
        return days;
    });

    //in mobile view, the date selected by the user for which to display the time entry
    const selectedDayKey = self.EmployeeId.peek();
    self.selectedDay = ko.observable(tryGetStoredDay());

    /*
    ** maintains last day user has accessed    
    */

    function tryGetStoredDay() {
        let value;
        try {
            value = storage.getItem(selectedDayKey);
        }
        catch (err) {
            console.log(err);
        }

        value = value ? new Date(value) : null;
        if (!value || value == "Invalid Date" || !(value instanceof Date) || moment(value).isBefore(self.StartDate.peek()) || moment(value).isAfter(self.EndDate.peek()))
            return null;
        return value;
    }
    function setStoredDay(newValue) {
        if (newValue) {
            try {
                storage.setItem(selectedDayKey, newValue);
            }
            catch (err) {
                console.log(err);
            }
        }
        return;
    }
    var setDaySubscription = self.selectedDay.subscribe(setStoredDay);

    self.collapsibleGroupStateInfo = self.Timecards().map(function (timecard) {
        var collapsibleGroupKey = "TimeSheet.Collapsible." + timecard.PositionId();
        var lastIsOpenBool = Ellucian.Storage.session.getItem(collapsibleGroupKey);
        if (!lastIsOpenBool) { lastIsOpenBool = false; } // if key not found in session storage, default to false (closed)
        var lastIsOpenBoolObs = ko.observable(lastIsOpenBool);
        var lastIsOpenSub = lastIsOpenBoolObs.subscribe(function (newVal) {
            Ellucian.Storage.session.setItem(collapsibleGroupKey, newVal);
        })
        var collapsibleGroupStateObj = { collapsibleIsOpenObs: lastIsOpenBoolObs, collapsibleIsOpenSub: lastIsOpenSub }
        return collapsibleGroupStateObj;
    });

    /*
     * create the date range objects to pass to the action bar
     */
    self.previousDateRange = ko.pureComputed(function () {
        if (self.PreviousStartDate() && self.PreviousEndDate()) {
            return {
                startDate: self.PreviousStartDate(),
                endDate: self.PreviousEndDate(),
                periodStartDate: self.PreviousPayPeriodStartDate(),
                periodEndDate: self.PreviousPayPeriodEndDate()
            }
        }
        return null;
    });
    self.nextDateRange = ko.pureComputed(function () {
        if (self.NextStartDate() && self.NextEndDate()) {
            return {
                startDate: self.NextStartDate(),
                endDate: self.NextEndDate(),
                periodStartDate: self.NextPayPeriodStartDate(),
                periodEndDate: self.NextPayPeriodEndDate()
            }
        }
        return null;
    });

    self.noActivePositionsMessage = resources.getObservable('TimeSheet.TimeEntryNoActivePositions');


    //the DailyTimeEntry data models have a MessageChannel observable array
    //that are used to pass messages to subscribers. This computed is one of those subscribers
    //We project each Notification into an object containing info from the errorDefinitions module,
    //and the identifiers of the data model where the message exists.    
    //the resulting errors are then published on the TIMESHEET_MESSAGES postbox topic so that
    //subscribers get error updates.

    let allMessages = ko.computed(function () {
        let messages = self.Timecards.peek().flatMap(function (timecard) {
            return timecard.WorkEntryList().flatMap(function (workEntry) {
                return workEntry.DailyTimeEntryGroupList.peek().flatMap(function (group) {
                    return group.DailyTimeEntryList().flatMap(function (entry) {
                        return entry.MessageChannel().map(function (notificationId) {
                            let error = errorDefinitions.get(notificationId);
                            return {
                                id: notificationId,
                                type: error.type,
                                message: error.message(),
                                date: entry.DateWorked.peek(),
                                timecardId: timecard.TimecardId.peek(),
                                workEntryId: workEntry.Id.peek(),
                                groupId: group.Id.peek(),
                                entryId: entry.Id.peek()
                            }
                        });

                    });
                });
            });
        }).unique(function (errorObj) { //get distinct errors based on the error id
            return errorObj.id;
        });
        return messages;
    }).publishOn(timeSheetEvents.TIMESHEET_MESSAGES);

    //thinking of the timecard data model projected into table, the model is
    //describe in terms of the rows of the table.
    //but for day based error checking, we need to project the data by columns
    //essentially, rotating the table on its side, so that the resulting 'rows' contain all DailyTimeEntrys
    //for a given day.
    self.collectionDayModules = ko.computed(function () {
        //loop through each day of the week, grabbing the DailyTimeEntryList
        //from each Group in that day
        let dayModuleArray = self.daysInWeek().map(function (day, index) {
            let entriesForDay = self.Timecards().flatMap(function (timecard) {
                return timecard.WorkEntryList().flatMap(function (workEntry) {
                    return workEntry.DailyTimeEntryGroupList()[index].DailyTimeEntryList();
                });
            });

            //pass the entries into the timeSheetDay module constructor
            return new timeSheetDay(entriesForDay);
        });

        return dayModuleArray;
    });

    //this function facilitates disposal of the timeSheetDay objects,
    //which themselves dispose of their computeds.
    function dayModuleDisposal(dayModules) {
        dayModules.forEach(function (dayModule) {
            dayModule.dispose();
        });
        return;
    }

    //subscribe to the collectionModules beforeChange event, so that we can
    //properly dispose of the previous timeSheetDay modules. 
    let dayModuleDisposalSubscription = self.collectionDayModules.subscribe(dayModuleDisposal, self, "beforeChange");


    //subscribe to the autosave button click events and simulate a successful save
    //this belongs at the top level component
    let autoSavePostboxSubscription = ko.postbox.subscribe(autoSaveEvents.AUTOSAVE_SAVEBUTTONPRESSED, function () {
        ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVECOMPLETE);
    });

    self.dispose = function () {
        autoSavePostboxSubscription.dispose();
        dayModuleDisposalSubscription.dispose();
        dayModuleDisposal(self.collectionDayModules());
        self.collectionDayModules.dispose();
        allMessages.stopPublishingOn(timeSheetEvents.TIMESHEET_MESSAGES);
        allMessages.dispose();
        setDaySubscription.dispose();

        self.collapsibleGroupStateInfo.forEach(function (collapsibleGroup) {
            collapsibleGroup.collapsibleIsOpenSub.dispose();
        });
    }
}


module.exports = { viewModel: TimecardCollectionViewModel, template: markup };

if (!ko.components.isRegistered('timecard-collection')) {
    ko.components.register('timecard-collection', module.exports);
}
