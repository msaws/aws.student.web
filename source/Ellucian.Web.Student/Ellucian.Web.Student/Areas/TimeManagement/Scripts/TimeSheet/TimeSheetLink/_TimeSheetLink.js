﻿var markup = require('./_TimeSheetLink.html'),
    dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping'),
    status = require('TimeManagement/Modules/submission.status'),
    resources = require('Site/resource.manager');

require("./_TimeSheetLink.scss");
/*
 * parent component(s): TimeSheetLinkCollection
 * params: {
 *  dataModel: the time sheet link data model, mapped to observables,
 *  payCycleId: the payCycleId
 * }
*/
function TimeSheetLinkViewModel(params) {
    var self = this;
    ko.mapping.fromJS(params.dataModel, dataMapping, self);

    // a description of the week
    self.weekDisplay = ko.pureComputed(function () {
        if (self.StartDate() && self.EndDate()) {
            var StartDate = Globalize.format(self.StartDate(), "d");
            var EndDate = Globalize.format(self.EndDate(), "d");
            return StartDate + " - " + EndDate;
        }
        else return "";
    });

    // when the timecard is due
    self.timecardDueDateTimeDisplay = ko.pureComputed(function () {
        return 'Due by: ' + Globalize.format(self.EmployeeSubmitDeadline(), 'd') + ' '
                          + Globalize.format(self.EmployeeSubmitDeadline(), 't');
    });

    // total hours line
    self.totalHoursDisplay = ko.pureComputed(function () {
        return resources.getObservable("TimeSheet.TimeSheetLinkTotalHoursLabel")() + " " + self.TotalHours().toFixed(2) + " " + resources.getObservable("TimeSheet.TimeSheetLinkTotalHoursUnits")();
    });

    // timesheet status list
    self.timecardPositionStatuses = ko.pureComputed(function () {
        var timecardPositionStatus = function (sts) {
            var currentStatus = status.get(sts.CurrentStatus());
            this.statusIconClass = currentStatus.code !== status.none.code ? currentStatus.statusClass : '';
            this.statusWord = currentStatus.code !== status.none.code ? currentStatus.statusLabel() : '';
            this.statusPosition = sts.PositionTitle();
        }
        if (self.TimecardPositionStatuses() && self.TimecardPositionStatuses().length > 0) {
            return self.TimecardPositionStatuses().map(function (status) { return new timecardPositionStatus(status); });
        }
        else {
            return [];
        }
    });


    self.timeSheetUrl = ko.pureComputed(function () {
        return "#s={0}&e={1}&p={2}&pps={3}&ppe={4}".format(
            Globalize.format(self.StartDate(), "d"),
            Globalize.format(self.EndDate(), "d"),
            params.payCycleId(),
            Globalize.format(self.PayPeriodStartDate(), 'd'),
            Globalize.format(self.PayPeriodEndDate(), 'd'));
    });
}


module.exports = { viewModel: TimeSheetLinkViewModel, template: markup };

if (!ko.components.isRegistered('time-sheet-link')) {
    ko.components.register('time-sheet-link', module.exports);
}

