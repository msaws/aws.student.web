﻿/* Copyright 2016-2017 Ellucian Company L.P. and its affiliates. */
var markup = require('./_ActionBar.html'),
    timeSheetEvents = require("TimeManagement/TimeSheet/time.sheet.events"),
    resources = require("Site/resource.manager"),
    errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");

require("./_ActionBar.scss");
require("Site/Components/Dropdown/_Dropdown");
require("Site/Components/AutoSave/_AutoSave");

/*
 * parent component(s): TimecardCollection
 * params: {      
 *  isMobile - observable - indicating if we're in mobile mode
 *  mobileDaySelectorDates: array of Dates used in by the mobile day selector,
 *  selectedDate: observable - the day selected by the mobile day selector - this component writes to this observable,
 *  previousCollectionDateRange - object or observable (optional): { startDate: date, endDate: date, periodStartDate: date, periodEndDate: date } 
 *  nextCollectionDateRange = object or observable (options): {startDate: date, endDate: date, periodStartDate: date, periodEndDate: date}
 * }
*/
function ActionBarViewModel(params) {
    var self = this;

    if (!params) {
        console.warn("params are required in ActionBarViewModel");
    }
    if (!params.isMobile || !ko.isObservable(params.isMobile)) {
        console.warn("params.isMobile is required and must be an observable");
    }
    if (!params.mobileDaySelectorDates) {
        console.warn("params.mobileDaySelectorDates is required");
    }
    if (!params.selectedDate || !ko.isObservable(params.selectedDate)) {
        console.warn("params.selectedDate is required and must be an observable");
    }

    self.previousHref = ko.pureComputed(function () {
        let range = ko.utils.unwrapObservable(params.previousCollectionDateRange);
        if (range) {
            let href = window.location.hash.replace(/s=(.*)&e=([^\n&]*)/, "s={0}&e={1}".format(Globalize.format(range.startDate, "d"), Globalize.format(range.endDate, "d")));
            href = href.replace(/pps=(.*)&ppe=([^\n&]*)/, "pps={0}&ppe={1}".format(Globalize.format(range.periodStartDate, "d"), Globalize.format(range.periodEndDate, "d")));
            return href;
        }
        return null;
    });
    self.isPreviousTimecardEnabled = ko.pureComputed(function () {
        return self.previousHref() !== null;
    });

    self.nextHref = ko.pureComputed(function () {
        let range = ko.utils.unwrapObservable(params.nextCollectionDateRange);
        if (range) {
            let href = window.location.hash.replace(/s=(.*)&e=([^\n&]*)/, "s={0}&e={1}".format(Globalize.format(range.startDate, "d"), Globalize.format(range.endDate, "d")));
            href = href.replace(/pps=(.*)&ppe=([^\n&]*)/, "pps={0}&ppe={1}".format(Globalize.format(range.periodStartDate, "d"), Globalize.format(range.periodEndDate, "d")));
            return href;
        }
        return null;
    });
    self.isNextTimecardEnabled = ko.pureComputed(function () {
        return self.nextHref() !== null;
    });

    let totalHours = ko.observable(0).subscribeTo(timeSheetEvents.TIMESHEET_TOTALHOURSUPDATE);

    self.weekDisplay = ko.pureComputed(function () {
        let startDate = Globalize.format(params.mobileDaySelectorDates()[0], 'd');
        let endDate = Globalize.format(params.mobileDaySelectorDates()[params.mobileDaySelectorDates().length - 1], 'd');
        return "Week {0} - {1}".format(startDate, endDate);
    });

    self.nextWeekDisplay = ko.pureComputed(function () {
        let startDate = Globalize.format(new Date(params.mobileDaySelectorDates()[0]).addDays(7), 'd');
        let endDate = Globalize.format(new Date(params.mobileDaySelectorDates()[params.mobileDaySelectorDates().length - 1]).addDays(7), 'd');
        return "Week {0} - {1}".format(startDate, endDate);
    });
    self.previousWeekDisplay = ko.pureComputed(function () {
        let startDate = Globalize.format(new Date(params.mobileDaySelectorDates()[0]).addDays(-7), 'd');
        let endDate = Globalize.format(new Date(params.mobileDaySelectorDates()[params.mobileDaySelectorDates().length - 1]).addDays(-7), 'd');
        return "Week {0} - {1}".format(startDate, endDate);
    });

    let totalHoursDisplayLabel = resources.getObservable('TimeSheet.TimecardCollectionTotalHoursLabel');
    self.totalHoursDisplay = ko.computed(function () {
        if (totalHoursDisplayLabel()) {
            return totalHoursDisplayLabel().format(totalHours().toFixed(2));
        }
    });

    self.previousWeekButtonTitle = ko.pureComputed(function () {
        return 'navigate to previous week of ' + self.previousWeekDisplay();
    });
    self.nextWeekButtonTitle = ko.pureComputed(function () {
        return 'navigate to next week of ' + self.nextWeekDisplay();
    });;


    //subscribe to the TIMESHEET_MESSAGES postbox topic
    let timeSheetErrors = ko.observable([]).subscribeTo(timeSheetEvents.TIMESHEET_MESSAGES, true);

    //then compute a set of error messages based on those errors.
    self.timeSheetErrorMessages = ko.computed(function () {
        let messages = timeSheetErrors().filter(function (err) {
            return err.type === errorDefinitions.types.error;
        })
        .map(function (err) {
            return Globalize.format(err.date, "dddd") + " - " + err.message;
        });
        return messages;
    });

    //compute a set of warning messages
    self.timeSheetWarningMessages = ko.computed(function () {
        let messages = timeSheetErrors().filter(function (err) {
            return err.type === errorDefinitions.types.warning;
        })
        .map(function (err) {
            return Globalize.format(err.date, "dddd") + " - " + err.message;
        });
        return messages;
    });

    //compute a single notification message to display on mobile
    self.mobileNotificationMessage = ko.computed(function () {
        if (self.timeSheetErrorMessages().length > 0) {
            return {
                type: errorDefinitions.types.error, message: self.timeSheetErrorMessages()[self.timeSheetErrorMessages().length - 1]
            };
        }
        else if (self.timeSheetWarningMessages().length > 0) {
            return {
                type: errorDefinitions.types.warning, message: self.timeSheetWarningMessages()[self.timeSheetWarningMessages().length - 1]
            };
        }
        return null;
    })


    self.isMobile = params.isMobile;

    self.selectedDate = params.selectedDate;
    self.mobileDaySelectorDates = params.mobileDaySelectorDates;
    self.getDayPickerString = function (date) {
        return Globalize.format(date, "dddd") + " " + Globalize.format(date, "M/d");
    }

    self.postProcessDropdown = function () {
        if (self.isMobile()) {
            $(".mobile-select-day .esg-button-group").addClass("esg-button-group--fluid");
        }
    }

    //debounce function used for the window scroll event to improve performance
    function debounce(callback, timeout) {
        var timeoutInstance;
        return function () {
            //investigate clearTimeout
            clearTimeout(timeoutInstance);
            timeoutInstance = ko.utils.setTimeout(callback, timeout);
        };
    }

    $(document).ready(function () {

        //get the initial width of the action bar
        var actionBar = $(".time-sheet-action-bar"),
            phantomBar = $(".phantom-time-sheet-action-bar"),
            initialPos = actionBar.position(),
            isProxy = $("#proxy-banner").length,
            heightAdjustment = self.isMobile() ? 100 : 60;

        phantomBar.hide();

        //register a function to fire on the scroll event so we can 
        //pin the action bar to the top of the page
        $(window).on("scroll.actionbar touchmove.actionbar", debounce(function () {

            if (!isProxy) {
                var windowpos = $(window).scrollTop(),

                      actionBarWidth = $('.timecard-collection-section').innerWidth();
                if (windowpos >= initialPos.top + heightAdjustment) {
                    phantomBar.css("height", actionBar.height());
                    actionBar.addClass("stick");
                    actionBar.css("width", actionBarWidth);
                    phantomBar.show();
                } else {
                    actionBar.removeClass("stick");
                    actionBar.css("width", "auto");
                    phantomBar.hide();
                }
            }

        }, 10))
    });


    self.dispose = function () {
        $(window).off("scroll.actionbar touchmove.actionbar");
        timeSheetErrors.unsubscribeFrom(timeSheetEvents.TIMESHEET_MESSAGES);
        self.timeSheetErrorMessages.dispose();
        totalHours.unsubscribeFrom(timeSheetEvents.TIMESHEET_TOTALHOURSUPDATE);
        self.totalHoursDisplay.dispose();
    }
}

module.exports = { viewModel: ActionBarViewModel, template: markup };

if (!ko.components.isRegistered('action-bar')) {
    ko.components.register('action-bar', module.exports);
}

