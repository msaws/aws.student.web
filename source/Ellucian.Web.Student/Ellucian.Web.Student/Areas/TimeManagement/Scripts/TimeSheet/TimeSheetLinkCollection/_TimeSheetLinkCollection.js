﻿var markup = require('./_TimeSheetLinkCollection.html');
var actions = require('TimeManagement/TimeSheet/time.sheet.actions');
var dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping');
var resources = require('Site/resource.manager');

require('TimeManagement/TimeSheet/TimeSheetLink/_TimeSheetLink');
/*
 * PARAMETERS: {
 *      payCycleId: the payCycleId of the collection
 * }
 */
function TimeSheetLinkCollectionViewModel(params) {
    var self = this;

    self.payCycleId = params.payCycleId;
    self.timePeriodsAreLoaded = ko.observable(false);
    self.loadError = ko.observable(false);

    function loadTimePeriods() {
        self.timePeriodsAreLoaded(false);
        self.loadError(false);
        return actions.GetTimePeriodsAsync(self.payCycleId()).then(function (timePeriods) {
            ko.mapping.fromJS(timePeriods, dataMapping, self);
            self.timePeriodsAreLoaded(true);
            return;
        }).catch(function (err) {
            console.error(err);
            self.loadError(true).isLoaded(true);
            return;
        });
    };
    loadTimePeriods();

    var sub = self.payCycleId.subscribe(loadTimePeriods);

    self.dispose = function () {
        sub.dispose();
    }
}


module.exports = { viewModel: TimeSheetLinkCollectionViewModel, template: markup };

if (!ko.components.isRegistered('time-sheet-link-collection')) {
    ko.components.register('time-sheet-link-collection', module.exports);
}

