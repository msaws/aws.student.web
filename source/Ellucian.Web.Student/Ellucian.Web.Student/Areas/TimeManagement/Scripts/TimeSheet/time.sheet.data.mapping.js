﻿var mapping = {
    "Timecards": {
        key: function (data) {
            return ko.utils.unwrapObservable(data.ModelId);
        }
    },
    "WorkEntryList": {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        }
    },
    "DailyTimeEntryGroupList" : {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        }
    },
    "DailyTimeEntryList": {
        key: function (data) {
            return ko.utils.unwrapObservable(data.DateWorked).getTime() + ko.utils.unwrapObservable(data.Id);
        }
    },
};

module.exports = mapping;