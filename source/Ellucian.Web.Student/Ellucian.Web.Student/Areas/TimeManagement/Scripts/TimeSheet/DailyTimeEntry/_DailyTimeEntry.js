﻿//Copyright 2015 Ellucian Company L.P. and its affiliates.
var markup = require('./_DailyTimeEntry.html'),
    mapper = require("TimeManagement/Modules/model.view.model.merge"),
    resources = require('Site/resource.manager'),
    errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");

require('./_DailyTimeEntry.scss');

/*
 * parent component(s): DailyTimeEntryRow
 * params = {
 *  dailyTimeEntryModel: the JSON data model, mapped to observables,
 *  isReadOnly - observable - indicating whether the daily time entry can be modified by the user
 *  onHoursWorkedChange: Function: callback invoked when hoursWorked is changed
 *                          Args: dailyTimeEntryModel - the data model that was updated
 * }
 */
function DailyTimeEntryViewModel(params) {
    var self = this;

    //argument verification
    if (!params) {
        throw new Error("params are required");
    }
    if (params.onHoursWorkedChange && typeof params.onHoursWorkedChange !== 'function') {
        throw new Error("params.onHoursWorkedChange must be a function");
    }
    if (params.isReadOnly && !ko.isObservable(params.isReadOnly)) {
        throw new Error("params.isReadOnly must be an observable");
    }

    mapper(params.dailyTimeEntryModel, self);

    // params.isReadOnly is based on the timecard's status
    // IsReadOnly from the server model is based on Date Worked in relation to the pay period start/end dates
    // if either of these is true, then set self.isReadOnly to true
    self.isReadOnly = ko.pureComputed(function () {
        return params.isReadOnly() || self.IsReadOnly();
    });

    //determines if the totalDayHours error key is in the message channel
    self.hasTotalTimeError = ko.computed(function () {
        return self.MessageChannel().indexOf(errorDefinitions.keys.totalDayTimeError) >= 0;
    });

    //computes summary error based on all possible errors
    self.hasInputError = ko.computed(function () {

        if (self.isReadOnly()) return false;

        if (self.hasTotalTimeError()) return true;

        return false;
    });



    /*note that updating the HoursWorked is broken up into separate functions
     * 1. formattedHours intercepts user input, normalizes it to null or a number, and writes to hoursWorkedBacker
     * 2. when hoursWorkedBacker changes, call updateHoursWorked.
     * 3. updateHoursWorked does 'business-logic' needed to modify the data model depending on the situation, and updates HoursWorked accordingly
     * 4. when HoursWorked changes, invoke the callback.
     * 
     * multiple levels are needed to facilitate detecting changes on normalized user input (hoursWorkedBacker), 
     * and then to detect changes in the data model (HoursWorked).
     */

    //function that updates the model based on whether HoursWorked was added, changed or deleted
    //invokes the onHoursWorkedChange call back at the end
    function updateHoursWorked(newValue) {
        if (isNaN(newValue)) {
            throw new Error("hours Worked Must be a number");
        }

        newValue = (newValue) ? parseFloat(newValue) : null;
        if (self.HoursWorked() != newValue) {

            //time input has an existing id :: either change or delete
            if (self.Id() !== null && self.Id() > 0) {
                if (newValue === null) {
                    //if newValue null :: this is a delete
                    self.Id(null);
                    self.HoursWorked(null);

                } else if (newValue !== self.HoursWorked()) {
                    //this is a change
                    self.HoursWorked(newValue);
                }
            } else {
                //time input does not have an id :: this is an add
                if (newValue !== self.HoursWorked()) {
                    self.HoursWorked(newValue);
                }
            }

            //invoke the callback only if there are no errors
            if (params.onHoursWorkedChange instanceof Function && !self.hasInputError()) {
                return params.onHoursWorkedChange(self);
            }            
        }
        return Promise.resolve();
    }

    //writeable computed that updates the hours worked
    var hoursWorkedBacker = ko.observable(null).extend({ notify: 'always' });
    self.formattedHours = ko.pureComputed({
        read: function () {
            return hoursWorkedBacker();
        },
        write: function (value) {
            if (value === null || value === "") {
                hoursWorkedBacker(null);
            } else {
                var stringValue = value.toString();
                var temp = parseFloat(stringValue.replace(/[^\.\d]/g, ""));
                hoursWorkedBacker(isNaN(temp) ? "0" : temp.toFixed(2));
            }
        }
    }).extend({ notify: 'always' });

    //initialize formatted hours to be the hours Worked
    self.formattedHours(self.HoursWorked());

    //when the backer change value, call the updateHoursWorked function
    var backerSubscription = hoursWorkedBacker.subscribe(updateHoursWorked);

    // id applied to the input
    self.hoursInputId = Math.random().toString().split('.')[1];
    self.hoursInputLabelText = ko.pureComputed(function () {
        let formattedDate = Globalize.format(self.DateWorked(), 'ddd') + ' ' + Globalize.format(self.DateWorked(), 'M/d');
        return formattedDate + ', ' + self.formattedHours() + ' hours';
    });

    self.dispose = function () {
        backerSubscription.dispose();
        self.hasInputError.dispose();
        self.hasTotalTimeError.dispose();
    }
}

module.exports = { viewModel: DailyTimeEntryViewModel, template: markup };

if (!ko.components.isRegistered('daily-time-entry')) {
    ko.components.register('daily-time-entry', module.exports);
}