﻿var markup = require('./_TimeSheetViewComponent.html'),
    actions = require('TimeManagement/TimeSheet/time.sheet.actions'),
    dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping');


require('TimeManagement/TimeSheet/TimecardCollection/_TimecardCollection');

/*
 * parent component(s): none
 * Params: {
 *      startDate: string date representation,
 *      endDate: string date representation,
 *      payCycleId: id of the paycycle for these time sheets,
 *      periodStartDate: string date representation of the startdate of the pay period for these timesheets
 *      periodEndDate: string date representation of the enddate of the pay period for these timesheets.
 * }
 *  
 */
function TimeSheetViewComponent(params) {
    var self = this;

    self.dataModel = {};

    actions.GetTimesheetDataAsync(params.startDate, params.endDate, params.payCycleId).then(function (timeSheetRawJSON) {
        self.dataModel = ko.mapping.fromJS(timeSheetRawJSON, dataMapping);
        self.isLoaded(true);
    }).catch(function (err) {
        console.error(err);
        self.isLoaded(true).loadError(true);
    });

    self.pageHeaderDisplay = ko.pureComputed(function () {
        let startDate = Globalize.format(params.periodStartDate, 'd');
        let endDate = Globalize.format(params.periodEndDate, 'd');
        return "Pay Period {0} - {1}".format(startDate, endDate);
    });
    self.pageHeaderDisplayMobile = ko.pureComputed(function () {
        let startDate = Globalize.format(params.startDate, 'd');
        let endDate = Globalize.format(params.endDate, 'd');
        return "Week of {0} - {1}".format(startDate, endDate);
    });
    self.backLinkHref = ko.pureComputed(function () {
        return "#p={0}".format(params.payCycleId);
    });

    self.isLoaded = ko.observable(false);

    self.loadError = ko.observable(false);
}



module.exports = { viewModel: TimeSheetViewComponent, template: markup };

if (!ko.components.isRegistered('time-sheet-view')) {
    ko.components.register('time-sheet-view', module.exports);
}
