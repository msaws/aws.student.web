﻿var markup = require('./_HomeViewComponent.html');
var actions = require('TimeManagement/TimeSheet/time.sheet.actions');
var dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping');
var resources = require('Site/resource.manager');

require('TimeManagement/TimeSheet/TimeSheetLinkCollection/_TimeSheetLinkCollection');

var payCycleCache = [];

/*
 * parent component(s): none
 * params: {
 *      payCycleId: the identifier of the payCycle for which to load data
 * }
 */
function HomeViewComponentModel(params) {
    var self = this;

    params = params || {};

    self.payCycleId = ko.observable(params.payCycleId || null);

    self.PayCycles = [];

    self.formatPayCycleHash = function (payCycle) {
        return '#p=' + payCycle.Id;
    }
    self.isSelected = function (payCycle) {
        return payCycle.Id == self.payCycleId();
    }

    var payCyclePromise;
    if (payCycleCache.length > 0) {
        payCyclePromise = Promise.resolve(payCycleCache);
    }
    else {
        payCyclePromise = actions.GetPayCyclesAsync();
    }

    self.loadError = ko.observable(false);
    self.payCyclesAreLoaded = ko.observable(false);

    payCyclePromise.then(function (payCycles) {
        if (payCycles.length === 0) {
            throw new Error('No available or historical paycycles to choose from');
        }

        payCycleCache = payCycles;
         
         // alphabetically sort the pay cycles by id
        if(payCycles.length > 1){
             self.PayCycles = payCycles.sort(function (a, b) {
                  return a.Id > b.Id
             });
        }
        else{
             self.PayCycles = payCycles;
        }

        // have the 'default' payCycleId be the first if none has been provided
        if (!self.payCycleId())
            self.payCycleId(self.PayCycles[0].Id);
        return self.payCyclesAreLoaded(true);
    }).catch(function (err) {
        console.error(err);
        self.loadError(true).isLoaded(true);
        return;
    });




    self.dispose = function () {

    }
}

module.exports = { viewModel: HomeViewComponentModel, template: markup };

if (!ko.components.isRegistered('home-view')) {
    ko.components.register('home-view', module.exports);
}


