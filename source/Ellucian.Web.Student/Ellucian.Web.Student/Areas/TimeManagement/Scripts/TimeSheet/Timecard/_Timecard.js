﻿var markup = require('./_Timecard.html'),
    autoSaveEvents = require('Site/Components/AutoSave/_AutoSaveEvents'),
    dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping'),
    actions = require('TimeManagement/TimeSheet/time.sheet.actions'),
    timeSheetEvents = require('TimeManagement/TimeSheet/time.sheet.events'),
    resources = require('Site/resource.manager'),
    mapper = require("TimeManagement/Modules/model.view.model.merge"),
    status = require('TimeManagement/Modules/submission.status'),
    errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");

require("./_Timecard.scss");

require('TimeManagement/TimeSheet/DailyTimeEntryRow/_DailyTimeEntryRow');
require('TimeManagement/Components/CommentsContainer/_CommentsContainer');


/*
 * parent component(s): TimecardCollection
 * params = {
 *  timecardModel: the JSON data model, mapped to observables,
 *  days: observableArray - the days in the week
 *  selectedDayObservable: an observable - not the value - of the day selected to view in a small size timecard
 *  employeeName: the name of the context employee
 * }
 */
function TimecardViewModel(params) {
    var self = this;

    if (!params) {
        console.warn("TimecardViewModel requires params");
    }
    if (!params.timecardModel) {
        console.warn("TimecardViewModel requires a timecard data model - params.timecardModel");
    }
    if (!params.selectedDayObservable || !ko.isObservable(params.selectedDayObservable)) {
        console.warn("params.selectedDayObservable is required to function on small devices and must be an observable instance. ")
    }


    mapper(params.timecardModel, self);

    self.days = params.days;

    self.employeeName = params.employeeName;

    // the display name of the employee in the comments dialog box
    self.employeeCommentName = ko.computed(function () {
        var ary = self.employeeName().split(" ");
        switch (ary.length) {
            case 1:
                return ary[0];
                break;
            case 2:
                return self.employeeName();
                break;
            case 3:
                return ary[0] + " " + ary[2];
                break;
            default:
                return self.employeeName();
                break;
        }

    });

    /*
     * subscribe to the TIMESHEET_MESSAGES postbox event to get updates
     * when the timesheet errors are updated. 
     * Filter errors and warnings by this timecardId to determine if this
     * timecard has errors or warnings.
     */
    self.hasErrors = ko.observable(false).subscribeTo(timeSheetEvents.TIMESHEET_MESSAGES, true, function (messages) {
        let timecardErrors = messages.filter(function (errorObj) {
            return errorObj.timecardId === self.TimecardId() && errorObj.type === errorDefinitions.types.error;
        });
        return timecardErrors.length > 0;
    });

    self.hasWarnings = ko.observable(false).subscribeTo(timeSheetEvents.TIMESHEET_MESSAGES, true, function (messages) {
        let timecardErrors = messages.filter(function (errorObj) {
            return errorObj.timecardId === self.TimecardId() && errorObj.type === errorDefinitions.types.warning;
        });
        return timecardErrors.length > 0;
    });


    self.workTypeChangeHandler = function (workTypeEntryModel) {
        //mcd - no changes are being made to the work type, so commenting this out for now.
        //if (workTypeEntryModel) {
        //    //ensure that the the workTypeEntryModel contains at least one HoursWorked value
        //    var updateFlag = ko.utils.arrayFirst(workTypeEntryModel.DailyTimeEntryList, function (item) {
        //        return ko.unwrap(item.HoursWorked);
        //    });
        //    if (updateFlag) saveThisTimecardAsync();
        //}
    }

    self.dailyTimeEntryChangeHandler = function (workTypeId, dailyTimeEntryGroupModel) {
        if (workTypeId && !self.hasErrors()) {
            if (self.TimecardId()) {
                latestSaveTask(saveTimecardTaskFactory());
            }
            else if (latestSaveTask() == null) {
                latestSaveTask(createTimecardTaskFactory());
            }
            else {                
                shouldSaveAfterTimecardIdUpdate(true);
            }
        }
    }

    self.commentsModel = ko.computed(function () {
        return {
            positionCommentsArray: [
                {
                    PositionId: self.PositionId(),
                    PositionTitle: self.PositionTitle(),
                    TimecardId: self.TimecardId(),
                    Comments: self.Comments(),
                    PayPeriodEndDate: self.PeriodEndDate(),
                    PayCycleId: self.PayCycleId()
                }
            ]
        }
    });

    var saveCommentWithNewTimecardIdSubscription = null;
    self.newCommentHandler = function (comment) {
        if (comment) {
            comment.EmployeeId = self.EmployeeId()

            if (!comment.TimecardId) {      // note bhr:  since the absence of data implies a timecard has no error conditions
                //saveThisTimecardAsync();    // we can therefor forego the error check.  No unsaveable errors can happen from single inputs at this time
                var saveTimecardTask = saveTimecardTaskFactory();
                return saveTimecardTask().then(function () {
                    comment.TimecardId = self.TimecardId();
                    return saveThisCommentAsync(comment);
                });

            } else {
                return saveThisCommentAsync(comment);
            }
        }
        return Promise.resolve();
    }

    function saveThisCommentAsync(comment) {

        ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVESTART);

        return actions.CreateTimeEntryCommentsAsync(comment).then(function (responseCommentJSON) {
            var mappedComments = ko.mapping.fromJS(responseCommentJSON);
            self.Comments.push(mappedComments);
            ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVECOMPLETE);
            return;
        }).catch(function (e) {
            console.log(e);
            ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVEERROR);
            return;
        });
    }


    function saveTimecardTaskFactory() {
        var saveActionAsync = function () {

            if (self.hasErrors()) {
                return Promise.resolve();
            }

            return actions.SaveTimecardAsync(self).catch(function (err) {
                console.log(err);
                ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVEERROR);
                return;
            });
 
        }
        return saveActionAsync;
    }

    function createTimecardTaskFactory() {
        var createActionAsync = function () {
            if (self.hasErrors()) {
                return Promise.resolve();
            }

            return actions.CreateTimecardAsync(self).then(function (timecardId) {
                self.TimecardId(timecardId);
                return;
            }).catch(function (err) {
                console.log(err);
                ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVEERROR);
                return;
            });
        }
        return createActionAsync;
    }


    var latestSaveTask = ko.observable(null).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 3000 } });
    var rateLimitedSaveSubscription = latestSaveTask.subscribe(function (saveAction) {
       
        if (saveAction) {
            ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVESTART);
            return saveAction().then(function () {
                ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVECOMPLETE);
                ko.postbox.publish(timeSheetEvents.TIMESHEET_SAVECOMPLETE, {
                    startDate: self.days()[0],
                    endDate: self.days()[6],
                    payCycleId: self.PayCycleId(),
                });
                return;
            });
        }
        return;
    });
    
    var shouldSaveAfterTimecardIdUpdate = ko.observable(false);
    var doSaveAfterTimecardIdUpdate = ko.computed(function () {
        var timecardId = self.TimecardId(),
            shouldSave = shouldSaveAfterTimecardIdUpdate();

        if (timecardId && shouldSave) {
            latestSaveTask(saveTimecardTaskFactory());
        }
    });

    self.isPaid = ko.computed(function () {
        if (status.get(self.TimecardStatus()) === status.paid) {
            return true;
        }
        return false;
    });

    self.statusIconClass = ko.computed(function () {
        if (self.isPaid() === true) {
            return status.paid.statusClass;
        }
        return "";
    })

    self.paidLabelText = ko.observable(status.paid.name);

    /*
     * when true - indicates that the user initiated a status submit and the application
     * is waiting on a response from the server.
     * when false - indicates that the server responded, or that the button was never clicked
     */
    self.isUpdatingStatus = ko.observable(false);

    /*
     * click handler for the submit timecard button
     */
    self.submitTimecardStatusAsync = function () {
        //only execute the save if there are no errors or the status is submitted (so the user can unsubmit)
        let currentStatus = status.get(self.TimecardStatus());
        if (!self.hasErrors() || currentStatus == status.submitted) {
            self.isUpdatingStatus(true);
            var startTime = Date.now();
            var nextStatus = getNextStatus();

            /*
             * a wrapper around the submitTimecardStatusAsync action
             */
            var submitActionAsync = function () {
                return actions.SubmitTimecardStatusAsync(nextStatus.code, self.TimecardId()).then(function (newStatus) {
                    var endTime = Date.now();

                    if (endTime - startTime > 2000) {
                        self.isUpdatingStatus(false);
                        self.TimecardStatus(newStatus);
                    } else {
                        setTimeout(function () {
                            self.isUpdatingStatus(false);
                            self.TimecardStatus(newStatus);
                        }, (2000 - (endTime - startTime)));
                    }
                    return;
                }).catch(function (err) {
                    console.log(err);
                    self.isUpdatingStatus(false);
                    return;
                });
            }

            //if there's an awaiting saveTimecard task, execute it, and attach the submitStatus task
            //to its resolve handler.
            var saveTimecardTask = latestSaveTask();
            if (saveTimecardTask) {
                latestSaveTask(null);
                return saveTimecardTask().then(function () {
                    return submitActionAsync();
                });
            }
            else {
                return submitActionAsync();
            }
        }

    }

    // todo refactor to use status module
    var getNextStatus = ko.computed(function () {
        var currentStatus = status.get(self.TimecardStatus());
        switch (currentStatus) {
            case status.none:
                return status.submitted;
            case status.submitted:
                return status.unsubmitted;
            case status.unsubmitted:
                return status.submitted;
            case status.rejected:
                return status.submitted;
            case status.approved:
                return status.approved;
            case status.paid:
                return status.paid;

            default:
                throw new Error("Unknown timecard status " + currentStatus);
        }
    });

    self.isReadOnly = ko.pureComputed(function () {
        var currentStatus = status.get(self.TimecardStatus());

        if (self.IsForHistoricalPayPeriod() &&
            (currentStatus == status.none ||
             currentStatus == status.unsubmitted ||
             currentStatus == status.rejected
            )) { // historical pay periods should be editable
            return false;
        } else if (Date.Compare(new Date(), self.EmployeeSubmitDeadline()) == 1) {
            return true;
        }

        switch (currentStatus) {

            case status.none:
                return false;

            case status.submitted:
                return true;

            case status.unsubmitted:
                return false;

            case status.rejected:
                return false;

            case status.approved:
                return true;

            case status.paid:
                return true;
            default:
                return true;
        }
    })

    //submit is prevented when today is past the employee submit deadline,
    //when the current status is approved or paid
    //when the timecard doesn't have an id (it's new, hasn't been created yet)
    //when the timecard has errors
    self.isSubmitPrevented = ko.computed(function () {
        var currentStatus = status.get(self.TimecardStatus());
        if (Date.Compare(new Date(), self.EmployeeSubmitDeadline()) == 1 ||
            currentStatus == status.approved ||
            currentStatus == status.paid ||
            !self.TimecardId() ||
            (self.hasErrors() && currentStatus != status.submitted) ||
            (self.hasWarnings() && currentStatus != status.submitted)
        ) {
            return true;
        }
        return false;
    });

    self.submitButtonTextResx = ko.pureComputed(function () {
        switch (getNextStatus()) {
            case status.none:
                return "TimeSheet.TimecardStatusButtonSubmitLabel";
            case status.submitted:
                return "TimeSheet.TimecardStatusButtonSubmitLabel";
            case status.unsubmitted:
                return "TimeSheet.TimecardStatusButtonUnsubmitLabel";
            case status.rejected:
                return "TimeSheet.TimecardStatusButtonSubmitLabel";
            default:
                return "";
        }
    });

    self.submitLoaderText = ko.computed(function () {
        self.isUpdatingStatus(); //register dependency so that this gets recomputed before modal dialog
        switch (getNextStatus()) {
            case status.none:
                return resources.getObservable("TimeSheet.TimecardStatusButtonSubmitLabel", [self.PositionId()]);
            case status.submitted:
                return resources.getObservable("TimeSheet.TimecardStatusLoaderSubmitLabel", [self.PositionId()])();
            case status.unsubmitted:
                return resources.getObservable("TimeSheet.TimecardStatusLoaderUnsubmitLabel", [self.PositionId()])();
            case status.rejected:
                return resources.getObservable("TimeSheet.TimecardStatusLoaderSubmitLabel", [self.PositionId()])();

        }
    });

    self.showSubmitButton = ko.pureComputed(function () {
        return (self.TimecardStatus() == 3 || self.TimecardStatus() == 4) ? false : true;
    });

    //not being used at the moment, commenting out so we don't ahve to detail with data model changes for code not being used
    //self.addWorkTypeEntry = function () {
    //    var dailyTimeEntryList = self.WorkEntryList()[0].DailyTimeEntryList().map(function (dailyTimeEntryModel, index) {
    //        return {
    //            Id: "",
    //            HoursWorked: null,
    //            DayIndex: index,
    //            DateWorked: dailyTimeEntryModel.DateWorked(),
    //            IsReadOnly: dailyTimeEntryModel.IsReadOnly()
    //        }
    //    });

    //    var workEntryJSON = { Id: self.WorkEntryList().length.toString(), EarningsTypeId: null, DailyTimeEntryList: dailyTimeEntryList };
    //    var model = ko.mapping.fromJS(workEntryJSON, dataMapping);

    //    self.WorkEntryList.push(model);
    //}


    self.daySelected = params.selectedDayObservable;


    self.dailyTotals = ko.pureComputed(function () {
        var dailyHoursArray = [];
        self.isReadOnly(); // to make sure self.isReadOnly is subscribed to
        self.days().forEach(function (date, index) {
            var time = self.WorkEntryList().reduce(function (dayIndexTime, workEntry) {
                return dayIndexTime += workEntry.DailyTimeEntryGroupList()[index].DailyTimeEntryList().reduce(function (groupTime, timeEntry) {
                    return groupTime += timeEntry.HoursWorked();
                }, 0);
            }, 0);
            time = !isNaN(time) ? time : 0.00;
            var dayString = Globalize.format(date, "ddd") + " " + Globalize.format(date, "M/d");
            var isOutsideTimecardDates = (Date.Compare(self.StartDate(), date) == 1) || (Date.Compare(date, self.EndDate()) == 1);
            dailyHoursArray.push({ dayString: dayString, dailyTotalHours: time, isReadOnly: isOutsideTimecardDates || self.isReadOnly() })
        });
        return dailyHoursArray;
    });

    self.weeklyTotalHours = ko.pureComputed(function () {
        return self.dailyTotals().reduce(function (weeklyTime, dailyTotalsObj) {
            return weeklyTime += dailyTotalsObj.dailyTotalHours;
        }, 0)
    });

    self.dispose = function () {
        self.employeeCommentName.dispose();
        self.commentsModel.dispose();
        self.hasErrors.unsubscribeFrom(timeSheetEvents.TIMESHEET_MESSAGES);
        self.hasWarnings.unsubscribeFrom(timeSheetEvents.TIMESHEET_MESSAGES);
        self.isPaid.dispose();
        self.statusIconClass.dispose();
        getNextStatus.dispose();
        self.submitLoaderText.dispose();
        self.isSubmitPrevented.dispose();
        rateLimitedSaveSubscription.dispose();
        doSaveAfterTimecardIdUpdate.dispose();
    }

}


module.exports = { viewModel: TimecardViewModel, template: markup };

if (!ko.components.isRegistered('timecard')) {
    ko.components.register('timecard', module.exports);
}

