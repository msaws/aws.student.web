﻿var dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping');

var applicationActions = {
    GetTimePeriodsAsync: function (payCycleId) {
        return getTimePeriodsAsync(payCycleId);
    },
    GetPayCyclesAsync: function() {
        return getPayCyclesAsync();
    },

    SaveTimecardAsync: function (timecardDataModel) {
        if (!timecardDataModel) {
            throw new Error("SaveTimecardAsync Action creator requires timecard Data model");
        }
        if (!ko.unwrap(timecardDataModel.TimecardId)) {
            throw new Error("SaveTimecardAsync Action creator requires the timecard id");
        }
        return updateTimecardAsync(timecardDataModel);

    },
    CreateTimecardAsync: function (timecardDataModel) {
        if (!timecardDataModel) {
            throw new Error("CreateTimecardAsync Action creator requires timecard Data model");
        }
        if (ko.unwrap(timecardDataModel.TimecardId)) {
            throw new Error("CreateTimecardAsync Action creator requires that the timecard id is null or empty");
        }
        return createTimecardAsync(timecardDataModel).then(function (responseModel) {
            //responseModel contains the new timecard id, and the pay period identifiers

            //asynchronously create the timecards for the rest of the pay period, without waiting for them to finish.
            createPayPeriodTimecardsAsync(responseModel.PeriodStartDate, responseModel.PeriodEndDate, responseModel.PayCycleId, responseModel.PositionId);
            return responseModel.Id;
        });
    },

    GetTimesheetDataAsync: function (startDate, endDate, payCycleId) {
        if (!startDate || !endDate || !payCycleId) {
            console.warn("GetTimeSheetData requires a start date, end date and pay cycle id");
        }
        return getTimeSheetDataAsync(startDate, endDate, payCycleId);
    },



    SubmitTimecardStatusAsync: function (newStatusCode, timecardId) {
        if (!timecardId) {
            console.warn("SubmitTimecardStatusAsync requires a timecardId");
        }
        var status = {
            "ActionType": newStatusCode,
            "TimecardId": timecardId,
            "ActionComments": ""
        };
        return submitTimecardStatusAsync(status);
    },

    CalculateOvertimeAsync: function (startDate, endDate, payCycleId) {
        if (!startDate || !endDate || !payCycleId)
            console.warn('Calculating overtime requires startDate, endDate, and paycycleId');
        var overtimeQueryCriteria = { PersonId: '', PaycycleId: payCycleId, StartDate: startDate, EndDate: endDate };
        return postOvertimeQueryCriteriaAsync(overtimeQueryCriteria);
    },

    CreateTimeEntryCommentsAsync: function (newComment) {
        if (!newComment)
            console.warn("comment object is required to post");
        return createTimeEntryCommentsAsync(newComment);
    }
};

function createTimeEntryCommentsAsync(newComment) {
    return Promise.resolve($.ajax({
        url: Ellucian.TimeSheet.ActionUrls.createTimeEntryCommentsUrl,
        type: 'POST',
        contentType: jsonContentType,
        data: JSON.stringify({ 'commentModelJson': ko.toJSON(newComment) }),
        dataType: 'json'
    })).then(function (response) {
        if (response.StatusCode === 200) {
            return response.Model;
        } else {
            throw new Error(response.Message);
        }
    })
}

function updateTimecardAsync(timecardDataModel) {

    var timecardJSON = JSON.stringify(ko.mapping.toJS(timecardDataModel), function(key, value) { 
        if (key === 'TimeStart' || key == 'TimeEnd') {
            if (value) {
                return moment(value).format();
            }
        }
        return value;
    });   

    var requestObj = {
        'timecardModelJson': timecardJSON
    }

    return Promise.resolve($.ajax({
        url: Ellucian.TimeSheet.ActionUrls.updateTimecardUrl,
        type: 'PUT',
        contentType: jsonContentType,
        dataType: 'json',
        data: JSON.stringify(requestObj)
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 204) {
            return;// responseObj.StatusCode;
        }
        else {
            throw new Error(responseObj.Message);
        }
    });
}

function createTimecardAsync(timecardDataModel) {

    var timecardJSON = JSON.stringify(ko.mapping.toJS(timecardDataModel), function (key, value) {
        if (key === 'TimeStart' || key == 'TimeEnd') {
            if (value) {
                return moment(value).format();
            }
        }
        return value;
    });

    var requestObj = {
        'timecardModelJson': timecardJSON
    }

    return Promise.resolve($.ajax({
        url: Ellucian.TimeSheet.ActionUrls.createTimecardUrl,
        type: 'POST',
        contentType: jsonContentType,
        dataType: 'json',
        data: JSON.stringify(requestObj)
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {
            return responseObj.Model;
        }
        else {
            throw new Error(responseObj.Message);
        }
    });
}


function createPayPeriodTimecardsAsync(payPeriodStartDate, payPeriodEndDate, payCycleId, positionId) {
    let start = moment(payPeriodStartDate).format(),
        end = moment(payPeriodEndDate).format()
    var query = encodeURI("?periodStartDate={0}&periodEndDate={1}&payCycleId={2}&positionId={3}".format(start, end, payCycleId, positionId));

    return Promise.resolve($.ajax({
        url: Ellucian.TimeSheet.ActionUrls.createPayPeriodTimecardsUrl + query,
        type: "POST",
        contentType: jsonContentType
    })).then(function(responseObj) {
        if (responseObj.StatusCode === 200) {
            return;
        }
        else {
            throw new Error(responseObj.Message);
        }
    });
}

function getTimePeriodsAsync(payCycleId) {
    var url = Ellucian.TimeSheet.ActionUrls.getHomeViewDataUrl + "?" +
        "payCycleId=" + payCycleId;

    return Promise.resolve($.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json"
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {
            return responseObj.Model;
        }
        else {
            throw new Error(responseObj.Message);
        }
    });
}

function getPayCyclesAsync() {
    return Promise.resolve($.ajax({
        url: Ellucian.TimeSheet.ActionUrls.getPayCyclesUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json"
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {
            return responseObj.Model;
        }
        else {
            throw new Error(responseObj.Message);
        }
    });
}

function postOvertimeQueryCriteriaAsync(overtimeQueryCriteria) {
    return Promise.resolve($.ajax({
        url: Ellucian.TimeSheet.ActionUrls.getOvertimeCalculationUrl,
        type: 'POST',
        contentType: jsonContentType,
        data: JSON.stringify({ 'overtimeQueryCriteriaJson': ko.toJSON(overtimeQueryCriteria) }),
        dataType: 'json'
    })).then(function (response) {
        if (response.StatusCode === 200) {
            return response.Model;
        } else {
            throw new Error(response.Message);
        }
    });
}

function getTimeSheetDataAsync(startDate, endDate, payCycleId) {
    var url = "{0}?startDate={1}&endDate={2}&payCycleId={3}".format(Ellucian.TimeSheet.ActionUrls.getTimeSheetDataUrl, startDate, endDate, payCycleId);


    return Promise.resolve($.ajax({
        url: url,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json"
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {
            return responseObj.Model;
        }
        else {
            throw new Error(responseObj.Message);
        }
    });
}

/*
 * Submits a timecard Status object that updates the status of the given timecard id.
 * Returns a promise that resolves to the new status of the given timecard id
 * Args
 *      status: timecard status JSON object
 *          { ActionType: 0-1, TimecardId: "id of the timecard", ActionComments: "user comments about the action" }
 */
function submitTimecardStatusAsync(status) {
    var payload = {
        "timecardStatusJson": ko.toJSON(status)
    }

    return Promise.resolve($.ajax({
        url: Ellucian.TimeSheet.ActionUrls.submitTimecardStatusUrl,
        type: "POST",
        contentType: jsonContentType,
        dataType: "json",
        data: JSON.stringify(payload)
    })).then(function (responseObj) {
        if (responseObj.StatusCode === 200) {

            // notification saying that the submission succeeded
            $('#notificationHost').notificationCenter('addNotification', { message: "Success!", type: "success", flash: true });

            //the model coming back is just the status code
            return responseObj.Model;

        }
        else {
            throw new Error(responseObj.Message);
            // notification saying that the submission failed
            $('#notificationHost').notificationCenter('addNotification', { message: "Oops, something went wrong.", type: "error", flash: true });
        }
    });
}

module.exports = applicationActions;
