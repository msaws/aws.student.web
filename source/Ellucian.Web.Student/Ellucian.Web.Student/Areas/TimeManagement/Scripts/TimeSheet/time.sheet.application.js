﻿/* Copyright 2016-2017 Ellucian Company L.P. and its affiliates. */
var TimeSheetApplicationViewModel = require('TimeManagement/TimeSheet/time.sheet.application.view.model');

require("TimeManagementContent/TimeManagement.scss")

__webpack_public_path__ = window.Ellucian.appUrl;

var applicationViewModel = new TimeSheetApplicationViewModel();

function hashChangeHandler() {
    var timeSheetViewRegex = /#s=(.*)&e=(.*)&p=(.*)&pps=(.*)&ppe=([^\n&]*)/i;
    var payCycleRegex = /#p=([^\n&]*)/i;
    if (payCycleRegex.test(window.location.hash)) {
        //if (applicationViewModel.currentViewComponentName() === 'home-view') {            
        //    applicationViewModel.currentViewComponentParams().payCycleId = payCycleRegex.exec(window.location.hash)[1].toUpperCase();
        //} else {
            applicationViewModel.currentViewComponentName('home-view');
            applicationViewModel.currentViewComponentParams({
                payCycleId: payCycleRegex.exec(window.location.hash)[1].toUpperCase()
            });
        //}
    } else if (timeSheetViewRegex.test(window.location.hash)) {
        //do time sheet view component

        var result = timeSheetViewRegex.exec(window.location.hash);

        applicationViewModel.currentViewComponentName('time-sheet-view');
        applicationViewModel.currentViewComponentParams({
            startDate: result[1],
            endDate: result[2],
            payCycleId: result[3].toUpperCase(),
            periodStartDate: result[4],
            periodEndDate: result[5]
        });
    } else { 
        //do home view component
        applicationViewModel.currentViewComponentName('home-view');
        applicationViewModel.currentViewComponentParams(null);
    }

    return;
}

$(window).on('hashchange', hashChangeHandler);

function loadApplication() {
    ko.applyBindings(applicationViewModel, document.getElementById('time-sheet-application'));

    hashChangeHandler();

    applicationViewModel.checkForMobile(window, document);
}


$(document).ready(function () {
    require("TimeManagement/TimeSheet/TimeSheetViewComponent/_TimeSheetViewComponent");
    require("TimeManagement/TimeSheet/HomeViewComponent/_HomeViewComponent");

    loadApplication();
});


if (__DEV__) {
    if (module.hot) {
        module.hot.accept();
    }
}