﻿//known dependencies that can't be injected
/*
 * BaseViewModel
 * ko
 */

function TimeSheetApplicationViewModel() {
    var self = this;


    BaseViewModel.call(self, 767);

    this.changeToMobile = function () {
        // This method should be overridden in any class that uses BaseViewModel
        //alert("Changing to mobile (override me)...");
    };

    this.changeToDesktop = function () {
        // This method should be overridden in any class that uses BaseViewModel
        //alert("Changing to desktop (override me)...");
    };


    self.isLoaded = ko.observable(false);

    self.backLinkUrl = ko.pureComputed(function () {
        if (self.currentViewComponentParams() === null) return '#';
        else if (self.currentViewComponentParams().payCycleId === null) return '#';
        else return '#p=' + self.currentViewComponentParams().payCycleId;
    })

    self.currentViewComponentName = ko.observable(null);
    self.currentViewComponentParams = ko.observable(null);
}

module.exports = TimeSheetApplicationViewModel;

