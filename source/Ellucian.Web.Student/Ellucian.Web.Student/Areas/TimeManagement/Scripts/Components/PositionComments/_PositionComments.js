﻿var markup = require("./_PositionComments.html");
var mapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");
var resources = require("Site/resource.manager");

require("./_PositionComments.scss");

//key: positionId
//value: true or false - indicates whether the position comments should be open
//var isOpenMap = {};

/*
 * parent component(s): CommentsContainer
 * params: {
 *  dataModel: - object with the following structure:
 *      {
 *          PositionId: id of position,
 *          PositionTitle: title of position,
 *          TimecardId: optional - id of the timecard this comment is related to
 *          Comments:  array of CommentModel (server) models
 *          PayPeriodEndDate: end date of the pay period to which this comment belongs
 *          PayCycleId: id of the pay cycle to which the comment belongs
 *      },
 *  newCommentHandler: function that handles new comments,
 *  authorName: name of the author writing the comment,
 *  index: this positionComments order in the parent container
 * }
 */
function PositionComments(params) {
    var self = this;

    ko.mapping.fromJS(params.dataModel, mapping, self);

    self.isOpen = ko.observable(params.index() === 0);



    //set overflow to auto on text area so ie doesn't always add scroll bar
    //set focus to text area

    self.commentCountDisplay = ko.pureComputed(function () {
        return "(" + self.Comments().length + ")";
    });

    self.collapsibleTitle = ko.pureComputed(function () {
        return self.PositionTitle() + " " + self.commentCountDisplay();
    });

    self.commentInput = ko.observable(null);

    self.enableSendButton = ko.pureComputed(function () {
        let commentInput = self.commentInput();
        return (commentInput && commentInput.length > 0)
    });

    self.sendComment = function (data, event) {
        var newComment = {
            CommentAuthorName: params.authorName,
            CommentDateTime: ko.observable(new Date()),
            CommentText: ko.observable(self.commentInput()),
            PositionId: self.PositionId,
            TimecardId: ko.unwrap(self.TimecardId),
            PayCycleId: self.PayCycleId,
            PayPeriodEndDate: self.PayPeriodEndDate
        }
        self.Comments.push(newComment);
        // the newCommentHandler function instantiates the save event
        params.newCommentHandler(newComment);
        self.commentInput(null);

        self.scrollToBottomAndFocus();

    }

    self.scrollToBottomAndFocus = function () {
        if (self.isOpen()) {
            $('#' + self.bodyId() + ' .comments-container').each(function (i, el) {
                el.scrollTop = el.scrollHeight;
            });
            $('#' + self.textAreaId()).focus();
        }
    }


    self.newCommentPlaceholder = resources.getObservable('TimeApproval.CommentsTextareaPlaceholder');

    var idKey = self.PayPeriodEndDate.peek().getTime().toString() + self.PositionId.peek();

    // note:  we are using Math.random because this element is created twice for the same payPeriod & position
    self.headerId = ko.pureComputed(function () {
        return idKey + '-comment-header' + Math.random().toString().split('.')[1];
    });
    self.bodyId = ko.pureComputed(function () {
        return idKey + '-comment-body' + Math.random().toString().split('.')[1];
    });

    self.textAreaId = ko.pureComputed(function () {
        return idKey + 'comment-textarea' + Math.random().toString().split('.')[1];
    });

    //self.dispose = function () {
    //    closeComments();
    //}
}

module.exports = { viewModel: PositionComments, template: markup };

if (!ko.components.isRegistered('position-comments')) {
    ko.components.register('position-comments', module.exports);
}
