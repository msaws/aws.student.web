﻿/* Copyright 2017 Ellucian Company L.P. and its affiliates. */
var markup = require('./_TimecardHeader.html'),
    status = require('TimeManagement/Modules/submission.status');

require("./_TimecardHeader.scss");

/*
 * params = {
 *  timecardModel: the timecard data (server, C#) model
 * }
 */
function TimecardHeaderViewModel(params) {
    var self = this;


    //get the statusObject based on the TimecardStatus value
    var statusObject = ko.pureComputed(function() {
        return status.get(params.timecardModel.TimecardStatus());
    })

    //has timecardStatus is true when the status does equal none.
    self.hasTimecardStatus = ko.pureComputed(function () {
        return statusObject() !== status.none;

    });

    //return the label of the status to be displayed
    self.timecardStatusText = ko.pureComputed(function () {
        return statusObject().statusLabel();
    });

    //return the esg classes to be applied to the label
    self.timecardStatusClass = ko.pureComputed(function() {
        return statusObject().statusClass;
    });

    //add up all the time card hours - loop through each work entry list's daily time entry list to sum up the hours.
    self.timecardHours = ko.pureComputed(function () {
        return params.timecardModel.WorkEntryList().reduce(function (timecardTotalHours, workEntry) {

            return timecardTotalHours += workEntry.DailyTimeEntryGroupList().reduce(function (groupHours, group) {
                return groupHours += group.DailyTimeEntryList().reduce(function (workEntryTotalHours, dailyEntry) {
                    return isNaN(dailyEntry.HoursWorked()) ? workEntryTotalHours : workEntryTotalHours += dailyEntry.HoursWorked();
                }, 0);
            },0)


        }, 0);
    })

}

module.exports = { viewModel: TimecardHeaderViewModel, template: markup };

if (!ko.components.isRegistered('timecard-header')) {
    ko.components.register('timecard-header', module.exports);
}
