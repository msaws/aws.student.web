﻿var markup = require("./_CommentsContainer.html");
var dataMapping = require("TimeManagement/TimeApproval/time.approval.data.mapping");
var resources = require("Site/resource.manager");

require('TimeManagement/Components/PositionComments/_PositionComments')

/*
 * parent component(s): EmployeeTimecardSummary, TimecardCollectionCarousel, Timecard
 * params = {
 *  dataModel: observable - object with the following structure 
 *  {
 *      positionCommentsArray: observable array with objects with the following structure
 *      {
 *          PositionId: id of position,
 *          PositionTitle: title of position,
 *          TimecardId: optional - id of the timecard these comments are related to
 *          Comments:  array of CommentModel (server) models
 *      },      
 * },
 * newCommentHandler: function that handles what to do with new comments
 * authorName: name of the person writing new comments
 */
function CommentsContainer(params) {
    var self = this;

    self.authorName = params.authorName;
    self.newCommentHandler = params.newCommentHandler;

    self.positionCommentsArray = ko.pureComputed(function () {
        return params.dataModel().positionCommentsArray;
    })

    self.totalComments = ko.pureComputed(function () {
        return params.dataModel().positionCommentsArray.reduce(function (numComments, positionComments) {
            return numComments + positionComments.Comments.length;
        }, 0);
    });

    self.commentsDialogIsOpen = ko.observable(false);
    self.toggleDialog = function () {
        if (self.commentsDialogIsOpen()) {
            self.commentsDialogIsOpen(false);
        } else {
            self.commentsDialogIsOpen(true);

        }
    }

    self.commentsDialogOptions = {
        autoOpen: false,
        modal: true,
        minWidth: $(window).width() / 1.7,
        closeOnEscape: true,
        draggable: true,
        dialogClass: 'position-comments-dialog',
        resizable: false,
        title: resources.getObservable('TimeApproval.EditCommentsTitle')()
    }
}

module.exports = { viewModel: CommentsContainer, template: markup };

if (!ko.components.isRegistered('comments-container')) {
    ko.components.register('comments-container', module.exports);
}
