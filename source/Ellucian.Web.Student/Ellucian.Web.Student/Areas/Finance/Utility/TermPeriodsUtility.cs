﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.Finance.Configuration;

namespace Ellucian.Web.Student.Areas.Finance.Utility
{
    /// <summary>
    /// Utility class for processing terms and financial periods
    /// </summary>
    public class TermPeriodsUtility
    {
        #region Private members and constants

        private readonly List<Term> _terms;
        private readonly List<FinancialPeriod> _periods = new List<FinancialPeriod>();

        private const string PastPeriodCode = "PAST";
        private const string CurrentPeriodCode = "CUR";
        private const string FuturePeriodCode = "FTR";

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for term/periods utility
        /// </summary>
        /// <param name="terms">List of Term objects</param>
        /// <param name="periods">List of Financial Period objects</param>
        public TermPeriodsUtility(IEnumerable<Term> terms, IEnumerable<FinancialPeriod> periods = null)
        {
            // Must have at least 1 term
            if (terms == null || terms.Count() < 1)
            {
                throw new ArgumentNullException("terms");
            }
            this._terms = new List<Term>(terms);

            // Financial periods only exist for PCF processing, so they can be null
            if (!CollectionUtility.IsNullOrEmpty(periods))
            {
                // Loop through the periods and default dates in for any null values
                foreach (var period in periods)
                {
                    if (period.Start == null)
                    {
                        period.Start = DateTime.MinValue;
                    }
                    if (period.End == null)
                    {
                        period.End = DateTime.MaxValue;
                    }
                    this._periods.Add(period);
                }
            }
        }

        #endregion

        #region Get Term

        /// <summary>
        /// Get the term object for a specified term code.
        /// </summary>
        /// <param name="termId">Term code</param>
        /// <returns>Term object</returns>
        public Term GetTerm(string termId)
        {
            Term term = null;
            if (!String.IsNullOrEmpty(termId))
            {
                var terms = _terms.Where(x => x.Code == termId);
                term = CollectionUtility.IsNullOrEmpty(terms) ? null : terms.FirstOrDefault();
            }

            return term;
        }

        #endregion

        #region Get Term Description

        /// <summary>
        /// Get the description of a term based on the term code.
        /// </summary>
        /// <param name="termId">Term code</param>
        /// <returns>Term description</returns>
        public string GetTermDescription(string termId)
        {
            string description = termId;
            if (!String.IsNullOrEmpty(termId))
            {
                var term = GetTerm(termId);
                if (term != null)
                {
                    description = term.Description;
                }
            }
            return description;
        }

        #endregion

        #region Get Term ID for Description

        /// <summary>
        /// Get the term ID associated with a term description
        /// </summary>
        /// <param name="termDescription">Description of a term</param>
        /// <param name="terms">Collection of terms</param>
        /// <returns>ID for the term</returns>
        public string GetTermIdForTermDescription(string termDescription)
        {
            var term = _terms.Where(t => t.Description == termDescription).FirstOrDefault();
            // If no matching term based on description, check to see if supplied description is an ID
            if (term == null)
            {
                term = _terms.Where(t => t.Code == termDescription).FirstOrDefault();
            }
            return term != null ? term.Code : null;
        }

        #endregion  

        #region Get the reporting term for a given term

        /// <summary>
        /// Get the reporting term for a term
        /// </summary>
        /// <param name="termId">Term ID</param>
        /// <returns>Reporting term ID</returns>
        public string GetReportingTerm(string termId)
        {
            var term = GetTerm(termId);
            return (term == null) ? String.Empty : term.ReportingTerm;
        }

        #endregion

        #region Is term a reporting term

        /// <summary>
        /// Determine whether the specified term is a reporting term
        /// </summary>
        /// <param name="termId">Term ID</param>
        /// <returns>Boolean value</returns>
        public bool IsReportingTerm(string termId)
        {
            if (String.IsNullOrEmpty(termId))
            {
                return false;
            }
            // A term is a reporting term if the reporting term and the term code are the same
            return CompareTerms(termId, GetReportingTerm(termId));
        }

        #endregion

        #region Is a term in a specified reporting term

        /// <summary>
        /// Is the specified term in the specified reporting term
        /// </summary>
        /// <param name="termId">Term ID</param>
        /// <param name="reportingTerm">Reporting term ID</param>
        /// <returns>Boolean value</returns>
        public bool IsInReportingTerm(string termId, string reportingTerm)
        {
            if (termId == null || reportingTerm == null)
            {
                return false;
            }
            return CompareTerms(GetReportingTerm(termId), reportingTerm);
        }

        #endregion

        #region Get the financial period for a term

        /// <summary>
        /// Get the Financial period in which a term falls.
        /// </summary>
        /// <param name="termId">Term code</param>
        /// <returns>Financial period</returns>
        public PeriodType? GetTermPeriod(string termId)
        {
            PeriodType? period = null;
            var term = GetTerm(termId);
            if (term != null)
            {
                period = term.FinancialPeriod;
            }

            return period;
        }

        #endregion

        #region Get the sort order for a term

        /// <summary>
        /// Get the sort value for a given term code
        /// </summary>
        /// <param name="termId">Term code</param>
        /// <returns>Term sort value</returns>
        /// <remarks>The sort order is as follows:
        ///  - Term Start Date
        ///  - Term Sequence
        ///  - Term End Date
        ///  - Term Code
        /// Non-term values always sort to the end.</remarks>
        public string GetTermSortOrder(string termId)
        {
            string order = String.Empty;
            var term = GetTerm(termId);
            if (term == null)
            {
                // Sort non-term entries to the end
                order += DateTime.MaxValue.ToString("s");           // Max date value
                order += "999";                                     // Max sequence number
                order += DateTime.MaxValue.ToString("s");           // Max date value
                order += "zzzzzzz";                                 // Max code value
            }
            else
            {
                order += term.StartDate.ToString("s");              // First is start date
                order += term.Sequence.ToString().PadLeft(3, '0');  // Next is sequence number
                order += term.EndDate.ToString("s");                // End date
                order += term.Code;                                 // Term code
            }

            return order;
        }

        #endregion

        #region Compare terms

        /// <summary>
        /// Compare two terms for the same Code value
        /// </summary>
        /// <param name="sourceTerm">Term 1</param>
        /// <param name="compareTerm">Term 2</param>
        /// <returns>Boolean comparison value</returns>
        public bool CompareTerms(Term sourceTerm, Term compareTerm)
        {
            // Terms are equal if they are both null or their Code values are equal
            if (sourceTerm == null && compareTerm == null)
            {
                return true;
            }
            else if (sourceTerm == null || compareTerm == null)
            {
                return false;
            }
            else
            {
                return CompareTerms(sourceTerm.Code, compareTerm.Code);
            }
        }

        /// <summary>
        /// Compare two term codes with a null term equal to a blank term.
        /// </summary>
        /// <param name="sourceTerm">Source term code</param>
        /// <param name="compareTerm">Comparison term code</param>
        /// <returns>Boolean comparison value</returns>
        public bool CompareTerms(string sourceTerm, string compareTerm)
        {
            return (sourceTerm ?? String.Empty) == (compareTerm ?? String.Empty);
        }

        #endregion

        #region Get the specified period

        /// <summary>
        /// Get the FinancialPeriod for the specified PeriodType
        /// </summary>
        /// <param name="type">Period type</param>
        /// <returns>Financial period</returns>
        public FinancialPeriod GetPeriod(PeriodType? type)
        {
            if (CollectionUtility.IsNullOrEmpty(_periods))
            {
                return null;
            }
            if (type == null)
            {
                return null;
            }
            var periods = _periods.Where(x => x.Type == type.Value);
            return (periods.Count() < 1) ? null : periods.First();
        }

        /// <summary>
        /// Get the FinancialPeriod for the specified period code
        /// </summary>
        /// <param name="type">Period code</param>
        /// <returns>Financial period</returns>
        public FinancialPeriod GetPeriod(string code)
        {
            if (String.IsNullOrEmpty(code))
            {
                return null;
            }
            var type = GetPeriodType(code);
            return GetPeriod(type);
        }

        #endregion
        
        #region Get the period type

        /// <summary>
        /// Determine the period type for a given term and date
        /// </summary>
        /// <param name="termId">Term code</param>
        /// <param name="date">Transaction date</param>
        /// <returns></returns>
        public PeriodType? GetPeriodType(string termId, DateTime date)
        {
            PeriodType? period = null;
            // There are no periods unless PCF is active
            if (_periods.Count() > 0)
            {
                if (String.IsNullOrEmpty(termId))
                {
                    // Get the period for the specified date
                    var periods = _periods.Where(x => DateUtility.IsDateInRange(date, x.Start, x.End));

                    // Make sure we got something back, then return it as the period type
                    if (periods != null)
                    {
                        period = periods.SingleOrDefault().Type;
                    }
                }
                else
                {
                    // Get the period for the specified term
                    period = GetTermPeriod(termId);
                }
            }

            return period;
        }

        /// <summary>
        /// Get the PeriodType for a period code
        /// </summary>
        /// <param name="code">Period code</param>
        /// <returns>Period type</returns>
        public PeriodType? GetPeriodType(string code)
        {
            PeriodType? type = null;
            switch (code)
            {
                case PastPeriodCode:
                    type = PeriodType.Past;
                    break;
                case CurrentPeriodCode:
                    type = PeriodType.Current;
                    break;
                case FuturePeriodCode:
                    type = PeriodType.Future;
                    break;
            }
            return type;
        }

        #endregion

        #region Get the terms in a period

        /// <summary>
        /// Get the terms for a specified financial period.
        /// </summary>
        /// <param name="type">Period type</param>
        /// <returns>List of terms in the period</returns>
        public IEnumerable<string> GetPeriodTerms(PeriodType? type)
        {
            if (type == null)
            {
                return new List<string>();
            }
            else
            {
                return _terms.Where(x => x.FinancialPeriod == type.Value).OrderBy(x => GetTermSortOrder(x.Code)).Select(x => x.Code).Reverse();
            }
        }

        #endregion

        #region Get period code

        /// <summary>
        /// Get the code for a given period.
        /// </summary>
        /// <param name="type">Period type</param>
        /// <returns>Period code</returns>
        public string GetPeriodCode(PeriodType? type)
        {
            string code = String.Empty;
            if (type != null)
            {
                switch (type)
                {
                    case PeriodType.Past:
                        code = PastPeriodCode;
                        break;

                    case PeriodType.Current:
                        code = CurrentPeriodCode;
                        break;

                    case PeriodType.Future:
                        code = FuturePeriodCode;
                        break;
                }
            }
            return code;
        }

        #endregion

        #region Get the financial period into which a date falls

        /// <summary>
        /// Get the PeriodType for a specified date.
        /// </summary>
        /// <param name="date">Date</param>
        /// <returns>Period type</returns>
        public PeriodType? GetDatePeriod(DateTime date)
        {
            // Get the period for the specified date
            var periods = _periods.Where(x => DateUtility.IsDateInRange(date, x.Start, x.End));
            var period = CollectionUtility.IsNullOrEmpty(periods) ? null : periods.SingleOrDefault();
            return (period == null) ? null : (PeriodType?)period.Type;
        }

        #endregion
        
        #region Compare a term and date against those for a period

        /// <summary>
        /// Compare a transaction's term and date against those of a specified financial period.
        /// </summary>
        /// <param name="sourceTerm">Transaction term code</param>
        /// <param name="sourceDate">Transaction date</param>
        /// <param name="periodType">Type of financial period</param>
        /// <returns>Boolean comparison value</returns>
        public bool ComparePeriod(string sourceTerm, DateTime sourceDate, PeriodType periodType)
        {
            var period = GetPeriod(periodType);
            if (period == null)
            {
                return false;
            }
            return ComparePeriod(sourceTerm, sourceDate, GetPeriodTerms(periodType), period.Start, period.End);
        }

        /// <summary>
        /// Compare a transaction's term and date against those of a PCF period.
        /// </summary>
        /// <param name="sourceTerm">Transaction term code</param>
        /// <param name="sourceDate">Transaction date</param>
        /// <param name="termIds">List of period's terms</param>
        /// <param name="startDate">Period start date</param>
        /// <param name="endDate">Period end date</param>
        /// <returns>Boolean comparison value</returns>
        public bool ComparePeriod(string sourceTerm, DateTime sourceDate, IEnumerable<string> termIds, DateTime? startDate, DateTime? endDate)
        {
            bool include = false;
            if (String.IsNullOrEmpty(sourceTerm))
            {
                // Source item is non-term - see if the source date is in the specified date range
                include = DateUtility.IsDateInRange(sourceDate, startDate, endDate);
            }
            else
            {
                // Source item is term-based - look for a matching term
                include = termIds.Contains(sourceTerm);
            }
            return include;
        }

        #endregion

        #region Is in Period

        /// <summary>
        /// Determine if a date is in a specified period
        /// </summary>
        /// <param name="date">Date to evaluate</param>
        /// <param name="type">Period for comparison</param>
        /// <returns>True/false indicator</returns>
        public bool IsInPeriod(DateTime date, PeriodType? type)
        {
            var period = GetPeriod(type);
            return period == null ? false : DateUtility.IsDateInRange(date, period.Start, period.End);
        }

        /// <summary>
        /// Determine if a date range falls in a specified period
        /// </summary>
        /// <param name="startDate">Range starting date</param>
        /// <param name="endDate">Range ending date</param>
        /// <param name="type">Period type for comparison</param>
        /// <returns>True/false indicator</returns>
        public bool IsInPeriod(DateTime? startDate, DateTime? endDate, PeriodType? type)
        {
            DateUtility.DateRangeDefaults(ref startDate, ref endDate);
            var period = GetPeriod(type);
            return (period == null) ? false : DateUtility.IsRangeOverlap(startDate.Value, endDate.Value, period.Start, period.End);
        }

        #endregion

        #region Get the period for a date range

        /// <summary>
        /// Get the period for a date range
        /// </summary>
        /// <param name="startDate">Range starting date</param>
        /// <param name="endDate">Range ending date</param>
        /// <returns>Period type in which date range falls</returns>
        /// <remarks>Even if the date range falls into multiple periods, this method will only return
        /// a single period.  If any part of the date range overlaps the Current period, then the Current
        /// period is returned.  Otherwise, the Future period is checked for an overlap.  If the date range
        /// isn't in either of those periods, then it is assumed to be in the Past period.</remarks>
        public PeriodType? GetDateRangePeriod(DateTime? startDate, DateTime? endDate)
        {
            PeriodType? period = null;
            // If there are no periods, don't do anything
            if (!CollectionUtility.IsNullOrEmpty(_periods))
            {
                // If any part of the date range falls in the current period, then it's in that period
                if (IsInPeriod(startDate, endDate, PeriodType.Current))
                {
                    period = PeriodType.Current;
                }
                else if (IsInPeriod(startDate, endDate, PeriodType.Future))
                {
                    period = PeriodType.Future;
                }
                else
                {
                    period = PeriodType.Past;
                }
            }
            return period;
        }
        #endregion

        #region Get the description for a period

        /// <summary>
        /// Gets the period description for a period ID
        /// </summary>
        /// <param name="selectedPeriodId">Selected period ID</param>
        /// <returns>Description of the period</returns>
        public string GetPeriodDescription(string selectedPeriodId)
        {
            string periodDescription = null;

            if (!String.IsNullOrEmpty(selectedPeriodId))
            {
                FinancialPeriod period = GetPeriod(selectedPeriodId);
                if (period != null)
                {
                    periodDescription = period.Type.ToString();
                }

            }

            return periodDescription;
        }

        #endregion

        #region Get the sort order for a period

        /// <summary>
        /// Get the sort value for a given period description
        /// </summary>
        /// <param name="periodDescription">Period Title</param>
        /// <returns>Period sort value</returns>
        public int GetPeriodSortOrder(string periodTitle)
        {
            int order = 0;
            if (String.IsNullOrEmpty(periodTitle))
            {
                order = int.MaxValue;
            }
            else
            {
                if (periodTitle == "Past")
                {
                    order = 0;
                }
                if (periodTitle == "Current")
                {
                    order = 1;
                }
                if (periodTitle == "Future")
                {
                    order = 2;
                }
            }

            return order;
        }

        #endregion

        /// <summary>
        /// Get the start and end dates for a given timeframe
        /// </summary>
        /// <param name="timeframeId">Term or Period code</param>
        /// <param name="config">Finance Configuration</param>
        /// <param name="terms">Collection of Terms</param>
        /// <param name="startDate">Start Date for the timeframe</param>
        /// <param name="endDate">End Date for the timeframe</param>
        public void GetDatesForPeriod(string timeframeId, FinanceConfiguration config, IEnumerable<Term> terms, out DateTime? startDate, out DateTime? endDate)
        {
            startDate = null;
            endDate = null;
            if (!string.IsNullOrEmpty(timeframeId))
            {
                var utility = new TermPeriodsUtility(terms, config.Periods);
                var period = config.Periods.Where(p => utility.GetPeriodCode(p.Type) == timeframeId).FirstOrDefault();
                if (period != null)
                {
                    switch (period.Type)
                    {
                        case PeriodType.Past:
                            endDate = period.End;
                            break;
                        case PeriodType.Current:
                            startDate = period.Start;
                            endDate = period.End;
                            break;
                        default:
                            startDate = period.Start;
                            break;
                    }
                }
            }
        }
    }
}