﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Text;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Utility
{
    /// <summary>
    /// Utility for processing section information
    /// </summary>
    public static class SectionUtility
    {
        /// <summary>
        /// To Be Announced string
        /// </summary>
        public static string TBA;

        /// <summary>
        /// Delimiter for section dates
        /// </summary>
        public static string SectionDatesDelimiter;

        /// <summary>
        /// Delimiter for course/section numbers
        /// </summary>
        public static string CourseDelimiter;

        // Constructor - gets values for static properties
        static SectionUtility()
        {
            TBA = GlobalResources.GetString(GlobalResourceFiles.SharedConstants, "ToBeDeterminedText");
            SectionDatesDelimiter = GlobalResources.GetString(GlobalResourceFiles.SharedConstants, "SectionDatesDelimiter");
            CourseDelimiter = GlobalResources.GetString(GlobalResourceFiles.SharedConstants, "CourseDelimiter");
        }

        /// <summary>
        /// Get the date range for a section
        /// </summary>
        /// <param name="section">The section</param>
        /// <returns>Range of section starting and ending dates</returns>
        public static string GetSectionDates(Section3 section)
        {
            return GetDateRange(section.StartDate, section.EndDate);
        }

        /// <summary>
        /// Get the dates for a list of section meetings
        /// </summary>
        /// <param name="section">The list of section meetings</param>
        /// <returns>List of starting and ending dates</returns>
        public static IEnumerable<string> GetMeetingDates(IEnumerable<SectionMeeting> meetings)
        {
            var meetingDates = new List<string>();
            if (CollectionUtility.IsNullOrEmpty(meetings))
            {
                meetingDates.Add(TBA);
            }
            else
            {
                foreach (var meeting in meetings)
                {
                    meetingDates.Add(GetMeetingDate(meeting));
                }
            }

            return meetingDates;
        }

        /// <summary>
        /// Get the dates for a section meeting
        /// </summary>
        /// <param name="section">The section meeting</param>
        /// <returns>Starting and ending dates</returns>
        public static string GetMeetingDate(SectionMeeting meeting)
        {
            if (meeting == null)
            {
                return TBA;
            }

            return GetDateRange(meeting.StartDate, meeting.EndDate);
        }

        /// <summary>
        /// Get the string value for starting and an ending date
        /// </summary>
        /// <param name="start">Starting date</param>
        /// <param name="end">Ending date</param>
        /// <returns>Combined date range</returns>
        private static string GetDateRange(DateTime? start, DateTime? end)
        {
            if (start == null && end == null)
            {
                return TBA;
            }

            var dateRange = new StringBuilder();
            if (start == null)
            {
                dateRange.Append(TBA);
            }
            else
            {
                dateRange.Append(start.Value.ToShortDateString());
            }
            dateRange.Append(SectionDatesDelimiter);
            if (end == null)
            {
                dateRange.Append(TBA);
            }
            else
            {
                dateRange.Append(end.Value.ToShortDateString());
            }

            return dateRange.ToString();
        }

        /// <summary>
        /// Get the meeting times for a section meetings
        /// </summary>
        /// <param name="meeting">The section meetings</param>
        /// <returns>The meeting times in output format</returns>
        public static string GetMeetingTimes(SectionMeeting2 meeting)
        {
            if (meeting == null)
            {
                return TBA;
            }

            return GetMeetingTimes(meeting.StartTime, meeting.EndTime);
        }

        /// <summary>
        /// Get the meeting times for specified starting and ending times
        /// </summary>
        /// <param name="startTime">Starting time in string format</param>
        /// <param name="endTime">Ending time in string format</param>
        /// <returns>The meeting times in output format</returns>
        public static string GetMeetingTimes(string startTime, string endTime)
        {
            DateTimeOffset? start = ConvertTimeString(startTime);
            DateTimeOffset? end = ConvertTimeString(endTime);
            return GetMeetingTimes(start, end);
        }

        private static DateTimeOffset? ConvertTimeString(string timeString)
        {
            if (string.IsNullOrEmpty(timeString))
            {
                return (DateTimeOffset?)null;
            }

            DateTime dateTime = Convert.ToDateTime(timeString);
            TimeSpan offset = DateUtility.GetTimeZoneOffset(dateTime);
            return new DateTimeOffset(dateTime, offset);
        }

        /// <summary>
        /// Get the meeting times for specified starting and ending times
        /// </summary>
        /// <param name="startTime">Starting time as a DateTimeOffset</param>
        /// <param name="endTime">Ending time as a DateTimeOffset</param>
        /// <returns>The meeting times in output format</returns>
        public static string GetMeetingTimes(DateTimeOffset? startTime, DateTimeOffset? endTime)
        {
            StringBuilder meetingTime = new StringBuilder();
            if (!startTime.HasValue)
            {
                if (!endTime.HasValue)
                {
                    // Show TBA if the course section meeting has no start or end time
                    meetingTime.Append(TBA);
                }
                else
                {
                    string endTimeString = endTime.Value.LocalDateTime.ToString("t");
                    string endTimeAmOrPm = endTimeString.Substring((endTimeString.Length - 2));
                    meetingTime.Append(TBA + "- " + endTimeString);
                }
            }
            else
            {
                string startTimeString = startTime.Value.LocalDateTime.ToString("t");
                string startTimeAmOrPm = startTimeString.Substring((startTimeString.Length - 2));
                if (!endTime.HasValue)
                {
                    meetingTime.Append(startTimeString + "-" + TBA);
                }
                else
                {
                    string endTimeString = endTime.Value.LocalDateTime.ToString("t");
                    string endTimeAmOrPm = endTimeString.Substring((endTimeString.Length - 2));
                    if (startTimeAmOrPm != endTimeAmOrPm)
                    {
                        meetingTime.Append(startTimeString + "-" + endTimeString);
                    }
                    else
                    {
                        meetingTime.Append(startTimeString.Substring(0, startTimeString.Length - 3) + "-" + endTimeString);
                    }
                }
            }
            return meetingTime.ToString();
        }

        /// <summary>
        /// Get the meeting information for a list of section meetings
        /// </summary>
        /// <param name="meetings">The list of section meetings</param>
        /// <returns>List of meeting information</returns>
        public static List<string> GetMeetingInformation(IEnumerable<SectionMeeting2> meetings)
        {
            List<String> meetingInfoLines = new List<String>();

            // If there are no meetings, return the TBA string
            if (CollectionUtility.IsNullOrEmpty(meetings))
            {
                meetingInfoLines.Add(TBA);
            }
            else
            {
                foreach (var meeting in meetings)
                {
                    StringBuilder meetingInformation = new StringBuilder();
                    meetingInformation.Append(DayOfWeekConverter.EnumCollectionToString(meeting.Days) + " ");
                    meetingInformation.Append(GetMeetingTimes(meeting.StartTime, meeting.EndTime));
                    meetingInfoLines.Add(meetingInformation.ToString());
                }
            }

            return meetingInfoLines;
        }
    }
}