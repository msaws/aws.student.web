﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Models.Search;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Utility
{
    /// <summary>
    /// Utility class for Student Finance Administrative Views
    /// </summary>
    public static class FinanceAdminUtility
    {
        /// <summary>
        /// Creates a <see cref="PersonSearchModel"/> for a Student Finance Administrative view
        /// </summary>
        /// <param name="controllerName">Name of the origin ASP.NET MVC controller</param>
        /// <returns>A <see cref="PersonSearchModel"/> for a Student Finance Administrative view</returns>
        public static PersonSearchModel CreatePersonSearchModel(string controllerName)
        {
            if (string.IsNullOrEmpty(controllerName))
            {
                throw new ArgumentNullException("controllerName", "An ASP.NET MVC controller name must be supplied to build the person search model.");
            }

            return new PersonSearchModel(GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchSearchPrompt"),
                GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchBeforeSearchText"),
                "_PersonSearchResults",
                new SearchActionModel("Admin", controllerName, "Finance"),
                new List<SearchActionModel>() { new SearchActionModel("SearchForAccountholdersAsync", "Admin", "Finance") })
            {
                ErrorMessage = null,
                ErrorOccurred = false,
                PlaceholderText = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchPlaceholderText"),
                PointOfOriginControllerName = controllerName,
                Search = new SearchActionModel("SearchForAccountholdersAsync", "Admin", "Finance"),
                SearchFieldLabel = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchSearchFieldLabel"),
                SearchString = null,
                SearchResults = null,
                SearchSubmitLabel = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchSearchSubmitLabel"),
                Select = new SearchActionModel("Admin", controllerName, "Finance"),
                SpinnerAlternateText = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchSpinnerAlternateText"),
                SpinnerText = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchSpinnerText")
            };
        }
    }
}