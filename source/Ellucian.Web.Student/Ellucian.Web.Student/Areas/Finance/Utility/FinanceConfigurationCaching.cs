﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;

namespace Ellucian.Web.Student.Areas.Finance.Utility
{
    /// <summary>
    /// Provides a caching mechanism for finance configuration data that does not change often.
    /// </summary>
    public static class FinanceConfigurationCaching
    {
        private static readonly object _lockObject = new object();

        // For this instance, a static member is better than using a DI container since this value 
        // must be retrieved with authenticated credentials. It also simplifies unit testing.
        private static FinanceConfiguration _configuration;
        private static ImmediatePaymentControl _immediatePaymentControl;

        /// <summary>
        /// Clears the configuration from memory with thread safety
        /// </summary>
        public static void ClearConfiguration()
        {
            // Block all other requests when removing the configuration
            lock (_lockObject)
            {
                _configuration = null;
            }
        }

        /// <summary>
        /// Retrieves the student finance configuration from cache or from web services before caching the data
        /// </summary>
        /// <param name="serviceClient">Web services client</param>
        /// <returns>Student Finance configuration</returns>
        public static FinanceConfiguration GetConfiguration(ColleagueApiClient serviceClient)
        {
            // First check to see if the configuration has been retrieved
            if (_configuration == null)
            {
                // Block all other requests when updating the configuration
                lock (_lockObject)
                {
                    // Double check to make sure that the configuration was not updated by another thread
                    // Without this check, multiple concurrent requests could send many unnecessary 
                    // requests for data that was retrieved previously
                    if (_configuration == null)
                    {
                        _configuration = serviceClient.GetConfiguration();
                    }
                }
            }

            return _configuration;
        }

        /// <summary>
        /// Retrieves the immediate payment control configuration from cache or web services
        /// </summary>
        /// <param name="serviceClient">Web services client</param>
        /// <returns>Immediate Payment Control configuration</returns>
        public static ImmediatePaymentControl GetImmediatePaymentControl(ColleagueApiClient serviceClient)
        {
            // First check to see if the configuration has been retrieved
            if (_immediatePaymentControl == null)
            {
                // Block all other requests when updating the configuration
                lock (_lockObject)
                {
                    // Double check to make sure that the configuration was not updated by another thread
                    // Without this check, multiple concurrent requests could send many unnecessary 
                    // requests for data that was retrieved previously
                    if (_immediatePaymentControl == null)
                    {
                        _immediatePaymentControl = serviceClient.GetImmediatePaymentControl();
                    }
                }
            }

            return _immediatePaymentControl;
        }
    }
}
