﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Models.Search;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Finance.Controllers
{
    /// <summary>
    /// The FinanceController class provides the various actions for student finance. 
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class AccountActivityController : BaseStudentFinanceController
    {
        private static readonly object _lock = new object();
        private readonly Settings _settings;
        private readonly ILogger _logger;

        /// <summary>
        /// AutoMapper adapter registry
        /// </summary>
        private readonly IAdapterRegistry _adapterRegistry;

        public AccountActivityController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _adapterRegistry = adapterRegistry;
            _settings = settings;
            _logger = logger;
        }

        #region Local Constants

        private const string _AccountActivityAction = "Index";
        private const string _AccountActivityAdminAction = "Admin";

        #endregion

        #region TempData Constant

        // Unique key to store and retrieve a value from TempData
        private const string _PersonId = "PersonId";

        #endregion

        #region Account Activity Action

        /// <summary>
        /// Action method for the AccountActivity page.
        /// </summary>
        /// <returns>AccountActivity view</returns>
        [LinkHelp]
        [PageAuthorize("aa")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "AccountActivityTitle");

            return View("Index");
        }

        #endregion

        #region Account Activity Administrator Display Action

        /// <summary>
        /// Action method for the AccountActivity Admin page.
        /// </summary>
        /// <param name="id">Person ID</param>
        /// <param name="changeUser">Flag indicating whether a user change is occurring</param>
        /// <returns>AccountActivity Administrator view</returns>
        [PageAuthorize("aaAdmin")]
        [LinkHelp]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "AccountActivityTitle") + GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "AdminPageTitle");
            ViewBag.IsAdmin = true;
            ViewBag.PersonId = string.IsNullOrEmpty(id) ? null : id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("Index");
            }
            else
            {
                PersonSearchModel model = FinanceAdminUtility.CreatePersonSearchModel("AccountActivity");
                return View("Admin", model);
            }
        }

        #endregion

        #region Display Statement

        /// <summary>
        /// Get a FileResult object that represents a PDF Student Statement report. This report will be opened
        /// within the browser, as opposed to downloaded to the user's machine.
        /// </summary>
        /// <param name="personId">ID of the student for whom the statement will be generated</param>
        /// <param name="timeframeId">ID of the timeframe for which the statement will be generated</param>
        /// <param name="startDate">Date on which the supplied timeframe starts</param>
        /// <param name="endDate">Date on which the supplied timeframe ends</param>
        /// <returns>A FileResult containing the Student Statement Report pdf file.</returns>
        [PageAuthorize("aa")]
        public async Task<FileResult> DisplayStatement(string personId, string timeframeId)
        {
            if (string.IsNullOrEmpty(personId)) personId = GetEffectivePersonId();

            try
            {
                var fileName = string.Empty;
                DateTime? startDate = null;
                DateTime? endDate = null;
                FinanceConfiguration config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                if (config.ActivityDisplay == ActivityDisplay.DisplayByPeriod)
                {
                    GetDatesForPeriod(timeframeId, config, out startDate, out endDate);
                }
                var report = ServiceClient.GetStudentStatement(personId, timeframeId, startDate, endDate, out fileName);

                MemoryStream memoryStream = new MemoryStream(report);

                return File(memoryStream, "application/Pdf");

            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                throw exception;
            }
        }

        #endregion

        #region Get Account Activity
        /// <summary>
        /// AJAX method to get account activity information
        /// </summary>
        /// <param name="personId">ID of the person for whom account activity will be retrieved</param>
        /// <param name="timeframeId">Term or Period code for which to retrieve account activity</param>
        /// <returns>JSON-formatted AccountActivityModel</returns>
        [HttpPost]
        public async Task<JsonResult> GetAccountActivityViewModel(string personId = null, string timeframeId = null)
        {
            try
            {
                // Use the person ID (if provided) or pull it from the current user
                if (string.IsNullOrEmpty(personId)) personId = GetEffectivePersonId();

                // Get the necessary data to build the model
                AccountActivityPeriods activityPeriods = ServiceClient.GetAccountActivityPeriodsForStudent(personId);
                AccountHolder accountHolder = ServiceClient.GetAccountHolder2(personId);
                DetailedAccountPeriod accountDetails = null;
                FinanceConfiguration config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                List<Term> terms = ServiceClient.GetTerms().ToList();

                // Set the timeframe ID and account details for Term Mode
                if (config.ActivityDisplay == ActivityDisplay.DisplayByTerm)
                {
                    timeframeId = string.IsNullOrEmpty(timeframeId) ? 
                        (activityPeriods.Periods.Count > 0 ? activityPeriods.Periods[0].Id 
                        : activityPeriods.NonTermActivity != null ? activityPeriods.NonTermActivity.Id : timeframeId) 
                        : timeframeId;
                    accountDetails = ServiceClient.GetAccountActivityByTermForStudent2(timeframeId, personId);
                }
                else
                {
                    if (activityPeriods == null || activityPeriods.Periods == null || activityPeriods.Periods.Count == 0)
                    {
                        accountDetails = CreateEmptyDetailedAccountPeriod();
                    }
                    else
                    {
                        // Set the timeframe ID, dates, and account details for PCF mode
                        AccountPeriod period;
                        if (string.IsNullOrEmpty(timeframeId))
                        {
                            period = string.IsNullOrEmpty(timeframeId) ? activityPeriods.Periods.Where(p => p.Description == "Current").FirstOrDefault() : null;
                            timeframeId = period != null ? period.Id : activityPeriods.Periods[0].Id;
                        }
                        period = activityPeriods.Periods.Where(p => p.Id.Equals(timeframeId)).FirstOrDefault();
                        var associatedPeriods = period != null ? period.AssociatedPeriods : new List<string>();
                        DateTime? startDate;
                        DateTime? endDate;
                        GetDatesForPeriod(timeframeId, config, out startDate, out endDate);
                        accountDetails = ServiceClient.GetAccountActivityByPeriodForStudent2(associatedPeriods, startDate, endDate, personId);
                    }
                }

                AccountActivityModel accountActivityModel = new AccountActivityModel();

                if (!string.IsNullOrEmpty(timeframeId))
                {
                    // Build the account activity model
                    accountActivityModel = Models.AccountActivity.AccountActivityModel.Build(activityPeriods, accountDetails,
                        accountHolder, config, timeframeId, terms, CurrentUser);
                }
                else
                {
                    // Build the account activity model
                    accountActivityModel = Models.AccountActivity.AccountActivityModel.Build(accountHolder, config, CurrentUser);
                }
                // Set privacy properties for model
                await SetPrivacyPropertiesForAdminModel(accountActivityModel);
                return Json(accountActivityModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string message = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "");
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = message;
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Get Cash Receipt
        /// <summary>
        /// AJAX method to get cash receipt information
        /// </summary>
        /// <param name="receiptId">ID of the cash receipt to be retrieved</param>
        /// <returns>JSON-formatted PaymentReceipt</returns>
        [HttpGet]
        public JsonResult GetCashReceipt(string receiptId)
        {
            try
            {
                PaymentReceipt receipt = ServiceClient.GetCashReceipt(null, receiptId);
                
                // Build and return a valid acknowledgement model if the receipt has any content
                if (receipt != null &&
                    (receipt.ConvenienceFees.Count != 0 || receipt.Deposits.Count != 0 || receipt.OtherItems.Count != 0 || receipt.PaymentMethods.Count != 0 || receipt.Payments.Count != 0))
                {
                    var ackModel = Models.Payments.PaymentAcknowledgementModel.Build(receipt);
                    return Json(ackModel, JsonRequestBehavior.AllowGet);
                }

                throw new ApplicationException("Unable to load cash receipt data.");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpGet]
        public async Task<JsonResult> GetPaymentPlanDetailsAsync(string planApprovalId)
        {
            try
            {
                // A payment plan approval ID must be provided
                if (string.IsNullOrEmpty(planApprovalId))
                {
                    throw new Exception(GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "PaymentPlanApprovalIdRequiredMessage"));
                }

                // Retrieve the payment plan approval
                var planApproval = ServiceClient.GetPaymentPlanApproval(planApprovalId);

                // Build the plan acknowledgment
                var payPlanAckModel = await BuildPaymentPlanAcknowledgementModel(planApproval);
                return Json(payPlanAckModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string message = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "UnableToGetPaymentPlanDetailsMessage");
                Logger.Info(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Get the start and end dates for a given timeframe
        /// </summary>
        /// <param name="timeframeId">Term or Period code</param>
        /// <param name="config">Finance Configuration</param>
        /// <param name="startDate">Start Date for the timeframe</param>
        /// <param name="endDate">End Date for the timeframe</param>
        private void GetDatesForPeriod(string timeframeId, FinanceConfiguration config, out DateTime? startDate, out DateTime? endDate)
        {
            startDate = null;
            endDate = null;
            if (!string.IsNullOrEmpty(timeframeId))
            {
                var utility = new TermPeriodsUtility(ServiceClient.GetTerms(), config.Periods);
                var period = config.Periods.Where(p => utility.GetPeriodCode(p.Type) == timeframeId).FirstOrDefault();
                if (period != null)
                {
                    switch(period.Type)
                    {
                        case PeriodType.Past:
                            endDate = period.End;
                            break;
                        case PeriodType.Current:
                            startDate = period.Start;
                            endDate = period.End;
                            break;
                        default:
                            startDate = period.Start;
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Builds an empty DetailedAccountPeriod DTO
        /// </summary>
        /// <returns>DetailedAccountPeriod DTO</returns>
        private DetailedAccountPeriod CreateEmptyDetailedAccountPeriod()
        {
            return new DetailedAccountPeriod()
            {
                AssociatedPeriods = new List<string>(),
                Charges = new ChargesCategory()
                {
                    FeeGroups = new List<FeeType>(),
                    Miscellaneous = new OtherType(),
                    OtherGroups = new List<OtherType>(),
                    RoomAndBoardGroups = new List<RoomAndBoardType>(),
                    TuitionBySectionGroups = new List<TuitionBySectionType>(),
                    TuitionByTotalGroups = new List<TuitionByTotalType>(),
                },
                Deposits = new DepositCategory()
                {
                    Deposits = new List<ActivityRemainingAmountItem>()
                },
                FinancialAid = new FinancialAidCategory()
                {
                    AnticipatedAid = new List<ActivityFinancialAidItem>(),
                    DisbursedAid = new List<ActivityDateTermItem>()
                },
                PaymentPlans = new PaymentPlanCategory()
                {
                    PaymentPlans = new List<ActivityPaymentPlanDetailsItem>()
                },
                Refunds = new RefundCategory()
                {
                    Refunds = new List<ActivityPaymentMethodItem>()
                },
                Sponsorships = new SponsorshipCategory()
                {
                    SponsorItems = new List<ActivitySponsorPaymentItem>()
                },
                StudentPayments = new StudentPaymentCategory() 
                {
                    StudentPayments = new List<ActivityPaymentPaidItem>()
                }
            };
        }
    }
}
