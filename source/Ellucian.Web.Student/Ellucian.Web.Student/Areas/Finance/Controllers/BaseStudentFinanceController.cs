﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Configuration;
using Ellucian.Web.Mvc.Controller;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Colleague.Api.Client;
using Microsoft.IdentityModel.Claims;
using System.Linq;
using Ellucian.Web.Mvc.Models;
using slf4net;
using Ellucian.Web.Security;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using System.Web.Mvc;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using System;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.Finance;


namespace Ellucian.Web.Student.Infrastructure
{
    [InitializeCredentials]
    public class BaseStudentFinanceController : BaseStudentController
    {
        public BaseStudentFinanceController(Settings settings, ILogger logger)
            : base(settings, logger)
        {
            ServiceClient.Credentials = null;
        }

        /// <summary>
        /// Creates a new instance of the <see cref="PaymentPlanAcknowledgementModel"/> class.
        /// </summary>
        /// <param name="planApproval">A <see cref="PaymentPlanApproval"/> object</param>
        /// <returns>Instance of the <see cref="PaymentPlanAcknowledgementModel"/> class</returns>
        public async Task<PaymentPlanAcknowledgementModel> BuildPaymentPlanAcknowledgementModel(PaymentPlanApproval planApproval)
        {
            try
            {
                var accountHolder = await ServiceClientCache.GetCachedAccountHolderAsync(ServiceClient, planApproval.StudentId);
                var ackDocument = await ServiceClientCache.GetCachedApprovalDocumentAsync(ServiceClient, planApproval.AcknowledgementDocumentId);
                var termsResponse = await ServiceClientCache.GetCachedApprovalResponseAsync(ServiceClient, planApproval.TermsResponseId);
                var termsDocument = await ServiceClientCache.GetCachedApprovalDocumentAsync(ServiceClient, termsResponse.DocumentId);
                var paymentPlan = ServiceClient.GetPaymentPlan(planApproval.PaymentPlanId);
                var termDescription = await GetTermDescription(paymentPlan.TermId);
                var planDetailsModel = PaymentPlanDetailsModel.BuildPlanAcknowledgement(paymentPlan, planApproval, string.Join("", ackDocument.Text), termsDocument, termsResponse, 0m, false, termDescription);

                // Display the Payment Plan Acknowledgement form
                var payPlanAckModel = PaymentPlanAcknowledgementModel.Build(planDetailsModel, null, null, CurrentUser);
                return payPlanAckModel;
            }
            catch (Exception ex)
            {
                Logger.Info(ex, GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "UnableToBuildPaymentPlanAcknowledgementModelMessage"));
                throw ex;
            }
        }

        /// <summary>
        /// Sets the privacy properties of a <see cref="StudentFinanceAdminModel"/> object.
        /// </summary>
        public async Task SetPrivacyPropertiesForAdminModel(StudentFinanceAdminModel model)
        {
            if (model == null)
            {
                return;
            }

            // If no privacy code, the data does not have a privacy restriction or associated message
            if (string.IsNullOrEmpty(model.PrivacyStatusCode))
            {
                model.HasPrivacyRestriction = false;
                model.PrivacyMessage = null;
                return;
            }
            else
            {
                // Users have access to their own data; proxy users have access to grantor data
                if (CurrentUser.IsPerson(model.PersonId) || model.PersonId == GetEffectivePersonId())
                {
                    model.HasPrivacyRestriction = false;
                    model.PrivacyMessage = null;
                }
                else
                {
                    // Administrators must have access to the privacy code in order to view the data;
                    // access the admin user's staff record (if available) to determine their privacy access privileges
                    Staff staff = null;
                    try
                    {
                        staff = await ServiceClient.GetStaffAsync(CurrentUser.PersonId);
                    }
                    catch
                    {
                        staff = new Staff(CurrentUser.PersonId, string.Empty) { PrivacyCodes = new List<string>() };
                    }

                    // Data is private if admin user does have access to the privacy code
                    model.HasPrivacyRestriction = !staff.PrivacyCodes.Contains(model.PrivacyStatusCode);
                    if (model.HasPrivacyRestriction)
                    {
                        // Use the record denial message from PID5 if the user does not have access to the data
                        PrivacyConfiguration privacyConfig = await ServiceClient.GetPrivacyConfigurationAsync();
                        model.PrivacyMessage = privacyConfig != null ? privacyConfig.RecordDenialMessage : string.Empty;
                    }
                    else
                    {
                        // Use the privacy warning message from PID5 if the user has access to the data
                        model.PrivacyMessage = await ServiceClient.GetCachedPrivacyMessageAsync(model.PrivacyStatusCode);
                    }
                }
                return;
            }
        }
    }
}