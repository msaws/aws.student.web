﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Finance.Controllers
{
    /// <summary>
    /// The AdminController class provides various actions for Student Finance Administrators 
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class AdminController : BaseStudentFinanceController
    {
        private static readonly object _lock = new object();
        private readonly Settings _settings;
        private readonly ILogger _logger;
        private readonly IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Creates a new instance of the <see cref="AdminController"/> class.
        /// </summary>
        /// <param name="adapterRegistry"><see cref="IAdapterRegistry"/></param>
        /// <param name="settings"><see cref="Settings"/></param>
        /// <param name="logger"><see cref="ILogger"/></param>
        public AdminController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _adapterRegistry = adapterRegistry;
            _settings = settings;
            _logger = logger;
        }

        /// <summary>
        /// AJAX method to search for account holders for the specified search criteria
        /// </summary>
        /// <param name="searchQueryJson">Criteria to search for account holders</param>
        /// <returns>JSON-formatted collection of account holders who meet the search criteria</returns>
        [HttpPost]
        public async Task<ActionResult> SearchForAccountholdersAsync(string searchQueryJson)
        {
            try
            {
                var query = JsonConvert.DeserializeObject<string>(searchQueryJson);
                if (string.IsNullOrEmpty(query))
                {
                    string errorMessage = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchNoCriteriaGivenError");
                    Logger.Error(errorMessage);
                    throw new ArgumentNullException("No search string provided.");
                }
                else
                {
                    var matches = new List<AccountHolder>();
                    try
                    {
                        matches = (await ServiceClient.QueryAccountHoldersByPost3Async(new AccountHolderQueryCriteria() { QueryKeyword = query })).ToList();
                        var resultsList = new List<PersonSearchResultModel>();
                        foreach(var match in matches)
                        {
                            var result = new PersonSearchResultModel(match.PreferredName, match.Id, match.PrivacyStatusCode);
                            await SetPrivacyPropertiesForAdminModel(result);
                            resultsList.Add(result);
                        }
                        return Json(resultsList, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PersonSearchError");
                        Logger.Error(ex.ToString());
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// AJAX method to get finance admin search view model in JSON format
        /// </summary>
        /// <param name="controllerName">Name of the origin ASP.NET MVC controller for search context</param>
        /// <returns>JSON-formatted PersonSearchModel</returns>
        [HttpGet]
        public JsonResult GetFinanceAdminSearchViewModel(string controllerName)
        {
            if (string.IsNullOrEmpty(controllerName))
            {
                string message = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "NoControllerNameSpecifiedMessage");
                Logger.Error(message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            try
            {
                PersonSearchModel model = FinanceAdminUtility.CreatePersonSearchModel(controllerName);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}