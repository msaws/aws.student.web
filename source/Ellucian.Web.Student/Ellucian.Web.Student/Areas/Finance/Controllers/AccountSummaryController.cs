﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Finance.Models.AccountSummary;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Models.Search;
using Ellucian.Web.Student.Utility;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Finance.Controllers
{
    /// <summary>
    /// The AccountSummaryController class provides the various actions for Student Finance Account Summary. 
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class AccountSummaryController : BaseStudentFinanceController
    {
        private static readonly object _lock = new object();
        private readonly Settings _settings;
        private readonly ILogger _logger;
        private readonly IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountSummaryController"/> class.
        /// </summary>
        /// <param name="adapterRegistry"></param>
        /// <param name="settings"></param>
        /// <param name="logger"></param>
        public AccountSummaryController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _adapterRegistry = adapterRegistry;
            _settings = settings;
            _logger = logger;
        }

        /// <summary>
        /// Action method for the Account Summary page.
        /// </summary>
        /// <returns>Account Summary view</returns>
        [LinkHelp]
        [PageAuthorize("acctSummary")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.AccountSummaryResources, "PageTitle");
            return View("Index");
        }

        /// <summary>
        /// Action method for the Account Summary Admin page.
        /// </summary>
        /// <param name="personId">Person ID</param>
        /// <param name="changeUser">Flag indicating whether a user change is occurring</param>
        /// <returns>Account Summary Administrator view</returns>
        [PageAuthorize("acctSummaryAdmin")]
        [LinkHelp]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.AccountSummaryResources, "PageTitle") + GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "AdminPageTitle");
            ViewBag.IsAdmin = true;
            ViewBag.PersonId = string.IsNullOrEmpty(id) ? null : id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("Index");
            }
            else
            {
                PersonSearchModel model = FinanceAdminUtility.CreatePersonSearchModel("AccountSummary");
                return View("Admin", model);
            }
        }
        /// <summary>
        /// AJAX method to get account summary information
        /// </summary>
        /// <param name="personId">ID of the person for whom the account summary will be retrieved</param>
        /// <returns>JSON-formatted AccountSummaryModel</returns>
        [HttpPost]
        public async Task<JsonResult> GetAccountSummaryViewModel(string personId = null)
        {
            try
            {
                // Use the person ID (if provided) or pull it from the current user
                if (string.IsNullOrEmpty(personId)) personId = GetEffectivePersonId();

                // Get the necessary display mode-agnostic data to build the model
                AccountHolder accountHolder = ServiceClient.GetAccountHolder2(personId);
                AccountActivityPeriods activityPeriods = ServiceClient.GetAccountActivityPeriodsForStudent(personId);
                FinanceConfiguration config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                List<Term> terms = (await ServiceClient.GetTermsAsync()).ToList();

                // Initialize the Account Summary  model
                AccountSummaryModel model = new AccountSummaryModel();

                // Get the display mode-specific account due information and build the corresponding model
                if (config.PaymentDisplay == PaymentDisplay.DisplayByTerm)
                {
                    AccountDue ad = ServiceClient.GetPaymentsDueByTermForStudent(personId);
                    model = AccountSummaryModel.BuildForPaymentsDueByTerm(accountHolder, ad, activityPeriods, config, terms, CurrentUser);
                }
                else
                {
                    AccountDuePeriod adp = ServiceClient.GetPaymentsDueByPeriodForStudent(personId);
                    model = AccountSummaryModel.BuildForPaymentsDueByPeriod(accountHolder, adp, activityPeriods, config, terms, CurrentUser);
                }
                // Set privacy properties for model
                await SetPrivacyPropertiesForAdminModel(model);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}