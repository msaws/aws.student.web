﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Ellucian.Web.Student.Areas.Finance.Controllers
{
    /// <summary>
    /// The FinanceController class provides the various actions for student finance. 
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class PaymentsController : BaseStudentFinanceController
    {
        private static readonly object _lock = new object();
        private readonly Settings _settings;
        private readonly IAdapterRegistry _adapterRegistry;

        private const string _CheckType = "CK";

        public PaymentsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _settings = settings;
            _adapterRegistry = adapterRegistry;
        }

        /// <summary>
        /// AJAX method to get payment due information
        /// </summary>
        /// <param name="personId">ID of the person for whom payment due information will be retrieved</param>
        /// <returns>JSON-formatted MakeAPaymentModel</returns>
        [HttpPost]
        public async Task<JsonResult> GetMakeAPaymentViewModel(string personId = null)
        {
            try
            {
                // Use the person ID (if provided) or pull it from the current user
                if (string.IsNullOrEmpty(personId)) personId = GetEffectivePersonId();

                // Get the necessary data to build the model
                AccountHolder accountHolder = ServiceClient.GetAccountHolder2(personId);
                FinanceConfiguration config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                MakeAPaymentModel makeAPaymentModel = null;
                // Set the timeframe ID and account details for Term Mode
                if (config.PaymentDisplay == PaymentDisplay.DisplayByTerm)
                {
                    // Get the term payment due data
                    AccountDue accountDue = ServiceClient.GetPaymentsDueByTermForStudent(personId);

                    // Build the make a payment term model
                    makeAPaymentModel = Models.Payments.MakeAPaymentModel.Build(accountDue, config, accountHolder, CurrentUser);
                }
                else
                {
                    // Get the period payment due data
                    AccountDuePeriod accountDuePeriod = ServiceClient.GetPaymentsDueByPeriodForStudent(personId);

                    // Build the make a payment period model
                    makeAPaymentModel = Models.Payments.MakeAPaymentModel.Build(accountDuePeriod, config, accountHolder, CurrentUser);
                }
                // Set privacy properties for model
                await SetPrivacyPropertiesForAdminModel(makeAPaymentModel);
                return Json(makeAPaymentModel, JsonRequestBehavior.AllowGet);

           }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Action method for the MakeAPayment page.
        /// </summary>
        /// <returns>MakeAPayment View</returns>
        [LinkHelp]
        [PageAuthorize("map")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "MakeAPaymentTitle");

            return View("Index");
        }

        /// <summary>
        /// Action method for the Make a Payment Admin page.
        /// </summary>
        /// <param name="id">Person ID</param>
        /// <param name="changeUser">Flag indicating whether a user change is occurring</param>
        /// <returns>Make a Payment Administrator view</returns>
        [PageAuthorize("mapAdmin")]
        [LinkHelp]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "MakeAPaymentTitle") + GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "AdminPageTitle");
            ViewBag.IsAdmin = true;
            ViewBag.PersonId = string.IsNullOrEmpty(id) ? null : id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("Index");
            }
            else
            {
                PersonSearchModel model = FinanceAdminUtility.CreatePersonSearchModel("Payments");
                return View("Admin", model);
            }
        }

        /// <summary>
        /// Redirect to Index on GET as this means the user has been redirected from the Login with no context
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PaymentReview()
        {
            return RedirectToAction("Index", "Payments", new { area = "Finance" });
        }

        /// <summary>
        /// This action processes a form submission for payments due. Processing errors are contained in ViewBag.Error.
        /// </summary>
        /// <param name="paymentModel">payments due model from form submission</param>
        /// <returns>payments due view or redirect for login</returns>
        [HttpPost] // Force HTTP POST for submission purposes
        [LinkHelp]
        public ActionResult PaymentReview(string model)
        {
            Ellucian.Web.Student.Areas.Finance.Models.Payments.PaymentReviewModel reviewModel;

            try
            {
                var localModel = JsonConvert.DeserializeObject<Models.Payments.MakeAPaymentModel>(model);

                string selectedDistribution;
                string selectedPaymentMethod = localModel.SelectedPaymentMethod.InternalCode;

                List<PaymentItem> paymentItems = new List<PaymentItem>();
                Payment payment = new Payment();

                // Identify the payment distribution being used and build the payment items based on the user's selections
                if (localModel.IsTermDisplay)
                {
                    selectedDistribution = localModel.TermModels.SelectMany(t => t.PaymentsDue).Where(pd => pd.IsSelected).First().PaymentGroup;
                    foreach (var term in localModel.TermModels)
                    {
                        paymentItems.AddRange(ConvertTermSelectedItemsToPaymentItems(term));
                    }
                }
                else
                {
                    selectedDistribution = localModel.PeriodModels.SelectMany(p => p.Terms).SelectMany(t => t.PaymentsDue).Where(pd => pd.IsSelected).First().PaymentGroup;
                    foreach (var period in localModel.PeriodModels)
                    {
                        paymentItems.AddRange(ConvertPeriodSelectedItemsToPaymentItems(period));
                    }
                }
                decimal totalAmountToPay = paymentItems.Sum(pi => pi.PaymentAmount);
                string totalAmountToPayString = totalAmountToPay.ToString();

                // Get the payment confirmation information and build the payment to be made
                var confirmation = ServiceClient.ConfirmStudentPayment(selectedDistribution, selectedPaymentMethod, totalAmountToPayString);
                if (confirmation != null)
                {
                    payment = new Payment()
                    {
                        AmountToPay = totalAmountToPay + confirmation.ConvenienceFeeAmount.GetValueOrDefault(),
                        Distribution = selectedDistribution,
                        PayMethod = selectedPaymentMethod,
                        PersonId = localModel.PersonId,
                        PayerId = CurrentUser.PersonId,
                        ConvenienceFee = confirmation.ConvenienceFeeCode,
                        ConvenienceFeeAmount = confirmation.ConvenienceFeeAmount.GetValueOrDefault(),
                        ConvenienceFeeGeneralLedgerNumber = confirmation.ConvenienceFeeGeneralLedgerNumber,
                        PaymentItems = paymentItems,
                        ProviderAccount = confirmation.ProviderAccount,
                    };
                }

                // Get the configuration and codes needed
                var config = FinanceConfigurationCaching.GetConfiguration(ServiceClient);
                var receivableTypes = ServiceClient.GetReceivableTypes();
                var convenienceFees = ServiceClient.GetConvenienceFees();

                // Build the review model
                reviewModel = Ellucian.Web.Student.Areas.Finance.Models.Payments.PaymentReviewModel.Build(payment, localModel.SelectedPaymentMethod, receivableTypes, convenienceFees, config.PaymentReviewMessage);
            }
            catch (Exception ex)
            {
                reviewModel = Ellucian.Web.Student.Areas.Finance.Models.Payments.PaymentReviewModel.BuildWithErrors();
                ViewBag.Error = ex.Message;
            }

            // Override the help for payment review view
            ViewBag.HelpContentId = "PaymentsPaymentReview";
            ViewBag.BodyId = "PaymentReview";
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentReviewHeader");
            ViewBag.json = JsonConvert.SerializeObject(reviewModel);
            return View("PaymentReview", reviewModel);
        }

        /// <summary>
        /// Redirect to Index on GET as this means the user has been redirected from the Login with no context
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ECheckEntry()
        {
            return RedirectToAction("Index", "Payments", new { area = "Finance" });
        }

        /// <summary>
        /// Process the payment review form and either display the e-check form or transfer control to a payment provider
        /// </summary>
        /// <param name="model">JSON-formatted payment review model</param>
        /// <returns>ECheckEntry View</returns>
        [LinkHelp]
        [HttpPost]
        public async Task<ActionResult> ECheckEntry(string model)
        {
            var localModel = JsonConvert.DeserializeObject<Models.Payments.PaymentReviewModel>(model);

            // Process e-checks and credit cards differently
            if (localModel.PaymentMethod.Type == _CheckType)
            {
                Models.Payments.ElectronicCheckEntryModel checkModel;
                try
                {
                    // Process an e-check
                    var config = FinanceConfigurationCaching.GetConfiguration(ServiceClient);
                    var payerInfo = ServiceClient.GetCheckPayerInformation(CurrentUser.PersonId);
                    var statesProvinces = await ServiceClient.GetStatesAsync();
                    checkModel = Models.Payments.ElectronicCheckEntryModel.Build(config, localModel, payerInfo, statesProvinces);
                    checkModel.Notification = new Notification("", NotificationType.Error);
                }
                catch (Exception ex)
                {
                    checkModel = Models.Payments.ElectronicCheckEntryModel.BuildWithErrors(ex.Message);
                    ViewBag.Error = ex.Message;
                }

                // Override the help for payment review view
                ViewBag.HelpContentId = "PaymentsECheckEntry";
                ViewBag.BodyId = "Payment";
                ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "ECheckEntryHeader");

                ViewBag.json = JsonConvert.SerializeObject(checkModel);
                return View("ECheckEntry", checkModel);
            }

            // Process a credit card payment

            // Build the return URL based on the current request context to preserve the original base URL
            localModel.Payment.ReturnUrl = Url.Action("PaymentAcknowledgement", "Payments", new { Area = "Finance" }, HttpContext.Request.Url.Scheme);
            localModel.Notification = new Notification("", NotificationType.Error);

            // Send the start payment request to the web service and capture the response
            try
            {
                var response = ServiceClient.ProcessStudentPayment(localModel.Payment);
                if (String.IsNullOrEmpty(response.ErrorMessage))
                {
                    // Redirect to the payment provider URL
                    return Redirect(response.RedirectUrl);
                }

                // Display an error condition in a notification
                localModel.Notification = new Notification(response.ErrorMessage, NotificationType.Error);
            }
            catch (Exception ex)
            {
                // Display an exception's message in a notification
                localModel.Notification = new Notification(ex.Message, NotificationType.Error);
            }

            // At this point, we've got an error condition - redisplay the payment review form
            ViewBag.json = JsonConvert.SerializeObject(localModel);
            return View("PaymentReview", localModel);
        }

        /// <summary>
        /// Redirect to Index on GET as this means the user has been redirected from the Login with no context
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ProcessPayment()
        {
            return RedirectToAction("Index", "Payments", new { area = "Finance" });
        }

        /// <summary>
        /// Process the e-check entry form and display the payment acknowledgement
        /// </summary>
        /// <param name="model">JSON-formatted e-check model</param>
        [LinkHelp]
        [HttpPost]
        public ActionResult ProcessPayment(string model)
        {
            var localModel = JsonConvert.DeserializeObject<Ellucian.Web.Student.Areas.Finance.Models.Payments.ElectronicCheckEntryModel>(model);

            // Process the e-check payment
            var result = ServiceClient.ProcessElectronicCheck(localModel.Payment);

            if (String.IsNullOrEmpty(result.ErrorMessage))
            {
                // Proceed to the acknowledgement
                return RedirectToAction("PaymentAcknowledgement", "Payments", new
                {
                    area = "Finance",
                    cashRcptsId = result.CashReceiptsId,
                });
            }

            // The payment request returned an error - add it to the model and redisplay the form
            var pmtErrorString = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentFailedMessage");
            localModel = Ellucian.Web.Student.Areas.Finance.Models.Payments.ElectronicCheckEntryModel.BuildWithErrors(pmtErrorString);
            Logger.Error(result.ErrorMessage);
            ViewBag.Error = result.ErrorMessage;

            // Override the help for payment review view
            ViewBag.HelpContentId = "PaymentsECheckEntry";
            ViewBag.BodyId = "Payment";
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "ECheckEntryHeader");

            ViewBag.json = JsonConvert.SerializeObject(localModel);
            return View("ECheckEntry", localModel);
        }


        /// <summary>
        /// This action displays a payment acknowledgement for the current user. Processing errors are contained in ViewBag.Error.
        /// </summary>
        /// <param name="EcPayTransId">electronic check transaction id</param>
        /// <param name="cashRcptsId">cash receipts id</param>
        /// <returns></returns>
        [HttpGet] // Force HTTP GET for display purposes
        [LinkHelp]
        public ActionResult PaymentAcknowledgement(string EcPayTransId, string cashRcptsId)
        {
            if (!string.IsNullOrEmpty(EcPayTransId) && !string.IsNullOrEmpty(cashRcptsId))
            {
                throw new Exception("PaymentAcknowledgement only accepts one argument => EcPayTransId: " + EcPayTransId + " CashRcptsId: " + cashRcptsId);
            }

            Models.Payments.PaymentAcknowledgementModel ackModel;
            try
            {
                // Retrieve the cash receipt from the given IDs
                PaymentReceipt receipt = ServiceClient.GetCashReceipt(EcPayTransId, cashRcptsId);

                // Build a valid acknowledgement model if the receipt has any content
                if (receipt != null && 
                    (receipt.ConvenienceFees.Count != 0 || receipt.Deposits.Count != 0 || receipt.OtherItems.Count != 0 || receipt.PaymentMethods.Count != 0 || receipt.Payments.Count != 0))
                {
                    ackModel = Models.Payments.PaymentAcknowledgementModel.Build(receipt);
                }

                // If the cash receipt is null/has no content, display build a canceled payment model
                else
                {
                    ackModel = Models.Payments.PaymentAcknowledgementModel.BuildForCanceledPayment(null, NotificationType.Information);
                    ViewBag.Error = ackModel.Notification.Message;
                }
            }
            catch (Exception ex)
            {
                ackModel = Models.Payments.PaymentAcknowledgementModel.BuildWithErrors(ex.Message);
                ViewBag.Error = ex.Message;
            }

            // Override the help for payment review view
            ViewBag.HelpContentId = "PaymentsPaymentAcknowledgement";
            ViewBag.BodyId = "PaymentAcknowledgement";
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentAcknowledgementHeader");

            ViewBag.json = JsonConvert.SerializeObject(ackModel);
            return View("PaymentAcknowledgement", ackModel);
        }

        /// <summary>
        /// Generate a printable version of the payment acknowledgement
        /// </summary>
        /// <param name="model">JSON-formatted payment acknowledgement model</param>
        public ActionResult PrintPaymentAcknowledgement(string ecPayTransId, string cashRcptsId)
        {
            if (!string.IsNullOrEmpty(ecPayTransId) && !string.IsNullOrEmpty(cashRcptsId))
            {
                throw new Exception("PaymentAcknowledgement only accepts one argument => EcPayTransId: " + ecPayTransId + " CashRcptsId: " + cashRcptsId);
            }

            Models.Payments.PaymentAcknowledgementModel ackModel;
            try
            {
                // Retrieve the cash receipt from the given IDs
                PaymentReceipt receipt = ServiceClient.GetCashReceipt(ecPayTransId, cashRcptsId);

                // Build a valid acknowledgement model if the receipt has any content
                if (receipt != null &&
                    (receipt.ConvenienceFees.Count != 0 || receipt.Deposits.Count != 0 || receipt.OtherItems.Count != 0 || receipt.PaymentMethods.Count != 0 || receipt.Payments.Count != 0))
                {
                    ackModel = Models.Payments.PaymentAcknowledgementModel.Build(receipt);
                }

                // If the cash receipt is null/has no content, display build a canceled payment model
                else
                {
                    ackModel = Models.Payments.PaymentAcknowledgementModel.BuildForCanceledPayment(null, NotificationType.Information);
                    ViewBag.Error = ackModel.Notification.Message;
                }
            }
            catch (Exception ex)
            {
                ackModel = Models.Payments.PaymentAcknowledgementModel.BuildWithErrors(ex.Message);
                ViewBag.Error = ex.Message;
            }

            // Override the help for payment review view
            ViewBag.HelpContentId = "PaymentsPaymentAcknowledgement";
            ViewBag.BodyId = "PaymentAcknowledgement";
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentAcknowledgementHeader");

            ViewBag.json = JsonConvert.SerializeObject(ackModel);
            return PartialView(ackModel);
        }


        /// <summary>
        /// This action catches a form submission for payment acknowledgement and redirects back to Make a Payment.
        /// </summary>
        /// <returns>Redirect to Make a Payment</returns>
        [HttpPost]
        public ActionResult PaymentComplete()
        {
            // Force redirect to MakeAPayment action
            return RedirectToAction("Index", "Payments", new { area = "Finance" });
        }

        [HttpPost]
        public async Task<JsonResult> GetPaymentPlanOptionsAsync(string termModelsJson, string periodModelsJson)
        {
            try
            {
                List<PeriodSummaryModel> periodModels = new List<PeriodSummaryModel>();
                List<TermSummaryModel> termModels = new List<TermSummaryModel>();
                if (!string.IsNullOrEmpty(periodModelsJson))
                {
                    periodModels.AddRange(JsonConvert.DeserializeObject<IEnumerable<PeriodSummaryModel>>(periodModelsJson));
                    foreach(var period in periodModels)
                    {
                        termModels.AddRange(period.Terms);
                    }
                }
                else
                {
                    // Deserialize the inbound JSON into a collection of TermSummaryModel objects
                    termModels.AddRange(JsonConvert.DeserializeObject<IEnumerable<TermSummaryModel>>(termModelsJson));
                }

                // Eliminate any non-eligible items from eligible terms
                termModels.ForEach(twei => twei.PaymentsDue.RemoveAll(pd => !pd.IsEligibleForPaymentPlan || !pd.IsPayable));

                // Eliminate any terms (and non-term) that do not have potentially plan-eligible items
                var termsWithEligibleItems = termModels.Where(t => t.PaymentsDue.Any(pd => pd.IsEligibleForPaymentPlan && pd.IsPayable && !string.IsNullOrEmpty(t.TermCode) && t.TermCode != "NON-TERM")).ToList();

                // Initialize DTO list for API call
                List<BillingTermPaymentPlanInformation> billingTermPayPlanItems = new List<BillingTermPaymentPlanInformation>();
                
                // Get the effective person ID
                string personId = GetEffectivePersonId();

                // Build DTOs from term line items by iterating over items due within each term
                foreach(var term in termsWithEligibleItems)
                {
                    foreach(var itemDue in term.PaymentsDue)
                    {
                        var billingTermPayPlanInfo = new BillingTermPaymentPlanInformation()
                        {
                            PaymentPlanAmount = itemDue.DueAmount,
                            PersonId = personId,
                            ReceivableTypeCode = itemDue.ReceivableTypeCode,
                            TermId = term.TermCode
                        };
                        billingTermPayPlanItems.Add(billingTermPayPlanInfo);
                    }
                }

                PaymentPlanOptionsModel response = new PaymentPlanOptionsModel();
                if (billingTermPayPlanItems.Any())
                {
                    // Evaluate the potential plan-eligible items to determine those that are eligible
                    var planEligibility = await ServiceClient.QueryAccountHolderPaymentPlanOptions2Async(new PaymentPlanQueryCriteria() { BillingTerms = billingTermPayPlanItems });
                    var planEligibleItems = planEligibility.EligibleItems != null ? planEligibility.EligibleItems.ToList() : new List<BillingTermPaymentPlanInformation>();

                    // Initialize dictionary of payments due by term for later reference
                    Dictionary<string, List<PaymentsDueModel>> paymentsDueByTerm = new Dictionary<string, List<PaymentsDueModel>>();
                
                    // Find payment due in term and add to dictionary
                    if (planEligibleItems.Any())
                    {
                        foreach (var pei in planEligibleItems)
                        {
                            var termModel = termModels.Where(tm => tm.TermCode == pei.TermId).FirstOrDefault();
                            if (termModel != null)
                            {
                                var termItem = termModel.PaymentsDue.Where(pd => pd.ReceivableTypeCode == pei.ReceivableTypeCode).FirstOrDefault();
                                if (termItem != null)
                                {
                                    termItem.PaymentPlanTemplateId = pei.PaymentPlanTemplateId;
                                    if (paymentsDueByTerm.ContainsKey(pei.TermId) && paymentsDueByTerm[pei.TermId] != null)
                                    {
                                        paymentsDueByTerm[pei.TermId].Add(termItem);
                                    }
                                    else
                                    {
                                        paymentsDueByTerm.Add(pei.TermId, new List<PaymentsDueModel>() { termItem });
                                    }
                                }
                            }
                        }

                        // Remove all term models that do not have plan eligible term items
                        termModels.RemoveAll(tm => !paymentsDueByTerm.Keys.Contains(tm.TermCode));

                        // Reset payments due in each term using dictionary
                        termModels.ForEach(tm => tm.PaymentsDue = paymentsDueByTerm[tm.TermCode]);

                        // Reset terms in periods using dictionary if necessary
                        if (periodModels.Any())
                        {
                            // Initialize dictionary of terms by period for later reference
                            Dictionary<string, List<TermSummaryModel>> periodTerms = new Dictionary<string, List<TermSummaryModel>>();

                            // Find term in each period and add to dictionary
                            foreach (var term in termModels)
                            {
                                var periodModel = periodModels.Where(pm => pm.Terms.Any(t => t.TermCode == term.TermCode)).FirstOrDefault();
                                if (periodModel != null)
                                {
                                    if (periodTerms.ContainsKey(periodModel.PeriodCode) && periodTerms[periodModel.PeriodCode] != null)
                                    {
                                        periodTerms[periodModel.PeriodCode].Add(term);
                                    }
                                    else
                                    {
                                        periodTerms.Add(periodModel.PeriodCode, new List<TermSummaryModel>() { term });
                                    }
                                }
                            }

                            // Remove all period models that do not have plan eligible terms
                            periodModels.RemoveAll(pm => !periodTerms.Keys.Contains(pm.PeriodCode));

                            // Reset terms in each period using dictionary
                            periodModels.ForEach(pm => pm.Terms = periodTerms[pm.PeriodCode]);

                            // Return revised set of periods
                            return Json(periodModels, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            // Return revised set of terms
                            return Json(termModels, JsonRequestBehavior.AllowGet);
                        }
                    } 
                    else
                    {
                        switch(planEligibility.IneligibilityReason)
                        {
                            case PaymentPlanIneligibilityReason.ChargesAreNotEligible:
                                response.Message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "CreatePaymentPlanNoEligibleItemsText");
                                break;
                            case PaymentPlanIneligibilityReason.PreventedBySystemConfiguration:
                            default:
                                response.Message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "CreatePaymentPlanCannotSignUpText");
                                break;
                        }
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    response.Message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "CreatePaymentPlanNoEligibleItemsText");
                    Logger.Info(response.Message);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string message = "Unable to get payment plan options for user.";
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Redirect to Index on GET as this means the user has been redirected from the Login with no context
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PaymentPlan()
        {
            return RedirectToAction("Index", "Payments", new { area = "Finance" });
        }

        /// <summary>
        /// Action method for the Payment Plan page.
        /// </summary>
        /// <returns>MakeAPayment View</returns>
        [LinkHelp]
        [PageAuthorize("map")]
        public async Task<ActionResult> PaymentPlan(string inputJson)
        {
            try
            {
                // Deserialize the inbound JSON into a TermSummaryModel object
                PaymentsDueModel inputModel = JsonConvert.DeserializeObject<PaymentsDueModel>(inputJson);
                if (inputModel != null)
                {
                    // Get the account holder ID
                    var personId = GetEffectivePersonId();

                    // Build a proposed payment plan for the account holder for the selected term, receivable type, and amount
                    try
                    {
                        var proposedPlan = await ServiceClientCache.GetCachedProposedPaymentPlanAsync(ServiceClient, personId, inputModel.TermId,
                         inputModel.ReceivableTypeCode, inputModel.DueAmount);
                        if (proposedPlan != null)
                        {
                            // Get the proposed payment plan's associated template
                            List<PaymentPlanTemplate> allTemplates = (await ServiceClientCache.GetCachedPaymentPlanTemplatesAsync(ServiceClient)).ToList();
                            if (allTemplates == null)
                            {
                                throw new ApplicationException("Could not retrieve payment plan templates.");
                            }
                            PaymentPlanTemplate template = allTemplates.Where(ppt => ppt.Id == proposedPlan.TemplateId).FirstOrDefault();
                            if (template == null)
                            {
                                throw new ApplicationException("No payment plan template with ID " + proposedPlan.TemplateId + " could be found.");
                            }

                            // Get the account holder's information
                            AccountHolder accountHolder = await ServiceClientCache.GetCachedAccountHolderAsync(ServiceClient, personId);
                            if (accountHolder == null)
                            {
                                throw new ApplicationException("No account holder with ID " + personId + " could be found.");
                            }

                            // Get the payment plan acknowledgment text
                            List<string> acknowledgementText = new List<string>() { GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PaymentPlanDetailsStudentMessage") };

                            // Get the payment plan terms and conditions
                            TextDocument termsAndConditionsDocument = await ServiceClient.GetTextDocumentAsync(template.TermsAndConditionsDocumentId, "PERSON", personId, personId);
                            if (termsAndConditionsDocument == null)
                            {
                                throw new ApplicationException("No text document with ID " + template.TermsAndConditionsDocumentId + " could be retrieved for account holder " + personId + ".");
                            }

                            // Get the term description
                            string termDescription = await GetTermDescription(proposedPlan.TermId);

                            // Build the proposed payment plan details
                            PaymentPlanDisplayModel planDisplay = PaymentPlanDisplayModel.Build(proposedPlan);
                            FinanceConfiguration config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                            PaymentPlanDetailsModel outputModel = PaymentPlanDetailsModel.BuildDisplayPlanPreview(planDisplay, accountHolder, acknowledgementText, termsAndConditionsDocument.Text,
                                config, termDescription);
                            ViewBag.json = JsonConvert.SerializeObject(outputModel);
                            return View("PaymentPlan", outputModel);
                        }
                        else
                        {
                            throw new ApplicationException("Could not retrieve proposed payment plan details from API.");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Could not retrieve proposed payment plan details from API: " + ex.Message);
                    }
                }
                else
                {
                    throw new ApplicationException("Inbound JSON deserialization is null.");
                }
            }
            catch (Exception ex)
            {
                string message = "Unable to get proposed payment plan details.";
                Logger.Error(ex, message);
                PaymentPlanDetailsModel outputModel = PaymentPlanDetailsModel.BuildWithErrorMessage();
                ViewBag.json = JsonConvert.SerializeObject(outputModel);
                return View("PaymentPlan", outputModel);
            }
        }

        /// <summary>
        /// AJAX method to process a user's acceptance of the payment plan terms and conditions
        /// </summary>
        /// <param name="planModelJson">JSON-formatted payment plan model</param>
        /// <returns>JSON-formatted payment plan approval</returns>
        [HttpPost]
        public async Task<JsonResult> AcceptPaymentPlanTermsAndConditionsAsync(string planModelJson)
        {
            try
            {
                PaymentPlanDetailsModel inputModel = JsonConvert.DeserializeObject<PaymentPlanDetailsModel>(planModelJson);

                // If the terms were accepted, then update Colleague with the acceptance
                PaymentPlanTermsAcceptance acceptance = new PaymentPlanTermsAcceptance()
                {
                    AcknowledgementDateTime = inputModel.DisplayTimestamp,
                    AcknowledgementText = inputModel.AcknowledgementText,
                    ApprovalReceived = DateTimeOffset.Now,
                    ApprovalUserId = CurrentUser.UserId,
                    DownPaymentAmount = inputModel.DisplayPaymentPlan.PaymentPlan.DownPaymentAmount,
                    DownPaymentDate = inputModel.DisplayPaymentPlan.PaymentPlan.DownPaymentDate,
                    ProposedPlan = inputModel.DisplayPaymentPlan.PaymentPlan,
                    StudentId = inputModel.DisplayPaymentPlan.PaymentPlan.PersonId,
                    StudentName = inputModel.StudentName,
                    TermsText = inputModel.TermsAndConditionsText
                };

                // Process the approval
                var planApproval = ServiceClient.AcceptPaymentPlanTerms(acceptance);

                // Build the plan acknowledgment
                var payPlanAckModel = await BuildPaymentPlanAcknowledgementModel(planApproval);
                return Json(payPlanAckModel, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                string message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "CreatePaymentPlanErrorProcessingAcceptanceMessage");
                Logger.Info(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult PaymentPlanAcknowledgement(PaymentPlanAcknowledgementModel model)
        {
            return View("PaymentPlanAcknowledgement", model);
        }

        [HttpGet]
        public ActionResult PaymentPlanDownPayment()
        {
            // Force redirect to MakeAPayment action
            return RedirectToAction("Index", "Payments", new { area = "Finance" });
        }

        /// <summary>
        /// Initiates the payment plan down payment process
        /// </summary>
        /// <param name="paymentPlanId">Payment Plan ID</param>
        /// <param name="paymentMethod">Selected payment method</param>
        /// <returns>Redirect to Payment Review page with down payment selected</returns>
        [HttpPost]
        public async Task<ActionResult> PaymentPlanDownPayment(string paymentPlanId, string selectedPaymentMethod)
        {
            if (string.IsNullOrEmpty(paymentPlanId) || string.IsNullOrEmpty(selectedPaymentMethod))
            {
                // Payment plan ID and/or payment method not supplied; log an error and return an appropriate message
                string errorMessage = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanIdPayMethodMissingError");
                Logger.Error(errorMessage);

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = errorMessage;
                return Json(errorMessage, JsonRequestBehavior.DenyGet);
            }
            try
            {
                // Get the payment plan
                paymentPlanId = JsonConvert.DeserializeObject<string>(paymentPlanId);
                PaymentPlan paymentPlan = ServiceClient.GetPaymentPlan(paymentPlanId);
                if (paymentPlan == null)
                {
                    // Payment plan not retrieved; log an error and return an appropriate message
                    string errorMessage = string.Format(GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanUnableToLoadError"), 
                        paymentPlanId);
                    Logger.Error(errorMessage);

                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    Response.StatusDescription = errorMessage;
                    return Json(errorMessage, JsonRequestBehavior.DenyGet);
                }
                if (paymentPlan.DownPaymentAmount > 0 && paymentPlan.DownPaymentDate.HasValue)
                {
                    // Get the person ID
                    string personId = GetEffectivePersonId();

                    // Get and initialize the necessary data to build the Make A Payment model
                    AccountHolder accountHolder = ServiceClient.GetAccountHolder2(personId);
                    FinanceConfiguration config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                    MakeAPaymentModel makeAPaymentModel = null;
                    PaymentsDueModel downPaymentDue = null;

                    // Term Mode
                    if (config.PaymentDisplay == PaymentDisplay.DisplayByTerm)
                    {
                        // Get the term payment due data
                        AccountDue accountDue = ServiceClient.GetPaymentsDueByTermForStudent(personId);

                        // Build the make a payment term model
                        makeAPaymentModel = Models.Payments.MakeAPaymentModel.Build(accountDue, config, accountHolder, CurrentUser);
                        if (makeAPaymentModel == null || makeAPaymentModel.TermModels == null)
                        {
                            string errorMessage = string.Format(GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanPaymentDataCouldNotLoadError"), personId);
                            Logger.Error(errorMessage);

                            Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            Response.StatusDescription = errorMessage;
                            return Json(errorMessage, JsonRequestBehavior.DenyGet);
                        }

                        // De-select all selected items
                        DeselectItemsInAllTerms(makeAPaymentModel.TermModels);

                        // Identify the Make a Payment item representing the payment plan down payment
                        downPaymentDue = FindPaymentPlanDownPaymentItem(makeAPaymentModel.TermModels.
                            SelectMany(term => term.PaymentsDue), paymentPlanId, paymentPlan.DownPaymentAmount, paymentPlan.DownPaymentDate.Value);

                    }
                    // PCF Mode
                    else
                    {
                        // Get the period payment due data
                        AccountDuePeriod accountDuePeriod = ServiceClient.GetPaymentsDueByPeriodForStudent(personId);

                        // Build the make a payment period model
                        makeAPaymentModel = Models.Payments.MakeAPaymentModel.Build(accountDuePeriod, config, accountHolder, CurrentUser);
                        if (makeAPaymentModel == null || makeAPaymentModel.PeriodModels == null)
                        {
                            string errorMessage = string.Format(GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanPaymentDataCouldNotLoadError"), personId);
                            Logger.Error(errorMessage);

                            Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            Response.StatusDescription = errorMessage;
                            return Json(errorMessage, JsonRequestBehavior.DenyGet);
                        }

                        // De-select all selected items
                        makeAPaymentModel.PeriodModels.ForEach(pm => DeselectItemsInAllTerms(pm.Terms));
                        
                        // Identify the Make a Payment item representing the payment plan down payment
                        downPaymentDue = FindPaymentPlanDownPaymentItem(makeAPaymentModel.PeriodModels.
                            SelectMany(period => period.Terms).
                            SelectMany(term => term.PaymentsDue), paymentPlanId, paymentPlan.DownPaymentAmount, paymentPlan.DownPaymentDate.Value);
                    }

                    // No matching payment plan down payment found on Make a Payment; log information
                    if (downPaymentDue == null)
                    {
                        string errorMessage = string.Format(GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanDownPaymentMissingError"), 
                            paymentPlanId,
                            paymentPlan.DownPaymentAmount.ToString("C",CultureInfo.CurrentCulture),
                            paymentPlan.DownPaymentDate.Value.ToShortDateString());
                        Logger.Error(errorMessage);
                        throw new ApplicationException(errorMessage);
                    }

                    // Down payment found; pre-select the down payment item, set the payment method on the MAP model, and return the model as JSON
                    // Page JS will subsequently funnel this model back into the Payment Review process
                    downPaymentDue.IsSelected = true;
                    makeAPaymentModel.SelectedPaymentMethod = JsonConvert.DeserializeObject<AvailablePaymentMethod>(selectedPaymentMethod);
                    return Json(makeAPaymentModel, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    // Payment plan down payment not required or due date missing; log the information
                    string message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanDownPaymentNotRequiredMessage");
                    Logger.Info(message);
                    throw new ApplicationException(message);
                }

            }
            // Catch any unhandled errors at a global level and serve up an appropriate JSON response
            catch (Exception ex)
            {
                string errorMessage = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanDownPaymentGenericError");
                Logger.Error(ex, errorMessage);

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = errorMessage;
                return Json(errorMessage, JsonRequestBehavior.DenyGet);
            }
        }

        /// <summary>
        /// Converts the selected payments due for a term into payment items
        /// </summary>
        /// <param name="source">Summary model for a term</param>
        /// <returns>List of payment items for the term's selected payments due</returns>
        private List<PaymentItem> ConvertTermSelectedItemsToPaymentItems(TermSummaryModel source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            var paymentItems = new List<PaymentItem>();

            // Identify just the selected payments due for the term
            var selectedPaymentsDue = source.PaymentsDue.Where(pd => pd.IsSelected && pd.IsPayable);

            // Build a PaymentItem DTO for each selected payment due
            foreach (var item in selectedPaymentsDue)
            {
                var paymentItem = new PaymentItem()
                {
                    AccountType = item.ReceivableTypeCode,
                    DepositDueId = item.DepositDueId,
                    Description = item.Description,
                    InvoiceId = item.InvoiceId,
                    Overdue = item.IsOverdue,
                    PaymentAmount = item.PaidAmount.GetValueOrDefault(),
                    PaymentComplete = true,
                    PaymentPlanId = item.PaymentPlanId,
                    Term = source.TermCode == "NON-TERM" ? string.Empty : source.TermCode
                };
                paymentItems.Add(paymentItem);
            }

            return paymentItems;
        }

        /// <summary>
        /// Converts the selected payments due for a term into payment items
        /// </summary>
        /// <param name="source">Summary model for a term</param>
        /// <returns>List of payment items for the term's selected payments due</returns>
        private List<PaymentItem> ConvertPeriodSelectedItemsToPaymentItems(PeriodSummaryModel source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            var paymentItems = new List<PaymentItem>();

            foreach (var term in source.Terms)
            {
                paymentItems.AddRange(ConvertTermSelectedItemsToPaymentItems(term));
            }

            return paymentItems;
        }

        /// <summary>
        /// Deselects all items in each term in a collection of <see cref="TermSummaryModel"/> objects.
        /// </summary>
        /// <param name="terms">Collection of <see cref="TermSummaryModel"/> objects</param>
        private void DeselectItemsInAllTerms(List<TermSummaryModel> terms)
        {
            if (terms == null)
            {
                throw new ArgumentNullException("Term model collection cannot be null.");
            }
            foreach(var term in terms)
            {
                if (term.PaymentsDue != null)
                {
                    term.PaymentsDue.ForEach(paymentDue => paymentDue.IsSelected = false);
                }
            }
        }

        /// <summary>
        /// Identifies a payment plan down payment from a collection of <see cref="PaymentsDueModel"/> objects.
        /// </summary>
        /// <param name="paymentsDue">Collection of <see cref="PaymentsDueModel"/> objects</param>
        /// <param name="paymentPlanId">Payment Plan ID</param>
        /// <param name="downPaymentAmount">Down Payment Amount</param>
        /// <param name="downPaymentDate">Down Payment Due Date</param>
        /// <returns>Payment plan down payment <see cref="PaymentsDueModel"/> object.</returns>
        private PaymentsDueModel FindPaymentPlanDownPaymentItem(IEnumerable<PaymentsDueModel> paymentsDue, string paymentPlanId, decimal downPaymentAmount, DateTime downPaymentDate)
        {
            if (paymentsDue == null || string.IsNullOrEmpty(paymentPlanId))
            {
                return null;
            }
            return paymentsDue.Where(paymentDue => paymentDue.PaymentPlanId == paymentPlanId &&
                paymentDue.DueDate == downPaymentDate &&
                paymentDue.DueAmount == downPaymentAmount).FirstOrDefault();
        }
    }
}
