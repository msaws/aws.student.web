﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using Ellucian.Web.Student.Areas.Finance.Models.RegistrationActivity;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Models.Search;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Areas.Finance.Controllers
{
    /// <summary>
    /// The RegistrationActivityController class provides the various actions for Registration Activity. 
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class RegistrationActivityController : BaseStudentFinanceController
    {
        private readonly Settings _settings;
        private readonly IAdapterRegistry _adapterRegistry;

        private const string _ImmediatePaymentDistribution = "PA";

        public RegistrationActivityController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _settings = settings;
            _adapterRegistry = adapterRegistry;
        }

        /// <summary>
        /// Action method for the AccountActivity page.
        /// </summary>
        /// <returns>AccountActivity view</returns>
        [LinkHelp]
        [PageAuthorize("regAct")]
        public ActionResult Index()
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "RegistrationActivityTitle");

            return View("Index");
        }

        /// <summary>
        /// Action method for the Registration Activity Admin page.
        /// </summary>
        /// <param name="id">Person ID</param>
        /// <param name="changeUser">Flag indicating whether a user change is occurring</param>
        /// <returns>Registration Activity Administrator view</returns>
        [PageAuthorize("regActAdmin")]
        [LinkHelp]
        public ActionResult Admin(string id, bool changeUser = false)
        {
            ViewBag.Title = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "RegistrationActivityTitle") + GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "AdminPageTitle");
            ViewBag.IsAdmin = true;
            ViewBag.PersonId = string.IsNullOrEmpty(id) ? null : id;
            ViewBag.ShowDynamicBackLink = !string.IsNullOrEmpty(id) && changeUser;
            if (!string.IsNullOrEmpty(id) && !changeUser)
            {
                return View("Index");
            }
            else
            {
                PersonSearchModel model = FinanceAdminUtility.CreatePersonSearchModel("RegistrationActivity");
                List<SearchActionModel> backLinkExclusions = new List<SearchActionModel>(model.BackLinkExclusions);
                backLinkExclusions.Add(new SearchActionModel("ReprintSummaryAdmin", "RegistrationActivity", "Finance"));
                backLinkExclusions.Add(new SearchActionModel("DisplayPaymentPlanSummary", "RegistrationActivity", "Finance"));
                model.BackLinkExclusions = backLinkExclusions;
                if (!string.IsNullOrEmpty(id))
                {
                    model.DynamicBackLinkDefault.RouteValues.Add("personId", id);
                }
                return View("Admin", model);
            }
        }

        /// <summary>
        /// AJAX method to get registration activity information
        /// </summary>
        /// <param name="personId">ID of the person for whom registration activity will be retrieved</param>
        /// <returns>JSON-formatted RegistrationActivityModel</returns>
        [HttpPost]
        public async Task<JsonResult> GetRegistrationActivityViewModel(string personId = null)
        {
            try
            {
                // Use the person ID (if provided) or pull it from the current user
                if (string.IsNullOrEmpty(personId)) personId = GetEffectivePersonId();

                // Get the data for the current student
                var paymentControls = ServiceClient.GetStudentPaymentControls(personId);
                var accountHolder = ServiceClient.GetAccountHolder2(personId);

                // No registration activity
                if (CollectionUtility.IsNullOrEmpty(paymentControls))
                {
                    var noActivityModel = RegistrationActivityModel.Build(accountHolder, CurrentUser);
                    noActivityModel.Message = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "NoRegistrationActivityMessage");
                    await SetPrivacyPropertiesForAdminModel(noActivityModel);
                    
                    ViewBag.json = JsonConvert.SerializeObject(noActivityModel);
                    return Json(noActivityModel, JsonRequestBehavior.AllowGet);
                }

                // Build the registration activity model, the resulting view, and the JSON version of the model
                var activityModel = await GetRegistrationActivityModel(paymentControls, accountHolder, CurrentUser);

                // Display the form
                return Json(activityModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [LinkHelp]
        [HttpPost]
        public async Task<ActionResult> DisplayPaymentPlanSummary(string paymentControlJson)
        {
            try
            {
                var paymentControl = JsonConvert.DeserializeObject<RegistrationPaymentControl>(paymentControlJson);

                var accountHolder = ServiceClient.GetAccountHolder2(paymentControl.StudentId);
                var planApproval = ServiceClient.GetPaymentPlanApproval(paymentControl.LastPlanApprovalId);
                var ackDocument = ServiceClient.GetApprovalDocument(planApproval.AcknowledgementDocumentId);
                var termsResponse = ServiceClient.GetApprovalResponse(planApproval.TermsResponseId);
                var termsDocument = ServiceClient.GetApprovalDocument(termsResponse.DocumentId);
                var paymentPlan = ServiceClient.GetPaymentPlan(planApproval.PaymentPlanId);

                var planDetailsModel = PaymentPlanDetailsModel.BuildPlanAcknowledgement(paymentPlan, planApproval, string.Join("", ackDocument.Text), termsDocument, termsResponse, 0, false);

                // Display the Payment Plan Acknowledgement form
                var payPlanAckModel = PaymentPlanAcknowledgementModel.Build(planDetailsModel, paymentControl, null, CurrentUser);
                payPlanAckModel.PersonId = planDetailsModel.PaymentPlan.PersonId;
                payPlanAckModel.PersonName = accountHolder.PreferredName;
                await SetPrivacyPropertiesForAdminModel(payPlanAckModel);

                ViewBag.HelpContentId = "ImmediatePaymentsPaymentPlanAcknowledgement";
                ViewBag.json = JsonConvert.SerializeObject(payPlanAckModel);
                ViewBag.PersonId = accountHolder.Id;
                ViewBag.AdminWorkflow = accountHolder.Id != GetEffectivePersonId();
                ViewBag.IsAdmin = CurrentUser.PersonId != accountHolder.Id && !CurrentUser.ProxySubjects.Any();

                return View("PaymentPlanSummary", payPlanAckModel);
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Print the payment plan summary 
        /// </summary>
        /// <param name="payControlId">Registration Payment Control ID</param>
        /// <param name="receivedDownPayment">Down Payment Amount</param>
        /// <param name="model">JSON-formatted payment acknowledgement model</param>
        public ActionResult PrintPaymentPlanSummary(string payControlId, decimal receivedDownPayment)
        {
            var paymentControl = ServiceClient.GetRegistrationPaymentControl(payControlId);
            var planApproval = ServiceClient.GetPaymentPlanApproval(paymentControl.LastPlanApprovalId);
            var ackDocument = ServiceClient.GetApprovalDocument(planApproval.AcknowledgementDocumentId);
            var termsResponse = ServiceClient.GetApprovalResponse(planApproval.TermsResponseId);
            var termsDocument = ServiceClient.GetApprovalDocument(termsResponse.DocumentId);
            var paymentPlan = ServiceClient.GetPaymentPlan(planApproval.PaymentPlanId);

            var planDetailsModel = PaymentPlanDetailsModel.BuildPlanAcknowledgement(paymentPlan, planApproval, string.Join("", ackDocument.Text), termsDocument, termsResponse, receivedDownPayment, true);

            // Display the Payment Plan Acknowledgement form
            var payPlanAckModel = PaymentPlanAcknowledgementModel.Build(planDetailsModel, paymentControl, null, CurrentUser);
            payPlanAckModel.IsPrintable = true;

            ViewBag.json = JsonConvert.SerializeObject(payPlanAckModel);
            ViewBag.PersonId = paymentControl.StudentId;

            return View("PrintPaymentPlanAcknowledgement", payPlanAckModel);
        }

        /// <summary>
        /// Display a printable version of the registration summary form
        /// </summary>
        /// <param name="payControlId">Payment control ID</param>
        /// <param name="ackDateTime">Date/time on the acknowledgement</param>
        /// <returns>Registration Summary form</returns>
        [LinkHelp]
        [HttpPost]
        public async Task<ActionResult> ReprintSummary(string model)
        {
            var localModel = JsonConvert.DeserializeObject<RegistrationActivityModel>(model);

            var newModel = await GetRegistrationSummaryModel(localModel.PaymentControl, localModel.IsAdminUser, localModel.PersonId, localModel.PersonName);

            // Display the form
            ViewBag.HelpContentId = "RegistrationActivityReprintSummary";
            ViewBag.SubMenuOverride = "Index";
            ViewBag.PersonId = newModel.PersonId;

            ViewBag.json = JsonConvert.SerializeObject(newModel);

            // Throw an error if the T&C text is null/empty
            if (CollectionUtility.IsNullOrEmpty(newModel.Acceptance.TermsText) || newModel.Acceptance.TermsText.Count() == 0)
            {
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "IncompleteSetupText");
            }

            return View(newModel);
        }

        /// <summary>
        /// Display a printable version of the registration summary form for the administrative user
        /// </summary>
        /// <param name="payControlId">Payment control ID</param>
        /// <param name="ackDateTime">Date/time on the acknowledgement</param>
        /// <returns>Registration Summary form</returns>
        [LinkHelp]
        [HttpPost]
        public async Task<ActionResult> ReprintSummaryAdmin(string model)
        {
            var localModel = JsonConvert.DeserializeObject<RegistrationActivityModel>(model);

            var newModel = await GetRegistrationSummaryModel(localModel.PaymentControl, localModel.IsAdminUser, localModel.PersonId, localModel.PersonName);

            // Display the form
            ViewBag.HelpContentId = "RegistrationActivityReprintSummaryAdmin";
            ViewBag.SubMenuOverride = "Admin";
            ViewBag.PersonId = localModel.PersonId; 
            ViewBag.json = JsonConvert.SerializeObject(newModel);

            // Throw an error if the T&C text is null/empty
            if (CollectionUtility.IsNullOrEmpty(newModel.Acceptance.TermsText) || newModel.Acceptance.TermsText.Count() == 0)
            {
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "IncompleteSetupText");
            }

            await SetPrivacyPropertiesForAdminModel(newModel);

            return View(newModel);
        }

        /// <summary>
        /// Display a printable version of the registration summary form
        /// </summary>
        /// <param name="payControlId">Payment control ID</param>
        /// <param name="ackDateTime">Date/time on the acknowledgement</param>
        /// <returns>Printable registration summary form</returns>
        [HttpGet]
        public async Task<ActionResult> PrintSummary(string payControlId, string ackDateTime = null)
        {
            RegistrationPaymentControl paymentControl = null;
            if (String.IsNullOrEmpty(payControlId))
            {
                // Get the first payment control for the current student that is not Complete
                paymentControl = ServiceClient.GetStudentPaymentControls(GetEffectivePersonId()).Where(x => x.PaymentStatus != RegistrationPaymentStatus.Complete).First();
            }
            else
            {
                // Get the specified reg payment control and the summary model for it
                paymentControl = ServiceClient.GetRegistrationPaymentControl(payControlId);
            }

            // Build the summary model from the payment control
            var model = await GetRegistrationSummaryModel(paymentControl, false, string.Empty, string.Empty);

            // Replace the model's date/time with that from the acknowledgement
            if (!String.IsNullOrEmpty(ackDateTime))
            {
                DateTime dateTimeStamp;
                if (DateTime.TryParse(ackDateTime, out dateTimeStamp))
                {
                    model.Acceptance.AcknowledgementDateTime = dateTimeStamp;
                }
            }

            // Flag the model as being printed
            model.IsPrintable = true;

            // Display the form
            ViewBag.json = JsonConvert.SerializeObject(model);
            ViewBag.PersonId = model.PersonId;


            // Display an error if the T&C text is null/empty
            if (CollectionUtility.IsNullOrEmpty(model.Acceptance.TermsText) || model.Acceptance.TermsText.Count() == 0)
            {
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "IncompleteSetupText");
            }

            return PartialView(model);
        }

        /// <summary>
        /// Get the registration summary model for a student's payment controls
        /// </summary>
        /// <param name="paymentControls">List of registration payment controls</param>
        /// <returns>The registration activity model</returns>
        private async Task<RegistrationActivityModel> GetRegistrationActivityModel(IEnumerable<RegistrationPaymentControl> paymentControls, AccountHolder accountHolder, ICurrentUser currentUser)
        {
            // Get the needed static data
            var terms = ServiceClient.GetTerms();
            var config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
            var periods = config.Periods;

            // Get the term utility
            var termUtility = new TermPeriodsUtility(terms, periods);

            // Sort payment controls using term sort order
            paymentControls = paymentControls.OrderBy(x => termUtility.GetTermSortOrder(x.TermId)).Reverse();

            // Get the display mode for Account Activity
            bool displayByTerm = config.ActivityDisplay == ActivityDisplay.DisplayByTerm;

            // Build the registration activity model, the resulting view, and the JSON version of the model
            RegistrationActivityModel model = RegistrationActivityModel.Build(paymentControls, termUtility, displayByTerm, accountHolder, currentUser);
            
            // Set privacy properties for model
            await SetPrivacyPropertiesForAdminModel(model);
            return model;
        }

        /// <summary>
        /// Get the registration summary model for a payment control
        /// </summary>
        /// <param name="paymentControl">Registration payment control</param>
        /// <returns>The registration summary model</returns>
        private async Task<RegistrationSummaryModel> GetRegistrationSummaryModel(RegistrationPaymentControl paymentControl, bool isAdmin, string personId, string personName)
        {
            // Get the needed static data
            var ipcControl = await ServiceClientCache.GetCachedImmediatePaymentControl(ServiceClient);
            var terms = ServiceClient.GetTerms();
            var locations = ServiceClient.GetLocations();
            var buildings = ServiceClient.GetBuildings();
            var receivableTypes = ServiceClient.GetReceivableTypes();

            // Get the term utility
            var termUtility = new TermPeriodsUtility(terms);

            // Get the document text to display based on the last time the student approved the T&C
            var lastTermsApproval = ServiceClient.GetRegistrationTermsApproval2(paymentControl.LastTermsApprovalId);
            
            var ackText = new List<string>();
            if (!string.IsNullOrEmpty(lastTermsApproval.AcknowledgementDocumentId))
            {
                ackText.AddRange(ServiceClient.GetApprovalDocument(lastTermsApproval.AcknowledgementDocumentId).Text);
            }

            var termsText = new List<string>();
            if (!string.IsNullOrEmpty(lastTermsApproval.TermsResponseId))
            {
                var termsApprovalDocumentId = ServiceClient.GetApprovalResponse(lastTermsApproval.TermsResponseId).DocumentId;
                termsText.AddRange(ServiceClient.GetApprovalDocument(termsApprovalDocumentId).Text);
            } 

            // Build the list of sections that will be displayed
            var sectionIds = lastTermsApproval.SectionIds.ToList();

            // Get the sections, their courses, and instructor info
            var sections = ServiceClient.GetSections4(sectionIds);
            var courses = new List<Course2>();
            var faculty = new List<Faculty>();
            foreach (var section in sections)
            {
                courses.Add(ServiceClient.GetCourse(section.CourseId));
                foreach (var id in section.FacultyIds)
                {
                    // Add missing faculty to the list
                    var temp = faculty.Where(x => x.Id == id).FirstOrDefault();
                    if (temp == null)
                    {
                        faculty.Add(ServiceClient.GetFaculty(id));
                    }
                }
            }

            // Get the invoices on the acknowledgement and the map of account types to distributions
            var invoices = ServiceClient.GetInvoices(lastTermsApproval.InvoiceIds);
            var receivableTypeCodes = invoices.Select(x => x.ReceivableTypeCode).Distinct().ToList();
            var distributions = ServiceClient.GetPaymentDistributions(paymentControl.StudentId, receivableTypeCodes, _ImmediatePaymentDistribution).ToList();
            var distributionMap = new Dictionary<string, string>();
            for (int i = 0; i < receivableTypeCodes.Count; i++)
            {
                distributionMap.Add(receivableTypeCodes[i], distributions[i]);
            }

            var returnUrl = Url.Action("Index");

            // Build the registration summary model, the resulting view, and the JSON version of the model
            var model = RegistrationSummaryModel.Build(ipcControl, termUtility, locations, buildings, receivableTypes, returnUrl,
                paymentControl, ackText, sections, courses, faculty, invoices, distributionMap, termsText);
            // Overwrite the date with that from the acceptance
            model.Acceptance.AcknowledgementDateTime = lastTermsApproval.AcknowledgementTimestamp;
            model.IsAdminUser = isAdmin;
            model.PersonId = personId;
            model.PersonName = personName;

            return model;
        }

        /// <summary>
        /// Get a document for a specified payment control
        /// </summary>
        /// <param name="payControlId">Payment Control ID</param>
        /// <param name="documentId">Document ID</param>
        /// <returns>The merged text document</returns>
        private TextDocument GetPaymentControlDocument(string payControlId, string documentId)
        {
            var document = new TextDocument();
            if (String.IsNullOrEmpty(payControlId) || String.IsNullOrEmpty(documentId))
            {
                return document;
            }

            document = ServiceClient.GetRegistrationPaymentControlDocument(payControlId, documentId);

            return document;
        }
    }  
}
