﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Utility;
using Newtonsoft.Json;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;

namespace Ellucian.Web.Student.Areas.Finance.Controllers
{
    /// <summary>
    /// The ImmediatePaymentsController class provides the various actions for Immediate Payments. 
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class ImmediatePaymentsController : BaseStudentController
    {
        private readonly Settings _settings;
        private readonly IAdapterRegistry _adapterRegistry;

        private const string _ImmediatePaymentDistribution = "PA";
        private const string _CheckType = "CK";

        public ImmediatePaymentsController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _settings = settings;
            _adapterRegistry = adapterRegistry;
        }

        /// <summary>
        /// Display the registration summary form
        /// </summary>
        /// <remarks>This method is called when the Pay for Registration menu option is selected.  It simply invokes the other
        /// method to display the registration summary form.</remarks>
        [LinkHelp]
        [PageAuthorize("payForReg")]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return await Index(null);
        }

        /// <summary>
        /// Route the user back to the start of the workflow
        /// </summary>
        /// <remarks>If a user times out while on this form, they are taken back to the login form, then rerouted back here. However, 
        /// this form is part of a workflow, and it cannot be an entry point for the user.  So, this simply takes the user
        /// back to the start of the workflow when this occurs.</remarks>
        [HttpGet]
        public ActionResult PaymentOptions()
        {
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Route the user back to the start of the workflow
        /// </summary>
        /// <remarks>If a user times out while on this form, they are taken back to the login form, then rerouted back here. However, 
        /// this form is part of a workflow, and it cannot be an entry point for the user.  So, this simply takes the user
        /// back to the start of the workflow when this occurs.</remarks>
        [HttpGet]
        public ActionResult PaymentReview()
        {
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display the registration summary form
        /// </summary>
        /// <remarks>This method is called when the student clicks the Pay for Registration button from Student Planning</remarks>
        [LinkHelp]
        [PageAuthorize("payForReg")]
        [HttpPost]
        public async Task<ActionResult> Index(string model)
        {
            ImmediatePaymentInputModel localModel = null;
            string returnUrl = null;
            RegistrationPaymentControl paymentControl = null;
            IEnumerable<RegistrationPaymentControl> paymentControls = null;

            if (!String.IsNullOrEmpty(model))
            {
                localModel = JsonConvert.DeserializeObject<ImmediatePaymentInputModel>(model);
                returnUrl = GetReturnUrl(localModel);
            }

            if (localModel != null && localModel.PaymentControl != null)
            {
                // A payment control was specified - refresh and use it
                paymentControl = ServiceClient.GetRegistrationPaymentControl(localModel.PaymentControl.Id);
                if (paymentControl.PaymentStatus == RegistrationPaymentStatus.Complete)
                {
                    // Nothing to pay - display a message and be done
                    var completeModel = GetCompleteModel(null, returnUrl);
                    completeModel.Message.Add(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "NoOutstandingPaymentsMessage"));

                    ViewBag.json = JsonConvert.SerializeObject(completeModel);
                    return View("PaymentComplete", completeModel);
                }
            }
            else
            {
                // Get the payment controls for the current student - exclude any that are Complete
                paymentControls = ServiceClient.GetStudentPaymentControls(GetEffectivePersonId()).Where(x => x.PaymentStatus != RegistrationPaymentStatus.Complete);
                // If there are none, then the student has no payment arrangements to make
                if (CollectionUtility.IsNullOrEmpty(paymentControls))
                {
                    // Nothing to pay - display a message and be done
                    var completeModel = GetCompleteModel(null, returnUrl);
                    completeModel.Message.Add(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "NoOutstandingPaymentsMessage"));

                    ViewBag.json = JsonConvert.SerializeObject(completeModel);
                    return View("PaymentComplete", completeModel);
                }
                // Student has outstanding payment requirements - process the first payment control
                paymentControl = paymentControls.First();
            }

            var newModel = await GetRegistrationSummaryModel(paymentControl, returnUrl);

            ViewBag.json = JsonConvert.SerializeObject(newModel);

            // Display an error if the T&C text is null/empty
            if (CollectionUtility.IsNullOrEmpty(newModel.Acceptance.TermsText) || newModel.Acceptance.TermsText.Count() == 0)
            {
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "IncompleteSetupText");
            }

            // Display the form
            return View("Index", newModel);
        }

        /// <summary>
        /// Display a printable version of the registration summary form
        /// </summary>
        /// <param name="payControlId">Payment control ID</param>
        /// <param name="ackDateTime">Date/time on the acknowledgement</param>
        /// <returns>Printable registration summary form</returns>
        [HttpGet]
        public async Task<ActionResult> PrintSummary(string payControlId, string ackDateTime = null)
        {
            RegistrationPaymentControl paymentControl = null;
            if (String.IsNullOrEmpty(payControlId))
            {
                // Get the first payment control for the current student that is not Complete
                paymentControl = ServiceClient.GetStudentPaymentControls(GetEffectivePersonId()).Where(x => x.PaymentStatus != RegistrationPaymentStatus.Complete).First();
            }
            else
            {
                // Get the specified reg payment control and the summary model for it
                paymentControl = ServiceClient.GetRegistrationPaymentControl(payControlId);
            }
            var model = await GetRegistrationSummaryModel(paymentControl);

            // Replace the model's date/time with that from the acknowledgement
            if (!String.IsNullOrEmpty(ackDateTime))
            {
                DateTime dateTimeStamp;
                if (DateTime.TryParse(ackDateTime, out dateTimeStamp))
                {
                    model.Acceptance.AcknowledgementDateTime = dateTimeStamp;
                }
            }

            // Flag the model as being printed
            model.IsPrintable = true;

            // Display the form
            ViewBag.json = JsonConvert.SerializeObject(model);

            // Display an error if the T&C text is null/empty
            if (CollectionUtility.IsNullOrEmpty(model.Acceptance.TermsText) || model.Acceptance.TermsText.Count() == 0)
            {
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "IncompleteSetupText");
            }

            return PartialView(model);
        }

        /// <summary>
        /// Process input from registration summary and display payment options form
        /// </summary>
        /// <param name="model">JSON-formatted registration summary model</param>
        [LinkHelp]
        [HttpPost]
        public async Task<ActionResult> PaymentOptions(string model)
        {
            if (string.IsNullOrEmpty(model))
            {
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentOptionsInputModelMissingMessage");
                ViewBag.json = "error: " + ViewBag.Error;
                return View("Index", new RegistrationSummaryModel());
            }
            var localModel = JsonConvert.DeserializeObject<RegistrationSummaryModel>(model);

            // If the terms were accepted, then update Colleague with the acceptance
            // NOTE: If the terms were not accepted, then that means the student already has an
            // Accepted status.  The only way to get to this point is to have the Accepted status
            // or accept the terms and conditions.
            var acceptance = localModel.Acceptance;
            if (localModel.AcceptTermsAndConditions)
            {
                // Save the T&C approval - update the acceptance structure with the current date and time
                acceptance.ApprovalReceived = DateTime.Now;

                // Process the approval
                try
                {
                    var approval = ServiceClient.AcceptRegistrationTerms2(acceptance);
                }
                catch
                {
                    // Return an error message
                    ViewBag.HelpContentId = "ImmediatePaymentsIndex";
                    ViewBag.Error = "There was a problem processing the acceptance - error!";
                    ViewBag.json = JsonConvert.SerializeObject(new RegistrationSummaryModel());
                    return View("Index", new RegistrationSummaryModel());
                }
            }

            // Acceptance was successful - Get the payment options and display them
            try
            {
                var paymentOptions = ServiceClient.GetRegistrationPaymentOptions(acceptance.PaymentControlId);

                // If the student is on a payment plan, see if it's an IPC plan.  If so, take them directly to
                // the down payment entry form.
                if (paymentOptions.ChargesOnPaymentPlan)
                {
                    // Build a list of all the payment plans this registration is tied to
                    var invoices = ServiceClient.GetInvoices(acceptance.InvoiceIds);
                    var allPaymentPlans = invoices.SelectMany(x => x.Charges).Where(x => x.PaymentPlanIds != null && x.PaymentPlanIds.Count > 0).SelectMany(x => x.PaymentPlanIds).Distinct();
                    // If the plan on the control record is in this list, then we need to make a down payment
                    var paymentControl = ServiceClient.GetRegistrationPaymentControl(acceptance.PaymentControlId);
                    if (allPaymentPlans.Contains(paymentControl.PaymentPlanId))
                    {
                        var plan = ServiceClient.GetPaymentPlan(paymentControl.PaymentPlanId);

                        // If the payment plan did not have a down payment, or if the student previously paid the down payment, go to Registration Complete
                        if (plan.DownPaymentAmount == 0 || plan.DownPaymentAmountPaid > 0)
                        {
                            // Update the payment control as complete
                            localModel.PaymentControl = UpdatePaymentStatus(localModel.PaymentControl);

                            // Display the Payment Complete form
                            var completeModel = GetCompleteModel(null, localModel.ReturnUrl);
                            completeModel.Message.Add(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentNotRequiredMessage"));

                            ViewBag.json = JsonConvert.SerializeObject(completeModel);
                            return View("PaymentComplete", completeModel);
                        }

                        // Take the user to the down payment form, which is the 3rd section of the Payment Options form.
                        return await DownPayment(localModel);
                    }
                }
                // If the student has a zero balance or is on a payment plan, update the payment control as
                // Complete, and go to the completion form
                if (paymentOptions.ChargesOnPaymentPlan || paymentOptions.RegistrationBalance == 0)
                {
                    // Update the payment control as complete
                    localModel.PaymentControl = UpdatePaymentStatus(localModel.PaymentControl);

                    // Display the Payment Complete form
                    var completeModel = GetCompleteModel(null, localModel.ReturnUrl);
                    completeModel.Message.Add(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentNotRequiredMessage"));

                    ViewBag.json = JsonConvert.SerializeObject(completeModel);
                    return View("PaymentComplete", completeModel);
                }

                // Build the payment options model
                var config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                localModel.PaymentControl = ServiceClient.GetRegistrationPaymentControl(localModel.PaymentControl.Id);
                var optionsModel = PaymentOptionsModel.Build(paymentOptions, config.PaymentMethods, localModel);

                // Override the help for payment options view
                ViewBag.HelpContentId = "ImmediatePaymentsPaymentOptionsMakePayment";
                ViewBag.json = JsonConvert.SerializeObject(optionsModel);
                return View("PaymentOptions", optionsModel);
            }
            catch
            {
                ViewBag.Error = "Error getting the payment options - see log for details.";
                ViewBag.json = "error: " + ViewBag.Error;
                return View("Index", new RegistrationSummaryModel());
            }

        }

        /// <summary>
        /// AJAX method to get the details of a payment plan
        /// </summary>
        /// <param name="inputModel">JSON-formatted input model</param>
        /// <returns>JSON response</returns>
        [HttpPost]
        public JsonResult GetPlanDetails(PaymentOptionsModel inputModel)
        {
            try
            {
                var template = ServiceClient.GetPaymentPlanTemplate(inputModel.PaymentPlanTemplateId);
                var proposedPlan = ServiceClient.GetPaymentControlProposedPaymentPlan(inputModel.PaymentControl.Id, inputModel.PlanReceivableType);
                var accountHolder = ServiceClient.GetAccountHolder2(inputModel.PaymentControl.StudentId);
                var ackDoc = new List<string>() { GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PaymentPlanDetailsStudentMessage") };
                var termsDoc = GetPaymentControlDocument(inputModel.PaymentControl.Id, template.TermsAndConditionsDocumentId);
                var planDetailsModel = PaymentPlanDetailsModel.BuildPlanPreview(proposedPlan, accountHolder, ackDoc, termsDoc.Text);

                var newModel = PaymentOptionsModel.BuildPlanDetails(inputModel, planDetailsModel);
                return Json(newModel, JsonRequestBehavior.DenyGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.DenyGet);
            }
        }

        /// <summary>
        /// AJAX method to process a user's acceptance of the payment plan's terms and conditions
        /// </summary>
        /// <param name="inputModel">JSON-formatted input model</param>
        /// <returns>JSON response</returns>
        [HttpPost]
        public JsonResult AcceptTermsAndConditions(PaymentOptionsModel inputModel)
        {
            // If the terms were accepted, then update Colleague with the acceptance
            var acceptance = inputModel.Acceptance;
            if (inputModel.AcceptTermsAndConditions)
            {
                // Save the T&C approval - update the acceptance structure with the current date and time
                acceptance.ApprovalReceived = DateTimeOffset.Now;

                try
                {
                    // Process the approval
                    var approval = ServiceClient.AcceptPaymentPlanTerms(acceptance);
                    // Read the newly created payment plan
                    var paymentPlan = ServiceClient.GetPaymentPlan(approval.PaymentPlanId);
                    // Get the updated payment control record with the plan approvals ID
                    inputModel.PaymentControl = ServiceClient.GetRegistrationPaymentControl(inputModel.PaymentControl.Id);

                    return Json(PaymentOptionsModel.BuildDownPayment(inputModel, paymentPlan), JsonRequestBehavior.DenyGet);
                }
                catch (Exception e)
                {
                    Logger.Error(e.ToString());
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json(e.Message, JsonRequestBehavior.DenyGet);
                }
            }

            // TODO: JTM/JPM2 Should never get here... do we need to remove the acceptance check?
            string message = "Terms and Conditions must be accepted before continuing.";
            Logger.Equals(message);
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(message, JsonRequestBehavior.DenyGet);
        }

        public async Task<ActionResult> DownPayment(RegistrationSummaryModel regModel)
        {
            var config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
            var payControl = regModel.PaymentControl;
            var planApproval = ServiceClient.GetPaymentPlanApproval(payControl.LastPlanApprovalId);
            var ackDoc = ServiceClient.GetApprovalDocument(planApproval.AcknowledgementDocumentId);
            var termsResponse = ServiceClient.GetApprovalResponse(planApproval.TermsResponseId);
            var termsDoc = ServiceClient.GetApprovalDocument(termsResponse.DocumentId);
            var paymentPlan = ServiceClient.GetPaymentPlan(payControl.PaymentPlanId);

            var optionsModel = PaymentOptionsModel.BuildDownPayment(paymentPlan, planApproval, config.PaymentMethods, regModel, ackDoc, termsDoc, termsResponse);

            // Override the help for payment options view
            ViewBag.HelpContentId = "ImmediatePaymentsPaymentOptionsMakePayment";
            ViewBag.json = JsonConvert.SerializeObject(optionsModel);
            return View("PaymentOptions", optionsModel);
        }

        /// <summary>
        /// Process the payment options and display the payment review form
        /// </summary>
        /// <param name="model">JSON-formatted payment options model</param> 
        [LinkHelp]
        [HttpPost]
        public async Task<ActionResult> PaymentReview(string model)
        {
            try
            {
                var localModel = JsonConvert.DeserializeObject<PaymentOptionsModel>(model);
                List<Ellucian.Colleague.Dtos.Finance.Payments.Payment> payments = new List<Ellucian.Colleague.Dtos.Finance.Payments.Payment>();

                // Student chose to sign up for a payment plan
                if (!string.IsNullOrEmpty(localModel.PaymentControl.LastPlanApprovalId))
                {
                    if (localModel.ActualDownPaymentAmount > 0)
                    {
                        // Down payment made - set up the payment info
                        localModel.OptionsPaymentMethod = localModel.PlanPaymentMethod;
                        localModel.PaymentAmount = localModel.ActualDownPaymentAmount;
                    }
                    else
                    {
                        // No down payment required - mark payment status as complete
                        localModel.PaymentControl = UpdatePaymentStatus(localModel.PaymentControl);
                        var accountHolder = ServiceClient.GetAccountHolder2(localModel.PaymentControl.StudentId);
                        var planApproval = ServiceClient.GetPaymentPlanApproval(localModel.PaymentControl.LastPlanApprovalId);
                        var ackText = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentPlanAcknowledgementStudentMessage");
                        var termsResponse = ServiceClient.GetApprovalResponse(planApproval.TermsResponseId);
                        var termsDocument = ServiceClient.GetApprovalDocument(termsResponse.DocumentId);
                        var paymentPlan = ServiceClient.GetPaymentPlan(planApproval.PaymentPlanId);

                        var planDetailsModel = PaymentPlanDetailsModel.BuildPlanAcknowledgement(paymentPlan, planApproval, ackText, termsDocument, termsResponse, localModel.ActualDownPaymentAmount, false);

                        // Display the Payment Plan Acknowledgement form
                        var payPlanAckModel = PaymentPlanAcknowledgementModel.Build(planDetailsModel, localModel.PaymentControl, localModel.ReturnUrl, CurrentUser);

                        ViewBag.HelpContentId = "ImmediatePaymentsPaymentPlanAcknowledgement";
                        ViewBag.json = JsonConvert.SerializeObject(payPlanAckModel);
                        return View("PaymentPlanAcknowledgement", payPlanAckModel);
                    }

                    // Determine payment(s) to be made
                    payments.Add(ServiceClient.GetPlanPaymentSummary(localModel.PlanDetails.PaymentPlan.Id, localModel.OptionsPaymentMethod.InternalCode, localModel.ActualDownPaymentAmount, localModel.PaymentControl.Id));
                }
                else
                {
                    // If nothing is being paid, update the payment control and display completion form
                    if (localModel.PaymentAmount == 0)
                    {
                        // Update the payment control as complete
                        var newPaymentControl = UpdatePaymentStatus(localModel.PaymentControl);

                        // Display the Payment Complete form
                        var ipcControl = await ServiceClientCache.GetCachedImmediatePaymentControl(ServiceClient);
                        var defText = GetPaymentControlDocument(newPaymentControl.Id, ipcControl.DeferralAcknowledgementDocumentId);
                        var completeModel = GetCompleteModel(defText.Text, localModel.ReturnUrl);

                        ViewBag.json = JsonConvert.SerializeObject(completeModel);
                        return View("PaymentComplete", completeModel);
                    }

                    // Determine payment(s) to be made
                    payments = ServiceClient.GetRegistrationPaymentSummary(localModel.PaymentControl.Id, localModel.OptionsPaymentMethod.InternalCode, 
                        localModel.PaymentAmount).ToList();
                }

                // Get the various codes needed and the payments to be processed
                var config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                var receivableTypes = ServiceClient.GetReceivableTypes();
                var convenienceFees = ServiceClient.GetConvenienceFees();

                // Take the first payment with an amount greater than zero
                int paymentCount = 0;
                bool found = false;
                for (int i = 0; i < payments.Count && !found; i++)
                {
                    if (payments[i].AmountToPay > 0)
                    {
                        paymentCount = i;
                        found = true;
                    }
                }

                // Build the review model
                var reviewModel = PaymentReviewModel.Build(config, receivableTypes, convenienceFees, localModel, payments, paymentCount);
                reviewModel.Notification = new Notification("", NotificationType.Error);

                // Override the help for payment review view
                ViewBag.HelpContentId = "ImmediatePaymentsPaymentReview";
                ViewBag.json = JsonConvert.SerializeObject(reviewModel);
                return View("PaymentReview", reviewModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                ViewBag.Error = "Error occurred - see log for details.";
                ViewBag.HelpContentId = "ImmediatePaymentsIndex";
                ViewBag.json = JsonConvert.SerializeObject(new RegistrationSummaryModel());
                return View("Index", new RegistrationSummaryModel());
            }
        }

        /// <summary>
        /// Redirect to Index on GET as this means the user has been redirected from the Login with no context
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ECheckEntry()
        {
            return RedirectToAction("Index", "ImmediatePayments", new { area = "Finance" });
        }

        /// <summary>
        /// Process the payment review form and either display the e-check form or transfer control to a payment provider
        /// </summary>
        /// <param name="model">JSON-formatted payment review model</param>
        [LinkHelp]
        [HttpPost]
        public async Task<ActionResult> ECheckEntry(string model)
        {
            var localModel = JsonConvert.DeserializeObject<PaymentReviewModel>(model);
            
            // Process e-checks and credit cards differently
            if (localModel.PaymentMethod.Type == _CheckType)
            {
                // Process an e-check
                var config = await ServiceClientCache.GetCachedFinanceConfiguration(ServiceClient);
                var payerInfo = ServiceClient.GetCheckPayerInformation(GetEffectivePersonId());
                var statesProvinces = await ServiceClient.GetStatesAsync();
                var checkModel = ElectronicCheckEntryModel.Build(config, localModel, payerInfo, statesProvinces);
                checkModel.Notification = new Notification("", NotificationType.Error);

                // Override the help for payment review view
                ViewBag.HelpContentId = "ImmediatePaymentsECheckEntry";
                ViewBag.json = JsonConvert.SerializeObject(checkModel);
                return View("ECheckEntry", checkModel);
            }

            // Process a credit card payment

            // Build the return URL based on the current request context to preserve the original base URL
            localModel.Payment.ReturnUrl = Url.Action("PaymentAcknowledgement", "ImmediatePayments", new { Area = "Finance" }, HttpContext.Request.Url.Scheme);
            localModel.Notification = new Notification("", NotificationType.Error);

            // Send the start payment request to the web service and capture the response
            try
            {
                var response = ServiceClient.StartRegistrationPayment(localModel.Payment);
                if (String.IsNullOrEmpty(response.ErrorMessage))
                {
                    // Redirect to the payment provider URL
                    return Redirect(response.RedirectUrl);
                }

                // Display an error condition in a notification
                localModel.Notification = new Notification(response.ErrorMessage, NotificationType.Error);
            }
            catch (Exception ex)
            {
                // Display an exception's message in a notification
                localModel.Notification = new Notification(ex.Message, NotificationType.Error);
            }

            // At this point, we've got an error condition - redisplay the payment review form
            ViewBag.HelpContentId = "ImmediatePaymentsPaymentReview";
            ViewBag.json = JsonConvert.SerializeObject(localModel);
            return View("PaymentReview", localModel);
        }

        /// <summary>
        /// Redirect to Index on GET as this means the user has been redirected from the Login with no context
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ProcessPayment()
        {
            return RedirectToAction("Index", "ImmediatePayments", new { area = "Finance" });
        }

        /// <summary>
        /// Process the e-check entry form and display the payment acknowledgement
        /// </summary>
        /// <param name="model">JSON-formatted e-check model</param>
        [LinkHelp]
        [HttpPost]
        public ActionResult ProcessPayment(string model)
        {
            var localModel = JsonConvert.DeserializeObject<ElectronicCheckEntryModel>(model);

            // Process the e-check payment
            var result = ServiceClient.ProcessElectronicCheck(localModel.Payment);

            if (String.IsNullOrEmpty(result.ErrorMessage))
            {
                // Payment was successful - get the list of updated payment controls
                var payControls = localModel.Payment.PaymentItems.Where(x => !String.IsNullOrEmpty(x.PaymentControlId));
                var payControlIds = (payControls == null) ? new List<string>() : payControls.Select(x => x.PaymentControlId).Distinct();

                // Proceed to the acknowledgement
                return RedirectToAction("PaymentAcknowledgement", "ImmediatePayments", new
                {
                    area = "Finance",
                    cashRcptsId = result.CashReceiptsId,
                    payControlId = String.Join(",", payControlIds),
                    returnUrl = localModel.ReturnUrl,
                });
            }

            // The payment request returned an error - add it to the model and redisplay the form
            localModel.Notification = new Notification(result.ErrorMessage, NotificationType.Error);
            ViewBag.HelpContentId = "ImmediatePaymentsECheckEntry";
            ViewBag.json = JsonConvert.SerializeObject(localModel);
            return View("ECheckEntry", localModel);
        }

        /// <summary>
        /// Display the payment acknowledgement form
        /// </summary>
        /// <param name="ecPayTransId">ID of the e-commerce transaction</param>
        /// <param name="cashRcptId">ID of the cash receipt</param>
        /// <remarks>Only one of the parameters can be specified.</remarks>
        [LinkHelp]
        [HttpGet]
        public ActionResult PaymentAcknowledgement(string ecPayTransId, string cashRcptsId, string payControlId, string returnUrl)
        {
            if (!string.IsNullOrEmpty(ecPayTransId) && !string.IsNullOrEmpty(cashRcptsId))
            {
                throw new ArgumentException("PaymentAcknowledgement only accepts one argument => EcPayTransId: " + ecPayTransId + " CashRcptsId: " + cashRcptsId);
            }

            try
            {
                // Retrieve the specified receipt
                var receipt = ServiceClient.GetCashReceipt(ecPayTransId, cashRcptsId);

                // Get the payment control
                List<string> payControlIds = null;
                if (String.IsNullOrEmpty(payControlId))
                {
                    var payControls = receipt.Payments.Where(x => !String.IsNullOrEmpty(x.PaymentControlId));
                    payControlIds = (payControls == null) ? new List<string>() : receipt.Payments.Select(x => x.PaymentControlId).Distinct().ToList();
                }
                else
                {
                    payControlIds = payControlId.Split(',').ToList();
                }
                RegistrationPaymentControl payControl = null;
                if (payControlIds == null || payControlIds.Count < 1)
                {
                    payControl = new RegistrationPaymentControl();
                }
                else
                {
                    payControl = ServiceClient.GetRegistrationPaymentControl(payControlIds[0]);
                }

                // Populate the payment acknowledgement model
                var model = PaymentAcknowledgementModel.Build(receipt, payControl, returnUrl);
                model.TransactionId = ecPayTransId;
                model.ReceiptId = cashRcptsId;

                // Override the help for payment review view
                ViewBag.HelpContentId = "ImmediatePaymentsPaymentAcknowledgement";
                ViewBag.json = JsonConvert.SerializeObject(model);
                return View(model);
            }
            catch (Exception ex)
            {
                // Log the error
                Logger.Error(ex.ToString());

                // Add an error to the ViewBag
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "DataRetrievalError");

                // Rethrow the error
                throw;
            }
        }

        /// <summary>
        /// Process the payment acknowledgement form
        /// </summary>
        /// <param name="model">JSON-formatted payment acknowledgement model</param>
        [LinkHelp]
        [HttpPost]
        public ActionResult PaymentAcknowledgement(string model)
        {
            var localModel = JsonConvert.DeserializeObject<PaymentAcknowledgementModel>(model);

            ActionResult result = null;
            if (localModel.PaymentControl != null)
            {
                // Where we take the user next is dependent on the payment status
                switch (localModel.PaymentControl.PaymentStatus)
                {
                    // An error status means that the payment was unsuccessful
                    case RegistrationPaymentStatus.Error:
                        ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentAcknowledgementErrorStatusMessage");
                        result = RedirectToAction("Index");
                        break;

                    // An accepted status means that a payment was made, but multiple payments are needed
                    case RegistrationPaymentStatus.Accepted:
                        List<string> viewMessage = new List<string>() { GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "MultiplePaymentsErrorText") };
                        var acceptModel = GetCompleteModel(viewMessage, localModel.ReturnUrl);

                        ViewBag.json = JsonConvert.SerializeObject(acceptModel);
                        result = View("PaymentComplete", acceptModel);
                        break;

                    // Is payment control complete?
                    case RegistrationPaymentStatus.Complete:
                        List<string> message = new List<string>();
                        if (!string.IsNullOrEmpty(localModel.PaymentControl.LastPlanApprovalId))
                        {
                            var planApproval = ServiceClient.GetPaymentPlanApproval(localModel.PaymentControl.LastPlanApprovalId);
                            var ackText = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentPlanAcknowledgementStudentMessage");
                            var termsResponse = ServiceClient.GetApprovalResponse(planApproval.TermsResponseId);
                            var termsDocument = ServiceClient.GetApprovalDocument(termsResponse.DocumentId);
                            var paymentPlan = ServiceClient.GetPaymentPlan(planApproval.PaymentPlanId);
                            var downPaymentAmount = localModel.Payments.Sum(x => x.NetAmount.GetValueOrDefault());

                            var planDetailsModel = PaymentPlanDetailsModel.BuildPlanAcknowledgement(paymentPlan, planApproval, ackText, termsDocument, termsResponse, downPaymentAmount, false);

                            // Display the Payment Plan Acknowledgement form
                            var payPlanAckModel = PaymentPlanAcknowledgementModel.Build(planDetailsModel, localModel.PaymentControl, localModel.ReturnUrl, CurrentUser);

                            ViewBag.HelpContentId = "ImmediatePaymentsPaymentPlanAcknowledgement";
                            ViewBag.json = JsonConvert.SerializeObject(payPlanAckModel);
                            return View("PaymentPlanAcknowledgement", payPlanAckModel);
                        }
                        else
                        {
                            message.Add(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentAcknowledgementCompleteStatusMessage"));
                        }
                        var completeModel = GetCompleteModel(message, localModel.ReturnUrl);

                        ViewBag.json = JsonConvert.SerializeObject(completeModel);
                        result = View("PaymentComplete", completeModel);
                        break;
                }
            }

            return result;
        }
        /// <summary>
        /// Generate a printable version of the payment acknowledgement
        /// </summary>
        /// <param name="model">JSON-formatted payment acknowledgement model</param>
        public ActionResult PrintPaymentAcknowledgement(string ecPayTransId, string cashRcptsId, string payControlId, string returnUrl)
        {
            if (!string.IsNullOrEmpty(ecPayTransId) && !string.IsNullOrEmpty(cashRcptsId))
            {
                throw new ArgumentException("PaymentAcknowledgement only accepts one argument => EcPayTransId: " + ecPayTransId + " CashRcptsId: " + cashRcptsId);
            }

            try
            {
                // Retrieve the specified receipt
                var receipt = ServiceClient.GetCashReceipt(ecPayTransId, cashRcptsId);

                // Get the payment control
                List<string> payControlIds = null;
                if (String.IsNullOrEmpty(payControlId))
                {
                    var payControls = receipt.Payments.Where(x => !String.IsNullOrEmpty(x.PaymentControlId));
                    payControlIds = (payControls == null) ? new List<string>() : receipt.Payments.Select(x => x.PaymentControlId).Distinct().ToList();
                }
                else
                {
                    payControlIds = payControlId.Split(',').ToList();
                }
                RegistrationPaymentControl payControl = null;
                if (payControlIds == null || payControlIds.Count < 1)
                {
                    payControl = new RegistrationPaymentControl();
                }
                else
                {
                    payControl = ServiceClient.GetRegistrationPaymentControl(payControlIds[0]);
                }

                // Populate the payment acknowledgement model
                var model = PaymentAcknowledgementModel.Build(receipt, payControl, returnUrl);

                // Override the help for payment review view
                ViewBag.HelpContentId = "ImmediatePaymentsPaymentAcknowledgement";
                ViewBag.json = JsonConvert.SerializeObject(model);
                return PartialView(model);
            }
            catch (Exception ex)
            {
                // Log the error
                Logger.Error(ex.ToString());

                // Add an error to the ViewBag
                ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "DataRetrievalError");

                // Rethrow the error
                throw;
            }
        }

        /// <summary>
        /// Process the payment plan acknowledgement form
        /// </summary>
        /// <param name="model">JSON-formatted payment plan acknowledgement model</param>
        [LinkHelp]
        [HttpPost]
        public ActionResult PaymentPlanAcknowledgement(string model)
        {
            var localModel = JsonConvert.DeserializeObject<PaymentPlanAcknowledgementModel>(model);

            ActionResult result = null;
            if (localModel.PaymentControl != null)
            {
                // Where we take the user next is dependent on the payment status
                switch (localModel.PaymentControl.PaymentStatus)
                {
                    // An error status means that the payment was unsuccessful
                    case RegistrationPaymentStatus.Error:
                        ViewBag.Error = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentAcknowledgementErrorStatusMessage");
                        result = RedirectToAction("Index");
                        break;

                    // An accepted status means that a payment was made, but multiple payments are needed
                    case RegistrationPaymentStatus.Accepted:
                        List<string> viewMessage = new List<string>() { GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "MultiplePaymentsErrorText") };
                        var acceptModel = GetCompleteModel(viewMessage, localModel.ReturnUrl);

                        ViewBag.json = JsonConvert.SerializeObject(acceptModel);
                        result = View("PaymentComplete", acceptModel);
                        break;

                    // Is payment control complete?
                    case RegistrationPaymentStatus.Complete:
                        List<string> message = new List<string>();
                        if (!string.IsNullOrEmpty(localModel.PaymentControl.LastPlanApprovalId))
                        {
                            message.Add(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "RegistrationCompleteUsingPlanMessage"));
                        }
                        else
                        {
                            message.Add(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentAcknowledgementCompleteStatusMessage"));
                        }
                        var completeModel = GetCompleteModel(message, localModel.ReturnUrl);

                        ViewBag.json = JsonConvert.SerializeObject(completeModel);
                        result = View("PaymentComplete", completeModel);
                        break;
                }
            }
            return result;
        }

        /// <summary>
        /// Process the payment acknowledgement form
        /// </summary>
        /// <param name="payControlId">Registration Payment Control ID</param>
        /// <param name="receivedDownPayment">Down Payment Amount</param>
        /// <param name="model">JSON-formatted payment acknowledgement model</param>
        public ActionResult PrintPaymentPlanAcknowledgement(string payControlId, decimal receivedDownPayment)
        {
            var paymentControl = ServiceClient.GetRegistrationPaymentControl(payControlId);
            var planApproval = ServiceClient.GetPaymentPlanApproval(paymentControl.LastPlanApprovalId);
            var ackText = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentPlanAcknowledgementStudentMessage");
            var termsResponse = ServiceClient.GetApprovalResponse(planApproval.TermsResponseId);
            var termsDocument = ServiceClient.GetApprovalDocument(termsResponse.DocumentId);
            var paymentPlan = ServiceClient.GetPaymentPlan(planApproval.PaymentPlanId);

            var planDetailsModel = PaymentPlanDetailsModel.BuildPlanAcknowledgement(paymentPlan, planApproval, ackText, termsDocument, termsResponse, receivedDownPayment, true);

            // Display the Payment Plan Acknowledgement form
            var payPlanAckModel = PaymentPlanAcknowledgementModel.Build(planDetailsModel, paymentControl, null, CurrentUser);
            payPlanAckModel.IsPrintable = true;

            ViewBag.json = JsonConvert.SerializeObject(payPlanAckModel);
            return View("PrintPaymentPlanAcknowledgement", payPlanAckModel);
        }

        /// <summary>
        /// Process the workflow completion form
        /// </summary>
        /// <param name="model">JSON-formatted workflow completion model</param>
        [LinkHelp]
        [HttpPost]
        public ActionResult PaymentComplete(PaymentCompleteModel completeModel)
        {
            ViewBag.json = JsonConvert.SerializeObject(completeModel);
            return View("PaymentComplete", completeModel);
        }

        /// <summary>
        /// Get all incomplete registration payment controls for the current user
        /// </summary>
        /// <returns>JSON-formatted array of payment controls</returns>
        /// <remarks>NOTE: This method is called from outside Student Finance to determine whether the student has completed all payment arrangements.</remarks>
        public JsonResult GetIncompletePayments()
        {
            try
            {
                var paymentControls = ServiceClient.GetStudentPaymentControls(GetEffectivePersonId()).Where(x => x.PaymentStatus != RegistrationPaymentStatus.Complete);
                return Json(paymentControls, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                // Return an empty result if an error occurred - TODO is this what we want to do here?
                return Json(new List<RegistrationPaymentControl>(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get the registration summary model for a payment control
        /// </summary>
        /// <param name="paymentControl">Registration payment control</param>
        /// <param name="returnUrl">Optional return URL</param>
        /// <returns>The registration summary model</returns>
        private async Task<RegistrationSummaryModel> GetRegistrationSummaryModel(RegistrationPaymentControl paymentControl, string returnUrl = null)
        {
            // Get the needed static data
            var ipcControl = await ServiceClientCache.GetCachedImmediatePaymentControl(ServiceClient);
            var terms = ServiceClient.GetTerms();
            var locations = ServiceClient.GetLocations();
            var buildings = ServiceClient.GetBuildings();
            var receivableTypes = ServiceClient.GetReceivableTypes();

            // Get the term utility
            var termUtility = new TermPeriodsUtility(terms);

            // Get the document text to display
            var ackText = GetPaymentControlDocument(paymentControl.Id, ipcControl.RegistrationAcknowledgementDocumentId).Text;
            var termsText = GetPaymentControlDocument(paymentControl.Id, ipcControl.TermsAndConditionsDocumentId).Text;

            // Build the list of sections that will be displayed
            var sectionIds = paymentControl.RegisteredSectionIds.ToList();

            // Get the sections, their courses, and instructor info
            var sections = ServiceClient.GetSections4(sectionIds);
            var courses = new List<Course2>();
            var faculty = new List<Faculty>();
            foreach (var section in sections)
            {
                courses.Add(ServiceClient.GetCourse(section.CourseId));
                foreach (var id in section.FacultyIds)
                {
                    // Add missing faculty to the list
                    var temp = faculty.Where(x => x.Id == id).FirstOrDefault();
                    if (temp == null)
                    {
                        faculty.Add(ServiceClient.GetFaculty(id));
                    }
                }
            }

            // Get the invoices on the acknowledgement and the map of account types to distributions
            var invoices = ServiceClient.GetInvoices(paymentControl.InvoiceIds);
            var receivableTypeCodes = invoices.Select(x => x.ReceivableTypeCode).Distinct().ToList();
            var distributions = ServiceClient.GetPaymentDistributions(paymentControl.StudentId, receivableTypeCodes, _ImmediatePaymentDistribution).ToList();
            var distributionMap = new Dictionary<string, string>();
            for (int i = 0; i < receivableTypeCodes.Count; i++)
            {
                distributionMap.Add(receivableTypeCodes[i], distributions[i]);
            }

            // Build the registration summary model, the resulting view, and the JSON version of the model
            var model = RegistrationSummaryModel.Build(ipcControl, termUtility, locations, buildings, receivableTypes, returnUrl,
                paymentControl, ackText, sections, courses, faculty, invoices, distributionMap, termsText);

            return model;
        }

        /// <summary>
        /// Get a document for a specified payment control
        /// </summary>
        /// <param name="payControlId">Payment Control ID</param>
        /// <param name="documentId">Document ID</param>
        /// <returns>The merged text document</returns>
        private TextDocument GetPaymentControlDocument(string payControlId, string documentId)
        {
            var document = new TextDocument();
            if (String.IsNullOrEmpty(payControlId) || String.IsNullOrEmpty(documentId))
            {
                return document;
            }

            try
            {
                document = ServiceClient.GetRegistrationPaymentControlDocument(payControlId, documentId);
            }
            catch
            {
                document = new TextDocument();
            }

            return document;
        }

        /// <summary>
        /// Update the payment control status
        /// </summary>
        /// <param name="paymentControl">Payment control DTO</param>
        /// <param name="status">New status, defaults to Complete</param>
        /// <returns>Updated Payment Control DTO</returns>
        private RegistrationPaymentControl UpdatePaymentStatus(RegistrationPaymentControl paymentControl, RegistrationPaymentStatus status = RegistrationPaymentStatus.Complete)
        {
            if (paymentControl == null)
            {
                throw new ArgumentNullException("paymentControl", "A Payment Control must be provided.");
            }
            paymentControl.PaymentStatus = status;
            var newPaymentControl = ServiceClient.UpdateRegistrationPaymentControl(paymentControl);

            return newPaymentControl;
        }

        /// <summary>
        /// Parse out the elements passed from Student Planning and build a return URL
        /// </summary>
        /// <param name="model">Input model from degree planning</param>
        /// <returns>The URL to return the user back to where they came from</returns>
        private string GetReturnUrl(ImmediatePaymentInputModel model)
        {
            // If a model exists, process it to build the return URL for the workflow
            string returnUrl = null;
            if (model != null && !String.IsNullOrEmpty(model.ReturnUrl))
            {
                // Remove any hashtag or query parameters on the URL
                string baseUrl = model.ReturnUrl.Split('#')[0].Split('?')[0];

                // Build the query parameters including only the parameters with values
                List<string> arguments = new List<string>();
                if (!String.IsNullOrEmpty(model.TermId))
                {
                    arguments.Add("currentTerm=" + HttpUtility.UrlEncode(model.TermId));
                }
                if (!String.IsNullOrEmpty(model.SelectedTab))
                {
                    arguments.Add("currentView=" + model.SelectedTab);
                }
                //if (model.PaymentControl != null && !String.IsNullOrEmpty(model.PaymentControl.Id))
                //{
                //    arguments.Add("payControlId=" + model.PaymentControl.Id);
                //}
                string query = (arguments.Count > 0) ? String.Join("&", arguments) : String.Empty;
                returnUrl = UrlUtility.CombineUrlPathAndArguments(baseUrl, query);
            }

            return returnUrl;
        }

        /// <summary>
        /// Get the payment completion model
        /// </summary>
        /// <param name="message">Message to display on the form</param>
        /// <param name="returnUrl">The return URL</param>
        /// <returns>The payment completion model</returns>
        private PaymentCompleteModel GetCompleteModel(List<string> message = null, string returnUrl = null)
        {
            string buttonLabel = null;
            ViewBag.HelpContentId = "ImmediatePaymentsPaymentComplete";
            if (String.IsNullOrEmpty(returnUrl))
            {
                // Return to Make a Payment
                returnUrl = Url.Action("Index", "Payments");
                buttonLabel = String.Format(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "RegistrationCompleteReturnButtonLabel"), GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "MakeAPaymentTitle"));
            }
            else
            {
                if (returnUrl.Contains("RegistrationActivity"))
                {
                    // Return to Registration Activity
                    buttonLabel = String.Format(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "RegistrationCompleteReturnButtonLabel"), GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "RegistrationActivityLabel"));
                }
                else
                {
                    // Return to Schedule
                    buttonLabel = String.Format(GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "RegistrationCompleteReturnButtonLabel"), GlobalResources.GetString(GlobalResourceFiles.StudentResources, "DegreePlansTitle"));
                }
            }
            var completeModel = PaymentCompleteModel.Build(message, buttonLabel, returnUrl);

            return completeModel;
        }
    }
}