﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System.ComponentModel;
using System.Web.Mvc;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Student.Licensing;

namespace Ellucian.Web.Student.Areas.Finance
{
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Finance)]
    public class FinanceAreaRegistration : LicenseAreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Finance";
            }
        }

        public override void LicensedRegistration(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Finance_default",
                "Finance/{controller}/{action}/{id}",
                new { controller = "AccountSummary", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
