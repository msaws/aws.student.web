﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
// -------------------------------
// Registration Summary view model
// -------------------------------
function RegistrationSummaryViewModel(modelData) {
    var self = this;
    // Map the input data to the view model
    var mapping = {
        "copy": ['WorkflowTitle', 'WorkflowStep', 'PaymentControl', 'ReturnUrl', "Acceptance", "RegisteredSections", "Charges", "IsPrintable", "IsAdminUser"],
        "observe": ["Notification", "AcceptTermsAndConditions", "PersonId", "PersonName"]
    };
    ko.mapping.fromJS(modelData, mapping, self);

    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
    BaseViewModel.call(self, 768);

    // Subscribe to mobile changes to trigger table responsification
    self.isMobile.subscribe(function () {
        $("table").makeTableResponsive();
    })

    // Overrides for base view model change functions to remove alerts - do nothing
    self.changeToDesktop = function () { }
    self.changeToMobile = function () { }

    self.ShowCredits = function () {
        var value = false;
        for (var i = 0; i < self.RegisteredSections.length && !value; i++) {
            if (self.RegisteredSections[i].Credits) {
                value = true;
            }
        }
        return value;
    };

    self.ShowCeus = function () {
        var value = false;
        for (var i = 0; i < self.RegisteredSections.length && !value; i++) {
            if (self.RegisteredSections[i].Ceus) {
                value = true;
            }
        }
        return value;
    };

    self.ShowLocation = function () {
        var value = false;
        for (var i = 0; i < self.RegisteredSections.length && !value; i++) {
            if (self.RegisteredSections[i].Location) {
                value = true;
            }
        }
        return value;
    };

    self.ShowMultiplePaymentsMessage = function () {
        var distributions = [];
        for (var i = 0; i < self.Charges.length; i++) {
            if (distributions.indexOf(self.Charges[i].Distribution) < 0) {
                distributions.push(self.Charges[i].Distribution);
            }
        }
        return (distributions.length > 1);
    };

    self.DisplayTermsText = function () {
        var value = "";
        if (self.Acceptance && self.Acceptance.TermsText && self.Acceptance.TermsText.length > 0) {
            value = self.Acceptance.TermsText.filter(function (line) { return line !== "" });
            value = self.Acceptance.TermsText.join("\n");
        }
        return value;
    };

    self.ProcessTermsAndConditions = function () {

        // Convert the model to JSON
        var model = ko.toJS(self);
        model.Acceptance.AcknowledgementText = sanitizeFormData(model.Acceptance.AcknowledgementText);
        model.Acceptance.TermsText = sanitizeFormData(model.Acceptance.TermsText);

        ko.utils.postJson(ipcPaymentOptionsActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
    };

    self.PrintSummaryUrl = ko.computed(function () {
        return ipcPrintSummaryActionUrl + "?payControlId=" + self.Acceptance.PaymentControlId + "&ackDateTime=" + self.Acceptance.AcknowledgementDateTime;
    });

    // URL to print the payment acknowledgement
    self.printSummary = function () {
        window.open(self.PrintSummaryUrl(), '_blank');
    }

    // URL for switching users in Admin mode
    self.changeUserUrl = ko.observable(Ellucian.Finance.RegistrationActivity.ActionUrls.registrationActivityChangeUserUrl);

    self.ReturnToRegistrationActivity = function () {
        var url = "";
        if (self.IsAdminUser) {
            url = registrationActivityAdminActionUrl + "/" + self.PersonId();
        } else {
            url = registrationActivityActionUrl;
        }
        window.location.href = url;
    };

    self.DisplayPayPlanSummary = function () {
        if (self.PaymentControl && self.PaymentControl.PaymentPlanId != null && self.PaymentControl.PaymentPlanId != '') {
            var model = self.PaymentControl;

            ko.utils.postJson(raDisplayPaymentPlanSummaryActionUrl, { paymentControlJson: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        }
        return null;
    };

    self.showPayPlanMessage = ko.computed(function () {
        return (self.PaymentControl && self.PaymentControl.PaymentPlanId != null && self.PaymentControl.PaymentPlanId != '');
    });

    // Make tables responsive
    $("#courses").makeTableResponsive();
    $("#charges").makeTableResponsive();
};
