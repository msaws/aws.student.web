﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
var accountSummaryViewModelInstance = new accountSummaryViewModel();

$(document).ready(function () {

    ko.applyBindings(accountSummaryViewModelInstance, document.getElementById("body"));

    accountSummaryViewModelInstance.getAccountSummaryInformation();
});