﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
function accountSummaryViewModel() {

    // Initialize the model
    var self = this;

    // Flag indicating whether or not the page content is ready to be displayed
    self.showUI = ko.observable(false);

    // Flag indicating whether or not an ajax request is being processed that should block the page
    // This needs to be initialized to true to prevent the AJAX request from firing when the 
    // bindings are initially applied.
    self.isLoading = ko.observable(true);

    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
    BaseViewModel.call(self, 768);

    // Subscribe to mobile changes to trigger table responsification
    self.isMobile.subscribe(function () {
        $("table").makeTableResponsive();
    })

    // Overrides for base view model change functions to remove alerts - call page reload function
    self.changeToDesktop = function () {
    }

    self.changeToMobile = function () {
    }

    // Initialize model observables for initial page render
    self.NextPaymentDueDate = ko.observable();
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.IsAdminUser = ko.observable(false);
    self.AmountDue = ko.observable();
    self.OverdueAmount = ko.observable();
    self.TotalAmountDue = ko.observable();
    self.AccountBalance = ko.observable();
    self.TimeframeBalances = ko.observableArray();
    self.HelpfulLinks = ko.observableArray();
    self.ShowAccountActivityLink = ko.observable();
    self.ShowMakeAPaymentLink = ko.observable();
    self.PrivacyMessage = ko.observable();

    // URL for switching users in Admin mode
    self.changeUserUrl = ko.observable(Ellucian.Finance.AccountSummary.ActionUrls.accountSummaryChangeUserUrl);
    
    // AJAX method for loading Account Activity data
    self.getAccountSummaryInformation = function () {

        //var url = addQueryParamsToUrl(getAccountSummaryActionUrl);
        var jsonData = { 'personId': (extractParametersFromUrl()).personId };

        // AJAX call for AccountActivityController action
        $.ajax({
            url: getAccountSummaryActionUrl,
            type: "POST",
            data: JSON.stringify(jsonData),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                self.isLoading(true);
                self.showUI(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.checkForMobile(window, document);
                    ko.mapping.fromJS(data, {}, accountSummaryViewModelInstance);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: accountSummaryUnableToLoadMessage, type: "error" });
                }
            },
            complete: function () {
                self.showUI(true);
                self.isLoading(false);
            }
        });
    }
}




