﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
function PaymentOptionsViewModel(modelData) {
	modelData = modelData || {};
	var self = this;
	mapping = {
		copy: ["SectionToProcess"],
		ignore: ["ActualDownPaymentAmount", "DisplayDownPayment", "DisplayMinimumPayment", "DisplayOtherPayment", "DisplayPaymentDeferral",
				"DisplayPaymentOptions", "DisplayPaymentPlan", "DisplayPlanDetails", "DisplayTermsText", "DownPaymentInputsCompleted", "DownPaymentMessage",
				"DownPaymentMessageText", "DownPaymentOption", "DownPaymentRequired", "editingAmount", "editingDownPaymentAmount", "EffectiveMinimum",
				"EffectiveMinimumDownPayment", "EnableDownPayment", "EnablePaymentOptions", "EnablePlanDetails", "formSectionController", "FrequencyDisplay",
				"IsLoading", "IsOtherDownPayment", "IsOtherPayment", "IsPrintable", "MaximumAmountMessage", "MaximumDownPaymentAmountMessage",
				"MinimumAmountMessage", "MinimumDownPaymentAmountMessage", "OtherDownPayment", "OtherDownPaymentLabel", "OtherDownPaymentValid",
				"OtherPayment", "OtherPaymentValid", "SelectedOption", "spinnerMessage", "StudentIdName",
				"TotalPlanAmount", "ValidDownPaymentPayMethodSelected", "ValidDownPaymentSelected", "ValidInputsCompleted", "ValidOptionSelected",
				"ValidPaymentMethodSelected"]
	}
	ko.mapping.fromJS(modelData, mapping, self);

    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
	BaseViewModel.call(self, 768);

    // Overrides for base view model change functions to remove alerts - do nothing
	self.changeToDesktop = function () { }
	self.changeToMobile = function () { }

	//----------------------------------------------------------------//
	// Spinner elements - these are needed to process spinner dialogs //
	//----------------------------------------------------------------//

	self.IsLoading = ko.observable(false);

	self.payPlanOptionLoading = ko.observable(false);
	self.payPlanOptionMessage = ko.observable(gettingProposedPlanString);

	self.acceptPlanTermsLoading = ko.observable(false);
	self.acceptPlanTermsMessage = ko.observable(processingTermsAndConditionsString);
	
	//----------------------------------------------------------//
	// Controller elements - handle workflow processing on form //
	//----------------------------------------------------------//

	// If the section controller hasn't been defined, create it as an observable
	if (self.formSectionController === undefined || self.formSectionController === null || typeof self.formSectionController !== "function") {
		self.formSectionController = ko.observable(1);
	}
	if (self.SectionToProcess && self.SectionToProcess > 0 && self.SectionToProcess <= 3) {
		self.formSectionController(self.SectionToProcess);
	}

	// Set computed values that control which form elements are displayed and
	// which are enabled.  Only the current section is enabled, but the current
	// section and any previous sections are displayed.
	self.DisplayPaymentOptions = ko.computed(function () {
		return self.formSectionController() >= 1;
	});
	self.DisplayPlanDetails = ko.computed(function () {
		return self.formSectionController() >= 2;
	});
	self.DisplayDownPayment = ko.computed(function () {
		return self.formSectionController() >= 3;
	});
	self.EnablePaymentOptions = ko.computed(function () {
		return self.formSectionController() === 1;
	});
	self.EnablePlanDetails = ko.computed(function () {
		return self.formSectionController() === 2;
	});
	self.EnableDownPayment = ko.computed(function () {
		return self.formSectionController() === 3;
	});

	// Payment method for payment options
	self.OptionsPaymentMethod = ko.observable();
	// Computed observable - indicates whether a payment method has been selected
	self.ValidPaymentMethodSelected = ko.computed(function () {
		return (self.OptionsPaymentMethod() || false);
	});

	// Set flag for the printable view of the plan details
	self.IsPrintable = ko.observable(false);

	// This is the handler for all AJAX calls when they are successful
	self.ajaxSuccessHandler = function (data) {
		if (!account.handleInvalidSessionResponse(data)) {
			// Increment the controller for the next section
			self.formSectionController(self.formSectionController() + 1);

			// The incoming data is the updated model - map it
			ko.mapping.fromJS(data, mapping, self);
			
			// Change the behavior of the accordion controls based on new settings
			resetAccordions();

			// Stop the spinner dialog
			self.IsLoading(false);
        }
	};

	//-----------------------------------------------//
	// Logic for Payment Options section of the form //
	//-----------------------------------------------//

	// If the minimum payment is between zero and $1, make it $1 for e-commerce processing
	if (self.MinimumPayment() > 0 && self.MinimumPayment() < 1) {
		self.MinimumPayment(1);
	}

	// Computed observable for the effective minimum payment - if the actual minimum is less than $1,
	// the effective minimum payment is $1 (the minimum amount for an e-commerce payment).  Otherwise,
	// the effective minimum payment is the minimum payment amount.
	self.EffectiveMinimum = ko.computed(function () {
		var min = self.MinimumPayment() || 1;
		if (isNaN(min) || min < 1) {
			min = 1;
		}
		return min;
	});

	// Format error messages
	self.MinimumAmountMessage = ko.observable(minimumAmountMessageString.format(formatAsCurrency(self.EffectiveMinimum())));
	self.MaximumAmountMessage = ko.observable(maximumAmountMessageString.format(formatAsCurrency(self.FullPayment())));

	// Observable to track which radio button is selected
	self.SelectedOption = ko.observable("None");
	// Observable to control when the other amount field is being edited
	self.editingAmount = ko.observable(false);

	// Computed observables that indicate whether the various payment options should be displayed
	self.DisplayMinimumPayment = ko.computed(function () {
		return self.DeferralPercentage() < 100 && self.DeferralPercentage() > 0 && self.MinimumPayment() > 0;
	});
	self.DisplayOtherPayment = ko.computed(function () {
		return self.DeferralPercentage() > 0;
	});
	self.DisplayPaymentPlan = ko.computed(function () {
		return (self.PaymentPlanTemplateId() && self.PaymentPlanFirstDueDate() && self.PaymentPlanAmount() > 0);
	});

	// Deferral option only gets shown when 100% deferral or there's no minimum payment
	self.DisplayPaymentDeferral = ko.computed(function () {
		return self.DeferralPercentage() === 100 || self.MinimumPayment() === 0;
	});
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												
	// Computed observable indicating when the "Other Payment" has been selected
	self.IsOtherPayment = ko.computed(function () {
		return (self.SelectedOption() === "OtherPayment");
	});

	// Observable for the Other Payment amount
	self.OtherPayment = ko.observable("").extend({
		numeric: {
			precision: 2,
			zeroNull: true,
			max: self.FullPayment(),
			maxMessage: self.MaximumAmountMessage(),
			min: self.EffectiveMinimum(),
			minMessage: self.MinimumAmountMessage(),
			nanMessage: invalidAmountMessageString,
			valCondition: function () { return self.IsOtherPayment(); }
		}
	});

    // Call error checking logic whenever checkbox is changed
	self.OtherPayment.validationMessage.subscribe(function () {
	    $("#aria-announcements").text(self.OtherPayment.validationMessage());
	});

	// Computed observable - indicates whether the "Other Payment" is valid
	self.OtherPaymentValid = ko.computed(function () {
		return self.IsOtherPayment() && !self.OtherPayment.hasError();
	});

	// Computed observable that specifies whether a valid option has been selected.
	self.ValidOptionSelected = ko.computed(function () {
		if (self.SelectedOption() === "None") {
			return false;
		}
		if (!self.IsOtherPayment()) {
			return true;
		}

		// Put the focus on the other amount field
		self.editingAmount(true);

		// If the amount is valid and greater than zero, then we've got a valid option
		return !self.OtherPayment.hasError() && parseANumber(self.OtherPayment()) > 0;
	});

	// Computed observable to say whether the student is signing up for a payment plan
	self.IsCreatingPaymentPlan = ko.computed(function () {
		return (self.SelectedOption() === "PaymentPlan");
	});
	// Text to display when the user selects a down payment
	self.DownPaymentMessageText = ko.computed(function () {
		if (self.DisplayPaymentPlan() && self.MinimumDownPaymentAmount() > 0 && self.DownPaymentDate().isDue()) {
			return paymentPlanDownPaymentString.format(formatAsCurrency(self.MinimumDownPaymentAmount()));
		}
		return null;
	});

	// Determines whether to display plan terms approval information
	self.DisplayPlanApprovalMessage = ko.computed(function () {
		if (self.PlanDetails.ApprovalTimestamp() && self.PlanDetails.ApprovalUserId()) {
			return true;
		}
		return false;
	});

	// Computed observable for the payment amount - based on the option selected
	self.PaymentAmount = ko.computed(function () {
		if (!self.ValidOptionSelected()) {
			return 0;
		}
		switch (self.SelectedOption()) {
			case "FullPayment":
				self.OtherPayment(0);
				return self.FullPayment();
			case "MinimumPayment":
				self.OtherPayment(0);
				return self.MinimumPayment();
			case "OtherPayment":
				return parseANumber(self.OtherPayment());
			default:
				self.OtherPayment(0);
				self.OptionsPaymentMethod(undefined);
				return 0;
		}
	});

	// Computed observable - indicates whether all the input fields have been completed
	// This controls whether the button is enabled or disabled
	self.ValidInputsCompleted = ko.computed(function () {
		var valid = self.ValidOptionSelected();
		if (valid && self.SelectedOption() !== "DeferPayment" && self.SelectedOption() !== "PaymentPlan") {
			// Payment method is required for everything except full deferral and payment plan
			valid = self.ValidPaymentMethodSelected() || false;
		}
		return valid;
	});

	// This function is processed when the button is clicked in the payment options section - it posts the data
	self.ProcessPaymentOption = function () {
		// If the payment plan option was selected, then go get the proposed plan.
		// Otherwise, continue with the payment review form.
		if (self.IsCreatingPaymentPlan()) {
			// Convert the model to JSON
			var model = ko.mapping.toJS(self);
			$.ajax({
				url: ipcGetProposedPlanActionUrl,
				type: "POST",
				data: JSON.stringify(model),
				contentType: jsonContentType,
				dataType: "json",
				beforeSend: function (data) {
				    self.payPlanOptionLoading(true);
				},
				success: function (data) {
				    self.ajaxSuccessHandler(data);
				    self.payPlanOptionLoading(false);
					$("#planDetailsAccordion").accordion({ collapsible: false });
					$("#planDetailsAccordion h3 span:first-child").hide();
					$("#optionsAccordion h3 span:first-child").show();
				},
				error: function (jqXHR, textStatus, errorThrown) {
					if (jqXHR.status !== 0) {
						// Stop the spinner dialog and send a notification to the user
					    self.IsLoading(false);
					    $('#notificationHost').notificationCenter('addNotification', { message: "Unable to process the selected payment option", type: "error" });
					}
				}
			});
		} else {
			// Convert the model to JSON
			var model = ko.mapping.toJS(self);
			ko.utils.postJson(ipcPaymentReviewActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
		}
	};

	//----------------------------------------------------//
	// Logic for Payment Plan Details section of the form //
	//----------------------------------------------------//

	self.StudentIdName = ko.computed(function () {
		var idName = (self.PaymentControl && self.PaymentControl.StudentId && typeof self.PaymentControl.StudentId === "function")
			? self.PaymentControl.StudentId() : "";
		if (self.PlanDetails.StudentName && typeof self.PlanDetails.StudentName === 'function') {
			idName += " " + self.PlanDetails.StudentName();
		}

		return idName;
	});

	// Display the frequency using entries from the resource file instead of the element itself to allow 
	// better support for customization, as well as I18n
	self.FrequencyDisplay = ko.computed(function () {
		if (!self.PlanDetails.PaymentPlan || !self.PlanDetails.PaymentPlan.Frequency || typeof self.PlanDetails.PaymentPlan.Frequency !== "function") {
			return "";
		}
		var frequency = self.PlanDetails.PaymentPlan.Frequency();
		switch (frequency) {
			case 0:
			case "Weekly": return weeklyFrequencyDescriptionString;
			case 1:
			case "Biweekly": return biweeklyFrequencyDescriptionString;
			case 2:
			case "Monthly": return monthlyFrequencyDescriptionString;
			case 3:
			case "Yearly": return yearlyFrequencyDescriptionString;
			case 4:
			case "Custom": return customFrequencyDescriptionString;
			default: return unknownFrequencyDescriptionString;
		}
	});

	self.DisplayTermsText = ko.computed(function () {
		var value = "";
		if (self.Acceptance && self.Acceptance.TermsText && typeof self.Acceptance.TermsText === "function" && self.Acceptance.TermsText() && self.Acceptance.TermsText().length > 0) {
			value = self.Acceptance.TermsText().join("\r");
		}
		return value;
	});

	// This function is processed when the button is clicked in the plan details section.
	// It saves the acceptance info in Colleague, and returns the down payment info.
	self.ProcessAcceptPlanTerms = function () {
	    var model = ko.mapping.toJS(self);
	    model.Acceptance.AcknowledgementText = sanitizeFormData(model.Acceptance.AcknowledgementText);
	    model.Acceptance.TermsText = sanitizeFormData(model.Acceptance.TermsText);
	    model.PlanDetails.AcknowledgementText = sanitizeFormData(model.PlanDetails.AcknowledgementText);
	    model.PlanDetails.TermsAndConditionsText = sanitizeFormData(model.PlanDetails.TermsAndConditionsText);

		$.ajax({
			url: ipcAcceptPlanTermsActionUrl,
			type: "POST",
			data: JSON.stringify(model),
			contentType: jsonContentType,
			dataType: "json",
			beforeSend: function (data) {
			    self.acceptPlanTermsLoading(true);
			},
			success: function (data) {
			    self.ajaxSuccessHandler(data);
			    self.acceptPlanTermsLoading(false);
				$("#planDownPaymentAccordion").accordion({ collapsible: false });
				$("#planDownPaymentAccordion h3 span:first-child").hide();
				$("#planDetailsAccordion h3 span:first-child").show();
				$('.payOption:visible:last').css('border-bottom', '1px solid #e6e6e6');
				ko.applyBindings(self, document.getElementsByClassName("help-content"));
				// Reset the selected payment method
				//self.OptionsPaymentMethod(undefined);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				if (jqXHR.status !== 0) {
					// Stop the spinner dialog and send a notification to the user
				    self.IsLoading(false);
				    $('#notificationHost').notificationCenter('addNotification', { message: "Unable to record the acceptance of the plan terms and conditions", type: "error" });
				}
			}
		});
	};

	self.TotalPlanAmount = ko.computed(function () {
		return (self && self.PlanDetails.PaymentPlan && self.PlanDetails.PaymentPlan.CurrentAmount && typeof self.PlanDetails.PaymentPlan.CurrentAmount === "function" && self.PlanDetails.PaymentPlan.TotalSetupChargeAmount && typeof self.PlanDetails.PaymentPlan.TotalSetupChargeAmount === "function")
			? (self.PlanDetails.PaymentPlan.CurrentAmount() || 0) + (self.PlanDetails.PaymentPlan.TotalSetupChargeAmount() || 0) : 0;
	});

	self.DownPaymentMessage = ko.computed(function () {
		if (self.MinimumDownPaymentAmount() !== 0 && self.DownPaymentDate().isDue()) {
			return paymentPlanDownPaymentString.format(formatAsCurrency(self.MinimumDownPaymentAmount()));
		}
		return null;
	});

	self.DownPaymentStyle = ko.observable('info');

	//----------------------------------------------------//
	// Logic for Down Payment Options section of the form //
	//----------------------------------------------------//

	self.DownPaymentRequired = ko.computed(function () {
		return (self.MinimumDownPaymentAmount() > 0 && self.DownPaymentDate().isDue());
	});

	// Computed observable for the effective minimum payment - if the actual minimum is less than $1,
	// the effective minimum payment is $1 (the minimum amount for an e-commerce payment).  Otherwise,
	// the effective minimum payment is the minimum payment amount.
	self.EffectiveMinimumDownPayment = ko.computed(function () {
		var min = self.MinimumDownPaymentAmount() || 1;
		if (isNaN(min) || min < 1) {
			min = 1;
		}
		return min;
	});

	// Observable to control when the other down payment amount field is being edited
	self.editingDownPaymentAmount = ko.observable(false);
	// Observable to track which radio button is selected
	self.DownPaymentOption = ko.observable("None");
	// Computed value for the field label for the "other" amount
	self.OtherDownPaymentLabel = ko.computed(function () {
		var label;
		if (self.DownPaymentRequired()) {
			label = downPaymentOtherAmountString;
		} else {
			label = downPaymentPayNowString;
		}
		return label;
	});

	// Computed observable indicating when the other down payment amount has been selected
	self.IsOtherDownPayment = ko.computed(function () {
		return (self.DownPaymentOption() === "OtherDownPayment");
	});

	// Format error messages
	self.MinimumDownPaymentAmountMessage = ko.observable(self.DownPaymentMessage());
	self.MaximumDownPaymentAmountMessage = ko.observable(maximumAmountMessageString.format(formatAsCurrency(self.PaymentPlanAmount())));

	// Observable for the Other Down Payment amount
	self.OtherDownPayment = ko.observable("").extend({
		numeric: {
			precision: 2,
			zeroNull: true,
			max: self.PaymentPlanAmount(),
			maxMessage: self.MaximumDownPaymentAmountMessage(),
			min: self.EffectiveMinimumDownPayment(),
			minMessage: self.MinimumDownPaymentAmountMessage(),
			nanMessage: invalidAmountMessageString,
			valCondition: function () { return self.IsOtherDownPayment(); }
		}
	});

	// Computed observable - indicates whether the "Other Payment" is valid
	self.OtherDownPaymentValid = ko.computed(function () {
		return self.IsOtherDownPayment() && !self.OtherDownPayment.hasError();
	});


	// Computed observable that specifies whether a valid option has been selected.
	self.ValidDownPaymentSelected = ko.computed(function () {
		if (self.DownPaymentOption() === "None") {
			return false;
		}
		if (!self.IsOtherDownPayment()) {
			return true;
		}

		// Put the focus on the other amount field
		self.editingDownPaymentAmount(true);

		// If the amount is valid and greater than zero, then we've got a valid option
		return !self.OtherDownPayment.hasError() && parseANumber(self.OtherDownPayment()) > 0;
	});

	// Computed observable for the payment amount - based on the option selected
	self.ActualDownPaymentAmount = ko.computed(function () {
		if (!self.ValidDownPaymentSelected()) {
			return 0;
		}
		switch (self.DownPaymentOption()) {
			case "RequiredDownPayment":
				self.OtherDownPayment(0);
				return self.MinimumDownPaymentAmount();
			case "OtherDownPayment":
				return parseANumber(self.OtherDownPayment());
			default:
				self.OtherDownPayment(0);
				self.PlanPaymentMethod(undefined);
				return 0;
		}
	});

	// Payment method for payment options
	self.PlanPaymentMethod = ko.observable();
	// Computed observable - indicates whether a payment method has been selected
	self.ValidDownPaymentPayMethodSelected = ko.computed(function () {
		return (typeof self.PlanPaymentMethod === "function" && self.PlanPaymentMethod() && typeof self.PlanPaymentMethod().InternalCode === "function" && self.PlanPaymentMethod().InternalCode());
	});

	// Computed observable - indicates whether all the input fields have been completed
	// This controls whether the button is enabled or disabled
	self.DownPaymentInputsCompleted = ko.computed(function () {
		var valid = self.ValidDownPaymentSelected();
		if (valid && self.DownPaymentOption() !== "DeferDownPayment") {
			// Payment method is required for everything except full deferral and payment plan
			valid = self.ValidDownPaymentPayMethodSelected() || false;
		}
		return valid;
	});

	// This function is processed when the button is clicked on the down payment section - it posts the data
	self.ProcessDownPayment = function () {
		// Convert the model to JSON
		var model = ko.toJS(self);
		ko.utils.postJson(ipcPaymentReviewActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
	};
}

//String.parseDate = function (dateString) {
//	// Try to parse the string into a date 
//	if (dateString === undefined || dateString === null || dateString.length < 1) return undefined;
//	var dateNet = /\/Date\((-?\d+)(?:[-\+]\d+)?\)\//i;
//	var msec;
//	if (dateNet.test(dateString)) {
//		msec = parseInt(dateString.replace(dateNet, "$1"));
//	} else {
//		msec = Date.parse(dateString);
//	}
//	if (isNaN(msec)) {
//		return undefined;
//	}
//	return new Date(msec);
//}

String.prototype.isDue = function () {
	var date = parseIsoDate(this);
	if (!date) return false;
	return date.isDue();
}

Date.prototype.isDue = function () {
	var today = new Date();
	return this <= today;
}

function initializeAccordions() {
	$(".accordion-region").accordion(
		{
			active: false,
			collapsible: true,
			autoHeight: false,
			clearStyle: true,
			icons: { 'header': 'ui-icon-carat-1-e', 'headerSelected': 'ui-icon-carat-1-s' }
		}
	);

	$(".accordion-region-opened").accordion(
		{
			collapsible: true,
			autoHeight: false,
			clearStyle: true,
			icons: { 'header': 'ui-icon-carat-1-e', 'headerSelected': 'ui-icon-carat-1-s' }
		}
	);

	$(".accordion-fixed").accordion(
		{
			disabled: true,
			active: 0,
			collapsible: false,
			autoHeight: false,
			clearStyle: true,
			icons: false
		}
	);

}

function resetAccordions() {
	// Reset the accordions according to their new specifications

	// Finish by refreshing all the accordions
	$(".accordion-region, .accordion-region-opened .accordion-fixed").accordion("refresh");

	// Re-enable any that have been disabled and shouldn't be
	$(".accordion-region, .accordion-region-opened").each(function () {
		// Enable the accordion
		if ($(this).accordion("option", "disabled")) {
			$(this).accordion("option", "disabled", false);
		}
		// Make it collapsible
		if (!$(this).accordion("option", "collapsible")) {
			$(this).accordion("option", "collapsible", true);
		}
		// give it icons
		if (!$(this).accordion("option", "icons")) {
			$(this).accordion("option", "icons", { 'header': 'ui-icon-carat-1-e', 'headerSelected': 'ui-icon-carat-1-s' })
		}
	});

	// Now close any open ones that should be closed
	$(".accordion-region").each(function () {
		var activeAccordion = $(this).accordion('option', 'active');
		// Expanded accordions have activeAccordion == '0'
		if (activeAccordion === 0) {
			$(this).accordion('activate', 0);
		}
	});

	// Now make sure all the ones that should be open are
	$(".accordion-region-opened").each(function () {
		var activeAccordion = $(this).accordion('option', 'active');
		// Expanded accordions have activeAccordion == '0'
		if (activeAccordion !== 0) {
			$(this).accordion('activate', 0);
		}
	});

	// Next, disable any that should be kept open
	$(".accordion-fixed").each(function () {
		// First, expand the accordion
		var activeAccordion = $(this).accordion('option', 'active');
		// Expanded accordions have activeAccordion == '0'
		if (activeAccordion !== 0) {
			$(this).accordion('activate', 0);
		}
		// Now, change its characteristics so it can't be closed
		$(this).accordion("option", "disabled", true);
		$(this).accordion("option", "active", false);
		$(this).accordion("option", "collapsible", false);
		$(this).accordion("option", "icons", false);
	});
}