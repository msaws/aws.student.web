﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
var makeAPaymentViewModelInstance = new makeAPaymentViewModel();

$(document).ready(function () {
    ko.applyBindings(makeAPaymentViewModelInstance, document.getElementById("body"));

    startThrobber();

    makeAPaymentViewModelInstance.LoadAccountActivityData(getAccountActivityActionUrl);
});