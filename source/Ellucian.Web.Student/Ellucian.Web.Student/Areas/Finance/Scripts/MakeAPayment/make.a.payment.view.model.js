﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
function makeAPaymentViewModel() {

    // Initialize the model
    var self = this;

    // Flag indicating whether or not the page content is ready to be displayed
    self.ShowUI = ko.observable(false);

    // Flag indicating whether or not an ajax request is being processed that should block the page
    // This needs to be initialized to true to prevent the AJAX request from firing when the 
    // bindings are initially applied.
    self.IsLoading = ko.observable(true);

    // "Inherit" the base view model
    BaseViewModel.call(self, 768);

    // Overrides for base view model change functions to remove alerts - do nothing
    self.changeToDesktop = function () { }
    self.changeToMobile = function () { }

    //// Initialize model observables for initial page render
    //self.AlertMessage = ko.observable();
    //self.Balance = ko.observable();
    //self.BalanceImageUrl = ko.observable();
    //self.BalanceImageAltText = ko.observable();
    //self.DisclaimerText = ko.observable();
    //self.DisplayTermPeriodCode = ko.observable();
    //self.DisplayTermPeriodDescription = ko.observable();
    //self.DropdownLabel = ko.observable();
    //self.FormulaCategories = ko.observableArray();
    //self.IsAdminUser = ko.observable(false);
    //self.IsTermDisplay = ko.observable();
    //self.NonFormulaCategories = ko.observableArray();
    //self.PersonId = ko.observable();
    //self.PersonName = ko.observable();
    //self.ShowDepositsDueLink = ko.observable(false);
    //self.ShowPaymentPlansLink = ko.observable(false);
    //self.TermPeriodBalances = ko.observableArray();

    //// Absolute path for balance icon image
    //self.BalanceImagePath = ko.computed(function () {
    //    if (typeof self.BalanceImageUrl() !== 'undefined' && typeof baseSiteUrl !== 'undefined') {
    //        return baseSiteUrl + self.BalanceImageUrl();
    //    }
    //    return null;
    //});

    //// The currently selected term or period
    //self.SelectedTermPeriod = ko.observable();
    ////self.SelectedTermPeriod = ko.observable(self.DisplayTermPeriodCode()).extend({ changeEvent: self.GetAccountActivity });

    //// Link to view student statement
    //self.ViewStatementLink = ko.computed(function () {
    //    return getStudentStatementReportActionUrl + "?personId=" + self.PersonId() + "&timeframeId=" + self.DisplayTermPeriodCode();
    //});

    //// Click handler for Deposits Due section
    //self.DepositDueClickHandler = function () {
    //    clickHandler("#deposits-due");
    //};

    //// Click handler for Payment Plans section
    //self.PaymentPlansClickHandler = function () {
    //    clickHandler("#payment-plans");
    //};

    //// Click handler for formula bar icons
    //self.IconBarClickHandler = function (data, event) {
    //    var id = "#" + this.CategoryType() + "-accordion";
    //    clickHandler(id);
    //}

    // Click handler for top of page link
    self.TopOfPageClickHandler = function () {
        clickHandler("#topOfForm");
    };

    //// Method to retrieve Account Activity data
    //self.GetAccountActivity = function () {
    //    if (self.IsLoading.peek()) {
    //        return;
    //    }
    //    var termPeriod = self.SelectedTermPeriod.peek();
    //    var accountActivityUrl = null;
    //    if (termPeriod && termPeriod.ProcessUrl) {
    //        accountActivityUrl = termPeriod.ProcessUrl.peek();
    //    }
    //    if (!accountActivityUrl) {
    //        accountActivityUrl = getAccountActivityActionUrl;
    //    }
    //    self.LoadAccountActivityData(accountActivityUrl);
    //};

    //// AJAX method for loading Account Activity data
    //self.LoadAccountActivityData = function (url) {
    //    url = url || getAccountActivityActionUrl;
    //    // AJAX call for AccountActivityController action
    //    $.ajax({
    //        url: url,
    //        type: "GET",
    //        contentType: jsonContentType,
    //        dataType: "json",
    //        beforeSend: function () {
    //            self.IsLoading(true);
    //            self.ShowUI(false);
    //        },
    //        success: function (data) {
    //            if (!account.handleInvalidSessionResponse(data)) {
    //                self.checkForMobile();
    //                ko.mapping.fromJS(data, accountActivityViewModelMapping, accountActivityViewModelInstance);
    //                var panelControllers = $('div.panel-controller');
    //                panelControllers.each(function () {
    //                    var panelContent = $(this).attr("aria-controls");
    //                    $("#" + panelContent).hide();
    //                });
    //            }
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            if (jqXHR.status != 0) {
    //                $('#notificationHost').notificationCenter('addNotification', { message: accountActivityUnableToLoadMessage, type: "error" });
    //            }
    //        },
    //        complete: function () {

    //            // Make all tables in view responsive
    //            $("table").makeTableResponsive();

    //            // Suppress all accordion content (needed specifically for nested accordion content)
    //            $(".multi-accordion-content").hide();
    //            stopThrobber();
    //            self.IsLoading(false);
    //            self.ShowUI(true);

    //            // Initialize dropdown selection to default term or period
    //            if (self.DisplayTermPeriodCode() !== null && typeof self.DisplayTermPeriodCode() !== 'undefined'
    //                && self.TermPeriodBalances() !== null && typeof self.TermPeriodBalances() !== 'undefined') {
    //                var dtpc = $.grep(self.TermPeriodBalances(), function (n, i) { return n.Id() === self.DisplayTermPeriodCode() })[0];
    //                self.SelectedTermPeriod(dtpc);
    //            }
    //        }
    //    });
    //}
}


