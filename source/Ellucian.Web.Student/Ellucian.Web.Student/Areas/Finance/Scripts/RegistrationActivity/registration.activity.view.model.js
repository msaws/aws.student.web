﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates.
// base namespace for Registration Activity

(function (registrationActivity, $, undefined) {

    // -------------------------------
    // Registration Activity view model
    // -------------------------------
    registrationActivity.RegistrationActivityViewModel = function() {
    var self = this;

    // Flag indicating whether or not the page content is ready to be displayed
    self.showUI = ko.observable(false);

    // Flag indicating whether or not an ajax request is being processed that should block the page
    // This needs to be initialized to true to prevent the AJAX request from firing when the 
    // bindings are initially applied.
    self.isLoading = ko.observable(true);

    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
    BaseViewModel.call(self, 768);

    // Subscribe to mobile changes to trigger table responsification
    self.isMobile.subscribe(function () {
        $("table").makeTableResponsive();
    })
        
    // Overrides for base view model change functions to remove alerts - do nothing
    self.changeToDesktop = function () { }
    self.changeToMobile = function () { }

    self.Message = ko.observable();
    self.PaymentControl = ko.observable();
    self.HasPrivacyRestriction = ko.observable(false);
    self.IsAdminUser = ko.observable(false);
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.RegistrationTerms = ko.observableArray([]);
    self.AccountActivityDisplayByTerm = ko.observable();
    self.PrivacyMessage = ko.observable();

    // URL for switching users in Admin mode
    self.changeUserUrl = ko.observable(Ellucian.Finance.RegistrationActivity.ActionUrls.registrationActivityChangeUserUrl);
    
    // AJAX method for loading Registration Activity data
    self.loadRegistrationActivityData = function (url) {

        url = url || getRegistrationActivityActionUrl;
        var jsonData = { 'personId': (extractParametersFromUrl()).personId };

        $.ajax({
            url: url,
            type: "POST",
            contentType: jsonContentType,
            data: JSON.stringify(jsonData),
            dataType: "json",
            beforeSend: function () {
                self.isLoading(true);
                self.showUI(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.checkForMobile(window, document);
                    ko.mapping.fromJS(data, {}, self);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: registrationActivityUnableToLoadMessage, type: "error" });
                }
            },
            complete: function () {
                self.showUI(true);
                self.isLoading(false);
            }
        });
    }


    self.ProcessLink = function (data) {
        var url = "";
        var model = {};

        self.PaymentControl = data.PaymentControl;

        // Is the registration complete?
        if (data.PaymentControl.PaymentStatus() == '2') {
            // Registration is complete - Is the student actively registered in at least one class?
            if (data.PaymentControl.RegisteredSectionIds().length > 0) {
                // At least one active registered class - View Registration Summary option
            if (self.IsAdminUser()) {
                url = raReprintSummaryAdminActionUrl;
            } else {
                url = raReprintSummaryActionUrl;
            }
            model = self;
            }
            // No active registered classes - Go to Account Activity option
            else {
                if (self.IsAdminUser()) {
                    url = accountActivityAdminActionUrl + data.AccountActivityUrlSuffix();
        } else {
                    url = accountActivityActionUrl + data.AccountActivityUrlSuffix();
                }
                model = null;
            }
        }
        else {
            // Registration not complete - Is this an administrative user?
            if (self.IsAdminUser()) {
                // If the status is New, the admin user can't do anything. Otherwise,
                // they get the view summary option
                if (data.Status !== "New") {
                    url = raReprintSummaryAdminActionUrl;
                    model = self;
                }
            } else {
                // Student user - go to Pay for Registration
                url = ipcRegistrationSummaryActionUrl;
                model = { "ReturnUrl": window.location.href, "PaymentControl": data.PaymentControl };
            }
        }

        if (url) {
            if (model) {
                ko.utils.postJson(url, { model: ko.toJS(model), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
            } else {
                window.location.href = url;
            }
        }
    };

    // Link to pull up payment plan acknowledgement information
    self.processPlanLink = function (data) {
        var model = data.PaymentControl;

        ko.utils.postJson(raDisplayPaymentPlanSummaryActionUrl, { paymentControlJson: ko.toJS(model), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
    };
};

    // ----------------------------------
    // Payment Plan Summary View Model
    // ----------------------------------
    registrationActivity.PaymentPlanSummaryModel = function(modelData) {
    var self = this;
    var mapping = {
        'copy': ['ScheduledPayments']
    };
    ko.mapping.fromJS(modelData, mapping, self);


    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
    BaseViewModel.call(self, 768);

    // Subscribe to mobile changes to trigger table responsification
    self.isMobile.subscribe(function () {
        $("table").makeTableResponsive();
    })

    // Overrides for base view model change functions to remove alerts - do nothing
    self.changeToDesktop = function () { }
    self.changeToMobile = function () { }

    self.AcceptTermsAndConditions = ko.computed(function () {
        return true;
    });

    self.EnablePlanDetails = ko.computed(function () {
        return true;
    });

    self.StudentIdName = ko.computed(function () {
        var idName = (self.PaymentControl && self.PaymentControl.StudentId && typeof self.PaymentControl.StudentId === "function")
        ? self.PaymentControl.StudentId() : "";
        if (self.PlanDetails.StudentName && typeof self.PlanDetails.StudentName === 'function') {
            idName += " " + self.PlanDetails.StudentName();
        }

        return idName;
    });

    // URL for switching users in Admin mode
    self.changeUserUrl = ko.observable(Ellucian.Finance.RegistrationActivity.ActionUrls.registrationActivityChangeUserUrl);

    // Display the frequency using entries from the resource file instead of the element itself to allow 
    // better support for customization, as well as I18n
    self.FrequencyDisplay = ko.computed(function () {
        if (!self.PlanDetails.PaymentPlan || !self.PlanDetails.PaymentPlan.Frequency || typeof self.PlanDetails.PaymentPlan.Frequency !== "function") {
            return "";
        }
        var frequency = self.PlanDetails.PaymentPlan.Frequency();
        switch (frequency) {
            case 0:
            case "Weekly": return weeklyFrequencyDescriptionString;
            case 1:
            case "Biweekly": return biweeklyFrequencyDescriptionString;
            case 2:
            case "Monthly": return monthlyFrequencyDescriptionString;
            case 3:
            case "Yearly": return yearlyFrequencyDescriptionString;
            case 4:
            case "Custom": return customFrequencyDescriptionString;
            default: return unknownFrequencyDescriptionString;
        }
    });

    self.TotalPlanAmount = ko.computed(function () {
        return (self && self.PlanDetails.PaymentPlan && self.PlanDetails.PaymentPlan.CurrentAmount && typeof self.PlanDetails.PaymentPlan.CurrentAmount === "function" && self.PlanDetails.PaymentPlan.TotalSetupChargeAmount && typeof self.PlanDetails.PaymentPlan.TotalSetupChargeAmount === "function")
        ? (self.PlanDetails.PaymentPlan.CurrentAmount() || 0) + (self.PlanDetails.PaymentPlan.TotalSetupChargeAmount() || 0) : 0;
    });

    self.DisplayTermsText = ko.computed(function () {
        var value = "";
        if (self.PlanDetails && self.PlanDetails.TermsAndConditionsText && typeof self.PlanDetails.TermsAndConditionsText === "function" && self.PlanDetails.TermsAndConditionsText() && self.PlanDetails.TermsAndConditionsText().length > 0) {
            value = self.PlanDetails.TermsAndConditionsText().join("\r");
        }
        return value;
    });

    self.DownPaymentMessage = ko.computed(function () {
        return (self && self.PlanDetails.ReceivedDownPayment() && self.PlanDetails.ReceivedDownPayment() !== 0) ? planDownPaymentReceivedString.format(formatAsCurrency(self.PlanDetails.ReceivedDownPayment())) : null;
    });

    self.DownPaymentStyle = ko.observable('success');

    self.DisplayPlanApprovalMessage = ko.computed(function () {
        if (self.PlanDetails.ApprovalTimestamp() && self.PlanDetails.ApprovalUserId()) {
            return true;
        }
        return false;
    });

    self.PrintPlanAcknowledgementUrl = ko.computed(function () {
        return raPrintPaymentPlanSummaryActionUrl + "?payControlId=" + self.PaymentControl.Id() + "&receivedDownPayment=" + self.PlanDetails.ReceivedDownPayment();
    });

    // URL to print the payment acknowledgement
    self.printPlanAcknowledgement = function () {
        window.open(self.PrintPlanAcknowledgementUrl(), '_blank');
    }

    self.ReturnToRegistrationActivity = function () {
        var url = "";
        if (self.IsAdminUser()) {
            url = registrationActivityAdminActionUrl + "/" + self.PaymentControl.StudentId();
        } else {
            url = registrationActivityActionUrl;
        }
        window.location.href = url;
    };
};

}(window.registrationActivity = window.registrationActivity || {}, jQuery));
