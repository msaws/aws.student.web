﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
var registrationActivityViewModelInstance = new registrationActivity.RegistrationActivityViewModel();

$(document).ready(function () {
    ko.applyBindings(registrationActivityViewModelInstance, document.getElementById("body"));

    registrationActivityViewModelInstance.loadRegistrationActivityData(getRegistrationActivityActionUrl);
});