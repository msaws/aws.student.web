﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

// Function to scroll to the associated element. If the element is an accordion,
// expand it and then scroll to the element. 
function clickHandler(id) {
    // If this is a multiAccordion element, offload click handling to knockout binding
    if ($(id).hasClass("multi-accordion-collapsed")) {
        $(id).click();
        $(id).focus();
    }
    $('html,body').animate({
        scrollTop: $(id).offset().top
    }, 1000);
    // If this is not a multiAccordion element, shift focus to element
    if (!$(id).hasClass("multi-accordion-controller")) {
        $(id).focus();
    } else if ($(id).hasClass("multi-accordion-expanded")) {
        $("#" + $(id).attr("aria-controls")).focus();
    }
    return false;
}

// Function to format an amount as currency using current culture settings
function formatAsCurrency(amount, precision) {
    precision = precision || 2;
    if (isNaN(precision)) { precision = 2; }
    return Globalize.format(amount, "C" + precision);
}

// Function to parse the dollar amount from a value
function parseDollarAmount(amount) {
    if (!isNaN(amount)) {
        return parseFloat(amount);
    } else if (typeof amount === 'string') {
        return parseFloat(amount.replace(/[^\d.-]/g, ''));
    } else {
        return 0;
    }
}

// Function to build the IPC progress bar
function formatWorkflowIndicator(currentStepId) {
    // Add the arrow points to the ends of all the indicators except the last one
    if (!$.browser.msie) {
        $(".si-container.horizontal > .step-indicator > li").not(":last").append("<div></div>");
    }

    if ($.browser.msie && parseInt($.browser.version, 10) > 7) {
        $(".si-container.horizontal > .step-indicator > li").not(":last").append("<div><div></div></div>");
    }

    // Add "done" class to all workflow steps before the current step and "current" to the current step
    var foundCurrentStep = false;
    $(".si-container.horizontal > .step-indicator > li").each(function (index, element) {
        $(this).addClass("future");
        if (!foundCurrentStep) {
            if (element.id === "wfi" + currentStepId) {
                $(this).removeClass("future");
                $(this).addClass("current");
                foundCurrentStep = true;
            } else {
                $(this).removeClass("future");
                $(this).addClass("done");
            }
        }
    });

    // inject HTML for icon
    // current step
    $(".step-indicator > li.current").prepend('<span><div class="icon-circle icon-play-circle" aria-hidden="true"></div></span>');
    // done step
    $(".step-indicator > li.done").prepend('<span><div class="icon-circle icon-success" aria-hidden="true"></div></span>');
    // future step
    $(".step-indicator > li.future").prepend('<span><div class="icon-circle-gray icon-circle-outline" aria-hidden="true"></div></span>');
}

// Controls top of page click/scroll behavior
function setTopOfPageHandler() {
    // Click handler for the top of page link at the bottom of views
    $("#top-of-page").click(function (e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: $("#topOfForm").offset().top }, 500);
    });
}

// Builds an admin change user URL
function buildAdminChangeUserUrl(baseUrl) {
    var url = baseUrl + "?changeUser=true";
    var urlArguments = getUrlVars();
    if (urlArguments && urlArguments.personId) {
        url += "&personId=" + urlArguments.personId;
    }
    return url;
}


// Function to grab existing query parameters and add them to a URL
function addQueryParamsToUrl (baseUrl) {
    var url = baseUrl;
    var urlArguments = getUrlVars();
    if (urlArguments) {
        if (urlArguments.personId) {
            url += "?personId=" + urlArguments.personId;
            if (urlArguments.timeframeId) {
                url += "&timeframeId=" + urlArguments.timeframeId;
            }
        } else {
            if (urlArguments.timeframeId) {
                url += "?timeframeId=" + urlArguments.timeframeId;
            }        
        }
    } 
    return url;
}
//Function to extract student id from url in admin mode
function extractParametersFromUrl() {
    var url = window.location.href;    
    var personId = null;
    var params = {
        personId: null,
        timeframeId: null
    };
    if (url && url.contains("Admin")) {
        var index = url.indexOf("Admin") + 6;
        personId = url.substring(index, index+7);
        params.personId = personId;
    }
    var urlArguments = getUrlVars();
    if (urlArguments && urlArguments.timeframeId) {
        var timeframeId = (urlArguments.timeframeId).replace("%2F", "/");
        params.timeframeId = timeframeId;
    }
    return params;
}

// Register the person restrictions widget component
ko.components.register('person-restrictions-widget', {
    require: 'PersonRestrictionsWidget/_PersonRestrictionsWidget'
});