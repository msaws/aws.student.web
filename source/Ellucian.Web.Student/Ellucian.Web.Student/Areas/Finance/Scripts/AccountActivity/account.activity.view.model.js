﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
function accountActivityViewModel() {

    // Initialize the model
    var self = this;

    // Flag indicating whether or not the page content is ready to be displayed
    self.showUI = ko.observable(false);

    // Flag indicating whether or not an ajax request is being processed that should block the page
    // This needs to be initialized to true to prevent the AJAX request from firing when the 
    // bindings are initially applied.
    self.isLoading = ko.observable(true);

    // Flag indicating whether or not a cash receipt is displayed
    self.receiptIsLoading = ko.observable(false);

    // Flag indicating whether or not a cash receipt is displayed
    self.receiptIsDisplayed = ko.observable(false);

    // Flag indicating whether or not a printable version of the cash receipt is being used
    self.isPrintable = ko.observable(false);

    // Cash receipt modal dialog options
    self.receiptDialogOptions = ko.observable({
        autoOpen: false, modal: true, minWidth: $(window).width() / 2,
        show: { effect: "fade", duration: dialogFadeTimeMs }, hide: { effect: "fade", duration: dialogFadeTimeMs }, resizable: false,
    });

    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
    BaseViewModel.call(self, 768);

    // Subscribe to mobile changes to trigger table responsification
    self.isMobile.subscribe(function () {
        $("table").makeTableResponsive();
    })

    // Overrides for base view model change functions to remove alerts - call page reload function
    self.changeToDesktop = function () {
        if (!self.isMobile() && self.showUI()) {
            self.reloadPage(window);
        }
    }
    self.changeToMobile = function () {
        if (self.isMobile() && self.showUI()) {
            self.reloadPage(window);
        }
    }

    // Action throbber observables - used when changing terms/periods
    self.actionThrobberMessage = ko.observable(accountActivityActionThrobberMessage);

    // Receipt action throbber observables - used when loading cash receipts
    self.receiptThrobberMessage = ko.observable(accountActivityReceiptThrobberMessage);

    // Expand All / Collapse All messages - used by screen reader to announce result of
    // clicking Expand All/Collapse All
    self.expandAllMessage = ko.observable(accountActivityExpandAllMessage);
    self.collapseAllMessage = ko.observable(accountActivityCollapseAllMessage);


    // Initialize model observables for initial page render
    self.AlertMessage = ko.observable();
    self.Balance = ko.observable();
    self.BalanceImageUrl = ko.observable();
    self.BalanceImageAltText = ko.observable();
    self.DisclaimerText = ko.observable();
    self.DisplayTermPeriodCode = ko.observable();
    self.DisplayTermPeriodDescription = ko.observable();
    self.DropdownLabel = ko.observable();
    self.FormulaCategories = ko.observableArray();
    self.HasData = ko.observable(false);
    self.HasPrivacyRestriction = ko.observable(false);
    self.IsAdminUser = ko.observable(false);
    self.IsTermDisplay = ko.observable();
    self.NoDataMessage = ko.observable();
    self.noDataMessageType = ko.observable("info");
    self.NonFormulaCategories = ko.observableArray();
    self.PersonId = ko.observable();
    self.PersonName = ko.observable();
    self.PrivacyMessage = ko.observable();
    self.ReceiptData = ko.observable();
    self.ShowDepositsDueLink = ko.observable(false);
    self.ShowPaymentPlansLink = ko.observable(false);
    self.TermPeriodBalances = ko.observableArray();

    // URL for switching users in Admin mode
    self.changeUserUrl = ko.observable(Ellucian.Finance.AccountActivity.ActionUrls.accountActivityChangeUserUrl);

    // ID and Name for student in Admin mode
    self.currentStudentInfo = ko.computed(function () {
        if (self.IsAdminUser()) {
            return self.PersonId() + " " + self.PersonName();
        }
        return null;
    });

    // Absolute path for balance icon image
    self.balanceImagePath = ko.computed(function () {
        if (typeof self.BalanceImageUrl() !== 'undefined' && typeof baseSiteUrl !== 'undefined') {
            return baseSiteUrl + self.BalanceImageUrl();
        }
        return null;
    });

    // The currently selected term or period
    self.selectedTermPeriod = ko.observable();

    // Link to view student statement
    self.viewStatementLink = ko.computed(function () {
        return getStudentStatementReportActionUrl + "?personId=" + self.PersonId() + "&timeframeId=" + self.DisplayTermPeriodCode();
    });

    // Click handler for Deposits Due section
    self.depositDueClickHandler = function () {
        clickHandler("#deposits-due");
    };

    // Click handler for Payment Plans section
    self.paymentPlansClickHandler = function () {
        clickHandler("#payment-plans");
    };

    // Click handler for formula bar icons
    self.iconBarClickHandler = function(data, event) {
        var id = "#" + data.CategoryType() + "-accordion";
        clickHandler(id);
    }

    // URL to print the payment acknowledgement
    self.printReceiptUrl = ko.observable();

    // Page reload function for clean transition from mobile --> desktop and vice-versa
    self.reloadPage = function(window) {
        var href = window.location.href.substring(0, window.location.href.indexOf('?'));
        if (self.selectedTermPeriod()) {
            var url = self.selectedTermPeriod().ProcessUrl();
            var urlArguments = getVariableUrlVars(url);
            if (urlArguments && urlArguments.timeframeId) {
                    href += "?timeframeId=" + urlArguments.timeframeId;
                }
            }
        window.location.href = href;
    }

    // Method to retrieve Account Activity data
    self.getAccountActivity = function (data, event) {
        if (self.isLoading.peek()) {
            return;
        }
        var timeframeId = null;

        if (event && event.originalEvent || !self.IsAdminUser()) {
            var termPeriod = self.selectedTermPeriod.peek();
            
            if (termPeriod) {
                timeframeId = termPeriod.Id();
            }
        }        
        self.loadAccountActivityData(getAccountActivityActionUrl, timeframeId);
    };

    // AJAX method for loading Account Activity data
    self.loadAccountActivityData = function (url, timeframeId) {
        url = url || getAccountActivityActionUrl;
        var params = extractParametersFromUrl();
        var jsonData = { 'personId': params.personId, 'timeframeId': timeframeId ? timeframeId : params.timeframeId };
        // AJAX call for AccountActivityController action
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(jsonData),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                self.isLoading(true);
                self.showUI(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.checkForMobile(window, document);
                    ko.mapping.fromJS(data, accountActivityViewModelMapping, accountActivityViewModelInstance);
                    self.ReceiptData(new makeAPayment.PaymentAcknowledgementViewModel(data.ReceiptData));
                    var panelControllers = $('div.panel-controller');
                    panelControllers.each(function () {
                        var panelContent = $(this).attr("aria-controls");
                        $("#" + panelContent).hide();
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Use the new global AJAX error handler from global.js to post 
                // a notification and do any other application-specific logic
                handleAjaxError(jqXHR, textStatus, errorThrown, accountActivityUnableToLoadMessage, function () {
                    self.NoDataMessage(accountActivityUnableToLoadMessage);
                    self.noDataMessageType("error");
                });
                //if (jqXHR.status != 0) {
                //    $('#notificationHost').notificationCenter('addNotification', { message: accountActivityUnableToLoadMessage, type: "error" });
                //    self.NoDataMessage(accountActivityUnableToLoadMessage);
                //    self.noDataMessageType("error");
                //}
            },
            complete: function () {

                // Make all tables in view responsive
                $("table").makeTableResponsive();

                // Suppress all accordion content (needed specifically for nested accordion content)
                $(".multi-accordion-content").hide();
                self.showUI(true);
                self.isLoading(false);

                // Initialize dropdown selection to default term or period
                if (self.DisplayTermPeriodCode() !== null && typeof self.DisplayTermPeriodCode() !== 'undefined'
                    && self.TermPeriodBalances() !== null && typeof self.TermPeriodBalances() !== 'undefined') {
                    var dtpc = $.grep(self.TermPeriodBalances(), function (n, i) { return n.Id() === self.DisplayTermPeriodCode() })[0];
                    self.selectedTermPeriod(dtpc);
                }

                setTopOfPageHandler();
            }
        });
    }

    // Function to display cash receipt information for a student payment or deposit
    self.displayReceipt = function (data) {
        var url = getCashReceiptActionUrl;
        var isDeposit = false;
        var receiptId = "";
        if (data.ReceiptNumber !== undefined) {
            receiptId = data.Id();
        }
        else if (data.ReceiptId !== undefined) {
            receiptId = data.ReceiptId();
            isDeposit = true;
        }
        url += "?receiptId=" + receiptId;
        $.ajax({
            url: url,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                self.receiptIsLoading(true);
                self.receiptIsDisplayed(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.ReceiptData(new makeAPayment.PaymentAcknowledgementViewModel(data));
                    $("table").makeTableResponsive();
                    self.receiptIsLoading(false);
                    self.receiptIsDisplayed(true);
                    self.printReceiptUrl(mapPrintAcknowledgementActionUrl + "?cashRcptsId=" + receiptId);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0)
                    var errorMsg = accountActivityUnableToLoadReceiptDialog;
                if (isDeposit) {
                    errorMsg = accountActivityUnableToLoadDepositReceiptMessage.format(data.Id());
                }
                else {
                    errorMsg = accountActivityUnableToLoadDepositReceiptMessage.format(data.ReceiptNumber());
                }
                $('#notificationHost').notificationCenter('addNotification', { message: errorMsg, type: "error" });
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }

    // Properties for the payment plan acknowledgement 
    self.planAcknowledgementIsLoading = ko.observable(false);
    self.planAcknowledgementIsVisible = ko.observable(false);
    self.planAcknowledgement = ko.observable(null);
    self.paymentPlanWarningMessage = ko.observable("abc");
    self.ApprovalInformationMessage = ko.observable();

    // Function to display payment plan acknowledgement information
    self.displayPlanAcknowledgement = function (planApprovalId) {

        var url = getPaymentPlanDetailsAsyncUrl + "?planApprovalId=" + planApprovalId;

        $.ajax({
            url: url,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                self.planAcknowledgementIsLoading(true);
                self.planAcknowledgementIsVisible(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.planAcknowledgement(new makeAPayment.PaymentPlanViewModel(data.PlanDetails));
                    self.ApprovalInformationMessage(data.ApprovalInformationMessage);
                    self.planAcknowledgement().consentProcessed(true);
                    self.planAcknowledgement().redisplayingPlanInformation(true);
                    $("table").makeTableResponsive();
                    self.planAcknowledgementIsVisible(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#notificationHost').notificationCenter('addNotification', { message: unableToLoadPlanAcknowledgementMessage, type: "error" });
            },
            complete: function () {
                self.planAcknowledgementIsLoading(false);
            }
        });
    }


    // Function to close the cash receipt dialog box
    self.closeReceipt = function () {
        self.receiptIsDisplayed(false);
    };

    // URL to print the payment acknowledgement
    self.printReceipt = function () {
        window.open(self.printReceiptUrl(), '_blank');
    }

    //Buttons for the cashReceipt modal dialog
    self.printCashReceiptButton = {
        id: "print-cash-receipt-dialog-button",
        isPrimary: true,
        title: Ellucian.AccountActivity.ButtonLabels.cashReceiptPrintButtonLabel,
        callback: self.printReceipt
    };

    self.closeCashReceiptDialogButton = {
        id: "close-cash-receipt-dialog-button",
        isPrimary: false,
        title: Ellucian.AccountActivity.ButtonLabels.cashReceiptCloseButtonLabel,
        callback: self.closeReceipt
    };
}




