﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
var accountActivityViewModelMapping = {
    'TermPeriodBalances': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        create: function (options) {
            return new balancesModel(options.data);
        }
    },
    'FormulaCategories': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
		create: function (options) {
			return new categoryModel(options.data);
		}
    },
    'NonFormulaCategories': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        create: function (options) {
            return new categoryModel(options.data);
        }
    },
    'ReceiptData': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.ReceiptNumber);
        },
        create: function (options) {
            return new makeAPayment.PaymentAcknowledgementViewModel(options.data);
        }
    }
};

function balancesModel(data) {
    var self = this;
    ko.mapping.fromJS(data, accountActivityViewModelMapping, this);
}

function categoryModel(data) {
    var self = this;
    ko.mapping.fromJS(data, accountActivityViewModelMapping, this);

    // Absolute path for category icon image
    self.imagePath = ko.observable(baseSiteUrl + self.ImageUrl());

    // Absolute path for category prefix image
    self.prefixImagePath = ko.observable(baseSiteUrl + self.PrefixImageUrl());

    // Financial Aid category totals
    if (self.CategoryType() === 2) {

        var totalDisbursed = 0;
        var totalAnticipated = 0;

        ko.utils.arrayForEach(self.Transactions(), function (finAid) {
            totalDisbursed += Number(finAid.DisbursedAidAmount());
            totalAnticipated += Number(finAid.AnticipatedAidAmount());
        });

        self.totalDisbursedAmount = ko.observable(totalDisbursed);
        self.totalAnticipatedAmount = ko.observable(totalAnticipated);
    }

    // Deposit category totals
    if (self.CategoryType() === 4) {

        var totalDeposits = 0;
        var totalApplied = 0;
        var totalOther = 0;
        var totalRefund = 0;
        var totalNet = 0;

        ko.utils.arrayForEach(self.Transactions(), function (deposit) {
            totalDeposits += Number(deposit.DepositAmount());
            totalApplied += Number(deposit.AppliedAmount());
            totalOther += Number(deposit.OtherAmount());
            totalRefund += Number(deposit.RefundAmount());
            totalNet += Number(deposit.NetAmount());
        });

        self.totalDepositAmount = ko.observable(totalDeposits);
        self.totalAppliedAmount = ko.observable(totalApplied);
        self.totalOtherAmount = ko.observable(totalOther);
        self.totalRefundAmount = ko.observable(totalRefund);
        self.totalNetAmount = ko.observable(totalNet);
    }

    // Deposit Due category totals
    if (self.CategoryType() === 11) {

        var totalOriginal = 0;
        var totalPaid = 0;
        var totalNet = 0;

        ko.utils.arrayForEach(self.Transactions(), function (depositDue) {
            totalOriginal += Number(depositDue.OriginalAmount());
            totalPaid += Number(depositDue.PaidAmount());
            totalNet += Number(depositDue.Amount());
        });

        self.totalOriginalAmount = ko.observable(totalOriginal);
        self.totalPaidAmount = ko.observable(totalPaid);
        self.totalNetAmount = ko.observable(totalNet);
    }
}
