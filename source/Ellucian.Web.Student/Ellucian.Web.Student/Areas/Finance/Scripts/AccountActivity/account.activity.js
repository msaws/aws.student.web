﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
var accountActivityViewModelInstance = new accountActivityViewModel();

$(document).ready(function () {
    try{
        // Register the modal dialog component
        ko.components.register('modal-dialog', {
            require: 'ModalDialog/_ModalDialog'
        });
    } catch(e){ /*it might have been registered - nothing to do*/ }

    ko.applyBindings(accountActivityViewModelInstance, document.getElementById("body"));

    accountActivityViewModelInstance.loadAccountActivityData(getAccountActivityActionUrl, null);
});