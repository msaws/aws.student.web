﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
// base namespace for Immediate Payments
(function (immediatePayments, $, undefined) {

    // --------------------------
    // Payment Review View Model
    // --------------------------
    immediatePayments.PaymentReviewViewModel = function (modelData) {
        var self = this;
        var mapping = {
            'copy': ["Message", "PaymentNumber", "PaymentCount", "IsCreatingPaymentPlan"]
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        self.PaymentCountMessage = ko.computed(function () {
            return paymentReviewCountMessage.format(self.PaymentNumber, self.PaymentCount);
        });

        self.ProcessPayment = function () {
            // The message may contain embedded HTML, so we can't return it
            self.Message = "";

            var model = ko.toJS(self);
            ko.utils.postJson(ipcECheckEntryActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };
    };

    // --------------------------
    // E-Check Entry View Model
    // --------------------------
    immediatePayments.ECheckEntryViewModel = function (modelData) {
        var self = this;

        // Initialize the validation options
        var options = {
            'errorMessageClass': 'errorArea'
        };
        ko.validation.init(options);

        // Map the input data to the view model
        var mapping = {
            "observe": ["Notification", "Message", "Type", "Flash"],
            'ignore': ['Payment.CheckDetails', 'CheckDetails'],
            'copy': ['WorkflowTitle', 'WorkflowStep', 'PaymentControl', 'ReturnUrl', 'UseGuaranteedChecks', 'StateList', 'Payment', 'IsCreatingPaymentPlan']
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        //----------------------------------------------------------------//
        // Spinner elements - these are needed to process spinner dialogs //
        //----------------------------------------------------------------//
        self.isLoading = ko.observable(false);

        // Action throbber observables - used when submitting payment information
        self.actionThrobberMessage = ko.observable(makePaymentEcheckActionThrobberMessage);

        // Hack to get around strangeness with Notification mapping
        if (modelData.Notification && typeof self.Notification !== 'function') {
            self.Notification = ko.observable(modelData.Notification);
            for (var key in modelData.Notification) {
                self.Notification[key] = ko.observable(modelData.Notification[key]);
            }
        }

        // Build additional message strings
        var routingNumberNotANumberMessage = fieldNotANumberMessageString.format(routingNumberLabelString || '');
        var accountNumberNotANumberMessage = fieldNotANumberMessageString.format(accountNumberLabelString || '');
        var checkNumberNotANumberMessage = fieldNotANumberMessageString.format(checkNumberLabelString || '');

        // Set up form fields with validation
        self.Payment.CheckDetails.AbaRoutingNumber = ko.observable(modelData.Payment.CheckDetails.AbaRoutingNumber).extend({
            required: { message: requiredFieldMessageString },
            number: { message: routingNumberNotANumberMessage },
            maxLength: { params: 9, message: maximumLengthMessageString },
            minLength: { params: 9, message: minimumLengthMessageString }
        });
        self.Payment.CheckDetails.BankAccountNumber = ko.observable(modelData.Payment.CheckDetails.BankAccountNumber).extend({
            required: { message: requiredFieldMessageString },
            number: { message: accountNumberNotANumberMessage },
            maxLength: { params: 17, message: maximumLengthMessageString }
        });
        self.Payment.CheckDetails.CheckNumber = ko.observable(modelData.Payment.CheckDetails.CheckNumber).extend({
            required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks; } },
            number: { message: checkNumberNotANumberMessage },
            maxLength: { params: 7, message: maximumLengthMessageString },
            minLength: { params: 3, message: minimumLengthMessageString }
        });
        self.Payment.CheckDetails.DriversLicenseNumber = ko.observable(modelData.Payment.CheckDetails.DriversLicenseNumber).extend({
            required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks; } },
            maxLength: { params: 31, message: maximumLengthMessageString }
        });
        self.Payment.CheckDetails.DriversLicenseState = ko.observable(modelData.Payment.CheckDetails.DriversLicenseState).extend({
            required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks && self.Payment.CheckDetails.DriversLicenseNumber(); } }
        });
        self.Payment.CheckDetails.FirstName = ko.observable(modelData.Payment.CheckDetails.FirstName).extend({
            required: { message: requiredFieldMessageString },
            maxLength: { params: 30, message: maximumLengthMessageString }
        });
        self.Payment.CheckDetails.LastName = ko.observable(modelData.Payment.CheckDetails.LastName).extend({
            required: { message: requiredFieldMessageString },
            maxLength: { params: 30, message: maximumLengthMessageString }
        });
        self.Payment.CheckDetails.BillingAddress1 = ko.observable(modelData.Payment.CheckDetails.BillingAddress1).extend({
            required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks; } },
            maxLength: { params: 30, message: maximumLengthMessageString }
        });
        self.Payment.CheckDetails.City = ko.observable(modelData.Payment.CheckDetails.City).extend({
            required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks; } },
            maxLength: { params: 30, message: maximumLengthMessageString }
        });
        self.Payment.CheckDetails.State = ko.observable(modelData.Payment.CheckDetails.State).extend({
            required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks; } }
        });
        self.Payment.CheckDetails.ZipCode = ko.observable(modelData.Payment.CheckDetails.ZipCode).extend({
            required: { message: requiredFieldMessageString },
            maxLength: { params: 9, message: maximumLengthMessageString },
            minLength: { params: 5, message: minimumLengthMessageString }
        });
        self.Payment.CheckDetails.EmailAddress = ko.observable(modelData.Payment.CheckDetails.EmailAddress).extend({
            required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks; } },
            maxLength: { params: 40, message: maximumLengthMessageString },
            email: { message: invalidEmailAddressMessageString }
        });

        // Payer's full name
        self.payerName = ko.computed(function () {
            var payerName = self.Payment.CheckDetails.FirstName() + " " + self.Payment.CheckDetails.LastName();
            return payerName;
        });

        // Amount to Pay formatted using global currency settings
        self.paymentAmount = ko.observable(formatAsCurrency(self.Payment.AmountToPay));

        // The routing number entered by the payer (or its placeholder prior to entry)
        self.userRoutingNumber = ko.computed(function () {
            var routingNumber = self.Payment.CheckDetails.AbaRoutingNumber();
            if (routingNumber == null || routingNumber.length == 0) {
                routingNumber = routingNumberPlaceholderString;
            }
            return routingNumber;
        });

        // The bank account number entered by the payer (or its placeholder prior to entry)
        self.userBankAccountNumber = ko.computed(function () {
            var bankAccountNumber = self.Payment.CheckDetails.BankAccountNumber();
            if (bankAccountNumber == null || bankAccountNumber.length == 0) {
                bankAccountNumber = bankAccountPlaceholderString;
            }
            return bankAccountNumber;
        });

        // The last 4 digits of the bank account number entered by the payer
        self.userBankAccountNumberLast4Digits = ko.computed(function () {
            var last4Digits = last4DigitsPlaceholderString;
            if (self.Payment.CheckDetails.BankAccountNumber() != null) {
                if (self.Payment.CheckDetails.BankAccountNumber().length > 3) {
                    last4Digits = self.Payment.CheckDetails.BankAccountNumber().slice(-4);
                }
            }
            return last4Digits;
        });

        // The ACH authorization text displayed to the payer
        self.achAuthorizationText = ko.computed(function () {
            var todaysDate = new Date();
            var month = todaysDate.getMonth();
            var day = todaysDate.getDate();
            var year = todaysDate.getFullYear();
            var todayString = Globalize.format(new Date(year, month, day), "d");
            var tomorrowsDate = new Date(todaysDate.getTime() + 24 * 60 * 60 * 1000);
            var tomorrowString = Globalize.format(tomorrowsDate, "d");

            var achAuthText = achAuthorizationTextString.format(
                self.payerName(),
                self.paymentAmount(),
                todayString,
                self.userRoutingNumber(),
                self.userBankAccountNumber(),
                self.userBankAccountNumberLast4Digits(),
                tomorrowString);

            return achAuthText;
        });

        // Validation group for any input validation errors
        self.errors = ko.validation.group(self.Payment.CheckDetails);

        // Flag indicating whether or not there are any input validation errors
        self.allFieldsValid = ko.computed(function () {
            return self.errors().length === 0;
        });

        // Flag indicating whether or not the banking information help window is displayed
        self.bankingInformationIsDisplayed = ko.observable(false);

        // Function to open the banking information help window
        self.openBankingInformation = function () {
            self.bankingInformationIsDisplayed(true);
        };

        // Function to close the banking information help window
        self.closeBankingInformation = function () {
            self.bankingInformationIsDisplayed(false);
        };

        //Buttons for the bank information modal dialog
        self.bankingInformationCloseButton = {
            id: 'close-banking-information-dialog-button',
            isPrimary: false,
            title: Ellucian.ImmediatePayment.ButtonLabels.bankingInformationCloseButtonLabel,
            callback: self.closeBankingInformation
        };

        // This function is processed when the submit button is clicked - it posts the payer's payment information
        self.processECheck = function () {
            self.isLoading(true);
            var model = ko.toJS(self);
            ko.utils.postJson(ipcProcessPaymentActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };
    };

    // ----------------------------------
    // Payment Acknowledgement View Model
    // ----------------------------------
    immediatePayments.PaymentAcknowledgementViewModel = function (modelData) {
        var self = this;
        var mapping = {
            "observe": ["Notification"],
            "copy": ["PaymentControl", "IsCreatingPaymentPlan"]
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        // Flag indicating whether or not a printable version of the cash receipt is being used
        self.isPrintable = ko.observable(false);

        // Flag indicating whether or not to display AR payments
        self.displayPayments = ko.observable(self.Payments && self.Payments.length > 0);

        // Flag indicating whether or not to display deposits
        self.displayDeposits = ko.observable(self.Deposits && self.Deposits.length > 0);

        // Flag indicating whether or not to display non-AR payments
        self.displayOtherItems = ko.observable(self.OtherItems && self.OtherItems.length > 0);

        // Flag indicating whether or not to display convenience fees
        self.displayFees = ko.observable(self.Fees && self.Fees.length > 0);

        // Flag indicating whether or not to display tendered payments
        self.displayPaymentsTendered = ko.observable(self.PaymentsTendered && self.PaymentsTendered.length > 0);

        // Flag indicating whether or not to display change returned
        self.displayChangeReturned = ko.computed(function () {
            return (self.ChangeReturned && self.ChangeReturned > 0);
        });

        // Flag indicating whether or not to display AR payment locations
        self.showPaymentsLocation = ko.computed(function () {
            if (!self.displayPayments()) { return false; }
            var value = false;
            for (var i = 0; i < self.Payments.length && !value; i++) {
                if (self.Payments[i].Location !== null) {
                    value = true;
                }
            }
            return value;
        });

        // Flag indicating whether or not to display deposit locations
        self.showDepositsLocation = ko.computed(function () {
            if (!self.displayDeposits()) { return false; }
            var value = false;
            for (var i = 0; i < self.Deposits.length && !value; i++) {
                if (self.Deposits[i].Location !== null) {
                    value = true;
                }
            }
            return value;
        });

        // Flag indicating whether or not to display non-AR payment locations
        self.showOtherItemsLocation = ko.computed(function () {
            if (!self.displayOtherItems()) { return false; }
            var value = false;
            for (var i = 0; i < self.OtherItems.length && !value; i++) {
                if (self.OtherItems[i].Location !== null) {
                    value = true;
                }
            }
            return value;
        });

        // URL to print the payment acknowledgement
        self.printAcknowledgementUrl = ko.computed(function () {
            if (self.TransactionId != null) {
                return ipcPrintAcknowledgementActionUrl + "?ecPayTransId=" + self.TransactionId + "&payControlId=" + self.PaymentControl.Id;
            }
            else {
                return ipcPrintAcknowledgementActionUrl + "?cashRcptsId=" + self.ReceiptId + "&payControlId=" + self.PaymentControl.Id;
            }
        });

        // URL to print the payment acknowledgement
        self.printAcknowledgement = function () {
            window.open(self.printAcknowledgementUrl(), '_blank');
        }

        // Function to complete the Make a Payment workflow
        self.paymentComplete = function () {
            self.AcknowledgementText = sanitizeFormData(self.AcknowledgementText);
            var mapping = {
                'ignore': ['FooterImage', 'FooterText']
            };
            var model = ko.mapping.toJS(self, mapping);

            ko.utils.postJson(ipcPaymentAcknowledgementActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };

        // Make tables responsive
        $("table").makeTableResponsive();
    };

    // ----------------------------------
    // Payment Plan Acknowledgement View Model
    // ----------------------------------
    immediatePayments.PaymentPlanAcknowledgementViewModel = function (modelData) {
        var self = this;
        var mapping = {
            'copy': ['ScheduledPayments']
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        self.AcceptTermsAndConditions = ko.computed(function () {
            return true;
        });

        self.EnablePlanDetails = ko.computed(function () {
            return true;
        });

        self.StudentIdName = ko.computed(function () {
            var idName = (self.PaymentControl && self.PaymentControl.StudentId && typeof self.PaymentControl.StudentId === "function")
            ? self.PaymentControl.StudentId() : "";
            if (self.PlanDetails.StudentName && typeof self.PlanDetails.StudentName === 'function') {
                idName += " " + self.PlanDetails.StudentName();
            }

            return idName;
        });

        // Display the frequency using entries from the resource file instead of the element itself to allow 
        // better support for customization, as well as I18n
        self.FrequencyDisplay = ko.computed(function () {
            if (!self.PlanDetails.PaymentPlan || !self.PlanDetails.PaymentPlan.Frequency || typeof self.PlanDetails.PaymentPlan.Frequency !== "function") {
                return "";
            }
            var frequency = self.PlanDetails.PaymentPlan.Frequency();
            switch (frequency) {
                case 0:
                case "Weekly": return weeklyFrequencyDescriptionString;
                case 1:
                case "Biweekly": return biweeklyFrequencyDescriptionString;
                case 2:
                case "Monthly": return monthlyFrequencyDescriptionString;
                case 3:
                case "Yearly": return yearlyFrequencyDescriptionString;
                case 4:
                case "Custom": return customFrequencyDescriptionString;
                default: return unknownFrequencyDescriptionString;
            }
        });

        self.TotalPlanAmount = ko.computed(function () {
            return (self && self.PlanDetails.PaymentPlan && self.PlanDetails.PaymentPlan.CurrentAmount && typeof self.PlanDetails.PaymentPlan.CurrentAmount === "function" && self.PlanDetails.PaymentPlan.TotalSetupChargeAmount && typeof self.PlanDetails.PaymentPlan.TotalSetupChargeAmount === "function")
            ? (self.PlanDetails.PaymentPlan.CurrentAmount() || 0) + (self.PlanDetails.PaymentPlan.TotalSetupChargeAmount() || 0) : 0;
        });

        self.DisplayTermsText = ko.computed(function () {
            var value = "";
            if (self.PlanDetails && self.PlanDetails.TermsAndConditionsText && typeof self.PlanDetails.TermsAndConditionsText === "function" && self.PlanDetails.TermsAndConditionsText() && self.PlanDetails.TermsAndConditionsText().length > 0) {
                value = self.PlanDetails.TermsAndConditionsText().join("\r");
            }
            return value;
        });

        self.DownPaymentMessage = ko.computed(function () {
            return (self && self.PlanDetails.ReceivedDownPayment() && self.PlanDetails.ReceivedDownPayment() !== 0) ? planDownPaymentReceivedString.format(formatAsCurrency(self.PlanDetails.ReceivedDownPayment())) : null;
        });

        self.DownPaymentStyle = ko.observable('success');

        self.DisplayPlanApprovalMessage = ko.computed(function () {
            if (self.PlanDetails.ApprovalTimestamp() && self.PlanDetails.ApprovalUserId()) {
                return true;
            }
            return false;
        });

        self.PrintPlanAcknowledgementUrl = ko.computed(function () {
            return ipcPrintPlanAcknowledgementActionUrl + "?payControlId=" + self.PaymentControl.Id() + "&receivedDownPayment=" + self.PlanDetails.ReceivedDownPayment();
        });

        // URL to print the payment plan acknowledgement
        self.printPlanAcknowledgement = function () {
            window.open(self.PrintPlanAcknowledgementUrl(), '_blank');
        }

        // This function is processed when the button is clicked - it posts the data
        self.WorkflowComplete = function () {
            var model = ko.toJS(self);
            model.DisplayTermsText = sanitizeFormData(model.DisplayTermsText);
            model.PlanDetails.AcknowledgementText = sanitizeFormData(model.PlanDetails.AcknowledgementText);
            model.PlanDetails.TermsAndConditionsText = sanitizeFormData(model.PlanDetails.TermsAndConditionsText);

            ko.utils.postJson(ipcPlanAcknowledgementActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };
    };

    // --------------------------
    // Payment Complete View Model
    // --------------------------
    immediatePayments.PaymentCompleteViewModel = function (modelData) {
        var self = this;
        var mapping = {
            "observe": []
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        self.TransferControl = function () {
            var returnUrl = self.ReturnUrl();
            if (returnUrl) {
                window.location.href = returnUrl;
            }
        };

        self.DisplayMessageText = function () {
            var value = "";
            if (self.Message !== null && self.Message.length > 0) {
                value = self.Message.join("\r");
            }
            return value;
        };

        //self.WorkflowComplete = function () {
        //    var model = ko.toJS(self);
        //    ko.utils.postJson(workflowCompleteActionUrl, { model: model });
        //};
    };

} (window.immediatePayments = window.immediatePayments || {}, jQuery));

function formatTcTextForPrinting() {
    $("<div />").html($('#tcText').text()).appendTo($('#PrintRegSummary'));
    $("div").last().addClass("tcText");
    $("#tcArea").css("display", "none");
}

function formatAsCurrency(amount, precision) {
    precision = precision || 2;
    if (isNaN(precision)) { precision = 2; }
    return Globalize.format(amount, "C" + precision);
}

function parseANumber(value) {
    value = value || 0;
    return (isNaN(value)) ? Globalize.parseFloat(value) : parseFloat(value);
}