﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
$(document).ready(function () {
    var paymentAcknowledgementViewModelInstance = new makeAPayment.PaymentAcknowledgementViewModel(jsonData);
    ko.applyBindings(paymentAcknowledgementViewModelInstance, document.getElementById("body"));

    // Make tables responsive
    $("table").makeTableResponsive();

    // Add any view model notification to the notification center
    if (paymentAcknowledgementViewModelInstance.Notification != null && paymentAcknowledgementViewModelInstance.Notification.Message != null && paymentAcknowledgementViewModelInstance.Notification.Message.Length > 0) {
        $('#notificationHost').notificationCenter('addNotification', { message: paymentAcknowledgementViewModelInstance.Notification.Message, type: paymentAcknowledgementViewModelInstance.Notification.Type.toLowerCase() });
    }
});