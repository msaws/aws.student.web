﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
var makeAPaymentViewModelInstance = new makeAPayment.PaymentsViewModel();

$(document).ready(function () {

    try {
        // Register the modal dialog component
        ko.components.register('modal-dialog', {
            require: 'ModalDialog/_ModalDialog'
        });
    } catch (e) { /*it might have been registered - nothing to do*/ }

    var self = this;

    ko.applyBindings(makeAPaymentViewModelInstance, document.getElementById("body"));

    self = makeAPaymentViewModelInstance;
    
    var jsonData = { 'personId': (extractParametersFromUrl()).personId };

    $.ajax({
        url: getMakeAPaymentActionUrl,
        type: "POST",
        data: JSON.stringify(jsonData),
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {
            self.isLoading(true);
            self.showUI(false);
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                self.checkForMobile(window, document);
                ko.mapping.fromJS(data, makeAPaymentViewModelMapping, makeAPaymentViewModelInstance);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0) {
                $('#notificationHost').notificationCenter('addNotification', { message: makePaymentUnableToLoadMessage, type: "error" });
                self.NoDataMessage(makePaymentUnableToLoadMessage);
                self.noDataMessageType("error");
            }
        },
        complete: function () {
            // Make all tables in view responsive
            $("table").makeTableResponsive();

            // Accordion content must be rendered before its controller because their IDs are dynamic;
            // this will move accordion content beneath its controller for proper accordion show/hide.
            $(".multi-accordion-content").each(function () {
                $(this).next().after($(this));
            });

            // Stop the loading throbber and display the UI
            self.isLoading(false);
            self.showUI(true);
            self.preventPayPlanOutOfOrderErrors();
            setTopOfPageHandler();

        }
    });
});