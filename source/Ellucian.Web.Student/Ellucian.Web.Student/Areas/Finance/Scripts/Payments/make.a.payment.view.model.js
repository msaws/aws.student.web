﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
(function (makeAPayment, $, undefined) {

    // --------------------------
    // Payments View Model
    // --------------------------
    makeAPayment.PaymentsViewModel = function (modelData) {

        // Initialize the model
        var self = this;
        var mapping = {
            'copy':[]
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // Flag indicating whether or not the page content is ready to be displayed
        self.showUI = ko.observable(false);

        // Flag indicating whether or not an ajax request is being processed that should block the page
        // This needs to be initialized to true to prevent the AJAX request from firing when the 
        // bindings are initially applied.
        self.isLoading = ko.observable(true);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        self.changeToDesktop = function () {}
        self.changeToMobile = function () {}

        // Action throbber observables - used when submitting payment selection
        self.actionThrobberMessage = ko.observable(makePaymentActionThrobberMessage);
        
        // Expand All / Collapse All messages - used by screen reader to announce result of
        // clicking Expand All/Collapse All
        self.expandAllMessage = ko.observable(makePaymentExpandAllMessage);
        self.collapseAllMessage = ko.observable(makePaymentCollapseAllMessage);

        // Initialize model observables for initial page render
        self.AlertMessage = ko.observable();
        self.AvailablePaymentMethods = ko.observableArray();
        self.DueAmount = ko.observable(0);
        self.HasData = ko.observable(false);
        self.HasPrivacyRestriction = ko.observable(false);
        self.IsAdminUser = ko.observable(false);
        self.IsTermDisplay = ko.observable();
        self.NoDataMessage = ko.observable();
        self.noDataMessageType = ko.observable("info");
        self.PaymentsAllowed = ko.observable(true);
        self.PeriodModels = ko.observableArray();
        self.PersonId = ko.observable();
        self.PersonName = ko.observable();
        self.SelectedPaymentMethod = ko.observable();
        self.ShowCreditAmounts = ko.observable(true);
        self.TermModels = ko.observableArray();
        self.UserPaymentPlanCreationEnabled = ko.observable(false);
        self.PaymentPlanTermModels = ko.observableArray([]);
        self.PaymentPlanPeriodModels = ko.observableArray([]);
        self.PaymentPlanEligibilityText = ko.observable();
        self.PrivacyMessage = ko.observable();

        // Observables to track progress through pay plan workflow
        self.isChoosingPlanOption = ko.observable(false);
        self.isLoadingPlanDetails = ko.observable(false);
        self.proposedPlanHasError = ko.observable(false);

        // URL for switching users in Admin mode
        self.changeUserUrl = ko.observable(Ellucian.Finance.MakeAPayment.ActionUrls.mapChangeUserUrl);

        // Flag to indicate whether any errors are present
        self.hasErrors = ko.observable(false);

        // "Dummy" observable to force globalErrorMessage re-evaluation when error condition changes
        self.errorChange = ko.observable();

        // ID and Name for student in Admin mode
        self.currentStudentInfo = ko.computed(function () {
            if (self.IsAdminUser()) {
                return self.PersonId() + " " + self.PersonName();
            }
            return null;
        });

        // Currency symbol for display adjacent to amount inputs
        self.currencySymbol = ko.computed(function () {
            return Globalize.culture().numberFormat.currency.symbol;
        });

        // Function to determine if multiple payment groups are selected
        self.checkForMultiplePaymentGroups = function () {
            var multipleGroups = false;

            // Term Mode
            if (self.IsTermDisplay()) {
                var selectedPaymentGroups = [];
                ko.utils.arrayForEach(self.TermModels(), function (term) {
                    if (!multipleGroups) {
                        var selectedItems = ko.utils.arrayFilter(term.PaymentsDue(), function (pmtDue) {
                            return pmtDue.IsSelected() == true;
                        });
                        var paymentGroups = selectedItems === null ? null : ko.utils.arrayMap(selectedItems, function (item) {
                            return item.PaymentGroup.peek();
                        });
                        if (paymentGroups !== null) {
                            var uniqueGroups = paymentGroups.filter(onlyUnique);
                            ko.utils.arrayForEach(uniqueGroups, function (group) {
                                selectedPaymentGroups.push(group);
                            });
                            selectedPaymentGroups = selectedPaymentGroups.filter(onlyUnique);
                            multipleGroups = (selectedPaymentGroups.length > 1);
                        } else {
                            multipleGroups = false;
                        }
                    }
                });
            }

                // PCF Mode
            else {
                var selectedPaymentGroups = [];
                ko.utils.arrayForEach(self.PeriodModels(), function (period) {
                    ko.utils.arrayForEach(period.Terms(), function (term) {
                        if (!multipleGroups) {
                            var selectedItems = ko.utils.arrayFilter(term.PaymentsDue(), function (pmtDue) {
                                return pmtDue.IsSelected() == true;
                            });
                            var paymentGroups = selectedItems === null ? null : ko.utils.arrayMap(selectedItems, function (item) {
                                return item.PaymentGroup.peek();
                            });
                            if (paymentGroups !== null) {
                                var uniqueGroups = paymentGroups.filter(onlyUnique);
                                ko.utils.arrayForEach(uniqueGroups, function (group) {
                                    selectedPaymentGroups.push(group);
                                });
                                selectedPaymentGroups = selectedPaymentGroups.filter(onlyUnique);
                                multipleGroups = (selectedPaymentGroups.length > 1);
                            } else {
                                multipleGroups = false;
                            }
                        }
                    });
                });

            }
            self.errorChange(multipleGroups);
            return multipleGroups;
        }

        // Function to determine if any selected items have errors
        self.checkForInputErrors = function () {
            var hasErrors = false;

            // Term Mode
            if (self.IsTermDisplay()) {
                var itemsWithErrors = null;
                ko.utils.arrayForEach(self.TermModels(), function (term) {
                    if (itemsWithErrors === null) {
                        itemsWithErrors = ko.utils.arrayFirst(term.PaymentsDue(), function (pmtDue) {
                            return pmtDue.amountToPay.hasError.peek() == true;
                        });
                    }

                    var clearedItems = ko.utils.arrayFilter(term.PaymentsDue(), function (pmtDue) {
                        return pmtDue.amountToPay() === "";
                    });
                    if (clearedItems !== null) {
                        ko.utils.arrayForEach(clearedItems, function (pmtDue) {
                            pmtDue.IsSelected(false);
                        });
                    }
                });

                // PCF Mode
            } else {
                var itemsWithErrors = null;
                ko.utils.arrayForEach(self.PeriodModels(), function (period) {
                    ko.utils.arrayForEach(period.Terms(), function (term) {
                        if (itemsWithErrors === null) {
                            itemsWithErrors = ko.utils.arrayFirst(term.PaymentsDue(), function (pmtDue) {
                                return pmtDue.amountToPay.hasError.peek() == true;
                            });
                        }

                        var clearedItems = ko.utils.arrayFilter(term.PaymentsDue(), function (pmtDue) {
                            return pmtDue.amountToPay() === "";
                        });
                        if (clearedItems !== null) {
                            ko.utils.arrayForEach(clearedItems, function (pmtDue) {
                                pmtDue.IsSelected(false);
                            });
                        }
                    });
                });
            }
            hasErrors = (itemsWithErrors !== null);
            self.errorChange(hasErrors);
            return hasErrors;
        }

        // Prevent payment plan payments from being selected out of order
        self.preventPayPlanOutOfOrderErrors = function () {

            var allPlanItems = [];
            // Term Mode
            if (self.IsTermDisplay()) {

                // Loop through each term and identify all payment plan items
                ko.utils.arrayForEach(self.TermModels(), function (term) {
                    allPlanItems = allPlanItems.concat(getTermPlanItems(term));
                });

                // PCF Mode
            } else {

                // Loop through each term and identify all payment plan items
                ko.utils.arrayForEach(self.PeriodModels(), function (period) {
                    ko.utils.arrayForEach(period.Terms(), function (term) {
                        allPlanItems = allPlanItems.concat(getTermPlanItems(term));
                    });
                });
            }

            if (allPlanItems !== null && allPlanItems.length > 0) {

                // Get all of the plan IDs
                var planIds = ko.utils.arrayMap(allPlanItems, function (pi) {
                    return pi.PaymentPlanId();
                }).filter(onlyUnique);
                planIds = planIds.filter(onlyUnique);

                // Build an array of plan items by plan ID
                var planItemsById = [];
                if (planIds.length > 0) {
                    ko.utils.arrayForEach(planIds, function (id) {
                        planItemsById.push(ko.utils.arrayFilter(allPlanItems, function (planItem) {
                            return planItem.PaymentPlanId() === id;
                        }));
                    });
                }

                // Evaluate plan items on a per-plan basis
                if (planItemsById.length > 0) {
                    ko.utils.arrayForEach(planItemsById, function (plan) {
                        ko.utils.arrayForEach(plan, function (planItem) {
                            var futureItems = ko.utils.arrayFilter(plan, function (item) {
                                return item.DueDate() > planItem.DueDate();
                            });
                            if (futureItems.length > 0) {
                                ko.utils.arrayForEach(futureItems, function (futureItem) {
                                    if (!planItem.IsSelected()) {
                                        futureItem.IsSelected(false);
                                        futureItem.isDisabled(true);
                                    }
                                    else if (planItem.IsEditable() && parseDollarAmount(planItem.amountToPay()) !== parseDollarAmount(planItem.DueAmount())) {
                                        futureItem.IsSelected(false);
                                        futureItem.isDisabled(true);
                                    } else {
                                        futureItem.isDisabled(false);
                                    }
                                });
                            }
                        });
                    });
                }
            }
        }

        // Function to evaluate user selections for errors
        self.checkForErrors = function () {

            // Check for multiple payment groups selected and user input errors
            var hasErrors = self.checkForMultiplePaymentGroups() || self.checkForInputErrors();

            // Set hasErrors indicator
            self.hasErrors(hasErrors);

            // Format all textboxes with 2 decimal places
            formatAmounts();
        }

        // Function to determine if a valid payment method has been chosen
        self.validPaymentMethodSelected = ko.computed(function () {
            return self.SelectedPaymentMethod();
        });

        // Global error message indicating error(s) on the page
        self.globalErrorMessage = ko.computed(function () {
            self.errorChange();
            var message = [];
            if (self.showUI()) {
                if (self.hasErrors() && self.checkForMultiplePaymentGroups()) {
                    message.push(multipleDistributionsMessage);
                    $("#aria-announcements").text(multipleDistributionsMessage);
                }
                if (self.hasErrors() && self.checkForInputErrors()) {
                    message.push(correctErrorsMessage);
                    $("#aria-announcements").text(correctErrorsMessage);
                }
                return message;
            }
            return null;
        });

        // Total payment amount based on selected items
        self.totalPayment = ko.computed(function () {
            if (!self.showUI()) {
                return 0;
            } else {
                var total = 0;
                if (self.IsTermDisplay()) {
                    ko.utils.arrayForEach(self.TermModels(), function (term) {
                        ko.utils.arrayForEach(term.PaymentsDue(), function (pmtDue) {
                            total += pmtDue.IsPayable() && pmtDue.IsSelected() && pmtDue.amountToPay() ? parseDollarAmount(pmtDue.amountToPay()) : 0;
                        });
                    });
                } else {
                    ko.utils.arrayForEach(self.PeriodModels(), function (period) {
                        ko.utils.arrayForEach(period.Terms(), function (term) {
                            ko.utils.arrayForEach(term.PaymentsDue(), function (pmtDue) {
                                total += pmtDue.IsPayable() && pmtDue.IsSelected() && pmtDue.amountToPay() ? parseDollarAmount(pmtDue.amountToPay()) : 0;
                            });
                        });
                    });
                }
                return total;
            }
        });

        // Function to post the payment data
        self.beginPayment = function () {
            self.isLoading(true);

            var mapping = {
                'ignore': ['Restrictions']
            };
            var model = ko.mapping.toJS(self, mapping);
            model.AlertMessage = sanitizeFormData(model.AlertMessage);
            model.PaymentPlanEligibilityText = sanitizeFormData(model.PaymentPlanEligibilityText);

            ko.utils.postJson(mapPaymentReviewActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };

        // Message when no payment plan options present
        self.noPaymentPlanOptionsMessage = ko.observable(null);

        // Function to begin payment plan workflow
        self.beginPaymentPlan = function () {

            // Set data for POST
            var data = { termModelsJson: null, periodModelsJson: null };
            if (self.IsTermDisplay()) {
                data.termModelsJson = ko.toJSON(self.TermModels());
            } else {
                data.periodModelsJson = ko.toJSON(self.PeriodModels());
            }

            // AJAX call for PaymentsController action
            $.ajax({
                url: mapGetPaymentPlanOptionsAsyncActionUrl,
                type: "POST",
                data: JSON.stringify(data),
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function () {
                    self.payPlanDialogIsLoading(true);
                    self.payPlanDialogIsDisplayed(false);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.checkForMobile(window, document);
                        if (typeof data.Message !== 'undefined' && data.Message !== null) {
                            self.noPaymentPlanOptionsMessage(data.Message);
                        } else {
                            if (self.IsTermDisplay()) {
                                ko.mapping.fromJS(data, makeAPaymentViewModelMapping, self.PaymentPlanTermModels);
                            } else {
                                ko.mapping.fromJS(data, makeAPaymentViewModelMapping, self.PaymentPlanPeriodModels);
                            }
                        }
                        self.isChoosingPlanOption(true);
                        self.payPlanDialogIsDisplayed(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: createPaymentPlanUnableToLoadMessage, type: "error" });
                    }
                },
                complete: function () {
                    self.payPlanDialogIsLoading(false);
                    $("table").makeTableResponsive();
                }
            });
        }

        // Function to submit payment plan selection
        self.submitPaymentPlan = function () {
            self.payPlanDialogIsDisplayed(false);
            self.isLoadingPlanDetails(true);
            ko.utils.postJson(mapPaymentPlanUrl, { inputJson: ko.toJS(self.selectedPaymentPlanOption()), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        }

        // Function to cancel payment plan workflow
        self.cancelPaymentPlan = function () {
            self.payPlanDialogIsDisplayed(false);
            self.proposedPlanHasError(false);
            self.selectedPaymentPlanOption(null);
            self.isChoosingPlanOption(false);
        }

        // Observable to track if pay plan dialog is loading
        self.payPlanDialogIsLoading = ko.observable(false);

        // Observable to track if pay plan dialog is visible
        self.payPlanDialogIsDisplayed = ko.observable(false);

        // Observable to track the selected payment plan option
        self.selectedPaymentPlanOption = ko.observable();

        // Flag indicating if the user has any payment plan options
        self.hasPaymentPlanOptions = ko.pureComputed(function () {
            return ((self.PaymentPlanTermModels() !== null && self.PaymentPlanTermModels().length > 0) ||
                (self.PaymentPlanPeriodModels() !== null && self.PaymentPlanPeriodModels().length > 0));
        });

        // Label for cancel button on payment plan dialog
        self.paymentPlanCancelButtonLabel = ko.pureComputed(function () {
            if (self.hasPaymentPlanOptions()) {
                return createPaymentPlanDialogCancelButtonLabel;
            }
            return createPaymentPlanOkButtonLabel;
        });

        // Flag indicating whether or not to show the continue button
        self.showContinueButton = ko.pureComputed(function () {
            return self.isChoosingPlanOption() && self.hasPaymentPlanOptions();
        });

        //Buttons for payment plan dialog
        self.submitPaymentPlanButton = {
            id: "submit-payment-plan-dialog-button",
            isPrimary: true,
            title: Ellucian.MakePayment.ButtonLabels.createPaymentPlanContinueButtonLabel,
            visible: self.showContinueButton,
            enabled: self.selectedPaymentPlanOption,
            callback: self.submitPaymentPlan
        };

        self.cancelPaymentPlanButton = {
            id: "close-payment-plan-dialog-button",
            isPrimary: false,
            title: self.paymentPlanCancelButtonLabel,
            callback: self.cancelPaymentPlan
        };
    }

    // --------------------------
    // Payment Review View Model
    // --------------------------
    makeAPayment.PaymentReviewViewModel = function (modelData) {
        var self = this;
        var mapping = {
            'copy': ["Message", "PaymentNumber", "PaymentCount", "IsCreatingPaymentPlan", "Notification"]
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // Flag indicating whether or not an ajax request is being processed that should block the page
        // This needs to be initialized to true to prevent the AJAX request from firing when the 
        // bindings are initially applied.
        self.isLoading = ko.observable(false);

        // Action throbber observables - used when submitting payment information
        self.actionThrobberMessage = ko.observable(makePaymentReviewActionThrobberMessage);
        
        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        // Message showing the current payment relative to the total number of payments
        self.PaymentCountMessage = ko.computed(function () {
            return paymentReviewCountMessage.format(self.PaymentNumber, self.PaymentCount);
        });

        // Function to post the payment review data
        self.ProcessPayment = function () {
            // The message may contain embedded HTML, so we can't return it
            self.Message = "";
            self.isLoading(true);

            var model = ko.toJS(self);
            ko.utils.postJson(mapECheckEntryActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };
    };

    // --------------------------
    // E-Check Entry View Model
    // --------------------------
    makeAPayment.ECheckEntryViewModel = function (modelData) {
        var self = this;

        // Initialize the validation options
        var options = {
            'errorMessageClass': 'errorArea'
        };
        ko.validation.init(options);

        var mapping = {
            'copy': []
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // Flag indicating whether or not an ajax request is being processed that should block the page
        // This needs to be initialized to true to prevent the AJAX request from firing when the 
        // bindings are initially applied.
        self.isLoading = ko.observable(false);

        // Action throbber observables - used when submitting payment information
        self.actionThrobberMessage = ko.observable(makePaymentEcheckActionThrobberMessage);
        
        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        // Set up form fields with validation
        if (self.Payment != null && self.Payment.CheckDetails != null) {
            self.Payment.CheckDetails.AbaRoutingNumber = ko.observable(modelData.Payment.CheckDetails.AbaRoutingNumber).extend({
                required: { message: requiredFieldMessageString },
                number: { message: fieldNotANumberMessageString.format(routingNumberLabelString || '') },
                maxLength: { params: 9, message: maximumLengthMessageString },
                minLength: { params: 9, message: minimumLengthMessageString }
            });
            self.Payment.CheckDetails.BankAccountNumber = ko.observable(modelData.Payment.CheckDetails.BankAccountNumber).extend({
                required: { message: requiredFieldMessageString },
                number: { message: fieldNotANumberMessageString.format(accountNumberLabelString || '') },
                maxLength: { params: 17, message: maximumLengthMessageString }
            });
            self.Payment.CheckDetails.CheckNumber = ko.observable(modelData.Payment.CheckDetails.CheckNumber).extend({
                required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks(); } },
                number: { message: fieldNotANumberMessageString.format(checkNumberLabelString || '') },
                maxLength: { params: 7, message: maximumLengthMessageString },
                minLength: { params: 3, message: minimumLengthMessageString }
            });
            self.Payment.CheckDetails.DriversLicenseNumber = ko.observable(modelData.Payment.CheckDetails.DriversLicenseNumber).extend({
                required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks(); } },
                maxLength: { params: 31, message: maximumLengthMessageString }
            });
            self.Payment.CheckDetails.DriversLicenseState = ko.observable(modelData.Payment.CheckDetails.DriversLicenseState).extend({
                required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks() && self.Payment.CheckDetails.DriversLicenseNumber(); } }
            });
            self.Payment.CheckDetails.FirstName = ko.observable(modelData.Payment.CheckDetails.FirstName).extend({
                required: { message: requiredFieldMessageString },
                maxLength: { params: 30, message: maximumLengthMessageString }
            });
            self.Payment.CheckDetails.LastName = ko.observable(modelData.Payment.CheckDetails.LastName).extend({
                required: { message: requiredFieldMessageString },
                maxLength: { params: 30, message: maximumLengthMessageString }
            });
            self.Payment.CheckDetails.BillingAddress1 = ko.observable(modelData.Payment.CheckDetails.BillingAddress1).extend({
                required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks(); } },
                maxLength: { params: 30, message: maximumLengthMessageString }
            });
            self.Payment.CheckDetails.City = ko.observable(modelData.Payment.CheckDetails.City).extend({
                required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks(); } },
                maxLength: { params: 30, message: maximumLengthMessageString }
            });
            self.Payment.CheckDetails.State = ko.observable(modelData.Payment.CheckDetails.State).extend({
                required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks(); } }
            });
            self.Payment.CheckDetails.ZipCode = ko.observable(modelData.Payment.CheckDetails.ZipCode).extend({
                required: { message: requiredFieldMessageString },
                maxLength: { params: 9, message: maximumLengthMessageString },
                minLength: { params: 5, message: minimumLengthMessageString }
            });
            self.Payment.CheckDetails.EmailAddress = ko.observable(modelData.Payment.CheckDetails.EmailAddress).extend({
                required: { message: requiredFieldMessageString, onlyIf: function () { return self.UseGuaranteedChecks(); } },
                maxLength: { params: 40, message: maximumLengthMessageString },
                email: { message: invalidEmailAddressMessageString }
            });
        }

        // Payer's full name
        self.payerName = ko.computed(function () {
            if (self.Payment != null && self.Payment.CheckDetails != null) {
                var payerName = self.Payment.CheckDetails.FirstName() + " " + self.Payment.CheckDetails.LastName();
                return payerName;
            }
            return null;
        });

        // Amount to Pay formatted using global currency settings
        self.paymentAmount = ko.computed(function () {
            if (self.Payment != null && self.Payment.AmountToPay !== undefined) {
                return formatAsCurrency(self.Payment.AmountToPay());
            }
            return formatAsCurrency(0);
        });

        // The routing number entered by the payer (or its placeholder prior to entry)
        self.userRoutingNumber = ko.computed(function () {
            if (self.Payment != null && self.Payment.CheckDetails != null) {
                var routingNumber = self.Payment.CheckDetails.AbaRoutingNumber();
                if (routingNumber == null || routingNumber.length == 0) {
                    routingNumber = routingNumberPlaceholderString;
                }
                return routingNumber;
            }
            return null;
        });

        // The bank account number entered by the payer (or its placeholder prior to entry)
        self.userBankAccountNumber = ko.computed(function () {
            if (self.Payment != null && self.Payment.CheckDetails != null) {
                var bankAccountNumber = self.Payment.CheckDetails.BankAccountNumber();
                if (bankAccountNumber == null || bankAccountNumber.length == 0) {
                    bankAccountNumber = bankAccountPlaceholderString;
                }
                return bankAccountNumber;
            }
            return null;
        });

        // The last 4 digits of the bank account number entered by the payer
        self.userBankAccountNumberLast4Digits = ko.computed(function () {
            if (self.Payment != null && self.Payment.CheckDetails != null) {
                var last4Digits = last4DigitsPlaceholderString;
                if (self.Payment.CheckDetails.BankAccountNumber() != null) {
                    if (self.Payment.CheckDetails.BankAccountNumber().length > 3) {
                        last4Digits = self.Payment.CheckDetails.BankAccountNumber().slice(-4);
                    }
                }
                return last4Digits;
            }
            return null;
        });

        // The ACH authorization text displayed to the payer
        self.achAuthorizationText = ko.computed(function () {
            var todaysDate = new Date();
            var month = todaysDate.getMonth();
            var day = todaysDate.getDate();
            var year = todaysDate.getFullYear();
            var todayString = Globalize.format(new Date(year, month, day), "d");
            var tomorrowsDate = new Date(todaysDate.getTime() + 24 * 60 * 60 * 1000);
            var tomorrowString = Globalize.format(tomorrowsDate, "d");

            var achAuthText = achAuthorizationTextString.format(
                self.payerName(),
                self.paymentAmount(),
                todayString,
                self.userRoutingNumber(),
                self.userBankAccountNumber(),
                self.userBankAccountNumberLast4Digits(),
                tomorrowString);

            return achAuthText;
        });

        if (self.Payment != null && self.Payment.CheckDetails != null) {
            // Validation group for any input validation errors
            self.errors = ko.validation.group(self.Payment.CheckDetails);

            // Flag indicating whether or not there are any input validation errors
            self.allFieldsValid = ko.computed(function () {
                return self.errors().length === 0;
            });
        }

        // Flag indicating whether or not the banking information help window is displayed
        self.bankingInformationIsDisplayed = ko.observable(false);

        // Function to open the banking information help window
        self.openBankingInformation = function () {
            self.bankingInformationIsDisplayed(true);
        };

        // Function to close the banking information help window
        self.closeBankingInformation = function () {
            self.bankingInformationIsDisplayed(false);
        };

        //Buttons for the bank information modal dialog
        self.bankingInformationCloseButton = {
            id: 'close-banking-information-dialog-button',
            isPrimary: false,
            title: Ellucian.MakePayment.ButtonLabels.bankingInformationCloseButtonLabel,
            callback: self.closeBankingInformation
        };

        // This function is processed when the submit button is clicked - it posts the payer's payment information
        self.processECheck = function () {
            self.isLoading(true);
            var model = ko.toJS(self);
            ko.utils.postJson(mapProcessPaymentActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };
    };

    // ----------------------------------
    // Payment Acknowledgement View Model
    // ----------------------------------
    makeAPayment.PaymentAcknowledgementViewModel = function (modelData) {

        // Initialize the model
        var self = this;
        var mapping = {
            'copy': []
        };
        ko.mapping.fromJS(modelData, mapping, self);

        // Flag indicating whether or not an ajax request is being processed that should block the page
        // This needs to be initialized to true to prevent the AJAX request from firing when the 
        // bindings are initially applied.
        self.isLoading = ko.observable(false);

        // Action throbber observables - used when submitting payment information
        self.actionThrobberMessage = ko.observable(makePaymentAcknowledgementActionThrobberMessage);

        // Flag indicating whether or not a printable version is being used
        self.isPrintable = ko.observable(false);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Overrides for base view model change functions to remove alerts - do nothing
        self.changeToDesktop = function () { }
        self.changeToMobile = function () { }

        // Flag indicating whether or not to display AR payments
        self.displayPayments = ko.computed(function () {
            return (self.Payments !== undefined && self.Payments() && self.Payments().length > 0);
        });

        // Flag indicating whether or not to display deposits
        self.displayDeposits = ko.computed(function () {
            return (self.Deposits !== undefined && self.Deposits() && self.Deposits().length > 0);
        });

        // Flag indicating whether or not to display non-AR payments
        self.displayOtherItems = ko.computed(function () {
            return (self.OtherItems !== undefined && self.OtherItems() && self.OtherItems().length > 0);
        });

        // Flag indicating whether or not to display convenience fees
        self.displayFees = ko.computed(function () {
            return (self.Fees !== undefined && self.Fees() && self.Fees().length > 0);
        });

        // Flag indicating whether or not to display tendered payments
        self.displayPaymentsTendered = ko.computed(function () {
            return (self.PaymentsTendered !== undefined && self.PaymentsTendered() && self.PaymentsTendered().length > 0);
        });

        // Flag indicating whether or not to display change returned
        self.displayChangeReturned = ko.computed(function () {
            return (self.ChangeReturned !== undefined && self.ChangeReturned() && self.ChangeReturned() > 0);
        });

        // Flag indicating whether or not to display AR payment locations
        self.showPaymentsLocation = ko.computed(function () {
            if (!self.displayPayments()) { return false; }
            var value = false;
            for (var i = 0; i < self.Payments().length && !value; i++) {
                if (self.Payments()[i].Location()) {
                    value = true;
                }
            }
            return value;
        });

        // Flag indicating whether or not to display deposit locations
        self.showDepositsLocation = ko.computed(function () {
            if (!self.displayDeposits()) { return false; }
            var value = false;
            for (var i = 0; i < self.Deposits().length && !value; i++) {
                if (self.Deposits()[i].Location()) {
                    value = true;
                }
            }
            return value;
        });

        // Flag indicating whether or not to display non-AR payment locations
        self.showOtherItemsLocation = ko.computed(function () {
            if (!self.displayOtherItems()) { return false; }
            var value = false;
            for (var i = 0; i < self.OtherItems().length && !value; i++) {
                if (self.OtherItems()[i].Location()) {
                    value = true;
                }
            }
            return value;
        });

        // URL to print the payment acknowledgement
        self.printAcknowledgementUrl = ko.computed(function () {
            if (self.TransactionId !== undefined && self.TransactionId()) {
                return mapPrintAcknowledgementActionUrl + "?ecPayTransId=" + self.TransactionId();
            }
            else if (self.ReceiptId !== undefined && self.ReceiptId()) {
                return mapPrintAcknowledgementActionUrl + "?cashRcptsId=" + self.ReceiptId();
            } else {
                return null;
            }
        });

        // URL to print the payment acknowledgement
        self.printAcknowledgement = function () {
            window.open(self.printAcknowledgementUrl(), '_blank');
        }

        // Aria Label for payment complete button
        self.buttonAriaLabel = ko.observable(makePaymentAcknowledgementAriaLabel);

        // Function to complete the Make a Payment workflow
        self.paymentComplete = function () {
            self.isLoading(true);

            var mapping = {
                'ignore': ['AcknowledgementText','FooterImage','FooterText']
            };

            var model = ko.mapping.toJS(self, mapping);
            ko.utils.postJson(mapCompleteActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        };
    };

    // ----------------------------------
    // Payment Plan View Model
    // ----------------------------------
    makeAPayment.PaymentPlanViewModel = function (modelData) {

        // Initialize the model
        var self = this;
        var mapping = {
            'observe': []
        };
        ko.mapping.fromJS(modelData, {}, this);

        // Flag indicating whether or not the page content is ready to be displayed
        self.showUI = ko.observable(false);

        // Flag indicating whether or not the page is loading data
        self.isLoading = ko.observable(false);

        // "Inherit" the base view model with a mobile breakpoint of 768 pixels
        BaseViewModel.call(self, 768);

        // Subscribe to mobile changes to trigger table responsification
        self.isMobile.subscribe(function () {
            $("table").makeTableResponsive();
        })

        // Action throbber observables - used when submitting payment information
        self.actionThrobberMessage = ko.observable(loadingPaymentPlanDetailsMessage);

        // Flag indicating whether or not the user's acceptance is being processed
        self.isProcessingAcceptance = ko.observable(false);

        // Flag indicating if terms and conditions are accepted
        self.termsAndConditionsAccepted = ko.observable(false);

        // Flag indicating if user's consent was processed
        self.consentProcessed = ko.observable(false);

        // Flag indicating if Return to MAP button should display
        self.returnToMapButtonDisplayed = ko.pureComputed(function () {
            return self.consentProcessed() || (self.ErrorMessage !== undefined && self.ErrorMessage());
        })

        // Overrides for base view model change functions to remove alerts
        self.changeToDesktop = function () {
            self.downPaymentDialogOptions({
                autoOpen: false, modal: true, minWidth: $(window).width() / 2, draggable: false,
                show: { effect: "fade", duration: dialogFadeTimeMs }, hide: { effect: "fade", duration: dialogFadeTimeMs }, resizable: false,
            });
        }
        self.changeToMobile = function () {
            self.downPaymentDialogOptions({
                autoOpen: false, modal: true, minWidth: $(window).width() / 2, draggable: false,
                show: { effect: "fade", duration: dialogFadeTimeMs }, hide: { effect: "fade", duration: dialogFadeTimeMs }, resizable: false,
            });
        }

        // Observables for view-model properties
        self.ApprovalInformationMessage = ko.observable();

        // Function to accept payment plan terms and conditions
        self.acceptTermsAndConditions = function () {

            self.DisplayPaymentPlan.AcknowledgementText = sanitizeFormData(self.DisplayPaymentPlan.AcknowledgementText);
            self.DisplayPaymentPlan.TermsAndConditionsText = sanitizeFormData(self.DisplayPaymentPlan.TermsAndConditionsText);
            var mapping = {
                'ignore': ['PlanTermsAndConditionsText']
            };
            var data = { planModelJson: ko.toJSON(self) };

            $.ajax({
                url: mapAcceptPaymentPlanTermsAndConditionsAsyncUrl,
                type: "POST",
                data: JSON.stringify(data),
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function () {
                    self.actionThrobberMessage(createPaymentPlanProcessingAcceptanceMessage);
                    self.isLoading(true);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.ApprovalInformationMessage(data.ApprovalInformationMessage);
                        self.consentProcessed(true);
                        ko.mapping.fromJS(data.PlanDetails.DisplayPaymentPlan, makeAPaymentViewModelMapping, self.DisplayPaymentPlan);
                        $('#notificationHost').notificationCenter('addNotification', { message: data.ApprovalInformationMessage, type: "success", flash: true });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#notificationHost').notificationCenter('addNotification', { message: createPaymentPlanUnableToProcessAcceptanceMessage, type: "error" });
                },
                complete: function () {
                    self.isLoading(false);
                }
            });
        }

        // Flag indicating whether or not to show down payment dialog
        self.downPaymentDialogIsDisplayed = ko.observable(false);
        
        // Observable to indicate that down payment warning should be visible
        self.redisplayingPlanInformation = ko.observable(false);
    
        // Warning message if down payment is required
        self.downPaymentWarning = ko.pureComputed(function () {
            if (self.DisplayPaymentPlan != null &&
                self.DisplayPaymentPlan.DownPaymentAmount !== undefined &&
                typeof self.DisplayPaymentPlan.DownPaymentAmount == 'function' &&
                self.DisplayPaymentPlan.DownPaymentAmount() > 0 &&
                !self.redisplayingPlanInformation()) {
                return paymentPlanDownPaymentString.format(formatAsCurrency(self.DisplayPaymentPlan.DownPaymentAmount()));
            }
            return null;
        });

        // "Must consent" warning message
        self.paymentPlanWarningMessage = ko.pureComputed(function () {
            return createPaymentPlanWarningMessage.format(createPaymentPlanAgreeCheckboxLabel, Ellucian.MakePayment.ButtonLabels.createPaymentPlanContinueButtonLabel);
        });

        // Flag indicating whether or not an error occurred
        self.errorOccurred = ko.observable(false);

        // Function to return to MAP
        self.goToMakeAPayment = function () {
            if (!self.errorOccurred() && 
                self.DisplayPaymentPlan != null &&
                self.DisplayPaymentPlan.DownPaymentAmount() != null &&
                self.DisplayPaymentPlan.DownPaymentAmount() > 0) {
                self.downPaymentDialogIsDisplayed(true);
            }
            else {
                openUrl(mapActionUrl);
            }
        }

        // Function to kick off down payment workflow
        self.beginDownPayment = function () {
            var data = { paymentPlanId: ko.toJSON(self.DisplayPaymentPlan.Id()), selectedPaymentMethod: ko.toJSON(self.SelectedPaymentMethod()) };

            $.ajax({
                url: mapPaymentPlanDownPaymentActionUrl,
                type: "POST",
                data: JSON.stringify(data),
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function () {
                    self.actionThrobberMessage(paymentPlanStartingDownPaymentMessage);
                    self.isLoading(true);
                },
                success: function(data) {
                    var model = data;
                    var mapping = {
                        'ignore': ['Restrictions']
                    };
                    var model = ko.mapping.toJS(model, mapping);
                    model.AlertMessage = sanitizeFormData(model.AlertMessage);
                    model.PaymentPlanEligibilityText = sanitizeFormData(model.PaymentPlanEligibilityText);
                    ko.utils.postJson(mapPaymentReviewActionUrl, { model: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    self.errorOccurred(true);
                    $('#notificationHost').notificationCenter('addNotification', { message: paymentPlanDownPaymentGenericError, type: "error" });
                },
                complete: function () {
                    self.isLoading(false);
                    self.downPaymentDialogIsDisplayed(false);
                }
            });
        }

        // Flag indicating whether or not a down payment pay method was selected
        self.downPaymentPayMethodSelected = ko.pureComputed(function () {
            if (self.SelectedPaymentMethod !== undefined && typeof self.SelectedPaymentMethod == 'function') {
                return self.SelectedPaymentMethod();
            }
            return false;
        });

        // Function to kick off down payment workflow
        self.exitPage = function () {
            openUrl(mapActionUrl);
        }

        // Function to load printer-friendly view
        self.printAdvisoryMessage = ko.pureComputed(function () {
            return paymentPlanPleasePrintMessage.format(paymentPlanSummaryHeaderLabel, paymentPlanScheduleHeaderLabel,
                paymentPlanTermsAndConditionsLabel);
        });

        // Observable to track workflow state
        self.workflowState = ko.pureComputed(function () {
            return self.consentProcessed() ? paymentPlanAcknowledgementTitle : paymentPlanPreviewTitle;

        });

        //Buttons for the make a down payment dialog
        self.makeDownPaymentButton = {
            id: "make-down-payment-dialog-button",
            isPrimary: true,
            enabled: self.downPaymentPayMethodSelected,
            title: Ellucian.MakePayment.ButtonLabels.paymentPlanAcknowledgementDownPaymentLabel,
            callback: self.beginDownPayment
        };

        self.payLaterButton = {
            id: "pay-later-dialog-button",
            isPrimary: false,
            title: Ellucian.MakePayment.ButtonLabels.paymentPlanPayDownPaymentLaterButtonLabel,
            callback: self.exitPage
        };
    };

}(window.makeAPayment = window.makeAPayment || {}, jQuery));

// Function to preserve amount textbox formatting
formatAmounts = function () {
    $('input[type="text"]').each(function () {
        var value = $(this).val();
        var float = !isNaN(value) ? parseFloat(value) : value;
        if (!isNaN(float)) {
            $(this).val(Globalize.format(float, "n2"));
        } else {
            return;
        }
    });
}

// Function to get all payment plan items for a term
getTermPlanItems = function (term) {
    var allPlanItems = [];
    var payPlanItems = ko.utils.arrayFilter(term.PaymentsDue(), function (pmtDue) {
        return pmtDue.PaymentPlanId() !== null && pmtDue.PaymentPlanId() !== '';
    });
    if (payPlanItems !== null) {
        ko.utils.arrayForEach(payPlanItems, function (ppi) {
            allPlanItems.push(ppi);
        });
    }
    return allPlanItems;
}

// Utility function to retrieve only unique values from an array
onlyUnique = function (value, index, self) {
    return self.indexOf(value) === index;
}

