﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
$(document).ready(function () {
    try {
        // Register the modal dialog component
        ko.components.register('modal-dialog', {
            require: 'ModalDialog/_ModalDialog'
        });
    } catch (e) { /*it might have been registered - nothing to do*/ }

    var eCheckEntryViewModelInstance = new makeAPayment.ECheckEntryViewModel(jsonData);
    ko.applyBindings(eCheckEntryViewModelInstance, document.getElementById("body"));

    // Add any view model notification to the notification center
    if (eCheckEntryViewModelInstance.Notification != null && eCheckEntryViewModelInstance.Notification.Message != null && eCheckEntryViewModelInstance.Notification.Message.Length > 0) {
        $('#notificationHost').notificationCenter('addNotification', { message: eCheckEntryViewModelInstance.Notification.Message, type: eCheckEntryViewModelInstance.Notification.Type.toLowerCase() });
    }
});