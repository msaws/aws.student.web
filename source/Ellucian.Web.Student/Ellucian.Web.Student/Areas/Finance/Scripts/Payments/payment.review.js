﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
$(document).ready(function () {
    var paymentReviewViewModelInstance = new makeAPayment.PaymentReviewViewModel(jsonData);
    ko.applyBindings(paymentReviewViewModelInstance, document.getElementById("body"));

    // Make tables responsive
    $("table").makeTableResponsive();

    // Add any view model notification to the notification center
    if (paymentReviewViewModelInstance.Notification != null && paymentReviewViewModelInstance.Notification.Message != null && paymentReviewViewModelInstance.Notification.Message.length > 0) {
        $('#notificationHost').notificationCenter('addNotification', { message: paymentReviewViewModelInstance.Notification.Message, type: paymentReviewViewModelInstance.Notification.Type.toLowerCase() });
    }
});