﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
var makeAPaymentViewModelMapping = {
    'TermModels': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.TermCode);
        },
        create: function (options) {
            return new termModel(options);
        }
    },
    'PeriodModels': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.PeriodCode);
        },
        create: function (options) {
            return new periodModel(options);
        }
    },
    'PaymentPlanTermModels': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.TermCode);
        },
        create: function (options) {
            return new termModel(options);
        }
    },
    'PaymentPlanPeriodModels': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.PeriodCode);
        },
        create: function (options) {
            return new periodModel(options);
        }
    }
};

function termModel(options) {
    var self = this;
    ko.mapping.fromJS(options.data, makeAPaymentViewModelMapping, this);

    // Build the term object
    self = createTerm(self, null, options);
}

function periodModel(options) {
    if (!options.parent.IsTermDisplay()) {
        var self = this;
        ko.mapping.fromJS(options.data, makeAPaymentViewModelMapping, this);

        // Build each term object
        ko.utils.arrayForEach(self.Terms(), function (term) {
            term = createTerm(term, self.PeriodCode(), options);
        });
    }
}

// function to create a term
function createTerm(term, periodCode, options) {

    // Observable for amount to pay on each payment due 
    ko.utils.arrayForEach(term.PaymentsDue(), function (pmtDue) {
        createPaymentDue(pmtDue, options);
    });
}

// function to create a payment due
function createPaymentDue(pmtDue, options) {

    // Observable to track if payment item is disabled
    pmtDue.isDisabled = ko.observable(false);

    // Input validation messages
    pmtDue.minimumAmountMessage = ko.observable(minimumAmountMessageString.format(formatAsCurrency(pmtDue.MinimumPayment())));
    pmtDue.maximumAmountMessage = ko.observable(maximumAmountMessageString.format(formatAsCurrency(pmtDue.DueAmount())));

    // Computed observable to auto-focus on amount input once item is selected
    pmtDue.amountHasFocus = ko.computed(function () {
        return pmtDue.IsSelected();
    });

    // Computed observable for default amount to pay
    pmtDue.defaultAmount = ko.computed(function () {
        if (pmtDue.IsCredit()) {
            return null;
        } else {
            return parseFloat(pmtDue.DueAmount()).toFixed(2);
        }
    });

    // Function to reset Paid Amount value
    pmtDue.resetAmountToPay = function () {
        pmtDue.amountToPay(pmtDue.defaultAmount());
    }

    // Call error checking logic whenever checkbox is changed
    pmtDue.IsSelected.subscribe(function () {
        options.parent.checkForErrors();
        options.parent.preventPayPlanOutOfOrderErrors();
    });

    // Validated observable for amount to pay
    pmtDue.amountToPay = ko.observable(pmtDue.defaultAmount()).extend({
        numeric: {
            precision: 2,
            zeroNull: true,
            max: pmtDue.DueAmount(),
            maxMessage: pmtDue.maximumAmountMessage(),
            min: pmtDue.MinimumPayment(),
            minMessage: pmtDue.minimumAmountMessage(),
            nanMessage: invalidAmountMessageString,
            valCondition: function () { return pmtDue.IsSelected(); },
            updateOnError: false
        }
    });

    // Set the PaidAmount to the current amountToPay value for POST
    pmtDue.PaidAmount(pmtDue.amountToPay());

    // Call error checking logic whenever amount to pay is changed
    pmtDue.amountToPay.subscribe(function () {
        options.parent.checkForErrors();
        options.parent.preventPayPlanOutOfOrderErrors();
        pmtDue.PaidAmount(pmtDue.amountToPay());
    });
}
