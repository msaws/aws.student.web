﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
var adminSearchViewModelInstance = new adminSearchViewModel();

$(document).ready(function () {
    ko.applyBindings(adminSearchViewModelInstance, document.getElementById("body"));
    adminSearchViewModelInstance.getFinanceAdminSearchViewModel("Payments");
});