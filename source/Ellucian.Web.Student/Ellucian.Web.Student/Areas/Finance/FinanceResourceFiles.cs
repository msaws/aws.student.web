﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.

namespace Ellucian.Web.Student.Areas.Finance
{
    public static class FinanceResourceFiles
    {
        public static readonly string AccountSummaryResources = "AccountSummaryResources";
        public static readonly string AccountActivityResources = "AccountActivityResources";
        public static readonly string FinanceResources = "FinanceResources";
        public static readonly string ImmediatePaymentResources = "ImmediatePaymentResources";
        public static readonly string PaymentResources = "PaymentResources";
        public static readonly string StatementResources = "StatementResources";
    }
}
