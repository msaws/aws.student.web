﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    /// <summary>
    /// Display model for a payment plan
    /// </summary>
    public class PaymentPlanDisplayModel
    {
        public PaymentPlan PaymentPlan { get; set; }

        /// <summary>
        /// Current amount of the plan
        /// </summary>
        public decimal CurrentAmount { get; set; }
       
        /// <summary>
        /// Current status of the plan
        /// </summary>
        public string CurrentStatus { get; set; }
        
        /// <summary>
        /// Date on which current status was given
        /// </summary>
        public string CurrentStatusDate { get; set; }
        
        /// <summary>
        /// Plan down payment amount
        /// </summary>
        public decimal DownPaymentAmount { get; set; }
        
        /// <summary>
        /// Amount of plan down payment that was paid
        /// </summary>
        public decimal DownPaymentAmountPaid { get; set; }
        
        /// <summary>
        /// Date on which down payment is due, if there is a down payment
        /// </summary>
        public string DownPaymentDate { get; set; }
        
        /// <summary>
        /// Percentage of the total plan amount that is due as down payment
        /// </summary>
        public decimal DownPaymentPercentage { get; set; }
        
        /// <summary>
        /// Date on which first scheduled payment is due
        /// </summary>
        public string FirstDueDate { get; set; }
        
        /// <summary>
        /// Scheduled payment frequency
        /// </summary>
        public string Frequency { get; set; }
        /// <summary>
        /// Number of days by which payments may be late before late charges are assessed
        /// </summary>
        public int GraceDays { get; set; }
        
        /// <summary>
        /// Unique identifier for the plan
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// Flat late fee amount
        /// </summary>
        public decimal LateChargeAmount { get; set; }
        
        /// <summary>
        /// Percentage late fee
        /// </summary>
        public decimal LateChargePercentage { get; set; }
        
        /// <summary>
        /// Number of scheduled payments
        /// </summary>
        public int NumberOfPayments { get; set; }
        
        /// <summary>
        /// Original payment plan amount
        /// </summary>
        public decimal OriginalAmount { get; set; }
        
        /// <summary>
        /// Identifier for the payment plan account holder
        /// </summary>
        public string PersonId { get; set; }
        
        /// <summary>
        /// Receivable type code for the payment plan
        /// </summary>
        public string ReceivableTypeCode { get; set; }
        
        /// <summary>
        /// Flat setup fee
        /// </summary>
        public decimal SetupAmount { get; set; }
        
        /// <summary>
        /// Percentage setup fee amount
        /// </summary>
        public decimal SetupPercentage { get; set; }
        
        /// <summary>
        /// Unique identifier for the payment plan template from which the plan was bulit
        /// </summary>
        public string TemplateId { get; set; }
        
        /// <summary>
        /// ID of the academic term in which the payment plan resides
        /// </summary>
        public string TermId { get; set; }
        
        /// <summary>
        /// Total setup charge amount
        /// </summary>
        public decimal TotalSetupChargeAmount { get; set; }

        /// <summary>
        /// Total payment plan amount including setup fees
        /// </summary>
        public decimal PlanTotalAmount { get { return CurrentAmount + TotalSetupChargeAmount; } }

        /// <summary>
        /// ID and name of the plan account holder
        /// </summary>
        public string PlanDisplayName { get; set; }

        /// <summary>
        /// Description of the plan term
        /// </summary>
        public string PlanDisplayTerm { get; set; }

        /// <summary>
        /// List of plan charges
        /// </summary>
        public List<PlanChargeDisplayModel> PlanCharges { get; set; }

        /// <summary>
        /// List of scheduled payments
        /// </summary>
        public List<ScheduledPaymentDisplayModel> ScheduledPayments { get; set; }

        /// <summary>
        /// Down payment required message
        /// </summary>
        public string DownPaymentMessage 
        { 
            get 
            {
                if (DownPaymentAmount > 0 && !string.IsNullOrEmpty(DownPaymentDate)) 
                {
                    return string.Format(GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanDownPaymentWarningMessage"),
                        DownPaymentAmount.ToString("C", CultureInfo.CurrentCulture), 
                        DownPaymentDate);
                }
                return null;
            }
        }

        /// <summary>
        /// Creates a new instance of the <see cref="PaymentPlanDisplayModel"/> class.
        /// </summary>
        /// <param name="paymentPlan">Payment Plan</param>
        /// <param name="cleanIds">Flag indicating whether or not to remove unique record keys</param>
        public static PaymentPlanDisplayModel Build(PaymentPlan paymentPlan, bool cleanIds = true)
        {
            if (paymentPlan == null)
            {
                throw new ArgumentNullException("paymentPlan", "A payment plan must be provided.");
            }
            PaymentPlanDisplayModel model = new PaymentPlanDisplayModel()
            {
                CurrentAmount = paymentPlan.CurrentAmount,
                CurrentStatus = paymentPlan.CurrentStatus.ToString(),
                CurrentStatusDate = paymentPlan.CurrentStatusDate.ToShortDateString(),
                DownPaymentAmount = paymentPlan.DownPaymentAmount,
                DownPaymentAmountPaid = paymentPlan.DownPaymentAmountPaid,
                DownPaymentDate = paymentPlan.DownPaymentDate.HasValue ? paymentPlan.DownPaymentDate.Value.ToShortDateString() : string.Empty,
                DownPaymentPercentage = paymentPlan.DownPaymentPercentage,
                FirstDueDate = paymentPlan.FirstDueDate.ToShortDateString(),
                Frequency = paymentPlan.Frequency.ToString(),
                GraceDays = paymentPlan.GraceDays,
                Id = paymentPlan.Id,
                LateChargeAmount = paymentPlan.LateChargeAmount,
                LateChargePercentage = paymentPlan.LateChargePercentage,
                NumberOfPayments = paymentPlan.NumberOfPayments,
                OriginalAmount = paymentPlan.OriginalAmount,
                PaymentPlan = paymentPlan,
                PersonId = paymentPlan.PersonId,
                PlanCharges = new List<PlanChargeDisplayModel>(),
                ReceivableTypeCode = paymentPlan.ReceivableTypeCode,
                ScheduledPayments = new List<ScheduledPaymentDisplayModel>(),
                SetupAmount = paymentPlan.SetupAmount,
                SetupPercentage = paymentPlan.SetupPercentage,
                TemplateId = paymentPlan.TemplateId,
                TermId = paymentPlan.TermId,
                TotalSetupChargeAmount = paymentPlan.TotalSetupChargeAmount,
            };

            if (paymentPlan.PlanCharges != null)
            {
                foreach (var planCharge in paymentPlan.PlanCharges)
                {
                    model.AddPlanCharge(planCharge);
                }
            }

            if (paymentPlan.ScheduledPayments != null)
            {
                foreach (var scheduledPayment in paymentPlan.ScheduledPayments)
                {
                    model.AddScheduledPayment(scheduledPayment);
                }
            }

            if (cleanIds) model.CleanRecordKeys();
            return model;
        }

        /// <summary>
        /// Adds a plan charge to the plan
        /// </summary>
        /// <param name="planCharge">Payment Plan Charge</param>
        private void AddPlanCharge(PlanCharge planCharge)
        {
            if (planCharge == null)
            {
                throw new ArgumentNullException("planCharge", "A payment plan charge must be provided.");
            }
            PlanCharges.Add(PlanChargeDisplayModel.Build(planCharge));
        }

        /// <summary>
        /// Adds a scheduled payment to the plan
        /// </summary>
        /// <param name="planCharge">Payment Plan Scheduled Payment</param>
        private void AddScheduledPayment(ScheduledPayment scheduledPayment)
        {
            if (scheduledPayment == null)
            {
                throw new ArgumentNullException("scheduledPayment", "A payment plan scheduled payment must be provided.");
            }
            ScheduledPayments.Add(ScheduledPaymentDisplayModel.Build(scheduledPayment));
        }

        private void CleanRecordKeys()
        {
            Id = string.Empty;
            if (ScheduledPayments != null && ScheduledPayments.Any())
            {
                ScheduledPayments.ForEach(sp => sp.Id = string.Empty);
                ScheduledPayments.ForEach(sp => sp.PlanId = string.Empty);
            }
            if (PlanCharges != null && PlanCharges.Any())
            {
                PlanCharges.ForEach(pc => pc.Id.Substring(pc.Id.IndexOf('*') + 1));
                PlanCharges.ForEach(pc => pc.PlanId = string.Empty);
            }
            if (PaymentPlan != null)
            {
                PaymentPlan.Id = string.Empty;
                if (PaymentPlan.ScheduledPayments != null && PaymentPlan.ScheduledPayments.Any())
                {
                    foreach (var sp in PaymentPlan.ScheduledPayments)
                    {
                        sp.Id = string.Empty;
                        sp.PlanId = string.Empty;
                    }
                }
                if (PaymentPlan.PlanCharges != null && PaymentPlan.PlanCharges.Any())
                {
                    foreach (var pc in PaymentPlan.PlanCharges)
                    {
                        pc.Id = pc.Id.Substring(pc.Id.IndexOf('*') + 1);
                        pc.PlanId = string.Empty;
                    }
                }
            }
        }
    }
}