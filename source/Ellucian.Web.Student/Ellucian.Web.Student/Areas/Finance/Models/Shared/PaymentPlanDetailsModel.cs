﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    /// <summary>
    /// Payment Plan Details model
    /// </summary>
    public class PaymentPlanDetailsModel
    {
        /// <summary>
        /// Time at which the payment plan details are displayed to the user
        /// </summary>
        public DateTimeOffset DisplayTimestamp { get; set; }

        /// <summary>
        /// Acknowledgement Text for the payment plan
        /// </summary>
        public List<string> AcknowledgementText { get; set; }

        /// <summary>
        /// Payment Plan information
        /// </summary>
        public PaymentPlan PaymentPlan { get; set; }

        /// <summary>
        /// Payment Plan display information
        /// </summary>
        public PaymentPlanDisplayModel DisplayPaymentPlan { get; set; }

        /// <summary>
        /// Name of the student
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// Terms and Conditions Text for the payment plan
        /// </summary>
        public List<string> TermsAndConditionsText { get; set; }
    
        /// <summary>
        /// Down Payment Amount - either the amount required or the actual payment amount made
        /// </summary>
        public decimal ReceivedDownPayment { get; set; }

        /// <summary>
        /// Date and time at which the payment plan terms and conditions were approved
        /// </summary>
        public DateTimeOffset? ApprovalTimestamp { get; set; }

        /// <summary>
        /// User ID of the person who approved the payment plan terms and conditions
        /// </summary>
        public string ApprovalUserId { get; set; }

        /// <summary>
        /// Flag specifying whether or not the plan details are printable
        /// </summary>
        public bool IsPrintable { get; set; }

        /// <summary>
        /// Plan frequency display
        /// </summary>
        public string PlanFrequencyDisplay { get; set; }

        /// <summary>
        /// Display name for account holder
        /// </summary>
        public string PlanDisplayName { get; set; }

        /// <summary>
        /// Total amount of the proposed payment plan
        /// </summary>
        public decimal PlanTotalAmount { get; set; }

        /// <summary>
        /// Formatted payment plan terms and conditions
        /// </summary>
        public string PlanTermsAndConditionsText { get; set; }

        /// <summary>
        /// Error message
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// The list of available payment methods
        /// </summary>
        public List<AvailablePaymentMethod> AvailablePaymentMethods { get; set; }

        /// <summary>
        /// The selected payment method
        /// </summary>
        public AvailablePaymentMethod SelectedPaymentMethod { get; set; }


        /// <summary>
        /// Model base constructor
        /// </summary>
        public PaymentPlanDetailsModel()
        {
            AcknowledgementText = new List<string>();
            PaymentPlan = new PaymentPlan();
            TermsAndConditionsText = new List<string>();
            DisplayPaymentPlan = new PaymentPlanDisplayModel();
        }

        /// <summary>
        /// Static method to build model for a payment plan that has not yet been approved
        /// </summary>
        /// <param name="proposedPlan">Proposed Payment Plan</param>
        /// <param name="accountHolder">Account Holder</param>
        /// <param name="ackText">Acknowledgement Text</param>
        /// <param name="termsText">Terms and Conditions Text</param>
        /// <param name="minimumDownPayment">Minimum Required Down Payment</param>
        /// <returns>Payment Plan Details model</returns>
        public static PaymentPlanDetailsModel BuildPlanPreview(PaymentPlan proposedPlan, AccountHolder accountHolder, 
            IEnumerable<string> ackText, IEnumerable<string> termsText)
        {
            var model = new PaymentPlanDetailsModel();

            model.DisplayTimestamp = DateTime.Now;
            model.AcknowledgementText = ackText.ToList();
            model.PaymentPlan = proposedPlan;
            model.StudentName = accountHolder.PreferredName;
            model.TermsAndConditionsText = termsText.ToList();
            model.ReceivedDownPayment = 0;
            model.ApprovalTimestamp = null;
            model.ApprovalUserId = null;
            model.IsPrintable = false;

            return model;
        }

        /// <summary>
        /// Static method to build model for an approved payment plan
        /// </summary>
        /// <param name="paymentPlan">Approved Payment Plan</param>
        /// <param name="planApproval">Payment Plan Approval</param>
        /// <param name="ackText">Acknowledgement Text</param>
        /// <param name="termsDoc">Terms and Conditions Document</param>
        /// <param name="termsResponse">Terms and Conditions Response</param>
        /// <param name="actualDownPayment">Actual Down Payment Amount</param>
        /// <param name="isPrintable">Flag specifying whether or not the plan details are printable</param>
        /// <param name="termDescription">Description of the plan term</param>
        /// <returns>Payment Plan Details model</returns>
        public static PaymentPlanDetailsModel BuildPlanAcknowledgement(PaymentPlan paymentPlan, PaymentPlanApproval planApproval,
            string ackText, ApprovalDocument termsDoc, ApprovalResponse termsResponse, decimal actualDownPayment, bool isPrintable,
            string termDescription = null)
        {
            var model = new PaymentPlanDetailsModel();

            model.DisplayTimestamp = planApproval.Timestamp;
            model.AcknowledgementText = new List<string>() { ackText };
            model.PaymentPlan = paymentPlan;
            model.StudentName = planApproval.StudentName;
            model.TermsAndConditionsText = termsDoc.Text.ToList();
            model.ReceivedDownPayment = actualDownPayment;
            model.ApprovalTimestamp = termsResponse.Received;
            model.ApprovalUserId = termsResponse.UserId;
            model.IsPrintable = isPrintable;
            model.DisplayPaymentPlan = PaymentPlanDisplayModel.Build(paymentPlan, false);
            model.DisplayPaymentPlan.PlanDisplayName = String.Format("{0} {1}", planApproval.StudentId, planApproval.StudentName);
            model.DisplayPaymentPlan.PlanDisplayTerm = termDescription;
            model.PlanTermsAndConditionsText = string.Join(Environment.NewLine, termsDoc.Text);

            return model;
        }

        /// <summary>
        /// Static method to build model for collecting the down payment
        /// </summary>
        /// <param name="paymentPlan">Approved Payment Plan</param>
        /// <param name="planApproval">Payment Plan Approval</param>
        /// <param name="ackDoc">Acknowledgement Document</param>
        /// <param name="termsDoc">Terms and Conditions Document</param>
        /// <param name="termsResponse">Terms and Conditions Response</param>
        /// <param name="termDescription">Description of the plan term</param>
        /// <returns>Payment Plan Details model</returns>
        public static PaymentPlanDetailsModel BuildDownPaymentModel(PaymentPlan paymentPlan, PaymentPlanApproval planApproval,
            ApprovalDocument ackDoc, ApprovalDocument termsDoc, ApprovalResponse termsResponse, string termDescription = null)
        {
            var model = new PaymentPlanDetailsModel();

            model.DisplayTimestamp = planApproval.Timestamp;
            model.AcknowledgementText = ackDoc.Text.ToList();
            model.PaymentPlan = paymentPlan;
            model.StudentName = planApproval.StudentName;
            model.TermsAndConditionsText = termsDoc.Text.ToList();
            model.ReceivedDownPayment = 0;
            model.ApprovalTimestamp = termsResponse.Received;
            model.ApprovalUserId = termsResponse.UserId;
            model.IsPrintable = false;
            model.DisplayPaymentPlan.PlanDisplayName = String.Format("{0} {1}", planApproval.StudentId, planApproval.StudentName);
            model.DisplayPaymentPlan.PlanDisplayTerm = termDescription;

            return model;
        }

        /// <summary>
        /// Static method to build model for a display-only payment plan that has not yet been approved
        /// </summary>
        /// <param name="proposedPlan">Proposed Payment Plan display model</param>
        /// <param name="accountHolder">Account Holder</param>
        /// <param name="ackText">Acknowledgement Text</param>
        /// <param name="termsText">Terms and Conditions Text</param>
        /// <param name="configuration">Finance configuration data</param>
        /// <param name="termDescription">Description of the plan term</param>
        /// <returns>Payment Plan Details model</returns>
        public static PaymentPlanDetailsModel BuildDisplayPlanPreview(PaymentPlanDisplayModel proposedPlan, AccountHolder accountHolder,
            IEnumerable<string> ackText, IEnumerable<string> termsText, FinanceConfiguration configuration, string termDescription)
        {
            if (proposedPlan == null)
            {
                throw new ArgumentNullException("proposedPlan", "Proposed payment plan cannot be null.");
            }
            if (accountHolder == null)
            {
                throw new ArgumentNullException("accountHolder", "Account Holder cannot be null.");
            }
            if (ackText == null || !ackText.Any())
            {
                throw new ArgumentNullException("ackText", "Acknowledgement Text cannot be null.");
            }
            if (termsText == null || !termsText.Any())
            {
                throw new ArgumentNullException("termsText", "Terms and Conditions Text cannot be null.");
            }
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration", "Finance configuration cannot be null.");
            }
            var model = new PaymentPlanDetailsModel();

            model.AvailablePaymentMethods = configuration.PaymentMethods;
            model.DisplayTimestamp = DateTime.Now;
            model.AcknowledgementText = ackText.ToList();
            model.DisplayPaymentPlan = proposedPlan;
            model.StudentName = accountHolder.PreferredName;
            model.TermsAndConditionsText = termsText.ToList();
            model.ReceivedDownPayment = 0;
            model.ApprovalTimestamp = null;
            model.ApprovalUserId = null;
            model.IsPrintable = false;
            model.DisplayPaymentPlan.PlanDisplayName = String.Format("{0} {1}", accountHolder.Id, accountHolder.PreferredName);
            model.DisplayPaymentPlan.PlanDisplayTerm = termDescription;
            model.PlanTermsAndConditionsText = string.Join(Environment.NewLine, termsText);

            return model;
        }

        /// <summary>
        /// Static method to build model when an error has occurred
        /// </summary>
        /// <returns>Payment Plan Details model</returns>
        public static PaymentPlanDetailsModel BuildWithErrorMessage()
        {
            var model = new PaymentPlanDetailsModel() 
            {
                ErrorMessage = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "CreatePaymentPlanUnableToGetProposedPlanMessage")
            };
            return model;
        }
    }
}
