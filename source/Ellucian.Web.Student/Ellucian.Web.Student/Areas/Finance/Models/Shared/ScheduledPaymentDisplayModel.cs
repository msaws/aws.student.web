﻿using Ellucian.Colleague.Dtos.Finance;
// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    public class ScheduledPaymentDisplayModel
    {
        /// <summary>
        /// Amount of the scheduled payment
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Unique identifier for the plan charge
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Paid amount of the scheduled payment
        /// </summary>
        public decimal AmountPaid { get; set; }

        /// <summary>
        /// Scheduled payment due date
        /// </summary>
        public string DueDate { get; set; }

        /// <summary>
        /// Flag indicating if the scheduled payment is overdue
        /// </summary>
        public bool IsPastDue { get; set; }

        /// <summary>
        /// Scheduled payment date of last payment
        /// </summary>
        public string LastPaidDate { get; set; }

        /// <summary>
        /// Payment plan ID for the scheduled payment
        /// </summary>
        public string PlanId { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="ScheduledPaymentDisplayModel"/> class.
        /// </summary>
        /// <param name="paymentPlan">Payment Plan Scheduled Payment</param>
        public static ScheduledPaymentDisplayModel Build(ScheduledPayment scheduledPayment)
        {
            if (scheduledPayment == null)
            {
                throw new ArgumentNullException("planCharge", "A payment plan scheduled payment must be provided.");
            }

            ScheduledPaymentDisplayModel model = new ScheduledPaymentDisplayModel()
            {
                Amount = scheduledPayment.Amount,
                AmountPaid = scheduledPayment.AmountPaid,
                DueDate = scheduledPayment.DueDate.ToShortDateString(),
                Id = scheduledPayment.Id,
                IsPastDue = scheduledPayment.IsPastDue,
                LastPaidDate = scheduledPayment.LastPaidDate.HasValue ? scheduledPayment.LastPaidDate.Value.ToShortDateString() : string.Empty,
                PlanId = scheduledPayment.PlanId
            };

            return model;
        }
    }
}