﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    /// <summary>
    /// Model for displaying section data
    /// </summary>
    public class SectionDisplayModel
    {
        /// <summary>
        /// Section name - subject, course number, section number
        /// </summary>
        public string Section { get; set; }

        /// <summary>
        /// Section title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Term description in which section is offered
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// Number of credits registered for
        /// </summary>
        public decimal? Credits { get; set; }

        /// <summary>
        /// Number of CEUs awarded for section
        /// </summary>
        public decimal? Ceus { get; set; }

        /// <summary>
        /// Description of the section's location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Section meeting information
        /// </summary>
        public List<string> MeetingInformation { get; set; }

        /// <summary>
        /// Section starting and ending dates
        /// </summary>
        public string StartEndDate { get; set; }

        /// <summary>
        /// Names of instructors teaching the section
        /// </summary>
        public List<string> Faculty { get; set; }

        /// <summary>
        /// Names of buildings and rooms in which the section is taught
        /// </summary>
        public List<string> Classrooms { get; set; }

        /// <summary>
        /// Model constructor
        /// </summary>
        public SectionDisplayModel()
        {
            MeetingInformation = new List<string>();
            Faculty = new List<string>();
            Classrooms = new List<string>();
        }
    }
}
