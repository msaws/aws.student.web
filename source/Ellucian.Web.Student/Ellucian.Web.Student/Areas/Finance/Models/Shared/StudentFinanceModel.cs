﻿using Ellucian.Colleague.Dtos.Base;
// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    /// <summary>
    /// Base model for Student Finance view models
    /// </summary>
    public abstract class StudentFinanceModel
    {
        /// <summary>
        /// This is the alert message displayed at the top of the form
        /// </summary>
        public string AlertMessage { get; set; }

        /// <summary>
        /// Is the current user an administrative user?
        /// </summary>
        public bool IsAdminUser { get; set; }

        /// <summary>
        /// The accountholder's ID
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// The accountholder's name
        /// </summary>
        public string PersonName { get; set; }

        /// <summary>
        /// Are we using TERM or PCF mode?
        /// </summary>
        public bool IsTermDisplay { get; set; }
    }
}