﻿using Ellucian.Colleague.Dtos.Finance;
// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    public class PlanChargeDisplayModel
    {
        /// <summary>
        /// Amount of the charge
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Charge details
        /// </summary>
        public ChargeDisplayModel Charge { get; set; }

        /// <summary>
        /// Unique identifier for the plan charge
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Flag indicating if the charge can be automatically modified
        /// </summary>
        public bool IsAutomaticallyModifiable { get; set; }

        /// <summary>
        /// Flag indicating if the charge is a setup fee
        /// </summary>
        public bool IsSetupCharge { get; set; }

        /// <summary>
        /// ID of the payment plan to which this charge belongs
        /// </summary>
        public string PlanId { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="PaymentPlanDisplayModel"/> class.
        /// </summary>
        /// <param name="paymentPlan">Payment Plan Charge</param>
        public static PlanChargeDisplayModel Build(PlanCharge planCharge)
        {
            if (planCharge == null)
            {
                throw new ArgumentNullException("planCharge", "A payment plan charge must be provided.");
            }
            var model = new PlanChargeDisplayModel()
            {
                Amount = planCharge.Amount,
                Charge = ChargeDisplayModel.Build(planCharge.Charge),
                Id = planCharge.Id,
                IsAutomaticallyModifiable = planCharge.IsAutomaticallyModifiable,
                IsSetupCharge = planCharge.IsSetupCharge,
                PlanId = planCharge.PlanId
            };
            return model;
        }
    }
}