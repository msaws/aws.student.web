﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    /// <summary>
    /// Model for displaying registration charges
    /// </summary>
    public class RegistrationChargeModel
    {
        /// <summary>
        /// AR account type description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Distribution/payment group code
        /// </summary>
        public string Distribution { get; set; }

        /// <summary>
        /// Term description
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// Amount of charges for the AR type
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Date charges are due
        /// </summary>
        public DateTime DueDate { get; set; }
    }
}
