﻿using Ellucian.Colleague.Dtos.Finance;
// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    public class ChargeDisplayModel
    {
        /// <summary>
        /// Amount of the charge
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Base amount of the charge
        /// </summary>
        public decimal BaseAmount { get; set; }

        /// <summary>
        /// Charge code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Charge description
        /// </summary>
        public List<string> Description { get; set; }

        /// <summary>
        /// Unique identifier for the charge
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Unique identifier for the invoice to which the charge belongs
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// Amount of tax on the charge
        /// </summary>
        public decimal TaxAmount { get; set; }

        public List<string> PaymentPlanIds { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="ChargeDisplayModel"/> class.
        /// </summary>
        /// <param name="paymentPlan">Charge</param>
        public static ChargeDisplayModel Build(Charge charge)
        {
            if (charge == null)
            {
                throw new ArgumentNullException("charge", "A charge must be provided.");
            }
            ChargeDisplayModel model = new ChargeDisplayModel()
            {
                Amount = charge.Amount,
                BaseAmount = charge.BaseAmount,
                Code = charge.Code,
                Description = charge.Description,
                Id = charge.Id,
                InvoiceId = charge.InvoiceId,
                TaxAmount = charge.TaxAmount,
                PaymentPlanIds = new List<string>()

            };
            if (charge.PaymentPlanIds != null)
            {
                foreach (var planId in charge.PaymentPlanIds)
                {
                    model.AddPaymentPlanId(planId);
                }
            }
            return model;
        }

        /// <summary>
        /// Adds a payment plan ID to the charge
        /// </summary>
        /// <param name="planId">Payment Plan ID</param>
        private void AddPaymentPlanId(string planId)
        {
            if (planId == null)
            {
                throw new ArgumentNullException("planId", "A payment plan ID must be provided.");
            }
            if (!PaymentPlanIds.Contains(planId))
            {
                PaymentPlanIds.Add(planId);
            }
        }
    }
}