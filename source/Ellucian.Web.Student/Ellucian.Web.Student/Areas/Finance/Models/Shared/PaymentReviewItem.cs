﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.

namespace Ellucian.Web.Student.Areas.Finance.Models.Shared
{
    /// <summary>
    /// A line item on the payment review form
    /// </summary>
    public class PaymentReviewItem
    {
        /// <summary>
        /// Line item description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Line item amount
        /// </summary>
        public decimal Amount { get; set; }
    }
}