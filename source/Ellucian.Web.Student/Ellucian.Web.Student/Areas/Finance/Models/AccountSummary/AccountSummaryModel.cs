﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Areas.Finance.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountSummary
{
    /// <summary>
    /// This is the main model used for the Account Summary view
    /// </summary>
    public class AccountSummaryModel : StudentFinanceAdminModel
    {
        /// <summary>
        /// Optional, due date for accountholder's next payment due
        /// </summary>
        public string NextPaymentDueDate { get; set; }

        /// <summary>
        /// Amount currently due to be paid by accountholder
        /// </summary>
        public decimal AmountDue { get; set; }

        /// <summary>
        /// Total amount past due for accountholder
        /// </summary>
        public decimal OverdueAmount { get; set; }

        /// <summary>
        /// Total amount due to be paid by accountholder
        /// </summary>
        public decimal TotalAmountDue { get { return AmountDue + OverdueAmount; } }

        /// <summary>
        /// Accountholder's overall account balance
        /// </summary>
        public decimal AccountBalance { get; set; }

        /// <summary>
        /// The list of terms/periods and their balances
        /// </summary>
        public List<TermPeriodBalanceModel> TimeframeBalances { get; set; }

        /// <summary>
        /// The list of helpful links
        /// </summary>
        public List<StudentFinanceLink> HelpfulLinks { get; set; }

        /// <summary>
        /// Flag indicating whether user can see a link to Account Activity
        /// </summary>
        public bool ShowAccountActivityLink { get; set; }

        /// <summary>
        /// Flag indicating whether user can see a link to Make a Payment
        /// </summary>
        public bool ShowMakeAPaymentLink { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountSummaryModel"/> class with payments due by term.
        /// </summary>
        /// <param name="accountHolder">The account holder</param>
        /// <param name="accountDue">Payments due by term</param>
        /// <param name="accountActivityTerms">Account activity terms and balances</param>
        /// <param name="config">Student Finance configuration</param>
        /// <param name="terms">Collection of terms</param>
        /// <param name="currentUser">The user currently logged into self service</param>
        /// <returns>An AccountSummaryModel object</returns>
        public static AccountSummaryModel BuildForPaymentsDueByTerm(AccountHolder accountHolder, 
            AccountDue accountDue, 
            AccountActivityPeriods accountActivityTerms, 
            FinanceConfiguration config,
            IEnumerable<Term> terms,
            ICurrentUser currentUser)
        {
            if (accountHolder == null)
            {
                throw new ArgumentNullException("accountHolder", "Account holder information cannot be null.");
            }
            if (accountDue == null || accountDue.AccountTerms == null)
            {
                throw new ArgumentNullException("accountDue", "Payments due by term information cannot be null.");
            }
            if (accountActivityTerms == null)
            {
                throw new ArgumentNullException("accountActivityTerms", "Account activity terms and balances cannot be null.");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config", "Student finance configuration data cannot be null.");
            }
            if (terms == null)
            {
                throw new ArgumentNullException("terms", "Collection of terms cannot be null.");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser", "Current authenticated user information cannot be null.");
            }

            // Build term models
            List<TermSummaryModel> termModels = new List<TermSummaryModel>();
            accountDue.AccountTerms.ForEach(at => termModels.Add(TermSummaryModel.Build(at, config)));

            // Isolate payments due by term
            List<PaymentsDueModel> paymentsDue = termModels.SelectMany(t => t.PaymentsDue).ToList();

            bool isAdmin = currentUser.PersonId != accountHolder.Id && !currentUser.ProxySubjects.Any();

            // Build timeframe account balances
            var termPeriodBalances = new List<TermPeriodBalanceModel>();
            foreach (var period in accountActivityTerms.Periods)
            {
                termPeriodBalances.Add(TermPeriodBalanceModel.Build(period, accountHolder.Id, isAdmin));
            }
            if (accountActivityTerms.NonTermActivity != null)
            {
                termPeriodBalances.Add(TermPeriodBalanceModel.Build(accountActivityTerms.NonTermActivity, accountHolder.Id, isAdmin));
            }
            UpdateTimeframeBalances(accountHolder, termPeriodBalances, terms, config, accountActivityTerms, isAdmin);

            // Build the account summary model from compiled data
            AccountSummaryModel model = new AccountSummaryModel()
            {
                AccountBalance = termPeriodBalances.Sum(tpb => tpb.Balance),
                AlertMessage = config.NotificationText,
                AmountDue = termModels.SelectMany(tm => tm.PaymentsDue).Where(pd => !pd.IsOverdue).Sum(pd => pd.DueAmount),
                HelpfulLinks = config.Links,
                IsAdminUser = currentUser.PersonId != accountHolder.Id && !currentUser.ProxySubjects.Any(),
                IsTermDisplay = false,
                NextPaymentDueDate = string.Empty,
                OverdueAmount = termModels.SelectMany(tm => tm.PaymentsDue).Where(pd => pd.IsOverdue).Sum(pd => pd.DueAmount),
                PersonId = accountHolder.Id,
                PersonName = accountHolder.PreferredName,
                PrivacyStatusCode = accountHolder.PrivacyStatusCode,
                ShowAccountActivityLink = currentUser.ProxySubjects != null && (!currentUser.ProxySubjects.Any() || currentUser.ProxySubjects.Any(ps => ps.Permissions.Contains("SFAA"))),
                ShowMakeAPaymentLink = currentUser.ProxySubjects != null && (!currentUser.ProxySubjects.Any() || currentUser.ProxySubjects.Any(ps => ps.Permissions.Contains("SFMAP"))),
                TimeframeBalances = termPeriodBalances
            };

            // Set the next payment due date
            if (termModels != null && termModels.Any())
            {
                var itemsWithDueDates = paymentsDue.Where(pd => pd.DueDate.HasValue).ToList();
                if (itemsWithDueDates.Any())
                {
                    model.NextPaymentDueDate = model.AmountDue > 0 ? itemsWithDueDates.Where(i => i.DueAmount > 0 && !i.IsCredit && !i.IsOverdue).Min(dd => dd.DueDate).Value.ToShortDateString() : string.Empty;
                }
            }
            
            return model;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountSummaryModel"/> class with payments due by period.
        /// </summary>
        /// <param name="accountHolder">The account holder</param>
        /// <param name="accountDuePeriod">Payments due by period</param>
        /// <param name="accountActivityPeriods">Account activity periods and balances</param>
        /// <param name="config">Student Finance configuration</param>
        /// <param name="terms">Collection of terms</param>
        /// <param name="currentUser">The user currently logged into self service</param>
        /// <returns>An AccountSummaryModel object</returns>
        public static AccountSummaryModel BuildForPaymentsDueByPeriod(AccountHolder accountHolder,
            AccountDuePeriod accountDuePeriod,
            AccountActivityPeriods accountActivityPeriods,
            FinanceConfiguration config,
            IEnumerable<Term> terms,
            ICurrentUser currentUser)
        {
            if (accountHolder == null)
            {
                throw new ArgumentNullException("accountHolder", "Account holder information cannot be null.");
            }
            if (accountDuePeriod == null || accountDuePeriod == null)
            {
                throw new ArgumentNullException("accountDuePeriod", "Payments due by period information cannot be null.");
            }
            if (accountActivityPeriods == null)
            {
                throw new ArgumentNullException("accountActivityPeriods", "Account activity timeframes and balances cannot be null.");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config", "Student finance configuration data cannot be null.");
            }
            if (terms == null)
            {
                throw new ArgumentNullException("terms", "Collection of terms cannot be null.");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser", "Current authenticated user information cannot be null.");
            }

            bool isAdmin = currentUser.PersonId != accountHolder.Id && !currentUser.ProxySubjects.Any();

            // Build period models
            List<PeriodSummaryModel> periodModels = new List<PeriodSummaryModel>();
            if (accountDuePeriod.Past != null)
            {
                periodModels.Add(PeriodSummaryModel.Build(accountDuePeriod.Past, config));
            }
            if (accountDuePeriod.Current != null)
            {
                periodModels.Add(PeriodSummaryModel.Build(accountDuePeriod.Current, config));
            }
            if (accountDuePeriod.Future != null)
            {
                periodModels.Add(PeriodSummaryModel.Build(accountDuePeriod.Future, config));
            }

            // Isolate payments due by period
            List<PaymentsDueModel> paymentsDue = periodModels.SelectMany(pm => pm.Terms).SelectMany(t => t.PaymentsDue).ToList();

            // Build timeframe account balances
            var termPeriodBalances = new List<TermPeriodBalanceModel>();
            foreach (var period in accountActivityPeriods.Periods)
            {
                termPeriodBalances.Add(TermPeriodBalanceModel.Build(period, accountHolder.Id, isAdmin));
            }
            if (accountActivityPeriods.NonTermActivity != null)
            {
                termPeriodBalances.Add(TermPeriodBalanceModel.Build(accountActivityPeriods.NonTermActivity, accountHolder.Id, isAdmin));
            }
            UpdateTimeframeBalances(accountHolder, termPeriodBalances, terms, config, accountActivityPeriods, isAdmin);

            // Build the account summary model from compiled data
            AccountSummaryModel model = new AccountSummaryModel()
            {
                AccountBalance = termPeriodBalances.Sum(tpb => tpb.Balance),
                AlertMessage = config.NotificationText,
                AmountDue = paymentsDue.Where(pd => !pd.IsOverdue).Sum(pd => pd.DueAmount),
                HelpfulLinks = config.Links,
                IsAdminUser = isAdmin,
                IsTermDisplay = false,
                NextPaymentDueDate = string.Empty,
                OverdueAmount = paymentsDue.Where(pd => pd.IsOverdue).Sum(pd => pd.DueAmount),
                PersonId = accountHolder.Id,
                PersonName = accountHolder.PreferredName,
                PrivacyStatusCode = accountHolder.PrivacyStatusCode,
                ShowAccountActivityLink = currentUser.ProxySubjects != null && (!currentUser.ProxySubjects.Any() || currentUser.ProxySubjects.Any(ps => ps.Permissions.Contains("SFAA"))),
                ShowMakeAPaymentLink = currentUser.ProxySubjects != null && (!currentUser.ProxySubjects.Any() || currentUser.ProxySubjects.Any(ps => ps.Permissions.Contains("SFMAP"))),
                TimeframeBalances = termPeriodBalances
            };

            // Set the next payment due date
            if (periodModels != null && periodModels.Any())
            {
                var itemsWithDueDates = paymentsDue.Where(pd => pd.DueDate.HasValue).ToList();
                if (itemsWithDueDates.Any())
                {
                    model.NextPaymentDueDate = model.AmountDue > 0 ? itemsWithDueDates.Where(i => i.DueAmount > 0 && !i.IsCredit && !i.IsOverdue).Min(dd => dd.DueDate).Value.ToShortDateString() : string.Empty;
                }
            }

            return model;
        }

        /// <summary>
        /// Check for deposits due in terms/periods not already represented in the list of term/period balances and update
        /// </summary>
        /// <param name="accountHolder">Accountholder info from the API</param>
        /// <param name="termPeriodBalances">List of term/period balances</param>
        /// <param name="terms">A list of all terms from the API</param>
        /// <param name="configuration">Student Finance Configuration from the API</param>
        /// <param name="allPeriods">Term/period balances from the API</param>
        private static void UpdateTimeframeBalances(AccountHolder accountHolder, List<TermPeriodBalanceModel> termPeriodBalances,
            IEnumerable<Term> terms, FinanceConfiguration configuration, AccountActivityPeriods allPeriods, bool isAdmin)
        {
            TermPeriodsUtility termUtility = new TermPeriodsUtility(terms, configuration.Periods);
            if (accountHolder.DepositsDue != null)
            {
                foreach (var dd in accountHolder.DepositsDue)
                {
                    if (configuration.ActivityDisplay == ActivityDisplay.DisplayByTerm)
                    {
                        if (!string.IsNullOrEmpty(dd.TermId))
                        {
                            var termPeriodBalance = termPeriodBalances.Where(tpb => tpb.AssociatedTerms.Contains(dd.TermId)).FirstOrDefault();
                            if (termPeriodBalance == null)
                            {
                                var term = terms.Where(t => t.Code == dd.TermId).FirstOrDefault();
                                termPeriodBalances.Add(TermPeriodBalanceModel.Build(
                                    new AccountPeriod()
                                    {
                                        AssociatedPeriods = new List<string>() { dd.TermId },
                                        Balance = 0m,
                                        Description = term != null ? term.Description : null,
                                        Id = dd.TermId,
                                        StartDate = null,
                                        EndDate = null
                                    }, accountHolder.Id, isAdmin));
                            }
                        }
                        else
                        {
                            var termPeriodBalance = termPeriodBalances.Where(tpb => tpb.Id == "NON-TERM").FirstOrDefault();
                            if (termPeriodBalance == null)
                            {
                                termPeriodBalances.Add(TermPeriodBalanceModel.Build(
                                    new AccountPeriod()
                                    {
                                        AssociatedPeriods = new List<string>(),
                                        Balance = 0m,
                                        Description = "Other",
                                        Id = "NON-TERM",
                                        StartDate = null,
                                        EndDate = null
                                    }, accountHolder.Id, isAdmin));
                            }
                        }
                    }
                    else
                    {
                        var periodType = termUtility.GetPeriodType(dd.TermId, dd.DueDate);
                        if (periodType != null)
                        {
                            var loc = termPeriodBalances.ToList().FindIndex(x => x.Description == periodType.ToString());
                            // A negative location means we need to add the period
                            if (loc >= 0)
                            {
                                // Existing period - make sure the term is one of the associated terms
                                // unless this is a non-term entry
                                if (!string.IsNullOrEmpty(dd.TermId))
                                {
                                    var exists = termPeriodBalances[loc].AssociatedTerms.Contains(dd.TermId);
                                    if (!exists)
                                    {
                                        // Term is not in list - add it
                                        termPeriodBalances[loc].AssociatedTerms.Add(dd.TermId);
                                    }
                                }
                            }
                            else
                            {
                                // New period - add it
                                termPeriodBalances.Add(TermPeriodBalanceModel.Build(
                                    new AccountPeriod()
                                    {
                                        Id = termUtility.GetPeriodCode(periodType),
                                        Description = periodType.ToString(),
                                        Balance = 0m,
                                        StartDate = configuration.Periods.Where(x => x.Type == periodType.Value).Single().Start,
                                        EndDate = configuration.Periods.Where(x => x.Type == periodType.Value).Single().End,
                                        AssociatedPeriods = string.IsNullOrEmpty(dd.TermId) ? new List<string>() : new List<string> { dd.TermId }
                                    }, dd.PersonId, isAdmin));
                            }
                        }

                    }
                }
            }
            termPeriodBalances = termPeriodBalances.OrderBy(tpb => configuration.ActivityDisplay == ActivityDisplay.DisplayByTerm ? termUtility.GetTermSortOrder(tpb.Id) :
                termUtility.GetPeriodSortOrder(tpb.Id).ToString()).Reverse().ToList();

            if (allPeriods.NonTermActivity != null)
            {
                var nonTermPeriod = termPeriodBalances.Where(tpb => tpb.Id == "NON-TERM").FirstOrDefault();
                var nonTermIndex = termPeriodBalances.IndexOf(nonTermPeriod);
                if (nonTermIndex < 0)
                {
                    termPeriodBalances.Add(TermPeriodBalanceModel.Build(allPeriods.NonTermActivity, accountHolder.Id, isAdmin));
                }
                else
                {
                    termPeriodBalances.RemoveAt(nonTermIndex);
                    termPeriodBalances.Add(TermPeriodBalanceModel.Build(allPeriods.NonTermActivity, accountHolder.Id, isAdmin));
                }
            }
        }
    }
}