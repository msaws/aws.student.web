﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using System;
using System.Text.RegularExpressions;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for groups of charges
    /// </summary>
    public class ChargeGroupModel : ITransactionModel
    {
        /// <summary>
        /// Unique ID for the group
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Group description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The type of charges included in this group
        /// </summary>
        public ChargeType ChargeType { get; set; }

        /// <summary>
        /// The order in which this group is to be displayed
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// The detail behind these charges
        /// </summary>
        public List<ChargeDetailModel> ChargeDetails { get; set; }

        /// <summary>
        /// Does this charge group get displayed on the Account Activity form?
        /// </summary>
        public bool IsVisible { get { return ChargeDetails != null && ChargeDetails.Count > 0; } }

        /// <summary>
        /// The total amount of these charges
        /// </summary>
        public decimal Amount { get { return ChargeDetails != null && ChargeDetails.Count > 0 ? ChargeDetails.Sum(c => c.Amount) : 0; } }

        /// <summary>
        /// Static build method for tuition by total charges
        /// </summary>
        /// <param name="tuition">Tuition by total data from the API</param>
        /// <param name="termUtility">Term/periods utility</param>
        /// <returns>ChargeGroupModel</returns>
        public static ChargeGroupModel Build(TuitionByTotalType tuition, TermPeriodsUtility termUtility)
        {
            if (tuition == null)
            {
                throw new ArgumentNullException("tuition");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new ChargeGroupModel()
            {
                Id = GetSanitizedChargeGroupName(tuition.Name),
                Description = tuition.Name,
                Order = tuition.DisplayOrder,
                ChargeType = ChargeType.TuitionByTotal,
                ChargeDetails = new List<ChargeDetailModel>()
            };

            var chargeDetails = new List<ChargeDetailModel>();
            if (tuition.TotalCharges != null)
            {
                foreach (var detail in tuition.TotalCharges)
                {
                    chargeDetails.Add(TuitionChargeModel.Build(detail, termUtility));
                }
            }

            // Sort charges in term sort order, then by name, with "Other Tuition Activity" at the end if applicable
            model.ChargeDetails = chargeDetails.OrderBy(cd => termUtility.GetTermSortOrder(cd.TermId)).ThenBy(cd => (cd as TuitionChargeModel).Name).ToList();
            var otherTuitionActivityIndices = model.ChargeDetails.Select((cd, i) => (cd as TuitionChargeModel).Name == "Other Tuition Activity" ? i : -1).Where(i => i >= 0).ToArray();
            if (otherTuitionActivityIndices.Count() > 0)
            {
                List<ChargeDetailModel> otherTuitionItems = new List<ChargeDetailModel>();
                foreach (var i in otherTuitionActivityIndices)
                {
                    otherTuitionItems.Add(model.ChargeDetails[i]);
                }
                model.ChargeDetails.RemoveAll(cd => otherTuitionItems.Contains(cd));
                model.ChargeDetails.AddRange(otherTuitionItems);
            }

            return model;
        }

        /// <summary>
        /// Static build method for tuition by section charges
        /// </summary>
        /// <param name="tuition">Tuition by section data from the API</param>
        /// <param name="termUtility">Term/periods utility</param>
        /// <returns>ChargeGroupModel</returns>
        public static ChargeGroupModel Build(TuitionBySectionType tuition, TermPeriodsUtility termUtility)
        {
            if (tuition == null)
            {
                throw new ArgumentNullException("tuition");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new ChargeGroupModel()
            {
                Id = GetSanitizedChargeGroupName(tuition.Name),
                Description = tuition.Name,
                Order = tuition.DisplayOrder,
                ChargeType = ChargeType.TuitionBySection,
                ChargeDetails = new List<ChargeDetailModel>()
            };

            var chargeDetails = new List<ChargeDetailModel>();
            if (tuition.SectionCharges != null)
            {
                foreach (var detail in tuition.SectionCharges)
                {
                    chargeDetails.Add(TuitionChargeModel.Build(detail, termUtility));
                }
            }

            // Sort charges in term sort order, then by name, with "Other Tuition Activity" at the end if applicable
            model.ChargeDetails = chargeDetails.OrderBy(cd => termUtility.GetTermSortOrder(cd.TermId)).ThenBy(cd => (cd as TuitionChargeModel).Name).ToList();
            var otherTuitionActivityIndices = model.ChargeDetails.Select((cd, i) => (cd as TuitionChargeModel).Name == "Other Tuition Activity" ? i : -1).Where(i => i >= 0).ToArray();
            if (otherTuitionActivityIndices.Count() > 0)
            {
                List<ChargeDetailModel> otherTuitionItems = new List<ChargeDetailModel>();
                foreach (var i in otherTuitionActivityIndices)
                {
                    otherTuitionItems.Add(model.ChargeDetails[i]);
                }
                model.ChargeDetails.RemoveAll(cd => otherTuitionItems.Contains(cd));
                model.ChargeDetails.AddRange(otherTuitionItems);
            }

            return model;
        }

        /// <summary>
        /// Static build method for fee charges
        /// </summary>
        /// <param name="fee">Fee data from the API</param>
        /// <param name="termUtility">Term/periods utility</param>
        /// <returns>ChargeGroupModel</returns>
        public static ChargeGroupModel Build(FeeType fee, TermPeriodsUtility termUtility)
        {
            if (fee == null)
            {
                throw new ArgumentNullException("fee");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new ChargeGroupModel()
            {
                Id = GetSanitizedChargeGroupName(fee.Name),
                Description = fee.Name,
                Order = fee.DisplayOrder,
                ChargeType = ChargeType.Fees,
                ChargeDetails = new List<ChargeDetailModel>()
            };

            if (fee.FeeCharges != null)
            {
                foreach (var detail in fee.FeeCharges)
                {
                    model.ChargeDetails.Add(FeeChargeModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Static build method for room and board charges
        /// </summary>
        /// <param name="rbGroup">Room and board data from the API</param>
        /// <param name="termUtility">Term/periods utility</param>
        /// <returns>ChargeGroupModel</returns>
        public static ChargeGroupModel Build(RoomAndBoardType rbGroup, TermPeriodsUtility termUtility)
        {
            if (rbGroup == null)
            {
                throw new ArgumentNullException("rbGroup");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new ChargeGroupModel()
            {
                Id = GetSanitizedChargeGroupName(rbGroup.Name),
                Description = rbGroup.Name,
                Order = rbGroup.DisplayOrder,
                ChargeType = ChargeType.RoomAndBoard,
                ChargeDetails = new List<ChargeDetailModel>()
            };

            if (rbGroup.RoomAndBoardCharges != null)
            {
                foreach (var detail in rbGroup.RoomAndBoardCharges)
                {
                    model.ChargeDetails.Add(RoomAndBoardChargeModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Static build method for "other" charges
        /// </summary>
        /// <param name="otherType">Other charge data from the API</param>
        /// <param name="termUtility">Term/periods utility</param>
        /// <returns>ChargeGroupModel</returns>
        public static ChargeGroupModel Build(OtherType otherType, TermPeriodsUtility termUtility, bool isMiscGroup = false)
        {
            if (otherType == null)
            {
                throw new ArgumentNullException("otherType");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new ChargeGroupModel()
            {
                Id = GetSanitizedChargeGroupName(otherType.Name),
                Description = otherType.Name,
                Order = otherType.DisplayOrder,
                ChargeType = isMiscGroup ? ChargeType.Miscellaneous : ChargeType.Other,
                ChargeDetails = new List<ChargeDetailModel>()
            };

            if (otherType.OtherCharges != null)
            {
                foreach (var detail in otherType.OtherCharges)
                {
                    model.ChargeDetails.Add(OtherChargeModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Returns a sanitized charge group name for a charge group name
        /// All non-alphanumeric characters will be replaced by underscores
        /// </summary>
        /// <param name="chargeGroupName">Charge group name</param>
        /// <returns>Sanitized charge group ID</returns>
        private static string GetSanitizedChargeGroupName(string chargeGroupName)
        {
            // Replace invalid characters with empty strings. 
            try
            {
                return Regex.Replace(chargeGroupName, @"[^a-zA-Z0-9]", "_",
                                     RegexOptions.None, TimeSpan.FromSeconds(1.5));
            }
            // If we timeout when replacing invalid characters,  
            // we should return Empty. 
            catch (RegexMatchTimeoutException)
            {
                return String.Empty;
            }
        }
    }
}