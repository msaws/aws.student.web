﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using System;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    public class AnticipatedAidModel
    {
        /// <summary>
        /// The term of the award
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The amount of the award that has been disbursed
        /// </summary>
        public decimal? DisbursementAmount { get; set; }

        /// <summary>
        /// The anticipated amount of the award
        /// </summary>
        public decimal? AnticipatedAmount { get; set; }

        /// <summary>
        /// Static method to build the AnticipatedAidModel
        /// </summary>
        /// <param name="detail">The input anticipated FA data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>AnticipatedAidModel</returns>
        public static AnticipatedAidModel Build(ActivityFinancialAidTerm detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new AnticipatedAidModel()
            {
                TermId = detail.AwardTerm,
                TermDescription = termUtility.GetTermDescription(detail.AwardTerm),
                DisbursementAmount = detail.DisbursedAmount,
                AnticipatedAmount = detail.AnticipatedAmount
            };

            return model;
        }
    }
}