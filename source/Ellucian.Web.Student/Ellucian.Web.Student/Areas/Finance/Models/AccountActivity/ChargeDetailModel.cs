﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The base class for the detailed information for all types of charges
    /// </summary>
    public abstract class ChargeDetailModel
    {
        /// <summary>
        /// The term in which the charges were incurred
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The amount of the charge
        /// </summary>
        public decimal Amount { get; set; }
    }
}
