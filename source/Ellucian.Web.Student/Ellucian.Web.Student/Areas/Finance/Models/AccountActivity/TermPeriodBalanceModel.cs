﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for showing balances for each term or period
    /// </summary>
    public class TermPeriodBalanceModel
    {
        /// <summary>
        /// The code identifying this term/period
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The description of the term/period
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The balance of the account for this term/period
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// The list of terms in a period
        /// </summary>
        public List<string> AssociatedTerms { get; set; }

        /// <summary>
        /// This is the text displayed in the dropdown box for a term/period
        /// </summary>
        public string DisplayText 
        {
            get
            {
                var displayText = new StringBuilder(Description);
                var label = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "BalanceLabel");
                if (!string.IsNullOrEmpty(label))
                {
                    displayText.Append(" - " + label);
                }
                displayText.Append(": " + FormatUtility.FormatAsCurrency(Balance));
                return displayText.ToString();
            }
        }

        /// <summary>
        /// This is the URL to process if this term/period is chosen from the dropdown box
        /// </summary>
        public string ProcessUrl { get; set; }

        /// <summary>
        /// This is the URL to launch Account Activity for this term/period
        /// </summary>
        public string AccountActivityUrl { get; set; }

        /// <summary>
        /// Static build method for TermPeriodBalanceModel
        /// </summary>
        /// <param name="period">A single account period's data from the API</param>
        /// <returns>TermPeriodBalanceModel</returns>
        public static TermPeriodBalanceModel Build(AccountPeriod period, string personId, bool isAdmin = false)
        {
            if (period == null)
            {
                throw new ArgumentNullException("period");
            }

            var model = new TermPeriodBalanceModel()
                {
                    Id = period.Id,
                    Description = period.Description,
                    Balance = period.Balance.GetValueOrDefault(),
                    AssociatedTerms = period.AssociatedPeriods
                };

            // Create the route dictionary used to build the URL
            RouteValueDictionary selectListAttributes = new RouteValueDictionary();
            // Append the Person ID when provided
            if (!string.IsNullOrEmpty(personId))
            {
                selectListAttributes.Add("id", personId);
            }

            // Add the values needed to build the rest of the URL
            selectListAttributes.Add("Area", "Finance");
            selectListAttributes.Add("timeframeId", period.Id);

            UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            model.ProcessUrl = url.Action("GetAccountActivityViewModel", "AccountActivity", selectListAttributes);
            
            string actionName = isAdmin ? "Admin" : "Index";
            model.AccountActivityUrl = url.Action(actionName, "AccountActivity", selectListAttributes);

            return model;
        }
    }
}