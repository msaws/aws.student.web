﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for "other" charges
    /// </summary>
    public class OtherChargeModel : ChargeDetailModel
    {
        /// <summary>
        /// The invoice number of the charge
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// The invoice date of the charge
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The description of the charge
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Static method to build the model for "other" charges
        /// </summary>
        /// <param name="detail">Other charges data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>OtherChargeModel</returns>
        public static OtherChargeModel Build(ActivityDateTermItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new OtherChargeModel()
            {
                Amount = detail.Amount.GetValueOrDefault(),
                InvoiceNumber = detail.Id,
                Description = detail.Description,
                Date = detail.Date.GetValueOrDefault(),
                TermId = detail.TermId,
                TermDescription = termUtility.GetTermDescription(detail.TermId)
            };

            return model;
        }
    }
}