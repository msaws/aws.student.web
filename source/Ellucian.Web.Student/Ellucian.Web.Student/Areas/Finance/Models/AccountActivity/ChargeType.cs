﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// Types of charges in the Account Activity view
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ChargeType
    {
        /// <summary>
        /// Tuition by total - one amount for a group of classes
        /// </summary>
        TuitionByTotal = 0,
        /// <summary>
        /// Tuition by section - each class has its own amount
        /// </summary>
        TuitionBySection = 1,
        /// <summary>
        /// Fees
        /// </summary>
        Fees = 2,
        /// <summary>
        /// Housing and meal charges
        /// </summary>
        RoomAndBoard = 3,
        /// <summary>
        /// Any other kind of charges
        /// </summary>
        Other = 4,
        /// <summary>
        /// The miscellaneous group of charges - always display last
        /// </summary>
        Miscellaneous = 9
    }
}