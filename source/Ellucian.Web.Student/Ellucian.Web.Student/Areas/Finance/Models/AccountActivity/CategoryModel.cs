﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for a category of transactions being displayed
    /// </summary>
    public class CategoryModel
    {
        /// <summary>
        /// Unique identifier of the category (needed for proper Knockout mapping)
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The type of transactions in this category
        /// </summary>
        public CategoryType CategoryType { get; set; }

        /// <summary>
        /// The order in which this category is displayed
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// The description of this category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The URL of the Image for this category, if any
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The alt text to display for this image
        /// </summary>
        public string ImageAltText { get; set; }

        /// <summary>
        /// The URL to the prefix (operator) image
        /// </summary>
        public string PrefixImageUrl { get; set; }

        /// <summary>
        /// The alt text to display for the prefix image
        /// </summary>
        public string PrefixImageAltText { get; set; }

        /// <summary>
        /// The name of the CSS class used to style the category
        /// </summary>
        public string CategoryStyle { get; set; }

        /// <summary>
        /// Does this amount get included in the overall balance?
        /// </summary>
        public bool IncludeInFormula { get; set; }

        /// <summary>
        /// List of detailed transactions in this category
        /// </summary>
        public List<ITransactionModel> Transactions { get; set; }

        /// <summary>
        /// The total amount displayed for this category
        /// </summary>
        public decimal? Amount { get { return IsVisible && IncludeInFormula ? Transactions.Sum(t => t.Amount) : (decimal?)null; } }

        /// <summary>
        /// Does this category get displayed on the Account Activity form?
        /// </summary>
        public bool IsVisible { get { return Transactions != null && Transactions.Count > 0; } }

        /// <summary>
        /// Do these transactions represent debits against the student's account?
        /// </summary>
        public bool IsDebit { get { return CategoryType == CategoryType.Charge || CategoryType == CategoryType.Refund; } }

        /// <summary>
        /// Do these transactions represent credits against the student's account?
        /// </summary>
        public bool IsCredit { get { return !IsDebit; } }

        /// <summary>
        /// Private static method to build the base CategoryModel
        /// </summary>
        /// <param name="categoryType">CategoryType of the transactions being processed</param>
        /// <returns>Generic CategoryModel</returns>
        private static CategoryModel Build(CategoryType categoryType, string displayTermPeriodCode)
        {
            var model = new CategoryModel();
            model.CategoryType = categoryType;
            model.Order = (int)categoryType;
            model.Id = displayTermPeriodCode + "-" + model.Order.ToString();
            model.IncludeInFormula = (int)categoryType < 10;

            var descriptionId = categoryType.ToString() + "Description";
            var description = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, descriptionId);
            model.Description = string.IsNullOrEmpty(description) ? categoryType.ToString() : description;

            if (model.IncludeInFormula)
            {
                var imageUrlId = categoryType.ToString() + "ImageUrl";
                var imageUrl = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, imageUrlId);
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    model.ImageUrl = imageUrl;
                    var imageAltTextId = categoryType.ToString() + "ImageAltText";
                    model.ImageAltText = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, imageAltTextId);
                }
                var prefixImageUrlId = categoryType.ToString() + "PrefixImageUrl";
                var prefixImageUrl = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, prefixImageUrlId);
                if (!string.IsNullOrEmpty(prefixImageUrl))
                {
                    model.PrefixImageUrl = prefixImageUrl;
                    var prefixAltTextId = categoryType.ToString() + "PrefixAltText";
                    model.PrefixImageAltText = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, prefixAltTextId);
                }
            }

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for charges
        /// </summary>
        /// <param name="chargesCategory">Charges data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>CategoryModel for ChargeGroupModel</returns>
        public static CategoryModel Build(ChargesCategory chargesCategory, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (chargesCategory == null)
            {
                throw new ArgumentNullException("chargesCategory");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.Charge, displayTermPeriodCode);

            var transactions = new List<ChargeGroupModel>();

            if (chargesCategory.TuitionByTotalGroups != null)
            {
                foreach (var tuitionTotal in chargesCategory.TuitionByTotalGroups)
                {
                    if (tuitionTotal.TotalCharges != null && tuitionTotal.TotalCharges.Any())
                    {
                        transactions.Add(ChargeGroupModel.Build(tuitionTotal, termUtility));
                    }
                }
            }

            if (chargesCategory.TuitionBySectionGroups != null)
            {
                foreach (var tuitionSection in chargesCategory.TuitionBySectionGroups)
                {
                    if (tuitionSection.SectionCharges != null && tuitionSection.SectionCharges.Any())
                    {
                        transactions.Add(ChargeGroupModel.Build(tuitionSection, termUtility));
                    }
                }
            }

            if (chargesCategory.FeeGroups != null)
            {
                foreach (var feeGroup in chargesCategory.FeeGroups)
                {
                    if (feeGroup.FeeCharges != null && feeGroup.FeeCharges.Any())
                    {
                        transactions.Add(ChargeGroupModel.Build(feeGroup, termUtility));
                    }
                }
            }

            if (chargesCategory.RoomAndBoardGroups != null)
            {
                foreach (var rbGroup in chargesCategory.RoomAndBoardGroups)
                {
                    if (rbGroup.RoomAndBoardCharges != null && rbGroup.RoomAndBoardCharges.Any())
                    {
                        transactions.Add(ChargeGroupModel.Build(rbGroup, termUtility));
                    }
                }
            }

            if (chargesCategory.OtherGroups != null)
            {
                foreach (var otherGroup in chargesCategory.OtherGroups)
                {
                    if (otherGroup.OtherCharges != null && otherGroup.OtherCharges.Any())
                    {
                        transactions.Add(ChargeGroupModel.Build(otherGroup, termUtility));
                    }
                }
            }

            if (chargesCategory.Miscellaneous != null && chargesCategory.Miscellaneous.OtherCharges != null && chargesCategory.Miscellaneous.OtherCharges.Any())
            {
                transactions.Add(ChargeGroupModel.Build(chargesCategory.Miscellaneous, termUtility, true));
            }

            model.Transactions = transactions.OrderBy(t => t.Order).ToList<ITransactionModel>();

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for receipt payments
        /// </summary>
        /// <param name="studentPaymentCategory">Receipt payments data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <param name="paymentMethods">A list of all payment methods</param>
        /// <returns>CategoryModel for ReceiptPaymentModel</returns>
        public static CategoryModel Build(StudentPaymentCategory studentPaymentCategory, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (studentPaymentCategory == null)
            {
                throw new ArgumentNullException("studentPaymentCategory");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.ReceiptPayment, displayTermPeriodCode);

            model.Transactions = new List<ITransactionModel>();

            if (studentPaymentCategory.StudentPayments != null)
            {
                foreach (var detail in studentPaymentCategory.StudentPayments)
                {
                    model.Transactions.Add(ReceiptPaymentModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for financial aid payments
        /// </summary>
        /// <param name="financialAidCategory">Financial Aid data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>CategoryModel for FinancialAidPaymentModel</returns>
        public static CategoryModel Build(FinancialAidCategory financialAidCategory, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (financialAidCategory == null)
            {
                throw new ArgumentNullException("financialAidCategory");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.FinancialAidPayment, displayTermPeriodCode);

            model.Transactions = new List<ITransactionModel>();

            if (financialAidCategory.AnticipatedAid != null)
            {
                foreach (var detail in financialAidCategory.AnticipatedAid)
                {
                    model.Transactions.Add(FinancialAidPaymentModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for sponsorships
        /// </summary>
        /// <param name="sponsorshipCategory">Sponsorship data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>CategoryModel for SponsorshipModel</returns>
        public static CategoryModel Build(SponsorshipCategory sponsorshipCategory, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (sponsorshipCategory == null)
            {
                throw new ArgumentNullException("sponsorshipCategory");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.Sponsorship, displayTermPeriodCode);

            model.Transactions = new List<ITransactionModel>();

            if (sponsorshipCategory.SponsorItems != null)
            {
                foreach (var detail in sponsorshipCategory.SponsorItems)
                {
                    model.Transactions.Add(SponsorshipModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for deposits
        /// </summary>
        /// <param name="depositCategory">Deposit data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>CategoryModel for DepositModel</returns>
        public static CategoryModel Build(DepositCategory depositCategory, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (depositCategory == null)
            {
                throw new ArgumentNullException("depositCategory");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.Deposit, displayTermPeriodCode);

            model.Transactions = new List<ITransactionModel>();

            if (depositCategory.Deposits != null)
            {
                foreach (var detail in depositCategory.Deposits)
                {
                    model.Transactions.Add(DepositModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for refunds
        /// </summary>
        /// <param name="refundCategory">Refund data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>CategoryModel for RefundModel</returns>
        public static CategoryModel Build(RefundCategory refundCategory, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (refundCategory == null)
            {
                throw new ArgumentNullException("refundCategory");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.Refund, displayTermPeriodCode);

            model.Transactions = new List<ITransactionModel>();

            if (refundCategory.Refunds != null)
            {
                foreach (var detail in refundCategory.Refunds)
                {
                    model.Transactions.Add(RefundModel.Build(detail, termUtility));
                }
            }

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for deposits due
        /// </summary>
        /// <param name="depositsDue">Deposits due data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>CategoryModel for DepositDueModel</returns>
        public static CategoryModel Build(IEnumerable<DepositDue> depositsDue, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (depositsDue == null)
            {
                throw new ArgumentNullException("depositsDue");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.DepositDue, displayTermPeriodCode);

            model.Transactions = new List<ITransactionModel>();

            foreach (var detail in depositsDue)
            {
                model.Transactions.Add(DepositDueModel.Build(detail, termUtility));
            }

            return model;
        }

        /// <summary>
        /// Static method to build the CategoryModel for payment plans
        /// </summary>
        /// <param name="paymentPlanCategory">Payment plan data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>CategoryModel for PaymentPlanModel</returns>
        public static CategoryModel Build(PaymentPlanCategory paymentPlanCategory, TermPeriodsUtility termUtility, string displayTermPeriodCode)
        {
            if (paymentPlanCategory == null)
            {
                throw new ArgumentNullException("paymentPlanCategory");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }
            if (string.IsNullOrEmpty(displayTermPeriodCode))
            {
                throw new ArgumentNullException("displayTermPeriodCode");
            }

            var model = Build(CategoryType.PaymentPlan, displayTermPeriodCode);

            model.Transactions = new List<ITransactionModel>();

            if (paymentPlanCategory.PaymentPlans != null)
            {
                foreach (var detail in paymentPlanCategory.PaymentPlans)
                {
                    model.Transactions.Add(PaymentPlanModel.Build(detail, termUtility));
                }
            }

            return model;
        }
    }
}