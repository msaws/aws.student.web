﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for a payment plan.
    /// </summary>
    public class PaymentPlanModel : ITransactionModel
    {
        /// <summary>
        /// Plan ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Plan Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Description of the plan's receivable type.
        /// </summary>
        public string TypeDescription { get; set; }

        /// <summary>
        /// ID of the term this plan is used for.
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// Description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The original amount of the plan.
        /// </summary>
        public decimal OriginalAmount { get { return Amount; } }

        /// <summary>
        /// The current balance of the plan - how much is still owed?
        /// </summary>
        public decimal CurrentBalance { get { return Schedules.Sum(s => s.Amount); } }

        /// <summary>
        /// Amount that is due now.
        /// </summary>
        public decimal AmountDue { get; set; }

        /// <summary>
        /// Pointer to the payment plan's associated approval record
        /// </summary>
        public string PaymentPlanApproval { get; set; }

        /// <summary>
        /// List of scheduled payments for the plan
        /// </summary>
        public List<PlanScheduleModel> Schedules { get; set; }

        /// <summary>
        /// Static method to build the PaymentPlanModel
        /// </summary>
        /// <param name="detail">The payment plan data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>PaymentPlanModel</returns>
        public static PaymentPlanModel Build(ActivityPaymentPlanDetailsItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new PaymentPlanModel()
            {
                Id = detail.Id,
                Amount = detail.OriginalAmount.GetValueOrDefault(),
                TypeDescription = detail.Type,
                TermId = detail.TermId,
                TermDescription = termUtility.GetTermDescription(detail.TermId),
                AmountDue = detail.Amount.GetValueOrDefault(),
                PaymentPlanApproval = detail.PaymentPlanApproval,
                Schedules = new List<PlanScheduleModel>()
            };

            if (detail.PaymentPlanSchedules != null)
            {
                foreach (var payment in detail.PaymentPlanSchedules)
                {
                    model.Schedules.Add(PlanScheduleModel.Build(detail.Id, payment));
                }
            }

            return model;
        }
    }
}