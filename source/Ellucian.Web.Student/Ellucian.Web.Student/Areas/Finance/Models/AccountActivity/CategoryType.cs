﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// These are the types of transactions displayed on Account Activity
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CategoryType
    {
        /// <summary>
        /// Charges
        /// </summary>
        Charge = 0,
        /// <summary>
        /// Receipt payments
        /// </summary>
        ReceiptPayment = 1,
        /// <summary>
        /// Financial Aid Payments
        /// </summary>
        FinancialAidPayment = 2,
        /// <summary>
        /// Sponsorships
        /// </summary>
        Sponsorship = 3,
        /// <summary>
        /// Deposit allocations
        /// </summary>
        Deposit = 4,
        /// <summary>
        /// Refunds
        /// </summary>
        Refund = 9,
        /// <summary>
        /// Deposits due
        /// </summary>
        DepositDue = 11,
        /// <summary>
        /// Payment plans
        /// </summary>
        PaymentPlan = 12
    }
}
