﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for receipt payments
    /// </summary>
    public class ReceiptPaymentModel : ITransactionModel
    {
        /// <summary>
        /// The ID of the receipt
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The receipt (reference) number
        /// </summary>
        public string ReceiptNumber { get; set; }

        /// <summary>
        /// The date of the receipt
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The term of the receipt
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The amount of the receipt
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The payment method used for payment
        /// </summary>
        public string PaymentMethodCode { get; set; }

        /// <summary>
        /// The reference number returned by the payment provider
        /// </summary>
        public string PaymentReferenceNumber { get; set; }

        /// <summary>
        /// Static build method for receipt payments
        /// </summary>
        /// <param name="detail">The receipt payment data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <param name="paymentMethods">List of all payment methods</param>
        /// <returns></returns>
        public static ReceiptPaymentModel Build(ActivityPaymentPaidItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new ReceiptPaymentModel()
            {
                Id = detail.Id,
                Amount = detail.Amount.GetValueOrDefault(),
                ReceiptNumber = detail.ReceiptNumber,
                Date = detail.Date.GetValueOrDefault(),
                TermId = detail.TermId,
                TermDescription = termUtility.GetTermDescription(detail.TermId),
                PaymentMethodCode = GetPaymentMethodValue(detail.Method),
                PaymentReferenceNumber = detail.ReferenceNumber
            };

            return model;
        }

        /// <summary>
        /// Get the display payment method for the student payment
        /// </summary>
        /// <param name="method">Payment method data</param>
        /// <param name="paymentMethods">List of all payment methods</param>
        /// <returns>Displayed payment method</returns>
        private static string GetPaymentMethodValue(string method)
        {
            if (method == "Returned Check")
            {
                return GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "CheckReturnedText");
            }
            if (method.Contains("Cash"))
            {
                return method.Replace("Cash", GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "CashPayMethodText"));
            }
            return method;
        }
    }
}