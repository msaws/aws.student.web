﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using System;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for showing the Fee charges
    /// </summary>
    public class FeeChargeModel : ChargeDetailModel
    {
        /// <summary>
        /// The description of the charge
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Static method to build the FeeChargeModel
        /// </summary>
        /// <param name="detail">The fee charge data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>FeeChargeModel</returns>
        public static FeeChargeModel Build(ActivityDateTermItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new FeeChargeModel()
            {
                Description = detail.Description,
                TermId = detail.TermId,
                TermDescription = termUtility.GetTermDescription(detail.TermId),
                Amount = detail.Amount.GetValueOrDefault()
            };
            
            return model;
        }
    }
}