﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using System;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// This is the main model used for the Account Activity view
    /// </summary>
    public class AccountActivityModel : StudentFinanceAdminModel
    {
        /// <summary>
        /// The code of the term/period currently being displayed
        /// </summary>
        public string DisplayTermPeriodCode { get; set; }

        /// <summary>
        /// The description of the term/period currently being displayed
        /// </summary>
        public string DisplayTermPeriodDescription { get; set; }

        /// <summary>
        /// The text label that appears next to the term/period dropdown box
        /// </summary>
        public string DropdownLabel { get; set; }

        /// <summary>
        /// The list of terms/periods and their balances
        /// </summary>
        public List<TermPeriodBalanceModel> TermPeriodBalances { get; set; }

        /// <summary>
        /// The list of categories to be displayed that are included in the formula
        /// </summary>
        public List<CategoryModel> FormulaCategories { get; set; }

        /// <summary>
        /// The list of categories to be displayed that are not included in the formula
        /// </summary>
        public List<CategoryModel> NonFormulaCategories { get; set; }

        /// <summary>
        /// This is the message displayed beneath the balance and above deposits due/payment plans
        /// </summary>
        public string DisclaimerText { get; set; }

        /// <summary>
        /// Calculate the student's overall balance
        /// </summary>
        public decimal Balance
        {
            get
            {
                var debits = FormulaCategories.Where(c => c.IsDebit).Sum(c => c.Amount.GetValueOrDefault());
                var credits = FormulaCategories.Where(c => c.IsCredit).Sum(c => c.Amount.GetValueOrDefault());
                return debits - credits;
            }
        }

        /// <summary>
        /// The URL of the Image for the balance, if any
        /// </summary>
        public string BalanceImageUrl { get; set; }

        /// <summary>
        /// The alt text to display for the balance image
        /// </summary>
        public string BalanceImageAltText { get; set; }

        /// <summary>
        /// Should the Deposits Due link be displayed at the top of the form?
        /// </summary>
        public bool ShowDepositsDueLink { get { return NonFormulaCategories.Where(c => c.CategoryType == CategoryType.DepositDue && c.IsVisible).Any(); } }

        /// <summary>
        /// Should the Payment Plans link be displayed at the top of the form?
        /// </summary>
        public bool ShowPaymentPlansLink { get { return NonFormulaCategories.Any(c => c.CategoryType == CategoryType.PaymentPlan && c.IsVisible); } }

        /// <summary>
        /// Does the account holder have any activity to display?
        /// </summary>
        public bool HasData 
        { 
            get 
            {
                return TermPeriodBalances != null && TermPeriodBalances.Any(); 
            } 
        }

        /// <summary>
        /// Message displayed when the account holder has no activity to display
        /// </summary>
        public string NoDataMessage 
        { 
            get 
            { 
                return GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "AccountActivityNotFound"); 
            } 
        }

        /// <summary>
        /// Cash Receipt display data
        /// </summary>
        public Models.Payments.PaymentAcknowledgementModel ReceiptData { get; set; }

        public AccountActivityModel()
        {
            TermPeriodBalances = null;
            FormulaCategories = new List<CategoryModel>();
            NonFormulaCategories = new List<CategoryModel>();
            ReceiptData = new Payments.PaymentAcknowledgementModel();
        }

        /// <summary>
        /// Static method for building the AccountActivityModel
        /// </summary>
        /// <param name="allPeriods">Term/period balances from the API</param>
        /// <param name="selectedPeriod">Detailed account info from the API for the term/period selected by the user</param>
        /// <param name="accountHolder">Accountholder info from the API</param>
        /// <param name="configuration">Student Finance Configuration from the API</param>
        /// <param name="displayTermPeriodCode">The term/period selected by the user</param>
        /// <param name="terms">A list of all terms from the API</param>
        /// <param name="currentUser">The user currently logged into self service</param>
        /// <returns>AccountActivityModel</returns>
        public static AccountActivityModel Build(AccountActivityPeriods allPeriods, DetailedAccountPeriod selectedPeriod, AccountHolder accountHolder,
            FinanceConfiguration configuration, string displayTermPeriodCode, List<Term> terms, ICurrentUser currentUser)
        {
            if (allPeriods == null)
            {
                throw new ArgumentNullException("allPeriods");
            }
            if (selectedPeriod == null)
            {
                throw new ArgumentNullException("selectedPeriod");
            }
            if (accountHolder == null)
            {
                throw new ArgumentNullException("accountHolder");
            }
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }
            if (terms == null || terms.Count() == 0)
            {
                throw new ArgumentNullException("terms");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser");
            }

            var termUtility = new TermPeriodsUtility(terms, configuration.Periods);

            var model = new AccountActivityModel();
            model.PrivacyStatusCode = accountHolder.PrivacyStatusCode;
            model.AlertMessage = configuration.NotificationText;
            model.IsTermDisplay = configuration.ActivityDisplay == ActivityDisplay.DisplayByTerm;
            model.DisplayTermPeriodCode = displayTermPeriodCode;

            var resourceId = model.IsTermDisplay ? "TermLabel" : "PeriodLabel";
            model.DropdownLabel = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, resourceId);
            if (model.IsTermDisplay)
            {
                if (displayTermPeriodCode == "NON-TERM")
                {
                    model.DisplayTermPeriodDescription = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "NonTermDescription");
                }
                else
                {
                    var term = terms.Where(t => t.Code == displayTermPeriodCode).FirstOrDefault();
                    model.DisplayTermPeriodDescription = term != null ? term.Description : string.Empty;
                }
            }
            else
            {
                var period = configuration.Periods.Where(p => termUtility.GetPeriodCode(p.Type) == displayTermPeriodCode).FirstOrDefault();
                model.DisplayTermPeriodDescription = period != null ? 
                    termUtility.GetPeriodDescription(displayTermPeriodCode) + " " + model.DropdownLabel : string.Empty;
            }


            model.PersonId = accountHolder.Id;
            model.PersonName = accountHolder.PreferredName;
            model.IsAdminUser = currentUser.PersonId != accountHolder.Id && !currentUser.ProxySubjects.Any();

            var termPeriodBalances = new List<TermPeriodBalanceModel>();
            foreach (var period in allPeriods.Periods)
            {
                termPeriodBalances.Add(TermPeriodBalanceModel.Build(period, model.PersonId));
            }
            UpdateTermPeriodBalances(model, accountHolder, termPeriodBalances, terms, termUtility, configuration, allPeriods);

            var formulaCategories = new List<CategoryModel>();
            formulaCategories.Add(CategoryModel.Build(selectedPeriod.Charges, termUtility, displayTermPeriodCode));
            formulaCategories.Add(CategoryModel.Build(selectedPeriod.StudentPayments, termUtility, displayTermPeriodCode));
            formulaCategories.Add(CategoryModel.Build(selectedPeriod.FinancialAid, termUtility, displayTermPeriodCode));
            formulaCategories.Add(CategoryModel.Build(selectedPeriod.Sponsorships, termUtility, displayTermPeriodCode));
            formulaCategories.Add(CategoryModel.Build(selectedPeriod.Deposits, termUtility, displayTermPeriodCode));
            formulaCategories.Add(CategoryModel.Build(selectedPeriod.Refunds, termUtility, displayTermPeriodCode));
            model.FormulaCategories = formulaCategories.OrderBy(fc => fc.Order).ToList();

            var nonFormulaCategories = new List<CategoryModel>();

            var timeframeDepositsDue = GetDepositsDueForTimeframe(configuration.ActivityDisplay, displayTermPeriodCode, accountHolder.DepositsDue, termUtility);

            nonFormulaCategories.Add(CategoryModel.Build(timeframeDepositsDue, termUtility, displayTermPeriodCode));
            nonFormulaCategories.Add(CategoryModel.Build(selectedPeriod.PaymentPlans, termUtility, displayTermPeriodCode));
            model.NonFormulaCategories = nonFormulaCategories.OrderBy(nfc => nfc.Order).ToList();

            model.DisclaimerText = model.NonFormulaCategories.Any(nfc => nfc.IsVisible) ? GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "DisclaimerText") : null;

            model.BalanceImageUrl = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "BalanceImageUrl");
            model.BalanceImageAltText = GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "BalanceImageAltText");


            return model;
        }


        /// <summary>
        /// Static method for building the AccountActivityModel for the special case of no account activity.  Still displays alerts,
        /// notifications, and administrative user status
        /// </summary>
        /// <param name="accountHolder">Accountholder info from the API</param>
        /// <param name="configuration">Student Finance Configuration from the API</param>
        /// <param name="restrictions">List of the restrictions active on the account</param>
        /// <param name="currentUserId">The person ID of the user currently logged into self service</param>
        /// <returns>AccountActivityModel</returns>
        public static AccountActivityModel Build(AccountHolder accountHolder, FinanceConfiguration configuration,
            ICurrentUser currentUser)
        {
            if (accountHolder == null)
            {
                throw new ArgumentNullException("accountHolder");
            }
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser");
            }
            
            var model = new AccountActivityModel();

            model.AlertMessage = configuration.NotificationText;
            model.PrivacyStatusCode = accountHolder.PrivacyStatusCode;
            model.PersonId = accountHolder.Id;
            model.PersonName = accountHolder.PreferredName;
            model.IsAdminUser = currentUser.PersonId != accountHolder.Id && !currentUser.ProxySubjects.Any();
            return model;
        }

        /// <summary>
        /// Get the deposits due for a term or period
        /// </summary>
        /// <param name="activityDisplay">Account Activity term/period display setting</param>
        /// <param name="displayTermPeriodCode">The term/period selected by the user</param>
        /// <param name="depositsDue">Collection of deposits due</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>Deposits due for a term or period, in sort order</returns>
        private static IEnumerable<DepositDue> GetDepositsDueForTimeframe(ActivityDisplay activityDisplay, string displayTermPeriodCode, IEnumerable<DepositDue> depositsDue,
            TermPeriodsUtility termUtility)
        {
            var timeframeDepositsDue = new List<DepositDue>();
            if (depositsDue != null)
            {
                if (activityDisplay == ActivityDisplay.DisplayByTerm)
                {
                    if (displayTermPeriodCode == "NON-TERM")
                    {
                        timeframeDepositsDue.AddRange(depositsDue.Where(dd => string.IsNullOrEmpty(dd.TermId)));
                    }
                    else
                    {
                        timeframeDepositsDue.AddRange(depositsDue.Where(dd => dd.TermId == displayTermPeriodCode));
                    }
                }
                else
                {
                    PeriodType? pt = null;
                    switch (displayTermPeriodCode)
                    {
                        case "PAST":
                            pt = PeriodType.Past;
                            break;
                        case "CUR":
                            pt = PeriodType.Current;
                            break;
                        case "FTR":
                            pt = PeriodType.Future;
                            break;
                        default:
                            break;
                    }
                    timeframeDepositsDue.AddRange(depositsDue.Where(dd => termUtility.GetPeriodType(dd.TermId, dd.DueDate) == pt));
                }
            }

            // Add term descriptions for all deposits due
            foreach (var dd in timeframeDepositsDue)
            {
                dd.TermDescription = !string.IsNullOrEmpty(dd.TermId) ? termUtility.GetTermDescription(dd.TermId) : dd.TermDescription;
            }
            return timeframeDepositsDue.OrderBy(dd => dd.SortOrder).ToList();
        }

        /// <summary>
        /// Check for deposits due in terms/periods not already represented in the list of term/period balances and update
        /// </summary>
        /// <param name="model">The AccountActivityModel instance</param>
        /// <param name="accountHolder">Accountholder info from the API</param>
        /// <param name="termPeriodBalances">List of term/period balances</param>
        /// <param name="terms">A list of all terms from the API</param>
        /// <param name="termUtility">Term/Period Utility</param>
        /// <param name="configuration">Student Finance Configuration from the API</param>
        /// <param name="allPeriods">Term/period balances from the API</param>
        private static void UpdateTermPeriodBalances(AccountActivityModel model, AccountHolder accountHolder, List<TermPeriodBalanceModel> termPeriodBalances,
            List<Term> terms, TermPeriodsUtility termUtility, FinanceConfiguration configuration, AccountActivityPeriods allPeriods)
        {
            if (accountHolder.DepositsDue != null)
            {
                foreach (var dd in accountHolder.DepositsDue)
                {
                    if (model.IsTermDisplay)
                    {
                        if (!string.IsNullOrEmpty(dd.TermId))
                        {
                            var termPeriodBalance = termPeriodBalances.Where(tpb => tpb.AssociatedTerms.Contains(dd.TermId)).FirstOrDefault();
                            if (termPeriodBalance == null)
                            {
                                var term = terms.Where(t => t.Code == dd.TermId).FirstOrDefault();
                                termPeriodBalances.Add(TermPeriodBalanceModel.Build(
                                    new AccountPeriod()
                                    {
                                        AssociatedPeriods = new List<string>() { dd.TermId },
                                        Balance = 0m,
                                        Description = term != null ? term.Description : null,
                                        Id = dd.TermId,
                                        StartDate = null,
                                        EndDate = null
                                    }, accountHolder.Id));
                            }
                        }
                        else
                        {
                            var termPeriodBalance = termPeriodBalances.Where(tpb => tpb.Id == "NON-TERM").FirstOrDefault();
                            if (termPeriodBalance == null)
                            {
                                termPeriodBalances.Add(TermPeriodBalanceModel.Build(
                                    new AccountPeriod()
                                    {
                                        AssociatedPeriods = new List<string>(),
                                        Balance = 0m,
                                        Description = "Other",
                                        Id = "NON-TERM",
                                        StartDate = null,
                                        EndDate = null
                                    }, accountHolder.Id));
                            }
                        }
                    }
                    else
                    {
                        var periodType = termUtility.GetPeriodType(dd.TermId, dd.DueDate);
                        if (periodType != null)
                        {
                            var loc = termPeriodBalances.ToList().FindIndex(x => x.Description == periodType.ToString());
                            // A negative location means we need to add the period
                            if (loc >= 0)
                            {
                                // Existing period - make sure the term is one of the associated terms
                                // unless this is a non-term entry
                                if (!string.IsNullOrEmpty(dd.TermId))
                                {
                                    var exists = termPeriodBalances[loc].AssociatedTerms.Contains(dd.TermId);
                                    if (!exists)
                                    {
                                        // Term is not in list - add it
                                        termPeriodBalances[loc].AssociatedTerms.Add(dd.TermId);
                                    }
                                }
                            }
                            else
                            {
                                // New period - add it
                                termPeriodBalances.Add(TermPeriodBalanceModel.Build(
                                    new AccountPeriod()
                                {
                                    Id = termUtility.GetPeriodCode(periodType),
                                    Description = periodType.ToString(),
                                    Balance = 0m,
                                    StartDate = configuration.Periods.Where(x => x.Type == periodType.Value).Single().Start,
                                    EndDate = configuration.Periods.Where(x => x.Type == periodType.Value).Single().End,
                                    AssociatedPeriods = string.IsNullOrEmpty(dd.TermId) ? new List<string>() : new List<string> { dd.TermId }
                                }, dd.PersonId));
                            }
                        }

                    }
                }
            }
            model.TermPeriodBalances = termPeriodBalances.OrderBy(tpb => model.IsTermDisplay ? termUtility.GetTermSortOrder(tpb.Id) :
                termUtility.GetPeriodSortOrder(tpb.Id).ToString()).Reverse().ToList();

            if (allPeriods.NonTermActivity != null)
            {
                var nonTermPeriod = model.TermPeriodBalances.Where(tpb => tpb.Id == "NON-TERM").FirstOrDefault();
                var nonTermIndex = model.TermPeriodBalances.IndexOf(nonTermPeriod);
                if (nonTermIndex < 0)
                {
                    model.TermPeriodBalances.Add(TermPeriodBalanceModel.Build(allPeriods.NonTermActivity, model.PersonId));
                }
                else
                {
                    model.TermPeriodBalances.RemoveAt(nonTermIndex);
                    model.TermPeriodBalances.Add(TermPeriodBalanceModel.Build(allPeriods.NonTermActivity, model.PersonId));
                }
            }
        }
    }
}