﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    public interface ITransactionModel
    {
        /// <summary>
        /// Transaction ID
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Transaction amount
        /// </summary>
        decimal Amount { get; }
    }
}
