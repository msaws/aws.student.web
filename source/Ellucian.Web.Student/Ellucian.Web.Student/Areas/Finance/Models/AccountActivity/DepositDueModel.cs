﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for deposits due
    /// </summary>
    public class DepositDueModel : ITransactionModel
    {
        /// <summary>
        /// The ID of the deposit due
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The deposit type of the deposit due.
        /// </summary>
        public string TypeCode { get; set; }

        /// <summary>
        /// The description of the deposit type
        /// </summary>
        public string TypeDescription { get; set; }

        /// <summary>
        /// The date by which the deposit must be paid
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// The term for which the deposit is due
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The total amount due on the deposit
        /// </summary>
        public decimal OriginalAmount { get; set; }

        /// <summary>
        /// The total amount paid toward the deposit
        /// </summary>
        public decimal? PaidAmount { get; set; }

        /// <summary>
        /// The net amount still due on the deposit
        /// </summary>
        public decimal Amount { get { return OriginalAmount - PaidAmount.GetValueOrDefault(); } }

        /// <summary>
        /// Static method to build the DepositDueModel
        /// </summary>
        /// <param name="detail">The deposit due data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>DepositDueModel</returns>
        public static DepositDueModel Build(DepositDue detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new DepositDueModel()
            {
                Id = detail.Id,
                TypeCode = detail.DepositType,
                TypeDescription = detail.DepositTypeDescription,
                TermId = detail.TermId,
                TermDescription = detail.TermDescription,
                DueDate = detail.DueDate,
                OriginalAmount = detail.Amount,
                PaidAmount = detail.AmountPaid
            };

            return model;
        }
    }
}