﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for deposits
    /// </summary>
    public class DepositModel : ITransactionModel
    {
        /// <summary>
        /// The ID of the deposit
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The description of the deposit type
        /// </summary>
        public string TypeDescription { get; set; }

        /// <summary>
        /// The date on which the deposit was paid
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The term for which the deposit is paid
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The original amount paid on the deposit
        /// </summary>
        public decimal DepositAmount { get; set; }

        /// <summary>
        /// The amount of the deposit applied to charges
        /// </summary>
        public decimal? AppliedAmount { get; set; }

        /// <summary>
        /// The amount of the deposit that is unavailable for allocation
        /// </summary>
        public decimal? OtherAmount { get; set; }

        /// <summary>
        /// The amount of the deposit that has been refunded
        /// </summary>
        public decimal? RefundAmount { get; set; }

        /// <summary>
        /// The net amount still available on the deposit
        /// </summary>
        public decimal NetAmount { get; set; }

        /// <summary>
        /// The amount that gets reported for this deposit. It rolls up into the other amounts.
        /// </summary>
        public decimal Amount { get { return AppliedAmount.GetValueOrDefault() + NetAmount; } }

        /// <summary>
        /// The deposit's associated cash receipt ID
        /// </summary>
        public string ReceiptId { get; set; }

        /// <summary>
        /// Static method to build the DepositModel
        /// </summary>
        /// <param name="detail">Deposits data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>DepositModel</returns>
        public static DepositModel Build(ActivityRemainingAmountItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new DepositModel()
            {
                Id = detail.Id,
                TypeDescription = detail.Description,
                Date = detail.Date.GetValueOrDefault(),
                TermId = detail.TermId,
                TermDescription = termUtility.GetTermDescription(detail.TermId),
                DepositAmount = detail.Amount.GetValueOrDefault(),
                AppliedAmount = detail.PaidAmount.GetValueOrDefault(),
                OtherAmount = detail.OtherAmount,
                RefundAmount = detail.RefundAmount,
                NetAmount = detail.RemainingAmount.GetValueOrDefault(),
                ReceiptId = detail.ReceiptId
            };

            return model;
        }
    }
}