﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;
using System;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// A tuition charge - could be tuition by total or tuition by section
    /// </summary>
    public class TuitionChargeModel : ChargeDetailModel
    {
        /// <summary>
        /// Section name; e.g. ENGL-1301-01
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The title of the section
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The number of billing credits for which the student was charged
        /// </summary>
        public decimal? BillingCredits { get; set; }

        /// <summary>
        /// The number of CEUs for which the student was registered
        /// </summary>
        public decimal? CEUs { get; set; }

        /// <summary>
        /// The days of the week on which the section meets
        /// </summary>
        public string MeetingDays { get; set; }

        /// <summary>
        /// The times at which the section meets
        /// </summary>
        public string MeetingTimes { get; set; }

        /// <summary>
        /// The room in which the section meets
        /// </summary>
        public string MeetingRoom { get; set; }

        /// <summary>
        /// The instructor of the section
        /// </summary>
        public string Instructor { get; set; }

        /// <summary>
        /// The status of the section, as per Colleague
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Static build method for tuition charges (either by total or by section)
        /// </summary>
        /// <param name="detail">Tuition charge data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>TuitionChargeModel</returns>
        public static TuitionChargeModel Build(ActivityTuitionItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new TuitionChargeModel()
            {
                Name = detail.Id,
                Title = detail.Description,
                TermId = termUtility.GetTermIdForTermDescription(detail.TermId),
                TermDescription = detail.TermId,
                Amount = detail.Amount.GetValueOrDefault(),
                BillingCredits = detail.BillingCredits,
                CEUs = detail.Ceus,
                MeetingDays = DayOfWeekConverter.EnumCollectionToString(detail.Days),
                MeetingTimes = SectionUtility.GetMeetingTimes(detail.StartTime, detail.EndTime),
                MeetingRoom = detail.Classroom != null ? detail.Classroom.Replace('*', ' ') : null,
                Instructor = detail.Instructor,
                Status = detail.Status
            };

            return model;
        }
    }
}