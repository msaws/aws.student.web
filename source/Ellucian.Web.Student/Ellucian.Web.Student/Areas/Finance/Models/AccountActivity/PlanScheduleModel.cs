﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// Account Activity model for a planned payment on a payment plan
    /// </summary>
    public class PlanScheduleModel
    {
        /// <summary>
        /// ID of plan this scheduled payment is for
        /// </summary>
        public string PlanId { get; set; }

        /// <summary>
        /// Date payment is due
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Amount of the payment that is due
        /// </summary>
        public decimal DueAmount { get; set; }

        /// <summary>
        /// Amount of the payment that goes toward the plan setup charge
        /// </summary>
        public decimal? SetupAmount { get; set; }

        /// <summary>
        /// Amount of late charges on this scheduled payment
        /// </summary>
        public decimal? LateAmount { get; set; }

        /// <summary>
        /// Total amount that has been paid toward this scheduled payment.
        /// </summary>
        public decimal? PaidAmount { get; set; }

        /// <summary>
        /// Date of the last payment made toward this scheduled payment
        /// </summary>
        public DateTime? LastPaidDate { get; set; }

        /// <summary>
        /// Net amount still owed on this scheduled payment.
        /// </summary>
        public decimal Amount { get { return DueAmount + SetupAmount.GetValueOrDefault() + LateAmount.GetValueOrDefault() - PaidAmount.GetValueOrDefault(); } }

        /// <summary>
        /// Static method to build the PlanScheduleModel
        /// </summary>
        /// <param name="planId">The ID of the plan this scheduled payment is on</param>
        /// <param name="detail">The scheduled payment data from the API</param>
        /// <returns>PlanScheduleModel</returns>
        public static PlanScheduleModel Build(string planId, ActivityPaymentPlanScheduleItem detail)
        {
            if (string.IsNullOrEmpty(planId))
            {
                throw new ArgumentNullException("planId");
            }
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }

            var model = new PlanScheduleModel()
            {
                PlanId = planId,
                DueAmount = detail.Amount.GetValueOrDefault(),
                DueDate = detail.Date.GetValueOrDefault(),
                SetupAmount = detail.SetupCharge,
                LateAmount = detail.LateCharge,
                PaidAmount = detail.AmountPaid,
                LastPaidDate = detail.DatePaid
            };

            return model;
        }
    }
}