﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for reporting housing and meal charges
    /// </summary>
    public class RoomAndBoardChargeModel : ChargeDetailModel
    {
        /// <summary>
        /// Description of the room and board charge
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Description of the building/room
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// Date for which the charge was posted
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Static build method for room and board charges
        /// </summary>
        /// <param name="detail">Room and board charge data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>RoomAndBoardChargeModel</returns>
        public static RoomAndBoardChargeModel Build(ActivityRoomAndBoardItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new RoomAndBoardChargeModel()
            {
                Amount = detail.Amount.GetValueOrDefault(),
                Building = detail.Room,
                Description = detail.Description,
                Date = detail.Date.GetValueOrDefault(),
                TermId = detail.TermId,
                TermDescription = termUtility.GetTermDescription(detail.TermId),
            };

            return model;
        }
    }
}