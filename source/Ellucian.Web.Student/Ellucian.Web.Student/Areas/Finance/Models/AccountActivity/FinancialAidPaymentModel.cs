﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for Financial Aid payments
    /// </summary>
    public class FinancialAidPaymentModel : ITransactionModel
    {
        /// <summary>
        /// The award code of the payment
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The description of the award
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The amount of the award
        /// </summary>
        public decimal? AwardAmount { get; set; }

        /// <summary>
        /// An amount that is not available for FA
        /// </summary>
        public decimal? OtherAmount { get; set; }

        /// <summary>
        /// The list of other terms this award has been paid on
        /// </summary>
        public List<string> OtherTermIds { get; set; }

        /// <summary>
        /// The amount of any loan fee required for this award
        /// </summary>
        public decimal? LoanFeeAmount { get; set; }

        /// <summary>
        /// The amount of the award that the student is not eligible for
        /// </summary>
        public decimal? IneligibleAmount { get; set; }

        /// <summary>
        /// Any comments related to the award, including any reasons for ineligible amounts
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// The list of anticipated aid models
        /// </summary>
        public List<AnticipatedAidModel> AwardTerms { get; set; }

        /// <summary>
        /// Amount of anticipated aid for this award across all award terms in this award period
        /// </summary>
        public decimal? AnticipatedAidAmount { get { return AwardTerms.Sum(at => at.AnticipatedAmount.GetValueOrDefault()); } }

        /// <summary>
        /// Amount of disbursed aid for this award across all award terms in this award period
        /// </summary>
        public decimal? DisbursedAidAmount { get { return AwardTerms.Sum(at => at.DisbursementAmount.GetValueOrDefault()); } }

        /// <summary>
        /// The amount to report for this award. This amount rolls up into the various totals.
        /// </summary>
        public decimal Amount { get { return AnticipatedAidAmount.GetValueOrDefault() + DisbursedAidAmount.GetValueOrDefault(); } }

        /// <summary>
        /// Static method to build the FinancialAidPaymentModel
        /// </summary>
        /// <param name="detail">The input FA data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>FinancialAidPaymentModel</returns>
        public static FinancialAidPaymentModel Build(ActivityFinancialAidItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new FinancialAidPaymentModel()
            {
                Id = detail.PeriodAward,
                Description = detail.AwardDescription,
                AwardAmount = detail.AwardAmount,
                Comment = detail.Comments,
                IneligibleAmount = detail.IneligibleAmount,
                LoanFeeAmount = detail.LoanFee,
                OtherAmount = detail.OtherTermAmount,
                AwardTerms = new List<AnticipatedAidModel>()
            };

            if (detail.AwardTerms != null)
            {
                foreach (var awardTerm in detail.AwardTerms.Where(t => !string.IsNullOrEmpty(t.AwardTerm)))
                {
                    model.AwardTerms.Add(AnticipatedAidModel.Build(awardTerm, termUtility));
                }
            }

            return model;
        }
    }
}