﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for sponsorships
    /// </summary>
    public class SponsorshipModel : ITransactionModel
    {
        /// <summary>
        /// The sponsorship ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The date of the sponsorship
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The ID of the sponsor
        /// </summary>
        public string SponsorId { get; set; }

        /// <summary>
        /// The name of the sponsor
        /// </summary>
        public string SponsorName { get; set; }

        /// <summary>
        /// The term of the sponsorship
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The amount of the sponsorship
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Static build method for sponsorships
        /// </summary>
        /// <param name="detail">Sponsorship data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>SponsorshipModel</returns>
        public static SponsorshipModel Build(ActivitySponsorPaymentItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new SponsorshipModel()
            {
                Id = detail.Id,
                Amount = detail.Amount.GetValueOrDefault(),
                SponsorId = detail.Id,
                SponsorName = detail.Sponsorship,
                TermDescription = detail.TermId,
                TermId = termUtility.GetTermIdForTermDescription(detail.TermId),
                Date = detail.Date.GetValueOrDefault()
            };

            return model;
        }
    }
}