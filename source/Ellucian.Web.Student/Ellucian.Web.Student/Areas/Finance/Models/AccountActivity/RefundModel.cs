﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.AccountActivity
{
    /// <summary>
    /// The Account Activity model for refund transactions
    /// </summary>
    public class RefundModel : ITransactionModel
    {
        /// <summary>
        /// The ID of the refund voucher
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The date on the refund voucher
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The term for which the refund is being processed
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The description of the term
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// This description identifies the reason for the refund
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Amount of the refund
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// This field identifies what method of payment will be used to process the refund
        /// </summary>
        public string DisbursementMethod { get; set; }

        /// <summary>
        /// Date associated with the status of the refund
        /// </summary>
        public DateTime StatusDate { get; set; }

        /// <summary>
        /// Last 4 digits of the credit card of the refund
        /// </summary>
        public string PaymentReferenceNumber { get; set; }

        /// <summary>
        /// E-Commerce transaction number of the refund
        /// </summary>
        public string TransactionNumber { get; set; }

        /// <summary>
        /// Static method to build the RefundModel
        /// </summary>
        /// <param name="detail">The input refund data from the API</param>
        /// <param name="termUtility">The term/periods utility</param>
        /// <returns>RefundModel</returns>
        public static RefundModel Build(ActivityPaymentMethodItem detail, TermPeriodsUtility termUtility)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility");
            }

            var model = new RefundModel()
            {
                Id = detail.Id,
                Description = detail.Description,
                Date = detail.Date.GetValueOrDefault(),
                Amount = detail.Amount.GetValueOrDefault(),
                TermId = detail.TermId,
                TermDescription = termUtility.GetTermDescription(detail.TermId),
                DisbursementMethod = detail.Method,
                StatusDate = DetermineStatusDate(detail),
                PaymentReferenceNumber = DetermineReferenceNumber(detail),
                TransactionNumber = detail.TransactionNumber
            };

            return model;
        }

        private static string DetermineReferenceNumber(ActivityPaymentMethodItem item)
        {
            switch (item.Status)
            {
                case RefundVoucherStatus.Outstanding:
                case RefundVoucherStatus.NotApproved:
                    return GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "RefundInProgressText");
                case RefundVoucherStatus.Paid:
                case RefundVoucherStatus.Reconciled:
                    if (!string.IsNullOrEmpty(item.CheckNumber))
                    {
                        return item.CheckNumber;
                    }
                    else
                    {
                        return item.CreditCardLastFourDigits;
                    }
                default:
                    return string.Empty;
            }
        }

        private static DateTime DetermineStatusDate(ActivityPaymentMethodItem item)
        {
            if (item.CheckDate.HasValue)
            {
                return item.CheckDate.GetValueOrDefault();
            }
            else
            {
                return item.StatusDate;
            }
        }
    }
}