﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// Container for a user's payment plan options
    /// </summary>
    public class PaymentPlanOptionsModel
    {
        /// <summary>
        /// Message to give context to payment plan options or lack thereof
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Collection of payment plan options
        /// </summary>
        public List<TermSummaryModel> Options { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="PaymentPlanOptionsModel"/> class.
        /// </summary>
        /// <param name="options">Collection of payment plan options</param>
        /// <param name="message">Message to give context to payment plan options or lack thereof</param>
        /// <returns>A <see cref="PaymentPlanOptionsModel"/> object</returns>
        public PaymentPlanOptionsModel()
        {
            Options = new List<TermSummaryModel>();
            Message = String.Empty;
        }
    }
}