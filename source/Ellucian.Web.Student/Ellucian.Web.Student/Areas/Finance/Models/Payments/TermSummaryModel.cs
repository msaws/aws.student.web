﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using System.Text.RegularExpressions;
using Ellucian.Colleague.Dtos.Finance;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// Model representing a billing term and its items to be paid by an account holder
    /// </summary>
    public class TermSummaryModel
    {
        /// <summary>
        /// The term of interest
        /// </summary>
        public string TermCode { get; set; }

        /// <summary>
        /// the term description
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// The list of financial items that are due for the term
        /// </summary>
        public List<PaymentsDueModel> PaymentsDue { get; set; }

        /// <summary>
        /// The total amount due for the term
        /// </summary>
        public decimal DueAmount
        {
            get
            {
                return PaymentsDue.Sum(x => x.DueAmount);
            }
        }

        /// <summary>
        /// The total amount paid for the term
        /// </summary>
        public decimal PaidAmount
        {
            get
            {
                return (PaymentsDue.Sum(x => x.PaidAmount??0));
            }
        }

        /// <summary>
        /// Sanitized identifier for the term
        /// </summary>
        public string SanitizedId { get; set; }

        /// <summary>
        /// Builds a TermSummaryModel to represent all of the financial items that are due for the term
        /// </summary>
        /// <param name="source">Information about the financial items that are due</param>
        /// <param name="config">The current system configuration</param>
        /// <returns>TermSummaryModel to represent all of the financial items that are due for the term</returns>
        public static TermSummaryModel Build(AccountTerm source, FinanceConfiguration config)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            TermSummaryModel item = new TermSummaryModel()
            {
                TermCode = source.TermId,
                TermDescription = source.Description,
                PaymentsDue = new List<PaymentsDueModel>(),
            };

            if (source.GeneralItems != null) item.PaymentsDue.AddRange(source.GeneralItems.ConvertAll(gi => PaymentsDueModel.Build(gi, config)));
            if (source.InvoiceItems != null) item.PaymentsDue.AddRange(source.InvoiceItems.ConvertAll(ii => PaymentsDueModel.Build(ii, config)));
            if (source.PaymentPlanItems != null) item.PaymentsDue.AddRange(source.PaymentPlanItems.ConvertAll(pi => PaymentsDueModel.Build(pi, config)));
            if (source.DepositDueItems != null) item.PaymentsDue.AddRange(source.DepositDueItems.ConvertAll(di => PaymentsDueModel.Build(di, config)));

            // Pre-select the next future payment plan payment due for each plan
            var remainingPayPlanItems = item.PaymentsDue.Where(i => !i.IsSelected && !string.IsNullOrEmpty(i.PaymentPlanId) && !i.IsCredit).
                OrderBy(i => i.PaymentPlanId).
                ThenBy(i => i.DueDate);
            var remainingPayPlanIds = remainingPayPlanItems.Select(pp => pp.PaymentPlanId).Distinct();
            var firstPlanItems = new List<PaymentsDueModel>();
            foreach (var id in remainingPayPlanIds)
            {
                var firstItem = remainingPayPlanItems.FirstOrDefault(p => p.PaymentPlanId == id);
                if (firstItem != null) firstItem.IsSelected = true;
            }

            // Set the sanitized ID for the term
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            item.SanitizedId = string.IsNullOrEmpty(item.TermCode) ? string.Empty : rgx.Replace(item.TermCode, "");
            item.PaymentsDue.ForEach(pd => pd.TermId = item.TermCode);

            return item;
        }

        /// <summary>
        /// Builds a TermSummaryModel to represent all the invoices tht are due for the term
        /// </summary>
        /// <param name="source">Tuple that contains following information
        /// <item name="Item1" type="string">Term Code</item>
        ///<item name="Item2" type="string">Distribution Code</item>
        ///<item name="Item3" type="string">Contents To Append To Invoice Description</item>
        /// <item name="Item4" type="List<Invoice>"><see cref="Ellucian.Colleague.Dtos.Finance.Invoice"/>Invoice</item>
        /// </param>
        /// <param name="config">The current system configuration</param>
        /// <returns>TermSummaryModel to represent all of the invoice items that are due for the term</returns>
         public static TermSummaryModel Build(Tuple<string,string,string, List<Invoice>> source, FinanceConfiguration config)
         {
             if (source == null)
             {
                 throw new ArgumentNullException("source");
             }
             if (config == null)
             {
                 throw new ArgumentNullException("config");
             }
            string termCode = source.Item1; //Term Code
             string distributionCode = source.Item2;// distribution Code
             string contentsToAppendToDescription = source.Item3;//Contents that needs to be appended to invoice description
             List<Invoice> lstOfInvoices = source.Item4;//List of invoices
             //validate source
             if (termCode == null)
             {
                 throw new ArgumentNullException("source");
             }
             
             //validate source
             if (string.IsNullOrWhiteSpace(distributionCode))
             {
                 throw new ArgumentNullException("source- distributionCode");
             }
             if (lstOfInvoices == null || (lstOfInvoices != null && lstOfInvoices.Count() == 0))
             {
                 throw new ArgumentNullException("source - invoices");
             }
             TermSummaryModel item = new TermSummaryModel()
             {
                 TermCode = termCode,
                 PaymentsDue = new List<PaymentsDueModel>()
             };
             if (source != null)
                 item.PaymentsDue.AddRange(lstOfInvoices.ConvertAll(ii => PaymentsDueModel.Build(Tuple.Create<string, string, Invoice>(distributionCode, contentsToAppendToDescription,ii), config)));
             item.PaymentsDue.ForEach(pd => pd.TermId = item.TermCode);
             return item;
         }
    }
}