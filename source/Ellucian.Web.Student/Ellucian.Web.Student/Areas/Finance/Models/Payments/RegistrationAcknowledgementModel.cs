﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Finance.AccountDue;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayment
{
    public class RegistrationAcknowledgementModel : ImmediatePaymentWorkflowModel
    {
        public RegistrationAcknowledgementModel()
        {
            AcknowledgementText = new List<string>();
            RegisteredSections = new List<SectionModel>();
            NonRegisteredSections = new List<SectionModel>();
            WaitlistedSections = new List<SectionModel>();
            Charges = new List<RegistrationChargeModel>();
        }

        [DisplayFormat(DataFormatString = "{0:g}")]
        [DataType(DataType.DateTime)]
        public DateTime RegistrationDate { get; set; }

        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public List<string> AcknowledgementText { get; set; }
        public string ErrorMessage { get; set; }

        public List<SectionModel> RegisteredSections { get; set; }
        public List<SectionModel> NonRegisteredSections { get; set; }
        public List<SectionModel> WaitlistedSections { get; set; }
        public List<RegistrationChargeModel> Charges { get; set; }
    }
}