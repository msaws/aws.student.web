﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Utility;
using System.Text.RegularExpressions;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// Model representing a period and the billing terms within it for an account holder
    /// </summary>
    public class PeriodSummaryModel
    {
        /// <summary>
        /// The period of interest
        /// </summary>
        public string PeriodCode { get; set; }

        /// <summary>
        /// The period description
        /// </summary>
        public string PeriodDescription { get; set; }

        /// <summary>
        /// Information about the financial items due by a term in a period
        /// </summary>
        public List<TermSummaryModel> Terms { get; set; }

        /// <summary>
        /// The total amount still due for the period
        /// </summary>
        public decimal DueAmount
        {
            get
            {
                return Terms.Sum(x => x.DueAmount);
            }
        }

        /// <summary>
        /// The total amount paid for the period
        /// </summary>
        public decimal PaidAmount
        {
            get
            {
                return Terms.Sum(x => x.PaidAmount);
            }
        }

        /// <summary>
        /// Does the period have any terms with payments due?
        /// </summary>
        public bool HasItems
        {
            get
            {
                return Terms != null && Terms.Any();
            }
        }

        /// <summary>
        /// Sanitized identifier for the period
        /// </summary>
        public string SanitizedId
        {
            get
            {
                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                return rgx.Replace(PeriodCode, "");
            }
        }

        /// <summary>
        /// Builds a PeriodSummaryModel to represent financial items due by terms in a period
        /// </summary>
        /// <param name="source">Account due information</param>
        /// <param name="config">The current system configuration</param>
        /// <returns>PeriodSummaryModel for display a period</returns>
        public static PeriodSummaryModel Build(AccountDue source, FinanceConfiguration config)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (source.AccountTerms == null)
            {
                throw new ArgumentException("Period has no terms.");
            }

            if (source.StartDate == null && source.EndDate == null)
            {
                throw new ArgumentException("Period has no dates.");
            }

            // Determine the period type for the provided periods
            PeriodType periodType = (source.StartDate == null && source.EndDate.HasValue) ? PeriodType.Past : 
                (source.StartDate.HasValue && source.EndDate == null) ? PeriodType.Future : 
                PeriodType.Current;

            // Initialize the period model
            PeriodSummaryModel item = new PeriodSummaryModel()
            {
                Terms = new List<TermSummaryModel>()
            };

            // Set the period code and description based on the period type
            string periodDesc = string.Empty;
            switch (periodType)
            {
                case PeriodType.Past:
                    item.PeriodCode = "PAST";
                    periodDesc = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PastPeriodDescription");
                    item.PeriodDescription = !string.IsNullOrEmpty(periodDesc) ? string.Format(periodDesc, source.EndDate.Value.ToShortDateString()) : periodDesc;
                    break;
                case PeriodType.Future:
                    item.PeriodCode = "FTR";
                    periodDesc = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "FuturePeriodDescription");
                    item.PeriodDescription = !string.IsNullOrEmpty(periodDesc) ? string.Format(periodDesc, source.StartDate.Value.ToShortDateString()) : periodDesc;
                    break;
                default:
                    item.PeriodCode = "CUR";
                    periodDesc = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "CurrentPeriodDescription");
                    item.PeriodDescription = !string.IsNullOrEmpty(periodDesc) ? string.Format(periodDesc, source.StartDate.Value.ToShortDateString(), 
                        source.EndDate.Value.ToShortDateString()) : periodDesc;
                    break;
            }

            // Build term models if there are any terms with items due
            if (source.AccountTerms != null)
            {
                item.Terms = source.AccountTerms.ConvertAll(at => TermSummaryModel.Build(at, config));
            }

            // Update sanitized term IDs with their sanitized period code
            foreach (var term in item.Terms)
            {
                term.SanitizedId = term.SanitizedId.Insert(0, item.SanitizedId + "_");
            }

            return item;
        }
    }
}