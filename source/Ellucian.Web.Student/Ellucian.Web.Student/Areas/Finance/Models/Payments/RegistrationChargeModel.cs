﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayment
{
    public class RegistrationChargeModel
    {
        public string Item { get; set; }
        public string Term { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DataType(DataType.Currency)]
        public decimal? TotalAmount { get; set; }
        public DateTime DueDate { get; set; }
    }
}
