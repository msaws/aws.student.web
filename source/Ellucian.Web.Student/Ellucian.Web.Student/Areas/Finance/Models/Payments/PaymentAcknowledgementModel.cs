﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// Model for the Payment Acknowledgement
    /// </summary>
    public class PaymentAcknowledgementModel
    {
        /// <summary>
        /// Receipt reference number
        /// </summary>
        public string ReceiptNumber { get; set; }

        /// <summary>
        /// Date and time receipt was created
        /// </summary>
        public DateTime? ReceiptDate { get; set; }

        /// <summary>
        /// Merchant name and address
        /// </summary>
        public List<string> Merchant { get; set; }

        /// <summary>
        /// Payer name
        /// </summary>
        public string PayerName { get; set; }

        /// <summary>
        /// Payer ID
        /// </summary>
        public string PayerId { get; set; }

        /// <summary>
        /// Text of acknowledgement to display
        /// </summary>
        public List<string> AcknowledgementText { get; set; }

        /// <summary>
        /// Text for page footer
        /// </summary>
        public List<string> FooterText { get; set; }

        /// <summary>
        /// URL of image for page footer
        /// </summary>
        public Uri FooterImage { get; set; }

        /// <summary>
        /// Error message - returned when receipt was not created
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// List of payments on receivables accounts
        /// </summary>
        public List<AccountsReceivablePayment> Payments { get; set; }

        /// <summary>
        /// List of payments on deposit accounts
        /// </summary>
        public List<AccountsReceivableDeposit> Deposits { get; set; }

        /// <summary>
        /// List of general payments (no account involved)
        /// </summary>
        public List<GeneralPayment> OtherItems { get; set; }

        /// <summary>
        /// List of convenience fees charged to payer
        /// </summary>
        public List<ConvenienceFee> Fees { get; set; }

        /// <summary>
        /// List of payments tendered
        /// </summary>
        public List<PaymentMethod> PaymentsTendered { get; set; }

        /// <summary>
        /// ID of the associated transaction
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// ID of the cash receipt
        /// </summary>
        public string ReceiptId { get; set; }

        /// <summary>
        /// Amount of change returned on the cash receipt
        /// </summary>
        public decimal ChangeReturned { get; set; }

        /// <summary>
        /// A notification message to display
        /// </summary>
        public Notification Notification { get; set; }

        /// <summary>
        /// Static method to build payment acknowledgement model
        /// </summary>
        /// <param name="receipt">Receipt information</param>
        /// <param name="payControl">Payment control</param>
        /// <param name="returnUrl">Return URL for workflow</param>
        /// <returns>Payment acknowledgement model</returns>
        public static PaymentAcknowledgementModel Build(PaymentReceipt receipt)
        {
            if (receipt == null)
            {
                throw new ArgumentNullException("receipt");
            }

            var model = new PaymentAcknowledgementModel()
            {
                AcknowledgementText = new List<string>(),
                ChangeReturned = receipt.ChangeReturned,
                Deposits = new List<AccountsReceivableDeposit>(),
                ErrorMessage = receipt.ErrorMessage,
                Fees = new List<ConvenienceFee>(),
                FooterImage = receipt.AcknowledgeFooterImageUrl,
                FooterText = new List<string>(),
                Merchant = new List<string>(),
                OtherItems = new List<GeneralPayment>(),
                PayerId = receipt.ReceiptPayerId,
                PayerName = receipt.ReceiptPayerName,
                Payments = new List<AccountsReceivablePayment>(),
                PaymentsTendered = new List<PaymentMethod>(),
                ReceiptDate = receipt.ReceiptDate,
                ReceiptNumber = receipt.ReceiptNo,
                ReceiptId = receipt.CashReceiptsId
            };

            if (receipt.Payments != null)
            {
                model.Payments.AddRange(receipt.Payments);
            }
            if (receipt.Deposits != null)
            {
                model.Deposits.AddRange(receipt.Deposits);
            }
            if (receipt.OtherItems != null)
            {
                model.OtherItems.AddRange(receipt.OtherItems);
            }
            if (receipt.ConvenienceFees != null)
            {
                model.Fees.AddRange(receipt.ConvenienceFees);
            }
            if (receipt.PaymentMethods != null)
            {
                model.PaymentsTendered.AddRange(receipt.PaymentMethods);
            }
            if (receipt.ReceiptAcknowledgeText != null)
            {
                model.AcknowledgementText.AddRange(receipt.ReceiptAcknowledgeText);
            }
            if (receipt.AcknowledgeFooterText != null)
            {
                model.FooterText.AddRange(receipt.AcknowledgeFooterText);
            }
            if (receipt.MerchantNameAddress != null)
            {
                model.Merchant.AddRange(receipt.MerchantNameAddress);
            }
            model.Merchant.Add(" ");
            model.Merchant.Add(receipt.MerchantEmail);
            model.Merchant.Add(receipt.MerchantPhone);

            // Combine the receipt time with the receipt date
            if (receipt.ReceiptTime != null && model.ReceiptDate != null)
            {
                TimeSpan time = new TimeSpan(receipt.ReceiptTime.Value.Hour, receipt.ReceiptTime.Value.Minute, receipt.ReceiptTime.Value.Second);
                model.ReceiptDate = model.ReceiptDate.Value.Add(time);
            }

            return model;
        }

        /// <summary>
        /// Creates an ElectronicCheckEntry model when an error has occurred
        /// </summary>
        /// <param name="message">Message to display to user</param>
        /// <param name="type">The type of notification</param>
        /// <returns>An ElectronicCheckEntry model</returns>
        public static PaymentAcknowledgementModel BuildWithErrors(string message, NotificationType type = NotificationType.Error)
        {
            if (string.IsNullOrEmpty(message))
            {
                message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "UnableToLoadPaymentAcknowledgementMessage");
            }

            var model = new PaymentAcknowledgementModel()
            {
                Notification = new Notification(message, type)
            };
            return model;
        }

        /// <summary>
        /// Creates an ElectronicCheckEntry model when a payment was canceled
        /// </summary>
        /// <param name="message">Message to display to user</param>
        /// <param name="type">The type of notification</param>
        /// <returns>An ElectronicCheckEntry model</returns>
        public static PaymentAcknowledgementModel BuildForCanceledPayment(string message, NotificationType type = NotificationType.Success)
        {
            if (string.IsNullOrEmpty(message))
            {
                message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentCanceledMessage");
            }

            var model = new PaymentAcknowledgementModel()
            {
                Notification = new Notification(message, type)
            };
            return model;
        }
    }
}