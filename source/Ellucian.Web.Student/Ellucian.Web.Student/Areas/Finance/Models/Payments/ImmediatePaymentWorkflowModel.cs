﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayment
{
    /// <summary>
    /// The ImmediatePaymentWorkflowModel base class provides common information for other pages within a Immediate Payment workflow.
    /// </summary>
    public class ImmediatePaymentWorkflowModel
    {
        // Title of the Immediate Payment workflow
        public string WorkflowTitle { get; set; }
    }
}