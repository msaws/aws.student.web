﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using System;
using System.Globalization;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// Model for Electronic Check Entry processing
    /// </summary>
    public class ElectronicCheckEntryModel
    {
        /// <summary>
        /// Indicator for guaranteed check processing
        /// </summary>
        public bool UseGuaranteedChecks { get; set; }

        /// <summary>
        /// List of all valid state/province codes
        /// </summary>
        public List<StateProvinceCode> StateList { get; set; }

        /// <summary>
        /// The payment being processed
        /// </summary>
        public Payment Payment { get; set; }

        /// <summary>
        /// A notification message to display
        /// </summary>
        public Notification Notification { get; set; }

        /// <summary>
        /// Creates an ElectronicCheckEntry model
        /// </summary>
        /// <param name="config">Student Finance configuration</param>
        /// <param name="reviewModel">Payment Review model</param>
        /// <param name="payer">Payer information</param>
        /// <param name="statesProvinces">Collection of states/provinces</param>
        /// <returns>An ElectronicCheckEntry model</returns>
        public static ElectronicCheckEntryModel Build(FinanceConfiguration config, PaymentReviewModel reviewModel, ElectronicCheckPayer payer, IEnumerable<State> statesProvinces)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", "Finance Configuration must be provided.");
            }
            if (reviewModel == null || reviewModel.Payment == null)
            {
                throw new ArgumentNullException("reviewModel", "Payment Review model payment information must be provided.");
            }
            if (payer == null)
            {
                throw new ArgumentNullException("payer", "Payer information must be provided.");
            }
            if (statesProvinces == null)
            {
                throw new ArgumentNullException("statesProvinces", "Collection of states/provinces must be provided.");
            }

            List<State> statesList = statesProvinces.ToList();

            var model = new ElectronicCheckEntryModel()
            {
                UseGuaranteedChecks = config.UseGuaranteedChecks,
                Payment = new Payment()
                {
                    AmountToPay = reviewModel.Payment.AmountToPay,
                    CheckDetails = new CheckPayment()
                    {
                        FirstName = payer.FirstName,
                        LastName = string.IsNullOrEmpty(payer.MiddleName) ? payer.LastName : payer.MiddleName + " " + payer.LastName,
                        BillingAddress1 = payer.Street.Substring(0, Math.Min(payer.Street.Length, 30)),
                        City = payer.City,
                        State = payer.State,
                        ZipCode = payer.PostalCode.Replace("-", ""),
                        EmailAddress = payer.Email
                    },
                    ConvenienceFee = reviewModel.Payment.ConvenienceFee,
                    ConvenienceFeeAmount = reviewModel.Payment.ConvenienceFeeAmount,
                    ConvenienceFeeGeneralLedgerNumber = reviewModel.Payment.ConvenienceFeeGeneralLedgerNumber,
                    Distribution = reviewModel.Payment.Distribution,
                    PaymentItems = reviewModel.Payment.PaymentItems,
                    PayMethod = reviewModel.Payment.PayMethod,
                    PersonId = reviewModel.Payment.PersonId,
                    PayerId = reviewModel.Payment.PayerId,
                    ProviderAccount = reviewModel.Payment.ProviderAccount,
                    ReturnToOriginUrl = reviewModel.Payment.ReturnToOriginUrl,
                    ReturnUrl = reviewModel.Payment.ReturnUrl
                },
                StateList = new List<StateProvinceCode>()
            };

            statesList.ForEach(s => model.StateList.Add(new StateProvinceCode(s.Code, s.Description, s.CountryCode)));

            return model;
        }

        /// <summary>
        /// Creates an ElectronicCheckEntry model when an error has occurred
        /// </summary>
        /// <param name="message">Message to display to user</param>
        /// <param name="type">The type of notification</param>
        /// <returns>An ElectronicCheckEntry model</returns>
        public static ElectronicCheckEntryModel BuildWithErrors(string message, NotificationType type = NotificationType.Error)
        {
            if (string.IsNullOrEmpty(message))
            {
                message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "UnableToLoadElectronicCheckEntryMessage");
            }

            var model = new ElectronicCheckEntryModel()
            {
                Notification = new Notification(message, type)
            };
            return model;
        }
    }
}