﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// This is the main model used for the Make a Payment view
    /// </summary>
    public class MakeAPaymentModel : StudentFinanceAdminModel
    {
        /// <summary>
        /// The list of available payment methods
        /// </summary>
        public List<AvailablePaymentMethod> AvailablePaymentMethods { get; set; }

        /// <summary>
        /// The selected payment method
        /// </summary>
        public AvailablePaymentMethod SelectedPaymentMethod { get; set; }

        /// <summary>
        /// The record of the person's financial activity, by term
        /// </summary>
        public List<TermSummaryModel> TermModels { get; set; }

        /// <summary>
        /// The record of the person's financial activity, by period
        /// </summary>
        public List<PeriodSummaryModel> PeriodModels { get; set; }

        /// <summary>
        /// Indicates if e-commerce payments are allowed
        /// </summary>
        public bool PaymentsAllowed { get; set; }

        /// <summary>
        /// Indicates if partial payments are allowed for accounts receivable payments due
        /// </summary>
        public bool PartialAccountPaymentsAllowed { get; set; }

        /// <summary>
        /// Indicates if partial payments are allowed for deposits due
        /// </summary>
        public bool PartialDepositPaymentsAllowed { get; set; }

        /// <summary>
        /// Partial payment plan payment permissions
        /// </summary>
        public PartialPlanPayments PartialPlanPaymentsAllowed { get; set; }

        /// <summary>
        /// The total amount currently due for the person
        /// </summary>
        public decimal DueAmount
        {
            get
            {
                if (IsTermDisplay)
                {
                    return TermModels == null ? 0 : TermModels.Sum(x => x.DueAmount);
                }
                else
                {
                    return PeriodModels == null ? 0 : PeriodModels.Sum(x => x.DueAmount);
                }
            }
        }

        /// <summary>
        /// The total amount paid by the person
        /// </summary>
        public decimal PaidAmount
        {
            get
            {
                if (IsTermDisplay)
                {
                    return TermModels == null ? 0 : TermModels.Sum(x => x.PaidAmount);
                }
                else
                {
                    return PeriodModels == null ? 0 : PeriodModels.Sum(x => x.PaidAmount);
                }
            }
        }

        /// <summary>
        /// Does the account holder have any payments due for display?
        /// </summary>
        public bool HasData
        {
            get
            {
                if (IsTermDisplay)
                {
                    return TermModels != null && TermModels.Any() && TermModels.Any(t => t.PaymentsDue.Any());
                }
                else
                {
                    return PeriodModels != null && PeriodModels.Any() && PeriodModels.Any(p => p.Terms.Any()) && PeriodModels.Any(p => p.Terms.Any(t => t.PaymentsDue.Any()));
                }
            }
        }

        /// <summary>
        /// Message displayed when the account holder has no payments due for display
        /// </summary>
        public string NoDataMessage
        {
            get
            {
                return GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "NoPaymentsDue");
            }
        }

        /// <summary>
        /// Flag indicating if credit amounts are displayed
        /// </summary>
        public bool ShowCreditAmounts { get; set; }

        /// <summary>
        /// Flag indicating if user payment plan creation is enabled
        /// </summary>
        public bool UserPaymentPlanCreationEnabled { get; set; }

        /// <summary>
        /// The record of the person's payment plan-eligible financial activity, by term
        /// </summary>
        public List<TermSummaryModel> PaymentPlanTermModels { get; set; }

        /// <summary>
        /// The record of the person's payment plan-eligible financial activity, by period
        /// </summary>
        public List<PeriodSummaryModel> PaymentPlanPeriodModels { get; set; }

        /// <summary>
        /// Text for communicating institution-defined messages regarding invoice and charge eligibility for payment plans
        /// </summary>
        public string PaymentPlanEligibilityText { get; set; }

        /// <summary>
        /// Creates a MakeAPayment model for term-based display
        /// </summary>
        /// <param name="source">The financial activity by term</param>
        /// <param name="config">The current system configuration</param>
        /// <param name="person">The person of interest</param>
        /// <param name="currentUser">The user currently logged into self service</param>
        /// <returns>A term-based MakeAPayment model</returns>
        public static MakeAPaymentModel Build(AccountDue source, FinanceConfiguration config, AccountHolder person, ICurrentUser currentUser)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            if (person == null)
            {
                throw new ArgumentNullException("person");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser");
            }
            if (config.PaymentDisplay == PaymentDisplay.DisplayByPeriod)
            {
                throw new ArgumentException("Payment data is in Term format but configuration is in Term mode.");
            }

            // Initialize the model
            MakeAPaymentModel item = new MakeAPaymentModel()
            {
                AlertMessage = config.NotificationText,
                AvailablePaymentMethods = config.PaymentMethods,
                IsAdminUser = currentUser.PersonId != person.Id && !currentUser.ProxySubjects.Any(),
                IsTermDisplay = true,
                PartialAccountPaymentsAllowed = config.PartialAccountPaymentsAllowed,
                PartialDepositPaymentsAllowed = config.PartialDepositPaymentsAllowed,
                PartialPlanPaymentsAllowed = config.PartialPlanPaymentsAllowed,
                PaymentsAllowed = (config.ECommercePaymentsAllowed && config.SelfServicePaymentsAllowed),
                PersonId = person.Id,
                PersonName = person.PreferredName,
                PrivacyStatusCode = person.PrivacyStatusCode,
                ShowCreditAmounts = config.ShowCreditAmounts,
                TermModels = new List<TermSummaryModel>(),
                UserPaymentPlanCreationEnabled = config.UserPaymentPlanCreationEnabled,
                PaymentPlanEligibilityText = config.PaymentPlanEligibilityText,
            };

            // Build term models if there are any terms with items due
            if (source.AccountTerms != null)
            {
                item.TermModels = source.AccountTerms.ConvertAll(x => TermSummaryModel.Build(x, config));
            }

            // Pre-select all items currently due but only for one payment group
            string firstPaymentGroup = null;
            DeSelectItemsFromMultiplePaymentGroups(item.TermModels, ref firstPaymentGroup);

            return item;
        }

        /// <summary>
        /// Creates a MakeAPayment model for PCF-based display
        /// </summary>
        /// <param name="source">The financial activity by period</param>
        /// <param name="config">The current system configuration</param>
        /// <param name="person">The person of interest</param>
        /// <param name="currentUser">The user currently logged into self service</param>
        /// <returns>A period-based MakeAPayment model</returns>
        public static MakeAPaymentModel Build(AccountDuePeriod source, FinanceConfiguration config, AccountHolder person, ICurrentUser currentUser)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            if (person == null)
            {
                throw new ArgumentNullException("person");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser");
            }
            if (config.PaymentDisplay == PaymentDisplay.DisplayByTerm)
            {
                throw new ArgumentException("Payment data is in Period format, but configuration is in Term mode.");
            }

            // Initialize the model
            MakeAPaymentModel item = new MakeAPaymentModel()
            {
                AlertMessage = config.NotificationText,
                AvailablePaymentMethods = config.PaymentMethods,
                IsAdminUser = currentUser.PersonId != person.Id && !currentUser.ProxySubjects.Any(),
                IsTermDisplay = false,
                PartialAccountPaymentsAllowed = config.PartialAccountPaymentsAllowed,
                PartialDepositPaymentsAllowed = config.PartialDepositPaymentsAllowed,
                PartialPlanPaymentsAllowed = config.PartialPlanPaymentsAllowed,
                PaymentsAllowed = (config.ECommercePaymentsAllowed && config.SelfServicePaymentsAllowed),
                PeriodModels = new List<PeriodSummaryModel>(),
                PersonId = person.Id,
                PersonName = person.PreferredName,
                PrivacyStatusCode = person.PrivacyStatusCode,
                ShowCreditAmounts = config.ShowCreditAmounts,
                UserPaymentPlanCreationEnabled = config.UserPaymentPlanCreationEnabled,
                PaymentPlanEligibilityText = config.PaymentPlanEligibilityText
            };

            // Build term models if there are any terms with items due
            if (source.Past != null)
            {
                item.PeriodModels.Add(PeriodSummaryModel.Build(source.Past, config));
            }
            if (source.Current != null)
            {
                item.PeriodModels.Add(PeriodSummaryModel.Build(source.Current, config));
            }
            if (source.Future != null)
            {
                item.PeriodModels.Add(PeriodSummaryModel.Build(source.Future, config));
            }

            // Pre-select all items currently due but only for one payment group
            string firstPaymentGroup = null;
            if (item.HasData)
            {
                foreach (var period in item.PeriodModels)
                {
                    DeSelectItemsFromMultiplePaymentGroups(period.Terms, ref firstPaymentGroup);
                }
            }
            return item;
        }
     
        /// <summary>
        /// Create a MakeAPaymentModel for invoices for term-based display 
        /// </summary>
        /// <param name="source">Invoices Due for payment
        /// </param>
        /// <param name="config">The current system configuration</param>
        /// <param name="person">The person of interest</param>
        /// <param name="currentUserId">The current user</param>
        /// <returns>A term-based MakeAPayment model</returns>
        public static MakeAPaymentModel Build(InvoiceDue source, FinanceConfiguration config, AccountHolder person, string currentUserId)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            if (person == null)
            {
                throw new ArgumentNullException("person");
            }
            if (string.IsNullOrEmpty(currentUserId))
            {
                throw new ArgumentNullException("currentUserId");
            }
            if (config.PaymentDisplay == PaymentDisplay.DisplayByPeriod)
            {
                throw new ArgumentException("Payment data is built only in Term format but configuration is in Period mode.");
            }
            //validate source
            if (string.IsNullOrWhiteSpace(source.DistributionCode))
            {
                throw new ArgumentNullException("source- distributionCode");
            }
            if (source.Invoices == null || (source.Invoices != null && source.Invoices.Count() == 0))
            {
                throw new ArgumentNullException("source - invoices");
            }

            // Initialize the model
            MakeAPaymentModel item = new MakeAPaymentModel()
            {
                AlertMessage = config.NotificationText,
                AvailablePaymentMethods = config.PaymentMethods,
                IsAdminUser = currentUserId != person.Id,
                IsTermDisplay = true,
                PartialAccountPaymentsAllowed = config.PartialAccountPaymentsAllowed,
                PartialDepositPaymentsAllowed = config.PartialDepositPaymentsAllowed,
                PartialPlanPaymentsAllowed = config.PartialPlanPaymentsAllowed,
                PaymentsAllowed = (config.ECommercePaymentsAllowed && config.SelfServicePaymentsAllowed),
                PersonId = person.Id,
                PersonName = person.PreferredName,
                PrivacyStatusCode = person.PrivacyStatusCode,
                ShowCreditAmounts = config.ShowCreditAmounts,
                TermModels = new List<TermSummaryModel>() //we will build invoices in group of terms
            };
            var groupedInvoices = from invoice in source.Invoices
                                  group invoice by new { invoice.TermId } into newGroup
                                  select newGroup;

            foreach (var nameGroup in groupedInvoices)
            {
                var invoicesForTerm = Tuple.Create<string,  string, string, List<Invoice>>(nameGroup.Key.TermId,  source.DistributionCode, source.ContentsForDescription, nameGroup.ToList<Invoice>());
                item.TermModels.Add(TermSummaryModel.Build(invoicesForTerm, config));
            }

            string firstPaymentGroup = null;
            DeSelectItemsFromMultiplePaymentGroups(item.TermModels, ref firstPaymentGroup);
            return item;
        }
        

        /// <summary>
        /// Only one payment group may be paid by a payment; this method determines if multiple payment groups'
        /// due items have been pre-selected and de-selects any items not belonging to the first encountered
        /// payment group
        /// </summary>
        /// <param name="terms">List of TermSummaryModel objects</param>
        /// <param name="firstPaymentGroup">The name of the first encountered payment group</param>
        private static void DeSelectItemsFromMultiplePaymentGroups(List<TermSummaryModel> terms, ref string firstPaymentGroup)
        {
            if (terms != null)
            {
                foreach (var term in terms)
                {
                    foreach (var dueItem in term.PaymentsDue.Where(p => p.IsSelected))
                    {
                        dueItem.IsSelected = firstPaymentGroup == null ? true : firstPaymentGroup == dueItem.PaymentGroup ? true : false;
                        firstPaymentGroup = firstPaymentGroup == null ? dueItem.PaymentGroup : firstPaymentGroup;
                    }
                }
            }
            return;
        }
    }
}