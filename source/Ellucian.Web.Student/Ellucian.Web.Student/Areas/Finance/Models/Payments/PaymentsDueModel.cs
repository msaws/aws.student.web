﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// Model representing an item to be paid by an account holder
    /// </summary>
    public class PaymentsDueModel
    {
        /// <summary>
        /// Indicates if the item has been selected for payment
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// The description of the item
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The payment group
        /// </summary>
        public string PaymentGroup { get; set; }

        /// <summary>
        /// The date by which this item is due
        /// </summary>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// The amount of the item
        /// </summary>
        public decimal DueAmount { get; set; }

        /// <summary>
        /// The amount that has been paid on this item
        /// </summary>
        public decimal? PaidAmount { get; set; }

        /// <summary>
        /// The receivable type
        /// </summary>
        public string ReceivableTypeCode { get; set; }

        /// <summary>
        /// If the item is an invoice, the identifier of the item
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// If the item is a payment plan payment, the identifier of the item
        /// </summary>
        public string PaymentPlanId { get; set; }

        /// <summary>
        /// If the item is a deposit due, the identifier of the item
        /// </summary>
        public string DepositDueId { get; set; }

        /// <summary>
        /// Indicates that this item has a negative amount to be paid
        /// </summary>
        public bool IsCredit
        {
            get
            {
                return (DueAmount < 0);
            }
        }

        /// <summary>
        /// Indicates that this item has not been paid in a timely manner
        /// </summary>
        public bool IsOverdue
        {
            get
            {
                return (DueDate != null && DueDate < DateTime.Today);
            }
        }

        /// <summary>
        /// Flag indicating if the Amount to Pay can be edited
        /// </summary>
        public bool IsEditable { get; set; }

        /// <summary>
        /// Minimum payment allowed for the item
        /// </summary>
        public decimal? MinimumPayment
        {
            get
            {
                return IsCredit? (decimal?) null: (IsEditable ? 1m : DueAmount);
            }
        }

        /// <summary>
        /// Unique identifier for the item to be paid
        /// </summary>
        public string Id
        {
            get
            {
                var id = string.Join("_", new[] { Description, DueAmount.ToString(), PaymentGroup, ReceivableTypeCode, InvoiceId, PaymentPlanId, DepositDueId });
                if (DueDate.HasValue)
                {
                    id += "_" + DueDate.Value.ToShortDateString();
                }
                return id;
            }
        }

        /// <summary>
        /// Flag indicating if the item can be paid or not
        /// </summary>
        public bool IsPayable { get; set; }

        /// <summary>
        /// Flag indicating if the item is eligible to be included on a payment plan
        /// </summary>
        public bool IsEligibleForPaymentPlan { get; set; }

        /// <summary>
        /// ID of the payment plan template to be used in creating a payment plan for the item
        /// </summary>
        public string PaymentPlanTemplateId { get; set; }

        /// <summary>
        /// ID of the term to which this item belongs
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// Creates a PaymentsDueModel for displaying the amount due for an item
        /// </summary>
        /// <param name="source">Information about the due item</param>
        /// <param name="config">The current system configuration</param>
        /// <returns>PaymentsDueModel for displaying an item</returns>
        public static PaymentsDueModel Build(AccountsReceivableDueItem source, FinanceConfiguration config)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            PaymentsDueModel item = new PaymentsDueModel()
            {
                Description = source.Description,
                DueAmount = source.AmountDue.GetValueOrDefault(),
                DueDate = source.DueDate,
                InvoiceId = (source is InvoiceDueItem) ? ((InvoiceDueItem)source).InvoiceId : string.Empty,
                PaymentGroup = source.Distribution,
                PaymentPlanId = (source is PaymentPlanDueItem) ? ((PaymentPlanDueItem)source).PaymentPlanId : string.Empty,
                ReceivableTypeCode = source.AccountType,
                DepositDueId = string.Empty,
            };

            // Determine if the amount to pay for the item can be edited and set the PaidAmount
            if (item.IsCredit)
            {
                item.IsEditable = false;
                item.PaidAmount = null;
            }
            else
            {
                item.PaidAmount = item.DueAmount;
                if (source is PaymentPlanDueItem)
                {
                    switch (config.PartialPlanPaymentsAllowed)
                    {
                        case PartialPlanPayments.Allowed:
                            item.IsEditable = true;
                            break;
                        case PartialPlanPayments.AllowedWhenNotOverdue:
                            item.IsEditable = item.IsOverdue ? false : true;
                            break;
                        default:
                            item.IsEditable = false;
                            break;
                    }
                }
                else
                {
                    item.IsEditable = config.PartialAccountPaymentsAllowed;
                }
            }

            // Pre-select the item (positive balances only) if it is currently due:
            // - overdue items (all types)
            // - general items
            // - invoice items
            if (!item.IsCredit && (item.IsOverdue || (item.DueDate.HasValue && item.DueDate <= DateTime.Today && !(source is PaymentPlanDueItem))))
            {
                item.IsSelected = true;
            }
            // Determine if the item is payable
            PaymentsDueModel.SetPayableAndSelectedFlags(config.DisplayedReceivableTypes, item);

            // Set payment plan eligibility flag
            item.IsEligibleForPaymentPlan = !(source is InvoiceDueItem) && !(source is PaymentPlanDueItem) && !item.IsCredit;


            return item;
        }

        /// <summary>
        /// Creates a PaymentsDueModel for a deposit due
        /// </summary>
        /// <param name="source">Information about the deposit due</param>
        /// <param name="config">The current system configuration</param>
        /// <returns>PaymentsDueMode for displaying a deposit due</returns>
        public static PaymentsDueModel Build(DepositDue source, FinanceConfiguration config)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            PaymentsDueModel item = new PaymentsDueModel()
            {
                Description = source.DepositTypeDescription,
                DueAmount = source.AmountDue,
                DueDate = source.DueDate,
                IsEditable = config.PartialDepositPaymentsAllowed,
                PaymentGroup = source.Distribution,
                DepositDueId = source.Id,
                InvoiceId = string.Empty,
                PaymentPlanId = string.Empty,
                IsPayable = true,
                IsEligibleForPaymentPlan = false
            };

            // Pre-select the item if it is currently due:
            // - overdue items
            // - items that are due today
            if (!item.IsCredit && (item.DueDate <= DateTime.Today))
            {
                item.IsSelected = true;
            }
            
            return item;
        }
        
        /// <summary>
        /// Create a PaymentsDueModel for the invoice
        /// </summary>
        /// <param name="source">A tuple containing following information
        /// <item name="Item1" type="string">Distribution Code</item>
        /// <item name="Item2" type="string">Contents To Append To Invoice Description</item>
        /// <item name="Item3" type="Invoice"><see cref="Ellucian.Colleague.Dtos.Finance.Invoice"/>Invoice</item>
        /// </param>
        /// <param name="config">The current system configuration</param>
        /// <returns>PaymentsDueModel for displaying an invoice item</returns>
        public static PaymentsDueModel Build(Tuple<string,string,Invoice> source, FinanceConfiguration config)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            string distributionCode = source.Item1;//Distribution Code
            string contentsToAppendToDescription = source.Item2;//Contents to append to invoice description
            Invoice invoice = source.Item3; //Invoice
            //validate source
            if (string.IsNullOrWhiteSpace(distributionCode))
            {
                throw new ArgumentNullException("source- distributionCode");
            }
            if (invoice == null )
            {
                throw new ArgumentNullException("source - invoice");
            }
            PaymentsDueModel item = new PaymentsDueModel()
            {
                Description = invoice.Description +( !string.IsNullOrWhiteSpace(contentsToAppendToDescription)? "- "+ contentsToAppendToDescription:""),
                DueAmount = invoice.Amount,
                DueDate = invoice.DueDate,
                IsEditable = config.PartialDepositPaymentsAllowed,
                PaymentGroup = distributionCode,
                DepositDueId = string.Empty,
                InvoiceId = invoice.Id,
                PaymentPlanId = string.Empty,
                ReceivableTypeCode = invoice.ReceivableTypeCode,
                IsEligibleForPaymentPlan = false
            };
            item.PaidAmount = item.DueAmount;
            item.IsSelected = true;

            // Determine if the item is payable
            PaymentsDueModel.SetPayableAndSelectedFlags(config.DisplayedReceivableTypes, item);

            return item;
        }

        /// <summary>
        /// Sets the IsPayable and IsSelected flags on a <see cref="PaymentsDueModel"/> object.
        /// </summary>
        /// <param name="receivableTypes">Collection of receivable types and whether or not they are payable</param>
        /// <param name="model">A <see cref="PaymentsDueModel"/> object</param>
        private static void SetPayableAndSelectedFlags(IEnumerable<PayableReceivableType> receivableTypes, PaymentsDueModel model)
        {
            if (receivableTypes == null)
            {
                throw new ArgumentNullException("receivableTypes", "Receivable types collection cannot be null");
            }
            if (model == null)
            {
                throw new ArgumentNullException("model", "Payment due cannot be null");
            }

            if (!model.IsCredit && (!receivableTypes.Any() || receivableTypes.Any(drt => drt.IsPayable && drt.Code == model.ReceivableTypeCode)))
            {
                model.IsPayable = true;
            }
            model.IsSelected = model.IsSelected && model.IsPayable;
        }
    }
}