﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.Payments
{
    /// <summary>
    /// Model for the Payment Review form
    /// </summary>
    public class PaymentReviewModel
    {
        /// <summary>
        /// Line items to display on the payment review form
        /// </summary>
        public List<PaymentReviewItem> LineItems { get; set; }

        /// <summary>
        /// The payment method selected by the user
        /// </summary>
        public AvailablePaymentMethod PaymentMethod { get; set; }

        /// <summary>
        /// The message to display before the line items; this may contain HTML markup
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The index (relative to 1) of the payment currently being processed
        /// </summary>
        public int PaymentNumber { get; set; }

        /// <summary>
        /// Total number of payments required
        /// </summary>
        public int PaymentCount { get; set; }

        /// <summary>
        /// Current payment to process
        /// </summary>
        public Payment Payment { get; set; }

        /// <summary>
        /// A notification message to display
        /// </summary>
        public Notification Notification { get; set; }

        /// <summary>
        /// Static method to build model
        /// </summary>
        /// <param name="payment">Payment to process</param>
        /// <param name="config">Student Finance configuration</param>
        /// <param name="receivableTypes">Collection of all receivable types</param>
        /// <param name="convenienceFees">Collection of all convenience fees</param>
        /// <param name="itemsForPayment">Collection of items to be paid</param>
        /// <returns>A PaymentReview Model</returns>
        public static PaymentReviewModel Build(Payment payment, AvailablePaymentMethod selectedPaymentMethod, IEnumerable<ReceivableType> receivableTypes,
            IEnumerable<Colleague.Dtos.Base.ConvenienceFee> convenienceFees, string reviewMessage)
        {
            if (payment == null || payment.PaymentItems == null || payment.PaymentItems.Count == 0)
            {
                throw new ArgumentNullException("payment", "A payment with at least one payment item must be supplied.");
            }
            if (selectedPaymentMethod == null)
            {
                throw new ArgumentNullException("selectedPaymentMethod");
            }
            if (receivableTypes == null || receivableTypes.Count() == 0)
            {
                throw new ArgumentNullException("receivableTypes");
            }

            var model = new PaymentReviewModel()
            {
                Message = reviewMessage,
                Payment = payment,
                PaymentMethod = selectedPaymentMethod,
                LineItems = new List<PaymentReviewItem>()
            };

            // Each line item on the payment gets a separate line
            foreach (var item in payment.PaymentItems)
            {
                model.LineItems.Add(new PaymentReviewItem()
                {
                    Description = item.Description,
                    Amount = item.PaymentAmount
                });
            }

            // If there's a convenience fee, add a line for it
            if (model.Payment.ConvenienceFeeAmount != null && model.Payment.ConvenienceFeeAmount.Value > 0)
            {
                if (convenienceFees == null || convenienceFees.Count() == 0)
                {
                    throw new ArgumentNullException("convenienceFees", "A convenience fee is being assessed but no convenience fees were provided.");
                }

                var fee = convenienceFees.FirstOrDefault(x => x.Code == model.Payment.ConvenienceFee);
                if (fee != null)
                {
                    model.LineItems.Add(new PaymentReviewItem()
                    {
                        Description = fee.Description,
                        Amount = model.Payment.ConvenienceFeeAmount.Value
                    });
                }
            }

            // If there's more than one line item, then add a total line
            if (model.LineItems.Count >= 1)
            {
                model.LineItems.Add(new PaymentReviewItem()
                {
                    Description = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "ReviewTotalPaymentDescription"),
                    Amount = model.LineItems.Sum(x => x.Amount)
                });
            }
            return model;
        }

        /// <summary>
        /// Creates an PaymentReview model when an error has occurred
        /// </summary>
        /// <param name="message">Message to display to user</param>
        /// <param name="type">The type of notification</param>
        /// <returns>An ElectronicCheckEntry model</returns>
        public static PaymentReviewModel BuildWithErrors(string message = null, NotificationType type = NotificationType.Error)
        {
            if (string.IsNullOrEmpty(message))
            {
                message = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "UnableToLoadPaymentReviewMessage");
            }

            var model = new PaymentReviewModel()
            {
                Notification = new Notification(message, type)
            };
            return model;
        }

    }
}