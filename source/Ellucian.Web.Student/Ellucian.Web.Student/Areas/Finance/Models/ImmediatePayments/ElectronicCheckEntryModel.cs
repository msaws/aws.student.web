﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using System;
using System.Globalization;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    public class ElectronicCheckEntryModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// Indicator for guaranteed check processing
        /// </summary>
        public bool UseGuaranteedChecks { get; set; }

        /// <summary>
        /// List of all valid state/province codes
        /// </summary>
        public List<StateProvinceCode> StateList { get; set; }

        /// <summary>
        /// The payment being processed
        /// </summary>
        public Payment Payment { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ElectronicCheckEntryModel() : base()
        {
            Payment = new Payment();
            StateList = new List<StateProvinceCode>();
        }

        /// <summary>
        /// Build this model
        /// </summary>
        /// <param name="config">Student Finance configuration</param>
        /// <param name="paymentModel">Payment Review model</param>
        /// <param name="payer">Payer information</param>
        /// <param name="statesProvinces">Collection of states/provinces</param>
        /// <returns>Electronic check entry model</returns>
        public static ElectronicCheckEntryModel Build(FinanceConfiguration config, PaymentReviewModel paymentModel, ElectronicCheckPayer payer, IEnumerable<State> statesProvinces)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", "Finance Configuration must be provided.");
            }
            if (paymentModel == null || paymentModel.Payment == null)
            {
                throw new ArgumentNullException("paymentModel", "Payment Review model payment information must be provided.");
            }
            if (payer == null)
            {
                throw new ArgumentNullException("payer", "Payer information must be provided.");
            }
            if (statesProvinces == null)
            {
                throw new ArgumentNullException("statesProvinces", "Collection of states/provinces must be provided.");
            }

            var model = ImmediatePaymentWorkflowModel.Build(new ElectronicCheckEntryModel(), paymentModel);

            model.WorkflowStep = "Payment";

            model.UseGuaranteedChecks = config.UseGuaranteedChecks;
            model.StateList = new List<StateProvinceCode>();
            
            List<State> statesList = statesProvinces.ToList();
            statesList.ForEach(s => model.StateList.Add(new StateProvinceCode(s.Code, s.Description, s.CountryCode)));

            model.Payment = paymentModel.Payment;
            model.Payment.CheckDetails = new CheckPayment()
                {
                    FirstName = payer.FirstName,
                    LastName = string.IsNullOrEmpty(payer.MiddleName) ? payer.LastName : payer.MiddleName + " " + payer.LastName,
                    BillingAddress1 = payer.Street.Substring(0, Math.Min(payer.Street.Length, 30)),
                    City = payer.City,
                    State = payer.State,
                    ZipCode = payer.PostalCode.Replace("-",""),
                    EmailAddress = payer.Email
                };

            return model;
        }
    }
}