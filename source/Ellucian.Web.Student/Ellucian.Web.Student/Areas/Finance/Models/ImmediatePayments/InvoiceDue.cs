﻿using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// Encapsulates list of invoices due for payment and the distribution code to use for the payments.
    /// </summary>
    public class InvoiceDue
    {
        /// <summary>
        /// list of invoices
        /// </summary>
        public List<Invoice> Invoices { get; set; }
        /// <summary>
        /// Distribution Code.
        /// </summary>
        public string DistributionCode { get; set; }
        /// <summary>
        /// This is the contents that needs to be appended to invoice description
        /// </summary>
        public string ContentsForDescription { get; set; }

        public InvoiceDue(string distributionCode, string contents, List<Invoice> invoices)
        {
            this.DistributionCode = distributionCode;
            this.ContentsForDescription = contents;
            this.Invoices = invoices;
        }
        public InvoiceDue()
        {}
    }
}