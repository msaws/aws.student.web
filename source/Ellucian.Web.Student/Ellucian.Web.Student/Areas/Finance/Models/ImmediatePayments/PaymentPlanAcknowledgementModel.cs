﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// Payment acknowledgement model for immediate payment processing
    /// </summary>
    public class PaymentPlanAcknowledgementModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// Full name of the student
        /// </summary>
        public string PersonName { get; set; }

        /// <summary>
        /// ID of the student
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// Details of proposed payment plan
        /// </summary>
        public PaymentPlanDetailsModel PlanDetails { get; set; }

        /// <summary>
        /// Indicates whether view will be printed
        /// </summary>
        public bool IsPrintable { get; set; }

        /// <summary>
        /// Is the current user an administrative user?
        /// </summary>
        public bool IsAdminUser { get; set; }

        /// <summary>
        /// Approval information message
        /// </summary>
        public string ApprovalInformationMessage
        {
            get
            {
                if (PlanDetails.ApprovalTimestamp.HasValue)
                {
                    return String.Format(GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "PaymentPlanTermsApprovedText"), 
                        PlanDetails.ApprovalTimestamp.Value.Date.ToShortDateString(), 
                        PlanDetails.ApprovalTimestamp.Value.ToLocalTime().ToString("t"),
                        PlanDetails.ApprovalUserId);
                }
                return null;
            }
        }

        /// <summary>
        /// Model constructor
        /// </summary>
        public PaymentPlanAcknowledgementModel()
        {
            PlanDetails = new PaymentPlanDetailsModel();
        }

        /// <summary>
        /// Static method to build payment plan acknowledgement model
        /// </summary>
        /// <param name="planDetails">Payment Plan information</param>
        /// <param name="payControl">Payment control</param>
        /// <param name="returnUrl">Return URL for workflow</param>
        /// <returns>Payment plan acknowledgement model</returns>
        public static PaymentPlanAcknowledgementModel Build(PaymentPlanDetailsModel planDetails, RegistrationPaymentControl payControl, string returnUrl, ICurrentUser currentUser)
        {
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser");
            }
            if (planDetails == null || planDetails.PaymentPlan == null)
            {
                throw new ArgumentNullException("planDetails");
            }

            var model = new PaymentPlanAcknowledgementModel();

            // Base properties
            model.WorkflowStep = "PaymentAcknowledgement";
            model.PaymentControl = payControl;
            model.ReturnUrl = (String.IsNullOrEmpty(returnUrl)) ? null : returnUrl;
            model.IsAdminUser = currentUser.PersonId != planDetails.PaymentPlan.PersonId && !currentUser.ProxySubjects.Any();

            // Model-specific properties
            model.PlanDetails = planDetails;

            return model;
        }
    }
}