﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using System.Linq;
using System;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// Payment options model
    /// </summary>
    public class PaymentOptionsModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// Full payment amount
        /// </summary>
        public decimal FullPayment { get; set; }

        /// <summary>
        /// Minimum payment amount
        /// </summary>
        public decimal MinimumPayment { get; set; }

        /// <summary>
        /// Deferral percentage
        /// </summary>
        public decimal DeferralPercentage { get; set; }

        /// <summary>
        /// ID of the template used in creating a payment plan for the student
        /// </summary>
        public string PaymentPlanTemplateId { get; set; }

        /// <summary>
        /// List of valid payment methods
        /// </summary>
        public List<AvailablePaymentMethod> PaymentMethods { get; set; }

        /// <summary>
        /// Amount the student is paying
        /// </summary>
        public decimal PaymentAmount { get; set; }

        /// <summary>
        /// Payment method used for payment
        /// </summary>
        public AvailablePaymentMethod OptionsPaymentMethod { get; set; }

        /// <summary>
        /// The receivable type going on the payment plan
        /// </summary>
        public string PlanReceivableType { get; set; }

        /// <summary>
        /// Date on which first scheduled payment will be due for a payment plan
        /// </summary>
        public DateTime? PaymentPlanFirstDueDate { get; set; }

        /// <summary>
        /// Amount of the payment plan to be created
        /// </summary>
        public decimal PaymentPlanAmount { get; set; }

        /// <summary>
        /// Minimum amount of the payment plan down payment
        /// </summary>
        public decimal MinimumDownPaymentAmount { get; set; }

        /// <summary>
        /// Date on which the down payment is due
        /// </summary>
        public DateTime? DownPaymentDate { get; set; }

        /// <summary>
        /// List of charges, grouped by AR type
        /// </summary>
        public List<RegistrationChargeModel> Charges { get; set; }

        /// <summary>
        /// Details of proposed payment plan
        /// </summary>
        public PaymentPlanDetailsModel PlanDetails { get; set; }

        /// <summary>
        /// Indicator for acceptance of the payment plan terms and conditions
        /// </summary>
        public bool AcceptTermsAndConditions { get; set; }

        /// <summary>
        /// DTO for processing the plan acceptance
        /// </summary>
        public PaymentPlanTermsAcceptance Acceptance { get; set; }

        /// <summary>
        /// Actual amount of the down payment being made
        /// </summary>
        public decimal ActualDownPaymentAmount { get; set; }

        /// <summary>
        /// Payment method used for down payment
        /// </summary>
        public AvailablePaymentMethod PlanPaymentMethod { get; set; }

        /// <summary>
        /// Section of the form to process next
        /// </summary>
        public int SectionToProcess { get; set; }

        /// <summary>
        /// Model constructor
        /// </summary>
        public PaymentOptionsModel()
            : base()
        {
            PaymentMethods = new List<AvailablePaymentMethod>();
            Charges = new List<RegistrationChargeModel>();
            PlanDetails = new PaymentPlanDetailsModel();
            Acceptance = new PaymentPlanTermsAcceptance();
            SectionToProcess = 0;
        }

        /// <summary>
        /// Static method to build model for base payment options
        /// </summary>
        /// <param name="paymentOptions">The valid payment options for this student for this term</param>
        /// <param name="paymentMethods">The valid payment methods the student can use, if needed</param>
        /// <param name="regModel">Registration summary model</param>
        public static PaymentOptionsModel Build(ImmediatePaymentOptions paymentOptions, IEnumerable<AvailablePaymentMethod> paymentMethods, 
            RegistrationSummaryModel regModel)
        {
            var model = ImmediatePaymentWorkflowModel.Build(new PaymentOptionsModel(), regModel);
            model.WorkflowStep = "PaymentOptions";
            model.Charges.AddRange(regModel.Charges);
            model.PaymentMethods.AddRange(paymentMethods);

            model.FullPayment = paymentOptions.RegistrationBalance;
            model.MinimumPayment = paymentOptions.MinimumPayment;
            model.DeferralPercentage = paymentOptions.DeferralPercentage;
            model.PaymentPlanTemplateId = paymentOptions.PaymentPlanTemplateId;
            model.PaymentPlanFirstDueDate = paymentOptions.PaymentPlanFirstDueDate;
            model.PaymentPlanAmount = paymentOptions.PaymentPlanAmount;
            model.MinimumDownPaymentAmount = paymentOptions.DownPaymentAmount;
            model.DownPaymentDate = paymentOptions.DownPaymentDate;
            model.PlanReceivableType = paymentOptions.PaymentPlanReceivableTypeCode;

            return model;
        }

        /// <summary>
        /// Static method to build model with plan details
        /// </summary>
        /// <param name="optionsModel">Payment Options model</param>
        /// <param name="proposedPlan">Proposed payment plan</param>
        /// <param name="accountHolder">Account Holder</param>
        /// <param name="termsDoc">Terms and conditions document</param>
        /// <param name="ackDoc">Acknowledgement document</param>
        /// <returns>Payment options model</returns>
        public static PaymentOptionsModel BuildPlanDetails(PaymentOptionsModel optionsModel, PaymentPlanDetailsModel planDetails)
        {
            var model = optionsModel;

            model.PlanDetails = planDetails;

            model.Acceptance.StudentId = model.PlanDetails.PaymentPlan.PersonId;
            model.Acceptance.StudentName = model.PlanDetails.StudentName;
            model.Acceptance.PaymentControlId = model.PaymentControl.Id;
            model.Acceptance.AcknowledgementDateTime = DateTime.Now;
            model.Acceptance.ProposedPlan = model.PlanDetails.PaymentPlan;
            model.Acceptance.DownPaymentAmount = model.MinimumDownPaymentAmount;
            model.Acceptance.DownPaymentDate = model.DownPaymentDate;
            model.Acceptance.AcknowledgementText = model.PlanDetails.AcknowledgementText.ToList();
            model.Acceptance.TermsText = model.PlanDetails.TermsAndConditionsText.ToList();
            model.Acceptance.RegistrationApprovalId = model.PaymentControl.LastTermsApprovalId;

            return model;
        }

        /// <summary>
        /// Static method to build model with down payment options
        /// </summary>
        /// <param name="optionsModel">Payment Options model</param>
        /// <returns>Payment Options model</returns>
        public static PaymentOptionsModel BuildDownPayment(PaymentOptionsModel optionsModel, PaymentPlan approvedPlan)
        {
            var model = optionsModel;

            model.PlanDetails.PaymentPlan = approvedPlan;

            return model;
        }

        /// <summary>
        /// Static method to build the model when going straight to the down payment options
        /// </summary>
        /// <param name="paymentPlan">The original payment plan approved by the user</param>
        /// <param name="approval">The plan approval information</param>
        /// <param name="paymentMethods">Valid methods of payment</param>
        /// <param name="regModel">The registration model used on the previous form in the workflow</param>
        /// <param name="ackDoc">The acknowledgement document</param>
        /// <param name="termsDoc">The terms and conditions document</param>
        /// <param name="termsResponse">The response to the terms and conditions</param>
        /// <returns>Payment Options model</returns>
        public static PaymentOptionsModel BuildDownPayment(PaymentPlan paymentPlan, PaymentPlanApproval approval, 
            IEnumerable<AvailablePaymentMethod> paymentMethods, RegistrationSummaryModel regModel,
            ApprovalDocument ackDoc, ApprovalDocument termsDoc, ApprovalResponse termsResponse)
        {
            var model = ImmediatePaymentWorkflowModel.Build(new PaymentOptionsModel(), regModel);
            model.WorkflowStep = "PaymentOptions";
            model.Charges.AddRange(regModel.Charges);
            model.PaymentMethods.AddRange(paymentMethods);

            model.PaymentPlanTemplateId = paymentPlan.TemplateId;
            model.PaymentPlanFirstDueDate = paymentPlan.FirstDueDate;
            model.PaymentPlanAmount = paymentPlan.OriginalAmount;
            model.MinimumDownPaymentAmount = paymentPlan.DownPaymentAmount;
            model.DownPaymentDate = paymentPlan.DownPaymentDate;
            model.PlanReceivableType = paymentPlan.ReceivableTypeCode;

            model.Acceptance.StudentId = approval.StudentId;
            model.Acceptance.StudentName = approval.StudentName;
            model.Acceptance.PaymentControlId = regModel.PaymentControl.Id;
            model.Acceptance.AcknowledgementDateTime = approval.Timestamp;
            model.Acceptance.ProposedPlan = paymentPlan;
            model.Acceptance.DownPaymentAmount = approval.DownPaymentAmount;
            model.Acceptance.DownPaymentDate = approval.DownPaymentDate;
            model.Acceptance.AcknowledgementText = ackDoc.Text.ToList();
            model.Acceptance.TermsText = termsDoc.Text.ToList();
            model.Acceptance.RegistrationApprovalId = regModel.PaymentControl.LastTermsApprovalId;
            model.Acceptance.ApprovalUserId = termsResponse.UserId;
            model.Acceptance.ApprovalReceived = termsResponse.Received;

            model.PlanDetails = PaymentPlanDetailsModel.BuildDownPaymentModel(paymentPlan, approval, ackDoc, termsDoc, termsResponse);
            model.SectionToProcess = 3;

            return model;
        }
    }
}