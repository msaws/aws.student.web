﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// Payment complete model
    /// </summary>
    public class PaymentCompleteModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// Message to display
        /// </summary>
        public List<string> Message { get; set; }

        /// <summary>
        /// Label for button on form
        /// </summary>
        public string ButtonLabel { get; set; }

        /// <summary>
        /// Model constructor
        /// </summary>
        public PaymentCompleteModel()
            : base()
        {
            Message = new List<string>();
        }

        /// <summary>
        /// Static method to build payment completion model
        /// </summary>
        /// <param name="message">Message to display</param>
        /// <param name="buttonLabel">Text for button label</param>
        /// <param name="returnUrl">URL of next form to process</param>
        /// <returns></returns>
        public static PaymentCompleteModel Build(IEnumerable<string> message, string buttonLabel, string returnUrl)
        {
            var model = new PaymentCompleteModel();

            model.WorkflowStep = "PaymentComplete";

            if (message != null && message.Count() > 0)
            {
                model.Message.AddRange(message);
            }
            if (!String.IsNullOrEmpty(buttonLabel))
            {
                model.ButtonLabel = buttonLabel;
            }
            if (!String.IsNullOrEmpty(returnUrl))
            {
                model.ReturnUrl = returnUrl;
            }

            return model;
        }
    }
}