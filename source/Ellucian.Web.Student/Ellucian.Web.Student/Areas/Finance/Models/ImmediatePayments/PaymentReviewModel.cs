﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// Model for the Payment Review form
    /// </summary>
    public class PaymentReviewModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// Line items to display on the payment review form
        /// </summary>
        public List<PaymentReviewItem> LineItems { get; set; }

        /// <summary>
        /// The payment method selected by the user
        /// </summary>
        public AvailablePaymentMethod PaymentMethod { get; set; }

        /// <summary>
        /// The message to display before the line items; this may contain HTML markup
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The index (relative to 1) of the payment currently being processed
        /// </summary>
        public int PaymentNumber { get; set; }

        /// <summary>
        /// Total number of payments required
        /// </summary>
        public int PaymentCount { get; set; }

        /// <summary>
        /// Current payment to process
        /// </summary>
        public Payment Payment { get; set; }

        /// <summary>
        /// Model constructor
        /// </summary>
        public PaymentReviewModel()
        {
            LineItems = new List<PaymentReviewItem>();
            PaymentMethod = new AvailablePaymentMethod();
        }

        /// <summary>
        /// Static method to build model
        /// </summary>
        /// <param name="config">Student Finance configuration</param>
        /// <param name="receivableTypes">List of all receivable types</param>
        /// <param name="convenienceFees">List of all convenience fees</param>
        /// <param name="optionsModel">Payment options model</param>
        /// <param name="payments">List of all payments to process</param>
        /// <param name="paymentCount">Zero-based count of the payment to process now</param>
        /// <returns></returns>
        public static PaymentReviewModel Build(FinanceConfiguration config, IEnumerable<ReceivableType> receivableTypes, 
            IEnumerable<Ellucian.Colleague.Dtos.Base.ConvenienceFee> convenienceFees, PaymentOptionsModel optionsModel, IEnumerable<Payment> payments,
            int paymentCount)
        {
            var model = ImmediatePaymentWorkflowModel.Build(new PaymentReviewModel(), optionsModel);

            model.WorkflowStep = "PaymentReview";

            model.PaymentMethod = optionsModel.OptionsPaymentMethod;
            model.Message = config.PaymentReviewMessage;
            model.PaymentNumber = paymentCount + 1;
            model.PaymentCount = payments.Count();
            model.Payment = payments.ElementAt(paymentCount);

            // Add the return URL to the Payment so we can get it back later
            model.Payment.ReturnToOriginUrl = optionsModel.ReturnUrl;

            // If this is the last payment, make sure to flag the last payment item as complete
            if (model.PaymentNumber == model.PaymentCount)
            {
                model.Payment.PaymentItems[model.Payment.PaymentItems.Count - 1].PaymentComplete = true;
            }
            
            // Each line item on the payment gets a separate line
            foreach (var item in model.Payment.PaymentItems)
            {
                var lineItem = new PaymentReviewItem() 
                    {
                        Description = receivableTypes.First(x => x.Code == item.AccountType).Description,
                        Amount = item.PaymentAmount
                    };
                model.LineItems.Add(lineItem);
            }
            // If there's a convenience fee, add a line for it
            if (model.Payment.ConvenienceFeeAmount != null && model.Payment.ConvenienceFeeAmount.Value > 0)
            {
                var convFeeLine = new PaymentReviewItem()
                    {
                        Description = convenienceFees.FirstOrDefault(x => x.Code == model.Payment.ConvenienceFee).Description,
                        Amount = model.Payment.ConvenienceFeeAmount.Value
                    };
                model.LineItems.Add(convFeeLine);
            }

            // If there's more than one line item, then add a total line
            if (model.LineItems.Count >= 1)
            {
                var totalLine = new PaymentReviewItem()
                    {
                        Description = GlobalResources.GetString(FinanceResourceFiles.PaymentResources, "ReviewTotalPaymentDescription"),
                        Amount = model.LineItems.Sum(x => x.Amount)
                    };
                model.LineItems.Add(totalLine);
            }

            return model;
        }
    }
}