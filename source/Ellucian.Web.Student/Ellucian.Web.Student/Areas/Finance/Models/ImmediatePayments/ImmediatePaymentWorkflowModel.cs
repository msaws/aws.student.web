﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// The ImmediatePaymentWorkflowModel base class provides common information for other pages within a Immediate Payment workflow.
    /// </summary>
    public abstract class ImmediatePaymentWorkflowModel : StudentFinanceAdminModel
    {
        /// <summary>
        /// Title of the Immediate Payment workflow
        /// </summary>
        public string WorkflowTitle { get; set; }

        /// <summary>
        /// A notification message to display
        /// </summary>
        public Notification Notification { get; set; }

        /// <summary>
        /// Contains the current workflow step being processed
        /// </summary>
        public string WorkflowStep { get; set; }
        
        /// <summary>
        /// URL to which the workflow returns when complete
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Registration payment control being processed
        /// </summary>
        public RegistrationPaymentControl PaymentControl { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        protected ImmediatePaymentWorkflowModel()
        {
            WorkflowTitle = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PayForRegistrationTitle");
        }
        /// <summary>
        /// Convenience constructor to create a notification
        /// </summary>
        /// <param name="message">Message text</param>
        /// <param name="type">Message type</param>
        protected ImmediatePaymentWorkflowModel(string message, NotificationType type)
            : this()
        {
            Notification = new Notification(message, type);
        }

        /// <summary>
        /// Static method to build the base properties for a new workflow model from another model
        /// </summary>
        /// <typeparam name="T">Type of the new model being created</typeparam>
        /// <param name="newModel">The new model</param>
        /// <param name="oldModel">The existing model</param>
        /// <returns>A new model with the base properties populated</returns>
        protected static T Build<T>(T newModel, ImmediatePaymentWorkflowModel oldModel)
            where T: ImmediatePaymentWorkflowModel
        {
            newModel.PaymentControl = oldModel.PaymentControl;
            newModel.ReturnUrl = oldModel.ReturnUrl;

            return newModel;
        }

    }
}