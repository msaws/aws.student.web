﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Finance;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    public class PlanAcknowledgementModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// DTO for processing the plan acceptance
        /// </summary>
        public PaymentPlanTermsAcceptance Acceptance { get; set; }

        public PlanAcknowledgementModel()
            : base()
        {
            Acceptance = new PaymentPlanTermsAcceptance();
        }

       public static PlanAcknowledgementModel Build(PaymentOptionsModel optionsModel, PaymentPlanTermsAcceptance acceptance)
       {
           var model = ImmediatePaymentWorkflowModel.Build(new PlanAcknowledgementModel(), optionsModel);
           model.WorkflowStep = "PlanAcknowledgement";
           model.Acceptance = acceptance;

           return model;
       }
    }
}