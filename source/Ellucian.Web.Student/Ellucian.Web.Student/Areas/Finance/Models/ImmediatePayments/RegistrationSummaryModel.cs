﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// Model for the Registration Summary form
    /// </summary>
    public class RegistrationSummaryModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// Acceptance information recorded in system
        /// </summary>
        public PaymentTermsAcceptance2 Acceptance { get; set; }

        /// <summary>
        /// Have the terms and conditions been accepted?
        /// </summary>
        public bool AcceptTermsAndConditions { get; set; }

        /// <summary>
        /// List of registered sections in the billing term
        /// </summary>
        public List<SectionDisplayModel> RegisteredSections { get; set; }

        /// <summary>
        /// List of charges, grouped by AR type
        /// </summary>
        public List<RegistrationChargeModel> Charges { get; set; }

        /// <summary>
        /// Indicates whether view will be printed
        /// </summary>
        public bool IsPrintable { get; set; }

        /// <summary>
        /// Indicates whether the current user is an administrative user
        /// </summary>
        public bool IsAdminUser { get; set; }

        /// <summary>
        /// ID of the student (only used in admin mode)
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// Name of the student (only used in admin mode)
        /// </summary>
        public string PersonName { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        public RegistrationSummaryModel() : base()
        {
            Acceptance = new PaymentTermsAcceptance2();
            RegisteredSections = new List<SectionDisplayModel>();
            Charges = new List<RegistrationChargeModel>();
            IsPrintable = false;
            IsAdminUser = false;
        }

        /// <summary>
        /// Convenience constructor to build a notification
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public RegistrationSummaryModel(string message, NotificationType type)
            : base(message, type)
        {
        }

        /// <summary>
        /// Static method to build this model
        /// </summary>
        /// <param name="ipcControl">Immediate payment parameters</param>
        /// <param name="termUtility">Utility for processing terms</param>
        /// <param name="locations">List of all locations</param>
        /// <param name="buildings">List of all buildings</param>
        /// <param name="receivableTypes">List of all receivable types</param>
        /// <param name="returnUrl">URL to return to at end of workflow</param>
        /// <param name="rpc">Registration payment control</param>
        /// <param name="ackDoc">Acknowledgement document</param>
        /// <param name="sections">List of sections student is taking</param>
        /// <param name="courses">List of courses related to sections</param>
        /// <param name="faculty">List of all faculty teaching the student's sections</param>
        /// <param name="invoices">List of invoices for registration charges</param>
        /// <param name="distributionMap">Mapping of receivable types to distributions</param>
        /// <param name="termsDoc">Terms and conditions document</param>
        /// <returns>Registration summary model</returns>
        public static RegistrationSummaryModel Build(ImmediatePaymentControl ipcControl, TermPeriodsUtility termUtility, IEnumerable<Location> locations,
            IEnumerable<Building> buildings, IEnumerable<ReceivableType> receivableTypes, string returnUrl, RegistrationPaymentControl rpc, IEnumerable<string> ackDoc,
            IEnumerable<Section3> sections, IEnumerable<Course2> courses, IEnumerable<Faculty> faculty, IEnumerable<Invoice> invoices,
            Dictionary<string, string> distributionMap, IEnumerable<string> termsDoc)
        {
            var model = new RegistrationSummaryModel();

            model.WorkflowStep = "RegistrationSummary";
            model.PaymentControl = rpc;
            model.ReturnUrl = returnUrl;

            model.Acceptance.StudentId = rpc.StudentId;
            model.Acceptance.PaymentControlId = rpc.Id;
            model.Acceptance.AcknowledgementDateTime = DateTime.Now;
            model.Acceptance.AcknowledgementText = ackDoc.ToList();
            model.Acceptance.TermsText = termsDoc.ToList();
            model.Acceptance.SectionIds = rpc.RegisteredSectionIds.ToList();
            model.Acceptance.InvoiceIds = rpc.InvoiceIds.ToList();

            model.RegisteredSections = BuildSectionCollection(rpc.RegisteredSectionIds, sections, courses, faculty, termUtility, locations, buildings);
            model.Charges = BuildChargesSummary(invoices, distributionMap, receivableTypes, termUtility);

            return model;
        }

        /// <summary>
        /// Build a list of sections to display
        /// </summary>
        /// <param name="sectionIds">List of student's section IDs</param>
        /// <param name="sections">List of all sections student is taking</param>
        /// <param name="courses">List of all courses student is taking</param>
        /// <param name="faculty">List of all faculty teaching student's sections</param>
        /// <param name="termUtility">Terms processing utility</param>
        /// <param name="locations">List of all locations</param>
        /// <param name="buildings">List of all buildings</param>
        /// <returns>List of SectionDisplayModel items</returns>
        private static List<SectionDisplayModel> BuildSectionCollection(IEnumerable<string> sectionIds, IEnumerable<Section3> sections, IEnumerable<Course2> courses,
            IEnumerable<Faculty> faculty, TermPeriodsUtility termUtility, IEnumerable<Location> locations, IEnumerable<Building> buildings)
        {
            if (CollectionUtility.IsNullOrEmpty(sectionIds))
            {
                return new List<SectionDisplayModel>();
            }

            List<SectionDisplayModel> sectionCollection = new List<SectionDisplayModel>();
            string courseDelimiter = GlobalResources.GetString(GlobalResourceFiles.SharedConstants, "CourseDelimiter");
            foreach (var section in sections)
            {
                SectionDisplayModel sectionModel = new SectionDisplayModel();
                Course2 course = courses.Where(x => x.Id == section.CourseId).First();
                sectionModel.Section = course.SubjectCode + courseDelimiter + course.Number + courseDelimiter + section.Number;
                sectionModel.Title = section.Title;
                sectionModel.Term = termUtility.GetTermDescription(section.TermId);
                sectionModel.Credits = section.MinimumCredits;
                sectionModel.Ceus = section.Ceus;
                sectionModel.MeetingInformation = SectionUtility.GetMeetingInformation(section.Meetings);
                sectionModel.StartEndDate = SectionUtility.GetSectionDates(section);
                sectionModel.Faculty = GetFacultyNames(section.FacultyIds, faculty);
                if (!String.IsNullOrEmpty(section.Location))
                {
                    sectionModel.Location = locations.Where(x => x.Code == section.Location).First().Description;
                }
                sectionModel.Classrooms = BuildMeetingRooms(section.Meetings, buildings);
                sectionCollection.Add(sectionModel);
            }
            return sectionCollection;
        }

        /// <summary>
        /// Get the list of faculty names to display from their IDs
        /// </summary>
        /// <param name="facultyIds">List of faculty IDs to process</param>
        /// <param name="faculty">List of all faculty for this student</param>
        /// <returns>List of faculty names</returns>
        private static List<string> GetFacultyNames(IEnumerable<string> facultyIds, IEnumerable<Faculty> faculty)
        {
            List<string> facultyNames = new List<String>();

            // If no faculty have been assigned, return only the TBA description
            if (CollectionUtility.IsNullOrEmpty(facultyIds))
            {
                facultyNames.Add(SectionUtility.TBA);
            }
            else
            {
                foreach (string id in facultyIds)
                {
                    // For now, ignore any IDs for which there is no faculty found.  This shouldn't ever
                    // happen anyway, but this makes sure.
                    var fac = faculty.Where(x => x.Id == id).FirstOrDefault();
                    if (fac != null)
                    {
                        facultyNames.Add(NameHelper.FacultyDisplayName(fac));
                    }
                }
            }

            return facultyNames;
        }

        /// <summary>
        /// Build the list of meeting rooms for a section, displaying the full building name followed by the room number
        /// </summary>
        /// <param name="meetings">List of the meeting times for a section</param>
        /// <param name="buildings">List of all buildings</param>
        /// <returns>List of meeting rooms and times</returns>
        private static List<string> BuildMeetingRooms(IEnumerable<SectionMeeting2> meetings, IEnumerable<Building> buildings)
        {
            var meetingDescriptions = new List<string>();

            // If no meetings are defined, return the TBA string
            if (CollectionUtility.IsNullOrEmpty(meetings))
            {
                meetingDescriptions.Add(SectionUtility.TBA);
            }
            else
            {
                foreach (var meeting in meetings)
                {
                    if (String.IsNullOrEmpty(meeting.Room))
                    {
                        meetingDescriptions.Add(SectionUtility.TBA);
                    }
                    else
                    {
                        var buildingRoom = meeting.Room.Split('*');
                        var building = buildings.Where(x => x.Code == buildingRoom[0]).FirstOrDefault();
                        // If the building isn't found, return the TBA string
                        if (building == null)
                        {
                            meetingDescriptions.Add(SectionUtility.TBA);
                        }
                        else if (buildingRoom.Length < 2 || String.IsNullOrEmpty(buildingRoom[1]))
                        {
                            meetingDescriptions.Add(building.Description);
                        }
                        else
                        {
                            meetingDescriptions.Add(building.Description + " " + buildingRoom[1]);
                        }
                    }
                }
            }

            return meetingDescriptions;
        }

        /// <summary>
        /// Build the summary of charges to display on the acknowledgement
        /// </summary>
        /// <param name="invoices">List of registration invoices</param>
        /// <param name="distributionMap">Mapping of receivable types to distributions</param>
        /// <param name="receivableTypes">List of all receivable types</param>
        /// <param name="termUtility">Utility for processing terms</param>
        /// <returns>List of RegistrationChargeModel items</returns>
        private static List<RegistrationChargeModel> BuildChargesSummary(IEnumerable<Invoice> invoices, Dictionary<string, string> distributionMap,
            IEnumerable<ReceivableType> receivableTypes, TermPeriodsUtility termUtility)
        {
            var charges = new Dictionary<string, RegistrationChargeModel>();
            foreach (var inv in invoices)
            {
                if (charges.ContainsKey(inv.ReceivableTypeCode))
                {
                    // Update the existing entry for the AR type
                    charges[inv.ReceivableTypeCode].Amount += inv.Amount;
                    if (inv.DueDate.Date > charges[inv.ReceivableTypeCode].DueDate.Date)
                    {
                        charges[inv.ReceivableTypeCode].DueDate = inv.DueDate;
                    }
                }
                else
                {
                    // Add a new entry for the AR type
                    var model = new RegistrationChargeModel();
                    model.Description = receivableTypes.Where(x => x.Code == inv.ReceivableTypeCode).First().Description;
                    model.Distribution = distributionMap[inv.ReceivableTypeCode];
                    model.DueDate = inv.DueDate;
                    model.TermDescription = termUtility.GetTermDescription(inv.TermId);
                    model.Amount = inv.Amount;
                    charges.Add(inv.ReceivableTypeCode, model);
                }
            }

            return charges.Values.OrderBy(x => x.DueDate).ToList();
        }
    }
}