﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Payments;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// Payment acknowledgement model for immediate payment processing
    /// </summary>
    public class PaymentAcknowledgementModel : ImmediatePaymentWorkflowModel
    {
        /// <summary>
        /// Receipt reference number
        /// </summary>
        public string ReceiptNumber { get; set; }

        /// <summary>
        /// Date and time receipt was created
        /// </summary>
        public DateTime? ReceiptDate { get; set; }

        /// <summary>
        /// Merchant name and address
        /// </summary>
        public List<string> Merchant { get; set; }

        /// <summary>
        /// Payer name
        /// </summary>
        public string PayerName { get; set; }

        /// <summary>
        /// Payer ID
        /// </summary>
        public string PayerId { get; set; }

        /// <summary>
        /// Text of acknowledgement to display
        /// </summary>
        public List<string> AcknowledgementText { get; set; }

        /// <summary>
        /// Text for page footer
        /// </summary>
        public List<string> FooterText { get; set; }

        /// <summary>
        /// URL of image for page footer
        /// </summary>
        public Uri FooterImage { get; set; }

        /// <summary>
        /// Error message - returned when receipt was not created
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// List of payments on receivables accounts
        /// </summary>
        public List<AccountsReceivablePayment> Payments { get; set; }

        /// <summary>
        /// List of payments on deposit accounts
        /// </summary>
        public List<AccountsReceivableDeposit> Deposits { get; set; }

        /// <summary>
        /// List of general payments (no account involved)
        /// </summary>
        public List<GeneralPayment> OtherItems { get; set; }

        /// <summary>
        /// List of convenience fees charged to payer
        /// </summary>
        public List<ConvenienceFee> Fees { get; set; }

        /// <summary>
        /// List of payments tendered
        /// </summary>
        public List<PaymentMethod> PaymentsTendered { get; set; }

        /// <summary>
        /// ID of the associated transaction
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// ID of the cash receipt
        /// </summary>
        public string ReceiptId { get; set; }

        /// <summary>
        /// Amount of change returned on the cash receipt
        /// </summary>
        public decimal ChangeReturned { get; set; }

        /// <summary>
        /// Model constructor
        /// </summary>
        public PaymentAcknowledgementModel()
        {
            Merchant = new List<string>();
            AcknowledgementText = new List<string>();
            FooterText = new List<string>();
            Payments = new List<AccountsReceivablePayment>();
            Deposits = new List<AccountsReceivableDeposit>();
            OtherItems = new List<GeneralPayment>();
            Fees = new List<ConvenienceFee>();
            PaymentsTendered = new List<PaymentMethod>();
        }

        /// <summary>
        /// Static method to build payment acknowledgement model
        /// </summary>
        /// <param name="receipt">Receipt information</param>
        /// <param name="payControl">Payment control</param>
        /// <param name="returnUrl">Return URL for workflow</param>
        /// <returns>Payment acknowledgement model</returns>
        public static PaymentAcknowledgementModel Build(PaymentReceipt receipt, RegistrationPaymentControl payControl, string returnUrl)
        {
            var model = new PaymentAcknowledgementModel();

            // Base properties
            model.WorkflowStep = "PaymentAcknowledgement";
            model.PaymentControl = payControl;
            model.ReturnUrl = (String.IsNullOrEmpty(returnUrl)) ? receipt.ReturnUrl : returnUrl;

            // Model-specific properties
            model.ErrorMessage = receipt.ErrorMessage;
            model.ChangeReturned = receipt.ChangeReturned;
            model.Payments = new List<AccountsReceivablePayment>(receipt.Payments);
            model.Deposits = new List<AccountsReceivableDeposit>(receipt.Deposits);
            model.OtherItems = new List<GeneralPayment>(receipt.OtherItems);
            model.Fees = new List<ConvenienceFee>(receipt.ConvenienceFees);

            model.Merchant = new List<string>(receipt.MerchantNameAddress);
            model.Merchant.Add(" ");
            model.Merchant.Add(receipt.MerchantEmail);
            model.Merchant.Add(receipt.MerchantPhone);

            model.PayerName = receipt.ReceiptPayerName;
            model.PayerId = receipt.ReceiptPayerId;

            model.AcknowledgementText = new List<string>(receipt.ReceiptAcknowledgeText);
            model.FooterImage = receipt.AcknowledgeFooterImageUrl;
            model.FooterText = new List<string>(receipt.AcknowledgeFooterText);

            model.ReceiptDate = receipt.ReceiptDate;
            model.ReceiptNumber = receipt.ReceiptNo;
            model.PaymentsTendered = new List<PaymentMethod>(receipt.PaymentMethods);

            // Combine the receipt time with the receipt date
            if (receipt.ReceiptTime != null && model.ReceiptDate != null)
            {
                TimeSpan time = new TimeSpan(receipt.ReceiptTime.Value.Hour, receipt.ReceiptTime.Value.Minute, receipt.ReceiptTime.Value.Second);
                model.ReceiptDate = model.ReceiptDate.Value.Add(time);
            }

            return model;
        }
    }
}