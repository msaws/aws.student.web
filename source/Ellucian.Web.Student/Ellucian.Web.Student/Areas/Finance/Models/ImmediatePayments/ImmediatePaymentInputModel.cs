﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;

namespace Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments
{
    /// <summary>
    /// This model is passed from Student Planning to the Immediate Payment workflow.
    /// </summary>
    public class ImmediatePaymentInputModel
    {
        /// <summary>
        /// URL of the page from which the user came
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// ID of the current term on the form
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// An index of the tab/view selected (Schedule/Timeline)
        /// </summary>
        public string SelectedTab { get; set; }

        /// <summary>
        /// Indicates whether this is an administrative user
        /// </summary>
        public bool IsAdminUser { get; set; }

        /// <summary>
        /// The Payment Control to process
        /// </summary>
        public RegistrationPaymentControl PaymentControl { get; set; }
    }
}