﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;

namespace Ellucian.Web.Student.Areas.Finance.Models.RegistrationActivity
{
    /// <summary>
    /// Model for the Registration Summary form
    /// </summary>
    public class RegistrationActivityModel : StudentFinanceAdminModel
    {
        /// <summary>
        /// Message displayed to student
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The payment control selected for processing
        /// </summary>
        public RegistrationPaymentControl PaymentControl { get; set; }

        /// <summary>
        /// List of registered terms
        /// </summary>
        public List<RegistrationTermDisplayModel> RegistrationTerms { get; set; }

        /// <summary>
        /// Indicates whether Account Activity is configured to display activity by term
        /// </summary>
        public bool AccountActivityDisplayByTerm { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        public RegistrationActivityModel()
        {
            IsAdminUser = false;
            RegistrationTerms = new List<RegistrationTermDisplayModel>();
            AccountActivityDisplayByTerm = true;
        }

        /// <summary>
        /// Static method to build this model
        /// </summary>
        /// <param name="accountHolder">Accountholder info from the API</param>
        /// <param name="currentUser">The user currently logged into self service</param>
        /// <returns>Registration activity model</returns>
        public static RegistrationActivityModel Build(AccountHolder accountHolder, ICurrentUser currentUser)
        {
            if (accountHolder == null)
            {
                throw new ArgumentNullException("accountHolder", "Registration Activity model requires an account holder.");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser", "Registration Activity model requires authenticated user information.");
            }

            var model = new RegistrationActivityModel();
            model.PersonId = accountHolder.Id;
            model.PersonName = accountHolder.PreferredName;
            model.PrivacyStatusCode = accountHolder.PrivacyStatusCode;
            model.IsAdminUser = currentUser.PersonId != accountHolder.Id && !currentUser.ProxySubjects.Any();

            return model;
        }

        /// <summary>
        /// Static method to build this model
        /// </summary>
        /// <param name="paymentControls">Registration payment controls</param>
        /// <param name="termUtility">Utility for processing terms</param>
        /// <param name="displayByTerm">Flag indicating whether or not activity displays by term</param>
        /// <param name="accountHolder">Accountholder info from the API</param>
        /// <param name="currentUser">The user currently logged into self service</param>
        /// <returns>Registration activity model</returns>
        public static RegistrationActivityModel Build(IEnumerable<RegistrationPaymentControl> paymentControls, TermPeriodsUtility termUtility, bool displayByTerm, 
            AccountHolder accountHolder, ICurrentUser currentUser)
        {
            if (paymentControls == null || paymentControls.Count() == 0)
            {
                throw new ArgumentNullException("paymentControls", "Registration Activity model requires at least one IPC record.");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility", "Registration Activity model requires a term period utility.");
            }
            if (accountHolder == null)
            {
                throw new ArgumentNullException("accountHolder", "Registration Activity model requires an account holder.");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser", "Registration Activity model requires authenticated user information.");
            }

            var model = new RegistrationActivityModel();
            model.AccountActivityDisplayByTerm = displayByTerm;
            model.PersonId = accountHolder.Id;
            model.PersonName = accountHolder.PreferredName;
            model.PrivacyStatusCode = accountHolder.PrivacyStatusCode;
            model.IsAdminUser = currentUser.PersonId != accountHolder.Id && !currentUser.ProxySubjects.Any();

            paymentControls.ToList().ForEach(pc => model.RegistrationTerms.Add(RegistrationTermDisplayModel.Build(pc, termUtility, accountHolder.Id, displayByTerm, model.IsAdminUser)));
            return model;
        }
    }
}