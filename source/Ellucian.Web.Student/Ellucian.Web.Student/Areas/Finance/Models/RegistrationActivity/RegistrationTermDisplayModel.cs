﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Utility;
using System;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Areas.Finance.Models.RegistrationActivity
{
    /// <summary>
    /// Model for displaying section data
    /// </summary>
    public class RegistrationTermDisplayModel
    {
        /// <summary>
        /// Payment Control
        /// </summary>
        public RegistrationPaymentControl PaymentControl { get; set; }

        /// <summary>
        /// Billing term description for the registration record
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// Payment status for the term
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Financial Period for the term
        /// </summary>
        public FinancialPeriod FinancialPeriod { get; set; }

        /// <summary>
        /// Internal code for the Financial Period
        /// </summary>
        public string PeriodCode { get; set; }

        /// <summary>
        /// Flag indicating whether or not the term is complete
        /// </summary>
        public bool IsComplete { get { return PaymentControl != null && PaymentControl.PaymentStatus == RegistrationPaymentStatus.Complete; } }

        /// <summary>
        /// Text displayed for the link button
        /// </summary>
        public string LinkLabel { get; set; }

        /// <summary>
        /// Text displayed when no action is needed
        /// </summary>
        public string NoActionText { get; set; }

        /// <summary>
        /// Link to Account Activity for term
        /// </summary>
        public string AccountActivityUrlSuffix { get; set; }

        /// <summary>
        /// Flag indicating whether or not the payment plan link is visible
        /// </summary>
        public bool DisplayPaymentPlanLink { get; set; }

        /// <summary>
        /// Text displayed for action header
        /// </summary>
        public string ActionHeaderLabel { get; set; }

        /// <summary>
        /// CSS class for status
        /// </summary>
        public string StatusStyle { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="RegistrationTermDisplayModel"/> class.
        /// </summary>
        /// <param name="rpc">A registration payment control record</param>
        //public RegistrationTermDisplayModel(RegistrationPaymentControl rpc, TermPeriodsUtility termUtility, string personId, bool displayByTerm, bool isAdminUser)
        //{
        //    if (rpc == null)
        //    {
        //        throw new ArgumentNullException("rpc", "Registration term requires a payment control record.");
        //    }
        //    if (termUtility == null)
        //    {
        //        throw new ArgumentNullException("termUtility", "A term period utility is required to build the registration term model.");
        //    }
        //    if (string.IsNullOrEmpty(personId))
        //    {
        //        throw new ArgumentNullException("personId", "A person ID is required to build the registration term model.");
        //    }
        //    PaymentControl = rpc;
        //    Status = GetStatus(rpc.PaymentStatus, rpc.RegisteredSectionIds.Count());
        //    StatusStyle = GetStyle(rpc.PaymentStatus, rpc.RegisteredSectionIds.Count());
        //    TermDescription = termUtility.GetTermDescription(rpc.TermId);
        //    FinancialPeriod = !displayByTerm ? termUtility.GetPeriod(termUtility.GetTermPeriod(rpc.TermId)) : null;
        //    PeriodCode = !displayByTerm ? termUtility.GetPeriodCode(termUtility.GetTermPeriod(rpc.TermId)) : null;           
        //    NoActionText = isAdminUser && rpc.PaymentStatus == RegistrationPaymentStatus.New ? GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "RegistrationActivityAdminNoActionText") : string.Empty;
        //    DisplayPaymentPlanLink = rpc.PaymentStatus == RegistrationPaymentStatus.Complete && !string.IsNullOrEmpty(rpc.LastPlanApprovalId) && !string.IsNullOrEmpty(rpc.PaymentPlanId);
        //    LinkLabel = GetLinkLabel(rpc, isAdminUser);
        //    ActionHeaderLabel = DisplayPaymentPlanLink ? GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "ActionsLinkLabel") : GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "ActionLinkLabel");
        //    AccountActivityUrlSuffix = GetUrlSuffix(rpc, displayByTerm, isAdminUser, personId,
        //        displayByTerm ? rpc.TermId : PeriodCode,
        //        FinancialPeriod != null ? FinancialPeriod.End : null,
        //        FinancialPeriod != null ? FinancialPeriod.Start : null);   
        //}

        /// <summary>
        /// Creates a new instance of the <see cref="RegistrationTermDisplayModel"/> class.
        /// </summary>
        /// <param name="rpc">A registration payment control record</param>
        /// <param name="termUtility">Term period utility</param>
        /// <param name="personId">Id of the person</param>
        /// <param name="displayByTerm">Flag indicating whether or not Account Activity is displayed by term</param>
        /// <param name="isAdminUser">Flag indicating whether or not the user is an administrator</param>
        public static RegistrationTermDisplayModel Build(RegistrationPaymentControl rpc, TermPeriodsUtility termUtility, string personId, bool displayByTerm, bool isAdminUser)
        {
            if (rpc == null)
            {
                throw new ArgumentNullException("rpc", "Registration term requires a payment control record.");
            }
            if (termUtility == null)
            {
                throw new ArgumentNullException("termUtility", "A term period utility is required to build the registration term model.");
            }
            if (string.IsNullOrEmpty(personId))
            {
                throw new ArgumentNullException("personId", "A person ID is required to build the registration term model.");
            }
            bool displayPayPlanLink = rpc.PaymentStatus == RegistrationPaymentStatus.Complete && !string.IsNullOrEmpty(rpc.LastPlanApprovalId) && !string.IsNullOrEmpty(rpc.PaymentPlanId);
            string periodCode = !displayByTerm ? termUtility.GetPeriodCode(termUtility.GetTermPeriod(rpc.TermId)) : null;
            FinancialPeriod period = !displayByTerm ? termUtility.GetPeriod(termUtility.GetTermPeriod(rpc.TermId)) : null;

            RegistrationTermDisplayModel model = new RegistrationTermDisplayModel() {
            PaymentControl = rpc,
            Status = GetStatus(rpc.PaymentStatus, rpc.RegisteredSectionIds.Count()),
            StatusStyle = GetStyle(rpc.PaymentStatus, rpc.RegisteredSectionIds.Count()),
            TermDescription = termUtility.GetTermDescription(rpc.TermId),
            FinancialPeriod = period,
            PeriodCode = periodCode,          
            NoActionText = isAdminUser && rpc.PaymentStatus == RegistrationPaymentStatus.New ? GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "RegistrationActivityAdminNoActionText") : string.Empty,
            DisplayPaymentPlanLink = displayPayPlanLink,
            LinkLabel = GetLinkLabel(rpc, isAdminUser),
            ActionHeaderLabel = displayPayPlanLink ? GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "ActionsLinkLabel") : GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "ActionLinkLabel"),
            AccountActivityUrlSuffix = GetUrlSuffix(rpc, displayByTerm, isAdminUser, personId,
                displayByTerm ? rpc.TermId : periodCode,
                period != null ? period.End : null,
                period != null ? period.Start : null)  
            };

            return model;
        }


        /// <summary>
        /// Get the link label for the term
        /// </summary>
        /// <param name="isAdmin">Flag indicating if user is an administrator</param>
        /// <param name="control">Registration Payment Control record</param>
        /// <returns></returns>
        public static string GetLinkLabel(RegistrationPaymentControl control, bool isAdmin)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control", "URL suffix calculations require a payment control record.");
            }

            string label = string.Empty;

            if (control.RegisteredSectionIds != null && control.RegisteredSectionIds.Any())
            {
                if (control.PaymentStatus == RegistrationPaymentStatus.Complete)
                {
                    // Registration complete - View registration summary
                    label = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "ViewRegistrationSummaryLabel");
                }
                else if (!isAdmin)
                {
                    // Registration not complete, Student user - Pay for registration
                    label = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "PayForRegistrationTitle");
                }
                else
                {
                    // Registration not complete, Admin user, not New status - View registration summary
                    label = GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "ViewRegistrationSummaryLabel");
                }
            }

            // No active registered classes - Go to Account Activity
            else
            {
                label = GlobalResources.GetString(FinanceResourceFiles.FinanceResources, "GoToAccountActivityLabel");
            }
            return label;
        }

        /// <summary>
        /// Gets the AA URL suffix
        /// </summary>
        /// <param name="control">Registration Payment Control record</param>
        /// <param name="displayByTerm">Flag indicating if AA is displayed by term</param>
        /// <param name="isAdmin">Flag indicating if user is an administrator</param>
        /// <param name="timeframeId">Term or Period Code</param>
        /// <param name="end">End Date for Period</param>
        /// <param name="start">Start Date for Period</param>
        /// <returns>AA URL Suffix</returns>
        public static string GetUrlSuffix(RegistrationPaymentControl control, bool displayByTerm, bool isAdmin, string personId, string timeframeId,
            DateTime? end = null, DateTime? start = null)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control", "URL suffix calculations require a payment control record.");
            }

            if (!displayByTerm && !end.HasValue && !start.HasValue)
            {
                throw new ArgumentException("When displaying by period, a start or end date or both is required.");
            }

            string suffix = string.Empty;
            if (!string.IsNullOrEmpty(personId) && isAdmin)
            {
                suffix += "/" + personId;
            }

            suffix += "?timeframeId=" + timeframeId;

            if (!displayByTerm)
            {
                if (timeframeId == "PAST")
                {
                    suffix += "&EndDate=" + end.Value.ToShortDateString();
                }
                else if (timeframeId == "CUR")
                {
                    suffix += "&StartDate=" + start.Value.ToShortDateString() + "&EndDate=" + end.Value.ToShortDateString();
                }
                else
                {
                    suffix += "&StartDate=" + start.Value.ToShortDateString();
                }
            }

            return suffix;
        }

        /// <summary>
        /// Get the CSS class to be applied to the status column
        /// </summary>
        /// <param name="status">Payment Control status</param>
        /// <param name="courseCount">Number of registered sections</param>
        /// <returns>CSS class</returns>
        public static string GetStyle(RegistrationPaymentStatus status, int courseCount)
        {
            if (courseCount > 0)
            {
                if (status == RegistrationPaymentStatus.Complete)
                {
                    return "requirementMet";
                }
                else
                {
                    return "requirementNotMet";
                }
            }
            else
            {
                return "noActiveClasses";
            }
        }

        public static string GetStatus(RegistrationPaymentStatus status, int courseCount)
        {
            if (courseCount > 0)
            {
                if (status == RegistrationPaymentStatus.Complete)
                {
                    return GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentRequirementsMetText");
                }
                else
                {
                    return GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "PaymentRequirementsNotMetText");
                }
            }
            else
            {
                return GlobalResources.GetString(FinanceResourceFiles.ImmediatePaymentResources, "NoActiveClassesText");
            }
        }
    }
}
