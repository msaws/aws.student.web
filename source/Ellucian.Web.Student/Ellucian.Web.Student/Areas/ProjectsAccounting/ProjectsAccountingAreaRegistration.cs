﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.using System;

using System.ComponentModel;
using System.Web.Mvc;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Student.Licensing;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting
{
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ProjectsAccounting)]
    public class ProjectsAccountingAreaRegistration : LicenseAreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ProjectsAccounting";
            }
        }

        public override void LicensedRegistration(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ProjectsAccounting_default",
                "ProjectsAccounting/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional, controller = "Home" },
                new string[] { "Ellucian.Web.Student.Areas.ProjectsAccounting.Controllers" }
            );

            context.MapRoute(
                name: "DefaultProjectGlAccount",
                url: "ProjectsAccounting/Home/{action}/{id}/{generalLedgerAccountId}/{projectId}",
                defaults: new { action = "Index", id = string.Empty, generalLedgerAccountId = string.Empty, projectId = string.Empty, controller = "Home" },
                namespaces: new string[] { "Ellucian.Web.Student.Areas.ProjectsAccounting.Controllers" }
            );
        }
    }
}