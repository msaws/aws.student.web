﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
namespace Ellucian.Web.Student.Areas.ProjectsAccounting
{
    public static class ProjectsAccountingResourceFiles
    {
        public static readonly string ProjectsAccountingResources = "ProjectsAccounting";
    }
}