﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Controllers;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using slf4net;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class HomeController : BaseStudentController
    {
        /// <summary>
        /// Creates a new HomeController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public HomeController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        /// <summary>
        /// Returns the default Index view for Projects Accounting, which shows the logged-in projects accounting user's list of projects (if any).
        /// </summary>
        /// <returns>The projects accounting user's list Index view.</returns>
        [LinkHelp]
        [PageAuthorize("myProjects")]
        public ActionResult Index()
        {
            // Set page title.
            ViewBag.Title = "Projects Accounting";
            ViewBag.CssAfterOverride = new List<string>();

            return View();
        }

        #region Projects
        /// <summary>
        /// Action method for the Project Line Items page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("projectLineItems")]
        public ActionResult ProjectLineItems()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Projects Accounting";
            ViewBag.CssAfterOverride = new List<string>();
            ViewBag.SubMenuOverride = "Index";

            return View();
        }

        /// <summary>
        /// Method to be invoked asynchronously to get all projects available and all user-defined project types.
        /// </summary>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="filter">Filter criteria used to reduce the number of projects returned.</param>
        /// <returns>Projects in JSON format</returns>
        public async Task<JsonResult> GetProjectsAsync(bool summaryOnly = true, string[][] filter = null)
        {
            try
            {
                // Initialize the summary view model to hold all the data
                var projectSummaryViewModel = new ProjectSummaryViewModel();

                // Create a list of UrlFilter objects from the URL parameters
                var urlFilters = ConvertFilterIntoUrlFilter(filter);

                var projects = await ServiceClient.GetProjectsAsync(summaryOnly, urlFilters);
                projects = projects.OrderBy(x => x.Number);
                foreach (var project in projects)
                {
                    projectSummaryViewModel.Projects.Add(new ProjectViewModel(project));
                }

                // Populate all project statuses in the view model
                projectSummaryViewModel.ProjectStatuses.Add(new ProjectStatusViewModel(ProjectStatus.Active));
                projectSummaryViewModel.ProjectStatuses.Add(new ProjectStatusViewModel(ProjectStatus.Inactive));
                projectSummaryViewModel.ProjectStatuses.Add(new ProjectStatusViewModel(ProjectStatus.Closed));

                // Get all project types (for the filter) and add them to the summary view model
                // and sort them by Description.
                var projectTypes = await ServiceClient.GetProjectTypesAsync();
                projectTypes = projectTypes.OrderBy(x => x.Description);
                foreach (var type in projectTypes)
                {
                    projectSummaryViewModel.ProjectTypes.Add(new ProjectTypesViewModel(type));
                }

                return Json(projectSummaryViewModel, JsonRequestBehavior.AllowGet);
            }
            catch (HttpRequestFailedException hrfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(hrfe.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(hrfe.Message);
            }
            catch (ResourceNotFoundException rnfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(rnfe.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(rnfe.Message);
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                Logger.Error(e.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message);
            }
        }

        /// <summary>
        /// Retrieve a single project along with its line items.
        /// </summary>
        /// <param name="id">ID of the project requested.</param>
        /// <param name="sequenceNumber">Sequence number to return project totals for a single budget period.</param>
        /// <returns>Project and line items in JSON format.</returns>
        public async Task<JsonResult> GetProjectLineItemsAsync(string id, string sequenceNumber)
        {
            try
            {
                // Get the project and initialize the detail view model.
                var project = await ServiceClient.GetProjectAsync(id, sequenceNumber);
                var projectDetailViewModel = new ProjectDetailViewModel(project);

                // Get all project item codes and populate the descriptions on the line items.
                var projectItemCodes = await ServiceClient.GetProjectItemCodesAsync();

                foreach (var itemCode in projectItemCodes)
                {
                    foreach (var assetLineItem in projectDetailViewModel.Project.AssetLineItems)
                    {
                        if (itemCode.Code == assetLineItem.ItemCode)
                        {
                            assetLineItem.ItemCodeDescription = itemCode.Description;
                        }
                    }

                    foreach (var liabilityLineItem in projectDetailViewModel.Project.LiabilityLineItems)
                    {
                        if (itemCode.Code == liabilityLineItem.ItemCode)
                        {
                            liabilityLineItem.ItemCodeDescription = itemCode.Description;
                        }
                    }

                    foreach (var fundBalanceLineItem in projectDetailViewModel.Project.FundBalanceLineItems)
                    {
                        if (itemCode.Code == fundBalanceLineItem.ItemCode)
                        {
                            fundBalanceLineItem.ItemCodeDescription = itemCode.Description;
                        }
                    }

                    foreach (var revenueLineItem in projectDetailViewModel.Project.RevenueLineItems)
                    {
                        if (itemCode.Code == revenueLineItem.ItemCode)
                        {
                            revenueLineItem.ItemCodeDescription = itemCode.Description;
                        }
                    }

                    foreach (var expenseLineItem in projectDetailViewModel.Project.ExpenseLineItems)
                    {
                        if (itemCode.Code == expenseLineItem.ItemCode)
                        {
                            expenseLineItem.ItemCodeDescription = itemCode.Description;
                        }
                    }
                }

                return Json(projectDetailViewModel, JsonRequestBehavior.AllowGet);
            }
            catch (HttpRequestFailedException hrfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(hrfe.Message);
                throw new HttpException((int)hrfe.StatusCode, hrfe.Message);
            }
            catch (ResourceNotFoundException rnfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(rnfe.Message);
                throw new HttpException((int)rnfe.StatusCode, rnfe.Message);
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                Logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
        }
        #endregion

        #region Document actions
        /// <summary>
        /// Action method for the Voucher View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceVoucher")]
        public ActionResult Voucher()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Voucher";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/Voucher.cshtml");
        }

        /// <summary>
        /// Action method for the Recurring Voucher View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceRecurringVoucher")]
        public ActionResult RecurringVoucher()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Recurring Voucher";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/RecurringVoucher.cshtml");
        }

        /// <summary>
        /// Action method for the Purchase Order View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinancePurchaseOrder")]
        public ActionResult PurchaseOrder()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Purchase Order";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/PurchaseOrder.cshtml");
        }

        /// <summary>
        /// Action method for the Blanket Purchase Order View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceBlanketPurchaseOrder")]
        public ActionResult BlanketPurchaseOrder()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Blanket Purchase Order";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/BlanketPurchaseOrder.cshtml");
        }

        /// <summary>
        /// Action method for the Requisition View page of Colleague Finance.
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceRequisition")]
        public ActionResult Requisition()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Requisition";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/Requisition.cshtml");
        }

        /// <summary>
        /// Action method for the Journal Entry View page of Colleague Finance
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("ColleagueFinanceJournalEntry")]
        public ActionResult JournalEntry()
        {
            // Set page title and get CSS file.
            ViewBag.Title = "Journal Entry";
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Areas/ColleagueFinance/Views/Home/JournalEntry.cshtml");
        }
        #endregion

        #region Document async methods
        /// <summary>
        /// Asynchronously get a Voucher object.
        /// </summary>
        /// <param name="voucherId">Voucher ID</param>
        /// <returns>Voucher in JSON format</returns>
        public async Task<JsonResult> GetVoucherAsync(string voucherId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetVoucherAsync(voucherId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Recurring Voucher object.
        /// </summary>
        /// <param name="recurringVoucherId">Recurring Voucher ID</param>
        /// <returns>Recurring Voucher in JSON format</returns>
        public async Task<JsonResult> GetRecurringVoucherAsync(string recurringVoucherId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetRecurringVoucherAsync(recurringVoucherId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a PurchaseOrder object.
        /// </summary>
        /// <param name="purchaseOrderId">PurchaseOrder ID</param>
        /// <returns>Purchase Order in JSON format</returns>
        public async Task<JsonResult> GetPurchaseOrderAsync(string purchaseOrderId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetPurchaseOrderAsync(purchaseOrderId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a BlanketPurchaseOrder object.
        /// </summary>
        /// <param name="blanketPurchaseOrderId">BlanketPurchaseOrder ID</param>
        /// <returns>BlanketPurchase Order in JSON format</returns>
        public async Task<JsonResult> GetBlanketPurchaseOrderAsync(string blanketPurchaseOrderId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetBlanketPurchaseOrderAsync(blanketPurchaseOrderId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Requisition object.
        /// </summary>
        /// <param name="purchaseOrderId">Requisition ID</param>
        /// <returns>Requisition in JSON format</returns>
        public async Task<JsonResult> GetRequisitionAsync(string requisitionId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetRequisitionAsync(requisitionId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Asynchronously get a Journal Entry object
        /// </summary>
        /// <param name="journalEntryId">Journal Entry ID</param>
        /// <returns>Journal Entry in JSON format</returns>
        public async Task<JsonResult> GetJournalEntryAsync(string journalEntryId, string generalLedgerAccountId)
        {
            return Json(await DocumentUtility.GetJournalEntryAsync(journalEntryId, generalLedgerAccountId, ServiceClient, Logger), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Default view preference
        /// <summary>
        /// Stores the Current User's Projects Accounting Summary UI choice.
        /// </summary>
        /// <param name="preferredUi">boolean: true means bar graph view; false means list view</param>
        /// <returns>The stored preference</returns>
        [HttpPost]
        [PageAuthorize("myProjects")]
        public async Task<JsonResult> SetUIPreference(bool preferredUi)
        {
            var isBarGraph = true;
            try
            {
                isBarGraph = await ColleagueFinanceUtility.SetUIPreference(ServiceClient, preferredUi, CurrentUser.PersonId, true);
            }
            catch (Exception e)
            {
                // Log the error and send back a generic HTTP status code
                Logger.Error(e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, e.Message);
            }
            return new JsonResult() { Data = isBarGraph, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Retreives the Current User's UI preference for the Projects Accounting Summary page
        /// </summary>
        /// <returns>boolean - true if bar graph view is chosen; false if list view is chosen</returns>
        [PageAuthorize("myProjects")]
        public async Task<JsonResult> GetUIPreference()
        {
            var isBarGraph = true;

            try
            {
                isBarGraph = Convert.ToBoolean((await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, "pa-graph-ui")).Preferences["bar-graph"]);
            }
            catch (Exception)
            {
                // Unable to retreive the UI preference (may not exist in DB yet) - instead of throwing an error, just use the default (true)
            }

            return new JsonResult() { Data = isBarGraph, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        #endregion
    }
}
