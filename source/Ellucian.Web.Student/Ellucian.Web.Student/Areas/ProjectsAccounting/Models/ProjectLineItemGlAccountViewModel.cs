﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    public class ProjectLineItemGlAccountViewModel
    {
        /// <summary>
        /// GL Account Number
        /// </summary>
        public string GlAccount { get; set; }

        /// <summary>
        /// Formatted GL account
        /// </summary>
        public string FormattedGlAccount { get; set; }

        /// <summary>
        /// GL Account Description
        /// </summary>
        public string Description { get; set; }

        private decimal actuals;

        /// <summary>
        /// Actuals for this GL number
        /// </summary>
        public string Actuals
        {
            get
            {
                return actuals.ToString("C");
            }
        }

        /// <summary>
        /// Actuals (sign-inverted) for this GL number
        /// </summary>
        public string NegativeActuals
        {
            get
            {
                return (actuals * -1).ToString("C");
            }
        }

        /// <summary>
        /// Private decimal encumbrance amount
        /// </summary>
        private decimal encumbrances;

        /// <summary>
        /// Encumbrances for this GL number
        /// </summary>
        public string Encumbrances
        {
            get
            {
                return encumbrances.ToString("C");
            }
        }

        /// <summary>
        /// Encumbrances (sign-inverted) for this GL number
        /// </summary>
        public string NegativeEncumbrances
        {
            get
            {
                return (encumbrances * -1).ToString("C");
            }
        }

        /// <summary>
        /// Display an output string for both actuals and encumbrances.
        /// </summary>
        public string ActualsAndEncumbrances
        {
            get
            {
                return (actuals + encumbrances).ToString("C");
            }
        }

        /// <summary>
        /// List of actual transactions.
        /// </summary>
        public List<GlTransactionViewModel> ActualTransactions;

        /// <summary>
        /// List of encumbrance transactions.
        /// </summary>
        public List<GlTransactionViewModel> EncumbranceTransactions;

        /// <summary>
        /// Initialize the attributes of the detail view model.
        /// </summary>
        /// <param name="projectDto">Project DTO</param>
        public ProjectLineItemGlAccountViewModel(ProjectLineItemGlAccount glAccount, GlClass glClass)
        {
            this.GlAccount = glAccount.GlAccount;
            this.FormattedGlAccount = glAccount.FormattedGlAccount;
            this.Description = glAccount.Description ?? "";
            // Limit the GL description to 60.
            if (this.Description.Length > 60)
            {
                this.Description = this.Description.Substring(0, 60);
            }
            this.actuals = glAccount.Actuals;
            this.encumbrances = glAccount.Encumbrances;

            if(glClass == GlClass.FundBalance || glClass == GlClass.Liability || glClass == GlClass.Revenue)
            {
                this.actuals *= -1;
                this.encumbrances *= -1;
            }

            this.ActualTransactions = new List<GlTransactionViewModel>();
            this.EncumbranceTransactions = new List<GlTransactionViewModel>();

            // Add the actuals and encumbrance transactions to the VM
            if (glAccount.ActualsGlTransactions != null)
            {
                foreach (var actualTransaction in glAccount.ActualsGlTransactions)
                {
                    this.ActualTransactions.Add(new GlTransactionViewModel(actualTransaction));
                }
            }

            if (glAccount.EncumbranceGlTransactions != null)
            {
                foreach (var encumbranceTransaction in glAccount.EncumbranceGlTransactions)
                {
                    this.EncumbranceTransactions.Add(new GlTransactionViewModel(encumbranceTransaction));
                }
            }
        }
    }
}