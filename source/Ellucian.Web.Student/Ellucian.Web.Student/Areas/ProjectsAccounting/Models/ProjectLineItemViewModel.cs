﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    public class ProjectLineItemViewModel
    {
        /// <summary>
        /// Item code for the project line item
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// Item code description for the project line item
        /// </summary>
        public string ItemCodeDescription { get; set; }

        /// <summary>
        /// List of GL Account objects
        /// </summary>
        public List<ProjectLineItemGlAccountViewModel> GlAccounts { get; set; }

        /// <summary>
        /// GL class enumerable for this project line item
        /// </summary>
        public GlClass GlClass { get; private set; }

        /// <summary>
        /// Private member used in calculations and to obtain a string representation of the total budget
        /// </summary>
        private decimal totalBudget;

        public string TotalBudget
        {
            get
            {
                if (totalBudget == 0 && (GlClass == GlClass.Asset || GlClass == GlClass.Liability || GlClass == GlClass.FundBalance))
                {
                    return string.Empty;
                }

                return totalBudget.ToString("C");
            }
        }

        /// <summary>
        /// Private member used to obtain a string representation of the total actuals
        /// </summary>
        private decimal totalActuals;

        /// <summary>
        /// Total actuals amount for this line item.
        /// This total actuals include both memos and posted actuals.
        /// </summary>
        public string TotalActuals
        {
            get
            {
                return totalActuals.ToString("C");
            }
        }

        /// <summary>
        /// Private member used to obtain a string representation of the total memo actuals
        /// </summary>
        private decimal totalMemoActuals;

        /// <summary>
        /// Total actuals memos amount for this line item
        /// </summary>
        public string TotalMemoActuals
        {
            get
            {
                return totalMemoActuals.ToString("C");
            }
        }

        /// <summary>
        /// Private member used to obtain a string representation of the total memo encumbrances
        /// </summary>
        private decimal totalEncumbrances;

        /// <summary>
        /// Total encumbrances amount for this line item
        /// This total encumbrances include both memos and posted encumbrances.
        /// </summary>
        public string TotalEncumbrances
        {
            get
            {
                return totalEncumbrances.ToString("C");
            }
        }

        /// <summary>
        /// Private member used to obtain a string representation of the total memo encumbrances
        /// </summary>
        private decimal totalMemoEncumbrances;

        /// <summary>
        /// Total encumbrances amount for this line item
        /// </summary>
        public string TotalMemoEncumbrances
        {
            get
            {
                return totalMemoEncumbrances.ToString("C");
            }
        }

        /// <summary>
        /// Private decimal that combines actuals and encumbrances - used in some calculations
        /// </summary>
        private decimal totalExpenses
        {
            get
            {
                return totalActuals + totalEncumbrances;
            }
        }

        /// <summary>
        /// Return the total expenses amount (which is actuals + encumbrances)
        /// </summary>
        public string TotalExpenses
        {
            get
            {
                return (totalActuals + totalEncumbrances).ToString("C");
            }
        }

        /// <summary>
        /// Return the total decimal memo amount (which is memo actuals + memo encumbrances)
        /// to check whether to display the Amount Pending Posting row.
        /// </summary>
        public decimal TotalMemos
        {
            get
            {
                return totalMemoActuals + totalMemoEncumbrances;
            }
        }

        /// <summary>
        /// Return the total memo amount in formatted mode for display.
        /// </summary>
        public string TotalMemosDisplay
        {
            get
            {
                return TotalMemos.ToString("C");
            }
        }

        /// <summary>
        /// This property returns the remaining budget amount.
        /// </summary>
        public decimal BudgetRemaining
        {
            get
            {
                return totalBudget - totalExpenses;
            }
        }

        /// <summary>
        /// Return the remaining budget formatted for display
        /// </summary>
        public string BudgetRemainingFormatted
        {
            get
            {
                if (this.GlClass == GlClass.Expense || this.GlClass == GlClass.Revenue)
                {
                    return (BudgetRemaining).ToString("C");
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// This property returns a string representation of the remaining budget amount.
        /// </summary>
        public string BudgetRemainingString
        {
            get
            {
                string remainingString = string.Empty;
                if (this.GlClass == GlClass.Expense || this.GlClass == GlClass.Revenue)
                {
                    if (BudgetRemaining >= 0)
                    {
                        remainingString = BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText");
                    }
                    else
                    {
                        remainingString = BudgetRemaining.ToString("C");
                    }
                }

                return remainingString;
            }
        }

        /// <summary>
        /// Removes the negative sign from the remaining budget so it can be properly
        /// read by the screen reader.
        /// </summary>
        public string BudgetRemainingForScreenReader
        {
            get
            {
                var remaining = "";
                if (this.GlClass == GlClass.Expense || this.GlClass == GlClass.Revenue)
                {
                    if (BudgetRemaining < 0)
                    {
                        remaining = Math.Abs(BudgetRemaining).ToString("C");
                    }
                }

                return remaining;
            }
        }

        /// <summary>
        /// Calculate the percent the bar graph is filled from the left side of the bar graph. For example
        /// if a project is 25% over budget, this property would calculate a 75% fill from the left. The
        /// remaining 25% is calculated by the Overage property.
        /// </summary>
        public decimal BudgetSpentPercent
        {
            get
            {
                decimal budgetSpentPercent = 0;

                if (totalBudget == 0 && totalExpenses == 0)
                // If Budget is $0 and expenses are $0 return a 0 percentage
                {
                    budgetSpentPercent = 0;
                }
                else if (totalExpenses < 0)
                {
                    // Total credit expenses return a zero percentage.
                    budgetSpentPercent = 0;
                }

                else if (totalExpenses > totalBudget)
                // If expenses are greater than budget return an overspent percentage
                // If expenses are greater than the budget, return a zero percentage if there is zero budget
                // Return the 100 percent minus the overage percent when the budget is greater than zero
                {
                    if (totalBudget == 0)
                    {
                        budgetSpentPercent = 0;
                    }
                    else
                    {
                        budgetSpentPercent = Math.Round(100 - Overage);
                    }
                }
                else
                // Calculate the return percentage
                {
                    budgetSpentPercent = Math.Round(((totalExpenses / totalBudget) * 100));
                }
                return budgetSpentPercent;
            }
        }

        /// <summary>
        /// Calculate the percentage the project is over budget. The value returned is used to fill the
        /// bar graph from the right side. For example, if a project is 25% over budget, this property
        /// would calculate a 25% fill from the right. The remaining 75% is calculated by the
        /// BudgetSpentPercent property.
        /// </summary>
        public decimal Overage
        {
            get
            {
                decimal overage = 0;
                if (totalExpenses > totalBudget)
                {
                    if (totalBudget > 0)
                    {
                        overage = Math.Round((((totalExpenses - totalBudget) / totalBudget) * 100), 2);

                        if (overage > 100)
                        {
                            overage = 100;
                        }
                    }
                    else
                    {
                        overage = 100;
                    }
                }

                return overage;
            }
        }

        /// <summary>
        /// A representation of the amount processed divided by the amount budgeted (in percent format).
        /// </summary>
        public string PercentProcessed
        {
            get
            {
                if (this.GlClass == GlClass.Expense || this.GlClass == GlClass.Revenue)
                {
                    var percent = 0m;

                    if (this.totalBudget == 0)
                    {
                        if ((this.totalExpenses) > 0)
                        {
                            percent = 101;
                        }
                        else if ((this.totalExpenses) < 0)
                        {
                            percent = -101;
                        }
                    }
                    else
                    {
                        percent = Math.Round(((this.totalExpenses) / this.totalBudget) * 100m);
                    }

                    return string.Format("{0:n0}", percent) + " %";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Return the text string stating the financial health of the project line item.
        /// </summary>
        public string FinancialHealthTextForScreenReader
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    var financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
                    if (BudgetRemaining < 0)
                    {
                        financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
                    }
                    else if (BudgetSpentPercent >= 85 && BudgetSpentPercent <= 100)
                    {
                        financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
                    }

                    return financialHealthText;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string FinancialHealthIndicatorClass
        {
            get
            {
                if (this.GlClass == GlClass.Expense)
                {
                    var financialHealthIcon = "group-icon-green";
                    if (this.BudgetRemaining < 0)
                    {
                        financialHealthIcon = "group-icon-red";
                    }
                    else if (this.BudgetSpentPercent >= 85 && this.BudgetSpentPercent <= 100)
                    {
                        financialHealthIcon = "group-icon-yellow";
                    }

                    return financialHealthIcon;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// The style for color coding certain columns
        /// </summary>
        public string ColorCodeStyle
        {
            get
            {
                var colorStyle = "remaining-black";
                if (this.GlClass == GlClass.Expense)
                {
                    if (this.totalExpenses > this.totalBudget)
                    {
                        colorStyle = "remaining-red";
                    }
                }
                else if (this.GlClass == GlClass.Revenue)
                {
                    if (this.totalExpenses > this.totalBudget)
                    {
                        colorStyle = "remaining-green";
                    }
                }

                return colorStyle;
            }
        }

        /// <summary>
        /// Initializes the object using DTO argument.
        /// </summary>
        /// <param name="projectDto">A Project DTO</param>
        public ProjectLineItemViewModel(ProjectLineItem projectLineItemDto)
        {
            if (projectLineItemDto != null)
            {
                this.ItemCode = projectLineItemDto.ItemCode;
                this.ItemCodeDescription = string.Empty;
                this.totalBudget = projectLineItemDto.TotalBudget;
                this.totalActuals = projectLineItemDto.TotalActuals;
                this.totalMemoActuals = projectLineItemDto.TotalMemoActuals;
                this.totalEncumbrances = projectLineItemDto.TotalEncumbrances;
                this.totalMemoEncumbrances = projectLineItemDto.TotalMemoEncumbrances;
                this.GlClass = projectLineItemDto.GlClass;

                // Invert values for Liability, Fund Balance, and Revenue GL classes
                switch (this.GlClass)
                {
                    case GlClass.FundBalance:
                        // Invert Asset values
                        this.totalBudget *= -1;
                        this.totalActuals *= -1;
                        this.totalEncumbrances *= -1;
                        this.totalMemoActuals *= -1;
                        this.totalMemoEncumbrances *= -1;
                        break;
                    case GlClass.Liability:
                        // Invert Liability values
                        this.totalBudget *= -1;
                        this.totalActuals *= -1;
                        this.totalEncumbrances *= -1;
                        this.totalMemoActuals *= -1;
                        this.totalMemoEncumbrances *= -1;
                        break;
                    case GlClass.Revenue:
                        // Invert Revenue values
                        this.totalBudget *= -1;
                        this.totalActuals *= -1;
                        this.totalEncumbrances *= -1;
                        this.totalMemoActuals *= -1;
                        this.totalMemoEncumbrances *= -1;
                        break;
                    default:
                        // Values are unchanged
                        break;
                }

                // Initialize the GL accounts for the line item
                this.GlAccounts = new List<ProjectLineItemGlAccountViewModel>();

                if (projectLineItemDto.GlAccounts != null)
                {
                    foreach (var glAccount in projectLineItemDto.GlAccounts)
                    {
                        this.GlAccounts.Add(new ProjectLineItemGlAccountViewModel(glAccount, this.GlClass));
                    }

                    // Sort the GL accounts by ascending GL number.
                    this.GlAccounts = this.GlAccounts.OrderBy(x => x.GlAccount).ToList();
                }
            }
        }
    }
}
