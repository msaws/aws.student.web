﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    public class ProjectDetailViewModel
    {
        /// <summary>
        /// This is the one project to be displayed on the line items (detail) page
        /// </summary>
        public ProjectViewModel Project { get; set; }

        /// <summary>
        /// Initialize the attributes of the detail view model.
        /// </summary>
        /// <param name="projectDto">Project DTO</param>
        public ProjectDetailViewModel(Project projectDto)
        {
            this.Project = new ProjectViewModel(projectDto);
        }
    }
}