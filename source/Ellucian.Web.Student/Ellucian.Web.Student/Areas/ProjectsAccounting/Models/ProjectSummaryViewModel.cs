﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    public class ProjectSummaryViewModel
    {
        /// <summary>
        /// This is a list of project view models.
        /// </summary>
        public List<ProjectViewModel> Projects { get; set; }

        /// <summary>
        /// List of filter view models for project types.
        /// </summary>
        public List<ProjectTypesViewModel> ProjectTypes { get; set; }

        /// <summary>
        /// List of filter view models for project statuses.
        /// </summary>
        public List<ProjectStatusViewModel> ProjectStatuses { get; set; }

        /// <summary>
        /// Boolean to indicate if any project in the list has a revenue-type line items.
        /// </summary>
        public bool HasAnyRevenue
        {
            get
            {
                return this.Projects.Any(cc => cc.HasRevenue);
            }
        }

        /// <summary>
        /// Boolean to indicate if any project in the list has an expense-type line items.
        /// </summary>
        public bool HasAnyExpense
        {
            get
            {
                return this.Projects.Any(cc => cc.HasExpense);
            }
        }

        /// <summary>
        /// Initializes the view model properties.
        /// </summary>
        public ProjectSummaryViewModel()
        {
            this.Projects = new List<ProjectViewModel>();
            this.ProjectTypes = new List<ProjectTypesViewModel>();
            this.ProjectStatuses = new List<ProjectStatusViewModel>();
        }
    }
}