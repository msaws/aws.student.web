﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    public class ProjectStatusViewModel
    {
        public ProjectStatus Status { get; set; }

        /// <summary>
        /// Return the description from the resource file based on the Status property.
        /// </summary>
        public string StatusDescription
        {
            get
            {
                string description = "";
                switch (Status)
                {
                    case ProjectStatus.Active:
                        description = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ActiveFilterOption");
                        break;
                    case ProjectStatus.Inactive:
                        description = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "InactiveFilterOption");
                        break;
                    case ProjectStatus.Closed:
                        description = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ClosedFilterOption");
                        break;
                }

                return description;
            }
        }

        public ProjectStatusViewModel(ProjectStatus status)
        {
            this.Status = status;
        }
    }
}