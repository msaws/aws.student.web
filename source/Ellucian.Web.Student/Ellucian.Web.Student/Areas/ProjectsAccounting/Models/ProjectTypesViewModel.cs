﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class represents data that is displayed in the project type filter.
    /// </summary>
    public class ProjectTypesViewModel
    {
        /// <summary>
        /// This is the project type code that is displayed in the project type filter.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// This is the project type description that is displayed in the project type filter.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Initializes the object using DTO argument.
        /// </summary>
        /// <param name="projectTypeDto">A Project Types DTO</param>
        public ProjectTypesViewModel(ProjectType projectTypeDto)
        {
            this.Code = projectTypeDto.Code;
            this.Description = projectTypeDto.Description;
        }
    }
}