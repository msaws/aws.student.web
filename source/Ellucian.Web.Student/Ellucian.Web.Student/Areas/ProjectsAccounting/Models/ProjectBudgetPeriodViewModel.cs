﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Colleague.Dtos.ProjectsAccounting;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class represents a project budget period.
    /// </summary>
    public class ProjectBudgetPeriodViewModel
    {
        /// <summary>
        /// Unique ID for this project budget period
        /// </summary>
        public string SequenceNumber { get; set; }

        /// <summary>
        /// Budget period start date
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Budget period end date
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Returns a date range representing the start and end of the budget period.
        /// </summary>
        public string PeriodDateRange
        {
            get
            {
                return StartDate.ToShortDateString() + " - " + EndDate.ToShortDateString();
            }
        }

        /// <summary>
        /// Initialize the project budget period
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public ProjectBudgetPeriodViewModel(ProjectBudgetPeriod budgetPeriodDto)
        {
            this.SequenceNumber = budgetPeriodDto.SequenceNumber;
            this.StartDate = budgetPeriodDto.StartDate;
            this.EndDate = budgetPeriodDto.EndDate;
        }
    }
}