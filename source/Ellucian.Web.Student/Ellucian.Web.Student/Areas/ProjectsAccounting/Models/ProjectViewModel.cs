﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class represents a project that is displayed or used in the markup.
    /// </summary>
    public class ProjectViewModel
    {
        /// <summary>
        /// This is the project ID.
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// This is the project number to be displayed in the markup.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// This is the project title to be displayed in the markup.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// This is the project status to be displayed in the markup.
        /// </summary>
        public ProjectStatus Status { get; set; }

        /// <summary>
        /// List of budget periods for the project.
        /// </summary>
        public List<ProjectBudgetPeriodViewModel> BudgetPeriods { get; set; }

        /// <summary>
        /// Return the description from the resource file based on the Status property.
        /// </summary>
        public string StatusDescription
        {
            get
            {
                string description = "";
                switch (Status)
                {
                    case ProjectStatus.Active:
                        description = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ActiveFilterOption");
                        break;
                    case ProjectStatus.Inactive:
                        description = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "InactiveFilterOption");
                        break;
                    case ProjectStatus.Closed:
                        description = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ClosedFilterOption");
                        break;
                }

                return description;
            }
        }

        #region Revenue
        /// <summary>
        /// Boolean to indicate if the Project has any revenue line items.
        /// </summary>
        public bool HasRevenue { get { return this.RevenueLineItems.Any(); } }

        /// <summary>
        /// List of revenue line items for this project.
        /// </summary>
        public List<ProjectLineItemViewModel> RevenueLineItems { get; set; }

        /// <summary>
        /// The string representation of the project's total budget for revenue-type line items.
        /// </summary>
        public string TotalBudgetRevenue { get { return this.HasRevenue ? totalBudgetRevenue.ToString("C") : string.Empty; } }
        private decimal totalBudgetRevenue;

        /// <summary>
        /// The string representation of the project's total actuals for revenue-type line items.
        /// </summary>
        public string TotalActualsRevenue { get { return this.HasRevenue ? totalActualsRevenue.ToString("C") : string.Empty; } }
        private decimal totalActualsRevenue;

        /// <summary>
        /// The string representation of the project's total encumbrances for revenue-type line items.
        /// </summary>
        public string TotalEncumbrancesRevenue { get { return this.HasRevenue ? totalEncumbrancesRevenue.ToString("C") : string.Empty; } }
        private decimal totalEncumbrancesRevenue;

        /// <summary>
        /// The string representation of the project's total actuals plus encumbrances for revenue-type line items.
        /// </summary>
        public string TotalExpensesRevenue { get { return this.HasRevenue ? totalExpensesRevenue.ToString("C") : string.Empty; } }
        private decimal totalExpensesRevenue { get { return this.totalActualsRevenue + this.totalEncumbrancesRevenue; } }

        /// <summary>
        /// The remaining budget amount for the revenue-type line items in the project.
        /// </summary>
        public decimal BudgetRemainingRevenue { get { return totalBudgetRevenue - totalExpensesRevenue; } }

        /// <summary>
        /// The remaining budget formatted for display for the revenue-type line items in the project.
        /// </summary>
        public string BudgetRemainingRevenueFormatted { get { return this.HasRevenue ? BudgetRemainingRevenue.ToString("C") : string.Empty; } }

        /// <summary>
        /// Removes the negative sign from the remaining budget (revenue) and formats it based on if there is 
        /// any budget remaining so it can be properly read by the screen reader.
        /// </summary>
        public string BudgetRemainingScreenReaderTextRevenue
        {
            get
            {
                if (this.HasRevenue)
                {
                    if (BudgetRemainingRevenue < 0)
                    {
                        var remaining = Math.Abs(BudgetRemainingRevenue).ToString("C");
                        return remaining + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterOverbudgetText");
                    }
                    else
                    {
                        return this.BudgetReceivedAmount;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Calculate the percentage the project is over budget on its revenue-type line items.
        /// The value returned is used to fill the revenue bar graph from the right side.
        /// For example, if a project is 25% over budget, this property would calculate a 25% fill
        /// from the right. The remaining 75% is calculated by the BudgetSpentPercent property.
        /// </summary>
        public decimal OverageRevenue
        {
            get
            {
                decimal overage = 0;
                if (totalExpensesRevenue > totalBudgetRevenue)
                {
                    if (totalBudgetRevenue > 0)
                    {
                        overage = Math.Round((((totalExpensesRevenue - totalBudgetRevenue) / totalBudgetRevenue) * 100), 2);

                        if (overage > 100)
                        {
                            overage = 100;
                        }
                    }
                    else
                    {
                        overage = 100;
                    }
                }

                return overage;
            }
        }

        /// <summary>
        /// Calculate the percent the revenue bar graph is filled from the left side of the bar graph.
        /// For example if a project is 25% over budget, this property would calculate a 75% fill
        /// from the left. The remaining 25% is calculated by the OverageRevenue property.
        /// </summary>
        public decimal BudgetReceivedPercentRevenue
        {
            get
            {
                decimal budgetSpentPercent = 0;

                if (totalBudgetRevenue == 0 && totalExpensesRevenue == 0)
                // If Budget is $0 and expenses are $0 return a 0 percentage
                {
                    budgetSpentPercent = 0;
                }
                else if (totalExpensesRevenue < 0)
                {
                    // Total credit expenses return a zero percentage.
                    budgetSpentPercent = 0;
                }

                else if (totalExpensesRevenue > totalBudgetRevenue)
                // If expenses are greater than budget return an overspent percentage.
                // If expenses are greater than the budget, return a zero percentage if there is zero budget.
                // Return the 100 percent minus the overage percent when the budget is greater than zero.
                {
                    if (totalBudgetRevenue == 0)
                    {
                        budgetSpentPercent = 0;
                    }
                    else
                    {
                        budgetSpentPercent = Math.Round(100 - OverageRevenue);
                    }
                }
                else
                // Calculate the return percentage.
                {
                    budgetSpentPercent = Math.Round(((totalExpensesRevenue / totalBudgetRevenue) * 100));
                }
                return budgetSpentPercent;
            }
        }

        /// <summary>
        /// The budget spent percent formatted for display for the revenue-type line items in the project.
        /// </summary>
        public string BudgetReceivedPercentRevenueFormatted
        {
            get
            {
                if (this.HasRevenue)
                {
                    var percent = "";
                    if (totalBudgetRevenue == 0)
                    {
                        if (totalExpensesRevenue <= 0)
                        {
                            percent = "0 %";
                        }
                        else
                        {
                            percent = "101 %";
                        }
                    }
                    else
                    {
                        percent = Math.Round((totalExpensesRevenue / totalBudgetRevenue) * 100, MidpointRounding.AwayFromZero).ToString() + " %";
                    }

                    return percent;
                }
                else
                {
                    return string.Empty;
                }
            }
                }

        /// <summary>
        /// Return the text string for labeling the revenue bar graph
        /// </summary>
        public string BudgetReceivedAmount
        {
            get
            {
                if (this.HasRevenue)
                {
                    return this.TotalExpensesRevenue + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarText") + this.TotalBudgetRevenue + " " + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetedText");
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        #endregion

        #region Expenses
        /// <summary>
        /// List of expense line items for this project.
        /// </summary>
        public List<ProjectLineItemViewModel> ExpenseLineItems { get; set; }

        /// <summary>
        /// Boolean to indicate if the Project has any expense line items.
        /// </summary>
        public bool HasExpense { get { return this.ExpenseLineItems.Any(); } }

        /// <summary>
        /// The string representation of the project's total budget for expense-type line items.
        /// </summary>
        public string TotalBudget { get { return this.HasExpense ? totalBudget.ToString("C") : string.Empty; } }
        private decimal totalBudget;

        /// <summary>
        /// The string representation of the project's total actuals for expense-type line items.
        /// </summary>
        public string TotalActuals { get { return this.HasExpense ? totalActuals.ToString("C") : string.Empty; } }
        private decimal totalActuals;

        /// <summary>
        /// The string representation of the project's total encumbrances for expense-type line items.
        /// </summary>
        public string TotalEncumbrances { get { return this.HasExpense ? totalEncumbrances.ToString("C") : string.Empty; } }
        private decimal totalEncumbrances;

        /// <summary>
        /// The string representation of the project's total actuals plus encumbrances for expense-type line items.
        /// </summary>
        public string TotalExpenses { get { return this.HasExpense ? totalExpenses.ToString("C") : string.Empty; } }
        private decimal totalExpenses;

        /// <summary>
        /// The remaining budget amount for the expense-type line items in the project.
        /// </summary>
        public decimal BudgetRemaining { get { return totalBudget - totalExpenses; } }

        /// <summary>
        /// The remaining budget formatted for display.
        /// </summary>
        public string BudgetRemainingFormatted { get { return this.HasExpense ? BudgetRemaining.ToString("C") : string.Empty; } }

        /// <summary>
        /// This property returns a string representation of the remaining budget amount
        /// for expense-type line items in the project.
        /// </summary>
        public string BudgetRemainingString
        {
            get
            {
                if (this.HasExpense)
                {
                    string remainingString;
                    if (BudgetRemaining >= 0)
                    {
                        remainingString = BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText");
                    }
                    else
                    {
                        remainingString = BudgetRemaining.ToString("C");
                    }

                    return remainingString;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Removes the negative sign from the remaining budget so it can be properly
        /// read by the screen reader.
        /// </summary>
        public string BudgetRemainingForScreenReader
        {
            get
            {
                if (this.HasExpense)
                {
                    var remaining = "";
                    if (BudgetRemaining < 0)
                    {
                        remaining = Math.Abs(BudgetRemaining).ToString("C");
                    }
                    return remaining;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Calculate the percentage the project is over budget on its expense-type line items.
        /// The value returned is used to fill the expense bar graph from the right side.
        /// For example, if a project is 25% over budget, this property would calculate a 25% fill
        /// from the right. The remaining 75% is calculated by the BudgetSpentPercent property.
        /// </summary>
        public decimal Overage
        {
            get
            {
                decimal overage = 0;
                if (totalExpenses > totalBudget)
                {
                    if (totalBudget > 0)
                    {
                        overage = Math.Round((((totalExpenses - totalBudget) / totalBudget) * 100), 2);

                        if (overage > 100)
                        {
                            overage = 100;
                        }
                    }
                    else
                    {
                        overage = 100;
                    }
                }

                return overage;
            }
        }

        /// <summary>
        /// Calculate the percent the expense bar graph is filled from the left side of the bar graph.
        /// For example if a project is 25% over budget, this property would calculate a 75% fill
        /// from the left. The remaining 25% is calculated by the Overage property.
        /// </summary>
        public decimal BudgetSpentPercent
        {
            get
            {
                decimal budgetSpentPercent = 0;

                if (totalBudget == 0 && totalExpenses == 0)
                // If Budget is $0 and expenses are $0 return a 0 percentage
                {
                    budgetSpentPercent = 0;
                }
                else if (totalExpenses < 0)
                {
                    // Total credit expenses return a zero percentage.
                    budgetSpentPercent = 0;
                }

                else if (totalExpenses > totalBudget)
                // If expenses are greater than budget return an overspent percentage
                // If expenses are greater than the budget, return a zero percentage if there is zero budget
                // Return the 100 percent minus the overage percent when the budget is greater than zero
                {
                    if (totalBudget == 0)
                    {
                        budgetSpentPercent = 0;
                    }
                    else
                    {
                        budgetSpentPercent = Math.Round(100 - Overage);
                    }
                }
                else
                // Calculate the return percentage
                {
                    budgetSpentPercent = Math.Round(((totalExpenses / totalBudget) * 100));
                }
                return budgetSpentPercent;
            }
        }

        /// <summary>
        /// Calculates what percentage of the budget has been spent for expense-type 
        /// line items in the project.
        /// </summary>
        public string ListViewBudgetSpentPercent
        {
            get
            {
                if (this.HasExpense)
                {
                    var percent = "";
                    if (totalBudget == 0)
                    {
                        if (totalExpenses <= 0)
                        {
                            percent = "0 %";
                        }
                        else
                        {
                            percent = "101 %";
                        }
                    }
                    else
                    {
                        percent = Math.Round((totalExpenses / totalBudget) * 100, MidpointRounding.AwayFromZero).ToString() + " %";
                    }

                    return percent;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Return the text string stating the financial health of the project line item.
        /// </summary>
        public string FinancialHealthTextForScreenReader
        {
            get
            {
                if (this.HasExpense)
                {
                    var financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
                    if (BudgetRemaining < 0)
                    {
                        financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
                    }
                    else if (BudgetSpentPercent >= 85 && BudgetSpentPercent <= 100)
                    {
                        financialHealthText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
                    }

                    return financialHealthText;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Return the text string for labeling the expenses bar graph.
        /// </summary>
        public string BudgetSpentAmount
        {
            get
            {
                if (this.HasExpense)
                {
                    return this.TotalExpenses + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarText") + this.TotalBudget;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        #endregion

        #region Assets

        /// <summary>
        /// Boolean to indicate if the Project has any expense line items.
        /// </summary>
        public bool HasAssets { get { return this.AssetLineItems.Any(); } }

        /// <summary>
        /// List of asset line items.
        /// </summary>
        public List<ProjectLineItemViewModel> AssetLineItems { get; set; }

        /// <summary>
        /// The string representation of the project's total actuals for asset-type line items.
        /// </summary>
        public string TotalActualsAsset { get { return this.HasAssets ? totalActualsAsset.ToString("C") : string.Empty; } }
        private decimal totalActualsAsset;

        /// <summary>
        /// The string representation of the project's total encumbrances for asset-type line items.
        /// </summary>
        public string TotalEncumbrancesAsset { get { return this.HasAssets ? totalEncumbrancesAsset.ToString("C") : string.Empty; } }
        private decimal totalEncumbrancesAsset;

        #endregion

        #region Liabilities

        /// <summary>
        /// Boolean to indicate if the Project has any expense line items.
        /// </summary>
        public bool HasLiabilities { get { return this.LiabilityLineItems.Any(); } }

        /// <summary>
        /// List of liability line items.
        /// </summary>
        public List<ProjectLineItemViewModel> LiabilityLineItems { get; set; }

        /// <summary>
        /// The string representation of the project's total actuals for liability-type line items.
        /// </summary>
        public string TotalActualsLiability { get { return this.HasLiabilities ? totalActualsLiability.ToString("C") : string.Empty; } }
        private decimal totalActualsLiability;

        /// <summary>
        /// The string representation of the project's total encumbrances for liability-type line items.
        /// </summary>
        public string TotalEncumbrancesLiability { get { return this.HasLiabilities ? totalEncumbrancesLiability.ToString("C") : string.Empty; } }
        private decimal totalEncumbrancesLiability;

        #endregion

        #region Fund Balances

        /// <summary>
        /// Boolean to indicate if the Project has any expense line items.
        /// </summary>
        public bool HasFundBalances { get { return this.FundBalanceLineItems.Any(); } }

        /// <summary>
        /// List of fund balance line items.
        /// </summary>
        public List<ProjectLineItemViewModel> FundBalanceLineItems { get; set; }

        /// <summary>
        /// The string representation of the project's total actuals for fund balance-type line items.
        /// </summary>
        public string TotalActualsFundBalance { get { return this.HasFundBalances ? totalActualsFundBalance.ToString("C") : string.Empty; } }
        private decimal totalActualsFundBalance;

        /// <summary>
        /// The string representation of the project's total encumbrances for fund balance-type line items.
        /// </summary>
        public string TotalEncumbrancesFundBalance { get { return this.HasFundBalances ? totalEncumbrancesFundBalance.ToString("C") : string.Empty; } }
        private decimal totalEncumbrancesFundBalance;

        #endregion

        #region Net Properties

        public string TotalBudgetNet { get { return (totalBudgetRevenue - totalBudget).ToString("C"); } }

        public string TotalActualsNet { get { return (totalActualsRevenue - totalActuals).ToString("C"); } }
        #endregion

        /// <summary>
        /// Initialize the view model based on the Project DTO.
        /// </summary>
        /// <param name="projectDto">A Project DTO</param>
        public ProjectViewModel(Project projectDto)
        {
            if (projectDto != null)
            {
                this.ProjectId = projectDto.Id;
                this.Number = projectDto.Number;
                this.Title = projectDto.Title;
                this.Status = projectDto.Status;

                // Assign the Expense amounts
                this.totalBudget = projectDto.TotalBudget;
                this.totalExpenses = projectDto.TotalExpenses;
                this.totalActuals = projectDto.TotalActuals;
                this.totalEncumbrances = projectDto.TotalEncumbrances;

                // Negate and assign the Revenue amounts.
                this.totalBudgetRevenue = projectDto.TotalBudgetRevenue * -1;
                this.totalActualsRevenue = projectDto.TotalActualsRevenue * -1;
                this.totalEncumbrancesRevenue = projectDto.TotalEncumbrancesRevenue * -1;

                // Assign the Asset amounts
                this.totalActualsAsset = projectDto.TotalActualsAssets;
                this.totalEncumbrancesAsset = projectDto.TotalEncumbrancesAssets;

                // Negate and assign the Libility amounts.
                this.totalActualsLiability = projectDto.TotalActualsLiabilities * -1;
                this.totalEncumbrancesLiability = projectDto.TotalEncumbrancesLiabilities * -1;

                // Negate and assign the Fund Balance amounts
                this.totalActualsFundBalance = projectDto.TotalActualsFundBalance * -1;
                this.totalEncumbrancesFundBalance = projectDto.TotalEncumbrancesFundBalance * -1;

                // Initialize and populate the line item lists.
                this.RevenueLineItems = new List<ProjectLineItemViewModel>();
                this.ExpenseLineItems = new List<ProjectLineItemViewModel>();
                this.AssetLineItems = new List<ProjectLineItemViewModel>();
                this.LiabilityLineItems = new List<ProjectLineItemViewModel>();
                this.FundBalanceLineItems = new List<ProjectLineItemViewModel>();
                if (projectDto.LineItems != null)
                {
                    foreach (var item in projectDto.LineItems)
                    {
                        if (item != null)
                        {
                            if (item.GlClass == GlClass.Revenue)
                            {
                                this.RevenueLineItems.Add(new ProjectLineItemViewModel(item));
                            }
                            else if (item.GlClass == GlClass.Expense)
                            {
                                this.ExpenseLineItems.Add(new ProjectLineItemViewModel(item));
                            }
                            else if (item.GlClass == GlClass.Asset)
                            {
                                this.AssetLineItems.Add(new ProjectLineItemViewModel(item));
                            }
                            else if (item.GlClass == GlClass.Liability)
                            {
                                this.LiabilityLineItems.Add(new ProjectLineItemViewModel(item));
                            }
                            else if (item.GlClass == GlClass.FundBalance)
                            {
                                this.FundBalanceLineItems.Add(new ProjectLineItemViewModel(item));
                            }
                        }
                    }
                }
            }

            // Sort the project line items by code description then by code.
            this.AssetLineItems = this.AssetLineItems.OrderBy(x => x.ItemCodeDescription).ThenBy(x => x.ItemCode).ToList();
            this.LiabilityLineItems = this.LiabilityLineItems.OrderBy(x => x.ItemCodeDescription).ThenBy(x => x.ItemCode).ToList();
            this.FundBalanceLineItems = this.FundBalanceLineItems.OrderBy(x => x.ItemCodeDescription).ThenBy(x => x.ItemCode).ToList();
            this.RevenueLineItems = this.RevenueLineItems.OrderBy(x => x.ItemCodeDescription).ThenBy(x => x.ItemCode).ToList();
            this.ExpenseLineItems = this.ExpenseLineItems.OrderBy(x => x.ItemCodeDescription).ThenBy(x => x.ItemCode).ToList();

            // Initialize the budget periods
            this.BudgetPeriods = new List<ProjectBudgetPeriodViewModel>();
            if (projectDto.BudgetPeriods != null)
            {
                foreach (var budgetPeriod in projectDto.BudgetPeriods)
                {
                    this.BudgetPeriods.Add(new ProjectBudgetPeriodViewModel(budgetPeriod));
                }
            }
        }
    }
}