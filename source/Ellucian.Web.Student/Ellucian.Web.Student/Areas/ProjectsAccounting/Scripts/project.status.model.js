﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

// Javascript representation of a project type
function projectStatusModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects (line items, etc)
    ko.mapping.fromJS(data, projectMapping, self);

    // Unique ID for this project type
    self.myId = ko.observable("project-status-" + data.StatusDescription);
};