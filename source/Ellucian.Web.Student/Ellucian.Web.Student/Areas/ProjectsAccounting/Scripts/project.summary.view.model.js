﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

// Set up a view model for the list of projects summary view models
function projectSummaryViewModel() {
    var self = this;

    // "Inherit" the base view model, specify the mobile screen width
    BaseViewModel.call(self, 768);

    // Change desktop structure to mobile structure
    self.changeToMobile = function () {
    }

    // Change mobile structure back to desktop view
    self.changeToDesktop = function () {
    }

    // Check for mobile and set the isMobile observable as determined (defined in BaseViewModel).
    self.checkForMobile(window, document);

    // Flag that indicates if the initial ajax request has completed and data has been loaded into the page.
    self.IsPageLoaded = ko.observable(false);

    // Used to keep track of the number of projects returned.
    self.ProjectCount = ko.observable(0);

    // Initial value for HasAnyRevenue boolean flag.
    self.HasAnyRevenue = ko.observable(false);

    // Initial value for HasAnyExpense boolean flag.
    self.HasAnyExpense = ko.observable(false);

    // Determine whether or not we should display the projects list
    self.ShouldDisplay = ko.computed(function () {
        return self.IsPageLoaded() && self.ProjectCount() > 0;
    }, self);

    // Used for spinner
    self.IsFilterUpdating = ko.observable(false);

    // The list of project objects which are displayed
    self.Projects = ko.observableArray();

    // The list of project type objects which are displayed in the project type filter
    self.ProjectTypes = ko.observableArray();

    // List of project status filter model objects which are displayed in the projec status filter.
    self.ProjectStatuses = ko.observableArray();

    // KO-managed list of status codes
    self.statusCodesSelected = ko.observableArray();

    // KO-managed list of type codes
    self.typeCodesSelected = ko.observableArray();

    // Detect whether the filter is being used
    // JAWS seemed to be triggering the focus change on pageload, keyboard trap on last
    // selected checkbox, but it may just have glitched, may not be necessary?
    self.usingFilter = ko.observable(false);
    
    // Observable for selected filter checkbox
    self.selectedFilterOption = ko.observable();

    // Observable boolean to control which UI view is shown.
    self.isBarGraphViewActive = ko.observable(true);

    // Observable boolean to control if the Export Dropdown is expanded/collapsed.
    self.isExportDropdownExpanded = ko.observable(false);

    // Boolean to indicate when it is safe to display the bar graph or list view.
    self.isUIViewKnown = ko.observable(false);

    self.updateProjectFilter = function (event) {
        // Get the new list of projects
        self.getProjects();

        // The filter is currently in use, change the value to true
        self.usingFilter(true);

        // Get the ID of the currently selected filter option checkbox
        // We will change the focus to this checkbox after the loading dialog has closed
        self.selectedFilterOption(event.myId());

        // We need to return true to allow the checkboxes to display properly when checked/unchecked.
        return true;
    };

    self.getProjects = function () {
        // Get the default view preference.
        self.getUIPreference();

        // Format the criteria array
        var criteria = {
            StatusCodes: self.statusCodesSelected(),
            TypeCodes: self.typeCodesSelected()
        };

        // Status gets index: 0.
        var urlQuery = "?summaryOnly=true";
        var index = 0;
        for (var i = 0; i < criteria.StatusCodes.length; i++) {
            // Format the field (index: 0) and value (index: 1) parameters.
            urlQuery += '&filter[' + index + '][0]=Status';
            urlQuery += '&filter[' + index + '][1]=' + criteria.StatusCodes[i];
            index++;
        }

        // Project type gets index: 1.
        for (var i = 0; i < criteria.TypeCodes.length; i++) {
            // Format the field (index: 0) and value (index: 1) parameters.
            urlQuery += '&filter[' + index + '][0]=ProjectType';
            urlQuery += '&filter[' + index + '][1]=' + criteria.TypeCodes[i];
            index++;
        }
        var projectsQueryUrl = Ellucian.ProjectsAccounting.getProjectsActionUrl + urlQuery;

        $.ajax({
            url: projectsQueryUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                // Show the loading spinner
                self.IsFilterUpdating(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    var projectsData = ko.mapping.fromJS(data, projectMapping);

                    // Clear and repopulate the projects array.
                    self.Projects.removeAll();
                    self.Projects.pushAll(projectsData.Projects());

                    // Clear and populate the project status list
                    self.ProjectStatuses.removeAll();
                    self.ProjectStatuses.pushAll(projectsData.ProjectStatuses());

                    // Clear and populate the project types list
                    self.ProjectTypes.removeAll();
                    self.ProjectTypes.pushAll(projectsData.ProjectTypes());

                    self.HasAnyExpense(projectsData.HasAnyExpense());
                    self.HasAnyRevenue(projectsData.HasAnyRevenue());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - Unknown status code
                var message = unableToLoadProjectsText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                // Once the ajax request is completed (regardless of success/error), set isLoaded to true, which
                // disables any spinners/loading icons and other related UI updates.
                self.IsPageLoaded(true);

                var count = self.Projects().length;
                self.ProjectCount(self.Projects().length);
                
                // Hide the filter spinner
                self.IsFilterUpdating(false);

                // always apply "display: block" to ui-progressbar-value to show text on all bar graphs
                $('.ui-progressbar-value').css({ display: "block" });

                // If the filter has been used, change the focus upon dialog close to selected checkbox
                if (self.usingFilter() == true) {
                    var selectedFilterId = "#" + self.selectedFilterOption();
                    $(selectedFilterId).focus();
                }

                // Show a notification if there are no projects
                if (self.Projects().length == 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: noProjectsToDisplayText, type: "info" });
                }
            }
        });
    }

    // Display the grid view
    self.toggleGrid = function () {
        self.isBarGraphViewActive(true);
        self.setUIPreference(true);
    }

    // Display the list view
    self.toggleList = function () {
        self.isBarGraphViewActive(false);
        self.setUIPreference(false);
    }

    self.setUIPreference = function (isBar) {
        var setPreferenceFailedNotification = {
            message: Ellucian.ProjectsAccounting.setPreferenceFailedMessage,
            type: "error"
        };

        $.ajax({
            url: Ellucian.ProjectsAccounting.setUIPreferenceUrl,
            type: "POST",
            data: ko.toJSON({ preferredUi: isBar }),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                // Remove the error notification.
                $('#notificationHost').notificationCenter('removeNotification', setPreferenceFailedNotification);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#notificationHost').notificationCenter('addNotification', setPreferenceFailedNotification);
            }
        });
    }

    self.getUIPreference = function () {
        $.ajax({
            url: Ellucian.ProjectsAccounting.getUIPreferenceUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.isBarGraphViewActive(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                self.isBarGraphViewActive(true)
            },
            complete: function () {
                self.isUIViewKnown(true);
            }
        });
    }

    // Function to expand/collapse the Export Dropdown
    self.toggleExportDropdown = function () {
        self.isExportDropdownExpanded(!self.isExportDropdownExpanded());
    }

    // The (ordered) columns to include in the CSV export
    self.csvColumns = ko.computed(function () {
        var columns = ko.observableArray([
            'Number',
            'Title',
            'StatusDescription'
        ]);
        if (self.HasAnyRevenue()) {
            columns.pushAll([
                'TotalBudgetRevenue',
                'TotalActualsRevenue'
            ]);
        }
        if (self.HasAnyExpense()) {
            columns.pushAll([
                'TotalBudget',
                'TotalExpenses',
                'BudgetRemainingFormatted',
                'PercentSpentFormatted'
            ]);
        }
        return columns();
    });

    // The (ordered) replacement headers for each CSV column
    self.csvColumnHeaders = ko.computed(function () {
        var columns = ko.observableArray([
            Ellucian.ProjectsAccounting.projectNumberHeader,
            Ellucian.ProjectsAccounting.projectTitleHeader,
            Ellucian.ProjectsAccounting.projectStatusHeader
        ]);
        if (self.HasAnyRevenue()) {
            columns.pushAll([
                Ellucian.ProjectsAccounting.listViewHeaderBudgetedRevenue,
                Ellucian.ProjectsAccounting.listViewHeaderActualRevenue
            ]);
        }
        if (self.HasAnyExpense()) {
            columns.pushAll([
                Ellucian.ProjectsAccounting.listViewHeaderBudget,
                Ellucian.ProjectsAccounting.listViewHeaderActualsEncumbrances,
                Ellucian.ProjectsAccounting.remainingExpensesLabel,
                Ellucian.ProjectsAccounting.percentSpentHeaderLabel
            ]);
        }
        return columns();
    });
}