﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

// Register CsvDownloadLink component
ko.components.register('csv-download-link', {
    require: 'CsvDownloadLink/_CsvDownloadLink'
});

// Set up viewModel instance
var projectSummaryViewModelInstance = new projectSummaryViewModel();

// Once the page has loaded...
$(document).ready(function () {

    ko.applyBindings(projectSummaryViewModelInstance, document.getElementById("main"));

    // Set the filter defaults to get projects that are:
    //  - Active
    projectSummaryViewModelInstance.statusCodesSelected().push("Active");

    // Execute AJAX request to get all available projects for the user
    projectSummaryViewModelInstance.getProjects();

});

// Provide "auto-close" functionality for Export Dropdown
$(document).mouseup(function (e) {
    closeExportDropDown(e);
});

function closeExportDropDown(clickEvent) {
    var close = true;
    var menuSelector = "#download-menu";
    var ddlSelector = "#download-dropdown";
    // If the click is within the drop down list don't close it. (And if the click is within the button, don't fire the click event again - let the standard click event handle it.)
    if ($(menuSelector).is(clickEvent.target) || $(menuSelector).has(clickEvent.target).length > 0 ||
        $(ddlSelector).is(clickEvent.target) || $(ddlSelector).has(clickEvent.target).length > 0) {
        close = false;
    }

    if (close && $(menuSelector).is(':visible')) {
        $(ddlSelector).click();
    }
}