﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

// Javascript representation of a project type
function projectLineItemModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects (line items, etc)
    ko.mapping.fromJS(data, projectMapping, self);
};