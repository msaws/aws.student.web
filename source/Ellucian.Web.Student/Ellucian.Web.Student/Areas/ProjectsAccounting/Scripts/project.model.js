﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

// Javascript representation of a project
function projectModel(data) {
    var self = this;

    // Perform any deeper mapping of child objects (line items, etc)
    ko.mapping.fromJS(data, projectMapping, self);

    // Perform mapping of project statuses
    self.projectStatusIndicator = ko.computed(function () {

        var statusIndicatorColor = "project-active";
        if (self.StatusDescription() == inactiveStatusText) {

            // if the project status is inactive, set the indicator color to inactive
            statusIndicatorColor = "project-inactive";
        }
        else if (self.StatusDescription() == closedStatusText) {
            statusIndicatorColor = "project-closed";
        }
        return statusIndicatorColor;
    }, self);

    // projectBarGraphData returns the "parts" required for the multi-part bar-graph
    // Only one "part" is required, but many may be used when appropriate
    // value = the % complete for each part
    // text = text to display inside the bar graph
    // barClass = the css styling to apply to the part (so each part can be a different color, etc)
    // title = text used in the tooltip for the bar graph
    self.projectBarGraphData = function () {
        var parts = [];
        var budgetBarColor = "bar-graph bar-graph--success";
        if (self.BudgetRemaining() < 0) {
            budgetBarColor = "bar-graph bar-graph--error";
        }
        else if (self.BudgetSpentPercent() >= 85 && self.BudgetSpentPercent() <= 100) {
            budgetBarColor = "bar-graph bar-graph--warning";
        }
        if (self.BudgetSpentPercent) {
            parts.push({ value: self.BudgetSpentPercent(), barClass: budgetBarColor, title: projectBarGraphTitle });
        }
        return parts;
    }

    self.projectRevenueBarGraphData = function () {
        var parts = [];
        var budgetBarColor = "bar-graph bar-graph--hint";

        if (self.BudgetReceivedPercentRevenue) {
            parts.push({ value: self.BudgetReceivedPercentRevenue(), barClass: budgetBarColor, title: projectBarGraphTitle });
        }

        return parts;
    }

    self.BudgetSpentAmount = ko.computed(function () {
        return self.TotalExpenses() + budgetBarText + self.TotalBudget();
    }, self);

    self.remainingBudgetColor = ko.computed(function () {
        return self.BudgetRemaining() < 0 ? "remaining-red" : "remaining-black";
    }, self);

    self.remainingBudgetRevenueColor = ko.computed(function () {
        if (self.BudgetRemainingRevenue) {
            return self.BudgetRemainingRevenue() < 0 ? "remaining-green" : "remaining-black";
        }
        else {
            return "";
        }
    }, self);

    self.overageColor = ko.computed(function () {
        var style = "";
        if (self.TotalBudget() == 0 || self.Overage() >= 100) {
            style = "zero-budget";
        }

        if (self.Overage() > 0) {
            style = style + " overage";
        }
        return style;
    }, self);

    self.overageRevenueColor = ko.computed(function () {
        var style = "";
        if ((self.TotalBudgetRevenue && self.TotalBudgetRevenue() == 0) || (self.OverageRevenue && self.OverageRevenue() >= 100)) {
            style = "zero-budget";
        }

        if (self.OverageRevenue && self.OverageRevenue() > 0) {
            style = style + " overage-revenue";
        }
        return style;
    }, self);

    // Determine financial health style
    self.financialHealthIndicator = ko.computed(function () {
        var financialHealthIcon = "";

        if (self.HasExpense && !self.HasExpense()) {
            return financialHealthIcon;
        }
        else if (self.BudgetRemaining && self.BudgetRemaining() < 0) {
            financialHealthIcon = "group-icon-red";
        }
        else if (self.BudgetSpentPercent && self.BudgetSpentPercent() >= 85 && self.BudgetSpentPercent() <= 100) {
            financialHealthIcon = "group-icon-yellow";
        }
        else {
            financialHealthIcon = "group-icon-green";
        }

        return financialHealthIcon;
    }, self);

    // Determine financial health style
    self.projectDetailFinancialHealthIndicator = ko.computed(function () {
        var financialHealthIcon = "project-line-item-icon-green";
        if (self.BudgetRemaining() < 0) {
            financialHealthIcon = "project-line-item-icon-red";
        }
        else if (self.BudgetSpentPercent() >= 85 && self.BudgetSpentPercent() <= 100) {
            financialHealthIcon = "project-line-item-icon-yellow";
        }

        return financialHealthIcon;
    }, self);

    self.PercentSpentFormatted = ko.computed(function () {
        if (self.ListViewBudgetSpentPercent) {
            var ps = self.ListViewBudgetSpentPercent();
            var regex = /(\d+)(\d{3})/;
            while (regex.test(ps)) {
                ps = ps.replace(regex, '$1' + ',' + '$2');
            }
            return ps;
        }
        else {
            return "";
        }
    }, self);
};