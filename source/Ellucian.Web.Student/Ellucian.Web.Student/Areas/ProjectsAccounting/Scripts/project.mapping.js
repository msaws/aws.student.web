﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

/// <reference path="../Views/Home/ProjectLineItems.cshtml" />
// Configure a knockout mapping object for the projects
var projectMapping = {
    'Projects': {
        // The key attribute tells knockout which field is the key for each object.  
        // This allows knockout to update projects without dropping/adding them completely
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        // The create attribute tells knockout how to create a project observable 
        create: function (options) {
            return new projectModel(options.data);
        }
    },
    'Project': {
        // The key attribute tells knockout which field is the key for each object.  
        // This allows knockout to update projects without dropping/adding them completely
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        // The create attribute tells knockout how to create a project observable 
        create: function (options) {
            return new projectModel(options.data);
        }
    },
    'ProjectTypes': {
        // The key attribute tells knockout which field is the key for each object.  
        // This allows knockout to update projects without dropping/adding them completely
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        // The create attribute tells knockout how to create a project observable 
        create: function (options) {
            return new projectTypeModel(options.data);
        }
    },
    'ProjectStatuses': {
        // The key attribute tells knockout which field is the key for each object.  
        // This allows knockout to update projects without dropping/adding them completely
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        // The create attribute tells knockout how to create a project observable 
        create: function (options) {
            return new projectStatusModel(options.data);
        }
    },
    'ExpenseLineItems': {
        // The key attribute tells knockout which field is the key for each object.  
        // This allows knockout to update projects without dropping/adding them completely
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        // The create attribute tells knockout how to create a project observable 
        create: function (options) {
            return new projectLineItemModel(options.data);
        }
    },
    'RevenueLineItems': {
        // The key attribute tells knockout which field is the key for each object.  
        // This allows knockout to update projects without dropping/adding them completely
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        // The create attribute tells knockout how to create a project observable 
        create: function (options) {
            return new projectLineItemModel(options.data);
        }
    },
    'EncumbranceTransactions': {
        create: function (options) {
            return new glTransactionViewModel(options.data);
        }
    },
    'ActualTransactions': {
        create: function (options) {
            return new glTransactionViewModel(options.data);
        }
    },
};