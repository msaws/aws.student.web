﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

// Javascript representation of a budget period object.
function budgetPeriodModel() {
    var self = this;

    self.Id = ko.observable()

    self.Description = ko.observable();
};