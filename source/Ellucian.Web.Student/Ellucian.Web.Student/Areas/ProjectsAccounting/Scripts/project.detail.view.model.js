﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
cfNS = cfNS || {};

// Initialize the ColleagueFinance namespace object for use in ColleagueFinance module.
var Ellucian = Ellucian || {};
Ellucian.ColleagueFinance = Ellucian.ColleagueFinance || {};

//Select which storage mechanism to use depending on browser
var memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();

// Set up a view model for the list of projects summary view models
function projectDetailViewModel() {
    var self = this;

    var defaultBudgetPeriodId = '0';
    var defaultBudgetPeriodDescription = Ellucian.ProjectsAccounting.allActivityOption;

    // Flag that indicates if the ajax request has completed and data has been loaded.
    self.IsPageLoaded = ko.observable(false);

    // Flag to control whether or not the filter spinner is displayed.
    self.IsFilterUpdating = ko.observable(false);

    // Flag that controls which view we display: Project Line Items or GL Detail
    self.GlDetailView = ko.observable(false);

    // Used to keep track of which GL Account number we're looking at when in GL Detail mode
    self.GlNumberSelected = ko.observable();

    // Used to store the description of the line item selected
    self.LineItemDescription = ko.observable();

    // Used to store the code of the line item selected
    self.LineItemCode = ko.observable();

    // Track which tab is selected when in mobile view
    self.GlDetailTabSelected = ko.observable('E');

    // This represents the project being displayed on the page
    self.Project = ko.observable();

    // Check if Project is undefined
    self.isProjectDefined = ko.computed(function () {
        return typeof self.Project() !== 'undefined';
    }, self);

    // Keep track of which GL account observable has been selected.
    self.SelectedGlAccountHasTransactions = ko.computed(function () {
        var hasTransactions = false;

        // Isolate the GL account observable using the GlNumberSelected observable.
        tempSelectedGlAccount = null;

        if (self.GlNumberSelected() && self.isProjectDefined()) {
            var glAccountFound = false;

            // Search for the selected GL number in Asset line items.
            for (var i = 0; i < self.Project().AssetLineItems().length; i++) {
                var lineItem = self.Project().AssetLineItems()[i];

                for (var j = 0; j < lineItem.GlAccounts().length; j++) {
                    var glAccount = lineItem.GlAccounts()[j];
                    if (glAccount.GlAccount() == self.GlNumberSelected()) {
                        tempSelectedGlAccount = glAccount;
                    }
                }
            }

            // Search for the selected GL number in Liability line items.
            if (!glAccountFound) {
                for (var i = 0; i < self.Project().LiabilityLineItems().length; i++) {
                    var lineItem = self.Project().LiabilityLineItems()[i];

                    for (var j = 0; j < lineItem.GlAccounts().length; j++) {
                        var glAccount = lineItem.GlAccounts()[j];
                        if (glAccount.GlAccount() == self.GlNumberSelected()) {
                            tempSelectedGlAccount = glAccount;
                        }
                    }
                }
            }

            // Search for the selected GL number in Fund Balance line items.
            if (!glAccountFound) {
                for (var i = 0; i < self.Project().FundBalanceLineItems().length; i++) {
                    var lineItem = self.Project().FundBalanceLineItems()[i];

                    for (var j = 0; j < lineItem.GlAccounts().length; j++) {
                        var glAccount = lineItem.GlAccounts()[j];
                        if (glAccount.GlAccount() == self.GlNumberSelected()) {
                            tempSelectedGlAccount = glAccount;
                        }
                    }
                }
            }

            // Search for the selected GL number in Revenue line items.
            if (!glAccountFound) {
                for (var i = 0; i < self.Project().RevenueLineItems().length; i++) {
                    var lineItem = self.Project().RevenueLineItems()[i];

                    for (var j = 0; j < lineItem.GlAccounts().length; j++) {
                        var glAccount = lineItem.GlAccounts()[j];
                        if (glAccount.GlAccount() == self.GlNumberSelected()) {
                            tempSelectedGlAccount = glAccount;
                        }
                    }
                }
            }

            // Search for the selected GL number in Expense line items.
            if (!glAccountFound) {
                for (var i = 0; i < self.Project().ExpenseLineItems().length; i++) {
                    var lineItem = self.Project().ExpenseLineItems()[i];

                    for (var j = 0; j < lineItem.GlAccounts().length; j++) {
                        var glAccount = lineItem.GlAccounts()[j];
                        if (glAccount.GlAccount() == self.GlNumberSelected()) {
                            tempSelectedGlAccount = glAccount;
                        }
                    }
                }
            }

            if (typeof tempSelectedGlAccount !== 'undefined' && tempSelectedGlAccount !== null) {
                if (tempSelectedGlAccount.ActualTransactions().length > 0) {
                    hasTransactions = true;
                }

                if (tempSelectedGlAccount.EncumbranceTransactions().length > 0) {
                    hasTransactions = true;
                }
            }
        }

        return hasTransactions;
    }, self);

    // Determine if the export button should be enabled.
    self.EnableExport = ko.computed(function () {
        return !self.GlDetailView() || (self.GlDetailView() && self.SelectedGlAccountHasTransactions())
    }, self);

    // New list of budget periods that includes the 'All Activity' option
    self.BudgetPeriodsSelection = ko.observableArray();

    // Generate a list of budget periods that includes the 'All Activity' option
    self.GenerateBudgetPeriodsSelection = function (budgetPeriodArray) {
        // Re-initialize the selection list
        self.BudgetPeriodsSelection([]);

        // Populate the 'All Activity' option
        var budgetPeriod = new budgetPeriodModel();
        budgetPeriod.Id(defaultBudgetPeriodId);
        budgetPeriod.Description(defaultBudgetPeriodDescription);
        self.BudgetPeriodsSelection.push(budgetPeriod);

        // Now, add the supplied budget periods to the list.
        for (var i = 0; i < budgetPeriodArray.length; i++) {
            budgetPeriod = new budgetPeriodModel();
            budgetPeriod.Id(budgetPeriodArray[i].SequenceNumber);
            budgetPeriod.Description(budgetPeriodArray[i].PeriodDateRange);
            self.BudgetPeriodsSelection.push(budgetPeriod);
        }
    }

    // Obtain the name of the export file.
    self.csvFileName = ko.computed(function () {
        // Get the budget period
        var budgetPeriodDescription = "";
        if (self.isProjectDefined()) {
            var budgetPeriod = self.BudgetPeriodsSelection().filter(function matchBudgetPeriodId(bp) {
                if (self.SelectedBudgetPeriod()) {
                    return bp.Id() === self.SelectedBudgetPeriod();
                }
                else {
                    return bp.Id() === defaultBudgetPeriodId;
                }
            });

            // Ensure we found a matching Budget Period
            if (budgetPeriod.length > 0) {

                budgetPeriodDescription = budgetPeriod[0].Description();
                budgetPeriodDescription = budgetPeriodDescription.replace('-', 'to').replace(/\//gm, '-').replace(/\s+/gm, '-');
            }
        }
        if (self.GlDetailView() == false) {
            // Export for the project detail view.
            var fileName = Ellucian.ProjectsAccounting.downloadProjectFileName;
            if (budgetPeriodDescription !== null && budgetPeriodDescription !== "") {
                fileName += '-' + budgetPeriodDescription;
            }
            return fileName + '.csv';
        }
        else {
            // Export for the GL detail view.
            var fileName = "Gl Detail";
            if (self.isProjectDefined()) {
                fileName += '-' + self.Project().Number();
            }
            if (self.GlNumberSelected()) {
                fileName += '-' + self.GlNumberSelected();
            }
            if (budgetPeriodDescription !== null && budgetPeriodDescription !== "") {
                fileName += '-' + budgetPeriodDescription;
            }
            return fileName + '.csv';
        }
    });

    // Hold the selected budget period
    self.SelectedBudgetPeriod = ko.observable();

    // Check if the filter is currently in use
    self.usingFilter = ko.observable(false);

    // Observable boolean to control if the Export Dropdown is expanded/collapsed.
    self.isExportDropdownExpanded = ko.observable(false);

    // Function to expand/collapse the Export Dropdown
    self.toggleExportDropdown = function () {
        self.isExportDropdownExpanded(!self.isExportDropdownExpanded());
    }

    self.pickBudgetPeriod = function () {
        // Only invoke the code below if the user selected a new reporting period.
        if (self.IsPageLoaded() && !self.IsFilterUpdating()) {
            // Update the project with the new reporting period data
            self.getProject(true);

            // Store the reporting period and active line items in browser memory when reporting period changes
            memory.setItem("period", self.SelectedBudgetPeriod());

            self.usingFilter(true);
        }

        // We need to return true to allow the checkboxes to display properly when checked/unchecked.
        return true;
    }

    self.clickTransaction = function (data) {
        if (data.isLinkable()) {
            window.document.location = self.fullDocumentUrl(data);
        }
    };

    self.fullDocumentUrl = function (data) {
        return (data.documentLink() + '/' + self.GlNumberSelected() + '/' + Ellucian.ColleagueFinance.projectId);
    };

    // The file base.view.model.js includes a change to desktop/mobile function ia
    // necessary to change the colspan for the budget cell when going down to mobile

    // "Inherit" the base view model, specify the mobile screen width
    BaseViewModel.call(self, 769);

    // Determine whether or not to show the Actuals or Encumbrances table
    self.ShowEncumbrancesTable = ko.computed(function () {
        // If we're not in detail view then hide (return false)
        if (self.GlDetailView() == false) {
            return false;
        }

        // If we're in detail view but NOT in mobile view then show (return true)
        if (self.GlDetailView() && !self.isMobile()) {
            return true;
        }

        // Lastly, if we're in detail view, AND we're in mobile view then determine whether to show the actuals or encumbrances tab
        if (self.GlDetailView() && self.isMobile() && self.GlDetailTabSelected() == "E") {
            $(".gl-tab-encumbrances").addClass("gl-detail-tab-selected");
            return true;
        }
        else {
            $(".gl-tab-encumbrances").removeClass("gl-detail-tab-selected");
            return false;
        }
    }, self);

    // Determine whether or not to show the Actuals or Encumbrances table
    self.ShowActualsTable = ko.computed(function () {
        // If we're not in detail view then hide (return false)
        if (self.GlDetailView() == false) {
            return false;
        }

        // If we're in detail view but NOT in mobile view then show (return true)
        if (self.GlDetailView() && !self.isMobile()) {
            return true;
        }

        // Lastly, if we're in detail view, AND we're in mobile view then determine whether to show the actuals or encumbrances tab
        if (self.GlDetailView() && self.isMobile() && self.GlDetailTabSelected() == "A") {
            $(".gl-tab-actuals").addClass("gl-detail-tab-selected");
            return true;
        }
        else {
            $(".gl-tab-actuals").removeClass("gl-detail-tab-selected");
            return false;
        }
    }, self);

    self.flatProjectLineItems = ko.computed(function () {
        var lineItems = ko.observableArray([]);

        if (self.GlDetailView() == false) {

            // Export for the project detail view.

            // This is the summary row for the asset line item being processed.
            // Only include first three columns.
            function flattenAssetLineItem(lineItem) {
                lineItems.push({
                    glClass: Ellucian.ProjectsAccounting.assetLabel,
                    lineItem: lineItem.ItemCodeDescription() + " " + lineItem.ItemCode(),
                    formattedGlAccount: "",
                    budget: lineItem.TotalBudget(),
                    actuals: "",
                    encumbrances: "",
                    remaining: "",
                    percentSpent: ""
                });
                flattenGlAccounts(lineItem);
            }

            // This is the summary row for the liability line item being processed.
            // Only include first three columns.
            function flattenLiabilityLineItem(lineItem) {
                lineItems.push({
                    glClass: Ellucian.ProjectsAccounting.liabilityLabel,
                    lineItem: lineItem.ItemCodeDescription() + " " + lineItem.ItemCode(),
                    formattedGlAccount: "",
                    budget: lineItem.TotalBudget(),
                    actuals: "",
                    encumbrances: "",
                    remaining: "",
                    percentSpent: ""
                });
                flattenGlAccounts(lineItem);
            }

            // This is the summary row for the fund balance line item being processed.
            // Only include first three columns.
            function flattenFundBalanceLineItem(lineItem) {
                lineItems.push({
                    glClass: Ellucian.ProjectsAccounting.fundBalanceLabel,
                    lineItem: lineItem.ItemCodeDescription() + " " + lineItem.ItemCode(),
                    formattedGlAccount: "",
                    budget: lineItem.TotalBudget(),
                    actuals: "",
                    encumbrances: "",
                    remaining: "",
                    percentSpent: ""
                });
                flattenGlAccounts(lineItem);
            }

            // This is the summary row for the revenue line item being processed.
            // Do not include actuals/encumbrances.
            function flattenRevenueLineItem(lineItem) {
                lineItems.push({
                    glClass: Ellucian.ProjectsAccounting.revenueLabel,
                    lineItem: lineItem.ItemCodeDescription() + " " + lineItem.ItemCode(),
                    formattedGlAccount: "",
                    budget: lineItem.TotalBudget(),
                    actuals: "",
                    encumbrances: "",
                    remaining: lineItem.BudgetRemainingFormatted(),
                    percentSpent: lineItem.PercentProcessed()
                });
                flattenGlAccounts(lineItem);
            }

            // This is the summary row for the expense line item being processed.
            // Do not include actuals/encumbrances.
            function flattenExpenseLineItem(lineItem) {
                lineItems.push({
                    glClass: Ellucian.ProjectsAccounting.expenseLabel,
                    lineItem: lineItem.ItemCodeDescription() + " " + lineItem.ItemCode(),
                    formattedGlAccount: "",
                    budget: lineItem.TotalBudget(),
                    actuals: "",
                    encumbrances: "",
                    remaining: lineItem.BudgetRemainingFormatted(),
                    percentSpent: lineItem.PercentProcessed()
                });
                flattenGlAccounts(lineItem);
            }

            function flattenGlAccounts(lineItem) {
                // Get the correct label depending on the type of line item being processed.
                var itemGlClass = '';
                switch (lineItem.GlClass()) {
                    case 0:
                        itemGlClass = Ellucian.ProjectsAccounting.expenseLabel;
                        break;
                    case 1:
                        itemGlClass = Ellucian.ProjectsAccounting.assetLabel;
                        break;
                    case 2:
                        itemGlClass = Ellucian.ProjectsAccounting.revenueLabel;
                        break;
                    case 3:
                        itemGlClass = Ellucian.ProjectsAccounting.liabilityLabel;
                        break;
                    case 4:
                        itemGlClass = Ellucian.ProjectsAccounting.fundBalanceLabel;
                        break;
                }

                // If there are amounts pending posting for this line item, include them with the appropriate label.
                if (lineItem.TotalMemos() != 0) {
                    lineItems.push({
                        glClass: itemGlClass,
                        lineItem: lineItem.ItemCodeDescription() + " " + lineItem.ItemCode(),
                        formattedGlAccount: Ellucian.ProjectsAccounting.amountPendingPostingLabel,
                        budget: "",
                        actuals: lineItem.TotalMemoActuals(),
                        encumbrances: lineItem.TotalMemoEncumbrances(),
                        remaining: "",
                        percentSpent: "",
                    });
                }

                // Include all of the GL accounts for this line item
                // Rows for the GL accounts will only have actuals/encumbrances.
                lineItems.pushAll(lineItem.GlAccounts().map(function glAccountsAdapter(item) {
                    return {
                        glClass: itemGlClass,
                        lineItem: lineItem.ItemCodeDescription() + " " + lineItem.ItemCode(),
                        formattedGlAccount: item.FormattedGlAccount(),
                        budget: "",
                        actuals: item.Actuals(),
                        encumbrances: item.Encumbrances(),
                        remaining: "",
                        percentSpent: "",
                    };
                }));
            }

            // Once there is data, go ahead and flatten it (assets, fund balance, liability, revenue, then expenses)
            if (self.isProjectDefined()) {
                ko.utils.arrayMap(self.Project().AssetLineItems(), flattenAssetLineItem);
                ko.utils.arrayMap(self.Project().LiabilityLineItems(), flattenLiabilityLineItem);
                ko.utils.arrayMap(self.Project().FundBalanceLineItems(), flattenFundBalanceLineItem);
                ko.utils.arrayMap(self.Project().RevenueLineItems(), flattenRevenueLineItem);
                ko.utils.arrayMap(self.Project().ExpenseLineItems(), flattenExpenseLineItem);
            }
            return lineItems();
        }
        else {
            // Export for the GL detail view.
            // Find the select GL account in each list of line items and then get its transactions.
            for (var i = 0; i < self.Project().AssetLineItems().length; i++) {
                var projectLineItem = self.Project().AssetLineItems()[i];
                findGlAccount(projectLineItem);
            }

            for (var i = 0; i < self.Project().LiabilityLineItems().length; i++) {
                var projectLineItem = self.Project().LiabilityLineItems()[i];
                findGlAccount(projectLineItem);
            }

            for (var i = 0; i < self.Project().FundBalanceLineItems().length; i++) {
                var projectLineItem = self.Project().FundBalanceLineItems()[i];
                findGlAccount(projectLineItem);
            }

            for (var i = 0; i < self.Project().RevenueLineItems().length; i++) {
                var projectLineItem = self.Project().RevenueLineItems()[i];
                findGlAccount(projectLineItem);
            }

            for (var i = 0; i < self.Project().ExpenseLineItems().length; i++) {
                var projectLineItem = self.Project().ExpenseLineItems()[i];
                findGlAccount(projectLineItem);
            }

            function ExportRow(type, documentRefNo, transactionDate, description, amount) {
                var rowData = {
                    "Type": type,
                    "Document": "'" + documentRefNo + "'",
                    "Date": transactionDate,
                    "Description": description,
                    "Amount": amount
                };
                if (documentRefNo.length === 0) {
                    rowData.Document = "";
                }
                return rowData;
            }

            // Flatten and add rows from a list of transactions
            function flattenTransactions(transactions, typeLabel) {
                for (var i = 0; i < transactions.length; i++) {
                    var transaction = transactions[i];
                    lineItems.push(new ExportRow(typeLabel, transaction.ReferenceNumber(), transaction.Date(), transaction.Description(), transaction.Amount()));
                }
            }

            // Try to find the selected GL account in this list of line items.
            function findGlAccount(projectLineItem) {
                for (var j = 0; j < projectLineItem.GlAccounts().length; j++) {
                    var glAccount = projectLineItem.GlAccounts()[j];
                    if (glAccount.GlAccount() == self.GlNumberSelected()) {
                        flattenTransactions(glAccount.EncumbranceTransactions(), Ellucian.ProjectsAccounting.encumbrancesLabel);
                        flattenTransactions(glAccount.ActualTransactions(), Ellucian.ProjectsAccounting.actualsLabel);
                    }
                }
            }

            return lineItems();
        }
    }).extend({ deferred: true });

    self.csvColumns = ko.computed(function () {
        if (self.GlDetailView() == false) {
            // Export for the project detail view.
            var columns = [
                "glClass",
                "lineItem",
                "formattedGlAccount",
                "budget",
                "actuals",
                "encumbrances",
                "remaining",
                "percentSpent"
            ];
        }
        else {
            // Export for the GL detail view.
            var columns = [
                "Type",
                "Document",
                "Date",
                "Description",
                "Amount"
            ];
        }
        return columns;
    });

    self.csvColumnHeaders = ko.computed(function () {
        if (self.GlDetailView() == false) {
            // Export for the project detail view.
            var columns = [
                Ellucian.ProjectsAccounting.glClassTitle,
                Ellucian.ProjectsAccounting.projectLineItemTitle,
                Ellucian.ProjectsAccounting.glAccountTitle,
                Ellucian.ProjectsAccounting.budgetTitle,
                Ellucian.ProjectsAccounting.actualsTitle,
                Ellucian.ProjectsAccounting.encumbrancesTitle,
                Ellucian.ProjectsAccounting.remainingTitle,
                Ellucian.ProjectsAccounting.percentSpentReceivedTitle
            ];
        }
        else {
            // Export for the GL detail view.
            var columns = [
                Ellucian.ProjectsAccounting.transactionTypeHeader,
                Ellucian.ProjectsAccounting.documentIdHeader,
                Ellucian.ProjectsAccounting.dateHeader,
                Ellucian.ProjectsAccounting.descriptionHeader,
                Ellucian.ProjectsAccounting.amountHeader
            ];
        }
        return columns;
    });

    // Change desktop structure to mobile structure
    this.changeToMobile = function () {
        // Change the colspan of the actuals and encumbrance headers
        $(".actuals-encumbrances-header.gl-details-align-left").attr("colspan", "1");

        // set tab panel accessibility at mobile size only
        $("#encumbrance-panel, #actual-panel").attr("role", "tabpanel");
        $("#encumbrance-panel").attr("aria-hidden", "false");
        $("#actual-panel").attr("aria-hidden", "true");
    }

    // Change mobile structure back to desktop view
    this.changeToDesktop = function () {
        // Change the colspan of the actuals and encumbrance headers
        $(".actuals-encumbrances-header.gl-details-align-left").attr("colspan", "3");

        // remove tab panel accessibility at desktop size
        $("#encumbrance-panel, #actual-panel").removeAttr("role aria-hidden");
    }


    // Set the active line items in an array
    self.setLineItems = function () {
        var activeLineItems = memory.getItem("activeLineItems");
        if (activeLineItems !== null && activeLineItems !== "") {
            var activeLineItemsArray = activeLineItems.split(",");

            if (activeLineItemsArray !== null && activeLineItemsArray.length > 0) {
                // Iterate through array to evaluate which rows need to be expanded
                for (var i = 0; i < activeLineItemsArray.length; i++) {
                    var rowId = "#" + activeLineItemsArray[i];

                    // pass in jQuery element of row previously selected, animate parameter is false
                    Ellucian.ColleagueFinance.initializeActiveRows($(rowId));
                }
            }
        }

    };

    self.toggleView = function (glNumber) {
        if (self.GlDetailView()) {
            // We're currently in GL Detail view, so change to the Line Items view.
            self.GlNumberSelected('');
            self.GlDetailView(false);
        }
        else {
            // We're currently in Line Items view, so change to the GL Detail view.
            self.GlDetailTabSelected("E");
            self.GlNumberSelected(glNumber);
            self.GlDetailView(true);
        }
    }

    self.clickEncumbrancesTab = function () {
        if (self.GlDetailTabSelected() != "E") {
            self.GlDetailTabSelected("E");

            // Set the ARIA attributes for the screen reader
            $(".gl-tab-encumbrances").attr("aria-selected", "true");
            $(".gl-tab-actuals").attr("aria-selected", "false");
            $("#encumbrance-panel").attr("aria-hidden", "false");
            $("#actual-panel").attr("aria-hidden", "true");
        }
    }

    self.clickActualsTab = function () {
        if (self.GlDetailTabSelected() != "A") {
            self.GlDetailTabSelected("A");

            // Set the ARIA attributes for the screen reader
            $(".gl-tab-encumbrances").attr("aria-selected", "false");
            $(".gl-tab-actuals").attr("aria-selected", "true");
            $("#encumbrance-panel").attr("aria-hidden", "true");
            $("#actual-panel").attr("aria-hidden", "false");
        }
    }

    // Execute AJAX request to get the project in context
    self.getProject = function (budgetPeriodChange) {
        // Initialize the query URL
        var projectQueryUrl = Ellucian.ProjectsAccounting.getProjectDetailAsyncActionUrl + '?id=' + Ellucian.ColleagueFinance.projectId;

        // Include the sequence number if we have one
        var sequenceNumber = '';
        if (!self.IsPageLoaded()) {
            if (memory.getItem("period") && memory.getItem("period") !== "0") {
                projectQueryUrl += "&sequenceNumber=" + memory.getItem("period");
            }
            else {
                // Do nothing and default to All Activity
            }
        }
        else {
            if (budgetPeriodChange && self.SelectedBudgetPeriod() !== "0") {
                projectQueryUrl += "&sequenceNumber=" + self.SelectedBudgetPeriod();
            }
        }

        $.ajax({
            url: projectQueryUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                // Show the filter spinner
                self.IsFilterUpdating(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    if (!budgetPeriodChange) {
                        // Generate budget periods, including All Activity (0)
                        var periods = data.Project.BudgetPeriods;
                        self.GenerateBudgetPeriodsSelection(periods);
                    }

                    // Execute the knockout mapping, which takes the response object and uses the 
                    // explicit mapping and models to update projectsViewModelInstance
                    ko.mapping.fromJS(data, projectMapping, projectDetailViewModelInstance);

                    // If navigating to a new document, reset the period and active line items
                    var lastProjectVisited = memory.getItem("lastProjectVisited");
                    var currentProjectNumber = projectDetailViewModelInstance.Project().Number();
                    if (!lastProjectVisited) {
                        memory.setItem("lastProjectVisited", currentProjectNumber);
                    }
                    else if (currentProjectNumber !== lastProjectVisited) {
                        memory.setItem("period", "");
                        memory.setItem("activeLineItems", "");
                        memory.setItem("lastProjectVisited", currentProjectNumber);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Defualt message for the following errors: 
                //  - 400: Bad request
                //  - 403: Forbidden
                //  - Unknown status code
                var message = unableToLoadProjectText;

                // Resource not found
                if (jqXHR.status == 404) {
                    message = resourceNotFoundText;
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            },
            complete: function () {
                // Once the ajax request is completed (regardless of success/error), set isLoaded to true, which
                // disables any spinners/loading icons and other related UI updates.
                self.IsPageLoaded(true);

                // Hide the filter spinner
                self.IsFilterUpdating(false);


                // Display the project.  We need this because the 'isLoaded' attribute is not usable BECAUSE we call the 'applyBindings' method
                // after calling the 'ko.mapping.fronJS' method. We need to call the methods in that order BECAUSE to do otherwise causes our
                // Project/LineItems objects to not be mapped correctly. This arose BECAUSE 'ko.mapping.fromJS' seems to only process object
                // arrays as opposed to single objects.
                $("#projects-details").show();

                // If the budget period has not changed and is not null, set it to the period stored in memory
                if (!budgetPeriodChange) {
                    if (memory.getItem("period") !== null) {
                        self.SelectedBudgetPeriod(memory.getItem("period"));
                    }
                }

                // Expand selected line items using browser memory
                self.setLineItems();

                // Set focus to the reporting period filter if it's currently in use.
                if (self.usingFilter() == true) {
                    $("#reporting-period-filter").focus();
                }

                // Check to see if page has gone down to mobile size on page load to alter colspan
                projectDetailViewModelInstance.checkForMobile(window, document);

                // Reset the click handler observable so we re-execute the click binding for GL Detail transactions
                self.GlDetailTabSelected("E");
                $(".gl-tab-encumbrances").addClass("gl-detail-tab-selected");
                $(".gl-tab-actuals").removeClass("gl-detail-tab-selected");

                // Toggle to the details view if we're coming back from a document
                if (Ellucian.ColleagueFinance.glNumber) {
                    if (!self.GlDetailView()) {
                        self.toggleView(Ellucian.ColleagueFinance.glNumber);
                    }
                }
            }
        });
    };
}