﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

$(document).ready(function () {
    var proxyDialogViewModelInstance = new BaseViewModel(768);
    ko.applyBindings(proxyDialogViewModelInstance, document.getElementById("proxy-selection"));
    ko.applyBindings(proxyDialogViewModelInstance, document.getElementById("userOptions"));
    // Log off options may not be rendered (e.g. when using Portal)
    var logOffOptionsElement = document.getElementById("logOffOptions");
    if (logOffOptionsElement !== null)
    {
        ko.applyBindings(proxyDialogViewModelInstance, logOffOptionsElement);
    }

    if (!$("#proxy-banner").length && $("#home").length) {
        proxyDialogViewModelInstance.getProxySubjects();
    }
});
