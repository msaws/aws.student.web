﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
var personProxyViewModelMapping = {
    'ProxyUsers': {
        key: function (data) {
            return ko.utils.unwrapObservable(data.Id);
        },
        update: function (options) {
            return new proxyUserModel(options);
        }
    }
};

var proxyUserModelMapping = {
    'PermissionGroups': {
        update: function (options) {
            return new permissionGroupModel(options);
        }
    },
    'DemographicInformation': {
        update: function (options) {
            return new demographicInfoModel(options);
        }
    }
};

var permissionGroupMapping = {
    'Permissions': {
        update: function (options) {
            return new permissionsModel(options);
        }
    }
};

// Creates a proxy user object
function proxyUserModel(options) {

    // Explicitly set context of "self"
    var self = this;

    // Map default properties of the inbound JS model to this view model
    ko.mapping.fromJS(options.data, proxyUserModelMapping, this);

    // Observable for title text on user's photo
    self.photoTitleText = ko.observable(personProxyPhotoTitleText.format(self.Name()));

    // Observable for edit button aria text for user
    self.editButtonAriaText = ko.observable(personProxyEditAccessButtonText.format(self.Name()));

    // Observable for offscreen legend text for access level radio buttons
    self.legendText = ko.observable(proxyAccessLevelLegend.format(self.Name()));

    // Observable for tracking access level
    self.accessLevel = ko.observable();

    // Controls access level based on selection
    self.accessController = ko.computed(function () {
        if (self.accessLevel()) {
            switch(self.accessLevel()) {
                case 'complete':
                    self.selectAll();
                    break;
                case 'select':
                    self.enableAll();
                    break;
                case 'none':
                    self.removeAll();
                    break;
                default:
                    break;
            }
        }
    });

    // Function to handle "Select All" functionality globally
    self.selectAll = function () {
        ko.utils.arrayForEach(self.PermissionGroups(), function (group) {
            group.disableSelectAll(true);
            ko.utils.arrayForEach(group.Permissions(), function (permission) {
                permission.isDisabled(true);
                if (permission.isVisible()) {
                    permission.IsAssigned(true);
                } else {
                    permission.IsAssigned(false);
                }
            });
        });
    };

    // Function to handle enabling all checkboxes
    self.enableAll = function () {
        ko.utils.arrayForEach(self.PermissionGroups(), function (group) {
            group.disableSelectAll(false);
            ko.utils.arrayForEach(group.Permissions(), function (permission) {
                permission.isDisabled(false);
            });
        });
    };

    // Function to handle "Remove All" functionality globally
    self.removeAll = function () {
        ko.utils.arrayForEach(self.PermissionGroups(), function (group) {
            ko.utils.arrayForEach(group.Permissions(), function (permission) {
                permission.isDisabled(true);
                permission.IsAssigned(false);
            });
        });
    };

    // Validated observables for users being added
    if (self.Id() === 'NEWPERSON') {

        self.Relationship = ko.observable().extend({
            required: { message: requiredFieldMessageString }
        });

        var validatedFields = ko.utils.arrayMap(self.DemographicInformation(), function (di) {
            return di.Value;
        });

        var emailField = ko.utils.arrayFirst(self.DemographicInformation(), function (di) {
            return di.Type() == 8 && di.Id() == "email_address";
        });
        var confirmEmailField = ko.utils.arrayFirst(self.DemographicInformation(), function (di) {
            return di.Type() == 8 && di.Id() == "confirm_email_address";
        });
        if (emailField && confirmEmailField) {
            confirmEmailField.Value.extend({ equal: { onlyIf: function () { return confirmEmailField.Type() == 8; }, params: emailField.Value, message: emailMismatchMessageString } });
        }

        var govtIdField = ko.utils.arrayFirst(self.DemographicInformation(), function (di) {
            return di.Type() == 15 && di.Id() == "ssn";
        });
        var confirmGovtIdField = ko.utils.arrayFirst(self.DemographicInformation(), function (di) {
            return di.Type() == 15 && di.Id() == "confirm_ssn";
        });
        if (govtIdField && confirmGovtIdField) {
            confirmGovtIdField.Value.extend({ equal: { onlyIf: function () { return confirmGovtIdField.Type() == 15; }, params: govtIdField.Value, message: governmentIdMismatchMessageString } });
        }

        validatedFields.push(self.Relationship);

        self.searchCriteria = ko.validatedObservable(validatedFields);
    }

    // Validated observables for an existing relation with no email address
    if (self.Id() !== 'NEWPERSON') {

        var validatedFields = ko.utils.arrayMap(self.DemographicInformation(), function (di) {
            return di.Value;
        });

        var emailField = ko.utils.arrayFirst(self.DemographicInformation(), function (di) {
            return di.Type() == 8 && di.Id() == "email_address";
        });
        var confirmEmailField = ko.utils.arrayFirst(self.DemographicInformation(), function (di) {
            return di.Type() == 8 && di.Id() == "confirm_email_address";
        });
        if (emailField && confirmEmailField) {
            confirmEmailField.Value.extend({ equal: { onlyIf: function () { return confirmEmailField.Type() == 8; }, params: emailField.Value, message: emailMismatchMessageString } });
        }

        self.searchCriteria = ko.validatedObservable(validatedFields);
    }

    // Set the effective date to the current date if not already set
    if (!options.data.EffectiveDate) {
        self.EffectiveDate(new Date());
    }

    self.canEditAccess = ko.computed(function () {
        return !self.IsCandidate() && !self.ReauthorizationIsNeeded();
    })
};

// Creates a demographic information object
function demographicInfoModel(options) {

    // Explicitly set context of "self"
    var self = this;

    // Map default properties of the inbound JS model to this view model
    ko.mapping.fromJS(options.data, proxyUserModelMapping, this);

    // Add validation to field value
    if (options.parent.Id() === 'NEWPERSON' || options.parent.Email() === null) {
        self.Value.extend({
            required: { onlyIf: self.IsRequired, message: requiredFieldMessageString },
            minLength: { onlyIf: self.IsRequired, params: self.MinimumLength(), message: minimumLengthMessageString },
            maxLength: { params: self.MaximumLength(), message: maximumLengthMessageString },
            email: { onlyIf: function () { return self.Type() == 8; }, message: invalidEmailAddressMessageString },
            date: { onlyIf: function () { return self.Type() == 13; }, message: invalidDateMessageString },
        });
    }

    // 
    self.maskStart = ko.computed(function () {
        if (self.Value() && self.Value().length > 4) {
            return self.Value().length - 4;
        }
        return 0;
    });

    // Flag indicating whether or not masked field is unmasked
    self.shown = false;

    // Function that masks/unmasks a masked field
    self.showValue = function () {
        if (!self.shown) {
            $('#proxy-' + self.Id() + '-field').mouseenter();
        }
        else {
            $('#proxy-' + self.Id() + '-field').mouseleave();
        }
        self.shown = !self.shown;
    }
};

// Creates a new permission group object
function permissionGroupModel(options) {

    // Explicitly set context of "self"
    var self = this;

    // Observable for tracking the name of the proxy user to whom this permission group belongs
    self.groupOwnerName = ko.observable(options.parent.Name());

    // Map default properties of the inbound JS model to this view model
    ko.mapping.fromJS(options.data, permissionGroupMapping, this);

    // Pure computed to determine resource string
    self.permissionGroupTooltipText = ko.pureComputed(function () {
        switch (options.data.Id) {
            case "SF":
                return ProxyPermissionGroupTooltipTextSF;
                break;
            case "FA":
                return ProxyPermissionGroupTooltipTextFA;
                break;
            case "CORE":
                return ProxyPermissionGroupTooltipTextCore;
                break;
            default:
                break;
        }
    });

    // Flag indicating whether or not the permissions group is visible
    self.isVisible = ko.computed(function () {
        var assignedPermissions = ko.utils.arrayFilter(self.Permissions(), function (permission) {
            return permission.IsAssigned();
        });
        return assignedPermissions.length > 0;
    });

    // Observable to handle "Select All" functionality within the permission group
    self.selectAll = ko.computed({
        read: function () {
            var permissions = ko.utils.arrayFilter(self.Permissions(), function (permission) {
                return permission.IsEnabled();
            });
            for (var i = 0, l = permissions.length; i < l; i++)
                if (!permissions[i].IsAssigned()) return false;
            return true;
        },
        write: function (value) {
            ko.utils.arrayForEach(self.Permissions(), function (permission) {
                if (permission.IsEnabled()) {
                    permission.IsAssigned(value);
                }
            });
        }
    });
	
	// Flag indicating whether or not to display the select all option
	self.displaySelectAll = ko.computed(function() {
		var enabledPermissions = ko.utils.arrayFilter(self.Permissions(), function (permission) {
		    return permission.isVisible();
		});
		return enabledPermissions.length > 1;
	});

    // Flag indicating whether or not to disable the select all option
	self.disableSelectAll = ko.observable(false);
};

// Creates a new permission object
function permissionsModel(options) {

    // Explicitly set context of "self"
    var self = this;

    // Map default properties of the inbound JS model to this view model
    ko.mapping.fromJS(options.data, {}, this);

    // Observable for tracking a permission's visibility
    self.isVisible = ko.computed(function () {
        return self.IsInitiallyAssigned() || self.IsEnabled();
    });

    // Observable for tracking whether or not a permission may be edited
    self.isDisabled = ko.observable(!self.IsInitiallyAssigned() && !self.IsAssigned() && !self.IsEnabled());
};

