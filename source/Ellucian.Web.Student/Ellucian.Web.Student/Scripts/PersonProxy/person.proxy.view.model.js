﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
function personProxyViewModel() {

    // Initialize the model
    var self = this;

    // Flag indicating whether or not the page content is ready to be displayed
    self.showUI = ko.observable(false);

    // Flag indicating whether or not an ajax request is being processed that should block the page
    // This needs to be initialized to true to prevent the AJAX request from firing when the 
    // bindings are initially applied.
    self.isLoading = ko.observable(true);

    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
    BaseViewModel.call(self, 768);

    // Overrides for base view model change functions to remove alerts
    self.changeToDesktop = function () {

    }
    self.changeToMobile = function () {

    }

    // Action throbber observables - used when submitting proxy updates
    self.actionThrobberMessage = ko.observable(personProxyActionThrobberMessage);

    // Proxy access edit throbber observables - used when loading proxy access edit form
    self.editFormThrobberMessage = ko.observable(personProxyEditProxyAccessThrobberMessage);

    // Proxy access edit throbber observables - used when loading proxy access edit form
    self.searchingThrobberMessage = ko.observable(personProxySearchingThrobberMessage);

    // Proxy access edit throbber observables - used when loading proxy access edit form
    self.reauthorizingThrobberMessage = ko.observable(loadingReauthorizationSpinnerMessage);

    // Initialize C# model observables for initial page render
    self.AnyPermissionsEnabled = ko.observable(false);
    self.DisclosureAgreementText = ko.observable();
    self.AddProxyHeaderText = ko.observable();
    self.ProxyFormHeaderText = ko.observable();
    self.ReauthorizationText = ko.observable();
    self.ReauthorizationIsNeeded = ko.observable();
    self.PersonProxyIsEnabled = ko.observable(true);
    self.ProxyUsers = ko.mapping.fromJS([]);
    self.RelationshipTypes = ko.observableArray();

    // Collection of proxy users with at least one assigned permission
    self.activeProxyUsers = ko.observableArray();

    // Collection of proxy users with no assigned permissions
    self.unassignedProxyUsers = ko.observableArray();

    // Observable to keep track of selected proxy user when adding a new proxy
    self.selectedProxyId = ko.observable();
    self.selectedProxy = ko.computed(function () {
        if (self.selectedProxyId()) {
            return ko.utils.arrayFirst(self.unassignedProxyUsers(), function (user) {
                return user.Id() == self.selectedProxyId();
            });
        }
    });

    // Observable to keep track of selected proxy user when editing an active proxy
    self.proxyUserBeingChangedId = ko.observable();
    self.proxyUserBeingChanged = ko.computed(function () {
        if (self.proxyUserBeingChangedId()) {
            return ko.utils.arrayFirst(self.activeProxyUsers(), function (user) {
                return user.Id() == self.proxyUserBeingChangedId();
            });
        }
    });

    // Function to set default selected access level option when editing proxy
    self.setUserBeingChangedAccess = function () {
        var totalPerms = 0;
        var enabledPerms = 0;
        if (self.proxyUserBeingChanged()) {
            ko.utils.arrayForEach(self.proxyUserBeingChanged().PermissionGroups(), function (group) {
                ko.utils.arrayForEach(group.Permissions(), function (permission) {
                    if (permission.IsEnabled()) {
                        totalPerms += 1;
                        if (permission.IsAssigned()) {
                            enabledPerms += 1;
                        }
                    }
                });
            });
            switch (enabledPerms) {
                case totalPerms:
                    self.proxyUserBeingChanged().accessLevel('complete');
                    break;
                default:
                    self.proxyUserBeingChanged().accessLevel('select');
                    break;
            }
        }
    }

    // Message indicating that the user has no active proxies
    self.noActiveProxiesMessage = ko.computed(function () {
        if (self.AnyPermissionsEnabled()) {
            return "{0}  {1}".format(personProxyNoActiveProxiesMessage, personProxyNoActiveProxiesAddAProxyMessage);
        };
        return personProxyNoActiveProxiesMessage;
    });

    // Flag indicating whether or not the principal has any active proxy users
    self.hasActiveProxies = ko.computed(function () {
        if (self.activeProxyUsers()) {
            return self.activeProxyUsers().length > 0;
        }
    });

    // Flag indicating whether or not the principal has any unassigned proxy users
    self.hasUnassignedProxies = ko.computed(function () {
        if (self.unassignedProxyUsers()) {
            return self.unassignedProxyUsers().length > 0;
        }
    });

    // Flag indicating whether or not the principal has any users to whom they can designate proxy access
    self.hasAvailableProxies = ko.computed(function () {
        return self.hasActiveProxies() || self.hasUnassignedProxies();
    });

    // Flag indicating whether or not an ajax request is being processed to update person proxy information
    self.isUpdating = ko.observable(false);

    // Flag indicating whether or not the user has given consent on the disclosure agreement
    self.consentGiven = ko.observable(false);

    // Flag indicating whether or not the user has given consent on the disclosure agreement
    self.reauthorizationConsentGiven = ko.observable(false);

    // Function to retrieve current proxy information
    self.getProxyInformation = function () {
        $.ajax({
            url: personProxyActionUrl,
            type: "GET",
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function () {
                self.isLoading(true);
                self.showUI(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, personProxyViewModelMapping, self);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: personProxyUnableToLoadMessage.format(name), type: "error", flash: true });
                }
            },
            complete: function () {
                self.checkForMobile(window, document);
                self.sortProxyUsers();

                // Make all tables in view responsive
                $("table").makeTableResponsive();

                // Stop the loading throbber and display the UI
                self.isLoading(false);
                self.showUI(true);
                if (!self.DisclosureAgreementText()) {
                    self.consentGiven(true);
                }
            }
        });
    };

    // Collection of matching persons for searches
    self.searchResults = ko.observable();

    // Flag indicating whether or not all search criteria were entered
    self.searchCriteriaInvalid = ko.observable(false);

    // Flag indicating whether or not any permissions is assigned
    self.noPermissionsAssigned = ko.computed(function () {
        if (self.selectedProxyId() && self.selectedProxy() && self.selectedProxy().PermissionGroups()) {
            return !anyPermissionsChanged(self.selectedProxy().PermissionGroups());
        }
        return true;
    });

    // Function to validate search criteria
    self.validateSearchCriteria = function () {
        self.selectedProxyId();
        if (self.selectedProxyId() && self.selectedProxyId() === 'NEWPERSON') {
            self.searchCriteriaInvalid(self.noPermissionsAssigned() || !self.consentGiven() || !self.selectedProxy().searchCriteria().isValid());
        } else {
            self.searchCriteriaInvalid(self.noPermissionsAssigned() || !self.consentGiven());
        }
    }

    // Function to search for existing users that match specified criteria
    self.searchForMatches = function () {

        //Convert the person proxy information to a JSON string
        var json = { criteriaJson: ko.toJSON(self.selectedProxy()) };

        //Post the data
        $.ajax({
            url: searchForMatchesActionUrl,
            data: JSON.stringify(json),
            type: "POST",
            dataType: "json",
            contentType: jsonContentType,
            beforeSend: function (data) {
                self.searchResultsDialogIsLoading(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.searchResults(ko.mapping.fromJS(data, personProxyViewModelMapping));
                    self.searchResultsDialogIsLoading(false);
                    self.searchResultsDialogIsDisplayed(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.textStatus != 0) {
                    self.searchResultsDialogIsLoading(false);
                    $('#notificationHost').notificationCenter('addNotification', { message: proxySearchNoResultsMessage, type: "error", flash: true });
                }
            },
            complete: function () {
                $("table").makeTableResponsive();
            }
        });
    };

    self.previousSubmit = ko.observable(false);

    // Function to verify search criteria, then request password verification
    self.startSearchWorkflow = function () {
        self.validateSearchCriteria();
        self.previousSubmit(true);
        if (!self.searchCriteriaInvalid()) {
            self.showVerifyPasswordDialog();
        }
    }

    // subscribe to isMobile to drive DOM changes
    self.disableSubmit = ko.computed(function () {
        if (self.selectedProxy() && !self.selectedProxy().CanReceiveProxyAccess()) {
            return true;
        }

        if (self.previousSubmit()) {
            if (self.selectedProxyId() && self.selectedProxyId() === 'NEWPERSON') {
                return (self.noPermissionsAssigned() || !self.consentGiven() || !self.selectedProxy().searchCriteria().isValid());
            }
            return (self.noPermissionsAssigned() || !self.consentGiven());
        }
        return false;
    });

    self.updateEditedProxyInformation = function () {
        self.updateProxyInformation(self.proxyUserBeingChanged());
    };

    // Function to post person proxy information changes
    self.updateProxyInformation = function (user) {

        if (anyPermissionsChanged(user.PermissionGroups()) && self.consentGiven() && (user.searchCriteria().isValid())) {
            //Convert the person proxy information to a JSON string
            var json = { proxyUserJson: ko.toJSON(user), consentTextJson: ko.toJSON(self.DisclosureAgreementText()) };

            //Post the data
            $.ajax({
                url: updatePersonProxyInformationActionUrl,
                data: JSON.stringify(json),
                type: "POST",
                dataType: "json",
                contentType: jsonContentType,
                beforeSend: function (data) {
                    self.editDialogIsDisplayed(false);
                    self.isUpdating(true);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.ProxyUsers.removeAll();
                        ko.mapping.fromJS(data.Data, personProxyViewModelMapping, self);
                        $('#notificationHost').notificationCenter('addNotification', { message: personProxySuccessfulUpdateMessage.format(user.Name()), type: "success", flash: true });
                        self.sortProxyUsers();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.textStatus != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: personProxyFailedUpdateMessage.format(user.Name()), type: "error", flash: true });
                        self.selectedProxyId(null);
                    }
                },
                complete: function () {
                    $("table").makeTableResponsive();
                    self.isUpdating(false);
                    self.proxyUserBeingChangedId(null);
                    self.previousSubmit(false);
                }
            });
        } else {
            self.searchCriteriaInvalid(true);
            self.previousSubmit(true);
        }
    };

    // Flag indicating whether or not the reauthorization dialog is displayed
    self.reauthorizationDialogIsDisplayed = ko.observable(false);

    // Flag indicating whether or not the reauthorization is being processed
    self.reauthorizationProcessing = ko.observable(false);

    // Function to display disclosure agreement in a dialog for reauthorization
    self.openReauthorizationDialog = function () {
        if (!self.DisclosureAgreementText()) {
            self.reauthorizationConsentGiven(true);
            self.reauthorizeAccess();
        } else {
            self.reauthorizationDialogIsDisplayed(true);
        }
    }

    // Override "base" password success function
    self.verifyingPasswordSuccessFunction = function () {
        self.searchForMatches();
    }

    // The button label when reauthorization is required
    self.reauthorizationButtonLabel = ko.computed(function () {
        if (!self.DisclosureAgreementText()) {
            return reauthorizeAccessButtonLabel;
        }
        return reviewDisclosureAgreementButtonLabel;
    })

    // Function to post reauthorization
    self.reauthorizeAccess = function () {

        //Convert the person proxy information to a JSON string
        var json = { activeProxiesJson: ko.toJSON(self.activeProxyUsers()), consentTextJson: ko.toJSON(self.DisclosureAgreementText()) };

        //Post the data
        $.ajax({
            url: reauthorizeAccessActionUrl,
            data: JSON.stringify(json),
            type: "POST",
            dataType: "json",
            contentType: jsonContentType,
            beforeSend: function (data) {
                self.reauthorizationDialogIsDisplayed(false);
                self.reauthorizationProcessing(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.ProxyUsers.removeAll();
                    ko.mapping.fromJS(data.Data, personProxyViewModelMapping, self);
                    $('#notificationHost').notificationCenter('addNotification', { message: reauthorizationProcessedMessage, type: "success", flash: true });
                    self.sortProxyUsers();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.textStatus != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: reauthorizationFailureMessage, type: "error", flash: true });
                }
            },
            complete: function () {
                $("table").makeTableResponsive();
                self.reauthorizationProcessing(false);
            }
        });
    }

    // Function to cancel pending proxy information changes
    self.cancelChanges = function () {
        ko.utils.arrayForEach(self.selectedProxy().PermissionGroups(), function (group) {
            ko.utils.arrayForEach(group.Permissions(), function (permission) {
                permission.IsAssigned(false);
            });
        });
        self.resetAddProxySection();
        self.selectedProxy().accessLevel(null);
        self.selectedProxyId(undefined);
        self.consentGiven(false);
    };

    // Flag indicating whether or not the proxy access edit form is loading
    self.editDialogIsLoading = ko.observable(false);

    // Flag indicating whether or not the proxy access edit form is displayed
    self.editDialogIsDisplayed = ko.observable(false);

    // Function to display edit proxy access dialog box
    self.editProxyAccess = function (proxyUser) {

        // Find the active proxy user from the supplied proxy ID
        var proxyUserInfo = ko.utils.arrayFirst(self.activeProxyUsers(), function (user) {
            return user.Id() == proxyUser.Id();
        });

        // Convert the person proxy information to a JSON string
        var json = { proxyInformationJson: ko.toJSON(proxyUserInfo) };

        $.ajax({
            url: getProxyInfoForEditActionUrl,
            data: JSON.stringify(json),
            type: "POST",
            dataType: "json",
            contentType: jsonContentType,
            beforeSend: function () {
                self.resetAddProxySection();
                self.selectedProxyId(null);
                self.editDialogIsLoading(true);
                self.editDialogIsDisplayed(false);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, personProxyViewModelMapping);
                    self.proxyUserBeingChangedId(data.Id);
                    self.setUserBeingChangedAccess();
                    self.consentGiven(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.textStatus != 0) {
                    // Add error notification
                    $('#notificationHost').notificationCenter('addNotification', { message: personProxyUnableToEditMessage, type: "error", flash: true });
                }
            },
            complete: function () {
                $("table").makeTableResponsive();
                self.editDialogIsLoading(false);
                self.editDialogIsDisplayed(true);
            }
        });
    }

    // Function to close the edit proxy access dialog box
    self.closeEditDialog = function () {
        ko.utils.arrayForEach(self.proxyUserBeingChanged().PermissionGroups(), function (group) {
            ko.utils.arrayForEach(group.Permissions(), function (permission) {
                permission.IsAssigned(permission.IsInitiallyAssigned());
            });
        });
        self.editDialogIsDisplayed(false);
        self.proxyUserBeingChanged().accessLevel(null);
        self.proxyUserBeingChangedId(null);
    };

    // Flag indicating whether or not the search results dialog is loading
    self.searchResultsDialogIsLoading = ko.observable(false);

    // Flag indicating whether or not the search results dialog is displayed
    self.searchResultsDialogIsDisplayed = ko.observable(false);

    // Function to create a relationship and grant proxy access to the specified user
    self.processSearchResults = function () {

        //Convert the person proxy information to a JSON string
        var json = { resultsJson: ko.toJSON(self.searchResults()), consentTextJson: ko.toJSON(self.DisclosureAgreementText()) };

        //Post the data
        $.ajax({
            url: processSearchResultsActionUrl,
            data: JSON.stringify(json),
            type: "POST",
            dataType: "json",
            contentType: jsonContentType,
            beforeSend: function (data) {
                self.searchResultsDialogIsDisplayed(false);
                self.isUpdating(true);
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.ProxyUsers.removeAll();
                    ko.mapping.fromJS(data.Data, personProxyViewModelMapping, self);
                    $('#notificationHost').notificationCenter('addNotification', { message: proxyRequestSuccessfulMessage, type: "success", flash: true });
                    self.sortProxyUsers();
                    self.resetAddProxySection();
                    self.selectedProxyId(null);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.textStatus != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: JSON.parse(jqXHR.responseText), type: "error", flash: true });
                    self.resetAddProxySection();
                    self.selectedProxyId(null);
                }
            },
            complete: function () {
                $("table").makeTableResponsive();
                self.isUpdating(false);
            }
        });
    };

    self.submitPersonSearchDialogButton = {
        id: "submit-person-choice-dialog-button",
        isPrimary: true,
        title: Ellucian.Proxy.ButtonLabels.confirmSelectedPersonDialogButtonLabel,
        callback: self.processSearchResults
    };

    // Observable to enable or disable the save button on the edit proxy access dialog box
    self.saveUpdatesButtonEnabled = ko.computed(function () {
        var changed = 0;

        if (self.proxyUserBeingChanged()) {
            ko.utils.arrayForEach(self.proxyUserBeingChanged().PermissionGroups(), function (group) {
                ko.utils.arrayForEach(group.Permissions(), function (permission) {
                    if (permission.IsInitiallyAssigned() != permission.IsAssigned()) {
                        changed += 1;
                    }
                });
            });
        }
        return (changed > 0);
    });

    // Function to categorize proxy users as active or unassigned
    self.sortProxyUsers = function () {
        var activeProxies = [];
        var unassignedProxies = [];
        ko.utils.arrayForEach(self.ProxyUsers(), function (user) {
            if (user.IsCandidate()) {
                activeProxies.push(user);
            } else {
                var groupsWithWorkflows = [];
                ko.utils.arrayForEach(user.PermissionGroups(), function (group) {
                    ko.utils.arrayForEach(group.Permissions(), function (permission) {
                        permission.IsInitiallyAssigned(permission.IsAssigned());
                    });
                    var assignedWorkflows = ko.utils.arrayFilter(group.Permissions(), function (workflow) {
                        return workflow.IsAssigned();
                    });
                    if (assignedWorkflows.length > 0) {
                        groupsWithWorkflows.push(group);
                    }
                });
                if (groupsWithWorkflows.length > 0) {
                    activeProxies.push(user);
                } else {
                    if (user.Relationship() != "") {
                        unassignedProxies.push(user);
                    }
                }
            }
        });

        self.activeProxyUsers(activeProxies);
        self.unassignedProxyUsers(unassignedProxies);
    }

    // Function to reset unassigned proxies' permissions when changing between selections
    self.resetAddProxySection = function (data) {
        ko.utils.arrayForEach(self.unassignedProxyUsers(), function (user) {
            ko.utils.arrayForEach(user.PermissionGroups(), function (group) {
                ko.utils.arrayForEach(group.Permissions(), function (permission) {
                    permission.IsAssigned(permission.IsInitiallyAssigned());
                });
            });
        });

        self.searchCriteriaInvalid(false);
        self.previousSubmit(false);

        // Reset consent to disclosure agreement text
        if (self.DisclosureAgreementText()) {
            self.consentGiven(false);
        } else {
            self.consentGiven(true);
        }

        // Clear out any previously entered search criteria
        if (self.selectedProxyId() === 'NEWPERSON') {
            self.selectedProxy().Relationship(null);
            self.selectedProxy().Relationship.isModified(false);
            ko.utils.arrayForEach(self.selectedProxy().DemographicInformation(), function (di) {
                di.Value(null);
                di.Value.isModified(false);
            });
        }

        // Clear out access level and announcement user change
        if (self.selectedProxy() != null) {

            // Reset email type if user doesn't have an email address;
            // This will prevent the "field is required" message from displaying on entry
            if (!self.selectedProxy().HasEmailAddress()) {
                var emailTypeField = ko.utils.arrayFirst(self.selectedProxy().DemographicInformation(), function (di) {
                    return di.Type() == 9 && di.Id() == "email_type";
                });
                if (emailTypeField) {
                    emailTypeField.Value(null);
                    emailTypeField.Value.isModified(false);
                }
            }


            self.selectedProxy().accessLevel('select');
            $("#aria-announcements").text(personProxyAddProxySelectedMessage.format(self.selectedProxy().Name()));
        }
    }

    self.saveEditProxyAccessButton = {
        id: 'save-edit-proxy-access-dialog-button',
        title: Ellucian.Proxy.ButtonLabels.editProxyAccessDialogSaveButtonLabel,
        isPrimary: true,
        enabled: self.saveUpdatesButtonEnabled,
        callback: self.updateEditedProxyInformation,
    };

    self.cancelEditProxyAccessButton = {
        id: 'close-edit-proxy-access-dialog-button',
        title: Ellucian.Proxy.ButtonLabels.editProxyAccessDialogCancelButtonLabel,
        isPrimary: false,
        callback: self.closeEditDialog,
    };

    self.saveReauthorizeProxyAccessButton = {
        id: "submit-reauthorize-dialog-button",
        title: Ellucian.Proxy.ButtonLabels.reauthorizeConfirmButtonLabel,
        isPrimary: true,
        enabled: self.reauthorizationConsentGiven,
        callback: self.reauthorizeAccess
    };


    ko.validation.init({ grouping: { deep: true, observable: true, live: false }, insertMessages: false, messagesOnModified: true }, true);
}

// Function to determine if any permissions are assigned for any groups
function anyPermissionsAssigned(groups) {
    var allGroups = ko.utils.arrayMap(groups, function (group) {
        return group;
    });
    if (allGroups) {
        var assignedPermissions = [];
        ko.utils.arrayForEach(allGroups, function (group) {
            ko.utils.arrayForEach(group.Permissions(), function (permission) {
                if (permission.IsAssigned() == true) {
                    assignedPermissions.push(permission);
                }
            });
        });
        return (assignedPermissions.length > 0);
    }
    return false;
}

// Function to determine if any permissions are changed for any groups
function anyPermissionsChanged(groups) {
    var allGroups = ko.utils.arrayMap(groups, function (group) {
        return group;
    });
    if (allGroups) {
        var changedPermissions = [];
        ko.utils.arrayForEach(allGroups, function (group) {
            ko.utils.arrayForEach(group.Permissions(), function (permission) {
                if (permission.IsAssigned() != permission.IsInitiallyAssigned()) {
                    changedPermissions.push(permission);
                }
            });
        });
        return (changedPermissions.length > 0);
    }
    return false;

}
