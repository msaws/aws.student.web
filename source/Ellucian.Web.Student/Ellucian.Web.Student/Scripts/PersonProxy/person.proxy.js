﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
var personProxyViewModelInstance = new personProxyViewModel();

$(document).ready(function () {

    ko.applyBindings(personProxyViewModelInstance, document.getElementById("body"));

    personProxyViewModelInstance.getProxyInformation();
});