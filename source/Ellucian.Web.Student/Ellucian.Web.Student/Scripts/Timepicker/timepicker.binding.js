﻿(function (factory) {
    if (typeof exports === "object" && exports &&
        typeof module === "object" && module && module.exports === exports) {                     
        factory(require("jquery"), require("ko"), require("Site/Timepicker/timepicker"));
    }  
    //else if (typeof define === 'function' && define.amd) {
        // AMD is not supported for this module in Ellucian SelfService
        //define(['jquery', 'ko', "timepicker"], factory);
//}
    else {
        // Browser globals
        // using globals, ensure you <script> the timepicker.js before this binding
        factory(jQuery, ko);
    }
}(function ($, ko, timepickerPlugin) {

    //timepickerPlugin may be null, undefined or empty object. the timepicker attaches itself to jquery as a plugin and doesn't return 
    //a module value. it's not used, but its required because this binding is a dependency on the timepicker

    /*
        The timepicker binding initializes the jquery timepicker (Site/timepicker.js) to the bound element.
        It supports updating the time value and updating options. Note that the date portion of the Date object
        is ignored due to features of the timepicker library.
        You must perform your own logic if you need to associate the time to a specific date. See the DailyDetailTimeEntry component in Time Management
        for an implementation example.
    
        Arguments:
        {
            value: observable - required - contains the Date object selected/entered by the timepicker ,
            timepickerOptions: the options to initialize the jquery timepicker. see https://github.com/jonthornton/jquery-timepicker for details
                            supports observable and non-observable. if observable, options will be updated. if null or noop, default options are used
        }
    
        Markup Example:
        <input type="text" data-bind="timepicker: timeValue, timepickerOptions: options" />
    */
    ko.bindingHandlers.timepicker = {
        init: function (element, valueAccessor, allBindings) {
            let options = allBindings().timepickerOptions || {};
            $(element).timepicker(ko.utils.unwrapObservable(options));

            if (!ko.isObservable(valueAccessor())) {
                throw new Error("value of timepicker binding must be observable");
            }

            //handles form input changing
            ko.utils.registerEventHandler(element, "change", function () {
                let observable = valueAccessor();
                observable($(element).timepicker('getTime'));
               
            });


        },
        update: function (element, valueAccessor, allBindings) {
            let options = allBindings().timepickerOptions || {};

            if (ko.isObservable(options)) {
                $(element).timepicker('option', options());
            }

            $(element).timepicker('setTime', ko.utils.unwrapObservable(valueAccessor()));
        }
    }
}));