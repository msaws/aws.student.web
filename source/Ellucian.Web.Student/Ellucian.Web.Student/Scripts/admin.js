﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
// Register the ESG modal spinner component
ko.components.register('file-upload', {
    require: 'FileUpload/_FileUpload'
});

function saveConfiguration(theData) {
    theData = "{ 'item': " + ko.toJSON(theData) + "}";
    $.ajax({
        url: saveConfigUrl,
        data: theData,
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        success: function (result) {
            $('#notificationHost').notificationCenter('reset');
            $('#notificationHost').notificationCenter('addNotification', { message: "Configuration updated.", type: "success", flash: true });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#notificationHost').notificationCenter('addNotification', { message: jqXHR.responseText, type: "error" });
        }
    });
}

function SiteViewModel() {
    var self = this;

    self.Pages = ko.observableArray(data.Pages);
    self.Links = ko.observableArray(data.Links);
    self.Submenus = ko.observableArray(data.Submenus);
    self.Menus = ko.observableArray(data.Menus);

    self.removePage = function (page) {
        self.Pages.remove(page);
    }

    self.removeLink = function (link) {
        self.Links.remove(link);
    }

    self.removeSubmenu = function (submenu) {
        self.Submenus.remove(submenu);
    }

    self.removeMenu = function (menu) {
        self.Menus.remove(menu);
    }

    this.saveConfig = function () {
        saveConfiguration(ko.toJSON(self));
    }

    self.traverseMenuItems = function (items) {
        for (var i = 0; i < items.length; i++) {
            if (items[i].ItemType === 0) // 0 = Menu
            {
                self.traverseMenuItems(items[i].Menu.Items);
                // Remove Submenu from Submenus observable - remaining contents are "orphaned" submenus
                self.Submenus.remove(function matchItems(item) {
                    return item.Id === items[i].Menu.Id;
                });
            }
            else if (items[i].ItemType === 1) // 1 = Page
            {
                self.Pages.remove(function matchItems(item) {
                    return item.Id === items[i].Page.Id;
                });
            }
        }
    }

    for (var i = 0; i < self.Menus().length; i++) {
        self.traverseMenuItems(self.Menus()[i].Items);
    }

    for (var i = 0; i < self.Submenus().length; i++) {
        self.traverseMenuItems(self.Submenus()[i].Items);
    }
}

function MessageIdViewModel(self) {
    self.MessageId = ko.observable(data.MessageId);

    var messageIdCache = {};
    self.filterMessageIds = function (term) {
        var cache = messageIdCache;
        if (typeof term !== "string" || term.length < 2) {
            return Promise.resolve([]);
        }
        if (term in cache) {
            return Promise.resolve(cache[term]);
        } else {
            var request = { term: term };
            return Promise.resolve($.getJSON(messagesUrl, request, function (data, status, xhr) {
                cache[term] = data;
                return data;
            }));
        }
    }
}

function UsersRolesViewModel(self) {
    self.AllowedRoles = ko.observableArray(data.AllowedRoles);
    self.AllowedUsers = ko.observableArray(data.AllowedUsers);

    self.roleToAdd = ko.observable("");
    self.userToAdd = ko.observable("");

    self.addRole = function () {
        if (self.roleToAdd() != "") {
            self.AllowedRoles.push(self.roleToAdd());
            self.roleToAdd("");
        }
    }

    self.addRole2 = function (role) {
        self.AllowedRoles.push(role);
        self.roleToAdd("");
    }

    self.removeRole = function (role) {
        self.AllowedRoles.remove(role);
    }

    self.addUser = function () {
        if (self.userToAdd() != "") {
            self.AllowedUsers.push(self.userToAdd());
            self.userToAdd("");
        }
    }

    self.addUser2 = function (user) {
        self.AllowedUsers.push(user);
        self.userToAdd("");
    }

    self.removeUser = function (user) {
        self.AllowedUsers.remove(user);
    }

    var roleCache = {};
    self.filterRoles = function (term) {
        if (typeof term !== "string" || term.length < 2) {
            return Promise.resolve([]);
        }
        var cache = roleCache;

        if (term in cache) {
            return Promise.resolve(cache[term]);
        }

        var request = { term: term };
        return Promise.resolve($.getJSON(rolesUrl, request, function (data, status, xhr) {
            cache[term] = data;
            return data;
        }));
    }

    var userCache = {};
    self.filterUsers = function (term) {
        if (typeof term !== "string" || term.length < 2) {
            return Promise.resolve([]);
        }
        var cache = userCache;
        if (term in cache) {
            return Promise.resolve(cache[term]);
        }
        var request = { term: term };

        return Promise.resolve($.getJSON(usersUrl, request, function (data, status, xhr) {
            cache[term] = data;
            return data;
        }));
    }
}

function PageViewModel() {
    var self = this;

    self.Id = ko.observable(data.Id);
    self.Label = ko.observable(data.Label);
    self.Order = ko.observable(data.Order);
    self.Hidden = ko.observable(data.Hidden);
    self.Area = ko.observable(data.Area);
    self.Controller = ko.observable(data.Controller);
    self.Action = ko.observable(data.Action);
    self.ProxyPermissions = ko.observableArray(data.ProxyPermissions);

    MessageIdViewModel(self);
    UsersRolesViewModel(self);

    this.saveConfig = function () {
        saveConfiguration(ko.toJSON(self));
    }
}

function LinkViewModel() {
    var self = this;

    self.Id = ko.observable(data.Id);
    self.Url = ko.observable(data.Url);
    self.Text = ko.observable(data.Text);
    self.Order = ko.observable(data.Order);
    self.ProxyPermissions = ko.observableArray(data.ProxyPermissions);

    UsersRolesViewModel(self);

    this.isNewLink = ko.observable(data.Id == null || data.Id == "");

    this.saveConfig = function () {
        saveConfiguration(ko.toJSON(self));
    }
}

function MenuViewModel() {
    var self = this;

    self.Id = ko.observable(data.Id);
    self.Label = ko.observable(data.Label);
    self.Order = ko.observable(data.Order);
    self.Hidden = ko.observable(data.Hidden);
    self.Items = ko.observableArray(data.Items);
    self.ProxyPermissions = ko.observableArray(data.ProxyPermissions);

    MessageIdViewModel(self);
    UsersRolesViewModel(self);

    self.Links = ko.computed(function () {
        return ko.utils.arrayFilter(self.Items(), function (item) {
            return item.ItemType === 2;
        });
    });
    self.linkToAdd = ko.observable({ Id: "" });
    self.addLink = function () {
        if (self.linkToAdd().Id != "") {
            self.Items.push(self.linkToAdd());
            self.linkToAdd({ Id: "" });
        }
    }
    self.removeLink = function (link) {
        self.Items.remove(link);
    }

    var linkCache = {};
    self.filterLinks = function (term) {
        var cache = linkCache;
        if (typeof term !== "string" || term.length < 2) {
            return Promise.resolve([]);
        }
        if (term in cache) {
            return Promise.resolve(cache[term]);
        }

        var request = { term: term };
        return Promise.resolve($.getJSON(linksUrl, request, function (data, status, xhr) {
            for (var i = 0; i < data.length; i++) {
                data[i].value = data[i].Link.Id;
                data[i].label = data[i].Link.Id;
            }
            cache[term] = data;
            return data;
        }));
    };
    self.selectLink = function (link) {
        self.linkToAdd(link);
        self.addLink();
    };

    this.saveConfig = function () {
        saveConfiguration(ko.toJSON(self));
    };
}

function SettingsViewModel() {
    var self = this;

    // Config
    self.ApiBaseUrl = ko.observable(data.ApiBaseUrl).extend({ required: true, message: " You must enter an API Base Url! " });
    self.UniqueCookieId = ko.observable(data.UniqueCookieId).extend({ required: true, message: " You must enter a Unique Cookie ID! " });
    self.Culture = ko.observable(data.Culture);
    self.ShortDateFormat = ko.observable(data.ShortDateFormat);
    self.SelectedLogLevel = ko.observable(data.SelectedLogLevel);
    self.LogLevels = ko.observableArray(data.LogLevels);
    self.EnableGoogleAnalytics = ko.observable(data.EnableGoogleAnalytics);
    self.EnableCustomTracking = ko.observable(data.EnableCustomTracking);
    self.InsertCustomTrackingAfterBody = ko.observable(data.InsertCustomTrackingAfterBody);
    self.ProxyDimensionIndex = ko.observable(data.ProxyDimensionIndex).extend({
        number: {
            message: "Dimension Index must be a number."
        }
    });
    self.TrackingId = ko.observable(data.TrackingId).extend({
        required: {
            message: "You must enter your tracking ID!",
            onlyIf: function () {
                return self.EnableGoogleAnalytics() && !self.EnableCustomTracking();
            }
        }
    });
    self.CustomTrackingAnalytics = ko.observable($("<div>").html(data.CustomTrackingAnalytics).text()).extend({
        required: {
            message: "You must enter your custom script!",
            onlyIf: function () {
                return self.EnableGoogleAnalytics() && self.EnableCustomTracking();
            }
        }
    });
    self.ClientPrivacyPolicy = ko.observable($("<div>").html(data.ClientPrivacyPolicy).text()).extend({
        required: {
            message: "You must enter your privacy policy if Google Analytics is enabled!",
            onlyIf: function () {
                return self.EnableGoogleAnalytics();
            }
        }
    });
    self.Errors = ko.validatedObservable({
        ApiBaseUrl: this.ApiBaseUrl,
        UniqueCookieId: this.UniqueCookieId,
        ClientPrivacyPolicy: this.ClientPrivacyPolicy,
        TrackingId: this.TrackingId,
        CustomTrackingAnalytics: this.CustomTrackingAnalytics
    });

    self.DateFormats = ko.observableArray([
        "Default",
        "MM-dd-yy",
        "MM-dd-yyyy",
        "dd-MM-yy",
        "dd-MM-yyyy",
        "yy-MM-dd",
        "yyyy-MM-dd",
        "MM/dd/yy",
        "MM/dd/yyyy",
        "dd/MM/yy",
        "dd/MM/yyyy",
        "yy/MM/dd",
        "yyyy/MM/dd"
    ]);

    var culture = function (code, description) {
        this.code = code;
        this.description = description;
    };

    self.Cultures = ko.observableArray([
            new culture("en-US", "United States (English)"),
            new culture("en-CA", "Canada (English)"),
            new culture("fr-CA", "Canada (French)")
    ]);


    // Branding
    self.ApplicationTitle = ko.observable(data.ApplicationTitle);
    self.HeaderLogoPath = ko.observable(data.HeaderLogoPath);
    self.HeaderLogoAltText = ko.observable(data.HeaderLogoAltText);
    self.FooterLogoPath = ko.observable(data.FooterLogoPath);
    self.FooterLogoAltText = ko.observable(data.FooterLogoAltText);
    self.ReportLogoPath = ko.observable(data.ReportLogoPath);
    self.Messages = ko.observableArray(data.Messages);
    self.Favicon = ko.observable(data.Favicon);
    self.UploadFilename = ko.observable();

    //----- Confirm Delete modal dialog pieces -----//

    // Is the Confirm Delete modal dialog displayed?
    self.confirmRemoveDialogIsOpen = ko.observable(false);

    // Confirm deletion
    self.confirmRemoveItem = function () {
        ko.utils.postJson(postBrandingActionUrl, { action: "remove", name: self.Favicon().Name, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
        self.confirmRemoveDialogIsOpen(false);
    };

    // "OK" button behavior
    self.submitConfirmRemoveDialogButton = {
        id: 'confirm-remove-ok',
        title: Ellucian.SharedComponents.ButtonLabels.buttonTextOk,
        isPrimary: true,
        callback: self.confirmRemoveItem
    };

    // Prompt displayed to user on Confirm Delete modal dialog
    self.confirmRemoveDialogPrompt = ko.pureComputed(function () {
        if (self.Favicon() && self.Favicon().Name) {
            return Ellucian.Admin.Resources.PermanentDeletePrompt.format(self.Favicon().Name);
        }
        return null;
    });

    //----- Confirm Upload modal dialog pieces -----//

    // Is the Confirm Upload modal dialog displayed?
    self.confirmUploadDialogIsOpen = ko.observable(false);

    // Is the upload overwriting an existing file?
    self.overwriteOnUpload = ko.observable();

    // Confirm upload
    self.confirmUploadItem = function () {
        self.overwriteOnUpload(true);
        $("#uploadFavicon").submit();
        self.confirmUploadDialogIsOpen(false);
    };

    // "OK" button behavior
    self.submitConfirmUploadDialogButton = {
        id: 'confirm-upload-ok',
        title: Ellucian.SharedComponents.ButtonLabels.buttonTextOk,
        isPrimary: true,
        callback: self.confirmUploadItem,
    };

    // Prompt displayed to user on Confirm Upload modal dialog
    self.confirmUploadDialogPrompt = ko.pureComputed(function () {
        if (self.UploadFilename()) {
            var startIndex = (self.UploadFilename().indexOf('\\') >= 0 ? self.UploadFilename().lastIndexOf('\\') : self.UploadFilename().lastIndexOf('/'));
            var filename = self.UploadFilename().substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            return Ellucian.Admin.Resources.OverwriteFilePrompt.format(filename);
        }
        return null;
    });

    // Form submit handler to determine overwrite and upload behaviors
    self.submitUpload = function () {
        var allowUpload = true;

        var fullPath = self.UploadFilename();
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }

        if (self.Favicon() && self.Favicon().Name.toLowerCase() === filename.toLowerCase()) {
            allowUpload = false;
        }

        if (self.overwriteOnUpload()) {
            allowUpload = true;
        }

        if (!allowUpload) {
            self.confirmUploadDialogIsOpen(true);
            self.overwriteOnUpload = ko.observable(false);
        }

        return allowUpload;
    }

    // Culture Overrides
    self.CulturePropertyOverrides = ko.observable(data.CulturePropertyOverrides);

    // Api Connection Limit
    self.ApiConnectionLimit = ko.observable(data.ApiConnectionLimit).extend({ apiConnectionLimitValidation: " Please enter an even number 2 or higher! " });

    // Save settings changes
    this.saveSettings = function () {
        $.ajax({
            url: saveConfigUrl,
            data: ko.toJSON(self),
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            success: function (result) {
                $('#notificationHost').notificationCenter('addNotification', { message: "Configuration updated.", type: "success", flash: true });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Check response to see if an entire HTML page was returned.  If so, try to find 
                // a meaningful message (based on known IIS YSOD layout), otherwise use a generic error.                
                var message = JSON.parse(jqXHR.responseText);
                var isHtml = /<!doctype html>/i.test(jqXHR.responseText);
                if (isHtml === true) {
                    var message = /<h2>\s*<i>(.*)<\/i>\s*<\/h2>/i.exec(message);
                    if (message != null) {
                        // First element of array is the entire match, includiing <h2> and <i> tags, 
                        // take the next element instead, which is the (.*) match only
                        message = message[1];
                    } else {
                        message = "Configuration failed to update. Please contact the system administrator.";
                    }
                }

                $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
            }
        });
    }

    this.CanSaveConfig = ko.computed(function () {
        //return (self.UniqueCookieId().length > 0 && self.ApiBaseUrl().length > 0);
        //return (!self.ApiBaseUrl.hasError() && self.ApiConnectionLimit.validationMessage() == "" && !self.UniqueCookieId.hasError());
        return self.ApiConnectionLimit.validationMessage() == "";
    }, this);

    // Push any messages to the notification center
    for (var i = 0; i < self.Messages().length; i++) {
        var notification = self.Messages()[i];
        $('#notificationHost').notificationCenter('addNotification', { message: notification, type: "error" });
    }
}

function autocompleteGuestUser(sourceSelector, koBinding, clearInput) {
    var cache = {};
    $(sourceSelector).autocomplete({
        delay: 500,
        minLength: 2,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[term]);
                return;
            }
            $.getJSON(usersUrl, request, function (data, status, xhr) {
                cache[term] = data;
                response(data);
            });
        },
        select: function (event, ui) {
            koBinding(ui.item.value);
            if (clearInput === true) {
                $(sourceSelector).attr("value", "");
            }
            return false;
        }
    });
}

// base admin namespace
(function (admin, $, undefined) {

}(window.admin = window.admin || {}, jQuery));


// theme namespace
(function (theme, $, undefined) {

    // knockout view model mapping
    theme.themeEditorViewModelMapping = {
        'ThemeSections': {
            create: function (options) {
                return new ThemeSection(options.data);
            }
        },
        'ThemeRules': {
            create: function (options) {
                return new ThemeRules(options.data);
            }
        }
    };

    // handles mapping the ThemeSections objects
    function ThemeSection(data) {
        // need this fucntion in order to traverse to the ThemeRuleSets level
        ko.mapping.fromJS(data, theme.themeEditorViewModelMapping, this);
    };

    // handles mapping the ThemeRules objects
    function ThemeRules(data) {
        ko.mapping.fromJS(data, theme.themeEditorViewModelMapping, this);
        var self = this;

        // values that need two-way binding support
        self.BackgroundColor = ko.observable(data.BackgroundColor).extend({ colorValidation: { validateColor: true } });
        self.Color = ko.observable(data.Color).extend({ colorValidation: { validateColor: true } });
        self.BorderColor = ko.observable(data.BorderColor).extend({ colorValidation: { validateColor: true } });
        self.BorderLeftColor = ko.observable(data.BorderLeftColor).extend({ colorValidation: { validateColor: true } });
        self.BorderRightColor = ko.observable(data.BorderRightColor).extend({ colorValidation: { validateColor: true } });
        self.BorderTopColor = ko.observable(data.BorderTopColor).extend({ colorValidation: { validateColor: true } });
        self.BorderBottomColor = ko.observable(data.BorderBottomColor).extend({ colorValidation: { validateColor: true } });
        // Display controls
        self.showRule = ko.observable(false);
        self.toggleCssSelector = function () {
            self.showRule(!self.showRule());
        }
    };

    // knockout view model
    theme.ThemeEditorViewModel = function () {
        var self = this;

        // model helper used to display notifications
        this.handleNotifications = function () {
            if (self.Messages().length > 0) {
                for (var i = 0; i < self.Messages().length; i++) {
                    var notification = ko.toJS(self.Messages()[i]);
                    $('#notificationHost').notificationCenter('addNotification', notification);
                }
            }
        }

        // serialize and post the current model data to the server
        this.saveTheme = function () {
            ko.utils.postJson(location.href, { themeEditorModel: ko.toJS(self), action: "save", __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() })
        }

        // ask the server to restore the default theme
        this.restoreDefaultTheme = function () {
            ko.utils.postJson(location.href, { action: "restore", __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() })
        }

        ko.bindingHandlers.colorpicker = {
            update: function (element, valueAccessor, allBindingsAccessor) {
                $(element).spectrum({
                    color: ko.unwrap(valueAccessor()),
                    showInput: true,
                    showAlpha: true,
                    preferredFormat: "rgb"
                });
            }
        };
    }

}(window.admin.theme = window.admin.theme || {}, jQuery));

// authentication configuration models
(function (authentication, $, undefined) {

    authentication.AuthenticationConfigurationViewModel = function (data) {
        var self = this;

        self.CertificatesViewModel = new authentication.CertificatesViewModel(data, true);

        self.AuthenticationType = ko.observable(data.AuthenticationType);


        self.Enabled = self.CanSave = ko.computed(function () {
            return self.AuthenticationType() === "SAML2";
        });

        self.IdentityProviderUrl = ko.observable(data.IdentityProviderUrl).extend({ requiredCustom: "Required" });
        self.IdentityProviderLogoutUrl = ko.observable(data.IdentityProviderLogoutUrl);
        self.UseEncryptedAssertions = ko.observable(data.UseEncryptedAssertions).extend({ requiredCustom: "Required" });

        self.IdentityProviderIssuer = ko.observable(data.IdentityProviderIssuer);

        self.SamlClockSkew = ko.observable(data.SamlClockSkew);

        self.UsernameClaim = ko.observable(data.UsernameClaim).extend({ requiredCustom: "Required" });
        self.ServiceProviderId = ko.observable(data.ServiceProviderId).extend({ requiredCustom: "Required" });

        self.IdentityProviderPublicKey = ko.observable(data.IdentityProviderPublicKey).extend({ requiredCustom: "Required" });
        self.IdentityProviderPublicKeyPassword = ko.observable(data.IdentityProviderPublicKeyPassword);

        self.BaseUrl = ko.observable(data.BaseUrl).extend({ requiredCustom: "Required" });
        self.ServiceProviderPrivateKey = ko.observable(data.ServiceProviderPrivateKey).extend({ requiredCustom: "Required" });
        self.ServiceProviderPrivateKeyPassword = ko.observable(data.ServiceProviderPrivateKeyPassword);

        self.PublicKeys = ko.observableArray(data.AvailablePublicKeys);
        self.PrivateKeys = ko.observableArray(data.AvailablePrivateKeys);


        self.ProxyUsername = ko.observable(data.ProxyUsername).extend({ requiredCustom: "Required" });
        self.ProxyPassword = ko.observable(data.ProxyPassword).extend({ requiredCustom: "Required" });

        self.GuestUsername = ko.observable(data.GuestUsername);
        self.GuestPassword = ko.observable(data.GuestPassword);

        self.SetNameIdPolicy = ko.observable(data.SetNameIdPolicy);
        self.SPNameQualifier = ko.observable(data.SPNameQualifier);

        self.Messages = ko.observableArray(data.Messages);

        self.guestUsernameHasValue = ko.computed(function () {
            return ((self.GuestUsername() !== null && typeof self.GuestUsername() !== undefined && self.GuestUsername() !== '') || !isNullOrEmpty(self.GuestPassword()));
        });

        self.CanSave = ko.computed(function () {
            return true;
        });

        self.Save = function () {

            var model = ko.toJS(self);
            ko.utils.postJson(location.href, { ssoSettings: model, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });

        }

        self.ClearGuestUser = function () {
            self.GuestUsername("");
            self.GuestPassword("");
            self.TestingGuestCredentials.messageToDisplay(null);
            self.TestingGuestCredentials.isSuccess(null);
        }

        self.TestingCredentials = ko.observable();
        self.TestCredentials = function () {

            var model = new Object();
            model.userName = self.ProxyUsername();
            model.password = self.ProxyPassword();
            self.TestingCredentials("Attempting login...");
            $.ajax({
                url: 'TestSsoProxyLogin',
                data: JSON.stringify(model),
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    self.TestingCredentials(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    self.TestingCredentials(data);
                }
            });
        }

        self.TestingGuestCredentials = { messageToDisplay: ko.observable(null), isSuccess: ko.observable(null) };
        self.TestGuestCredentials = function () {

            var model = new Object();
            model.userName = self.GuestUsername();
            model.password = self.GuestPassword();
            model.success = successMessage;
            model.failure = failureMessage;
            self.TestingGuestCredentials.isSuccess(null);
            self.TestingGuestCredentials.messageToDisplay(attemptingLoginMessage);
            $.ajax({
                url: 'TestLogin',
                data: JSON.stringify(model),
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    self.TestingGuestCredentials.messageToDisplay(data);
                    self.TestingGuestCredentials.isSuccess(true);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    self.TestingGuestCredentials.messageToDisplay(JSON.parse(jqXHR.responseText));
                    self.TestingGuestCredentials.isSuccess(false);
                }
            });
        }

        self.GetCertificateData = function () {
            $.ajax({
                url: 'ApplicationCertificates',
                type: 'GET',
                success: function (data, textStatus, jqXHR) {
                    try {
                        if (data != null) {
                            var publicKeys = new Array();
                            publicKeys.push("");
                            if (data.CurrentCertificates != null && data.CurrentCertificates.length > 0) {
                                for (var i = 0; i < data.CurrentCertificates.length; i++) {
                                    publicKeys.push(data.CurrentCertificates[i].Name);
                                }
                            }
                            self.PublicKeys(publicKeys);

                            var privateKeys = new Array();
                            privateKeys.push("");
                            if (data.CurrentKeys != null && data.CurrentKeys.length > 0) {
                                for (var i = 0; i < data.CurrentKeys.length; i++) {
                                    privateKeys.push(data.CurrentKeys[i].Name);
                                }
                            }
                            self.PrivateKeys(privateKeys);
                        }
                    } catch (e) { }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        for (var i = 0; i < self.Messages().length; i++) {
            var notification = self.Messages()[i];
            if ($('#notificationHost').length) {
                $('#notificationHost').notificationCenter('addNotification', notification);
            }
            else {
                alert(notification.Type + ": " + notification.Message);
            }
        }

        autocompleteGuestUser("#guestUserId", self.GuestUsername, false);
    }

    authentication.CertificatesViewModel = function (data, suppressMessages) {
        var self = this;

        self.CurrentCertificates = ko.observableArray(data.CurrentCertificates);
        self.CurrentKeys = ko.observableArray(data.CurrentKeys);
        self.Messages = ko.observableArray(data.Messages);
        self.UploadFilename = ko.observable();

        self.confirmRemoveDialogIsOpen = ko.observable(false);

        self.confirmRemoveItem = function () {
            var itemName = $("#confirmRemoveDialog").data("itemName");
            ko.utils.postJson(postCertificatesActionUrl, { action: "remove", name: itemName, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() });
            $("#confirmRemoveDialog").data("itemName", "");
            self.confirmRemoveDialogIsOpen(false);
        };

        self.closeConfirmRemoveDialog = function () {
            self.confirmRemoveDialogIsOpen(false);
            $("#confirmRemoveDialog").data("itemName", "");
        };

        self.Remove = function (item, event) {
            $("#confirmRemoveDialog").data("itemName", item.Name);
            $("#confirmRemoveDialogSpan").text("Permanently delete " + item.Name + "?");
            self.confirmRemoveDialogIsOpen(true);
        }

        self.confirmUploadDialogIsOpen = ko.observable(false);

        self.closeConfirmUploadDialog = function () {
            self.confirmUploadDialogIsOpen(false);
            $("#confirmUploadDialogSpan").text("");
        };

        self.confirmUploadItem = function () {
            $("#confirmUploadDialogSpan").text("");
            $("#uploadCertForm").find('input[type="submit"]').data("overwrite", true);
            $("#uploadCertForm").submit();
            self.confirmUploadDialogIsOpen(false);
        };

        $('#uploadCertForm').submit(function () {
            var allowUpload = true;

            var fullPath = self.UploadFilename();
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }

            ko.utils.arrayForEach(self.CurrentCertificates(), function (item) {
                if (item.Name.toLowerCase() === filename.toLowerCase()) {
                    allowUpload = false;
                }
            });

            ko.utils.arrayForEach(self.CurrentKeys(), function (item) {
                if (item.Name.toLowerCase() === filename.toLowerCase()) {
                    allowUpload = false;
                }
            });

            var overwrite = $("#uploadCertForm").find('input[type="submit"]').data("overwrite");
            if (overwrite !== null && overwrite === true) {
                allowUpload = true;
            }

            if (!allowUpload) {
                $("#confirmUploadDialogSpan").text("The file " + filename + " already exists. Overwrite?");
                self.confirmUploadDialogIsOpen(true);

                $("#uploadCertForm").find('input[type="submit"]').data("overwrite", false);
            }

            return allowUpload;
        });

        if (!suppressMessages) {
            for (var i = 0; i < self.Messages().length; i++) {
                var notification = self.Messages()[i];
                if ($('#notificationHost').length) {
                    $('#notificationHost').notificationCenter('addNotification', notification);
                }
                else {
                    alert(notification.Type + ": " + notification.Message);
                }
            }
        }
    }

}(window.admin.authentication = window.admin.authentication || {}, jQuery));

// Provides a KO extender function to mark fields as required
ko.extenders.requiredCustom = function (target, overrideMessage) {
    //add some sub-observables to our observable
    target.hasError = ko.observable();
    target.validationMessage = ko.observable();

    //define a function to do validation
    function validate(newValue) {
        target.hasError(newValue ? false : true);
        target.validationMessage(newValue ? "" : overrideMessage || "This field is required.");
    }

    //initial validation
    validate(target());

    //validate whenever the value changes
    target.subscribe(validate);

    //return the original observable
    return target;
};

ko.extenders.apiConnectionLimitValidation = function (target, message) {
    //add some sub-observables to our observable
    target.validationMessage = ko.observable('');
    //define a function to do validation
    function validate(newValue) {
        if (!newValue || newValue < 2 || newValue % 2 != 0) {
            target.validationMessage(message);
        }
        else {
            target.validationMessage('');
        }
    }
    //initial validation
    validate(target());
    //validate whenever the value changes
    target.subscribe(validate);
    //return the original observable
    return target;
}

/* The colorValidation extender checks for properly formatted colors in the ThemeEditor.
   Valid colors include hex (3 and 6 digits, with and without preceding #) as well as rgb and rgba (with or without spaces between commas)

   Params:
     validationMessage: For validation error message
 */
ko.extenders.colorValidation = function (target, options) {
    // color regex from https://gist.github.com/sethlopezme/d072b945969a3cc2cc11, minus HSL/HSLA, with optional whitespace added for RGB/RGBA, and ignore case (/i)
    var colorRegex = /^(#?([a-f\d]{3}|[a-f\d]{6})|rgb\((0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d),\s*(0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d),\s*(0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d)\)|rgba\((0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d),\s*(0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d),\s*(0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d),\s*(0?\.\d|1(\.0)?)\))$/i;

    if (options.validateColor) {
        var validationMessage = options.validateMessage || Ellucian.Admin.Resources.ColorErrorMessage;
        target.extend({
            validation: {
                validator: function (value) {
                    var validatedColor = new RegExp(colorRegex).test(value);

                    // Validation is done on a rule basis; each rule has a list of color properties, some of which may be null -- this is still valid
                    if (validatedColor || value == null) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },
                message: validationMessage
            }
        });
    }
}