﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
/*
* Provides functionality related to the AccountController to support
* login and session functionality.
*/

(function (account, $, undefined) {

    //Public properties
    account.loginLinkId = "#loginLink";
    account.ajaxEnabledAccountPopups = true;
    
    //Public methods

    // returns true when an invalid session was handled, false when session is OK.
    account.handleInvalidSessionResponse = function (jsonResonse, showLoginOnInvalidSession) {

        // default missing flags...
        if (showLoginOnInvalidSession == undefined) { showLoginOnInvalidSession = true; }

        try {
            if (jsonResonse.InvalidSession != null) {
                if (jsonResonse.InvalidSession === "true") {

                    if (showLoginOnInvalidSession) {
                        if (account.ajaxEnabledAccountPopups) {
                            // show the ajax login popup dialog...
                            var id = account.loginLinkId;
                                account.loadAndShowDialog(id, jsonResonse.LoginPageUrl);
                        } else {
                            // redirect to the full login page...
                            var returnUrl = location.pathname + (location.search || '') + (location.hash || '');
                            location.href = jsonResonse.LoginPageUrl + "?returnUrl=" + encodeURIComponent(returnUrl);
                        }
                    }
                    return true;
                }
            }
        } catch (e) {

        }
        return false;
    };

    account.loadAndShowDialog = function (id, url) {
        var separator = url.indexOf('?') >= 0 ? '&' : '?';

        // Load the dialog with the content=1 QueryString in order to get a PartialView
        $.get(url + separator + 'content=1')
            .done(function (content) {

                var $responseHtml = $('<div class="esg-modal-dialog"><div class="esg-modal-dialog__header"><h3 class="esg-modal-dialog__title"></h3></div><div class="esg-modal-dialog__body">' + content + '</div></div><div class="esg-modal-overlay"></div>');
                $responseHtml.find('.esg-modal-dialog__title').text("Sign In");

                $responseHtml
                    .hide() // Hide the dialog for now so we prevent flicker
                    .appendTo(document.body)
                    .filter('div') // Filter for the div tag only, script tags could surface
                    .show() // Show the dialog
                    .find('form') // Attach logic on forms
                        .submit(formSubmitHandler)
                    .find('esg-modal-overlay') // Find and show the overlay
                        .show()
                    .end();
            });
    };

    // Private methods

    function getValidationSummaryErrors($form) {
        // We verify if we created it beforehand
        var errorSummary = $form.find('.validation-summary-errors, .validation-summary-valid');
        if (!errorSummary.length) {
            errorSummary = $('<div class="validation-summary-errors"><span>Please correct the errors and try again.</span><ul></ul></div>')
                .prependTo($form);
        }

        return errorSummary;
    };

    function displayErrors(form, errors) {
        var errorSummary = getValidationSummaryErrors(form)
            .removeClass('validation-summary-valid')
            .addClass('validation-summary-errors');

        var items = $.map(errors, function (error) {
            return '<li>' + error + '</li>';
        }).join('');

        var ul = errorSummary
            .find('ul')
            .empty()
            .append(items);
    };

    function resetForm($form) {
        // We reset the form so we make sure unobtrusive errors get cleared out.
        $form[0].reset();

        getValidationSummaryErrors($form)
            .removeClass('validation-summary-errors')
            .addClass('validation-summary-valid')
    };

    function disableInput() {
        $('input:submit').attr('disabled', 'disabled');
    };

    function enableInput() {
        $('input').removeAttr('disabled');
    };

    function formSubmitHandler(e) {
        var $form = $(this);

        // We check if jQuery.validator exists on the form
        if (!$form.valid || $form.valid()) {

            var rvt = $form.find('input[name="__RequestVerificationToken"]').val();

            $.ajax($form.attr('action'), {
                type: "POST",
                data: $form.serializeArray(),
                beforeSend: function (request) {
                    request.setRequestHeader("__RequestVerificationToken", rvt);
                },
                success: function (data, textStatus, jqXHR) {
                    json = data || {};

                    // In case of success, we redirect to the provided URL or the same page.
                    if (json.success) {
                        window.location.reload();
                    } else if (json.errors) {
                        displayErrors($form, json.errors);
                    }
                    enableInput();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    displayErrors($form, ['An unknown error happened.']);
                    enableInput();
                }
            });

            // must do this after otherwise the input fields will not be picked up by the post...
            disableInput();
        }

        // Prevent the normal behavior since we opened the dialog
        e.preventDefault();
    };

} (window.account = window.account || {}, jQuery));
