﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
// 
// Badge - a counter that reflects the quantity of items associated with a category or component.
// Parameters:
// number     (required) - number - the quantity of items associated with the category/component
//                         to which the badge is associated
// type       (optional) - text/enum - the purpose of the badge
//                         "error", "pending", "success", or "draft"
// Usage:
//
// <badge params="number: 123, type:'success'"></badge>
//
// Notes:
// The template (markup) for this component can be found in ./_Badge.html
//
// When placing badges on a page, be cognizant of the "scope" of the icon. Badges do not typically convey information 
// on their own; they should usually be placed alongside meaningful content (for accessibility).
//
define(['text!Badge/_Badge.html'], function (markup) {
    function BadgeViewModel(params) {
        var self = this;
        self.number = null;
        if (typeof (params.number) === "undefined") {
            throw "Please provide a valid number value.";
        } else {
            self.number = ko.isObservable(params.number) ? params.number : ko.observable(params.number);
        }

        self.type = (typeof (params.type) !== "undefined" ? (ko.isObservable(params.type) ? params.type : ko.observable(params.type)) : ko.observable(null));
        self.badgeClasses = ko.computed(function () {
            var value = "esg-badge";
            var modifier = " esg-badge--{0}";
            switch (self.type()) {
                case 'error':
                case 'pending':
                case 'success':
                case 'draft':
                    value += modifier.format(self.type());
                    break;
                default:
                    break;
            }
            return value;
        });

        self.dispose = function () {
            self.badgeClasses.dispose();
        }
    }
    return { viewModel: BadgeViewModel, template: markup };
});