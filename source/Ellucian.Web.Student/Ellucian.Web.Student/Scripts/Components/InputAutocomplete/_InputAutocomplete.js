﻿/* Copyright 2017 Ellucian Company L.P. and its affiliates.
 * Input Autocomplete
 *
 * This component provides a consistent way to implement a text box with
 * autocomplete and search functionality. In the dormant state, it shows
 * a standard input text box. When focus is gained and the user begins
 * typing, the provided promise from the filter promise factory will be
 * evaluated and any results will be displayed to the user with the
 * Style Guide Lookup pattern. The user can then select a result which
 * will be passed as an argument to the selectionCallback parameter.
 * 
 * In simple mode (default, configured by setting the complexSearch to false)
 * the results obtained from the filter promise should be strings and the
 * result is populated to the text box, passed to the given selectionCallback
 * function parameter upon user selection and the filterObservable parameter 
 * passed in will be set to the result, if provided.
 *
 * In complex search mode (configured by setting the complexSearch parameter to true)
 * the results can be a string or any javascript type that contains a string property as
 * defined in the descriptionProperty parameter which will be used to display the
 * result to the user for selection. The text box is not updated upon selecting a
 * result by default, but can be configured to clear itself by passing true to the
 * clearFilterOnSelection parameter. When a result item is selected, it will be
 * passed as a parameter to the selectionCallback function.
 * 
 * Simple Mode Example:
 * <input-auto-complete params="inputId: 'someId',
 *                              filterPromiseFactory: findResults,
 *                              filterObservable: myViewModelObservable" />
 * 
 * Complex Mode Example: 
 * <input-auto-complete params="complexSearch: true,
 *                              inputId: 'someId',
 *                              filterPromiseFactory: findResults,
 *                              descriptionProperty: 'myPropertyName',
 *                              selectionCallback: doSomethingWithResult,
 *                              clearFilterOnSelection: true" />
 *                              
 *
 * Parameters:
 * 
 *   filterPromiseFactory:
 *     A function that should return a promise that returns the results for a given 
 *     user input, passed as the only parameter.
 *   selectionCallback:
 *     A function that is called with the result item when a user clicks the item
 *     from the results dropdown.
 *   filterObservable:
 *     (Optional) Observable string that will be synched to the value of the filter text box.
 *   complexSearch:
 *     Boolean that turns on or off complexSearch. Defaults to false (off).
 *   clearFilterOnSelection:
 *     Boolean that controls behavior of the text box after user selection. Defaults to false (off).
 *   placeholderText:
 *     (Optional) String or observable that will be used as the placeholder for the filter text box
 *   inputId:
 *     (Optional) string or observable that will be used as the ID of the filter text box.
 *   inputName:
 *     (Optional) string or observable that will be used as the name of the filter text box.
 */

(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["text!InputAutocomplete/_InputAutocomplete.html"], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory(require('raw!./_InputAutocomplete.html'));
    }
}(this, function (markup) {
    function ResultModel(resultItem, descriptionProperty) {
        var self = this;
        var _descriptionProperty = descriptionProperty ? descriptionProperty : 'description';
        self.item = resultItem;
        self.description = ko.pureComputed(function () {
            if (typeof self.item === 'string') {
                return self.item;
            } else if (_descriptionProperty in self.item) {
                return self.item[_descriptionProperty];
            }
        }, self);
        self.resultHasFocus = ko.observable(false);
    }

    function InputAutocompleteViewModel(params) {
        if (!params.filterPromiseFactory) {
            console.warn("A filter promise factory is required for autocomplete.");
        } else if (typeof params.filterPromiseFactory !== 'function') {
            console.warn("Filter promise factory for autocomplete should be a function.");
        }
        if (params.selectionCallback && typeof params.selectionCallback !== 'function') {
            console.warn("Selection callback for autocomplete should be a function.");
        }

        var self = this;
        self.filterText = ko.isObservable(params.filterObservable) ? params.filterObservable : ko.observable('');
        self.complexSearch = ko.isObservable(params.complexSearch) ? params.complexSearch : ko.observable(!!params.complexSearch);
        self.inputId = ko.isObservable(params.inputId) ? params.inputId : ko.observable(params.inputId);
        self.inputName = ko.isObservable(params.inputName) ? params.inputName : ko.observable(params.inputName);
        self.placeholderText = ko.isObservable(params.placeholderText) ? params.placeholderText : ko.observable(params.placeholderText);
        self.clearFilterOnSelection = ko.isObservable(params.clearFilterOnSelection) ? params.clearFilterOnSelection : ko.observable(!!params.clearFilterOnSelection);
        self.descriptionProperty = ko.isObservable(params.descriptionProperty) ? params.descriptionProperty : ko.observable(params.descriptionProperty);
        self.inputHasFocus = ko.observable(false);
        self.isActive = ko.observable(false);

        // The results list hides 200ms after the component (i.e. the input box
        // or any of the results) loses focus. This allows the user to move focus
        // between the input box and clicking a result because the browser momentarily
        // shifts focus to the body between the two.
        var pendingDeactivation = false;
        self.activateLookup = function () {
            self.isActive(true);
            pendingDeactivation = false;
        };
        self.deactivateLookup = function () {
            pendingDeactivation = true;
            setTimeout(function () {
                if (pendingDeactivation) {
                    self.isActive(false);
                }
            }, 200);
        };

        var _results = ko.observableArray([]);
        self.results = ko.computed(function () {
            var arr = [];
            ko.utils.arrayForEach(_results(), function (item) {
                arr.push(new ResultModel(item, self.descriptionProperty()));
            });
            return arr;
        });

        self.lookupIsActive = ko.computed(function () {
            return (self.isActive()) && self.results().length > 0;
        }, self);
        self.activeClass = ko.computed(function () {
            if (self.lookupIsActive()) {
                return "esg-is-active";
            } else {
                return "";
            }
        }, self);
        self.filterText.subscribe(function (filter) {
            if (typeof params.filterPromiseFactory === "function") {
                params.filterPromiseFactory(filter).then(function (val) {
                    _results(val);
                }).catch(function (val) {
                    _results([]);
                });
            }
        });
        self.selectResult = function (result) {
            if (typeof params.selectionCallback === "function") {
                params.selectionCallback(result.item);
            }
            if (!self.complexSearch() && typeof result.item === "string") {
                self.filterText(result.item);
                self.isActive(false);
            } else if (self.complexSearch() && self.clearFilterOnSelection()) {
                self.filterText("");
            }
            self.inputHasFocus(true);
        }
        self.handleResultKeydown = function (result, e) {
            // Allow arrows keys to move between and enter to select highlighted results
            if (e.keyCode === 13) { // Enter
                self.selectResult(result);
                e.preventDefault();
                return false;
            } else if (e.keyCode === 38) { // Arrow Up
                var context = ko.contextFor(e.target);
                var resultIndex = context.$index();
                if (resultIndex > 0) {
                    self.results()[resultIndex - 1].resultHasFocus(true);
                }
                return false;
            } else if (e.keyCode === 40) { // Arrow Down
                var context = ko.contextFor(e.target);
                var resultIndex = context.$index();
                if (resultIndex < self.results().length - 1) {
                    self.results()[resultIndex + 1].resultHasFocus(true);
                }
                return false;
            } else {
                return true;
            }
        }
        self.handleInputKeydown = function (data, e) {
            // From the text input, arrow down should select the first result
            if (e.keyCode === 40) { // Arrow Down
                if (self.results().length > 0) {
                    self.results()[0].resultHasFocus(true);
                    return false;
                } else {
                    return true;
                }
            } else {
                self.activateLookup();
                return true;
            }
        }
    }

    return { viewModel: InputAutocompleteViewModel, template: markup }
}));
