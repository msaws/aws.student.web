﻿(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        define([], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory();
    }
}(this, function () {
    return {
        //published when users presses save button
        AUTOSAVE_SAVEBUTTONPRESSED: "autosave:savebuttonpressed",

        //component subscribes to this event. event should be published when saving begins
        AUTOSAVE_SAVESTART: "autosave:savestart",

        //component subscribes to this event. event should be published when saving completes successfully
        AUTOSAVE_SAVECOMPLETE: "autosave:savecomplete",

        //component subscribes to this event. event should be published when an error occurs during save
        AUTOSAVE_SAVEERROR: "autosave:saveerror",

        //component subscribes to this event and sets the save button enabled status to 
        //the published value (true - enabled, or false -disabled). the event should published 
        //when the application wants to enable or disable the save button, for example, in 
        //error conditions
        AUTOSAVE_SETBUTTONSTATUS: "autosave:setbuttonstatus",
    }
}));


