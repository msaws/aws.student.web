﻿/*known dependencies, unable to inject
ko
ko.postbox
globalize
*/

(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["text!AutoSave/_AutoSave.html", "AutoSave/_AutoSaveEvents", "resource.manager"], factory);
    } else if (typeof module === "object" && module.exports) {
        require("./_AutoSave.scss");
        module.exports = factory(require('./_AutoSave.html'), require('Site/Components/AutoSave/_AutoSaveEvents'), require('Site/resource.manager'));
        if (!ko.components.isRegistered('auto-save')) {
            ko.components.register('auto-save', module.exports);
        }
    } 
}(this, function (markup, autoSaveEvents, resources) {

    /* params: {
     *      initialLastSaveDate: Date - optional - the last time something was saved,
     *      isMobile: Observable - true or false indicating this is a mobile device
     * }
     */
    function AutoSaveViewModel(params) {

        var self = this;

        if (!params) {
            console.warn("AutoSaveViewModel params are required");
        }
        if (!params.isMobile || !ko.isObservable(params.isMobile)) {
            console.warn("params.isMobile is required and must be an observable");
        }

        self.isMobile = params.isMobile;

        self.saveError = ko.observable(false);
        self.saveButtonEnabled = ko.observable(true).subscribeTo(autoSaveEvents.AUTOSAVE_SETBUTTONSTATUS);

        var lastSaveDate = ko.observable(params.initialLastSaveDate || null);
        var lastSaveTimestamp = ko.pureComputed(function () {
            if (lastSaveDate()) {
                if ((Date.now() - lastSaveDate().getTime() > 86400000)) {
                    return Globalize.format(lastSaveDate(), "M");
                }
                else {
                    return Globalize.format(lastSaveDate(), "t");
                }
            }
            return "";
        });



        self.saveTextResx = ko.observable(lastSaveDate() ? ["AutoSave.LastSavedMessage", lastSaveTimestamp()] : "AutoSave.InitialMessage");
        self.shortSaveTextResx = ko.observable(lastSaveDate() ? ["AutoSave.LastSavedMessageShort", lastSaveTimestamp()] : "AutoSave.InitialMessageShort");

        var lastSaveTextChange = ko.computed(function () {
            if (lastSaveDate())
                createSaveTimeout();
        });

        self.saveButtonClickHandler = function () {
            ko.postbox.publish(autoSaveEvents.AUTOSAVE_SAVEBUTTONPRESSED, new Date());
        }

        ko.postbox.subscribe(autoSaveEvents.AUTOSAVE_SAVESTART, function () {
            self.saveTextResx("AutoSave.SavingMessage");
            self.shortSaveTextResx("AutoSave.SavingMessageShort");
            self.saveError(false);
        });

        ko.postbox.subscribe(autoSaveEvents.AUTOSAVE_SAVECOMPLETE, function () {

            self.saveTextResx("AutoSave.SavedJustNowMessage");
            self.shortSaveTextResx("AutoSave.SavedJustNowMessageShort");
            self.saveError(false);
            lastSaveDate(new Date());
        });

        ko.postbox.subscribe(autoSaveEvents.AUTOSAVE_SAVEERROR, function () {
            self.saveTextResx("AutoSave.ErrorSavingMessage");
            self.shortSaveTextResx("AutoSave.ErrorSavingMessageShort");
            self.saveError(true);
        });


        var currentSaveTimeoutId = null;
        function createSaveTimeout() {

            if (self.saveButtonEnabled()) {
                clearSaveTimeout();

                currentSaveTimeoutId = window.setTimeout(function () {

                    self.saveTextResx(["AutoSave.LastSavedMessage", lastSaveTimestamp()]);
                    self.shortSaveTextResx(["AutoSave.LastSavedMessageShort", lastSaveTimestamp()]);
                }, 15000);

                return currentSaveTimeoutId;
            }
        }
        function clearSaveTimeout() {
            if (currentSaveTimeoutId) {
                window.clearTimeout(currentSaveTimeoutId);
            }
        }




        self.dispose = function () {
            clearSaveTimeout();
            lastSaveTextChange.dispose();

            self.saveButtonEnabled.unsubscribeFrom(autoSaveEvents.AUTOSAVE_SETBUTTONSTATUS);
            ko.postbox.stopPublishingOn(autoSaveEvents.AUTOSAVE_SAVEBUTTONPRESSED);
            ko.postbox.unsubscribeFrom(autoSaveEvents.AUTOSAVE_SAVESTART);
            ko.postbox.unsubscribeFrom(autoSaveEvents.AUTOSAVE_SAVECOMPLETE);
            ko.postbox.unsubscribeFrom(autoSaveEvents.AUTOSAVE_SAVEERROR);
        }
    }

    return { viewModel: AutoSaveViewModel, template: markup };

}));