﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
// 
// StepProgress - indicate to users their location in a step-by-step process
// Parameters:
// steps (required) - array - collection of steps in the step-by-step process
// 
//       step - object - an individual step in the step-by-step process
//              { state : 'previous', text : stepText(), href: stepDestination() }
//       Parameters:
//       text (required)  - text - text of the step
//       href (optional)  - text - URL to which user is taken when he/she clicks on the step link
//                          Default (no value) is '#"
// activeStep (required) - number - indicates which step in the 'steps' array is the active step
//                         Note: any steps lower than this are previous steps; any steps higher than this are future steps.
// Usage:
//
// <step-progress params="steps: [{ text: 'Step 1', href: 'http://www.ellucian.edu/workflow/step1'}, 
//                                { text: 'Step 2', href: 'http://www.ellucian.edu/workflow/step2'}, 
//                                { text: 'Step 3'}], activeStep: 2 "></step-progress>
//
// Notes:
// The template (markup) for this component can be found in ./StepProgress.html
//
define(['text!StepProgress/_StepProgress.html'], function (markup) {
    function StepProgressViewModel(params) {
        var self = this;

        // Configure the steps
        if (typeof params.steps === "undefined" || (typeof params.steps !== 'object' && typeof params.steps !== 'function')) {
            throw "Please provide a valid steps parameter. This parameter must be of type 'object' or 'function.'"
        } else {
            self.steps = ko.isObservable(params.steps) ? params.steps : ko.observableArray([]);
        }

        // Configure the active step for the progress bar
        if (typeof params.activeStep === "undefined" || (typeof params.activeStep !== 'number' && typeof params.activeStep !== 'function')) {
            throw "Please provide a valid activeStep parameter.  This parameter must be of type 'number' or 'function.'"
        } else {
            var proposed = ko.unwrap(params.activeStep);
            if (proposed > self.steps().length -1 || proposed < 0) {
                throw "Please provide a valid activeStep parameter.  This parameter must be between 0 and " + (self.steps().length - 1);
            }
            self.activeStep = ko.isObservable(params.activeStep) ? params.activeStep : ko.observable(params.activeStep);
        }

        // Iterate through the step objects provided in params, updating as needed
        self.steps().forEach(function (step, index, steps) {

            // Set the step text
            if (typeof step.text === "undefined") {
                throw "Please provide a valid step.text parameter."
            } else {
                step.text = ko.isObservable(step.text) ? step.text : ko.observable(step.text);
            }

            // Set the step href
            step.href = ko.computed(function () {
                if (typeof step.href !== "undefined") {
                    return ko.unwrap(step.href);
                }
                return '#';
            });

            // Set the step CSS classes
            step.classes = ko.computed(function (index) {
                var classes = "esg-step-progress__item";
                if (self.steps.indexOf(step) < self.activeStep()) {
                    classes += " esg-is-previous";
                } else if (self.activeStep() === self.steps.indexOf(step)) {
                    classes += " esg-is-active";
                };
                return classes;
            })

            // Dispose function for step
            step.dispose = function () {
                step.classes.dispose();
                step.href.dispose();
            }
        });
    }

    return { viewModel: StepProgressViewModel, template: markup };
});