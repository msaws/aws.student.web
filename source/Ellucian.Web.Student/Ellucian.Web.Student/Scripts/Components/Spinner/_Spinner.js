﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
// 
// Spinner - a "page loading" spinner UI element 
// Parameters:
// isVisible  (required) - boolean - is the message visible at this time?
// message    (optional) - text - the message to be displayed
//                       - empty string ('') will result in the message being hidden
//                       - Default (undefined) is SpinnerDefaultMessage in SiteResources.resx
//
// Usage:
//
// <spinner params="isVisible: showNow, message: someText"></spinner>
//
// Notes:
// The template (markup) for this component can be found in ./_Spinner.html
//
define(['text!Spinner/_Spinner.html'], function (markup) {
    function SpinnerViewModel(params) {
        try {
            if (typeof params.isVisible === "undefined") {
                throw "Please provide a valid isVisible parameter."
            }

            var self = this;
            self.isVisible = ko.isObservable(params.isVisible) ? params.isVisible : ko.observable(params.isVisible);
            
            if (typeof (params.message) === "undefined") {
                self.message = ko.observable(Ellucian.Site.Resources.SpinnerDefaultMessage);
            } else {
                self.message = ko.isObservable(params.message) ? params.message : ko.observable(params.message);
            }

            self.showMessage = ko.observable(true);
            self.wrapperClass = ko.observable("css-spinner-wrapper");

            var noMessageComputed = ko.computed(function () {
                if (!self.message() || self.message().length === 0) {
                    self.showMessage(false);
                    self.wrapperClass("css-spinner-wrapper css-spinner-wrapper--no-padding-top");
                }
                else {
                    self.showMessage = ko.observable(true);
                    self.wrapperClass = ko.observable("css-spinner-wrapper");
                }
            });

            self.dispose = function () {
                noMessageComputed.dispose();
            }
        } catch (error) {
            console.log(error);
        }
    }
    return { viewModel: SpinnerViewModel, template: markup };
});