﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
// 
// StatusLabel - adds a status indication or category to a component. 
//               Default status labels are typically used for taxonomies such as tags, which are used in searching, filtering, and navigating content.
// Parameters:
// text (required) - text -      the text associated with the status label
// type (optional) - text/enum - the purpose of the status label
//                               "error", "pending", "success", or "draft"
// Usage:
//
// <status-label params="text: 123, type:'success'"></status-label>
//
// Notes:
// The template (markup) for this component can be found in ./_StatusLabel.html
//
// When placing status labels on a page, be cognizant of the "scope" of the status label. Status labels do not typically convey information 
// on their own; they should usually be placed alongside meaningful content (for accessibility).
//
define(['text!StatusLabel/_StatusLabel.html'], function (markup) {
    function StatusLabelViewModel(params) {
        var self = this;
        self.text = null;
        if (typeof (params.text) === "undefined") {
            throw "Please provide a valid text value.";
        } else {
            self.text = ko.isObservable(params.text) ? params.text : ko.observable(params.text);
        }

        self.type = (typeof (params.type) !== "undefined" ? (ko.isObservable(params.type) ? params.type : ko.observable(params.type)) : ko.observable(null));
        self.statusLabelClasses = ko.computed(function () {
            var value = "esg-label";
            var modifier = " esg-label--{0}";
            switch (self.type()) {
                case 'error':
                case 'pending':
                case 'success':
                case 'draft':
                    value += modifier.format(self.type());
                    break;
                default:
                    break;
            }
            return value;
        });

        self.dispose = function () {
            self.statusLabelClasses.dispose();
        }
    }
    return { viewModel: StatusLabelViewModel, template: markup };
});