﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
// 
// FileUpload - a file upload component
// Parameters:
// id          (required) - text - The id to apply to the input element
// name        (required) - text - The name to apply to the input element
// label       (required) - text - The text for the field label
// acceptTypes (required) - array/text - an array of accept types 
//                          [ file_extension,"audio/*","video/*","image/*",media_type ]
// value       (optional) - observable to track the selected filename (useful for enabling submit buttons or validating form)
//
// Usage:
//
// <file-upload params="name: 'name', label: someText, acceptTypes: [ '.gif', '.jpg'], value: $data.someObservable"></file-upload>
//
// Notes:
// The template (markup) for this component can be found in ./_FileUpload.html
//
define(['text!FileUpload/_FileUpload.html'], function (markup) {
    function FileUploadModel(params) {
        try {
            if (typeof params.id === "undefined") {
                throw "Please provide a valid 'id' parameter.";
            }
            if (typeof params.name === "undefined") {
                throw "Please provide a valid 'name' parameter.";
            }
            if (typeof params.label === "undefined") {
                throw "Please provide a valid 'label' parameter.";
            }
            if (typeof params.acceptTypes === "undefined" || Array.isArray(ko.unwrap(params.acceptTypes)) === false) {
                throw "Please provide a valid 'acceptTypes' parameter.";
            }
            // If a value is provided, it must be an observable
            if (typeof params.value !== "undefined" && !ko.isObservable(params.value)) {
                throw "Please provide a valid observable for the 'value' parameter";
            }

            var self = this;
            
            self.id = params.id;
            self.name = params.name;
            self.label = params.label;
            self.acceptTypes = ko.pureComputed(function () {
                return params.acceptTypes.join(",");
            });
            self.value = params.value;

        } catch (error) {
            console.log(error);
        }
    }
    return { viewModel: FileUploadModel, template: markup };
});