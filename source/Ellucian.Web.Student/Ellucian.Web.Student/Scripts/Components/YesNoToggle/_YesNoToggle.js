﻿//Copyright 2015 Ellucian Company L.P. and its affiliates.

define(['text!YesNoToggle/_YesNoToggle.html'], function (markup) {
     function YesNoToggleViewModel(params) {
          var self = this;
          self.value = params.valueToToggle;
          self.positive = ko.observable("Yes");
          self.negative = ko.observable("No");
          self.elementId = ko.observable();
          self.checkBoxDescription = ko.observable(params.checkBoxDescription);

          //Optionally pass in specific element ID if a label needs to be associated with component
          if (params.id !== null && typeof params.id !== 'undefined') {
               self.elementId(params.id);
          }

          //Set the text of the toggle button
          if (params.positive !== null && typeof params.positive !== 'undefined') {
               self.positive = ko.observable(params.positive);
          }
          if (params.negative !== null && typeof params.negative !== 'undefined') {
               self.negative = ko.observable(params.negative);
          }
     }
     return { viewModel: YesNoToggleViewModel, template: markup };
});