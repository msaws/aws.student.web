﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

// The FilterControl component accepts up to five arguments:

// name (required):                     * This string (or observable string) is the displayed name of the filter component. 
//                                      * It is also used to compute an id and a placeholder. 
//                                      * It can be an observable or a normal javascript string.

// criteriaArray (required*):           * This observableArray is the list of values that is used for the filter functionality.
//                                      * It can be used to prepopulate (if the observableArray contains values) or 
//                                        just as an observableArray passthrough (if no values are present).
//                                      ** While this argument isn't technically required, without it, you won't be able to 
//                                         access the criteria that the user has selected from outside this component. Since this 
//                                         component doesn't handle the submission of filter criteria, leaving this argument off would 
//                                         basically make the component a UI-only filter.

// validate (optional):                 * This function will be called when it is time to validate the filter selection. 
//                                      * The function should accept a criteria string (to validate) and an array/observableArray of errorMessages.
//                                      * If no function is provided, the validation will always return true.

// placeholder (optional):              * This string (or observable string) is concatenated to the end of the "name" parameter to be used as the placeholder text.
//                                      * The default placeholder is "{name} value or range". Note that only the "value or range" portion is controlled by this argument.

// tooltip (optional):                  * This string (or observable string) is used as the tooltip for the text input icon.
//                                      * If this parameter is not specified, there will be no tooltip and the info-icon will be hidden.

// criteria (optional):                 * This observable is used to access criteria that haven't been added to the criteriaArray.


define(['text!FilterControl/_FilterControl.html'], function (markup) {
    function FilterControlViewModel(params) {
        var self = this;

        var placeholder = "value or range";

        // Name for this component type
        self.ComponentName = ko.computed(function () {
            return ko.unwrap(params.name);
        }, self);

        // Unique ID for this component type
        self.ComponentId = ko.computed(function () {
            return "general-ledger-component-" + self.ComponentName();
        }, self);

        // Placeholder for the component input field
        self.Placeholder = ko.computed(function () {
            return self.ComponentName() + " " + ko.unwrap(params.placeholder || placeholder);
        }, self);

        // Tooltip observable
        self.tooltip = ko.observable(ko.unwrap(params.tooltip || ""));

        // The observable that is bound to the html input
        self.workingCriteria = params.criteria || ko.observable("");

        // The array of entered criteria
        self.tokenizedFilterCriteria = params.criteriaArray || ko.observableArray([]);

        // Error messages
        self.errorMessages = ko.observableArray([]);

        // Wait a short time between a keystroke and kicking off validation. The timer will be cleared every time another keystroke is detected within 500 ms.
        var validationTimer;
        function setValidationTimer() {
            clearTimeout(validationTimer);
            if (self.workingCriteria().replace(/\s+/gm, '').replace(/^,+|,+$/gm, '') === "") {
                // If the inout box is empty, remove the validation errors immediately.
                self.errorMessages.removeAll();
            }
            else {
                // Only validate non-whitespace criteria
                validationTimer = setTimeout(self.validateFilterCriteria, 500);
            }
        }

        // Any time a change occurs (due to textInput binding), set the validation timer.
        self.workingCriteria.subscribe(setValidationTimer);

        // This boolean value indicates if the filter component group is expanded.
        self.expanded = ko.observable(false);

        // Toggle the component input visibility.
        self.toggleCriteria = function () {
            self.expanded(!self.expanded());
        };

        ko.postbox.subscribe("triggerEnterKey", function () {
            clearTimeout(validationTimer);
            self.addFilterCriteria();
        });
        
        // Fires the addFilterCriteria function when the enter key is pressed and cancels any validationTimer that 
        // might be running (since the addFilterCriteria function will validate immediately, anyway).
        self.keyUp = function (d, e) {
            if (e.keyCode == 13) {
                clearTimeout(validationTimer);
                self.addFilterCriteria();
            }
        };

        // Validate then add the criteria specified by the user to the list of tokenized filter criteria.
        self.addFilterCriteria = function () {
            if (self.workingCriteria().replace(/\s+/gm, '').replace(/^,+|,+$/gm, '') === "") {
                // Don't add blank values to the list of criteria.
            }
            else if (self.validateFilterCriteria()) {
                // Validation successful - add unique criteria to the tokenized filter criteria array and clear the text input.
                self.tokenizedFilterCriteria.pushAll(self.uniqueCriteria(self.workingCriteria()));
                self.workingCriteria("");
            }
        };

        // Remove a criteria token from the list of tokenized filter criteria.
        self.removeFilterCriteria = function (criteriaToken) {
            self.tokenizedFilterCriteria.remove(criteriaToken);
        };

        // Return the unique/distinct criteria tokens (as an array), given a comma-delimited list of 1-to-many tokens.
        self.uniqueCriteria = function (criteria) {
            var uniqueCriteria = [];
            // Remove whitespace and trim commas from beginning and end of criteria and split into an array.
            var subCriteria = criteria.replace(/\s+/gm, '').replace(/^,+|,+$/gm, '').split(",");

            for (var s = 0; s < subCriteria.length; s++) {
                if (self.tokenizedFilterCriteria().indexOf(subCriteria[s]) === -1
                    && uniqueCriteria.indexOf(subCriteria[s]) === -1) {
                    uniqueCriteria.push(subCriteria[s]);
                }
            }
            return uniqueCriteria;
        };

        // Wrapper method for the validateLength function
        self.validateFilterCriteria = function () {
            self.errorMessages.removeAll();
            if (params.validate) {
                // Call function from params (if available).
                return params.validate(self.workingCriteria(), self.errorMessages);
            }
            else {
                return true;
            }
        };

    }
    return { viewModel: FilterControlViewModel, template: markup };
});
