﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
// 
// The CsvDownloadLink component is designed to allow the conversion of an arbitrary, but simple,
// array or observableArray into a *.csv file by using a Blob.
//
// The following parameters are expected.  
// data: Required, the array (or observableArray) object that is to be converted into csv.
//       It is assumed the array contains similar objects (keys for values are consistent across objects in array).
//       Currently, this component only supports 'simple' objects in the array (array elements do not contain nested objects).
//
// linkText: Optional, default: "Download File", descriptive text (string or observable) to use as the visible link text.
//
// fileName: Optional, default: "download.csv", the name of the file (string or observable) that will be downloaded (using the download attribute)
//         
// includeKeys: Optional, default: true, include the array keys as the first line of the csv file
//
// limitToKeys: Optional, default: empty array, an array or observableArray of strings that contains the keys that you wish to include in the CSV file.
//              Any key that is not in this list will be omitted from the CSV file. When the array is empty or not included in the params, no omission will occur.
//              The order will be the same as in the array argument.
//              Example: key1, key2, key3
//
// keyTextReplacements: Optional, default: empty array, an array or observableArray of strings that contains the replacement values for the keys listed in "limitToKeys"
//
define(['text!CsvDownloadLink/_CsvDownloadLink.html'], function (markup) {
    function CsvDownloadLinkViewModel(params) {
        var self = this;

        try {
            // Get the desired file name and link text from params.  Use default values if not provided.
            var defaultLinkText = "Download File",
                defaultFileName = "download.csv";

            // Optional list of keys with which to limit the columns of data exported
            self.limitToKeys = ko.computed(function () {
                return ko.unwrap(params.limitToKeys ? params.limitToKeys : []);
            }, self);

            // Optional list of key text replacements for the columns of data exported
            self.keyTextReplacements = ko.computed(function () {
                return ko.unwrap(params.keyTextReplacements ? params.keyTextReplacements : []);
            }, self);

            // Optional boolean value to determine if keys should be included
            self.includeKeys = ko.computed(function () {
                return ko.unwrap(params.includeKeys != null && typeof params.includeKeys !== "undefined" ? params.includeKeys : true);
            }, self);

            // The text to display for the link
            self.linkText = ko.computed(function () {
                return ko.unwrap(params.linkText ? params.linkText : defaultLinkText);
            }, self);

            // The name of the file that will be downloaded
            self.fileName = ko.computed(function () {
                return ko.unwrap(params.fileName ? params.fileName : defaultFileName);
            }, self);

            // Computed contains a "cleaned" (no knockout artifacts - if any were present to begin with) version of the raw data
            self.jsData = ko.computed(function () {
                return ko.toJS(params.data);
            }, self);

            // Used when the user-friendly href is swapped out for the blob href
            self.overrideHref = ko.observable("");

            // Used to determine which href is currently active
            self.href = ko.computed({
                read: function () {
                    if (self.overrideHref()) {
                        // The href has been changed (by the download function) so return that value instead of the one from the argument
                        return self.overrideHref();
                    }
                    else {
                        // If there is no overriden href value, pass the observable through from the params argument.
                        return self.fileName();
                    }
                },
                write: function (value) {
                    if (value === self.fileName()) {
                        // If we are setting the href back to the same value as the argument version, clear out the overrideHref value so that the read() will process correctly.
                        self.overrideHref("");
                    }
                    else {
                        // If we are changing the href to something other than the value from the argument, set the overrideHref so that read() will process the explicit value.
                        self.overrideHref(value);
                    }
                },
                owner: self     // Ensure "this" context is same as self.
            });

            // Store data in a blob using the following logic:
            // 0. Ensure that there is data in the jsData observable.
            //      0.1 Determine order of the columns
            //      0.2 Replace headers with their replacements from the keyTextReplacements computed.
            //      0.3 Sort the headers
            // 1. (If includeKeys is true) Add the sorted keys as the first line of csv (possibly limited by the limitToKeys array)
            // 2. Loop over array, get values, and insert values into csv string (possibly limited by the limitToKeys array)
            //      2.1 Respect the ordering from step 0.1
            //      2.2 Append row content with a '\n' at line-end
            self.convertToCsv = ko.computed(function () {
                var csv = "",
                    keyArray = [],
                    keyIndexArray = [];

                if (self.jsData().length > 0) {     // 0
                    keyArray = Object.keys(self.jsData()[0]).filter(function limit(e) {
                        return (self.limitToKeys().length > 0 && self.limitToKeys().indexOf(e) >= 0) || self.limitToKeys().length === 0;
                    });
                    // 0.1 - Determine the order of the columns...
                    for (var m = 0; m < self.limitToKeys().length; m++) {
                        var originalIndex = keyArray.indexOf(self.limitToKeys()[m]);
                        keyIndexArray.push(originalIndex);
                    }

                    // 0.2 - If there are the same number of keys to include as replacement headers, use the replacement header for each key...
                    if (self.limitToKeys().length > 0 && self.keyTextReplacements().length === self.limitToKeys().length) {
                        keyArray = keyArray.map(function (k, initialIndex) {
                            var idx = self.limitToKeys().indexOf(k);
                            return self.keyTextReplacements()[idx];
                        });
                    }

                    // 0.3 - Sort keys
                    var sortedKeyData = [];
                    for (var k = 0; k < keyIndexArray.length; k++) {
                        sortedKeyData.push(keyArray[keyIndexArray[k]]);
                    }
                    if (sortedKeyData.length === 0) {
                        sortedKeyData = keyArray;
                    }


                    if (self.includeKeys()) {       // 1
                        for (var i = 0; i < sortedKeyData.length; i++) {
                            if (i > 0) { csv += ","; }
                            csv += '"' + sortedKeyData[i] + '"';
                        }
                        csv += '\n';
                    }
                    // 2 - Loop over data
                    for (var i = 0; i < self.jsData().length; i++) {
                        var rowData = Object.keys(self.jsData()[i]).map(    // Row
                            function (k) {                                  // Column
                                // Add column if the key is listed in limitToKeys or if there was no limitToKeys specified.
                                if ((self.limitToKeys().length > 0 && self.limitToKeys().indexOf(k) >= 0) || self.limitToKeys().length === 0) {
                                    // Convert each cell to a string type by concatenating an empty string.
                                    // Replace any double quotes with two double quotes.
                                    // Encapsulate each cell in double quotes to ensure that cells with commas render correctly.
                                    return '"' + (self.jsData()[i][k] + '').replace(/["]/gm, '""') + '"';
                                }
                            }
                        ).filter(function removeUndefined(e) {
                            return e !== undefined;                 // Remove any undefined values (those columns that were not specified in limitToKeys).
                        });
                        // 2.1 - Respect ordering
                        var sortedRowData = [];
                        for (var j = 0; j < keyIndexArray.length; j++) {
                            sortedRowData.push(rowData[keyIndexArray[j]]);
                        }

                        if (sortedRowData.length === 0) {
                            sortedRowData = rowData;
                        }
                        // 2.2 - Append row content
                        csv += sortedRowData.join(",") + '\n';
                    }
                }
                return csv;
            }, self);

            // Create blob (local, file-like object in the browser) using the CSV data.
            self.blob = ko.computed(function () {
                return new Blob([self.convertToCsv()], { type: "text/csv" });
            }, self).extend({ deferred: true });

            // 4. Set viewModel.href to the ugly, generated URL for the blob just in time.
            // 5. Once the download has begun, use a 5ms timeout to revert the href back to the pretty version.
            self.downloadFile = function () {
                if (window.navigator.msSaveOrOpenBlob) {
                    var oldHref = self.href();
                    self.href("#");
                    setTimeout(function () {
                        self.href(oldHref);
                    }, 5);
                    window.navigator.msSaveOrOpenBlob(self.blob(), self.fileName());
                    return false;
                }
                else {
                    var oldHref = self.href();
                    self.href(window.URL.createObjectURL(self.blob()));
                    setTimeout(function () {
                        self.href(oldHref);
                    }, 5);
                    return true;
                }

            };
        }
        catch (err) {
            console.log(err);
        }
    }
    return { viewModel: CsvDownloadLinkViewModel, template: markup };
});