﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
// 
// PersonRestrictionsWidget - a widget that displays person restrictions for a given person.
// Parameters:
// personId  (required) - text   - ID of the person whose restrictions will be displayed
// isOpen    (optional) - bool   - Indicates if the widget is open or closed by default
//                                 Defaults to false
// Usage:
//
// <person-restrictions-widget params="personId: '0001234', isOpen: true"></person-restrictions-widget>
//
// Notes:
// The template (markup) for this component can be found in ./_PersonRestrictionsWidget.html
//
define(['text!PersonRestrictionsWidget/_PersonRestrictionsWidget.html'], function (markup) {
    function PersonRestrictionsViewModel(params) {
        var self = this;

        // Validate/set personId parameter
        if (typeof (params.personId) === "undefined") {
            throw "Please provide a valid person ID.";
        } else {
            self.personId = ko.isObservable(params.personId) ? params.personId : ko.observable(params.personId);
        }

        // Check for/set isOpen parameter; set to false if not specified
        if (typeof (params.isOpen) === "undefined") {
            params.isOpen = false;
        }
        self.isOpen = ko.isObservable(params.isOpen) ? params.isOpen : ko.observable(params.isOpen);

        self.showWidget = ko.observable(true);

        // Declare view model properties
        self.Restrictions = ko.observableArray([]);

        // Declare function for retrieving restrictions
        self.getRestrictions = function () {
            var url = Ellucian.PersonRestrictions.ActionUrls.getPersonRestrictionsModelActionUrl;
            if (self.personId()) {
                url += "/" + self.personId()
            }
            $.ajax({
                url: url,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json"
            }).done(function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    ko.mapping.fromJS(data, {}, self);
                }
            }).fail(function (jqXHR) {
                if (jqXHR.status === 403) {
                    // Silently hide the widget if the user does not have permission to see restrictions
                    self.showWidget(false);
                } else if (jqXHR.status != 0) {
                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Site.Resources.PersonRestrictionsWidgetError, type: "error" });
                }
            });
        }

        // Subscribe to person ID changes to trigger restriction retrievals
        self.personIdChange = self.personId.subscribe(function () {
            self.getRestrictions();
        });

        // Retrieve person restrictions on initialization
        self.getRestrictions(self.personId());

        // Clean-up
        self.dispose = function () {
            self.personIdChange.dispose();
        }
    }
    return { viewModel: PersonRestrictionsViewModel, template: markup };
});