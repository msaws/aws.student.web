﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
// 
// ProfilePhoto - UI element to load a user's avatar, with a fallbak to a default
// Parameters:
// id         (required) - text - the person's system ID
//
// altText    (optional) - text - the alt text to be used
//                       - defaults to "Person's profile photo"
//
// size    (optional)    - text - small, medium, or large
//                       - defaults medium
//
// usedInTable (optional)- if used in a table, vertically aligns image
//                       - defaults to false
// Usage:
// <profilePhoto params="id: personId, altText: someText, size: 'large', usedInTable: true"></spinner>
//
// Notes:
// The template (markup) for this component can be found in ./ProfilePhoto.html
//
define(['text!ProfilePhoto/_ProfilePhoto.html'], function (markup) {
    function ProfilePhotoViewModel(params) {
        var self = this;

        // Validate/set id property
        if (typeof (params.id) === "undefined") {
            throw "Please provide a valid person ID."
        } else {
            self.id = ko.isObservable(params.id) ? params.id : ko.observable(params.id);
        }

        // Set altText property
        if (typeof (params.altText) === "undefined") {
            self.altText = ko.observable(Ellucian.Site.Resources.PersonProfilePhotoDefaultAltText);
        } else {
            self.altText = ko.isObservable(params.altText) ? params.altText : ko.observable(params.altText);
        }

        // Set usedInTable property
        if (typeof (params.usedInTable) === "undefined") {
            self.usedInTable = ko.observable(false);
        }
        else {
            self.usedInTable = ko.isObservable(params.usedInTable) ? params.usedInTable : ko.observable(params.usedInTable);
        }

        // Set classes computed property
        self.classes = ko.pureComputed(function () {
            var classes = "esg-avatar esg-avatar--";
            if (typeof (params.size) !== "undefined") {
                switch (ko.unwrap(params.size)) {
                    case 'large':
                        classes += "large";
                        break;
                    case 'small':
                        classes += "small";
                        break;
                    case 'medium':
                    default:
                        classes += "medium";
                }
            }
            else {
                classes += "medium";
            }

            if (self.usedInTable()) {
                classes += " esg-avatar--vertical-align-middle";
            }

            return classes;
        });

        // Set source computed property
        self.source = ko.pureComputed(function () {
            return userPhotoBaseUrl + self.id();
        });

        self.dispose = function () {
            self.classes.dispose();
            self.source.dispose();
        }
    }

    return { viewModel: ProfilePhotoViewModel, template: markup };
});