﻿(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["text!CollapsibleGroup/_CollapsibleGroup.html"], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory(require('./_CollapsibleGroup.html'));
    }
}(this, function (markup) {
    /*
     * params = {
     *  isOpen: bool - observable. also indicates the default position of the collapsible group. is updated when the group opens and closes. defaults to false
     *  tertiaryContent: template name or object containing tertiary content
     *  content: object. the data used to render elements in the component 
     *  titleText: string - collapsible group title text
     *  subtitleText: string - collapsible group subtitle text
     *  titleContent: template name or object containing additional markup for the collapsible group header (displayed inline with main header)
     *  tertiaryContent: template name or object containing additional markup for the collapsible group header (displayed below main header/subheader)
     * }
     * Sample Markup:
     * <collapsible-group params="
            titleText: 'This Is a Title',
            subtitleText: 'This Is a Subtitle',
            isOpen: true,
            titleContent: 'template-name', // will default to template name, but can also pass in data, nodes, etc., or object
            tertiaryContent: 'template-name' // will default to template name, but can also pass in data, nodes, etc., or object">
            <div id="test-collapsible-body">Body</div>
        </collapsible-group>
     */
    function CollapsibleGroupViewModel(params) {

        var self = this;

        if (!params.titleText) {
            throw new Error("params.titleText is required");
        }

        // use jQuery to find the collapsibles and assign IDs that way instead (select 'collapsible-group')
        noOfCollapsibles = ($('collapsible-group').length) + 1 || 0;

        self.groupBodyId = 'collapsible-' + noOfCollapsibles + '-collapseBody';
        self.groupHeaderId = 'collapsible-' + noOfCollapsibles + '-groupHeading';

        self.titleText = ko.unwrap(params.titleText);
        self.subtitleText = ko.unwrap(params.subtitleText);
        self.titleContent = params.titleContent || '';
        self.tertiaryContent = params.tertiaryContent || '';
        self.content = params.content;

        var isOpen = ko.observable(ko.unwrap(params.isOpen));
        var isOpenSub = isOpen.subscribe(function (newVal) {
            if (ko.isWriteableObservable(params.isOpen)) {
                params.isOpen(newVal);
            }
        });

        // determine if there is subtitle text
        if (self.subtitleText === null || typeof self.subtitleText === 'undefined') {
            self.hasSubtitle = ko.observable(false);
        }
        else {
            self.hasSubtitle = ko.observable(true);
        }

        // determine if there is title content
        if (self.titleContent === null || typeof self.titleContent === 'undefined') {
            self.hasTitleContent = ko.observable(false);
        }
        else {
            self.hasTitleContent = ko.observable(true);
        }

        // determine if there is tertiary content
        if (self.tertiaryContent === null || typeof self.tertiaryContent === 'undefined') {
            self.hasTertiaryContent = ko.observable(false);
        }
        else {
            self.hasTertiaryContent = ko.observable(true);
        }

        self.subtitleClass = ko.pureComputed(function () {
            return self.hasSubtitle ? 'esg-has-subtitle' : '';
        });

        self.subtitleWrapperClass = ko.pureComputed(function () {
            return self.hasSubtitle ? 'esg-collapsible-group-subtitle__wrapper esg-col-xs-12 esg-col--float-none esg-col--padding-0' : 'esg-col-xs-11 esg-col--padding-0';
        });

        self.collapsibleClass = ko.pureComputed(function () {
            if (isOpen()) {
                return 'esg-is-open';
            }
            else {
                return 'esg-is-collapsed';
            }
        });

        self.arrowIconClass = ko.pureComputed(function () {
            return isOpen() ? 'esg-icon--up' : 'esg-icon--down';
        });
        self.toggleCollapsible = function (data, event) {
            isOpen(!isOpen());
            return;
        }

        self.dispose = function () {
            isOpenSub.dispose();
        }
    }


    return { viewModel: CollapsibleGroupViewModel, template: markup };

}));

