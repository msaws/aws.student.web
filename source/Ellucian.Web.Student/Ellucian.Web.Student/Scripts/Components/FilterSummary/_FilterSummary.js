﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

// The FilterSummary accepts the following arguments:

// selections (required)                * This array/observableArray of strings contains the content to display in each "badge".


define(['text!FilterSummary/_FilterSummary.html'], function (markup) {
    function FilterSummaryViewModel(params) {
        var self = this;

        // Ensure that "badgeArray" is an observable array
        if (typeof params.selections === "function") {
            self.badgeArray = params.selections;
        }
        else {
            self.badgeArray = ko.observableArray(params.selections);
        }
    }
    return { viewModel: FilterSummaryViewModel, template: markup };
});
