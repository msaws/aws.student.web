﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
// 
// ComplexTable - a table that will hide, show, and re-order columns
// Parameters:
// rows             (required) - Array of objects (rows), each containing an array of text (cells)
// columnProperties (required) - Array of objects (column settings), each containing an array of text (properties for each column, ex. order, visibility, templateName for cell)
// tableId          (required) - Unique Id to identify the table, used as identifier in memory
// tableCaption     (optional) - Caption for the table, default is null if not provided
//
// Usage:
//
// <complex-table params="rows: dataRows, columnProperties: dataProperties, tableId: 'testScores'"></complex-table>
//
// Notes:
// The template (markup) for this component can be found in ./_ComplexTable.html
// Please see complex.table.component.tests.js for sample data structure for rows and columnProperties params
// Pages MUST follow the established JSON format to ensure proper functionality!
//
define(['text!ComplexTable/_ComplexTable.html'], function (markup) {
    function ComplexTableViewModel(params) {
        var self = this;

        //Select which storage mechanism to use depending on browser
        var memory = window.localStorage ||
                        (window.UserDataStorage && new UserDataStorage()) ||
                        new CookieStorage();

        self.columnProperties = ko.observableArray([]);

        self.columnPropertiesDefault = ko.observableArray([]);

        // The unique ID of the table
        self.tableId = null;
        if (typeof (params.tableId) === "undefined") {
            throw "Please provide a valid tableId value.";
        } else {
            self.tableId = ko.isObservable(params.tableId) ? params.tableId : ko.observable(params.tableId);
        }

        // Table caption text (optional)
        self.tableCaption = (typeof (params.tableCaption) !== "undefined" ? (ko.isObservable(params.tableCaption) ? params.tableCaption : ko.observable(params.tableCaption)) : ko.observable(null));

        self.columnPropertiesFromMemory = null;

        if (typeof (params.columnProperties) === "undefined") {
            throw "Please provide a valid columnProperties value.";
        } else {
            // Save initial params as default properties
            ko.utils.arrayForEach(ko.unwrap(params.columnProperties),
                function (columnProperty) {
                    self.columnPropertiesDefault.push({
                        headerText: ko.isObservable(columnProperty.headerText)
                            ? columnProperty.headerText
                            : ko.observable(columnProperty.headerText),
                        headerId: ko.isObservable(columnProperty.headerId)
                            ? columnProperty.headerId
                            : ko.observable(columnProperty.headerId),
                        isVisible: ko.isObservable(columnProperty.isVisible)
                            ? columnProperty.isVisible
                            : ko.observable(columnProperty.isVisible),
                        order: ko.isObservable(columnProperty.order)
                            ? columnProperty.order
                            : ko.observable(columnProperty.order),
                        templateName: ko.isObservable(columnProperty.templateName)
                            ? columnProperty.templateName
                            : ko.observable(columnProperty.templateName)
                    });
                }
            );

            // If memory contains key of tableId, replace default param properties with memory properties
            if (memory.getItem(self.tableId()) !== null) {
                // Get and parse columnProperties stored in memory
                self.columnPropertiesFromMemory = JSON.parse(memory.getItem(self.tableId()));

                params.columnProperties = self.columnPropertiesFromMemory;
            }

            // Current column properties (may be replaced by properties in memory)
            ko.utils.arrayForEach(ko.unwrap(params.columnProperties),
                function (columnProperty) {
                    self.columnProperties.push({
                        headerText: ko.isObservable(columnProperty.headerText)
                            ? columnProperty.headerText
                            : ko.observable(columnProperty.headerText),
                        headerId: ko.isObservable(columnProperty.headerId)
                            ? columnProperty.headerId
                            : ko.observable(columnProperty.headerId),
                        isVisible: ko.isObservable(columnProperty.isVisible)
                            ? columnProperty.isVisible
                            : ko.observable(columnProperty.isVisible),
                        order: ko.isObservable(columnProperty.order)
                            ? columnProperty.order
                            : ko.observable(columnProperty.order),
                        templateName: ko.isObservable(columnProperty.templateName)
                            ? columnProperty.templateName
                            : ko.observable(columnProperty.templateName)
                    });
                }
            );
        }

        // Initial page load sort
        self.columnProperties().sort(dynamicSort('order'));

        // Save column properties in memory
        self.saveColumnProperties = function (data, event) {
            // Session storage cannot store objects, so they must be converted to JSON on set and parsed on get
            memory.setItem(self.tableId(), ko.toJSON(self.columnProperties()));
            $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.SharedComponents.ComplexTable.ComplexTableSaveSuccess, type: "success", flash: true });
        }

        // Reset column properties from default
        self.resetColumnProperties = function (data, event) {
            // For each item in columnProperties, replace values with default settings
            for (i = 0; i < self.columnProperties().length; i++) {
                self.columnProperties()[i].headerId(self.columnPropertiesDefault()[i].headerId());
                self.columnProperties()[i].headerText(self.columnPropertiesDefault()[i].headerText());
                self.columnProperties()[i].isVisible(self.columnPropertiesDefault()[i].isVisible());
                self.columnProperties()[i].order(self.columnPropertiesDefault()[i].order());
                self.columnProperties()[i].templateName(self.columnPropertiesDefault()[i].templateName());
            }

            // When we update the column order, it doesn't notify subscribers, so call valueHasMutated again
            self.columnProperties.valueHasMutated();

            // Remove saved column properties from memory
            memory.removeItem(self.tableId());
            $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.SharedComponents.ComplexTable.ComplexTableResetSuccess, type: "success", flash: true });
        }

        // Ensure that our array is always sorted
        // From http://stackoverflow.com/questions/17153309/how-to-ensure-a-knockout-observable-array-remains-sorted
        var sortedColumnPropertiesSubscription = self.columnProperties.subscribe(onMyArrayChange);
        function onMyArrayChange() {
            //Remove the subscription before sorting, to prevent an infinite loop
            sortedColumnPropertiesSubscription.dispose();
            sortedColumnPropertiesSubscription = null;

            //Force a sort of the array here. 
            self.columnProperties().sort(dynamicSort('order'));

            // When we update the column order, it doesn't notify subscribers, so call valueHasMutated again
            self.columnProperties.valueHasMutated();

            //Re-subscribe
            sortedColumnPropertiesSubscription = self.columnProperties.subscribe(onMyArrayChange);
        }

        // The rows of the table (array of objects containing row properties and row data)
        if (typeof (params.rows) === "undefined") {
            throw "Please provide a valid rows value.";
        } else {
            self.rows = ko.isObservable(params.rows) ? params.rows : ko.observable(params.rows);
        }

        self.dropdownIsActive = ko.observable(false);

        self.toggleDropdown = function (data, event) {
            data.dropdownIsActive(!data.dropdownIsActive());
        }

        self.columnToMoveDown = ko.observable();
        self.columnToMoveDownNext = ko.observable();
        // Move column down by swapping order with next item
        self.moveColumnDown = function (data, event) {
            self.columnToMoveDown(data.order());
            if (self.columnToMoveDown() < self.columnProperties().length - 1) {
                self.columnToMoveDownNext(self.columnToMoveDown() + 1);
                self.columnProperties()[self.columnToMoveDownNext()].order(self.columnProperties()[self.columnToMoveDownNext()].order() - 1);
                self.columnProperties()[self.columnToMoveDown()].order(self.columnProperties()[self.columnToMoveDown()].order() + 1);
                self.columnProperties.valueHasMutated();
            }
        }

        self.columnToMoveUp = ko.observable();
        self.columnToMoveUpPrevious = ko.observable();
        // Move column up by swapping order with previous item
        self.moveColumnUp = function (data, event) {
            self.columnToMoveUp(data.order());
            if (self.columnToMoveUp() > 0) {
                self.columnToMoveUpPrevious(self.columnToMoveUp() - 1);
                self.columnProperties()[self.columnToMoveUpPrevious()].order(self.columnProperties()[self.columnToMoveUpPrevious()].order() + 1);
                self.columnProperties()[self.columnToMoveUp()].order(self.columnProperties()[self.columnToMoveUp()].order() - 1);
                self.columnProperties.valueHasMutated();
            }
        }

        // Determine first visible column in order to display expand/collapse caret
        self.firstVisibleColumn = ko.computed(function () {
            var visibleColumn = ko.utils.arrayFirst(self.columnProperties(), function (column) {
                return column.isVisible();
            });

            if (visibleColumn !== null) {
                return visibleColumn.headerId();
            }
            else {
                return null;
            }
        });
    }
    return { viewModel: ComplexTableViewModel, template: markup };
});

