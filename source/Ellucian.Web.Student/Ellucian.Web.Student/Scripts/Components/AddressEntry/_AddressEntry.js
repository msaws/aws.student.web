﻿//Copyright 2015 Ellucian Company L.P. and its affiliates.

define(['text!AddressEntry/_AddressEntry.html'], function (markup) {
     function AddressEntryViewModel(params) {
          var self = this;

          //Variables
          self.hasErrorsFlag = ko.observable(false);
          self.States = params.AddressInfo().States;
          self.Countries = params.AddressInfo().Countries;
          self.internationalSelected = params.AddressInfo().internationalSelected;
          self.errorMessage = ko.observable(params.errorMessage);
          self.checkBoxDescription = ko.observable(params.checkBoxDescription);

          //Address Information
          self.Street1 = params.AddressInfo().Street1;
          self.Street2 = params.AddressInfo().Street2;
          self.Street3 = params.AddressInfo().Street3;
          self.Street4 = params.AddressInfo().Street4;
          self.City = params.AddressInfo().City;
          self.State = params.AddressInfo().State;
          self.PostalCode = params.AddressInfo().PostalCode;
          self.Country = params.AddressInfo().Country;

          if (params.hasErrorsFlag != null) {
               self.hasErrorsFlag(params.hasErrorsFlag);
          }
          self.CountryLabel = ko.computed(function () {
               if (self.State() && self.State().CountryCode != '' && !self.internationalSelected()) {
                    var c = ko.utils.arrayFirst(self.Countries(), function (item) {
                         return item.Code === self.State().CountryCode;
                    });
                    if (c != null) {
                         return c.Description;
                    }
               }
          });
          self.ShowCountryLabel = ko.computed(function () {
               return !self.internationalSelected() && self.CountryLabel() != null;
          });
         
          self.countryError = ko.computed(function () {
               return (params.AddressInfo().Country.isValid()) ? "" : params.AddressInfo().Country.error;
          },this);
     }

     return { viewModel: AddressEntryViewModel, template: markup };
});