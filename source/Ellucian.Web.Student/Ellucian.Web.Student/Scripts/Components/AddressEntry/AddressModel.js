﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

function AddressModel() {
     var self = this;
     self.validate = ko.observable(true);
     self.Countries = ko.observableArray();
     self.filteredCountries = ko.observableArray();
     self.States = ko.observableArray();
     self.internationalSelected = ko.observable(false);
     self.Street1 = ko.observable("").extend({
          required: {
               onlyIf: function () { return self.validate() }
          }
     });
     self.Street2 = ko.observable("");
     self.Street3 = ko.observable("");
     self.Street4 = ko.observable("");
     self.City = ko.observable("").extend({
          required: {
               onlyIf: function () { return !self.internationalSelected() && self.validate() }
          }
     });
     self.State = ko.observable().extend({
          required: {
               onlyIf: function () { return !self.internationalSelected() && self.validate() }
          }
     });
     self.PostalCode = ko.observable("").extend({
          required: {
               onlyIf: function () { return !self.internationalSelected() && self.validate() }
          }
     });
     self.Country = ko.observable().extend({
          required: {
               onlyIf: function () { return self.internationalSelected() && self.validate() }
          }
     });
    self.TypeCode = ko.observable("");

     //Remove US and Canada  from Countries if International is selected
     //Insert US and Canada from Countries if International is not selected
     self.filter = ko.computed(function () {
          if (self.internationalSelected()) {
               if (self.filteredCountries().length === 0) {
                    var list = self.Countries.remove(function (item) { return item.Code === "USA" || item.Code === "US" || item.Code === "CA" })
                    for (var i = 0; i < list.length; i++) {
                         self.filteredCountries.push(list[i]);
                    }
               }
               else {
                    ko.utils.arrayForEach(self.filteredCountries(), function (c) {
                         self.Countries.remove(c);
                    });
               }
          }
          else {
               ko.utils.arrayForEach(self.filteredCountries(), function (c) {
                    self.Countries.push(c);
               });
          }
     }, this);
};
