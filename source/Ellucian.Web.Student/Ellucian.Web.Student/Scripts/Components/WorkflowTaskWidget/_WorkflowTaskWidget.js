﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

define(['text!WorkflowTaskWidget/_WorkflowTaskWidget.html', 'WorkflowTaskWidget/WorkflowTaskModel'], function (markup, WorkflowTaskModel) {
    function WorkflowTaskWidgetViewModel() {
        var self = this;
        
        this.workflowTasks = ko.observableArray([]);

        this.workflowCategoryTasks = ko.pureComputed(function () {
            var categoryTaskObject = {};
            var categoryTasks = [];

            // Count up the tasks by category, stored as { category1: { count: n, processLink: 'url' }, category2: ... }
            ko.utils.arrayForEach(self.workflowTasks(), function (workflowTask) {
                if (categoryTaskObject[workflowTask.Category()]) {
                    categoryTaskObject[workflowTask.Category()].count++;
                } else {
                    categoryTaskObject[workflowTask.Category()] = { count: 1, processLink: workflowTask.ProcessLink() };
                }
            });

            // Transform the previous object into one more easily iterated over by knockout,
            // an array of { category: 'name', numberOfTasks: n, processLink: 'url' }
            for (var key in categoryTaskObject) {
                if (categoryTaskObject.hasOwnProperty(key)) {
                    var categoryTask = {
                        category: key,
                        numberOfTasks: categoryTaskObject[key].count,
                        processLink: categoryTaskObject[key].processLink
                    }
                    categoryTasks.push(categoryTask);
                }
            }
            return categoryTasks;
        });

        this.totalNumberOfTasks = ko.pureComputed(function () {
            return self.workflowTasks().length;
        });

        this.showMobileTaskList = function () {
            ko.postbox.publish("widgetTemplate", "workflow-task-list-mobile-template");
            ko.postbox.publish("widgetData", self);
            ko.postbox.publish("activeWidget", true);
            return true;
        }

        this.hideMobileTaskList = function () {
            ko.postbox.publish("activeWidget", false);
            ko.postbox.publish("widgetTemplate", "");
            ko.postbox.publish("widgetData", null);
            return true;
        }

        $(document).ready(function () {
            $.ajax({
                url: getWorkflowTasksActionUrl,
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        var workflowTaskModels = [];

                        // The data received should be an array of objects used to create WorkflowTaskModels
                        for (var key in data) {
                            if (data.hasOwnProperty(key)) {
                                var taskModel = new WorkflowTaskModel(data[key]);
                                workflowTaskModels.push(taskModel);
                            }
                        }
                        self.workflowTasks(workflowTaskModels);
                    }
                }
            });
        });
    }

    return { viewModel: WorkflowTaskWidgetViewModel, template: markup };
});