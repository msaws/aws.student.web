﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
define([], function () {
    function WorkflowTaskModel(source) {
        this.Id = ko.observable(source.Id);
        this.Category = ko.observable(source.Category);
        this.Description = ko.observable(source.Description);
        this.ProcessLink = ko.observable(source.ProcessLink);
    }

    return WorkflowTaskModel;
});