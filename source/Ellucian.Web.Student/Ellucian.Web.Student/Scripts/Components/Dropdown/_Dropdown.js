﻿/* Copyright 2017 Ellucian Company L.P. and its affiliates. */
(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["text!Dropdown/_Dropdown.html"], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory(require('./_Dropdown.html'));
        if (!ko.components.isRegistered('dropdown')) {
            ko.components.register('dropdown', module.exports);
        }
    }
}(this, function (markup) {


    /*
     * Generic ESG-ified Dropdown component, intended to mimic the functionality of knockout's options binding
     * 
     * params = {
     *  buttonId: customId for the button. if none is supplied one will be generated in order to accomodate accessibility. Recomend that you pass in an observable so that the generated Id can be passed back to the parent component
     *  buttonCaption: caption displayed on the button when no options are selected,
     *  items: array (or observableArray) of strings or arbitrary objects
     *  itemsText: (optional) if using arbitrary objects, a string identifying the attribute of the object containing the text to display, or a function that computes the text to display,
     *  itemsValue: (optional) if using arbitrary objects, a string identifying the attribute of the object containing the value to return, or a function that computes thevalue to return,
     *  value: observable that will be updated with the selected value,
     *  afterRender: callback function invoked after the component is rendered. useful if you want to apply post-processing display to the component, such as adding or removing classes based on screen size. Callback function is pass an array of the DOM nodes rendered by the component
     * }
     */
    function DropdownViewModel(params) {

        var self = this;

       
        if (!params.value || !ko.isObservable(params.value)) {
            throw new Error("params.value should be an observable");
        }
        if (!params.items) {
            throw new Error("params.items should have value");
        }

        //the caption that displays on the button. allows 'undefined' to be selected   
        self.buttonCaption = ko.unwrap(params.buttonCaption);

        //computed wrapper around params.items so the dropdown re-renders when items is updated
        //if a caption is specified, unshift (add to front) an undefined value.
        var originalItems = ko.computed(function () {
            var itemList = ko.isObservable(params.items) ? params.items() : params.items;
            if (self.buttonCaption) {
                if (itemList[0] !== undefined) {
                    itemList.unshift(undefined);
                }
            }
            return itemList;
        });

        //if both the buttonCaption and the value parameter are null or undefined, then
        //set the value parameter to be the first item in the original items list - mimics the functionality of knockout's options binding
        if (!self.buttonCaption && !params.value()) {
            params.value(originalItems()[0]);
        }

        //computes the id of the button that controls the dropdown. the id can be passed in as a param or computed
        //by this function. the id is necessary for aria-labelledby.
        //this computed also sets the value of params.buttonId (if its observable) in case the parent component needs its value
        self.buttonId = ko.computed(function () {
            var buttonParam = ko.unwrap(params.buttonId);
            if (!buttonParam) {
                var buttonId = $('.esg-button').length + "-dropdownButton";
                if (ko.isObservable(params.buttonId)) {
                    params.buttonId(buttonId);
                }                
                return buttonId;                
            }
            return buttonParam;
        });
  

        //computes the list of dropdownItem objects mapped from the original items.
        //these objects are augmented with additional functions for display purposes
        self.dropdownItems = ko.computed(function () {
            return originalItems().map(function (paramItem) {
                return new dropdownItem(paramItem);
            })
        });

        //the dropdownItem 'class'
        function dropdownItem(item) {
            var dself = this;

            //store off the originalItem
            dself.originalItem = item;

            //compute the text to be displayed based on the type of the original item and the itemsText param
            dself.displayText = ko.computed(function () {
                if (!item) {
                    return self.buttonCaption;
                }
                if (typeof item == 'string' || item instanceof String) {
                    return item;
                } else {
                    if (typeof params.itemsText == 'string' || params.itemsText instanceof String) {
                        if (ko.isObservable(item[params.itemsText]))
                        {
                            return item[params.itemsText]();
                        }
                        else
                        {
                            return item[params.itemsText];
                        }
                    } else if (params.itemsText instanceof Function) {
                        return params.itemsText(item);
                    } else {
                        return item ? item.toString() : "";
                    }
                }
            });


            //compute the text to be displayed based on the type of the original item and the itemsText param
            dself.selectedItemValue = ko.computed(function () {
                if (!item) {
                    return undefined;
                }
                if (typeof item == 'string' || item instanceof String) {
                    return item;
                } else {
                    if (typeof params.itemsValue == 'string' || params.itemsValue instanceof String) {
                        if (ko.isObservable(item[params.itemsValue])) {
                            return item[params.itemsValue]();
                        }
                        else {
                            return item[params.itemsValue];
                        }
                    } else if (params.itemsValue instanceof Function) {
                        return params.itemsValue(item);
                    } else {
                        return item ;
                    }
                }
            });

            //bound to the click event on the <li> dropdown item, which effectively 'selects' that item. 
            //sets the value param to the original item,
            //sets the selectedDropdown item to this object,
            //toggles the dropdown 
            dself.selectItem = function (data, event) {
                params.value(dself.selectedItemValue());
                selectedDropdownItem(dself);
                self.toggleDropdown(data, event);

            }

            //helper function to determine if the original item soft-equals the argument
            //may need improvement as functionality warrants
            dself.isOriginalMatch = function (itemCheck) {

                if (dself.originalItem instanceof Date)
                    return Date.Compare(dself.originalItem, itemCheck) === 0;

                return dself.originalItem == itemCheck;
            }
        }

        //contains the selected dropdownItem object
        var selectedDropdownItem = ko.observable();

        //computed that updates selectedDropdownItem if params.value is updated programmatically.
        var selectedValueUpdate = ko.computed(function () {
            if (!params.value()) {
                //if no selected value is passed in as a parameter
                if (self.buttonCaption) selectedDropdownItem(undefined); //return undefined if a caption is set
                else if (self.dropdownItems().length === 0) selectedDropdownItem(undefined); //return undefined if the length of items is 0
                else {
                    //params.value(self.dropdownItems()[0].originalItem);
                    selectedDropdownItem(self.dropdownItems()[0]); //return the first item
                }
            }
            else {
                //a selected value was passed in, so find the dropdownItem match
                var val = ko.utils.arrayFirst(self.dropdownItems(), function (ddItem) {
                    return ddItem.isOriginalMatch(params.value());
                });
                selectedDropdownItem(val || self.dropdownItems()[0]);
            }
        })

        //computes the text of the selected item to be displayed on the button
        self.selectedText = ko.computed(function () {
            if (selectedDropdownItem()) {
                return selectedDropdownItem().displayText();
            }
            else {
                return self.buttonCaption;
            }

        });

        var isOpen = ko.observable(false);
        self.openClass = ko.pureComputed(function() {
            return isOpen() ? 'esg-is-open' : '';
        });
        self.iconClass = ko.pureComputed(function() {
            return isOpen() ? 'esg-icon--up' : 'esg-icon--down';
        })
        //bound to click event on the button, toggles the dropdown
        
        self.toggleDropdown = function (data, event) {
           isOpen(!isOpen());
        }

        //namespaces the click event so it can be disposed of.
        var clickEventName = ko.pureComputed(function () {
            return "click." + ko.unwrap(self.buttonId);
        });

        //used to close the dropdown anytime the user clicks on the document
        //the button and dropdown items are bound to a click that uses the clickBubble: false binding to prevent
        //those specific click events from bubbling up to here
        $(document).on(clickEventName(), function () {
            isOpen(false);
        });

        //post processing function that's executed after the component is rendered.
        //(in actuality, its executed after the last dom element on the component, which is an empty virtual template binding, is rendered)
        //invokes the afterRender callback param
        self.postProcess = function (elements) {
            if (params.afterRender && params.afterRender instanceof Function) {
                params.afterRender(elements);
            }
        }

        //dispose of all regular computeds and turn off events
        self.dispose = function () {
            $(document).off(clickEventName());
            self.selectedText.dispose();
            selectedValueUpdate.dispose();
            self.dropdownItems().forEach(function (item) {
                item.displayText.dispose();
                item.selectedItemValue.dispose();
            });
            self.dropdownItems.dispose();
            if (ko.isComputed(self.buttonId)) self.buttonId.dispose();
            originalItems.dispose();
        }

    }


    return { viewModel: DropdownViewModel, template: markup };
}));