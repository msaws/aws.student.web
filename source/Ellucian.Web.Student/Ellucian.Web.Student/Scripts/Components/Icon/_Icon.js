﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
// 
// Message - a generic message UI element 
// Parameters:
// isVisible  (required) - boolean - is the message visible at this time?
// icon       (required) - text/enum - 'info', 'warning', 'error', etc
//                         See Ellucian style guide for full list of available icons
// color      (optional) - text/enum - 'light', 'neutral', 'info', 'info-dark', 'warning',
//                                     'warning-dark', 'error', 'error-dark', 'success', 'success-dark'
//                         Default (no value) is no visible container
// direction  (optional) - text/enum - 'right', 'down', 'left', 'up'
// size       (optional) - text/enum - 'xsmall', 'small', 'medium', 'large'
//                         Note: medium is actually larger than the default
// Usage:
//
// <icon params="isVisible: showNow, icon:'add', container: 'dark'"></message>
//
// Notes:
// The template (markup) for this component can be found in ./_Icon.html
//
// When placing icons on a page, be cognizant of the "scope" of the icon. Icons don't typically convery information 
// on their own; they should usually be placed alongside meaningful content (for accessibility).
//
define(['text!Icon/_Icon.html'], function (markup) {
    function IconViewModel(params) {
        var self = this;

        try {

            self.isVisible = null;
            if (typeof (params.isVisible) === "undefined") {
                throw "Please provide a valid isVisible value.";
            } else {
                self.isVisible = ko.isObservable(params.isVisible) ? params.isVisible : ko.observable(params.isVisible);
            }

            self.iconId = null;
            if (typeof (params.icon) === "undefined") {
                throw "Please provide a valid icon value.";
            } else {
                self.iconId = ko.computed(function () {
                    var value = "";
                    var icon = ko.unwrap(params.icon);
                    switch (icon) {
                        case "add":
                        case "arrow":
                        case "arrow-double":
                        case "avatar":
                        case "calendar":
                        case "calendar-term":
                        case "cards":
                        case "check":
                        case "clock":
                        case "clear":
                        case "close":
                        case "comment":
                        case "delete":
                        case "document":
                        case "drag":
                        case "edit":
                        case "email":
                        case "error":
                        case "export":
                        case "favorite":
                        case "filter":
                        case "folder-open":
                        case "folder-closed":
                        case "graduation":
                        case "group":
                        case "help":
                        case "hide":
                        case "home":
                        case "image":
                        case "info":
                        case "list":
                        case "location":
                        case "lookup":
                        case "menu":
                        case "more":
                        case "new-document":
                        case "notification":
                        case "print":
                        case "refresh":
                        case "save":
                        case "search":
                        case "settings":
                        case "share":
                        case "skip":
                        case "swap":
                        case "view":
                        case "warning":
                            value = "#icon-" + icon;
                            break;
                        default:
                            throw "Invalid icon value provided!"
                    }
                    return value;
                });
            }

            self.iconClasses = ko.computed(function () {
                var value = "";
                if (typeof (params.direction) !== "undefined") {
                    var direction = ko.unwrap(params.direction)
                    switch (direction) {
                        case "right":
                        case "down":
                        case "left":
                        case "up":
                            value += " esg-icon--" + direction;
                            break;
                        default:
                            throw "Invalid direction value provided!";
                    }
                }
                if (typeof (params.size) !== "undefined") {
                    var size = ko.unwrap(params.size)
                    switch (size) {
                        case "xsmall":
                        case "small":
                        case "medium":
                        case "large":
                            value += " esg-icon--" + size;
                            break;
                        default:
                            throw "Invalid size value provided!";
                    }
                }
                return value;
            });

            self.iconContainerClasses = ko.computed(function () {
                var value = "";
                if (typeof (params.color) !== "undefined") {
                    var color = ko.unwrap(params.color);
                    switch (color) {
                        case 'light':
                        case 'neutral':
                        case 'info':
                        case 'info-dark':
                        case 'warning':
                        case 'warning-dark':
                        case 'error':
                        case 'error-dark':
                        case 'success':
                        case 'success-dark':
                            value += " esg-icon__container--" + color;
                            break;
                        default:
                            throw "Invalid container color value provided!";
                    }
                    return value;
                }
            });
        } catch (error) {
            console.log(error);
        }
    }
    return { viewModel: IconViewModel, template: markup };
});