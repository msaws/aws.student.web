﻿//Copyright 2017 Ellucian Company L.P. and its affiliates
var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

Ellucian.UserProfile.viewEmergencyInformationRepository = (function () {
    return {

        // Get emergency information
        get: function (id) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.emergencyInformationSearchUrls.queryEmergencyInformationAsyncUrl + '/' + id,
                    type: "GET",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                        return;
                    }
                    reject(Ellucian.UserProfile.emergencyInformationMessages.retrievalFailure);
                    return;
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.emergencyInformationMessages.retrievalFailure);
                    return;
                });
            });
        }
    }
})();
