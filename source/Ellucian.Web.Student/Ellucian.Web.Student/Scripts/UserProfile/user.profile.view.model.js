﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

function UserProfileViewModel(repository) {
    var self = this;
    ko.validation.init({ insertMessages: false });

    this.isLoaded = ko.observable(false);
    this.isWaiting = ko.observable(false);
    this.ViewableAddresses = ko.observableArray();
    this.ViewableEmailAddresses = ko.observableArray();
    this.ViewablePhones = ko.observableArray();
    this.Configuration = ko.observable(null);
    this.UserProfileModel = ko.observable();
    this.Text = ko.observableArray();
    this.UpdatableEmailTypes = ko.observableArray();
    this.UserHasEmailUpdatePermission = ko.observable(false);
    this.UpdatablePhoneTypes = ko.observableArray();
    this.UserHasPhoneUpdatePermission = ko.observable(false);
    this.UserHasAddressUpdatePermission = ko.observable(false);
    this.addressEditDialogIsOpen = ko.observable(false);
    this.UpdatableAddressTypes = ko.observableArray().extend({ minLength: { onlyIf: self.addressEditDialogIsOpen, params: 1 } });
    this.PersonalPronounTypes = ko.observableArray();

    this.personalPronouns = ko.computed(function () {
        if (self.UserProfileModel() !== undefined) {
            if (self.UserProfileModel().PersonalPronounCode !== undefined && self.UserProfileModel().PersonalPronounCode() !== null) {
                var personalPronounType = ko.utils.arrayFirst(self.PersonalPronounTypes(), function (type) {
                    if (type.Code() === self.UserProfileModel().PersonalPronounCode()) {
                        return true;
                    }
                })
                return personalPronounType;
            }
        }
        return null;
    });

    // Email Dialog Properties
    this.submitEmailButtonLabel = ko.observable();
    this.emailEditDialogIsOpen = ko.observable(false);
    this.emailRemoveDialogIsOpen = ko.observable(false);

    // Get the email types and descriptions available for selection in the email
    // update dialog. Only those types that have not been used are available.
    this.emailTypeOptions = ko.computed(function () {
        var emailTypeOptions = [];
        var usedTypeCodes = [];

        if (self.UserProfileModel() !== undefined) {
            for (var i = 0; i < self.UserProfileModel().EmailAddresses().length; i++) {
                usedTypeCodes.push(self.UserProfileModel().EmailAddresses()[i].TypeCode());
            }

            for (var i = 0; i < self.UpdatableEmailTypes().length; i++) {
                var emailType = self.UpdatableEmailTypes()[i];
                if (ko.utils.arrayIndexOf(usedTypeCodes, emailType.Code()) === -1 ||
                    self.candidateEmail().OriginalTypeCode() == emailType.Code()) {
                    emailTypeOptions.push(emailType);
                }
            }

            if (self.candidateEmail().OriginalTypeCode() !== "") {
                emailTypeOptions.push()
            }
        }

        return emailTypeOptions;
    });

    this.canUpdatePreferred = ko.computed(function () {
        // Search for a preferred email address in those that are viewable
        var preferredViewableEmail = ko.utils.arrayFirst(self.ViewableEmailAddresses(), function (viewableEmail) {
            if (viewableEmail.EmailAddress.IsPreferred()) {
                return true;
            }
        });

        if (preferredViewableEmail !== null) {
            // The current preferred email address was found, use its updatable state
            return preferredViewableEmail.IsUpdatable();
        }

        if (typeof self.UserProfileModel() !== "undefined") {
            // Search for a preferred email address in all emails, including those nonviewable
            var preferredNonviewableEmail = ko.utils.arrayFirst(self.UserProfileModel().EmailAddresses(), function (emailAddress) {
                if (emailAddress.IsPreferred()) {
                    return true;
                }
            });
            // The preferred email address was not found in viewable, so if we find
            // one here, assume we cannot update it
            if (preferredNonviewableEmail !== null) {
                return false;
            }

        }
        // Didn't find any preferred flags -- assume we can set it
        return true;
    });

    this.isNewCandidateEmail = ko.observable(false);
    this.candidateEmail = ko.validatedObservable({
        OriginalIsPreferred: ko.observable(false),
        OriginalTypeCode: ko.observable(""),
        OriginalValue: ko.observable(""),
        IsPreferred: ko.observable(false).extend({
            validation: {
                validator: function (value) {
                    if (!self.emailEditDialogIsOpen()) return true;
                    if (value === true) {
                        return self.canUpdatePreferred();
                    } else {
                        return true;
                    }
                },
                message: Ellucian.UserProfile.userProfileMessages.updatePreferredEmailPermissionFailure,
            }
        }),
        TypeCode: ko.observable("").extend({
            validation: [{
                validator: function (value) {
                    if (!self.emailEditDialogIsOpen()) return true;
                    if (self.emailTypeOptions().length == 0) {
                        return true;
                    }
                    if (value && self.emailTypeOptions().length > 0) {
                        var availableTypeCodes = ko.utils.arrayMap(self.emailTypeOptions(), function (emailType) { return emailType.Code(); });
                        if (ko.utils.arrayIndexOf(availableTypeCodes, value) >= 0) {
                            return true;
                        }
                    }
                    return false;
                },
                message: Ellucian.UserProfile.userProfileMessages.duplicateEmailTypeErrorMessage,
            }, {
                validator: function (value) {
                    if (!self.emailEditDialogIsOpen()) return true;
                    if (self.emailTypeOptions().length == 0 && self.isNewCandidateEmail()) {
                        return false;
                    } else {
                        return true;
                    }
                },
                message: Ellucian.UserProfile.userProfileMessages.noEmailTypesLeftErrorMessage,
            }]
        }),
        Value: ko.observable("").extend({
            throttle: 500,
            required: {
                onlyIf: self.emailEditDialogIsOpen,
            },
            maxLength: {
                params: 50,
                onlyIf: self.emailEditDialogIsOpen,
            },
            email: {
                params: true,
                onlyIf: self.emailEditDialogIsOpen,
            }
        }),
    });

    this.candidateEmailIsChanged = ko.computed(function () {
        return self.candidateEmail().OriginalIsPreferred() !== self.candidateEmail().IsPreferred() ||
            self.candidateEmail().OriginalTypeCode() !== self.candidateEmail().TypeCode() ||
            self.candidateEmail().OriginalValue() !== self.candidateEmail().Value();
    }).extend({
        equal: {
            params: true,
            onlyIf: self.emailEditDialogIsOpen,
        },
    });

    this.emailToRemove = ko.observable(null);

    // Edit Email Methods
    this.showEmailNewDialog = function () {
        self.candidateEmail().OriginalIsPreferred(false);
        self.candidateEmail().OriginalTypeCode("");
        self.candidateEmail().OriginalValue("");
        self.candidateEmail().IsPreferred(false);
        self.candidateEmail().TypeCode("");
        self.candidateEmail().Value("");
        self.candidateEmail().Value.isModified(false);
        self.isNewCandidateEmail(true);
        self.submitEmailButtonLabel(Ellucian.UserProfile.userProfileMessages.emailAddLabel);
        self.emailEditDialogIsOpen(true);
    };

    this.showEmailEditDialog = function (viewableEmailAddress) {
        self.candidateEmail().OriginalIsPreferred(viewableEmailAddress.EmailAddress.IsPreferred());
        self.candidateEmail().OriginalTypeCode(viewableEmailAddress.EmailAddress.TypeCode());
        self.candidateEmail().OriginalValue(viewableEmailAddress.EmailAddress.Value());
        self.candidateEmail().IsPreferred(viewableEmailAddress.EmailAddress.IsPreferred());
        self.candidateEmail().TypeCode(viewableEmailAddress.EmailAddress.TypeCode());
        self.candidateEmail().Value(viewableEmailAddress.EmailAddress.Value());
        self.candidateEmail().Value.isModified(false);
        self.isNewCandidateEmail(false);
        self.submitEmailButtonLabel(Ellucian.UserProfile.userProfileMessages.emailUpdateLabel);
        self.emailEditDialogIsOpen(true);
    };

    this.submitEmailAddress = function () {
        if (self.isValid()) {
            if (self.isNewCandidateEmail()) {
                // This is a new email, copy and push it into the array 
                var newEmail = {
                    IsPreferred: ko.observable(self.candidateEmail().IsPreferred()),
                    TypeCode: ko.observable(self.candidateEmail().TypeCode()),
                    Value: ko.observable(self.candidateEmail().Value()),
                };
                if (self.candidateEmail().IsPreferred()) {
                    // User set a new preferred email, unset any current preferred
                    ko.utils.arrayForEach(self.UserProfileModel().EmailAddresses(), function (email) {
                        email.IsPreferred(false);
                    });
                }
                self.UserProfileModel().EmailAddresses.push(newEmail);
            } else {
                // This is not a new email, find the match and update the array
                ko.utils.arrayForEach(self.UserProfileModel().EmailAddresses(), function (email) {
                    if (self.candidateEmail().OriginalIsPreferred() === email.IsPreferred() &&
                        self.candidateEmail().OriginalTypeCode() === email.TypeCode() &&
                        self.candidateEmail().OriginalValue() === email.Value()) {
                        // Found a match, perform update
                        email.IsPreferred(self.candidateEmail().IsPreferred());
                        email.TypeCode(self.candidateEmail().TypeCode());
                        email.Value(self.candidateEmail().Value());
                    } else if (self.candidateEmail().IsPreferred()) {
                        // Current email is not a match, but the updated email is a preferred email,
                        // so unset any current preferred
                        email.IsPreferred(false);
                    }
                });
            }
            self.updateUserProfile();
            self.emailEditDialogIsOpen(false);
        }
    }

    this.submitEmailButton = { title: self.submitEmailButtonLabel, isPrimary: true, callback: self.submitEmailAddress, enabled: self.candidateEmail.isValid };

    this.emailEditDialogIsOpen.subscribe(function (newState) {
        if (newState === false) {
            self.candidateEmail().IsPreferred(false);
            self.candidateEmail().TypeCode("");
            self.candidateEmail().Value("");
            self.isNewCandidateEmail(false);
            self.candidateEmail().OriginalIsPreferred(false);
            self.candidateEmail().OriginalTypeCode("");
            self.candidateEmail().OriginalValue("");
        }
    });

    // Remove Email Methods
    this.emailRemoveDialogIsOpen.subscribe(function (newState) {
        if (newState === false) {
            self.emailToRemove(null);
        }
    });

    this.removeEmailAddress = function () {
        if (self.emailToRemove() !== null && self.emailToRemove().IsUpdatable()) {
            ko.utils.arrayRemoveItem(self.UserProfileModel().EmailAddresses(),
                ko.utils.arrayFirst(self.UserProfileModel().EmailAddresses(), function (email) {
                    return email.Value() == self.emailToRemove().EmailAddress.Value() &&
                        email.TypeCode() == self.emailToRemove().EmailAddress.TypeCode();
                }));
        }
        self.updateUserProfile();
        self.emailRemoveDialogIsOpen(false);
    };

    this.showEmailRemoveDialog = function (viewableEmailAddress) {
        self.emailToRemove(viewableEmailAddress);
        self.emailRemoveDialogIsOpen(true);
    };

    // Phone members
    this.phoneEditDialogIsOpen = ko.observable(false);
    this.phoneRemoveDialogIsOpen = ko.observable(false);
    this.submitPhoneButtonLabel = ko.observable("");

    this.phoneTypeOptions = ko.computed(function () {
        if (self.UserProfileModel() !== undefined) {
            return self.UpdatablePhoneTypes();
        }
        return [];
    });

    this.candidatePhone = ko.validatedObservable({
        OriginalNumber: ko.observable(""),
        OriginalTypeCode: ko.observable(""),
        OriginalExtension: ko.observable(""),
        Number: ko.observable("").extend({
            required: {
                onlyIf: self.phoneEditDialogIsOpen,
            },
            maxLength: {
                params: 20,
                onlyIf: self.phoneEditDialogIsOpen,
            },
            formattedPhone: {
                validateLength: true
            }
        }),
        Extension: ko.observable("").extend({
            maxLength: {
                params: 10,
                onlyIf: self.phoneEditDialogIsOpen,
            },
        }),
        TypeCode: ko.observable("").extend({
            validation: {
                validator: function (value) {
                    if (!self.phoneEditDialogIsOpen()) return true;
                    var phoneTypeUpdatableMatch = ko.utils.arrayFirst(self.phoneTypeOptions(), function (phoneTypeOption) {
                        return value == phoneTypeOption.Code();
                    })
                    return phoneTypeUpdatableMatch !== null;
                },
                message: Ellucian.UserProfile.userProfileMessages.phoneTypeValidationError,
            }
        }),
        IsDuplicate: ko.computed(function () {
            if (typeof self.UserProfileModel() !== "undefined" && self.isNewCandidatePhone() === true) {
                var phoneDuplicate = ko.utils.arrayFirst(self.UserProfileModel().Phones(), function (phone) {
                    if (phone.Number() === self.candidatePhone().Number() &&
                        phone.Extension() === self.candidatePhone().Extension() &&
                        phone.TypeCode() === self.candidatePhone().TypeCode()) {
                        return true;
                    } else {
                        return false;
                    }
                });
                return phoneDuplicate !== null;
            } else {
                return false;
            }
        }).extend({
            throttle: 500,
            notEqual: {
                value: true,
                message: Ellucian.UserProfile.userProfileMessages.duplicatePhoneNumberError,
            }
        }),
    });

    this.isNewCandidatePhone = ko.observable(false);

    this.phoneToRemove = ko.observable(null);

    // Edit Phone Methods
    this.showPhoneNewDialog = function () {
        self.candidatePhone().OriginalNumber("");
        self.candidatePhone().OriginalExtension("");
        self.candidatePhone().OriginalTypeCode("");
        self.candidatePhone().Number("");
        self.candidatePhone().Extension("");
        self.candidatePhone().TypeCode("");
        self.candidatePhone().Number.isModified(false);
        self.isNewCandidatePhone(true);
        self.submitPhoneButtonLabel(Ellucian.UserProfile.userProfileMessages.phoneAddLabel);
        self.phoneEditDialogIsOpen(true);
    }

    this.showPhoneEditDialog = function (viewablePhone) {
        self.candidatePhone().OriginalNumber(viewablePhone.Phone.Number());
        self.candidatePhone().OriginalExtension(viewablePhone.Phone.Extension());
        self.candidatePhone().OriginalTypeCode(viewablePhone.Phone.TypeCode());
        self.candidatePhone().Number(viewablePhone.Phone.Number());
        self.candidatePhone().Extension(viewablePhone.Phone.Extension());
        self.candidatePhone().TypeCode(viewablePhone.Phone.TypeCode());
        self.candidatePhone().Number.isModified(false);
        self.isNewCandidatePhone(false);
        self.submitPhoneButtonLabel(Ellucian.UserProfile.userProfileMessages.phoneUpdateLabel);
        self.phoneEditDialogIsOpen(true);
    }

    this.submitPhone = function () {
        if (self.isValid()) {
            if (self.isNewCandidatePhone()) {
                // This is a new phone, copy and push it into the array
                var newPhone = {
                    Number: ko.observable(self.candidatePhone().Number()),
                    Extension: ko.observable(self.candidatePhone().Extension()),
                    TypeCode: ko.observable(self.candidatePhone().TypeCode()),
                }
                self.UserProfileModel().Phones.push(newPhone);
            } else {
                // This is not a new phone, find the match and update the array
                ko.utils.arrayForEach(self.UserProfileModel().Phones(), function (phone) {
                    if (self.candidatePhone().OriginalNumber() === phone.Number() &&
                        self.candidatePhone().OriginalTypeCode() === phone.TypeCode() &&
                        self.candidatePhone().OriginalExtension() === phone.Extension()) {
                        // Found a match, perform update
                        phone.Number(self.candidatePhone().Number());
                        phone.TypeCode(self.candidatePhone().TypeCode());
                        phone.Extension(self.candidatePhone().Extension());
                    }
                });
            }
            self.updateUserProfile();
            self.closePhoneEditDialog();
        }
    };
    this.submitPhoneButton = { title: self.submitPhoneButtonLabel, callback: self.submitPhone, isPrimary: true, enabled: self.candidatePhone.isValid };

    this.phoneEditDialogIsOpen.subscribe(function (newState) {
        if (newState === false) {
            self.candidatePhone().Number("");
            self.candidatePhone().Extension("");
            self.candidatePhone().TypeCode("");
            self.isNewCandidatePhone(false);
            self.candidatePhone().OriginalNumber("");
            self.candidatePhone().OriginalExtension("");
            self.candidatePhone().OriginalTypeCode("");
        }
    });

    this.closePhoneEditDialog = function () {
        this.phoneEditDialogIsOpen(false);
    };

    // Remove Phone Methods
    this.phoneRemoveDialogIsOpen.subscribe(function (newState) {
        if (newState === false) {
            self.phoneToRemove(null);
        }
    });

    this.removePhone = function () {
        if (self.phoneToRemove() !== null && self.phoneToRemove().IsUpdatable()) {
            ko.utils.arrayRemoveItem(self.UserProfileModel().Phones(),
                ko.utils.arrayFirst(self.UserProfileModel().Phones(), function (phone) {
                    return phone.Number() == self.phoneToRemove().Phone.Number() &&
                        phone.TypeCode() == self.phoneToRemove().Phone.TypeCode() &&
                        phone.Extension() == self.phoneToRemove().Phone.Extension();
                }));
        }
        self.updateUserProfile();
        self.closePhoneRemoveDialog();
    };
    this.removePhoneButton = { title: Ellucian.SharedComponents.ButtonLabels.buttonTextAccept, isPrimary: true, callback: self.removePhone };

    this.showPhoneRemoveDialog = function (viewablePhone) {
        self.phoneToRemove(viewablePhone);
        self.phoneRemoveDialogIsOpen(true);
    };

    this.closePhoneRemoveDialog = function () {
        self.phoneRemoveDialogIsOpen(false);
    };

    // Addresses
    this.candidateAddress = ko.validatedObservable(new AddressModel());
    this.candidateAddress().validate(false);
    this.candidateAddress().OriginalTypeCode = ko.observable("");
    this.candidateAddress().addressId = ko.observable("");
    this.submitAddressButtonLabel = ko.observable("");
    this.isNewCandidateAddress = ko.observable(false);

    this.addressTypeOptions = ko.computed(function() {
        var _addressTypeOptions = [];
        var usedTypeCodes = [];

        if (self.UserProfileModel() !== undefined) {
            for (var i = 0; i < self.UserProfileModel().Addresses().length; i++) {
                usedTypeCodes.push(self.UserProfileModel().Addresses()[i].TypeCode());
            }

            for (var j = 0; j < self.UpdatableAddressTypes().length; j++) {
                var addressType = self.UpdatableAddressTypes()[j];
                if (ko.utils.arrayIndexOf(usedTypeCodes, addressType.Code()) === -1 ||
                    self.candidateAddress().OriginalTypeCode() == addressType.Code()) {
                    _addressTypeOptions.push(addressType);
                }
            }
        }

        return _addressTypeOptions;
    });

    this.addressEditDialogIsOpen.subscribe(function (newState) {
        if (newState === true) {
            self.candidateAddress().validate(false);
        } else {
            self.candidateAddress().Street1("");
            self.candidateAddress().Street2("");
            self.candidateAddress().Street3("");
            self.candidateAddress().Street4("");
            self.candidateAddress().City("");
            self.candidateAddress().State(null);
            self.candidateAddress().Country(null);
            self.candidateAddress().PostalCode("");
            self.candidateAddress().internationalSelected(false);
            self.candidateAddress().TypeCode("");
            self.candidateAddress().OriginalTypeCode("");
            self.candidateAddress().validate(false);
        }
    });

    this.submitAddress = function () {
        self.candidateAddress().validate(true);
        if (self.isValid()) {
            if (!self.candidateAddress().internationalSelected() && self.candidateAddress().State() != null) {
                var c = ko.utils.arrayFirst(self.candidateAddress().Countries(), function (item) {
                    return item.Code() === self.candidateAddress().State().CountryCode();
                });
                self.candidateAddress().Country(c);
                self.candidateAddress().Street3(null);
                self.candidateAddress().Street4(null);
            }
            else {
                self.candidateAddress().City(null);
                self.candidateAddress().State(null);
                self.candidateAddress().PostalCode(null);
            }
            var lines = [];
            if (self.candidateAddress().Street1() != null && self.candidateAddress().Street1().length > 0) lines.push(self.candidateAddress().Street1());
            if (self.candidateAddress().Street2() != null && self.candidateAddress().Street2().length > 0) lines.push(self.candidateAddress().Street2());
            if (self.candidateAddress().Street3() != null && self.candidateAddress().Street3().length > 0) lines.push(self.candidateAddress().Street3());
            if (self.candidateAddress().Street4() != null && self.candidateAddress().Street4().length > 0) lines.push(self.candidateAddress().Street4());

            if (self.isNewCandidateAddress()) {

                // This is a new address, copy and push it into the array 

                var newAddress = {
                    PersonId: self.UserProfileModel().Id(),
                    AddressLines: ko.observable(lines),
                    City: ko.observable(self.candidateAddress().City()),
                    State: ko.observable(self.candidateAddress().State() != null ? self.candidateAddress().State().Code() : null),
                    PostalCode: ko.observable(self.candidateAddress().PostalCode()),
                    Country: ko.observable(self.candidateAddress().Country() != null ? self.candidateAddress().Country().Description() : null),
                    CountryCode: ko.observable(self.candidateAddress().Country() != null ? self.candidateAddress().Country().Code() : null),
                    TypeCode: ko.observable(self.candidateAddress().TypeCode())
                };
                self.UserProfileModel().Addresses.push(newAddress);
            } else {
                // This is an existing address - need to edit it.

                ko.utils.arrayForEach(self.UserProfileModel().Addresses(), function (addr) {
                    if (addr.AddressId() === self.candidateAddress().addressId()) {
                        addr.AddressLines(lines);
                        addr.City(self.candidateAddress().City());
                        if (self.candidateAddress().State() != null) {
                            addr.State(self.candidateAddress().State().Code());
                        }
                        else {
                            addr.State(null);
                        }
                        addr.PostalCode(self.candidateAddress().PostalCode());
                        if (self.candidateAddress().Country() != null) {
                            addr.CountryCode(self.candidateAddress().Country().Code());
                            addr.Country(self.candidateAddress().Country().Description());
                        }
                        else {
                            addr.CountryCode(null);
                            addr.Country(null);
                        }
                        addr.TypeCode(self.candidateAddress().TypeCode());
                        addr.PersonId(self.UserProfileModel().Id());
                    }
                });

                // Need more info about moving the Address Entry component into an actual Colleague Address object.
            }
            self.updateUserProfile();
            self.closeAddressEditDialog();
        }
    }
    this.submitAddressButton = { title: self.submitAddressButtonLabel, isPrimary: true, callback: self.submitAddress, enabled: self.candidateAddress.isValid };

    this.addressToRemove = ko.observable(null);
    this.addressRemoveDialogIsOpen = ko.observable(false);

    this.showAddressNewDialog = function () {
        self.addressEditDialogIsOpen(true);

        self.submitAddressButtonLabel(Ellucian.UserProfile.userProfileMessages.addressAddLabel);
        self.isNewCandidateAddress(true);
        self.candidateAddress().States(self.States());
        self.candidateAddress().Countries(self.Countries());
        self.candidateAddress().Street1("");
        self.candidateAddress().Street2("");
        self.candidateAddress().Street3("");
        self.candidateAddress().Street4("");
        self.candidateAddress().City("");
        self.candidateAddress().State(null);
        self.candidateAddress().Country(null);
        self.candidateAddress().PostalCode("");
        self.candidateAddress().TypeCode("");
        self.candidateAddress().internationalSelected(false);
        self.candidateAddress().OriginalTypeCode("");

    };

    this.showAddressEditDialog = function (viewableAddress) {
        // logic here
        //self.submitAddressButtonLabel(Ellucian.UserProfile.userProfileMessages.addressUpdateLabel);
        self.addressEditDialogIsOpen(true);




        self.submitAddressButtonLabel(Ellucian.UserProfile.userProfileMessages.addressUpdateLabel);
        self.isNewCandidateAddress(false);
        self.candidateAddress().States(self.States());
        self.candidateAddress().Countries(self.Countries());
        if (viewableAddress.Address.AddressLines()[0] != null) self.candidateAddress().Street1(viewableAddress.Address.AddressLines()[0]);
        if (viewableAddress.Address.AddressLines()[1] != null) self.candidateAddress().Street2(viewableAddress.Address.AddressLines()[1]);
        if (viewableAddress.Address.AddressLines()[2] != null) self.candidateAddress().Street3(viewableAddress.Address.AddressLines()[2]);
        if (viewableAddress.Address.AddressLines()[3] != null) self.candidateAddress().Street4(viewableAddress.Address.AddressLines()[3]);
        self.candidateAddress().City(viewableAddress.Address.City());
        var stateMatch = ko.utils.arrayFirst(self.States(), function (st) {
            return st.Code() === viewableAddress.Address.State();
        });
        self.candidateAddress().State(stateMatch);
        var countryMatch = ko.utils.arrayFirst(self.Countries(), function (c) {
            return c.Code() === viewableAddress.Address.CountryCode();
        });
        self.candidateAddress().Country(countryMatch);
        self.candidateAddress().PostalCode(viewableAddress.Address.PostalCode());
        self.candidateAddress().internationalSelected(viewableAddress.Address.State() == null || viewableAddress.Address.State() === "");
        self.candidateAddress().OriginalTypeCode(viewableAddress.Address.TypeCode());
        self.candidateAddress().TypeCode(viewableAddress.Address.TypeCode());
        self.candidateAddress().addressId(viewableAddress.Address.AddressId());
    };

    this.wbAddresstype = ko.computed(function () {
        return self.UpdatableAddressTypes()[0];
    });

    this.removeAddress = function () {
        if (self.addressToRemove() !== null && self.addressToRemove().IsUpdatable()) {
            ko.utils.arrayRemoveItem(self.UserProfileModel().Addresses(),
                ko.utils.arrayFirst(self.UserProfileModel().Addresses(), function (address) {
                    return address.TypeCode() == self.addressToRemove().Address.TypeCode();
                }));
        }
        //logic here
        self.updateUserProfile();
        self.closeAddressRemoveDialog();
    };

    this.closeAddressEditDialog = function () {
        self.addressEditDialogIsOpen(false);
    };

    this.removeAddressButton = { title: Ellucian.SharedComponents.ButtonLabels.buttonTextAccept, isPrimary: true, callback: self.removeAddress };

    this.showAddressRemoveDialog = function (viewableAddress) {
        self.addressToRemove(viewableAddress);
        self.addressRemoveDialogIsOpen(true);
    };

    this.closeAddressRemoveDialog = function () {
        self.addressRemoveDialogIsOpen(false);
    };

    this.get = function () {
        repository.get()
            .then(function (data) {
                ko.mapping.fromJS(data, {}, self);
                self.isLoaded(true);
                $("table").makeTableResponsive();
                return;
            })
        .catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
            self.isLoaded(true);
            $("table").makeTableResponsive();
            return;
        });
    };

    // Update User Profile
    this.updateUserProfile = function () {

        self.isWaiting(true);

        repository.update(self.UserProfileModel())
        .then(function (data) {
            ko.mapping.fromJS(data, {}, self);
            repositoryCompleteHandler();
            return;
        })
        .catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
            repositoryCompleteHandler();
            return;
        });
    };

    // Verification methods
    this.verifyAddressInformation = function () {
        verifyUserProfileInformation(Ellucian.UserProfile.userProfileUrls.verifyAddress);
    };

    this.verifyEmailAddressInformation = function () {
        verifyUserProfileInformation(Ellucian.UserProfile.userProfileUrls.verifyEmailAddress);
    };

    this.verifyPhoneInformation = function () {
        verifyUserProfileInformation(Ellucian.UserProfile.userProfileUrls.verifyPhone);
    };

    var verifyUserProfileInformation = function (verificationUrl) {

        self.isWaiting(true);

        repository.verify(self.UserProfileModel(), verificationUrl)
            .then(function (data) {
                ko.mapping.fromJS(data, {}, self)
                self.showNotification();
                repositoryCompleteHandler();
                return;
            })
            .catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.UserProfile.userProfileMessages.updateFailure, type: "error" });
                repositoryCompleteHandler();
                return;
            });
    };

    var repositoryCompleteHandler = function () {
        setTimeout(function () {
            self.isWaiting(false)
        }, 200);
        $('table').makeTableResponsive();
    };

    this.showNotification = function () {
        $('#notificationHost').notificationCenter('reset');
        if (typeof self.Notification() != "undefined" && self.Notification() != null) {
            if (self.Notification().Type() == "Error") {
                $('#notificationHost').notificationCenter('addNotification', { message: self.Notification().Message(), type: "error" });
            } else {
                $('#notificationHost').notificationCenter('addNotification', { message: self.Notification().Message(), type: "success" });
            }
        }
    };

    this.removeEmailButton = { title: Ellucian.SharedComponents.ButtonLabels.buttonTextAccept, isPrimary: true, callback: self.removeEmailAddress };
};
