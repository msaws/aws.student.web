﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
/*
* Provides functionality related to the UserProfileController to support
* data retreival and posting
*
*/

var emergencyInformation = new Ellucian.UserProfile.EmergencyInformationViewModel(Ellucian.UserProfile.emergencyInformationRepository);
var throbber = null;

$(document).ready(function () {

    ko.validation.configure({
        messagesOnModified: true,
        insertMessages: true
    });

    ko.applyBindings(emergencyInformation, document.getElementById("emergency-information"));


    Ellucian.UserProfile.emergencyInformationRepository.get()
        .then(function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, {}, emergencyInformation);
            }
            emergencyInformation.retrieved(true);
        })
        .catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
        });
});