﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
/*
* Load data for the user preferences page
*/

var preferencesInstance = new Ellucian.UserProfile.PreferencesViewModel(Ellucian.UserProfile.preferencesRepository);

$(document).ready(function () {
    // Bind the view model
    ko.applyBindings(preferencesInstance, document.getElementById("account-preferences"));

    preferencesInstance.get();
});
