﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

Ellucian.UserProfile.emergencyInformationRepository = (function () {

    var updateSucceeded = function (data) {
        if (!account.handleInvalidSessionResponse(data)) {
            return Promise.resolve(data);
        }
        return Promise.reject(Ellucian.UserProfile.emergencyInformationMessages.updateFailure);
    };

    return {

        // Get emergency information
        get: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.emergencyInformationUrls.get,
                    type: "GET",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                        return;
                    }
                    reject(Ellucian.UserProfile.emergencyInformationMessages.retrievalFailure);
                    return;
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.emergencyInformationMessages.retrievalFailure);
                    return;
                });
            });
        },

        update: function (data) {
            //convert the emergency information to json string
            var json = {
                emergencyInformationJson: ko.toJSON(data)
            };
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.emergencyInformationUrls.update,
                    data: JSON.stringify(json),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                    return;
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.emergencyInformationMessages.updateFailure);
                    return;
                });
            });
        },

        verify: function (data) {
            //convert the emergency information to json string
            var json = { emergencyInformationJson: ko.toJSON(data) };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.emergencyInformationUrls.verify,
                    data: JSON.stringify(json),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                    return;
                })
                .fail(function (data) {
                    try {
                        var errorMessage = JSON.parse(data.responseText);
                        reject(errorMessage);
                    } catch (e) {
                        reject(Ellucian.UserProfile.emergencyInformationMessages.confirmFailure);
                    }
                    return;
                });
            });
        }
    }
})();