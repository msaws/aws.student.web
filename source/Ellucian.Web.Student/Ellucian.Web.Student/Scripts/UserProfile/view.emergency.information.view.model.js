﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

Ellucian.UserProfile.viewEmergencyInformationViewModel = function viewEmergencyInformationViewModel() {
    var self = this;

    this.retrieved = ko.observable(false);

    this.IsLoading = ko.observable(false);

    this.LoadingMessage = ko.observable("");

    this.dateFormat = ko.computed(function () {
        var format = Globalize.findClosestCulture().calendar.patterns.d;
        return format;
    });

    this.HasPrivacyRestriction = ko.observable(true);
    this.PrivacyMessage = ko.observable("");
    this.EmergencyContacts = ko.observableArray();
    this.HospitalPreference = ko.observable("");
    this.InsuranceInformation = ko.observable("");
    this.AdditionalInformation = ko.observable("");
    this.HealthConditions = ko.observableArray();
    this.ConfirmedDate = ko.observable("");
    this.OptOut = ko.observable(false);
    this.Notification = ko.observable(null);
    this.PersonId = ko.observable("");
    this.PersonName = ko.observable("");
    this.CurrentDateString = ko.observable("");
    this.EmergencyInformationConfiguration = ko.observable({
        AllowOptOut: ko.observable(false),
        HideHealthConditions: ko.observable(false),
        HideOtherInformation: ko.observable(false),
        RequireContact: ko.observable(false)
    });

    this.showHealthConditions = ko.pureComputed(function () {
        return !self.EmergencyInformationConfiguration().HideHealthConditions() && self.HealthConditions().length > 0;
    });

    this.showOtherInformation = ko.pureComputed(function () {
        return !self.EmergencyInformationConfiguration().HideOtherInformation();
    });

    var showNotification = function () {
        $('#notificationHost').notificationCenter('reset');
        if (typeof self.Notification() !== "undefined" && self.Notification() !== null) {
            if (self.Notification().Type() === "Error") {
                $('#notificationHost').notificationCenter('addNotification', { message: self.Notification().Message(), type: "error" });
            } else {
                $('#notificationHost').notificationCenter('addNotification', { message: self.Notification().Message(), type: "success" });
            }
        }
    };

}