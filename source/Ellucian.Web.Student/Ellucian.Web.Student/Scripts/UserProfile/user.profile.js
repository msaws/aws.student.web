﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
/*
* Provides functionality related to the UserProfileController to support
* data retreival and posting
*
*/

var userProfileInstance = new UserProfileViewModel(Ellucian.UserProfile.userProfileRepository);

$(document).ready(function () {
    userProfileInstance.errors = ko.validation.group(userProfileInstance, { deep: true });
    ko.components.register('address-entry', {
        require: 'AddressEntry/_AddressEntry'
    });
    ko.applyBindings(userProfileInstance, document.getElementById("user-profile"));
    
    $("#candidate-email-form").submit(function (e) {
        e.preventDefault();
    });

    userProfileInstance.get();
});
