﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

Ellucian.UserProfile.userProfileRepository = (function () {

    var updateSucceeded = function (data) {
        if (!account.handleInvalidSessionResponse(data)) {
            return Promise.resolve(data);
        }
        return Promise.reject(Ellucian.UserProfile.userProfileMessages.updateFailure);
    };

    return {

        // Get  user profile
        get: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.userProfileUrls.get,
                    type: "GET",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                    }
                    reject(Ellucian.UserProfile.userProfileMessages.retrievalFailure);
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.userProfileMessages.retrievalFailure);
                });
            });
        },

        update: function (data) {
            //convert the emergency information to json string
            var json = {
                userProfileJson: ko.toJSON(data)
            };
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.userProfileUrls.update,
                    data: JSON.stringify(json),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                    return;
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.userProfileMessages.updateFailure);
                    return;
                });
            });
        },

        verify: function (data, url) {
            //convert the emergency information to json string
            var json = { userProfileJson: ko.toJSON(data) };

            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: url,
                    data: JSON.stringify(json),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                    return;
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.userProfileMessages.updateFailure);
                    return;
                });
            });
        }
    }
})();