﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var viewEmergencyInformation = new Ellucian.UserProfile.viewEmergencyInformationViewModel(Ellucian.UserProfile.viewEmergencyInformationRepository);

$(document).ready(function () {

    ko.applyBindings(viewEmergencyInformation, document.getElementById("view-emergency-information"));


    Ellucian.UserProfile.viewEmergencyInformationRepository.get(Ellucian.UserProfile.viewEmergencyInformationId)
        .then(function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, {}, viewEmergencyInformation);
            }
            viewEmergencyInformation.retrieved(true);
        })
        .catch(function (error) {
            $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
        });
});
