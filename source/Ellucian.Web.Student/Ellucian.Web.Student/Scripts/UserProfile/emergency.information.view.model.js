﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
/*
* Provides functionality related to the UserProfileController to support
* data retreival and posting
*
*/

var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

Ellucian.UserProfile.EmergencyInformationViewModel = function EmergencyInformationViewModel(repository) {
    var self = this;
    ko.validation.init({ insertMessages: false });

    this.retrieved = ko.observable(false);

    this.IsLoading = ko.observable(false);

    this.LoadingMessage = ko.observable("");

    this.dateFormat = ko.computed(function () {
        var format = Globalize.findClosestCulture().calendar.patterns.d;
        return format;
    });

    this.EmergencyContacts = ko.observableArray();
    this.HospitalPreference = ko.observable("").extend({
        maxLength: 50
    });
    this.InsuranceInformation = ko.observable("");
    this.AdditionalInformation = ko.observable("");
    this.HealthConditions = ko.observableArray();
    this.ConfirmedDate = ko.observable("");
    this.OptOut = ko.observable(false);
    this.Notification = ko.observable(null);
    this.PersonId;
    this.CurrentDateString = ko.observable("");
    this.EmergencyInformationConfiguration = ko.observable({
        AllowOptOut: ko.observable(false),
        HideHealthConditions: ko.observable(false),
        HideOtherInformation: ko.observable(false),
        RequireContact: ko.observable(false)
    });

    this.selectedContactIndex;

    this.isNewContact = false;

    this.missingContactType = ko.observable(false);

    this.validationMessages = ko.observableArray([]);

    this.editDialogIsOpen = ko.observable(false);
    this.editDialogButtonLabel = ko.observable("");

    this.removeDialogIsOpen = ko.observable(false);
    this.disableRemoveContact = ko.computed(function() {
        // Return true if the user should not be allowed to remove the last contact
        return self.EmergencyContacts().length <= 1 &&
            self.EmergencyInformationConfiguration().RequireContact();
    });
    this.contactToRemove = ko.observable(null);
    this.submitButtonLabel = ko.observable("");

    this.optoutDialogIsOpen = ko.observable(false);

    this.showHealthConditions = ko.pureComputed(function () {
        return !self.EmergencyInformationConfiguration().HideHealthConditions() && self.HealthConditions().length > 0;
    });

    this.showOtherInformation = ko.pureComputed(function () {
        return !self.EmergencyInformationConfiguration().HideOtherInformation();
    });

    this.updateOptOutButtonText = ko.computed(function () {
        return Ellucian.SharedComponents.ButtonLabels.buttonTextAccept;
    });

    this.optoutConfirm = function () {
        self.EmergencyContacts.removeAll();
        self.OptOut(true);
        self.updateEmergencyInformation();
        self.optoutDialogIsOpen(false);
    }

    this.updateOptOut = function () {
        // Opting out will remove any entered contacts.
        // Open a confirmation dialog if this would result in data loss.
        if (self.OptOut() === true && self.EmergencyContacts().length > 0) {
            // Reset to false pending confirmation from dialog
            self.OptOut(false);
            self.optoutDialogIsOpen(true);
        } else {
            self.updateEmergencyInformation();
        }
        return true;
    }

    this.updateOptOutButton = {
        title: self.updateOptOutButtonText, isPrimary: true, callback: self.optoutConfirm
    };

    // contactHasPhone takes an object representing an emergency contact
    // and returns true if at least one phone observable is not empty
    var contactHasPhone = function (contact) {
        return (contact.DaytimePhone() !== null && contact.DaytimePhone() !== "")
            || (contact.EveningPhone() !== null && contact.EveningPhone() !== "")
            || (contact.OtherPhone() !== null && contact.OtherPhone() !== "");
    }

    // Container for the contact being edited/added. 
    var editContactViewModel = {
        Name: ko.observable("").extend({
            maxLength: 57,
            required: {
                onlyIf: function () {
                    return self.editDialogIsOpen();
                }
            }
        }),
        Relationship: ko.observable("").extend({
            maxLength: 25
        }),
        DaytimePhone: ko.observable("").extend({
            maxLength: 20,
            formattedPhone: { validateLength: true }
        }),
        EveningPhone: ko.observable("").extend({
            maxLength: 20,
            formattedPhone: { validateLength: true }
        }),
        OtherPhone: ko.observable("").extend({
            maxLength: 20,
            formattedPhone: { validateLength: true }
        }),
        IsEmergencyContact: ko.observable(true).extend({
            equal: {
                value: true,
                onlyIf: function () {
                    if (self.editDialogIsOpen()) {
                        if (typeof self.editContact === 'undefined') return false;
                        var x = self.editContact().IsMissingPersonContact() == false;
                        return x;
                    } else {
                        return false;
                    }
                },
                message: Ellucian.UserProfile.emergencyInformationMessages.contactTypeValidation
            }
        }),
        IsMissingPersonContact: ko.observable(false).extend({
            equal: {
                value: true,
                onlyIf: function () {
                    if (self.editDialogIsOpen()) {
                        if (typeof self.editContact === 'undefined') return false;
                        var x = self.editContact().IsEmergencyContact() == false;
                        return x;
                    } else {
                        return false;
                    }
                },
                message: Ellucian.UserProfile.emergencyInformationMessages.contactTypeValidation
            }
        }),
        Address: ko.observable("").extend({
            maxLength: 75
        }),
        EffectiveDate: ko.observable().extend({
            validation: {
                validator: function (value) {
                    try {
                        // No error if the date is blank - not a required field
                        if (value == null || value.length == 0) return true;
                        var dateString = Globalize.parseDate(value);
                        if (dateString == null) throw exception;
                        return true;
                    } catch (e) {
                        return false;
                    }
                },
                message: Ellucian.UserProfile.emergencyInformationMessages.contactDateValidation
            }
        })
    };

    editContactViewModel.PhoneValidation = ko.computed(function () {
        return contactHasPhone(editContactViewModel);
    }).extend({
        equal: {
            value: true,
            message: Ellucian.UserProfile.emergencyInformationMessages.contactPhoneValidation,
            onlyIf: function () {
                return self.editDialogIsOpen();
            }
        },
    });

    // Add ViewModel as observable after all properties are defined so validation is run on all
    this.editContact = ko.validatedObservable(editContactViewModel);

    // Open add contact dialog
    this.addEmergencyContact = function () {
        self.isNewContact = true;
        self.editDialogIsOpen(true);

        self.editDialogButtonLabel(Ellucian.UserProfile.emergencyInformationMessages.add);
        self.submitButtonLabel(Ellucian.UserProfile.emergencyInformationMessages.addAria);

        // Clear the existing emergency contact values
        self.editContact().Name("");
        self.editContact().Relationship("");
        self.editContact().DaytimePhone("");
        self.editContact().EveningPhone("");
        self.editContact().OtherPhone("");
        self.editContact().Address("");
        self.editContact().EffectiveDate(self.CurrentDateString());
        self.editContact().IsEmergencyContact(true);
        self.editContact().IsMissingPersonContact(false);
        self.editContact().Name.isModified(false);
        self.editContact().PhoneValidation.isModified(false);
    }

    // Open edit contact dialog
    this.editEmergencyContact = function (contact) {
        self.isNewContact = false;
        self.editDialogIsOpen(true);

        self.editDialogButtonLabel(Ellucian.UserProfile.emergencyInformationMessages.update);
        self.submitButtonLabel(Ellucian.UserProfile.emergencyInformationMessages.updateAria);

        var date = contact.EffectiveDate();

        self.editContact().Name(contact.Name());
        self.editContact().Relationship(contact.Relationship());
        self.editContact().DaytimePhone(contact.DaytimePhone());
        self.editContact().EveningPhone(contact.EveningPhone());
        self.editContact().OtherPhone(contact.OtherPhone());
        self.editContact().Address(contact.Address());
        self.editContact().EffectiveDate(date);
        self.editContact().IsEmergencyContact(contact.IsEmergencyContact());
        self.editContact().IsMissingPersonContact(contact.IsMissingPersonContact());
        self.selectedContactIndex = self.EmergencyContacts.indexOf(contact)
    }

    this.submitEmergencyInformation = function () {

        // If the user has entered data in the name field
        if (self.editContact.isValid()) {
            // Performs a deep copy of contact
            var editContactDeepCopy = {
                Name: ko.observable(self.editContact().Name()),
                Relationship: ko.observable(self.editContact().Relationship()),
                DaytimePhone: ko.observable(self.editContact().DaytimePhone()),
                EveningPhone: ko.observable(self.editContact().EveningPhone()),
                OtherPhone: ko.observable(self.editContact().OtherPhone()),
                Address: ko.observable(self.editContact().Address()),
                EffectiveDate: ko.observable(self.editContact().EffectiveDate()),
                IsEmergencyContact: ko.observable(self.editContact().IsEmergencyContact()),
                IsMissingPersonContact: ko.observable(self.editContact().IsMissingPersonContact())
            };

            // If not adding a new contact, perform a copy, otherwise, push the new contact into the list
            if (!self.isNewContact) {
                self.EmergencyContacts()[self.selectedContactIndex].Name(editContactDeepCopy.Name());
                self.EmergencyContacts()[self.selectedContactIndex].Relationship(editContactDeepCopy.Relationship());
                self.EmergencyContacts()[self.selectedContactIndex].DaytimePhone(editContactDeepCopy.DaytimePhone());
                self.EmergencyContacts()[self.selectedContactIndex].EveningPhone(editContactDeepCopy.EveningPhone());
                self.EmergencyContacts()[self.selectedContactIndex].OtherPhone(editContactDeepCopy.OtherPhone());
                self.EmergencyContacts()[self.selectedContactIndex].Address(editContactDeepCopy.Address());
                self.EmergencyContacts()[self.selectedContactIndex].EffectiveDate(editContactDeepCopy.EffectiveDate());
                self.EmergencyContacts()[self.selectedContactIndex].IsEmergencyContact(editContactDeepCopy.IsEmergencyContact());
                self.EmergencyContacts()[self.selectedContactIndex].IsMissingPersonContact(editContactDeepCopy.IsMissingPersonContact());
            }
            else {
                self.EmergencyContacts.push(editContactDeepCopy);
            }

            // Opt back in when submitting contact information
            if (self.OptOut() === true) {
                self.OptOut(false);
            }
            self.isNewContact = false;
            self.missingContactType(false);
            self.validationMessages([]);
            self.editDialogIsOpen(false);
            self.updateEmergencyInformation();
        }
    };

    this.submitEmergencyInformationButton = { title: self.editDialogButtonLabel, isPrimary: true, callback: self.submitEmergencyInformation, enabled: self.editContact.isValid };

    // Try to post emergency information to database
    this.updateEmergencyInformation = function () {
        if (self.editContact.isValid()) {

            if (typeof Ellucian.UserProfile.emergencyInformationMessages.updating === 'string') {
                self.LoadingMessage(Ellucian.UserProfile.emergencyInformationMessages.updating);
            }
            self.Notification(null);
            self.IsLoading(true);

            repository.update(self)
                .then(function (data) {
                    // Update page (to show the correct last confirmed on date)
                    ko.mapping.fromJS(data, {}, self);
                    // See if there is a notification included that should be shown.
                    showNotification();
                    repositoryCompleteHandler();
                    return;
                })
                .catch(function (message) {
                    $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
                    repositoryCompleteHandler();
                    return;
                });
        }
    };

    // Verify emergency information. This is essentially an update but relying on the Controller method to set the confirmed date and do the update
    // so that if it is not successful we still have the original confirmed date.
    this.verifyEmergencyInformation = function () {
        if (self.editContact.isValid()) {
            //Additional requirement of phones for every contact, not enforced by API
            var contactMissingPhone = ko.utils.arrayFirst(self.EmergencyContacts(), function (contact) {
                if (!contactHasPhone(contact)) {
                    return contact;
                }
            })
            if (contactMissingPhone !== null) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.UserProfile.emergencyInformationMessages.updateNeedPhones, type: "error" });
                return;
            }

            if (typeof Ellucian.UserProfile.emergencyInformationMessages.confirming === 'string') {
                self.LoadingMessage(Ellucian.UserProfile.emergencyInformationMessages.confirming);
            }
            self.IsLoading(true);

            //post the data
            repository.verify(self)
                .then(function (data) {
                    // Update page (to show the correct last confirmed on date)
                    ko.mapping.fromJS(data, {}, self);
                    showNotification();
                    repositoryCompleteHandler();
                    return;
                })
                .catch(function (message) {
                    $('#notificationHost').notificationCenter('addNotification', { message: message, type: "error" });
                    repositoryCompleteHandler();
                    return;
                });
        }
    };

    var repositoryCompleteHandler = function () {
        setTimeout(function () {
            self.IsLoading(false)
        }, 200);
    };

    var showNotification = function () {
        $('#notificationHost').notificationCenter('reset');
        if (typeof self.Notification() != "undefined" && self.Notification() != null) {
            if (self.Notification().Type() == "Error") {
                $('#notificationHost').notificationCenter('addNotification', { message: self.Notification().Message(), type: "error" });
            } else {
                $('#notificationHost').notificationCenter('addNotification', { message: self.Notification().Message(), type: "success" });
            }
        }
    };

    // Ask the user to confirm removal
    this.openRemoveDialog = function (contact) {
        if (self.disableRemoveContact()) {
            var message = Ellucian.UserProfile.emergencyInformationMessages.removeRequiredContactError;
            $('#notificationHost').notificationCenter('addNotification', { message: message, flash: true, type: "error" });
        } else {
            self.removeDialogIsOpen(true);
            self.contactToRemove(contact);
        }
    };

    // Remove the emergency contact
    this.removeEmergencyContact = function () {
        if (self.disableRemoveContact()) {
            var message = Ellucian.UserProfile.emergencyInformationMessages.removeRequiredContactError;
            $('#notificationHost').notificationCenter('addNotification', { message: message, flash: true, type: "error" });
        } else {
            self.EmergencyContacts.remove(self.contactToRemove());
            self.updateEmergencyInformation();
        }
        self.removeDialogIsOpen(false);
    };

    this.removeEmergencyContactButton = { title: Ellucian.SharedComponents.ButtonLabels.buttonTextAccept, isPrimary: true, callback: self.removeEmergencyContact };

    // Close the remove contact dialog
    this.closeRemoveDialog = function () {
        self.removeDialogIsOpen(false);
    };
};