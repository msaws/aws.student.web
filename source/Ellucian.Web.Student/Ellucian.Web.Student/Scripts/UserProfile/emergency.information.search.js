﻿//Copyright 2017 Ellucian Company L.P. and its affiliates
$(document).ready(function() {
    var emergencyInformationSearchInstance = new Ellucian.UserProfile.emergencyInformationSearchViewModel();
    ko.applyBindings(emergencyInformationSearchInstance, document.getElementById("emergency-information-search"));
    emergencyInformationSearchInstance.getEmergencyInformationSearchViewModel("UserProfile");

})