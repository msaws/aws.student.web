﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

Ellucian.UserProfile.PreferencesViewModel = function PreferencesViewModel(preferencesRepository) {
    var self = this;

    // PreferencesModel.cs properties...
    self.Homepage = ko.observable("");

    // Knockout properties...
    self.isWaiting = ko.observable(false);

    self.waitingMessage = ko.observable("");

    this.get = function () {

        self.waitingMessage(Ellucian.UserProfile.preferencesMessages.loadingMessage);
        self.isWaiting(true);

        preferencesRepository.get()
            .then(function (data) {
                ko.mapping.fromJS(data, {}, self);
                updateCompleted();
                return;
            })
            .catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
                updateCompleted();
                return;
            });
    };

    this.update = function () {
        self.waitingMessage(Ellucian.UserProfile.preferencesMessages.waitingUpdateMessage);
        self.isWaiting(true);

        preferencesRepository.update(self)
            .then(function (data) {
                successHandler(data);
                updateCompleted();
                return;
            })
            .catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" });
                updateCompleted();
                return;
            });
    };

    var successHandler = function (data) {
        ko.mapping.fromJS(data, {}, self);
        $('#notificationHost').notificationCenter('reset');
        $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.UserProfile.preferencesMessages.updateSuccess, type: "success", flash: true });
    };

    // Extends the time that the loading/waiting message is displayed to prevent the page from appearing to glitch when loading really fast
    var updateCompleted = function () {
        self.isWaiting(false);
        $("#landing-page-input").focus();   // Move the focus to the DDL when finished the AJAX call.
    };
};