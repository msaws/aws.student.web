﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

Ellucian.UserProfile.preferencesRepository = (function () {

    var updateSucceeded = function (data) {
        if (!account.handleInvalidSessionResponse(data)) {
            return Promise.resolve(data);
        }
        return Promise.reject(Ellucian.UserProfile.preferencesMessages.updateFailed);
    };

    return {

        // Get a user's preferences
        get: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.preferencesRepositoryUrls.get,
                    type: "GET",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                        return;
                    }
                    reject(Ellucian.UserProfile.preferencesMessages.getFailed);
                    return;
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.preferencesMessages.getFailed);
                    return;
                });
            });
        },

        update: function (preferences) {
            var json = { preferencesJson: ko.toJSON(preferences) };
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.UserProfile.preferencesRepositoryUrls.update,
                    data: JSON.stringify(json),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                })
                .done(function (data) {
                    resolve(updateSucceeded(data));
                    return;
                })
                .fail(function () {
                    reject(Ellucian.UserProfile.preferencesMessages.updateFailed);
                    return;
                });
            });
        }
    }
})();