﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
$(document).ready(function () {
    var vm = new PageViewModel();
    var viewModel = ko.applyBindings(vm, document.getElementById("main"));
});