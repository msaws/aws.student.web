﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
/////////////////////////////////////////////////////////////////////////////////////////////
//  This file contains utility functionality that needs to be made available across
//  two or more views in the entire Self Service application tree. Functions placed into this
//  file should be as succint and context-less as possible. These functions will be available
//  in every view in the Self Service solution, including all Areas, and loaded in the global
//  scripts bundle. Do NOT place anything into this file that is specific to areas such
//  Planning or Finance.
/////////////////////////////////////////////////////////////////////////////////////////////

// Add an antiforgerytoken request header to all ajax requests. 
// This code is run when the document is loaded and it assumes all pages/forms 
// contain the hidden antiforgerytoken field named "__RequestVerificationToken". 
// Note: headers specified by an individual $.ajax() call will be appended
// to the list of headers of that ajax request.
$(document).ready( function () {
    var antiForgeryToken = $('input[name="__RequestVerificationToken"]').val();
    $.ajaxSetup({
        headers: {
            __RequestVerificationToken: antiForgeryToken
        },
        converters: {
        "text json": function (msg) {
            return $.parseJSON(msg, true);
            }
        }
    });
});

// New jQuery selector to do case-insensitive contains()
(function ($) {
    $.expr[":"].containsany = $.expr.createPseudo(function (arg) {
        if (typeof arg === "undefined") arg = "";
        return function (e) {
            return $(e).text().toLowerCase().indexOf(arg.toLowerCase()) >= 0;
        };
    });
})(jQuery);

// A content-type property for AJAX calls which send JSON data.
var jsonContentType = "application/json, charset=utf-8";

// Setting cookies to help with unsupported browser check
var browserCookieName = "Ellucian.Web.Student__SupportedBrowserCheck";

// Unified constant for jQuery dialog fade in/out animation
var dialogFadeTimeMs = 200;


// Checks whether a variable is null or empty. Any of the following conditions mean that it is:
//   value = ''
//   value = null
//   value = 'null' (this is helpful for checking values that come from serialized JSON)
//   value = undefined
function isNullOrEmpty(value) {
    return (typeof value === "undefined" || value === null || value === "" || value === "null" || value === undefined);
}

// Converts the given number of minutes to a formatted time string. This begins at 12:00AM,
// so that passing in a value of 90 will yield a string of "1:30 AM".
function minutesToTime(minutes) {
    var time = "";
    var hours = Math.floor(minutes / 60);
    var minutesRemainder = minutes % (60 * (hours ? hours : 1));
    var AMPM = "AM";

    if (minutes >= 720 && minutes != 1440) AMPM = "PM";
    if (hours > 12) {
        hours = hours - 12;
    }
    if (hours == 0) hours = "12";
    if (minutesRemainder < 10) minutesRemainder = "0" + minutesRemainder;

    return hours + ":" + minutesRemainder + " " + AMPM;
}


// Date utility to easily add a given number of hours to a Date object
Date.prototype.addHours = function (hours) {
    this.setTime(this.getTime() + (hours * 3600 * 1000));
    return this;
}

// Static Date utility to return a new Date object (time at midnight) of today;
Date.Today = function today() {
    var todayDate = new Date();
    return new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate());
}

// Static Date utility to return a new Date object (time at midnight) of today + the integer offset number of days
// offset can be positive or negative, and it properly rolls the month and year forward or back depending on the date
Date.TodayDateOffset = function todayOffset(offset) {
    var todayDate = new Date();
    return new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate() + offset);
}

// Static Date utility to return a new Date object (time at midnight) of the date passed in + the integer offset number
// of days. Offset can be positive or negative  and it properly rolls the month and year forward or back depending on the date
Date.DateOffset = function dateOffset(date, offset) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate() + offset);
}

//Adds a method to the date prototype to add an offset number of days to this Date object.
Date.prototype.addDays = function (offset) {
    this.setDate(this.getDate() + offset);
    return this;
}

//Static Date utility to compare two Dates. This function use the specific Time value of the date
//to compare.
//Returns:
//  1 if date1 is greater than date2.
//  0 if date1 is equal to date2
//  -1 if date1 is less than date2
Date.Compare = function compareDates(date1, date2) {
    var time1 = (date1) ? date1.getTime() : null,
        time2 = (date2) ? date2.getTime() : null;

    if (time1 > time2) return 1;
    else if (time1 === time2) return 0;
    else return -1;
}

// Determines whether a given string represents a number. So, both 12 and "12"
// are numbers, but "12a" is not.
function isNumber(string) {
    return !isNaN(parseFloat(string)) && isFinite(string);
}

// Replicate .NET string functionality in JavaScript
String.prototype.format = function () {
    var text = this;
    for (var i = 0; i < arguments.length; i++) {
        var match = "{" + i + "}";
        text = text.replace(match, arguments[i]);
    }
    return text;
};
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.contains = function (str) {
    return this.indexOf(str) !== -1;
};

// Determine if browser is unsupported
function checkSupportedBrowser() {

    // The initial release of Self-Service needs to support IE9 despite that browser not really supporting HTML5.
    // The following combination of checks should give the warning in IE7/8 and any other ancient and/or limited browsers
    // but will also pass browsers which are not "officially" supported in STSS, such as Opera.
    //
    // Put another way, the only browsers that should fail this test are IE7 and IE8, plus other ancient browsers long since put out of use (Firefox 1.0, Netscape Navigator, etc).
    // Extremely early (say, v1.0) versions of Firefox and Chrome may also fail, but it'll be a VERY rare case when someone is
    // actually using those. Mobile browsers such as Android stock (2.3+), Chrome on Android and iOS, Safari on iOS, Dolphin, etc should
    // all pass this test as well, so it's hardly a restrictive test.

    var supported = (Modernizr.cssgradients ||          //just check for basic support of at least one CSS3 item (IE9 does not support proper CSS gradients)
                     Modernizr.borderradius ||
                     Modernizr.boxshadow) &&
                     Modernizr.canvas &&                //this is a catch-all HTML5/new-ish browser check - considered using inputtypes.search but IE9 fails that (although there's no real consequence of that failure)
                     jQuery.support.ajax;               //must support AJAX to be able to do most actions

    if (!supported) {
        if (document.cookie.indexOf(browserCookieName) == -1) {
            //this check allow clients, if they really really want to, bypass warning of users by emptying out the warning string in the SiteResources.resx file
            if (typeof unsupportedBrowserMessage === "string" && unsupportedBrowserMessage.length > 0) {
                alert(unsupportedBrowserMessage);
            }
            //set it to expire in 8 hours
            var expiry = new Date().addHours(8);
            var cookie = browserCookieName + "=true;expires=" + expiry.toGMTString() + ";path=/";
            document.cookie = cookie;
        }
    }
}

// Provide easy access to query string parameters within script on any given page
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

// Provide easy access to query string parameters for URLs within existing JS variables
function getVariableUrlVars(url) {
    if (!url) {
        return null;
    }
    var vars = [], hash;
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

/**************    Array functions    ***************/
Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

Array.prototype.mergeAll = function () {
    var results = [];
    this.forEach(function (subArray) {
        results.push.apply(results, subArray);
    })
    return results;
};

Array.prototype.flatMap = function (projectionFunctionThatReturnsArray) {
    return this.
        map(function (item) {
            return projectionFunctionThatReturnsArray(item);
        }).
    mergeAll();
};

Array.prototype.unique = function (functionThatReturnsKeyOfArrayItem) {
    var results = {};
    this.forEach(function (item) {
        var key = functionThatReturnsKeyOfArrayItem(item);
        results[key] = item;
    });
    return Object.keys(results).map(function (key) {
        return results[key];
    });
};

Array.zip = function (left, right, combinerFunction) {
    var counter,
        results = [];

    for (counter = 0; counter < Math.min(left.length, right.length) ; counter++) {
        results.push(combinerFunction(left[counter], right[counter]));
    }
};



// jQuery extension to convert incoming dates into true JS Date objects
//  - Code source: http://erraticdev.blogspot.com/2010/12/converting-dates-in-json-strings-using.html
//  - Minor changes were made to the code in the blog based on comments on the blog entry
/*!
 * jQuery.parseJSON() extension (supports ISO & Asp.net date conversion)
 *
 * Version 1.0 (13 Jan 2011)
 *
 * Copyright (c) 2011 Robert Koritnik
 * Licensed under the terms of the MIT license
 * http://www.opensource.org/licenses/mit-license.php
 */
(function ($) {

    // JSON RegExp
    var rvalidchars = /^[\],:{}\s]*$/;
    var rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
    var rvalidtokens = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
    var rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g;
    var dateISO = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:[.,]\d+)?Z?/i;
    var dateNet = /\/Date\((-?\d+)(?:[-\+]\d+)?\)\//i;

    // replacer RegExp
    var replaceISO = /"(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:[.,](\d+))?Z?"/gi;
    var replaceNet = /"\\\/Date\((-?\d+)(?:[-\+]\d+)?\)\\\/"/gi;

    // determine JSON native support
    var nativeJSON = (window.JSON && window.JSON.parse) ? true : false;
    var extendedJSON = nativeJSON && window.JSON.parse('{"x":9}', function (k, v) { return "Y"; }) === "Y";

    var jsonDateConverter = function (key, value) {
        if (typeof (value) === "string") {
            if (dateISO.test(value)) {
                return new Date(value);
            }
            if (dateNet.test(value)) {
                return new Date(parseInt(value.replace(dateNet, "$1")));
            }
        }
        return value;
    };

    $.extend({
        parseJSON: function (data, convertDates) {
            /// <summary>Takes a well-formed JSON string and returns the resulting JavaScript object.</summary>
            /// <param name="data" type="String">The JSON string to parse.</param>
            /// <param name="convertDates" optional="true" type="Boolean">Set to true when you want ISO/Asp.net dates to be auto-converted to dates.</param>

            if (typeof data !== "string" || !data) {
                return null;
            }

            // Make sure leading/trailing whitespace is removed (IE can't handle it)
            data = $.trim(data);

            // Make sure the incoming data is actual JSON
            // Logic borrowed from http://json.org/json2.js
            if (rvalidchars.test(data
                .replace(rvalidescape, "@")
                .replace(rvalidtokens, "]")
                .replace(rvalidbraces, ""))) {
                // Try to use the native JSON parser
                if (extendedJSON || (nativeJSON && convertDates !== true)) {
                    return window.JSON.parse(data, convertDates === true ? jsonDateConverter : undefined);
                }
                else {
                    data = convertDates === true ?
                        data.replace(replaceISO, "new Date(parseInt('$1',10),parseInt('$2',10)-1,parseInt('$3',10),parseInt('$4',10),parseInt('$5',10),parseInt('$6',10),(function(s){return parseInt(s,10)||0;})('$7'))")
                            .replace(replaceNet, "new Date($1)") :
                        data;
                    return (new Function("return " + data))();
                }
            } else {
                $.error("Invalid JSON: " + data);
            }
        }
    });
})(jQuery);

// JavaScript function to parse an ISO date, date-time, or date-time-offset string
// and return a JavaScript Date object.
function parseIsoDate(input) {
    var iso = /^([+-]?\d{4})(?:(?:-?(\d{2})-?(\d{2}))|-?W(\d{2})(?:-?(\d))?|-?(\d{3})|-(\d{2}))(?:[T ](\d{2})(?::?(\d{2}))?(?::?(\d{2}))?(?:[,.](\d+))?)?(?:(Z)?(?:([+-]\d{2}))?(?::?(\d{2}))?)?$/;

    var parts = String(input).match(iso);
    if (parts === null) {
        parts = input.toISOString().match(iso);;
    }

    if (parts === null) {
        return undefined;
    }

    var year = Number(parts[1]);
    var months, days, weeks;
    if (typeof parts[2] !== "undefined") {
        months = Number(parts[2]) - 1;
        days = Number(parts[3]);
    } else if (typeof parts[4] !== "undefined") {
        /* Convert weeks to days, months 0 */
        weeks = Number(parts[4]) - 1;
        days = Number(parts[5]);
        if (typeof days === "undefined") {
            days = 0;
        }
        days += weeks * 7;
        months = 0;
    } else if (typeof parts[6] !== "undefined") {
        /* it's an ordinal date... */
        days = Number(parts[6]);
        months = 0;
    } else {
        // Month only - assume first of the month
        months = Number(parts[7]) - 1;
        days = 1;
    }

    var hours = 0, minutes = 0, seconds = 0, milliseconds = 0;
    if (typeof parts[8] !== "undefined") {
        hours = Number(parts[8]);
        if (typeof parts[9] !== "undefined") {
            minutes = Number(parts[9]);

            if (typeof parts[10] !== "undefined") {
                seconds = Number(parts[10]);

                if (typeof parts[11] !== "undefined") {
                    var fractional = Number(parts[11]);
                    milliseconds = fractional / 100;
                }
            } else if (typeof parts[11] !== "undefined") {
                var len = parts[11].length;
                var power = Math.pow(10, len);
                var fractional = Number(parts[11]) / power;
                //var fractional = Number(parts[11]) / Math.pow(10, parts[11].length);
                seconds = fractional * 60;
            }
        } else if (typeof parts[11] !== "undefined") {
            var len = parts[11].length;
            var power = Math.pow(10, len);
            var fractional = Number(parts[11]) / power;
            //var fractional = Number(parts[11]) / Math.pow(10, parts[11].length);
            minutes = fractional * 60;
        }

        var timezone;
        var localzone = new Date().getTimezoneOffset();
        if (typeof parts[12] !== "undefined" && typeof parts[13] === "undefined") {
            // Zulu specified with no additional offset
            timezone = 0;
        } else if (typeof parts[12] === "undefined" && typeof parts[13] === "undefined") {
            // Zulu not specified and no additional offset - use local offset
            timezone = localzone * -1;
        } else {
            var offsetHours = Number(parts[13]);
            var offsetMinutes = Number(parts[14] || 0);
            timezone = ((offsetHours * 60) + offsetMinutes);
        }

        minutes = Number(minutes) - (timezone + localzone);
    }
    else {
        var hours = 0;
        var minutes = 0;
        var seconds = 0;
        var fractional = 0;
        var milliseconds = 0;
    }

    return new Date(year, months, days, hours, minutes, seconds, milliseconds);
}

var negativeNumberPatterns = ["(n)", "-n", "- n", "n-", "n -"];
var negativePercentPatterns = ["-n %", "-n%", "-%n", "%-n", "%n-", "n-%", "n%-", "-% n", "n %-", "% n-", "% -n", "n- %"];
var positivePercentPatterns = ["n %", "n%", "%n", "% n"];
var negativeCurrencyPatterns = ["($n)", "-$n", "$-n", "$n-", "(n$)", "-n$", "n-$", "n$-", "-n $", "-$ n", "n $-", "$ n-", "$ -n", "n- $", "($ n)", "(n $)"];
var positiveCurrencyPatterns = ["$n", "n$", "$ n", "n $"];

// Set culture definitions for Globalize to use
function setGlobalizeDefaults(currentCulture) {
    if (!currentCulture || typeof currentCulture !== "object") {
        return;
    }
    //Globalize.culture(currentCulture.Name);
    var cultureInfo = {};

    // A unique name for the culture in the form <language code>-<country/region code>
    if (currentCulture.Name) {
        cultureInfo["name"] = currentCulture.Name;
    }
    // the name of the culture in the english language
    if (currentCulture.EnglishName) {
        cultureInfo["englishName"] = currentCulture.EnglishName;
    }
    // the name of the culture in its own language
    if (currentCulture.NativeName) {
        cultureInfo["nativeName"] = currentCulture.NativeName;
    }
    // whether the culture uses right-to-left text
    cultureInfo["isRTL"] = currentCulture.TextInfo.IsRightToLeft || false;
    // "language" is used for so-called "specific" cultures.
    if (currentCulture.TwoLetterISOLanguageName) {
        cultureInfo["language"] = currentCulture.TwoLetterISOLanguageName;
    }

    var numberFormatInfo = currentCulture.NumberFormat;
    if (numberFormatInfo && typeof numberFormatInfo === "object") {
        // numberFormat defines general number formatting rules, like the digits in
        // each grouping, the group separator, and how negative numbers are displayed.
        var numberInfo = {};
        // [negativePattern]
        // Note, numberFormat.pattern has no "positivePattern" unlike percent and currency,
        // but is still defined as an array for consistency with them.
        //   negativePattern: one of "(n)|-n|- n|n-|n -"
        var negativeNumberPattern = numberFormatInfo.NumberNegativePattern || 0;
        if (negativeNumberPattern > negativeNumberPatterns.length) {
            negativeNumberPattern = 0;
        }
        numberInfo["pattern"] = negativeNumberPatterns[negativeNumberPattern];
        // number of decimal places normally shown
        numberInfo["decimals"] = numberFormatInfo.NumberDecimalDigits || 2;
        // string that separates number groups, as in 1,000,000
        if (numberFormatInfo.NumberGroupSeparator) {
            numberInfo[","] = numberFormatInfo.NumberGroupSeparator;
        }
        // string that separates a number from the fractional portion, as in 1.99
        if (numberFormatInfo.NumberDecimalSeparator) {
            numberInfo["."] = numberFormatInfo.NumberDecimalSeparator;
        }
        // array of numbers indicating the size of each number group.
        if (numberFormatInfo.NumberGroupSizes) {
            numberInfo["groupSizes"] = numberFormatInfo.NumberGroupSizes;
        }
        // symbol used for positive numbers
        if (numberFormatInfo.PositiveSign) {
            numberInfo["+"] = numberFormatInfo.PositiveSign;
        }
        // symbol used for negative numbers
        if (numberFormatInfo.NegativeSign) {
            numberInfo["-"] = numberFormatInfo.NegativeSign;
        }
        // symbol used for NaN (Not-A-Number)
        if (numberFormatInfo.NaNSymbol) {
            numberInfo["NaN"] = numberFormatInfo.NaNSymbol;
        }
        // symbol used for Negative Infinity
        if (numberFormatInfo.NegativeInfinitySymbol) {
            numberInfo["negativeInfinity"] = numberFormatInfo.NegativeInfinitySymbol;
        }
        // symbol used for Positive Infinity
        if (numberFormatInfo.PositiveInfinitySymbol) {
            numberInfo["positiveInfinity"] = numberFormatInfo.PositiveInfinitySymbol;
        }

        var percentInfo = {};
        // [negativePattern, positivePattern]
        //   negativePattern: one of "-n %|-n%|-%n|%-n|%n-|n-%|n%-|-% n|n %-|% n-|% -n|n- %"
        //   positivePattern: one of "n %|n%|%n|% n"
        var negativePercentPattern = numberFormatInfo.PercentNegativePattern || 0;
        if (negativePercentPattern > negativePercentPatterns.length) {
            negativePercentPattern = 0;
        }
        var positivePercentPattern = numberFormatInfo.PercentPositivePattern || 0;
        if (positivePercentPattern > positivePercentPatterns.length) {
            positivePercentPattern = 0;
        }
        percentInfo["pattern"] = [negativePercentPatterns[negativePercentPattern], positivePercentPatterns[positivePercentPattern]];
        // number of decimal places normally shown
        percentInfo["decimals"] = numberFormatInfo.PercentDecimalDigits || 2;
        // array of numbers indicating the size of each number group.
        percentInfo["groupSizes"] = numberFormatInfo.PercentGroupSizes;
        // string that separates number groups, as in 1,000,000
        percentInfo[","] = numberFormatInfo.PercentGroupSeparator;
        // string that separates a number from the fractional portion, as in 1.99
        percentInfo["."] = numberFormatInfo.PercentDecimalSeparator;
        // symbol used to represent a percentage
        percentInfo["symbol"] = numberFormatInfo.PercentSymbol;
        if (percentInfo && percentInfo.length) {
            numberInfo["percent"] = percentInfo;
        }

        var currencyInfo = {};
        // [negativePattern, positivePattern]
        //   negativePattern: one of "($n)|-$n|$-n|$n-|(n$)|-n$|n-$|n$-|-n $|-$ n|n $-|$ n-|$ -n|n- $|($ n)|(n $)"
        //   positivePattern: one of "$n|n$|$ n|n $"
        var negativeCurrencyPattern = numberFormatInfo.CurrencyNegativePattern || 0;
        if (negativeCurrencyPattern > negativeCurrencyPatterns.length) {
            negativeCurrencyPattern = 0;
        }
        var positiveCurrencyPattern = numberFormatInfo.CurrencyPositivePattern || 0;
        if (positiveCurrencyPattern > positiveCurrencyPatterns.length) {
            positiveCurrencyPattern = 0;
        }
        currencyInfo["pattern"] = [negativeCurrencyPatterns[negativeCurrencyPattern], positiveCurrencyPatterns[positiveCurrencyPattern]];
        // number of decimal places normally shown
        currencyInfo["decimals"] = numberFormatInfo.CurrencyDecimalDigits || 2;
        // array of numbers indicating the size of each number group.
        currencyInfo["groupSizes"] = numberFormatInfo.CurrencyGroupSizes;
        // string that separates number groups, as in 1,000,000
        currencyInfo[","] = numberFormatInfo.CurrencyGroupSeparator;
        // string that separates a number from the fractional portion, as in 1.99
        currencyInfo["."] = numberFormatInfo.CurrencyDecimalSeparator;
        // symbol used to represent currency
        currencyInfo["symbol"] = numberFormatInfo.CurrencySymbol;
        if (currencyInfo && currencyInfo.length) {
            numberInfo["currency"] = currencyInfo;
        }

        // Update the culture with the number formatting info
        if (numberInfo && numberInfo.length) {
            cultureInfo["numberFormat"] = numberInfo;
        }
    }

    var calendarInfo = currentCulture.DateTimeFormat;
    if (calendarInfo && typeof calendarInfo === "object") {
        var standardCalendar = {};
        standardCalendar["name"] = calendarInfo.NativeCalendarName;
        standardCalendar["/"] = calendarInfo.DateSeparator;
        standardCalendar[":"] = calendarInfo.TimeSeparator;
        standardCalendar["firstDay"] = calendarInfo.FirstDayOfWeek;
        standardCalendar["days"] = {
            names: calendarInfo.DayNames,
            namesAbbr: calendarInfo.AbbreviatedDayNames,
            namesShort: calendarInfo.ShortestDayNames
        };
        standardCalendar["months"] = {
            names: calendarInfo.MonthNames,
            namesAbbr: calendarInfo.AbbreviatedMonthNames
        };
        standardCalendar["AM"] = [calendarInfo.AMDesignator];
        standardCalendar["PM"] = [calendarInfo.PMDesignator];
        standardCalendar["eras"] = [
            // eras in reverse chronological order.
            // name: the name of the era in this culture (e.g. A.D., C.E.)
            // start: when the era starts in ticks (gregorian, gmt), null if it is the earliest supported era.
            // offset: offset in years from gregorian calendar
            {
                "name": "A.D.",
                "start": null,
                "offset": 0
            }
        ];
        // No .NET equivalent to this, so don't do anything with it
        // standardCalendar["twoDigitYearMax"] = "";
        // A set of predefined date and time patterns used by the culture
        // these represent the format someone in this culture would expect
        // to see given the portions of the date that are shown.
        standardCalendar["patterns"] = {
            // short date pattern
            d: calendarInfo.ShortDatePattern,
            // long date pattern
            D: calendarInfo.LongDatePattern,
            // short time pattern
            t: calendarInfo.ShortTimePattern,
            // long time pattern
            T: calendarInfo.LongTimePattern,
            // long date, short time pattern
            f: calendarInfo.LongDatePattern + " " + calendarInfo.ShortTimePattern,
            // long date, long time pattern
            F: calendarInfo.FullDateTimePattern,
            // month/day pattern
            M: calendarInfo.MonthDayPattern,
            // month/year pattern
            Y: calendarInfo.YearMonthPattern,
            // S is a sortable format that does not vary by culture
            S: calendarInfo.SortableDateTimePattern
        };
        cultureInfo["calendars"] = {};
        cultureInfo.calendars["standard"] = standardCalendar;
    }

    // Add the culture settings and make then active
    Globalize.addCultureInfo(currentCulture.Name, cultureInfo);
    Globalize.culture(currentCulture.Name);
}

//Static function that gets the specified Date format of the closest current culture
//  Arguments:
//      dateFormat: optionally, specify the date format to find. The default is small d.
Globalize.getClosestCultureDateFormat = function (dateFormat) {
    var format = dateFormat || "d";
    return Globalize.findClosestCulture().calendars['standard'].patterns[format];
}

/*
Object to compute whether any property of the model has changed. This object
subscribes to every property on the model, so consider the usage of this carefully on
large, complex models. Also carefully consider whether you want dirty notification for
ALL properties of your model (including computeds) or just specific properties of your model - in which case
don't use this object =)
    Constructor:        
        Pass in your viewModel and whether or not the model is initially dirty,
        Example:
            new ko.dirtyFlag(viewModel, false)

    Methods:
        isDirty() - ko.computed that compares the initial state to the current state.
        reset() - function to reset the initial state of the model to the current state of the model
*/
ko.dirtyFlag = function (model, isInitiallyDirty) {
    var result = function () { },
            _initialState = ko.observable(ko.toJSON(model)),
            _isInitiallyDirty = ko.observable(isInitiallyDirty);

    result.isDirty = ko.computed(function () {
        return _isInitiallyDirty() || _initialState() !== ko.toJSON(model);
    });

    result.reset = function () {
        _initialState(ko.toJSON(model));
        _isInitiallyDirty(false);
    };

    return result;
};

// Function to remove mark-up from submitted data
function sanitizeFormData(data) {
    if (data) {
        if (data.constructor === Array) {
            for (i = 0; i < data.length; i++) {
                if (data[i].constructor === String) {
                    data[i] = data[i].replace(/(<([^>]+)>)/ig, "");
                }
            }
        }
        if (data.constructor === String) {
            data = data.replace(/(<([^>]+)>)/ig, "");
        }
        return data;
    }
}

// Function to shift focus to supplied element
function setFocus(jqSelector) {
    if (jqSelector) {
        $(jqSelector).first().focus();
    }
}

// Function to push an ARIA announcement
function makeAnnouncement(announcement) {
    if (announcement) {
        $("#aria-announcements").text(announcement);
    }
}

// Function to show or hide a DOM element based on presence or lack of multiple items
function showOrHideElement (items, selector) {
    if (items.length > 1) {
        // Make sure the link is visible
        selector.show("fast");
    } else {
        // Hide the link
        selector.hide("fast");
    }
};

// Generic AJAX error handler that throws an error notification into notification center
// and triggers a callback function
// Arguments:
// - jqXHR: inherited from $.ajax's error event
// - textStatus: inherited from $.ajax's error event
// - errorThrown: inherited from $.ajax's error event
// - errorMessageOverride: application's preferred notification message; if not specified,
//      the message returned from the controller will be used
// - callback: optional function to execute application-specific logic independent of 
//      the generic error handling
function handleAjaxError(jqXHR, textStatus, errorThrown, errorMessageOverride, callback) {
    if (jqXHR.status != 0) {
        $('#notificationHost').notificationCenter('addNotification', { message: errorMessageOverride ? errorMessageOverride : jqXHR.statusText, type: "error" });
    }
    if (callback != null && typeof callback == 'function') {
        callback();
    }
}

// Function from David Walsh: http://davidwalsh.name/css-animation-callback
// Posible that multiple transitions triggering in Chrome was fixed post 2014
var transitionEvent = function whichTransitionEvent() {
    var t,
        el = document.createElement("fakeelement");

    var transitions = {
        "transition": "transitionend",
        "OTransition": "oTransitionEnd",
        "MozTransition": "transitionend",
        "WebkitTransition": "webkitTransitionEnd"
    }

    for (t in transitions) {
        if (el.style[t] !== undefined) {
            return transitions[t];
        }
    }
}

// Function from: http://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value-in-javascript/4760279#4760279
// Allows sorting of array of objects by string property value
function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        // modified from the original, a and b are observables and need to be called to access their properties
        var result = (a[property]() < b[property]()) ? -1 : (a[property]() > b[property]()) ? 1 : 0;
        return result * sortOrder;
    }
}