﻿; (function ($) {
    // This will create an accordion for the menu navigation structure
    // It should be invoked on the top-level <ul> element of the navigation
    // Example: $("ul#nav-root").menuAccordion();
    //
    // Creating submenus:
    //      <li> elements with a class of 'menu-wrapper' are assumed to contain a child menu, and should contain
    //      a <ul> element with a class of 'child-menu'. The first element with a class of 'menu-item' will be
    //      wrapped with a label element and a checkbox included to control collapse state
    // Expected sample navigation list:
    // <ul id="nav-root"> <!-- This is the element that the plugin should be invoked on -->
    //    <li class="menu-wrapper">
    //        <span class="menu-item"><a href="#">Top level menu 1</a></span>
    //        <ul class="child-menu">
    //            <li class="menu-wrapper">
    //                <span class="menu-item"><a href="#">Submenu 1-1</a></span>
    //                <ul class="child-menu">
    //                    <li><span class="menu-item"><a href="#">Item 1-1-1</a></span></li>
    //                    <li><span class="menu-item"><a href="#">Item 1-1-2</a></span></li>
    //                </ul>
    //            </li>
    //            <li><span class="menu-item">Item 1-1</span></li>
    //        </ul>
    //    </li>
    //    <li class="menu-wrapper">
    //        <span class="menu-item">Top level menu 2</span>
    //        <ul class="child-menu">
    //            <li><span class="menu-item">Item 2-1</span></li>
    //            <li><span class="menu-item">Item 2-2</span></li>
    //            <li><span class="menu-item">Item 2-3</span></li>
    //            <li class="menu-wrapper">
    //                <span class="menu-item"><a href="#">Submenu 2-1</a></span>
    //                <ul class="child-menu">
    //                    <li><span class="menu-item"><a href="#">Item 2-1-1</a></span></li>
    //                    <li><span class="menu-item"><a href="#">Item 2-1-2</a></span></li>
    //                </ul>
    //            </li>
    //        </ul>
    //    </li>
    //    <li><span class="menu-item">Top level menu 3</span></li>
    //</ul>

    $.fn.menuAccordion = function (options) {
        var self = this;
        // Use resource string if available
        var defaultCheckboxAriaLabel = typeof navigationCheckboxExpandText !== 'undefined' ? navigationCheckboxExpandText : "Check to expand menu";
        this.defaults = {
            menuLevelClasses: ['primary-menu', 'secondary-menu', 'tertiary-menu'],
            headerLabelClass: 'menu-header',
            menuWrapperClass: 'menu-wrapper',
            menuItemClass: 'menu-item',
            includeCollapseStateIcon: true,
            collapseStateElement: "<span class='float-right arrow'>",
            collapseStateClosedClass: 'arrow-down',
            collapseStateOpenClass: 'arrow-up',
            checkboxIdPrefix: 'nav-group',
            checkboxAriaLabel: defaultCheckboxAriaLabel
        };

        var opts = $.extend({}, this.defaults, options);

        // Builds accordion functionality into the navigation menu.
        // When not called recursively from itself, it is expected that menuElement be the top level
        // menu <ul>, level be 0, and the checkboxIdPrefix should be a valid string for use in element ids
        // Accordion functionality is provided via hidden checkboxes that are linked to <label>s that are wrapped around menu headers.
        var initializeMenu = function (menuElement, level, checkboxIdPrefix) {
            return new Promise(function (resolve, reject) {
                if (opts.menuLevelClasses.length > level) {
                    $(menuElement).addClass(opts.menuLevelClasses[level]);
                }
                $(menuElement).children("li." + opts.menuWrapperClass).each(function (index, element) {
                    var headerLink = $(element).children("." + opts.menuItemClass).first();
                    var childMenu = $(element).children("ul.child-menu").first();
                    if (headerLink.length && childMenu.length) {
                        // This list item has a nested list -- generate accordion
                        var checkboxId = checkboxIdPrefix + "-" + index;
                        var headerLabel = $("<label for='" + checkboxId + "' class='" + opts.headerLabelClass + "'>");
                        var ariaLabelText = headerLink.text() + " " + opts.checkboxAriaLabel;
                        var openCheckbox = $("<input type='checkbox' id='" + checkboxId + "' data-menu-level='" + level + "' aria-label='" + ariaLabelText + "' class='offScreen' aria-expanded='false' />");
                        
                        // Start collapsed
                        var collapseElement = $(opts.collapseStateElement).addClass(opts.collapseStateClosedClass);
                        childMenu.slideUp();

                        // When a checkbox is checked, show the related menu and hide others on the same 'level'
                        // If unchecked, hide the related menu
                        openCheckbox.on('change', function (e) {
                            if ($(this).prop('checked')) {
                                collapseElement.removeClass(opts.collapseStateClosedClass).addClass(opts.collapseStateOpenClass);
                                $(this).attr('aria-expanded', true);
                                $(self).find('input[data-menu-level=' + level + ']').each(function (ev) {
                                    if ($(this).prop('id') !== openCheckbox.prop('id') && $(this).prop('checked')) {
                                        $(this).prop('checked', false).trigger('change');
                                    }
                                });
                                childMenu.slideDown();
                            } else {
                                collapseElement.removeClass(opts.collapseStateOpenClass).addClass(opts.collapseStateClosedClass);
                                $(this).attr('aria-expanded', false);
                                childMenu.slideUp();
                            }
                        });
                        openCheckbox.on('focusin', function (e) {
                            $(this).siblings().children("span[rel~='tooltip']").first().mouseenter();
                        });
                        openCheckbox.on('focusout', function (e) {
                            $(this).siblings().children("span[rel~='tooltip']").first().mouseleave();
                        });
                        headerLink.before(openCheckbox);
                        headerLink.wrap(headerLabel).append(collapseElement);

                        // Recurse into child menus
                        initializeMenu(childMenu, level + 1, checkboxId);
                    }
                });
                resolve();
            });
        };

        // openToElement opens all the parent accordions in order to show a given element
        this.openToElement = function (element, classToAdd) {
            $(element).addClass(classToAdd);
            $(element).parentsUntil(self, '.' + opts.menuWrapperClass).children("input[data-menu-level]").each(function (e) {
                if (!$(this).prop('checked')) {
                    $(this).prop('checked', 'true').trigger('change');
                }
            });

        };

        this.addClassToElementAndParentMenus = function (element, classToAdd) {
            $(element).addClass(classToAdd);
            $(element).parentsUntil(self, '.' + opts.menuWrapperClass).each(function (e) {
                $(this).addClass(classToAdd);
            });
        }

        this.menuLoad = initializeMenu(this, 0, opts.checkboxIdPrefix);

        return this;
    };
}(jQuery));
