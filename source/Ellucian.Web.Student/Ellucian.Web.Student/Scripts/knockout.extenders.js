﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/

// Extender that evaluates for correct date format uzing Globalize settings
// Properities:
//  hasFormatError - observable is true if the date is not formatted correctly.
//  errorMessage - observable has some error message value if hasFormatError is true
// Options:
//  culture: Optionally specify a culture setting. Default is the current culture
//  dateFormat: Optionally specify the Globalize date format string to use when parsing and formatting. Default is small d - "d"
//  minDate: Optionally specify an inclusive minimum date. If set, the input value cannot be before this date.
//  maxDate: Optionally specify an inclusive maximum date. If set, the input value cannot be after this date.
ko.extenders.dateCheck = function (target, options) {
    target.hasFormatError = ko.observable();
    target.errorMessage = ko.observable();

    var culture = options.culture || Globalize.culture(),
        dateFormat = options.dateFormat || "d",
        minDate = options.minDate,
        maxDate = options.maxDate;

    function success() {
        target.hasFormatError(false);
        target.errorMessage(null);
    }

    function error(message) {
        target.hasFormatError(true);
        target.errorMessage(message);
        $('#aria-announcements').text(message);
    }

    function validateDate(input) {
        var parsedDate = Globalize.parseDate(input, dateFormat, culture)
        if (input === null || input === "") {
            success();
        }
        else if (parsedDate) {
            if (minDate && minDate > parsedDate) {
                error("Please enter a date on or after " + Globalize.format(minDate, dateFormat));
                return;
            }
            if (maxDate && maxDate < parsedDate) {
                error("Please enter a date on or before " + Globalize.format(maxDate, dateFormat));
                return;
            }
            success();
        }
        else {
            error("Please enter a date in the correct format " + Globalize.getClosestCultureDateFormat());
        }
    }

    validateDate(target());
    target.subscribe(validateDate);

    return target;
}

/* 
This was copied from immediate.payments.js
numeric : {
    precision: int, precision value; default is 2
    zeroNull: bool, true - empty value equates with zero; false - empty values are replaced with zero; default is false
    maxValue: decimal, maximum value; default is no maximum
    maxMessage: string, custom error message to display when the max value is exceeded (only used if maxValue is specified)
    minValue: decimal, minimum value; default is no minimum
    minMessage: string, custom error message to display when the min value is exceeded (only used if minValue is specified)
    nanMessage: string, custom error message to display if the user-entered value is not a number
    valCondition: function, bool result of function indicates whether or not to validate the target
    observable: ko.observable, if you have an underlying observable to update with the same value of the target observable
    updateOnError: bool, true - update the target (and observable) even if there is an error; default is false
}
*/
ko.extenders.numeric = function (target, arguments) {
    // Process the arguments passed in
    //var precision = arguments.precision || 2;
    var precision = arguments.precision;
    if (isNaN(precision)) {
        precision = 2;
    }

    // Process the argument to use an empty/null value in place of zero
    var zeroNull = arguments.zeroNull || false;
    var zeroFormat = (zeroNull) ? "" : 0;

    // Get maximum value, if any, and its error message
    var maxValue = arguments.max;
    if (maxValue && isNaN(maxValue)) {
        maxValue = undefined;
    }
    if (maxValue) {
        var maxMessage = arguments.maxMessage || "You must enter a value less than {0}".format(maxValue);
    }

    // Get minimum value, if any, and its error message
    var minValue = arguments.min;
    var minMessage = "";
    if (isNaN(minValue)) {
        minValue = undefined;
    } else {
        minMessage = arguments.minMessage || "You must enter a value greater than or equal to {0}.".format(minValue);
    }

    // Get the "not a number" message and the validation condition
    var nanMessage = arguments.nanMessage || "Please enter a valid number.";
    var valCondition = arguments.valCondition;

    var underlyingObservable = arguments.observable;
    var updateOnError = arguments.updateOnError;

    //create a writeable computed observable to intercept writes to our observable
    var result = ko.computed({
        read: function () {
            return Globalize.format(target(), "n" + precision);
        },
        write: function (newValue) {
            //add some sub-observables to our observable if they don't exist
            if (!result.hasError) {
                result.hasError = ko.observable(false);
            }
            if (!result.validationMessage) {
                result.validationMessage = ko.observable("");
            }

            // Get the current value, and turn the new value into a numeric
            var current = target(),
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = parseANumber(newValue);
            // Get the current error and message values and initialize new values
            var hasError = result.hasError(),
                newError = false;
            var valMessage = result.validationMessage(),
                newMessage = "";

            // Only validate when a new value is available (i.e. not null) and the validation condition is true, if one was specified
            if (newValue && valCondition && typeof valCondition === "function" && valCondition()) {
                // Validate the data
                if (isNaN(newValueAsNum)) {
                    // Not a number
                    newError = true;
                    newMessage = nanMessage;
                } else if (minValue != "undefined" && newValueAsNum < minValue) {
                    // Value is less than the minimum
                    newError = true;
                    newMessage = minMessage;
                } else if (maxValue && newValueAsNum > maxValue) {
                    // Value is greater than the maximum
                    newError = true;
                    newMessage = maxMessage;
                }
            }

            if (updateOnError || !newError) {

                // Make sure we have a number before determining the value to write out
                if (isNaN(newValueAsNum)) {
                    newValueAsNum = 0;
                }
                var valueToWrite = (newValueAsNum === 0) ? zeroFormat : Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;

                // Only write the new value if it changed
                if (valueToWrite !== current) {
                    target(valueToWrite);
                    if (underlyingObservable && ko.isObservable(underlyingObservable))
                        underlyingObservable(valueToWrite);
                } else {
                    // If the rounded value is the same, but a different value was written, force a notification for the current field
                    if (newValueAsNum !== current) {
                        target.notifySubscribers(valueToWrite);
                        if (underlyingObservable && ko.isObservable(underlyingObservable))
                            underlyingObservable.notifySubscribers(valueToWrite);
                    }
                }
            }
            // If the error or message values have changed, save the new values. Because these are part of
            // a dependent (computed) observable, we have to notify the subscribers manually.
            if (newError !== hasError) {
                result.hasError(newError);
                result.hasError.notifySubscribers(newError);
            }
            if (newMessage !== valMessage) {
                result.validationMessage(newMessage);
                result.validationMessage.notifySubscribers(newMessage);
            }
        }
    });

    // Initialize the computed observable with the current value to make sure it is rounded appropriately
    result(target());
    // Return the new computed observable
    return result;
};



/*This is a simpler implementation of the numeric extender. It only validates that the input is a number 
  and optionally checks against a specified min and max value
    Properities:
        hasFormatError - observable is true if the date is not formatted correctly.
        errorMessage - observable has some error message value if hasFormatError is true
    Options:
        
        minDate: Optionally specify an inclusive minimum date. If set, the input value cannot be before this date.
        maxDate: Optionally specify an inclusive maximum date. If set, the input value cannot be after this date.
        invalidMessage: Optionally specify an errorMessage if the input is not a number. Default is "Please enter a valid value"
*/
ko.extenders.simpleNumericValidation = function (target, options) {
    target.hasError = ko.observable();
    target.errorMessage = ko.observable();

    var minValue = options.minValue,
        maxValue = options.maxValue,
        invalidMessage = options.invalidMessage || "Please enter a valid value";


    function success() {
        target.hasError(false);
        target.errorMessage(null);
    }
    function fail(message) {
        target.hasError(true);
        target.errorMessage(message);
        $('#aria-announcements').text(message);
    }

    function validateNumber(input) {
        if (!input || input === "") {
            success();
        }
        else {
            var parsedValue = Globalize.parseFloat(input);
            if (!isNaN(parsedValue)) {
                if (ko.utils.unwrapObservable(minValue) && parsedValue < ko.utils.unwrapObservable(minValue)) {
                    fail("Enter a value greater than or equal to {0}".format(ko.utils.unwrapObservable(minValue)))
                }
                if (ko.utils.unwrapObservable(maxValue) && parsedValue > ko.utils.unwrapObservable(maxValue)) {
                    fail("Enter a value less than or equal to {0}".format(ko.utils.unwrapObservable(maxValue)));
                }
                success();
            }
            else {
                fail(ko.utils.unwrapObservable(invalidMessage));
            }
        }
    }
    validateNumber(target());
    target.subscribe(validateNumber);

    return target;

};

/* The formattedPhone extender attempts to keep phone number inputs formatted consistently.
   The phone number will be stripped of characters not consisting of digits 0-9 or +-()/. or space
   If the number starts with a +, no further formatting will be applied.
   Otherwise if the number consists 7 or 10 digits, disregarding all other characters, it will be converted
   to US style formatting XXX-XXXX or XXX-XXX-XXXX
   If the number consists of some other number of digits, formatting will be preserved.

   Options:
     validateLength: If true, adds a knockout validation rule to ensure the number contains between 7 and 15
                     numerical digits, not including formatting. Also validates with 0 digits. If the number
                     is required to be entered, use a required validation to achieve this.
     validationMessage: Optional string that overrides the default validation error message
 */
ko.extenders.formattedPhone = function (target, options) {
    var applyPhoneFormatting = function (value) {
        var nonDigitsRegex = RegExp("\\D", "g"); // Matches any non digit (0-9)
        var internationalPhoneNumber = RegExp("^\\+"); // Starts with a plus (+)
        // Valid phone characters: 0-9 or the phone format characters of + - . ( ) / and space
        var invalidPhoneCharactersRegex = RegExp("[^\\d+-\\.()/ ]", "g");
        var phoneDigits = value.replace(nonDigitsRegex, '');
        var formattedPhoneNumber = "";
        if (value.match(internationalPhoneNumber)) {
            // If the number starts with a +, don't attempt to format it US style, even if it is 7 or 10 digits
            formattedPhoneNumber = value.replace(invalidPhoneCharactersRegex, '');
        } else if (phoneDigits.length === 7) {
            // Apply US style phone number formatting XXX-XXXX, keeping only digits from original
            formattedPhoneNumber = [phoneDigits.slice(0, 3), '-', phoneDigits.slice(3)].join('');
        } else if (phoneDigits.length === 10) {
            // Apply US style phone number formatting XXX-XXX-XXXX, keeping only digits from original
            formattedPhoneNumber = [phoneDigits.slice(0, 3), '-', phoneDigits.slice(3, 6), '-', phoneDigits.slice(6)].join('');
        } else {
            // The number did not contain 7 or 10 digits, preserve formatting while removing invalid characters
            formattedPhoneNumber = value.replace(invalidPhoneCharactersRegex, '');
        }
        return formattedPhoneNumber;
    }

    if (options.validateLength) {
        var validationMessage = options.validateMessage || phoneLengthErrorMessage;
        target.extend({
            validation: {
                validator: function (value) {
                    var nonDigitsRegex = RegExp("\\D", "g");
                    var phoneDigits = value.replace(nonDigitsRegex, '');
                    if (phoneDigits.length === 0 || (phoneDigits.length >= 7 && phoneDigits.length <= 15)) {
                        return true;
                    } else {
                        return false;
                    }
                },
                message: validationMessage
            }
        });
    }

    target.subscribe(function (newValue) {
        var formattedValue = applyPhoneFormatting(newValue);
        if (formattedValue !== newValue) {
            target(formattedValue);
        }
    });
};