﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

var BaseViewModel = function (mobileScreenWidth) {

    var self = this;

    this.mobileScreenWidth = mobileScreenWidth || 480;

    this.isMobile = ko.observable(false);

    // Check the window, if it is mobile-sized, render appropriately.
    this.checkForMobile = function (win, doc) {
        // get the appropriate viewport width, so that JS and media queries trigger at same time
        // viewport script snippit from http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
        var windowValue = win, innerValue = 'inner';

        // determine whether to calculaute viewport using window.innerWidth or document.clientWidth (depends on browser)
        if (!('innerWidth' in win)) {
            innerValue = 'client';
            windowValue = doc.documentElement || doc.body;
        }

        // calculate viewport width
        var viewport = { width: windowValue[innerValue + 'Width'] };

        //checks viewport size on page load and adjusts page appropriately (copied/pasted from resize function)
        if (viewport["width"] <= self.mobileScreenWidth) {
            self.isMobile(true);
        }
        else {
            self.isMobile(false);
        }
    }

    // subscribe to isMobile to drive DOM changes
    this.isMobile.subscribe(function (newValue) {
        if (newValue === true) {
            self.changeToMobile();
        } else {
            self.changeToDesktop();
        }
    });

    this.changeToMobile = function () {
        // This method should be overridden in any class that uses BaseViewModel
    };

    this.changeToDesktop = function () {
        // This method should be overridden in any class that uses BaseViewModel
    };

    // Action throbber observables - used when loading proxy selection dialog
    self.loadingMessage = ko.observable(personProxyLoadingThrobberMessage);

    // Flag indicating whether or not the selection dialog is displayed
    self.proxySelectionDialogIsLoading = ko.observable(false);

    // Flag indicating whether or not the selection dialog is displayed
    self.proxySelectionDialogIsDisplayed = ko.observable(false);

    // List of available proxy subjects
    this.ProxySubjects = ko.observableArray();

    // Observable to keep track of the proxy subject that the user selects
    self.selectedProxySubject = ko.observable(null);

    // Function to process the Change User button to change the proxy subject
    self.changeProxySubject = function () {
        hideProxyDialog = false;
        self.getProxySubjects();
    }

    // Updates the view model with proxy subject data
    self.updateViewModelWithProxySubjectData = function () {
        self.proxySelectionDialogIsLoading(false);
        self.proxySelectionDialogIsDisplayed(!hideProxyDialog && self.ProxySubjects().length > 1);
        showOrHideElement(self.ProxySubjects(), $("#changeProxyUser-link"));
        showOrHideElement(self.ProxySubjects(), $("#changeProxyUser-opsLink"))
        if (self.proxySelectionDialogIsDisplayed()) {
            setFocus("input.buttonless");
        } else {
            setFocus("#topOfForm");
        }
    }

    // Call to retrieve the user's available proxy subjects and build the proxy dialog JS view model
    this.getProxySubjects = function () {
        var proxySubjects = Ellucian.Storage.session.getItem("Ellucian.proxySubjects")
        if (!proxySubjects) {
            $.ajax({
                url: getProxySubjectsActionUrl,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {
                    self.proxySelectionDialogIsLoading(true);
                },
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        Ellucian.Storage.session.setItem("Ellucian.proxySubjects", data.ProxySubjects)
                        ko.mapping.fromJS(data, {}, self);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: proxySelectionUnableToLoadSubjectsMessage, type: "error" });
                    }
                },
                complete: function () {
                    self.updateViewModelWithProxySubjectData();
                }
            });
        } else {
            ko.mapping.fromJS(proxySubjects, {}, self.ProxySubjects);
            self.updateViewModelWithProxySubjectData();
        }
    }

    // Function to close the proxy subject modal dialog
    self.cancelProxySelectionDialog = function () {
        self.proxySelectionDialogIsDisplayed(false);
        self.selectedProxySubject(null);
    }

    // Action throbber observables - used when loading proxy selection dialog
    self.changingUserMessage = ko.observable(personProxyChangingThrobberMessage);

    // Flag indicating whether or not the selection dialog is displayed
    self.changingProxyUsers = ko.observable(false);

    // Function to process the user's proxy subject selection
    self.saveProxySelectionDialog = function () {

        // If the user selects not to proxy anyone and they just logged in, simply close the dialog
        var json = { subjectJson: ko.toJSON(self.selectedProxySubject()) };

        var url = homeUrl;
        if (self.selectedProxySubject().Id() == currentUserId) {
            url += "?hideProxyDialog=true";
        }

        $.ajax({
            url: setProxySubjectActionUrl,
            type: "PUT",
            contentType: jsonContentType,
            data: JSON.stringify(json),
            dataType: "json",
            beforeSend: function (data) {
                self.changingUserMessage(personProxyChangingThrobberMessage.format(self.selectedProxySubject().FullName()));
                self.changingProxyUsers(true);
            },
            complete: function (data) {
                window.location.href = url;
            }
        });
    }

    // Observable to determine whether or not the submit button is enabled on the proxy selection dialog
    self.saveProxySelectionEnabled = ko.computed(function () {
        return self.selectedProxySubject();
    });

    // Announce the user's proxy subject selection
    self.selectedProxySubject.subscribe(function () {
        if (self.selectedProxySubject()) {
            makeAnnouncement(proxySelectionUserSelectedMessage.format(self.selectedProxySubject().FullName()));
        }
    });

    // Action throbber observables - used when verifying user's password
    self.verifyingPasswordMessage = ko.observable(verifyingPasswordMessage);
    self.verifyingPasswordAltText = ko.observable(verifyingPasswordAltText);

    // Flag indicating whether or not the user's password is being verified
    self.verifyingPassword = ko.observable(false);

    // Observable to store user's password for verification
    self.userPassword = ko.observable();

    // Observable to store password verification message
    self.verifyPasswordMessage = ko.observable();

    // Flag indicating whether or not the Verify Password dialog is displayed
    self.verifyPasswordDialogIsDisplayed = ko.observable(false);

    // Function to display verify password dialog
    self.showVerifyPasswordDialog = function () {
        self.verifyPasswordDialogIsDisplayed(true);
    }

    // Function to close the Verify Password modal dialog
    self.cancelVerifyPasswordDialog = function () {
        self.verifyPasswordDialogIsDisplayed(false);
        self.verifyPasswordMessage(null);
        self.userPassword(null);
    }

    // Flag indicating whether or not the submit verify password button is enabled
    self.verifyPasswordButtonEnabled = ko.computed(function () {
        return self.userPassword() && self.userPassword() !== "";
    })

    // Function to process the user's password verification
    self.submitVerifyPasswordDialog = function () {
        var json = { passwordJson: ko.toJSON(self.userPassword()) };
        $.ajax({
            url: reauthenticateUrl,
            type: "POST",
            contentType: jsonContentType,
            data: JSON.stringify(json),
            dataType: "json",
            beforeSend: function (data) {
                self.verifyPasswordDialogIsDisplayed(false);
                self.verifyingPassword(true);
            },
            success: function (data) {
                self.verifyingPassword(false);
                self.verifyingPasswordSuccessFunction();
                self.verifyPasswordMessage(null);
                self.userPassword(null);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0) {
                    self.verifyPasswordMessage(JSON.parse(jqXHR.responseText));
                }
                self.verifyingPassword(false);
                self.verifyPasswordDialogIsDisplayed(true);
            }
        });
    }

    // Function called when password verification is successful
    self.verifyingPasswordSuccessFunction = function () { };

    self.saveProxySelectionButton = {
        id: "save-proxy-selection-dialog-button",
        isPrimary: true,
        title: Ellucian.Proxy.ButtonLabels.saveProxySelectionDialogButtonLabel,
        callback: self.saveProxySelectionDialog,
        enabled: self.saveProxySelectionEnabled
    };

    self.cancelProxySelectionButton = {
        id: "cancel-proxy-selection-dialog-button",
        isPrimary: false,
        title: Ellucian.Proxy.ButtonLabels.cancelProxySelectionDialogButtonLabel,
        callback: self.cancelProxySelectionDialog
    };

    self.verifyPasswordDialogButton = {
        id: "verify-password-dialog-button",
        isPrimary: true,
        title: Ellucian.SharedComponents.ButtonLabels.buttonTextVerifyPassword,
        callback: self.submitVerifyPasswordDialog,
        enabled: self.verifyPasswordButtonEnabled
    };

    self.cancelPasswordDialogButton = {
        id: "cancel-dialog-button",
        title: Ellucian.SharedComponents.ButtonLabels.buttonTextCancel,
        callback: self.cancelVerifyPasswordDialog
    };


    $(document).ready(function () {

        // On page resize, check if we need to adapt the page for mobile.
        // thottleResize is used with a timeout to prevent repeated calls to checkForMobile in a single resize
        var throttleResize = 0;
        $(window).resize(function () {
            clearTimeout(throttleResize);
            throttleResize = setTimeout(function () { self.checkForMobile(window, document) }, 100);
        });
    });
}