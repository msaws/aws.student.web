﻿/* Copyright 2012-2017 Ellucian Company L.P. and its affiliates. */

/* site.js 2012 Ellucian
*  
* LOAD LAST!!
*/

// base site namespace and functions
(function (site, $, undefined) {

    // **************
    // public members
    // **************

    // Init site - call into other init funcitons as well.
    site.initialize = function () {
        site.toolbar.initialize();
    };

}(window.site = window.site || {}, jQuery));


// toolbar namespace and functions
(function (toolbar, $, undefined) {
    // **************
    // public members
    // **************

    // message shown when no help is found for the help URL.
    toolbar.helpNotAvailableMessage = "";

    // Init the toolbar
    toolbar.initialize = function () {
        // Only enable toggling of the user options if there are items to display
        // Proxy users acting on behalf of another user currently will have no items in the options menu
        if ($("#userOptions").has("li").length > 0) {
            $("#userName").toggle(showUserOptions, hideUserOptions);
        }
        if ($("#logOff").length > 0) {
            $("#logOff").click(function () {

                // Clear storage to ensure nobody is accessing data from another user on a shared browser
                // Also done during log-in, but do here as well just to be double-sure
                Ellucian.Storage.session.clear();

                window.location.href = logOffUrl
            });
        }

        // Attempt to get proxy information to show change proxy option if toolbar menus are available
        if ($("#userOptions").length > 0 || $("#logOff").length > 0) {

            var proxySubjects = Ellucian.Storage.session.getItem("Ellucian.proxySubjects");
            if (proxySubjects) {
            if ($("#proxy-banner").length <= 0) {
                    var data = { ProxySubjects: proxySubjects };
                    if (data.ProxySubjects && data.ProxySubjects.length > 1) {
                        toolbar.setMenuBehavior(data);
                    }
                }
            } else {
                $.ajax({
                    url: getProxySubjectsActionUrl,
                    type: "GET",
                    contentType: jsonContentType,
                    dataType: "json",
                    success: function (data) {
                        if (!account.handleInvalidSessionResponse(data)) {
                            if (data.ProxySubjects) {
                                Ellucian.Storage.session.setItem("Ellucian.proxySubjects", data.ProxySubjects);
                                if (data.ProxySubjects.length > 1) {
                                    toolbar.setMenuBehavior(data);
                                }
                            }
                        }
                    }
                });
            }
        }

        $("#logIn").click(function () {
            window.location.href = logInUrl
        });


        $("#help").toggle(showHelp, hideHelp);
        // notifications takes care of itself, but we need to know when the control
        // open's itself and simulate a click to ensure the other tabs are closed
        $("#siteToolbar").bind("notifications_open", function (e) {
            closeOpenTabs("javascript:void(0)");
        });

        $(document).mouseup(function (e) {
            closeOpenTabs(e);
        });
    };

    // Function to drive menu behavior based on underlying data
    toolbar.setMenuBehavior = function (data) {
        $("#logOff").unbind("click");
        $("#logOff").toggle(showLogOffOptions, hideLogOffOptions);
        showOrHideElement(data.ProxySubjects, $("#changeProxyUser-link"));
        showOrHideElement(data.ProxySubjects, $("#changeProxyUser-opsLink"));
    }

    // Fetches the help content for the help tab.
    toolbar.addHelpContent = function (helpUrl) {
        var formattedHelpMessage = "<h2>" + toolbar.helpNotAvailableMessage + "</h2>";

        // Fails if helpUrl is null, empty string, or undefined
        if (helpUrl) {
            $("#help").one("click", function () {
                var jqxhr = $.get(helpUrl, function (data) {

                    if (data.length > 0) {
                        $("#help-body").html(data);
                    }
                    else {
                        $("#help-body").html(formattedHelpMessage);
                    }

                    $(".help-content").accordion(
                    {
                        collapsible: true,
                        active: false,
                        autoHeight: false,
                        clearStyle: true
                    }
                );

                    // Loop through and add unique IDs to each panel and aria-controls to each header
                    var panelId = 0;
                    $(".help-content .ui-accordion-header").each(function (panelId) {
                        $(this).next().attr('id', 'help-panel-' + panelId);
                        $(this).attr('aria-controls', 'help-panel-' + panelId);
                        panelId++;
                    });
                    

                    // Hide the help regions initially
                    $(".more-help").hide();

                    $('<a href="#" class="more-help-link">Click Here For More Information</a>').insertBefore(".more-help");

                    $(".more-help-link").one("click", function () {
                        $(this).hide();
                        $(this).next("div").show();
                        $(this).next("div").focus();
                    });
                }).error(function () { $("#help-body").html(formattedHelpMessage); });
            });
        } else {
            $("#help-body").html(formattedHelpMessage);
        }
    }

    // ***************
    // private members
    // ***************

    // Used to manage the tabs in the site toolbar (user options, help, notifications). Note, notifications is not, in this list
    // because it is a custom jQuery UI plugin and takes care of itself.
    //
    // clickTarget = jQuery selector that represents the element a user clicks on (used to determine if the tab was clicked and simulate clicks from the script)
    // childTargets = jQuery selectors that represent child elements of the tab (e.g. if the mouse click is on these, it is ignored)
    // visible = flag to track tab visibility (always init to false)
    var tabInformation =
        // user options menu
        [{
            clickTarget: "#userName",
            childTargets: ["#userName", "#userOptions"],
            visible: false
        },
        // Sign out menu
        {
            clickTarget: "#logOff",
            childTargets: ["#logOff", "#logOffOptions"],
            visible: false
        },
        // help menu
        {
            clickTarget: "#help",
            childTargets: ["#help", "#help-region"],
            visible: false
        }];

    // helper functions to show/hide the user options menu
    function showUserOptions() {
        $("#userName").addClass("esg-is-open");
        $("#userName").attr("aria-expanded", true);
        tabInformation[0].visible = true;
        return false;
    }
    function hideUserOptions() {
        tabInformation[0].visible = false;
        $("#userName").focus();
        $("#userName").removeClass("esg-is-open");
        $("#userName").attr("aria-expanded", false);
        return false;
    }

    // helper functions to show/hide the log off menu
    function showLogOffOptions() {
        $("#logOff").addClass("esg-is-open");
        $("#logOff").attr("aria-expanded", true);
        tabInformation[1].visible = true;
        return false;
    }
    function hideLogOffOptions() {
        tabInformation[1].visible = false;
        $("#logOff").focus();
        $("#logOff").removeClass("esg-is-open");
        $("#logOff").attr("aria-expanded", false);
        return false;
    }

    // helper functions to show/hide the help menu
    function showHelp() {
        $("#help").addClass("esg-is-open");
        $("#help").attr("aria-expanded", true);
        tabInformation[2].visible = true;
        return false;
    }
    function hideHelp() {
        tabInformation[2].visible = false;
        $("#help").focus();        
        $("#help").removeClass("esg-is-open");
        $("#help").attr("aria-expanded", false);
        return false;
    }

    // Tab management function that closes open tabs when a mouse click occurs.
    //
    // If a click is on another tab, then this will close any other open tabs so only one is open.
    // If a click is outside a tab (e.g. somewhere on the page) then this will close any open tabs.
    // This function has to handle the notifications center as a special case, as it can only be closed
    // by a tab click (either another tab or itself), so the only time this will close the notifications
    // tab is when the click was on another tab.
    function closeOpenTabs(clickEvent) {
        // try to close all open tabs in the tabInformation list
        $.each(tabInformation, function (i, tab) {
            var close = true;
            // if the click is within the tab itself, do not close (indicated by the 
            $.each(tab.childTargets, function (j, target) {
                if ($(target).is(clickEvent.target) || $(target).has(clickEvent.target).length > 0) {
                    close = false;
                }
            });

            if (close && tab.visible) {
                $(tab.clickTarget).click();
            }
        });

        var isTabClick = false;
        // check to see if this is a 'tab' click
        $.each(tabInformation, function (i, tab) {
            if ($(tab.clickTarget).is(clickEvent.target) || $(tab.clickTarget).has(clickEvent.target).length > 0) {
                isTabClick = true;
            }
        });

        // close notifications if the click was on another tab
        if (isTabClick) {
            $("#notificationHost").notificationCenter('hide');
        }
    }

}(window.site.toolbar = window.site.toolbar || {}, jQuery));


(function (window, $) {

    openUrl = function (url) {
        if (url) {
            window.location.href = url;
        } else {
            return;
        }
    }

    // Size of maximum width of single column media query
    var smallScreen = 769;

    // checks window size on page load and adjusts page appropriately (copied/pasted from resize function)
    var adjustSingleColumnClasses = function () {
        if ($(window).width() < smallScreen) {
            $("body").addClass("small-screen");
        } else {
            $("body").removeClass("small-screen");
        }
    }

    $(window).resize(function () {
        // add/remove classes everytime the window resize event fires
        adjustSingleColumnClasses();
        if ($(window).width() >= smallScreen) {
            $('#new-filter-dropdown').hide();
        }
    });

    $(document).ready(function ($) {
        // New drawer + accordion navigation
        navMenu = $('ul#nav-root').menuAccordion();

        var openNavigation = function () {
            $('html').addClass('active-nav');
            if (navigationMenuCloseText) {
                $('#toggle-nav-button').attr('aria-label', navigationMenuCloseText);
            }
        }

        var closeNavigation = function () {
            $('html').removeClass('active-nav');
            if (navigationMenuOpenText) {
                $('#toggle-nav-button').attr('aria-label', navigationMenuOpenText);
            }
            $('ul#nav-root input[type=checkbox][data-menu-level]').each(function (e) {
                $(this).prop('checked', false).trigger('change');
            })
        }

        $(document).keydown(function (e) {
            if (e.keyCode === 27) {
                closeNavigation();
            }
        });

        $(".nav-overlay").on('click', function (event) {
            closeNavigation();
        });

        $("#nav-root > li:not(.supress-expand)").on('click', function (event) {
            openNavigation();
        });

        $("#toggle-nav-button").on('click', function (event) {
            if ($('html').hasClass('active-nav')) {
                closeNavigation();
            } else {
                openNavigation();
                event.stopPropagation();
            }
        });
    });

    window.activateFilter = function () {
        $('#new-filter-dropdown').toggle();
    }
})(window, jQuery);

//debounce function used for the window scroll event to improve performance
function debounce(callback, timeout) {
    var timeoutInstance;
    return function () {
        //investigate clearTimeout
        clearTimeout(timeoutInstance);
        timeoutInstance = ko.utils.setTimeout(callback, timeout);
    };
}
