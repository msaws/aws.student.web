﻿function ExpandAllAccordions() {
    $(".expand-collapse-all").text("Collapse All");
    $(".accordion-region, .accordion-region-opened").each(function () {
        var activeAccordion = $(this).accordion('option', 'active');
        // Collapse accordions have activeAccordion == 'false'
        if (activeAccordion !== 0) {
            $(this).accordion('activate', 0);
        }
    });
    $(this).focus();
}

function CollapseAllAccordions() {
    $(".expand-collapse-all").text("Expand All");
    $(".accordion-region, .accordion-region-opened").each(function () {
        var activeAccordion = $(this).accordion('option', 'active');
        // Expanded accordions have activeAccordion == '0'
        if (activeAccordion === 0) {
            $(this).accordion('activate', 0);
        }
    });
    $(this).focus();
}

$(document).ready(function () {
    if ($(".accordion-region").length > 0) {
        $(".accordion-region").accordion(
            {
                active: false,
                collapsible: true,
                autoHeight: false,
                clearStyle: true,
                icons: { 'header': 'ui-icon-carat-1-e', 'headerSelected': 'ui-icon-carat-1-s' }
            }
        );
    }
    if ($(".accordion-region-opened").length > 0) {
        $(".accordion-region-opened").accordion(
            {
                collapsible: true,
                autoHeight: false,
                clearStyle: true,
                icons: { 'header': 'ui-icon-carat-1-e', 'headerSelected': 'ui-icon-carat-1-s' }
            }
        );
    }
    if ($(".accordion-fixed").length > 0) {
        $(".accordion-fixed").accordion(
            {
                disabled: true,
                active: 0,
                collapsible: false,
                autoHeight: false,
                clearStyle: true,
                icons: false
            }
        );
    }

    if ($(".accordion-region-opened").length > 0 && $(".accordion-region").length === 0) {
        ExpandAllAccordions();
        $(".expand-collapse-all").toggle(CollapseAllAccordions, ExpandAllAccordions);
    } else {
        $(".expand-collapse-all").toggle(ExpandAllAccordions, CollapseAllAccordions);
    }
});