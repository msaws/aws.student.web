﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
var vm = new LinkViewModel();

$(document).ready(function () {
    var viewModel = ko.applyBindings(vm, document.getElementById("main"));

    // If a link deletion fails, the server should redirect back to this page, with a message.
    if (errorMessage.length > 0) {
        $('#notificationHost').notificationCenter('addNotification', { message: errorMessage, type: "error" });
    }
});