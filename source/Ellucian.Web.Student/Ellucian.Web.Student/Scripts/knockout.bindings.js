﻿/// <reference path="admin.settings.js" />
// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Provides KnockOut binding definitions for custom bindings that are specific to the Self-Service.
// This includes bindings for the calendar, accordions, tabs, etc.
///////////////////////////////////////////////////////////////////////////////////////////////////////////

// Add a subscribed function that will provide access to the previous value AND the new value
ko.subscribable.fn.subscribeChanged = function (callback) {
    var previousValue;
    var previousSubscription = this.subscribe(function (_previousValue) {
        previousValue = _previousValue;
    }, undefined, 'beforeChange');
    var latestSubscription = this.subscribe(function (latestValue) {
        callback(previousValue, latestValue);
    });

    return {
        previousSubscription: previousSubscription,
        latestSubscription: latestSubscription,
        dispose: function () {
            previousSubscription.dispose();
            latestSubscription.dispose();
        }
    }
};

// custom binding for a jQuery slide effect
ko.bindingHandlers.slide = {
    update: function (element, valueAccessor, allBindings) {
        // First get the latest data that we're bound to
        var value = valueAccessor();

        // Next, whether or not the supplied model property is observable, get its current value
        var valueUnwrapped = ko.utils.unwrapObservable(value);
        // Grab some more data from another binding property
        var duration = allBindings().duration || 400; // 400ms is default duration unless otherwise specified

        // Now manipulate the DOM element
        if (valueUnwrapped == true)
            $(element).slideDown(duration, function () { $(element).css('overflow', ''); }); // Make the element visible
        else
            $(element).slideUp(duration);   // Make the element invisible
    }
};

//custom binding to initialize a jQuery UI accordion
ko.bindingHandlers.jqAccordion = {
    init: function (element, valueAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor()) || {};

        //handle disposal
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).accordion("destroy");
        });

        $(element).accordion(options);
    }
};

// custom binding to initialize a jQuery tab widget
ko.bindingHandlers.jqTabs = {
    update: function (element, valueAccessor, allBindingsAccessor) {

        // If the tabs are to be destroyed, do it and skip the rest.
        if (ko.utils.unwrapObservable(allBindingsAccessor().jqTabsDestroy) === true) {
            $(element).tabs("destroy");
        } else {
            var config = ko.utils.unwrapObservable(valueAccessor());

            //make sure that elements are set from template before calling tabs API
            setTimeout(function () {
                $(element).tabs("destroy").tabs(config);

                // Tabs may be hidden by default to prevent flicker; show them after jQuery builds them out
                $(element).show();
                $(element).css('visibility', 'visible');
            }, 0);
        }
    }
};

// Custom binding to insert photos, with default fallback on error
ko.bindingHandlers.photo = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        element.onerror = function () {
            element.src = defaultPhotoUrl;
        };
        element.src = ko.unwrap(valueAccessor());
    }
};

// Custom binding that changes any html element to span
ko.bindingHandlers.readOnlySpan = {
    update: function (element, valueAccessor) {
        var values = ko.utils.unwrapObservable(valueAccessor()) || {};
        var showSpan = values.showSpan;
        var message = values.message || '';
        var styles = values.styles || '';
        var id = values.id || $(element).attr('id');
        if (showSpan != null && showSpan === true) {
            $(element).replaceWith("<span class='" + styles + "' id='" + id + "'></span>");
            $("#" + id).html(message);
        }
    }
}

/*
 * Adds a readonly attribute to <input /> elements. readonly is a Boolean attribute that 
 * indicates that the user cannot modify the value of the control. 
 * HTML5 ignors readonly if the value of the input type attribute is hidden, range, color, checkbox, radio, file, or a button type (such as button or submit).
 * readonly is not a valid attribute of <select> element.
 * *****
 * Example:
 * 
 * JS:
 * viewModel : {
 *  isReadOnly: ko.observable(true),
 *  isNotReadOnly: false
 * }
 * 
 * Markup and result:
 * <input type='text' data-bind='readOnlyInput: isReadOnly' readonly='readonly'/>
 * <input type='text' data-bind='readOnlyInput: isNotReadOnly' />

 */
ko.bindingHandlers.readOnlyInput = {
    update: function (element, valueAccessor) {
        var show = ko.utils.unwrapObservable(valueAccessor());
        if (show) {
            $(element).attr('readonly', 'readonly');
        } else {
            $(element).removeAttr('readonly');
        }
    }
}

//custom binding to initialize a jQuery UI multi-progressbar
ko.bindingHandlers.jqProgressBar = {
    init: function (element, valueAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor()) || {};

        //handle disposal
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiprogressbar("destroy");
        });

        $(element).multiprogressbar(options);
    }
};

// Custom binding for a jQuery UI Dialog
ko.bindingHandlers.jqDialog = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor());
        options.close = function (event, ui) { allBindingsAccessor().dialogOpen(false); };

        // Use a timeout to prevent the binding parser from hitting the dialog twice (jquery moves the DOM elements around when it builds the dialog)
        setTimeout(function () { $(element).dialog(options || {}); }, 0);

        //handle disposal (not strictly necessary in this scenario)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).dialog("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var shouldBeOpen = ko.utils.unwrapObservable(allBindingsAccessor().dialogOpen);
        try {
            $(element).dialog(shouldBeOpen ? "open" : "close");
        } catch (error) { }
    }
};

// Custom binding for forcing Knockout to skip certain child elements when doing recursive bindings.
// This can be helpful if certain data will not be available until after the Ready event and you need
// to leave the markup with bindings in place (especially nested bindings).
ko.bindingHandlers.skipBindings = {
    init: function () {
        return { controlsDescendantBindings: true };
    }
};
ko.virtualElements.allowedBindings.skipBindings = true;

// Utility method to insert an array into an observableArray without having to insert one-by-one (and thus cause a chain of updates)
ko.observableArray.fn.pushAll = function (valuesToPush) {
    var underlyingArray = this();
    this.valueWillMutate();
    ko.utils.arrayPushAll(underlyingArray, valuesToPush);
    this.valueHasMutated();
    return this;
};

// Utility method to replace the values in an observableArray with all the values in the given array without having to insert one-by-one (and thus cause a chain of updates)
ko.observableArray.fn.replaceAll = function (valuesToReplace) {
    var underlyingArray = this();
    this.valueWillMutate();
    this.removeAll();
    ko.utils.arrayPushAll(underlyingArray, valuesToReplace);
    this.valueHasMutated();
    return this;
}

// Knockout binding to place a watermark/placeholder in a text field
// Relies upon the jquery.watermark.js file
ko.bindingHandlers.watermark = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var allBindings = allBindingsAccessor();
        var text, options, value;
        var watermark = allBindings.watermark;

        if (typeof watermark == "string")
        { text = watermark; }
        else {
            text = watermark.text;
            options = watermark.options;
            value = ko.utils.unwrapObservable(watermark.value);
            if (value != null && value.length > 0) {
                text = value;
                //did a search, no one else uses watermark with the value binding
                //$(element).val(value);
            }
        }
        $(element).watermark(text, options);
    }
};

// Knockout binding for a date field
ko.bindingHandlers.date = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var underlyingObservable = valueAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(underlyingObservable);
        var format = allBindingsAccessor().format || "d";

        // The jQuery date formatter uses a few different tokens than .NET, so
        // change certain formats to their jQuery equivalents
        var jqFormat = "";
        switch (format) {
            case "m":
                jqFormat = "M";
                break;
            case "s":
                jqFormat = "S";
                break;
            case "y":
                jqFormat = "Y";
                break;
            default:
                jqFormat = format;
                break;
        }

        var strDate = "";
        if (valueUnwrapped) {
            try {
                var date = valueUnwrapped;

                // If date is not a Date object, attempt to parse a Date object from its value
                if (typeof date.getMonth !== 'function') {
                    date = parseIsoDate(valueUnwrapped);
                }
                if (date) {
                    // Globalize has no "g" or "G" option, so handle those now
                    switch (jqFormat) {
                        case "g":
                            // Short date, short time
                            strDate = Globalize.format(date, "d") + " " + Globalize.format(date, "t");
                            break;
                        case "G":
                            // Short date, long time
                            strDate = Globalize.format(date, "d") + " " + Globalize.format(date, "T");
                            break;
                        default:
                            strDate = Globalize.format(date, jqFormat);
                            break;
                    }
                }
            } catch (error) { }
        }
        if ($(element).is("input")) {
            $(element).val(strDate);
        } else {
            $(element).text(strDate);
        }
    }
};

// Knockout binding for a currency field
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will take a numeric value and display it in currency format. 
// ===================================================================================================
// Options
// ===================================================================================================
// - precision: (optional) Number of decimal places to display for the formatted number
//      default: 2 (use two decimal places, i.e. $1.00)
// - negativePattern: (optional) Negative currency pattern for the formatted number
//      default: 0 (use parentheses for negative numbers, i.e. ($1.00) )
//      valid values: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 
//          (see negativeCurrencyPatterns in global.js to see which pattern corresponds to each value)
// ===================================================================================================
// Mark-Up Examples (2)
// ===================================================================================================
//  <!-- This will place a number formatted as currency inside the span with 2 decimal places (default)
//      and negative pattern 0 (default) -->
// 
//      <span data-bind="currency: 5"></span>
//      <span data-bind="currency: -5"></span>
//
//      $5.00
//      ($5.00)
//
//  <!-- This will place a number formatted as currency inside the span with no decimals
//      and negative currency pattern 1 (leading minus sign) -->
// 
//      <span data-bind="currency: 5, precision: 0, negativePattern: 1"></span>
//      <span data-bind="currency: -5, precision: 0, negativePattern: 1"></span>
//      
//      $5
//      -$5
// ===================================================================================================
ko.bindingHandlers.currency = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = valueAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);
        //try to parse the value - it may be a formatted number such as "10,000.00"
        if (valueUnwrapped && isNaN(valueUnwrapped)) valueUnwrapped = Globalize.parseFloat(valueUnwrapped);
        var precision = allBindingsAccessor().precision;
        if (isNaN(precision)) {
            precision = 2;
        }
        var negativePattern = parseInt(allBindingsAccessor().negativePattern);
        var cultureSelector = Globalize.findClosestCulture();
        if (negativePattern && !isNaN(negativePattern) && negativePattern <= negativeCurrencyPatterns.length) {
            cultureSelector.numberFormat.currency.pattern[0] = negativeCurrencyPatterns[negativePattern]
        } else {
            cultureSelector.numberFormat.currency.pattern[0] = negativeCurrencyPatterns[0];
        }

        var amount = "";
        try {
            if (valueUnwrapped !== undefined && valueUnwrapped !== null && !isNaN(valueUnwrapped)) {
                amount = parseFloat(valueUnwrapped);
                amount = Globalize.format(amount, "c" + precision, cultureSelector);
            }
        } catch (error) { }
        $(element).text(amount);
    }
};

// Knockout binding for a decimal field
ko.bindingHandlers.decimal = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = valueAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);
        var precision = allBindingsAccessor().precision;
        if (isNaN(precision)) {
            precision = 2;
        }

        var amount = "";
        try {
            if (valueUnwrapped !== undefined && valueUnwrapped !== null) {
                if (isNaN(valueUnwrapped)) {
                    amount = Globalize.parseFloat(valueUnwrapped);
                } else {
                    amount = parseFloat(valueUnwrapped);
                }
                if (!isNaN(amount)) {
                    amount = Globalize.format(amount, "n" + precision);
                }
            }
        } catch (error) { }
        $(element).text(amount);
    }
};

// Knockout binding for a percentage field
ko.bindingHandlers.percentage = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = valueAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);
        var precision = allBindingsAccessor().precision;
        if (isNaN(precision)) {
            precision = 2;
        }

        var percent = "";
        try {
            if (valueUnwrapped !== undefined && valueUnwrapped !== null) {
                if (isNaN(valueUnwrapped)) {
                    percent = Globalize.parseFloat(valueUnwrapped);
                } else {
                    percent = parseFloat(valueUnwrapped);
                }
                if (!isNaN(percent)) {
                    percent = percent > 1 ? percent / 100 : percent;
                    percent = Globalize.format(percent, "p" + precision);
                }
            }
        } catch (error) { }
        $(element).text(percent.replace(" ", ""));
    }
};

// Knockout binding for a phone number
ko.bindingHandlers.phone = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = valueAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);
        var phoneNumber = "tel:";

        if ($(element).is("a")) {
            try {
                if (valueUnwrapped !== undefined && valueUnwrapped !== null) {
                    phoneNumber += valueUnwrapped.replace(/\D/g, '');
                    $(element).attr("href", phoneNumber);
                }
            } catch (error) { }

            $(element).text(valueUnwrapped);
        }
    }
};

// Knockout binding for an email address
// To use do mailto: {email: emailvalue, label: labelvalue}
// The label property is optional but adds the aria-label, title and alt attributes.
ko.bindingHandlers.mailto = {
    update: function (element, valueAccessor) {
        if ($(element).is("a")) {
            var email = ko.utils.unwrapObservable(valueAccessor().email);
            $(element).attr("href", "mailto:" + email);
            if (valueAccessor().label != null) {
                var label = ko.utils.unwrapObservable(valueAccessor().label);
                $(element).attr("aria-label", label);
                $(element).attr("title", label);
                $(element).attr("alt", label);
            }
        }
    }
};

/*
Binding adds markup to an anchor that links to a url outside of the SelfService application.
External links will open in a new tab/window and have an offscreen "External Link" identifier that will be
read by screen readers. The external link message comes from _GlobalStringConstantDefinitions and Resources.SiteResources.

Binding depends on css class 'offScreen' that positions the offscreen text absolutely outside of the viewport

Arguments (both are required):
{
    href: 'the url to link to',
    label: 'the text to display in place of the url'
}

Markup Example:
<a data-bind="externalLink: { href: Url(), label: Title() }"></a>

Result Example:
<a data-bind="externalLink: { href: Url(), label: Title() }" href="http://www.google.com" target="_blank">
    Search Google
    <span class='offScreen'>External Link</span>
</a>
*/
ko.bindingHandlers.externalLink = {
    update: function (element, valueAccessor) {
        if ($(element).is("a")) {
            var params = ko.utils.unwrapObservable(valueAccessor()),
                href = ko.utils.unwrapObservable(params.href),
                label = ko.utils.unwrapObservable(params.label);

            if (label && href) {
                $(element)
                    .attr("href", href)
                    .attr("target", "_blank")
                    .html(label + "<span class='offScreen'>" + screenReaderExternalLinkMessage + "</span>");


            }
        }
    }
}

// Knockout binding for a sliding panel
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will create a panel of content (hidden by default) that will slide into the page when 
// its controller is clicked.  The binding is applied to the controller itself, which must be a <div> 
// element, and the panel is built from the binding options. 
// ===================================================================================================
// Options
// ===================================================================================================
// - contentId: ID of the element containing the content to be shown on the panel when it slides in
// - description: Text to describe the contents of the panel (needed for accessibility)
// - parentId: (optional) ID of the parent container that the panel will cover
//      default: The main page <div> element, whose ID is "#body"
// - backLinkLabel: (optional) Text to be used for the Back link label at the top of the panel 
//      default: GlobalResources.SiteResources.BackLinkLabel value
// - backLinkText: (optional) Text to be used for the Back link text at the top of the panel
//      default: GlobalResources.SiteResources.BackLinkText value
// - direction: (optional) Direction from which the panel will slide in
//      valid values:   left, right, bottom
//      default: right
// ===================================================================================================
// Mark-Up Examples (2)
// ===================================================================================================
//  <!-- This will create a panel whose contents are the "panel-content-id" div element, described by
//      the panelDescription() KO observable, with none of the optional binding options - it will 
//      slide in from the right, its back link label will use the 
//      GlobalResources.SiteResources.BackLinkLabel value, and its back link text will use the
//      GlobalResources.SiteResources.BackLinkText value -->
// 
//  <div data-bind="panel: { contentId: 'panel-content-id', description: panelDescription() }">
//      <span>Text for button that will slide in the panel</span>
//  </div>
//  <div id="panel-content-id" class="panel-content">
//      <span>Content of the sliding panel</span>
//  </div>
//
//
//
//  <!-- This will create a panel whose contents are the "panel-content-id" div element, described by
//      the panelDescription() KO observable, with parentId, direction, and backLinkText binding options 
//      - it will slide in from the left, cover the "parent-container" element, its back link 
//      label will say "Go Back," and its back link text will say "Return to Previous Page" -->
// 
//  <div data-bind="panel: { contentId: 'panel-content-id', , description: panelDescription(), 
//      parentId: 'parent-container', direction: 'right', backLinkLabel: 'Go Back', 
//      backLinkText: 'Return to Previous Page' }">
//      <span>Text for button that will slide in the panel</span>
//  </div>
//  <div id="panel-content-id" class="panel-content">
//      <span>Content of the sliding panel</span>
//  </div>
// ===================================================================================================
ko.bindingHandlers.panel = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var options = ko.utils.unwrapObservable(valueAccessor()) || {};

        if ($(element).is("div,button,tr")) {
            var panelContentId = options.contentId ? options.contentId : null;
            var panelDescription = options.description ? options.description : null;
            var panelId = options.contentId ? options.contentId + "-panel" : null;
            var parentId = options.parentId ? options.parentId : "body";
            var panelBackLabel = options.backLinkLabel ? options.backLinkLabel : panelBackLinkString;
            var panelBackText = options.backLinkText ? options.backLinkText : panelBackTextString;
            var wrappingTable = "<table class='panel-table'><tbody></tbody></table>";
            var slideFrom = "right";
            if (options.direction) {
                switch (options.direction) {
                    case "left":
                    case "bottom":
                    case "top":
                        slideFrom = options.direction;
                        break;
                    default:
                        break;
                }
            }

            if (panelId !== null && typeof panelId !== 'undefined' &&
                panelDescription !== null && typeof panelDescription !== 'undefined' &&
                panelContentId !== null && typeof panelContentId !== 'undefined' &&
                parentId !== null && typeof parentId !== 'undefined' &&
                panelBackLabel !== null && typeof panelBackLabel !== 'undefined' &&
                panelBackText !== null && typeof panelBackText !== 'undefined') {

                // Create shortcut for panel content container identifier
                var bodyContentId = "#" + panelContentId;

                // Remove any duplicate instances of panelContent (in cases where panelContentId is dynamic)
                var idInstances = $('[id="' + panelContentId + '"]');
                var idCounter = 0;
                if (idInstances.length > 1) {
                    do {
                        $(idInstances[idCounter]).remove();
                        idCounter++;
                    } while (idCounter < idInstances.length - 1);
                }

                // Add "button" data role to <div> elements, or add "no-styles" class to <button> elements
                // Do not add no-styles to rows
                if ($(element).is("div")) {
                    $(element).attr("role", "button");
                } else if ($(element).is("button")) {
                    $(element).addClass("no-styles");
                }

                // Add accessibility and navigation attributes and mark-up to panel controller
                $(element).attr("tabindex", "0");
                $(element).addClass("panel-controller");
                $(element).attr("aria-controls", panelId);

                // Ensure that panel wrapper is not placed in an invalid location if using row and that arrow is placed inside last cell if using row
                var appendPanel = "<div id='" + panelId + "' class='panel-wrapper'><div id='" + panelId + "-back-link' class='panel-back-link-container' role='button' tabindex='0'><span class='arrow arrow-left'></span><h2 class='panel-back-link'>" + panelBackLabel + "</h2><p>" + panelBackText + "</p></div></div>";
                var appendPanelDescription = "<span id='" + panelId + "-description' class='hide-content'>" + panelDescription + "</span>";
                var appendArrow = "<span class='arrow arrow-right'></span>";
                if ($(element).is("tr")) {
                    $(element).parentsUntil('table').append(appendPanel);
                    $(element).parentsUntil('table').append(appendPanelDescription);
                    $(element).children().last().append(appendArrow);
                }
                else {
                    $(element).append(appendPanel);
                    $(element).append(appendPanelDescription);
                    $(element).append(appendArrow);
                }

                // Add attributes to panel container
                $("#" + panelId).attr("aria-labelledby", panelId + '-description');
                $("#" + panelId).attr("aria-hidden", true);
                $("#" + panelId).attr("role", "dialog");

                // Determine if the default parent is being used
                var defaultParent = (parentId === "body");

                // Add "panel-content" class to panel content container so that it is suppressed initially
                $(bodyContentId).addClass("panel-content");

                // ================================
                // Click event for panel controller
                // ================================
                $(element).click(function () {

                    // Add the panel content container to the panel
                    $("#" + panelId).append($(bodyContentId));

                    // Check for table contents and fix excess additions
                    var checkForTable = "#" + options.contentId;

                    if ($(checkForTable).is("tr") && ($(checkForTable).parent().length > 0)) {
                        // if panel contents is a row, wrap in a table to be semantically correct
                        $(checkForTable).wrap(wrappingTable);

                        // remove any empty panel-tables (panel removed on slide out, but wrapping table remains)
                        $('.panel-table tbody').filter(function (index) {
                            return $(this).children().length < 1;
                        }).parent().remove();
                    }

                    // Show the panel contents
                    $("#" + panelId + " " + bodyContentId).show();

                    // Create shortcut for panel identifier
                    var panel = "#" + $(this).attr("aria-controls")

                    // Set the panel positioning based on the parent container
                    if (defaultParent) {
                        $(panel).css("top", '0px');
                        $(panel).css("left", "0px");
                    }
                    else {
                        var parentPosition = $("#" + parentId).position();
                        $(panel).css("top", parentPosition.top);
                        $(panel).css("left", parentPosition.left);

                        // Only set the width if we're not looking at a table row. This should be ok
                        // for everybody, but we don't want to break the panel for people already using it.
                        if (!$(element).is("tr")) {
                            $(panel).css("width", $("#" + parentId).css("width"));
                        }
                        $(panel + " div.panel-back-link-container").addClass("inner-back-link");
                    }

                    // For nested panels, use the height of the parent panel back link as an offset
                    if ($("#" + parentId).parent().hasClass("panel-wrapper")) {
                        $(panel).attr("position", "fixed");
                        $(panel).css("top", "0px");
                    }

                    // Prepend the panel to its parent
                    $(panel).prependTo("#" + parentId);

                    // Set the panel height to the greater of the parent or the panel height, plus the back link container height
                    var parentHeight = $("#" + parentId).height();
                    parentHeight -= defaultParent ? $("#page-description").height() : 0;

                    var panelHeight = $(panel).height() - $(panel + " div.panel-back-link-container").height();
                    if (parentHeight > panelHeight) {
                        var newHeight = parentHeight + $(panel + " div.panel-back-link-container").height() - 1;
                        $(panel).css("height", newHeight + 'px');
                    }
                    else {
                        var newHeight = panelHeight + $(panel + " div.panel-back-link-container").height() - 1;
                        $(panel).css("height", newHeight + 'px');
                    }

                    // Determine which slide animation will introduce the panel
                    // Once the animation completes, hide all non-panel content from the parent container, 
                    // reset the parent container height to the panel's height, then remove
                    // the aria-hidden property from the panel and set focus to panel content container
                    switch (slideFrom) {
                        case "bottom":
                            $(panel).slideToggle("slow").promise().done(function () {
                                if (defaultParent) {
                                    $("#" + parentId).children().not("#" + panelId).hide();
                                } else {
                                    var topLevelPanel = $("#" + parentId).parent();
                                    $("#body").children().not(topLevelPanel).hide();
                                }
                                $(panel).attr("aria-hidden", false);
                                $("#" + parentId).css("height", $(panel).height() + 1 + 'px');
                                $("#" + panelId + " " + bodyContentId).focus();
                            });
                            break;
                        default:
                            $(panel).toggle("slide", { direction: slideFrom }).promise().done(function () {
                                $("#" + parentId).children().not("#" + panelId).hide();

                                $(panel).attr("aria-hidden", false);

                                if ($(element).is("tr")) {
                                    // Set the line items tab panel height to the sum of the slide panel back link height and the slide panel table height
                                    $("#" + parentId).height($(panel).children(".panel-back-link-container").height() + $(panel).children(".panel-table").height());

                                    // Set the width of the panel after the panel appears to account for the scroll bar, should it appear
                                    $(panel).css("width", $("#" + parentId).css("width"));
                                }
                                else {
                                    $("#" + parentId).css("height", $(panel).height() + 1 + 'px');
                                }

                                $("#" + panelId + " " + bodyContentId).focus();
                            });
                            $(window).scrollTop(0);
                            break;
                    }
                });

                // Mimic click handler when control is executed with spacebar or return key
                $(element).keypress(function (e) {
                    var code = e.which;
                    if ((code === 13) || (code === 32)) {
                        $(element).click();
                    }
                });

                // ================================
                // Click event for panel back control
                // ================================
                $("#" + panelId + " div.panel-back-link-container").click(function () {

                    if ($(this).parent().attr("aria-hidden") === "false") {
                        // Determine the slide animation to dismiss the panel
                        switch (slideFrom) {
                            case "bottom":
                                $(this).parent().slideToggle("slow").promise().done(function () {
                                    // Remove the panel content, and set focus to parent
                                    $("#" + parentId).remove("#" + panelId);
                                    $("#" + parentId).focus();
                                });
                                break;
                            default:
                                $(this).parent().toggle("slide", { direction: slideFrom }).promise().done(function () {
                                    // Remove the panel content, and set focus to parent
                                    $("#" + parentId).remove("#" + panelId);
                                    $("#" + parentId).focus();
                                });
                                break;
                        }

                        // Reset the parent container height to its previous height
                        if (defaultParent) {
                            $("#" + parentId).css("height", "auto");
                        }
                        else {
                            $("#" + parentId).css("height", "auto");
                        }

                        // Re-apply the aria-hidden attribute to the panel container and re-display all non-panel content inside the parent
                        $(this).parent().attr("aria-hidden", true);
                        $("#" + parentId).children().not("#" + panelId).not(".panel-wrapper").show();

                        // Collapse any accordions embedded in the panel
                        var accordions = $("#" + panelId + " div.multi-accordion-expanded");
                        if (accordions.length > 0) {
                            for (i = 0; i < accordions.length; i++) {
                                $(accordions[i]).click();
                            }
                        }
                    }
                });

                // Mimic click handler when back link is executed with spacebar or return key
                $("#" + panelId + " div.panel-back-link-container").keypress(function (e) {
                    var code = e.which;
                    if ((code === 13) || (code === 32)) {
                        $("#" + panelId + " div.panel-back-link-container").click();
                    }
                });
            }
        }
    }
};

// Knockout binding for an accordion that supports multiple active accordions simultaneously
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will create an accordion that can be activated alongside other accordions at the same
// time.  
// 
// Accordion can be activated/deactivated by click, spacebar, or return key.
// ===================================================================================================
// Options
// ===================================================================================================
// {
//      contentId: ID of the element containing the content to be shown when the accordion is activated,
//      
//      expanded:  (optional) Flag indicating if the accordion is expanded by default
//          valid value: true (any non-"true" value will be treated as false)
//          default: false (accordion will be collapsed on page load),
//
//      inactive: (optional) Flag indicating whether the accordion should be inactive or not. If true (inactive)
//          valid value: true (any non-"true" value will be treated as false)
//          default: false (accordion will be active on page load),
// }
// ===================================================================================================
// Mark-Up Examples (2)
// ===================================================================================================
//  <!-- This will create an accordion whose content is the "panel-content-id" <div> element;
//      this accordion will be collapsed when the page loads -->
//  <div data-bind="multiAccordion: { contentId: 'panel-content-id' }">
//  </div>
//  <div id="panel-content-id">
//      <span>Content of the sliding panel</span>
//  </div>
//
//  <!-- This will create an accordion whose content is the "panel-content-id" <div> element;
//      this accordion will be expanded when the page loads -->
// 
//  <div data-bind="multiAccordion: { contentId: 'panel-content-id', expand: true }">
//  </div>
//  <div id="panel-content-id">
//      <span>Content of the sliding panel</span>
//  </div>
// ===================================================================================================
// Implementation Notes
// ===================================================================================================
// If the content referenced by your multiAccordion binding has a dynamic ID (i.e. its ID is set by
// a data binding ("attr: { id: someValue() }"), then you will need to take 3 steps to ensure proper 
// rendering.  If you do not take these steps, then your accordions will not work because the 
// multiAccordion binding will look for an element with an ID of "contentId" that has not yet been
// rendered.  Take these steps to remedy this issue:
//
// 1. Move the content element ABOVE the multiAccordion element within your mark-up.
// 2. Assign the content element an abritrary class name - something like "multi-accordion-content"
// 3. In the "complete" function of the AJAX call that loads your view data, add jQuery code to 
//    programmatically move the content element beneath the multiAccordion element.
//
// Example - in your mark-up:
// 
//  <div data-bind="attr: { id: someContentId() }" class="multi-accordion-content">
//      <span>Content of the sliding panel</span>
//  </div>
//  <div data-bind="multiAccordion: { contentId: someContentId() }">
//  </div>
//
// Example - in the "complete" function of your AJAX call:
//
// $(".multi-accordion-content").each(function () {
//    $(this).next().after($(this));
// });
//
//
// Inactive option:
//  This can be used when you don't have any "details" for which a drop down accordion would display. One implementation
//  example is on the Financial Aid SatisfactoryAcademicProgress.cshtml page. If no explanation exists for a detail item, don't
//  attach an accordion to that item and remove any visual indication that the item is clickable.
//  To accomplish this, we turn "off" all events attached to the DOM element, change the cursor style, and 
/// make the arrow invisible (but not hidden in order to preserve padding)
// ===================================================================================================
ko.bindingHandlers.multiAccordion = {
    init: function (element, valueAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor()) || {};
        //Escape special characters in the contentId
        var contentId = options.contentId ? options.contentId.replace(/(:|;|\.|\,|\!|\?|\@|\#|\$|\%|\^|\&|\*|\(|\)|\[|\]|\{|\})/g, "\\$1") : null;
        var expanded = options.expanded ? (options.expanded === true ? true : false) : false;
        var inactive = options.inactive || false;

        if (contentId !== null && typeof contentId !== 'undefined') {
            $(element).attr("tabindex", "0");
            $(element).addClass("multi-accordion-controller");
            $(element).attr("role", "button");
            $(element).attr("aria-controls", contentId);
            $("#" + contentId).attr("aria-labelledby", $(element).attr("id"));

            // Initialize the expandable content
            if ($(element).is('tr')) {
                if ($(element).hasClass("multi-accordion-expanded")) {
                    $(element).children().first().prepend("<span class='arrow arrow-up'></span>");
                }
                else {
                    $(element).children().first().prepend("<span class='arrow arrow-down'></span>");
                }
            }
            else {
                if (expanded) {
                    $(element).prepend("<span class='arrow arrow-up'></span>");
                    $(element).addClass("multi-accordion-expanded");
                } else {
                    $(element).addClass("multi-accordion-collapsed");
                    $(element).prepend("<span class='arrow arrow-down'></span>");
                    $("#" + contentId).hide();
                }
            }

            // Take action on click/select
            $(element).click(function () {
                if ($(element).hasClass("multi-accordion-expanded")) {
                    if (!$(element).is("tr")) {
                        $(element).children(".arrow-up").replaceWith("<span class='arrow arrow-down'></span>");
                        $(element).removeClass("multi-accordion-expanded");
                        $(element).attr("aria-expanded", false);
                        $(element).addClass("multi-accordion-collapsed");
                    }
                }
                else {
                    if (!$(element).is("tr")) {
                        $(element).children(".arrow-down").replaceWith("<span class='arrow arrow-up'></span>");
                        $(element).removeClass("multi-accordion-collapsed");
                        $(element).attr("aria-expanded", true);
                        $(element).addClass("multi-accordion-expanded");
                        $("#" + contentId).focus();
                    }
                }


                if ($(element).is("tr")) {
                    // if expanded, then close
                    if ($(element).hasClass("multi-accordion-expanded")) {
                        $(element).find(".arrow-up").replaceWith("<span class='arrow arrow-down'></span>");
                        $(element).removeClass("multi-accordion-expanded");
                        $(element).attr("aria-expanded", false);
                        $(element).addClass("multi-accordion-collapsed");

                        // Update ARIA for the button
                        $(element).find("button.expand-details").attr("aria-expanded", false);

                        // In order to show the animation we have a DIV inside our table row. Now, a TR cannot be animated so
                        // we need to animate the DIV first, then close the TR.
                        $(element).next().find('.accordion-table-wrapper').slideToggle(function () {
                            $(element).next().toggle();
                        });
                    }
                        // if collapsed, then open
                    else if ($(element).hasClass("multi-accordion-collapsed") || !$(element).hasClass("multi-accordion-expanded")) {
                        // The '.accordion-table-wrapper' DIV needs to be displayed when the page loads so once the row is selected we
                        // need to keep things in sync by first hiding the DIV, showing the TR, then animating the DIV.
                        $(element).next().find('.accordion-table-wrapper').hide();
                        $(element).next().toggle();
                        $(element).next().find('.accordion-table-wrapper').slideToggle();

                        $(element).find(".arrow-down").replaceWith("<span class='arrow arrow-up'></span>");
                        $(element).removeClass("multi-accordion-collapsed");
                        $(element).attr("aria-expanded", true);
                        $(element).addClass("multi-accordion-expanded");

                        // Update ARIA for the button
                        $(element).find("button.expand-details").attr("aria-expanded", true);

                        $("#" + contentId).focus();
                    }
                }
                else {
                    $("#" + contentId).slideToggle("slow");
                }
            });

            // Mimic click handler when master is executed with spacebar or return key
            $(element).keypress(function (e) {
                var code = e.which;
                if ((code === 13) || (code === 32)) {
                    $(element).click();
                }
            });


            //See Inactive option in documentation for more info.
            //Turn off event handlers. 
            //Change the cursour style not to indicate a button.
            //Replace arrow spans with 'no-arrow'
            if (inactive) {
                $(element)
                    .off()
                    .css("cursor", "default");

                // If accordion has no details, remove role, aria, tabindex so that it appears as normal text
                $(element).removeAttr("role aria-controls tabindex");
                $('.arrow', element).replaceWith("<span class='arrow no-arrow'></span>");
            }
        }
    }
};

// Knockout binding for an accordion expand all/ collapse all button
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will create a button that allows the end user to expand or collapse all accordions
// on a page.  The expand all / collapse all functionality is tied specifically to accordions built by
// the "multiAccordion" data binding.  The text of this button will alternate depending on the state
// of the page accordions; when all accordions are expanded, users will see the value of 
// GlobalResources.SiteResources.CollapseAllLabel, otherwise users will see the value of
// GlobalResources.SiteResources.ExpandAllLabel.  
// 
// Button can be activated/deactivated by click, spacebar, or return key.
//
// Its "reset" option specifices a KO observable that, when changed, will "reset" this control. Without
// this, the control may not show the correct label when page content changes.  
// 
// Its "expandMessage" and "collapseMessage" options specify announcements for screen readers to tell 
// the user a context-appropriate result of their expanding or collapsing all accordions on a page.
// ===================================================================================================
// Options
// ===================================================================================================
// - reset: Observable that, when changed, will force the element to be reset
// - expandMessage: Message to be announced by screen readers when expanding all accordions
// - collapseMessage: Message to be announced by screen readers when collapsing all accordions
// ===================================================================================================
// Mark-Up Examples (2)
// ===================================================================================================
// <input id="expand-collapse-button" type="button" 
//  data-bind="accordionMaster: { reset: observableThatKeysReset() }" >
//
// <input id="expand-collapse-button" type="button" 
//  data-bind="accordionMaster: { reset: observableThatKeysReset(), expandMessage: 'All accordions expanded.', 
//      collapseMessage: 'All accordions collapsed.' }" >
// ===================================================================================================
ko.bindingHandlers.accordionMaster = {
    init: function (element, valueAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor()) || {};
        var expandedMessage = options.expandMessage ? options.expandMessage : null;
        var collapsedMessage = options.collapseMessage ? options.collapseMessage : null;

        // Update accordion classes when the master is clicked
        $(element).click(function () {
            var accordionCount = $(".multi-accordion-controller").length;
            var activeAccordionCount = $(".multi-accordion-expanded").length;
            if (activeAccordionCount < accordionCount) {
                var collapsedControllers = document.querySelectorAll(".multi-accordion-collapsed");
                if (collapsedControllers.length > 0) {
                    for (i = 0; i < collapsedControllers.length; i++) {
                        $("#" + $(collapsedControllers[i]).attr("aria-controls")).slideToggle("slow");
                        $(collapsedControllers[i]).children(".arrow-down").replaceWith("<span class='arrow arrow-up'></span>");
                        $(collapsedControllers[i]).addClass("multi-accordion-expanded");
                        $(collapsedControllers[i]).attr("aria-expanded", true);
                        $(collapsedControllers[i]).removeClass("multi-accordion-collapsed");
                    }
                }
                if ($("#aria-announcements").length > 0) {
                    $("#aria-announcements").text(expandedMessage);
                }
            }
            if (activeAccordionCount === accordionCount) {
                var expandedControllers = document.querySelectorAll(".multi-accordion-expanded");
                if (expandedControllers.length > 0) {
                    for (i = 0; i < expandedControllers.length; i++) {
                        $("#" + $(expandedControllers[i]).attr("aria-controls")).slideToggle("slow");
                        $(expandedControllers[i]).children(".arrow-up").replaceWith("<span class='arrow arrow-down'></span>");
                        $(expandedControllers[i]).removeClass("multi-accordion-expanded");
                        $(expandedControllers[i]).attr("aria-expanded", false);
                        $(expandedControllers[i]).addClass("multi-accordion-collapsed");
                    }
                }
                if ($("#aria-announcements").length > 0) {
                    $("#aria-announcements").text(collapsedMessage);
                }
            }
            $(element).focus();
        });

        // Mimic click handler when master is executed with spacebar or return key
        $(element).keypress(function (e) {
            var code = e.which;
            if ((code === 13) || (code === 32)) {
                $(element).click();
            }
        });

        // Pick up any non-master accordion actions to properly update the master text
        $("body").click(function () {
            var accordionCount = $(".multi-accordion-controller").length;
            var activeAccordionCount = $(".multi-accordion-expanded").length;
            if (activeAccordionCount < accordionCount) {
                $(element).val(expandAllLabel);
            } else {
                $(element).val(collapseAllLabel);
            }
            $(element).attr("aria-label", $(element).val());
        });
    },
    update: function (element, valueAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor()) || {};
        var resetter = options.reset ? options.reset : null;
        var accordionCount = $(".multi-accordion-controller").length;
        var activeAccordionCount = $(".multi-accordion-expanded").length;
        $(element).attr("tabindex", "0");
        $(element).attr("aria-hidden", true);

        if (activeAccordionCount < accordionCount) {
            $(element).val(expandAllLabel);
            $(element).attr("aria-label", $(element).val());
        } else {
            $(element).val(collapseAllLabel);
            $(element).attr("aria-label", $(element).val());
        }
    }
}

// Knockout binding for an element that will hover at the top of the page when the user scrolls
// past its initial position
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will enable an element to hover at the top of the page when the user scrolls
// down past its initial position, and then "snap back" into its original position if the user scrolls 
// up past its initial position.
// 
// Its "reset" option specifices a KO observable that, when changed, will "reset" this control. Without
// this, the control may not behave correctly when the page is loaded.
// ===================================================================================================
// Options
// ===================================================================================================
// - reset: Observable that, when changed, will force the element to be reset
// ===================================================================================================
// Mark-Up Examples (2)
// ===================================================================================================
// <div id="hovering-element" data-bind="hover: { reset: observableThatKeysReset() }" >
// ===================================================================================================
ko.bindingHandlers.hover = {
    update: function (element, valueAccessor) {
        var options = ko.utils.unwrapObservable(valueAccessor()) || {};
        var resetter = options.reset ? options.reset : null;

        // Note the element's initial top position
        var floatTop = $(element).position().top;
        var floatWidth = $(element).css("width");

        var topPos = 0;
        if ($("#proxy-banner").length) {
            topPos += $("#proxy-banner").outerHeight(true) + 'px';
        }

        // Hover the box when scrolling - the box will retain its initial display properties until the user scrolls
        // past its initial top position.  At that time, the box will begin hovering as the user scrolls further 
        // down the page.  If the user scrolls back up past the initial top position, then the box will "lock" back 
        // to its initial position
        $(window).scroll(function () {
            floatWidth = $(element).css("width");

            var scroll = $(window).scrollTop();
            if (scroll >= floatTop) {
                $(element).css("width", floatWidth);
                $(element).addClass("floating");
                $(element).css("top", topPos)
            }
            if (scroll < floatTop) {
                $(element).removeClass("floating");
                $(element).css("width", "auto");
                $(element).css("top", floatTop);
            }
        });

        // Maintain proper element width during screen resizing
        $(window).resize(function () {
            $(element).css("width", floatWidth);
        });
    }
}

// Knockout binding for an element that will stick to the top of the page when the user scrolls
// past its initial position
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will enable an element to stick to the top of the page when the user scrolls
// down past its initial position, and then "snap back" into its original position if the user scrolls 
// up past its initial position.
// 
// Its "mobile" option specifices whether the user is using a mobile device and its "ignoreProxy"
// option dictates whether or not the item should stick when the user is a proxy
// ===================================================================================================
// Options
// ===================================================================================================
// - mobile: flag that indicates if a mobile device is being used
// - ignoreProxy - flag that indicates whether or not the binding should be ignored when 
//    the user is a proxy; "true" indicates that the item should stick even for a proxy user
//    and false indicates that the item should not stick
// ===================================================================================================
// Mark-Up Example
// ===================================================================================================
// <div id="hovering-element" data-bind="stick: { mobile: isMobile(), ignoreProxy: false }" >
// ===================================================================================================

ko.bindingHandlers.stick = {
    init: function (element, valueAccessor) {

        // Create the phantom bar and hide it
        $("<div class='phantom-sticky-bar'></div>").insertAfter($(element));
        var phantomBar = $(".phantom-sticky-bar");
        phantomBar.hide();

        //handle disposal (not strictly necessary in this scenario)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            phantomBar.remove();
            $(window).off("scroll.actionbar touchmove.actionbar");
        });
    },
    update: function (element, valueAccessor) {

        var options = ko.utils.unwrapObservable(valueAccessor()) || {};

        //get the initial width of the action bar
        var actionBar = $(element),
            initialPos = actionBar.position(),
            isProxy = $("#proxy-banner").length,
            ignoresProxy = options.ignoreProxy ? options.ignoreProxy : true;
        heightAdjustment = options.mobile ? 100 : 66;

        var phantomBar = $(".phantom-sticky-bar");

        //register a function to fire on the scroll event so we can 
        //pin the action bar to the top of the page
        $(window).on("scroll.actionbar touchmove.actionbar", debounce(function () {

            if (!isProxy || ignoresProxy) {
                var windowpos = $(window).scrollTop(),
                    actionBarWidth = actionBar.outerWidth();
                if (windowpos >= initialPos.top + heightAdjustment) {
                    phantomBar.css("height", actionBar.height());
                    actionBar.addClass("stick-content");
                    actionBar.css("width", actionBarWidth);
                    phantomBar.show();
                } else {
                    actionBar.removeClass("stick-content");
                    actionBar.css("width", "auto");
                    phantomBar.hide();
                }
            }

        }, 10))
    }
}

// Knockout binding for a tooltip
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will create a tooltip that will appear near a target element.
// 
// Tooltip can be activated/deactivated by hover/focus on desktop and focus/touch on mobile.
// ===================================================================================================
// Options
// ===================================================================================================
// {
//      message: Message to be displayed inside the tooltip
//
//      position: Position of tooltip (top, left, right, bottom) - top is default if not provided
//
//      preventParentClickOnMobile: Prevent parent click event - false is default if not provided
// }
// ===================================================================================================
// Mark-Up Examples (2)
// ===================================================================================================
//  <!-- Place binding on target element that will receive tooltip -->
//  <div data-bind="tooltip: { position: 'left', message: 'Hi I am a tooltip' }">
//  </div>
// ===================================================================================================
ko.bindingHandlers.tooltip = {
    init: function (element, valueAccessor) {

        var options = ko.utils.unwrapObservable(valueAccessor()) || {};
        var message = options.message ? options.message : null;
        var position = options.position ? options.position : 'top';
        var preventParentClickOnMobile = options.preventParentClickOnMobile ? options.preventParentClickOnMobile : false;
        var zIndex = options.zIndex ? parseInt(options.zIndex) : false;

        window.noOfTooltips = window.noOfTooltips + 1 || 0;

        var currentTooltip = 'tooltip-' + window.noOfTooltips;

        var target = element;

        // If there is no message for the tooltip, display a console warning
        if (message == null) {
            console.warn("Tooltip message is null for: " + $(target)[0].outerHTML);
        }

            // Tooltip has message, proceed
        else {
            // Create the tooltip with hidden visibility (using visibility instead of display: none for CSS animation)
            $('<div id="' + currentTooltip + '" class="esg-tooltip css-tooltip hidden" role="tooltip"><div class="esg-tooltip__arrow"></div><div class="esg-tooltip__content">' + message + '</div></div>').appendTo('body');
            var tooltip = $('#' + currentTooltip);
            if (zIndex && !isNaN(zIndex)) {
                $(tooltip).css("z-index", zIndex);
            }

            // Check if tooltip placement positions tooltip outside of document
            var tooltipExceedsDocument = false;

            var tooltipHeight = $(tooltip).outerHeight();
            var tooltipWidth = $(tooltip).outerWidth();
            var targetHeight = $(target).outerHeight();
            var targetMidPoint = Math.round(targetHeight / 2);
            var tooltipTextHeight = $('#' + currentTooltip + ' .esg-tooltip__content').height();
            var tooltipFontSize = $('#' + currentTooltip + ' .esg-tooltip__content').css('line-height');
            var tooltipLineHeight = parseInt(tooltipFontSize.replace('px', ''));

            // Calculate if tooltip wraps to multiple lines
            var multiLine = function () {
                if ((tooltipTextHeight / tooltipLineHeight) > 1) {
                    return true;
                }
                else {
                    return false;
                }
            }

            var top = function () {
                var spacing = 5;
                var topEnd = $(target).offset().top;
                var topPosition;

                // Return rounded numbers to avoid bouncing tooltips
                if (tooltipClass() == "esg-tooltip--right" || tooltipClass() == "esg-tooltip--left") {
                    topPosition = topEnd + targetMidPoint - tooltipHeight / 2;
                    return Math.round(topPosition);
                } else if (tooltipClass() == "esg-tooltip--bottom") {
                    topPosition = topEnd + targetHeight + spacing;
                    return Math.round(topPosition);
                } else {
                    topPosition = topEnd - tooltipHeight - spacing;
                    return Math.round(topPosition);
                }
            };

            var left = function () {
                var spacing = 10;
                var outerWidth = tooltipWidth + spacing;
                var targetLeftEnd = Math.round($(target).offset().left);

                // Corrected left position for tooltips going offscreen on the right side of the screen
                var tooltipCorrectedLeftPosition = Math.round(targetLeftEnd + targetMidPoint - tooltipWidth / 2);

                // Corrected left position for tooltips going offscreen on the left side of the screen
                var tooltipCorrectedRightPosition = Math.round(targetLeftEnd - targetMidPoint);

                // check if tooltip is in the sidebar (menu items have rel="menu")
                var attr = $(target).attr('rel');
                if ((typeof attr !== typeof undefined && attr !== false) && (attr.indexOf('menu') >= 0)) {
                    leftPosition = 80;
                    return leftPosition;
                } else {
                    // Return rounded numbers to avoid bouncing tooltips
                    if (position.toLowerCase() == "right") {
                        var rightPosition = Math.round(targetLeftEnd + $(target).outerWidth() + spacing);
                        if (outerWidth + rightPosition > $(document).width()) {
                            tooltipExceedsDocument = true;
                            rightPosition = tooltipCorrectedLeftPosition;
                            if (rightPosition < 0) {
                                rightPosition = tooltipCorrectedRightPosition;
                            }
                        } else {
                            tooltipExceedsDocument = false;
                        }
                        return rightPosition;
                    } else if (position.toLowerCase() == "left") {
                        var leftPosition = Math.round(targetLeftEnd - tooltipWidth);
                        if (leftPosition < 0) {
                            tooltipExceedsDocument = true;
                            leftPosition = tooltipCorrectedLeftPosition;
                        } else {
                            tooltipExceedsDocument = false;
                        }
                        return leftPosition;
                        // For top or bottom tooltip
                    } else {
                        if (outerWidth + tooltipCorrectedLeftPosition > $(document).width()) {
                            tooltipCorrectedLeftPosition = $(document).width() - outerWidth;
                        }

                        // If corrected position would place the tooltip off the screen on the left, use right positioning
                        if (tooltipCorrectedLeftPosition < 0) {
                            tooltipCorrectedLeftPosition = tooltipCorrectedRightPosition;
                        }

                        // Set tooltipExceedsDocument to false in case a previous left or right tooltip has set it to true
                        tooltipExceedsDocument = false;
                        return tooltipCorrectedLeftPosition;
                    }
                }
            };

            var tooltipClass = function () {
                if (tooltipExceedsDocument == true) {
                    return "esg-tooltip--top";
                } else {
                    return "esg-tooltip--" + position.toLowerCase();
                }
            };

            // if the target is in dialog, get the dialog's z-index as an integer and add that to tooltip +1
            if ($(target).closest('.ui-dialog').length > 0) {
                var currentTooltipZindex = parseInt($(target).closest('.ui-dialog').css("z-index"), 10);
                tooltip.css("z-index", currentTooltipZindex + 1);
            }

            // hide all tooltips
            function hideAllTooltips() {
                $('.esg-tooltip').removeClass('fade-in');
                $('.esg-tooltip').addClass('hidden fade-out');
            }

            // show current tooltip
            function showCurrentTooltip() {
                tooltip.removeClass('hidden fade-out');
                tooltip.addClass('fade-in');
            }

            // hide current tooltip
            function hideCurrentTooltip() {
                tooltip.removeClass('fade-in');
                tooltip.addClass('hidden fade-out');
            }

            $(target).on('mouseenter focus touchstart click', function (event) {
                $(tooltip).addClass(tooltipClass);
                // reposition multiline tooltip caret for height of tooltip content + 6px (box shadow & border)
                if ((multiLine() == true) && (tooltipClass() == 'esg-tooltip--top')) {
                    var currentTooltipHeight = $('#' + currentTooltip + ' .esg-tooltip__content').innerHeight();
                    $('#' + currentTooltip + ' .esg-tooltip__arrow').attr('style', 'top: ' + parseInt(currentTooltipHeight + 6) + 'px;');
                }
                hideAllTooltips();
                showCurrentTooltip();

                $(tooltip).css({ 'left': left, 'top': top });

                if (event.type == 'touchstart' && preventParentClickOnMobile == true) {
                    event.stopPropagation();
                    return false;
                }
            });

            $(target).on('mouseleave blur', hideCurrentTooltip);

            // if event is bound, unbind it (prevent a document bind for each tooltip)
            $(document).off('touchstart', hideAllTooltips);

            // bind again
            $(document).on('touchstart', hideAllTooltips);

            //handle disposal (not strictly necessary in this scenario)
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                tooltip.remove();
            });
        }
    },
    update: function (element, valueAccessor) {

    }
}


ko.bindingHandlers.noParentBubbling = {
    update: function (element, valueAccessor) {
        $(element).click(function (event) {
            event.stopPropagation();
        });
    }
}

// Knockout binding for an element that will call the specified function when the Enter key is pressed
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will call the specified function when the Enter key is pressed.
// ===================================================================================================
// Mark-Up Example
// ===================================================================================================
// <input id="some-element" data-bind="enterKey: functionToBeTriggeredByEnterKeyPress }" >
// ===================================================================================================
ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var callback = valueAccessor();
        $(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
                $(element).blur();
                $(element).focus();
                callback.call(viewModel);
                return false;
            }
            return true;
        });
    }
};

// Knockout binding for an element that will mimic a link when the Enter or Space key is pressed
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding will call an element's click behavior when the Enter or Space key is pressed
// ===================================================================================================
// Mark-Up Example
// ===================================================================================================
// <input id="some-element" data-bind="treatAsLink: {}">
// ===================================================================================================
ko.bindingHandlers.treatAsLink = {
    init: function (element) {
        $(element).keypress(function (e) {
            var code = e.which;
            if ((code === 13) || (code === 32)) {
                $(element).click();
            }
        });
    }
}

//ko.bindingHandlers.defaultText = {
//    init : function() {
//        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
//        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
//        return { 'controlsDescendantBindings': true };
//    },
//    update: function (element, valueAccessor) {
//        var options = ko.utils.unwrapObservable(valueAccessor()) || {};
//        var defaultText = options.default || "";
//        var text = options.text || "";

//        if (text.trim()) {
//            ko.utils.setTextContent(element, text);
//        } else {
//            ko.utils.setTextContent(element, defaultText);
//        }
//    }
//}



function parseANumber(value) {
    value = value || 0;
    return (isNaN(value)) ? Globalize.parseFloat(value) : parseFloat(value);
}

// Handler for collapsible row
ko.bindingHandlers.toggleCollapsibleRow = {
    init: function (element, valueAccessor) {
        var values = ko.utils.unwrapObservable(valueAccessor()) || {};
        var isTable = values.isTable;

        // Determine whether open or false to determine which order to call
        // toggle for td and slide for wrapping div .collapsible-item
        var isOpen = values.isOpen || false;
        // to initialize visible
        if (isOpen) {
            $(element).nextUntil(".collapsible").slideToggle();
        }

        // on click toggle the line item visibility
        $(element).click(function () {
            // Toggle the nested rows until the next top-level row
            if (isTable === true) {
                if (isOpen === false) {
                    $(element).next().toggle();
                    $(element).next().find('.collapsible-item').slideToggle();
                    isOpen = true;
                }
                else {
                    // use callback on slideToggle to determine when animation is complete
                    $(element).next().find('.collapsible-item').slideToggle(function () {
                        $(element).next().toggle();
                        isOpen = false;
                    });
                }
            }
            else {
                $(element).nextUntil(".collapsible").slideToggle();
            }

            // Toggle the up arrow class (default down .arrow alone for collapsed, .arrow.arrow-up for expanded)
            var replaceArrow = $(element).find('.arrow');
            $(replaceArrow).toggleClass("arrow-up");

            // Toggle the aria-expanded attribute when expanded/collapsed
            if ($(element).find('.expand-details').attr("aria-expanded") == "true") {
                $(element).find('.expand-details').attr("aria-expanded", "false");
            }
            else {
                $(element).find('.expand-details').attr("aria-expanded", "true");
            }
        });
    }
};

// Masks input characters with specificity and event options
//to be used along side a textInput binding
// options
//      maskChar - default *
//      unmaskStart - default 0
//      unmaskEnd - default 0
ko.bindingHandlers.mask = {
    createMask: function (input, options) {
        var maskingChars = ko.bindingHandlers.mask.maskOptions.maskChar,
            unmaskStart = ko.bindingHandlers.mask.maskOptions.unmaskStart,
            unmaskEnd = ko.bindingHandlers.mask.maskOptions.unmaskEnd,
            maskMin = ko.bindingHandlers.mask.maskOptions.maskMin,
            maskSet1 = '',
            maskSet2 = '';

        if (!input || input.length <= maskMin) {
            return input;
        }
        for (var i = 0; i < Math.abs(unmaskStart) ; i++) {
            maskSet1 += maskingChars;
        }
        for (var i = Math.abs(unmaskEnd) ; i < input.length; i++) {
            maskSet2 += maskingChars;
        }
        return '{0}{1}{2}'.format(maskSet1, input.slice(unmaskStart, unmaskEnd), maskSet2);
    },
    maskOptions: {
        maskChar: '*',
        maskMin: 0,
        unmaskStart: 0,
        unmaskEnd: 0
    },
    setMaskOptions: function (bindingOptions) {
        if (bindingOptions.maskChar && bindingOptions.maskChar !== "")
            ko.bindingHandlers.mask.maskOptions.maskChar = bindingOptions.maskChar;

        if (bindingOptions.unmaskStart && bindingOptions.unmaskStart !== "")
            ko.bindingHandlers.mask.maskOptions.unmaskStart = bindingOptions.unmaskStart;

        if (bindingOptions.unmaskEnd && bindingOptions.unmaskEnd !== "")
            ko.bindingHandlers.mask.maskOptions.unmaskEnd = bindingOptions.unmaskEnd;

        if (bindingOptions.unmaskEnd && bindingOptions.maskMin !== "")
            ko.bindingHandlers.mask.maskOptions.maskMin = bindingOptions.maskMin;
    },
    init: function (element, valueAccessor, allBindings) {
        var e = $(element),
            observable = allBindings().textInput;

        ko.bindingHandlers.mask.setMaskOptions(
            ko.utils.unwrapObservable(valueAccessor()));

        //value should appear masked on init
        e.val(ko.bindingHandlers.mask.createMask(observable()));

        //register event handlers
        // remove the mask on hover or FOCUS!
        e.on("mouseenter focusin", function () {
            e.val(observable());
        });

        // reinstate the mask afterwards
        e.on("mouseleave", function () {
            if (!e.is(':focus'))
                e.val(ko.bindingHandlers.mask.createMask(observable()));
        });

        e.on("focusout", function () {
            e.val(ko.bindingHandlers.mask.createMask(observable()));
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            e.off();
        });

    },
    update: function (element, valueAccessor, allBindings) {
        ko.bindingHandlers.mask.setMaskOptions(
            ko.utils.unwrapObservable(valueAccessor()));
    }
};




//adding a closure function to encapsulate all the functionality for table custom bindings like - sorting, searching, paging
var tableFilters = (function () {

    //this binding is used to tell that collection that table is bound to is dynamic.
    //it means that table could be changed either with new addition of item or deletion of item or if table is re-mapped through some other action
    //addign this binding to table defines that table sort order will be maintained after it is changed dynamically
    //if this is not added and insertion/deletion or re-mapping of table bound array/contents is changed, sorting will not happen and will show the data with old sort image
    //this binding simply add a read observable to collection to create dependency
    //hence when array/collection is modified, its sortable binding update is ran, where it is verified that other bindigAccessors have dynamicCollection binding, and if it has it re-sorts it.
    //see the expample of usage in FacultySectionGradingPartila.cshtml
    ko.bindingHandlers.dynamicCollection = {
        init: function (element, valueAccessor) {
            try {
                var args = arguments;
                ko.computed({
                    read: function () {
                        ko.utils.unwrapObservable(valueAccessor());
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
            catch (e) {
                console.error(e);
            }


        }
    };




    //this function encapsulate functionality for sorting
    function sortClass() {
        var maximumNestedFieldsToSort = 3;
        var errorMessages = ["Cannot have more than " + maximumNestedFieldsToSort + " nested fields for sorting"];
        var imagePath = ko.observable();
        var bothImage = ko.computed(function () {
            return imagePath() + 'both.png';
        });
        var upImage = ko.computed(function () {
            return imagePath() + 'up.png';
        });
        var downImage = ko.computed(function () {
            return imagePath() + 'down.png';
        });

        var elementIndex = 0;
        //compare two values
        function compare(firstValue, secondValue) {
            if (typeof firstValue == 'undefined') {
                firstValue = null;
            }
            if (typeof secondValue == 'undefined') {
                secondValue = null;
            }

            //check if string
            if (typeof firstValue === 'string' && typeof secondValue === 'string') {

                //check if string has numbers
                if (isNumber(firstValue) === true && isNumber(secondValue) === true) {
                    return Number(firstValue) - Number(secondValue);
                }
                    //check if string has dates
                else if (moment(firstValue).isValid() && moment(secondValue).isValid()) {
                    return moment(firstValue) - moment(secondValue);
                }
                else {
                    //this string can have any alphanumeric values
                    return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase(), Globalize.findClosestCulture().name, { sensitivity: 'base' });
                }
            }
            else if (typeof firstValue === 'number' && typeof secondValue === 'number') {
                return firstValue - secondValue;

            }
            else if (typeof firstValue === 'object' && typeof secondValue === 'object') {
                if (moment(firstValue).isValid() && moment(secondValue).isValid()) {
                    return moment(firstValue) - moment(secondValue);
                }
                else {
                    //any other object, convert to json string and then compare
                    firstValue = JSON.stringify(firstValue);
                    secondValue = JSON.stringify(secondValue);
                    return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase(), Globalize.findClosestCulture().name, { sensitivity: 'base' });
                }
            }
            else {
                //if one is number and other is not
                if (typeof firstValue === 'number' || typeof secondValue === 'number') {
                    if (isNullOrEmpty(firstValue) === true || isNullOrEmpty(secondValue) === true) {
                        if (isNullOrEmpty(firstValue) === true) {
                            firstValue = Number.MIN_VALUE;
                        }
                        else if (isNullOrEmpty(secondValue) === true) {
                            secondValue = Number.MIN_VALUE;
                        }
                        return Number(firstValue) - Number(secondValue);
                    }
                        //if one is string convert number to string
                    else if (typeof firstValue === "string" || typeof secondValue === "string") {
                        //if one is string convert number to string
                        if (typeof firstValue === 'number') {
                            firstValue = firstValue.toString();
                        }
                        else if (typeof secondValue === 'number') {
                            secondValue = secondValue.toString();
                        }
                        return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase(), Globalize.findClosestCulture().name, { sensitivity: 'base' });
                    }
                        //if one is date convert date to number
                    else if ((firstValue instanceof Date) || (secondValue instanceof Date)) {
                        //if one is date convert to number
                        if (firstValue instanceof Date) {
                            firstValue = firstValue.valueOf();
                        }
                        if (secondValue instanceof Date) {
                            secondValue = secondValue.valueOf();
                        }
                        return Number(firstValue) - Number(secondValue);
                    }
                        //if one is object convert number to object
                    else if (typeof firstValue === 'object' || typeof secondValue === 'object') {
                        firstValue = JSON.stringify(firstValue);
                        secondValue = JSON.stringify(secondValue);
                        return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase(), Globalize.findClosestCulture().name, { sensitivity: 'base' });
                    }


                }
                    //if one is date and other is not
                else if ((firstValue instanceof Date) || (secondValue instanceof Date)) {
                    if (isNullOrEmpty(firstValue) === true || isNullOrEmpty(secondValue) === true) {
                        if (isNullOrEmpty(firstValue) === true) {
                            firstValue = moment(1700, 1, 1);
                        }
                        else if (isNullOrEmpty(secondValue) === true) {
                            secondValue = moment(1700, 1, 1);
                        }
                        return moment(firstValue) - moment(secondValue);
                    }
                        //if one is string convert number to string
                    else if (typeof firstValue === "string" || typeof secondValue === "string") {
                        //if one is string convert number to string
                        if (firstValue instanceof Date) {
                            firstValue = firstValue.toString();
                        }
                        else if (secondValue instanceof Date) {
                            secondValue = secondValue.toString();
                        }
                        return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase(), Globalize.findClosestCulture().name, { sensitivity: 'base' });
                    }

                        //if one is object convert number to object
                    else if (typeof firstValue === 'object' || typeof secondValue === 'object') {
                        firstValue = JSON.stringify(firstValue);
                        secondValue = JSON.stringify(secondValue);
                        return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase(), Globalize.findClosestCulture().name, { sensitivity: 'base' });
                    }


                }
                else if (typeof firstValue === 'string' || typeof secondValue === 'string') {
                    if (isNullOrEmpty(firstValue) === true || isNullOrEmpty(secondValue) === true) {
                        if (isNullOrEmpty(firstValue) === true) {
                            firstValue = "";
                        }
                        else if (isNullOrEmpty(secondValue) === true) {
                            secondValue = "";
                        }
                        return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase(), Globalize.findClosestCulture().name, { sensitivity: 'base' });
                    }
                }
                else {
                    //this will be any ohter type like boolean
                    return firstValue < secondValue ? -1 : firstValue > secondValue ? 1 : 0;
                }
            }
        };

        //sort method
        function sort(collection) {
            if (typeof collection === 'undefined' || collection === null)
                return;
            var sortField = collection.sortField();
            var sortDirection = collection.sortDirection();

            if (typeof sortField === 'undefined' || sortField === null || typeof sortDirection === 'undefined' || sortDirection === null)
                return;
            //scan sortField and split on ; to have different fields. put those in array
            var sortFields = sortField.split(';');
            //map collection to another array before sorting
            var mapped = collection();

            //sort the mapped array
            mapped.sort(function (a, b) {
                var compareResult = 0;
                for (var i = 0; i < sortFields.length && i < maximumNestedFieldsToSort; i++) {
                    var firstValue = ko.utils.unwrapObservable(a[sortFields[i]]);
                    var secondValue = ko.utils.unwrapObservable(b[sortFields[i]]);
                    if (sortDirection === 'asc') {
                        compareResult = compare(firstValue, secondValue);
                    }

                    else if (sortDirection === 'desc') {
                        compareResult = compare(secondValue, firstValue);
                    }
                    if (compareResult !== 0) {
                        break;
                    }
                }
                return compareResult;
            });

            //again push sorted array back to observable
            collection(mapped);
        };

        //custom binding for sorting at table header per field
        /* Usability:
         * this binding is required on fields/columns that needs to be sorted.
         * selective columns can have this binding. not necessary that all columns should have.
         * <th id="displayNameLfmHeader" data-bind="sortOn: {fieldName:'DisplayNameLfm', displayAs:'Names',defaultField:true, defaultDirection:'desc'}">
         * only attribute required with sortOn data-bind is fieldName.
         * displayAs is optional attribute-  needed only if you want dropdown to have this field appear with other name than the fieldName. By default dropdown would have same name as fieldName
         * defaultField is optional attribute- if provided then at page-load, table will have this field as default sortable. If none of the fields have this value, data bound to DOM will be not be sorted at all.
         * defaultDirection is optional attribute- works in conjuction with dafaultField, can have two values 'asc' or 'desc'. if not provided, defaultField will be sorted in ascending order.
         * 
         */
        ko.bindingHandlers.sortOn = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                try {
                    var unwrappedValues = ko.unwrap(valueAccessor());
                    var fieldToSortProperty = unwrappedValues.fieldName;
                    var displayAsProperty = unwrappedValues.displayAs;
                    var arrayToSort = bindingContext.contents;
                    var sameCollectionUsed = false;
                    imagePath(bindingContext.imagePath);

                    if (typeof fieldToSortProperty == 'undefined' || fieldToSortProperty == null)
                        return;
                    if (typeof arrayToSort == 'undefined' || arrayToSort == null)
                        return;
                    if (typeof displayAsProperty == 'undefined' || displayAsProperty == null) {
                        displayAsProperty = fieldToSortProperty;
                    }

                    if (fieldToSortProperty.split(';').length > maximumNestedFieldsToSort) {
                        throw new Error(errorMessages[0] + " " + fieldToSortProperty);
                    }
                    var isMobile = bindingContext.isMobile;
                    var isThisDefaultFieldToSort = unwrappedValues.defaultField;
                    var defaultDirection = unwrappedValues.defaultDirection;

                    arrayToSort.sortOptions.push({ 'sortField': fieldToSortProperty, 'displayAs': displayAsProperty, 'sortDirection': 'ascending' });
                    arrayToSort.sortOptions.push({ 'sortField': fieldToSortProperty, 'displayAs': displayAsProperty, 'sortDirection': 'descending' });
                    //add an image of being sortable
                    if (isNullOrEmpty(isMobile) || isMobile === false) {
                        $(element).addClass("sortable");
                    }


                    if (typeof isThisDefaultFieldToSort !== 'undefinied' && isThisDefaultFieldToSort !== null && isThisDefaultFieldToSort === true) {
                        if (typeof defaultDirection == 'undefined' || defaultDirection == null) {
                            defaultDirection = 'asc';
                        }
                        //sort me now
                        ko.bindingHandlers.sortOn.sortMeNow(element, fieldToSortProperty, defaultDirection, arrayToSort, isMobile);
                    }
                    //add a click event to element, change the background image of current element to asc or desc and for all other siblings change it to default image
                    $(element).click(function () {

                        try {
                            $(element).removeClass("sortable");
                            $(element).removeClass("sortable-asc");
                            $(element).removeClass("sortable-desc");
                            if (arrayToSort.sortField() === fieldToSortProperty) {
                                if (arrayToSort.sortDirection() === 'asc') {
                                    $(element).addClass("sortable-desc");
                                    arrayToSort.sortDirection('desc');
                                }
                                else {
                                    $(element).addClass("sortable-asc");
                                    arrayToSort.sortDirection('asc');
                                }
                            }
                            else {
                                //get all the elements that are siblings of $element that have associated sorting class 
                                var siblingElement = $(element).siblings(".sortable-asc, .sortable-desc");
                                $(siblingElement).removeClass("sortable-asc");
                                $(siblingElement).removeClass("sortable-desc");
                                $(siblingElement).addClass("sortable");
                                $(element).addClass("sortable-asc");
                                arrayToSort.sortDirection('asc');
                                arrayToSort.sortField(fieldToSortProperty);
                            }
                        }
                        catch (e) {
                            cosole.error(e.message);
                        }
                    });
                }
                catch (e) {
                    console.error(e.message);
                }
            },


            //added functionality to sort the array by default
            sortMeNow: function (element, fieldToSortProperty, direction, collection, isMobile) {
                try {
                    if (isMobile === false) {
                        $(element).children(".sortable-column-img").remove();
                    }

                    if (direction === 'asc') {
                        if (isMobile === false) {
                            $(element).addClass("sortable-asc");
                        }
                        var s = ko.utils.arrayFirst(collection.sortOptions(), function (item) {
                            return item.sortField === fieldToSortProperty && item.sortDirection === 'ascending';
                        });
                        collection.selectedSortOption(s);
                    }
                    else {
                        if (isMobile === false) {
                            $(element).addClass("sortable-desc");
                        }
                        var s = ko.utils.arrayFirst(collection.sortOptions(), function (item) {
                            return item.sortField === fieldToSortProperty && item.sortDirection === 'descending';
                        });
                        collection.selectedSortOption(s);
                    }
                }
                catch (e) {
                    console.error(e.message);
                }
            },
        };

        //custom binidng for sorting at table
        /*
         * Usability:
         * on tabl element add follwoing data-bind attribute
         * data-bind="sortable:{isMobile:$parent.isMobile(),contents:StudentGrades, imagePath: '@Url.Content("~/Content/Images/")'}"
         * where isMobile is optional, if not provided , there won't be any sorting available in mobile view
         * contents is the observable array that needs to be sorted.
         * in desktop view, column names that are defined as sortable, will appear with sorting images
         * in mobile view, column names that are defined as sortable, will appear in drop down option twice as '<fieldName>, ascending'  and '<fieldName>, descending'
         * Currently only one field or column is sortable. there is no primary or secondary fields.
         * Please note for mobile view, you need to add mobile-view template on your cshtml. This template has dropdown mark-up.
         * @Html.Partial(@"~\Views\Shared\MobileViewSortingTemplate.cshtml")
         */
        ko.bindingHandlers.sortable = {

            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                try {
                    var unwrappedValues = ko.unwrap(valueAccessor());
                    var isMobile = unwrappedValues.isMobile;
                    var arrayToSort = unwrappedValues.contents;
                    var imagePath = unwrappedValues.imagePath;

                    if (typeof arrayToSort.sortField === 'undefined') {

                        arrayToSort.sortField = ko.observable();

                    }
                    if (typeof arrayToSort.sortDirection === 'undefined') {
                        arrayToSort.sortDirection = ko.observable();

                    }
                    if (typeof arrayToSort.sortOptions === 'undefined') {
                        arrayToSort.sortOptions = ko.observableArray([]);
                        arrayToSort.selectedSortOption = ko.observable();
                    }



                    if (arrayToSort.sortField.getSubscriptionsCount() == 0) {
                        arrayToSort.sortField.subscribe(function () {

                            sort(arrayToSort);
                        });
                    }
                    if (arrayToSort.sortDirection.getSubscriptionsCount() == 0) {
                        arrayToSort.sortDirection.subscribe(function () {

                            sort(arrayToSort);
                        });
                    }


                    if (arrayToSort.selectedSortOption.getSubscriptionsCount() == 0) {
                        arrayToSort.selectedSortOption.subscribe(function () {
                            if (arrayToSort.selectedSortOption().sortDirection === 'ascending') {
                                arrayToSort.sortDirection("asc");
                            }
                            else {
                                arrayToSort.sortDirection("desc");
                            }
                            arrayToSort.sortField(arrayToSort.selectedSortOption().sortField);
                        });

                    }

                    //carrying over parent declared value accessors to child binding elements, this will bring contents and isMobile() values available to child elements
                    var childBindingContext = bindingContext.createChildContext(
                bindingContext.$rawData,
                null,
                function (context) {
                    ko.utils.extend(context, valueAccessor());
                });
                    ko.applyBindingsToDescendants(childBindingContext, element);

                    return { controlsDescendantBindings: true };
                }
                catch (e) {
                    console.error(e.message);
                }
            },
            update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                try {
                    var unwrappedValues = ko.unwrap(valueAccessor());
                    var isMobile = unwrappedValues.isMobile;
                    var arrayToSort = unwrappedValues.contents;

                    //check if mobile
                    //append  a template before current element
                    if (isMobile === true) {
                        $(element).prev("").remove("#mobile-sorting-template-id");
                        //this is to add reduntant div element before table
                        var mobileElement = "<div id='placeholder-for-sorting-template'></div>";
                        $(element).before(mobileElement);
                        //above redundant div element was required so that it could be relpaced with the template that is rendered dynamically
                        var result = ko.renderTemplate("mobile-sorting-template", valueAccessor(), {}, $(element).prev("#placeholder-for-sorting-template"), "replaceNode");
                    }
                    else {
                        $(element).prev("").remove("#mobile-sorting-template-id");
                    }


                    if (allBindingsAccessor().hasOwnProperty('dynamicCollection')) {

                        if (typeof arrayToSort !== 'undefined' && arrayToSort !== null && !isNullOrEmpty(arrayToSort.sortField()) && !isNullOrEmpty(arrayToSort.sortDirection())) {
                            sort(arrayToSort);
                        }
                    }
                }
                catch (e) {
                    console.debug(e);
                }

            }
        };

        return {
            sort: sort
        }
    }
    return {
        sorting: sortClass()
    }

}());

// Knockout binding to create a persistent alias that you can reference in child contexts
// ===================================================================================================
// Summary
// ===================================================================================================
// This binding mimics the "as" property of the foreach binding, but it can be used without foreach 
// ===================================================================================================
// Mark-Up Example
// ===================================================================================================
// <div data-bind="let: { myAlias: $data }"> 
// ===================================================================================================
ko.bindingHandlers['let'] = {
    init: function (element, valueAccessor, allBindings, vm, bindingContext) {
        // Make a modified binding context, with extra properties, and apply it to descendant elements
        var innerContext = bindingContext.extend(valueAccessor);
        ko.applyBindingsToDescendants(innerContext, element);

        return { controlsDescendantBindings: true };
    }
};
ko.virtualElements.allowedBindings['let'] = true;

