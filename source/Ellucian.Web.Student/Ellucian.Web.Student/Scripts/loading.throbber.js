﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

// Initialize the throbber
var throbber = null;

// Creates the throbber, appends it to the DOM, and starts it
function startThrobber() {
    try {
        if (throbber == null) {
            throbber = new Throbber({ color: '#555555', size: 24 });
            throbber.appendTo(document.getElementById('loading-throbber'));
            throbber.start();
        }
    } catch (e) { }
}

// Stops the throbber
function stopThrobber() {
    try {
        throbber.stop();
        $("#loading-throbber").hide();
        $("#loading-throbber-text").hide();
    } catch (e) { }
}