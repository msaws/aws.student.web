﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates

function catalogSearchSubjectsViewModel() {
    var self = this;
    
    this.subjects = ko.observableArray();
    this.filter = ko.observable("");


    this.filteredSubjects = ko.computed(function () {
       
            return ko.utils.arrayFilter(self.subjects(), function (subject) {
                return subject.Description().toUpperCase().indexOf(self.filter().toUpperCase()) !== -1;

            });
       
    });


    this.isLoaded = ko.observable(false);

    // catalogResultsDisplayed is used by embedded (asynchronous) course catalogs.  It subscribes to the
    // "catalogResultDisplayed" topic.  When a search result is displayed, the process displaying the result should
    // post a message (with value of true) to the "catalogResultDisplayed" topic, allowing the catalog index to hide.
    this.catalogResultsDisplayed = ko.observable(false).subscribeTo("catalogResultDisplayed");

};

// searchUrl = string - the url to which to submit search requests
// searchAsync = boolean - submit search request async via Ajax, or sync as a page request
function subjectModel(data, baseUrl, searchAsync) {
    ko.mapping.fromJS(data, {}, this);

    var self = this;

    this.searchUrl = ko.observable(baseUrl + "?subjects=" + encodeURIComponent(self.Code()));

    this.searchAsync = ko.observable(searchAsync).publishOn("searchAsync");

    // When a search result is requested and received, publish to the "catalogResult" topic,
    // allowing the result's view model to display the data.
    this.catalogResult = ko.observable(null).publishOn("catalogResult", true);

    // catalogResultsDisplayed is used by embedded (asynchronous) course catalogs.  It posts to the
    // "catalogResultDisplayed" topic.  When a search result is displayed, post a message (with value of true) 
    // to the "catalogResultDisplayed" topic, allowing the catalog index to hide.
    this.catalogResultsDisplayed = ko.observable(false).publishOn("catalogResultDisplayed", true);

    this.clickHandler = function () {

        if (self.searchAsync() == false) {
            return true;
        } else {
            var query = "";
            if (window.location.href.indexOf('?') !== -1) {
                query = window.location.href.slice(window.location.href.indexOf('?'));
            }
            var searchUrl = self.searchUrl() + query;
            $.ajax({
                url: searchUrl,
                type: "GET",
                contentType: jsonContentType,
                dataType: "json",
                success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        self.catalogResult(data);
                        if (self.catalogResultsDisplayed() !== true) {
                            self.catalogResultsDisplayed(true);
                        } else {
                            self.catalogResultsDisplayed.valueHasMutated();
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0)
                        $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve search results.", type: "error" });
                },
                complete: function (jqXHR, textStatus) {

                }
            });
            return false;
        }
    };
}