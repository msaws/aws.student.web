﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.//
/////////////////////////////////////////////////////////////////////////////////////////////
//  This file contains utility functionality that needs to be made available anywhere a
//  course or section can be shown in a full details view, with the option of being able to
//  add said course or section to a plan.
/////////////////////////////////////////////////////////////////////////////////////////////

// Callback methods for adding a Section or Course from the Details dialog
var addSectionCallback = null;
var addCourseCallback = null;
var updateCourseCallback = null;

// Track section being modified
var currentSection = null;
var currentCourse = null;
// Track course being modified and its original term
var currentCourseOriginalTerm = null;

// Track the last element that was focused so that a screen reader isn't thrown off after they close a dialog box
var lastFocus = null;


// Default options set for calling one of the Details functions
var defaultDetailsOptions = {
    course: null,
    allowAdding: false,
    allowUpdating: false,
    addTerms: null,
    addCallback: null,
    creditsOverride: null,
    gradingTypeOverride: null
};

// Keep track of AJAX requests
var xhr = null;

// Proxy function to handle showing either Course or Section details, based upon what is available
this.showCourseOrSectionDetails = function (options) {
    var options = $.extend({}, defaultDetailsOptions, options);
    var sec = ko.utils.unwrapObservable(options.course.Section);
     if (sec != null) {
          options.section = options.course.Section;
          showSectionDetails(options);
     }
     else if (!isNullOrEmpty(options.course)) {
          showCourseDetails(options);
     }
}

// Course details dialog, which can be called as a Partial View anywhere
this.showCourseDetails = function (options) {
     var options = $.extend({}, defaultDetailsOptions, options);
     lastFocus = document.activeElement;

     if (!isNullOrEmpty(options.course) && typeof options.course !== 'function') {
          if (options.allowAdding) {
               addCourseCallback = options.addCallback;
               addSectionCallback = null;
          }
          else {
               addCourseCallback = null;
               addSectionCallback = null;
          }

          if (options.allowUpdating) {
               updateCourseCallback = options.updateCallback;
          } else {
               updateCourseCallback = null;
          }

          currentCourse = options.course;

          // Title
          var fullTitle = options.course.SubjectCode() + Ellucian.Course.courseDelimiter + options.course.Number() + ": " + options.course.Title();
          cvm.FullTitle(fullTitle);

          // Description
          if (isNullOrEmpty(options.course.Description())) {
               cvm.Description(Ellucian.Course.SectionDetails.courseDescriptionUnavailable);
          }
          else {
               cvm.Description(options.course.Description());
          }

          // Terms and Years offered
          if (isNullOrEmpty(options.course.TermsOffered())) {
               cvm.TermsOffered(null);
          } else {
               cvm.TermsOffered(Ellucian.Course.SectionCourseCommonDetails.CourseTypicallyOffered.format(options.course.TermsOffered()));
          }
          if (isNullOrEmpty(options.course.YearsOffered())) {
               cvm.YearsOffered(null);
          }
          else {
              cvm.YearsOffered(Ellucian.Course.SectionCourseCommonDetails.CourseTypicallyOffered.format(options.course.YearsOffered()));
          }

          // Credits
          if (options.creditsOverride) {
               cvm.IsStaticCredits(true);
               cvm.CreditsStatic(options.creditsOverride);
               cvm.IsDropDownCredits(false);
               cvm.IsFreeFormCredits(false);
               cvm.CreditsLabel("");
          }
          else if (options.allowAdding) {
               // Scenario for adding a course
               // Build and inject the credits Html based on whether this course offer variable credits
               if (isCourseOrSectionVariableCredit(ko.mapping.toJS(options.course))) {
                    // Build the array of values to choose from
                    var interval = options.course.VariableCreditIncrement();
                    var min = options.course.MinimumCredits();
                    var max = options.course.MaximumCredits();

                    // Increment can be null, in which case the user can type any value they want
                    if (interval != null) {
                         var values = new Array();
                         for (var i = min; i < max; i += interval) {
                              // javascript math with floating decimals is not precise - need to round back to 5 digits.
                              var j = Math.round(i * 100000) / 100000;
                              values.push(j);
                         }
                         values.push(max);

                         // dynamically re-inject the options set for the credits drop-down
                         for (var i = 0; i < values.length; i++) {
                              cvm.CreditsVariableIncrements.push(values[i]);
                         }
                         cvm.CreditsVariableIncrements();
                         cvm.IsDropDownCredits(true);
                         cvm.IsFreeFormCredits(false);
                         cvm.IsStaticCredits(false);
                    } else {
                         cvm.IsDropDownCredits(false);
                         cvm.IsFreeFormCredits(true);;
                         cvm.FreeFormCreditsValue(min);
                         cvm.IsStaticCredits(false);
                         setTimeout(function () { $("#coursedetails-freeformcredits").select(); }, 100);
                    }
                    cvm.CreditsLabel(Ellucian.Course.SectionCourseCommonDetails.MinimumToMaximumCreditsFormatString.format(min, max));
               } else {
                    // Just show the concrete number of credits
                    var credits = courseOrSectionCreditsCeusDisplay(ko.mapping.toJS(options.course));
                    var creditString = "";
                    if (isNullOrEmpty(credits)) {
                         creditString = Ellucian.Course.SectionCourseCommonDetails.ToBeDeterminedAbbreviation;
                    } else {
                         creditString = credits;
                    }
                    cvm.IsDropDownCredits(false);
                    cvm.CreditsLabel("");
                    cvm.IsFreeFormCredits(false);
                    cvm.IsStaticCredits(true);
                    cvm.CreditsStatic(creditString);
               }
          }
          else {
               cvm.IsDropDownCredits(false);
               cvm.IsFreeFormCredits(false);
               cvm.CreditsLabel("");
               cvm.IsStaticCredits(true);
               cvm.CreditsStatic(options.course.CreditsCeusDisplay());
          }

          if (options.allowAdding || options.allowUpdating) {
               // Fill in the Term selection drop-down
               if (options.addTerms != null) {
                    cvm.AddTerms.removeAll();
                    currentCourseOriginalTerm = options.selectedTerm;
                    for (var i = 0; i < options.addTerms.length; i++) {
                         cvm.AddTerms.push({ code: options.addTerms[i].Code, description: options.addTerms[i].Description });
                    }
                    if (!isNullOrEmpty(options.selectedTerm)) {
                         try {
                              cvm.SelectedTerm(options.selectedTerm);
                         } catch (e) { }
                    }
               }
          }

          // Toggle Add/Update buttons based on flag
          cvm.AllowAdding(options.allowAdding);
          cvm.AllowUpdating(options.allowUpdating);

          // Retrieve the rest of the data as needed and fill in when it arrives
          cvm.Requisites.removeAll();
          cvm.Locations.removeAll();
          cvm.isLoading(true);

          // Show the dialog now
          cvm.courseIsVisible(true);

          // Get the rest of the data via ajax
          var jsonData = { courseId: options.course.Id() };
          if (xhr && xhr.readyState != 4) {
               xhr.abort();
          }
          xhr = $.ajax({
               url: courseDetailsActionUrl,
               data: JSON.stringify(jsonData),
               type: "POST",
               contentType: jsonContentType,
               dataType: "json",
               success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {

                         cvm.Requisites.removeAll();
                         if (data.RequisiteItems) {
                              for (var i = 0; i < data.RequisiteItems.length; i++) {
                                   cvm.Requisites.push(data.RequisiteItems[i]);
                              }
                         }

                         cvm.Locations.removeAll();
                         if (data.LocationItems && data.LocationItems.length > 0) {
                              for (i = 0; i < data.LocationItems.length; i++) {
                                   cvm.Locations.push(data.LocationItems[i]);
                              }
                         }

                         cvm.LocationCycleRestrictionDescriptions.removeAll();
                         if (data.LocationCycleRestrictionDescriptions && data.LocationCycleRestrictionDescriptions.length > 0) {
                             for (i = 0; i < data.LocationCycleRestrictionDescriptions.length; i++) {
                                 cvm.LocationCycleRestrictionDescriptions.push(data.LocationCycleRestrictionDescriptions[i]);
                             }
                             cvm.showLocationCycleRestrictionDescriptions(true);
                         }

                    }
               },
               error: function (jqXHR, textStatus, errorThrown) {
                    // Mark the failed items as Unknown
                    cvm.Requisites.removeAll();
                    cvm.Locations.removeAll();
               },
               complete: function () {
                   cvm.isLoading(false);
               }
          });
     }
}

this.showSectionDetails = function (options) {
     var options = $.extend({}, defaultDetailsOptions, options);
     lastFocus = document.activeElement;

     if (!isNullOrEmpty(options.course) && typeof options.course !== 'function' && !isNullOrEmpty(options.section) && typeof options.section !== 'function') {
          currentSection = ko.mapping.toJS(options.section);

          if (options.allowAdding) {
               addSectionCallback = options.addCallback;
               addCourseCallback = null;
          }
          else {
               addSectionCallback = null;
               addCourseCallback = null;
          }

          // Toggle Add and Update buttons based on flag
          svm.AllowAdding(options.allowAdding);

          // Set the things which can be set right away

          svm.canSkipWaitlist(options.canSkipWaitlist);

          // Title
          var fullTitle = options.course.SubjectCode() + Ellucian.Course.courseDelimiter + options.course.Number() + Ellucian.Course.courseDelimiter + currentSection.Number + ": " + currentSection.Title;
          svm.FullTitle(fullTitle);
          // Description
          if (isNullOrEmpty(options.course.Description())) {
              svm.Description(Ellucian.Course.SectionDetails.courseDescriptionUnavailable);
          }
          else {
               svm.Description(options.course.Description());
          }

          // Credits
          if (options.creditsOverride && !options.allowAdding) {
               svm.IsStaticCredits(true);
               svm.CreditsStatic(options.creditsOverride);
               svm.IsDropDownCredits(false);
               svm.IsFreeFormCredits(false);
               svm.CreditsLabel("");
          }
          else if (options.allowAdding) {
               // Scenario for adding a section
               // Build and inject the credits Html based on whether this section offer variable credits
               if (isCourseOrSectionVariableCredit(ko.toJS(currentSection))) {
                    // Build the array of values to choose from
                    var interval = currentSection.VariableCreditIncrement;
                    var min = currentSection.MinimumCredits;
                    var max = currentSection.MaximumCredits;

                    // Increment can be null, in which case the user can type any value they want
                    if (interval != null) {
                         var values = new Array();
                         for (var i = min; i < max; i += interval) {
                              // javascript math with floating decimals is not precise - need to round back to 5 digits.
                              var j = Math.round(i * 100000) / 100000;
                              values.push(j);
                         }
                         values.push(max);

                         // dynamically re-inject the options set for the credits drop-down
                         svm.CreditsVariableIncrements.removeAll();
                         for (var i = 0; i < values.length; i++) {
                              svm.CreditsVariableIncrements.push(values[i]);
                         }
                         //svm.CreditsVariableIncrements();
                         svm.IsDropDownCredits(true);
                         svm.IsFreeFormCredits(false);
                         svm.IsStaticCredits(false);
                    } else {
                         svm.IsDropDownCredits(false);
                         svm.IsFreeFormCredits(true);;
                         svm.FreeFormCreditsValue(min);
                         svm.IsStaticCredits(false);
                         setTimeout(function () { $("#sectiondetails-freeformcredits").select(); }, 100);
                    }
                    svm.CreditsLabel(Ellucian.Course.SectionCourseCommonDetails.MinimumToMaximumCreditsFormatString.format(min, max));
               } else {
                    // Just show the concrete number of credits
                    var credits = courseOrSectionCreditsCeusDisplay(ko.mapping.toJS(currentSection));
                    var creditString = "";
                    if (isNullOrEmpty(credits)) {
                         creditString = Ellucian.Course.SectionCourseCommonDetails.ToBeDeterminedAbbreviation;
                    } else {
                         creditString = credits;
                    }
                    svm.IsDropDownCredits(false);
                    svm.CreditsLabel("");
                    svm.IsFreeFormCredits(false);
                    svm.IsStaticCredits(true);
                    svm.CreditsStatic(creditString);
               }
          }
          else {
               //when user can neither add nor update, and no override is provided, just use a default static display
               svm.IsDropDownCredits(false);
               svm.IsFreeFormCredits(false);
               svm.CreditsLabel("");
               svm.IsStaticCredits(true);
               svm.CreditsStatic(courseOrSectionCreditsCeusDisplay(ko.mapping.toJS(currentSection)));
          }

          // Grading Type - show drop-down if in edit mode, else just show text
          if (svm.AllowAdding()) {
               svm.IsStaticGrading(false);

               svm.GradingOptions.removeAll();
               if (currentSection.OnlyPassNoPass) {
                   svm.GradingOptions.push( { Description: Ellucian.Course.SectionDetails.stringPassFailGrading, Code: Ellucian.Course.SectionDetails.stringPassFailGradingCode });
               }
               else {
                   svm.GradingOptions.push( {Description:Ellucian.Course.SectionDetails.stringGradedGrading, Code:Ellucian.Course.SectionDetails.stringGradedGradingCode});
                    if (currentSection.AllowPassNoPass) {
                        svm.GradingOptions.push( { Description: Ellucian.Course.SectionDetails.stringPassFailGrading, Code: Ellucian.Course.SectionDetails.stringPassFailGradingCode });
                    }
                    if (currentSection.AllowAudit) {
                        svm.GradingOptions.push( { Description: Ellucian.Course.SectionDetails.stringAuditGrading, Code: Ellucian.Course.SectionDetails.stringAuditGradingCode });
                    }
               }
               if (options.gradingTypeOverride !== null) {
                   svm.gradingSelected(options.gradingTypeOverride);
               }
          } else {
               svm.IsStaticGrading(true);
               if (options.gradingTypeOverride !== null) {
                   var gradeString = Ellucian.Course.SectionDetails.stringGradedGrading;
                   if (options.gradingTypeOverride == Ellucian.Course.SectionDetails.stringAuditGradingCode) {
                         gradeString = Ellucian.Course.SectionDetails.stringAuditGrading;
                         svm.IsStaticCredits(true);
                    }
                    else if (options.gradingTypeOverride == Ellucian.Course.SectionDetails.stringPassFailGradingCode) { 
                        gradeString = Ellucian.Course.SectionDetails.stringPassFailGrading;
                    }
                    svm.GradingStatic(gradeString);
               }
               else {
                    var gTypes = new Array();
                    if (currentSection.OnlyPassNoPass) {
                        gTypes.push(Ellucian.Course.SectionDetails.stringPassFailGrading);
                    }
                    else {
                        gTypes.push(Ellucian.Course.SectionDetails.stringGradedGrading);
                         if (currentSection.AllowPassNoPass) {
                             gTypes.push(Ellucian.Course.SectionDetails.stringPassFailGrading);
                         }
                         if (currentSection.AllowAudit) {
                              gTypes.push(Ellucian.Course.SectionDetails.stringAuditGrading);
                         }
                    }
                    svm.GradingStatic(gTypes.join(", "));
               }
          }

          // Seats and Waitlist
          svm.Capacity(currentSection.Capacity);
          svm.Available(currentSection.Available);
          svm.WaitlistAvailable(currentSection.WaitlistAvailable);
          svm.Waitlisted(currentSection.Waitlisted);

          // Set the TBD items temporarily
          svm.Corequisites.removeAll();
          svm.Requisites.removeAll();
          svm.TimeLocations.removeAll();
          svm.Dates(Ellucian.Course.SectionCourseCommonDetails.ToBeDeterminedAbbreviation);
          svm.Instructors.removeAll();
          svm.BooksTotal("");
          svm.BookstoreUrl(null);
          svm.TermDisplay("");
          svm.TopicCodeDescription("");

          svm.isLoading(true);

          // Show the dialog now
            svm.sectionIsVisible(true);

          // Ajax call to get the rest
          var jsonData = { sectionId: currentSection.Id, studentId: options.studentId };
          if (xhr && xhr.readyState != 4) {
               xhr.abort();
          }
          xhr = $.ajax({
               url: Ellucian.Course.ActionUrls.sectionDetailsActionUrl,
               data: JSON.stringify(jsonData),
               type: "POST",
               contentType: jsonContentType,
               dataType: "json",
               success: function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {

                         // Section Corequisites
                         svm.Corequisites.removeAll();
                         if (data.CorequisiteItems) {
                              for (i = 0; i < data.CorequisiteItems.length; i++) {
                                   svm.Corequisites.push(data.CorequisiteItems[i]);
                              }
                         }

                         // Requisites
                         svm.Requisites.removeAll();
                         if (data.RequisiteItems) {
                              for (i = 0; i < data.RequisiteItems.length; i++) {
                                   svm.Requisites.push(data.RequisiteItems[i]);
                              }
                         }

                         // Times & Locations
                         if (data.TimeLocationItems) {
                              for (i = 0; i < data.TimeLocationItems.length; i++) {
                                   svm.TimeLocations.push({ Time: (data.TimeLocationItems[i].Time), Location: (data.TimeLocationItems[i].Location), Dates: (data.TimeLocationItems[i].Dates), Counter: (i + 1) });
                              }
                         }

                         // Dates
                         svm.Dates(data.DatesDisplay);

                         // Term
                         svm.TermDisplay(data.TermDisplay);

                         // Instructors
                         svm.Instructors.removeAll();
                         if (data.InstructorItems && data.InstructorItems.length > 0) {
                              for (i = 0; i < data.InstructorItems.length; i++) {
                                   var name = data.InstructorItems[i].Name;
                                   var email = "";
                                   var phone = "";
                                   if (data.InstructorItems[i].EmailAddresses && data.InstructorItems[i].EmailAddresses.length > 0) {
                                        email = data.InstructorItems[i].EmailAddresses[0];
                                   }
                                   if (data.InstructorItems[i].PhoneNumbers && data.InstructorItems[i].PhoneNumbers.length > 0) {
                                        phone = data.InstructorItems[i].PhoneNumbers[0];
                                   }
                                   svm.Instructors.push({ Name: name, Email: email, Phone: phone });
                              }
                         }
                         // Book Information
                         svm.BooksTotal(data.BooksTotal);
                         svm.BookstoreUrl(data.BookstoreUrl);

                         // Seats and Waitlist - this is set initially, but update it here in case it has changed (comes for free on the DTO anyways)
                         svm.Capacity(data.Capacity);
                         svm.Available(data.Available);
                         svm.WaitlistAvailable(data.WaitlistAvailable);
                         svm.Waitlisted(data.Waitlisted);
                         svm.TopicCodeDescription(data.TopicCodeDescription);

                         // GradingOptions
                         svm.GradingOptions.removeAll();
                         if (data.OnlyPassNoPass) {
                              if (data.PassNoPassIsRestricted == false) {
                                  svm.GradingOptions.push( { Description: Ellucian.Course.SectionDetails.stringPassFailGrading, Code: Ellucian.Course.SectionDetails.stringPassFailGradingCode });
                              } else {
                                   // Make sure allow adding is set to false since they don't have the option.
                                   svm.AllowAdding(false);
                              }

                         }
                         else {
                             svm.GradingOptions.push( { Description: Ellucian.Course.SectionDetails.stringGradedGrading, Code: Ellucian.Course.SectionDetails.stringGradedGradingCode });
                              if (data.AllowPassNoPass && data.PassNoPassIsRestricted == false) {
                                  svm.GradingOptions.push( { Description: Ellucian.Course.SectionDetails.stringPassFailGrading, Code: Ellucian.Course.SectionDetails.stringPassFailGradingCode });
                              }
                              if (data.AllowAudit && data.AuditIsRestricted == false) {
                                  svm.GradingOptions.push( { Description: Ellucian.Course.SectionDetails.stringAuditGrading, Code: Ellucian.Course.SectionDetails.stringAuditGradingCode });
                              }
                         }
                        
                         svm.GradingOptionsMatch(data.GradingOptionsMatch);

                         svm.Comments(data.Comments);
                         svm.TransferStatusDescription(data.TransferStatusDescription);

                    }
               },
               error: function (jqXHR, textStatus, errorThrown) {
                    // Mark the failed items as Unknown
                    svm.Corequisites.removeAll();
                    svm.Requisites.removeAll();
                    svm.TimeLocations.removeAll();
                    svm.Dates(Ellucian.Course.SectionCourseCommonDetails.ToBeDeterminedAbbreviation);
                    svm.Instructors.removeAll();
                    svm.BooksTotal("");
                    svm.BookstoreUrl(null);
                    //leave Seat counts unchanged from the initial set

               },
               complete: function () {
                   // Hide the spinner
                   svm.isLoading(false);
               }
          });
     }
}

// Handle clicking Add in Section Details dialog
this.addSectionDetails = function () {
     if (typeof (addSectionCallback) == typeof (Function)) {
          var credits = computeChosenSectionCredits();
          var grading = computeChosenSectionGrading();
          addSectionCallback({ chosenCredits: credits, chosenGradingType: grading, chosenTerm: currentSection.TermId });
     }
}

this.computeChosenSectionCredits = function () {
     var credits = null;
     if ($("#sectiondetails-selectvarcredit").is(":visible")) {
          credits = $("#sectiondetails-selectvarcredit").val();
     }
     else if ($("#sectiondetails-freeformcredits").is(":visible")) {
          credits = $("#sectiondetails-freeformcredits").val();
     }
     else {
          credits = $("#sectiondetails-creditsstatic").text();
     }
     return credits;
}

this.computeChosenSectionGrading = function () {
    if(svm.GradingOptions() && svm.GradingOptions().length==1)
    {
        return svm.GradingOptions()[0].Code;
    }
     return svm.gradingSelected();
}

// Handle clicking Add in Course Details dialog
this.addCourseDetails = function () {
     if (typeof (addCourseCallback) == typeof (Function)) {
          var credits = null;
          if ($("#coursedetails-selectvarcredit").is(":visible")) {
               credits = $("#coursedetails-selectvarcredit").val();
          }
          else if ($("#coursedetails-freeformcredits").is(":visible")) {
               credits = $("#coursedetails-freeformcredits").val();
          }
          else {
               credits = $("#coursedetails-creditsstatic").text();
          }
          var term = $("#coursedetails-selectterm").val();
          addCourseCallback({ chosenCredits: credits, chosenTerm: term });
          cvm.SelectedTerm(null);
     }
}

// Handle clicking Update in Course Details dialog
this.updateCourseDetails = function () {
     if (typeof updateCourseCallback === "function") {
          var newTerm = $("#coursedetails-selectterm").val();
          updateCourseCallback({ courseId: currentCourse.Id(), oldTerm: currentCourseOriginalTerm, newTerm: newTerm });
     }
}

// Focus in input field of Add Course dialog
this.focusAddingCourse = function () {
     if ($("#coursedetails-freeformcredits").is(":visible")) {
          $("#coursedetails-freeformcredits").focus();
     }
     else if ($("#coursedetails-selectvarcredit").is(":visible")) {
          $("#coursedetails-selectvarcredit").focus();
     }
}

// Focus in input field of Add Section dialog
this.focusAddingSection = function () {
     if ($("#sectiondetails-freeformcredits").is(":visible")) {
          $("#sectiondetails-freeformcredits").focus();
     }
     else if ($("#sectiondetails-selectvarcredit").is(":visible")) {
          $("#sectiondetails-selectvarcredit").focus();
     }
     else if ($("#sectiondetails-selectgrading").is(":visible")) {
          $("#sectiondetails-selectgrading").focus();
     }
}

// Close Course details dialog
this.closeCourseDetails = function () {
    cvm.courseIsVisible(false);
    cvm.showLocationCycleRestrictionDescriptions(false);
     cvm.SelectedTerm(null);
}

// Close Section details dialog
this.closeSectionDetails = function () {
    svm.sectionIsVisible(false);
}

// Close either of the Course or Section details dialogs; use this if you called showCourseOrSectionDetails() to open a dialog
this.closeCourseOrSectionDetails = function () {
     if (dialogContainer) {
          dialogContainer.dialog("close");
          if (xhr && xhr.readyState != 4) {
               xhr.abort();
          }
     }
}

// Catch Enter key on Add dialogs
this.checkForCourseAddEnter = function (e) {
     if (e.keyCode == 13) {
          addCourseDetails();
          return false;
     }
}
this.checkForSectionAddEnter = function (e) {
     if (e.keyCode == 13) {
          addSectionDetails();
          return false;
     }
}

// Course Details ViewModel definition
function courseDetailsViewModel() {
    var self = this;

    this.FullTitle = ko.observable();
    this.Description = ko.observable();
    this.TermsOffered = ko.observable();
    this.YearsOffered = ko.observable();
    this.CreditsStatic = ko.observable();
    this.CreditsVariableIncrements = ko.observableArray();
    this.IsFreeFormCredits = ko.observable(false);
    this.IsStaticCredits = ko.observable(false);
    this.IsDropDownCredits = ko.observable(false);
    this.FreeFormCreditsValue = ko.observable();
    this.CreditsLabel = ko.observable();
    this.AllowAdding = ko.observable();
    this.Locations = ko.observableArray();
    this.AddTerms = ko.observableArray();
    this.AllowUpdating = ko.observable();
    this.Requisites = ko.observableArray();
    this.LocationCycleRestrictionDescriptions = ko.observableArray();
    this.SelectedTerm = ko.observable(null);

    this.showLocationCycleRestrictionDescriptions = ko.observable(false);

    this.showTermsOffered = ko.computed(function () {
        return !this.showLocationCycleRestrictionDescriptions() && !isNullOrEmpty(this.TermsOffered());
    }, this);


    this.showYearsOffered = ko.computed(function () {
        return !this.showLocationCycleRestrictionDescriptions() && !isNullOrEmpty(this.YearsOffered());
    }, this);
    this.isLoading = ko.observable(true);

    this.courseIsVisible = ko.observable(false);

    this.disableCourseAddButton = ko.pureComputed(function () {
        return !self.SelectedTerm();
    })

    this.courseDetailsAddButton = {
        id: 'coursedetails-add',
        title: Ellucian.Course.SectionCourseCommonDetails.CourseAddButtonLabel,
        isPrimary: true,
        visible: self.AllowAdding,
        disabled: self.disableCourseAddButton,
        callback: addCourseDetails
    };

    this.courseDetailsUpdateButton = {
        id: 'coursedetails-update',
        title: Ellucian.Course.SectionCourseCommonDetails.CourseUpdateButtonLabel,
        visible: self.AllowUpdating,
        isPrimary: true,
        callback: updateCourseDetails
    };

    this.closeCourseDetailsButton = {
        id: 'coursedetails-close',
        title: Ellucian.Course.SectionCourseCommonDetails.CloseButton,
        aria: Ellucian.Course.SectionCourseCommonDetails.CourseCloseTitle,
        isPrimary: false,
        callback: closeCourseDetails
    };
}

function sectionDetailsViewModel() {
    var self = this;
    this.gradingSelected = ko.observable();
    this.FullTitle = ko.observable();
    this.Description = ko.observable();
    this.CreditsStatic = ko.observable();
    this.CreditsVariableIncrements = ko.observableArray();
    this.IsFreeFormCredits = ko.observable(false);
    this.IsStaticCredits = ko.observable(false);
    this.IsDropDownCredits = ko.observable(false);
    this.FreeFormCreditsValue = ko.observable();
    this.GradingOptions = ko.observableArray();
    this.IsStaticGrading = ko.observable(false);
    this.GradingStatic = ko.observable();
    this.GradingOptionsMatch = ko.observable(true);
    this.CreditsLabel = ko.observable();
    this.Corequisites = ko.observableArray();
    this.Requisites = ko.observableArray();
    this.TimeLocations = ko.observableArray();
    this.Dates = ko.observable();
    this.Instructors = ko.observableArray();
    this.BooksTotal = ko.observable();
    this.BookstoreUrl = ko.observable();
    this.AllowAdding = ko.observable();
    this.TermDisplay = ko.observable();
    this.Capacity = ko.observable();
    this.Available = ko.observable();
    this.WaitlistAvailable = ko.observable(false);
    this.Waitlisted = ko.observable();
    this.TopicCodeDescription = ko.observable();
    this.gradingStaticValue = ko.pureComputed(function () {
        if (self.IsStaticGrading()) {
            return self.GradingStatic();
        }
        else if (self.GradingOptions() && self.GradingOptions().length == 1 && self.GradingOptions()[0] !== undefined && self.GradingOptions()[0].Description !== undefined) {
            return self.GradingOptions()[0].Description;
        }
        else
            return "";

    }).extend({ deferred: true });

    this.SeatlistString = ko.computed(function () {
        return Ellucian.Course.SectionCourseCommonDetails.SeatlistFormatString.format(self.Available(), self.Capacity());
    });
    this.IsUnlimitedSeating = ko.computed(function () {
        return self.Available() === null || self.Capacity() === null;
    });
    this.Comments = ko.observable();
    this.TransferStatusDescription = ko.observable();
    this.canSkipWaitlist = ko.observable(false);
    this.isLoading = ko.observable(false);

    this.sectionIsVisible = ko.observable(false);

    this.addSectionDetailsButton = {
        id: 'addSectionDetailsButton',
        title: Ellucian.Course.SectionCourseCommonDetails.SectionAddButtonLabel,
        isPrimary: true,
        visible: self.AllowAdding,
        callback: addSectionDetails
    };

    this.closeSectionDetailsButton = {
        id: 'closeSectionDetailsButton',
        title: Ellucian.Course.SectionCourseCommonDetails.CloseButton,
        aria: Ellucian.Course.SectionCourseCommonDetails.SectionCloseTitle,
        isPrimary: false,
        callback: closeSectionDetails
    };
}

//View Model Instances
var cvm = new courseDetailsViewModel();
var svm = new sectionDetailsViewModel();

