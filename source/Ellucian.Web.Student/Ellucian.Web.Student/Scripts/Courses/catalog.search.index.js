﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates

function catalogSearchViewModel() {
    var self = this;
    self.catalogAdvancedSearchViewModelInstance = new Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchViewModel();
    self.catalogSearchSubjectViewModelInstance = new catalogSearchSubjectsViewModel();
    self.TabSelected = ko.observable(1);
    self.initialization = function () {
         $.ajax({
        url: Ellucian.Course.ActionUrls.getAdvancedSearchUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, {}, self.catalogAdvancedSearchViewModelInstance);
                var subjects = [];
                for (var i = 0; i < data.Subjects.length; i++) {
                    subjects.push(new subjectModel(data.Subjects[i], Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl, false));

                }
                self.catalogSearchSubjectViewModelInstance.subjects.pushAll(subjects);
                
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve search options.", type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            self.catalogAdvancedSearchViewModelInstance.isLoaded(true);
            self.catalogSearchSubjectViewModelInstance.isLoaded(true);
        }
    });
};
};
var catalogSearchViewInstance = new catalogSearchViewModel();


$(document).ready(function () {
    ko.applyBindings(catalogSearchViewInstance, document.getElementById("main"));
    catalogSearchViewInstance.initialization();
    $("#coursecatalog-filter").watermark(" Type a subject...");
   
});