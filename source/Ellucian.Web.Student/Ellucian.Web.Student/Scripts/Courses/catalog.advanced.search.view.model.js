﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates
Ellucian.CatalogAdvancedSearch = Ellucian.CatalogAdvancedSearch || {};

Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchViewModel=function() {
    var self = this;
    
    BaseViewModel.call(self, 769);
    // Overrides for base view model change functions to remove alerts - do nothing
    self.changeToDesktop = function () { }
    self.changeToMobile = function () { }
    // Check for mobile and set the isMobile observable as determined (defined in BaseViewModel.
    self.checkForMobile(window, document);

    this.Subjects = ko.observableArray();
    this.Terms = ko.observableArray();
    this.Locations = ko.observableArray();
    this.AcademicLevels = ko.observableArray();
    this.CourseTypes = ko.observableArray();
    this.TimeRanges = ko.observableArray();
    this.DaysOfWeek = ko.observableArray();
    this.LatestSearchDate = ko.observable(null);
    this.EarliestSearchDate = ko.observable(null);
    this.catalogAdvancedSearchDataModelInstance = new Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchDataModel(self);

    this.isLoaded = ko.observable(false);
    this.isSubmissionInProgress = ko.observable(false);

    this.dirtyFlag = new ko.dirtyFlag(self.catalogAdvancedSearchDataModelInstance, false);

    //all labels and messages
    this.labels={
        academicLevelLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.academicLevelLabel),
    addCourseButtonLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.addCourseButtonLabel),
    clearButtonLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.clearButtonLabel),
    dayOfWeekLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.dayOfWeekLabel),
    endMeetingDateLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.endMeetingDateLabel),
    headerText:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.headerText),
    locationLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.locationLabel),
    searchButtonLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.searchButtonLabel),
    startMeetingDateLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.startMeetingDateLabel),
    subjectSelectCaption:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.subjectSelectCaption),
    termLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.termLabel),
    termSelectCaption:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.termSelectCaption),
    timeOfDayLabel:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.timeOfDayLabel),
    timeOfDayCaption:ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.timeOfDayCaption),
    courseLabel : ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.courseLabel),
    locationSelectCaption : ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.locationSelectCaption),
    academicLevelSelectCaption: ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.academicLevelSelectCaption),
    courseTypeLabel:  ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.courseTypeLabel),
    courseTypeSelectCaption: ko.observable(Ellucian.CatalogAdvancedSearch.MarkupLabels.courseTypeSelectCaption)
    };


    this.areDaysSelected = ko.computed(function () {
        
            //move selected days to course search data model
            for (var i = 0; i < self.DaysOfWeek().length; i++) {
                if (self.DaysOfWeek()[i].Item3() == true) {
                    return true;
                }
            }
            return false;
    });

    this.isSearchEnabled = ko.computed(function () {
        return self.dirtyFlag.isDirty() || self.areDaysSelected();
    });

    this.submit = function () {
        try {
            self.isSubmissionInProgress(true);
                //move selected days to course search data model
                    for (var i = 0; i < self.DaysOfWeek().length; i++) {
                        if(self.DaysOfWeek()[i].Item3()==true)
                        {
                            self.catalogAdvancedSearchDataModelInstance.days.push(self.DaysOfWeek()[i].Item2());
                    }
            }

                ko.utils.postJson(Ellucian.Course.ActionUrls.postAdvancedSearchUrl, { model: self.catalogAdvancedSearchDataModelInstance.mapToJS(), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        });
    }
        catch (e) {
            self.isSubmissionInProgress(false);
            console.error(e.message);
            $('#notificationHost').notificationCenter('addNotification', { message: 'advanced search failed', type: "error"
        });
    }
    };

    this.clear = function () {
        for (var i = 0; i < self.DaysOfWeek().length; i++) {
            self.DaysOfWeek()[i].Item3(false);
    }
      return self.catalogAdvancedSearchDataModelInstance.clear();
    };
};

Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchDataModel=function(parent) {
    var self = this;
    this.term = ko.observable();
    this.location = ko.observable();
    this.academicLevel = ko.observable();
    this.courseType = ko.observable();
    this.startDate = ko.observable("");
    this.endDate = ko.observable("");


    this.startDate.ErrorMessage = ko.observable("");
    this.startDate.isValid = ko.computed(function () {

        if (!isNullOrEmpty(self.startDate()) && Globalize.parseDate(self.startDate()) == null) {
            self.startDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.invalidDateFormatErrorMessage);
            return false;
        }

        if(!isNullOrEmpty(self.endDate()) && isNullOrEmpty(self.startDate())) {
            self.startDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.startMeetingDateRequiredMessage);
            return false;
        }

        else if (!isNullOrEmpty(self.startDate()) && !isNullOrEmpty(self.endDate())) {
            if(moment(self.startDate()) >  moment(self.endDate())) {
                self.startDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.startMeetingDateImproperMessage);
                return false;
            }
            else if (moment(self.startDate()) < moment(parent.EarliestSearchDate()) || moment(self.startDate()) > moment(parent.LatestSearchDate())) {
                self.startDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.startMeetingDateRangeMessage + " " + parent.EarliestSearchDate().toLocaleDateString() + " " + Ellucian.Course.SearchResult.timesDatesDelimiter + " " + parent.LatestSearchDate().toLocaleDateString());
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    });
    this.startDate.isEnabled = ko.computed(function () {
        if (isNullOrEmpty(self.term())) {
            return true;
        }
        else {
            self.startDate("");
            return false;
        }

    });

    this.endDate.ErrorMessage = ko.observable("");
    this.endDate.isValid = ko.computed(function () {
        if (!isNullOrEmpty(self.endDate()) && Globalize.parseDate(self.endDate()) == null) {
            self.endDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.invalidDateFormatErrorMessage);
            return false;
        }

        if (!isNullOrEmpty(self.startDate()) && isNullOrEmpty(self.endDate())) {

            self.endDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.endMeetingDateRequiredMessage);
            return false;
        }
        else if(!isNullOrEmpty(self.startDate()) && !isNullOrEmpty(self.endDate()))
        {
            
            if(moment(self.endDate()) < moment(self.startDate()))
            {
                self.endDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.endMeetingDateImproperMessage);
                return false;
            }
            else if (moment(self.endDate()) > moment(parent.LatestSearchDate()) || moment(self.endDate()) < moment(parent.EarliestSearchDate())) {
                self.endDate.ErrorMessage(Ellucian.CatalogAdvancedSearch.MarkupLabels.endMeetingDateRangeMessage + " "+parent.EarliestSearchDate().toLocaleDateString() + " " +Ellucian.Course.SearchResult.timesDatesDelimiter + " " +parent.LatestSearchDate().toLocaleDateString());
                return false;
            }
            else {
                return true;
            }
        }
        else
        {
            return true;
        }
    });
    this.endDate.isEnabled = ko.computed(function () {
        if (isNullOrEmpty(self.term())) {
            return true;
        }
        else {
            self.endDate("");
            return false;
        }

    });


    this.days = ko.observableArray();
    this.timeRange = ko.observable();
    this.keywordComponents = ko.observableArray();
        
    if (self.keywordComponents !== null && self.keywordComponents()!==null &&  self.keywordComponents().length == 0) {
        for (var i = 0; i < (parent.isMobile() ? 1 : 3) ; i++) {
            this.keywordComponents.push(new Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchKeywordComponentsModel());
        }
    }
    

    this.addCourse = function () {
        this.keywordComponents.push(new Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchKeywordComponentsModel());
    };

    this.isValid = ko.pureComputed(function () {
        if(self.endDate.isValid() && self.startDate.isValid())
        {
            return true;
        }
        else
        {
            return false;
        }
    });

    this.clear = function () {
        var tempKeywords = [];
        self.term("");
        self.location("");
        self.startDate("");
        self.endDate("");
        self.days([]);
        self.academicLevel("");
        self.courseType("");
        self.timeRange(null);
            self.keywordComponents([]);
            for (var i = 0; i < (parent.isMobile() ? 1 : 3) ; i++) {
                this.keywordComponents.push(new Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchKeywordComponentsModel());
            }
        return;
    };

    this.mapToJS = function () {
        var self = this;
        var tempKeywords = [];
        if (self.keywordComponents !== null && self.keywordComponents().length > 0) {
            for (var i = 0; i < self.keywordComponents().length; i++) {
                if (self.keywordComponents()[i].isNullOrEmpty() == false) {
                    tempKeywords.push(self.keywordComponents()[i]);
                }
            }
            self.keywordComponents([]);
            self.keywordComponents.pushAll(tempKeywords);
        }
        var copy = ko.mapping.toJS(self);
        return copy; //return the copy to be serialized
    };

   
};
