﻿//index page script for catalog search

var catalogSearchSubjectsViewModelInstance = new catalogSearchSubjectsViewModel();
$(document).ready(function () {
    ko.applyBindings(catalogSearchSubjectsViewModelInstance, document.getElementById("main"));

    $.ajax({
        url: Ellucian.Course.ActionUrls.getSubjectsActionUrl,
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                var subjects = [];
                for (var i = 0; i < data.length; i++) {
                    subjects.push(new subjectModel(data[i], Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl, false));
                    
                }
                catalogSearchSubjectsViewModelInstance.subjects.pushAll(subjects);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve subject list.", type: "error" });
        },
        complete: function (jqXHR, textStatus) {

        }
    });

    //textbox management
    $("#coursecatalog-filter").watermark(" Type a subject...");
    $("#coursecatalog-filter").click(function () { this.select(); });
});