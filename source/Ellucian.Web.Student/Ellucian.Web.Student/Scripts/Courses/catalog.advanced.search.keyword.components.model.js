﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates

Ellucian.CatalogAdvancedSearch = Ellucian.CatalogAdvancedSearch || {};
Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchKeywordComponentsModel=function() {
    var self = this;
    this.subject = ko.observable();
   
    this.courseNumber = ko.observable("");
    this.courseNumber.isEnabled = ko.computed(function () {
        if (isNullOrEmpty(self.subject()))
        {
            self.courseNumber("");
            return false;
        }
        else
        {
            return true;
        }
    });
    this.section = ko.observable("");
    this.section.isEnabled = ko.computed(function () {
        if (isNullOrEmpty(self.courseNumber())) {
            self.section("");
            return false;
        }
        else
        {
            return true;
        }
    });
    this.isNullOrEmpty = function () {
        if (isNullOrEmpty(self.subject()) && isNullOrEmpty(self.courseNumber()) && isNullOrEmpty(self.section())) {
            return true;
        }
        else
            return false;
    };
  
};

