﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.//
$(document).ready(function () {
    //initial viewmodel
    if (svm) {
        ko.applyBindings(svm, document.getElementById("sectiondetails-dialogcontainer"));
    }
});