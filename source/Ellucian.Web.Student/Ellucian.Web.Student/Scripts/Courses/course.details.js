﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.//
$(document).ready(function () {
    //initial viewmodel
    if (cvm) {
        ko.applyBindings(cvm, document.getElementById("coursedetails-dialogcontainer"));
    }
});