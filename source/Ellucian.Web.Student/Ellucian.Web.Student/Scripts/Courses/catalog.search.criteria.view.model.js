﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates


function catalogSearchCriteriaModel() {
    var self = this;
    this.keyword = "";
    this.terms =[];
    this.requirement="";
    this.subrequirement="";
    this.courseIds = [];
    this.sectionIds = [];
    this.requirementText="";
    this.subrequirementText="";
    this.group = "";
    this.startTime = ""
    this.endTime = ""
    this.openSections = ""
    this.subjects=[];
    this.academicLevels=[];
    this.courseLevels = [];
    this.courseTypes = [];
    this.topicCodes = [];
    this.terms = [];
    this.days = [];
    this.locations = [];
    this.faculty = [];
    this.onlineCategories = [];
    this.keywordComponents = [];
    this.startDate = "";
    this.endDate = "";
    this.pageNumber = 1;
};

catalogSearchCriteriaModel.prototype.toJSON = function () {
    var copy = ko.toJS(this); //easy way to get a clean copy
    delete copy.__ko_mapping__;
    return copy; //return the copy to be serialized
};


