﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
//base model for catalog result view model
function CatalogResultBaseViewModel(baseUrl) {
    var self = this;

    // "Inherit" the base view model
    BaseViewModel.call(self);
    this.changeToMobile = function () {
        // no-op
    };

    this.changeToDesktop = function () {
        // no-op
    };

    this.searchUrl = ko.observable(baseUrl);


    this.addCourseTerms = ko.observable();
    this.PlanTerms = ko.observableArray();
    this.PlanTerms.subscribe(function (result) {
        self.addCourseTerms(result);
    });

    // Subscribe to the "catalogResult" topic - when another view model receives a result, it gets posted 
    // to the "catalogResult" topic, allowing this view model to display the data.
    this.catalogResult = ko.observable(null).subscribeTo("catalogResult");


    // catalogResultsDisplayed is used by embedded (asynchronous) course catalogs.  It subscribes to the
    // "catalogResultDisplayed" topic.  When a search result is displayed, a message (with value of true) is posted
    // to the "catalogResultDisplayed" topic, allowing the catalog index to hide and the catalog results to show.
    this.catalogResultsDisplayed = ko.observable(false).syncWith("catalogResultDisplayed");

    this.showCatalogSubjectIndex = function () {
        self.catalogResultsDisplayed(false);
    }

    this.Courses = ko.observableArray();

    this.Keyword = ko.observable('');
    this.Requirement = ko.observable();
    this.Subrequirement = ko.observable();
    this.RequirementText = ko.observable();
    this.AdvancedSearchText = ko.observable();
    this.SubrequirementText = ko.observable();
    this.Group = ko.observable();
    this.CourseIds = ko.observable();
    this.StartTime = ko.observable(null);
    this.EndTime = ko.observable(null);
    this.OpenSections = ko.observable(null);

    this.RequirementTextDisplay = ko.computed(function () {
        if (typeof self.RequirementText() !== 'undefined') {
            if (self.RequirementText().length > 0) return true;
        }
        return false;
    }, this);

    this.AdvancedSearchTextDisplay = ko.computed(function () {
        if (typeof self.AdvancedSearchText() !== 'undefined' && self.AdvancedSearchText() !== null) {
            if (self.AdvancedSearchText().length > 0) return true;
        }
        return false;
    }, this);

    this.CurrentPageIndex = ko.observable();
    this.PageSize = ko.observable();

    this.ShowAllSubjects = ko.observable(false);
    this.ShowAllSubjectsHandler = function () {
        self.ShowAllSubjects(true);
    };
    this.Subjects = ko.observableArray();
    this.SubjectsPartialList = ko.computed(function () {
        if (self.ShowAllSubjects() != true) {
            return self.Subjects().slice(0, 5);
        } else {
            return self.Subjects();
        }
    });
    this.isShowAllSubjectsVisible = ko.pureComputed(function () {

        return self.ShowAllSubjects() == false && self.Subjects().length > 5;
    });
    this.ShowAllTerms = ko.observable(false);
    this.ShowAllTermsHandler = function () {
        self.ShowAllTerms(true);
    };
    this.TermFilters = ko.observableArray();
    this.TermFiltersPartialList = ko.computed(function () {
        if (self.ShowAllTerms() != true) {
            return self.TermFilters().slice(0, 5);
        } else {
            return self.TermFilters();
        }
    });
    this.isShowAllTermsVisible = ko.pureComputed(function () {

        return self.ShowAllTerms() == false && self.TermFilters().length > 5;
    });
    this.ShowAllAcademicLevels = ko.observable(false);
    this.ShowAllAcademicLevelsHandler = function () {
        self.ShowAllAcademicLevels(true);
    };
    this.AcademicLevels = ko.observableArray();
    this.AcademicLevelsPartialList = ko.computed(function () {
        if (self.ShowAllAcademicLevels() != true) {
            return self.AcademicLevels().slice(0, 5);
        } else {
            return self.AcademicLevels();
        }
    });
    this.isShowAllAcademicLevelsVisible = ko.pureComputed(function () {

        return self.ShowAllAcademicLevels() == false && self.AcademicLevels().length > 5;
    });
    this.ShowAllCourseLevels = ko.observable(false);
    this.ShowAllCourseLevelsHandler = function () {
        self.ShowAllCourseLevels(true);
    };
    this.CourseLevels = ko.observableArray();
    this.CourseLevelsPartialList = ko.computed(function () {
        if (self.ShowAllCourseLevels() != true) {
            return self.CourseLevels().slice(0, 5);
        } else {
            return self.CourseLevels();
        }
    });
    this.isShowAllCourseLevelsVisible = ko.pureComputed(function () {

        return self.ShowAllCourseLevels() == false && self.CourseLevels().length > 5;
    });
    this.ShowAllCourseTypes = ko.observable(false);
    this.ShowAllCourseTypesHandler = function () {
        self.ShowAllCourseTypes(true);
    };
    this.CourseTypes = ko.observableArray();
    this.CourseTypesPartialList = ko.computed(function () {
        if (self.ShowAllCourseTypes() != true) {
            return self.CourseTypes().slice(0, 5);
        } else {
            return self.CourseTypes();
        }
    });
    this.isShowAllCourseTypesVisible = ko.pureComputed(function () {

        return self.ShowAllCourseTypes() == false && self.CourseTypes().length > 5;
    });
    this.ShowAllTopicCodes = ko.observable(false);
    this.ShowAllTopicCodesHandler = function () {
        self.ShowAllTopicCodes(true);
    };
    this.TopicCodes = ko.observableArray();
    this.TopicCodesPartialList = ko.computed(function () {
        if (self.ShowAllTopicCodes() != true) {
            return self.TopicCodes().slice(0, 5);
        } else {
            return self.TopicCodes();
        }
    });
    this.isShowAllTopicCodesVisible = ko.pureComputed(function () {

        return self.ShowAllTopicCodes() == false && self.TopicCodes().length > 5;
    });
    this.DaysOfWeek = ko.observableArray();

    this.ShowAllLocations = ko.observable(false);
    this.ShowAllLocationsHandler = function () {
        self.ShowAllLocations(true);
    };
    this.Locations = ko.observableArray();
    this.LocationsPartialList = ko.computed(function () {
        if (self.ShowAllLocations() != true) {
            return self.Locations().slice(0, 8);
        } else {
            return self.Locations();
        }
    });
    this.isShowAllLocationsVisible = ko.pureComputed(function () {

        return self.ShowAllLocations() == false && self.Locations().length > 8;
    });
    this.ShowAllFaculty = ko.observable(false);
    this.ShowAllFacultyHandler = function () {
        self.ShowAllFaculty(true);
    };
    this.Faculty = ko.observableArray();
    this.FacultyPartialList = ko.computed(function () {
        if (self.ShowAllFaculty() != true) {
            return self.Faculty().slice(0, 5);
        } else {
            return self.Faculty();
        }
    });
    this.isShowAllFacultyVisible = ko.pureComputed(function () {

        return self.ShowAllFaculty() == false && self.Faculty().length > 5;
    });
    this.ShowAllOnlineCategories = ko.observable(false);
    this.ShowAllOnlineCategoriesHandler = function () {
        self.ShowAllOnlineCategories(true);
    };
    this.OnlineCategories = ko.observableArray();
    this.OnlineCategoriesPartialList = ko.computed(function () {
        if (self.ShowAllOnlineCategories() != true) {
            return self.OnlineCategories().slice(0, 5);
        } else {
            return self.OnlineCategories();
        }
    });
    this.isShowAllOnlineCategoriesVisible = ko.pureComputed(function () {

        return self.ShowAllOnlineCategories() == false && self.OnlineCategories().length > 5;
    });
    this.ResultsOptions = { collapsible: true, active: false, autoHeight: false };

    this.UnselectFilter = function (filter) {
        filter.Selected(false);
        self.UpdateSearchResults();
    }

    this.SelectedSubjects = ko.computed(function () {
        return ko.utils.arrayFilter(self.Subjects(), function (subject) {
            return subject.Selected();
        });
    });

    this.SelectedAcademicLevels = ko.computed(function () {
        return ko.utils.arrayFilter(self.AcademicLevels(), function (level) {
            return level.Selected();
        });
    });

    this.SelectedCourseLevels = ko.computed(function () {
        return ko.utils.arrayFilter(self.CourseLevels(), function (level) {
            return level.Selected();
        });
    });

    this.SelectedCourseTypes = ko.computed(function () {
        return ko.utils.arrayFilter(self.CourseTypes(), function (type) {
            return type.Selected();
        });
    });

    this.SelectedTopicCodes = ko.computed(function () {
        return ko.utils.arrayFilter(self.TopicCodes(), function (type) {
            return type.Selected();
        });
    });

    this.SelectedTerms = ko.computed(function () {
        return ko.utils.arrayFilter(self.TermFilters(), function (type) {
            return type.Selected();
        });
    });

    this.SelectedDaysOfWeek = ko.computed(function () {
        return ko.utils.arrayFilter(self.DaysOfWeek(), function (day) {
            return day.Selected();
        });
    });

    this.SelectedLocations = ko.computed(function () {
        return ko.utils.arrayFilter(self.Locations(), function (location) {
            return location.Selected();
        });
    });

    this.SelectedFaculty = ko.computed(function () {
        return ko.utils.arrayFilter(self.Faculty(), function (faculty) {
            return faculty.Selected();
        });
    });

    this.SelectedTime = ko.observable();

    this.SelectedOnlineCategories = ko.computed(function () {
        return ko.utils.arrayFilter(self.OnlineCategories(), function (onlineCategories) {
            return onlineCategories.Selected();
        });
    });

    this.IsAtLeastOneFilterSelected = ko.pureComputed(function () {
        var result = (
            (typeof self.SelectedAcademicLevels === "function" ? self.SelectedAcademicLevels().length > 0 : self.SelectedAcademicLevels.length > 0) ||
            (typeof self.SelectedCourseLevels === "function" ? self.SelectedCourseLevels().length > 0 : self.SelectedCourseLevels.length > 0) ||
            (typeof self.SelectedCourseTypes === "function" ? self.SelectedCourseTypes().length > 0 : self.SelectedCourseTypes.length > 0) ||
            (typeof self.SelectedDaysOfWeek === "function" ? self.SelectedDaysOfWeek().length > 0 : self.SelectedDaysOfWeek.length > 0) ||
            (typeof self.SelectedFaculty === "function" ? self.SelectedFaculty().length > 0 : self.SelectedFaculty.length > 0) ||
            (typeof self.SelectedLocations === "function" ? self.SelectedLocations().length > 0 : self.SelectedLocations.length > 0) ||
            (typeof self.SelectedSubjects === "function" ? self.SelectedSubjects().length > 0 : self.SelectedSubjects.length > 0) ||
            (typeof self.SelectedTime() === 'undefined' || self.SelectedTime() === '' ? false : self.SelectedTime().Start != 0 && self.SelectedTime().End != 1440) ||
            (typeof self.SelectedOnlineCategories === "function" ? self.SelectedOnlineCategories().length > 0 : self.SelectedOnlineCategories.length > 0) ||
            (typeof self.SelectedTerms === "function" ? self.SelectedTerms().length > 0 : self.SelectedTerms.length > 0)
        );
        return result;
    });
    this.IsAdvancedCriteriaApplied = ko.pureComputed(function () {
        return (typeof self.AdvancedSearchText() === 'undefined' ? false : self.AdvancedSearchText() !== "");
    });
    this.ShowAvailabilityFilter = ko.pureComputed(function () {
        return self.IsAtLeastOneFilterSelected() || self.IsAdvancedCriteriaApplied() || !isNullOrEmpty(self.Keyword()) || !isNullOrEmpty(self.RequirementText());
    }).extend({ deferred: true });

    this.ResetOpenSectionSelect = ko.computed(function () {
        // The open section filter must be unselected whenever there is no filter, no advanced criteria applied and no keyword because the filter will be hidden
        // and we want it be to in the unselected state when it is hidden so that all sections appear.
        if (self.IsAtLeastOneFilterSelected() == false && self.IsAdvancedCriteriaApplied() == false && isNullOrEmpty(self.Keyword()) && self.OpenSections() !== null && isNullOrEmpty(self.RequirementText())) {
            self.OpenSections().Selected(false);
        }
        return false;
    });

    this.UpdateSearchResults = function () {
        if (typeof Ellucian.Course.SearchResult.stringUpdatingFiltersSpinner === "string") {
            //show the proper spinner message - the action will result in a page refresh, so there isn't any need to set it back
            self.LoadingMessage(Ellucian.Course.SearchResult.stringUpdatingFiltersSpinner);
        }

        self.BuildSearchCriteria();
        var url = self.searchUrl();
        self.submitAsyncSearchRequest(url);
    };

    this.TimeRanges = new Array();
    if (typeof Ellucian.Course.SearchResult.stringTimeFilterEarlyMorning === "string" && typeof Ellucian.Course.SearchResult.stringTimeFilterMorning === "string" && typeof Ellucian.Course.SearchResult.stringTimeFilterAfternoon === "string" &&
        typeof Ellucian.Course.SearchResult.stringTimeFilterEvening === "string" && typeof Ellucian.Course.SearchResult.stringTimeFilterNight === "string") {
        self.TimeRanges.push({ 'Text': Ellucian.Course.SearchResult.stringTimeFilterEarlyMorning, 'StartTime': 0, 'EndTime': 480 });
        self.TimeRanges.push({ 'Text': Ellucian.Course.SearchResult.stringTimeFilterMorning, 'StartTime': 480, 'EndTime': 720 });
        self.TimeRanges.push({ 'Text': Ellucian.Course.SearchResult.stringTimeFilterAfternoon, 'StartTime': 720, 'EndTime': 960 });
        self.TimeRanges.push({ 'Text': Ellucian.Course.SearchResult.stringTimeFilterEvening, 'StartTime': 960, 'EndTime': 1200 });
        self.TimeRanges.push({ 'Text': Ellucian.Course.SearchResult.stringTimeFilterNight, 'StartTime': 1200, 'EndTime': 1440 });
    }
    else {
        self.TimeRanges.push({ 'Text': 'Early Morning (Midnight - 8am)', 'StartTime': 0, 'EndTime': 480 });
        self.TimeRanges.push({ 'Text': 'Morning (8am - Midday)', 'StartTime': 480, 'EndTime': 720 });
        self.TimeRanges.push({ 'Text': 'Afternoon (Midday - 4pm)', 'StartTime': 720, 'EndTime': 960 });
        self.TimeRanges.push({ 'Text': 'Evening (4pm - 8pm)', 'StartTime': 960, 'EndTime': 1200 });
        self.TimeRanges.push({ 'Text': 'Night (8pm - Midnight)', 'StartTime': 1200, 'EndTime': 1440 });
    }

    this.RemoveTimeFilter = function () {
        self.StartTime(0);
        self.EndTime(1440);
        self.SelectedTime('');
    };

    // This is the time range to display on the filter button
    // Because the time filter doesn't update automatically (requires submit), we want to keep 
    // the button display separate (so it only updates on Submits).
    this.TimeFilterSelected = ko.computed(function () {
        if (typeof self.SelectedTime() === "undefined" || self.SelectedTime() === '') return false;
        if (self.SelectedTime().StartTime === 0 && self.SelectedTime().EndTime === 1440) return false;
        return true;
    });;
    this.TimeFilterButtonValue = ko.observable();
    this.UpdateSearchResultsTimeOfDay = function () {
        if (typeof self.SelectedTime() !== 'undefined' && self.SelectedTime() != '') {
            self.StartTime(self.SelectedTime().StartTime);
            self.EndTime(self.SelectedTime().EndTime);
            self.UpdateSearchResults();
        } else {
            self.StartTime(0);
            self.EndTime(1440);
            self.UpdateSearchResults();
        }
    };


    this.BuildSearchCriteria = function () {
        if (!isNullOrEmpty(self.Keyword())) {
            catalogSearchCriteriaModelInstance.keyword = self.Keyword();
        }
        else {
            catalogSearchCriteriaModelInstance.keyword = "";
        }

        var subjects = self.SelectedSubjects();
        catalogSearchCriteriaModelInstance.subjects = [];
        if (subjects.length > 0) {
            var length = subjects.length;
            for (var i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.subjects.push(subjects[i].Value());
            }
        }

        var acadLevels = self.SelectedAcademicLevels();
        catalogSearchCriteriaModelInstance.academicLevels = [];
        if (acadLevels.length > 0) {
            length = acadLevels.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.academicLevels.push(acadLevels[i].Value());
            }
        }

        var courseLevels = self.SelectedCourseLevels();
        catalogSearchCriteriaModelInstance.courseLevels = [];
        if (courseLevels.length > 0) {
            length = courseLevels.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.courseLevels.push(courseLevels[i].Value());
            }
        }

        var courseTypes = self.SelectedCourseTypes();
        catalogSearchCriteriaModelInstance.courseTypes = [];
        if (courseTypes.length > 0) {
            length = courseTypes.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.courseTypes.push(courseTypes[i].Value());
            }
        }

        var topicCodes = self.SelectedTopicCodes();
        catalogSearchCriteriaModelInstance.topicCodes = [];
        if (topicCodes.length > 0) {
            length = topicCodes.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.topicCodes.push(topicCodes[i].Value());
            }
        }

        var terms = self.SelectedTerms();
        catalogSearchCriteriaModelInstance.terms = [];
        if (terms.length > 0) {
            length = terms.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.terms.push(terms[i].Value());
            }
        }

        var daysOfWeek = self.SelectedDaysOfWeek();
        catalogSearchCriteriaModelInstance.days = [];
        if (daysOfWeek.length > 0) {
            length = daysOfWeek.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.days.push(daysOfWeek[i].Value());
            }
        }

        var locations = self.SelectedLocations();
        catalogSearchCriteriaModelInstance.locations = [];
        if (locations.length > 0) {
            length = locations.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.locations.push(locations[i].Value());
            }
        }

        var faculty = self.SelectedFaculty();
        catalogSearchCriteriaModelInstance.faculty = [];
        if (faculty.length > 0) {
            length = faculty.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.faculty.push(faculty[i].Value());
            }
        }

        if (self.StartTime() != null) {
            catalogSearchCriteriaModelInstance.startTime = self.StartTime();
        }
        if (self.EndTime() != null) {
            catalogSearchCriteriaModelInstance.endTime = self.EndTime();
        }

        var onlineCategories = self.SelectedOnlineCategories();
        catalogSearchCriteriaModelInstance.onlineCategories = [];
        if (onlineCategories.length > 0) {
            length = onlineCategories.length;
            for (i = 0; i < length; i++) {
                catalogSearchCriteriaModelInstance.onlineCategories.push(onlineCategories[i].Value());
            }
        }
        if (self.OpenSections() !== null) {
            catalogSearchCriteriaModelInstance.openSections = self.OpenSections().Selected();
        }

    };

    this.PageLink = function (page, update) {
        if (update) {
            catalogSearchCriteriaModelInstance.pageNumber = page;
            var url = self.searchUrl();
            self.submitAsyncSearchRequest(url);
        }
    };

    this.submitAsyncSearchRequest = function (url) {
        var jsonSearchData = { 'searchParameters': ko.toJSON(catalogSearchCriteriaModelInstance) };
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(jsonSearchData),
            contentType: jsonContentType,
            dataType: "json",
            beforeSend: function (data) {
                $("html body").animate({ scrollTop: 0 }, 100, function () {
                    if (typeof Ellucian.Course.SearchResult.stringUpdatingFiltersSpinner === "string") {
                        //show the proper spinner message - the action will result in a page refresh, so there isn't any need to set it back
                        self.LoadingMessage(Ellucian.Course.SearchResult.stringUpdatingFiltersSpinner);
                    }
                    self.isLoading(true);
                });
            },
            success: function (data) {
                if (!account.handleInvalidSessionResponse(data)) {
                    self.catalogResult(data);
                    self.catalogResultsDisplayed(true);
                    var start = catalogResultViewModelInstance.StartTime();
                    var end = catalogResultViewModelInstance.EndTime();
                    for (var i = 0; i < catalogResultViewModelInstance.TimeRanges.length; i++) {
                        if (start == catalogResultViewModelInstance.TimeRanges[i].StartTime && end == catalogResultViewModelInstance.TimeRanges[i].EndTime) {
                            catalogResultViewModelInstance.SelectedTime(catalogResultViewModelInstance.TimeRanges[i]);
                            catalogResultViewModelInstance.TimeFilterButtonValue(catalogResultViewModelInstance.TimeRanges[i]);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status != 0)
                    $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve search results.", type: "error" });
            },
            complete: function (jqXHR, textStatus) {
                var timer = window.setInterval(function () {
                    if ($("html body").scrollTop() == 0) {
                        self.isLoading(false);
                        clearInterval(timer);
                    }
                }, 100);
            }
        });
    };

    // Publish to the "isLoading" topic, in case the loading spinner is bound to a different view model than this one.
    this.isLoading = ko.observable(null).publishOn("isLoading");
    this.LoadingMessage = ko.observable(Ellucian.Course.SearchResult.stringAddingCourseSpinner).syncWith("LoadingMessage");


}

function catalogResultViewModelWithoutDegreePlan(baseUrl) {
    var self = this;

    // "Inherit" the catalog base view model
    CatalogResultBaseViewModel.call(self, baseUrl);

    this.catalogResult.subscribe(function (result) {
        ko.mapping.fromJS(result, courseMapping, self);
    });
}

function catalogResultViewModelWithDegreePlan(baseUrl, planRepository) {
    var self = this;
    //include program requirements subscriber model
    var programRequirementsSubscriberModelInstance = new Ellucian.Planning.programRequirementsSubscriberModel();


    // "Inherit" the catalog base view model
    CatalogResultBaseViewModel.call(self, baseUrl);

    this.DegreePlan = ko.observable().syncWith("degreePlan", true);
    catalogResultViewModelExtension.apply(self, [planRepository]);
}

function catalogResultViewModelWithDegreePlanForAdvisors(baseUrl, planRepository) {
    var self = this;

    // "Inherit" the catalog base view model
    CatalogResultBaseViewModel.call(self, baseUrl);

this.DegreePlan = ko.observable().syncWith("degreePlan", true);

catalogResultViewModelExtension.apply(self, [planRepository]);

this.advisingKeywordSearchHandler = function () {
    // Switch to the catalog tab
    $("#advising-content-nav").tabs("option", "selected", 3);

    // Clear out previous search criteria - only search on the keyword
    self.Requirement(null);
    self.Subrequirement(null);
    self.CourseIds(null);
    self.RequirementText("");
    self.SubrequirementText(null);
    self.Group(null);
    self.Subjects([]);
    self.AcademicLevels([]);
    self.CourseLevels([]);
    self.CourseTypes([]);
    self.TopicCodes([]);
    self.TermFilters([]);
    self.DaysOfWeek([]);
    self.Locations([]);
    self.Faculty([]);
    self.StartTime(null); self.EndTime(null);
    self.OnlineCategories([]);
    self.OpenSections(null);
    //clear search criteria instance too
    catalogSearchCriteriaModelInstance.requirement = null;
    catalogSearchCriteriaModelInstance.subrequirement = null;
    catalogSearchCriteriaModelInstance.group = null;
    catalogSearchCriteriaModelInstance.requirementText = null;
    catalogSearchCriteriaModelInstance.subrequirmentText = null;
    catalogSearchCriteriaModelInstance.startTime = null;
    catalogSearchCriteriaModelInstance.endTime = null;

    catalogSearchCriteriaModelInstance.openSections = null;
    // Update the search results
    self.UpdateSearchResults();
    };

}


//extension method
function catalogResultViewModelExtension(planRepository) {
    var self = this;
    this.addSectionButtonLabel = ko.computed(function () {
        // if the degree plan isn’t loaded yet, the buttons get disabled and a label explaining why
        if (typeof self.DegreePlan() == "undefined") return "Loading plan...";
        // if the page is loaded on  a small screen, use a short label
        if (self.isMobile() === true) return addSectionLabelMobile;
        // default long label
        return addSectionLabel;
    });
    //to publish in order to notify subscribers (prpgramRequirementSubscriberModel) if degree-plan is modified
    this.degreePlanChanged = ko.observable(false).publishOn("degreePlanChanged");

    // isProxyUser should be set to true if the plan is being accessed by a proxy user (advisor)
    // isProxyUser subscribes to the "isProxyUser" topic so it can be enabled
    this.isProxyUser = ko.observable().subscribeTo("isProxyUser", true);
    this.canEdit = ko.observable(true).subscribeTo("canEdit", true);
    this.catalogResult.subscribe(function (result) {

        if (result.resultModel == undefined && result.searchModel == undefined && result !== undefined) {
            ko.mapping.fromJS(result, courseMapping, self);
            self.addCourseTerms(result.PlanTerms);
            self.UpdateViewModelWithPlanningStatus();
            return;
        }

        if (result.resultModel !== undefined) {
            //update search criteria
            ko.mapping.fromJS(result.resultModel, courseMapping, self);
            self.addCourseTerms(result.resultModel.PlanTerms);
            self.UpdateViewModelWithPlanningStatus();
        }
        if (result.searchModel !== undefined) {
            ko.mapping.fromJSON(result.searchModel, {}, catalogSearchCriteriaModelInstance);
        }

    });


    this.showAddCourseDialog = function (courseModel, section, term, studentId, canSkipWaitlist) {
        if (courseModel != null) {
            // Is this just a course, or a course and specific section?

            // Just a course
            if (section == null) {
                // Open global Course Details dialog
                showCourseDetails({ course: courseModel, section: null, allowAdding: true, addTerms: self.addCourseTerms(), addCallback: self.commitAddCourse, creditsOverride: null });

                addingFullSection = false;
                addingSectionId = null;
                addingTermId = null;

                //for now, non-term courses are not represented, only full sections can be non-term
                addingCourseNonTerm = false;

                if (courseModel.IsVariableCredit()) {
                    var interval = courseModel.VariableCreditIncrement();
                    var min = courseModel.MinimumCredits();
                    var max = courseModel.MaximumCredits();

                    // Increment can be null, in which case the user can type any value they want
                    if (interval != null) {
                        addingCourseCreditsFreeform = false;
                    } else {
                        addingCourseCreditsFreeform = true;
                        addingCourseFreeformMin = min;
                        addingCourseFreeformMax = max;
                    }
                } else {
                    addingCourseCreditsFreeform = false;
                    if (!isNullOrEmpty(courseModel.MinimumCredits())) {
                        addingCourseStaticCredits = courseModel.MinimumCredits();
                    } else {
                        addingCourseStaticCredits = -1;
                    }
                }
            } else {    // Course AND Section
                // Open global Section Details dialog
                showSectionDetails({ course: courseModel, section: section, allowAdding: true, addTerms: null, addCallback: self.commitAddCourse, creditsOverride: null, studentId: studentId, canSkipWaitlist: canSkipWaitlist });

                // Hide the term selection menu
                addingFullSection = true;
                addingSectionId = section.Id();
                addingTermId = term.Term.Code();
                addingCourseNonTerm = section.IsNonTermOffering();

                // Set up credit tracking if user decided to add from global dialog
                if (isCourseOrSectionVariableCredit(ko.mapping.toJS(section))) {
                    var interval = section.VariableCreditIncrement();
                    var min = section.MinimumCredits();
                    var max = section.MaximumCredits();

                    // Increment can be null, in which case the user can type any value they want
                    if (interval != null) {
                        addingCourseCreditsFreeform = false;
                    } else {
                        addingCourseCreditsFreeform = true;
                        addingCourseFreeformMin = min;
                        addingCourseFreeformMax = max;
                    }
                } else {
                    addingCourseCreditsFreeform = false;
                    if (!isNullOrEmpty(section.MinimumCredits())) {
                        addingCourseStaticCredits = section.MinimumCredits();
                    } else {
                        addingCourseStaticCredits = -1;
                    }
                }
            }

            // Store the data for next step of submission
            addingCourseId = courseModel.Id();
        }
    };

    this.updateEvaluations = ko.observable().syncWith("updateEvaluations");

    // Handler for clicking Add Course (adding course to degree plan from search)
    var defaultAddOptions = { chosenCredits: null, chosenTerm: null };
    this.commitAddCourse = function (options) {
        var options = $.extend({}, defaultAddOptions, options);

        if (addingCourseId != null) {

            // First ensure the chosen number of credits (if variable) is OK
            if (addingCourseCreditsFreeform) {
                var value = options.chosenCredits;
                // Must not be null
                if (isNullOrEmpty(value)) {
                    alert(Ellucian.Planning.planViewModelMessages.specifyCredits);
                    if (addingFullSection) {
                        focusAddingSection();
                    }
                    else {
                        focusAddingCourse();
                    }
                    return;
                }
                // Must be a number
                if (!isNumber(value)) {
                    alert(Ellucian.Planning.planViewModelMessages.specifyCreditsRange.format(addingCourseFreeformMin, addingCourseFreeformMax));
                    if (addingFullSection) {
                        focusAddingSection();
                    }
                    else {
                        focusAddingCourse();
                    }
                    return;
                }
                // Must be within the range
                if (value > addingCourseFreeformMax || value < addingCourseFreeformMin) {
                    alert(Ellucian.Planning.planViewModelMessages.specifyCreditsRange.format(addingCourseFreeformMin, addingCourseFreeformMax));
                    if (addingFullSection) {
                        focusAddingSection();
                    }
                    else {
                        focusAddingCourse();
                    }
                    return;
                }
            }

            // How many credits? Is it variable?
            var numCredits;
            if (!isNullOrEmpty(options.chosenCredits)) {
                numCredits = options.chosenCredits;
            }
            else {
                //static credits amount
                numCredits = addingCourseStaticCredits;
            }

            //if this one is a non-term item, strip off the the term ID
            if (addingCourseNonTerm) {
                addingTermId = "";
            }

            //throbber
            if (typeof Ellucian.Course.SearchResult.stringUpdatingFiltersSpinner === "string") {
                //show the proper spinner message - the action will result in a page refresh, so there isn't any need to set it back
                catalogResultViewModelInstance.LoadingMessage(Ellucian.Course.SearchResult.stringAddingCourseSpinner);
            }
            catalogResultViewModelInstance.isLoading(true);

            // Are we adding a full section, or just a generic course?
            if (addingFullSection) {

                //close the details dialog
                closeSectionDetails();
                catalogResultViewModelInstance.LoadingMessage(Ellucian.Course.SearchResult.stringAddingSectionSpinner);

                planRepository.addSection(self.DegreePlan().PersonId(), addingCourseId, addingTermId, addingSectionId, options.chosenGradingType, numCredits, ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
                .then(function (data) {
    self.degreePlanChanged(true);
    catalogResultViewModelInstance.isLoading(false);
    if (data.Notifications.length > 0) {
        $('#notificationHost').notificationCenter('addNotification', data.Notifications);
    }

    self.DegreePlan(new DegreePlan(data));

    //clear all of the saved data for this add
    addingCourseId = null;
    addingSectionId = null;
    addingTermId = null;
    addingFullSection = false;
    self.updateEvaluations(true);
})
                .catch(function (error) {
                    catalogResultViewModelInstance.isLoading(false);
                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.Planning.planViewModelMessages.cannotAddSection, type: "error" });
                    addingCourseId = null;
                    addingSectionId = null;
                    addingTermId = null;
                    addingFullSection = false;
                });
            }
            else {
                //close the details dialog
                closeCourseDetails();
                catalogResultViewModelInstance.LoadingMessage(Ellucian.Course.SearchResult.stringAddingCourseSpinner);

                planRepository.addCourse(self.DegreePlan().PersonId(), addingCourseId, options.chosenTerm, numCredits, ko.mapping.toJSON(self.DegreePlan().DegreePlanDto))
                .then(function (data) {
                    self.degreePlanChanged(true);
                    catalogResultViewModelInstance.isLoading(false);
                    if (data.Notifications.length > 0) {
                        $('#notificationHost').notificationCenter('addNotification', data.Notifications);
                    }

                    self.DegreePlan(new DegreePlan(data));

                    addingCourseId = null;

                    self.updateEvaluations(true);
                })
                .catch(function (jqXHR, textStatus, errorThrown) {
                    catalogResultViewModelInstance.isLoading(false);
                    $('#notificationHost').notificationCenter('addNotification', { message: stringCannotAddCourse, type: "error" });
                    addingCourseId = null;
                });
            }
        }
    };

    // Update each course in the search result with the planning status of the course from the DegreePlan
    this.UpdateViewModelWithPlanningStatus = function () {
        if (typeof ko.utils.unwrapObservable(self.DegreePlan) != "undefined" && typeof ko.utils.unwrapObservable(self.CourseFullModels) != "undefined") {
            // loop through the courses in the catalog search result and update each with the course planning status from the degree plan, if any
            for (var i = 0; i < self.CourseFullModels().length; i++) {
                var courseId = self.CourseFullModels()[i].Id();
                // Find this course in the degree plan list of PlanningStatuses and get the display class and display text
                for (var j = 0; j < self.DegreePlan().PlanningStatuses().length; j++) {
                    if (self.DegreePlan().PlanningStatuses()[j].CourseId() == courseId) {
                        //var planningStatusClass = self.DegreePlan().PlanningStatuses()[j].Class();
                        self.CourseFullModels()[i].PlanningStatusClass(self.DegreePlan().PlanningStatuses()[j].Class());
                        //var planningStatusText = self.DegreePlan().PlanningStatuses()[j].Text();
                        self.CourseFullModels()[i].PlanningStatusText(self.DegreePlan().PlanningStatuses()[j].Text());
                        j = self.DegreePlan().PlanningStatuses().length; // end loop if course is found
                    }
                }
            }
        }
    };
}


//factory method to retun appropriate catalog result object
function catalogResultViewModel(baseUrl, isWithDegreePlan, isAdvisor, planRepository) {

    if (typeof isWithDegreePlan == 'undefined' || isWithDegreePlan == null || isWithDegreePlan == false) {
        return new catalogResultViewModelWithoutDegreePlan(baseUrl);

    }
    else {
        if (planRepository) {
            if (isAdvisor == true) {
                return new catalogResultViewModelWithDegreePlanForAdvisors(baseUrl, planRepository);
            }
            else {
                return new catalogResultViewModelWithDegreePlan(baseUrl, planRepository);
            }
        }
        else
        {
            return new catalogResultViewModelWithoutDegreePlan(baseUrl);
        }

    }
}