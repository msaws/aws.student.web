﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates
// Tracking vars

var catalogResultViewModelInstance = null;
var catalogSearchCriteriaModelInstance = null;
var addingCourseId = null;
var addingFullSection = false;
var addingSectionId = null;
var addingTermId = null;
var addingCourseCreditsFreeform = false;
var addingCourseFreeformMin = 0;
var addingCourseFreeformMax = 0;
var addingCourseStaticCredits = 0;
var addingCourseNonTerm = false;

function spinnerModel() {
    var self = this;
    this.isLoading = ko.observable(true);
}
spinner = new spinnerModel();

$(document).ready(function () {
    ko.applyBindings(spinner, document.getElementById("spinner"));
    if (isWithDegreePlan) {
        catalogResultViewModelInstance = new catalogResultViewModel(Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, true, false, Ellucian.Planning.planRepository);
    }
    else {
        catalogResultViewModelInstance = new catalogResultViewModel(Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, false, false);
    }
    catalogSearchCriteriaModelInstance = new catalogSearchCriteriaModel();
    if (Ellucian.Course.SearchResult.jsonData !== null) {
        ko.mapping.fromJS(Ellucian.Course.SearchResult.jsonData, {}, catalogSearchCriteriaModelInstance);
    }


    // Get the degree plan, so we can enable any UI elements that are related to it, only when not guest user
    if (isWithDegreePlan && Ellucian.Planning && Ellucian.Planning.planRepository) {
        var planRepository = Ellucian.Planning.planRepository;
        planRepository.get(currentUserId)
            .then(function (results) {
                var degreePlan = results[0];
                catalogResultViewModelInstance.DegreePlan(new DegreePlan(degreePlan));
                // Update catalog if the catalog with degree plan information
                catalogResultViewModelInstance.UpdateViewModelWithPlanningStatus();
            }).catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: error, type: "error" })
            });
    }

    var searchUrl = Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl;
    var jsonSearchData = { 'searchParameters': ko.toJSON(catalogSearchCriteriaModelInstance) };
    $.ajax({
        url: searchUrl,
        type: "POST",
        data: JSON.stringify(jsonSearchData),
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function () {
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                ko.mapping.fromJS(data, courseMapping, catalogResultViewModelInstance);
                ko.applyBindings(catalogResultViewModelInstance, document.getElementById("course-search-result"));
                ko.applyBindings(catalogResultViewModelInstance, document.getElementById("keyword"));  //the following two items fall outside the main container, so we need to bind them separately
                ko.applyBindings(catalogResultViewModelInstance, document.getElementById("loading"));

                var start = catalogResultViewModelInstance.StartTime();
                var end = catalogResultViewModelInstance.EndTime();
                for (var i = 0; i < catalogResultViewModelInstance.TimeRanges.length; i++) {
                    if (start == catalogResultViewModelInstance.TimeRanges[i].StartTime && end == catalogResultViewModelInstance.TimeRanges[i].EndTime) {
                        catalogResultViewModelInstance.SelectedTime(catalogResultViewModelInstance.TimeRanges[i]);
                        catalogResultViewModelInstance.TimeFilterButtonValue(catalogResultViewModelInstance.TimeRanges[i]);
                    }
                }
                // Update catalog result view model if degree plan exists 
                if (isWithDegreePlan) {
                    catalogResultViewModelInstance.UpdateViewModelWithPlanningStatus();
                }
            }
            // Now that the page is loaded with data, check if we need to adapt the page for mobile.
            catalogResultViewModelInstance.checkForMobile(window, document);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: "Unable to retrieve search results.", type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            spinner.isLoading(false);
        }
    });
});
