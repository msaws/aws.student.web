﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates.
var courseMapping = {
    'CourseFullModels': {
        create: function (options) {
            return new courseFullModel(options.data);
        }
    },
    'TermsAndSections': {
        create: function (options) {
            return new termsAndSections(options.data);
        }
    },
    'Sections': {
        create: function (options) {
            return new section(options.data);
        }
    },
    'LocationCycleRestrictionDescriptions': {
        create: function (options) {
            return new locationCycleRestrictionDescription(options.data);
        }
    }
};

function courseFullModel(data) {
    ko.mapping.fromJS(data, courseMapping, this);

    var self = this;

    this.isLoading = ko.observable(true);

    this.TermsAndSections = ko.observable();

    this.PlanningStatusClass = ko.observable();
    this.PlanningStatusText = ko.observable();

    // handler for lazy loading section data
    this.bindSections = function () {

        // If terms and sections have already been retreived, don't request them again.
        if (self.TermsAndSections() != null) return;

        //ajax request to load section data
        var jsonData = { courseId: self.Id(), sectionIds: self.MatchingSectionIds() };
        $.ajax({
            url: Ellucian.Course.ActionUrls.getSectionsActionUrl,
            data: JSON.stringify(jsonData),
            type: "POST",
            contentType: jsonContentType,
            dataType: "json",
            success: function (data) {
                var termsAndSections = ko.mapping.fromJS(data, courseMapping, self);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            },
            complete: function (data) {
                self.isLoading(false)
            }
        });
    }
    this.showLocationCycleRestrictionDescriptions = ko.computed(function () {
        if (typeof self.LocationCycleRestrictionDescriptions() !== 'undefined') {
            if (self.LocationCycleRestrictionDescriptions().length > 0) return true;
        }
        return false;
    }), this;

    this.courseOffering = ko.computed(function () {
        if (!isNullOrEmpty(self.TermsOffered()) && !isNullOrEmpty(self.YearsOffered())) {
            return self.TermsOffered() + ", " + self.YearsOffered();
        } else if (!isNullOrEmpty(self.TermsOffered())) {
            return self.TermsOffered();
        } else if (!isNullOrEmpty(self.YearsOffered())) {
            return self.YearsOffered();
        } else {
            return null;
        }
    }), this;

    this.showCourseOffering = ko.computed(function () {
        return !this.showLocationCycleRestrictionDescriptions() && !isNullOrEmpty(this.courseOffering());
    }, this);

}

function termsAndSections(data) {
    ko.mapping.fromJS(data, courseMapping, this);

    var self = this;
}

function section(data) {
    ko.mapping.fromJS(data, courseMapping, this);

    var self = this;

        this.getSeatsClass = function () {
            if (!isNullOrEmpty(section)) {
                if (self.Capacity !== null && self.Available !== null && self.Available === 0) {
                    if (self.WaitlistAvailable === true) {
                        return "search-sectionseatswaitlisted";
                    } else {
                        return "search-sectionseatsnone";
                    }
                } else {
                    return "search-sectionseatsopen";
                }
            }
    }
}

function locationCycleRestrictionDescription(data) {
    ko.mapping.fromJS(data, courseMapping, this);

    var self = this;
}


// Strings

var stringCannotAddSection = "The specified section cannot be planned at this time.";
var stringCannotAddCourse = "The specified course cannot be added at this time.";
var stringAddingSectionDescription = "Section {0}";
var stringSpecifyCredits = "Please specify the number of credits to take.";
var stringSpecifyCreditsRange = "Please specify a valid number of credits between {0} and {1}.";
var stringSectionsTBDText = "Section information TBD";