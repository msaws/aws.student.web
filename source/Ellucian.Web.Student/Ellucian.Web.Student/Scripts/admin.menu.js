﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
$(document).ready(function () {
    var vm = new MenuViewModel();
    var viewModel = ko.applyBindings(vm, document.getElementById("main"));
});