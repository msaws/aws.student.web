﻿/****************************************************/
//
// Library that wraps sessionStorage for easier use
//
// Uses the same interface as sessionStorage, but
// handles conversion from JavaScript objects to JSON
/****************************************************/
var Ellucian = Ellucian || {};
Ellucian.Storage = Ellucian.Storage || {};

Ellucian.Storage = function (global) {

    // Simple function to return the type of an object (more robust/accurate than "typeof")
    var toType = function (obj) {
        return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();
    };

    var getItem = function (webStorage, key) {
        var item = webStorage.getItem(key);

        try {
            item = JSON.parse(item);
        } catch (e) {
            // parse is expected to fail on some types, ignore this and return anyway.
        }

        return item;
    };

    var setItem = function (webStorage, key, value) {
        // Get the type of value
        var type = toType(value);

        // If value is an object or array, stringify it for webStorage,
        // as sessionStorage only accepts strings 
        if (/object|array/.test(type)) {
            value = JSON.stringify(value);
        }
        try {
            webStorage.setItem(key, value);
        } catch (error) {
            // If we don't handle errors, the action could grace in a less than graceful way
            // We handle the errors as a no-op, but we could add an Analytics event or log
            if (error instanceof DOMException && error.code === 22) {
                // Ooops, ran out of webStorage space
            } else {
                // Some other error
            }
        }
    };

    var removeItem = function (webStorage, key) {
        webStorage.removeItem(key);
    };

    var clear = function (webStorage) {
        webStorage.clear();
    };

    return {
        session: {
            getItem: function (key) {
                return getItem(global.sessionStorage, key);
            },
            setItem: function (key, value) {
                return setItem(global.sessionStorage, key, value);
            },
            removeItem: function (key) {
                return removeItem(global.sessionStorage, key);
            },
            clear: function () {
                return clear(global.sessionStorage);
            }
        },
        local: {
            getItem: function (key) {
                return getItem(global.localStorage, key);
            },
            setItem: function (key, value) {
                return setItem(global.localStorage, key, value);
            },
            removeItem: function (key) {
                return removeItem(global.localStorage, key);
            },
            clear: function () {
                return clear(global.localStorage);
            }
        }
    };
}(this);
