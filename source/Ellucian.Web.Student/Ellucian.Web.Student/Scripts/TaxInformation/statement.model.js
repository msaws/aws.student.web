﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

// map properties from the view model
function StatementModel(data) {
    var self = this;
    ko.mapping.fromJS(data, taxInformationViewModelMapping, self);
};