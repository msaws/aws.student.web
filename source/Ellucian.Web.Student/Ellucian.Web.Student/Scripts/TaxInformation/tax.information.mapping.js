﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

// configure a knockout mapping object for the W2 tax consent forms
var taxInformationViewModelMapping = {
    'Consents': {
        create: function (options) {
            return new ConsentModel(options.data);
        }
    },
    'Statements': {
        create: function (options) {
            return new StatementModel(options.data);
        }
    }
};