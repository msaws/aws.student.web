﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

// javascript representation of a tax information view model
function taxInformationViewModel() {
    var self = this;

    // Tax information data
    self.Consents = ko.observableArray([]);
    self.IsW2 = ko.observable();
    self.Is1095C = ko.observable();
    self.Is1098 = ko.observable();

    self.showUI = ko.observable(false);
    self.isPageLoading = ko.observable(true);
    self.isTabPanelLoading = ko.observable(true);
    self.changePreferencesButtonClicked = ko.observable(false);
    self.consentChoice = ko.observable(false);
    self.waitingForConsentUpdate = ko.observable(false);
    self.taxFormSelected = ko.observable(false);

    // "Inherit" the base view model, specify the mobile screen width
    BaseViewModel.call(self, 600);

    // Change desktop structure to mobile structure
    this.changeToMobile = function () {
    }

    // Change mobile structure back to desktop view
    this.changeToDesktop = function () {
    }

    // Does the user have consent history?
    self.hasConsentHistory = ko.computed(function () {
        return self.Consents() !== null && typeof self.Consents() !== 'undefined' && self.Consents().length > 0;
    });

    self.currentConsentChoice = ko.computed(function () {
        if (!self.hasConsentHistory()) {
            return false;
        } else {
            return self.Consents()[0].HasConsented();
        }
    });

    // Returns true if user is changing consent options or has no consent history.
    self.showConsentOptions = ko.computed(function () {
        return self.changePreferencesButtonClicked() || !self.hasConsentHistory();
    });

    // Returns heading text for appropriate tax form.
    self.taxFormHeadingText = ko.computed(function () {
        if (self.IsW2()) {
            return W2HeadingText;
        }
        if (self.Is1095C()) {
            return Form1095CHeadingText;
        }
        if (self.Is1098()) {
            return Form1098HeadingText;
        }

        return "";
    }, self);

    // Returns consent text for appropriate tax form.
    self.taxFormConsentGivenText = ko.computed(function () {
        if (self.IsW2()) {
            return W2ConsentGivenText;
        }
        if (self.Is1095C()) {
            return Form1095CConsentGivenText;
        }
        if (self.Is1098()) {
            return Form1098ConsentGivenText;
        }

        return "";
    }, self);

    // Returns the 'you have opted to withhold your consent' label.
    self.taxFormConsentWithheldText = ko.computed(function () {
        if (self.IsW2()) {
            return FormW2ConsentWithheldText;
        }
        if (self.Is1095C()) {
            return Form1095CConsentWithheldText;
        }
        if (self.Is1098()) {
            return Form1098ConsentWithheldText;
        }

        return "";
    }, self);

    // Returns "short" consent text for appropriate tax form.
    self.taxFormConsentGivenShortText = ko.computed(function () {
        if (self.IsW2()) {
            return W2ReceiveElectronicOnlyText;
        }
        if (self.Is1095C()) {
            return Form1095CReceiveElectronicOnlyText;
        }
        if (self.Is1098()) {
            return Form1098ReceiveElectronicOnlyText;
        }

        return "";
    }, self);

    // Returns the text to select a consent option.
    self.consentUnknownText = ko.computed(function () {
        if (self.IsW2()) {
            return W2ConsentUnknownText;
        }
        if (self.Is1095C()) {
            return Form1095CConsentUnknownText;
        }
        if (self.Is1098()) {
            return Form1098ConsentUnknownText;
        }

        return "";
    }, self);

    // Returns the text for the statement history.
    self.statementHistoryLabel = ko.computed(function () {
        if (self.IsW2()) {
            return W2StatementHistoryLabel;
        }
        if (self.Is1095C()) {
            return Form1095CStatementHistoryLabel;
        }
        if (self.Is1098()) {
            return Form1098StatementHistoryLabel;
        }

        return "";
    }, self);

    // Returns the text for the consent history.
    self.consentHistoryLabel = ko.computed(function () {
        if (self.IsW2()) {
            return W2ConsentHistoryLabel;
        }
        if (self.Is1095C()) {
            return Form1095CConsentHistoryLabel;
        }
        if (self.Is1098()) {
            return Form1098ConsentHistoryLabel;
        }

        return "";
    }, self);

    // Returns the paper-only label
    self.receivePaperOnlyLabel = ko.computed(function () {
        if (self.IsW2()) {
            return ReceiveW2PaperOnlyText;
        }
        if (self.Is1095C()) {
            return Receive1095CPaperOnlyText;
        }
        if (self.Is1098()) {
            return Receive1098PaperOnlyText;
        }

        return "";
    }, self);

    // Show the radio buttons consent options when the user clicks on the change preferences button
    self.changePreferences = function () {
        self.changePreferencesButtonClicked(true);
    }

    // Reset the UI if the user clicks cancel on the change preferences form.
    self.cancelConsentChoice = function () {
        // Update this observable to hide the consent options and redisplay the current choice.
        self.changePreferencesButtonClicked(false);

        self.consentChoice(self.currentConsentChoice());
    }

    // Create a new tax form consent
    self.createNewTaxFormConsent = function () {
        // Hide the consent options
        self.changePreferencesButtonClicked(false);

        // Only submit the request if the consent choice has changed, or if the user has no consent history.
        if ((self.consentChoice() !== self.currentConsentChoice()) || !self.hasConsentHistory()) {
            var urlString = addTaxFormConsentUrl + "?taxForm=" + location.hash.substring(1) + "&hasConsented=" + self.consentChoice();

            $.ajax({
                url: urlString,
                type: "POST",
                contentType: jsonContentType,
                dataType: "json",
                beforeSend: function (data) {
                    // Show the loading throbber
                    self.waitingForConsentUpdate(true);
                },
                success: function (data) {
                    self.Consents.unshift(new ConsentModel(data));
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 0) {
                        $('#notificationHost').notificationCenter('addNotification', { message: unableToUpdateTaxFormConsent, type: "error" });

                        // Reset the UI
                        self.cancelConsentChoice();
                    }
                },
                complete: function (jqXHR, textStatus) {
                    // Hide the loading throbber
                    self.waitingForConsentUpdate(false);
                }
            });
        }
    }

    self.displayTabs = function (taxFormId) {
        var tabList = $("#tax-info-contents");
        var selectedTab = -1;

        // Default taxFormId when empty
        if (taxFormId === "") {
            if (tabList.children("#form-W2").index() > -1) {
                taxFormId = formW2Id;
            }
            else if (tabList.children("#form-1095C").index() > -1) {
                taxFormId = form1095CId;
            }
            else if (tabList.children("#form-1098").index() > -1) {
                taxFormId = form1098Id;
            }
        }
        // Determine the index of the selectedTab
        if (taxFormId === formW2Id) {
            selectedTab = tabList.children("#form-W2").index();
        }
        else if (taxFormId === form1095CId) {
            selectedTab = tabList.children("#form-1095C").index();
        }
        else if (taxFormId === form1098Id) {
            selectedTab = tabList.children("#form-1098").index();
        }
        if (selectedTab == -1) {
            // User entered an invalid fragment identifier - default to the first available form
            selectedTab = tabList.children().index();
            if (tabList.children("#form-W2").index() > -1) {
                taxFormId = formW2Id;
            }
            else if (tabList.children("#form-1095C").index() > -1) {
                taxFormId = form1095CId;
            }
            else if (tabList.children("#form-1098").index() > -1) {
                taxFormId = form1098Id;
            }
        }
        // Select and show tabs/content if available

        $("#tax-info-tabs").tabs({ selected: selectedTab });
        self.taxFormSelected(true);
        // Assign the fragment identifier and get the tax information
        location.hash = taxFormId;
        getTaxInformation();
    }

    self.changeTab = function (taxFormId) {
        self.cancelConsentChoice();

        location.hash = taxFormId;
        getTaxInformation();
    }

    // Returns the name of the controller method that gets the appropriate PDF data
    self.getPdfData = ko.computed(function () {
        if (self.Is1095C()) {
            return get1095cPdfUrl;
        }
        if (self.IsW2()) {
            return getW2PdfUrl;
        }
        if (self.Is1098()) {
            return get1098PdfUrl;
        }

        return "";
    }, self);
}
