﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

// setting up view model instance of the tax information    
var taxInformationViewModelInstance = new taxInformationViewModel();

// Once the page has loaded...
$(document).ready(function () {
    $("#tax-info-tabs").tabs();
    ko.applyBindings(taxInformationViewModelInstance, document.getElementById("main"));

    // If there's data in the hash, use it to display the tax info for the specified tax form
    if (location.hash.length > 0) {
        taxInformationViewModelInstance.displayTabs(location.hash.substring(1));
    } else {
        //if there's no data in the hash, default to the first tab.
        taxInformationViewModelInstance.displayTabs("");
    }

    taxInformationViewModelInstance.isPageLoading(false);
});

function getTaxInformation() {
    // Execute an AJAX request to get the specified tax form consents.
    $.ajax({
        url: getTaxFormInfoUrl + "?taxFormId=" + location.hash.substring(1),
        type: "GET",
        contentType: jsonContentType,
        dataType: "json",
        beforeSend: function() {
            taxInformationViewModelInstance.isTabPanelLoading(true);
        },
        success: function (data) {
            if (!account.handleInvalidSessionResponse(data)) {
                taxInformationViewModelInstance.checkForMobile(window, document);
                ko.mapping.fromJS(data, taxInformationViewModelMapping, taxInformationViewModelInstance);
                taxInformationViewModelInstance.showUI(true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 0)
                $('#notificationHost').notificationCenter('addNotification', { message: unableToRetrieveTaxFormConsents, type: "error" });
        },
        complete: function (jqXHR, textStatus) {
            taxInformationViewModelInstance.isTabPanelLoading(false);

            // Check the most recent consent choice and default the radio buttons appropriately
            taxInformationViewModelInstance.consentChoice(taxInformationViewModelInstance.currentConsentChoice());
        }
    });
}