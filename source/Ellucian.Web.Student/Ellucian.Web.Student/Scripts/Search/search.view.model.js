﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
function searchViewModel() {
    var self= this;

    // Flag indicating whether or not the page content is ready to be displayed
    self.showUI= ko.observable(false);

    // Flag indicating whether or not an ajax request is being processed that should block the page
    // This needs to be initialized to true to prevent the AJAX request from firing when the 
    // bindings are initially applied.
    self.isLoading= ko.observable(true);

    // "Inherit" the base view model with a mobile breakpoint of 768 pixels
    BaseViewModel.call(self, 768);

    // Overrides for base view model change functions to remove alerts
    self.changeToDesktop= function () {}
    self.changeToMobile= function () {}

    // Flag indicating whether a search is being executed
    self.searching= ko.observable(false);

    // Initialize C# model properties
    self.SearchPrompt = ko.observable();
    self.PlaceholderText = ko.observable();
    self.BeforeSearchText = ko.observable();
    self.ErrorOccurred = ko.observable();
    self.ErrorMessage = ko.observable();
    self.SearchFieldLabel = ko.observable();
    self.SearchSubmitLabel = ko.observable();
    self.Search = ko.observable();
    self.Select = ko.observable();
    self.PointOfOriginControllerName = ko.observable();
    self.DynamicBackLinkDefault = ko.observable();
    self.BackLinkExclusions = ko.observableArray([]);
    self.SearchString = ko.observable();
    self.SearchResultsViewName = ko.observable();
    self.SearchResults = ko.observable([]);
    self.SpinnerAlternateText = ko.observable();
    self.SpinnerText = ko.observable();

    // Format the number of search results for display
    self.searchResultsCount = ko.computed(function () {
        var count = '({0})';
        if (self.SearchResults()) {
            return count.format(self.SearchResults().length);
        }
        return count.format('0');
    })

    // Flag indicating whether or not a search has occurred - set to true when a search is executed
    self.searchHasOccurred = ko.observable(false);

    // Function for executing searches - must be overridden in any class that uses searchViewModel
    self.executeSearch = function () {
        alert("Executing search (override me)...");
    };
    
    // Function for executing seraches - must be overridden in any class that users searchViewModel
    self.triggerSearch = function () {
        self.ErrorOccurred(false);
        self.ErrorMessage(null);
        self.searchHasOccurred(true);
        self.executeSearch();
    };

    // Function to reset search
    self.resetSearch = function () {
        self.SearchString(null);
        self.SearchResults(null);
        self.searchHasOccurred(false);
    }
}