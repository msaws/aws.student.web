//Copyright 2012-2016 Ellucian Company L.P. and its affiliates
(function ($) {

    var notifications = [];

    var methods = {

        init: function (options) {

            // setup
            return this.each(function () {
                $(this).find('.esg-notification-center__dropdown').bind('click', methods.toggle);
                updateVisualState(false);
            });

        },

        destroy: function () {
            return this.each(function () {
                $(this).unbind('click');
            });
        },

        toggle: function () {
            if ($("ul.esg-notification-center").is(":visible")) {
                if ($('.esg-notification-center__prompt').length == 0) {
                    $.notificationCenter('hide');
                }
            } else {
                $.notificationCenter('show');
            }
        },

        show: function () {
            if ($('ul[role = "alert"] > li').length > 0) {
                $(".esg-notification-center__dropdown").attr("aria-expanded", true);
                $(".esg-notification-center__dropdown").addClass("esg-is-open");
                $("ul.esg-notification-center").focus();
                updateVisualState(true);
            }
        },

        hide: function () {
            $(".esg-notification-center__dropdown").attr("aria-expanded", false);
            $(".esg-notification-center__dropdown").removeClass("esg-is-open");
            updateVisualState(false);
        },

        addNotification: function (input) {
            if (input != null) {

                if (!$.isArray(input)) {
                    input = [input];
                }

                $.each(input, function (i, notification) {
                    notification = createNotificationObject(notification);
                    var notifIndex = notifications.push(notification) - 1;
                    notification.index = notifIndex;
                    if (notification.message.length > 0) {

                        // check for this message in the stack...
                        var okToAdd = true;
                        $('ul[role = "alert"] > li > div > span').each(function () {
                            if ($(this).text().toLowerCase() == notification.message.toLowerCase()) { okToAdd = false; }
                        });

                        if (okToAdd) {
                            var fullMessage = "";
                            // If there is no Notification Center UI (most likely because you are not logged in) then just show an alert
                            // This is needed for when initially configuring the system via the Admin page
                            if ($("#notificationHost").length === 0) {
                                //some of the expected messages may contain quotes and slashes that display poorly unless massaged
                                var content = notification.message;
                                try {
                                    content = $.parseJSON(notification.message);
                                } catch (e) {
                                    //this may fail if you try it in older browsers, not much we can do about it
                                }
                                fullMessage = content;
                                switch (notification.type.toLowerCase()) {
                                    case "error":
                                        fullMessage = "Error!\n\n" + content;
                                        break;
                                    case "warning":
                                        fullMessage = "Warning!\n\n" + content;
                                        break;
                                }
                                if (fullMessage.length > 0) {
                                    alert(fullMessage);
                                }
                                return true;
                            }

                            var notificationClass = "esg-alert--info";
                            var iconMarkup = '<span class="esg-alert__icon-wrapper"><span class="esg-alert__icon esg-icon__container" aria-hidden="True"><svg class="esg-icon esg-icon--info-dark"><use xlink:href="#icon-info"></use></svg></span></span>';
                            var sortOrder = 3;

                            switch (notification.type.toLowerCase()) {
                                case "success":
                                    notificationClass = "esg-alert--success";
                                    iconMarkup = '<span class="esg-alert__icon-wrapper"><span class="esg-alert__icon esg-icon__container" aria-hidden="True"><svg class="esg-icon esg-icon--success-dark"><use xlink:href="#icon-check"></use></svg></span></span>';
                                    sortOrder = 1;
                                    break;
                                case "error":
                                    notificationClass = "esg-alert--error";
                                    iconMarkup = '<span class="esg-alert__icon-wrapper"><span class="esg-alert__icon esg-icon__container" aria-hidden="True"><svg class="esg-icon esg-icon--error-dark"><use xlink:href="#icon-error"></use></svg></span></span>';
                                    sortOrder = 2;
                                    break;
                                case "warning":
                                    notificationClass = "esg-alert--warning";
                                    iconMarkup = '<span class="esg-alert__icon-wrapper"><span class="esg-alert__icon esg-icon__container" aria-hidden="True"><svg class="esg-icon esg-icon--warning-dark"><use xlink:href="#icon-warning"></use></svg></span></span>';
                                    sortOrder = 3;
                                    break;
                                case "information":
                                    notificationClass = "esg-alert--info";
                                    iconMarkup = '<span class="esg-alert__icon-wrapper"><span class="esg-alert__icon esg-icon__container" aria-hidden="True"><svg class="esg-icon esg-icon--info-dark"><use xlink:href="#icon-info"></use></svg></span></span>';
                                    sortOrder = 3;
                                    break;
                            }

                            var listItem = $("<li></li>").addClass("esg-notification-center__item").attr({ 'data-sort-Order': sortOrder }).attr({ 'data-index': notifIndex });
                            // Mark the notification list element as "permanent" and/or "flash", according to the properties.
                            if (notification.permanent === true) {
                                listItem = listItem.attr("data-type", "permanent");
                            }
                            if (notification.flash === true) {
                                listItem = listItem.attr("data-type", "flash");
                            }
                            // Build the notification message from the provided arguments...
                            fullMessage = "";
                            // Title (will be bold weight font)
                            if (notification.title && notification.title.length > 0) {
                                fullMessage += "<div class='esg-alert__message--title'>" + notification.title + "</div>";
                            }
                            // Message (no additional styling)
                            if (notification.message && notification.message.length > 0) {
                                fullMessage += "<div>" + notification.message + "</div>";
                            }
                            // Link
                            if (notification.link && notification.link.length > 0) {
                                fullMessage += "<a class='esg-alert__message--link' href='" + notification.link + "' target='_blank'>";
                                if (notification.linkText && notification.linkText.length > 0) {
                                    fullMessage += notification.linkText;
                                }
                                else {
                                    fullMessage += notification.link;
                                }
                                fullMessage += "</a>";
                            }
                            // Insert the message content
                            var messageDiv = $("<div></div>").addClass("esg-alert").addClass(notificationClass).append(iconMarkup).append($("<div class='esg-alert__message'></div>").append(fullMessage));
                            // Append the message to the list element
                            listItem.append(messageDiv);

                            if (notification.prompts && notification.prompts.length > 0) {

                                promptsDiv = $("<div></div>").addClass("esg-notification-center__prompt").append('<div class="esg-button-group" role="group" aria-label="button group"></div>');

                                $.each(notification.prompts, function (i, prompt) {
                                    var b = $("<button class='esg-button esg-button--secondary'></button>").html(prompt.label).click(prompt.action);
                                    promptsDiv.children(".esg-button-group").append(b);
                                });

                                listItem.append(promptsDiv);
                            }

                            if ($('ul[role = "alert"] > li').length > 0) {
                                // add based on sort order...
                                // todo.. sort order...
                                $('ul[role = "alert"]').append(listItem);
                            } else {
                                $('ul[role = "alert"]').append(listItem);
                            }
                            // Flash notifications should self-remove after a set duration (default: 10 seconds)...
                            if (notification.flash == true) {
                                setTimeout(function () {
                                    $.notificationCenter('removeNotification', notification);
                                    // Determine if there are any remaining "flash" type notifications.
                                    var flashNotificationCount = $('ul[role = "alert"] > li:not([data-type = "flash"])').length;
                                    if (flashNotificationCount === 0) {
                                        // If the last "flash" notification was just removed, hide the notification flyout.
                                        $.notificationCenter('hide');
                                    }
                                }, 10000);
                            }
                                // Non-"flash" type notifications...
                            else {
                                // "Permanent" notifications should not include a removal link (X to the right of the message).
                                // Notifications with prompts are permanent (because the user needs to select something)
                                if (notification.permanent === false && !(notification.prompts && notification.prompts.length > 0)) {
                                    messageDiv.children(".esg-alert__message").prepend("<a class='esg-icon__container float-right' href='javascript:void(0)' title='Remove notification' aria-label='Remove notification '><svg class='esg-icon esg-icon--neutral'><use xlink:href='#icon-close'></use></svg></a>");

                                    messageDiv.find("a").click(function () {
                                        $.notificationCenter('removeNotification', notification);
                                    });
                                }
                            }

                            $.notificationCenter('update');

                            // Show notifications that have "showByDefault" set to true (default: true).
                            if (notification.showByDefault === true) {
                                $.notificationCenter('show');   // Visual state is updated by "show" function.
                                var offset = $('.esg-notification-center__dropdown').offset();
                                $('html, body').animate({ scrollTop: offset.top || 0 });
                            }
                            else {
                                // force the visual state to update without showing notifications (needed for the styling).
                                updateVisualState(false);
                            }
                        }
                    }
                });
            }

            return this.each(function () {
            });
        },

        removeNotification: function (notification) {
            if (notification.index != null) {
                $('ul.esg-notification-center > li').each(function () {
                    if ($(this).attr('data-index') == notification.index) {
                        $(this).remove();
                    }
                });
            } else if (notification.message.length > 0) {
                $('ul.esg-notification-center > li').each(function () {
                    var s = $(this).find('div.esg-alert__message > div:not([class])');

                    if (s.length > 0) {
                        if (s.text().toLowerCase() == notification.message.toLowerCase()) {
                            $(this).remove();
                        }
                    }
                });
            }
            $.notificationCenter('update');

            return this.each(function () {
            });
        },

        update: function () {
            if (updateCount() == 0) {
                $.notificationCenter('hide');
            }
        },

        reset: function () {
            // Remove all non-"permanent" notifications from the DOM
            $('ul[role = "alert"] > li:not([data-type = "permanent"])').each(function () {
                $(this).remove();
            });
            $.notificationCenter('update');
            return this.each(function () {
            });
        }
    };

    function updateCount() {
        var count = $('ul[role = "alert"] > li').length || 0;
        $("#notificationMenu").html(count);
        return count;
    }

    function updateVisualState(open) {
        var count = updateCount();

        if (count <= 0) {
            $("#notificationHost").css("display", "none");
            $(".esg-header-bar__menu-item").addClass("esg-header-bar__menu-item-no-notifications");
        } else {
            $("#notificationHost").css("display", "inline-block");
            $(".esg-header-bar__menu-item").removeClass("esg-header-bar__menu-item-no-notifications");
        }
    }

    // Given a "raw" notification (may be missing some attributes, or use incorrect casing on attribute names),
    // create a usable notification object (defaults assigned, etc).
    function createNotificationObject(rawNotification) {
        var notification = {
            message: "",
            type: "information",
            flash: false,
            prompts: [],
            index: null,
            showByDefault: true,    // Show the notification as soon as it is added?
            permanent: false,       // Prevent the notification from being removed by "removeNotification", "reset", etc?
            link: "",               // href value for the hyperlink
            linkText: "",           // Text value for the hyperlink
            title: ""               // Title for the notification 
        };

        if (rawNotification != null) {
            // message prop
            if (rawNotification.message != undefined) {
                notification.message = rawNotification.message;
            } else if (rawNotification.Message != undefined) {
                notification.message = rawNotification.Message;
            } else {
                $.error("jQuery.notificationCenter: Missing property 'message'");
            }

            // type prop
            if (rawNotification.type != undefined) {
                notification.type = rawNotification.type;
            } else if (rawNotification.Type != undefined) {
                notification.type = rawNotification.Type;
            }

            // flash prop
            if (rawNotification.flash != undefined) {
                notification.flash = rawNotification.flash;
            } else if (rawNotification.Flash != undefined) {
                notification.flash = rawNotification.Flash;
            }

            // prompts
            if (rawNotification.prompts != undefined) {
                notification.prompts = rawNotification.prompts;
            } else if (rawNotification.Prompts != undefined) {
                notification.prompts = rawNotification.Prompts;
            }

            // showByDefault property
            if (rawNotification.showByDefault != undefined) {
                notification.showByDefault = rawNotification.showByDefault;
            } else if (rawNotification.ShowByDefault != undefined) {
                notification.showByDefault = rawNotification.ShowByDefault;
            }

            // permanent property
            if (rawNotification.permanent != undefined) {
                notification.permanent = rawNotification.permanent;
            } else if (rawNotification.Permanent != undefined) {
                notification.permanent = rawNotification.Permanent;
            }

            // link property
            if (rawNotification.link != undefined) {
                notification.link = rawNotification.link;
            } else if (rawNotification.Link != undefined) {
                notification.link = rawNotification.Link;
            }

            // link text property
            if (rawNotification.linkText != undefined) {
                notification.linkText = rawNotification.linkText;
            } else if (rawNotification.LinkText != undefined) {
                notification.linkText = rawNotification.LinkText;
            }

            // title property
            if (rawNotification.title != undefined) {
                notification.title = rawNotification.title;
            } else if (rawNotification.Title != undefined) {
                notification.title = rawNotification.Title;
            }
        }

        return notification;
    }

    $.fn.notificationCenter = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.notificationCenter');
        }
    };

    $.notificationCenter = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.notificationCenter');
        }
    };

})(jQuery);


function Notification(message, type, flash) {
    this.type = "success";
    this.message = "";
    this.flash = false;
    this.prompts = [];

    if (type != undefined) { this.type = type; }
    if (message != undefined) { this.message = message; }
    if (flash != undefined) { this.flash = flash; }
}

function Notification(message, type, flash, title, link, linkText, permanent, showByDefault) {
    this.type = "success";
    this.message = "";
    this.flash = false;
    this.prompts = [];
    this.title = "";
    this.link = "";
    this.linkText = "";
    this.permanent = false;
    this.showByDefault = true;

    if (type != undefined) { this.type = type; }
    if (message != undefined) { this.message = message; }
    if (flash != undefined) { this.flash = flash; }
    if (title != undefined) { this.title = title; }
    if (link != undefined) { this.link = link; }
    if (linkText != undefined) { this.linkText = linkText; }
    if (permanent != undefined) { this.permanent = permanent; }
    if (showByDefault != undefined) { this.showByDefault = showByDefault; }
}

Notification.prototype.addPromptAction = function (label, action) {
    if (label != 'undefined') {
        this.prompts.push(new NotificationPrompt(label, action || function () { }));
    }
}

function NotificationPrompt(label, action) {
    this.label = label;
    this.action = action;
}


$(document).ready(function () {
    if ($("#notificationHost").length > 0) {
        $("#notificationHost").notificationCenter();
    }
});