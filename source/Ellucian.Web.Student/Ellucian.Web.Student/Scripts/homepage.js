﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
$(document).ready(function () {
    ko.components.register("workflow-task-widget", {
        require: 'WorkflowTaskWidget/_WorkflowTaskWidget'
    });
    var homePageViewModel = new HomePageViewModel();
    ko.applyBindings(homePageViewModel, document.getElementById("main"));
});

function HomePageViewModel() {
    var self = this;
    BaseViewModel.call(self, 769);

    this.activeWidget = ko.observable(false).syncWith("activeWidget");
    this.showWidget = ko.computed(function () {
        return self.activeWidget() && self.isMobile();
    });
    this.widgetTemplate = ko.observable(null).syncWith("widgetTemplate");
    this.widgetData = ko.observable(null).syncWith("widgetData");

    this.clearActiveWidget = function () {
        self.activeWidget(false);
        self.widgetTemplate(null);
        self.widgetData(null);
    }

    // Disable any active mobile widgets when switching views
    this.changeToMobile = function () {
        self.clearActiveWidget();
    }

    this.changeToDesktop = function () {
        self.clearActiveWidget();
    }

    this.checkForMobile(window, document);

}