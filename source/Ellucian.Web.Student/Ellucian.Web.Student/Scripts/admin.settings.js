﻿$(document).ready(function () {
    var vm = new SettingsViewModel();
    var viewModel = ko.applyBindings(vm, document.getElementById("main"));

    $('#baseUrl').change(function () {
        if (vm.ApiBaseUrl() == '') { $('#baseUrlMessage').html(''); return; }
        $('#baseUrlMessage').html('Checking for Web API...');
        $.ajax({
            url: pingWebApiUrl + '?url=' + encodeURIComponent(vm.ApiBaseUrl()),
            data: '',
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            success: function (result) {
                $('#baseUrlMessage').html(result);
            }
        });
    });

    $('#dateFormatCombo').change(function () {
        $('#dateFormatExample').html('');
        $.get(
            currentDateUrl + '?dateFormat=' + vm.ShortDateFormat(),
            function (data) {
                $('#dateFormatExample').html('Example: ' + data);
            });
    });
});