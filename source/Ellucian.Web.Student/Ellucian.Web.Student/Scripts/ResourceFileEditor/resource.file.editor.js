﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
$(document).ready(function () {
    var viewmodel = new resourcefileeditor.ViewModel();
    viewmodel.cancelUrl = resourceFileEditorCancelActionUrl;
    ko.applyBindings(viewmodel, document.getElementById("body"));
    viewmodel.GetResourceFileNames();
});