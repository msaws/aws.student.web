// The purpose of this file is to provide JavaScript Intellisense for the referenced script files in Visual Studio 2012+

// **********  Global scripts **********
/// <reference path="account.js" />
/// <reference path="activate-accordion.js" />
/// <reference path="admin.js" />
/// <reference path="admin.menu.js" />
/// <reference path="admin.page.js" />
/// <reference path="admin.settings.js" />
/// <reference path="admin.site.js" />
/// <reference path="base.view.model.js" />
/// <reference path="jquery-1.8.3.js" />
/// <reference path="jquery-ui-1.8.23.js" />
/// <reference path="spectrum.js" />
/// <reference path="jquery.outerhtml.js" />
/// <reference path="jquery.ui.multiprogressbar.js" />
/// <reference path="jquery.ui.plugin_notification-center.js" />
/// <reference path="jquery.ui.plugin_responsive-table.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.watermark.min.js" />
/// <reference path="json2.js" />
/// <reference path="knockout-3.4.0.debug.js" />
/// <reference path="knockout.mapping.js" />
/// <reference path="knockout-postbox.js" />
/// <reference path="modernizr-custom.js" />
/// <reference path="moment.js" />
/// <reference path="respond.min.js" />
/// <reference path="site.js" />
/// <reference path="tooltip.js" />


// **********  Planning scripts **********
/// <reference path="../Areas/Planning/Scripts/advisees.js" />
/// <reference path="../Areas/Planning/Scripts/advising.js" />
/// <reference path="../Areas/Planning/Scripts/catalog.index.js" />
/// <reference path="../Areas/Planning/Scripts/catalog.result.js" />
/// <reference path="../Areas/Planning/Scripts/course.section.details.js" />
/// <reference path="../Areas/Planning/Scripts/plan.mapping.js" />
/// <reference path="../Areas/Planning/Scripts/fullcalendar.js" />
/// <reference path="../Areas/Planning/Scripts/home.js" />
/// <reference path="../Areas/Planning/Scripts/planning.knockout.bindings.js" />
/// <reference path="../Areas/Planning/Scripts/load.sample.plan.js" />
/// <reference path="../Areas/Planning/Scripts/planning.global.js" />
/// <reference path="../Areas/Planning/Scripts/my.progress.js" />
/// <reference path="../Areas/Planning/Scripts/requirements.mappings.js" />
/// <reference path="../Areas/Planning/Scripts/program.requirements.view.model.js" />
/// <reference path="../Areas/Planning/Scripts/plan.view.model.js" />
/// <reference path="../Areas/Planning/Scripts/plan.archive.view.model.js" />
/// <reference path="../Areas/Planning/Scripts/test.score.view.model.js" />

// **********  Financial Aid scripts **********
/// <reference path="../Areas/FinancialAid/Scripts/financial.aid.knockout.bindings.js" />
/// <reference path="../Areas/FinancialAid/Scripts/awards-tooltip.js" />
/// <reference path="../Areas/FinancialAid/Scripts/year.selector.view.model.js" />
