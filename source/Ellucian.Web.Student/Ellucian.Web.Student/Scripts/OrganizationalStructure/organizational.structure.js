﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
/*
* Provides functionality related to the OrganizationalStructureController to support
* data retrieval and posting
*/

var organizationalStructureInstance = new Ellucian.OrganizationalStructure.OrganizationalStructureViewModel(Ellucian.OrganizationalStructure.organizationalStructureRepository);

$(document).ready(function () {

    organizationalStructureInstance.loadConfiguration();
    ko.applyBindings(organizationalStructureInstance, document.getElementById("organizational-structure"));

});
