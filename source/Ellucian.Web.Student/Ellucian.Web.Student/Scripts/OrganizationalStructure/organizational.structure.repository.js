﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.OrganizationalStructure = Ellucian.OrganizationalStructure || {};

Ellucian.OrganizationalStructure.organizationalStructureRepository = (function () {
    return {

        // Get organizational relationships
        getOrganizationalRelationships: function (query) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.OrganizationalStructure.organizationalStructureUrls.getOrganizationalRelationships + '?query=' + query,
                    type: "GET",
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                    }
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);
                })
                .fail(function () {
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationGetRelationshipFailed);
                });
            });
        },

        createOrganizationalRelationship: function (relationship) {
            var json = {
                organizationalRelationshipJson: ko.toJSON(relationship)
            };
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.OrganizationalStructure.organizationalStructureUrls.createOrganizationalRelationship,
                    data: JSON.stringify(json),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                    }
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);
                })
                .fail(function () {
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAddFailed);
                });
            });
        },

        updateOrganizationalRelationship: function (relationship) {
            var json = {
                organizationalRelationshipJson: ko.toJSON(relationship)
            };
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.OrganizationalStructure.organizationalStructureUrls.updateOrganizationalRelationship,
                    data: JSON.stringify(json),
                    type: "POST",
                    contentType: jsonContentType,
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                    }
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);
                })
                .fail(function () {
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipUpdateFailed);
                });
            });
        },

        deleteOrganizationalRelationship: function (id) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.OrganizationalStructure.organizationalStructureUrls.deleteOrganizationalRelationship + '/' + id,
                    type: "DELETE",
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                    }
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);
                })
                .fail(function () {
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemovalFailed);
                });
            });
        },

        getOrganizationalRelationshipConfiguration: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.OrganizationalStructure.organizationalStructureUrls.getOrganizationalRelationshipConfiguration,
                    type: "GET",
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                    }
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);
                })
                .fail(function () {
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipConfigurationRetrievalFailed);
                });
            });
        },

        getOrganizationalPersonPosition: function (id) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: Ellucian.OrganizationalStructure.organizationalStructureUrls.getOrganizationalPersonPosition + '/' + id,
                    type: "GET",
                    dataType: "json"
                })
                .done(function (data) {
                    if (!account.handleInvalidSessionResponse(data)) {
                        resolve(data);
                    }
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);
                })
                .fail(function () {
                    reject(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationPersonPositionRetrievalFailed);
                });
            });
        }
    }

})();