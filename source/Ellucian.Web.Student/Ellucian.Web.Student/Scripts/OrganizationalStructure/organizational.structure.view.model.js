﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var Ellucian = Ellucian || {};
Ellucian.OrganizationalStructure = Ellucian.OrganizationalStructure || {};

Ellucian.OrganizationalStructure.OrganizationalStructureViewModel = function OrganizationalStructureViewModel(repository) {
    var self = this;

    self.ManagerRelationshipTypes = ko.observable([]);

    self.isUserSearchLoaded = ko.observable(true);

    self.isManagerSearchLoaded = ko.observable(true);

    self.isSubordinateSearchLoaded = ko.observable(true);

    self.userSearchErrorOccurred = ko.observable(false);

    self.managerSearchErrorOccurred = ko.observable(false);

    self.subordinateSearchErrorOccurred = ko.observable(false);

    self.searchCriteriaForUser = ko.observable("");
    self.resultsForUser = ko.observableArray([]);
    self.selectedUser = ko.observable(null);
    self.userSearchErrorMessage = ko.observable(null);

    self.searchForUser = function () {
        // Pass search criteria (query) to get relationships

        self.isUserSearchLoaded(false);
        self.userSearchErrorOccurred(false);

        return repository.getOrganizationalRelationships(self.searchCriteriaForUser())
            .then(function (data) {
                // Set results based on returned data from promise
                self.resultsForUser(data);
                self.isUserSearchLoaded(true);
                if (self.resultsForUser().length === 0) {
                    self.userSearchErrorOccurred(true);
                }
            })
            .catch(function (error) {
                // clear search
                self.resultsForUser([]);
                self.isUserSearchLoaded(true);

                // create error message
                var searchErrorMessage = Ellucian.OrganizationalStructure.organizationalStructureMessages.SearchErrorMessage;
                self.userSearchErrorMessage(searchErrorMessage.format(self.searchCriteriaForUser()));

                // display error
                self.userSearchErrorOccurred(true);
            });
    };

    // Set selectedUser from click
    self.selectUser = function (result) {
        // Set selectedUser to the result (param passed from click)
        self.selectedUser(result);

        // Resest the results to its original value, an empty array
        self.resultsForUser([]);
    };

    self.userManagerRelationship = ko.computed(function () {
        if (self.selectedUser() !== null) {
            if (self.selectedUser().Relationships.length > 0) {
                // If there is a selectedUser with at least 1 relationship, loop through relationships
                for (i = 0; i < self.selectedUser().Relationships.length; i++) {
                    if (self.ManagerRelationshipTypes().indexOf(self.selectedUser().Relationships[i].Category) !== -1) {
                        // If one of the Relationships is that of a manager, check if id is the same as Relationship OrganizationalPersonPositionId
                        if (self.selectedUser().Id === self.selectedUser().Relationships[i].OrganizationalPersonPositionId) {
                            // If Ids match, return relationship
                            return self.selectedUser().Relationships[i];
                        }
                    }
                }
            }
        }
        return null;
    });

    self.userSubordinateRelationships = ko.computed(function () {
        var subordinateRelationships = [];
        if (self.selectedUser() !== null) {
            if (self.selectedUser().Relationships.length > 0) {
                // If there is a selectedUser with at least 1 relationship, loop through relationships
                for (i = 0; i < self.selectedUser().Relationships.length; i++) {
                    if (self.ManagerRelationshipTypes().indexOf(self.selectedUser().Relationships[i].Category) !== -1) {
                        // If one of the Relationships is that of a manager, check if id is the same as Relationship RelatedOrganizationalPersonPositionId
                        if (self.selectedUser().Id === self.selectedUser().Relationships[i].RelatedOrganizationalPersonPositionId) {
                            // If Ids match, return relationship
                            subordinateRelationships.push(self.selectedUser().Relationships[i]);
                        }
                    }
                }
            }
        }
        return subordinateRelationships;
    });

    self.userHasManager = ko.computed(function () {
        // check if userManagerRelationship contains relationship
        return (self.userManagerRelationship() !== null);
    });
    
    self.userHasSubordinates = ko.computed(function () {
        // check if userSubordinateRelationships contains relationships
        return (self.userSubordinateRelationships().length > 0);
    });

    self.editManagerDialogIsVisible = ko.observable(false);

    self.editManager = function (data) {
        self.editManagerDialogIsVisible(true);
    };

    self.closeEditManagerDialog = function () {
        self.editManagerDialogIsVisible(false);
    };

    self.editSubordinatesDialogIsVisible = ko.observable(false);

    self.editSubordinates = function (data) {
        self.editSubordinatesDialogIsVisible(true);
    };

    self.closeEditSubordinatesDialog = function () {
        self.editSubordinatesDialogIsVisible(false);
    };

    self.removeManagerMessage = ko.computed(function () {
        if (self.selectedUser() !== null) {
            var userName = self.selectedUser().PersonName;
            return Ellucian.OrganizationalStructure.organizationalStructureMessages.RemoveManagerForUserMessage.format(userName);
        }
        else {
            return null;
        }
    });

    self.removeSubordinatesMessage = ko.computed(function () {
        if (self.selectedUser() !== null) {
            var userName = self.selectedUser().PersonName;
            return Ellucian.OrganizationalStructure.organizationalStructureMessages.RemoveSubordinatesForUserMessage.format(userName);
        }
        else {
            return null;
        }
    });

    self.searchCriteriaForManager = ko.observable("");
    self.resultsForManager = ko.observableArray([]);
    self.selectedManager = ko.observable(null);
    self.managerSearchErrorMessage = ko.observable(null);

    self.searchForManager = function () {
        self.isManagerSearchLoaded(false);
        self.managerSearchErrorOccurred(false);

        // Pass search criteria (query) to get relationships
        return repository.getOrganizationalRelationships(self.searchCriteriaForManager())
            .then(function (data) {
                self.resultsForManager(data);
                self.isManagerSearchLoaded(true);
                if (self.resultsForManager().length === 0) {
                    self.managerSearchErrorOccurred(true);
                }
            })
            .catch(function (error) {
                // clear search
                self.resultsForManager([]);
                self.isManagerSearchLoaded(true);

                // create error message
                var searchErrorMessage = Ellucian.OrganizationalStructure.organizationalStructureMessages.SearchErrorMessage;
                self.managerSearchErrorMessage(searchErrorMessage.format(self.searchCriteriaForManager()));

                // display error
                self.managerSearchErrorOccurred(true);
            });
    };

    self.searchCriteriaForSubordinate = ko.observable("");
    self.resultsForSubordinate = ko.observableArray([]);
    self.selectedSubordinate = ko.observable(null);
    self.subordinateSearchErrorMessage = ko.observable(null);

    self.searchForSubordinate = function () {
        self.isSubordinateSearchLoaded(false);
        self.subordinateSearchErrorOccurred(false);

        // Pass search criteria (query) to get relationships
        return repository.getOrganizationalRelationships(self.searchCriteriaForSubordinate())
            .then(function (data) {
                self.resultsForSubordinate(data);
                self.isSubordinateSearchLoaded(true);
                if (self.resultsForSubordinate().length === 0) {
                    self.subordinateSearchErrorOccurred(true);
                }
            })
            .catch(function (error) {
                // clear search
                self.resultsForSubordinate([]);
                self.isSubordinateSearchLoaded(true);

                // create error message
                var searchErrorMessage = Ellucian.OrganizationalStructure.organizationalStructureMessages.SearchErrorMessage;
                self.subordinateSearchErrorMessage(searchErrorMessage.format(self.searchCriteriaForSubordinate()));

                // display error
                self.subordinateSearchErrorOccurred(true);
            });
    };

    self.resetPage = function () {
        self.searchCriteriaForUser("");
        self.searchCriteriaForManager("");
        self.searchCriteriaForSubordinate("");
        self.resultsForUser([]);
        self.resultsForManager([]);
        self.resultsForSubordinate([]);
        self.selectedManager(null);
        self.selectedSubordinate(null);
    }

    var deleteViaRepository = function (relationshipToDelete) {
        return repository.deleteOrganizationalRelationship(relationshipToDelete)
                .then(function (data) {
                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemoved, type: 'success', flash: true });
                    self.resetPage();
                    self.updateSelectedUserData();
                    return data;
                })
                .catch(function (error) {
                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemovalFailed, type: "error" });
                });
    };

    // Click to remove relationship for manager
    self.removeManager = function () {
        if (self.userHasManager()) {
            return deleteViaRepository(self.userManagerRelationship().Id);
        }  
    };

    // Click to remove relationship for subordinate (pass in Id via click)
    self.removeSubordinate = function (relationshipToDelete) {
        return deleteViaRepository(relationshipToDelete.Id);
    };

    // Determine whether to add or update (only add is used at this time)
    self.addOrUpdateManager = function (result) {
        // Set selected manager to manager that was clicked
        self.selectedManager(result);
        self.resultsForManager([]);

        if (self.userHasManager()) {
            // update
            var updatedRelationship;

            if (self.userHasManager()) {
                // copy information into new object before we get success back from server
                updatedRelationship = {
                    Id: self.userManagerRelationship().Id,
                    Category: self.userManagerRelationship().Category,
                    OrganizationalPersonPositionId: self.userManagerRelationship().OrganizationalPersonPositionId,
                    RelatedOrganizationalPersonPositionId: self.selectedManager().Id
                };
            }
            return updateManagerViaRepository(updatedRelationship);
        }
        else {
            var addedRelationship = {
                Id: '',
                Category: '',
                OrganizationalPersonPositionId: self.selectedUser().Id,
                RelatedOrganizationalPersonPositionId: self.selectedManager().Id
            };

            return addViaRepository(addedRelationship);
        }
    };

    self.addSubordinate = function (result) {
        self.selectedSubordinate(result);
        self.resultsForSubordinate([]);

        var addedRelationship = {
            Id: '',
            Category: '',
            OrganizationalPersonPositionId: self.selectedSubordinate().Id,
            RelatedOrganizationalPersonPositionId: self.selectedUser().Id
        };

        return addViaRepository(addedRelationship);
    };

    var updateManagerViaRepository = function (updatedRelationship) {
        // View model promise
        return repository.updateOrganizationalRelationship(updatedRelationship)
            .then(function (data) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipUpdated, type: 'success', flash: true });
                self.resetPage();
                self.updateSelectedUserData();
                return data;
            })
            .catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipUpdateFailed, type: "error" });
            });
    }

    var addViaRepository = function (addedRelationship) {
        // View model promise
        return repository.createOrganizationalRelationship(addedRelationship)
            .then(function (data) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAdded, type: 'success', flash: true });
                self.resetPage();
                self.updateSelectedUserData();
                return data;
            })
            .catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAddFailed, type: "error" });
            });
    };

    // Determine whether to display "Add" or "Update" as the button text for the supervisor
    self.addOrUpdateManagerText = ko.computed(function () {
        if (self.userHasManager()) {
            return Ellucian.OrganizationalStructure.organizationalStructureMessages.UpdateManagerButton;
        }
        else {
            return Ellucian.OrganizationalStructure.organizationalStructureMessages.AddManagerButton;
        }
    });

    self.loadConfiguration = function () {
        return repository.getOrganizationalRelationshipConfiguration().then(function (data) {
            ko.mapping.fromJS(data, {}, self);
        });
    };

    // Determine whether to display "Add" or "Update" as the button text for subordinates
    self.addOrUpdateSubordinateText = ko.computed(function () {
        if (self.userHasSubordinates()) {
            return Ellucian.OrganizationalStructure.organizationalStructureMessages.EditSubordinatesButtonAndDialogTitle;
        }
        else {
            return Ellucian.OrganizationalStructure.organizationalStructureMessages.AddSubordinateButtonAndDialogTitle;
        }
    });

    self.updateSelectedUserData = function () {
        if (self.selectedUser() != null) {
            return repository.getOrganizationalPersonPosition(self.selectedUser().Id)
                .then(function (data) {
                    self.selectedUser(data);
                }).catch(function (error) {
                    $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationUserRefreshFailed, type: "error" });
                });
        }
    };

    self.focusClickedManager = function (data) {
        var idToChange = data.RelatedOrganizationalPersonPositionId;

        return repository.getOrganizationalPersonPosition(idToChange)
            .then(function (data) {
                self.selectedUser(data);
            }).catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationUserNavigationFailed, type: "error" });
            });
    };

    self.focusClickedSubordinate = function (data) {
        var idToChange = data.OrganizationalPersonPositionId;

        return repository.getOrganizationalPersonPosition(idToChange)
            .then(function (data) {
                self.selectedUser(data);
            }).catch(function (error) {
                $('#notificationHost').notificationCenter('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationUserNavigationFailed, type: "error" });
            });
    };

};
