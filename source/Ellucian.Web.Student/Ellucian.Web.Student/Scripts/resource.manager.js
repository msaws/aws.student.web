﻿/*
 * assumed dependencies
 *  ko
 *  jquery
 *  actionUrls
 */

(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        define([], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory();
    } else {
        root.resourceManager = factory();
    }

}(this, function() {
    //use session storage as a fallback
    var storage = window.sessionStorage;

    //try to use Ellucian.Storage.session (session storage wrapper) if it exists
    if (window.Ellucian && window.Ellucian.Storage && window.Ellucian.Storage.session) {
        storage = window.Ellucian.Storage.session;
    }
    

    /*
     * Resx binding handler allows you bind DOM elements to a resource string.
     * The resource tables are added to the browser's session storage cache after retrieval from the server.
     * The binding also lets you specify format parameters when accessing a format string.
     * 
     * Normal usage:
     * <span data-bind="resx: 'AutoSave.InitialSaveMessage'"></span>
     * Result: 
     * <span>Changes are automatically saved</span>
     * 
     * 
     * Format Paramter Usage
     * <span data-bind="resx: ['AutoSave.LastSavedMessage', 'June 4']"></span>
     * Result:
     * <span>Last saved on June 4</span>
    */
    ko.bindingHandlers.resx = ko.bindingHandlers.resx || {
        update: function (element, valueAccessor, allBindings) {
            var resx = ko.utils.unwrapObservable(valueAccessor());
            var resourceId = Array.isArray(resx) ? resx[0] : resx;
            var formatParams = Array.isArray(resx) && resx.length > 1 ? resx.slice(1) : [];
            if (resourceId) {
                getFormattedFromStorageOrServerAsync(resourceId, formatParams).then(function (resourceString) {
                    $(element).text(resourceString);
                });
            }
        }
    }

    var observableMap = {};
    var resourceContainer = function () {
        var self = this;
        self.rawString = ko.observable();
        self.formatParams = ko.observable();
        self.result = ko.pureComputed(function () {
            if (self.rawString()) {
                if (self.formatParams() && self.formatParams().length > 0) {
                    return self.rawString().format(self.formatParams());
                }
                return self.rawString();
            }
            return null;
        })
    }
    function getObservable(resourceId, formatParams) {

        if (observableMap[resourceId]) {
            if (formatParams && formatParams.length > 0) {
                observableMap[resourceId].formatParams(formatParams);
                //observableMap[resourceId](observableMap[resourceId]().format(formatParams));
            }
            return observableMap[resourceId].result;

        }

        observableMap[resourceId] = new resourceContainer();

        getUnformattedFromStorageOrServerAsync(resourceId, formatParams).then(function (resourceString) {
            observableMap[resourceId].rawString(resourceString);
            observableMap[resourceId].formatParams(formatParams);
            return;
        });
        return observableMap[resourceId].result;
    }



    function getFormattedFromStorageOrServerAsync(resourceId, formatParams) {

        return getUnformattedFromStorageOrServerAsync(resourceId).then(function (unformattedString) {
            var formatted = unformattedString.format(formatParams);
            return formatted;
        });
    }

    function getUnformattedFromStorageOrServerAsync(resourceId) {
        //if the resource string exists in storage, get it, format it and resolve
        var resourceString = storage.getItem(resourceId);
        if (resourceString) {

            return Promise.resolve(resourceString);
        } else {
            return loadResourceFileIntoCacheAsync(resourceId).then(function () {
                var resourceString = storage.getItem(resourceId);

                return resourceString;
            })
        }
    }

    //maintain a resourceLoader object where the key is the resource file and the value
    //is the promise that resolves the ajax request. Using this, each resource file will 
    //only be retrieved once, instead of each time the resource is requested while waiting for it to load
    //this occurs on a page load basis, but since the file was cached in Storage, that's ok.
    var resourceLoaders = {};
    function loadResourceFileIntoCacheAsync(resourceId) {

        var file = resourceId.split('.')[0];
        var url = (typeof getResourceUrl !== 'undefined') ? getResourceUrl + "/" + file : "/" + file;

        if (resourceLoaders[file]) {
            return resourceLoaders[file];
        }

        resourceLoaders[file] = Promise.resolve($.ajax(url)).then(function (data) {
            for (var key in data) {
                storage.setItem(file + "." + key, data[key]);
            }
            return;
        }).catch(function (err) {
            console.error(err);
        });

        return resourceLoaders[file];
    }



    return {
        get: function (resourceId, formatParams) {
            return getFormattedFromStorageOrServerAsync(resourceId, formatParams);
        },
        getObservable: function (resourceId, formatParams) {
            return getObservable(resourceId, formatParams);
        }
    }

}));