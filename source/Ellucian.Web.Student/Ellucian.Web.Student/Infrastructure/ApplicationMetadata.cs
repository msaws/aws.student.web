﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Student.Licensing;

namespace Ellucian.Web.Student.Infrastructure
{
    /// <summary>
    /// ApplicationMetadata acts as storage for useful metadata about the Self-Service web application
    /// </summary>
    public class ApplicationMetadata
    {
        private static string version = null;
        private static IEnumerable<string> licensedAreas = null;

        /// <summary>
        /// Retrieves the version of the application, as defined by the file version of the StudentApplication assembly, Ellucian.Web.Student.dll
        /// </summary>
        public static string Version
        {
            get
            {
                if (version != null)
                {
                    return version;
                }
                else
                {
                    try
                    {
                        System.Reflection.Assembly assembly = System.Reflection.Assembly.GetAssembly(typeof(Ellucian.Web.Student.StudentApplication));
                        System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
                        version = fvi.FileVersion;
                        return version;
                    }
                    catch (Exception ex)
                    {
                        var logger = DependencyResolver.Current.GetService<ILogger>();
                        logger.Info(ex, "Unable to retrieve application version.");

                        version = "Unknown";
                        return version;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves the list of all licensed area names in the application.
        /// </summary>
        public static IEnumerable<string> LicensedAreas
        {
            get
            {
                if (licensedAreas != null)
                {
                    return licensedAreas;
                }
                else
                {
                    var _licensedAreas = new List<string>();
                    var licensingService = DependencyResolver.Current.GetService<ILicensingService>();
                    var licensedModuleCodes = licensingService.GetLicensedModules();
                    foreach (var areaList in ModuleConstants.ModuleAreas)
                    {
                        if (licensedModuleCodes.Contains(areaList.Key))
                        {
                            _licensedAreas.AddRange(areaList.Value);
                        }
                    }

                    licensedAreas = _licensedAreas;
                    return licensedAreas;
                }
            }
        }
    }
}