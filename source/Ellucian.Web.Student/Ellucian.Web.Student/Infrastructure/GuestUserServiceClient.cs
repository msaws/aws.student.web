﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.IdentityModel.Tokens;
using System.Text.RegularExpressions;
using Ellucian.Web.Security;
using System.Security.Principal;

namespace Ellucian.Web.Student.Infrastructure
{
    /// <summary>
    /// This is a singleton class that return guest token
    /// It creates first time and then check on expiration date of token before it gets new one
    /// </summary>
    public sealed class GuestUserServiceClient
    {

       
            public static Settings Settings;
            public static ColleagueApiClient ServiceClient;
            private static volatile string token;
            private static object syncRoot = new Object();
            private static  DateTime tokenExpirationTime;
           
            public static string Token
            {
                get
                {
                        if (token != null)
                        {

                            if (DateTime.Now >= tokenExpirationTime )
                            {
                                token = null;
                            }
                        }
                        if (token == null)
                        {
                            lock (syncRoot)
                            {
                                if (token == null)
                                {
                                    try
                                    {
                                        Assembly assembly = Assembly.GetExecutingAssembly();
                                        FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                                        string userName = EncryptionUtility.Decrypt(Settings.GuestUsername);
                                        string password = EncryptionUtility.Decrypt(Settings.GuestPassword);
                                        token = Task.Run(async () =>
                                        {
                                            return await ServiceClient.Login2Async(userName, password, "StuSelfSvc", fvi.FileVersion);
                                        }).GetAwaiter().GetResult();

                                        DateTime tokenCreationTime = DateTime.Now;
                                        int timeoutInSeconds = 0;
                                        var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(token);
                                        string timeout = claim.Identities.First().Claims.First(c => c.ClaimType == ClaimConstants.SessionTimeout).Value;
                                        bool isSuccess = Int32.TryParse(timeout, out timeoutInSeconds);
                                        tokenExpirationTime = tokenCreationTime.AddSeconds(timeoutInSeconds);
                                    }
                                    catch(Exception ex)
                                    {
                                        throw new Ellucian.Web.Student.Filters.GuestAccessException(ex.Message, ex);
                                    }
                                }
                            }
                        }
                    
                    return token;

                }
            }
    }
}