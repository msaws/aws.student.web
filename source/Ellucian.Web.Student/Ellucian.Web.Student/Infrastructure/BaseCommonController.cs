﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client;
using Ellucian.Web.Mvc.Controller;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Utility;
using Microsoft.IdentityModel.Claims;
using slf4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Infrastructure
{
    /// <summary>
    /// This controller is commonly used for anonymous or guest access and logged in user
    /// </summary>
    public class BaseCommonController : BaseStudentController
    {
        protected readonly Settings Settings;

        public BaseCommonController(Settings settings, ILogger logger)
            : base(settings, logger)
        {
            this.Settings = settings;
            GuestUserServiceClient.Settings = settings;
            GuestUserServiceClient.ServiceClient = ServiceClient;
        }

        public override void Initialize()
        {
            // Assign session credentials before sending the request
            if (User != null && User is IClaimsPrincipal)
            {
                ServiceClient.Credentials = JwtHelper.Create(User);
                this.ViewBag.IsGuest = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(Settings.GuestUsername) && User.Identity.IsAuthenticated==false && User.Identity!=null && User!=null)
                {
                    var token=GuestUserServiceClient.Token;
                    ServiceClient.Credentials = token;
                    this.ViewBag.IsGuest = true;
                }
            }
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            if (ex is Ellucian.Web.Student.Filters.GuestAccessException)
            {
                filterContext.ExceptionHandled = true;
                filterContext.Result=RedirectToAction("Unauthorized", "Account");
            }
            base.OnException(filterContext);
        }
    }
}