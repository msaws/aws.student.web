﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Configuration;
using Ellucian.Web.Mvc.Controller;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Colleague.Api.Client;
using Microsoft.IdentityModel.Claims;
using System.Linq;
using Ellucian.Web.Mvc.Models;
using slf4net;
using Ellucian.Web.Security;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using System.Web.Mvc;
using System.Threading.Tasks;
using System;
using Ellucian.Web.Student.Models.Person;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;


namespace Ellucian.Web.Student.Infrastructure
{
    [InitializeCredentials]
    public class BaseStudentController : BaseCompressedController
    {
        protected readonly ColleagueApiClient ServiceClient;
        private ICurrentUser currentUser;

        public BaseStudentController(Settings settings, ILogger logger)
            : base(logger)
        {
            ServiceClient = new ColleagueApiClient(settings.ApiBaseUrl, settings.ApiConnectionLimit, logger);
            ServiceClient.Credentials = null;

        }

        public override void Initialize()
        {
                // Assign session credentials before sending the request
           
                ServiceClient.Credentials = JwtHelper.Create(User);
                this.ViewBag.IsGuest = false;
        }

        /// <summary>
        /// Returns the person ID of the proxy subject (if present);
        /// otherwise, returns the person ID of the authenticated user
        /// </summary>
        /// <returns>Person ID of the proxy subject (if present) or the authenticated user (if no proxy subject present)</returns>
        protected string GetEffectivePersonId()
        {
            return (CurrentUser.ProxySubjects != null && CurrentUser.ProxySubjects.Any()) ? CurrentUser.ProxySubjects.First().PersonId : GetAuthenticatedPersonId();
        }

        /// <summary>
        /// Returns the person ID of the authenticated user
        /// </summary>
        /// <returns>Person ID of the authenticated user</returns>
        protected string GetAuthenticatedPersonId()
        {
            return CurrentUser.PersonId;
        }

        protected ICurrentUser CurrentUser
        {
            get
            {
                if (currentUser == null && User is IClaimsPrincipal)
                {
                    currentUser = new CurrentUser(User as IClaimsPrincipal);
                }
                return currentUser;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public IEnumerable<UrlFilter> ConvertFilterIntoUrlFilter(string[][] filter)
        {
            // Create a list of UrlFilter objects from the URL parameters
            var urlFilter = new List<UrlFilter>();
            if (filter != null)
            {
                for (int i = 0; i < filter.Length; i++)
                {
                    // Only include the filter option if it has a field (index: 0) and value (index: 1)
                    if (filter[i][0].Length > 0 && filter[i][1].Length > 0)
                    {
                        var filterCriteria = new UrlFilter();
                        filterCriteria.Field = filter[i][0];
                        filterCriteria.Value = filter[i][1];
                        urlFilter.Add(filterCriteria);
                    }
                }
            }
            return urlFilter;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Gets the description for a term
        /// </summary>
        /// <param name="termCode"></param>
        /// <returns>Term description</returns>
        protected async Task<string> GetTermDescription(string termCode)
        {
            if (string.IsNullOrEmpty(termCode))
            {
                return string.Empty;
            }
            string termDescription = termCode;
            var allTerms = await ServiceClientCache.GetCachedTermsAsync(ServiceClient);
            if (allTerms != null)
            {
                var planTerm = allTerms.Where(term => term.Code == termCode).FirstOrDefault();
                termDescription = planTerm != null ? planTerm.Description : termDescription;
            }
            return termDescription;
        }

        /// <summary>
        /// Determines if the current user has proxy access to the specified workflow for the specified person
        /// </summary>
        /// <param name="personId">Person ID</param>
        /// <param name="workflowId">Proxy Workflow ID</param>
        /// <returns>Flag indicating if the current user has proxy access to the specified workflow for the specified person</returns>
        protected bool HasProxyAccessToWorkflowForPerson(string personId, string workflowId)
        {
            if (string.IsNullOrEmpty(personId) || string.IsNullOrEmpty(workflowId))
            {
                return false;
            }

            return (CurrentUser.ProxySubjects != null && 
                CurrentUser.ProxySubjects.First() != null && 
                CurrentUser.ProxySubjects.First().PersonId == personId &&
                CurrentUser.ProxySubjects.First().Permissions != null &&
                CurrentUser.ProxySubjects.First().Permissions.Any(p => string.Equals(p, workflowId, System.StringComparison.CurrentCultureIgnoreCase)));
        }

        /// <summary>
        /// Sets privacy properties for PersonSearchResultModel
        /// </summary>
        /// <param name="model">PersonSearchResultModel</param>
        /// <returns></returns>
        protected async Task SetPrivacyPropertiesForAdminModel(PersonSearchResultModel model)
        {
            if (model == null)
            {
                return;
            }

            // If no privacy code, the data does not have a privacy restriction or associated message
            if (string.IsNullOrEmpty(model.PrivacyStatusCode))
            {
                model.HasPrivacyRestriction = false;
                model.PrivacyMessage = null;
                return;
            }
            else
            {
                // Users have access to their own data and person proxy can access person's data
                if (CurrentUser.IsPerson(model.Id) || model.Id == GetEffectivePersonId())
                {
                    model.HasPrivacyRestriction = false;
                    model.PrivacyMessage = null;
                }
                else
                {
                    // Administrators must have access to the privacy code in order to view the data;
                    // access the admin user's staff record (if available) to determine their privacy access privileges
                    Staff staff = null;
                    try
                    {
                        staff = await ServiceClient.GetStaffAsync(CurrentUser.PersonId);
                    }
                    catch
                    {
                        staff = new Staff(CurrentUser.PersonId, string.Empty) { PrivacyCodes = new List<string>() };
                    }

                    // Data is private if admin user does have access to the privacy code
                    model.HasPrivacyRestriction = !staff.PrivacyCodes.Contains(model.PrivacyStatusCode);
                    if (model.HasPrivacyRestriction)
                    {
                        // Use the record denial message from PID5 if the user does not have access to the data
                        PrivacyConfiguration privacyConfig = await ServiceClient.GetPrivacyConfigurationAsync();
                        model.PrivacyMessage = privacyConfig != null ? privacyConfig.RecordDenialMessage : string.Empty;
                    }
                    else
                    {
                        // Use the privacy warning message from PID5 if the user has access to the data
                        model.PrivacyMessage = await ServiceClient.GetCachedPrivacyMessageAsync(model.PrivacyStatusCode);
                    }
                }
                return;
            }
        }
    }
}