﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Api.Client;
using System.Web.Mvc;
using Ellucian.Web.Cache;
using System.Timers;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Threading;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Infrastructure
{
    public static class ServiceClientCache
    {
        /// <summary>
        /// Cache timeout in minutes
        /// </summary>
        private static double CacheTimeout = 5;

        /// <summary>
        /// Check for cached rooms else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.Room>> GetCachedRoomsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.Room>>("API_Rooms", async () =>
            {
                return await serviceClient.GetRoomsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.Building>> GetCachedBuildingsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.Building>>("API_Buildings", async () =>
            {
                return await serviceClient.GetBuildingsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.SessionCycle>> GetCachedSessionCyclesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.SessionCycle>>("API_SessionCycles", async () =>
                 {
                     return await serviceClient.GetSessionCyclesAsync();
                 });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.YearlyCycle>> GetCachedYearlyCyclesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.YearlyCycle>>("API_YearlyCycles", async () =>
                 {
                     return await serviceClient.GetYearlyCyclesAsync();
                 });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Grade>> GetCachedGradesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Grade>>("API_Grades", async () =>
            {
                return await serviceClient.GetGradesAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.Location>> GetCachedLocationsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.Location>>("API_Locations", async () =>
            {
                return await serviceClient.GetLocationsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Program>> GetCachedProgramsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Program>>("API_Programs", async () =>
            {
                return await serviceClient.GetProgramsAsync();
            });
        }

        /// <summary>
        /// Retrieve cached account holder with the specified ID or call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service Client</param>
        /// <param name="approvalDocumentId">Account Holder ID</param>
        /// <returns>Account Holder</returns>
        public static async Task<Ellucian.Colleague.Dtos.Finance.AccountHolder> GetCachedAccountHolderAsync(this ColleagueApiClient serviceClient, string accountHolderId)
        {
            if (string.IsNullOrEmpty(accountHolderId))
            {
                return null;
            }
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Finance.AccountHolder>("API_AccountHolder_" + accountHolderId, async () =>
            {
                return await Task.FromResult(serviceClient.GetAccountHolder2(accountHolderId));
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Program>> GetCachedActiveProgramsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Program>>("API_ActivePrograms", async () =>
            {
                return await serviceClient.GetActiveProgramsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Term>> GetCachedTermsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Term>>("API_Terms", async () =>
            {
                return await serviceClient.GetTermsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Term>> GetCachedPlanningTermsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Term>>("API_PlanningTerms", async () =>
            {
                return await serviceClient.GetPlanningTermsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Term>> GetCachedRegistrationTermsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Term>>("API_RegistrationTerms", async () =>
            {
                return await serviceClient.GetRegistrationTermsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.InstructionalMethod>> GetCachedInstructionalMethodsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.InstructionalMethod>>("API_InstructionalMethods", async () =>
            {
                return await serviceClient.GetInstructionalMethodsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Subject>> GetCachedSubjectsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Subject>>("API_Subjects", async () =>
            {
                return await serviceClient.GetSubjectsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.Department>> GetCachedDepartmentsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.Department>>("API_Departments", async () =>
            {
                return await serviceClient.GetDepartmentsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.AdvisorType>> GetCachedAdvisorTypesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.AdvisorType>>("API_AdvisorTypes", async () =>
            {
                return await serviceClient.GetAdvisorTypesAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.AcademicLevel>> GetCachedAcademicLevelsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.AcademicLevel>>("API_AcademicLevels", async () =>
            {
                return await serviceClient.GetAcademicLevelsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.CourseLevel>> GetCachedCourseLevelsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.CourseLevel>>("API_CourseLevels", async () =>
            {
                return await serviceClient.GetCourseLevelsAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.CourseType>> GetCachedCourseTypesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.CourseType>>("API_CourseTypes", async () =>
            {
                return await serviceClient.GetCourseTypesAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.TopicCode>> GetCachedTopicCodesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.TopicCode>>("API_TopicCodes", async () =>
            {
                return await serviceClient.GetTopicCodesAsync();
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Finance.PaymentPlanTemplate>> GetCachedPaymentPlanTemplatesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Finance.PaymentPlanTemplate>>("API_PaymentPlanTemplates", async () =>
            {
                return await Task.FromResult(serviceClient.GetPaymentPlanTemplates());
            });
        }

        public static async Task<Ellucian.Colleague.Dtos.Finance.PaymentPlan> GetCachedProposedPaymentPlanAsync(this ColleagueApiClient serviceClient,
            string personId, string termId, string receivableTypeCode, decimal planAmount)
        {
            if (string.IsNullOrEmpty(personId) || string.IsNullOrEmpty(termId) || string.IsNullOrEmpty(receivableTypeCode)) return null;
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Finance.PaymentPlan>("API_ProposedPaymentPlan_" 
                + personId + "_" 
                + termId + "_" 
                + receivableTypeCode + "_" 
                + planAmount.ToString(), async () =>
            {
                return await serviceClient.GetProposedPaymentPlanAsync(personId, termId, receivableTypeCode, planAmount);
            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.PetitionStatus>> GetCachedPetitionStatusesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.PetitionStatus>>("API_PetitionStatuses", async () =>
            {
                return await serviceClient.GetPetitionStatusesAsync();

            });
        }

        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.StudentPetitionReason>> GetCachedStudentPetitionReasonsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.StudentPetitionReason>>("API_PetitionReasons", async () =>
            {
                return await serviceClient.GetStudentPetitionReasonsAsync();
            });
        }

        /// <summary>
        /// Check for cached phone types else call service client to get them.
        /// </summary>
        /// <param name="serviceClient"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.PhoneType>> GetCachedPhoneTypesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.PhoneType>>("API_PhoneTypes", async () =>
            {
                return await serviceClient.GetPhoneTypesAsync();
            });
        }

        /// <summary>
        /// Check for cached email types else call service client to get them.
        /// </summary>
        /// <param name="serviceClient"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.EmailType>> GetCachedEmailTypesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.EmailType>>("API_EmailTypes", async () =>
            {
                return await serviceClient.GetEmailTypesAsync();
            });
        }

        /// <summary>
        /// Check for cached personal pronun types else call service client to get them.
        /// </summary>
        /// <param name="serviceClient"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.PersonalPronounType>> GetCachedPersonalPronounTypesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.PersonalPronounType>>("API_PersonalPronounTypes", async () =>
            {
                return await serviceClient.GetPersonalPronounTypesAsync();
            });
        }

        /// <summary>
        /// Check for cached address types else call service client to get them.
        /// </summary>
        /// <param name="serviceClient"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.AddressType2>> GetCachedAddressTypesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.AddressType2>>("API_AddressTypes", async () =>
            {
                return await serviceClient.GetAddressTypesAsync();
            });
        }

        /// <summary>
        /// Check for cached user configuration else call service client to get them.
        /// </summary>
        /// <param name="serviceClient"></param>
        /// <returns></returns>
        public static async Task<Ellucian.Colleague.Dtos.Base.UserProfileConfiguration> GetCachedUserProfileConfigurationAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Base.UserProfileConfiguration>("API_UserProfileConfiguration", async () =>
            {
                return await serviceClient.GetUserProfileConfigurationAsync();
            });
        }

        /// <summary>
        /// Check for cached user configuration else call service client to get them.
        /// </summary>
        /// <param name="serviceClient"></param>
        /// <returns></returns>
        public static async Task<Ellucian.Colleague.Dtos.Base.UserProfileConfiguration2> GetCachedUserProfileConfiguration2Async(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Base.UserProfileConfiguration2>("API_UserProfileConfiguration2", async () =>
            {
                return await serviceClient.GetUserProfileConfiguration2Async();
            });
        }

        /// <summary>
        /// Check for cached commencement sites else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.CommencementSite>> GetCachedCommencementSitesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.CommencementSite>>("API_CommencementSites", async () =>
            {
                return await serviceClient.GetCommencementSitesAsync();
            });
        }

        /// <summary>
        /// Check for cached cap sizes else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.CapSize>> GetCachedCapSizesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.CapSize>>("API_CapSizes", async () =>
            {
                return await serviceClient.GetCapSizesAsync();
            });
        }

        /// <summary>
        /// Check for cached gown sizes else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.GownSize>> GetCachedGownSizesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.GownSize>>("API_GownSizes", async () =>
            {
                return await serviceClient.GetGownSizesAsync();
            });
        }

        /// <summary>
        /// Check for cached countries else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.Country>> GetCachedCountriesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.Country>>("API_Countries", async () =>
            {
                return await serviceClient.GetCountriesAsync();
            });
        }
        /// <summary>
        /// Check for cached states else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.State>> GetCachedStatesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.State>>("API_States", async () =>
            {
                return await serviceClient.GetStatesAsync();
            });
        }

        /// <summary>
        /// Check for cached consent paragraphs for tax forms, else call the service client to get them from the API cache.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <param name="taxFormId">The tax form (W-2, 1095-C, 1099-Misc, etc)</param>
        /// <returns>The consent and withheld paragraphs for the type of tax form</returns>
        public static async Task<Ellucian.Colleague.Dtos.Base.TaxFormConfiguration> GetCachedTaxFormConfigurationaAsync(this ColleagueApiClient serviceClient, Ellucian.Colleague.Dtos.Base.TaxForms taxFormId)
        {
            // Set the cache object depending on what form we are processing
            var cacheId = string.Empty;
            switch (taxFormId)
            {
                case Colleague.Dtos.Base.TaxForms.FormW2:
                    cacheId = "W2ConsentParagraphs";
                    break;
                case Colleague.Dtos.Base.TaxForms.Form1095C:
                    cacheId = "1095cConsentParagraphs";
                    break;
            }
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Base.TaxFormConfiguration>(cacheId, async () =>
            {
                return await serviceClient.GetTaxFormConfiguration(taxFormId);
            });
        }

        /// <summary>
        /// Check for cached majors else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Major>> GetCachedMajorsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Major>>("API_Majors", async () =>
            {
                return await serviceClient.GetMajorsAsync();
            });
        }

        /// <summary>
        /// Check for cached minors else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Minor>> GetCachedMinorsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Minor>>("API_Minors", async () =>
            {
                return await serviceClient.GetMinorsAsync();
            });
        }

        /// <summary>
        /// Check for cached specializations else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.Specialization>> GetCachedSpecializationsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Student.Specialization>>("API_Specializations", async () =>
            {
                return await serviceClient.GetSpecializationsAsync();
            });
        }

        /// <summary>
        /// Check for cached graduation configuration else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<Ellucian.Colleague.Dtos.Student.GraduationConfiguration> GetCachedGraduationConfigurationAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Student.GraduationConfiguration>("API_Grad_Configuration", async () =>
            {
                return await serviceClient.GetGraduationConfigurationAsync();
            });
        }

        /// <summary>
        /// Check for cached roles else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.Base.Role>> GetCachedRolesAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.Base.Role>>("API_Roles", async () =>
            {
                return await serviceClient.GetRolesAsync();
            });
        }

        /// <summary>
        /// Gets a list of MiscellaneousText objects from Colleague.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>IEnumerable of MiscellaneousText objects</returns>
        public static async Task<IEnumerable<MiscellaneousText>> GetCachedMiscellaneousTextsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<MiscellaneousText>>("MTxt_API", async () =>
            {
                return await serviceClient.GetAllMiscellaneousTextAsync();
            });
        }

        /// <summary>
        /// Check for cached health conditions else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>Health conditions</returns>
        public static async Task<List<Ellucian.Colleague.Dtos.Base.HealthConditions>> GetCachedHealthConditionsAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<List<Ellucian.Colleague.Dtos.Base.HealthConditions>>("API_HealthConditions", async () =>
            {
                return await serviceClient.GetHealthConditionsAsync();
            });
        }
        /// <summary>
        /// Check for cached emergency information configuration else call service client to get them.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>EmergencyInformationConfiguration</returns>
        public static async Task<Ellucian.Colleague.Dtos.Base.EmergencyInformationConfiguration2> GetCachedEmergencyInformationConfigurationAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Base.EmergencyInformationConfiguration2>("API_EmergencyInformationConfiguration", async () =>
            {
                return await serviceClient.GetEmergencyInformationConfigurationAsync();
            });
        }

        /// <summary>
        /// Check for cached student finance configuration else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>FinanceConfiguration</returns>
        public static async Task<Ellucian.Colleague.Dtos.Finance.Configuration.FinanceConfiguration> GetCachedFinanceConfiguration(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Finance.Configuration.FinanceConfiguration>("API_FinanceConfiguration", async () =>
            {
                return await Task.FromResult(serviceClient.GetConfiguration());
            });
        }

        /// <summary>
        /// Check for cached immediate payment control configuration else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>FinanceConfiguration</returns>
        public static async Task<Ellucian.Colleague.Dtos.Finance.ImmediatePaymentControl> GetCachedImmediatePaymentControl(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Finance.ImmediatePaymentControl>("API_ImmediatePaymentControl", async () =>
            {
                return await Task.FromResult(serviceClient.GetImmediatePaymentControl());
            });
        }

        /// <summary>
        /// Check for cached general ledger configuration else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>GeneralLedgerConfiguration</returns>
        public static async Task<Ellucian.Colleague.Dtos.ColleagueFinance.GeneralLedgerConfiguration> GetCachedGeneralLedgerConfiguration(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.ColleagueFinance.GeneralLedgerConfiguration>("API_GeneralLedgerConfiguration", async () =>
            {
                return await serviceClient.GetGeneralLedgerConfigurationAsync();
            });
        }

        /// <summary>
        /// Check for cached fiscal years else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>List of fiscal years</returns>
        public static async Task<IEnumerable<string>> GetCachedFiscalYears(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<string>>("API_FiscalYears", async () =>
            {
                return await serviceClient.GetFiscalYearsAsync();
            });
        }

        /// <summary>
        /// Check for cached accounts payable types else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>List of Accounts Payable Types</returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.ColleagueFinance.AccountsPayableType>> GetCachedAccountsPayableTypes(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.ColleagueFinance.AccountsPayableType>>("API_AccountsPayableType", async () =>
            {
                return await serviceClient.GetAccountsPayableTypesAsync();
            });
        }

        /// <summary>
        /// Check for cached accounts payable taxes else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns>List of Accounts Payable Taxes</returns>
        public static async Task<IEnumerable<Ellucian.Colleague.Dtos.ColleagueFinance.AccountsPayableTax>> GetCachedAccountsPayableTaxes(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.ColleagueFinance.AccountsPayableTax>>("API_AccountsPayableTax", async () =>
            {
                return await serviceClient.GetAccountsPayableTaxesAsync();
            });
        }

        /// <summary>
        /// Check for cached privacy statuses, else call service client to get them.
        /// </summary>
        /// <param name="serviceClient"></param>
        /// <returns></returns>
        public static async Task<string> GetCachedPrivacyMessageAsync(this ColleagueApiClient serviceClient, string privacyCode)
        {
            // If the privacy code is null empty, immediately return
            if (string.IsNullOrEmpty(privacyCode)) return string.Empty;
            var privacyConfig = await GetCachedPrivacyConfigurationAsync(serviceClient);
            string privacyMessage = privacyConfig != null ? privacyConfig.RecordDenialMessage : string.Empty;

            var privacyStatuses = await GetOrAddToCacheAsync<IEnumerable<Ellucian.Colleague.Dtos.PrivacyStatus>>("API_PrivacyStatuses", async () =>
            {
                return await serviceClient.GetPrivacyStatusesAsync();
            });

            if (privacyStatuses != null)
            {
                var privacyStatus = privacyStatuses.FirstOrDefault(x => x.Code == privacyCode);

                if (privacyStatus != null)
                {
                    privacyMessage = !string.IsNullOrEmpty(privacyStatus.Description) ? privacyStatus.Description : privacyStatus.Title;
                }
            }

            return privacyMessage;
        }

        public static async Task<PrivacyConfiguration> GetCachedPrivacyConfigurationAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Base.PrivacyConfiguration>("API_PrivacyConfiguration", async () =>
            {
                return await serviceClient.GetPrivacyConfigurationAsync();
            });
        }

        /// <summary>
        /// Check for cached course catalog configuration else call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service client being extended</param>
        /// <returns></returns>
        public static async Task<Ellucian.Colleague.Dtos.Student.CourseCatalogConfiguration> GetCachedCourseCatalogConfigurationAsync(this ColleagueApiClient serviceClient)
        {
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Student.CourseCatalogConfiguration>("API_Catalog_Configuration", async () =>
            {
                return await serviceClient.GetCourseCatalogConfigurationAsync();
            });
        }

        /// <summary>
        /// Retrieve cached approval document with the specified ID or call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service Client</param>
        /// <param name="approvalDocumentId">Approval Document ID</param>
        /// <returns>Approval Document</returns>
        public static async Task<Ellucian.Colleague.Dtos.Base.ApprovalDocument> GetCachedApprovalDocumentAsync(this ColleagueApiClient serviceClient, string approvalDocumentId)
        {
            if (string.IsNullOrEmpty(approvalDocumentId))
            {
                return null;
            }
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Base.ApprovalDocument>("API_ApprovalDocument_" + approvalDocumentId, async () =>
            {
                return await serviceClient.GetApprovalDocumentAsync(approvalDocumentId);
            });
        }

        /// <summary>
        /// Retrieve cached approval response with the specified ID or call service client to get it.
        /// </summary>
        /// <param name="serviceClient">Colleague API Service Client</param>
        /// <param name="approvalDocumentId">Approval Response ID</param>
        /// <returns>Approval Response</returns>
        public static async Task<Ellucian.Colleague.Dtos.Base.ApprovalResponse> GetCachedApprovalResponseAsync(this ColleagueApiClient serviceClient, string approvalResponseId)
        {
            if (string.IsNullOrEmpty(approvalResponseId))
            {
                return null;
            }
            return await GetOrAddToCacheAsync<Ellucian.Colleague.Dtos.Base.ApprovalResponse>("API_ApprovalResponse_" + approvalResponseId, async () =>
            {
                return await serviceClient.GetApprovalResponseAsync(approvalResponseId);
            });
        }

        /// <summary>
        /// This method allows you to try to get an object from the cache and failover to a function
        /// that will create an object if necessary. 
        /// </summary>
        /// <typeparam name="T">requested object type</typeparam>
        /// <param name="cacheKey">unique cache key</param>
        /// <param name="createObjectFunctionAsync">ASYNC function to create an object of type T</param>
        /// <param name="cacheTimeoutOverride">cache timeout override</param>
        /// <returns>object of type T or null</returns>
        public static async Task<T> GetOrAddToCacheAsync<T>(string cacheKey, Func<Task<T>> createObjectFunctionAsync, double? cacheTimeoutOverride = null)
        {
            return await getOrAddToCacheAsync(cacheKey, createObjectFunctionAsync, cacheTimeoutOverride);
        }

        private static async Task<T> getOrAddToCacheAsync<T>(string cacheKey, Func<Task<T>> createObjectFunctionAsync, double? cacheTimeoutOverride = null)
        {
            var cache = DependencyResolver.Current.GetService<ICacheProvider>();

            var fullCacheKey = string.Format("ServiceClient_{0}", cacheKey);
            var timeout = cacheTimeoutOverride ?? CacheTimeout;
            T requestedObject;

            if (cache.Contains(fullCacheKey))
            {
                // Key already exists in the cache; do a non-locking Get from the cache
                requestedObject = (T)cache.Get(fullCacheKey);
            }
            else
            {
                // Get and lock the cached item
                SemaphoreSlim lockHandle;
                var resultTuple = await cache.GetAndLockSemaphoreAsync(fullCacheKey).ConfigureAwait(false);
                requestedObject = (T)resultTuple.Item1;
                lockHandle = resultTuple.Item2;


                if (requestedObject == null)
                {
                    // Cache item does not yet exist; execute the function and add the new value to the cache
                    try
                    {

                        requestedObject = await createObjectFunctionAsync.Invoke().ConfigureAwait(false);

                        // The AddToCacheAndUnlockSemaphore call will update the cache and unlock the item
                        AddToCacheAndUnlockSemaphore(cache, fullCacheKey, requestedObject, timeout, lockHandle);
                    }
                    catch
                    {
                        // An exception occurred; that means the item in the cache is likely still locked; release it now
                        cache.UnlockSemaphore(fullCacheKey, lockHandle);

                        // Since the cache build method (in createObjectFunction) may throw a number of different
                        // exception types, need to catch any exception generically here, and re-throw it
                        throw;
                    }
                }
                else
                {
                    // Cache item already exists; unlock the lock we obtained and allow that item to be returned
                    cache.UnlockSemaphore(fullCacheKey, lockHandle);
                }
            }

            return requestedObject;
        }

        /// <summary>
        /// Adds an item to the cache then releases its lock.
        /// Usage Contract: If a lock was attained on the cache item prior to calling this method, then the corresponding lock handle MUST be
        /// passed into this method.  Doing so will correctly allow the cache item to be updated and the corresponding lock to be released
        /// so that subsequent attempts to update the cache item can proceed.
        /// Attempts to add null objects to cache will be ignored.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key to the cache item to add.</param>
        /// <param name="cacheObject">The object to be cached. Cannot be null.</param>
        /// <param name="timeout">The timeout/duration for the cache item.</param>
        /// <param name="lockHandle">The handle to the cached item if a lock was placed prior to this attempt to update; pass in null to 
        /// add the item without regard to locking.  This parameter is required if a lock was attained prior to calling this method</param>
        /// <exception cref="System.ArgumentNullException">cacheObject</exception>
        private static void AddToCacheAndUnlockSemaphore<T>(ICacheProvider cache, string key, T cacheObject, double timeout, SemaphoreSlim lockHandle)
        {
            var policy = new CacheItemPolicy();
            //policy.SlidingExpiration = TimeSpan.FromMinutes(timeout);
            policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(timeout);

            if (lockHandle == null)
            {
                // No lock handle provided; add the item to the cache without regard to locking
                cache.Add(key, cacheObject, policy);
            }
            else
            {
                // Lock handle provided; update and release the lock
                cache.AddAndUnlockSemaphore(key, cacheObject, lockHandle, policy);
            }
        }

        /// <summary>
        /// Clears entries from the cache.
        /// </summary>
        /// <param name="removalFilter">List of cache key filters; if provided, only those cache keys which contain the strings in this list will be cleared from the cache.</param>
        public static void ClearCache(List<string> removalFilter = null)
        {
            var cache = DependencyResolver.Current.GetService<ICacheProvider>();
            // First, select all cache keys
            List<string> allCacheKeys = cache.Select(x => x.Key).ToList();

            if (allCacheKeys != null && allCacheKeys.Count > 0)
            {
                // There are items in the cache; filter them (if applicable) before clearing
                List<string> clearCacheKeys = new List<string>();

                if ((removalFilter != null) && (removalFilter.Count > 0))
                {
                    // Filter the keys down to only those that match the removal filters
                    clearCacheKeys = allCacheKeys.Where(k => removalFilter.Any(f => k.Contains(f))).Distinct().ToList();
                }
                else
                {
                    // No filter; clear all
                    clearCacheKeys = allCacheKeys;
                }

                // Remove each of the keys in our assembled list
                foreach (string removeKey in clearCacheKeys)
                {
                    cache.Remove(removeKey);
                }
            }
        }
    }
}