﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Infrastructure
{
    public static class Extensions
    {
        public static string LogAggregateExceptions(this AggregateException ae, ILogger logger)
        {
            if (ae == null)
            {
                return string.Empty;
            }
            ae.Handle((x) =>
            {
                logger.Error(x,x.ToString());
                return true;
            });
           return ae.Flatten().InnerException.Message;
        }
    }
}
