﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Mvc.Session;
using Ellucian.Web.Security;
using Microsoft.IdentityModel.Claims;
using slf4net;

namespace Ellucian.Web.Student.Infrastructure
{
    /// <summary>
    /// Provides central authentication actions.
    /// </summary>
    public class UserAuthentication
    {

        private ColleagueApiClient client;
        private HttpContextBase httpContext;
        private ILogger logger;


        public UserAuthentication(ColleagueApiClient apiClient, HttpContextBase context, ILogger logger)
        {
            this.client = apiClient;
            this.httpContext = context;
            this.logger = logger;
        }

        public async Task<bool> ValidateProxy(string proxyUserId, string proxyPassword, string userId)
        {
            return await ValidateUser(proxyUserId, proxyPassword, userId);
        }

        public async Task<bool> Validate(string userId, string password)
        {
            return await ValidateUser(userId, password);
        }

        public async Task<bool> InvalidateUser()
        {
            try
            {
                SessionCookieManager.RemoveSessionCookies(this.httpContext);
                if (this.httpContext.User is IClaimsPrincipal)
                {
                    // Sanitize username claims to remove domain
                    httpContext.User = RemoveDomainFromUsernameClaim(httpContext.User as IClaimsPrincipal);

                    await client.LogoutAsync(JwtHelper.Create(this.httpContext.User));
                }
                return true;
            }
            catch (Exception ex)
            {
                this.logger.Error(ex, "InvalidateUser error");
                return false;
            }
        }

        /// <summary>
        /// Removes the domain from the username of the supplied <see cref="IClaimsPrincipal"/> 
        /// </summary>
        /// <param name="user"><see cref="IClaimsPrincipal"/> to act upon</param>
        /// <returns><see cref="IClaimsPrincipal"/> without the domain</returns>
        public static IClaimsPrincipal RemoveDomainFromUsernameClaim(IClaimsPrincipal user)
        {
            // Input validations
            if (user == null)
            {
                throw new ArgumentNullException("IClaimsPrincipal cannot be null.");
            }
            if (!(user is IClaimsPrincipal))
            {
                throw new ArgumentException("Type must be IClaimsPrincipal", "user");
            }

            // Get the set of all user ID claims for the user.
            List<Claim> userIdClaims = user.Identities.First().Claims.Where(x => x.ClaimType == ClaimConstants.UserId).ToList();

            // Check each claim 
            foreach (var claim in userIdClaims)
            {
                // If we find a DOMAIN, remove and replace the claim with just the username
                // Check for the DOMAIN\[user ID] pattern
                string[] splitClaim = claim.Value.Split('\\');

                if (splitClaim.Length > 1)
                {
                    user.Identities.First().Claims.Remove(claim);
                    user.Identities.First().Claims.Add(new Claim(claim.ClaimType, splitClaim[1]));
                }
                else
                {
                    // Check for [user ID]@DOMAIN pattern
                    splitClaim = claim.Value.Split('@');
                    if (splitClaim.Length > 1)
                    {
                        user.Identities.First().Claims.Remove(claim);
                        user.Identities.First().Claims.Add(new Claim(claim.ClaimType, splitClaim[0]));
                    }
                    else
                    {
                        // Check for DOMAIN/[user ID] pattern
                        splitClaim = claim.Value.Split('/');
                        if (splitClaim.Length > 1)
                        {
                            user.Identities.First().Claims.Remove(claim);
                            user.Identities.First().Claims.Add(new Claim(claim.ClaimType, splitClaim[1]));
                        }
                    }
                }
            }

            return user;
        }

        private async Task<bool> ValidateUser(string userId, string password, string proxyUserId = null)
        {
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                string token;

                if (proxyUserId != null)
                {
                    logger.Trace("ValidateUser(" + userId + ") calling ColleagueApiClient.ProxyLogin version(" + fvi.FileVersion + ")");
                    token = await client.ProxyLogin2Async(userId, password, proxyUserId, "StuSelfSvc", fvi.FileVersion);
                }
                else
                {
                    logger.Trace("ValidateUser(" + userId + ") calling ColleagueApiClient.Login version(" + fvi.FileVersion + ")");
                    token = await client.Login2Async(userId, password, "StuSelfSvc", fvi.FileVersion);
                }

                httpContext.User = JwtHelper.CreatePrincipal(token);

                // Sanitize username claims to remove domain
                httpContext.User = RemoveDomainFromUsernameClaim(httpContext.User as IClaimsPrincipal);

                // Regenerate the token based on the updated set of claims
                token = JwtHelper.Create(httpContext.User);

                // set cookies
                var authCookies = SessionCookieManager.BuildJwtAuthenticationCookies(httpContext.User as IClaimsPrincipal, token);
                foreach (var authCookie in authCookies)
                {
                    httpContext.Response.Cookies.Add(authCookie);
                }
                var fixationCookie = SessionCookieManager.BuildSessionFixationCookie(httpContext.User as IClaimsPrincipal);
                httpContext.Response.Cookies.Add(fixationCookie);
                return true;
            }
            catch (LoginException lex)
            {
                logger.Info(lex.Message);
                return false;
            }
            catch (PasswordExpiredException exp)
            {
                logger.Info(exp.Message);
                throw exp;
            }
            catch (ListenerNotFoundException lx)
            {
                logger.Error(lx.Message);
                throw;
            }
            catch (HttpRequestFailedException hx)
            {
                logger.Error(hx.Message);
                throw;
            }
            catch (Exception ex)
            {
                logger.Debug(ex, "ValidateUser - Exception");
                return false;
            }
        }

        /// <summary>
        /// Asynchronously add a proxy subject to the user's current session
        /// </summary>
        /// <param name="subject">The proxy subject to add</param>
        /// <returns>Boolean indicating whether or not the session was updated successfully</returns>
        public async Task<bool> AddProxySubjectToSession(Colleague.Dtos.Base.ProxySubject subject)
        {
            if (subject == null)
            {
                throw new ArgumentNullException("subject", "A proxy subject must be provided.");
            }
            try
            {
                string token;

                logger.Trace("ValidateProxySubject(" + subject.Id + ") calling ColleagueApiClient.PutSessionProxySubjectsAsync");
                token = await client.PutSessionProxySubjectsAsync(subject);

                httpContext.User = JwtHelper.CreatePrincipal(token);

                // Sanitize username claims to remove domain
                httpContext.User = RemoveDomainFromUsernameClaim(httpContext.User as IClaimsPrincipal);

                return true;
            }
            catch (LoginException)
            {
                return false;
            }
            catch (PasswordExpiredException exp)
            {
                throw exp;
            }
            catch (Exception ex)
            {
                logger.Debug(ex, "ValidateProxySubject - Exception");
                return false;
            }
        }
    }
}