﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Licensing
{
    public static class ModuleConstants
    {
        public const string Base = "SS00";
        public const string Student = "SS01";
        public const string Planning = "SS02";
        public const string Finance = "SS03";
        public const string FinancialAid = "SS04";
        public const string ColleagueFinance = "CF";
        public const string ProjectsAccounting = "SS05";
        public const string TimeManagement = "WTEA";

        /// <summary>
        /// Dictionary for module codes that defines the relationship between the module code key and
        /// the list of area names licensed by that module.
        /// </summary>
        public static IDictionary<string, IEnumerable<string>> ModuleAreas = new Dictionary<string, IEnumerable<string>>()
        {
            { Base, new List<string>() { "HumanResources" } },
            { Student, new List<string>() { "Student" } },
            { Planning, new List<string>() { "Planning" } },
            { Finance, new List<string>() { "Finance" } },
            { FinancialAid, new List<string>() { "FinancialAid" } },
            { ColleagueFinance, new List<string>() { "ColleagueFinance" } },
            { ProjectsAccounting, new List<string>() { "ProjectsAccounting" } },
            { TimeManagement, new List<string>() { "TimeManagement" } }
        };
    }
}