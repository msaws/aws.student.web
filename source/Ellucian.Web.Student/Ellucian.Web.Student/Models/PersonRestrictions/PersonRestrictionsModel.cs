﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.PersonRestrictions
{
    /// <summary>
    /// Model for viewing person restrictions
    /// </summary>
    public class PersonRestrictionsModel
    {
        private List<AlertStyle> _alertStyles = new List<AlertStyle>() { AlertStyle.Critical, AlertStyle.Warning, AlertStyle.Information };
        private List<RestrictionDisplayModel> _restrictions;

        /// <summary>
        /// Displayed person restrictions
        /// </summary>
        public ReadOnlyCollection<RestrictionDisplayModel> Restrictions { get; private set; }

        /// <summary>
        /// Creates a new <see cref="PersonRestrictionModel"/> object.
        /// </summary>
        /// <param name="config">Restriction configuration information</param>
        /// <param name="restrictions">Collection of person restrictions</param>
        public PersonRestrictionsModel(RestrictionConfiguration config, IEnumerable<PersonRestriction> restrictions)
        {
            List<RestrictionDisplayModel> displayedRestrictions = new List<RestrictionDisplayModel>();
            List<PersonRestriction> uncategorizedRestrictions = new List<PersonRestriction>();

            // If no restrictions are present, use an empty list
            if (restrictions == null || !restrictions.Any())
            {
                displayedRestrictions = new List<RestrictionDisplayModel>();
            }
            // If restrictions are present, attempt to map styles to them
            else
            {
                // All restrictions are uncategorized to start
                uncategorizedRestrictions = restrictions.ToList();

                // If the restriction configuration and its mapping are present, use them to map styles to restrictions
                if (config != null && config.Mapping != null)
                {
                    foreach (var alertStyle in _alertStyles)
                    {
                        // Multiple severity ranges are permitted for a given style - get the subset of Severity Style Mappings that matches the current style in the loop.
                        var mappingSubset = config.Mapping.Where(m => m.Style == alertStyle).ToList();

                        // Retrieve the subset of matching Person Restrictions.
                        IEnumerable<PersonRestriction> restrictionSubset = uncategorizedRestrictions.Where(pr => mappingSubset.Any(ms => pr.Severity >= ms.SeverityStart && pr.Severity <= ms.SeverityEnd)).ToList();

                        // Remove the subset of Person Restrictions from the list of remaining Person Restrictions.
                        uncategorizedRestrictions = uncategorizedRestrictions.Except(restrictionSubset).ToList();

                        // Iterate through the matching Person Restrictions
                        foreach (var restriction in restrictionSubset)
                        {
                            displayedRestrictions.Add(new RestrictionDisplayModel(restriction, alertStyle));
                        }
                    }
                }

                // Iterate through uncategorized Person Restrictions with the "Information" styling
                foreach (var restriction in uncategorizedRestrictions)
                {
                    displayedRestrictions.Add(new RestrictionDisplayModel(restriction, AlertStyle.Information));
                }
            }

            // Set model properties
            _restrictions = displayedRestrictions;
            Restrictions = _restrictions.AsReadOnly();
        }
    }
}