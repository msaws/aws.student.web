﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.PersonRestrictions
{
    /// <summary>
    /// Display model for a person restriction
    /// </summary>
    public class RestrictionDisplayModel : PersonRestriction
    {
        private Dictionary<AlertStyle, string> _styleDictionary = new Dictionary<AlertStyle, string>() { 
            { AlertStyle.Critical,    "error" },
            { AlertStyle.Warning,     "warning" },
            { AlertStyle.Information, "info" } 
        };
        
        private string _style;
        /// <summary>
        /// Styling for the person restriction
        /// </summary>
        public string Style { get { return _style; } }

        /// <summary>
        /// Creates a new <see cref="RestrictionDisplayModel"/> object.
        /// </summary>
        /// <param name="restriction">The person restriction</param>
        /// <param name="style">Alert style for the person restriction</param>
        public RestrictionDisplayModel(PersonRestriction restriction, AlertStyle style)
        {
            if (restriction == null)
            {
                throw new ArgumentNullException("restriction");
            }
            this.Details = restriction.Details;
            this.EndDate = restriction.EndDate;
            this.Hyperlink = restriction.Hyperlink;
            this.HyperlinkText = string.IsNullOrEmpty(restriction.HyperlinkText) ? "None" : restriction.HyperlinkText;
            this.Id = restriction.Id;
            this.OfficeUseOnly = restriction.OfficeUseOnly;
            this.RestrictionId = restriction.RestrictionId;
            this.Severity = restriction.Severity;
            this.StartDate = restriction.StartDate;
            this.StudentId = restriction.StudentId;
            this.Title = restriction.Title;
            _style = _styleDictionary[style];
        }
    }
}