﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.PersonProxy
{
    /// <summary>
    /// A workflow or task for which proxy permission may be granted to another user
    /// </summary>
    public class ProxyAccessPermission
    {
        /// <summary>
        /// Unique identifier for the proxy access permission
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Description of the proxy access permission
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Unique identifier for the proxy access permission group
        /// </summary>
        public string GroupId { get; set; }

        /// <summary>
        /// Description of the proxy access permission group
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// Flag indicating whether or not the proxy access permission may be granted or not
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Flag indicating whether or not the proxy access permission is granted
        /// </summary>
        public bool IsAssigned { get; set; }

        /// <summary>
        /// Flag indicating whether or not the proxy access permission is granted when the model is first built
        /// </summary>
        public bool IsInitiallyAssigned { get; set; }

        /// <summary>
        /// Date on which the proxy access permission was granted
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Date by which reauthorization is required before access is revoked
        /// </summary>
        public DateTime? ReauthorizationDate { get; set; }

        /// <summary>
        /// Flag indicating whether or not reauthorization is currently needed
        /// </summary>
        public bool ReauthorizationIsNeeded { get { return ReauthorizationDate.HasValue && ReauthorizationDate.Value < DateTime.Today; } }

        /// <summary>
        /// Constructor for the proxy access permission
        /// </summary>
        /// <param name="id">Unique identifier for the proxy access permission</param>
        /// <param name="name">Description of the proxy access permission</param>
        /// <param name="effectiveDate">Date on which the proxy access permission was granted</param>
        /// <param name="isAssigned">Flag indicating whether or not the proxy access permission is granted</param>
        /// <param name="isEnabled">Flag indicating whether or not the proxy access permission may be granted or not</param>
        /// <param name="reauthDate">Date by which reauthorization is required before access is revoked</param>
        public static ProxyAccessPermission Build(string id, string name, DateTime? effectiveDate, bool isEnabled = false, bool isAssigned = false, DateTime? reauthDate = null)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            ProxyAccessPermission pap = new ProxyAccessPermission()
            {
                Id = id,
                Name = name,
                EffectiveDate = effectiveDate.HasValue ? effectiveDate.Value : DateTime.Today,
                ReauthorizationDate = reauthDate,
                IsEnabled = isEnabled,
                IsAssigned = isAssigned,
                IsInitiallyAssigned = isAssigned
            };

            return pap;
        }
    }
}