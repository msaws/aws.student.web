﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Web.Student.Utility;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Models.PersonProxy
{
    /// <summary>
    /// A user who may be granted, or has already received, proxy permissions for the principal
    /// </summary>
    public class ProxyUser
    {
        /// <summary>
        /// Unique identifier for the proxy user
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name of the proxy user
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The email address for the proxy user
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Email type of the user's email address
        /// </summary>
        public string EmailType { get; set; }

        /// <summary>
        /// Indicates if user includes demographics to prompt for email address. Set to true if user already has an email or
        /// if we are not prompting for email address due to configuration settings or because user is already a proxy and
        /// we will only prompt if they are a relation that is not yet a proxy.
        /// </summary>
        public bool HasEmailAddress { get; set; }

        /// <summary>
        /// The relationship between the proxy and the principal
        /// </summary>
        public string Relationship { get; set; }

        /// <summary>
        /// Date on which the user's proxy access was last updated
        /// </summary>
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// Flag indicating whether or not the user is a proxy candidate
        /// </summary>
        public bool IsCandidate { get; set; }

        /// <summary>
        /// List of all proxy access groups with proxy access permissions that may be granted to users
        /// </summary>
        public List<ProxyAccessGroup> PermissionGroups { get; set; }

        /// <summary>
        /// List of demographic information fields for a new proxy user
        /// </summary>
        public List<DataEntryField> DemographicInformation { get; set; }

        /// <summary>
        /// Date by which reauthorization is needed
        /// </summary>
        public DateTime? ReauthorizationDate 
        { 
            get 
            {
                if (PermissionGroups.Any())
                {
                    return PermissionGroups.SelectMany(pg => pg.Permissions).Select(p => p.ReauthorizationDate).Where(rd => rd.HasValue).Min();
                }
                return null;
            } 
        }

        /// <summary>
        /// Action Required Message
        /// </summary>
        public string ActionRequiredText
        {
            get
            {
                if (ReauthorizationDate.HasValue)
                {
                    if (ReauthorizationDate.Value < DateTime.Today) return GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ReauthorizationOverdueText");
                    else if (ReauthorizationDate.Value == DateTime.Today) return GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ReauthorizationNeededText");
                    else return string.Format(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ReauthorizationDueByText"),ReauthorizationDate.Value.ToShortDateString());
                }
                return null;
            }
        }
        
        /// <summary>
        /// Flag indicating whether or not reauthorization is currently needed
        /// </summary>
        public bool ReauthorizationIsNeeded { get { return PermissionGroups.Any() && PermissionGroups.SelectMany(pg => pg.Permissions).Any(p => p.ReauthorizationDate.HasValue); } }

        /// <summary>
        /// Flag indicating whether or not the user may be given proxy access
        /// </summary>
        public bool CanReceiveProxyAccess { get; set; }

        /// <summary>
        /// Constructor for the proxy user
        /// </summary>
        /// <param name="id">Unique identifier for the proxy user</param>
        /// <param name="name">Name of the proxy user</param>
        /// <param name="relationship">The relationship between the proxy and the principal</param>
        /// <param name="email">The email Address for the proxy user</param>
        /// <param name="photoUrl">URL for photo of the proxy user</param>
        /// <param name="effectiveDate">Date on which the user's proxy access was last updated</param>
        /// <param name="workflowGroups">Proxy workflow groups</param>
        /// <param name="existingPermissions">Existing proxy access permissions</param>
        /// <param name="config">Proxy configuration</param>
        /// <param name="isCurrentProxy">Indicates if this proxy user is currently a proxy</param>
        public static ProxyUser Build(string id, string name, string relationship, string email, DateTime? effectiveDate, IEnumerable<Ellucian.Colleague.Dtos.Base.ProxyWorkflowGroup> workflowGroups, IEnumerable<Ellucian.Colleague.Dtos.Base.ProxyAccessPermission> existingPermissions, ProxyConfiguration config = null, bool isCurrentProxy = false)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            if (workflowGroups == null || !workflowGroups.Any())
            {
                throw new ArgumentNullException("workflowGroups");
            }

            if (!isCurrentProxy && string.IsNullOrEmpty(email) && config == null)
            {
                throw new ArgumentNullException("configuration");
            }

            ProxyUser pu = new ProxyUser()
            {
                Id = id,
                Name = name,
                Relationship = relationship,
                Email = email,
                PermissionGroups = new List<ProxyAccessGroup>(),
                EffectiveDate = effectiveDate,
                DemographicInformation = new List<DataEntryField>(),
                CanReceiveProxyAccess = true
            };

            foreach (var group in workflowGroups)
            {
                var accessGroup = ProxyAccessGroup.Build(group.Code, group.Description);
                accessGroup.Permissions = new List<ProxyAccessPermission>();

                foreach(var workflow in group.Workflows)
                {
                    var existingPermission = existingPermissions != null ? existingPermissions.Where(ep => ep.ProxyUserId == id && ep.ProxyWorkflowCode == workflow.Code && ep.IsGranted).FirstOrDefault() : null;
                    ProxyAccessPermission permission = new ProxyAccessPermission();
                    if (existingPermission != null)
                    {
                        permission = ProxyAccessPermission.Build(workflow.Code, workflow.Description, existingPermission.StartDate, workflow.IsEnabled, 
                            existingPermission.IsGranted, existingPermission.ReauthorizationDate);
                    }
                    else
                    {
                        permission = ProxyAccessPermission.Build(workflow.Code, workflow.Description, null, workflow.IsEnabled, false, null);
                    }
                    permission.GroupId = accessGroup.Id;
                    permission.GroupName = accessGroup.Name;
                    accessGroup.Permissions.Add(permission);
                }
                if (accessGroup.Permissions.Where(p => p.IsAssigned || p.IsEnabled).Any())
                {
                    pu.PermissionGroups.Add(accessGroup);
                }
            }

            // Prevent the user from receiving proxy access when they are not a current proxy,
            // do not have an email address, and new users cannot be added
            if (!isCurrentProxy && string.IsNullOrEmpty(email) && !config.CanAddOtherUsers)
            {
                pu.CanReceiveProxyAccess = false;
                pu.Email = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ExistingUserMissingEmailAddressMessage");
            }

            // For an existing relation without email (but not if a current proxy), set up inputs for email (and email type if configured)
            if (!isCurrentProxy && string.IsNullOrEmpty(email) && config.CanAddOtherUsers)
            {
                // Find the email address field from proxy configuration
                var emailAddressField = config.DemographicFields.Where(f => f.Type == DemographicFieldType.EmailAddress && f.Requirement != DemographicFieldRequirement.Hidden).FirstOrDefault();
                pu.DemographicInformation.Add(DataEntryField.Build(emailAddressField)); 
                if (emailAddressField != null)
                {
                    // Add email address and confirm email address fields to Demographic Information
                    pu.DemographicInformation.Add(DataEntryField.Build(new DemographicField()
                    {
                        Code = "CONFIRM_EMAIL_ADDRESS",
                        Description = "Confirm Email",
                        Requirement = DemographicFieldRequirement.Required,
                        Type = DemographicFieldType.EmailAddress
                    }));

                    // Manually add extra field for confirming email address entry
                    pu.DemographicInformation.Last().Label = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyConfirmEmailAddressLabel");

                    // Add email type field if it is not hidden
                    var emailTypeField = config.DemographicFields.Where(f => f.Type == DemographicFieldType.EmailType && f.Requirement != DemographicFieldRequirement.Hidden).FirstOrDefault();
                    if (emailTypeField != null)
                    {
                        pu.DemographicInformation.Add(DataEntryField.Build(emailTypeField));
                    }
                }
            }
            else
            {
                pu.HasEmailAddress = true;
            }

            return pu;
        }
    
        /// <summary>
        /// Constructor for an empty proxy user
        /// </summary>
        /// <param name="config">Proxy configuration</param>
        public static ProxyUser BuildEmpty(ProxyConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", "Proxy configuration must be provided.");
            }

            ProxyUser pu = new ProxyUser()
            {
                Id = "NEWPERSON",
                Name = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "AddAnotherUserCaption"),
                PermissionGroups = new List<ProxyAccessGroup>(),
                DemographicInformation = new List<DataEntryField>(),
                CanReceiveProxyAccess = true
            };

            if (config.WorkflowGroups != null && config.WorkflowGroups.Any())
            {
                foreach (var group in config.WorkflowGroups)
                {
                    var accessGroup = ProxyAccessGroup.Build(group.Code, group.Description);
                    accessGroup.Permissions = new List<ProxyAccessPermission>();

                    foreach (var workflow in group.Workflows)
                    {
                        ProxyAccessPermission permission = ProxyAccessPermission.Build(workflow.Code, workflow.Description, null, workflow.IsEnabled, false, null);
                        permission.GroupId = accessGroup.Id;
                        permission.GroupName = accessGroup.Name;
                        accessGroup.Permissions.Add(permission);
                    }
                    if (accessGroup.Permissions.Where(p => p.IsEnabled).Any())
                    {
                        pu.PermissionGroups.Add(accessGroup);
                    }
                }
            }
            if (config.DemographicFields != null && config.DemographicFields.Any())
            {
                foreach(var field in config.DemographicFields)
                {
                    pu.DemographicInformation.Add(DataEntryField.Build(field));
                }

                if (config.DemographicFields.Any(f => f.Type == DemographicFieldType.EmailAddress && f.Requirement != DemographicFieldRequirement.Hidden))
                {
                    pu.DemographicInformation.Add(DataEntryField.Build(new DemographicField()
                    {
                        Code = "CONFIRM_EMAIL_ADDRESS",
                        Description = "Confirm Email",
                        Requirement = DemographicFieldRequirement.Required,
                        Type = DemographicFieldType.EmailAddress
                    }));

                    // Manually add extra field for confirming email address entry
                    pu.DemographicInformation.Last().Label = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyConfirmEmailAddressLabel");
                }

                pu.DemographicInformation = pu.DemographicInformation.OrderByDescending(di => di.Label).OrderBy(di => di.Type).ToList();

                // Manually add extra field for confirming government ID entry if such a field exists and is visible
                List<DataEntryField> governmentIdFields = pu.DemographicInformation.Where(f => f.Type == DemographicFieldType.GovernmentId && f.IsVisible).ToList();
                if (governmentIdFields.Any())
                {
                    foreach(var gif in governmentIdFields)
                    {
                        int gifIndex = pu.DemographicInformation.IndexOf(gif);
                        DataEntryField def = DataEntryField.Build(new DemographicField()
                        {
                            Code = "CONFIRM_SSN",
                            Description = "Confirm SSN",
                            Requirement = gif.IsRequired ? DemographicFieldRequirement.Required : gif.IsVisible ? DemographicFieldRequirement.Optional : DemographicFieldRequirement.Hidden,
                            Type = DemographicFieldType.GovernmentId
                        });
                        def.Label = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyConfirmGovernmentIdLabel");
                        pu.DemographicInformation.Insert(gifIndex + 1, def);
                    }
                }
            }

            return pu;
        }

        /// <summary>
        /// Constructor for a proxy candidate
        /// </summary>
        /// <param name="config">Proxy configuration</param>
        /// <param name="candidate">Proxy candidate</param>
        /// <param name="relationship">The relationship between the candidate and the principal</param>
        public static ProxyUser BuildCandidate(ProxyConfiguration config, ProxyCandidate candidate, string relationship)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", "Proxy configuration must be provided.");
            }
            if (candidate == null)
            {
                throw new ArgumentNullException("candidate", "Proxy candidate must be provided.");
            }
            ProxyUser user = ProxyUser.BuildEmpty(config);

            user.Id = "PROXYCANDIDATE";
            user.Email = candidate.EmailAddress;
            user.Name = candidate.FirstName + " " + candidate.LastName;
            user.Relationship = relationship;
            user.IsCandidate = true;
            foreach (var perm in candidate.GrantedPermissions)
            {
                var grantedPerm = user.PermissionGroups.SelectMany(pg => pg.Permissions).Where(p => p.Id == perm).FirstOrDefault();
                if (grantedPerm != null && grantedPerm.IsEnabled)
                {
                    grantedPerm.IsAssigned = true;
                }
            }
            return user;
        }
    }
}