﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.PersonProxy
{
    /// <summary>
    /// Model for selecting a proxy subject for whom a user may proxy
    /// </summary>
    public class ProxySelectionDialogModel
    {
        /// <summary>
        /// List of available proxy subjects for the user
        /// </summary>
        public List<Ellucian.Colleague.Dtos.Base.ProxySubject> ProxySubjects { get; set; }

        /// <summary>
        /// Constructor for a proxy selection dialog model
        /// </summary>
        /// <param name="proxySubjects">Enumeration of proxy subjects for whom a user may proxy</param>
        /// <param name="currentUser">Current user object</param>
        /// <returns>Proxy selection dialog model</returns>
        public static ProxySelectionDialogModel Build(IEnumerable<Ellucian.Colleague.Dtos.Base.ProxySubject> proxySubjects, ICurrentUser currentUser)
        {
            var model = new ProxySelectionDialogModel();
            model.ProxySubjects = new List<Ellucian.Colleague.Dtos.Base.ProxySubject>();
            if (proxySubjects != null && proxySubjects.Any())
            {
                foreach (var subject in proxySubjects)
                {
                    model.ProxySubjects.Add(subject);
                }
                if (currentUser != null)
                {
                    model.ProxySubjects.Insert(0, new Colleague.Dtos.Base.ProxySubject()
                        {
                            EffectiveDate = null,
                            FullName = currentUser.FormattedName,
                            Id = currentUser.PersonId,
                            Permissions = null
                        });
                }
            }
            return model;
        }
    }
}