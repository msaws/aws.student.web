﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Models.PersonProxy
{
    /// <summary>
    /// Model for viewing and maintaining person proxy access
    /// </summary>
    public class PersonProxyModel
    {
        /// <summary>
        /// Id of the person whose proxy information is being maintained
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Flag indicating whether or not person proxy functionality is enabled
        /// </summary>
        public bool PersonProxyIsEnabled { get; set; }

        /// <summary>
        /// List of all available and active proxy users
        /// </summary>
        public List<ProxyUser> ProxyUsers { get; set; }

        /// <summary>
        /// Flag indicating whether or not proxy privileges may be granted to any proxy workflows
        /// </summary>
        public bool AnyPermissionsEnabled { get; set; }

        /// <summary>
        /// Disclosure Agreement Text to which the user must consent before proxy access may be granted
        /// </summary>
        public string DisclosureAgreementText { get; set; }

        /// <summary>
        /// Text conveying information about the Add a Proxy workflow within the View/Add Third Party Access process
        /// </summary>
        public string AddProxyHeaderText { get; set; }

        /// <summary>
        /// Text conveying information about the View/Add Third Party Access process
        /// </summary>
        public string ProxyFormHeaderText { get; set; }

        /// <summary>
        /// Text conveying information about the user's need to reauthorize their granted access
        /// </summary>
        public string ReauthorizationText { get; set; }

        /// <summary>
        /// Flag indicating whether or not reauthorization is currently needed
        /// </summary>
        public bool ReauthorizationIsNeeded { get { return ProxyUsers.Any(p => p.ReauthorizationIsNeeded); } }

        /// <summary>
        /// List of relationship types for which proxy permissions may be assigned
        /// </summary>
        public List<RelationshipType> RelationshipTypes { get; set; }

        /// <summary>
        /// Constructor for a person proxy model
        /// </summary>
        /// <param name="id">Unique identifier for the proxy access group</param>
        /// <param name="config">Proxy configuration</param>
        /// <param name="allRelationships">All relationships for a user</param>
        /// <param name="allProfiles">All profiles for persons/organizations related to a user</param>
        /// <param name="allProxyUsers">All existing proxy users with proxy access permissions for the user</param>
        /// <param name="allRelationshipTypes">All relationship types</param>
        /// <param name="allPrefixes">All prefixes</param>
        /// <param name="allSuffixes">All suffixes</param>
        /// <param name="allEmailTypes">All email types</param>
        /// <param name="allPhoneTypes">All phone Types</param>
        /// <param name="proxyCandidates">Proxy candidates for the user</param>
        /// <param name="profileConfig">User profile configuration</param>
        public static PersonProxyModel Build(string id,
            ProxyConfiguration config,
            IEnumerable<Relationship> allRelationships,
            IEnumerable<Profile> allProfiles,
            IEnumerable<Ellucian.Colleague.Dtos.Base.ProxyUser> allProxyUsers,
            IEnumerable<RelationshipType> allRelationshipTypes,
            IEnumerable<Prefix> allPrefixes,
            IEnumerable<Suffix> allSuffixes,
            IEnumerable<EmailType> allEmailTypes,
            IEnumerable<PhoneType> allPhoneTypes,
            IEnumerable<ProxyCandidate> proxyCandidates,
            UserProfileConfiguration profileConfig)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (allRelationships == null)
            {
                throw new ArgumentNullException("allRelationships");
            }

            if (allProfiles == null)
            {
                throw new ArgumentNullException("allProfiles");
            }

            if (allRelationshipTypes == null)
            {
                throw new ArgumentNullException("allRelationshipTypes");
            }

            if (allPrefixes == null)
            {
                throw new ArgumentNullException("allPrefixes");
            }

            if (allSuffixes == null)
            {
                throw new ArgumentNullException("allSuffixes");
            }

            if (allEmailTypes == null)
            {
                throw new ArgumentNullException("allEmailTypes");
            }

            if (allPhoneTypes == null)
            {
                throw new ArgumentNullException("allPhoneTypes");
            }

            if (proxyCandidates == null)
            {
                throw new ArgumentNullException("proxyCandidates");
            }

            if (profileConfig == null)
            {
                throw new ArgumentNullException("profileConfig");
            }

            if (profileConfig.UpdatableEmailTypes == null)
            {
                throw new ArgumentNullException("profileConfig.UpdatableEmailTypes");
            }

            if (profileConfig.UpdatablePhoneTypes == null)
            {
                throw new ArgumentNullException("profileConfig.UpdatablePhoneTypes");
            }

            List<Models.PersonProxy.ProxyUser> proxyUsers = new List<Models.PersonProxy.ProxyUser>();
            List<Relationship> allRelationshipsList = allRelationships.ToList();

            try
            {
                // Build proxy user shells for current proxy users
                if (allProxyUsers != null && allProxyUsers.Any())
                {
                    foreach (var user in allProxyUsers)
                    {
                        if (user.Permissions.Where(p => p.IsGranted).Any())
                        {
                            var matchingProfile = allProfiles.Where(p => p.Id == user.Id).FirstOrDefault();
                            var matchingRelationship = allRelationshipsList.Where(r => r.OtherEntity == user.Id).FirstOrDefault();
                            var relationshipType = new RelationshipType();
                            var relationshipDesc = string.Empty;
                            if (matchingRelationship != null)
                            {
                                relationshipType = allRelationshipTypes.Where(rt => rt.Code == matchingRelationship.RelationshipType).FirstOrDefault();
                                relationshipDesc = relationshipType != null ? relationshipType.Description : string.Empty;
                            }

                            if (matchingProfile != null)
                            {
                                bool isCurrentProxy = true;
                                var proxyUser = ProxyUser.Build(user.Id, matchingProfile.PreferredName, relationshipDesc, matchingProfile.PreferredEmailAddress, user.EffectiveDate, config.WorkflowGroups, user.Permissions, config, isCurrentProxy);

                                // Populate email types if field is present
                                var emailTypeFields = proxyUser.DemographicInformation.Where(di => di.Type == DemographicFieldType.EmailType).ToList();
                                foreach (var emailTypeField in emailTypeFields)
                                {
                                    emailTypeField.BuildEmailTypeOptions(allEmailTypes, profileConfig);
                                }
                                proxyUsers.Add(proxyUser);
                                if (matchingRelationship != null)
                                {
                                    allRelationshipsList.RemoveAll(r => r.OtherEntity == user.Id);
                                }
                            }
                        }
                    }
                }

                // Create proxy user shells for proxy candidates
                if (proxyCandidates.Any())
                {
                    var candidateList = proxyCandidates.ToList();
                    for (int i = 0; i < candidateList.Count; i++)
                    {
                        var relationship = allRelationshipTypes.Where(rt => rt.Code == candidateList[i].RelationType).FirstOrDefault();
                        var relDesc = relationship != null ? relationship.Description : string.Empty;
                        var user = ProxyUser.BuildCandidate(config, candidateList[i], relDesc);
                        user.Id += i.ToString();
                        proxyUsers.Add(user);
                    }
                }

                // If relationship type filtering is used, remove users with unauthorized relationship types
                if (config.RelationshipTypeCodes != null && config.RelationshipTypeCodes.Any())
                {
                    allRelationshipsList.RemoveAll(r => !config.RelationshipTypeCodes.Contains(r.RelationshipType));
                }

                // Create proxy user shells for potential proxy users
                if (allRelationshipsList.Any())
                {
                    foreach (var relationship in allRelationshipsList)
                    {
                        var matchingProfile = allProfiles.Where(p => p.Id == relationship.OtherEntity).FirstOrDefault();
                        var relationshipType = allRelationshipTypes.Where(rt => rt.Code == relationship.RelationshipType).FirstOrDefault();
                        var relationshipDesc = relationshipType != null ? relationshipType.Description : string.Empty;

                        if (matchingProfile != null && !string.IsNullOrEmpty(relationshipDesc))
                        {
                            bool isCurrentProxy = false;
                            var proxyUser = ProxyUser.Build(relationship.OtherEntity, matchingProfile.PreferredName, relationshipDesc, matchingProfile.PreferredEmailAddress, null, config.WorkflowGroups, null, config, isCurrentProxy);
                            var emailTypeFields = proxyUser.DemographicInformation.Where(di => di.Type == DemographicFieldType.EmailType).ToList();
                            foreach (var emailTypeField in emailTypeFields)
                            {
                                emailTypeField.BuildEmailTypeOptions(allEmailTypes, profileConfig);
                            } 
                            proxyUsers.Add(proxyUser);
                        }
                    }
                }

                // Create "dummy" proxy user for allowing addition of non-related users
                if (config.CanAddOtherUsers)
                {
                    var emptyUser = ProxyUser.BuildEmpty(config);

                    var prefixFields = emptyUser.DemographicInformation.Where(di => di.Type == DemographicFieldType.Prefix).ToList();
                    prefixFields.ForEach(pfx => pfx.Options.AddRange(allPrefixes.ToList().ConvertAll(prefix => DataEntryOption.Build(prefix))));

                    var suffixFields = emptyUser.DemographicInformation.Where(di => di.Type == DemographicFieldType.Suffix).ToList();
                    suffixFields.ForEach(sfx => sfx.Options.AddRange(allSuffixes.ToList().ConvertAll(suffix => DataEntryOption.Build(suffix))));

                    var emailTypeFields = emptyUser.DemographicInformation.Where(di => di.Type == DemographicFieldType.EmailType).ToList();
                    foreach (var emailTypeField in emailTypeFields)
                    {
                        emailTypeField.BuildEmailTypeOptions(allEmailTypes, profileConfig);
                    }

                    var phoneTypeFields = emptyUser.DemographicInformation.Where(di => di.Type == DemographicFieldType.PhoneType).ToList();
                    if (profileConfig.UpdatablePhoneTypes.Any())
                    {
                        allPhoneTypes = allPhoneTypes.Where(pt => profileConfig.UpdatablePhoneTypes.Contains(pt.Code));
                    }
                    if (allPhoneTypes.Count() == profileConfig.UpdatablePhoneTypes.Count())
                    {
                        phoneTypeFields.ForEach(pt => pt.Options.AddRange(allPhoneTypes.ToList().ConvertAll(type => DataEntryOption.Build(type))));
                    }
                    else
                    {
                        foreach (var field in phoneTypeFields)
                        {
                            field.IsVisible = false;
                            field.IsRequired = false;
                        }
                    }


                    var genderFields = emptyUser.DemographicInformation.Where(di => di.Type == DemographicFieldType.Gender).ToList();
                    genderFields.ForEach(g => g.Options.AddRange(new GenderCollection()));

                    proxyUsers.Add(emptyUser);
                }

                // Build the person proxy model
                PersonProxyModel ppm = new PersonProxyModel();
                ppm.AnyPermissionsEnabled = config.WorkflowGroups.SelectMany(wg => wg.Workflows).Any(w => w.IsEnabled);
                ppm.ProxyUsers = proxyUsers;
                ppm.Id = id;
                ppm.PersonProxyIsEnabled = config.ProxyIsEnabled;
                ppm.DisclosureAgreementText = string.Join(Environment.NewLine, config.DisclosureReleaseText).Replace("" + Environment.NewLine, "<br />");
                ppm.AddProxyHeaderText = string.Join(Environment.NewLine, config.AddProxyHeaderText).Replace("" + Environment.NewLine, "<br />");
                ppm.ProxyFormHeaderText = string.Join(Environment.NewLine, config.ProxyFormHeaderText).Replace("" + Environment.NewLine, "<br />");
                ppm.ReauthorizationText = string.Join(Environment.NewLine, config.ReauthorizationText).Replace("" + Environment.NewLine, "<br />");
                ppm.RelationshipTypes = allRelationshipTypes.ToList();
                if (config.RelationshipTypeCodes != null && config.RelationshipTypeCodes.Any())
                {
                    ppm.RelationshipTypes.RemoveAll(rt => !config.RelationshipTypeCodes.Contains(rt.Code));
                }

                return ppm;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to build person proxy information.", ex.InnerException);
            }
        }
    }
}