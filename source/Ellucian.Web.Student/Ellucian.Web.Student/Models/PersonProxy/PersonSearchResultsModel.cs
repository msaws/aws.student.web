﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.PersonProxy
{
    /// <summary>
    /// Model for the results of a search for matching persons
    /// </summary>
    public class PersonSearchResultsModel
    {
        /// <summary>
        /// Criteria used in searching for matching persons
        /// </summary>
        public ProxyUser SearchCriteria { get; set; }

        /// <summary>
        /// Collection of search results
        /// </summary>
        public IEnumerable<Ellucian.Colleague.Dtos.Base.PersonMatchResult> Results { get; set; }

        /// <summary>
        /// Message to display based on the results
        /// </summary>
        public string Message 
        { 
            get 
            {
                string msg = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "NoMatchingPersonsMessage");
                if (Results.Any())
                {
                    if (Results.Count() == 1 && Results.First().MatchCategory == Ellucian.Colleague.Dtos.Base.PersonMatchCategoryType.Definite)
                    {
                        msg = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "OneMatchingPersonMessage");
                    }
                    else
                    {
                        msg = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "MultipleMatchingPersonsMessage");
                    }
                }
                return msg; 
            } 
        }

        /// <summary>
        /// Constructor for a PersonSearchResultsModel
        /// </summary>
        /// <param name="criteria">Proxy User object containing the search criteria</param>
        /// <param name="results">Collection of search results</param>
        /// <returns>A PersonSearchResultsModel</returns>
        public static PersonSearchResultsModel Build(ProxyUser criteria, IEnumerable<Ellucian.Colleague.Dtos.Base.PersonMatchResult> results)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }
            if (results == null)
            {
                throw new ArgumentNullException("results");
            }

            PersonSearchResultsModel model = new PersonSearchResultsModel()
            {
                SearchCriteria = criteria,
                Results = results,
            };

            return model;
        }
    }
}