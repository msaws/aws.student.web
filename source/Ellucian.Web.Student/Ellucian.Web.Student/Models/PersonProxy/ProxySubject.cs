﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Models.PersonProxy
{
    /// <summary>
    /// A user who has granted proxy permissions to the current user
    /// </summary>
    public class ProxySubject
    {
        /// <summary>
        /// Unique identifier for the proxy subject
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name of the proxy subject
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// URL for photo of the proxy subject
        /// </summary>
        public string PhotoUrl { get; set; }

        /// <summary>
        /// Indicates whether this proxy subject is the one selected by the proxy user
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// Constructor for the proxy user
        /// </summary>
        /// <param name="id">Unique identifier for the proxy user</param>
        /// <param name="name">Name of the proxy user</param>
        /// <param name="photoUrl">URL for photo of the proxy user</param>
        public static ProxySubject Build(string id, string name, string photoUrl)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            if (string.IsNullOrEmpty(photoUrl))
            {
                photoUrl = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "MissingPhotoImageUrl");
            }

            var subject = new ProxySubject()
            {
                Id = id,
                Name = name,
                PhotoUrl = photoUrl,
                IsSelected = false
              };

            return subject;
        }
    }
}