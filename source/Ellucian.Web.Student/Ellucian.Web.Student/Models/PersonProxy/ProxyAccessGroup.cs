﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.PersonProxy
{
    /// <summary>
    /// A grouping of related proxy access permissions
    /// </summary>
    public class ProxyAccessGroup
    {
        /// <summary>
        /// Unique identifier for the proxy access group
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Description of the proxy access group
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Collection of related proxy access permissions for the group
        /// </summary>
        public List<ProxyAccessPermission> Permissions { get; set; }

        /// <summary>
        /// Constructor for a proxy access group
        /// </summary>
        /// <param name="id">Unique identifier for the proxy access group</param>
        /// <param name="name">Description of the proxy access group</param>
        public static ProxyAccessGroup Build(string id, string name)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            ProxyAccessGroup pag = new ProxyAccessGroup()
            {
                Id = id,
                Name = name,
                Permissions = new List<ProxyAccessPermission>()
            };

            return pag;
        }
    }
}