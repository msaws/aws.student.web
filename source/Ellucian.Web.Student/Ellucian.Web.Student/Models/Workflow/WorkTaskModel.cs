﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Models.Workflow
{
    /// <summary>
    /// A Work Task Model represents a single task that a user needs to complete.
    /// It also can include a link to the page where the user can complete this task.
    /// </summary>
    public class WorkTaskModel
    {
        /// <summary>
        /// The Id of the Work Task
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Category is a descriptive name for the type of task
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Description contains details about the specific task
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ProcessLink is a URL to the relevant Self-Service Page to complete this task
        /// </summary>
        public string ProcessLink { get; set; }

        /// <summary>
        /// Constructor for the Work Task Model. 
        /// </summary>
        /// <param name="id">Unique Id for the Work Task</param>
        /// <param name="category">Category name</param>
        /// <param name="description">Work Task details</param>
        /// <param name="processCode">Used to generate a link to the relevant page in Self-Service</param>
        public WorkTaskModel(string id, string category, string description, Colleague.Dtos.Base.WorkTaskProcess processCode)
        {
            Id = id;
            Category = category;
            Description = description;
            ProcessLink = MapWorkTaskProcessToLink(processCode);
        }

        /// <summary>
        /// Converts the process into a URL suitable for including on a page
        /// </summary>
        /// <param name="process">Process type</param>
        /// <returns>Url where the process can be completed</returns>
        private string MapWorkTaskProcessToLink(Colleague.Dtos.Base.WorkTaskProcess process)
        {
            var processLink = string.Empty;
            if (HttpContext.Current != null)
            {
                var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                switch (process)
                {
                    case Colleague.Dtos.Base.WorkTaskProcess.TimeApproval:
                        return urlHelper.Action("Index", "TimeApproval", new { area = "TimeManagement" });
                    case Colleague.Dtos.Base.WorkTaskProcess.LeaveRequestApproval:
                    case Colleague.Dtos.Base.WorkTaskProcess.None:
                    default:
                        break;
                }
            }
            return processLink;

        }
    }
}