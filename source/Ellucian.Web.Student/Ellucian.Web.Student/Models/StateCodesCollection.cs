﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Web.Mvc;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Models
{
    public class StateCodesCollection : List<SelectListItem>
    {
        public StateCodesCollection()
        {
            Add(new SelectListItem() { Value = "DEFAULT", Text = GlobalResources.GetString(GlobalResourceFiles.SiteResources, "StateProvincePrompt") });
            Add(new SelectListItem() { Value = "AL", Text = "Alabama" });
            Add(new SelectListItem() { Value = "AK", Text = "Alaska" });
            Add(new SelectListItem() { Value = "AB", Text = "Alberta" });
            Add(new SelectListItem() { Value = "AZ", Text = "Arizona" });
            Add(new SelectListItem() { Value = "AR", Text = "Arkansas" });
            Add(new SelectListItem() { Value = "BC", Text = "British Colombia" });
            Add(new SelectListItem() { Value = "CA", Text = "California" });
            Add(new SelectListItem() { Value = "CO", Text = "Colorado" });
            Add(new SelectListItem() { Value = "CT", Text = "Connecticut" });
            Add(new SelectListItem() { Value = "DE", Text = "Delaware" });
            Add(new SelectListItem() { Value = "DC", Text = "District of Columbia" });
            Add(new SelectListItem() { Value = "FL", Text = "Florida" });
            Add(new SelectListItem() { Value = "GA", Text = "Georgia" });
            Add(new SelectListItem() { Value = "HI", Text = "Hawaii" });
            Add(new SelectListItem() { Value = "ID", Text = "Idaho" });
            Add(new SelectListItem() { Value = "IL", Text = "Illinois" });
            Add(new SelectListItem() { Value = "IN", Text = "Indiana" });
            Add(new SelectListItem() { Value = "IA", Text = "Iowa" });
            Add(new SelectListItem() { Value = "KS", Text = "Kansas" });
            Add(new SelectListItem() { Value = "KY", Text = "Kentucky" });
            Add(new SelectListItem() { Value = "LA", Text = "Louisiana" });
            Add(new SelectListItem() { Value = "ME", Text = "Maine" });
            Add(new SelectListItem() { Value = "MB", Text = "Manitoba" });
            Add(new SelectListItem() { Value = "MD", Text = "Maryland" });
            Add(new SelectListItem() { Value = "MA", Text = "Massachusetts" });
            Add(new SelectListItem() { Value = "MI", Text = "Michigan" });
            Add(new SelectListItem() { Value = "MN", Text = "Minnesota" });
            Add(new SelectListItem() { Value = "MS", Text = "Mississippi" });
            Add(new SelectListItem() { Value = "MO", Text = "Missouri" });
            Add(new SelectListItem() { Value = "MT", Text = "Montana" });
            Add(new SelectListItem() { Value = "NE", Text = "Nebraska" });
            Add(new SelectListItem() { Value = "NV", Text = "Nevada" });
            Add(new SelectListItem() { Value = "NB", Text = "New Brunswick" });
            Add(new SelectListItem() { Value = "NH", Text = "New Hampshire" });
            Add(new SelectListItem() { Value = "NJ", Text = "New Jersey" });
            Add(new SelectListItem() { Value = "NM", Text = "New Mexico" });
            Add(new SelectListItem() { Value = "NY", Text = "New York" });
            Add(new SelectListItem() { Value = "NL", Text = "Newfoundland and Labrador" });
            Add(new SelectListItem() { Value = "NC", Text = "North Carolina" });
            Add(new SelectListItem() { Value = "ND", Text = "North Dakota" });
            Add(new SelectListItem() { Value = "NT", Text = "Northwest Territories" });
            Add(new SelectListItem() { Value = "NS", Text = "Nova Scotia" });
            Add(new SelectListItem() { Value = "NU", Text = "Nunavut" });
            Add(new SelectListItem() { Value = "OH", Text = "Ohio" });
            Add(new SelectListItem() { Value = "OK", Text = "Oklahoma" });
            Add(new SelectListItem() { Value = "ON", Text = "Ontario" });
            Add(new SelectListItem() { Value = "OR", Text = "Oregon" });
            Add(new SelectListItem() { Value = "PA", Text = "Pennsylvania" });
            Add(new SelectListItem() { Value = "PE", Text = "Prince Edward Island" });
            Add(new SelectListItem() { Value = "QC", Text = "Quebec" });
            Add(new SelectListItem() { Value = "RI", Text = "Rhode Island" });
            Add(new SelectListItem() { Value = "SK", Text = "Saskatchewan" });
            Add(new SelectListItem() { Value = "SC", Text = "South Carolina" });
            Add(new SelectListItem() { Value = "SD", Text = "South Dakota" });
            Add(new SelectListItem() { Value = "TN", Text = "Tennessee" });
            Add(new SelectListItem() { Value = "TX", Text = "Texas" });
            Add(new SelectListItem() { Value = "UT", Text = "Utah" });
            Add(new SelectListItem() { Value = "VT", Text = "Vermont" });
            Add(new SelectListItem() { Value = "VA", Text = "Virginia" });
            Add(new SelectListItem() { Value = "WA", Text = "Washington" });
            Add(new SelectListItem() { Value = "WV", Text = "West Virginia" });
            Add(new SelectListItem() { Value = "WI", Text = "Wisconsin" });
            Add(new SelectListItem() { Value = "WY", Text = "Wyoming" });
            Add(new SelectListItem() { Value = "YT", Text = "Yukon" });
        }
    }
}