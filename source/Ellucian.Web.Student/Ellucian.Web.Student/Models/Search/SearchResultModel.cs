﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
namespace Ellucian.Web.Student.Models.Search
{
    /// <summary>
    /// Model for a search result
    /// </summary>
    public class SearchResultModel
    {
        private string _id;

        /// <summary>
        /// ID of the search result object
        /// </summary>
        public string Id { get { return _id; } }

        /// <summary>
        /// Creates a new instance of the <see cref="SearchResultModel"/> class
        /// </summary>
        /// <param name="id">Unique identifier for a search result</param>
        public SearchResultModel(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "Each search result must have a unique identifier.");
            }

            _id = id;
        }
    }
}