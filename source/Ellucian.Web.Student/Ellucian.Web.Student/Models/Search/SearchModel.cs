﻿using System;
// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;

namespace Ellucian.Web.Student.Models.Search
{
    /// <summary>
    /// Model for looking up objects
    /// </summary>
    public abstract class SearchModel
    {
        /// <summary>
        /// Prompt to the user on the search context (e.g. "Who would you like to work with?")
        /// </summary>
        public string SearchPrompt { get; set; }

        /// <summary>
        /// Placeholder/watermark text for the search textbox
        /// </summary>
        public string PlaceholderText { get; set; }

        /// <summary>
        /// Text displayed to the user prior to a search
        /// </summary>
        public string BeforeSearchText { get; set; }

        /// <summary>
        /// Flag indicating whether or not an error occurred during the search
        /// </summary>
        public bool ErrorOccurred { get; set; }

        /// <summary>
        /// Error message displayed to the user when an error occurs during a search
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Label for the search textbox
        /// </summary>
        public string SearchFieldLabel { get; set; }

        /// <summary>
        /// Label for the submit button 
        /// </summary>
        public string SearchSubmitLabel { get; set; }

        /// <summary>
        /// Action to execute the search
        /// </summary>
        public SearchActionModel Search { get; set; }

        /// <summary>
        /// Action to execute the search
        /// </summary>
        public SearchActionModel Select { get; set; }

        /// <summary>
        ///  Name of the point-of-origin ASP.NET MVC controller 
        /// </summary>
        public string PointOfOriginControllerName { get; set; }

        /// <summary>
        /// Default action for dynamic back link
        /// </summary>
        public SearchActionModel DynamicBackLinkDefault { get; set; }

        /// <summary>
        /// Actions excluded from dynamic back link processing
        /// </summary>
        public IEnumerable<SearchActionModel> BackLinkExclusions { get; set; }

        /// <summary>
        /// The user's submitted search query
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Name of the ASP.NET Razor MVC partial view used to display formatted search results
        /// </summary>
        public string SearchResultsViewName { get; set; }

        /// <summary>
        /// Collection of objects matching the user's submitted query
        /// </summary>
        public IEnumerable<SearchResultModel> SearchResults { get; set; }

        /// <summary>
        /// Alternate text for the spinner image displayed while a search executes
        /// </summary>
        public string SpinnerAlternateText { get; set; }

        /// <summary>
        /// Text displayed beneath the spinner while a search executes
        /// </summary>
        public string SpinnerText { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="SearchModel"/> class.
        /// </summary>
        /// <param name="searchPrompt">Prompt to the user of what they will search for</param>
        /// <param name="beforeSearchText">Text displayed before a search has been run</param>
        /// <param name="searchResultsViewName">Name of an ASP.NET MVC Razor Partial view for rendering search results</param>
        /// <param name="dynamicBackLinkDefault">A <see cref="SearchActionModel"/> for a fallback dynamic back link location</param>
        /// <param name="backLinkExclusions">A collection of <see cref="SearchActionModel"/> for forbidden back link locations</param>
        public SearchModel(string searchPrompt, string beforeSearchText, string searchResultsViewName, SearchActionModel dynamicBackLinkDefault = null, IEnumerable<SearchActionModel> backLinkExclusions = null)
        {
            if (string.IsNullOrEmpty(searchPrompt))
            {
                throw new ArgumentNullException("searchPrompt", "A search prompt must be specified.");
            }
            if (string.IsNullOrEmpty(beforeSearchText))
            {
                throw new ArgumentNullException("beforeSearchText", "A pre-search text must be specified.");
            }
            if (string.IsNullOrEmpty(searchResultsViewName))
            {
                throw new ArgumentNullException("searchResultsViewName", "A search results partial view must be specified for displaying search results.");
            }
            SearchPrompt = searchPrompt;
            BeforeSearchText = beforeSearchText;
            SearchResultsViewName = searchResultsViewName;
            DynamicBackLinkDefault = dynamicBackLinkDefault;
            BackLinkExclusions = backLinkExclusions;
        }
    }
}