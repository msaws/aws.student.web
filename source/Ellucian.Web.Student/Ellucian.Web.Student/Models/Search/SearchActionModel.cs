﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Ellucian.Web.Student.Models.Search
{
    /// <summary>
    /// Model for an ASP.NET MVC controller action called in the object lookup engine
    /// </summary>
    public class SearchActionModel
    {
        private string _action;
        private string _controller;
        private string _area;

        /// <summary>
        /// Name of the ASP.NET MVC controller action
        /// </summary>
        public string Action { get { return _action; } }

        /// <summary>
        ///  Name of the ASP.NET MVC controller
        /// </summary>
        public string Controller { get { return _controller; } }

        /// <summary>
        /// Name of the area in which the ASP.NET MVC controller resides
        /// </summary>
        public string Area { get { return _area; } }

        /// <summary>
        /// Optional collection of route values
        /// </summary>
        public RouteValueDictionary RouteValues { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="SearchActionModel"/> object.
        /// </summary>
        /// <param name="action">ASP.NET MVC controller action</param>
        /// <param name="controller">ASP.NET MVC controller</param>
        /// <param name="area">Area in which the ASP.NET MVC controller resides</param>
        public SearchActionModel(string action, string controller, string area = null)
        {
            if (string.IsNullOrEmpty(action))
            {
                throw new ArgumentNullException("action", "An ASP.NET MVC controller action must be specified.");
            }

            if (string.IsNullOrEmpty(controller))
            {
                throw new ArgumentNullException("controller", "An ASP.NET MVC controller must be specified.");
            }

            _action = action;
            _controller = controller;
            _area = area;
            RouteValues = new RouteValueDictionary();
        }
    }
}