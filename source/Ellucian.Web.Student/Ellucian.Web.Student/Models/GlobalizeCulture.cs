﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Ellucian.Web.Student.Models
{
    /// <summary>
    /// Model to initialize the Globalize culture settings
    /// </summary>
    public class GlobalizeCulture
    {
        /// <summary>
        /// Culture name as known by the system, format languagecode-country
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Culture name in English
        /// </summary>
        public string EnglishName { get; set; }

        /// <summary>
        /// Culture name, language, country/region, and optional script the culture will display
        /// </summary>
        public string NativeName { get; set; }

        /// <summary>
        /// The ISO 639-1 two-letter code for the language of the current CultureInfo
        /// </summary>
        public string TwoLetterISOLanguageName { get; set; }

        /// <summary>
        /// The TextInfo that defines the writing system associated with the culture
        /// </summary>
        public TextInfo TextInfo { get; set; }

        /// <summary>
        /// The NumberFormatInfo that defines the culturally appropriate format of displaying numbers, currency, and percentage
        /// </summary>
        public NumberFormatInfo NumberFormat { get; set; }

        /// <summary>
        /// The DateTimeFormatInfo that defines the culturally appropriate format of displaying dates and times
        /// </summary>
        public DateTimeFormatInfo DateTimeFormat { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalizeCulture()
        {
        }

        /// <summary>
        /// Static method that builds the GlobalizeCulture object from the current culture information.
        /// </summary>
        /// <returns>The culture information to initialize Globalize</returns>
        public static GlobalizeCulture Build()
        {
            var globalizeCulture = new GlobalizeCulture();
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;

            globalizeCulture.Name = culture.Name;
            globalizeCulture.EnglishName = culture.EnglishName;
            globalizeCulture.NativeName = culture.NativeName;
            globalizeCulture.TwoLetterISOLanguageName = culture.TwoLetterISOLanguageName;

            globalizeCulture.TextInfo = culture.TextInfo;
            globalizeCulture.NumberFormat = culture.NumberFormat;
            globalizeCulture.DateTimeFormat = culture.DateTimeFormat;

            return globalizeCulture;
        }

        /// <summary>
        /// Static method that serializes the GlobalizeCulture object
        /// </summary>
        /// <returns></returns>
        public static string Serialize()
        {
            var serializedObject = JsonConvert.SerializeObject(Build());
            return serializedObject;
        }
    }
}