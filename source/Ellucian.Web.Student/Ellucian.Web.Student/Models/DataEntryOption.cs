﻿using Ellucian.Web.Student.Utility;
// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models
{
    /// <summary>
    /// Option of a data entry field whose possible values are static
    /// </summary>
    public class DataEntryOption
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Label/text
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Constructor for a data entry option
        /// </summary>
        /// <param name="input">Object from which to build the data entry option</param>
        /// <returns>Data entry option object</returns>
        public static DataEntryOption Build(Object input)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            DataEntryOption option = new DataEntryOption();
            ReflectionUtilities.MapProperty(input, option, "Code");
            ReflectionUtilities.MapProperty(input, option, "Description");
            return option;
        }
    }
}