﻿using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Models.Courses
{
    public class CourseSearchResultSection
    {
        /// <summary>
        /// Gets or sets the list of meeting time objects for this section.
        /// </summary>
        public List<CourseSearchResultMeetingTime> FormattedMeetingTimes { get; set; }

        /// <summary>
        /// Gets or sets whether this item is a non-term offering.
        /// </summary>
        public bool IsNonTermOffering { get; set; }

        /// <summary>
        /// Gets or sets whether this section has start and/or end dates that do not match the
        /// start and/or end dates of the containing term. If this section is a non-term offering
        /// then this flag is always true by default, but in some cases there are sections which
        /// are associated with a term that still have distinct date ranges.
        /// </summary>
        public bool IsNonStandardDates { get; set; }

        /// <summary>
        /// Gets or sets the Start Date display value. This is needed for non-term offerings, in which the
        /// dates are important.
        /// </summary>
        public string StartDateDisplay { get; set; }

        /// <summary>
        /// Gets or sets the End Date display value. This is needed for non-term offerings, in which the
        /// dates are important.
        /// </summary>
        public string EndDateDisplay { get; set; }

        /// <summary>
        /// Gets or sets the Location value to be displayed for this section.
        /// </summary>
        public string LocationDisplay { get; set; }

        /// <summary>
        /// Gets whether this section has unlimited capacity or if it has a defined seat count.
        /// </summary>
        public bool HasUnlimitedSeats
        {
            get { return this.Capacity == null; }
        }

        #region Inherited from Section DTO
        /// <summary>
        /// Gets or sets whether this section allows Audit grading.
        /// </summary>
        public bool AllowAudit { get; set; }
        /// <summary>
        /// Gets or sets whether this section allows Pass/Fail grading.
        /// </summary>
        public bool AllowPassNoPass { get; set; }
        /// <summary>
        /// Gets or sets the number of available seats in this section.
        /// </summary>
        public int? Available { get; set; }
        /// <summary>
        /// Gets or sets the Books for this section.
        /// </summary>
        public IEnumerable<SectionBook> Books { get; set; }
        /// <summary>
        /// Gets or sets the total capacity of seats for this section, if there is a limit.
        /// </summary>
        public int? Capacity { get; set; }
        /// <summary>
        /// Gets or sets the CEUs assigned to this section, if there is an amount.
        /// </summary>
        public decimal? Ceus { get; set; }
        /// <summary>
        /// Gets or sets the ID of the general course of this section.
        /// </summary>
        public string CourseId { get; set; }
        /// <summary>
        /// Gets or sets the date on which this section completes.
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Gets or sets the list of IDs of faculty teaching this section.
        /// </summary>
        public IEnumerable<string> FacultyIds { get; set; }
        /// <summary>
        /// Gets or sets the ID of this section.
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Gets or sets a flag indicating whether this section is active.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Gets or sets the location for where this section takes place.
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// Gets or sets the maximum amount of credits for this section, if it is variable.
        /// </summary>
        public decimal? MaximumCredits { get; set; }
        /// <summary>
        /// Gets or sets the meeting blocks for this section.
        /// </summary>
        public IEnumerable<SectionMeeting2> Meetings { get; set; }
        /// <summary>
        /// Gets or sets the minimum amount of credits for this section. If this section is not variable credits then this is the only amount.
        /// </summary>
        public decimal? MinimumCredits { get; set; }
        /// <summary>
        /// Gets or sets the number of this section (e.g. Math 101 01 versus Math 101 02).
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// Gets or sets a flag indicating whether this section only allows Pass/Fail grading.
        /// </summary>
        public bool OnlyPassNoPass { get; set; }
        /// <summary>
        /// Indicates whether Section overrides Course requisites, or whether Course Requisites should be retrieved
        /// </summary>
        public bool OverridesCourseRequisites { get; set; }
        /// <summary>
        /// List of Requisites for this section
        /// </summary>
        public IEnumerable<Requisite> Requisites { get; set; }
        /// <summary>
        /// Gets or sets the date on which this section begins.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets the ID of the term in which this section takes places. This will be empty if the section is a non-term item.
        /// </summary>
        public string TermId { get; set; }
        /// <summary>
        /// Gets or sets the title of this specific section.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the increment value for credits, if this is a variable credit course. (e.g. 0.5, which yields credit values 3.0, 3.5, 4.0).
        /// </summary>
        public decimal? VariableCreditIncrement { get; set; }
        /// <summary>
        /// Gets or sets a flag indicating whether a waitlist is available for this section.
        /// </summary>
        public bool WaitlistAvailable { get; set; }
        /// <summary>
        /// Gets or sets the number of students on the waitlist for this section.
        /// </summary>
        public int Waitlisted { get; set; }

        public bool ShowWaitlistInfo {
            get
            {
                return this.WaitlistAvailable && (this.Waitlisted > 0 || (this.Capacity.HasValue && this.Capacity > 0 && this.Available.HasValue && this.Available <= 0));
            }
        }
        #endregion

        //Properties from the Section DTO that are not being used and are not represented here:
        //  ActiveStudentIds
        //  LearningProvider
        //  LearningProviderSideId
        //  PrimarySectionId

        public CourseSearchResultSection(Section3 section)
        {
            if (section == null)
            {
                throw new ArgumentNullException("section", "section must not be null");
            }

            this.AllowAudit = section.AllowAudit;
            this.AllowPassNoPass = section.AllowPassNoPass;
            this.Available = section.Available;
            this.Books = section.Books;
            this.Capacity = section.Capacity;
            this.Ceus = section.Ceus;
            this.CourseId = section.CourseId;
            this.EndDate = section.EndDate;
            this.FacultyIds = section.FacultyIds;
            this.Id = section.Id;
            this.IsActive = section.IsActive;
            this.Location = section.Location;
            this.MaximumCredits = section.MaximumCredits;
            this.Meetings = section.Meetings;
            this.MinimumCredits = section.MinimumCredits;
            this.Number = section.Number;
            this.OnlyPassNoPass = section.OnlyPassNoPass;
            this.OverridesCourseRequisites = section.OverridesCourseRequisites;
            this.Requisites = section.Requisites;
            this.StartDate = section.StartDate;
            this.TermId = section.TermId;
            this.Title = section.Title;
            this.VariableCreditIncrement = section.VariableCreditIncrement;
            this.WaitlistAvailable = section.WaitlistAvailable;
            this.Waitlisted = section.Waitlisted;

            //set the items that are not part of the Section DTO
            this.StartDateDisplay = string.Empty;
            this.EndDateDisplay = string.Empty;
            this.IsNonTermOffering = false;
            this.IsNonStandardDates = false;
        }
    }
}