﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Courses
{
    public class RequisiteModel
    {
        public RequisiteModel() { }

        public string DisplayText { get; set; }
        public string DisplayTextExtension { get; set; }
        public string RequisiteId { get; set; }
        //public string CoRequisiteCourseId { get; set; }
        //public List<string> CoRequisiteSectionIds { get; set; }
        //public int NumberNeeded { get; set; }
        public bool IsRequired { get; set; }
    }

}
