﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Utility;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Models.Courses
{
    public class SectionDetailsMeetingModel
    {
        public SectionDetailsMeetingModel()
        {
        
        }

        public SectionDetailsMeetingModel(Ellucian.Colleague.Dtos.Student.SectionMeeting2 meeting,
                            string location,
                            Dictionary<string, Ellucian.Colleague.Dtos.Base.Location> allLocations,
                            Dictionary<string, Ellucian.Colleague.Dtos.Base.Building> allBuildings,
                            Dictionary<string, Ellucian.Colleague.Dtos.Base.Room> allRooms,
                            Dictionary<string, Ellucian.Colleague.Dtos.Student.InstructionalMethod> allInstrMethods)
        {

            // First line in meeting information block contains days of week and times
            Time = string.Empty;

            //days
            Time += DayOfWeekConverter.EnumCollectionToString(meeting.Days, GlobalResources.GetString(GlobalResourceFiles.SectionCourseCommonDetailsResources, "SectionDaysOfWeekSeparator"));

            //time
            try
            {
                if (meeting.StartTime.HasValue && meeting.EndTime.HasValue)
                {
                    Time += " " + meeting.StartTime.Value.LocalDateTime.ToShortTimeString() + " " + GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "CourseDelimiter") + " " + meeting.EndTime.Value.LocalDateTime.ToShortTimeString();
                }
                else if (meeting.StartTime.HasValue)
                {
                    Time += string.Format(GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "CourseTimeStartsText"), meeting.StartTime.Value.LocalDateTime.ToShortTimeString());
                }
                else
                {
                    if (!meeting.IsOnline)
                    {
                        Time += " " + GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "TimesToBeDeterminedText");
                    }
                }
            }
            catch
            {
                if (!meeting.IsOnline)
                {
                    Time += " " + GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "TimesToBeDeterminedText");
                }
            }

            // Second line of meeting information block contains campus location, building, room, and instructional method
            Location = string.Empty;

            //campus
            Location = (!string.IsNullOrEmpty(location) && allLocations.ContainsKey(location)) ? " " + allLocations[location].Description + ", " : " ";

            //room and building
            if (!string.IsNullOrEmpty(meeting.Room) && allRooms.ContainsKey(meeting.Room))
            {
                Room room = allRooms[meeting.Room];
                if (!string.IsNullOrEmpty(room.BuildingCode) && allBuildings.ContainsKey(room.BuildingCode))
                {
                    Building building = allBuildings[room.BuildingCode];
                    if (!string.IsNullOrEmpty(building.Description))
                    {
                        Location += building.Description;
                        if (!string.IsNullOrEmpty(room.Code))
                        {
                            Location += " " + room.Code;
                        }
                    }
                    else
                    {
                        if (!meeting.IsOnline)
                        {
                            Location += GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "ToBeDeterminedText");  
                        }
                    }
                }
                else
                {
                    if (!meeting.IsOnline)
                    {
                        Location += GlobalResources.GetString(GlobalResourceFiles.CourseResources, "ToBeDeterminedText");
                    }
                }
            }
            else
            {
                if (!meeting.IsOnline)
                {
                    Location += GlobalResources.GetString(GlobalResourceFiles.CourseResources, "ToBeDeterminedText");
                }
            }

            //instructional method
            if (!string.IsNullOrEmpty(meeting.InstructionalMethodCode) && allInstrMethods.ContainsKey(meeting.InstructionalMethodCode))
            {
                Location += " (" + allInstrMethods[meeting.InstructionalMethodCode].Description + ")";
            }

            // Third line of meeting information block contains start and end date
            Dates = string.Empty;
            if (meeting.StartDate != null && meeting.EndDate != null)
            {
                Dates = meeting.StartDate.GetValueOrDefault().ToShortDateString() + " - " + meeting.EndDate.GetValueOrDefault().ToShortDateString();
            }
        }


        public string Time { get; set; }

        public string Location { get; set; }

        public string Dates { get; set; }
    }
}