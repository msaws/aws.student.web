﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Courses
{
     public class LocationCycleRestrictionModel
     {

          public LocationCycleRestrictionModel() 
          {
              SessionCycleDescription = new List<string>();
              YearlyCycleDescription = new List<string>();

          }
          public string LocationCode;
          public string LocationDescription;
          public List<string> SessionCycleDescription;
          public List<string> YearlyCycleDescription;

     }
}