﻿using System.Collections.Generic;

namespace Ellucian.Web.Student.Models.Courses
{
    /// <summary>
    /// Represents a faculty item for display in the section details dialog. This includes the name, various
    /// email addresses and various phone numbers with extensions.
    /// </summary>
    public class SectionDetailsFacultyModel
    {
        /// <summary>
        /// Creates a new instance of SectionDetailsFacultyModel.
        /// </summary>
        public SectionDetailsFacultyModel()
        {
            Name = string.Empty;
            EmailAddresses = new List<string>();
            PhoneNumbers = new List<string>();
        }

        /// <summary>
        /// Gets or sets the name to display.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the email addresses to display.
        /// </summary>
        public List<string> EmailAddresses { get; set; }
        /// <summary>
        /// Gets or sets the phone numbers to display.
        /// </summary>
        public List<string> PhoneNumbers { get; set; }
    }
}