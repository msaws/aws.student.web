﻿using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Models.Courses
{
    /// <summary>
    /// Groups a Term with a list of Sections to be related to a course after a search.
    /// </summary>
    public class TermSectionList
    {
        /// <summary>
        /// Gets or sets the term associated with this list of sections.
        /// </summary>
        public Term Term { get; set; }
        /// <summary>
        /// Gets or sets the list of sections for this term.
        /// </summary>
        public List<SectionInstructorsModel> Sections { get; set; }

        /// <summary>
        /// Default constructor, creates an empty TermSectionList instance.
        /// </summary>
        public TermSectionList()
        {
            Term = null;
            Sections = new List<SectionInstructorsModel>();
        }
    }
}