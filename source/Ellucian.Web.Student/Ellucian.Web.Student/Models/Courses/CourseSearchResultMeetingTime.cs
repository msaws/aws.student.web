﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.using System;
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Student;


namespace Ellucian.Web.Student.Models.Courses
{
    public class CourseSearchResultMeetingTime
    {
        /// <summary>
        /// Gets a formatted string displaying the meeting days, such as M, W, F.
        /// </summary>
        public string DaysOfWeekDisplay
        {
            get
            {
                List<string> dow = new List<string>();
                if (this.Days.Contains(DayOfWeek.Monday))
                {
                    dow.Add("M");
                }
                if (this.Days.Contains(DayOfWeek.Tuesday))
                {
                    dow.Add("T");
                }
                if (this.Days.Contains(DayOfWeek.Wednesday))
                {
                    dow.Add("W");
                }
                if (this.Days.Contains(DayOfWeek.Thursday))
                {
                    dow.Add("Th");
                }
                if (this.Days.Contains(DayOfWeek.Friday))
                {
                    dow.Add("F");
                }
                if (this.Days.Contains(DayOfWeek.Saturday))
                {
                    dow.Add("Sa");
                }
                if (this.Days.Contains(DayOfWeek.Sunday))
                {
                    dow.Add("Su");
                }

                return string.Join("/", dow.ToArray());
            }
        }

        /// <summary>
        /// Gets a formatted string of the start time.
        /// </summary>
        public string StartTimeDisplay
        {
            get
            {
                if (!string.IsNullOrEmpty(this.StartTime))
                {
                    try
                    {
                        DateTime dt = DateTime.Parse(this.StartTime);
                        return dt.ToShortTimeString();
                    }
                    catch
                    {
                        return this.StartTime;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets a formatted string of the end time.
        /// </summary>
        public string EndTimeDisplay
        {
            get
            {
                if (!string.IsNullOrEmpty(this.EndTime))
                {
                    try
                    {
                        DateTime dt = DateTime.Parse(this.EndTime);
                        return dt.ToShortTimeString();
                    }
                    catch
                    {
                        return this.EndTime;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets the full InstructionalMethod to be displayed. This should be an expansion of the provided code.
        /// </summary>
        public string InstructionalMethodDisplay { get; set; }

        /// <summary>
        /// Gets or sets the full Building data to be displayed. This should be an expansion of the provided code.
        /// </summary>
        public string BuildingDisplay { get; set; }

        /// <summary>
        /// Gets or sets the full Room data to be displayed. This should be an expansion of the provided code.
        /// </summary>
        public string RoomDisplay { get; set; }

        /// <summary>
        /// Gets or sets a formatted date display string for this section.
        /// </summary>
        /// <remarks>This could also be done in JavaScript given the Section object, but it's easier to
        /// format it here, so in it goes.</remarks>
        public string DatesDisplay
        {
            get
            {
                string dateDisplay = string.Empty;

                if ((this.StartDate.HasValue) && (this.EndDate.HasValue))
                {
                    dateDisplay = this.StartDate.GetValueOrDefault().ToShortDateString() + " - " + this.EndDate.Value.ToShortDateString();
                }
                else if (this.StartDate.HasValue)
                {
                    dateDisplay = this.StartDate.GetValueOrDefault().ToShortDateString();
                }

                return dateDisplay;
            }
        }

        public CourseSearchResultMeetingTime(SectionMeeting2 mt)
        {
            this.Days = mt.Days;
            this.EndDate = mt.EndDate;
            if (mt.EndTime.HasValue)
            {
                this.EndTime = mt.EndTime.Value.LocalDateTime.TimeOfDay.ToString();
            }
            this.Frequency = mt.Frequency;
            this.InstructionalMethodCode = mt.InstructionalMethodCode;
            this.Room = mt.Room;
            this.StartDate = mt.StartDate;
            if (mt.StartTime.HasValue)
            {
                this.StartTime = mt.StartTime.Value.LocalDateTime.TimeOfDay.ToString();
            }
            this.IsOnline = mt.IsOnline;
            //set the items that are not part of the SectionMeeting DTO
            BuildingDisplay = string.Empty;
            InstructionalMethodDisplay = string.Empty;
            RoomDisplay = string.Empty;
        }

        #region Inherited from SectionMeeting
        /// <summary>
        /// Gets or sets the list of days on which this section meets.
        /// </summary>
        public IEnumerable<DayOfWeek> Days { get; set; }
        /// <summary>
        /// Gets or sets the date on which this meeting pattern ends.
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Gets or sets the time at which this meeting ends.
        /// </summary>
        public string EndTime { get; set; }
        /// <summary>
        /// Gets or sets the frequency with which this meeting occurs.
        /// </summary>
        public string Frequency { get; set; }
        /// <summary>
        /// Gets or sets the instructional type code for this meeting.
        /// </summary>
        public string InstructionalMethodCode { get; set; }
        /// <summary>
        /// Gets or sets room in which this meeting occurs.
        /// </summary>
        public string Room { get; set; }
        /// <summary>
        /// Gets or sets the date on which this meeting pattern begins.
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// Gets or sets the time at which this meeting begins.
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// Gets or sets whether meeting is an online type of meeting.
        /// </summary>
        public bool IsOnline { get; set; }
        #endregion
    }
}