﻿// Copyright 2012 - 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Models.Courses
{
    public class SectionInstructorsModel
    {
        /// <summary>
        /// Gets or sets the Section to be associated with faculty.
        /// </summary>
        public CourseSearchResultSection Section { get; set; }
        /// <summary>
        /// Gets or sets a string to display the full list of instructors.
        /// </summary>
        public string FacultyDisplay { get; set; }
        /// <summary>
        /// Gets or sets the full list of Faculty objects associated with this Section.
        /// </summary>
        public List<Ellucian.Colleague.Dtos.Student.Faculty> Instructors { get; set; }
    }
}