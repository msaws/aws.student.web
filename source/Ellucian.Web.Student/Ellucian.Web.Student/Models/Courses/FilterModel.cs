﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models
{
    public class FilterModel
    {
        public string Value { get; set; }
        public string Description { get; set; }
        public int Count { get; set; }
        public bool Selected { get; set; }
    }
}