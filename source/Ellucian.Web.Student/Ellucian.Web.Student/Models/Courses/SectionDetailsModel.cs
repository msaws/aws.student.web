﻿// Copyright 2013-2014 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Models.Courses
{
     /// <summary>
     /// Represents a model for showing full details about a specific Section of a course.
     /// </summary>
     public class SectionDetailsModel : Section3
     {
          /// <summary>
          /// Creates a new SectionDetailsModel instance based on the provided Section DTO.
          /// </summary>
          /// <param name="section">A section object with which to populate the model</param>
          public SectionDetailsModel()
               : base()
          {
               PassNoPassIsRestricted = false;
               AuditIsRestricted = false;
          }

          /// <summary>
          /// The list of requisites for this section, whether native or from the course
          /// </summary>
          public IEnumerable<RequisiteModel> RequisiteItems { get; set; }

          /// <summary>
          /// Gets or sets the list of Time/Location items to show in the details dialog. This is a concatenation of 
          /// the days of week, time, and location showing on one line. Each section may have multiple of these.
          /// </summary>
          public IEnumerable<SectionDetailsMeetingModel> TimeLocationItems { get; set; }

          /// <summary>
          /// Gets or sets the list of Instructor items to show in the details dialog. This is a collection of
          /// the name, email address as a link, and phone number, and may include multiple of each. Each section may have
          /// multiple instructors.
          /// </summary>
          public IEnumerable<SectionDetailsFacultyModel> InstructorItems { get; set; }

          /// <summary>
          /// Gets or sets the total cost of all the books for this section.
          /// </summary>
          public string BooksCostRequired { get; set; }

          /// <summary>
          /// Gets or sets the total cost of all the books for this section.
          /// </summary>
          public string BooksCostOptional { get; set; }

          /// <summary>
          /// Displays cost information for books, separating required and optional costs.
          /// </summary>
          public string BooksTotal
          {
               get
               {
                    string booksTotal = string.Empty;
                    if (!string.IsNullOrEmpty(BooksCostRequired))
                    {
                        booksTotal = BooksCostRequired + " " + GlobalResources.GetString(GlobalResourceFiles.SectionCourseCommonDetailsResources, "SectionBooksRequiredLabel");
                    }
                    if (!string.IsNullOrEmpty(BooksCostOptional))
                    {
                         if (!string.IsNullOrEmpty(BooksCostRequired))
                         {
                              booksTotal += ", ";
                         }
                         booksTotal += BooksCostOptional + " " +  GlobalResources.GetString(GlobalResourceFiles.SectionCourseCommonDetailsResources, "SectionBooksOptionalLabel");
                    }
                    return booksTotal;
               }

          }

          /// <summary>
          /// Gets or sets the actual term display value.
          /// </summary>
          public string TermDisplay { get; set; }

          /// <summary>
          /// Gets or sets a formatted date display string for this section.
          /// </summary>
          /// <remarks>This could also be done in JavaScript given the Section object, but it's easier to
          /// format it here, so in it goes.</remarks>
          public string DatesDisplay
          {
               get
               {
                    string dateDisplay = string.Empty;

                    if (this.EndDate.HasValue)
                    {
                         dateDisplay = this.StartDate.ToShortDateString() + " - " + this.EndDate.Value.ToShortDateString();
                    }
                    else
                    {
                         dateDisplay = this.StartDate.ToShortDateString();
                    }

                    return dateDisplay;
               }
          }

          /// <summary>
          /// Indicates the person viewing the section details model is prohibited from taking a class pass/fail.
          /// </summary>
          public bool PassNoPassIsRestricted { get; set; }
          /// <summary>
          /// Indicates the person viewing the section details model is prohibited from taking a class audit.
          /// </summary>
          public bool AuditIsRestricted { get; set; }

          public bool GradingOptionsMatch { get; set; }

          /// <summary>
          /// Indicates whether or not this section will transfer to other institutions
          /// </summary>
          public string TransferStatusDescription { get; set; }

          /// <summary>
          /// The description of the TopicCode associated with this Section
          /// </summary>
          public string TopicCodeDescription { get; set; }
     }
}