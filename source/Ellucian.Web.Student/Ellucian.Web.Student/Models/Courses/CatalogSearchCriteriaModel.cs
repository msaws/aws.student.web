﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Courses
{
    public class CatalogSearchCriteriaModel
    {
        public List<string> Subjects {get;set;}
        public List<string> AcademicLevels { get; set; }
        public List<string> CourseLevels { get; set; }
        public List<string> CourseTypes { get; set; }
        public List<string> TopicCodes { get; set; }
        public List<string> Terms { get; set; }
        public List<string> Days { get; set; }
        public List<string> Locations { get; set; }
        public List<string> Faculty { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int? StartTime{get;set;}
        public int? EndTime{get;set;}
        public string Keyword{get;set;}
        public string Requirement{get;set;}
        public string Subrequirement{get;set;}
        public string Group{get;set;}
        public List<string> CourseIds{get;set;}
        public List<string> SectionIds{get;set;}
        public string RequirementText{get;set;}
        public string SubRequirementText { get; set; }
        public List<string> OnlineCategories { get; set; }
         public int PageNumber{get;set;}
        public  int QuantityPerPage{get;set;}
        public bool? OpenSections { get; set; }
        public List<CatalogAdvancedSearchKeywordComponentsModel> KeywordComponents { get; set; }

        public CatalogSearchCriteriaModel()
        {
            PageNumber = 1;
            QuantityPerPage = Ellucian.Web.Student.Models.Courses.CourseSearchResult.QuantityPerPage;
            KeywordComponents = new List<CatalogAdvancedSearchKeywordComponentsModel>();
            Subjects = new List<string>();
            AcademicLevels = new List<string>();
            CourseLevels = new List<string>();
            CourseTypes = new List<string>();
            TopicCodes = new List<string>();
            Terms = new List<string>();
            Days = new List<string>();
            Locations = new List<string>();
            Faculty = new List<string>();


        }
        public CatalogSearchCriteriaModel(CatalogAdvancedSearchViewModel searchModel):this()
        {
            if(searchModel.Term!=null)
            {
                this.Terms.Add(searchModel.Term);
            }
            if(searchModel.Location!=null)
            {
                this.Locations.Add(searchModel.Location);
            }
            if (searchModel.AcademicLevel != null)
            {
                this.AcademicLevels.Add(searchModel.AcademicLevel);
            }
            if (searchModel.CourseType != null)
            {
                this.CourseTypes.Add(searchModel.CourseType);
            }
            if(searchModel.KeywordComponents!=null && searchModel.KeywordComponents.Count > 0)
            {
                this.KeywordComponents.AddRange(searchModel.KeywordComponents);
            }
            if(!string.IsNullOrEmpty(searchModel.StartDate))
            {
                this.StartDate = searchModel.StartDate;
            }
            if (!string.IsNullOrEmpty(searchModel.StartDate))
            {
                this.EndDate = searchModel.EndDate;
            }
            if(searchModel.TimeRange!=null)
            {
                    this.StartTime = searchModel.TimeRange.Item2; //start date
                    this.EndTime = searchModel.TimeRange.Item3; //end date
            }

            if (searchModel.Days != null && searchModel.Days.Count > 0)
            {
                this.Days.AddRange(searchModel.Days);
            }
            if (!string.IsNullOrEmpty(searchModel.Keyword))
            {
                this.Keyword = searchModel.Keyword;
            }
            if (!string.IsNullOrEmpty(searchModel.Subject))
            {
                this.Subjects.Add(searchModel.Subject);
            }
        }

    }
}