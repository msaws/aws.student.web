﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Models.Courses
{
    /// <summary>
    /// Defines a mapping between Courses and their associated Sections.
    /// </summary>
    public class CourseFullModel : CourseSearch2
    {
        public CourseFullModel(CourseSearch2 course)
            : base()
        {
            LocationsDisplay = string.Empty;
            TermsAndSections = null;
            HasSections = false;

            // From the course DTO
            Ceus = course.Ceus;
            Description = course.Description;
            Id = course.Id;
            LocationCodes = course.LocationCodes;
            MaximumCredits = course.MaximumCredits;
            MinimumCredits = course.MinimumCredits;
            Number = course.Number;
            SubjectCode = course.SubjectCode;
            TermsOffered = course.TermsOffered;
            Title = course.Title;
            VariableCreditIncrement = course.VariableCreditIncrement;
            YearsOffered = course.YearsOffered;
            Requisites = course.Requisites;
            LocationCycleRestrictions = course.LocationCycleRestrictions;
            TermSessionCycle = course.TermSessionCycle;
            TermYearlyCycle = course.TermYearlyCycle;

            MatchingSectionIds = course.MatchingSectionIds;
        }

        /// <summary>
        /// Gets or sets a display value representing all of the locations associated with this Course item.
        /// </summary>
        public string LocationsDisplay { get; set; }

        /// <summary>
        /// List of Requisites for this course
        /// </summary>
        public IEnumerable<RequisiteModel> CourseRequisites { get; set; }

        /// <summary>
        /// Gets or sets the nested collection of terms and sections for this course.
        /// </summary>
        public List<TermSectionList> TermsAndSections { get; set; }

        /// <summary>
        /// Gets or sets whether this result has any sections that can be retrieved later.
        /// </summary>
        public bool HasSections { get; set; }

         /// <summary>
         /// Gets or sets a list of LocationCycleRestrictionModels
         /// </summary>
        public List<LocationCycleRestrictionModel> LocationCycleRestrictionDescriptions { get; set; }

        /// <summary>
        /// Returns the correct display string for the number of credits or CEUs
        /// </summary>
        public string CreditsCeusDisplay
        {
            get
            {
                string display = string.Empty;
                if (this.Ceus.HasValue && this.Ceus.Value >= 0)
                {
                    display = (this.Ceus.Value % 1 == 0) ? this.Ceus.Value.ToString("0") : this.Ceus.Value.ToString("0.#####");
                }
                else if (this.MaximumCredits.HasValue && this.MaximumCredits.Value > 0 && this.MinimumCredits.HasValue)
                {
                    display = (this.MinimumCredits.Value % 1 == 0) ? this.MinimumCredits.Value.ToString("0") : this.MinimumCredits.Value.ToString("0.#####");
                    display += " to ";
                    display += (this.MaximumCredits.Value % 1 == 0) ? this.MaximumCredits.Value.ToString("0") : this.MaximumCredits.Value.ToString("0.#####");
                }
                else if (this.MinimumCredits.HasValue)
                {
                    display = (this.MinimumCredits.Value % 1 == 0) ? this.MinimumCredits.Value.ToString("0") : this.MinimumCredits.Value.ToString("0.#####");
                }
                return display;
            }
        }

        /// <summary>
        /// Returns "CEUs" or "Credits", depending on which is populated
        /// </summary>
        public string CreditsDisplayLabel
        {
            get
            {
                return Ceus != null ? "CEUs" : "Credits";
            }
        }

        public bool IsVariableCredit
        {
            get
            {
                return (MinimumCredits != null && MaximumCredits != null && MinimumCredits != MaximumCredits);
            }
        }
    }
}