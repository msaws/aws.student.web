﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Ellucian.Web.Student.Models.Courses
{
    public class CatalogAdvancedSearchViewModel
    {
        public string Location { get; set; }
        public string AcademicLevel { get; set; }
        public string CourseType { get; set; }
        public string Term { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<string> Days { get; set; }
        public List<CatalogAdvancedSearchKeywordComponentsModel> KeywordComponents { get; set; }
        public Tuple<string, int, int> TimeRange { get; set; }
         public string Keyword { get; set; }
         public string Subject { get; set; }
        public CatalogAdvancedSearchViewModel()
        {
            KeywordComponents = new List<CatalogAdvancedSearchKeywordComponentsModel>();
            Days = new List<string>();
        }
       
    }

   
}