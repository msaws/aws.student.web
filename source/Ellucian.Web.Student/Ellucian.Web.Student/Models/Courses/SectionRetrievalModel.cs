﻿using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Models.Courses
{
    /// <summary>
    /// Represents a model for retrieving full Section information for course search results.
    /// </summary>
    public class SectionRetrievalModel
    {
        /// <summary>
        /// Gets or sets the nested collection of terms and sections for a course.
        /// </summary>
        public List<TermSectionList> TermsAndSections { get; set; }

        /// <summary>
        /// Gets or sets the full Course object to which these sections are attached.
        /// </summary>
        public Course2 Course { get; set; }

        /// <summary>
        /// Creates a new, empty SectionRetrievalModel instance.
        /// </summary>
        public SectionRetrievalModel()
        {
            TermsAndSections = new List<TermSectionList>();
            Course = null;
        }
    }
}