﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Models.Courses
{
    public class CourseSearchResult
    {
        /// <summary>
        /// Defines the amount of results to show per page.
        /// </summary>
        public const int QuantityPerPage = 30;

        /// <summary>
        /// Gets or sets the search keyword.
        /// </summary>
        public string Keyword = string.Empty;
        /// <summary>
        /// Gets or sets list of matching CourseSearch items returned via the search service.
        /// </summary>
        public List<CourseSearch2> Courses = new List<CourseSearch2>();
        /// <summary>
        /// Gets or sets the full list of assembled CoursesSectionsModel items matching the search. These will include
        /// the full data set for a search result, including full strings where only codes were returned from the service.
        /// </summary>
        public List<CourseFullModel> CourseFullModels { get; set; }

        /// <summary>
        /// Gets or sets the total number of matching items in this set of search results.
        /// </summary>
        public int TotalItems { get; set; }
        /// <summary>
        /// Gets or sets the total number of pages in this set of search results.
        /// </summary>
        public int TotalPages { get; set; }
        /// <summary>
        /// Gets or sets the number of items on each page.
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Gets or sets the page that is currently showing.
        /// </summary>
        public int CurrentPageIndex { get; set; }
        /// <summary>
        /// Gets or sets the filter list of Subjects applied to this search.
        /// </summary>
        public List<FilterModel> Subjects = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Academic Levels applied to this search.
        /// </summary>
        public List<FilterModel> AcademicLevels = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Course Levels applied to this search.
        /// </summary>
        public List<FilterModel> CourseLevels = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Days of the Week applied to this search.
        /// </summary>
        public List<FilterModel> DaysOfWeek = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Locations applied to this search.
        /// </summary>
        public List<FilterModel> Locations = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Faculty applied to this search.
        /// </summary>
        public List<FilterModel> Faculty = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Course Types applied to this search.
        /// </summary>
        public List<FilterModel> CourseTypes = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Course Topic Code applied to this search.
        /// </summary>
        public List<FilterModel> TopicCodes = new List<FilterModel>();
        /// <summary>
        /// Gets or sets the filter list of Section Term applied to this search.
        /// </summary>
        public List<FilterModel> TermFilters = new List<FilterModel>();

        /// <summary>
        /// Gets or sets the Start Time filter as it exists in sync with the database. Time of Day filtering is unique in that changes to the slider are not
        /// immediately submitted for filtering, so the value that is held in this property is the last value which was submitted as a filter refresh.
        /// </summary>
        public int StartTime { get; set; }

        /// <summary>
        /// Gets or sets the End Time filter as it exists in sync with the database. Time of Day filtering is unique in that changes to the slider are not
        /// immediately submitted for filtering, so the value that is held in this property is the last value which was submitted as a filter refresh.
        /// </summary>
        public int EndTime { get; set; }

        /// <summary>
        /// open sections
        /// </summary>
        public FilterModel OpenSections = new FilterModel();

        /// <summary>
        /// Gets or sets the filter list of Online Categories.
        /// </summary>
        public List<FilterModel> OnlineCategories = new List<FilterModel>();

        /// <summary>
        /// Gets or sets a Requirement associated with this search (i.e. if it was originated from the Degree Requirements page).
        /// </summary>
        public string Requirement { get; set; }
        /// <summary>
        /// Gets or sets a SubRequirement associated with this search (i.e. if it was originated from the Degree Requirements page).
        /// </summary>
        public string Subrequirement { get; set; }
        /// <summary>
        /// Gets or sets the Requirement text associated with this search (i.e. if it was originated from the Degree Requirements page).
        /// </summary>
        public string RequirementText { get; set; }
        /// <summary>
        /// Gets or sets the Keyword components converted into string text associated with this search (i.e. if it was originated from the Advance Search tab).
        /// </summary>
        public string AdvancedSearchText { get; set; }
        /// <summary>
        /// Gets or sets the Group associated with this search (i.e. if it was originated from the Degree Requirements page).
        /// </summary>
        public string Group { get; set; }
        /// <summary>
        /// Gets or sets the courseIds associated with this search (i.e. if it was originated from the Plan.)
        /// </summary>
        public string CourseIds { get; set; }

        /// <summary>
        /// Gets or sets a potential error message associated with the current search.
        /// </summary>
        public string ErrorMessage = string.Empty;

        /// <summary>
        /// Gets or sets the list of terms that should be available for students when placing courses on their plan.
        /// </summary>
        public IEnumerable<Term> PlanTerms { get; set; }

        /// <summary>
        /// Creates a new CourseSearchResult package based on the provided data.
        /// </summary>
        /// <param name="coursePage">The CoursePage DTO that was returned from the search service</param>
        /// <param name="subjects">A list of all Subjects for lookup</param>
        /// <param name="academicLevels">A list of all Academic Levels for lookup</param>
        /// <param name="courseLevels">A list of all Course Levels for lookup</param>
        /// <param name="locations">A list of all Locations for lookup</param>
        /// <param name="faculty">A list of all Faculty for lookup</param>
        /// <param name="courseTypes">A list of all Course Types for lookup</param>
        public CourseSearchResult(CoursePage2 coursePage, IEnumerable<Subject> subjects, IEnumerable<AcademicLevel> academicLevels,
            IEnumerable<CourseLevel> courseLevels, IEnumerable<Location> locations, IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty, IEnumerable<CourseType> courseTypes, IEnumerable<TopicCode> topicCodes, IEnumerable<Term> terms)
        {
            if (coursePage == null)
            {
                throw new ArgumentNullException("coursePage", "coursePage must not be null");
            }
            TotalItems = coursePage.TotalItems;
            TotalPages = coursePage.TotalPages;
            PageSize = coursePage.PageSize;
            CurrentPageIndex = coursePage.CurrentPageIndex;
            PlanTerms = new List<Term>();

            //null guarding of lookups
            if (subjects == null)
            {
                subjects = new List<Subject>();
            }
            if (academicLevels == null)
            {
                academicLevels = new List<AcademicLevel>();
            }
            if (courseLevels == null)
            {
                courseLevels = new List<CourseLevel>();
            }
            if (locations == null)
            {
                locations = new List<Location>();
            }
            if (faculty == null)
            {
                faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>();
            }
            if (courseTypes == null)
            {
                courseTypes = new List<CourseType>();
            }
            if (topicCodes == null)
            {
                topicCodes = new List<TopicCode>();
            }
            if (terms == null)
            {
                terms = new List<Term>();
            }
            
            // Start and end times in minutes from midnight.  1am == 60.  9:45am == 585.
            StartTime = coursePage.EarliestTime;
            EndTime = coursePage.LatestTime == 0 ? 1440 : coursePage.LatestTime;

            foreach (var item in coursePage.Subjects)
            {
                var description = subjects.Where(x => item.Value == x.Code).FirstOrDefault() == null ? item.Value : subjects.Where(x => item.Value == x.Code).FirstOrDefault().Description;
                Subjects.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
            }
            Subjects = Subjects.OrderByDescending(x => x.Count).ThenBy(x => x.Description).ToList();

            foreach (var item in coursePage.AcademicLevels)
            {
                var description = academicLevels.Where(x => item.Value == x.Code).FirstOrDefault() == null ? item.Value : academicLevels.Where(x => item.Value == x.Code).FirstOrDefault().Description;
                AcademicLevels.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
            }
            AcademicLevels = AcademicLevels.OrderByDescending(x => x.Count).ThenBy(x => x.Description).ToList();

            foreach (var item in coursePage.CourseLevels)
            {
                var description = courseLevels.Where(x => item.Value == x.Code).FirstOrDefault() == null ? item.Value : courseLevels.Where(x => item.Value == x.Code).FirstOrDefault().Description;
                CourseLevels.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
            }
            CourseLevels = CourseLevels.OrderByDescending(x => x.Count).ThenBy(x => x.Description).ToList();

            foreach (var item in coursePage.CourseTypes)
            {
                var description = courseTypes.Where(x => item.Value == x.Code).FirstOrDefault() == null ? item.Value : courseTypes.Where(x => item.Value == x.Code).FirstOrDefault().Description;
                if (!string.IsNullOrEmpty(description))
                {
                    CourseTypes.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
                }
            }
            CourseTypes = CourseTypes.OrderByDescending(x => x.Count).ThenBy(x => x.Description).ToList();

            foreach (var item in coursePage.TopicCodes)
            {
                var description = topicCodes.Where(x => item.Value == x.Code).FirstOrDefault() == null ? item.Value : topicCodes.Where(x => item.Value == x.Code).FirstOrDefault().Description;
                if (!string.IsNullOrEmpty(description))
                {
                    TopicCodes.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
                }
            }
            TopicCodes = TopicCodes.OrderByDescending(x => x.Count).ThenBy(x => x.Description).ToList();

            foreach (var item in coursePage.Terms)
            {
                var description = terms.Where(x => item.Value == x.Code).FirstOrDefault() == null ? item.Value : terms.Where(x => item.Value == x.Code).FirstOrDefault().Description;
                if (!string.IsNullOrEmpty(description))
                {
                    TermFilters.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
                }
            }
            TermFilters = TermFilters.OrderByDescending(x => x.Count).ThenBy(x => x.Description).ToList();

            foreach (var item in coursePage.DaysOfWeek)
            {
                var description = DayOfWeekConverter.IntToDayString(Int32.Parse(item.Value));
                DaysOfWeek.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
            }
            DaysOfWeek = DaysOfWeek.OrderBy(x => x.Value).ToList();

            foreach (var item in coursePage.Locations)
            {
                var description = locations.Where(x => item.Value == x.Code).FirstOrDefault() == null ? item.Value : locations.Where(x => item.Value == x.Code).FirstOrDefault().Description;
                if (!string.IsNullOrEmpty(description))
                { //description = "None Specified";
                    Locations.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
                }
            }
            Locations = Locations.OrderBy(x => x.Description).ToList();

            foreach (var item in coursePage.Faculty)
            {
                var description = faculty.Where(x => item.Value == x.Id).FirstOrDefault() == null ? item.Value : NameHelper.FacultyDisplayName(faculty.Where(x => item.Value == x.Id).FirstOrDefault());
                Faculty.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
            }
            Faculty = Faculty.OrderBy(x => x.Description).ToList();

            // Get the user-specified filter value for each possible online category. Default provides a filter for unexpected values.
            foreach (var item in coursePage.OnlineCategories)
            {
                var description = string.Empty;
                switch (item.Value)
                {
                    case "Online":
                        description = GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "OnlineCategoryOnline");
                        break;
                    case "NotOnline":
                        description = GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "OnlineCategoryNotOnline");
                        break;
                    case "Hybrid":
                        description = GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "OnlineCategoryHybrid");
                        break;
                    default:
                        description = item.Value;
                        break;
                }
                OnlineCategories.Add(new FilterModel { Count = item.Count, Description = description, Selected = item.Selected, Value = item.Value });
            }

            //set filter details for Open sections (seats availibility) 
            OpenSections.Count = coursePage.OpenSections.Count;
            OpenSections.Selected = coursePage.OpenSections.Selected;
            OpenSections.Description = GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "OpenSectionsOnly");

            
            CourseFullModels = new List<CourseFullModel>();

            // If there are no courses, set the error message and set page number to 1
            if (coursePage.CurrentPageItems == null || coursePage.CurrentPageItems.Count() == 0)
            {
                ErrorMessage = GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "NoneFoundError");
            }
            else
            {
                Courses.AddRange(coursePage.CurrentPageItems.ToList());
            }
        }
    }
}