﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Ellucian.Web.Student.Models.Courses
{

    public class CatalogAdvancedSearchKeywordComponentsModel
    {
        [JsonProperty("subject")]
        public string Subject { get; set; }
        [JsonProperty("courseNumber")]
        public string CourseNumber { get; set; }
        [JsonProperty("section")]
        public string Section { get; set; }

        public CatalogAdvancedSearchKeywordComponentsModel()
        {
        }

        public CatalogAdvancedSearchKeywordComponentsModel(string subject, string courseNumber, string sectionNumber)
        {
            this.Subject = subject;
            this.CourseNumber = courseNumber;
            this.Section = sectionNumber;
        }
        public bool IsEmpty()
        {
            if (string.IsNullOrWhiteSpace(this.Subject) && string.IsNullOrWhiteSpace(CourseNumber) && string.IsNullOrWhiteSpace(Section))
            {
                return true;
            }
            else
                return false;
        }
    }
    
}