﻿// Copyright 2012 - 2017 Ellucian Company L.P. and its affiliates

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Courses
{
    public class CatalogAdvancedSearchItemsModel
    {
        public List<Ellucian.Colleague.Dtos.Student.Subject> Subjects { get; set; }
        public List<Tuple<string,string>> Terms { get; set; }
        public List<Tuple<string, string>> Locations { get; set; }
        public List<Tuple<string, string>> AcademicLevels { get; set; }
        public List<Tuple<string, string>> CourseTypes { get; set; }
        public List<Tuple<string, int, int>> TimeRanges { get; set; }
        public List<Tuple<string, string, bool>> DaysOfWeek { get; set; }
        public DateTime? EarliestSearchDate { get; set; }
        public DateTime? LatestSearchDate { get; set; }
        
        public CatalogAdvancedSearchItemsModel()
        {
            Subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>();
            Terms = new List<Tuple<string, string>>();
            Locations = new List<Tuple<string, string>>();
            AcademicLevels = new List<Tuple<string, string>>();
            CourseTypes = new List<Tuple<string, string>>();
            TimeRanges = new List<Tuple<string, int, int>>();
            DaysOfWeek = new List<Tuple<string, string, bool>>();
            
            
        }

        public CatalogAdvancedSearchItemsModel(List<Ellucian.Colleague.Dtos.Student.Subject> subjects, List<Ellucian.Colleague.Dtos.Student.Term> terms, List<Ellucian.Colleague.Dtos.Base.Location> locations,
            List<Ellucian.Colleague.Dtos.Student.AcademicLevel> academicLevels,List<Ellucian.Colleague.Dtos.Student.CourseType> courseTypes, List<Tuple<string, int, int>> timeRanges, List<Tuple<string, string, bool>> daysOfWeek,Ellucian.Colleague.Dtos.Student.CourseCatalogConfiguration catalogConfiguration )
            : this()
        {
            if (subjects != null)
            {
                this.Subjects = subjects;
            }
            if (terms != null)
            {
                this.Terms = terms.Select(t => new Tuple<string, string>(t.Code, t.Description)).ToList();
            }
            if (locations != null)
            {
                this.Locations = locations.Select(t => new Tuple<string, string>(t.Code, t.Description)).ToList();
            }
            if (academicLevels != null)
            {
                this.AcademicLevels = academicLevels.Select(t => new Tuple<string, string>(t.Code, t.Description)).ToList();
            }
            if (courseTypes != null)
            {
                this.CourseTypes = courseTypes.Select(t => new Tuple<string, string>(t.Code, t.Description)).ToList();
            }
            if (timeRanges != null)
            {
                this.TimeRanges = timeRanges;
            }
            if (daysOfWeek != null)
            {
                this.DaysOfWeek = daysOfWeek;
            }
            if (catalogConfiguration != null)
            {
                this.EarliestSearchDate = catalogConfiguration.EarliestSearchDate;
                this.LatestSearchDate = catalogConfiguration.LatestSearchDate;
            }
        }

    }
}