﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Models.UserProfile
{
    public class ProfilePhoneModel
    {
        public bool IsUpdatable { get; set; }
        public Phone Phone { get; set; }
        public string PhoneTypeDescription { get; set; }
    }
}