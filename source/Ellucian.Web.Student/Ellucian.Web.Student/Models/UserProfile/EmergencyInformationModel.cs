﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.UserProfile
{
    public class EmergencyContact
    {
        public string Address { get; set; }
        public string DaytimePhone { get; set; }
        public string EffectiveDate { get; set; }
        public string EveningPhone { get; set; }
        public bool IsEmergencyContact { get; set; }
        public bool IsMissingPersonContact { get; set; }
        public string Name { get; set; }
        public string OtherPhone { get; set; }
        public string Relationship { get; set; }
    }

    public class EmergencyInformationModel
    {
        /// <summary>
        /// Id of the person the information belongs to
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// Name of the person the information belongs to
        /// </summary>
        public string PersonName { get; set; }

        /// <summary>
        /// List of emergency contact models
        /// </summary>
        public List<EmergencyContact> EmergencyContacts { get; set; }

        /// <summary>
        /// Hospital Preference
        /// </summary>
        public string HospitalPreference { get; set; }

        /// <summary>
        /// Insurance Information
        /// </summary>
        public string InsuranceInformation { get; set; }

        /// <summary>
        /// Date when the information was last confirmed.  Time not included.
        /// </summary>
        public string ConfirmedDate { get; set; }

        /// <summary>
        /// List of health conditions - includes all with a selector saying which this person has recorded.
        /// </summary>
        public List<HealthConditionModel> HealthConditions { get; set; }

        /// <summary>
        /// Additional emergency information the person chooses to relay
        /// </summary>
        public string AdditionalInformation { get; set; }

        /// <summary>
        /// Notification to be shown when the model is displayed
        /// </summary>
        public Notification Notification { get; set; }

        /// <summary>
        /// Emergency information configuration
        /// </summary>
        public EmergencyInformationConfiguration2 EmergencyInformationConfiguration { get; set; }

        /// <summary>
        /// Opt out of providing emergency and missing contact information
        /// </summary>
        public bool OptOut { get; set; }

        /// <summary>
        /// Does this person have a privacy restriction
        /// </summary>
        public bool HasPrivacyRestriction { get; set; }

        /// <summary>
        /// The privacy message
        /// </summary>
        public string PrivacyMessage { get; set; }

        /// <summary>
        /// Basic constructor - needed for serialization
        /// </summary>
        public EmergencyInformationModel()
        {
            EmergencyContacts = new List<EmergencyContact>();
            HealthConditions = new List<HealthConditionModel>();
            EmergencyInformationConfiguration = new EmergencyInformationConfiguration2();
        }

        public string CurrentDateString
        {
             get { return DateTime.Today.ToShortDateString(); }
        }

        /// <summary>
        /// Constructor for building a full fledged emergency model
        /// </summary>
        /// <param name="emergencyInformation">The Emergency Information DTO</param>
        /// <param name="healthConditions">All Health conditions</param>
        /// <param name="emergencyInformationConfiguration">Emergency information configuration</param>
        public EmergencyInformationModel(Ellucian.Colleague.Dtos.Base.EmergencyInformation emergencyInformation, List<Ellucian.Colleague.Dtos.Base.HealthConditions> healthConditions, EmergencyInformationConfiguration2 emergencyInformationConfiguration, bool hasPrivacyRestriction = false, string privacyMessage = "")
        {
            if (emergencyInformation == null)
            {
                throw new ArgumentNullException("emergencyInformation", "Emergency information DTO must be provided.");
            }
            // add assignments of all properties
            PersonId = emergencyInformation.PersonId;
            PersonName = emergencyInformation.PersonName;
            EmergencyContacts = new List<EmergencyContact>();

            HasPrivacyRestriction = hasPrivacyRestriction;
            PrivacyMessage = privacyMessage;

            if (emergencyInformation.EmergencyContacts != null)
            {
                foreach (var contact in emergencyInformation.EmergencyContacts)
                {
                    var date = contact.EffectiveDate.HasValue ? contact.EffectiveDate.Value.ToShortDateString() : "";
                    EmergencyContacts.Add(new EmergencyContact()
                    {
                        Address = contact.Address,
                        DaytimePhone = contact.DaytimePhone,
                        EffectiveDate = date,
                        EveningPhone = contact.EveningPhone,
                        IsEmergencyContact = contact.IsEmergencyContact,
                        IsMissingPersonContact = contact.IsMissingPersonContact,
                        Name = contact.Name,
                        OtherPhone = contact.OtherPhone,
                        Relationship = contact.Relationship
                    });
                }
            }
            OptOut = emergencyInformation.OptOut;
            HospitalPreference = emergencyInformation.HospitalPreference;
            InsuranceInformation = emergencyInformation.InsuranceInformation;
            ConfirmedDate = emergencyInformation.ConfirmedDate.HasValue ? emergencyInformation.ConfirmedDate.Value.ToShortDateString() : string.Empty;
            AdditionalInformation = emergencyInformation.AdditionalInformation;
            var selected = false;
            HealthConditions = new List<HealthConditionModel>();
            // If no health conditions we will just return an empty list
            if (healthConditions != null)
            {
                for (var i = 0; i < healthConditions.Count; i++)
                {
                    if (emergencyInformation.HealthConditions != null)
                    {
                        selected = emergencyInformation.HealthConditions.Contains(healthConditions[i].Code);
                    }
                    HealthConditions.Add(new HealthConditionModel() { Code = healthConditions[i].Code, Description = healthConditions[i].Description, isSelected = selected });
                }
            }

            EmergencyInformationConfiguration = emergencyInformationConfiguration;
        }
    }

}