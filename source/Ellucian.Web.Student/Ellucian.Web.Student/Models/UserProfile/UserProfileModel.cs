﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Web.Student;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Utility;
using System.Text.RegularExpressions;

namespace Ellucian.Web.Student.Models.UserProfile
{
    public class UserProfileModel : Profile
    {
        /// <summary>
        /// Full name
        /// </summary>
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        /// <summary>
        /// Full Chosen Name
        /// </summary>
        public string FullChosenName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ChosenFirstName) || !string.IsNullOrWhiteSpace(ChosenMiddleName) || !string.IsNullOrWhiteSpace(ChosenLastName))
                {
                    return ChosenFirstName + (!string.IsNullOrEmpty(ChosenMiddleName) ? " " + ChosenMiddleName : "") + " " + ChosenLastName;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Date of birth
        /// </summary>
        public string DateOfBirth
        {
            get
            {
                return this.BirthDate.HasValue ? this.BirthDate.Value.ToShortDateString() : string.Empty;
            }
        }

        /// <summary>
        /// Address Confirmation Date in short local date format
        /// </summary>
        public string AddressConfirmationDate
        {
            get
            {
                return AddressConfirmationDateTime.HasValue ? AddressConfirmationDateTime.Value.LocalDateTime.ToShortDateString() : "";
            }
        }

        /// <summary>
        /// Email Address Confirmation Date in short local date format
        /// </summary>
        public string EmailAddressConfirmationDate
        {
            get
            {
                return EmailAddressConfirmationDateTime.HasValue ? EmailAddressConfirmationDateTime.Value.LocalDateTime.ToShortDateString() : "";
            }
        }

        /// <summary>
        /// Phone Confirmation Date in short local date format
        /// </summary>
        public string PhoneConfirmationDate
        {
            get
            {
                return PhoneConfirmationDateTime.HasValue ? PhoneConfirmationDateTime.Value.LocalDateTime.ToShortDateString() : "";
            }
        }

        /// <summary>
        /// Constructor for UserProfileModel. Combines profile and configuration for use in self service.
        /// </summary>
        /// <param name="profile">Person profile DTO</param>
        public UserProfileModel(Ellucian.Colleague.Dtos.Base.Profile profile)
            : base()
        {
            if (profile == null)
            {
                throw new ArgumentNullException("profile", "Profile is required to create a UserProfileModel");
            }
            this.Id = profile.Id;
            this.BirthDate = profile.BirthDate;
            this.FirstName = profile.FirstName;
            this.LastName = profile.LastName;
            this.ChosenFirstName = profile.ChosenFirstName;
            this.ChosenMiddleName = profile.ChosenMiddleName;
            this.ChosenLastName = profile.ChosenLastName;
            this.PersonalPronounCode = profile.PersonalPronounCode;
            this.PreferredEmailAddress = profile.PreferredEmailAddress;
            this.Addresses = profile.Addresses;
            this.EmailAddresses = profile.EmailAddresses;
            this.Phones = profile.Phones;
            this.LastChangedDateTime = profile.LastChangedDateTime;
            this.AddressConfirmationDateTime = profile.AddressConfirmationDateTime;
            this.EmailAddressConfirmationDateTime = profile.EmailAddressConfirmationDateTime;
            this.PhoneConfirmationDateTime = profile.PhoneConfirmationDateTime;
        }

        /// <summary>
        /// Constructor for UserProfileModel. Required for serialization.
        /// </summary>
        public UserProfileModel() : base()
        {
        }
    }
}