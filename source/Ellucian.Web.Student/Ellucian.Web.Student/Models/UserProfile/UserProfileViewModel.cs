﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Security;
using AddressType2 = Ellucian.Colleague.Dtos.AddressType2;

namespace Ellucian.Web.Student.Models.UserProfile
{
    /// <summary>
    /// Model for the User Profile page containing the user profile and configuration
    /// </summary>
    public class UserProfileViewModel
    {
        /// <summary>
        /// User profile model
        /// </summary>
        public UserProfileModel UserProfileModel { get; set; }

        /// <summary>
        /// Configuration settings for this user profile
        /// </summary>
        public UserProfileConfiguration2 Configuration { get; set; }

        /// <summary>
        /// Phone type and description associations
        /// </summary>
        public IEnumerable<PhoneType> PhoneTypes { get; set; }

        /// <summary>
        /// Updatable Phone types with descriptions
        /// </summary>
        public IEnumerable<PhoneType> UpdatablePhoneTypes
        {
            get
            {
                List<PhoneType> updatablePhoneTypes = new List<PhoneType>();
                foreach (var phoneType in PhoneTypes)
                {
                    bool phoneTypeIsUpdatable = Configuration.UpdatablePhoneTypes.Contains(phoneType.Code);
                    bool userCanUpdatePhone = UserHasPhoneUpdatePermission && phoneTypeIsUpdatable;
                    if (userCanUpdatePhone)
                    {
                        updatablePhoneTypes.Add(phoneType);
                    }
                }
                return updatablePhoneTypes;
            }
        }

        /// <summary>
        /// Email type and description associations
        /// </summary>
        public IEnumerable<EmailType> EmailTypes { get; set; }

        /// <summary>
        /// Updatable Email types with descriptions
        /// </summary>
        public IEnumerable<EmailType> UpdatableEmailTypes
        {
            get
            {
                List<EmailType> updatableEmailTypes = new List<EmailType>();
                foreach (var emailType in EmailTypes)
                {
                    bool emailTypeIsUpdatable = Configuration.AllEmailTypesAreUpdatable || Configuration.UpdatableEmailTypes.Contains(emailType.Code);
                    bool userCanUpdateEmail = UserHasEmailUpdatePermission && emailTypeIsUpdatable;
                    if (userCanUpdateEmail)
                    {
                        updatableEmailTypes.Add(emailType);
                    }
                }
                return updatableEmailTypes;
            }
        }

        /// <summary>
        /// Addresses this user may view
        /// </summary>
        public List<ProfileAddressModel> ViewableAddresses { get; set; }

        /// <summary>
        /// Email Addresses this user may view
        /// </summary>
        public List<ProfileEmailAddressModel> ViewableEmailAddresses { get; set; }

        /// <summary>
        /// Whether or not the user has update address permissions
        /// </summary>
        public bool UserHasAddressUpdatePermission { get; set; }

        /// <summary>
        /// Address type and description associations
        /// </summary>
        public IEnumerable<AddressType2> AddressTypes { get; set; }

        /// <summary>
        /// Updatable Address types with descriptions
        /// </summary>
        public IEnumerable<AddressType2> UpdatableAddressTypes
        {
            get
            {
                List<AddressType2> updatableAddressTypes = new List<AddressType2>();
                if (UserHasAddressUpdatePermission)
                {
                    foreach (var addressType in AddressTypes)
                    {
                        if (Configuration.UpdatableAddressTypes.Contains(addressType.Code))
                        {
                            updatableAddressTypes.Add(addressType);
                        }
                    }
                }
                return updatableAddressTypes;
            }
        }

        /// <summary>
        /// Phones this user may view
        /// </summary>
        public List<ProfilePhoneModel> ViewablePhones { get; set; }


        /// <summary>
        /// Personal pronoun type and description associations
        /// </summary>
       public IEnumerable<PersonalPronounType> PersonalPronounTypes { get; set; }

        /// <summary>
        /// Text displayed to the user
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Notification to be shown when the model is displayed
        /// </summary>
        public Notification Notification { get; set; }

        /// <summary>
        /// Whether or not the user has update email permissions
        /// </summary>
        public bool UserHasEmailUpdatePermission { get; set; }

        /// <summary>
        /// Whether or not the user has update phone permissions
        /// </summary>
        public bool UserHasPhoneUpdatePermission { get; set; }

        /// <summary>
        /// A list of countries to be used by the Address component
        /// </summary>
        public List<Country> Countries { get; set; }

        /// <summary>
        /// A list of states to be used by the Address component
        /// </summary>
        public List<State> States { get; set; }


        /// <summary>
        /// Creates a new instance of the UserProfileViewModel class, combining the profile and configuration objects
        /// </summary>
        /// <param name="profile">The Profile DTO for the user being modeled</param>
        /// <param name="configuration">The UserProfileConfiguration for the user</param>
        /// <param name="phoneTypes">List of phone types with descriptions</param>
        /// <param name="emailTypes">List of email types with descriptions</param>
        /// <param name="addressTypes">List of address types with descriptions</param>
        /// <param name="currentUser">Current user</param>
        /// <param name="roles">List of roles</param>
        /// <param name="countries">List of countries</param>
        /// <param name="states">List of states</param>
        /// <param name="personalPronounTypes">List of personal pronoun types with descriptions</param>
        public UserProfileViewModel(Ellucian.Colleague.Dtos.Base.Profile profile, UserProfileConfiguration2 configuration, IEnumerable<PhoneType> phoneTypes, IEnumerable<EmailType> emailTypes, IEnumerable<AddressType2> addressTypes, ICurrentUser currentUser, IEnumerable<Role> roles, IEnumerable<Country> countries, IEnumerable<State> states, IEnumerable<PersonalPronounType> personalPronounTypes)
        {
            if (profile == null)
            {
                throw new ArgumentNullException("profile", "Profile is required to create a UserProfileViewModel");
            }
            if (currentUser == null)
            {
                throw new ArgumentNullException("currentUser", "Current User is required to create a UserProfileViewModel");
            }
            if (roles == null)
            {
                throw new ArgumentNullException("roles", "Roles are required to create a UserProfileViewModel");
            }
            this.UserProfileModel = new UserProfileModel(profile);
            this.Configuration = configuration;
            this.EmailTypes = emailTypes;
            this.AddressTypes = addressTypes;
            this.PhoneTypes = phoneTypes;
            this.PersonalPronounTypes = personalPronounTypes;

            // Check whether Colleague is set to not require permissions, otherwise see if 
            // any role the user is a member of has the update permission
            var updateEmailPermissionCode = "UPDATE.OWN.EMAIL";
            var updatePhonePermissionCode = "UPDATE.OWN.PHONE";
            var updateAddressPermissionCode = "UPDATE.OWN.ADDRESS";
            IEnumerable<string> roleTitlesWithEmailUpdatePermissions =
                roles.Where(r => r.Permissions.Where(p => p.Code == updateEmailPermissionCode).Any())
                     .Select(r => r.Title);
            IEnumerable<string> roleTitlesWithPhoneUpdatePermissions =
                roles.Where(r => r.Permissions.Where(p => p.Code == updatePhonePermissionCode).Any())
                     .Select(r => r.Title);
            IEnumerable<string> roleTitlesWithAddressUpdatePermissions =
                roles.Where(r => r.Permissions.Where(p => p.Code == updateAddressPermissionCode).Any())
                     .Select(r => r.Title);
            this.UserHasEmailUpdatePermission = false;
            this.UserHasPhoneUpdatePermission = false;
            this.UserHasAddressUpdatePermission = false;
            if (this.Configuration != null)
            {
                this.UserHasEmailUpdatePermission = Configuration.CanUpdateEmailWithoutPermission || currentUser.Roles.Intersect(roleTitlesWithEmailUpdatePermissions).Any();
                this.UserHasPhoneUpdatePermission = Configuration.CanUpdatePhoneWithoutPermission || currentUser.Roles.Intersect(roleTitlesWithPhoneUpdatePermissions).Any();
                this.UserHasAddressUpdatePermission = Configuration.CanUpdateAddressWithoutPermission || currentUser.Roles.Intersect(roleTitlesWithAddressUpdatePermissions).Any();
            }

            this.ViewablePhones = UserProfileHelper.BuildProfilePhones(profile.Phones, phoneTypes, configuration, UserHasPhoneUpdatePermission);
            this.ViewableEmailAddresses = UserProfileHelper.BuildProfileEmails(profile.EmailAddresses, emailTypes, configuration, UserHasEmailUpdatePermission);
            this.ViewableAddresses = UserProfileHelper.BuildProfileAddresses(profile.Addresses, configuration, UserHasAddressUpdatePermission);

            this.Countries = countries.Where(ct => !ct.IsNotInUse).OrderBy(c => c.Description).ToList();
            this.States = states.OrderBy(c => c.Description).ToList();


            if (configuration != null && configuration.Text != null)
            {
                this.Text = configuration.Text.Replace("" + Environment.NewLine, "<br />");
            }
        }
    }
}