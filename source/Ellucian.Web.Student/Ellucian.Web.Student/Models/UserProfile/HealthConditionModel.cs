﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.UserProfile
{
    public class HealthConditionModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public bool isSelected { get; set; }
    }
}