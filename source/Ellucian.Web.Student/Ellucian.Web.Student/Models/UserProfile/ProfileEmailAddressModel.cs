﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Models.UserProfile
{
    public class ProfileEmailAddressModel
    {
        public EmailAddress EmailAddress { get; set; }
        public bool IsUpdatable { get; set; }
        public string EmailTypeDescription { get; set; }
    }
}
