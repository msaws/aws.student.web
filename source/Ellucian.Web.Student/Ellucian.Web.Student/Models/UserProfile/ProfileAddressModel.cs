﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Models.UserProfile
{
    public class ProfileAddressModel
    {
        public Address Address { get; set; }
        public bool IsUpdatable { get; set; }
        public string AddressTypeDescription { get; set; }
    }
}