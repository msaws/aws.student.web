﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models
{
    public class BasePermissionCodes
    {

        #region User Profile
        // Enables a person to update their email(s)
        public const string UpdateOwnEmail = "UPDATE.OWN.EMAIL";

        // Enables a person to update their phone(s)
        public const string UpdateOwnPhone = "UPDATE.OWN.PHONE";

        // Enables a person to update their address(es)
        public const string UpdateOwnAddress = "UPDATE.OWN.ADDRESS";
        #endregion

        #region Organizational Structure
        // Enables a person to update the organizational relationships (assign managers/subordinates)
        public const string UpdateOrganizationalRelationships = "UPDATE.ORGANIZATIONAL.RELATIONSHIPS";
        #endregion
    }
}