﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Web.Mvc;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Models
{
    public class GenderCollection : List<DataEntryOption>
    {
        public GenderCollection()
        {
            Add(new DataEntryOption() { Code = "M", Description = GlobalResources.GetString(GlobalResourceFiles.SiteResources, "MaleString")});
            Add(new DataEntryOption() { Code = "F", Description = GlobalResources.GetString(GlobalResourceFiles.SiteResources, "FemaleString")});
        }
    }
}