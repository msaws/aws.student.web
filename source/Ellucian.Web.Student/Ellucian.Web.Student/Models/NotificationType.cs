﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.

namespace Ellucian.Web.Student.Models
{
    public enum NotificationType
    {
        Information,
        Success,
        Warning,
        Error
    }
}