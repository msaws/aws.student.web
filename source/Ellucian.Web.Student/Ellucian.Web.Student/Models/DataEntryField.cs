﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models
{
    /// <summary>
    /// Generic model for a data entry field
    /// </summary>
    public class DataEntryField
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Functional description
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Placeholder text
        /// </summary>
        public string Placeholder { get; set; }

        /// <summary>
        /// Minimum number of characters for a valid entry
        /// </summary>
        public int? MinimumLength { get; set; }

        /// <summary>
        /// Maximum number of characters for a valid entry
        /// </summary>
        public int? MaximumLength { get; set; }

        /// <summary>
        /// Flag indicating whether or not entry is required
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Flag indicating visibility to users
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// The value entered in the field
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Type of data
        /// </summary>
        public DemographicFieldType Type { get; set; }

        /// <summary>
        /// Field input type for mark-up
        /// </summary>
        public string InputType { get; set; }

        /// <summary>
        /// Available options for dropdowns
        /// </summary>
        public List<Object> Options { get; set; }

        /// <summary>
        /// Constructor for a data entry field
        /// </summary>
        /// <param name="fieldDto">DemographicField object</param>
        /// <returns>A DataEntryField object</returns>
        public static DataEntryField Build(DemographicField fieldDto)
        {
            if (fieldDto == null)
            {
                throw new ArgumentNullException("fieldDto", "The supplied DemographicField object cannot be null.");
            }

            DataEntryField field = new DataEntryField()
            {
                Id = fieldDto.Code.ToLowerInvariant().Replace(" ", "-"),
                InputType = "text",
                IsRequired = fieldDto.Requirement == DemographicFieldRequirement.Required,
                IsVisible = fieldDto.Requirement != DemographicFieldRequirement.Hidden,
                Label = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "Proxy" + fieldDto.Type.ToString() + "Label"),
                MinimumLength = fieldDto.Requirement == DemographicFieldRequirement.Required ? 1 : (int?)null,
                Type = fieldDto.Type,
                Options = new List<Object>()
            };

            field.Placeholder = field.Label;

            switch (fieldDto.Type)
            {
                case DemographicFieldType.BirthDate:
                    field.MaximumLength = 13;
                    field.Placeholder = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                    break;
                case DemographicFieldType.EmailAddress:
                    field.MaximumLength = 50;
                    field.InputType = "email";
                    break;
                case DemographicFieldType.EmailType:
                case DemographicFieldType.PhoneType:
                case DemographicFieldType.PhoneExtension:
                    field.MaximumLength = 10;
                    break;
                case DemographicFieldType.FirstName:
                case DemographicFieldType.FormerFirstName:
                    field.MaximumLength = 15;
                    break;
                case DemographicFieldType.MiddleName:
                case DemographicFieldType.FormerMiddleName:
                    field.MaximumLength = 14;
                    break;
                case DemographicFieldType.LastName:
                case DemographicFieldType.FormerLastName:
                    field.MaximumLength = 25;
                    break;
                case DemographicFieldType.Gender:
                    field.MaximumLength = 1;
                    break;
                case DemographicFieldType.GovernmentId:
                    field.MaximumLength = 11;
                    break;
                case DemographicFieldType.Phone:
                    field.MaximumLength = 20;
                    break;
                case DemographicFieldType.Prefix:
                case DemographicFieldType.Suffix:
                    field.MaximumLength = 6;
                    break;
                default:
                    break;
            }

            return field;
        }

        /// <summary>
        /// Use data from profile config and the list of all email types to build a list of email types to present for selection.
        /// </summary>
        /// <param name="allEmailTypes"></param>
        /// <param name="profileConfig"></param>
        public void BuildEmailTypeOptions(IEnumerable<EmailType> allEmailTypes, UserProfileConfiguration profileConfig)
        {
            if (profileConfig.AllEmailTypesAreUpdatable)
            {
                this.Options.AddRange(allEmailTypes.ToList().ConvertAll(type => DataEntryOption.Build(type)));
            }
            else
            {
                if (profileConfig.UpdatableEmailTypes.Any())
                {
                    allEmailTypes = allEmailTypes.Where(et => profileConfig.UpdatableEmailTypes.Contains(et.Code));
                }
                if (allEmailTypes.Count() == profileConfig.UpdatableEmailTypes.Count())
                {
                    this.Options.AddRange(allEmailTypes.ToList().ConvertAll(type => DataEntryOption.Build(type)));
                }
                else
                {
                    this.IsVisible = false;
                    this.IsRequired = false;
                }
            }
        }
    }
}