﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Models
{
    public class StandardJsonResult
    {
        public object Model { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        public string Message { get; set; }

        public StandardJsonResult(object model=null, HttpStatusCode statuscode = HttpStatusCode.OK, string message = "")
        {
            Model = model;
            StatusCode = statuscode;
            Message = message;
        }
    }
}