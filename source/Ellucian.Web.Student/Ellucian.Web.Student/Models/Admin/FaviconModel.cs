﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Admin
{
    public class FaviconModel
    {
        public string Name { get; set; }
    }
}