﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Admin
{
    public class ApplicationCertificatesAndKeysModel
    {
        public List<ApplicationCertificateModel> CurrentCertificates { get; set; }
        public List<ApplicationCertificateModel> CurrentKeys { get; set; }
        public List<Models.Notification> Messages { get; set; }
    }
}