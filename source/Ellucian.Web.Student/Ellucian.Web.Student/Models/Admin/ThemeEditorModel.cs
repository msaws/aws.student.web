﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Mvc.Models;

namespace Ellucian.Web.Student.Models.Admin
{

    public class ThemeEditorModel
    {
        public Mvc.Models.Theme Theme;
        public List<Notification> Messages;

        public ThemeEditorModel()
        {
            Theme = new Mvc.Models.Theme();
            Messages = new List<Notification>();
        }
    }



}