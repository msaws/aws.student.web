﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Admin
{
    /// <summary>
    /// Branding base model
    /// </summary>
    public class BrandingModel : Settings
    {
        /// <summary>
        /// Collection of messages pertaining to branding
        /// </summary>
        public List<Models.Notification> Messages { get; set; }

        /// <summary>
        /// Custom Favicon
        /// </summary>
        public FaviconModel Favicon { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="BrandingModel"/> class.
        /// </summary>
        /// <param name="settings">A <see cref="Settings"/> object</param>
        public BrandingModel(Settings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            EnableGoogleAnalytics = settings.EnableGoogleAnalytics;
            EnableCustomTracking = settings.EnableCustomTracking;
            InsertCustomTrackingAfterBody = settings.InsertCustomTrackingAfterBody;
            CustomTrackingAnalytics = settings.CustomTrackingAnalytics;
            ClientPrivacyPolicy = settings.ClientPrivacyPolicy;
            ApiBaseUrl = settings.ApiBaseUrl;
            ApiConnectionLimit = settings.ApiConnectionLimit;
            ApplicationTitle = settings.ApplicationTitle;
            TrackingId = settings.TrackingId;
            ProxyDimensionIndex = settings.ProxyDimensionIndex;
            Culture = settings.Culture;
            CulturePropertyOverrides = settings.CulturePropertyOverrides;
            FooterLogoAltText = settings.FooterLogoAltText;
            FooterLogoPath = settings.FooterLogoPath;
            HeaderLogoAltText = settings.HeaderLogoAltText;
            HeaderLogoPath = settings.HeaderLogoPath;
            LogLevels = settings.LogLevels;
            ReportLogoPath = settings.ReportLogoPath;
            SelectedLogLevel = settings.SelectedLogLevel;
            ShortDateFormat = settings.ShortDateFormat;
            UniqueCookieId = settings.UniqueCookieId;
            Messages = new List<Notification>();
        }
    }
}