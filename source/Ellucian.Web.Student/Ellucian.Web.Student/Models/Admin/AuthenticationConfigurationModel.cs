﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;

namespace Ellucian.Web.Student.Models.Admin
{
    public class AuthenticationConfigurationModel
    {
        public string AuthenticationType {get; set;}

        public string IdentityProviderUrl { get; set; }
        public string IdentityProviderLogoutUrl { get; set; }
        public bool UseEncryptedAssertions { get; set; }

        public string IdentityProviderIssuer { get; set; }

        public int? SamlClockSkew { get; set; }

        public string UsernameClaim { get; set; }
        public string ServiceProviderId { get; set; }

        public string IdentityProviderPublicKey { get; set; }
        public string IdentityProviderPublicKeyPassword { get; set; }

        public string BaseUrl { get; set; }
        public string ServiceProviderPrivateKey { get; set; }
        public string ServiceProviderPrivateKeyPassword { get; set; }

        public List<string> AvailablePublicKeys { get; set; }
        public List<string> AvailablePrivateKeys { get; set; }

        public List<ApplicationCertificateModel> CurrentCertificates { get; set; }
        public List<ApplicationCertificateModel> CurrentKeys { get; set; }

        public string ProxyUsername { get; set; }
        public string ProxyPassword { get; set; }

        public string GuestUsername { get; set; }
        public string GuestPassword { get; set; }

        public bool SetNameIdPolicy { get; set; }
        public string SPNameQualifier { get; set; }

        public List<Notification> Messages { get; set; }

        public AuthenticationConfigurationModel()
        {
            AvailablePrivateKeys = new List<string>();
            AvailablePublicKeys = new List<string>();
            Messages = new List<Notification>();
        }
    }

}