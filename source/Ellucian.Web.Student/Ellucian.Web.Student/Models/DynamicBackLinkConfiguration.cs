﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Ellucian.Web.Student.Models
{
    public class DynamicBackLinkConfiguration
    {
        /// <summary>
        /// Whether or not to show a default back link if there is no referrer.
        /// </summary>
        public bool ShowDefaultIfNoReferrer { get; private set; }

        /// <summary>
        /// RouteValueDictionary with values for the default route when no referrer is present.
        /// </summary>
        public RouteValueDictionary DefaultRouteValues { get; private set; }

        /// <summary>
        /// Format string for the back link. Will be formatted with a page label as the single argument.
        /// </summary>
        public string BackLinkStringFormat { get; set; }

        /// <summary>
        /// Default text to use for pages no in the sitemap
        /// </summary>
        public string DefaultPageLabel { get; set; }

        /// <summary>
        /// List of the names of controllers that should not be valid back link targets.
        /// </summary>
        public List<string> DisallowedControllers { get; set; }

        /// <summary>
        /// List of RouteValueDictionary with values that should not be valid back link targets.
        /// </summary>
        public List<RouteValueDictionary> DisallowedActions { get; set; }

        /// <summary>
        /// Creates a DynamicBackLinkConfiguration object 
        /// </summary>
        /// <param name="showDefaultIfNoReferrer">Whether or not to show a default back link if no referrer is found.</param>
        /// <param name="defaultRouteValues">RouteValueDictionary with values for the default route. Must be given if showDefaultIfNoReferrer is true.</param>
        /// <param name="defaultStringFormat">The format string used to create the link text for the back link.</param>
        /// <param name="defaultPageLabel">The default page label for pages not in the site map.</param>
        public DynamicBackLinkConfiguration(bool showDefaultIfNoReferrer = false, RouteValueDictionary defaultRouteValues = null,
            string defaultStringFormat = null, string defaultPageLabel = null)
        {
            if (showDefaultIfNoReferrer && defaultRouteValues == null)
            {
                throw new ArgumentNullException("defaultRouteValues", "defaultRouteValues is required if showDefaultIfNoReferrer is true");
            }

            if (defaultStringFormat == null)
            {
                BackLinkStringFormat = GlobalResources.GetString("Navigation", "BackLinkFormat");
            }
            else
            {
                BackLinkStringFormat = defaultStringFormat;
            }

            if (defaultPageLabel == null)
            {
                DefaultPageLabel = GlobalResources.GetString("Navigation", "PreviousPage");
            }
            else
            {
                DefaultPageLabel = defaultPageLabel;
            }

            ShowDefaultIfNoReferrer = showDefaultIfNoReferrer;
            DefaultRouteValues = defaultRouteValues;

            DisallowedControllers = new List<string>();
            DisallowedActions = new List<RouteValueDictionary>();

            // Disallow the Account controller so that redirects from the Login page to not link back to the login.
            DisallowedControllers.Add("Account");
        }

        public void SetDefaultRoute(bool showDefaultIfNoReferrer, RouteValueDictionary defaultRouteValues)
        {
            if (showDefaultIfNoReferrer && defaultRouteValues == null)
            {
                throw new ArgumentNullException("defaultRouteValues", "defaultRouteValues is required if showDefaultIfNoReferrer is true");
            }

            ShowDefaultIfNoReferrer = showDefaultIfNoReferrer;
            DefaultRouteValues = defaultRouteValues;
        }
    }
}