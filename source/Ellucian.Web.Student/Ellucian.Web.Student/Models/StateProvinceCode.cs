﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;

namespace Ellucian.Web.Student.Models
{
    /// <summary>
    /// A code identifying a state or province. These codes are intended to be used for standard dropdowns on data entry forms.
    /// </summary>
    public class StateProvinceCode
    {
        /// <summary>
        /// Code identifying the state/province
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Description/name of the state/province
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ISO 2-character country code
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="code">State/province code</param>
        /// <param name="description">State/province name</param>
        /// <param name="country">Country code</param>
        public StateProvinceCode(string code, string description, string country)
        {
            if (String.IsNullOrEmpty(code))
            {
                throw new ArgumentNullException("code", "A state or province code is required.");
            }
            if (String.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException("description", "A description of the state/province is required.");
            }

            Code = code;
            Description = description;
            Country = string.IsNullOrEmpty(country) ? "US" : country;
        }
    }
}