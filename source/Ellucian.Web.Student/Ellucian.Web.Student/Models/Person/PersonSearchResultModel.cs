﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Ellucian.Web.Student.Utility;
using System;

namespace Ellucian.Web.Student.Models.Person
{
    /// <summary>
    /// Model for a person search result
    /// </summary>
    public class PersonSearchResultModel : PrivacyRestrictionModel
    {
        private string _name;
        private string _id;

        /// <summary>
        /// Name of the person
        /// </summary>
        public string Name { get { return _name; } }

        /// <summary>
        /// Unique identifier for the person
        /// </summary>
        public string Id { get { return _id; } }

        /// <summary>
        /// Alternate text for a photo of the person
        /// </summary>
        public string PhotoAltText { get { return String.Format(GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchResultAltText"), _name); } }

        /// <summary>
        /// Alternate text for a photo of the person
        /// </summary>
        public string OffscreenText { get { return String.Format(GlobalResources.GetString(GlobalResourceFiles.PersonResources, "PersonSearchResultOffscreenText"), _name); } }

        /// <summary>
        /// Heading for mobile search results card
        /// </summary>
        public string MobileHeader
        {
            get
            {
                if (!string.IsNullOrEmpty(PrivacyMessage))
                {
                    return Name + Environment.NewLine + PrivacyMessage;
                }
                return Name;
            }
        }

        /// <summary>
        /// Creates a new instance of the <see cref="PersonSearchResultModel"/> class.
        /// </summary>
        /// <param name="name">Name of the person</param>
        /// <param name="id">Unique identifier for the person</param>
        /// <param name="privacyStatusCode">Privacy status code for the person, if applicable</param>
        public PersonSearchResultModel(string name, string id, string privacyStatusCode = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name", "A person search result must have a name.");
            }
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "A person search result must have a unique identifier.");
            }
            _name = name;
            _id = id;
            PrivacyStatusCode = privacyStatusCode;
        }
    }
}