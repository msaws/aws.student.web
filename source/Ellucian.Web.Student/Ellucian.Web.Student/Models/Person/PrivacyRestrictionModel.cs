﻿/*Copyright 2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.Person
{
    public class PrivacyRestrictionModel
    {
        /// <summary>
        /// Privacy status code for the model
        /// </summary>
        public string PrivacyStatusCode { get; set; }

        /// <summary>
        /// Flag indicating whether or not the model has a privacy restriction
        /// </summary>
        public bool HasPrivacyRestriction { get; set; }

        /// <summary>
        /// Privacy message for model with privacy restriction
        /// </summary>
        public string PrivacyMessage { get; set; }
    }
}