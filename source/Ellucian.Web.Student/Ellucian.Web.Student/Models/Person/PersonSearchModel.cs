﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.Search;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Models.Person
{
    /// <summary>
    /// Model for looking up account holders
    /// </summary>
    public class PersonSearchModel : SearchModel
    {
        /// <summary>
        /// Creates a new instance of the <see cref="SearchModel"/> class.
        /// </summary>
        /// <param name="searchPrompt">Prompt to the user of what they will search for</param>
        /// <param name="beforeSearchText">Text displayed before a search has been run</param>
        /// <param name="searchResultsViewName">Name of an ASP.NET MVC Razor Partial view for rendering search results</param>
        /// <param name="dynamicBackLinkDefault">A <see cref="SearchActionModel"/> for a fallback dynamic back link location</param>
        /// <param name="backLinkExclusions">A collection of <see cref="SearchActionModel"/> for forbidden back link locations</param>
        public PersonSearchModel(string searchPrompt, string beforeSearchText, string searchResultsViewName, SearchActionModel dynamicBackLinkDefault = null, IEnumerable<SearchActionModel> backLinkExclusions = null)
            :base(searchPrompt, beforeSearchText, searchResultsViewName, dynamicBackLinkDefault, backLinkExclusions)
        {
        }
    }
}