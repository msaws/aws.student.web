﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;
using System;
using System.Text;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Models
{
    /// <summary>
    /// The page header for a Colleague Self-Service page
    /// </summary>
    public class PageHeaderModel
    {
        private string _title;
        private string _subtitle;
        private string _staticBacklinkDestination;
        private string _staticBacklinkText;
        private DynamicBackLinkConfiguration _dynamicBackLinkConfiguration;

        /// <summary>
        /// Page Title
        /// </summary>
        public string Title { get { return _title; } }

        /// <summary>
        /// Page Subtitle
        /// </summary>
        public string Subtitle { get { return _subtitle; } }

        /// <summary>
        /// Page to which back link directs the user when clicked
        /// </summary>
        public string StaticBacklinkDestination { get { return _staticBacklinkDestination; } }

        /// <summary>
        /// Page back link text
        /// </summary>
        /// <remarks>If a <see cref="StaticBacklinkDestination"/> is provided, 
        /// defaults to "Return to previous page" if a value is not provided.</remarks>
        public string StaticBacklinkText 
        { 
            get 
            { 
                if (string.IsNullOrEmpty(_staticBacklinkDestination)) 
                { 
                    return null; 
                } else {
                    if (!string.IsNullOrEmpty(_staticBacklinkText))
                    {
                        return _staticBacklinkText;
                    }
                    else {
                        return GlobalResources.GetString(GlobalResourceFiles.SiteResources, "BackLinkText");
                    }
                }
            } 
        }

        /// <summary>
        /// Dynamic back link configuration
        /// </summary>
        public DynamicBackLinkConfiguration DynamicBackLinkConfiguration { get { return _dynamicBackLinkConfiguration; } }

        /// <summary>
        /// Creates an instance of the <see cref="PageHeaderModel"/> class
        /// </summary>
        /// <param name="title">Page title</param>
        /// <param name="subtitle">Page subtitle</param>
        /// <param name="staticBacklinkDestination">Page to which back link directs the user when clicked</param>
        /// <param name="staticBacklinkText">Page backlink text</param>
        /// <param name="dynamicBackLinkConfiguration">A <see cref="DynamicBackLinkConfiguration"/> object.</param>
        public PageHeaderModel(string title, string subtitle, string staticBacklinkDestination, string staticBacklinkText,
            DynamicBackLinkConfiguration dynamicBackLinkConfiguration = null)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException("Cannot build a page header without a page title.");
            }
            if (!string.IsNullOrEmpty(subtitle) && (!string.IsNullOrEmpty(staticBacklinkDestination) || !string.IsNullOrEmpty(staticBacklinkText) || dynamicBackLinkConfiguration != null))
            {
                throw new ArgumentException("Page header cannot contain a subtitle and a back link.");
            }            
            if (dynamicBackLinkConfiguration != null && (!string.IsNullOrEmpty(staticBacklinkDestination) || !string.IsNullOrEmpty(staticBacklinkText)))
            {
                throw new ArgumentException("Page header cannot have both static and dynamic back links.");
            }

            _title = title;
            _subtitle = subtitle;
            _staticBacklinkDestination = staticBacklinkDestination;
            _staticBacklinkText = staticBacklinkText;
            _dynamicBackLinkConfiguration = dynamicBackLinkConfiguration;
        }
    }
}