﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models
{
    /// <summary>
    /// Model for the User-defined preferences (currently just home/start page; will expand to favorites, etc.)
    /// </summary>
    [JsonObject]
    public class PreferencesModel
    {
        /// <summary>
        /// The page ID for the user's chosen home/start page.
        /// </summary>
        [JsonProperty]
        public string Homepage { get; set; }

        /// <summary>
        /// Default constructor for the Preferences Model.
        /// </summary>
        [JsonConstructor]
        public PreferencesModel()
        {
            Homepage = string.Empty;
        }

        /// <summary>
        /// Constructor for PreferencesModel using dynamic dictionary
        /// </summary>
        /// <param name="preferences">Preferences dictionary with property value pairs</param>
        public PreferencesModel(IDictionary<string, dynamic> preferences)
        {
            dynamic homepage;
            preferences.TryGetValue("Homepage", out homepage);
            Homepage = homepage as string;
        }
    }
}