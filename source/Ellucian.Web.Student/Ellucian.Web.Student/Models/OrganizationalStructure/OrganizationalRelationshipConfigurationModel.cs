﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Models.OrganizationalStructure
{

    /// <summary>
    /// Organizational Relationship Configuration
    /// </summary>
    public class OrganizationalRelationshipConfigurationModel
    {
        /// <summary>
        /// The type code(s) representing a manager relationship
        /// </summary>
        public List<string> ManagerRelationshipTypes { get; set; }

        /// <summary>
        /// Whether or not the user can update organizational relationships
        /// </summary>
        public bool UserCanUpdateOrganizationalRelationships { get; set; }

        /// <summary>
        /// Organizational Relationship Configuration Constructor
        /// </summary>
        public OrganizationalRelationshipConfigurationModel()
        {
            ManagerRelationshipTypes = new List<string>();

        }
        /// <summary>
        /// Organizational Relationship Configuration Constructor accepting configuration
        /// </summary>
        /// <param name="orgRelConfiguration">Organizational Relationship Configuration</param>
        /// <param name="roles">List of roles</param>
        /// <param name="currentUser">The current user</param>
        public OrganizationalRelationshipConfigurationModel(OrganizationalRelationshipConfiguration orgRelConfiguration, IEnumerable<Role> roles, ICurrentUser currentUser)
        {
            if (orgRelConfiguration != null && orgRelConfiguration.RelationshipTypeCodeMapping.ContainsKey(OrganizationalRelationshipType.Manager))
            {
                var managerRelationTypeKeyValues = orgRelConfiguration.RelationshipTypeCodeMapping.FirstOrDefault(t => t.Key == OrganizationalRelationshipType.Manager);
                ManagerRelationshipTypes = managerRelationTypeKeyValues.Value;
            }
            else
            {
                ManagerRelationshipTypes = new List<string>();
            }


            var updateOrganizationalRelationshipsPermissionCode = BasePermissionCodes.UpdateOrganizationalRelationships;
            IEnumerable<string> roleTitlesWithOrganizationalRelationshipUpdatePermissions =
                roles.Where(r => r.Permissions.Where(p => p.Code == updateOrganizationalRelationshipsPermissionCode).Any())
                     .Select(r => r.Title);
            this.UserCanUpdateOrganizationalRelationships = false;
            this.UserCanUpdateOrganizationalRelationships = currentUser.Roles.Intersect(roleTitlesWithOrganizationalRelationshipUpdatePermissions).Any();

        }
    }
}