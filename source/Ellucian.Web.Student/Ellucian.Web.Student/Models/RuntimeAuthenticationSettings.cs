﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Web.Mvc;
using Hedtech.Identity;
namespace Ellucian.Web.Student.Models
{
    /// <summary>
    /// Provides runtime configuration settings related to authentication
    /// </summary>
    public class RuntimeAuthenticationSettings
    {
        /// <summary>
        /// Gets a value indicating if the change password link should be displayed.
        /// </summary>
        public bool DisplayChangePasswordLink { get; private set; }

        /// <summary>
        /// Gets a value indicating if the login link should be displayed.
        /// </summary>
        public bool DisplayLoginLink { get; private set; }

        /// <summary>
        /// Gets a value indicating if the logoff link should be displayed.
        /// </summary>
        public bool DisplayLogOffLink { get; private set; }

        /// <summary>
        /// Gets a value indicating if SAML2 is enabled.
        /// </summary>
        public bool Saml2Enabled { get; private set; }

        /// <summary>
        /// Gets a value indicating if the AJAX account login form can be used.
        /// </summary>
        public bool DisplayAjaxAccountForm { get; private set; }
        
        /// <summary>
        /// Creates a default <see cref="RuntimeAuthenticationSettings"/>
        /// </summary>
        public RuntimeAuthenticationSettings()
            :this(false,false)
        {
        }

        /// <summary>
        /// Creates a <see cref="RuntimeAuthenticationSettings"/> using existing information.
        /// </summary>
        /// <param name="ViewBag">Current page ViewBag</param>
        /// <param name="dependencyResolver">Current dependency resolver</param>
        public RuntimeAuthenticationSettings(dynamic ViewBag, IDependencyResolver dependencyResolver)
        {
            if (ViewBag != null && dependencyResolver != null)
            {
                bool fromPortal = ViewBag.PortalBehaviorEnabled == null ? false : ViewBag.PortalBehaviorEnabled;
                IdentityManagementSettings idmSettings = dependencyResolver.GetService<IdentityManagementSettings>();
                bool saml2Enabled = idmSettings != null ? idmSettings.IsIdmEnabled : false;
                Initialize(fromPortal, saml2Enabled);
            }
        }

        /// <summary>
        /// Creates a <see cref="RuntimeAuthenticationSettings"/> using raw settings.
        /// </summary>
        /// <param name="fromPortal">Indicate if this session was initiated from portal</param>
        /// <param name="allowSaml2">Indicate if this session supports SAML2</param>
        public RuntimeAuthenticationSettings(bool fromPortal, bool allowSaml2)
        {
            Initialize(fromPortal, allowSaml2);
        }

        /// <summary>
        /// Set all of the properties.
        /// </summary>
        /// <param name="fromPortal">Indicate if this session was initiated from portal</param>
        /// <param name="allowSaml2">Indicate if this session supports SAML2</param>
        private void Initialize(bool fromPortal, bool allowSaml2)
        {
            this.DisplayChangePasswordLink = true;
            this.DisplayLoginLink = true;
            this.DisplayLogOffLink = true;
            this.DisplayAjaxAccountForm = true;
            

            // portal = turn everything off
            if (fromPortal)
            {
                this.DisplayChangePasswordLink = false;
                this.DisplayLoginLink = false;
                this.DisplayLogOffLink = false;
                this.DisplayAjaxAccountForm = false;
            }

            // SAML2 = turn off change password and AJAX login
            this.Saml2Enabled = allowSaml2;
            if (allowSaml2)
            {
                this.DisplayChangePasswordLink = false;
                this.DisplayLoginLink = true;
                this.DisplayLogOffLink = true;
                this.DisplayAjaxAccountForm = false;
            }
        }


    }
}