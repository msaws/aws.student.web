﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using System.Globalization;

namespace Ellucian.Web.Student.Models.TaxInformation
{

    /// <summary>
    /// class represents a tax form consent
    /// </summary>
    public class TaxFormConsentsViewModel
    {
        /// <summary>
        /// consent form date formatted for display to user
        /// </summary>
        public string DisplayDate;

        /// <summary>
        /// consent form time formatted for display to user
        /// </summary>
        public string DisplayTime;

        /// <summary>
        /// true or false to indicate whether person has consented
        /// </summary>
        public bool HasConsented;
        
        /// <summary>
        /// this creates a new tax form consent view model
        /// </summary>
        /// <param name="taxFormConsentDto">tax form consent DTO </param>
        public TaxFormConsentsViewModel(TaxFormConsent taxFormConsentDto)
        {
            HasConsented = taxFormConsentDto.HasConsented;

            //separated date and time to display in different columns in SS for easier alignment of text
            DisplayDate = taxFormConsentDto.TimeStamp.LocalDateTime.ToShortDateString();
            DisplayTime = taxFormConsentDto.TimeStamp.LocalDateTime.ToLongTimeString();
        }
    }
}