﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;

namespace Ellucian.Web.Student.Models.TaxInformation
{
    /// <summary>
    /// this class represents a list of tax form consents
    /// </summary>
    public class TaxInformationViewModel
    {
        /// <summary>
        /// list of tax form consents for the user
        /// </summary>
        public List<TaxFormConsentsViewModel> Consents { get; set; }

        /// <summary>
        /// Set of tax form Statements for the user
        /// </summary>
        public List<TaxFormStatementsViewModel> Statements { get; set; }

        /// <summary>
        /// Consent text paragraph for tax forms
        /// </summary>
        public string ConsentGivenText { get; set; }

        /// <summary>
        /// Withhold consent text paragraph for tax forms.
        /// </summary>
        public string ConsentWithheldText { get; set; }

        /// <summary>
        /// Tax Form ID.
        /// </summary>
        public TaxForms TaxFormId { get; set; }

        /// <summary>
        /// W2 Form boolean.
        /// </summary>
        public bool IsW2 { get { return TaxFormId == TaxForms.FormW2; } }

        /// <summary>
        /// 1095C Form boolean.
        /// </summary>
        public bool Is1095C { get { return TaxFormId == TaxForms.Form1095C; } }

        /// <summary>
        /// 1098 Form boolean.
        /// </summary>
        public bool Is1098 { get { return TaxFormId == TaxForms.Form1098; } }

        /// <summary>
        /// Gets tax form information 
        /// </summary>
        /// <param name="taxFormConsents">Tax form consent DTOs</param>
        public TaxInformationViewModel(IEnumerable<TaxFormConsent> taxFormConsents, IEnumerable<TaxFormStatement2> statements, TaxFormConfiguration taxFormConfiguration, TaxForms taxFormId)
        {
            TaxFormId = taxFormId;

            // Populate tax form Consent VMs
            Consents = taxFormConsents.Where(t => t.TaxForm == taxFormId).OrderByDescending(t => t.TimeStamp)
                .Select(t => new TaxFormConsentsViewModel(t)).ToList();
            
            // Populate tax form Statements VMs
            Statements = statements.Where(t => t.TaxForm == taxFormId).OrderByDescending(t => t.TaxYear)
                .Select(t => new TaxFormStatementsViewModel(t)).ToList();

            ConsentGivenText = taxFormConfiguration.ConsentText;
            ConsentWithheldText = taxFormConfiguration.ConsentWithheldText;
        }
    }
}