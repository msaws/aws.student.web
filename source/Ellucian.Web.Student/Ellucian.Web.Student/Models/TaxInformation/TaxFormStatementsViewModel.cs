﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Models.TaxInformation
{
    /// <summary>
    /// This class represents a W2 tax form statement
    /// </summary>
    public class TaxFormStatementsViewModel
    {
        /// <summary>
        /// ID of the person to whom this tax form is assigned
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// Tax Year for the W2 statement 
        /// </summary>
        public string TaxYear { get; set; }

        /// <summary>
        /// Type of tax form
        /// </summary>
        public TaxForms TaxForm { get; set; }

        /// <summary>
        /// Notation text to display
        /// </summary>
        public string NotationDisplay { get; set; }

        /// <summary>
        /// Tax form enumeration indicating correction, multiple forms, etc.
        /// </summary>
        public TaxFormNotations2 Notation { get; set; }

        /// <summary>
        /// ID of the record that contains the PDF data.
        /// </summary>
        public string PdfRecordId { get; set; }

        /// <summary>
        /// Boolean that indicates whether the PDF data is linkable.
        /// </summary>
        public bool IsLinkable { get { return Notation != TaxFormNotations2.NotAvailable && Notation != TaxFormNotations2.Correction; } }

        /// <summary>
        /// Creates a new W2 tax form statement 
        /// </summary>
        /// <param name="taxFormConsents">Tax form consent DTOs</param>
        public TaxFormStatementsViewModel(TaxFormStatement2 statementDto)
        {
            this.PersonId = statementDto.PersonId;
            this.TaxYear = statementDto.TaxYear;
            this.TaxForm = statementDto.TaxForm;
            this.PdfRecordId = statementDto.PdfRecordId;
            this.Notation = statementDto.Notation;
            if (this.Notation == TaxFormNotations2.NotAvailable)
            {
                switch (this.TaxForm)
                {
                    case TaxForms.FormW2:
                        this.NotationDisplay = GlobalResources.GetString("TaxForms", "W2NotAvailableText");
                        break;
                    case TaxForms.Form1095C:
                        this.NotationDisplay = GlobalResources.GetString("TaxForms", "Form1095CNotAvailableText");
                        break;
                    case TaxForms.Form1098:
                        this.NotationDisplay = GlobalResources.GetString("TaxForms", "Form1098NotAvailableText");
                        break;
                }
            }
            else if (this.Notation == TaxFormNotations2.Correction)
            {
                switch (this.TaxForm)
                {
                    case TaxForms.FormW2:
                        this.NotationDisplay = GlobalResources.GetString("TaxForms", "W2CorrectionText");
                        break;
                    case TaxForms.Form1095C:
                        this.NotationDisplay = GlobalResources.GetString("TaxForms", "Form1095CCorrectionText");
                        break;
                    case TaxForms.Form1098:
                        this.NotationDisplay = GlobalResources.GetString("TaxForms", "Form1098CorrectionText");
                        break;
                }
            }
            else if (this.Notation == TaxFormNotations2.IsOverflow || this.Notation == TaxFormNotations2.HasOverflowData || this.Notation == TaxFormNotations2.MultipleForms)
            {
                this.NotationDisplay = GlobalResources.GetString("TaxForms", "W2MultipleFormsText");
            }
            else
            {
                string taxFormLabel = string.Empty;
                switch(this.TaxForm)
                {
                    case TaxForms.FormW2:
                        this.NotationDisplay = statementDto.TaxYear + " "
                            + GlobalResources.GetString("TaxForms", "FormW2Label") + " "
                            + GlobalResources.GetString("TaxForms", "StatementText");
                        break;
                    case TaxForms.Form1095C:
                        this.NotationDisplay = statementDto.TaxYear + " "
                            + GlobalResources.GetString("TaxForms", "Form1095CLabel") + " "
                            + GlobalResources.GetString("TaxForms", "StatementText");
                        break;
                    case TaxForms.Form1098:
                        this.NotationDisplay = statementDto.TaxYear + " "
                            + GlobalResources.GetString("TaxForms", "Form1098Label") + " "
                            + GlobalResources.GetString("TaxForms", "StatementText");
                        break;
                }
            }
        }
    }
}