﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;

namespace Ellucian.Web.Student.Models
{
    public class Notification
    {
        public string Message { get; set; }
        public string Type { get; set; }
        public bool Flash { get; set; }

        public Notification()
            :this(string.Empty, NotificationType.Information, false)
        {
        }

        public Notification(string message, NotificationType type)
            : this(message, type, false)
        {
        }

        public Notification(string message, NotificationType type, bool flash)
        {
            Message = message;
            Type = Enum.GetName(type.GetType(), type);
            Flash = flash;
        }
    }
}