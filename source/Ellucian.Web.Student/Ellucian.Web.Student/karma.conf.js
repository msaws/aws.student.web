/* Copyright 2016-2017 Ellucian Company L.P. and its affiliates. */
// Karma configuration
// Generated on Mon Aug 29 2016 20:21:50 GMT-0400 (Eastern Daylight Time)

var path = require("path"),
    webpack = require("webpack"),
    webpackConfigFactory = require("./webpack.conf.js");




module.exports = function (config) {

    var testType = config.testType || "test-cold";
    let webpackConfig = webpackConfigFactory(testType, function () { return {} }, "./");

    //instrumenter to get coverage for webpack modules
    webpackConfig.module.rules.push({
        test: /\.js$/,
        enforce: 'pre',
        include: [
            path.resolve("./Areas/HumanResources/Scripts/BankingInformation"),
            path.resolve("./Areas/TimeManagement/Scripts"),
            path.resolve("./Scripts/Components"),
            path.resolve("./Scripts/resource.manager")
        ],
        loader: 'istanbul-instrumenter-loader'
    });

    webpackConfig.devtool = 'eval-source-map';

    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [          
          'Scripts/jquery-*.js',
          'Scripts/jquery.ui.plugin_notification-center.js',
          'Scripts/knockout*.js/',
          'Scripts/globalize.js',
          'Scripts/global.js',
          'Scripts/Polyfills/*.js',
          'Scripts/moment.js',
          'Scripts/base.view.model.js',
          'Scripts/storage.js',

          //'../Ellucian.Web.Student.Tests/Areas/TimeManagement/Scripts/*.tests.js',
          //'../Ellucian.Web.Student.Tests/Scripts/*.component.tests.js'

          '../Ellucian.Web.Student.Tests/Areas/HumanResources/BankingInformationScripts/banking.information.test.bundle.js',
          '../Ellucian.Web.Student.Tests/Areas/TimeManagement/Scripts/time.management.test.bundle.js',
          '../Ellucian.Web.Student.Tests/Scripts/test.bundle.js'
        ],


        // list of files to exclude
        exclude: [
          '**/*min.js'
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            '../Ellucian.Web.Student.Tests/Areas/TimeManagement/Scripts/time.management.test.bundle.js': ['webpack', 'sourcemap'], //preprocess the test files by packing them up and extracting sourcemaps (karma-webpack)
            '../Ellucian.Web.Student.Tests/Scripts/test.bundle.js': ['webpack', 'sourcemap'],
            '../Ellucian.Web.Student.Tests/Areas/HumanResources/BankingInformationScripts/banking.information.test.bundle.js' : ['webpack', 'sourcemap']
            //'../Ellucian.Web.Student.Tests/Areas/TimeManagement/Scripts/*.tests.js': ['webpack', 'sourcemap'],
            //'../Ellucian.Web.Student.Tests/Scripts/*.tests.js' : ['webpack', 'sourcemap']
            
        },

        webpack: webpackConfig,

        webpackMiddleware: {
            noInfo: true           
        },

        // test results reporter to use
        //by default, use progress reporter (outputs to console) and 
        //coverage reporter - uses coverageReporter configurtion object (karma-coverage)
        //  coverage reporter also uses a preprocessor to instrument the source code
        reporters: ['progress', 'coverage'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        //singleRun and autoWatch both set to false so that the gulp tasks can
        //specify the run mode
        singleRun: false,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity,

        //coverage reporter
        coverageReporter: {
            type: 'html',
            dir: '../Ellucian.Web.Student.Tests/Coverage',
            subdir: '.'
        }

    })

    //console.log(config);
    
}
