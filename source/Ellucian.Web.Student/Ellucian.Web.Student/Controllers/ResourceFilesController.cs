﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Resource;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.ResourceFileEditor;
using Ellucian.Web.Student.Utility;
using slf4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Controllers
{
    /// <summary>
    /// Controller to manipulate resource files.
    /// Get resource contents, modify and save the history of modifications to the resource files.
    /// </summary>
    /// <seealso cref="Ellucian.Web.Mvc.Controller.BaseCompressedController" />
    [OutputCache(CacheProfile = "NoCache")]
    public class ResourceFilesController : BaseStudentController
    {
        private IResourceRepository resourceRepository;
        private ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceFilesController" /> class.
        /// </summary>
        /// <param name="resourceRepository">The resource repository.</param>
        /// <param name="settings">The settings object</param>
        /// <param name="logger">The logger.</param>
        /// <exception cref="System.ArgumentNullException">resourceRepository</exception>
        public ResourceFilesController(IResourceRepository resourceRepository, Settings settings, ILogger logger)
            : base(settings, logger)
        {
            if (resourceRepository == null)
            {
                throw new ArgumentNullException("resourceRepository");
            }
            this.resourceRepository = resourceRepository;
            this.logger = logger;
        }

        /// <summary>
        /// Returns the Resource File Editor
        /// </summary>
        /// <returns>Resource File Editor</returns>
        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult ResourceFileEditor()
        {
            ViewBag.Title = GlobalResources.GetString(GlobalResourceFiles.SiteResources, "ResourceFileEditorTitle");
            return View();
        }

        /// <summary>
        /// Gets the list of resource files found in the working directory
        /// </summary>
        /// <returns>JSON containing the resource files and their paths</returns>
        public ActionResult GetResourceFiles()
        {
            List<string> listResFilesFound = resourceRepository.GetResourceFilePaths("App_GlobalResources", true);

            List<KeyValuePair<string, string>> resFileNamePaths = listResFilesFound.Select(x => new KeyValuePair<string, string>(System.IO.Path.GetFileName(x), x)).OrderBy(resx => resx.Key).ToList();

            return Json(resFileNamePaths, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Gets the resource items of the resource file in the provided file path
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>Gets the resource items in the file as a JSonResult object </returns>
        public ActionResult GetResourceItemsByFile(string filePath)
        {

            try
            {
                ResourceFile currentFile = resourceRepository.GetResourceFile(filePath);
                List<ResourceFileEntry> listResourceItems = currentFile.ResourceFileEntries;

                return Json(currentFile, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                string errorMessage = "Error when saving values: " + ex.Message;
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    errorMessage += " (" + ex.InnerException.Message + ")";
                }
                return Json(errorMessage);
            }
        }

        /// <summary>
        /// Saves the updated resource file to the path of the file
        /// </summary>
        /// <param name="model">Json representing the resource file with the updated values</param>
        /// <returns>JSON containing the status of the request</returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveResourceFile(string model)
        {
            try
            {
                ResourceFileModel file = Newtonsoft.Json.JsonConvert.DeserializeObject<ResourceFileModel>(model);
                ResourceFile updatedResourceFile = new ResourceFile(file.ResourceFileName);

                //Map the fileEntryModels to FileEntry object
                updatedResourceFile.ResourceFileEntries = file.ResourceFileEntries.Select
                    (x => new ResourceFileEntry() { Key = x.Key, Value = x.Value, Comment = x.Comment, OriginalValue = x.OriginalValue }).ToList();

                resourceRepository.UpdateResourceFile(file.ResourceFilePath, updatedResourceFile);
                return Json("Success");

            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                string errorMessage = "Error when saving values: " + ex.Message;
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    errorMessage += " (" + ex.InnerException.Message + ")";
                }
                return Json(errorMessage);
            }
        }

        /// <summary>
        /// Get the resource string dictionary for the given id, e.g. Navigation, TimeSheet, etc.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetResourceFile(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Json(new object { });
            }

            try
            {
                var normalizedName = id.Insert(0, "Resources.");

                var resxManager = new ResourceManager(normalizedName, Assembly.Load("App_GlobalResources"));

                var set = resxManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);

                var resourceDict = new Dictionary<string, string>();
                foreach (DictionaryEntry item in set)
                {
                    resourceDict.Add(item.Key.ToString(), item.Value.ToString());
                }

                return Json(resourceDict, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                throw new HttpException((int)HttpStatusCode.BadRequest, string.Format("Unable to resolve resource file for id {0} - {1}", id, e.Message));
            }
        }
    }
}
