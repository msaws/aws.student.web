﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Controller;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Models;
using Ellucian.Colleague.Dtos.Base;
using Newtonsoft.Json;
using System.Globalization;
using System.Data;
using Ellucian.Web.Student.Models.UserProfile;
using System.Net;
using System.Threading.Tasks;
using Ellucian.Web.Student.Models.Person;

namespace Ellucian.Web.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class UserProfileController : BaseStudentController
    {
        /// <summary>
        /// Creates a new UserProfileController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public UserProfileController(Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        [LinkHelp]
        [PageAuthorize("aboutYou")]
        [ActionName("Index")]
        public ActionResult IndexAsync()
        {
            // Configure the view
            ViewBag.Title = GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "AreaDescription");

            return View();
        }

        /// <summary>
        /// Gets the current user profile
        /// </summary>
        /// <returns>User profile view model as JSON</returns>
        [PageAuthorize("aboutYou")]
        [ActionName("GetUserProfile")]
        public async Task<JsonResult> GetUserProfileAsync()
        {
            // Get the current user's system ID, use it to get the user's basic demographic and configuration data.

            var personId = CurrentUser.PersonId;

            // Bypass the cache to ensure latest data, which can be necessary for updates to succeed
            var profile = await ServiceClient.GetProfileAsync(personId, false);
            var userProfileViewModel = await BuildUserProfileViewModelFromProfileAsync(profile);

            return Json(userProfileViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets a user's photo by id.
        /// </summary>
        /// <param name="id">user's id</param>
        /// <returns>The user's photo as an image file.</returns>
        [ActionName("UserProfilePhoto")]
        public async Task<ActionResult> UserProfilePhotoAsync(string id)
        {
            try
            {
                ApiFileStream apiClientResult = await ServiceClient.GetPersonPhotoAsync(id);
                string filename = !string.IsNullOrEmpty(apiClientResult.Filename) ? apiClientResult.Filename : id;
                return File(apiClientResult.Data, apiClientResult.ContentType, filename);
            }

            catch (Exception e)
            {
                if (e is AggregateException)
                {
                    AggregateException ae = e as AggregateException;
                    var exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                Logger.Debug(e, "Error fetching photo for id '{0}'", id);
                return HttpNotFound();
            }
        }

        /// <summary>
        /// Generate the emergency information page
        /// </summary>
        /// <returns>The emergency information view</returns>
        [LinkHelp]
        [PageAuthorize("emergencyInfo")]
        public ActionResult EmergencyInformation()
        {
            // Configure the view
            ViewBag.Title = GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "AreaDescription");
            ViewBag.CssAfterOverride = new List<string>();
            return View();

        }

        /// <summary>
        /// Generate the emergency information search page
        /// </summary>
        /// <returns>The emergency information search view</returns>
        [LinkHelp]
        [PageAuthorize("emergencyInfoSearch")]
        public ActionResult EmergencyInformationSearch()
        {
            var personSearchModel = BuildEmergencyInformationPersonSearchModel();
            return View(personSearchModel);
        }

        /// <summary>
        /// Generate the emergency information inquiry view for authorized users
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The emergency information inquiry view</returns>
        [LinkHelp]
        [PageAuthorize("emergencyInfoSearch")]
        public ActionResult ViewEmergencyInformation(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return RedirectToAction("EmergencyInformationSearch", "UserProfile");
            }
            ViewBag.viewEmergencyInformationId = id;
            return View();

        }

        /// <summary>
        /// Query for another user's emergency information
        /// </summary>
        /// <param name="id">The Person Id whose emergency information you want to retrieve</param>
        /// <returns>Emergency information model in JSON</returns>
        [LinkHelp]
        [PageAuthorize("emergencyInfoSearch")]
        public async Task<JsonResult> QueryEmergencyInformationAsync(string id)
        {
            try
            {
                var emergencyInformationModel = await RetrieveEmergencyInformationAsync(id);
                return Json(emergencyInformationModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                var message = "Unable to get emergency information. Error: " + exceptionMessage;
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to get emergency information.");
            }
        }

        /// <summary>
        /// Get a person's emergency contact information asynchronously.
        /// </summary>
        /// <returns>Returns an emergency information object containing emergency contacts and other information</returns>
        [PageAuthorize("emergencyInfo")]
        [ActionName("GetEmergencyInformation")]
        public async Task<JsonResult> GetEmergencyInformationAsync()
        {
            var studentId = CurrentUser.PersonId;
            try
            {
                EmergencyInformationModel emergencyInformationModel = await RetrieveEmergencyInformationAsync(studentId);
                return Json(emergencyInformationModel, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                var message = "Unable to get emergency information. Error: " + exceptionMessage;
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to get emergency information.");
            }
        }

        /// <summary>
        /// Gets the Emergency Information Search model
        /// </summary>
        /// <returns></returns>
        [PageAuthorize("emergencyInfoSearch")]
        public JsonResult GetEmergencyInformationSearchModel()
        {
            var personSearchModel = BuildEmergencyInformationPersonSearchModel();
            return Json(personSearchModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [PageAuthorize("emergencyInfoSearch")]
        [HttpPost]
        public async Task<JsonResult> SearchPersonEmergencyInformationAsync(string criteria)
        {
            if (string.IsNullOrEmpty(criteria))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationSearchBadCriteria"));
            }

            try
            {
                var personNameQueryCriteria = new PersonNameQueryCriteria()
                {
                    Ids = new List<string>(),
                    QueryKeyword = JsonConvert.DeserializeObject<string>(criteria)
                };
                var results = await ServiceClient.QueryPersonNamesByPostAsync(personNameQueryCriteria);
                var personSearchResults = new List<PersonSearchResultModel>();
                foreach (var result in results)
                {

                    var model = new PersonSearchResultModel(result.PreferredName, result.Id, result.PrivacyStatusCode);
                    var privacy = await SetPrivacyPropertiesForModel(model.Id, model.PrivacyStatusCode);
                    model.HasPrivacyRestriction = privacy.Key;
                    model.PrivacyMessage = privacy.Value;
                    personSearchResults.Add(model);

                }
                return Json(personSearchResults);
            }
            catch (Exception e)
            {
                Logger.Info(e, string.Format("Emergency Information search failed for {0}", criteria));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationSearchBadCriteria"));
            }

        }

        /// <summary>
        /// Updates the users emergency information
        /// </summary>
        /// <param name="emergencyInformationJson">Emergency information object converted to JSON string</param>
        /// <returns>Updated emergency information JSON string</returns>
        [PageAuthorize("emergencyInfo")]
        [ActionName("UpdateEmergencyInformation")]
        public async Task<JsonResult> UpdateEmergencyInformationAsync(string emergencyInformationJson)
        {
            var emergencyInformationModel = JsonConvert.DeserializeObject<EmergencyInformationModel>(emergencyInformationJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
            var originalEmergencyInformationModel = emergencyInformationModel;
            var emergencyInformation = BuildEmergencyInformationDto(emergencyInformationModel);
            try
            {
                var updatedEmergencyInformationTask = ServiceClient.UpdateEmergencyInformationAsync(emergencyInformation);
                // Build the Emergency Information Model
                var healthConditionsTask = ServiceClient.GetCachedHealthConditionsAsync();
                var emergencyInformationConfigurationTask = ServiceClient.GetCachedEmergencyInformationConfigurationAsync();
                await Task.WhenAll(updatedEmergencyInformationTask, healthConditionsTask, emergencyInformationConfigurationTask);
                EmergencyInformationModel updatedEmergencyModel = new EmergencyInformationModel(updatedEmergencyInformationTask.Result, healthConditionsTask.Result, emergencyInformationConfigurationTask.Result);
                return Json(updatedEmergencyModel);
            }
            catch (Exception ex)
            {
                // instead of throwing a 404 Bad Request - retrieve the original emergency notification info so page can be reset but include a failure notification.

                // But first log the error that the update returned.
                string message = "Unable to update emergency information. Error: " + ex.Message;
                Logger.Error(ex, message);

                try
                {
                    originalEmergencyInformationModel.Notification = new Notification(GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationUpdateFailure"), NotificationType.Error, true);
                    return Json(originalEmergencyInformationModel, JsonRequestBehavior.AllowGet);
                }
                catch (Exception gex)
                {
                    var exceptionMessage = gex.Message;
                    if (gex is AggregateException)
                    {
                        AggregateException ae = gex as AggregateException;
                        exceptionMessage = ae.LogAggregateExceptions(Logger);
                    }
                    message = "Unable to get emergency information. Error: " + exceptionMessage;
                    Logger.Error(gex, message);
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json(GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationUpdateFailure"));
                }

            }
        }

        /// <summary>
        /// Confirms the users emergency information
        /// </summary>
        /// <param name="emergencyInformationJson">Emergency information object converted to JSON string</param>
        /// <returns>Updated emergency information JSON string</returns>
        [PageAuthorize("emergencyInfo")]
        [ActionName("VerifyEmergencyInformation")]
        public async Task<JsonResult> VerifyEmergencyInformationAsync(string emergencyInformationJson)
        {
            var emergencyInformationModel = JsonConvert.DeserializeObject<EmergencyInformationModel>(emergencyInformationJson, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local });
            try
            {
                var emergencyInformationConfiguration = await ServiceClient.GetCachedEmergencyInformationConfigurationAsync();
                if (!emergencyInformationConfiguration.AllowOptOut)
                {
                    emergencyInformationModel.OptOut = false;
                }

                // Institutions may require users to provide emergency contact information.
                // However, they may also allow opt out, so check for those settings as well
                if (emergencyInformationConfiguration.RequireContact && !emergencyInformationModel.EmergencyContacts.Any())
                {
                    if (!emergencyInformationConfiguration.AllowOptOut || !emergencyInformationModel.OptOut)
                    {
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        var errorMessage = GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationConfirmFailure")
                            + "<br />" + GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationConfirmNeedContact");
                        return Json(errorMessage);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Info(e, "Unable to retrieve emergency information configuration.");
            }

            var emergencyInformation = BuildEmergencyInformationDto(emergencyInformationModel);
            emergencyInformation.ConfirmedDate = DateTime.Today;

            try
            {
                foreach (var contact in emergencyInformation.EmergencyContacts)
                {
                    if (!ContactHasPhone(contact))
                    {
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        var errorMessage = GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationConfirmFailure")
                            + "<br /> " + GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationUpdateNeedPhones");
                        return Json(errorMessage);
                    }
                }

                var updatedEmergencyInformationTask = ServiceClient.UpdateEmergencyInformationAsync(emergencyInformation);
                // Build the Emergency Information Model
                var healthConditionsTask = ServiceClient.GetCachedHealthConditionsAsync();
                var emergencyInformationConfigurationTask = ServiceClient.GetCachedEmergencyInformationConfigurationAsync();
                await Task.WhenAll(updatedEmergencyInformationTask, healthConditionsTask, emergencyInformationConfigurationTask);
                var updatedEmergencyInformation = updatedEmergencyInformationTask.Result;
                var healthConditions = healthConditionsTask.Result;
                var emergencyInformationConfiguration = emergencyInformationConfigurationTask.Result;
                EmergencyInformationModel updatedEmergencyModel = new EmergencyInformationModel(updatedEmergencyInformation, healthConditions, emergencyInformationConfiguration);
                updatedEmergencyModel.Notification = new Notification(string.Format(GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationConfirmed"), updatedEmergencyInformation.ConfirmedDate.Value.ToShortDateString()), NotificationType.Success, true);
                return Json(updatedEmergencyModel);
            }

            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    exceptionMessage = ae.LogAggregateExceptions(Logger);
                }
                string message = "Unable to confirm emergency information. Error: " + exceptionMessage;
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationConfirmFailure"));
            }
        }

        /// <summary>
        /// Updates the user profile with a new address information verification
        /// </summary>
        /// <param name="userProfileJson">The user profile to verify</param>
        /// <returns>JSON representation of the updated profile</returns>
        [HttpPost]
        [PageAuthorize("aboutYou")]
        public async Task<JsonResult> VerifyAddressInformationAsync(string userProfileJson)
        {
            var userProfileModel = JsonConvert.DeserializeObject<UserProfileModel>(userProfileJson);
            return await VerifyUserProfileInformationTypeAsync(userProfileModel, UserProfileInformationType.Address);
        }

        /// <summary>
        /// Updates the user profile with a new email address information verification
        /// </summary>
        /// <param name="userProfileJson">The user profile to verify</param>
        /// <returns>JSON representation of the updated profile</returns>
        [HttpPost]
        [PageAuthorize("aboutYou")]
        public async Task<JsonResult> VerifyEmailAddressInformationAsync(string userProfileJson)
        {
            var userProfileModel = JsonConvert.DeserializeObject<UserProfileModel>(userProfileJson);
            return await VerifyUserProfileInformationTypeAsync(userProfileModel, UserProfileInformationType.EmailAddress);
        }

        /// <summary>
        /// Updates the user profile with a new phone information verification
        /// </summary>
        /// <param name="userProfileJson">The user profile to verify</param>
        /// <returns>JSON representation of the updated profile</returns>
        [HttpPost]
        [PageAuthorize("aboutYou")]
        public async Task<JsonResult> VerifyPhoneInformationAsync(string userProfileJson)
        {
            var userProfileModel = JsonConvert.DeserializeObject<UserProfileModel>(userProfileJson);
            return await VerifyUserProfileInformationTypeAsync(userProfileModel, UserProfileInformationType.Phone);
        }

        /// <summary>
        /// Updates the user profile with any new information
        /// </summary>
        /// <param name="userProfileJson">The user profile to update</param>
        /// <returns>JSON representation of the updated profile</returns>
        [HttpPost]
        [PageAuthorize("aboutYou")]
        public async Task<JsonResult> UpdateUserProfileAsync(string userProfileJson)
        {
            var userProfileModel = JsonConvert.DeserializeObject<UserProfileModel>(userProfileJson);
            try
            {
                var updatedProfile = await ServiceClient.UpdateProfile2Async(userProfileModel as Profile);
                var updatedUserProfileViewModel = await BuildUserProfileViewModelFromProfileAsync(updatedProfile);
                return Json(updatedUserProfileViewModel);
            }
            catch (Exception ex)
            {
                string message = "Unable to update user profile. Error: ";
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    message += ae.LogAggregateExceptions(Logger);
                }
                else
                {
                    message += ex.Message;
                }
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to update user profile.");
            }
        }


        #region Account Preferences

        //
        // GET: /UserProfile/Preferences
        [LinkHelp]
        [PageAuthorize("accountPreferences")]
        public async Task<ActionResult> Preferences()
        {
            // Default model object
            PreferencesModel model = new PreferencesModel();
            try
            {
                var accountPrefs = await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, "selfservice");

                if (accountPrefs != null && accountPrefs.Preferences != null)
                {
                    model = new PreferencesModel(accountPrefs.Preferences);
                }
            }
            catch (Exception ex)
            {
                if (CurrentUser != null)
                {
                    Logger.Debug(ex, "Error retrieving account preferences for person with id: " + CurrentUser.PersonId);
                }
                else
                {
                    Logger.Debug(ex, "Error retrieving account preferences for null user.");
                }
            }
            return View(model);
        }

        /// <summary>
        /// Attempts to find and return the current user's PreferencesModel object as a JSONResult.
        /// </summary>
        /// <returns>JSONResult containing the user's preferences</returns>
        [PageAuthorize("accountPreferences")]
        public async Task<JsonResult> GetPreferencesAsync()
        {
            // Default model object
            PreferencesModel model = new PreferencesModel();

            try
            {
                var accountPrefs = await ServiceClient.GetSelfservicePreferenceAsync(CurrentUser.PersonId, "selfservice");

                if (accountPrefs != null && accountPrefs.Preferences != null)
                {
                    model = new PreferencesModel(accountPrefs.Preferences);
                }
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                if (CurrentUser != null)
                {
                    Logger.Error(ex, "Error retrieving account preferences for person with id: " + CurrentUser.PersonId);
                }
                else
                {
                    Logger.Error(ex, "Error retrieving account preferences for null user.");
                }
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Updates the user's preferences using a serialized JSON representation of the PreferencesModel object.
        /// </summary>
        /// <param name="preferencesJson">A string containing PreferencesModel data serialized as JSON</param>
        /// <returns>The updated PreferencesModel object in a JSONResult</returns>
        [HttpPost]
        [PageAuthorize("accountPreferences")]
        public async Task<JsonResult> SavePreferencesAsync(string preferencesJson)
        {
            try
            {
                // Create the model from the JSON string that was passed in
                var preferencesModel = JsonConvert.DeserializeObject<PreferencesModel>(preferencesJson);

                // Serialize and store the the PreferencesModel object in the SelfservicePreference dto
                var preferences = new Dictionary<string, dynamic>();
                preferences.Add("Homepage", preferencesModel.Homepage);
                var preferenceDto = new SelfservicePreference()
                {
                    Id = "",
                    PersonId = CurrentUser.PersonId,
                    PreferenceType = "selfservice",
                    Preferences = preferences
                };

                var updatedPreference = await ServiceClient.UpdateSelfservicePreferenceAsync(preferenceDto);

                return Json(updatedPreference);
            }
            catch (Exception ex)
            {
                if (CurrentUser != null)
                {
                    Logger.Debug(ex, "Error updating account preferences for person with id: " + CurrentUser.PersonId);
                }
                else
                {
                    Logger.Debug(ex, "Error updating account preferences for null user.");
                }
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Error updating preferences.");
            }
        }

        #endregion

        /// <summary>
        /// Sets the privacy properties of a object.
        /// </summary>
        public async Task<KeyValuePair<bool, string>> SetPrivacyPropertiesForModel(string id, string privacyStatusCode)
        {
            // If no privacy code, the data does not have a privacy restriction or associated message
            if (CurrentUser.IsPerson(id) || string.IsNullOrEmpty(privacyStatusCode))
            {
                return new KeyValuePair<bool, string>(false, null);
            }
            else
            {
                // Administrators must have access to the privacy code in order to view the data;
                // access the admin user's staff record (if available) to determine their privacy access privileges
                Staff staff = null;
                try
                {
                    staff = await ServiceClient.GetStaffAsync(CurrentUser.PersonId);
                }
                catch
                {
                    staff = new Staff(CurrentUser.PersonId, string.Empty) { PrivacyCodes = new List<string>() };
                }

                // Data is private if admin user does have access to the privacy code
                var hasPrivacyRestriction = !staff.PrivacyCodes.Contains(privacyStatusCode);
                string privacyMessage = null;
                if (hasPrivacyRestriction)
                {
                    // Use the record denial message from PID5 if the user does not have access to the data
                    PrivacyConfiguration privacyConfig = await ServiceClient.GetPrivacyConfigurationAsync();
                    privacyMessage = privacyConfig != null ? privacyConfig.RecordDenialMessage : string.Empty;
                }
                else
                {
                    // Use the privacy warning message from PID5 if the user has access to the data
                    privacyMessage = await ServiceClient.GetCachedPrivacyMessageAsync(privacyStatusCode);
                }
                return new KeyValuePair<bool, string>(hasPrivacyRestriction, privacyMessage);
            }
        }

        private EmergencyInformation BuildEmergencyInformationDto(EmergencyInformationModel emergencyInformationModel)
        {
            EmergencyInformation emergencyInformation = new EmergencyInformation();
            emergencyInformation.EmergencyContacts = new List<Ellucian.Colleague.Dtos.Base.EmergencyContact>();
            foreach (var contact in emergencyInformationModel.EmergencyContacts)
            {
                DateTime? date = null;
                try
                {
                    date = contact.EffectiveDate != null && contact.EffectiveDate != string.Empty ? (DateTime?)Convert.ToDateTime(contact.EffectiveDate) : null;
                }
                catch (Exception)
                {

                }

                emergencyInformation.EmergencyContacts.Add(new Ellucian.Colleague.Dtos.Base.EmergencyContact()
                {
                    Relationship = contact.Relationship,
                    OtherPhone = contact.OtherPhone,
                    Name = contact.Name,
                    IsMissingPersonContact = contact.IsMissingPersonContact,
                    IsEmergencyContact = contact.IsEmergencyContact,
                    EveningPhone = contact.EveningPhone,
                    EffectiveDate = date,
                    DaytimePhone = contact.DaytimePhone,
                    Address = contact.Address
                });
            }
            if (!string.IsNullOrEmpty(emergencyInformationModel.ConfirmedDate))
            {
                Convert.ToDateTime(emergencyInformationModel.ConfirmedDate);
            }
            emergencyInformation.OptOut = emergencyInformationModel.OptOut;
            emergencyInformation.HospitalPreference = emergencyInformationModel.HospitalPreference;
            emergencyInformation.InsuranceInformation = emergencyInformationModel.InsuranceInformation;
            emergencyInformation.AdditionalInformation = emergencyInformationModel.AdditionalInformation;
            emergencyInformation.PersonId = emergencyInformationModel.PersonId;
            emergencyInformation.HealthConditions = new List<string>();
            foreach (var condition in emergencyInformationModel.HealthConditions)
            {
                if (condition.isSelected)
                {
                    emergencyInformation.HealthConditions.Add(condition.Code);
                }
            }
            return emergencyInformation;
        }

        /// <summary>
        /// Checks if the contact DTO has at least one phone number (no checks are performed, any text will count as a phone number).
        /// </summary>
        /// <param name="emergencyContactDto"></param>
        /// <returns>True if at least one phone number is present, otherwise false.</returns>
        private bool ContactHasPhone(Ellucian.Colleague.Dtos.Base.EmergencyContact emergencyContactDto)
        {
            return !string.IsNullOrEmpty(emergencyContactDto.DaytimePhone)
                || !string.IsNullOrEmpty(emergencyContactDto.EveningPhone)
                || !string.IsNullOrEmpty(emergencyContactDto.OtherPhone);
        }

        private async Task<UserProfileViewModel> BuildUserProfileViewModelFromProfileAsync(Profile profile)
        {
            var phoneTypesTask = ServiceClient.GetCachedPhoneTypesAsync();
            var emailTypesTask = ServiceClient.GetCachedEmailTypesAsync();
            var addressTypesTask = ServiceClient.GetCachedAddressTypesAsync();
            var personalPronounTypesTask = ServiceClient.GetCachedPersonalPronounTypesAsync();
            var configurationTask = ServiceClient.GetCachedUserProfileConfiguration2Async();
            var rolesTask = ServiceClient.GetCachedRolesAsync();
            var countriesTask = ServiceClient.GetCachedCountriesAsync();
            var statesTask = ServiceClient.GetCachedStatesAsync();

            await Task.WhenAll(phoneTypesTask, emailTypesTask, addressTypesTask, configurationTask, rolesTask, countriesTask, statesTask, personalPronounTypesTask);

            var phoneTypes = phoneTypesTask.Result;
            var emailTypes = emailTypesTask.Result;
            var addressTypes = addressTypesTask.Result;
            var configuration = configurationTask.Result;
            var roles = rolesTask.Result;
            var countries = countriesTask.Result;
            var states = statesTask.Result;
            var personalPronounTypes = personalPronounTypesTask.Result;


            return new UserProfileViewModel(profile, configuration, phoneTypes, emailTypes, addressTypes, CurrentUser, roles, countries, states, personalPronounTypes);
        }

        /// <summary>
        /// Updates a user profile with a new confirmation date for the given information type
        /// </summary>
        /// <param name="userProfileModel">User Profile Model</param>
        /// <param name="infoType">Information Type to be updated</param>
        /// <returns>JSON representation of the updated profile</returns>
        private async Task<JsonResult> VerifyUserProfileInformationTypeAsync(UserProfileModel userProfileModel, UserProfileInformationType infoType)
        {
            try
            {
                Profile profile = userProfileModel as Profile;
                switch (infoType)
                {
                    case UserProfileInformationType.Address:
                        profile.AddressConfirmationDateTime = DateTimeOffset.Now;
                        break;
                    case UserProfileInformationType.EmailAddress:
                        profile.EmailAddressConfirmationDateTime = DateTimeOffset.Now;
                        break;
                    case UserProfileInformationType.Phone:
                        profile.PhoneConfirmationDateTime = DateTimeOffset.Now;
                        break;
                }
                var updatedProfile = await ServiceClient.UpdateProfile2Async(profile);
                DateTimeOffset? updatedConfirmationDate = null;
                switch (infoType)
                {
                    case UserProfileInformationType.Address:
                        updatedConfirmationDate = updatedProfile.AddressConfirmationDateTime;
                        break;
                    case UserProfileInformationType.EmailAddress:
                        updatedConfirmationDate = updatedProfile.EmailAddressConfirmationDateTime;
                        break;
                    case UserProfileInformationType.Phone:
                        updatedConfirmationDate = updatedProfile.PhoneConfirmationDateTime;
                        break;
                }

                var notificationMessage = string.Format(GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "UserProfileInformationConfirmed"), updatedConfirmationDate.Value.LocalDateTime.ToShortDateString());
                var updatedUserProfileViewModel = await BuildUserProfileViewModelFromProfileAsync(updatedProfile);
                updatedUserProfileViewModel.Notification = new Notification(notificationMessage, NotificationType.Success);
                return Json(updatedUserProfileViewModel);
            }
            catch (Exception ex)
            {
                string message = "Unable to confirm user profile information. Error: ";
                if (ex is AggregateException)
                {
                    AggregateException ae = ex as AggregateException;
                    message += ae.LogAggregateExceptions(Logger);
                }
                else
                {
                    message += ex.Message;
                }
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to confirm user profile information.");
            }
        }

        /// <summary>
        /// Retrieves emergency information and configuration and builds an EmergencyInformationModel
        /// </summary>
        /// <param name="id">Person Id for which to retrieve emergency information</param>
        /// <returns>EmergencyInformationModel for the given person</returns>
        private async Task<EmergencyInformationModel> RetrieveEmergencyInformationAsync(string id)
        {
            var healthConditionsTask = ServiceClient.GetCachedHealthConditionsAsync();
            var emergencyInformationTask = ServiceClient.GetPersonEmergencyInformationAsync(id);
            var emergencyInformationConfigurationTask = ServiceClient.GetCachedEmergencyInformationConfigurationAsync();
            await Task.WhenAll(healthConditionsTask, emergencyInformationTask, emergencyInformationConfigurationTask);
            var emergencyInformation = emergencyInformationTask.Result;
            var healthConditions = healthConditionsTask.Result;
            var emergencyInformationConfiguration = emergencyInformationConfigurationTask.Result;

            // If no privacy code, the data does not have a privacy restriction or associated message
            var privacy = await SetPrivacyPropertiesForModel(emergencyInformation.PersonId, emergencyInformation.PrivacyStatusCode);

            EmergencyInformationModel emergencyInformationModel = new EmergencyInformationModel(emergencyInformation, healthConditions, emergencyInformationConfiguration, privacy.Key, privacy.Value);

            return emergencyInformationModel;
        }

        private PersonSearchModel BuildEmergencyInformationPersonSearchModel()
        {
            var personSearchModel = new PersonSearchModel(
                GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationSearchPrompt"),
                GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationSearchBeforeSearchText"),
                "_EmergencyInformationSearchResult")
            {
                Search = new Models.Search.SearchActionModel("QueryEmergencyInformationAsync", "UserProfile"),
                Select = new Models.Search.SearchActionModel("ViewEmergencyInformation", "UserProfile"),
                PlaceholderText = GlobalResources.GetString(GlobalResourceFiles.UserProfileResources, "EmergencyInformationSearchPlaceholderText")
            };
            return personSearchModel;
        }

        /// <summary>
        /// The type of User Profile Information
        /// </summary>
        private enum UserProfileInformationType
        {
            Address,
            EmailAddress,
            Phone,
        }
    }
}
