﻿// Copyright 2016 - 2017 Ellucian Company L.P. and its affiliates

using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Licensing;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Models.Courses;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Controllers
{
    [GuestAuthorize]
    public class CoursesController : BaseCommonController
    {
                private CourseSearchHelper _helper;
                public CoursesController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            :base(settings, logger)
        {
            _helper = new CourseSearchHelper(ServiceClient, logger, adapterRegistry);

        }

        [LinkHelp]
       [GuestRedirect("Courses", "Index","Planning",ModuleConstants.Planning)]
       [PageAuthorize("courseCatalogExcludeDegreePlanHome")]
        public async Task< ActionResult> Index()
        {
           

            ViewBag.Title = "Course Catalog";
            var catalogConfiguration = await ServiceClient.GetCachedCourseCatalogConfigurationAsync();
            dynamic dynamicModel = _helper.RetrieveDynamicViewModel(catalogConfiguration, null);

            return View(dynamicModel);
        }

        /// <summary>
        /// Get lookup details for course catalog advanced search
        /// </summary>
        /// <returns>advanced search model.</returns>
        [GuestRedirect("Courses", "Index", "Planning", ModuleConstants.Planning)]
        [PageAuthorize("courseCatalogExcludeDegreePlanHome")]
        public async Task<JsonResult> GetCatalogAdvancedSearchAsync()
        {
            CatalogAdvancedSearchItemsModel searchModel = await _helper.RetrieveAdvancedSearchItemsAsync();
            return Json(searchModel, JsonRequestBehavior.AllowGet);

        }

        
        /// <summary>
        /// Load the catalog search page.
        /// </summary>
        /// <returns>The catalog search view</returns>
        [LinkHelp]
        [GuestRedirect("Courses", "Search","Planning",ModuleConstants.Planning)]
        [PageAuthorize("courseCatalogExcludeDegreePlanSearch")]
        public async Task<ActionResult> Search(string model, CatalogSearchCriteriaModel searchParams)
        {
            CatalogSearchCriteriaModel searchCriteriaModel = new CatalogSearchCriteriaModel();
            if (model != null)
            {
                searchCriteriaModel = _helper.RetrieveCatalogSearchCriteriaFromAdvance(model);
            }
            else
            {
                searchCriteriaModel = searchParams;
            }
            var catalogConfiguration = await ServiceClient.GetCachedCourseCatalogConfigurationAsync();
            dynamic dynamicModel = _helper.RetrieveDynamicViewModel(catalogConfiguration, searchCriteriaModel);

            ViewBag.Title = "Course Catalog";
            this.ViewBag.IsWithDegreePlan = false;

            return View("SearchResult", dynamicModel);
        }


        /// <summary>
        /// Searches for courses
        /// </summary>
        /// <param name="searchParameters"></param>
        /// <returns>JSON response containing search results</returns>
        [ValidateInput(false)]
        [GuestRedirect("Courses", "SearchAsync", "Planning", ModuleConstants.Planning)]
        [HttpPost]
        public async Task<JsonResult> SearchAsync(string searchParameters)
        {
            CatalogSearchCriteriaModel searchCriteriaModel = _helper.RetrieveCatalogSearchCriteria(searchParameters);
            var viewModel = await _helper.CreateCourseSearchResultAsync(searchCriteriaModel);
            var result= Json(viewModel, JsonRequestBehavior.AllowGet);
            return result;
        }

       



        /// <summary>
        /// Asynchronously retrieves section data for the given course.
        /// </summary>
        /// <param name="courseId">The ID of the course for which to retrieve the full section data list</param>
        /// <param name="sectionIds">A collection of matching section IDs to retrieve</param>
        /// <returns>A collection of sections and associated data for this course, sorted by number (BIO100-01, BIO100-02, BIO100-03, ...)</returns>
        [ActionName("SectionsAsync")]
        [GuestRedirect("Courses", "SectionsAsync", "Planning", ModuleConstants.Planning)]
        public async Task<JsonResult> SectionsAsync(string courseId, IEnumerable<string> sectionIds)
        {
            SectionRetrievalModel sections = await _helper.CreateSectionRetrievalModelAsync(courseId, sectionIds);


            return new JsonResult() { Data = sections, MaxJsonLength = Int32.MaxValue };
        }

        /// <summary>
        /// Retrieves Section details for the given section ID and returns it as a JSON model.
        /// </summary>
        /// <param name="sectionId">The ID of the specific section of a course for which to retrieve details</param>
        /// <param name="studentId">The ID of the student reviewing the section details or adding the section. Need to determine which grading options to show.</param>
        /// <returns>A JSON model of the full section details</returns>
        [ActionName("SectionDetails")]
        [GuestRedirect("Courses", "SectionDetails", "Planning", ModuleConstants.Planning)]
        public async Task<JsonResult> SectionDetailsAsync(string sectionId, string studentId)
        {
            SectionDetailsModel model = null;

            model = await _helper.CreateSectionDetailsAsync(sectionId, studentId, CurrentUser==null?null:CurrentUser.PersonId);


            return Json(model);
        }

     
    }
}