﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using Hedtech.Identity;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class HomeController : BaseStudentController
    {
        private const string _notificationsProxyWorkflowCode = "CONO";
        private Settings _settings;
        #region Injection Constructor
        public HomeController(Settings settings, ILogger logger)
            : base(settings, logger)
        {
            _settings = settings;
        }
        #endregion

        [LinkHelp]
        public async Task<ActionResult> Index(bool hideProxyDialog = true)
        {
            try
            {
                if (HttpContext != null &&
                                HttpContext.Request != null &&
                                HttpContext.Request.UrlReferrer != null &&
                                HttpContext.Request.UrlReferrer.AbsolutePath != null &&
                                (HttpContext.Request.UrlReferrer.AbsolutePath.Contains("Account/Login") ||
                                HttpContext.Request.UrlReferrer.AbsolutePath.Contains("/SamlSso")))
                {
                    // Always show the dialog when redirected from login or SSO
                    ViewBag.HideProxyDialog = false;
                }
                else
                {
                    ViewBag.HideProxyDialog = hideProxyDialog;
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex, ex.Message);
                ViewBag.HideProxyDialog = hideProxyDialog;
            }


            IdentityManagementSettings settings = DependencyResolver.Current.GetService<IdentityManagementSettings>();
            if (settings != null && settings.IsIdmEnabled && !HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            // Get the Severity Style Mappings
            ViewBag.RestrictionConfiguration = await ServiceClient.GetRestrictionConfigurationAsync();

            List<PersonRestriction> restrictions = null;
            try
            {
                restrictions = (await GetActivePersonRestrictions(GetEffectivePersonId())).ToList();
            }
            catch
            {
                restrictions = new List<PersonRestriction>();
            }
            return View(restrictions);
        }

        /// <summary>
        /// Gets and serializes the effective user's Person Restriction records from the API
        /// </summary>
        /// <returns>A JSON string containing 0 to many PersonRestriction objects</returns>
        [HttpGet]
        public async Task<ActionResult> GetEffectiveUserRestrictions()
        {
            List<PersonRestriction> activeRestrictions = null;
            try
            {
                activeRestrictions = (await GetActivePersonRestrictions(GetEffectivePersonId())).ToList();
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Unable to retreive person restrictions.");
            }
            return Json(activeRestrictions, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves a list of active PersonRestriction objects based on the current date and the personId argument
        /// </summary>
        /// <param name="personId">The student ID for whom to access restrictions</param>
        /// <returns>An IEnumerable of PersonRestriction objects</returns>
        private async Task<IEnumerable<PersonRestriction>> GetActivePersonRestrictions(string personId)
        {
            List<PersonRestriction> activeRestrictions = new List<PersonRestriction>();

            // User has access to restrictions if they are
            // - acting as themselves
            // - have proxy access to notifications for the specified person
            var hasAccessToRestrictions = CurrentUser.IsPerson(personId) || HasProxyAccessToWorkflowForPerson(personId, _notificationsProxyWorkflowCode);
            if (hasAccessToRestrictions)
            {
                var restrictions = await ServiceClient.GetStudentRestrictionsAsync(personId, true);
                if (restrictions != null)
                {
                    activeRestrictions = restrictions.Where(r => DateUtility.IsDateInRange(DateTime.Today, r.StartDate, r.EndDate)).ToList();
                }
                foreach (var restriction in activeRestrictions)
                {
                    restriction.HyperlinkText = string.IsNullOrEmpty(restriction.HyperlinkText) ? "None" : restriction.HyperlinkText;
                }
            }

            return activeRestrictions;
        }

        [HttpGet]
        public async Task<string> GetRestrictionConfigurationAsync()
        {
            var rc = await ServiceClient.GetRestrictionConfigurationAsync();
            return JsonConvert.SerializeObject(rc);
        }

        [LinkHelp]
        [AllowAnonymous]
        public ActionResult Privacy()
        {
            return View();
        }
    }
}
