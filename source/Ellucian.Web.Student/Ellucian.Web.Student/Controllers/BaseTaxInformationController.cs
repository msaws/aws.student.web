﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.TaxInformation;
using Ellucian.Web.Student.Utility;
using slf4net;

namespace Ellucian.Web.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class BaseTaxInformationController : BaseStudentController
    {
        /// <summary>
        /// Creates a new HomeController instance.
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry helper; injected by the framework</param>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public BaseTaxInformationController(IAdapterRegistry adapterRegistry, Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        /// <summary>
        /// retrieve all tax form consents for user
        /// </summary>
        /// <returns>it returns the tax form consents in JSON format</returns>
        [PageAuthorize("taxInformation", "studentTaxInformation")]
        public async Task<JsonResult> GetTaxFormInformationAsync(string taxFormId)
        {
            try
            {
                if(string.IsNullOrEmpty(taxFormId))
                {
                    throw new ArgumentNullException("taxFormId", "taxFormId is required.");
                }

                // Convert the parameter to a dto. Default it to W2.
                var taxFormEnumId = TaxForms.FormW2;
                switch (taxFormId.ToUpper())
                {
                    case "FORM1095C":
                        taxFormEnumId = TaxForms.Form1095C;
                        break;
                    case "FORM1098":
                        taxFormEnumId = TaxForms.Form1098;
                        break;
                }

                // For the user, get the list of tax form consents, the list of tax statements and
                // the consent and withheld paragraphs. The paragraphs are cached in the server side
                // for the default 5 minutes, and also cached in the API for 24 hours.
                var taxFormConsentsTask = ServiceClient.GetTaxFormConsents(CurrentUser.PersonId, taxFormEnumId);
                var taxFormStatementsTask = ServiceClient.GetTaxFormStatements2(CurrentUser.PersonId, taxFormEnumId);
                var taxFormConfigurationTask = ServiceClient.GetCachedTaxFormConfigurationaAsync(taxFormEnumId);
                await Task.WhenAll(taxFormConsentsTask, taxFormStatementsTask, taxFormConfigurationTask);

                var taxFormConsentsDtos = taxFormConsentsTask.Result;
                var taxFormStatementsDtos = taxFormStatementsTask.Result;
                var taxFormConfigurationDtos = taxFormConfigurationTask.Result;

                var taxInformationViewModel = new TaxInformationViewModel(taxFormConsentsDtos, taxFormStatementsDtos, taxFormConfigurationDtos, taxFormEnumId);
                return Json(taxInformationViewModel, JsonRequestBehavior.AllowGet);
            }
            catch (HttpRequestFailedException hrfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(hrfe.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(hrfe.Message);
            }
            catch (ResourceNotFoundException rnfe)
            {
                // Log the error and send back the exact HTTP status code
                Logger.Error(rnfe.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(rnfe.Message);
            }
            catch (Exception e)
            {
                // Log the error and send back a more generic HTTP status code
                Logger.Error(e.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message);
            }
        }

        /// <summary>
        /// Add a new consent for the user
        /// </summary>
        /// <param name="taxForm">The tax form that applies to the consent (W-2, 1095C, etc)</param>
        /// <param name="hasConsented">Boolean to represent whether the user consented or not to have the statement online</param>
        /// <returns>A JSON representation of the new consent</returns>
        [PageAuthorize("taxInformation", "studentTaxInformation")]
        public async Task<JsonResult> AddTaxFormConsentAsync(string taxForm, bool hasConsented)
        {
            try
            {
                // Add the new consent.
                // The person ID is assigned in the API.
                var taxFormConsentDto = HumanResourcesUtility.BuildTaxFormConsentDto(this.GetEffectivePersonId(), DateTime.Now, taxForm, hasConsented);
                var addedTaxFormConsent = await ServiceClient.AddTaxFormConsent(taxFormConsentDto);
                var taxFormconsentsViewModel = new TaxFormConsentsViewModel(addedTaxFormConsent);

                return Json(taxFormconsentsViewModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// An action method for the tax information view page for employees
        /// </summary>
        /// <returns>ActionResult</returns>
        [LinkHelp]
        [PageAuthorize("taxInformation", "studentTaxInformation")]
        public async Task<ActionResult> TaxInformation()
        {
            try
            {
                var userPermissions = await GetCurrentUserPermissionsAsync();
                var w2Permission = userPermissions.Contains("VIEW.W2");
                var form1095CPermission = userPermissions.Contains("VIEW.1095C");
                var form1098Permission = userPermissions.Contains("VIEW.1098");

                var allPermissions = await GetAllDefinedPermissions();
                var w2Defined = allPermissions.Contains("VIEW.W2");
                var form1095CDefined = allPermissions.Contains("VIEW.1095C");
                var form1098Defined = allPermissions.Contains("VIEW.1098");

                var permissionsAssigned = w2Defined || form1095CDefined || form1098Defined;

                ViewBag.W2 = permissionsAssigned ? w2Permission : true;
                ViewBag.Form1095C = permissionsAssigned ? form1095CPermission : true;
                ViewBag.Form1098 = permissionsAssigned ? form1098Permission : true;
            }
            catch (Exception)
            {
                ViewBag.W2 = true;
                ViewBag.Form1095C = true;
                ViewBag.Form1098 = true;
            }

            ViewBag.Title = GlobalResources.GetString("TaxForms", "AreaDescription");
            ViewBag.CssAfterOverride = new List<string>();

            return View("~/Views/TaxInformation/TaxInformation.cshtml");
        }

        /// <summary>
        /// Get the W-2 PDF data for a specified W-2 for the person logged in
        /// </summary>
        /// <param name="id">ID of the W-2 PDF record</param>
        /// <returns>W-2 PDF data record</returns>
        [PageAuthorize("taxInformation", "studentTaxInformation")]
        public async Task<FileResult> GetW2Pdf(string id)
        {
            try
            {
                var pdfData = await ServiceClient.GetW2TaxFormPdf(this.GetEffectivePersonId(), id);
                var pdfMemoryStream = new MemoryStream(pdfData);

                return File(pdfMemoryStream, "application/Pdf");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return null;
            }
        }

        /// <summary>
        /// Get the 1095-C PDF data for a specified 1095-C for the person logged in
        /// </summary>
        /// <param name="id">ID of the 1095-C PDF record</param>
        /// <returns>1095-C PDF data record</returns>
        [PageAuthorize("taxInformation", "studentTaxInformation")]
        public async Task<FileResult> Get1095cPdf(string id)
        {
            try
            {
                var pdfData = await ServiceClient.Get1095cTaxFormPdf(this.GetEffectivePersonId(), id);
                var pdfMemoryStream = new MemoryStream(pdfData);

                return File(pdfMemoryStream, "application/Pdf");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return null;
            }
        }

        /// <summary>
        /// Get the 1098 PDF data for a specified 1098 for the person logged in
        /// </summary>
        /// <param name="id">ID of the 1098 PDF record</param>
        /// <returns>1098 PDF data record</returns>
        [PageAuthorize("taxInformation", "studentTaxInformation")]
        public async Task<FileResult> Get1098Pdf(string id)
        {
            try
            {
                // TODO: Change Get1095cTaxFormPdf to Get1098TaxFormPdf
                var pdfData = await ServiceClient.Get1098tTaxFormPdf(this.GetEffectivePersonId(), id);
                var pdfMemoryStream = new MemoryStream(pdfData);

                return File(pdfMemoryStream, "application/Pdf");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return null;
            }
        }

        private async Task<IEnumerable<string>> GetCurrentUserPermissionsAsync()
        {
            var roles = await ServiceClient.GetRolesAsync();
            var userPermissions = roles
                .Where(r => CurrentUser.Roles.Contains(r.Title))
                .SelectMany(r =>
                    r.Permissions.Select(p => p.Code));

            return userPermissions;
        }

        private async Task<IEnumerable<string>> GetAllDefinedPermissions()
        {
            var roles = await ServiceClient.GetRolesAsync();
            var userPermissions = roles
                .SelectMany(r =>
                    r.Permissions.Select(p => p.Code));

            return userPermissions;
        }
    }
}