﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.OrganizationalStructure;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Controllers
{
    /// <summary>
    /// Organizational Structure controller containing actions in support of
    /// adding and maintaining organizational relationships for organizational
    /// person positions.
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class OrganizationalStructureController : BaseStudentController
    {
        #region Injection Constructor
        public OrganizationalStructureController(Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }
        #endregion

        [LinkHelp]
        [PageAuthorize("organizationalStructure")]
        public ActionResult Index()
        {
            return View();
        }

        #region Get Relationship
        /// <summary>
        /// Gets the relationship for the person matching the query
        /// </summary>
        /// <returns>Organizational Person Position collection as JSON</returns>
        [PageAuthorize("organizationalStructure")]
        public async Task<JsonResult> GetOrganizationalRelationshipsAsync(string query)
        {
            try
            {
                var criteria = new OrganizationalPersonPositionQueryCriteria()
                {
                    SearchString = query
                };
                var organizationalRelationships = await ServiceClient.GetOrganizationalPersonPositionsAsync(criteria);
                return Json(organizationalRelationships, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info(e, "Failed to find person");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.OrganizationalStructureResources, "ErrorNoRelationship"), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the Organizational Person Position for the given ID
        /// </summary>
        /// <param name="id">Organizational Person Position ID</param>
        /// <returns>Organizational Person Position as JSON</returns>
        [PageAuthorize("organizationalStructure")]
        public async Task<JsonResult> GetOrganizationalPersonPositionAsync(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("id", "ID is required to retrieve organizational person position.");
                }
                var organizationalRelationships = await ServiceClient.GetOrganizationalPersonPositionByIdAsync(id);
                return Json(organizationalRelationships, JsonRequestBehavior.AllowGet);
            }
            catch (ArgumentNullException ane)
            {
                Logger.Info(ane, "Failed to retrieve Organizational Person Position due to null or empty ID.");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.OrganizationalStructureResources, "ErrorNoRelationship"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info(e, "Failed to retrieve Organizational Person Position for ID: " + id);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.OrganizationalStructureResources, "ErrorNoRelationship"), JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Add Relationship
        /// <summary>
        /// Add a relationship
        /// </summary>
        /// <param name="organizationalRelationshipJson">Json for added organizational relationship</param>
        /// <returns>The added relationship</returns>
        [PageAuthorize("organizationalStructure")]
        [HttpPost]
        public async Task<JsonResult> CreateOrganizationalRelationshipAsync(string organizationalRelationshipJson)
        {
            try
            {
                var organizationalRelationshipDto = JsonConvert.DeserializeObject<OrganizationalRelationship>(organizationalRelationshipJson);
                var organizationalRelationshipAdded = await ServiceClient.CreateOrganizationalRelationshipAsync(organizationalRelationshipDto);
                return Json(organizationalRelationshipAdded);

            }
            catch (Exception e)
            {
                Logger.Info(e, "Failed to add relationship");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.OrganizationalStructureResources, "ErrorAddingRelationship"));
            }
        }
        #endregion

        #region Update Relationship
        /// <summary>
        /// Update relationship
        /// </summary>
        /// <param name="organizationalRelationshipJson">Json for updated organizational relationship</param>
        /// <returns>The updated relationship</returns>
        [PageAuthorize("organizationalStructure")]
        public async Task<JsonResult> UpdateOrganizationalRelationshipAsync(string organizationalRelationshipJson)
        {
            try
            {
                var organizationalRelationshipDto = JsonConvert.DeserializeObject<OrganizationalRelationship>(organizationalRelationshipJson);
                var organizationalRelationshipUpdated = await ServiceClient.UpdateOrganizationalRelationshipAsync(organizationalRelationshipDto);
                return Json(organizationalRelationshipUpdated);

            }
            catch (Exception e)
            {
                Logger.Info(e, "Failed to update relationship");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.OrganizationalStructureResources, "ErrorUpdatingRelationship"));
            }
        }
        #endregion

        #region Delete Relationship
        /// <summary>
        /// Delete relationship
        /// </summary>
        /// <param name="id">Id for organizational relationship to be deleted</param>
        /// <returns>Empty object if successful</returns>
        [PageAuthorize("organizationalStructure")]
        public async Task<JsonResult> DeleteOrganizationalRelationshipAsync(string id)
        {
            try
            {
                await ServiceClient.DeleteOrganizationalRelationshipAsync(id);
                return Json(new object { });
            }
            catch (Exception e)
            {
                Logger.Info(e, "Failed to delete relationship");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.OrganizationalStructureResources, "ErrorDeletingRelationship"));
            }
        }
        #endregion

        #region Get Configuration
        /// <summary>
        /// Get the configuration for organizational relationships
        /// </summary>
        /// <returns>Organizational relationship configuration as JSON</returns>
        [PageAuthorize("organizationalStructure")]
        public async Task<JsonResult> GetOrganizationalRelationshipConfigurationAsync()
        {
            try
            {
                var orgRelConfigurationTask = ServiceClient.GetOrganizationalRelationshipConfigurationAsync();
                var rolesTask = ServiceClient.GetRolesAsync();
                await Task.WhenAll(orgRelConfigurationTask, rolesTask);
                var configurationModel = new OrganizationalRelationshipConfigurationModel(orgRelConfigurationTask.Result, rolesTask.Result, CurrentUser);

                return Json(configurationModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info(e, "Error occurred retrieving organizational relationship configuration.");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(GlobalResources.GetString(GlobalResourceFiles.OrganizationalStructureResources, "ErrorRetrievingOrganizationalRelationshipConfiguration"), JsonRequestBehavior.AllowGet);
            }

        }
        #endregion
    }
}