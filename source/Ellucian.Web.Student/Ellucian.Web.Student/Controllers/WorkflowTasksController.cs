﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.Workflow;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Controllers
{
    /// <summary>
    /// The Workflow Tasks Controller contains actions related to retrieving and updating a user's workflow tasks
    /// </summary>
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class WorkflowTasksController : BaseStudentController
    {
        public WorkflowTasksController(Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        /// <summary>
        /// Retrieves the users workflow tasks from the API
        /// </summary>
        /// <returns>A list of <see cref="WorkTaskModel"/> as JSON</returns>
        public async Task<JsonResult> GetWorkTasksAsync()
        {
            var workTaskDtos = await ServiceClient.GetWorkTasksAsync(GetEffectivePersonId());
            var selfServiceWorkTasks = workTaskDtos.Where(task => task.TaskProcess != WorkTaskProcess.None);
            var workTaskModels = new List<WorkTaskModel>();
            foreach (var taskDto in selfServiceWorkTasks)
            {
                var taskModel = new WorkTaskModel(taskDto.Id, taskDto.Category, taskDto.Description, taskDto.TaskProcess);
                workTaskModels.Add(taskModel);
            }
            return Json(workTaskModels, JsonRequestBehavior.AllowGet);
        }
    }
}