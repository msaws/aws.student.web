﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.PersonRestrictions;
using Ellucian.Web.Student.Utility;
using slf4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class PersonRestrictionsController : BaseStudentController
    {
        /// <summary>
        /// Injection constructor
        /// </summary>
        /// <param name="settings">Settings base model</param>
        /// <param name="logger">Common interface of an arbitrary implementation that provides logging capabilities</param>
        public PersonRestrictionsController(Settings settings, ILogger logger)
            : base(settings, logger)
        {}

        /// <summary>
        /// Gets and serializes the given user's Person Restrictions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<JsonResult> GetPersonRestrictionsModelAsync(string id)
        {
            // Check for a person ID; if none is supplied, use the person ID of the authenticated user
            if (string.IsNullOrEmpty(id))
            {
                id = CurrentUser.PersonId;
            }

            // Initialize data sources
            RestrictionConfiguration restrictionConfiguration = null;
            IEnumerable<PersonRestriction> personRestrictions = null;

            // Try to retrieve restriction configuration and restriction data
            try
            {
                restrictionConfiguration = await ServiceClient.GetRestrictionConfigurationAsync();
                personRestrictions = await ServiceClient.GetStudentRestrictions2Async(id, true);
            }
            catch (HttpRequestFailedException hrex)
            {
                if (hrex.StatusCode == HttpStatusCode.Forbidden)
                {
                    Response.StatusCode = (int)HttpStatusCode.Forbidden;
                }
                string message = string.Format(GlobalResources.GetString(GlobalResourceFiles.SiteResources, "PersonRestrictionsWidgetError"), id);
                Logger.Info(hrex, message);
            }
            catch (Exception ex)
            {
                string message= string.Format(GlobalResources.GetString(GlobalResourceFiles.SiteResources, "PersonRestrictionsWidgetError"), id);
                Logger.Info(ex, message);
            }

            // Build the model
            PersonRestrictionsModel model = new PersonRestrictionsModel(restrictionConfiguration, personRestrictions);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}