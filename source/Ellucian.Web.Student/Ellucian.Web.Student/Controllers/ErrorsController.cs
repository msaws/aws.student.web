﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using slf4net;
using System.Net;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Controllers
{
    /// <summary>
    /// Controller for handling errors in the Colleague Self-Service application
    /// </summary>
    public class ErrorsController : Controller
    {
        private Settings settings;
        private ILogger logger;

        /// <summary>
        /// Creates a new <see cref="ErrorsController"/> instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public ErrorsController(Settings settings, ILogger logger)
        {
            this.settings = settings;
            this.logger = logger;
        }

        /// <summary>
        /// Handles non-AJAX errors
        /// </summary>
        /// <returns>ErrorOccurred view</returns>
        public ActionResult ErrorOccurred(HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            string errorCode = string.Format("Code{0}", ((int)statusCode).ToString());
            string errorMessage = GlobalResources.GetString(GlobalResourceFiles.ErrorMessageResources, errorCode);
            if (string.IsNullOrEmpty(errorMessage)) errorMessage = GlobalResources.GetString(GlobalResourceFiles.ErrorMessageResources, "Code500");
            
            ViewBag.ErrorMessage = errorMessage;
            return View("Error");
        }

        /// <summary>
        /// Handles AJAX errors
        /// </summary>
        /// <returns>JSON-formatted error message</returns>
        public JsonResult AjaxErrorOccurred()
        {
            string errorMessage = GlobalResources.GetString(GlobalResourceFiles.SiteResources, "GenericErrorMessage");

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            Response.StatusDescription = errorMessage;
            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }
    }
}