﻿using System;
using System.Web.Mvc;
using Ellucian.Web.Mvc.Controller;
using slf4net;

namespace Ellucian.Web.Student.Controllers
{
    /// <summary>
    /// The HelpController serves help topics for Self-Service
    /// </summary>
    [OutputCache(CacheProfile = "NoCache")]
    public class HelpController : BaseCompressedController
    {
        /// <summary>
        /// HelpController constructor
        /// </summary>
        /// <param name="logger">An instance of a logger</param>
        public HelpController(ILogger logger)
            : base(logger)
        {

        }

        /// <summary>
        /// For a given piece of content, get the relevant help 
        /// </summary>
        /// <param name="contentId">The ID of the piece of content to return</param>
        /// <param name="contentArea">Optional parameter to indicate if the help is specific to an area</param>
        /// <returns>A view containing the help</returns>
        public ActionResult Index(string contentId, string contentArea)
        {
            try
            {
                if (string.IsNullOrEmpty(contentId))
                {
                    throw new ArgumentNullException("contentId");
                }
                if (!string.IsNullOrEmpty(contentArea))
                {
                    contentId = string.Format("~/Areas/{0}/Views/Help/{1}.cshtml", contentArea, contentId);
                }

                return View(contentId);
            }
            catch
            {
                return new HttpNotFoundResult();
            }
        }

        /// <summary>
        /// For a given piece of content, get the relevant help 
        /// </summary>
        /// <param name="contentId">The ID of the piece of content to return</param>
        /// <param name="contentArea">Optional parameter to indicate if the help is specific to an area</param>
        /// <returns>A partial view containing the help, for use embedded in other views</returns>
        public ActionResult Partial(string contentId, string contentArea)
        {
            try
            {
                if (string.IsNullOrEmpty(contentId))
                {
                    throw new ArgumentNullException("contentId");
                }
                if (!string.IsNullOrEmpty(contentArea))
                {
                    contentId = string.Format("~/Areas/{0}/Views/Help/{1}.cshtml", contentArea, contentId);
                }

                return PartialView(contentId);
            }
            catch
            {
                return new HttpNotFoundResult();
            }
        }
    }
}
