﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Mvc.Configuration;
using Ellucian.Web.Mvc.Data.Repositories;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Mvc.Theme;
using Ellucian.Web.Student.Configuration;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models.Admin;
using Ellucian.Web.Student.Utility;
using Hedtech.Identity;
using Hedtech.Identity.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using slf4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace Ellucian.Web.Student.Controllers
{
    /// <summary>
    /// Controller to allow users to update site settings, like menus, pages, cache, etc.
    /// </summary>
    [OutputCache(CacheProfile = "NoCache")]
    public class AdminController : BaseStudentController
    {
        private const string ApplicationCertsPath = "app_data\\certs";
        private const string RecycleAppPoolFailedMessage = "The settings were saved, but restarting the application failed. Either manually recycle the application pool, or verify that the web.config file is not read only and that the application pool has write permissions.";
        private string[] CertificateExtensions = { ".cer", ".crt" };
        private string[] PrivateKeyExtensions = { ".pfx", ".p12" };
        private string[] FaviconExtensions = { ".ico" };

        private ISettingsService settingsService;
        private ISiteService siteService;
        private IThemeService themeService;

        public AdminController(ISettingsService settingsService, Settings settings, ILogger logger, ISiteService siteService, IThemeService themeService)
            : base(settings, logger)
        {
            this.settingsService = settingsService;
            this.siteService = siteService;
            this.themeService = themeService;
        }

        public AdminController(Settings settings, ILogger logger)
            : base(settings, logger)
        {

        }

        [LinkHelp]
        public ActionResult Configuration()
        {
            if (UserIsNotAdminOrLocal)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return View();
            }
            var settings = DependencyResolver.Current.GetService<Settings>();
            var htmlEncodedSettings = HtmlEncodedSettings;

            ViewBag.JSON = JsonConvert.SerializeObject(htmlEncodedSettings);
            return View();
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult Branding()
        {
            var brandingModel = BrandingViewModel;
            ViewBag.JSON = JsonConvert.SerializeObject(brandingModel);
            return View();
        }

        [HttpPost]
        [LinkHelp]
        public ActionResult Branding(string action, string name, HttpPostedFileBase newFile)
        {
            List<Models.Notification> messages = new List<Models.Notification>();
            try
            {
                if (UserIsNotAdminOrLocal)
                {
                    Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    return View();
                }
                else
                {
                    action = action.Trim('"');
                    var appRootPath = Server.MapPath("~");
                    switch (action.ToLower())
                    {
                        case "add":
                            if (newFile == null)
                            {
                                string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "BrandingFileNotSpecifiedMessage");
                                Logger.Error(message);
                                messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                            }
                            else
                            {
                                if (Directory.Exists(appRootPath))
                                {
                                    var exts = FaviconExtensions;
                                    if (exts.Contains(Path.GetExtension(newFile.FileName.ToLower())))
                                    {
                                        string filename = Path.Combine(appRootPath, Path.GetFileName(newFile.FileName));
                                        newFile.SaveAs(filename);
                                    }
                                    else
                                    {
                                        string message = string.Format("\"{0}\" is not an acceptable file type ({1})", newFile.FileName, string.Join(", ", exts));
                                        Logger.Error(message);
                                        messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                                    }
                                }
                                else
                                {
                                    string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "BrandingRootPathInaccessibleMessage");
                                    Logger.Error(message);
                                    messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                                }
                            }
                            break;

                        case "remove":
                            if (string.IsNullOrEmpty(name))
                            {
                                string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "BrandingNoNameSpecifiedMessage");
                                Logger.Error(message);
                                messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                            }
                            else
                            {
                                name = name.Trim('"');
                                if (!string.IsNullOrEmpty(name))
                                {
                                    string filename = Path.Combine(appRootPath, name);
                                    if (System.IO.File.Exists(filename))
                                    {
                                        System.IO.File.Delete(filename);
                                    }
                                }
                            }
                            break;
                        default:
                            string errorMessage = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "BrandingNoActionSpecifiedMessage");
                            throw new ApplicationException(errorMessage);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                messages.Add(new Models.Notification(GlobalResources.GetString(GlobalResourceFiles.AdminResources, "BrandingErrorMessage"), Models.NotificationType.Error));
            }

            var brandingModel = BrandingViewModel;
            brandingModel.Messages = messages;
            ViewBag.JSON = JsonConvert.SerializeObject(brandingModel);
            return View("Branding");
        }

        [ActionName("PingWebApi")]
        [HttpPost]
        public async Task<JsonResult> PingWebApiAsync(string url)
        {
            string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "WebApiAvailableMessage");
            try
            {
                if (string.IsNullOrEmpty(url))
                {
                    message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "WebApiUrlNotSpecifiedMessage");
                    throw new ArgumentNullException(url);
                }
                if (UserIsNotAdminOrLocal)
                {
                    message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "WebApiNotAuthorizedMessage");
                    Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    return Json(message);
                }
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(string.Format("{0}/version", url.TrimEnd('/')));
                    if (!response.IsSuccessStatusCode)
                    {
                        var baseMessage = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "WebApiNotAvailableMessage");
                        message = string.Format(baseMessage, (int)response.StatusCode, response.ReasonPhrase);
                    }
                    else
                    {
                        try
                        {
                            var result = await response.Content.ReadAsStringAsync();
                            if (!string.IsNullOrEmpty(result))
                            {
                                JToken token = JObject.Parse(result);
                                string productVersion = (string)token.SelectToken("ProductVersion");
                                if (productVersion != null)
                                {
                                    message = string.Format(GlobalResources.GetString(GlobalResourceFiles.AdminResources, "WebApiVersionFormatString"), message, productVersion);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var baseMessage = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "WebApiNotAvailableMessage");
                message = string.Format(baseMessage, string.Empty, ex.Message);
                Logger.Error(ex, message);
            }
            return Json(message);
        }

        public JsonResult CurrentDate(string dateFormat)
        {
            if (string.IsNullOrEmpty(dateFormat)) dateFormat = "d";
            try
            {
                if (dateFormat == "Default")
                {
                    return Json(FormatUtility.FormatAsShortDate(DateTime.Now), JsonRequestBehavior.AllowGet);
                }
                return Json(string.Format("{0:" + dateFormat + "}", DateTime.Now), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string error = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "CurrentDateFailedError");
                Logger.Error(e, error);
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveSettings(Settings settings)
        {

            if (UserIsNotAdminOrLocal)
            {
                string error = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsNotAuthorizedMessage");
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return Json(error);
            }
            if (settings == null)
            {
                // Nothing to save
                string error = string.Format(GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsInvalidConfigurationFormatString"),
                    GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsValidSettingsConfiguration"));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(error);
            }
            if (string.IsNullOrEmpty(settings.ApiBaseUrl))
            {
                // No API Url set
                string error = string.Format(GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsInvalidConfigurationFormatString"),
                    GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsValidWebApiBaseUrl"));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(error);
            }
            if (string.IsNullOrEmpty(settings.UniqueCookieId))
            {
                // No Unique Cookie set
                string error = string.Format(GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsInvalidConfigurationFormatString"),
                    GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsValidUniqueCookieId"));
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(error);
            }
            if (string.IsNullOrEmpty(settings.GuestUsername) || string.IsNullOrEmpty(settings.GuestPassword))
            {
                var existingSettings = DependencyResolver.Current.GetService<Settings>();
                settings.GuestUsername = existingSettings.GuestUsername;
                settings.GuestPassword = existingSettings.GuestPassword;
            }
            try
            {
                settingsService.Update(settings);
                if (!RecycleAppPool())
                {
                    return Json(RecycleAppPoolFailedMessage);
                }
                return Json(settings);
            }
            catch (Exception ex)
            {
                string error = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "SaveSettingsGenericErrorMessage");
                Logger.Error(ex, error);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(error);
            }
        }


        [LinkHelp]
        public ActionResult Index()
        {
            if (UserIsNotAdminOrLocal)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }

            return View();
        }

        [LinkHelp]
        public ActionResult About()
        {
            if (UserIsNotAdminOrLocal)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return View();
            }

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            Version version = new Version(fvi.FileVersion);
            return View(version);
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult Site()
        {
            var site = SitemapUtility.SortSite(siteService.Get());

            ViewBag.JSON = JsonConvert.SerializeObject(site);
            return View();
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult Menu(string id)
        {
            try
            {
                var menu = siteService.Get().Menus.First(m => m.Id == id);
                ViewBag.Title = "Menu Configuration: " + menu.Label;
                ViewBag.JSON = JsonConvert.SerializeObject(menu);
                return View();
            }
            catch
            {
                return new HttpNotFoundResult();
            }
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult Submenu(string id)
        {
            try
            {
                var submenu = siteService.Get().Submenus.First(m => m.Id == id);
                ViewBag.Title = "Submenu Configuration: " + submenu.Label;
                ViewBag.JSON = JsonConvert.SerializeObject(submenu);
                return View();
            }
            catch
            {
                return new HttpNotFoundResult();
            }
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult Page(string id)
        {
            try
            {
                var page = siteService.Get().Pages.First(p => p.Id == id);
                ViewBag.Title = "Page Configuration: " + page.Label;
                ViewBag.JSON = JsonConvert.SerializeObject(page);
                return View();
            }
            catch
            {
                return new HttpNotFoundResult();
            }
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult Link(string id, string errorMessage)
        {
            try
            {
                // If id is null, create a new link instead of looking for an existing link
                Link link = null;
                if (string.IsNullOrEmpty(id))
                {
                    link = new Link();
                }
                else
                {
                    link = siteService.Get().Links.First(l => l.Id == id);
                    ViewBag.Id = link.Id;
                    ViewBag.Deletable = true;
                }
                ViewBag.Title = "Link Configuration: ";
                ViewBag.JSON = JsonConvert.SerializeObject(link);
                ViewBag.ErrorMessage = errorMessage;
                return View();
            }
            catch
            {
                return new HttpNotFoundResult();
            }
        }

        /// <summary>
        /// Retrieves an array of users matching the specified term
        /// </summary>
        /// <param name="term">Criterion for identifying matching users</param>
        /// <returns>Array of users matching the specified term</returns>
        [PageAuthorize("admin")]
        public async Task<JsonResult> Users(string term)
        {
            string[] users = new string[] {};
            try 
            {
                if (string.IsNullOrEmpty(term))
                {
                    Logger.Info(GlobalResources.GetString(GlobalResourceFiles.AdminResources, "UsersNoTermSpecifiedMessage"));
                }
                else
                {
                    var results = await ServiceClient.GetUsersAsync(term);
                    users = results.Select(u => u.Login).ToArray();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                users = new string[] {};
            }
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        [PageAuthorize("admin")]
        public async Task<JsonResult> Roles(string term)
        {
            var uTerm = term.ToUpper();
            var roles = await ServiceClient.GetRolesAsync();
            return Json(roles.Select(r => r.Title).Where(t => t.ToUpper().StartsWith(uTerm)).ToArray(), JsonRequestBehavior.AllowGet);
        }

        [PageAuthorize("admin")]
        public string Links(string term)
        {
            var uTerm = term.ToUpper();
            var links = siteService.Get().Links;
            var trimmedLinks = links.Where(l => l.Id.ToUpper().StartsWith(uTerm)).ToArray();
            var menuItemLinks = new List<MenuItemLink>();
            foreach (var link in trimmedLinks)
            {
                menuItemLinks.Add(new MenuItemLink() { Link = link, ItemType = Mvc.Domain.Entities.MenuItemType.Link });
            }
            var resultsJson = JsonConvert.SerializeObject(menuItemLinks, new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.Objects
            });
            return resultsJson;
        }

        [PageAuthorize("admin")]
        public async Task<JsonResult> Messages(string term)
        {
            var uTerm = term.ToUpper();
            var messages = await ServiceClient.GetCachedMiscellaneousTextsAsync();
            return Json(messages.Select(m => m.Id).Where(t => t.ToUpper().StartsWith(uTerm)).ToArray(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update a menu with the object provided
        /// </summary>
        /// <param name="item">A JSON string representing the updated menu being saved.</param>
        /// <returns>The updated menu</returns>
        [HttpPost]
        [PageAuthorize("admin")]
        public ActionResult SaveMenu(string item)
        {
            try
            {
                // Deserialize the submenu with JsonConvert because it can handle sub-classes (the MS default JSON deserialization cannot).
                var menu = JsonConvert.DeserializeObject<Menu>(item);

                var site = siteService.Get();
                int pos = -1;
                for (int i = 0; i < site.Menus.Count; i++)
                {
                    var m = site.Menus[i];
                    if (m.Id == menu.Id)
                    {
                        pos = i;
                        break;
                    }
                }
                if (pos != -1)
                {
                    site.Menus[pos] = menu;
                }
                siteService.Update(site);
                if (!RecycleAppPool())
                {
                    return Json(RecycleAppPoolFailedMessage);
                }
                return Json(menu);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Logger.Error(ex.Message);
                return this.Content("Unable to update menu", "application/json");
            }
        }

        /// <summary>
        /// Update a submenu with the object provided
        /// </summary>
        /// <param name="item">A JSON string representing the updated submenu being saved.</param>
        /// <returns>The updated submenu</returns>
        [HttpPost]
        [PageAuthorize("admin")]
        public ActionResult SaveSubmenu(string item)
        {
            try
            {
                // Deserialize the submenu with JsonConvert because it can handle sub-classes (the MS default JSON deserialization cannot).
                var submenu = JsonConvert.DeserializeObject<Menu>(item);

                var site = siteService.Get();
                int pos = -1;
                for (int i = 0; i < site.Submenus.Count; i++)
                {
                    var m = site.Submenus[i];
                    if (m.Id == submenu.Id)
                    {
                        pos = i;
                        break;
                    }
                }
                if (pos != -1)
                {
                    site.Submenus[pos] = submenu;
                }
                siteService.Update(site);
                if (!RecycleAppPool())
                {
                    return Json(RecycleAppPoolFailedMessage);
                }
                return Json(submenu);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Logger.Error(ex.Message);
                return this.Content("Unable to update submenu", "application/json");
            }
        }

        /// <summary>
        /// Update a page with the object provided
        /// </summary>
        /// <param name="item">A JSON string representing the updated page being saved.</param>
        /// <returns>The updated page</returns>
        [HttpPost]
        [PageAuthorize("admin")]
        public ActionResult SavePage(string item)
        {
            try
            {
                // Deserialize the item with JsonConvert because it can handle sub-classes (the MS default JSON deserialization cannot).
                var page = JsonConvert.DeserializeObject<Page>(item);

                var site = siteService.Get();
                int pos = -1;
                for (int i = 0; i < site.Pages.Count; i++)
                {
                    var p = site.Pages[i];
                    if (p.Id == page.Id)
                    {
                        pos = i;
                        break;
                    }
                }
                if (pos != -1)
                {
                    // If Area is set to "" in the sitemap.config for a page it is coming back null.
                    // Change to an empty string to allow the update.
                    if (page.Area == null)
                    {
                        page.Area = string.Empty;
                    }
                    site.Pages[pos] = page;
                }
                siteService.Update(site);
                if (!RecycleAppPool())
                {
                    return Json(RecycleAppPoolFailedMessage);
                }
                return Json(page);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Logger.Error(ex.Message);
                return this.Content("Unable to update page", "application/json");
            }
        }

        /// <summary>
        /// Update a link with the object provided
        /// </summary>
        /// <param name="item">A JSON string representing the updated link being saved.</param>
        /// <returns>The updated link</returns>
        [HttpPost]
        [PageAuthorize("admin")]
        public ActionResult SaveLink(string item)
        {
            try
            {
                // Deserialize the item with JsonConvert because it can handle sub-classes (the MS default JSON deserialization cannot).
                var link = JsonConvert.DeserializeObject<Link>(item);

                // Check for valid link
                string message = null;
                if (string.IsNullOrEmpty(link.Id) || link.Id.Any(x => Char.IsWhiteSpace(x)))
                {
                    message = "Please provide a valid ID for the link.  It must not contain white space.";
                }
                else if (string.IsNullOrEmpty(link.Url) || !(link.Url.Contains("http://") || link.Url.Contains("https://")))
                {
                    message = "Please provide a valid URL for the link. It should start with 'http://' or 'https://'.";
                }
                else if (string.IsNullOrEmpty(link.Text))
                {
                    message = "Please provide text for the link.";
                }

                if (message != null)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return this.Content(message);
                }

                var site = siteService.Get();
                int pos = -1;
                for (int i = 0; i < site.Links.Count; i++)
                {
                    var l = site.Links[i];
                    if (l.Id == link.Id)
                    {
                        pos = i;
                        break;
                    }
                }
                // If the link is found in the existing sitemap, pos will be greater than -1
                if (pos != -1)
                {
                    site.Links[pos] = link;
                }
                else // If the link was not found, pos will be -1 and we should add it
                {
                    site.Links.Add(link);
                }
                siteService.Update(site);
                if (!RecycleAppPool())
                {
                    return Json(RecycleAppPoolFailedMessage);
                }
                return Json(link);
            }
            catch (Exception ex)
            {
                string message = "Unable to update link."; ;
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = message;
                Logger.Error(ex.Message);
                return this.Content(message);
            }
        }

        /// <summary>
        /// Delete the link with the id provided, including removing references from menus and submenus
        /// </summary>
        /// <param name="id">A string representing the id of the link being deleted.</param>
        /// <returns>A redirect to the sitemap page</returns>
        [HttpPost]
        [PageAuthorize("admin")]
        public ActionResult DeleteLink(string id)
        {
            try
            {
                var site = siteService.Get();

                var link = site.Links.FirstOrDefault(x => x.Id == id);

                site.Links.Remove(link);

                // Make sure none of the menus or submenus have orphaned references to the link we just removed
                foreach (var menu in site.Menus)
                {
                    var items = menu.Items.ToList();
                    for (int i = items.Count - 1; i >= 0; i--)
                    {
                        if (items.ElementAt(i).ItemType == Mvc.Domain.Entities.MenuItemType.Link)
                        {
                            var linkItem = (MenuItemLink)items.ElementAt(i);
                            if (linkItem.Link.Id == id)
                            {
                                items.RemoveAt(i);
                            }
                        }
                    }
                    menu.Items = items;
                }
                foreach (var submenu in site.Submenus)
                {
                    var items = submenu.Items.ToList();
                    for (int i = items.Count - 1; i >= 0; i--)
                    {
                        if (items.ElementAt(i).ItemType == Mvc.Domain.Entities.MenuItemType.Link)
                        {
                            var linkItem = (MenuItemLink)items.ElementAt(i);
                            if (linkItem.Link.Id == id)
                            {
                                items.RemoveAt(i);
                            }
                        }
                    }
                    submenu.Items = items;
                }

                siteService.Update(site);
                if (!RecycleAppPool())
                {
                    return Json(RecycleAppPoolFailedMessage);
                }
                return RedirectToAction("Site");

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToAction("Link", new { id = id, errorMessage = "Unable to delete link." });
            }
        }

        [HttpPost]
        [PageAuthorize("admin")]
        public ActionResult SaveSite(Site site)
        {
            try
            {
                siteService.Update(site);
                return Json(site);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return this.Content(ex.Message, "application/json");
            }
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult ThemeEditor()
        {
            ThemeEditorModel model = new ThemeEditorModel();
            model.Theme = themeService.Get();
            ViewBag.JSON = JsonConvert.SerializeObject(model);
            return View();
        }

        [HttpPost]
        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult ThemeEditor(string themeEditorModel, string action)
        {
            ThemeEditorModel model = null;
            if (!string.IsNullOrEmpty(themeEditorModel))
            {
                model = JsonConvert.DeserializeObject<ThemeEditorModel>(themeEditorModel);
                model.Messages.Clear();
            }

            if (model == null)
            {
                model = new ThemeEditorModel();
            }

            if (!string.IsNullOrEmpty(action))
            {
                action = JsonConvert.DeserializeObject<string>(action);

                if (action.Equals("save"))
                {
                    // make a backup (as of right now, the save action does this the first time).
                    //if (themeService.CreateBackup(string.Empty))
                    //{
                    //    model.Messages.Add(new Models.Notification("Backup Created", Models.NotificationType.Success, true));
                    //}

                    // save the file
                    if (themeService.Update(model.Theme))
                    {
                        model.Messages.Add(new Models.Notification("Theme Saved!", Models.NotificationType.Success, true));
                    }
                    else
                    {
                        model.Messages.Add(new Models.Notification("Unable to update theme.", Models.NotificationType.Error, false));
                    }
                }
                else if (action.Equals("restore"))
                {
                    if (themeService.RestoreBackup())
                    {
                        model.Messages.Add(new Models.Notification("Default Theme Restored", Models.NotificationType.Success, true));
                    }
                    else
                    {
                        model.Messages.Add(new Models.Notification("Unable to restore theme.", Models.NotificationType.Error, false));
                    }
                }
            }

            model.Theme = null;
            model.Theme = themeService.Get();

            ViewBag.JSON = JsonConvert.SerializeObject(model);
            return View();
        }

        [PageAuthorize("admin")]
        [LinkHelp]
        public ActionResult CacheManager()
        {
            return View();
        }

        [PageAuthorize("admin")]
        [HttpPost]
        [LinkHelp]
        public ActionResult ClearCache()
        {
            try
            {
                ClearTemporaryInternetFiles();
                ServiceClientCache.ClearCache(new System.Collections.Generic.List<string>() { "ServiceClient_" });
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
            }
            return RedirectToAction("Index");
        }

        [LinkHelp]
        public ActionResult Authentication()
        {
            if (UserIsNotAdminOrLocal)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return View();
            }

            ViewBag.JSON = JsonConvert.SerializeObject(BuildSsoConfigurationModel());
            return View();
        }

        [HttpPost]
        [LinkHelp]
        public ActionResult Authentication(string ssoSettings)
        {
            if (UserIsNotAdminOrLocal)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return View();
            }

            List<Models.Notification> messages = new List<Models.Notification>();

            if (!string.IsNullOrEmpty(ssoSettings))
            {
                AuthenticationConfigurationModel model = JsonConvert.DeserializeObject<AuthenticationConfigurationModel>(ssoSettings);
                if (model != null)
                {
                    try
                    {
                        Settings appSettings = settingsService.Get();

                        // Encrypt the guest username and password, if given
                        if (!string.IsNullOrEmpty(model.GuestUsername))
                        {
                            appSettings.GuestUsername = EncryptionUtility.Encrypt(model.GuestUsername);
                        }
                        else
                        {
                            appSettings.GuestUsername = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(model.GuestPassword))
                        {
                            appSettings.GuestPassword = EncryptionUtility.Encrypt(model.GuestPassword);
                        }
                        else
                        {
                            appSettings.GuestPassword = string.Empty;
                        }
                        settingsService.Update(appSettings);


                        SamlConfigurationRepository scr = new SamlConfigurationRepository(Logger);
                        var fullCertsPath = Path.Combine(Server.MapPath("~"), ApplicationCertsPath);
                        IdentityManagementSettings samlConfig = scr.GetIdentityManagementSettings();

                        samlConfig.IsIdmEnabled = model.AuthenticationType == "SAML2";
                        samlConfig.UseEncryptedAssertions = model.UseEncryptedAssertions;
                        samlConfig.IdentityProviderUrl = model.IdentityProviderUrl;
                        samlConfig.IdentityProviderLogoutUrl = model.IdentityProviderLogoutUrl;
                        samlConfig.SamlUsernameAttribute = model.UsernameClaim;
                        samlConfig.ServiceProviderId = model.ServiceProviderId;
                        samlConfig.IdentityProviderPublicKeyFilename = Path.Combine(fullCertsPath, model.IdentityProviderPublicKey);
                        samlConfig.IdentityProviderPublicKeyPassword = model.IdentityProviderPublicKeyPassword;
                        samlConfig.ServiceProviderBaseUrl = model.BaseUrl;
                        samlConfig.ServiceProviderPrivateKeyFilename = Path.Combine(fullCertsPath, model.ServiceProviderPrivateKey);
                        samlConfig.ServiceProviderPrivateKeyPassword = model.ServiceProviderPrivateKeyPassword;
                        samlConfig.ServiceProviderSsoPath = "SamlSso";
                        samlConfig.ApplicationLogoutPath = "SamlSso/SignOut";
                        samlConfig.ProxyUserName = model.ProxyUsername;
                        samlConfig.ProxyPassword = model.ProxyPassword;
                        samlConfig.IdentityProviderIssuer = model.IdentityProviderIssuer;
                        samlConfig.SamlClockSkew = model.SamlClockSkew;
                        samlConfig.SetNameIdPolicy = model.SetNameIdPolicy;
                        samlConfig.SPNameQualifier = model.SPNameQualifier;

                        scr.UpdateIdentityManagementSettings(samlConfig);

                        messages.Add(new Models.Notification("Configuration saved", Models.NotificationType.Success));
                    }
                    catch (Exception e)
                    {
                        messages.Add(new Models.Notification(string.Format("Error updating configuration: {0}", e.Message), Models.NotificationType.Error));
                    }
                }
            }

            var responseModel = BuildSsoConfigurationModel();
            responseModel.Messages = messages;
            ViewBag.JSON = JsonConvert.SerializeObject(responseModel);
            return View();
        }

        public async Task<ActionResult> TestSsoProxyLogin(string username, string password)
        {
            if (UserIsNotAdminOrLocal)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return Json("Not authorized");
            }
            try
            {
                var token = await ServiceClient.Login2Async(username, password);
                if (!string.IsNullOrEmpty(token))
                {
                    try
                    {
                        await ServiceClient.LogoutAsync(token);
                    }
                    catch (Exception)
                    {
                    }
                }
                return Json("Login successful");
            }
            catch (LoginException le)
            {
                return Json(le.Message);
            }
            catch (PasswordExpiredException exp)
            {
                return Json(exp.Message);
            }
            catch (Exception e)
            {
                return Json("Proxy login failed: " + e.Message);
            }
        }

        public async Task<ActionResult> TestLogin(string username, string password, string success, string failed)
        {
            try
            {
                var token = await ServiceClient.Login2Async(username, password);
                if (!string.IsNullOrEmpty(token))
                {
                    try
                    {
                        await ServiceClient.LogoutAsync(token);
                    }
                    catch (Exception)
                    {
                    }
                }
                return Json(success);
            }
            catch (LoginException le)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(le.Message);
            }
            catch (PasswordExpiredException exp)
            {
                Response.StatusCode = (int)HttpStatusCode.Forbidden;
                return Json(exp.Message);
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return Json(failed + " " + e.Message);
            }
        }

        [LinkHelp]
        public ActionResult ApplicationCertificates()
        {
            if (UserIsNotAdminOrLocal)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return View();
            }

            // build-up data and display appropriate page
            var certificates = new List<ApplicationCertificateModel>();
            var privateKeys = new List<ApplicationCertificateModel>();
            GetCertificateFilenames(out certificates, out privateKeys);

            var model = new ApplicationCertificatesAndKeysModel()
            {
                CurrentCertificates = certificates,
                CurrentKeys = privateKeys
            };

            if (Request.IsAjaxRequest())
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.JSON = JsonConvert.SerializeObject(model);
                return View();
            }
        }

        /// <summary>
        /// Processes an application certificate addition or removal
        /// </summary>
        /// <param name="action">Action to be performed; e.g. Add or Remove</param>
        /// <param name="name">Certificate name</param>
        /// <param name="newFile">Uploaded file</param>
        /// <returns>Updated <see cref="ApplicationCertificatesAndKeysModel"/> model.</returns>
        [HttpPost]
        [LinkHelp]
        public ActionResult ApplicationCertificates(string action, string name, HttpPostedFileBase newFile)
        {
            List<Models.Notification> messages = new List<Models.Notification>();
            try
            {
                if (UserIsNotAdminOrLocal)
                {
                    Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    return View();
                }
                if (string.IsNullOrEmpty(action))
                {
                    string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "ApplicationCertificatesNoActionSpecifiedMessage");
                    throw new ApplicationException(message);
                }
                else
                {
                    action = action.Trim('"');
                    var fullCertsPath = Path.Combine(Server.MapPath("~"), ApplicationCertsPath);
                    switch (action.ToLower())
                    {
                        case "add":
                            if (newFile == null)
                            {
                                string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "ApplicationCertificatesFileNotSpecifiedMessage");
                                Logger.Error(message);
                                messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                            }
                            else
                            {
                                if (!Directory.Exists(fullCertsPath))
                                {
                                    Directory.CreateDirectory(fullCertsPath);
                                }

                                var exts = CertificateExtensions.Concat(PrivateKeyExtensions);
                                if (exts.Contains(Path.GetExtension(newFile.FileName.ToLower())))
                                {
                                    string filename = Path.Combine(fullCertsPath, Path.GetFileName(newFile.FileName));
                                    newFile.SaveAs(filename);
                                }
                                else
                                {
                                    string message = string.Format("\"{0}\" is not an acceptable file type ({1})", newFile.FileName, string.Join(", ", exts));
                                    Logger.Error(message);
                                    messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                                }
                            }
                            break;

                        case "remove":
                            if (string.IsNullOrEmpty(name))
                            {
                                string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "ApplicationCertificatesNoNameSpecifiedMessage");
                                Logger.Error(message);
                                messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                            }
                            else
                            {
                                name = name.Trim('"');
                                if (!string.IsNullOrEmpty(name))
                                {
                                    string filename = Path.Combine(fullCertsPath, name);
                                    if (System.IO.File.Exists(filename))
                                    {
                                        System.IO.File.Delete(filename);
                                    }
                                }
                            }
                            break;
                        default:
                            string errorMessage = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "ApplicationCertificatesNoActionSpecifiedMessage");
                            throw new ApplicationException(errorMessage);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                messages.Add(new Models.Notification(GlobalResources.GetString(GlobalResourceFiles.AdminResources, "ApplicationCertificatesErrorMessage"), Models.NotificationType.Error));
            }

            // build-up data and return to appropriate calling page
            var certificates = new List<ApplicationCertificateModel>();
            var privateKeys = new List<ApplicationCertificateModel>();
            GetCertificateFilenames(out certificates, out privateKeys);

            // Make into reusable class
            var model = new ApplicationCertificatesAndKeysModel()
            {
                CurrentCertificates = certificates,
                CurrentKeys = privateKeys,
                Messages = messages
            };

            ViewBag.JSON = JsonConvert.SerializeObject(model);
            return View();
        }

        /// <summary>
        /// Gets the <see cref="AuthenticationConfigurationModel"/> from configuration.
        /// </summary>
        /// <returns><see cref="AuthenticationConfigurationModel"/></returns>
        private AuthenticationConfigurationModel BuildSsoConfigurationModel()
        {
            var model = new AuthenticationConfigurationModel();

            SamlConfigurationRepository scr = new SamlConfigurationRepository(Logger);
            var samlConfig = scr.GetIdentityManagementSettings();

            model.AuthenticationType = samlConfig.IsIdmEnabled ? "SAML2" : "Forms";
            model.IdentityProviderUrl = samlConfig.IdentityProviderUrl;
            model.IdentityProviderLogoutUrl = samlConfig.IdentityProviderLogoutUrl;
            model.UsernameClaim = samlConfig.SamlUsernameAttribute;
            model.ServiceProviderId = samlConfig.ServiceProviderId;
            model.IdentityProviderPublicKey = Path.GetFileName(samlConfig.IdentityProviderPublicKeyFilename);
            model.IdentityProviderPublicKeyPassword = samlConfig.IdentityProviderPublicKeyPassword;
            model.IdentityProviderIssuer = samlConfig.IdentityProviderIssuer;
            model.BaseUrl = samlConfig.ServiceProviderBaseUrl;
            if (string.IsNullOrEmpty(model.BaseUrl))
            {
                model.BaseUrl = Url.Action("Index", "Home", null, Request.Url.Scheme, null);
            }
            model.ServiceProviderPrivateKey = Path.GetFileName(samlConfig.ServiceProviderPrivateKeyFilename);
            model.ServiceProviderPrivateKeyPassword = samlConfig.ServiceProviderPrivateKeyPassword;
            model.SetNameIdPolicy = samlConfig.SetNameIdPolicy ?? false;
            model.SPNameQualifier = samlConfig.SPNameQualifier ?? string.Empty;
            model.UseEncryptedAssertions = samlConfig.UseEncryptedAssertions;
            model.ProxyUsername = samlConfig.ProxyUserName;
            model.ProxyPassword = samlConfig.ProxyPassword;

            var certificates = new List<ApplicationCertificateModel>();
            var privateKeys = new List<ApplicationCertificateModel>();
            GetCertificateFilenames(out certificates, out privateKeys);

            model.AvailablePublicKeys.Add(string.Empty);
            model.AvailablePublicKeys.AddRange(certificates.Select(x => x.Name));
            model.AvailablePrivateKeys.Add(string.Empty);
            model.AvailablePrivateKeys.AddRange(privateKeys.Select(x => x.Name));

            model.CurrentCertificates = certificates;
            model.CurrentKeys = privateKeys;

            model.SamlClockSkew = samlConfig.SamlClockSkew;

            Settings appSettings = settingsService.Get();

            model.GuestUsername = EncryptionUtility.Decrypt(appSettings.GuestUsername);
            model.GuestPassword = EncryptionUtility.Decrypt(appSettings.GuestPassword);

            return model;

        }

        /// <summary>
        /// Searches the application’s certificate directory and returns all certificate and private key names found.
        /// </summary>
        /// <param name="certificates">Out - list of certificates</param>
        /// <param name="privateKeys">Out - list of private keys</param>
        private void GetCertificateFilenames(out List<ApplicationCertificateModel> certificates, out List<ApplicationCertificateModel> privateKeys)
        {
            certificates = new List<ApplicationCertificateModel>();
            privateKeys = new List<ApplicationCertificateModel>();
            var fullCertsPath = Path.Combine(Server.MapPath("~"), ApplicationCertsPath);

            if (Directory.Exists(fullCertsPath))
            {
                var cers = Directory.GetFiles(fullCertsPath, "*.*").Where(file => CertificateExtensions.Contains(Path.GetExtension(file).ToLower())).Select(x => Path.GetFileName(x).ToLower()).ToList();
                var pfxs = Directory.GetFiles(fullCertsPath, "*.*").Where(file => PrivateKeyExtensions.Contains(Path.GetExtension(file).ToLower())).Select(x => Path.GetFileName(x).ToLower()).ToList();

                foreach (var cert in cers)
                {
                    certificates.Add(new ApplicationCertificateModel() { Name = Path.GetFileName(cert) });
                }
                foreach (var key in pfxs)
                {
                    privateKeys.Add(new ApplicationCertificateModel() { Name = Path.GetFileName(key) });
                }
            }
        }

        /// <summary>
        /// Returns true if the request is allowed to administer pages.
        /// </summary>
        private bool UserIsNotAdminOrLocal
        {
            get
            {
                return (CurrentUser == null && !Request.IsLocal) ||
                    (CurrentUser != null && siteService.Get(CurrentUser).Pages.FirstOrDefault(p => p.Id == "admin") == null);
            }
        }

        private bool RecycleAppPool()
        {
            try
            {
                // Touch web.config to force a recycle
                string ConfigPath = HttpContext.Request.PhysicalApplicationPath + "\\web.config";
                System.IO.File.SetLastWriteTimeUtc(ConfigPath, DateTime.UtcNow);
            }
            catch (UnauthorizedAccessException uae)
            {
                Logger.Debug(uae.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Response.StatusDescription = RecycleAppPoolFailedMessage;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieves an HTML-encoded <see cref="Settings">
        /// </summary>
        private Settings HtmlEncodedSettings
        {
            get
            {
                try
                {
                    var settings = DependencyResolver.Current.GetService<Settings>();
                    var htmlEncodedSettings = new Settings();
                    htmlEncodedSettings.EnableGoogleAnalytics = settings.EnableGoogleAnalytics;
                    htmlEncodedSettings.EnableCustomTracking = settings.EnableCustomTracking;
                    htmlEncodedSettings.InsertCustomTrackingAfterBody = settings.InsertCustomTrackingAfterBody;
                    htmlEncodedSettings.CustomTrackingAnalytics = HttpUtility.HtmlEncode(settings.CustomTrackingAnalytics);
                    htmlEncodedSettings.ClientPrivacyPolicy = HttpUtility.HtmlEncode(settings.ClientPrivacyPolicy);
                    htmlEncodedSettings.ApiBaseUrl = settings.ApiBaseUrl;
                    htmlEncodedSettings.ApiConnectionLimit = settings.ApiConnectionLimit;
                    htmlEncodedSettings.ApplicationTitle = settings.ApplicationTitle;
                    htmlEncodedSettings.TrackingId = settings.TrackingId;
                    htmlEncodedSettings.ProxyDimensionIndex = settings.ProxyDimensionIndex;
                    htmlEncodedSettings.Culture = settings.Culture;
                    htmlEncodedSettings.CulturePropertyOverrides = settings.CulturePropertyOverrides;
                    htmlEncodedSettings.FooterLogoAltText = settings.FooterLogoAltText;
                    htmlEncodedSettings.FooterLogoPath = settings.FooterLogoPath;
                    htmlEncodedSettings.HeaderLogoAltText = settings.HeaderLogoAltText;
                    htmlEncodedSettings.HeaderLogoPath = settings.HeaderLogoPath;
                    htmlEncodedSettings.LogLevels = settings.LogLevels;
                    htmlEncodedSettings.ReportLogoPath = settings.ReportLogoPath;
                    htmlEncodedSettings.SelectedLogLevel = settings.SelectedLogLevel;
                    htmlEncodedSettings.ShortDateFormat = settings.ShortDateFormat;
                    htmlEncodedSettings.UniqueCookieId = settings.UniqueCookieId;
                    return htmlEncodedSettings;
                }
                catch (Exception ex)
                {
                    Logger.Info(ex, "Error retrieving HTML-encoded settings model.");
                    return null;
                }
            }
        }

        /// <summary>
        /// Retrieves an HTML-encoded <see cref="BrandingModel">
        /// </summary>
        private BrandingModel BrandingViewModel
        {
            get
            {
                try
                {
                    BrandingModel model = new BrandingModel(HtmlEncodedSettings);
                    List<Models.Notification> messages = new List<Models.Notification>();
                    var appRootPath = Server.MapPath("~");

                    if (Directory.Exists(appRootPath))
                    {
                        var faviconIcoFile = Directory.GetFiles(appRootPath, "*.*").Where(file => FaviconExtensions.Contains(Path.GetExtension(file).ToLower()) && Path.GetFileName(file).ToLower() == "favicon.ico").FirstOrDefault();
                        if (faviconIcoFile != null)
                        {
                            model.Favicon = new FaviconModel() { Name = Path.GetFileName(faviconIcoFile) };
                        }
                    }
                    else
                    {
                        string message = GlobalResources.GetString(GlobalResourceFiles.AdminResources, "BrandingRootPathInaccessibleMessage");
                        Logger.Error(message);
                        messages.Add(new Models.Notification(message, Models.NotificationType.Error));
                    }


                    return model;
                }
                catch (Exception ex)
                {
                    Logger.Info(ex, "Error retrieving Branding View model.");
                    return null;
                }
            }
        }

        /// <summary>
        /// Clears temporary internet files on server
        /// </summary>
        private void ClearTemporaryInternetFiles()
        {
            System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl, ClearMyTracksByProcess 8");
        }
    }
}