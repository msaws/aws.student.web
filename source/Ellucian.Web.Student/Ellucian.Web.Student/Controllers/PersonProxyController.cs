﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Models.PersonProxy;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Controllers
{
    [Authorize]
    [OutputCache(CacheProfile = "NoCache")]
    public class PersonProxyController : BaseStudentController
    {
        private const string matchCriteriaRecordKey = "PROXY.PERSON";

        /// <summary>
        /// Creates a new PersonProxyController instance.
        /// </summary>
        /// <param name="settings">Settings helper; injected by the framework</param>
        /// <param name="logger">Logging helper; injected by the framework</param>
        public PersonProxyController(Settings settings, ILogger logger)
            : base(settings, logger)
        {
        }

        /// <summary>
        /// Generate the person proxy index page
        /// </summary>
        /// <param name="name">The name of a proxy user whose permissions were updated.</param>
        /// <returns>The person proxy view</returns>
        [LinkHelp]
        [PageAuthorize("personProxy")]
        public ActionResult Index(string name = null)
        {
            ViewBag.Title = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "PersonProxyPageTitle");
            ViewBag.HelpContentId = "PersonProxyIndex";

            return View("Index");
        }

        /// <summary>
        /// Gets a person's photo by id.
        /// </summary>
        /// <param name="id">user's id</param>
        /// <returns>The user's photo as an image file.</returns>
        public ActionResult PersonPhoto(string id)
        {
            try
            {
                ApiFileStream apiClientResult = ServiceClient.GetPersonPhoto(id);
                string filename = !string.IsNullOrEmpty(apiClientResult.Filename) ? apiClientResult.Filename : id;
                return File(apiClientResult.Data, apiClientResult.ContentType, filename);
            }
            catch (Exception e)
            {
                Logger.Debug(e, "Error fetching photo for id '{0}'", id);
                return HttpNotFound();
            }
        }

        /// <summary>
        /// Get a person's proxy information asynchronously.
        /// </summary>
        /// <returns>Returns a proxy information object containing active and available proxies</returns>
        [HttpGet]
        public async Task<JsonResult> GetPersonProxyModel()
        {
            PersonProxyModel personProxyModel;

            try
            {
                var studentId = CurrentUser.PersonId;

                var config = await ServiceClient.GetProxyConfigurationAsync();
                var allRelationships = await ServiceClient.GetPersonPrimaryRelationshipsAsync(studentId);
                var allProxyUsers = await ServiceClient.GetUserProxyPermissionsAsync(studentId);
                var allRelationshipTypes = await ServiceClient.GetRelationshipTypesAsync();
                var allPrefixes = await ServiceClient.GetPrefixesAsync();
                var allSuffixes = await ServiceClient.GetSuffixesAsync();
                var allEmailTypes = await ServiceClient.GetEmailTypesAsync();
                var allPhoneTypes = await ServiceClient.GetPhoneTypesAsync();
                var allCandidates = await ServiceClient.GetUserProxyCandidatesAsync(studentId);
                var profileConfig = await ServiceClient.GetCachedUserProfileConfigurationAsync();

                var personIds = allRelationships.Where(r => r.IsActive).Select(r => r.OtherEntity).ToList();
                personIds.AddRange(allProxyUsers.Select(pu => pu.Id));
                personIds = personIds.Distinct().ToList();
                List<Profile> personProfiles = new List<Profile>();
                foreach(var id in personIds)
                {
                    try
                    {
                        var profileTask = ServiceClient.GetProfileAsync(id, false);
                        var profile = await profileTask;
                        personProfiles.Add(profile);
                    }
                    catch (Exception ex)
                    {
                        Logger.Info(ex, "Unable to retrieve information for related person/organization " + id + ".");
                    }
                }

                personProxyModel = PersonProxyModel.Build(studentId, config, allRelationships, personProfiles, allProxyUsers, allRelationshipTypes, allPrefixes, allSuffixes, allEmailTypes, allPhoneTypes, allCandidates, profileConfig);
                return Json(personProxyModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string message = "Unable to get person proxy information. Error: " + ex.Message;
                Logger.Error(ex, message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Unable to get person proxy information.");
            }
        }

        /// <summary>
        /// Get the proxy subjects for the current user, asynchronously.
        /// </summary>
        /// <returns>Returns a list of proxy subjects</returns>
        [HttpGet]
        public async Task<JsonResult> GetProxySubjectsAsync()
        {
            try
            {
                var subjects = await ServiceClient.GetUserProxySubjectsAsync(CurrentUser.PersonId);
                var model = ProxySelectionDialogModel.Build(subjects, CurrentUser);
                return Json(model, JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception ex)
            {
                var message = "Unable to retrieve proxy subjects for " + User.Identity.Name;
                Logger.Error(ex, message);
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }
        
        /// <summary>
        /// Update the current user's session with the selected proxy subject.
        /// </summary>
        /// <param name="subjectJson">Selected proxy subject</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<JsonResult> SetProxySubjectAsync(string subjectJson)
        {
            try
            {
                var subject = JsonConvert.DeserializeObject<Colleague.Dtos.Base.ProxySubject>(subjectJson);
                var userAuthentication = new UserAuthentication(ServiceClient, HttpContext, Logger);

                if (!await userAuthentication.AddProxySubjectToSession(subject))
                {
                    ModelState.AddModelError("", "SetProxySubjectAsync Failed. Please see log files.");
                }
                return Json("success!", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "SetProxySubjectAsync Failed. Please see log files.");
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Updates the user's person proxy information
        /// </summary>
        /// <param name="proxyUserJson">JSON representation of a proxy</param>
        /// <param name="consentTextJson">JSON representation of disclosure agreement text</param>
        /// <returns>Updated person proxy information JSON string</returns>
        [HttpPost]
        public async Task<JsonResult> UpdatePersonProxyInformation(string proxyUserJson, string consentTextJson)
        {
            try
            {
                var proxyUser = JsonConvert.DeserializeObject<Models.PersonProxy.ProxyUser>(proxyUserJson);
                var consentText = !string.IsNullOrEmpty(consentTextJson) ? JsonConvert.DeserializeObject<string>(consentTextJson) : string.Empty;
                // If email address was previously null, updated new values from the demographics fields
                if (!proxyUser.HasEmailAddress)
                {
                    proxyUser.Email = GetFieldValue(proxyUser.DemographicInformation, DemographicFieldType.EmailAddress);
                    proxyUser.EmailType = GetFieldValue(proxyUser.DemographicInformation, DemographicFieldType.EmailType);
                }
                await UpdateProxyPermissions(proxyUser, consentText);
                var personProxyModel = await GetPersonProxyModel();
                return Json(personProxyModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string message = "Unable to update person proxy information. Error: " + ex.Message;
                Logger.Error(ex, message);

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message);
            }
        }

        /// <summary>
        /// Reauthorizes the user's granted proxy access
        /// </summary>
        /// <param name="activeProxiesJson">JSON representation of user's active proxies</param>
        /// <param name="consentTextJson">JSON representation of disclosure agreement text</param>
        /// <returns>Updated person proxy information JSON string</returns>
        [HttpPost]
        public async Task<JsonResult> ReauthorizeAccessAsync(string activeProxiesJson, string consentTextJson)
        {
            string consentText = null;
            if (string.IsNullOrEmpty(activeProxiesJson))
            {
                var personProxyModel = await GetPersonProxyModel();
                return Json(personProxyModel, JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(consentTextJson))
            {
                consentText = String.Empty;
            }
            try 
            {
                var activeProxies = JsonConvert.DeserializeObject<IEnumerable<Models.PersonProxy.ProxyUser>>(activeProxiesJson).ToList();
                consentText = JsonConvert.DeserializeObject<string>(consentTextJson);


                // Revoke all currently granted access
                var permissionsAssignmentDto = new Colleague.Dtos.Base.ProxyPermissionAssignment()
                {
                    ProxySubjectId = CurrentUser.PersonId,
                    ProxySubjectApprovalDocumentText = new List<string>() { consentText },
                    Permissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>(),
                    IsReauthorizing = true
                };

                var revokedPermissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>();

                foreach (var proxyUser in activeProxies)
                {
                    if (proxyUser.PermissionGroups.Any())
                    {
                        foreach (var group in proxyUser.PermissionGroups)
                        {
                            foreach (var permission in group.Permissions)
                            {
                                if (permission.ReauthorizationDate.HasValue)
                                {
                                    var revokedPerm = new Colleague.Dtos.Base.ProxyAccessPermission()
                                    {
                                        IsGranted = false,
                                        ProxySubjectId = CurrentUser.PersonId,
                                        ProxyUserId = proxyUser.Id,
                                        ProxyWorkflowCode = permission.Id,
                                        StartDate = permission.EffectiveDate,
                                        EffectiveDate = DateTime.Today,
                                        EndDate = (DateTime?)DateTime.Today,
                                    };
                                    permissionsAssignmentDto.Permissions.Add(revokedPerm);
                                    revokedPermissions.Add(revokedPerm);
                                }
                            }
                        }
                    }
                }
                await ServiceClient.PostUserProxyPermissionsAsync(permissionsAssignmentDto);

                //Re-grant the access
                permissionsAssignmentDto = new Colleague.Dtos.Base.ProxyPermissionAssignment()
                {
                    ProxySubjectId = CurrentUser.PersonId,
                    ProxySubjectApprovalDocumentText = new List<string>() { consentText },
                    Permissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>(),
                    IsReauthorizing = true
                };

                foreach(var rp in revokedPermissions)
                {
                    permissionsAssignmentDto.Permissions.Add(new Colleague.Dtos.Base.ProxyAccessPermission()
                    {
                        IsGranted = true,
                        ProxySubjectId = rp.ProxySubjectId,
                        ProxyUserId = rp.ProxyUserId,
                        ProxyWorkflowCode = rp.ProxyWorkflowCode,
                        StartDate = DateTime.Today,
                        EffectiveDate = DateTime.Today,
                        EndDate = null,
                    });
                }

                await ServiceClient.PostUserProxyPermissionsAsync(permissionsAssignmentDto);

                // Re-build the PersonProxyModel and return it as JSON
                var personProxyModel = await GetPersonProxyModel();
                return Json(personProxyModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string message = "Unable to process reauthorization. Error: " + ex.Message;
                Logger.Error(ex, message);

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message);
            }
        }

        /// <summary>
        /// AJAX method to get proxy access information
        /// </summary>
        /// <param name="proxyInformationJson">Person proxy information object converted to JSON string</param>
        /// <returns>JSON-formatted ProxyUser</returns>
        [HttpPost]
        public JsonResult GetProxyInformationForEdit(string proxyInformationJson)
        {
            try
            {
                var proxyInformation = JsonConvert.DeserializeObject<Models.PersonProxy.ProxyUser>(proxyInformationJson);
                return Json(proxyInformation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Updates the user's person proxy information
        /// </summary>
        /// <param name="proxyInformationJson">Person proxy information object converted to JSON string</param>
        /// <returns>Updated person proxy information JSON string</returns>
        [HttpPost]
        public async Task<JsonResult> QueryMatchingPersonsAsync(string criteriaJson)
        {
            try
            {
                var person = JsonConvert.DeserializeObject<Models.PersonProxy.ProxyUser>(criteriaJson);
                if (person != null)
                {
                    var criteriaDto = new Ellucian.Colleague.Dtos.Base.PersonMatchCriteria()
                    {
                        MatchCriteriaIdentifier = matchCriteriaRecordKey,
                        MatchNames = new List<Ellucian.Colleague.Dtos.Base.PersonName>()
                        {
                            new Ellucian.Colleague.Dtos.Base.PersonName()
                            {
                                GivenName = GetFieldValue(person.DemographicInformation, DemographicFieldType.FirstName),
                                MiddleName = GetFieldValue(person.DemographicInformation, DemographicFieldType.MiddleName),
                                FamilyName = GetFieldValue(person.DemographicInformation, DemographicFieldType.LastName)
                            },
                            new Ellucian.Colleague.Dtos.Base.PersonName()
                            {
                                GivenName = GetFieldValue(person.DemographicInformation, DemographicFieldType.FormerFirstName),
                                MiddleName = GetFieldValue(person.DemographicInformation, DemographicFieldType.FormerMiddleName),
                                FamilyName = GetFieldValue(person.DemographicInformation, DemographicFieldType.FormerLastName)
                            }
                        },
                        GovernmentId = GetFieldValue(person.DemographicInformation, DemographicFieldType.GovernmentId),
                        Gender = GetFieldValue(person.DemographicInformation, DemographicFieldType.Gender),
                        EmailAddress = GetFieldValue(person.DemographicInformation, DemographicFieldType.EmailAddress),
                        Phone = GetFieldValue(person.DemographicInformation, DemographicFieldType.Phone),
                        PhoneExtension = GetFieldValue(person.DemographicInformation, DemographicFieldType.PhoneExtension),
                        Prefix = GetFieldValue(person.DemographicInformation, DemographicFieldType.Prefix),
                        Suffix = GetFieldValue(person.DemographicInformation, DemographicFieldType.Suffix),
                    };

                    DateTime birthDate;
                    bool birthDateSupplied = DateTime.TryParse(GetFieldValue(person.DemographicInformation, DemographicFieldType.BirthDate), out birthDate);
                    if (birthDateSupplied) criteriaDto.BirthDate = birthDate;

                    var results = (await ServiceClient.QueryPersonMatchResultsByPostAsync(criteriaDto)).ToList();
                    var model = PersonSearchResultsModel.Build(person, results);
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                throw new Exception(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ErrorSearchingForMatchesMessage"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
        }

        /// <summary>
        /// Creates proxy candidates records 
        /// </summary>
        /// <param name="resultsJson">Person proxy information object converted to JSON string</param>
        /// <param name="consentTextJson">Disclosure agreement text to which user gave consent, converted to JSON string</param>
        /// <returns>Message indicating outcome of proxy candidate processing</returns>
        [HttpPost]
        public async Task<JsonResult> ProcessResultsAsync(string resultsJson, string consentTextJson)
        {
            try
            {
                // De-serialize inbound JSON into appropriate objects
                var result = JsonConvert.DeserializeObject<Models.PersonProxy.PersonSearchResultsModel>(resultsJson);
                var consentText = !string.IsNullOrEmpty(consentTextJson) ? JsonConvert.DeserializeObject<string>(consentTextJson) : string.Empty;

                if (result != null && result.SearchCriteria != null && result.SearchCriteria.DemographicInformation != null)
                {
                    // Extract name information
                    var firstName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FirstName);
                    var lastName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.LastName);
                    string message = string.Empty;

                    // If there are no definite or possible matches...
                    if (!result.Results.Any())
                    {
                        // Build the PersonProxyUser DTO
                        var personDto = new Colleague.Dtos.Base.PersonProxyUser
                        {
                            EmailAddresses = new List<EmailAddress>()
                            {
                                new EmailAddress()
                                {
                                    IsPreferred = true,
                                    Value = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.EmailAddress),
                                    TypeCode = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.EmailType)
                                }
                            },
                            FirstName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FirstName),
                            Gender = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Gender),
                            LastName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.LastName),
                            MiddleName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.MiddleName),
                            Prefix = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Prefix),
                            GovernmentId = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.GovernmentId),
                            Suffix = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Suffix),
                        };

                        // Set the birth date (if supplied)
                        DateTime birthDate;
                        bool birthDateSupplied = DateTime.TryParse(GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.BirthDate), out birthDate);
                        if (birthDateSupplied) personDto.BirthDate = birthDate;

                        // Get an email type if one is not supplied
                        var userProfileConfig = await ServiceClient.GetCachedUserProfileConfigurationAsync();
                        personDto.EmailAddresses.First().TypeCode = await SetEmailAddressTypeIfNotSet(personDto.EmailAddresses.First().TypeCode, userProfileConfig);

                        // Determine if a phone number is supplied.  If so, set the PersonProxyUser phone list and get a phone type if one is not supplied
                        var number = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Phone);
                        if (!string.IsNullOrEmpty(number))
                        {
                            personDto.Phones = new List<Phone>()
                            {
                                new Phone()
                                {
                                    Number = number,
                                    Extension = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.PhoneExtension),
                                    TypeCode = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.PhoneType),
                                }
                            };
                            personDto.Phones.First().TypeCode = await SetPhoneTypeIfNotSet(personDto.Phones.First().TypeCode, userProfileConfig);
                        }

                        // Determine if a former name is supplied.  If so, set the PersonProxyUser former names list 
                        var formerGiven = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FormerFirstName);
                        if (!string.IsNullOrEmpty(formerGiven))
                        {
                            personDto.FormerNames = new List<PersonName>()
                            {
                                new PersonName()
                                {
                                    GivenName = formerGiven,
                                    FamilyName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FormerLastName),
                                    MiddleName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FormerMiddleName)
                                }
                            };
                        }

                        // Create the person
                        try
                        {
                            var person = await ServiceClient.PostProxyUserAsync(personDto);

                            // Create the specified relationship with the now-existent person
                            var relationshipDto = new Colleague.Dtos.Base.Relationship()
                            {
                                PrimaryEntity = CurrentUser.PersonId,
                                OtherEntity = person.Id,
                                RelationshipType = result.SearchCriteria.Relationship
                            };
                            try
                            {
                                var rel = await ServiceClient.PostRelationshipAsync(relationshipDto);

                                // Grant the specificed proxy access to the person
                                if (rel != null)
                                {
                                    result.SearchCriteria.Id = rel.OtherEntity;
                                    await UpdateProxyPermissions(result.SearchCriteria, consentText);
                                    var personProxyModel = await GetPersonProxyModel();
                                    return Json(personProxyModel, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    throw new Exception(String.Format(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyAccessNotUpdatedMessage"), firstName + " " + lastName));
                                }
                            }
                            catch (Exception ex)
                            {
                                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                                message = String.Format(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyAccessNotUpdatedMessage"), firstName + " " + lastName);
                                Logger.Error(ex, message);
                            }

                        }
                        catch (Exception ex)
                        {
                            Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            message = String.Format(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyAccessNotUpdatedMessage"), firstName + " " + lastName);
                            Logger.Error(ex, message);
                        }
                    }

                    // If matches contains 1 definite match and no possible matches...
                    else if (result.Results.Count() == 1 && result.Results.First().MatchCategory == Ellucian.Colleague.Dtos.Base.PersonMatchCategoryType.Definite)
                    {
                        // Create the specified relationship with the definite match
                        var firstMatch = result.Results.First();
                        var relationshipDto = new Colleague.Dtos.Base.Relationship()
                        {
                            PrimaryEntity = CurrentUser.PersonId,
                            OtherEntity = firstMatch.PersonId,
                            RelationshipType = result.SearchCriteria.Relationship
                        };
                        try
                        {
                            var rel = await ServiceClient.PostRelationshipAsync(relationshipDto);

                            // Grant the specificed proxy access to the person
                            if (rel != null)
                            {
                                result.SearchCriteria.Id = firstMatch.PersonId;
                                await UpdateProxyPermissions(result.SearchCriteria, consentText);
                                var personProxyModel = await GetPersonProxyModel();
                                return Json(personProxyModel, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                throw new Exception(String.Format(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyAccessNotUpdatedMessage"), firstName + " " + lastName));
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            message = String.Format(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "ProxyAccessNotUpdatedMessage"), firstName + " " + lastName);
                            Logger.Error(ex, message);
                        }
                    }

                    // If there is at least one potential match...
                    else
                    {
                        // Build the ProxyCandidate DTO
                        var candidateDto = new Colleague.Dtos.Base.ProxyCandidate()
                        {
                            EmailAddress = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.EmailAddress),
                            EmailType = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.EmailType),
                            FirstName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FirstName),
                            FormerFirstName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FormerFirstName),
                            FormerLastName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FormerLastName),
                            FormerMiddleName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.FormerMiddleName),
                            Gender = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Gender),
                            LastName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.LastName),
                            MiddleName = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.MiddleName),
                            Phone = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Phone),
                            PhoneExtension = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.PhoneExtension),
                            PhoneType = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.PhoneType),
                            Prefix = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Prefix),
                            GovernmentId = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.GovernmentId),
                            Suffix = GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.Suffix),
                            RelationType = result.SearchCriteria.Relationship,
                            GrantedPermissions = result.SearchCriteria.PermissionGroups.SelectMany(pg => pg.Permissions).Where(p => p.IsAssigned).Select(p => p.Id).ToList(),
                            ProxyMatchResults = result.Results,
                            ProxySubject = CurrentUser.PersonId
                        };

                        // Ensure that email type and phone type are set if email address/phone number are supplied
                        var userProfileConfig = await ServiceClient.GetCachedUserProfileConfigurationAsync();
                        candidateDto.EmailType = await SetEmailAddressTypeIfNotSet(candidateDto.EmailType, userProfileConfig);
                        if (!string.IsNullOrEmpty(candidateDto.Phone))
                        {
                            candidateDto.PhoneType = await SetPhoneTypeIfNotSet(candidateDto.PhoneType, userProfileConfig);
                        }

                        // Set the birth date (if supplied)
                        DateTime birthDate;
                        bool birthDateSupplied = DateTime.TryParse(GetFieldValue(result.SearchCriteria.DemographicInformation, DemographicFieldType.BirthDate), out birthDate);
                        if (birthDateSupplied) candidateDto.BirthDate = birthDate;

                        try
                        {
                            // Create a candidate record from the specified information
                            var candidate = await ServiceClient.PostProxyCandidateAsync(candidateDto);
                            if (candidate != null)
                            {
                                var personProxyModel = await GetPersonProxyModel();
                                return Json(personProxyModel, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                throw new Exception(GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "UnableToProcessMessage"));
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            message = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "UnableToProcessMessage");
                            Logger.Error(ex, message);
                        }
                    }

                    // If any errors occured, return the error message.
                    return Json(message);
                }
                throw new Exception("Unable to deserialize search criteria and/or person duplicate information needed to process request.");
            }
            catch (Exception ex)
            {
                string message = GlobalResources.GetString(GlobalResourceFiles.PersonProxyResources, "UnableToProcessMessage");
                Logger.Error(ex, String.Format(message, " - Error: " + ex.Message));

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message);
            }

        }

        /// <summary>
        /// Retrieves a field value
        /// </summary>
        /// <param name="user"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private string GetFieldValue(IEnumerable<DataEntryField> fields, DemographicFieldType type)
        {
            if (fields != null && fields.Any())
            {
                var fieldOfType = fields.FirstOrDefault(field => field.Type == type);
                if (fieldOfType != null) return fieldOfType.Value;
            }
            return null;
        }

        /// <summary>
        /// Updates proxy access for a user
        /// </summary>
        /// <param name="proxyUser">The proxy user for whom access is being updated</param>
        /// <param name="consentText">Optional disclosure agreement text to which the user may have consented</param>
        private async Task UpdateProxyPermissions(Ellucian.Web.Student.Models.PersonProxy.ProxyUser proxyUser, string consentText)
        {
            if (proxyUser == null)
            {
                throw new ArgumentNullException("proxyUser");
            }
            var permissionsAssignmentDto = new Colleague.Dtos.Base.ProxyPermissionAssignment()
            {
                ProxySubjectId = CurrentUser.PersonId,
                ProxySubjectApprovalDocumentText = new List<string>() { consentText },
                Permissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>()
            };
            if (proxyUser.PermissionGroups.Any() && proxyUser.PermissionGroups.TrueForAll(pg => pg.Permissions != null))
            {
                List<Models.PersonProxy.ProxyAccessPermission> changedPermissions = proxyUser.PermissionGroups.SelectMany(pg => pg.Permissions).Where(p => p.IsInitiallyAssigned != p.IsAssigned).ToList();
                foreach(var permission in changedPermissions)
                {
                    permissionsAssignmentDto.Permissions.Add(new Colleague.Dtos.Base.ProxyAccessPermission()
                    {
                        IsGranted = permission.IsAssigned,
                        ProxySubjectId = CurrentUser.PersonId,
                        ProxyUserId = proxyUser.Id,
                        ProxyWorkflowCode = permission.Id,
                        StartDate = DateTime.Today,
                        EffectiveDate = DateTime.Today,
                        EndDate = permission.IsAssigned ? null : (DateTime?)DateTime.Today,
                    });
                }
                // Set proxy email address and type only if user did not initially have an email address
                if (!proxyUser.HasEmailAddress)
                {
                    permissionsAssignmentDto.ProxyEmailAddress = proxyUser.Email;
                    permissionsAssignmentDto.ProxyEmailType = proxyUser.EmailType;
                }
                await ServiceClient.PostUserProxyPermissionsAsync(permissionsAssignmentDto);
            }
        }

        /// <summary>
        /// Sets an email address type if is not already set
        /// </summary>
        /// <param name="typeCode">The email address type code to be checked/set</param>
        /// <param name="userProfileConfig">UserProfileConfiguration information</param>
        private async Task<string> SetEmailAddressTypeIfNotSet(string typeCode, UserProfileConfiguration userProfileConfig)
        {
            if (string.IsNullOrEmpty(typeCode))
            {
                if (userProfileConfig != null && userProfileConfig.UpdatableEmailTypes != null && userProfileConfig.UpdatableEmailTypes.Any())
                {
                    typeCode = userProfileConfig.UpdatableEmailTypes.First();
                }
                if (string.IsNullOrEmpty(typeCode))
                {
                    var emailTypes = await ServiceClient.GetCachedEmailTypesAsync();
                    if (emailTypes != null && emailTypes.Any())
                    {
                        var type = emailTypes.First();
                        typeCode = type != null ? type.Code : string.Empty;
                    }
                }
            }
            return typeCode;
        }

        /// <summary>
        /// Sets a phone type if is not already set
        /// </summary>
        /// <param name="typeCode">The phone type code to be checked/set</param>
        /// <param name="userProfileConfig">UserProfileConfiguration information</param>
        private async Task<string> SetPhoneTypeIfNotSet(string typeCode, UserProfileConfiguration userProfileConfig)
        {
            if (string.IsNullOrEmpty(typeCode))
            {
                if (userProfileConfig != null && userProfileConfig.UpdatablePhoneTypes != null && userProfileConfig.UpdatablePhoneTypes.Any())
                {
                    typeCode = userProfileConfig.UpdatablePhoneTypes.First();
                }
                if (string.IsNullOrEmpty(typeCode))
                {
                    var phoneTypes = await ServiceClient.GetCachedPhoneTypesAsync();
                    if (phoneTypes != null && phoneTypes.Any())
                    {
                        var type = phoneTypes.First();
                        typeCode = type != null ? type.Code : string.Empty;
                    }
                }
            }
            return typeCode;
        }
    }
}
