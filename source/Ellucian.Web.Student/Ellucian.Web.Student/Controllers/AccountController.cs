﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Mvc.Filter;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Modules;
using Hedtech.Identity;
using Hedtech.Identity.Mvc;
using Microsoft.IdentityModel.Claims;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Resources;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Controllers
{
    /// <summary>
    /// Provides a controller for account management.
    /// </summary>
    /// <remarks>
    /// Note about Anti-Forgery: this class implements the anti-forgery filter on a per-action bases as
    /// some actions (those that support JSON Get) need to not enforce the filter as the user state
    /// can change between these calls and cause validation errors.
    /// </remarks>
    [OutputCache(CacheProfile = "NoCache")]
    [AddHttpResponseHeaderCustomFilter("X-Frame-Options", "DENY")]
    public class AccountController : Controller
    {
        private Settings settings;
        private ILogger logger;
        private readonly ColleagueApiClient client;
        private readonly IdentityManagementSettings idmSettings;

        public AccountController(Settings settings, ILogger logger)
        {
            this.settings = settings;
            this.logger = logger;
            this.client = new ColleagueApiClient(settings.ApiBaseUrl, settings.ApiConnectionLimit, logger);
            this.idmSettings = DependencyResolver.Current.GetService<IdentityManagementSettings>();
        }

        #region login
        // GET: /Account/Login
        [LinkHelp]
        public async Task<ActionResult> Login(string returnUrl, string error)
        {
            return await ContextDependentView(returnUrl, error);
        }

        [HttpPost]
        [ValidateAntiForgeryTokenCustomFilter]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            try
            {
                if (model == null)
                {
                    throw new Exception(GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "ModelNullMessage"));
                }
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    ViewBag.RouteValues = new { returnUrl = returnUrl };
                }
                if (ModelState.IsValid)
                {
                    try
                    {
                        var userAuthentication = new UserAuthentication(this.client, HttpContext, this.logger);
                        if (await userAuthentication.Validate(model.UserName, model.Password))
                        {
                            // ReturnUrl query parameter is first option
                            if (Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            // Next, attempt to use the User-defined landing page
                            else
                            {
                                var landingPageUrl = await GetLandingPageUrlAsync();
                                if (!string.IsNullOrEmpty(landingPageUrl))
                                {
                                    return Redirect(landingPageUrl);
                                }
                            }
                            // Default landing page
                            return RedirectToAction("Index", "Home", new { hideProxyDialog = false });
                        }
                        else
                        {
                            ModelState.AddModelError("", GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "LoginFailureMessage"));
                        }
                    }
                    catch (PasswordExpiredException exp)
                    {
                        logger.Info(exp.Message);
                        string passwordExpiredMessage = GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "ChangePasswordExpiredPasswordMessage");
                        ModelState.AddModelError("", passwordExpiredMessage);
                        ViewBag.ReturnUrl = returnUrl;
                        return View("ChangePassword", new ChangePasswordModel() { Username = model.UserName });
                    }
                    catch (ListenerNotFoundException lex)
                    {
                        logger.Error(lex.Message);
                        ModelState.AddModelError("", GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "ServiceUnavailableMessage"));
                    }
                    catch (HttpRequestFailedException hex)
                    {
                        logger.Error(hex.Message);
                        ModelState.AddModelError("", GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "ServiceUnavailableMessage"));
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                    }
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }
            return View(model);
        }

        /// <summary>
        /// Verifies a user's password
        /// </summary>
        /// <param name="passwordJson">User password</param>
        /// <returns>Indicator of verification success or failure</returns>
        [HttpPost]
        [ValidateAntiForgeryTokenCustomFilter]
        public async Task<JsonResult> VerifyPasswordAsync(string passwordJson)
        {
            try
            {
                if (string.IsNullOrEmpty(passwordJson))
                {
                    throw new Exception(GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "ModelNullMessage"));
                }
                CurrentUser currentUser = new CurrentUser(User as IClaimsPrincipal);
                string username = currentUser.UserId;
                string password = JsonConvert.DeserializeObject<string>(passwordJson);

                await this.client.Login2Async(username, password);
                return Json(new { success = true }, JsonRequestBehavior.DenyGet);

            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                string message = GlobalResources.GetString(GlobalResourceFiles.SiteResources, "PasswordVerificationFailedErrorMessage");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(message, JsonRequestBehavior.DenyGet);
            }
        }

        #endregion

        #region logoff

        //
        // GET: /Account/LogOff
        [AllowAnonymous]
        public async Task<ActionResult> LogOff()
        {
            // local logout
            var userAuthentication = new UserAuthentication(this.client, HttpContext, this.logger);
            await userAuthentication.InvalidateUser();

            if (idmSettings.IsIdmEnabled)
            {
                // sso logout response
                ControllerContext.HttpContext.Items.Add(AuthenticationModule.SsoActionKey, AuthenticationModule.SsoLogoutResponseAction);
                return RedirectToAction("Login", "Account", new { returnUrl = Url.Action("LandingPage", "Account") });
            }
            else
            {
                //local logout response
                return RedirectToAction("Login", "Account");
            }
        }

        #endregion

        #region reauthenticate

        // POST: /Account/JsonLogin
        [HttpPost]
        [ValidateAntiForgeryTokenCustomFilter]
        public async Task<JsonResult> JsonLogin(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuthentication = new UserAuthentication(this.client, HttpContext, this.logger);
                    if (await userAuthentication.Validate(model.UserName, model.Password))
                    {
                        return Json(new { success = true, redirect = returnUrl });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Login Failed. Please check User Name and Password.");
                    }
                }
                catch (PasswordExpiredException exp)
                {
                    string changePasswordUrl = string.Format("{0}?error={1}", Url.Action("ChangePassword", "Account"), Url.Encode(exp.Message));
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        // add return url...
                    }
                    // success here causes the redirect url to be follored
                    return Json(new { success = true, redirect = changePasswordUrl });
                }
                catch (HttpRequestFailedException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            // If we got this far, something failed
            return Json(new { errors = GetErrorsFromModelState() });
        }

        #endregion

        #region change password

        //
        // GET: /Account/ChangePassword
        [LinkHelp]
        public ActionResult ChangePassword(string error)
        {
            if (!string.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("", error);
            }
            return View();
        }

        /// <summary>
        /// Processes a password change
        /// </summary>
        /// <param name="model">A <see cref="ChangePasswordModel"/> object.</param>
        /// <returns>Redirect to ChangePasswordSuccess on success; updated ModelState on error</returns>
        [HttpPost]
        [ValidateAntiForgeryTokenCustomFilter]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            try
            {
                if (model == null)
                {
                    throw new Exception(GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "ModelNullMessage"));
                }
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather
                    // than return false in certain failure scenarios.
                    await client.ChangePasswordAsync(model.Username, model.OldPassword, model.NewPassword);
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    List<KeyValuePair<string, string>> errors = new List<KeyValuePair<string, string>>();
                    foreach (ModelState modelState in ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            errors.Add(new KeyValuePair<string, string>("", error.ErrorMessage));
                        }
                    }
                    errors.ForEach(e => ModelState.AddModelError(e.Key, e.Value));
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        #endregion

        /// <summary>
        /// Determine and show the landing page (from returnUrl parameter, Preferences API, or default (Home/Index).
        /// </summary>
        /// <param name="returnUrl">Destination page (takes top priority)</param>
        /// <returns>Redirects to the landing page</returns>
        public async Task<ActionResult> LandingPage(string returnUrl)
        {
            // ReturnUrl query parameter is first option
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            // Next, attempt to use the User-defined landing page
            else
            {
                var landingPageUrl = await GetLandingPageUrlAsync();
                if (!string.IsNullOrEmpty(landingPageUrl))
                {
                    Redirect(landingPageUrl);
                }
            }
            // Default landing page
            return RedirectToAction("Index", "Home", new { hideProxyDialog = false });
        }

        public ActionResult Unauthorized(string page)
        {
            if (!string.IsNullOrEmpty(page))
            {
                ViewBag.RouteValues = new { returnUrl = page };
            }
            return View();
        }

        public ActionResult Error(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.Error = message;
            }
            return View();
        }

        /// <summary>
        /// Private method to retrieve the landing page for the current user from the preferences API
        /// </summary>
        /// <returns>The fully qualified URL for the user's homepage if available, otherwise empty string</returns>
        private async Task<string> GetLandingPageUrlAsync()
        {
            string landingPageUrl = string.Empty;
            if (User is IClaimsPrincipal)
            {
                try
                {
                    CurrentUser currentUser = new CurrentUser(User as IClaimsPrincipal);
                    if (currentUser != null)
                    {
                        // Can't rely on the client controller being logged in because this request could be hitting the LandingPage action coming straight in from SSO
                        var serviceClient = new Ellucian.Colleague.Api.Client.ColleagueApiClient(settings.ApiBaseUrl, settings.ApiConnectionLimit, logger);
                        serviceClient.Credentials = JwtHelper.Create(User);
                        var preferencesDto = await serviceClient.GetSelfservicePreferenceAsync(currentUser.PersonId, "selfservice");

                        // Attempt to create the PreferencesModel object from the API and redirect to that URL if it is local
                        var preferencesModel = new PreferencesModel(preferencesDto.Preferences);
                        if (preferencesModel != null && !string.IsNullOrEmpty(preferencesModel.Homepage))
                        {
                            var siteService = DependencyResolver.Current.GetService<Ellucian.Web.Mvc.Menu.ISiteService>();
                            var site = siteService.Get(currentUser);
                            if (site != null)
                            {
                                var page = site.Pages.FirstOrDefault(p => p.Id == preferencesModel.Homepage);
                                if (page != null)
                                {
                                    landingPageUrl = Url.Action(page.Action, page.Controller, new { area = page.Area });
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e, "Unable to retrieve user preferences for person landing page.");
                }

            }
            return landingPageUrl;

        }

        private async Task<ActionResult> ContextDependentView(string returnUrl, string error)
        {
            string actionName = ControllerContext.RouteData.GetRequiredString("action");

            // always add the error to the modelstate
            if (!string.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("", error);
            }

            // Code within the SessionCookieManager (called in "LoginFormPartial.cshtml") will
            // invalidate the session cookies, so we need to "log out" the server-side session
            // token to prevent someone else picking it up before garbage collection deletes it
            if (HttpContext.User is IClaimsPrincipal)
            {
                await client.LogoutAsync(JwtHelper.Create(HttpContext.User));
            }

            // Now that the user has been logged out on the server clear their Principal
            // This sets IsAuthenticated to false and prevents the previously logged in
            // Identify from being displayed (or used) elsewhere during this request
            HttpContext.User = null;

            // An AJAX request was redirected to the login page by authorization filter, so
            // talk back in JSON so that the client-side scripts know to show the login form.
            if (Request.AcceptTypes.Contains("application/json"))
            {
                return Json(new
                {
                    InvalidSession = "true", // simple flag to allow client-side scipt to check quickly for this message
                    LoginPageUrl = Url.Action("Login", "Account"), // allow the client-side script to redirect to the full page login
                    Errors = GetErrorsFromModelState() // list of model errors for ajax display
                }, JsonRequestBehavior.AllowGet);
            }

            // The AJAX login form has requested the login page, return the partial view only.
            // Tell the page that it should post back to a differnt action.
            else if (Request.QueryString["content"] != null)
            {
                ViewBag.FormAction = "Json" + actionName;

                if (!string.IsNullOrEmpty(returnUrl))
                {
                    ViewBag.RouteValues = new { returnUrl = returnUrl };
                }

                // when passing back the login form, it will contain a new anti-forgery token
                // and in order to get the cookie replaced to match the user context 
                // (which is now unauthenticated)we have to delete the incomming one
                Request.Cookies.Remove(AntiForgeryConfig.CookieName);

                return PartialView("LoginFormPartial");
            }

            // Standard, full page login
            else
            {
                if (idmSettings.IsIdmEnabled)
                {
                    // sso login
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        returnUrl = Url.Action("LandingPage", "Account", new { returnUrl = returnUrl });
                    }
                    if (!returnUrl.Contains("hideProxyDialog"))
                    {
                        if (returnUrl.Contains("?"))
                        {
                            returnUrl += "&";
                        }
                        else
                        {
                            returnUrl += "?";
                        }
                        returnUrl += "hideProxyDialog=false";
                    }
                    return SamlActions.SamlLoginActionResult(returnUrl);
                }
                else
                {
                    // local login
                    ViewBag.FormAction = actionName;
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        ViewBag.RouteValues = new { returnUrl = returnUrl };
                    }
                    return View();
                }
            }

        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}
