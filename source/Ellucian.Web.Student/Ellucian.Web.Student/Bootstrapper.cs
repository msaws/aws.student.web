// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Adapters;
using Ellucian.Web.Mvc.Configuration;
using Ellucian.Web.Mvc.Data.Repositories;
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Mvc.Theme;
using Ellucian.Web.Resource;
using Ellucian.Web.Resource.Repositories;
using Ellucian.Web.Student.Areas.Student.Utility;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Utility;
using Hedtech.Identity;
using Hedtech.Identity.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.Mvc;
using slf4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ellucian.Web.Student
{
    public static class Bootstrapper
    {
        private static string LogFile = "App_Data\\Logs\\StudentSelfService.log";
        private static string LogCategory = "SelfServiceApplication";
        private static string baseResourcePath = AppDomain.CurrentDomain.BaseDirectory;
        private static string resourceCustomizationFilePath = AppDomain.CurrentDomain.GetData("DataDirectory").ToString() + @"\ResourceCustomization.json";


        // Planning-specific setting for Home Page performance
        public static bool RegistrationMode = false;

        // Planning-specific setting to force enable the registration buttons 
        // This will ignore registration periods in Self-Service, always allow the user to attempt to register,
        // and rely on the back-end registration process to enforce registration dates
        public static bool ForceEnableRegistrationButtons = false;

        /// <summary>
        /// Planning-specific setting to control collapsing behavior of the My Progress page.
        /// If true, requirements/sub-requirements/groups that are fully planned or complete will be collapsed by default.
        /// If false, only requirements/sub-requirements/groups that are complete will be collapsed by default.
        /// </summary>
        public static bool DetailsCollapsedByDefaultWhenFullyPlanned = false;

        /// <summary>
        /// The port to be used when generating a URL in the scenario where an SSL-terminating load balancer 
        /// has been detected and is not using the default HTTPS port. A value of -1 will become the default port
        /// for the scheme when used with a UriBuilder.
        ///
        /// Not necessary in most circumstances, such as if the load balancer is using the default port for HTTPS (443)
        /// or is not terminating SSL.
        /// </summary>
        public static int LoadBalancerSecurePort = -1;

        public static void Initialize()
        {
            var container = BuildUnityContainer();

            // Set the dependency resolver to use the Unity container
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            container.RegisterType<ColleagueApiClient, ColleagueApiClient>();

            // start out with a no-op logger
            ILogger logger = slf4net.NOPLogger.Instance;

            // We can't setup enterprise logger until we know the settings.
            // use a NO-OP logger until then...
            AdapterRegistry registry = new AdapterRegistry(new HashSet<ITypeAdapter>(), logger);

            // Now read the settings
            container.RegisterInstance<ISettingsService>(new SettingsService(new MvcSettingsRepository(), registry));
            container.RegisterInstance<Mvc.Models.Settings>(container.Resolve<ISettingsService>().Get());

            // Configure the logging
            SourceLevels logLevel = Ellucian.Logging.EnterpriseLibraryLoggerAdapter.LevelFromString(container.Resolve<Mvc.Models.Settings>().SelectedLogLevel);
            Ellucian.Logging.EnterpriseLibraryLoggerAdapter.Configure(LogFile, logLevel, LogCategory, template: "{timestamp}{newline}{message}{newline}");
            logger = slf4net.LoggerFactory.GetLogger(LogCategory);
            container.RegisterInstance<ILogger>(logger);

            //register student helper
            container.RegisterInstance<IStudentHelper>(new StudentHelper());

            // site and theme registry setup
            registry = new AdapterRegistry(new HashSet<ITypeAdapter>(), logger);
            // Replaced SiteAdapter with SiteToSiteModelAdapter to account for new menu structure
            registry.AddAdapter(new SiteToSiteModelAdapter(registry, logger));
            registry.AddAdapter(new SiteToSiteAdapter(registry, logger));
            registry.AddAdapter(new ThemeAdapter(registry, logger));
            registry.AddAdapter(new ThemeToThemeAdapter(registry, logger));
            container.RegisterInstance<ILicensingService>(new LicensingService(logger));
            container.RegisterInstance<ISiteService>(new SiteService(new XmlSiteRepository(logger), ApplicationMetadata.LicensedAreas, registry, logger));
            container.RegisterInstance<IThemeService>(new ThemeService(new CssThemeRepository(HostingEnvironment.MapPath("~/Content/Theme.css"), logger), registry, logger));

            // Support for property injection on filters
            var oldProvider = FilterProviders.Providers.Single(f => f is FilterAttributeFilterProvider);
            FilterProviders.Providers.Remove(oldProvider);
            var provider = new Microsoft.Practices.Unity.Mvc.UnityFilterAttributeFilterProvider(container);
            FilterProviders.Providers.Add(provider);

            // the following call depends on all assemblies being loaded...
            LoadAllAssembliesIntoAppDomain(container);

            RegisterAdapters(container);

            bool registrationMode = false;
            if (bool.TryParse(WebConfigurationManager.AppSettings["RegistrationMode"], out registrationMode))
            {
                RegistrationMode = registrationMode;
            }

            bool forceEnableRegistrationButtons = false;
            if (bool.TryParse(WebConfigurationManager.AppSettings["ForceEnableRegistrationButtons"], out forceEnableRegistrationButtons))
            {
                ForceEnableRegistrationButtons = forceEnableRegistrationButtons;
            }

            bool detailsCollapsedByDefaultWhenFullyPlanned = false;
            if (bool.TryParse(WebConfigurationManager.AppSettings["DetailsCollapsedByDefaultWhenFullyPlanned"], out detailsCollapsedByDefaultWhenFullyPlanned))
            {
                DetailsCollapsedByDefaultWhenFullyPlanned = detailsCollapsedByDefaultWhenFullyPlanned;
            }

            int loadBalancerSecurePort = -1;

            if (int.TryParse(WebConfigurationManager.AppSettings["LoadBalancerSecurePort"], out loadBalancerSecurePort))
            {
                LoadBalancerSecurePort = loadBalancerSecurePort;
            }

            // Create the IdM settings and register the instance
            IdentityManagementSettings idmSettings = GetIdentityManagementSettings(container);
            container.RegisterInstance<IdentityManagementSettings>(idmSettings);
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // Read the Unity configuration
            UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            section.Configure(container);

            //container.RegisterControllers();
            var localResourceRepository = new LocalResourceRepository(baseResourcePath, resourceCustomizationFilePath);
            container.RegisterInstance<IResourceRepository>(localResourceRepository);

            return container;
        }

        private static void LoadAllAssembliesIntoAppDomain(IUnityContainer container)
        {
            var binDirectory = System.Web.HttpRuntime.BinDirectory;
            var files = Directory.GetFiles(binDirectory, "*.dll", SearchOption.AllDirectories);
            AssemblyName a = null;
            foreach (var s in files)
            {
                a = AssemblyName.GetAssemblyName(s);
                if (!AppDomain.CurrentDomain.GetAssemblies().Any(
                    assembly => AssemblyName.ReferenceMatchesDefinition(
                    assembly.GetName(), a)))
                {
                    Assembly.LoadFrom(s);
                }
            }

            // debug
            ILogger logger = container.Resolve<ILogger>();
            if (logger.IsDebugEnabled)
            {
                var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                List<Assembly> sortedLoadedAssemblies = loadedAssemblies.OrderBy(x => x.FullName).ToList();
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Loaded AppDomain Assemblies");
                foreach (var asm in sortedLoadedAssemblies)
                {
                    sb.AppendLine(asm.FullName);
                }
                logger.Debug(sb.ToString());
            }
        }

        private static void RegisterAdapters(IUnityContainer container)
        {
            ILogger logger = container.Resolve<ILogger>();
            try
            {
                var baseAdapterInterface = typeof(ITypeAdapter);
                var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();

                // Select adapter types from this assembly that inherits from ITypeAdapter; the adapter interfaces are excluded
                // since concrete types are required
                var ellucianLoadedAssemblies = loadedAssemblies.Where(x => x.GetName().Name.StartsWith("ellucian", StringComparison.InvariantCultureIgnoreCase)).ToArray();
                var adapterTypes = ellucianLoadedAssemblies.SelectMany(assembly => assembly.GetTypes().Where(x => baseAdapterInterface.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract && !x.ContainsGenericParameters));

                ISet<ITypeAdapter> adapterCollection = new HashSet<ITypeAdapter>();
                AdapterRegistry registry = new AdapterRegistry(adapterCollection, logger);
                foreach (var adapterType in adapterTypes)
                {
                    // Instantiate 
                    var adapterObject = adapterType.GetConstructor(new Type[] { typeof(IAdapterRegistry), typeof(ILogger) }).Invoke(new object[] { registry, logger }) as ITypeAdapter;
                    registry.AddAdapter(adapterObject);
                    logger.Debug("RegisterAdapters - added: " + adapterObject.GetType().ToString());
                }

                // Register the adapter registry as a singleton instance
                container.RegisterInstance<IAdapterRegistry>(registry, new ContainerControlledLifetimeManager());
            }
            catch (ReflectionTypeLoadException e)
            {
                logger.Error("RegisterAdapters error(s)", e);
                if (e.LoaderExceptions != null)
                {
                    foreach (var le in e.LoaderExceptions)
                    {
                        logger.Error("Loader Exception: " + le.Message);
                    }
                }
                throw e;
            }
        }

        private static IdentityManagementSettings GetIdentityManagementSettings(IUnityContainer container)
        {
            SamlConfigurationRepository scr = new SamlConfigurationRepository(container.Resolve<ILogger>());

            IdentityManagementSettings identitySettings = scr.GetIdentityManagementSettings();

            // Set SSO cookie Id to be unique to this instance
            var uiParameters = container.Resolve<Settings>();
            var UniqueCookieId = uiParameters.UniqueCookieId;
            if (!string.IsNullOrEmpty(UniqueCookieId))
            {
                // If not set, the default cookie name is "hedtechsso"
                identitySettings.ServiceProviderSsoSessionCookieId = UniqueCookieId + "_Sso";
            }

            // Set the callback method that will create the application's (JWT) session token
            identitySettings.AuthenticationCallback =
                (userName, httpContext) =>
                {
                    // Strip leading "domain\" prefix from userName, if present
                    var index = userName.IndexOf(@"\");
                    if (index > 0)
                    {
                        userName = userName.Substring(index + 1);
                    }

                    var settings = container.Resolve<Settings>();
                    var logger = container.Resolve<ILogger>();
                    var client = new ColleagueApiClient(settings.ApiBaseUrl, settings.ApiConnectionLimit, logger);
                    var userValidation = new UserAuthentication(client, httpContext, logger);
                    return Task.Run(async () =>
                    {
                        return await userValidation.ValidateProxy(identitySettings.ProxyUserName, identitySettings.ProxyPassword, userName);
                    }).GetAwaiter().GetResult();
                };

            // Set the callback method to use when there is a SSO session authentication failure
            identitySettings.SsoFailureCallback =
                (failureReason, returnUrl) =>
                {
                    if (failureReason == SamlSupport.AuthFailureReasons.SamlResponseValidationFailure)
                    {
                        // this is a hard stop and likely means configuration is incorrect (here or in the IdP).
                        return new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            action = "Error",
                            controller = "Account",
                            area = "",
                            message = GlobalResources.GetString(GlobalResourceFiles.UserAccountResources, "SamlValidationError")
                        }));
                    }
                    else
                    {
                        return new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            action = "Unauthorized",
                            controller = "Account",
                            area = "",
                            page = returnUrl,
                            authFailed = "true"
                        }));
                    }
                };

            identitySettings.enableSha256();

            return identitySettings;
        }
    }
}
