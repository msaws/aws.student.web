/* Copyright 2016-2017 Ellucian Company L.P. and its affiliates. */
"use strict";

var gulp = require("gulp"),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    gutil = require("gulp-util"),
    rename = require("gulp-rename"),
    watch = require("gulp-watch"),
    open = require("gulp-open"),
    webpack = require("webpack"),
    webpackStream = require("webpack-stream"),
    path = require("path"),
    fs = require("fs"),
    karma = require('karma').Server,
    webpackConfig = require('./webpack.conf.js'),
    webpackDevServer = require("webpack-dev-server");



//gulp.task("default", ["devbuild", "productionbuild"]);

gulp.task("bankingInformation", function () {
    if (!fileExists("Areas/HumanResources")) return gulp.src([]);
    var entry = {
        "banking.information": "./Areas/HumanResources/Scripts/BankingInformation/banking.information.application",
    }
    var outDir = "./Areas/HumanResources/Build/";

    return doBuild(entry, outDir);
});

gulp.task("timeManagement", function () {
    if (!fileExists("Areas/TimeManagement")) return gulp.src([]);
    var entry = {
        "time.sheet": "./Areas/TimeManagement/Scripts/TimeSheet/time.sheet.application",
        "time.approval": "./Areas/TimeManagement/Scripts/TimeApproval/time.approval.application"
    }
    var outDir = "./Areas/TimeManagement/Build/"
    return doBuild(entry, outDir);

})

gulp.task("watch-bankingInformation", ["bankingInformation"], function () {
    if (!fileExists("Areas/HumanResources")) return gulp.src([]);
    var watchArray = getSharedWatch();
    watchArray.push("Areas/HumanResources/Scripts/BankingInformation/**/*.*");

    var entry = {
        "banking.information": [
            'webpack-dev-server/client?http://localhost:9000',
            'webpack/hot/only-dev-server',
            "./Areas/HumanResources/Scripts/BankingInformation/banking.information.application",
        ]
    };

    var devConfig = webpackConfig("development-hot", entry, "./Areas/HumanResources/Build/");
    devConfig.devServer = {
        compress: true,
        port: 9000,
        contentBase: path.join(__dirname, "./Areas/HumanResources/"),
        hot: true,

    };
    devConfig.plugins.push(new webpack.HotModuleReplacementPlugin())

    new webpackDevServer(webpack(devConfig))
        .listen(9000, "localhost", function (err) {
            if (err) throw new gutil.PluginError("webpack-dev-server", err);
            gutil.log("[webpack-dev-server]", "http://localhost:9000/index.html");
        });


    return gulp.watch(watchArray, ["bankingInformation"]);
});

gulp.task("watch-timeManagement", ["timeManagement"], function () {
    if (!fileExists("Areas/TimeManagement")) return gulp.src([]);
    var watchArray = getSharedWatch();
    watchArray.concat([
        "Areas/TimeManagement/Scripts/**/*.*",
        "Scripts/Timepicker/*.*"
    ]);

    var entry = {
        "time.sheet": [
            'webpack-dev-server/client?http://localhost:9000',
            'webpack/hot/only-dev-server',
            "./Areas/TimeManagement/Scripts/TimeSheet/time.sheet.application"
        ],
        "time.approval": [
            'webpack-dev-server/client?http://localhost:9000',
            'webpack/hot/only-dev-server',
            "./Areas/TimeManagement/Scripts/TimeApproval/time.approval.application"
        ]
    }

    var devConfig = webpackConfig("development-hot", entry, "./Areas/TimeManagement/Build/");
    devConfig.devServer = {
        compress: true,
        port: 9000,
        contentBase: path.join(__dirname, "./Areas/TimeManagement/"),
        hot: true,
    }
    devConfig.plugins.push(new webpack.HotModuleReplacementPlugin())

    new webpackDevServer(webpack(devConfig))
        .listen(9000, "localhost", function (err) {
            if (err) throw new gutil.PluginError("webpack-dev-server", err);
            gutil.log("[webpack-dev-server]", "http://localhost:9000/index.html");
        });

    return gulp.watch(watchArray, ["timeManagement"]);
});

gulp.task("stats", function (callback) {
    var entry = {
        "time.sheet": "./Areas/TimeManagement/Scripts/TimeSheet/time.sheet.application",
        "time.approval": "./Areas/TimeManagement/Scripts/TimeApproval/time.approval.application",
        "banking.information": "./Areas/HumanResources/Scripts/BankingInformation/banking.information.application",
    }

    var config = webpackConfig("production", entry, "./webpack-stats");

    // run webpack
    webpack(config, function (err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack-json]", stats.toJson({
            chunkModules: true
        }));
        fs.writeFile('./webpack-stats/stats.json', JSON.stringify(stats.toJson("verbose")), callback)
    });
})

function doBuild(entry, outputDir) {
    var devConfig = webpackConfig("development-cold", entry, outputDir);
    var prodConfig = webpackConfig("production", entry, outputDir);

    var devTask = webpackStream(devConfig, webpack)
        .pipe(gulp.dest("."));

    var prodTask = webpackStream(prodConfig, webpack)
        .pipe(gulp.dest("."));

    return [devTask, prodTask];
}

function getSharedWatch() {
    return [
        "Scripts/resource.manager.js",
        "Scripts/Components/**/*.*"
    ];
}


gulp.task("test", function (done) {

    new karma({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true,
        reporters: ['progress'],
        browsers: ['PhantomJS'],
        testType: "test-cold",
    }, done()).start();
});

gulp.task("debugTests", function (done) {
    new karma({
        configFile: __dirname + '/karma.conf.js',
        autoWatch: true,
        reporters: ['kjhtml'],
        browsers: ['Chrome'],
        testType: "test-hot",
    }, done).start();
});

gulp.task("coverage", function (done) {
    new karma({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true,
        reporters: ['progress', 'coverage'],
        browsers: ['PhantomJS'],
        testType: "test-cold",
    }, function () {
        gulp.src("../Ellucian.Web.Student.Tests/Coverage/index.html")
            .pipe(open());
        done();
    }).start();
});


function fileExists(filePath) {
    try {
        return fs.statSync(filePath).isFile() || fs.statSync(filePath).isDirectory();
    }
    catch (err) {
        return false;
    }
}