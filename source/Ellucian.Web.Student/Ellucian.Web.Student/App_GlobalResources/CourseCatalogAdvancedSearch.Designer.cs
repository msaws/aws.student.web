//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class CourseCatalogAdvancedSearch {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CourseCatalogAdvancedSearch() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.CourseCatalogAdvancedSearch", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Academic Level.
        /// </summary>
        internal static string AcademicLevelCaption {
            get {
                return ResourceManager.GetString("AcademicLevelCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Academic Level.
        /// </summary>
        internal static string AcademicLevelLabel {
            get {
                return ResourceManager.GetString("AcademicLevelLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Academic Level.
        /// </summary>
        internal static string AcademicLevelSelectCaption {
            get {
                return ResourceManager.GetString("AcademicLevelSelectCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Course.
        /// </summary>
        internal static string AddCourseButton {
            get {
                return ResourceManager.GetString("AddCourseButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear.
        /// </summary>
        internal static string ClearButton {
            get {
                return ResourceManager.GetString("ClearButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Courses.
        /// </summary>
        internal static string CourseLabel {
            get {
                return ResourceManager.GetString("CourseLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Day Of Week.
        /// </summary>
        internal static string DayOfWeekLabel {
            get {
                return ResourceManager.GetString("DayOfWeekLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End meeting date is less than Start meeting date.
        /// </summary>
        internal static string EndMeetingDateImproperMessage {
            get {
                return ResourceManager.GetString("EndMeetingDateImproperMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End Meeting Date.
        /// </summary>
        internal static string EndMeetingDateLabel {
            get {
                return ResourceManager.GetString("EndMeetingDateLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End date should be in range of.
        /// </summary>
        internal static string EndMeetingDateRangeMessage {
            get {
                return ResourceManager.GetString("EndMeetingDateRangeMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End meeting date is required.
        /// </summary>
        internal static string EndMeetingDateRequiredMessage {
            get {
                return ResourceManager.GetString("EndMeetingDateRequiredMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Catalog Advanced Search.
        /// </summary>
        internal static string HeaderText {
            get {
                return ResourceManager.GetString("HeaderText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid date format.
        /// </summary>
        internal static string InvalidDateFormatErrorMessage {
            get {
                return ResourceManager.GetString("InvalidDateFormatErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading Advanced Search.
        /// </summary>
        internal static string LoadingMessageText {
            get {
                return ResourceManager.GetString("LoadingMessageText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location.
        /// </summary>
        internal static string LocationLabel {
            get {
                return ResourceManager.GetString("LocationLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Location.
        /// </summary>
        internal static string LocationSelectCaption {
            get {
                return ResourceManager.GetString("LocationSelectCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        internal static string SearchButton {
            get {
                return ResourceManager.GetString("SearchButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Processing.
        /// </summary>
        internal static string SpinnerMessageText {
            get {
                return ResourceManager.GetString("SpinnerMessageText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start meeting date is greater than End meeting date.
        /// </summary>
        internal static string StartMeetingDateImproperMessage {
            get {
                return ResourceManager.GetString("StartMeetingDateImproperMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start Meeting Date.
        /// </summary>
        internal static string StartMeetingDateLabel {
            get {
                return ResourceManager.GetString("StartMeetingDateLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start date should be in range of.
        /// </summary>
        internal static string StartMeetingDateRangeMessage {
            get {
                return ResourceManager.GetString("StartMeetingDateRangeMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start meeting date is required.
        /// </summary>
        internal static string StartMeetingDateRequiredMessage {
            get {
                return ResourceManager.GetString("StartMeetingDateRequiredMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Subject.
        /// </summary>
        internal static string SubjectSelectCaption {
            get {
                return ResourceManager.GetString("SubjectSelectCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Term.
        /// </summary>
        internal static string TermLabel {
            get {
                return ResourceManager.GetString("TermLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Term.
        /// </summary>
        internal static string TermSelectCaption {
            get {
                return ResourceManager.GetString("TermSelectCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Time Of Day.
        /// </summary>
        internal static string TimeOfDayCaption {
            get {
                return ResourceManager.GetString("TimeOfDayCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Of Day.
        /// </summary>
        internal static string TimeOfDayLabel {
            get {
                return ResourceManager.GetString("TimeOfDayLabel", resourceCulture);
            }
        }
    }
}
