﻿/* User Profile/Context Card*/
@import 'variables';
@import 'mixins';

#user-profile-header {
    padding: 10px;
    border-top: 1px solid $color-base-55;
    border-right: 1px solid $color-base-55;
    border-left: 1px solid $color-base-55;
    color: $color-base-10;
}

#user-profile-header.bottom-border {
    border-bottom: 1px solid $color-base-55;
}

#user-profile-header.user-profile-header--no-side-borders {
    border-right: 0;
    border-left: 0;
}

#user-profile-name {
    font-weight: bold;
    font-size: 1.60em;
    padding-bottom: .2%;
    float: left;
}

@media all and (max-width: 480px) {
    #user-profile-name {
        float: none;
    }
}

.user-profile-detail {
    clear: both;
    padding-top: .5em;
}

.user-profile-detail__chosen-name-label {
    font-weight: bold;
    font-style: italic;
}

.user-profile-detail__chosen-name {
    font-style: italic;
}

@include query-medium {
    .user-profile-detail__chosen-name-label {
        padding-left: 1.5rem;
    }
}

.user-profile-detail__personal-pronouns {
    font-style: italic;
}

@include query-medium {
    .user-profile-detail__personal-pronouns {
        padding-left: 1.5rem;
    }
}

#user-profile-left {
    float: left;
}

@media all and (max-width: 480px) {
    #user-profile-left {
        border-bottom: 1px solid $color-base-55;
        width: 100%;
        float: none;
        padding-bottom: .62em;
    }
}

#user-profile-right {
    float: right;
}

#user-profile-details {
    display: inline-block;
    margin-left: .77em;
    vertical-align: top;
}

.css-org-card__wrapper--non-context {
    @include flexbox;
    @include justify-content(center);
}

.css-org-card--non-context {
    display: block;
    border: 0.0625rem solid $color-base-70;
    box-shadow: none;
    margin-bottom: 1rem;

    .css-org-card__badge {
        top: -2rem;
        left: -.75rem;
    }
}
.css-org-card--non-context:hover {
    background: $color-secondary-105;
    border-color: $color-secondary-100;
    cursor: pointer;
}

.css-org-card__badge {
    position: relative;
    top: 2rem;
    left: .5rem;
    float: right;
}

/* Overrides */
/* For use within responsive tables */
.esg-avatar--vertical-align-middle {
    vertical-align: middle;
}

/* Fixes to limit width of landscape card at mobile size */
.esg-person-card--landscape {
    width: 100%;
    max-width: 20rem;

    @include query-xsmall {
        max-width: 25rem;
    }

    @include query-small {
        max-width: 30.625rem;
    }
}

.esg-person-card--centered {
    margin: 2rem auto;
}

.esg-person-card--non-modal {
    border: .125rem solid $color-accent;
    box-shadow: none;
}

.esg-person-card__list-item--bold {
    font-weight: 600;
}

.esg-person-card__footer--with-options {
    border: 0 none;
    padding: 0 1.25rem 1.25rem 1.25rem;

    .esg-button--primary,
    .esg-button--primary:hover,
    .esg-button--primary:focus,
    .esg-button--primary:active,
    .esg-button--primary:active:hover,
    .esg-button--primary:active:focus {
        border-top: .0625rem solid;
    }
}