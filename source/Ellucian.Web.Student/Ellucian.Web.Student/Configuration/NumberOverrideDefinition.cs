﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Ellucian.Web.Student.Configuration
{
    /// <summary>
    /// The NumberOverrideDefinition class contains number format overrides for a culture.
    /// </summary>
    public class NumberOverrideDefinition : ConfigurationSection
    {
        #region Configuration Constant
        public const string NumberOverrideDefinitionName = "ellucian/culture/numberOverride";
        #endregion

        #region Attribute Constants
        private const string NegativePatternName = "negativePattern";
        private const string PositivePatternName = "positivePattern";
        private const string GroupSeparatorName = "groupSeparator";
        private const string DecimalSeparatorName = "decimalSeparator";
        private const string SymbolName = "symbol";
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public NumberOverrideDefinition()
        {
        }
        #endregion

        #region Negative Pattern
        [ConfigurationProperty(NegativePatternName, IsRequired = true)]
        public int NegativePattern
        {
            get { return (int)this[NegativePatternName]; }
            set { this[NegativePatternName] = value; }
        }
        #endregion

        #region Positive Pattern
        [ConfigurationProperty(PositivePatternName, IsRequired = true)]
        public int PositivePattern
        {
            get { return (int)this[PositivePatternName]; }
            set { this[PositivePatternName] = value; }
        }
        #endregion

        #region Group Separator
        [ConfigurationProperty(GroupSeparatorName, IsRequired = true)]
        public string GroupSeparator
        {
            get { return (string)this[GroupSeparatorName]; }
            set { this[GroupSeparatorName] = value; }
        }
        #endregion

        #region Decimal Separator
        [ConfigurationProperty(DecimalSeparatorName, IsRequired = true)]
        public string DecimalSeparator
        {
            get { return (string)this[DecimalSeparatorName]; }
            set { this[DecimalSeparatorName] = value; }
        }
        #endregion

        #region Symbol
        [ConfigurationProperty(SymbolName, IsRequired = true)]
        public string Symbol
        {
            get { return (string)this[SymbolName]; }
            set { this[SymbolName] = value; }
        }
        #endregion
    }
}