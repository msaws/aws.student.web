﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Ellucian.Web.Student.Configuration
{
    /// <summary>
    /// The DateTimeOverrideDefinition class contains DateTime format overrides for a culture.
    /// </summary>
    public class DateTimeOverrideDefinition : ConfigurationSection
    {
        #region Configuration Constant
        public const string DateTimeFormatOverrideDefinitionName = "ellucian/culture/dateTimeOverride";
        #endregion

        #region Attribute Constants
        private const string ShortDateTimePatternName = "shortDateTimePattern";
        private const string LongDateTimePatternName = "longDateTimePattern";
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public DateTimeOverrideDefinition()
        {
        }
        #endregion

        #region Short DateTime Pattern
        [ConfigurationProperty(ShortDateTimePatternName, IsRequired = true)]
        public string ShortDateTimePattern
        {
            get { return (string)this[ShortDateTimePatternName]; }
            set { this[ShortDateTimePatternName] = value; }
        }
        #endregion

        #region Long DateTime Pattern
        [ConfigurationProperty(LongDateTimePatternName, IsRequired = true)]
        public string LongDateTimePattern
        {
            get { return (string)this[LongDateTimePatternName]; }
            set { this[LongDateTimePatternName] = value; }
        }
        #endregion
    }
}