﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;
using Ellucian.Web.Mvc.Cache;
using Ellucian.Web.Mvc.Session;
using System.Net;


namespace Ellucian.Web.Student
{
    public class StudentApplication : System.Web.HttpApplication
    {
        #region Lock Object

        public static readonly object Lock = new object();

        #endregion

        #region Updated Culture

        private static CultureInfo _updatedCulture = null;

        #endregion

        #region AJAX constants

        private static string _xmlHttpRequest = "XMLHttpRequest";
        private static string _xRequestedWith = "x-requested-with";

        #endregion

        #region Application Start

        protected void Application_Start()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            AreaRegistration.RegisterAllAreas();

            // Initialize dependency resolver
            Bootstrapper.Initialize();

            // Update the culture
            UpdateCulture();

            // ASP.NET MVC 4 now uses static classes for routes, filters, and bundles
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = "sid"; // use the session id

            // Override certain display modes for tablets
            // Tablets should get the default views, not the mobile views
            // Don't need to define Mobile or desktop/default display providrs - MVC(4+) does that for us
            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode("")
            {
                ContextCondition = (ctx =>
                    ctx.Request.UserAgent.IndexOf("iPad", StringComparison.OrdinalIgnoreCase) >= 0
                    || (ctx.Request.UserAgent.IndexOf("Android", StringComparison.OrdinalIgnoreCase) >= 0
                            && ctx.Request.UserAgent.IndexOf("mobile", StringComparison.OrdinalIgnoreCase) < 0
                       )
                 )
            });
        }

        #endregion

        #region Application BeginRequest

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (_updatedCulture == null)
            {
                UpdateCulture();
            }

            // Assign the updated culture
            Thread.CurrentThread.CurrentCulture = _updatedCulture;
            Thread.CurrentThread.CurrentUICulture = _updatedCulture;
        }

        #endregion

        #region Custom Cache Type Override

        public override string GetVaryByCustomString(HttpContext context, string arg)
        {
            ICacheOutputCustom custom = DependencyResolver.Current.GetService<ICacheOutputCustom>();

            if (custom != null && arg.Equals(custom.GetUniqueKey()))
            {
                // Injected Custom Cache Type
                return custom.BuildCustomString(context, arg);
            }
            else
            {
                // Other Custom Strings
                return base.GetVaryByCustomString(context, arg);
            }
        }

        #endregion

        #region Application Error

        protected void Application_Error(Object sender, EventArgs e)
        {
            // Determine if the request was AJAX or not
            bool isAjaxCall = false;
            if (Context != null && Context.Request != null && Context.Request.Headers != null)
            {
                isAjaxCall = string.Equals(_xmlHttpRequest, Context.Request.Headers[_xRequestedWith], StringComparison.OrdinalIgnoreCase);
            }

            // Get the most recently thrown exception and its status code
            Exception exception = Server.GetLastError();
            Response.Clear();
            HttpException httpException = exception as HttpException;
            int statusCode = httpException != null ? httpException.GetHttpCode() : (int)HttpStatusCode.InternalServerError;

            // Handle AJAX and non-AJAX requests appropriately
            if (isAjaxCall)
            {
                Response.Redirect("~/Errors/AjaxErrorOccurred");
            }
            else 
            {
                Response.Redirect(string.Format("~/Errors/ErrorOccurred?statusCode={0}",statusCode.ToString()));
            }
        }

        #endregion

        #region Update Culture

        private void UpdateCulture()
        {
            lock (Lock)
            {
                // In case multiple threads lock on this, only update once
                if (_updatedCulture == null)
                {
                    var settings = DependencyResolver.Current.GetService<Mvc.Models.Settings>();

                    // Get the current culture and override if set
                    var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    if (!string.IsNullOrEmpty(settings.Culture))
                    {
                        culture = new CultureInfo(settings.Culture);
                    }

                    if (!string.IsNullOrEmpty(settings.ShortDateFormat) && !"Default".Equals(settings.ShortDateFormat))
                    {
                        culture.DateTimeFormat.ShortDatePattern = settings.ShortDateFormat;
                    }

                    if (settings.CulturePropertyOverrides != null && settings.CulturePropertyOverrides.Count > 0)
                    {
                        foreach (var property in settings.CulturePropertyOverrides)
                        {
                            Utility.ReflectionUtilities.SetProperty(culture, property.Key.Replace('-', '.'), property.Value);
                        }
                    }

                    // Assign the updated culture
                    _updatedCulture = culture;
                }
            }
        }

        #endregion
    }
}