﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Filters
{
    /// <summary>
    /// A controller or action that receives this attribute inherits the security of the specified page.
    /// </summary>
    public class PageAuthorizeAttribute : UserInterfaceAuthorizeAttribute
    {
        private readonly IEnumerable<string> pageIds;

        /// <summary>
        /// <para>The Page Authorize attribute controls access to a controller or an action
        /// based on the user's access to a given page from the sitemap.</para>
        /// <para>If a user has not been granted access via role or user ID to the page
        /// they will be denied access to the action.</para>
        /// </summary>
        /// <param name="menuIds">Menu and/or submenu ids to grant access</param>
        public PageAuthorizeAttribute(params string[] pageIds)
        {
            if (pageIds == null || !pageIds.Any())
            {
                throw new ArgumentNullException("pageIds");
            }
            this.pageIds = pageIds;
        }

        protected override bool AuthorizeCurrentUser(ICurrentUser currentUser)
        {
            var result= SiteService.Get(currentUser).Pages.Select(p => p.Id).Intersect(pageIds).Any();
            return result;
        }
    }
}