﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using System.Web.Mvc;
using Microsoft.IdentityModel.Claims;
using Ellucian.Web.Mvc.Menu;
using System.Web.Routing;
using Ellucian.Web.Security;
using Ellucian.Web.Mvc.Models;

namespace Ellucian.Web.Student.Filters
{
    /// <summary>
    /// The base class for authorizing the current user against menu and page metadata.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public abstract class UserInterfaceAuthorizeAttribute : AuthorizeAttribute
    {
        [Dependency]
        public ISiteService SiteService { get; set; }

        [Dependency]
        public Settings settings { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // If no user and authorization required, deny access
            if (!(httpContext.User is IClaimsPrincipal))
            {
                return false;
            }

            return AuthorizeCurrentUser(new CurrentUser(httpContext.User as IClaimsPrincipal));
        }

        /// <summary>
        /// Default action when a subclass fails AuthorizeCurrentUser.
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if(!string.IsNullOrEmpty(settings.GuestUsername) && filterContext.ActionDescriptor.GetCustomAttributes(typeof(GuestRedirectAttribute), true).Any())
            {
                var actionAttrs = (GuestRedirectAttribute[])filterContext.ActionDescriptor.GetCustomAttributes(typeof(GuestRedirectAttribute), true);
                var action = "";
                var controller = "";
                string area = null;
                bool redirectToGuestUrl = false;
                var attr = actionAttrs[0];
                    action = attr.Action;
                    controller = attr.Controller;
                    area = attr.Area;
                    redirectToGuestUrl=attr.RedirectToGuestUrl;
                if (redirectToGuestUrl)
                {
                    var rvd = new RouteValueDictionary();
                    rvd.Add("controller", controller);
                    rvd.Add("action", action);
                    rvd.Add("area", area ?? "");
                    var qsKeys = filterContext.HttpContext.Request.QueryString.AllKeys;
                    foreach (var key in qsKeys)
                    {
                        rvd.Add(key, filterContext.HttpContext.Request.QueryString[key]);
                    }

                    filterContext.Result = new RedirectToRouteResult(rvd);
                }
                else if(!(filterContext.HttpContext.User is IClaimsPrincipal))
                {
                    return;
                }
                else
                {
                    RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                    redirectTargetDictionary.Add("area", "");
                    redirectTargetDictionary.Add("controller", "Account");
                    redirectTargetDictionary.Add("action", "Unauthorized");
                    redirectTargetDictionary.Add("page", filterContext.RequestContext.HttpContext.Request.RawUrl);
                    filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);

                }
            }
            else
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("area", "");
                redirectTargetDictionary.Add("controller", "Account");
                redirectTargetDictionary.Add("action", "Unauthorized");
                redirectTargetDictionary.Add("page", filterContext.RequestContext.HttpContext.Request.RawUrl);
                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }
        }

        /// <summary>
        /// Subclasses must implement this method to answer if the current user is authorized for the requested item.
        /// If there is no principal on the current request, this method will not be called.
        /// </summary>
        /// <param name="currentUser">the current user, will never be null</param>
        /// <returns></returns>
        protected abstract bool AuthorizeCurrentUser(ICurrentUser currentUser);
    }
}