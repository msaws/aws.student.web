﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Ellucian.Colleague.Api.Client;
using System.Collections.Specialized;
using System.Web.Routing;
using System.Web.Security;
using Ellucian.Web.Mvc.Configuration;
using Ellucian.Web.Mvc.Extensions;
using Ellucian.Web.Mvc.Session;
using Ellucian.Web.Mvc.Models;
using slf4net;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Infrastructure;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Filters
{
    public class SingleSignOnFilter : IAuthorizationFilter
    {
        /*
 * Note, this filter is implemented as an authorozation filter because those filters get executed
 * first in the pipeline and this filter must occur as the very first filter (meaning that in MVC,
 * the filter is added to the list of filters in the Global.asax).
 */

        /*
         * Intended workflows:
         * 1) Request with valid sso token (non empty, valid session data): 
         *       Create the session and redirect to requested page.
         * 2) Request with invalid or missing sso token:
         *       Perform a sign-out (just in-case there was another session active)
         *       and redirect to the requested page. The routing to the requested resource
         *       will determine if the login page is shown.
         * >> Under any workflow, if the redirect portion fails:
         *      Perform a sign-out and redirect to the home controller.
         */

        private ILogger logger;
        private Settings settings;

        public SingleSignOnFilter(Settings settings, ILogger logger)
        {
            this.logger = logger;
            this.settings = settings;
        }

        #region Constants

        /// <summary>
        /// Query string parameter name that represents the SSO information.
        /// </summary>
        const string SSOTokenParameterName = "Token";

        /// <summary>
        /// Portal Behavior Parameter Name
        /// </summary>
        const string PortalBehaviorParameterName = "PortalBehavior";

        #endregion

        #region OnAuthorization Override

        /// <summary>
        /// Applies the filter to the current request.
        /// </summary>
        /// <param name="filterContext">filter context</param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (IsSSORequest(filterContext))
            {
                #region Debug Message
                logger.Debug("WA3SSOAuthorizeFilter: Processing request...");
                #endregion
                Task.Run(async () => await ProcessSSOToken(filterContext)).GetAwaiter().GetResult();

                // redirect (orginial URL minus the token parameter - otherwise we fall into an infinate loop of hitting this filter)
                // this also allows the cookie to be established at the client before making the request for the resource.
                Redirect(filterContext);
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Checks the incoming request to determine if it contains SSO information.
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns>True if the request contains the token parameter.</returns>
        private bool IsSSORequest(AuthorizationContext filterContext)
        {
            bool result = false;

            try
            {
                if (filterContext.HttpContext.Request.QueryString.HasKeys() &&
                    filterContext.HttpContext.Request.QueryString[SSOTokenParameterName] != null)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                logger.Debug(e.ToString());
            }

            return result;
        }

        /// <summary>
        /// Uses the SSO information from this request and attempts to establish a session
        /// using the backing Colleague session information.
        /// </summary>
        /// <param name="filterContext">filter context</param>
        private async Task ProcessSSOToken(AuthorizationContext filterContext)
        {
            #region Debug Messages
            logger.Info("Begin processing SSO token");
            StringBuilder debugMessage = new StringBuilder();
            #endregion

            // Read the incoming SSO Token parameter
            string tokenValue = filterContext.HttpContext.Request.QueryString[SSOTokenParameterName];
            #region Debug Message
            debugMessage.AppendLine("ProcessSSOToken: Incoming Token Value ==> " + tokenValue);
            #endregion

            // Read the incoming Portal Behavior parameter
            string portalBehaviorParameter = filterContext.HttpContext.Request.QueryString[PortalBehaviorParameterName];

            // Determine if Portal Behavior was found
            bool portalBehaviorFound = !string.IsNullOrEmpty(portalBehaviorParameter);

            if (!string.IsNullOrEmpty(tokenValue))
            {
                try
                {
                    // Decode the token value passed in
                    string decodedToken = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(tokenValue));
                    #region Debug Message
                    debugMessage.AppendLine("ProcessSSOToken: Decoded Token: " + decodedToken);
                    #endregion
                    // Split the decoded token
                    string[] decodedTokenParts = decodedToken.Split('*');

                    // Make sure there are three parts to the SSO token
                    if (decodedTokenParts != null && decodedTokenParts.Length == 3)
                    {
                        // The securtity token is the first parameter
                        string securityToken = decodedTokenParts[1];

                        // The control ID is the second parameter
                        string controlId = decodedTokenParts[2];

                        // Request a JWT in exchange for colleague session info.
                        var client = new ColleagueApiClient(settings.ApiBaseUrl, logger);
                        string token = await client.GetTokenAsync(securityToken, controlId);
                        filterContext.HttpContext.User = JwtHelper.CreatePrincipal(token);

                        // Sanitize username claims to remove domain
                        filterContext.HttpContext.User = UserAuthentication.RemoveDomainFromUsernameClaim(filterContext.HttpContext.User as Microsoft.IdentityModel.Claims.IClaimsPrincipal);

                        // Regenerate the token based on the updated set of claims
                        token = JwtHelper.Create(filterContext.HttpContext.User);

                        // set cookies
                        var authCookies = SessionCookieManager.BuildJwtAuthenticationCookies(filterContext.HttpContext.User as Microsoft.IdentityModel.Claims.IClaimsPrincipal, token);
                        foreach (var authCookie in authCookies)
                        {
                        filterContext.HttpContext.Response.Cookies.Add(authCookie);
                        }
                        if (portalBehaviorFound)
                        {
                            var portalCookie = SessionCookieManager.BuildPortalBehaviorCookie(true);
                            filterContext.HttpContext.Response.Cookies.Add(portalCookie);
                        }
                        var fixationCookie = SessionCookieManager.BuildSessionFixationCookie(filterContext.HttpContext.User as Microsoft.IdentityModel.Claims.IClaimsPrincipal);
                        filterContext.HttpContext.Response.Cookies.Add(fixationCookie);
                    }
                }
                catch (Exception e)
                {
                    #region Debug Message
                    logger.Debug(e.ToString());
                    #endregion
                    // if there's a problem trying to use the token, make sure any existing sessions are removed...
                    SignOut(filterContext);
                }
            }
            else
            {
                // if no session is passed in, make sure any existing sessions are removed...
                #region Debug Message
                debugMessage.AppendLine("ProcessSSOToken: Token parameter was null or empty.");
                #endregion
                SignOut(filterContext);
            }

            #region Debug Messages
            logger.Debug(debugMessage.ToString());
            logger.Debug("End processing SSO token");
            #endregion
        }

        /// <summary>
        /// Take the inbound URL and create a redirect to the same URL, minus the token parameter, 
        /// but retaining any other query string parameters that may have been supplied.
        /// </summary>
        /// <param name="filterContext"></param>
        private void Redirect(AuthorizationContext filterContext)
        {
            try
            {
                UriBuilder returnUrl = new UriBuilder(filterContext.HttpContext.Request.Url);

                // Properly set the scheme for SS instances behind SSL terminated load balancers
                if (filterContext.HttpContext.Request.IsForwardedSecureConnection())
                {
                    returnUrl.Scheme = Uri.UriSchemeHttps;

                    // Use the custom port defined in AppSettings, if available
                    if (Bootstrapper.LoadBalancerSecurePort > 0)
                    {
                        returnUrl.Port = Bootstrapper.LoadBalancerSecurePort;
                    }
                    else
                    {
                    // Resets the port to the default for the given scheme
                    returnUrl.Port = -1;
                }
                }

                // clear query and rebuild
                returnUrl.Query = "";

                // only need to rebuild if there were two or more query string parameters
                if (filterContext.HttpContext.Request.QueryString.Count > 1)
                {
                    StringBuilder sb = new StringBuilder();

                    NameValueCollection parameters = filterContext.HttpContext.Request.QueryString;
                    foreach (var parameter in parameters)
                    {
                        if (!parameter.ToString().Equals(SSOTokenParameterName, StringComparison.InvariantCultureIgnoreCase) &&
                            !parameter.ToString().Equals(PortalBehaviorParameterName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append("&");
                            }
                            sb.Append(parameter.ToString());
                            sb.Append("=");
                            sb.Append(HttpUtility.UrlEncode(parameters[parameter.ToString()]));
                        }
                    }

                    // Show the proxy dialog on login by default. Only happens on the homepage
                    if (!parameters.AllKeys.Contains("hideProxyDialog"))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append("&");
                        }
                        sb.Append("hideProxyDialog=false");
                    }

                    returnUrl.Query = sb.ToString();
                }

                filterContext.Result = new RedirectResult(returnUrl.Uri.AbsoluteUri);
            }
            catch (Exception e)
            {
                /* the likely hood of an exception in this method is low (this is the type
                 * of comment us developers love to write and have to eat our words later
                 * on), but, if we do end up here bail on enerything, even if the sso
                 * did process just incase this is malicious.
                 */
                logger.Debug("WA3SSOAuthorizeFilter: Create redirect failed");
                logger.Debug(e.ToString());

                SignOut(filterContext);

                // set a redirect to the home page...
                RouteValueDictionary rvd = new RouteValueDictionary();
                rvd.Add("Area", "");
                rvd.Add("Controller", "Home");
                filterContext.Result = new RedirectToRouteResult(rvd);
            }

        }

        /// <summary>
        /// Signs-out of any active session.
        /// </summary>
        private void SignOut(AuthorizationContext filterContext)
        {
            SessionCookieManager.RemoveSessionCookies(filterContext.HttpContext);

            try
            {
                FormsAuthentication.SignOut();
            }
            catch (Exception ex)
            {
                // Log the error and move on because nothing can be recovered
                logger.Error(ex.Message);
            }
        }

        #endregion

    }
}
