﻿using Ellucian.Web.Mvc.Models;
using Microsoft.IdentityModel.Claims;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Filters
{
    /// <summary>
    /// Skip the authorization if action or controller has GuestRedirect attribute defined and guestName is configured
    /// configuring guestName means we are allowing guest/anonymous access
    /// does not matter if it is anonymous user or logged in user 
    /// This works only to skip authorization
    /// this attribute in conjunction with GuestRedirect attribute can redirect to other URLs depending upon where it is defined to go
    /// </summary>
    public class GuestAuthorizeAttribute : AuthorizeAttribute
    {
        [Dependency]
        public Settings settings { get; set; }

        public GuestAuthorizeAttribute()
            : base()
        {

        }

        public override void OnAuthorization(System.Web.Mvc.AuthorizationContext filterContext)
        {
           
            bool skipAuthorization = !string.IsNullOrEmpty(settings.GuestUsername) && (filterContext.ActionDescriptor.IsDefined(typeof(GuestRedirectAttribute), inherit: true)
                                     || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(GuestRedirectAttribute), inherit: true));

            
            if (skipAuthorization )
            {
                return;
            }

            base.OnAuthorization(filterContext);
        }
    }
}