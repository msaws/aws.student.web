﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Filters
{
    /// <summary>
    /// The CSSOutputCacheAttribute class allows a CSS file to exist in the output cache with the correct content type. 
    /// </summary>
    public class CssOutputCacheAttribute : OutputCacheAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Force the content type to text/css because the content requires this for the browser
            filterContext.HttpContext.Response.ContentType = "text/css";
        }
    }
}