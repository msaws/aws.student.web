﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Client;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Infrastructure;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Filters
{
    /// <summary>
    /// SiteMessageFilter attribute
    /// </summary>
    public class SiteMessageFilter : ActionFilterAttribute
    {
        private Site site;
        private Settings settings;
        private ILogger logger;

        public SiteMessageFilter(Site site, Settings settings, ILogger logger)
        {
            this.site = site;
            this.settings = settings;
            this.logger = logger;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.RouteData != null && !filterContext.HttpContext.Request.IsAjaxRequest())
            {
                var area = filterContext.RouteData.DataTokens.ContainsKey("area") ? filterContext.RouteData.DataTokens["area"] as string : "";
                var controller = filterContext.RouteData.GetRequiredString("controller") as string;
                var action = filterContext.RouteData.GetRequiredString("action") as string;

                var page = site.Pages.FirstOrDefault(x => x.Area == area && x.Controller == controller && x.Action == action);
                var messageId = page != null ? page.MessageId : string.Empty;

                if (string.IsNullOrEmpty(messageId) && page != null)
                {
                    var submenu = site.Submenus.FirstOrDefault(x => MenuIsAncestorToPage(x, page));
                    messageId = submenu != null ? submenu.MessageId : string.Empty;
                }

                if (string.IsNullOrEmpty(messageId) && page != null)
                {
                    var menu = site.Menus.FirstOrDefault(x => MenuIsAncestorToPage(x, page));
                    messageId = menu != null ? menu.MessageId : string.Empty;
                }

                if (!string.IsNullOrEmpty(messageId))
                {
                    var client = new ColleagueApiClient(settings.ApiBaseUrl, null);
                    client.Credentials = JwtHelper.Create(filterContext.HttpContext.User);  // Use the current User's credentials to access the service client.
                    try
                    {
                        var allMessages = Task.Run(async () => await client.GetCachedMiscellaneousTextsAsync()).Result;
                        var matchedMessage = allMessages.FirstOrDefault(x => x.Id == messageId);
                        if(matchedMessage != null)
                        {
                            filterContext.Controller.ViewBag.Message = matchedMessage.Message;
                        }
                        else
                        {
                            logger.Error("Message with ID: " + messageId + " not found.");
                        }
                    }
                    catch(Exception e)
                    {
                        logger.Error(e, "Couldn't get Miscellaneous Text records from Colleague.");
                    }
                    
                }
            }

            base.OnActionExecuting(filterContext);
        }

        private bool MenuIsAncestorToPage(Menu menu, Page page)
        {
            foreach(var item in menu.Items)
            {
                if(item.ItemType == Mvc.Domain.Entities.MenuItemType.Menu)
                {
                    // Recurse into next generation
                    if(MenuIsAncestorToPage(((MenuItemMenu)item).Menu, page))
                    {
                        return true;
                    }
                }
                else if(item.ItemType == Mvc.Domain.Entities.MenuItemType.Page)
                {
                    // Page has been found in Menu - return true
                    if(((MenuItemPage)item).Page.Id == page.Id)
                    {
                        return true;
                    }
                }
            }
            // Page not found anywhere in Menu - return false
            return false;
        }
    }
}
