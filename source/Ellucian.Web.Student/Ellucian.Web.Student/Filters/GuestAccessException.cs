﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Filters
{
    public class GuestAccessException:Exception
    {

        public GuestAccessException()
        { }

        public GuestAccessException(string message, Exception inner):base(message,inner)
        { }
    }
}