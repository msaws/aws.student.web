﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Microsoft.IdentityModel.Claims;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Filters
{
    /// <summary>
    /// A controller or action that receives this attribute inherits the security of the specified (sub)menu.
    /// </summary>
    public class MenuAuthorizeAttribute : UserInterfaceAuthorizeAttribute
    {
        private readonly IEnumerable<string> menuIds;

        /// <summary>
        /// <para>The Menu Authorize attribute controls access to a controller or an action
        /// based on the user's access to a given menu or submenu from the sitemap.</para>
        /// <para>If a user has not been granted access via role or user ID to the given
        /// menu or submenu they will be denied access to the action.</para>
        /// </summary>
        /// <param name="menuIds">Menu and/or submenu ids to grant access</param>
        public MenuAuthorizeAttribute(params string[] menuIds)
        {
            if (menuIds == null || !menuIds.Any())
            {
                throw new ArgumentNullException("menuIds");
            }

            this.menuIds = menuIds;
        }

        protected override bool AuthorizeCurrentUser(ICurrentUser currentUser)
        {
            var site = SiteService.Get(currentUser);
            return site.Menus.Concat(site.Submenus).Select(m => m.Id).Intersect(menuIds).Any();
        }
    }
}