﻿using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;
using Microsoft.IdentityModel.Claims;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ellucian.Web.Student.Filters
{
    /// <summary>
    /// Guest Controller Attribute: used to redirect actions to their guest counterpart.
    /// </summary>
    public class GuestRedirectAttribute : ActionFilterAttribute
    {
        private string _controller;
        private string _action;
        private string _area;
        private string _license;
        private bool _redirectToGuestUrl;

        public string Controller { get { return _controller; } }
        public string Action { get { return _action; } }
        public string Area { get { return _area; } }
        /// <summary>
        /// this determines if true -redirect to guest controller URL
        /// if false- redirect to non-guest controller
        /// </summary>
        public bool RedirectToGuestUrl { get { return _redirectToGuestUrl; } }
         public string License {get {return _license;}}

          [Dependency]
        public ISiteService SiteService { get; set; }
         [Dependency]
        public ILicensingService LicenseService { get; set; }
        [Dependency]
        public Settings settings {get; set;}


        public GuestRedirectAttribute(string controller, string action)
        {
            this._controller = controller;
            this._action = action;
            this._area = null;
            this._redirectToGuestUrl = true;
            this._license=null;
            
        }


        public GuestRedirectAttribute(string controller, string action, string area)
        {
            this._controller = controller;
            this._action = action;
            this._area = area;
            this._redirectToGuestUrl = true;
            this._license=null;
        }
        /// <summary>
        /// Sends the user to the Controller and Action Specified in this constructor
        /// </summary>
        /// <param name="controller">The Controller to redirect to</param>
        /// <param name="action">The Action to redirect to</param>
        /// 
         public GuestRedirectAttribute(string controller, string action, string area, bool redirectToGuest)
        {
            this._controller = controller;
            this._action = action;
            this._area = area;
            this._redirectToGuestUrl=redirectToGuest;
            this._license = null;
        }

        public GuestRedirectAttribute(string controller, string action, string area, string license, bool redirectToGuest)
        {
            this._controller = controller;
            this._action = action;
            this._area = area;
            this._license=license;
            this._redirectToGuestUrl = redirectToGuest;
        }

        public GuestRedirectAttribute(string controller, string action, string area,  string license)
        {
            this._controller = controller;
            this._action = action;
            this._area = area;
            this._license = license;
            this._redirectToGuestUrl = false;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Logged in user
            if (filterContext.HttpContext.User is ClaimsPrincipal)
            {
                //stay where you are
                //it means suppose I am already in planning then I don't have to go anywhere
                if (this.RedirectToGuestUrl == true)
                    return;
                    //I am in guest controller but logged in user
                else if (this.RedirectToGuestUrl == false)
                {

                    // Logged in user: Determine if the user has access to the non-guest version of the action and if so, do the redirect...
                    var currentUser = new CurrentUser(filterContext.HttpContext.User as IClaimsPrincipal);
                    var site = SiteService.Get(currentUser);
                    //check for license too... or site pages are created based upon license...??
                    var isAuthorizedForNonGuestAction = site.Pages.Any(p => p.Action == this.Action && p.Controller == this.Controller && p.Area == (this.Area ?? filterContext.RouteData.DataTokens["area"].ToString()));
                    if (isAuthorizedForNonGuestAction)
                    {
                        List<string> allLicenses = LicenseService.GetLicensedModules().ToList();
                        if (this.License != null && allLicenses.Contains(this.License))
                        {
                            var rvd = new RouteValueDictionary();
                            rvd.Add("controller", this._controller);
                            rvd.Add("action", this._action);
                            if (this._area == null)
                            {
                                rvd.Add("area", filterContext.RouteData.DataTokens["area"] ?? "");
                            }
                            else
                            {
                                rvd.Add("area", this._area);
                            }
                            var qsKeys = filterContext.HttpContext.Request.QueryString.AllKeys;
                            foreach (var key in qsKeys)
                            {
                                rvd.Add(key, filterContext.HttpContext.Request.QueryString[key]);
                            }
                            filterContext.Result = new RedirectToRouteResult(rvd);
                        }


                    }

                }
            }
            else
            {  //if not logged in user and wants to redirect to guest controller with guestusername
                if (!string.IsNullOrEmpty(settings.GuestUsername) && RedirectToGuestUrl)
                {
                    // Anonymous user
                    var rvd = new RouteValueDictionary();
                    rvd.Add("controller", this._controller);
                    rvd.Add("action", this._action);
                    if (this._area == null)
                    {
                        rvd.Add("area", filterContext.RouteData.DataTokens["area"] ?? "");
                    }
                    else
                    {
                        rvd.Add("area", this._area);
                    }
                    var qsKeys = filterContext.HttpContext.Request.QueryString.AllKeys;
                    foreach (var key in qsKeys)
                    {
                        rvd.Add(key, filterContext.HttpContext.Request.QueryString[key]);
                    }
                    filterContext.Result = new RedirectToRouteResult(rvd);
                }
                else
                {
                    //if guestusername not given then stay there
                    //if wants to redirect from guest controller to somewhere else, stay at guest controller only
                    return;
                }
            }
        }
    }
}