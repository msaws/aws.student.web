﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Models.UserProfile;
using System.Text;

namespace Ellucian.Web.Student.Utility
{
    public static class UserProfileHelper
    {
        /// <summary>
        /// Updates the phone model and adds description.
        /// </summary>
        /// <param name="profilePhones"></param>
        /// <param name="phoneTypes"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static List<ProfilePhoneModel> BuildProfilePhones(List<Phone> profilePhones, IEnumerable<Ellucian.Colleague.Dtos.Base.PhoneType> phoneTypes, UserProfileConfiguration2 configuration, bool userHasUpdatePermission)
        {
            var profilePhoneModels = new List<ProfilePhoneModel>();
            ProfilePhoneModel profilePhoneModel = null;
            
            if (profilePhones != null)
            {
                foreach (var profilePhone in profilePhones)
                {
                    if (configuration == null || configuration.ViewablePhoneTypes == null || configuration.ViewablePhoneTypes.Count() == 0 || configuration.ViewablePhoneTypes.Contains(profilePhone.TypeCode))
                    {
                        Ellucian.Colleague.Dtos.Base.PhoneType phoneType = null;
                        if (phoneTypes != null)
                        {
                            phoneType = phoneTypes.Where(pt => pt.Code == profilePhone.TypeCode).FirstOrDefault();
                        }
                        // All phone types may be set as updatable in colleague without specifying
                        // which types individually, otherwise check list of updatable types
                        bool phoneTypeIsUpdatable = configuration != null && configuration.UpdatablePhoneTypes.Contains(profilePhone.TypeCode);
                        bool userCanUpdatePhone = userHasUpdatePermission && phoneTypeIsUpdatable;
                        profilePhoneModel = new ProfilePhoneModel()
                        {
                            Phone = profilePhone,
                            PhoneTypeDescription = phoneType != null ? phoneType.Description : profilePhone.TypeCode,
                            IsUpdatable = userCanUpdatePhone
                        };
                        profilePhoneModels.Add(profilePhoneModel);
                    }
                }
            }
            return profilePhoneModels;
        }

        /// <summary>
        /// Updates email model and adds description and updatability state.
        /// </summary>
        /// <param name="profileEmailAddresses">Email addresses to include in the model</param>
        /// <param name="emailTypes">All email types available</param>
        /// <param name="configuration">User Profile Configuration</param>
        /// <param name="userHasUpdatePermission">Whether or not the user has permission to update emails</param>
        /// <returns>List containing populated email address models</returns>
        public static List<ProfileEmailAddressModel> BuildProfileEmails(IEnumerable<EmailAddress> profileEmailAddresses, IEnumerable<Ellucian.Colleague.Dtos.Base.EmailType> emailTypes, UserProfileConfiguration2 configuration, bool userHasUpdatePermission)
        {
            var profileEmailAddressModels = new List<ProfileEmailAddressModel>();

            if (profileEmailAddresses != null)
            {
                foreach (var profileEmailAddress in profileEmailAddresses)
                {
                    if (configuration == null || configuration.ViewableEmailTypes == null || configuration.ViewableEmailTypes.Count() == 0 || configuration.ViewableEmailTypes.Contains(profileEmailAddress.TypeCode))
                    {
                        Ellucian.Colleague.Dtos.Base.EmailType emailType = null;
                        if (emailTypes != null)
                        {
                            emailType = emailTypes.Where(et => et.Code == profileEmailAddress.TypeCode).FirstOrDefault();
                        }

                        // All email types may be set as updatable in colleague without specifying
                        // which types individually, otherwise check list of updatable types
                        bool emailTypeIsUpdatable = configuration != null && (configuration.AllEmailTypesAreUpdatable || configuration.UpdatableEmailTypes.Contains(profileEmailAddress.TypeCode));
                        bool userCanUpdateEmail = userHasUpdatePermission && emailTypeIsUpdatable;
                        var profileEmailAddressModel = new ProfileEmailAddressModel()
                        {
                            EmailAddress = profileEmailAddress,
                            EmailTypeDescription = emailType != null ? emailType.Description : profileEmailAddress.TypeCode,
                            IsUpdatable = userCanUpdateEmail
                        };
                        profileEmailAddressModels.Add(profileEmailAddressModel);
                    }
                }
            }
            return profileEmailAddressModels;
        }

        /// <summary>
        /// Build User Profile address model.
        /// </summary>
        /// <param name="profileAddresses"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static List<ProfileAddressModel> BuildProfileAddresses(List<Address> profileAddresses, UserProfileConfiguration2 configuration, bool userHasUpdatePermission)
        {
            var profileAddressModels = new List<ProfileAddressModel>();
            if (profileAddresses != null)
            {
                foreach (var profileAddress in profileAddresses)
                {
                    // Address typecode will be comma-delimited when an address has multiple types and is delivered as part of a profile request
                    var addressTypeCodes = profileAddress.TypeCode.Split(',');
                    // Address types cannot be viewed if configuration AddressesOfAllTypesCanBeViewed is set to No and no address types specified.
                    // Address types can be viewed if:
                    //  1) no configuration specified
                    //  2) Addresses of All Types Can be viewed == true 
                    //  3) Addresses of All Types Can be viewed == false with a list of address types specified and an address type matches one of those types 
                    if (configuration == null || configuration.AllAddressTypesAreViewable == true || (configuration.AllAddressTypesAreViewable == false && configuration.ViewableAddressTypes != null && configuration.ViewableAddressTypes.Count() > 0 && configuration.ViewableAddressTypes.Intersect(addressTypeCodes).Any()))
                    {
                        var addressTypeIsUpdatable = configuration != null && configuration.UpdatableAddressTypes.Contains(profileAddress.TypeCode);
                        bool userCanUpdateAddress = userHasUpdatePermission && addressTypeIsUpdatable;
                        ProfileAddressModel profileAddressModel = new ProfileAddressModel()
                        {
                            Address = profileAddress,
                            AddressTypeDescription = profileAddress.Type,
                            IsUpdatable = userCanUpdateAddress
                        };
                        profileAddressModels.Add(profileAddressModel);
                    }
                }
            }
            return profileAddressModels;
        }
    }
}