﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Utility
{
    public static class FormatUtility
    {
        static readonly string CurrencyFormat = "{0:C}";
        static readonly string ShortDateFormat = "{0:d}";

        public static string FormatAsCurrency(object rawObject, IFormatProvider formatProvider = null)
        {
            return FormatObject(rawObject, CurrencyFormat, formatProvider);
        }

        public static string FormatAsShortDate(object rawObject, IFormatProvider formatProvider = null)
        {
            return FormatObject(rawObject, ShortDateFormat, formatProvider);
        }

        private static string FormatObject(object rawObject, string format, IFormatProvider formatProvider = null)
        {
            if (formatProvider != null)
            {
                return string.Format(formatProvider, format, rawObject);
            }
            else
            {
                return string.Format(format, rawObject);
            }
        }
    }
}