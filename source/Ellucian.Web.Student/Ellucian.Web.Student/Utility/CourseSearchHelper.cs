﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Models.Courses;
using Ellucian.Colleague.Api.Client;
using System.Threading.Tasks;
using Ellucian.Web.Student.Infrastructure;
using slf4net;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Adapters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Diagnostics;
using System.Dynamic;

namespace Ellucian.Web.Student.Utility
{
    public class CourseSearchHelper
    {
        private const string _nonTermSortPrefix = "^***TempSort***^";
        private readonly bool _includeDegreePlan = false;
        private readonly ColleagueApiClient _serviceClient;
        private readonly ILogger _logger;
        private readonly IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Creates a new, empty instance of a CourseSearchHelper.
        /// </summary>
        public CourseSearchHelper(ColleagueApiClient serviceClient, ILogger logger, IAdapterRegistry adapterRegistry, bool includeDegreePlan)
        {
            this._includeDegreePlan = includeDegreePlan;
            this._serviceClient = serviceClient;
            this._logger = logger;
            this._adapterRegistry = adapterRegistry;
        }

        /// <summary>
        /// Creates a new, empty instance of a CourseSearchHelper.
        /// </summary>
        public CourseSearchHelper(ColleagueApiClient serviceClient, ILogger logger, IAdapterRegistry adapterRegistry)
        {
            this._includeDegreePlan = false;
            this._serviceClient = serviceClient;
            this._logger = logger;
            this._adapterRegistry = adapterRegistry;
        }

        // Parameterless constructor for unit tests
        public CourseSearchHelper()
        {

        }
        /// <summary>
        /// create course search result model
        /// </summary>
        /// <param name="searchParameters"></param>
        /// <returns></returns>
        public async Task<CourseSearchResult> CreateCourseSearchResultAsync(CatalogSearchCriteriaModel searchParameters)
        {

            string keyword = searchParameters.Keyword;


            if (searchParameters.KeywordComponents != null && searchParameters.KeywordComponents.Any() && searchParameters.KeywordComponents.Any(c => c.IsEmpty() == false))
            {
                keyword = BuildKeywordString(keyword, searchParameters.KeywordComponents);
            }

            int pageNumber = searchParameters.PageNumber;
            int quantityPerPage = searchParameters.QuantityPerPage;

            // If quantity per page or page number is <= 0, fix it before courses are retrieved
            if (quantityPerPage <= 0)
            {
                quantityPerPage = CourseSearchResult.QuantityPerPage;
            }
            if (pageNumber <= 0)
            {
                pageNumber = 1;
            }


            RequirementGroup requirementGroup = null;
            string requirementCode = null;
            if (!string.IsNullOrEmpty(searchParameters.Requirement))
            {
                // If there is a requirement code, subrequirement id, AND a group - then do a search on the specific requirement group.
                // Otherwise, assume it is a requisite requirement code and the search is for the entire requirement.
                if (!string.IsNullOrEmpty(searchParameters.Subrequirement) && !string.IsNullOrEmpty(searchParameters.Group))
                {
                    requirementGroup = new RequirementGroup();
                    requirementGroup.RequirementCode = searchParameters.Requirement;
                    requirementGroup.SubRequirementId = searchParameters.Subrequirement;
                    requirementGroup.GroupId = searchParameters.Group;
                }
                else
                {
                    requirementCode = searchParameters.Requirement;
                }
            }
            DateTime date;
            DateTime? startDate = !string.IsNullOrEmpty(searchParameters.StartDate) ? DateTime.TryParse(searchParameters.StartDate, out date) ? date : default(DateTime?) : default(DateTime?);
            DateTime? endDate = !string.IsNullOrEmpty(searchParameters.EndDate) ? DateTime.TryParse(searchParameters.EndDate, out date) ? date : default(DateTime?) : default(DateTime?);

            var resultTask = _serviceClient.SearchCoursesAsync(searchParameters.CourseIds, searchParameters.Subjects, searchParameters.AcademicLevels,
                searchParameters.CourseLevels, searchParameters.CourseTypes, searchParameters.TopicCodes, searchParameters.Terms, searchParameters.Days, searchParameters.Locations, searchParameters.Faculty, searchParameters.StartTime, searchParameters.EndTime, keyword, requirementGroup, requirementCode, searchParameters.SectionIds, searchParameters.OnlineCategories, searchParameters.OpenSections, startDate, endDate, quantityPerPage, pageNumber);
            var subjectTask = _serviceClient.GetCachedSubjectsAsync();
            var academicLevelTask = _serviceClient.GetCachedAcademicLevelsAsync();
            var courseLevelTask = _serviceClient.GetCachedCourseLevelsAsync();
            var courseTypeTask = _serviceClient.GetCachedCourseTypesAsync();
            var topicCodeTask = _serviceClient.GetCachedTopicCodesAsync();
            var locationTask = _serviceClient.GetCachedLocationsAsync();
            await Task.WhenAll(resultTask, subjectTask, academicLevelTask, courseLevelTask, courseTypeTask, topicCodeTask, locationTask);
            var result = resultTask.Result;
            var subjectDTOs = subjectTask.Result.Where(s => s.ShowInCourseSearch); //not all subjects should be shown in search
            var academicLevelDTOs = academicLevelTask.Result;
            var courseLevelDTOs = courseLevelTask.Result;
            var courseTypeDTOs = courseTypeTask.Result;
            var topicCodeDTOs = topicCodeTask.Result;
            var locationDTOs = locationTask.Result;


            var facultyDTOs = new List<Faculty>();
            var facultyQueryCriteria = new FacultyQueryCriteria();
            facultyQueryCriteria.FacultyIds = result.Faculty.Where(f => f != null).Select(f => f.Value).Distinct();
            if (facultyQueryCriteria.FacultyIds != null && facultyQueryCriteria.FacultyIds.Count() > 0)
            {
                facultyDTOs = (await _serviceClient.QueryFacultyAsync(facultyQueryCriteria)).ToList();
            }
            foreach (var facultyId in facultyQueryCriteria.FacultyIds)
            {
                var facultyDTO = facultyDTOs.Where(f => f.Id == facultyId).FirstOrDefault();
                if (facultyDTO == null)
                {
                    facultyDTO = new Faculty() { Id = facultyId, FirstName = "Unavailable", LastName = "Unavailable", ProfessionalName = "Unavailable", EmailAddresses = new List<string>(), Phones = new List<Phone>() };
                    facultyDTOs.Add(facultyDTO);
                }
            }

            // first filter out courses that should not show in the search
            result.CurrentPageItems = result.CurrentPageItems.Where(csr => (subjectDTOs.Where(s => s.Code == csr.SubjectCode)).Count() > 0);
            var termDTOs = await GetPlanningTermsAsync();
            var viewModel = new CourseSearchResult(result, subjectDTOs, academicLevelDTOs, courseLevelDTOs, locationDTOs, facultyDTOs, courseTypeDTOs, topicCodeDTOs, termDTOs);
            viewModel.Keyword = searchParameters.Keyword;
            viewModel.Requirement = searchParameters.Requirement;
            viewModel.Subrequirement = searchParameters.Subrequirement;
            viewModel.RequirementText = searchParameters.RequirementText == null ? string.Empty : searchParameters.RequirementText;
            viewModel.AdvancedSearchText = GetAdvancedSearchText(searchParameters.KeywordComponents, startDate, endDate);
            viewModel.Group = searchParameters.Group == null ? string.Empty : searchParameters.Group;
            viewModel.CourseIds = searchParameters.CourseIds == null ? string.Empty : string.Join(",", searchParameters.CourseIds.AsEnumerable<string>());

            //get the student's available term list (exclusing finished terms) to go with the viewmodel
            viewModel.PlanTerms = termDTOs;

            //get the sections and location info for each course
            if (viewModel.Courses.Count() > 0)
            {
                try
                {
                    var instructionalMethodsTask = GetAllInstructionalMethodsAsync();
                    var locationsTask = GetAllLocationsAsync(locationDTOs);
                    var sessionCyclesTask = GetAllSessionCyclesAsync();
                    var yearlyCyclesTask = GetAllYearlyCyclesAsync();
                    await Task.WhenAll(instructionalMethodsTask, locationsTask, sessionCyclesTask, yearlyCyclesTask);
                    BuildSlimCoursesSectionsModel(viewModel, instructionalMethodsTask.Result, locationsTask.Result, sessionCyclesTask.Result, yearlyCyclesTask.Result);

                    IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Requirement> requisiteRequirements = new List<Ellucian.Colleague.Dtos.Student.Requirements.Requirement>();
                    var requirementCodes = viewModel.Courses.Where(c => c.Requisites != null).SelectMany(c => c.Requisites).Select(r => r.RequirementCode).Where(r => !string.IsNullOrEmpty(r)).Distinct().ToList();
                    if (requirementCodes != null && requirementCodes.Count() > 0)
                    {
                        requisiteRequirements = await _serviceClient.QueryRequirementsAsync(requirementCodes);
                    }

                    // Get the Requisites for each Course
                    foreach (CourseFullModel csm in viewModel.CourseFullModels)
                    {
                        // Build requisites simply based on the course's requisites. There won't be any section requisites.
                        csm.CourseRequisites = await BuildRequisiteListAsync(csm.Requisites, new List<SectionRequisite>(), requisiteRequirements);
                    }
                }
                catch (InvalidOperationException e)
                {
                    _logger.Error(e.Message);
                    viewModel.CourseFullModels = new List<CourseFullModel>();
                }
            }

            return viewModel;
        }

        /// <summary>
        /// Asynchronously retrieves section data for the given course.
        /// </summary>
        /// <param name="courseId">The ID of the course for which to retrieve the full section data list</param>
        /// <param name="sectionIds">A collection of matching section IDs to retrieve</param>
        /// <returns>A collection of sections and associated data for this course, sorted by number (BIO100-01, BIO100-02, BIO100-03, ...)</returns>
        public async Task<SectionRetrievalModel> CreateSectionRetrievalModelAsync(string courseId, IEnumerable<string> sectionIds)
        {
            SectionRetrievalModel sections = new SectionRetrievalModel();
            if (!string.IsNullOrEmpty(courseId) && sectionIds != null && sectionIds.Count() > 0)
            {
                try
                {
                    var allSections = await _serviceClient.GetSections4Async(sectionIds.ToList(), false);
                    if (allSections != null && allSections.Count() > 0)
                    {
                        allSections = allSections.OrderBy(s => s.Number);
                    }
                    var course = await _serviceClient.GetCourseAsync(courseId);
                    if (allSections.Count() > 0 && course != null)
                    {
                        sections.Course = course;
                        // The only terms we need are those terms open for registration because only sections in those terms (or null terms) are returned in the results
                        sections.TermsAndSections = BuildTermsAndSectionsList(allSections.ToList(), await GetRegistrationTermsAsync(), await GetAllLocationsAsync(),
                            await GetAllBuildingsAsync(), await GetAllRoomsAsync(), await GetAllInstructionalMethodsAsync(), await GetFacultyDictionaryFromSectionsAsync(allSections.ToList()));
                    }
                }
                catch (Exception e)
                {
                    sections.TermsAndSections = new List<TermSectionList>();
                    _logger.Error(e.ToString());
                }
            }

            return sections;
        }



        public async Task<SectionDetailsModel> CreateSectionDetailsAsync(string sectionId, string studentId, string personId)
        {
            SectionDetailsModel model = null;
            List<SectionTransferStatus> transferStatuses = null;

            if (!string.IsNullOrEmpty(sectionId))
            {
                try
                {
                    Section3 section = await _serviceClient.GetSection3Async(sectionId, useCache: false);

                    RegistrationOptions registrationOptions = null;

                    if (personId != null)
                    {
                        registrationOptions = await _serviceClient.GetRegistrationOptionsAsync(personId);
                    }

                    if (section != null)
                    {
                        var sectionMapAdapter = _adapterRegistry.GetAdapter<Section3, SectionDetailsModel>();
                        model = sectionMapAdapter.MapToType(section);
                        //term
                        if (!string.IsNullOrEmpty(section.TermId))
                        {
                            model.TermDisplay = section.TermId;
                            var allTerms = await _serviceClient.GetCachedTermsAsync();
                            if (allTerms != null)
                            {
                                var term = allTerms.Where(t => t.Code == section.TermId).FirstOrDefault();
                                if (term != null)
                                {
                                    model.TermDisplay = term.Description;
                                }
                            }
                        }
                        if (transferStatuses == null)
                        {
                            transferStatuses = (await _serviceClient.GetSectionTransferStatusesAsync()).ToList();
                        }

                        // If the section has a topic code, pass the correct TopicCode object to the model
                        var topicCodes = await _serviceClient.GetCachedTopicCodesAsync();
                        if (topicCodes != null)
                        {
                            var topicCodeObject = topicCodes.Where(t => t.Code == model.TopicCode).FirstOrDefault();
                            if (topicCodeObject != null)
                                model.TopicCodeDescription = topicCodeObject.Description;
                        }


                        var transferStatus = transferStatuses.Where(x => x.Code == model.TransferStatus).FirstOrDefault();
                        model.TransferStatusDescription = transferStatus != null ? transferStatus.Description : string.Empty;

                        // Use the RegistrationOptions to factor in whether the pass/audit options should be displayed.
                        if (registrationOptions != null && !registrationOptions.GradingTypes.Contains(GradingType.Audit))
                        {
                            model.AuditIsRestricted = true;
                        }
                        if (registrationOptions != null && !registrationOptions.GradingTypes.Contains(GradingType.PassFail))
                        {
                            model.PassNoPassIsRestricted = true;
                        }

                        Course2 course = await _serviceClient.GetCourseAsync(section.CourseId);
                        if (course != null)
                        {
                            List<string> coreqItems = new List<string>();
                            List<SectionDetailsFacultyModel> instructorItems = new List<SectionDetailsFacultyModel>();
                            var timeItems = new List<SectionDetailsMeetingModel>();

                            // Build requisites into the model
                            // All section requisites come from the section
                            var sectionRequisites = section.SectionRequisites;
                            // Other requisites might come from the course or the section, depending on override
                            var requisites = new List<Requisite>();
                            if (section.OverridesCourseRequisites)
                            {
                                requisites = section.Requisites.ToList();
                                // Even if the section overrides the course requisites, any protected course requisites will also still apply
                                var protectedCourseRequisites = course.Requisites.Where(r => !string.IsNullOrEmpty(r.RequirementCode) && r.IsProtected).ToList();
                                if (protectedCourseRequisites.Count() > 0)
                                {
                                    requisites.AddRange(protectedCourseRequisites);
                                }
                            }
                            else
                            {
                                // If section does not override course requisites, get all the requirement-coded requisites from the course.
                                // But course-coded corequisites should still come from the section (they will exist pre-conversion only)
                                requisites = course.Requisites.Where(r => !string.IsNullOrEmpty(r.RequirementCode)).ToList();
                                requisites.AddRange(section.Requisites.Where(r => !string.IsNullOrEmpty(r.CorequisiteCourseId)));
                            }
                            // Build the requisite items from the selected requisites and section requisites
                            model.RequisiteItems = await BuildRequisiteListAsync(requisites, sectionRequisites);

                            //Time & Location
                            Dictionary<string, Location> allLocations = await GetAllLocationsAsync();
                            Dictionary<string, Building> allBuildings = await GetAllBuildingsAsync();
                            Dictionary<string, Room> allRooms = await GetAllRoomsAsync();
                            Dictionary<string, InstructionalMethod> allInstrMethods = await GetAllInstructionalMethodsAsync();

                            var meetingModelItems = new List<SectionDetailsMeetingModel>();

                            for (int i = 0; i < section.Meetings.Count(); i++)
                            {
                                var meetingModel = new SectionDetailsMeetingModel(section.Meetings.ElementAt(i), section.Location, allLocations, allBuildings, allRooms, allInstrMethods);

                                meetingModelItems.Add(meetingModel);
                            }
                            model.TimeLocationItems = meetingModelItems;

                            //Instructors
                            foreach (var fac in section.FacultyIds)
                            {
                                try
                                {
                                    var facultyDto = await _serviceClient.GetFacultyAsync(fac);

                                    SectionDetailsFacultyModel faculty = new SectionDetailsFacultyModel();
                                    if (facultyDto != null)
                                    {
                                        faculty.Name = NameHelper.FacultyDisplayName(facultyDto);
                                    }
                                    else { faculty.Name = "Unavailable"; }
                                    faculty.EmailAddresses = facultyDto.EmailAddresses.Select(e => e.Trim()).ToList();
                                    foreach (Phone p in facultyDto.Phones)
                                    {
                                        if (!string.IsNullOrEmpty(p.Number) && !string.IsNullOrEmpty(p.Extension))
                                        {
                                            faculty.PhoneNumbers.Add(p.Number + " " + GlobalResources.GetString(GlobalResourceFiles.CourseResources, "FacultyPhoneExtensionPrefix") + p.Extension);
                                        }
                                        else if (!string.IsNullOrEmpty(p.Number))
                                        {
                                            faculty.PhoneNumbers.Add(p.Number);
                                        }
                                        else if (!string.IsNullOrEmpty(p.Extension))
                                        {
                                            faculty.PhoneNumbers.Add(GlobalResources.GetString(GlobalResourceFiles.CourseResources, "FacultyPhoneExtensionPrefix") + p.Extension);
                                        }
                                    }

                                    instructorItems.Add(faculty);
                                }
                                catch (Exception ex)
                                {
                                    //ignore this faculty for purposes of building display list
                                    _logger.Debug(ex, "Non-fatal exception thrown while processing faculty: " + fac);
                                }
                            }
                            model.InstructorItems = instructorItems;

                            //Books Total Cost
                            //If there are no book the Books Total Display will be empty.
                            if (section.Books != null && section.Books.Count() > 0)
                            {
                                //note that -1 is different from 0, and equates to an unknown cost, where zero is reserved for when there truly is no book cost
                                //This way we can only show required or option
                                decimal booksCostRequired = -1.0m;
                                decimal booksCostOptional = -1.0m;
                                foreach (SectionBook book in section.Books)
                                {
                                    try
                                    {
                                        Book actualBook = await _serviceClient.GetBookAsync(book.BookId);
                                        if (actualBook != null && actualBook.Price.HasValue)
                                        {
                                            if (book.IsRequired)
                                            {
                                                if (booksCostRequired == -1)
                                                {
                                                    booksCostRequired = 0.0m;
                                                }
                                                booksCostRequired += actualBook.Price.Value;
                                            }
                                            else
                                            {
                                                if (booksCostOptional == -1)
                                                {
                                                    booksCostOptional = 0.0m;
                                                }
                                                booksCostOptional += actualBook.Price.Value;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //ignore this book for cost computing purposes; failure will be logged in service client
                                        _logger.Debug(ex, "Non-fatal exception thrown while processing book: " + book.BookId);
                                    }
                                }
                                // If there ended up being at least one required book (it is not negative) set required cost
                                if (booksCostRequired >= 0)
                                {
                                    model.BooksCostRequired = FormatUtility.FormatAsCurrency(booksCostRequired);
                                }
                                // If there ended up being at least one optional book set optional cost
                                if (booksCostOptional >= 0)
                                {
                                    model.BooksCostOptional = FormatUtility.FormatAsCurrency(booksCostOptional);
                                }
                            }
                        }
                    }

                    // If the current user isn't the student (this is an advisor)
                    model.GradingOptionsMatch = true;
                    if (!string.IsNullOrEmpty(studentId) && personId != studentId)
                    {
                        var otherRegistrationOptions = await _serviceClient.GetRegistrationOptionsAsync(studentId);

                        // Check to see if they share the same grading options
                        if (registrationOptions.GradingTypes.Count() != otherRegistrationOptions.GradingTypes.Count())
                        {
                            model.GradingOptionsMatch = false;
                        }
                        else
                        {
                            foreach (var type in registrationOptions.GradingTypes)
                            {
                                if (!otherRegistrationOptions.GradingTypes.Contains(type))
                                {
                                    model.GradingOptionsMatch = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    //just log errors and return an empty model for no data
                    _logger.Error(e.ToString());
                    model = null;
                }
            }

            return model;
        }

        public async Task<IEnumerable<Term>> GetPlanningTermsAsync()
        {
            var termDTOs = (await _serviceClient.GetCachedPlanningTermsAsync()).ToList();
            if (termDTOs == null || !termDTOs.Any())
            {
                // If there are no planning terms (which could be a larger list to account for future terms where courses can be planned) get terms open for registration to allow sections to display for clients not using planning.
                termDTOs = (await _serviceClient.GetCachedRegistrationTermsAsync()).ToList();
            }
            return termDTOs.Where(t => t.EndDate > DateTime.Now).OrderBy(t => t.ReportingYear).ThenBy(t => t.Sequence).ToList();
        }

        public async Task<IEnumerable<Term>> GetRegistrationTermsAsync()
        {
            var termDTOs = (await _serviceClient.GetCachedRegistrationTermsAsync()).ToList();
            return termDTOs.Where(t => t.EndDate > DateTime.Now).OrderBy(t => t.ReportingYear).ThenBy(t => t.Sequence).ToList();
        }

        /// <summary>
        /// Gets all buildings out of the repository.
        /// </summary>
        /// <returns>A Dictionary of all buildings mapped by their IDs</returns>
        public async Task<Dictionary<string, Building>> GetAllBuildingsAsync()
        {
            _logger.Info("guest issue- getall buildings called by " + _serviceClient.Credentials);
            Dictionary<string, Building> buildings = new Dictionary<string, Building>();
            List<Building> allBuildings = (await _serviceClient.GetCachedBuildingsAsync()).ToList();
            if (allBuildings != null && allBuildings.Count > 0)
            {
                foreach (Building building in allBuildings)
                {
                    if (!buildings.ContainsKey(building.Code))
                    {
                        buildings.Add(building.Code, building);
                    }
                }
            }

            return buildings;
        }

        /// <summary>
        /// Gets all rooms out of the repository.
        /// </summary>
        /// <returns>A Dictionary of all rooms mapped by their IDs</returns>
        public async Task<Dictionary<string, Room>> GetAllRoomsAsync()
        {
            _logger.Info("guest issue- getall buildings called by " + _serviceClient.Credentials);

            Dictionary<string, Room> rooms = new Dictionary<string, Room>();
            List<Room> allRooms = (await _serviceClient.GetCachedRoomsAsync()).ToList();
            if (allRooms != null && allRooms.Count > 0)
            {
                foreach (Room room in allRooms)
                {
                    if (!rooms.ContainsKey(room.Id))
                    {
                        rooms.Add(room.Id, room);
                    }
                }
            }

            return rooms;
        }

        /// <summary>
        /// Gets all locations out of the repository.
        /// </summary>
        /// <returns>A Dictionary of all locations mapped by their IDs</returns>
        public async Task<Dictionary<string, Location>> GetAllLocationsAsync(IEnumerable<Location> locationDTOs = null)
        {
            _logger.Info("guest issue- getall buildings called by " + _serviceClient.Credentials);

            Dictionary<string, Location> locations = new Dictionary<string, Location>();
            List<Location> allLocations = new List<Location>();
            if (locationDTOs == null)
            {
                allLocations = (await _serviceClient.GetCachedLocationsAsync()).ToList();
            }
            else
            {
                allLocations = locationDTOs.ToList();
            }
            if (allLocations != null && allLocations.Count > 0)
            {
                foreach (Location loc in allLocations)
                {
                    if (!locations.ContainsKey(loc.Code))
                    {
                        locations.Add(loc.Code, loc);
                    }
                }
            }

            return locations;
        }

        /// <summary>
        /// Gets all instructional methods out of the repository.
        /// </summary>
        /// <returns>A Dictionary of all instructional methods mapped by their IDs</returns>
        public async Task<Dictionary<string, InstructionalMethod>> GetAllInstructionalMethodsAsync()
        {
            Dictionary<string, InstructionalMethod> methods = new Dictionary<string, InstructionalMethod>();
            List<InstructionalMethod> allInstrMethods = (await _serviceClient.GetCachedInstructionalMethodsAsync()).ToList();
            if (allInstrMethods != null && allInstrMethods.Count > 0)
            {
                foreach (InstructionalMethod method in allInstrMethods)
                {
                    if (!methods.ContainsKey(method.Code))
                    {
                        methods.Add(method.Code, method);
                    }
                }
            }

            return methods;
        }

        /// <summary>
        /// Gets all session cycles out of the repository.
        /// </summary>
        /// <returns>A Dictionary of all session cycles mapped by their IDs</returns>
        public async Task<Dictionary<string, SessionCycle>> GetAllSessionCyclesAsync()
        {
            Dictionary<string, SessionCycle> cycles = new Dictionary<string, SessionCycle>();
            List<SessionCycle> allSessionCycles = (await _serviceClient.GetCachedSessionCyclesAsync()).ToList();
            if (allSessionCycles != null && allSessionCycles.Count > 0)
            {
                foreach (SessionCycle cycle in allSessionCycles)
                {
                    if (!cycles.ContainsKey(cycle.Code))
                    {
                        cycles.Add(cycle.Code, cycle);
                    }
                }
            }

            return cycles;
        }

        /// <summary>
        /// Gets all yearly cycles out of the repository.
        /// </summary>
        /// <returns>A Dictionary of all yearly cycles mapped by their IDs</returns>
        public async Task<Dictionary<string, YearlyCycle>> GetAllYearlyCyclesAsync()
        {
            Dictionary<string, YearlyCycle> cycles = new Dictionary<string, YearlyCycle>();
            List<YearlyCycle> allYearlyCycles = (await _serviceClient.GetCachedYearlyCyclesAsync()).ToList();
            if (allYearlyCycles != null && allYearlyCycles.Count > 0)
            {
                foreach (YearlyCycle cycle in allYearlyCycles)
                {
                    if (!cycles.ContainsKey(cycle.Code))
                    {
                        cycles.Add(cycle.Code, cycle);
                    }
                }
            }

            return cycles;
        }
        /// <summary>
        /// Retrieve items to populate advanced search form
        /// </summary>
        /// <returns></returns>
        public async Task<CatalogAdvancedSearchItemsModel> RetrieveAdvancedSearchItemsAsync()
        {
            var catalogConfiguration =await _serviceClient.GetCachedCourseCatalogConfigurationAsync();

            Task<IEnumerable<Term>> termsTask =  Task.FromResult<IEnumerable<Term>>(new List<Term>());
            Task<IEnumerable<Location>> locationsTask = Task.FromResult<IEnumerable<Location>>(new List<Location>());
            Task<IEnumerable<AcademicLevel>> academicLevelsTask = Task.FromResult<IEnumerable<AcademicLevel>>(new List<AcademicLevel>());
            Task<IEnumerable<CourseType>> courseTypesTask = Task.FromResult<IEnumerable<CourseType>>(new List<CourseType>());

            Dictionary<CatalogFilterType, bool> filterSettings = RetrieveDynamicViewModel(catalogConfiguration, null).DefaultFiltersSettings;
           
           var subjectsTask = _serviceClient.GetCachedSubjectsAsync();
           if (!filterSettings.ContainsKey(CatalogFilterType.Terms) || filterSettings[CatalogFilterType.Terms])
           {
               termsTask = _serviceClient.GetCachedRegistrationTermsAsync();
           }
           if (!filterSettings.ContainsKey(CatalogFilterType.Locations) || filterSettings[CatalogFilterType.Locations])
           {
               locationsTask = _serviceClient.GetCachedLocationsAsync();
           }
           if (!filterSettings.ContainsKey(CatalogFilterType.AcademicLevels) || filterSettings[CatalogFilterType.AcademicLevels])
           {
               academicLevelsTask = _serviceClient.GetCachedAcademicLevelsAsync();
           }
           if (!filterSettings.ContainsKey(CatalogFilterType.CourseTypes) || filterSettings[CatalogFilterType.CourseTypes])
           {
               courseTypesTask = _serviceClient.GetCachedCourseTypesAsync();
           }
            await Task.WhenAll(subjectsTask, termsTask, locationsTask, academicLevelsTask,courseTypesTask);

            var subjects = subjectsTask.Result!=null?subjectsTask.Result.Where(s => s.ShowInCourseSearch).OrderBy(s => s.Description).ToList():null;
            var terms = termsTask.Result != null ? termsTask.Result.OrderBy(s => s.Description).ToList() : null ;
            var locations = locationsTask.Result!=null?locationsTask.Result.ToList():null;
            var academicLevels = academicLevelsTask.Result!=null?academicLevelsTask.Result.ToList():null;
            var courseTypes =courseTypesTask.Result!=null? courseTypesTask.Result.Where(s=>s.ShowInCourseSearch).ToList():null;
            List<Tuple<string, int, int>> timeRanges = new List<Tuple<string, int, int>>();
            if (!filterSettings.ContainsKey(CatalogFilterType.TimesOfDay) || filterSettings[CatalogFilterType.TimesOfDay])
            {
                timeRanges.Add(new Tuple<string, int, int>(GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "TimeFilterEarlyMorning"), 0, 480));
                timeRanges.Add(new Tuple<string, int, int>(GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "TimeFilterMorning"), 480, 720));
                timeRanges.Add(new Tuple<string, int, int>(GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "TimeFilterAfternoon"), 720, 960));
                timeRanges.Add(new Tuple<string, int, int>(GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "TimeFilterEvening"), 960, 1200));
                timeRanges.Add(new Tuple<string, int, int>(GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "TimeFilterNight"), 1200, 1440));
            }
            List<Tuple<string, string, bool>> daysOfWeek = new List<Tuple<string, string, bool>>();
            if (!filterSettings.ContainsKey(CatalogFilterType.DaysOfWeek) || filterSettings[CatalogFilterType.DaysOfWeek])
            {
                daysOfWeek.Add(new Tuple<string, string, bool>("Sunday", "0", false));
                daysOfWeek.Add(new Tuple<string, string, bool>("Monday", "1", false));
                daysOfWeek.Add(new Tuple<string, string, bool>("Tuesday", "2", false));
                daysOfWeek.Add(new Tuple<string, string, bool>("Wednesday", "3", false));
                daysOfWeek.Add(new Tuple<string, string, bool>("Thursday", "4", false));
                daysOfWeek.Add(new Tuple<string, string, bool>("Friday", "5", false));
                daysOfWeek.Add(new Tuple<string, string, bool>("Saturday", "6", false));
            }
            CatalogAdvancedSearchItemsModel searchModel = new CatalogAdvancedSearchItemsModel(subjects, terms, locations, academicLevels,courseTypes, timeRanges, daysOfWeek, catalogConfiguration);
            return searchModel;
        }

        /// <summary>
        /// this is to map JSON string of advanced search model to course search criteria model
        /// while deserializing, we want JsonConvert to handle property names by using camel case resolver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CatalogSearchCriteriaModel RetrieveCatalogSearchCriteriaFromAdvance(string model)
        {
            CatalogAdvancedSearchViewModel searchModel = new CatalogAdvancedSearchViewModel();
            if (model != null)
            {
                var settings = new JsonSerializerSettings
                {
                   ContractResolver = new CamelCasePropertyNamesContractResolver(),
                };
                searchModel = JsonConvert.DeserializeObject<CatalogAdvancedSearchViewModel>(model, settings);
                CatalogSearchCriteriaModel searchCriteriaModel = new CatalogSearchCriteriaModel(searchModel);
                return searchCriteriaModel;
            }
            return new CatalogSearchCriteriaModel();
        }

        /// <summary>
        /// this is to map other searches that are done by passing query string (and not through POST of advanced search model)
        /// to course search criteria model
        /// </summary>
        /// <param name="searchParameters"></param>
        /// <returns></returns>
        public CatalogSearchCriteriaModel RetrieveCatalogSearchCriteria(string searchParameters)
        {
            CatalogSearchCriteriaModel searchModel = new CatalogSearchCriteriaModel();
            if (searchParameters != null)
            {
                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                };
                searchModel = JsonConvert.DeserializeObject<CatalogSearchCriteriaModel>(searchParameters, settings);
            }

            return searchModel;
        }

        #region PrivateMethods

        /// <summary>
        /// Builds list of Requisites
        /// </summary>
        /// <param name="requisites">List of requisite DTOs for a given course/section</param>
        /// <returns>List of RequisiteModel items</returns>
        private async Task<IEnumerable<RequisiteModel>> BuildRequisiteListAsync(IEnumerable<Requisite> requisites, IEnumerable<SectionRequisite> sectionRequisites, IEnumerable<Ellucian.Colleague.Dtos.Student.Requirements.Requirement> requisiteRequirements = null)
        {
            var reqModels = new List<RequisiteModel>();

            foreach (var requisite in requisites)
            {
                var reqModel = new RequisiteModel();
                reqModel.IsRequired = requisite.IsRequired;
                // First determine which display text extension to use.
                if (requisite.IsRequired == true && requisite.CompletionOrder == RequisiteCompletionOrder.Previous)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "PreviousRequisiteRequired");
                }
                if (requisite.IsRequired == false && requisite.CompletionOrder == RequisiteCompletionOrder.Previous)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "PreviousRequisiteRecommended");
                }
                if (requisite.IsRequired == true && requisite.CompletionOrder == RequisiteCompletionOrder.Concurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "ConcurrentRequisiteRequired");
                }
                if (requisite.IsRequired == false && requisite.CompletionOrder == RequisiteCompletionOrder.Concurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "ConcurrentRequisiteRecommended");
                }
                if (requisite.IsRequired == true && requisite.CompletionOrder == RequisiteCompletionOrder.PreviousOrConcurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "PreviousOrConcurrentRequired");
                }
                if (requisite.IsRequired == false && requisite.CompletionOrder == RequisiteCompletionOrder.PreviousOrConcurrent)
                {
                    reqModel.DisplayTextExtension = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "PreviousOrConcurrentRecommended");
                }
                // Get the requirement text or course name to display, depending on what's in the incoming requisite
                if (!string.IsNullOrEmpty(requisite.RequirementCode))
                {
                    Ellucian.Colleague.Dtos.Student.Requirements.Requirement req = null;
                    if (requisiteRequirements != null)
                    {
                        req = requisiteRequirements.Where(r => r.Code == requisite.RequirementCode).FirstOrDefault();
                    }
                    else
                    {
                        req = await _serviceClient.GetRequirementAsync(requisite.RequirementCode);
                    }
                    if (req != null)
                    {
                        reqModel.DisplayText = string.IsNullOrEmpty(req.DisplayText) ? GlobalResources.GetString(GlobalResourceFiles.CourseResources, "DefaultRequisiteText") : req.DisplayText;
                    }
                }
                else if (!string.IsNullOrEmpty(requisite.CorequisiteCourseId))
                {
                    // This will occur only if client has not yet converted to the new format that always uses requirements
                    var reqCourse = await _serviceClient.GetCourseAsync(requisite.CorequisiteCourseId);
                    if (reqCourse != null)
                    {
                        reqModel.DisplayText = reqCourse.SubjectCode + GlobalResources.GetString(GlobalResourceFiles.CourseResources, "CourseDelimiter") + reqCourse.Number;
                    }
                }
                else
                {
                    // If requisite does not have a requirement code or a course id it's invalid. Log it.
                    _logger.Info("Requisite is not compliant with any expected format--ignoring");
                }

                reqModels.Add(reqModel);
            }
            foreach (var requisite in sectionRequisites)
            {
                var reqModel = new RequisiteModel();
                reqModel.IsRequired = requisite.IsRequired;
                if (requisite.CorequisiteSectionIds != null && requisite.CorequisiteSectionIds.Count() > 0)
                {
                    // Single required or recommended corequisite section
                    if (requisite.CorequisiteSectionIds.Count() == 1)
                    {
                        reqModel.DisplayText = await BuildSectionDetailTextAsync(requisite.CorequisiteSectionIds.ElementAt(0));
                        reqModel.DisplayTextExtension = requisite.IsRequired ? GlobalResources.GetString(GlobalResourceFiles.CourseResources, "ConcurrentRequisiteRequired") : GlobalResources.GetString(GlobalResourceFiles.CourseResources, "ConcurrentRequisiteRecommended");
                    }
                    else
                    {
                        // Multiple corequisite section: ie Take X of the following sections 
                        reqModel.DisplayText = string.Format(GlobalResources.GetString(GlobalResourceFiles.CourseResources, "MultipleNumberOfSectionsRequisiteText"), requisite.NumberNeeded.ToString());
                        for (int i = 0; i < requisite.CorequisiteSectionIds.Count(); i++)
                        {
                            var connector = (i == 0 ? " " : ", ");
                            reqModel.DisplayText += connector + await BuildSectionDetailTextAsync(requisite.CorequisiteSectionIds.ElementAt(i));
                        }
                        reqModel.DisplayTextExtension = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "ConcurrentRequisiteRequired");
                    }
                }
                reqModels.Add(reqModel);
            }
            return reqModels;
        }


        private async Task<string> BuildSectionDetailTextAsync(string sectionId)
        {
            var reqSection = await _serviceClient.GetSection3Async(sectionId);
            var reqCourse = await _serviceClient.GetCourseAsync(reqSection.CourseId);
            return (reqCourse.SubjectCode + GlobalResources.GetString(GlobalResourceFiles.CourseResources, "CourseDelimiter") + reqCourse.Number + GlobalResources.GetString(GlobalResourceFiles.CourseResources, "CourseDelimiter") + reqSection.Number);
        }

        /// <summary>
        /// Builds a Dictionary of all Faculty members that are associated with any of the provided sections.
        /// </summary>
        /// <param name="sections">A list of sections for which to examine and build the Dictionary of Faculty members</param>
        /// <returns>A Dictionary mapping the associated Faculty items by their IDs</returns>
        private async Task<Dictionary<string, Faculty>> GetFacultyDictionaryFromSectionsAsync(List<Section3> sections)
        {
            Dictionary<string, Faculty> faculty = new Dictionary<string, Faculty>();
            List<string> ids = new List<string>();
            if (sections != null && sections.Count > 0)
            {
                foreach (Section3 section in sections)
                {
                    if (section.FacultyIds != null)
                    {
                        foreach (string id in section.FacultyIds)
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(id) && !faculty.ContainsKey(id))
                                {
                                    faculty.Add(id, await _serviceClient.GetFacultyAsync(id));
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.Error(ex, "Unable to get faculty: " + id);
                            }
                        }
                    }
                }
            }

            return faculty;
        }

        private string GetAdvancedSearchText(List<CatalogAdvancedSearchKeywordComponentsModel> keywordComponents, DateTime? searchStartDate, DateTime? searchEndDate)
        {
            if ((keywordComponents == null || !keywordComponents.Any()) && searchStartDate == null && searchEndDate == null)
            {
                return string.Empty;
            }
            List<string> itemList = new List<string>();
            foreach (var item in keywordComponents)
            {
                string componentText = KeywordComponentFormatter(item);
                if (!string.IsNullOrEmpty(componentText))
                {
                    itemList.Add(KeywordComponentFormatter(item));
                }
            }
            if ((searchStartDate != null && searchStartDate > DateTime.MinValue) && (searchEndDate != null && searchEndDate < DateTime.MaxValue))
            {
                itemList.Add(GlobalResources.GetString(GlobalResourceFiles.CourseResources, "MeetingDatesWithin") + searchStartDate.Value.ToShortDateString() + " - " + searchEndDate.Value.ToShortDateString());
            }
            else if (searchStartDate != null && searchStartDate > DateTime.MinValue)
            {
                itemList.Add(GlobalResources.GetString(GlobalResourceFiles.CourseResources, "MeetingDatesAfter") + searchStartDate.Value.ToShortDateString());
            }
            else if (searchEndDate != null && searchEndDate < DateTime.MaxValue)
            {
                itemList.Add(GlobalResources.GetString(GlobalResourceFiles.CourseResources, "MeetingDatesBefore") + searchEndDate.Value.ToShortDateString());
            }
            return string.Join(", ", itemList);
        }
        private string KeywordComponentFormatter(CatalogAdvancedSearchKeywordComponentsModel keywordComponent)
        {
            string delim = !string.IsNullOrEmpty(GlobalResources.GetString(GlobalResourceFiles.CourseResources, "CourseDelimiter")) ? GlobalResources.GetString(GlobalResourceFiles.CourseResources, "CourseDelimiter") : "-";
            string keywordString = string.Empty;
            if (string.IsNullOrEmpty(keywordComponent.Subject) && string.IsNullOrEmpty(keywordComponent.CourseNumber) && string.IsNullOrEmpty(keywordComponent.Section))
            {
                return keywordString;
            }
            if (!string.IsNullOrEmpty(keywordComponent.Section))
            {
                return keywordComponent.Subject + delim + keywordComponent.CourseNumber + delim + keywordComponent.Section;
            }
            else if (!string.IsNullOrEmpty(keywordComponent.CourseNumber))
            {
                return keywordComponent.Subject + delim + keywordComponent.CourseNumber;
            }
            else
            {
                return keywordComponent.Subject;
            }
        }

        #endregion

        #region methodsToCreateObjects
        /// <summary>
        /// Constructs a partial CoursesSectionsModel set for the given CourseSearchResult object. No section data will be retrieved, 
        /// so sections must instead be loaded on demand elsewhere (use BuildTermsAndSectionsList).
        /// </summary>
        /// <param name="csr">The original CourseSearchResult object</param>
        /// <param name="allInstrMethods">A dictionary mapping InstructionalMethod codes to their full records</param>
        /// <param name="allLocations">A dictionary mapping Location codes to their full objects</param>
        /// <exception cref="System.InvalidOperationException">Thrown when the model cannot be constructed for any reason</exception>
        /// <returns>A reference to the updated CourseSearchResult object</returns>
        public CourseSearchResult BuildSlimCoursesSectionsModel(CourseSearchResult csr, Dictionary<string, InstructionalMethod> allInstrMethods,
            Dictionary<string, Location> allLocations, Dictionary<string, SessionCycle> allSessionCycles, Dictionary<string, YearlyCycle> allYearlyCycles)
        {
            if (csr != null)
            {
                try
                {
                    csr.CourseFullModels = new List<CourseFullModel>();

                    // for each Course in the result set...
                    foreach (var course in csr.Courses)
                    {
                        //get ready to add this course to the list
                        CourseFullModel model = new CourseFullModel(course);

                        //set the Locations display
                        if (model.LocationCodes != null && model.LocationCodes.Any())
                        {
                            List<string> locs = new List<string>();
                            foreach (string loc in model.LocationCodes)
                            {
                                if (allLocations.ContainsKey(loc) && !locs.Contains(allLocations[loc].Description))
                                {
                                    locs.Add(allLocations[loc].Description);
                                }
                            }

                            model.LocationsDisplay = string.Join(", ", locs.ToArray());
                        }
                        model.LocationCycleRestrictionDescriptions = BuildCourseLocationCycleRestrictionModels(course, allLocations, allSessionCycles, allYearlyCycles);


                        //in the slim model, this data is retrieved on demand to speed up searches
                        model.TermsAndSections = new List<TermSectionList>();

                        //are there any sections we can get dynamically?
                        model.HasSections = course.MatchingSectionIds.Count > 0;

                        //add this course and its section list to the viewModel
                        csr.CourseFullModels.Add(model);
                    }
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException("Could not hydrate CoursesSectionsModel: " + e.Message);
                }
            }

            return csr;
        }

        /// <summary>
        /// Builds the full set of Section data for the given Sections. This will include locations, buildings, rooms, terms, faculty,
        /// and instructional methods, gathered into a List of TermSectionList objects.
        /// </summary>
        /// <param name="matchingSections">The set of Sections for which to build the full view-model</param>
        /// <param name="terms">A Dictionary of terms that might appear in this set</param>
        /// <param name="allLocations">A Dictionary of locations that might appear in this set</param>
        /// <param name="allBuildings">A Dictionary of buildings that might appear in this set</param>
        /// <param name="allRooms">A Dictionary of rooms that might appear in this set</param>
        /// <param name="allInstrMethods">A Dictionary of instructional methods that might appear in this set</param>
        /// <param name="allFaculty">A Dictionary of faculty that might appear in this set</param>
        /// <returns>The full List of TermSectionList objects grouping full Section data by Term</returns>
        public List<TermSectionList> BuildTermsAndSectionsList(List<Section3> matchingSections, IEnumerable<Term> terms,
            Dictionary<string, Location> allLocations, Dictionary<string, Building> allBuildings, Dictionary<string, Room> allRooms,
            Dictionary<string, InstructionalMethod> allInstrMethods, Dictionary<string, Faculty> allFaculty)
        {
            //group the matching sections into groups based on the attached term
            var groupedByTerm = matchingSections.GroupBy(s => s.TermId == null ? string.Empty : s.TermId).Select(x => x.ToList()).ToList();

            List<TermSectionList> result = new List<TermSectionList>();

            //first have to loop through and move the non-term sections to the appropriate bucket
            //this is probably a little wasteful to check them all but it doesn't assume that the empty TermId list is at groupedByTerm[0]
            for (int i = 0; i < groupedByTerm.Count; i++)
            {
                if (groupedByTerm[i].Count > 0 && string.IsNullOrEmpty(groupedByTerm[i][0].TermId))
                {
                    var group = groupedByTerm[i];
                    for (int j = 0; j < group.Count; j++)
                    {
                        Section3 section = group[j];

                        //first check the other grouped-by lists to see if an existing group can hold this item
                        bool foundPlacement = false;
                        foreach (var outerGroup in groupedByTerm)
                        {
                            if (outerGroup != group)
                            {
                                //check both the real groups and the "fake" lists that we've temporarily created
                                if (outerGroup.Count > 0 && !string.IsNullOrEmpty(outerGroup[0].TermId) &&
                                    (terms.Any(x => x.Code == outerGroup[0].TermId) ||
                                        (outerGroup[0].TermId.StartsWith(_nonTermSortPrefix) &&
                                        terms.Any(x => x.Code == outerGroup[0].TermId.Replace(_nonTermSortPrefix, string.Empty)))))
                                {
                                    Term t = (outerGroup[0].TermId.StartsWith(_nonTermSortPrefix)) ? terms.First(x => x.Code == outerGroup[0].TermId.Replace(_nonTermSortPrefix, string.Empty)) :
                                        terms.First(x => x.Code == outerGroup[0].TermId);
                                    if (t != null && section.StartDate != null && t.StartDate != null && t.EndDate != null &&
                                        section.StartDate >= t.StartDate && section.StartDate <= t.EndDate)
                                    {
                                        foundPlacement = true;
                                        section.TermId = _nonTermSortPrefix + t.Code;   //doesn't really belong in this list, so use the prefix marker
                                        outerGroup.Add(section);
                                        group.RemoveAt(j);
                                        j--;
                                        break;
                                    }
                                }
                            }
                        }

                        //if it did not find anything then we need to look through the whole set to find a suitable term and make a new group
                        if (!foundPlacement)
                        {
                            Term match = null;
                            foreach (var term in terms)
                            {
                                if (section.StartDate != null && term.StartDate != null && term.EndDate != null &&
                                    section.StartDate >= term.StartDate && section.StartDate <= term.EndDate)
                                {
                                    match = term;
                                    break;
                                }
                            }
                            if (match != null)
                            {
                                section.TermId = _nonTermSortPrefix + match.Code;
                                groupedByTerm.Add(new List<Section3>() { section });
                            }

                            //either way, at this point we have to get rid of the term-less, ungrouped item
                            //if we didn't find any match, then too bad, we can't support that section
                            group.RemoveAt(j);
                            j--;
                        }
                    }
                    break;
                }
            }

            //now build out the full model for each term and its associated sections
            foreach (var group in groupedByTerm)
            {
                TermSectionList tsl = new TermSectionList();
                foreach (var section in group)
                {
                    if (!string.IsNullOrEmpty(section.TermId))
                    {
                        #region Figure out the Term
                        //does this section have a term assigned?
                        if (terms.Any(x => x.Code == section.TermId))
                        {
                            tsl.Term = terms.First(x => x.Code == section.TermId);
                        }
                        else
                        {
                            if (section.TermId.StartsWith(_nonTermSortPrefix))
                            {
                                string actualTermKey = section.TermId.Replace(_nonTermSortPrefix, string.Empty);
                                if (terms.Any(x => x.Code == actualTermKey))
                                {
                                    tsl.Term = terms.First(x => x.Code == actualTermKey);
                                }
                                //reset so that this one is marked as non-term later
                                section.TermId = string.Empty;
                            }
                            if (tsl.Term == null)
                            {
                                //no match, so it will be skipped later when we add the tsl to the collected TermsAndSections list
                            }
                        }
                        #endregion

                        //a null Term means that we're skipping this one altogether
                        if (tsl.Term != null)
                        {
                            SectionInstructorsModel m = new SectionInstructorsModel();
                            m.Instructors = new List<Faculty>();
                            m.Section = new CourseSearchResultSection(section);
                            if (section.Location != null && allLocations.ContainsKey(section.Location))
                            {
                                m.Section.LocationDisplay = allLocations[section.Location].Description;
                            }
                            else
                            {
                                m.Section.LocationDisplay = m.Section.Location;
                            }

                            m.Section.StartDateDisplay = section.StartDate.ToShortDateString();
                            m.Section.EndDateDisplay = section.EndDate.HasValue ? section.EndDate.Value.ToShortDateString() : string.Empty;

                            //is this a non-term section?
                            m.Section.IsNonTermOffering = (string.IsNullOrEmpty(section.TermId));
                            if (m.Section.IsNonTermOffering)
                            {
                                m.Section.IsNonStandardDates = true;
                            }

                            //does this section have non-standard dates?
                            if (!m.Section.IsNonTermOffering)
                            {
                                if (!m.Section.StartDate.ToShortDateString().Equals(tsl.Term.StartDate.ToShortDateString(), StringComparison.OrdinalIgnoreCase))
                                {
                                    m.Section.IsNonStandardDates = true;
                                }
                                else if (m.Section.EndDate.HasValue && !m.Section.EndDate.Value.ToShortDateString().Equals(tsl.Term.EndDate.ToShortDateString(), StringComparison.OrdinalIgnoreCase))
                                {
                                    m.Section.IsNonStandardDates = true;
                                }
                            }

                            //format the MeetingTime objects
                            m.Section.FormattedMeetingTimes = new List<CourseSearchResultMeetingTime>();
                            foreach (SectionMeeting2 mt in m.Section.Meetings)
                            {
                                CourseSearchResultMeetingTime csrmt = new CourseSearchResultMeetingTime(mt);
                                if (mt.Room != null && allRooms.ContainsKey(mt.Room))
                                {
                                    Room r = allRooms[mt.Room];
                                    if (allBuildings.ContainsKey(r.BuildingCode))
                                    {
                                        Building b = allBuildings[r.BuildingCode];
                                        csrmt.BuildingDisplay = b.Description;
                                    }
                                    csrmt.RoomDisplay = r.Code;
                                }
                                csrmt.InstructionalMethodDisplay = allInstrMethods.ContainsKey(mt.InstructionalMethodCode) ? allInstrMethods[mt.InstructionalMethodCode].Description : mt.InstructionalMethodCode;
                                m.Section.FormattedMeetingTimes.Add(csrmt);
                            }
                            if (section.FacultyIds != null)
                            {
                                foreach (string id in section.FacultyIds)
                                {
                                    if (allFaculty.ContainsKey(id))
                                    {
                                        m.Instructors.Add(allFaculty[id]);
                                    }
                                }
                                m.FacultyDisplay = string.Join(", ", m.Instructors.Select(i => NameHelper.FacultyDisplayName(i)).ToArray());
                            }
                            tsl.Sections.Add(m);
                        }
                    }
                }
                if (tsl.Term != null)
                {
                    result.Add(tsl);
                }
            }

            var sorted = result.OrderBy(t => t.Term.ReportingYear).ThenBy(t => t.Term.Sequence);
            return sorted.ToList();
        }

        public List<LocationCycleRestrictionModel> BuildCourseLocationCycleRestrictionModels(Course2 course, Dictionary<string, Location> allLocations, Dictionary<string, SessionCycle> allSessionCycles, Dictionary<string, YearlyCycle> allYearlyCycles)
        {
            List<LocationCycleRestrictionModel> courseLocationCycleRestrictions = new List<LocationCycleRestrictionModel>();
            if (course != null && course.LocationCycleRestrictions != null && course.LocationCycleRestrictions.Any())
            {
                //First correctly order the items
                var sortedCourseCycleRestrictions = course.LocationCycleRestrictions.OrderBy(l => l.Location).ThenBy(ls => ls.SessionCycle);
                //for each LocationCycleRestriction for the course, build the correct location cycle restriciton model.
                foreach (var lcr in course.LocationCycleRestrictions)
                {
                    var sessionCycleDescription = (!string.IsNullOrEmpty(lcr.SessionCycle) && allSessionCycles.ContainsKey(lcr.SessionCycle)) ? allSessionCycles[lcr.SessionCycle].Description : GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "EverySessionCycle");
                    var yearlyCycleDescription = (!string.IsNullOrEmpty(lcr.YearlyCycle) && allYearlyCycles.ContainsKey(lcr.YearlyCycle)) ? allYearlyCycles[lcr.YearlyCycle].Description : GlobalResources.GetString(GlobalResourceFiles.CourseSearchResultResources, "EveryYearlyCycle");
                    var existingLcr = courseLocationCycleRestrictions.Where(ee => ee.LocationCode == lcr.Location).FirstOrDefault();
                    if (existingLcr != null)
                    {
                        existingLcr.SessionCycleDescription.Add(sessionCycleDescription);
                        existingLcr.YearlyCycleDescription.Add(yearlyCycleDescription);
                    }
                    else
                    {
                        var lcrm = new LocationCycleRestrictionModel();
                        if (allLocations.ContainsKey(lcr.Location))
                        {
                            lcrm.LocationDescription = allLocations[lcr.Location].Description;
                            lcrm.LocationCode = lcr.Location;
                            lcrm.SessionCycleDescription.Add(sessionCycleDescription);
                            lcrm.YearlyCycleDescription.Add(yearlyCycleDescription);
                            //add the new LocationCycleRestrictionModel to the list
                            courseLocationCycleRestrictions.Add(lcrm);
                        }
                    }
                }

                // add a LocationCycleRestrictionModel for all other locations if necessary
                // If the course has locations then use those to determine if there any others - otherwise look at the overall number of locations
                if ((course.LocationCodes != null && course.LocationCodes.Any() && course.LocationCodes.Count() > course.LocationCycleRestrictions.Count()) || ((course.LocationCodes == null || !course.LocationCodes.Any()) && allLocations != null && allLocations.Any() && allLocations.Count() > course.LocationCycleRestrictions.Count()))
                {
                    if (courseLocationCycleRestrictions.Count() > 0)
                    {
                        var lcrm = new LocationCycleRestrictionModel();
                        lcrm.LocationDescription = GlobalResources.GetString(GlobalResourceFiles.CourseResources, "AllOtherLocationsLabel");
                        lcrm.SessionCycleDescription.Add((!string.IsNullOrEmpty(course.TermSessionCycle) && allSessionCycles.ContainsKey(course.TermSessionCycle) ? allSessionCycles[course.TermSessionCycle].Description : GlobalResources.GetString(GlobalResourceFiles.CourseResources, "EverySessionCycle")));
                        lcrm.YearlyCycleDescription.Add((!string.IsNullOrEmpty(course.TermYearlyCycle)) && allYearlyCycles.ContainsKey(course.TermYearlyCycle) ? allYearlyCycles[course.TermYearlyCycle].Description : GlobalResources.GetString(GlobalResourceFiles.CourseResources, "EveryYearlyCycle"));


                        //add the all other location LocationCycleRestrictionModel to the list
                        courseLocationCycleRestrictions.Add(lcrm);
                    }
                }
            }
            return courseLocationCycleRestrictions;
        }

        /// <summary>
        /// Take a set of keyword components and generate the correct Lucene query syntax to return the desired results.
        /// </summary>
        /// <param name="keywordComponents"></param>
        /// <returns></returns>
        public string BuildKeywordString(string keyword, List<CatalogAdvancedSearchKeywordComponentsModel> keywordComponents)
        {

            if (keywordComponents == null || !keywordComponents.Any())
            {
                return keyword;
            }
            StringBuilder keywordString = new StringBuilder(keyword == null ? string.Empty : keyword);

            for (int i = 0; i < keywordComponents.Count(); i++)
            {
                if (!keywordComponents[i].IsEmpty())
                {
                    if (keywordString.Length > 0)
                    {
                        keywordString.Append(" or ");
                    }
                    if (!string.IsNullOrEmpty(keywordComponents[i].Section))
                    {
                        keywordString.Append("name:" + KeywordComponentFormatter(keywordComponents[i]));
                    }
                    else if (!string.IsNullOrEmpty(keywordComponents[i].CourseNumber))
                    {
                        keywordString.Append("name:" + KeywordComponentFormatter(keywordComponents[i]));
                    }
                    else
                    {
                        keywordString.Append("subject:" + keywordComponents[i].Subject);
                    }
                }
            }

            return keywordString.ToString();
        }
        #endregion


        public dynamic RetrieveDynamicViewModel(CourseCatalogConfiguration catalogConfiguration, Object viewModel=null)
        {
            dynamic dynamicModel = new ExpandoObject();
            dynamicModel.ViewModel = viewModel;
            dynamicModel.DefaultFiltersSettings = catalogConfiguration != null && catalogConfiguration.CatalogFilterOptions != null ? catalogConfiguration.CatalogFilterOptions.ToDictionary(s => s.Type, s => !s.IsHidden) : new Dictionary<CatalogFilterType, bool>();
            return dynamicModel;

        }
    }
}



