﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// Static utility for processing dates
    /// </summary>
    public static class DateUtility
    {
        /// <summary>
        /// Gets the local time zone offset in +/-HHMM format
        /// </summary>
        public static TimeSpan LocalTimeZoneOffset
        {
            get
            {
                return GetTimeZoneOffset(DateTime.Now);
            }
        }

        /// <summary>
        /// Gets the local time zone offset in +/-HHMM format
        /// </summary>
        public static string LocalTimeZoneOffsetString
        {
            get
            {
                return GetTimeZoneOffsetString(DateTime.Now);
            }
        }

        /// <summary>
        /// Get the time-zone offset for a specified date
        /// </summary>
        /// <param name="date">The date for which you want the offset</param>
        /// <returns>The TimeZoneOffset</returns>
        public static TimeSpan GetTimeZoneOffset(DateTime date)
        {
            return date.ToLocalTime() - date;
        }

        /// <summary>
        /// Get the time-zone offset for a specified date in +/-HHMM format
        /// </summary>
        /// <param name="date">The date for which you want the offset</param>
        /// <returns>The TimeZoneOffset</returns>
        public static string GetTimeZoneOffsetString(DateTime date)
        {
            var offset = GetTimeZoneOffset(date);
            return (offset > TimeSpan.Zero ? "+" : "-") + offset.ToString("hhmm");
        }

        /// <summary>
        /// Evaluates whether a date is in a specified date range
        /// </summary>
        /// <param name="date">Date to evaluate</param>
        /// <param name="rangeStart">Range start date</param>
        /// <param name="rangeEnd">Range end date</param>
        /// <returns>Boolean comparison value</returns>
        public static bool IsDateInRange(DateTime date, DateTime? rangeStart, DateTime? rangeEnd)
        {
            // Assign default values for null range dates
            DateRangeDefaults(ref rangeStart, ref rangeEnd);
            return (date.Date >= rangeStart.Value.Date) && (date.Date <= rangeEnd.Value.Date);
        }

        /// <summary>
        /// Evaluate whether two date ranges overlap
        /// </summary>
        /// <param name="range1Start">Range 1 start date</param>
        /// <param name="range1End">Range 1 end date</param>
        /// <param name="range2Start">Range 2 start date</param>
        /// <param name="range2End">Range 2 end date</param>
        /// <returns>Boolean comparison value</returns>
        public static bool IsRangeOverlap(DateTime? range1Start, DateTime? range1End, DateTime? range2Start, DateTime? range2End)
        {
            DateRangeDefaults(ref range1Start, ref range1End);
            DateRangeDefaults(ref range2Start, ref range2End);
            return IsDateInRange(range1Start.Value, range2Start, range2End)
                || IsDateInRange(range1End.Value, range2Start, range2End)
                || IsDateInRange(range2Start.Value, range1Start, range1End)
                || IsDateInRange(range2End.Value, range1Start, range1End);
        }

        /// <summary>
        /// Return default values for a date range and put them in order
        /// </summary>
        /// <param name="rangeStart">Start of date range</param>
        /// <param name="rangeEnd">End of date range</param>
        public static void DateRangeDefaults(ref DateTime? rangeStart, ref DateTime? rangeEnd)
        {
            // Assign default values for null range dates
            if (rangeStart == null)
            {
                rangeStart = DateTime.MinValue;
            }
            if (rangeEnd == null)
            {
                rangeEnd = DateTime.MaxValue;
            }
            // Put range dates in correct order
            if (rangeStart > rangeEnd)
            {
                var temp = rangeEnd;
                rangeEnd = rangeStart;
                rangeStart = temp;
            }
        }

        /// <summary>
        /// Converts a DateTime into an ISO-8601 formatted string for use in JSON data
        /// </summary>
        /// <param name="dateTime">The DateTime to convert</param>
        /// <returns>The date/time in ISO-8601 format</returns>
        public static string ToJsonDate(DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                return ToJsonDate(dateTime);
            }
            return string.Empty;
        }

        /// <summary>
        /// Converts a DateTime into an ISO-8601 formatted string for use in JSON data
        /// </summary>
        /// <param name="dateTime">The DateTime to convert</param>
        /// <returns>The date/time in ISO-8601 format</returns>
        public static string ToJsonDate(DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToString("o");
        }

        /// <summary>
        /// Converts a DateTimeOffset into an ISO-8601 formatted string for use in JSON data
        /// </summary>
        /// <param name="dateTime">The DateTimeOffset to convert</param>
        /// <returns>The date/time in ISO-8601 format</returns>
        public static string ToJsonDate(DateTimeOffset? dateTimeOffset)
        {
            if (dateTimeOffset.HasValue)
            {
                return ToJsonDate(dateTimeOffset);
            }
            return string.Empty;
        }

        /// <summary>
        /// Converts a DateTimeOffset into an ISO-8601 formatted string for use in JSON data
        /// </summary>
        /// <param name="dateTime">The DateTimeOffset to convert</param>
        /// <returns>The date/time in ISO-8601 format</returns>
        public static string ToJsonDate(DateTimeOffset dateTimeOffset)
        {
            return dateTimeOffset.ToString("o");
        }
    }
}