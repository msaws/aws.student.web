﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Web.Student.Models;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// Static class for processing state and province codes
    /// </summary>
    public static class StateProvinceUtility
    {
        // List of all state/province codes
        private static List<StateProvinceCode> _AllCodes = new List<StateProvinceCode>();

        // Constructor populates the codes on first time used
        static StateProvinceUtility()
        {
            Populate();
        }

        /// <summary>
        /// Return all the codes for a country, or all codes if no country specified
        /// </summary>
        /// <param name="country">Optional country code</param>
        /// <returns>List of state/province code information</returns>
        public static IEnumerable<StateProvinceCode> Get(string country = null)
        {
            if (String.IsNullOrEmpty(country))
            {
                return _AllCodes;
            }

            return _AllCodes.Where(x => x.Country == country);
        }

        /// <summary>
        /// Get the state/province codes ordered by code value
        /// </summary>
        /// <param name="country">Optional country code</param>
        /// <returns>List of state/province codes sorted by code</returns>
        public static IEnumerable<StateProvinceCode> GetByCode(string country = null)
        {
            return Get(country).OrderBy(x => x.Code);
        }

        /// <summary>
        /// Get the state/province codes ordered by code description
        /// </summary>
        /// <param name="country">Optional country code</param>
        /// <returns>List of state/province codes sorted by description</returns>
        public static IEnumerable<StateProvinceCode> GetByName(string country = null)
        {
            return Get(country).OrderBy(x => x.Description.ToUpper());
        }

        // Private method to populate the codes - currently contains only codes for US and Canada
        private static void Populate()
        {
            // US state codes
            _AllCodes.Add(new StateProvinceCode("AL", "Alabama", "US"));
            _AllCodes.Add(new StateProvinceCode("AK", "Alaska", "US"));
            _AllCodes.Add(new StateProvinceCode("AZ", "Arizona", "US"));
            _AllCodes.Add(new StateProvinceCode("AR", "Arkansas", "US"));
            _AllCodes.Add(new StateProvinceCode("CA", "California", "US"));
            _AllCodes.Add(new StateProvinceCode("CO", "Colorado", "US"));
            _AllCodes.Add(new StateProvinceCode("CT", "Connecticut", "US"));
            _AllCodes.Add(new StateProvinceCode("DE", "Delaware", "US"));
            _AllCodes.Add(new StateProvinceCode("DC", "District of Columbia", "US"));
            _AllCodes.Add(new StateProvinceCode("FL", "Florida", "US"));
            _AllCodes.Add(new StateProvinceCode("GA", "Georgia", "US"));
            _AllCodes.Add(new StateProvinceCode("HI", "Hawaii", "US"));
            _AllCodes.Add(new StateProvinceCode("ID", "Idaho", "US"));
            _AllCodes.Add(new StateProvinceCode("IL", "Illinois", "US"));
            _AllCodes.Add(new StateProvinceCode("IN", "Indiana", "US"));
            _AllCodes.Add(new StateProvinceCode("IA", "Iowa", "US"));
            _AllCodes.Add(new StateProvinceCode("KS", "Kansas", "US"));
            _AllCodes.Add(new StateProvinceCode("KY", "Kentucky", "US"));
            _AllCodes.Add(new StateProvinceCode("LA", "Louisiana", "US"));
            _AllCodes.Add(new StateProvinceCode("ME", "Maine", "US"));
            _AllCodes.Add(new StateProvinceCode("MD", "Maryland", "US"));
            _AllCodes.Add(new StateProvinceCode("MA", "Massachusetts", "US"));
            _AllCodes.Add(new StateProvinceCode("MI", "Michigan", "US"));
            _AllCodes.Add(new StateProvinceCode("MN", "Minnesota", "US"));
            _AllCodes.Add(new StateProvinceCode("MS", "Mississippi", "US"));
            _AllCodes.Add(new StateProvinceCode("MO", "Missouri", "US"));
            _AllCodes.Add(new StateProvinceCode("MT", "Montana", "US"));
            _AllCodes.Add(new StateProvinceCode("NE", "Nebraska", "US"));
            _AllCodes.Add(new StateProvinceCode("NV", "Nevada", "US"));
            _AllCodes.Add(new StateProvinceCode("NH", "New Hampshire", "US"));
            _AllCodes.Add(new StateProvinceCode("NJ", "New Jersey", "US"));
            _AllCodes.Add(new StateProvinceCode("NM", "New Mexico", "US"));
            _AllCodes.Add(new StateProvinceCode("NY", "New York", "US"));
            _AllCodes.Add(new StateProvinceCode("NC", "North Carolina", "US"));
            _AllCodes.Add(new StateProvinceCode("ND", "North Dakota", "US"));
            _AllCodes.Add(new StateProvinceCode("OH", "Ohio", "US"));
            _AllCodes.Add(new StateProvinceCode("OK", "Oklahoma", "US"));
            _AllCodes.Add(new StateProvinceCode("OR", "Oregon", "US"));
            _AllCodes.Add(new StateProvinceCode("PA", "Pennsylvania", "US"));
            _AllCodes.Add(new StateProvinceCode("RI", "Rhode Island", "US"));
            _AllCodes.Add(new StateProvinceCode("SC", "South Carolina", "US"));
            _AllCodes.Add(new StateProvinceCode("SD", "South Dakota", "US"));
            _AllCodes.Add(new StateProvinceCode("TN", "Tennessee", "US"));
            _AllCodes.Add(new StateProvinceCode("TX", "Texas", "US"));
            _AllCodes.Add(new StateProvinceCode("UT", "Utah", "US"));
            _AllCodes.Add(new StateProvinceCode("VT", "Vermont", "US"));
            _AllCodes.Add(new StateProvinceCode("VA", "Virginia", "US"));
            _AllCodes.Add(new StateProvinceCode("WA", "Washington", "US"));
            _AllCodes.Add(new StateProvinceCode("WV", "West Virginia", "US"));
            _AllCodes.Add(new StateProvinceCode("WI", "Wisconsin", "US"));
            _AllCodes.Add(new StateProvinceCode("WY", "Wyoming", "US"));

            // Canadian province codes
            _AllCodes.Add(new StateProvinceCode("AB", "Alberta", "CA"));
            _AllCodes.Add(new StateProvinceCode("BC", "British Colombia", "CA"));
            _AllCodes.Add(new StateProvinceCode("MB", "Manitoba", "CA"));
            _AllCodes.Add(new StateProvinceCode("NB", "New Brunswick", "CA"));
            _AllCodes.Add(new StateProvinceCode("NL", "Newfoundland and Labrador", "CA"));
            _AllCodes.Add(new StateProvinceCode("NT", "Northwest Territories", "CA"));
            _AllCodes.Add(new StateProvinceCode("NS", "Nova Scotia", "CA"));
            _AllCodes.Add(new StateProvinceCode("NU", "Nunavut", "CA"));
            _AllCodes.Add(new StateProvinceCode("ON", "Ontario", "CA"));
            _AllCodes.Add(new StateProvinceCode("PE", "Prince Edward Island", "CA"));
            _AllCodes.Add(new StateProvinceCode("QC", "Quebec", "CA"));
            _AllCodes.Add(new StateProvinceCode("SK", "Saskatchewan", "CA"));
            _AllCodes.Add(new StateProvinceCode("YT", "Yukon", "CA"));
        }
    }
}