﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// Provides utility functionality for converting various data types to string-based days of a week, such as converting 
    /// &quot;0&quot; to &quot;Sunday&quot;.
    /// </summary>
    public static class DayOfWeekConverter
    {
        /// <summary>
        /// Converts a collection of DayOfWeek enumerations to a string for display. For example, if the collection
        /// contains Monday, Wednesday, and Friday, the string would return M/W/F.
        /// </summary>
        /// <param name="collection">A collection of DayOfWeek items.</param>
        /// <param name="separator">A separator string for the display value. If not provided, this will be empty.</param>
        /// <returns>A formatted string of days of week for display</returns>
        public static string EnumCollectionToString(IEnumerable<DayOfWeek> collection, string separator = "")
        {
            List<string> dow = new List<string>();
            if (collection.Contains(DayOfWeek.Monday))
            {
                dow.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "MondayAbbreviation"));
            }
            if (collection.Contains(DayOfWeek.Tuesday))
            {
                dow.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "TuesdayAbbreviation"));
            }
            if (collection.Contains(DayOfWeek.Wednesday))
            {
                dow.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "WednesdayAbbreviation"));
            }
            if (collection.Contains(DayOfWeek.Thursday))
            {
                dow.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "ThursdayAbbreviation"));
            }
            if (collection.Contains(DayOfWeek.Friday))
            {
                dow.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "FridayAbbreviation"));
            }
            if (collection.Contains(DayOfWeek.Saturday))
            {
                dow.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "SaturdayAbbreviation"));
            }
            if (collection.Contains(DayOfWeek.Sunday))
            {
                dow.Add(GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "SundayAbbreviation"));
            }

            return string.Join(separator, dow.ToArray());
        }

        public static string IntToDayString(int day)
        {
            string dayText = string.Empty;
            switch (day)
            {
                case 0:
                    dayText = GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Sunday");
                    break;
                case 1:
                    dayText = GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Monday");
                    break;
                case 2:
                    dayText = GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Tuesday");
                    break;
                case 3:
                    dayText = GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Wednesday");
                    break;
                case 4:
                    dayText = GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Thursday");
                    break;
                case 5:
                    dayText = GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Friday");
                    break;
                case 6:
                    dayText = GlobalResources.GetString(GlobalResourceFiles.DaysOfWeekResources, "Saturday");
                    break;
            }
            return dayText;
        }
    }
}