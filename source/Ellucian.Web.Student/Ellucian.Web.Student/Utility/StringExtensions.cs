﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Utility
{
	public static class StringExtensions
	{
        public static string PrependTilde(this String str)
        {
            if (str.StartsWith("~"))
            {
                return str;
            }
            return "~" + str;
        }

        /// <summary>
        /// Normalizes string values. If the input string is null, empty or whitespace, return an empty string.
        /// Otherwise trim whitespace from both ends of the string;
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TidyString(this String str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            return str.Trim();
        }
	}
}