﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// Abstracts retrieval of global resource strings and strongly types the resource values.
    /// </summary>
    public static class GlobalResources
    {
        /// <summary>
        /// Retrieves the requested resource value from global resources
        /// </summary>
        /// <param name="resourceFile">resource file name</param>
        /// <param name="resourceKey">resource key</param>
        /// <returns>resource value or null</returns>
        public static string GetString(string resourceFile, string resourceKey)
        {
            // If performance issues are encountered, then a local ConcurrentDictionary cache may help
            return HttpContext.GetGlobalResourceObject(resourceFile, resourceKey) as string;
        }
    }
}