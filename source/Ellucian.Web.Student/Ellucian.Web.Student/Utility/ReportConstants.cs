﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.

namespace Ellucian.Web.Student.Utility
{
    public class ReportConstants
    {
        public const string ReportType = "PDF";
        public const string DeviceInfo = "<DeviceInfo>" +
                " <OutputFormat>PDF</OutputFormat>" +
                " <PageWidth>8.5in</PageWidth>" +
                " <PageHeight>11in</PageHeight>" +
                " <MarginTop>0.5in</MarginTop>" +
                " <MarginLeft>0.5in</MarginLeft>" +
                " <MarginRight>0.5in</MarginRight>" +
                " <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";
    }
}