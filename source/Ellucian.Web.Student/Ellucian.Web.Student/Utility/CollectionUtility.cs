﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// CollectionUtility provides utility functions for collections.
    /// </summary>
    public static class CollectionUtility
    {
        /// <summary>
        /// This method determines if the enumerable object is null or empty.
        /// </summary>
        /// <typeparam name="T">generic type</typeparam>
        /// <param name="enumerableCollection">collection</param>
        /// <returns>true/false</returns>
        public static bool IsNullOrEmpty<T>(IEnumerable<T> enumerableCollection)
        {
            return (enumerableCollection == null || (enumerableCollection != null && enumerableCollection.Count() == 0));
        }

        /// <summary>
        /// This method is used to take data stored in a collection and transform it into XML.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static DataSet ToDataSet(Object[] values)
        {
            DataSet ds = new DataSet();
            Type temp = values.GetType();
            XmlSerializer xmlSerializer = new XmlSerializer(values.GetType());
            StringWriter writer = new StringWriter();

            xmlSerializer.Serialize(writer, values);
            StringReader reader = new StringReader(writer.ToString());
            ds.ReadXml(reader);
            return ds;
        }
    }
}