﻿// Copyright 2016 - 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Text;
using System.Web.Security;

namespace Ellucian.Web.Student.Utility
{
    public static class EncryptionUtility
    {
        public static string Encrypt(string plainText)
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(plainText);
            try
            {
                return MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
            }
            catch
            {
                return plainText;
            }
        }

        public static string Decrypt(string encrypted)
        {
            try
            {
                var decryptedBytes = MachineKey.Decode(encrypted, MachineKeyProtection.All);
                return Encoding.UTF8.GetString(decryptedBytes);
            }
            catch
            {
                return encrypted;
            }
        }

        /// <summary>
        /// Encrypts a string using the MachineKey API
        /// then converts to base 64 encoding
        /// </summary>
        /// <param name="text"></param>
        /// <returns>protected string</returns>
        public static string Protect(string text)
        {
            var protectedBytes = MachineKey.Protect(Encoding.UTF8.GetBytes(text));
            return Convert.ToBase64String(protectedBytes);
        }

        /// <summary>
        /// Decrypts a protected string using the MachineKeyAPI
        /// then converts from base 64 encoding
        /// </summary>
        /// <param name="text"></param>
        /// <returns>unprotected string</returns>
        public static string Unprotect(string base64EncodedProtected)
        {
            var protectedBytes = Convert.FromBase64String(base64EncodedProtected);
            var unprotectedBytes = MachineKey.Unprotect(protectedBytes);                
            return Encoding.UTF8.GetString(unprotectedBytes);
        }
    }    
}