﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using System;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// Defines the possible name display formats.
    /// </summary>
    public enum NameFormats
    {
        /// <summary>
        /// Represents the Last, First MiddleInitial format.
        /// </summary>
        LastFirstMiddleInitial,
        /// <summary>
        /// Represents the First MiddleInitial Last format.
        /// </summary>
        FirstMiddleInitialLast,
        ///<summary>
        ///Represents the full first, Full Middle and Full Last Names.
        /// </summary>
        FirstMiddleLast
    }

    public struct DisplayNameResult
    {
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
    }

    /// <summary>
    /// Helper functions for formatting names for proper display in the UI.
    /// </summary>
    public static class NameHelper
    {
        /// <summary>
        /// Creates a formatted faculty name value for display based on a Faculty object.
        /// </summary>
        /// <param name="faculty">The Faculty object for which to create a properly-formatted display name</param>
        /// <exception cref="ArgumentNullException"><paramref name="faculty"/> is null.</exception>
        /// <returns>A string representing the formatted display name</returns>
        public static string FacultyDisplayName(Faculty faculty)
        {
            if (faculty == null)
            {
                throw new ArgumentNullException("faculty", "Unable to format display name for null faculty");
            }
            string name = faculty.ProfessionalName;
            if (string.IsNullOrEmpty(name)) {
                name = faculty.LastName;
                if (!string.IsNullOrEmpty(faculty.FirstName))
                {
                    name += ", " + faculty.FirstName[0];
                }
            }
            return name;
        }

        /// <summary>
        /// Creates a formatted name value based on a generic Person object. The format can be specified via use of the NameFormats enumeration. This
        /// method can also be used for classes that extend Person, such as Advisee and Student.
        /// </summary>
        /// <param name="person">The Person object for which to create the properly-formatted display name</param>
        /// <param name="format">The format to use when creating the display name</param>
        /// <param name="separator">An optional parameter specifying the separator character to use between the last and first names. If not specified then
        /// the default value of a comma will be used. This is only used in the LastFirstMiddleInitial format.</param>
        /// <returns>A string representing the formatted display name in the specified format</returns>
        public static string PersonDisplayName(Person person, NameFormats format = NameFormats.LastFirstMiddleInitial, string separator = ", ")
        {
            string name = string.Empty;
            if (person != null)
            {
                name = PersonDisplayName(person.LastName, person.FirstName, person.MiddleName, format, separator);
            }
            return name;
        }

        /// <summary>
        /// Creates a formatted name value based on a generic Person object. The format can be specified via use of the NameFormats enumeration. This
        /// method can also be used for classes that extend Person, such as Advisee and Student.
        /// </summary>
        /// <param name="lastName">Last name to use in the construction of the name</param>
        /// <param name="firstName">First name to use in the construction of the name</param>
        /// <param name="middleName">Middle name to use in the construction of the name</param>
        /// <param name="format">The format to use when creating the display name</param>
        /// <param name="separator">An optional parameter specifying the separator character to use between the last and first names. If not specified then
        /// the default value of a comma will be used. This is only used in the LastFirstMiddleInitial format.</param>
        /// <returns>A string representing the formatted display name in the specified format (May be empty)</returns>
        public static string PersonDisplayName(string lastName, string firstName, string middleName, NameFormats format = NameFormats.LastFirstMiddleInitial, string separator = ", ")
        {
            string name = string.Empty;
            if (lastName != null)
            {
                switch (format)
                {
                    case NameFormats.LastFirstMiddleInitial:
                        if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(middleName))
                        {
                            name = lastName + separator + firstName + " " + middleName.Substring(0, 1) + ".";
                        }
                        else if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                        {
                            name = lastName + separator + firstName;
                        }
                        else if (!string.IsNullOrEmpty(lastName))
                        {
                            name = lastName;
                        }
                        break;

                    case NameFormats.FirstMiddleInitialLast:
                        if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(middleName))
                        {
                            name = firstName + " " + middleName.Substring(0, 1) + ". " + lastName;
                        }
                        else if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                        {
                            name = firstName + " " + lastName;
                        }
                        else if (!string.IsNullOrEmpty(lastName))
                        {
                            name = lastName;
                        }
                        break;
                    case NameFormats.FirstMiddleLast:
                        if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(middleName))
                        {
                            name = firstName + " " + middleName + " " + lastName;
                        }
                        else if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                        {
                            name = firstName + " " + lastName;
                        }
                        else if (!string.IsNullOrEmpty(lastName))
                        {
                            name = lastName;
                        }
                        break;
                }
            }
            return name;
        }

        public static DisplayNameResult GetDisplayNameResult(Person person, PersonHierarchyName hierarchyName, NameFormats format = NameFormats.LastFirstMiddleInitial, string separator = ", ")
        {
            if (person != null)
            {
                return GetDisplayNameResult(person.LastName, person.FirstName, person.MiddleName, hierarchyName, format, separator);
            }
            else
            {
                return new DisplayNameResult() {
                    FullName = string.Empty,
                    LastName = string.Empty,
                    FirstName = string.Empty,
                    MiddleName = string.Empty
                } ;
            }

        }

        public static DisplayNameResult GetDisplayNameResult(string lastName, string firstName, string middleName, PersonHierarchyName hierarchyName, NameFormats format = NameFormats.LastFirstMiddleInitial, string separator = ", ")
        {
            if (hierarchyName != null && !string.IsNullOrEmpty(hierarchyName.FullName))
            {
                return new DisplayNameResult()
                {
                    FullName = hierarchyName.FullName,
                    LastName = !string.IsNullOrEmpty(hierarchyName.LastName) ? hierarchyName.LastName : hierarchyName.FullName,
                    FirstName = hierarchyName.FirstName,
                    MiddleName = hierarchyName.MiddleName
                };
            }
            return new DisplayNameResult()
            {
                FullName = PersonDisplayName(lastName, firstName, middleName, format, separator),
                LastName = lastName,
                FirstName = firstName,
                MiddleName = middleName
            };
        }
    }
}