﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;

namespace Ellucian.Web.Student.Utility
{
    public class ConstantDictionary : ConcurrentDictionary<string, string>
    {
        private const int _RetryLimit = 3;

        public ConstantDictionary()
        {
            HasBeenSet = false;
        }

        public bool HasBeenSet { get; set; }

        public void AddConstant(string key, string value)
        {
            int retryCount = 0;
            bool success = false;

            do
            {
                success = TryAdd(key, value);
            }
            while (!success && retryCount++ < _RetryLimit);
        }
    }
}