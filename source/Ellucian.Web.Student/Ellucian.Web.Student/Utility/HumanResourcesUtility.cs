﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Utility
{
    public class HumanResourcesUtility
    {
        /// <summary>
        /// Create a new tax form consent DTO
        /// </summary>
        /// <param name="personId">Person ID of the user logged in</param>
        /// <param name="timeStamp">Date and Time for the new consent</param>
        /// <param name="taxForm">The type of tax form for the new consent</param>
        /// <param name="hasConsented">Whether the user consented or not to have the tax form online</param>
        /// <returns>Tax Form Consent DTO</returns>
        public static TaxFormConsent BuildTaxFormConsentDto(string personId, DateTimeOffset timeStamp, string taxForm, bool hasConsented)
        {
            var taxFormType = TaxForms.Form1095C;
            switch (taxForm)
            {
                case "FormW2":
                    taxFormType = TaxForms.FormW2;
                    break;
                case "Form1095C":
                    taxFormType = TaxForms.Form1095C;
                    break;
                case "Form1098":
                    taxFormType = TaxForms.Form1098;
                    break;
                default:
                    throw new ApplicationException("Invalid tax form: " + taxForm);
            }

            return new TaxFormConsent()
            {
                PersonId = personId,
                TaxForm = taxFormType,
                TimeStamp = timeStamp,
                HasConsented = hasConsented,
            };
        }
    }
}