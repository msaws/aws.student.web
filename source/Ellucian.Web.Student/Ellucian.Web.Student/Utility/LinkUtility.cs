﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ellucian.Web.Student.Utility
{
    public class LinkUtility
    {
        /// <summary>
        /// Compares the controller, action, and area of the two <see cref="RouteValueDictionary"/> given.
        /// Both must have at least controller and action. Area is optional.
        /// </summary>
        /// <param name="rvd1">The first RouteValueDictionary</param>
        /// <param name="rvd2">The second RouteValueDictionary</param>
        /// <returns>True if the controller, action, and area match. False otherwise</returns>
        public static bool RouteValuesMatch(RouteValueDictionary rvd1, RouteValueDictionary rvd2)
        {
            if (rvd1 == null)
            {
                throw new ArgumentNullException("rvd1", "rvd1 must not be null.");
            }

            if (rvd2 == null)
            {
                throw new ArgumentNullException("rvd2", "rvd2 must not be null.");
            }

            // All routes must have at least a controller and action
            if (!rvd1.ContainsKey("controller") || !rvd1.ContainsKey("action") ||
                !rvd2.ContainsKey("controller") || !rvd2.ContainsKey("action"))
            {
                return false;
            }

            // Controllers and actions must match
            if (!rvd1["controller"].Equals(rvd2["controller"]) ||
                !rvd1["action"].Equals(rvd2["action"]))
            {
                return false;
            }

            // If both contain an area, the other must match
            if (rvd1.ContainsKey("area") && rvd2.ContainsKey("area") &&
                !rvd1["area"].Equals(rvd2["area"]))
            {
                return false;
            }
            // Treat null/empty areas the same as not containing the key
            else if (rvd1.ContainsKey("area") && !string.IsNullOrEmpty(rvd1["area"].ToString()) &&
                    (!rvd2.ContainsKey("area") || string.IsNullOrEmpty(rvd2["area"].ToString())))
            {
                return false;
            }
            else if (rvd2.ContainsKey("area") && !string.IsNullOrEmpty(rvd2["area"].ToString()) &&
                    (!rvd1.ContainsKey("area") || string.IsNullOrEmpty(rvd1["area"].ToString())))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Compares the area between that of a RouteValueDictionary and a Page in the sitemap
        /// </summary>
        /// <param name="rvd">The RouteValueDictionary to compare</param>
        /// <param name="page">The page to compare</param>
        /// <returns>True if the areas match or neither have an area. False otherwise.</returns>
        public static bool RouteValueDictionaryAreaMatchesPageArea(RouteValueDictionary rvd, Ellucian.Web.Mvc.Models.Page page)
        {
            if (rvd == null)
            {
                throw new ArgumentNullException("rvd", "Route value dictionary cannot be null.");
            }

            if (page == null)
            {
                throw new ArgumentNullException("page", "Page cannot be null.");
            }

            if (!string.IsNullOrEmpty(page.Area) &&
                rvd.ContainsKey("area") &&
                page.Area.Equals(rvd["area"]))
            {
                return true;
            }
            else if (string.IsNullOrEmpty(page.Area) &&
                (!rvd.ContainsKey("area") || string.IsNullOrEmpty(rvd["area"].ToString())))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Attempts to retrieve the label of a Page in the sitemap that matches the given RouteData.
        /// </summary>
        /// <param name="rvd">The RouteValueDictionary to match a Page with.</param>
        /// <param name="currentUser">The current user, used to trim the sitemap.</param>
        /// <param name="defaultLabel">The default label used if no page was found. If null or not passed, will use the resource file default.</param>
        /// <returns>The label of the Page if a match was found, defaultLabel otherwise.</returns>
        public static string GetPageLabelFromRouteValueDictionary(RouteValueDictionary rvd, ICurrentUser currentUser, string defaultLabel = null)
        {
            if (rvd != null &&
                rvd.ContainsKey("controller") &&
                rvd.ContainsKey("action"))
            {
                var siteService = DependencyResolver.Current.GetService<ISiteService>();
                if (siteService != null)
                {
                    Site site = siteService.Get(currentUser);

                    if (site != null)
                    {
                        foreach (var page in site.Pages)
                        {
                            if (page.Controller.Equals(rvd["controller"]) &&
                                page.Action.Equals(rvd["action"]) &&
                                RouteValueDictionaryAreaMatchesPageArea(rvd, page))
                            {
                                return page.Label;
                            }
                        }
                    }

                }
            }

            if (defaultLabel == null)
            {
                defaultLabel = GlobalResources.GetString("Navigation", "PreviousPage");
            }
            return defaultLabel;
        }

        /// <summary>
        /// Retrieves the RouteData that would be associated with a given Uri.
        /// </summary>
        /// <param name="uri">The Uri to extract the RouteData from.</param>
        /// <returns>The RouteData represented by the Uri.</returns>
        public static RouteData GetRouteDataFromUri(Uri uri)
        {
            // We have to create a request and response to get an httpcontext that lets
            // us retrieve the routedata. No actual request is made.
            var request = new HttpRequest(null, uri.AbsoluteUri, uri.Query);
            var response = new HttpResponse(new StringWriter());
            var httpContext = new HttpContext(request, response);
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));
            if (routeData != null &&
                routeData.Values.ContainsKey("controller") &&
                !string.IsNullOrEmpty(routeData.Values["controller"].ToString()))
            {
                if (routeData.DataTokens.ContainsKey("area"))
                {
                    routeData.Values.Add("area", routeData.DataTokens["area"].ToString());
                }
            }

            return routeData;
        }

        /// <summary>
        /// Determines whether or not the given Uri represents a Uri within the application.
        /// </summary>
        /// <param name="uri">The Uri to test.</param>
        /// <returns>True if the Uri is within the application.</returns>
        public static bool UriIsInApplication(Uri uri)
        {
            if (uri != null &&
                !string.IsNullOrEmpty(uri.LocalPath) &&
                !string.IsNullOrEmpty(uri.AbsoluteUri) &&
                uri.Host == HttpContext.Current.Request.Url.Host &&
                uri.LocalPath.StartsWith(HttpContext.Current.Request.ApplicationPath))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether or not the given RouteData is a valid target for a dynamic back link
        /// </summary>
        /// <param name="routeData">The RouteData to test.</param>
        /// <param name="dynamicBackLink">The DynamicBackLink configuration.</param>
        /// <returns>True if the RouteData is a valid target for a back link.</returns>
        public static bool RouteDataIsValidForBacklink(RouteData routeData, DynamicBackLinkConfiguration dynamicBackLink)
        {
            if (routeData == null || !routeData.Values.ContainsKey("controller") || !routeData.Values.ContainsKey("action"))
            {
                return false;
            }

            // Strip blacklisted controllers
            if (dynamicBackLink != null && dynamicBackLink.DisallowedControllers.Any())
            {
                if (dynamicBackLink.DisallowedControllers.Contains(routeData.Values["controller"]))
                {
                    return false;
                }
            }

            // Strip blacklisted actions
            if (dynamicBackLink != null && dynamicBackLink.DisallowedActions.Any())
            {
                if (dynamicBackLink.DisallowedActions.Where(da => RouteValuesMatch(da, routeData.Values)).Any())
                {
                    return false;
                }
            }
            return true;
        }

        private LinkUtility() { }
    }
}