﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// Provides utility functionality for formatting credits display values.
    /// </summary>
    public static class CreditsFormatter
    {
        /// <summary>
        /// Converts the given credits decimal amount into a properly-formatted string for display. If the value is
        /// a whole number it will display without decimal places, such as "2"; otherwise the value will be truncated
        /// using the format defined in DegreePlanResources via CreditsFormattingString.
        /// </summary>
        /// <param name="credits">The decimal amount of credits</param>
        /// <returns>A display-formatted version of this credit value</returns>
        public static string FormattedCreditsString(decimal credits)
        {
            return (credits % 1 == 0) ? credits.ToString("0") : credits.ToString(GlobalResources.GetString(GlobalResourceFiles.StudentResources, "CreditsFormattingString"));
        }

        /// <summary>
        /// Converts the given GPA decimal amount into a properly-formatted string for display. This will display
        /// using the format defined in DegreePlanResources via GPAFormattingString.
        /// </summary>
        /// <param name="GPA">The decimal GPA value</param>
        /// <returns>A display-formatted version of this GPA value</returns>
        public static string FormattedGpaString(decimal gpa)
        {
            return gpa.ToString(GlobalResources.GetString(GlobalResourceFiles.StudentResources, "GpaFormattingString"));
        }

        /// <summary>
        /// The utility will take an academic credit credits and CEUs and a CEU text string and determine the best way to display
        /// the resulting information.  An academic credit can only either have CEUs or Credits.
        /// The decimal will be converted into a properly-formatted string for display showing only significant digits. If the value is
        /// a whole number it will display without decimal places, such as "2"; otherwise the value will be truncated
        /// using the format defined in DegreePlanResources via CreditsFormattingString.
        /// </summary>
        /// <param name="academicCreditCredits">The decimal amount of credits for the academic credit</param>
        /// <param name="academicCreditCeus">The decimal amount of continuing education units for the Academic Credit</param>
        /// <returns>A display-formatted version of this credit or CEUs</returns>
        public static string FormattedCreditsCeusString(decimal academicCreditCredits, decimal academicCreditCeus)
        {
            string formattedCreditCeuDisplay = string.Empty;
            // At this time the student can only either credits or ceus in academic credit.  A section can be either but the student must choose when they register which they are taking.
            // Check first to see if the section has any Ceus. If so show that.
            if (academicCreditCeus > 0) formattedCreditCeuDisplay = string.Format(GlobalResources.GetString(GlobalResourceFiles.StudentResources, "UiCourseCeus"), CreditsFormatter.FormattedCreditsString(academicCreditCeus));
            // If no Ceus then format the credit display - allowing for the fact that a section can have zero credit.
            if (string.IsNullOrEmpty(formattedCreditCeuDisplay) && academicCreditCredits >= 0) formattedCreditCeuDisplay = CreditsFormatter.FormattedCreditsString(academicCreditCredits);
            return formattedCreditCeuDisplay;
        }

        /// <summary>
        /// Converts the given test score decimal result into a properly-formatted string for display. If the value is
        /// a whole number it will display without decimal places, such as "2"; otherwise the value will be truncated
        /// using the format defined in DegreePlanResources via CreditsFormattingString.
        /// </summary>
        /// <param name="credits">The decimal amount of test score</param>
        /// <returns>A display-formatted version of this score value</returns>
        public static string FormattedTestScoreString(decimal? testScore)
        {
            return testScore.HasValue ? (testScore % 1 == 0) ? testScore.Value.ToString("0") : testScore.Value.ToString(GlobalResources.GetString(GlobalResourceFiles.StudentResources, "TestScoreFormattingString")) : null;
        }
    }
}