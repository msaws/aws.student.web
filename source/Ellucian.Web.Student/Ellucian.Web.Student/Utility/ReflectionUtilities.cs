﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace Ellucian.Web.Student.Utility
{
    public static class ReflectionUtilities
    {
        public static void SetProperty(object instance, string propertyName, string propertyValue)
        {
            if (instance != null && !string.IsNullOrEmpty(propertyName) && !string.IsNullOrEmpty(propertyValue))
            {
                // check for nesting
                string[] dots = propertyName.Split('.');
                if (dots != null && dots.Length > 1)
                {
                    string a = string.Join(".", dots, 1, (dots.Length - 1));
                    object b = instance.GetType().GetProperty(dots[0]).GetValue(instance, null);

                    if (b != null)
                    {
                        SetProperty(b, a, propertyValue);
                    }
                }
                else
                {
                    ApplyValue(instance, propertyName, propertyValue);
                }
            }
        }

        public static void MapProperty(object source, object target, string propertyName)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }
            var sourceType = source.GetType();
            var sourceProp = sourceType.GetProperty(propertyName);
            var targetType = target.GetType();
            var targetProp = targetType.GetProperty(propertyName);

            if (sourceProp == null)
            {
                throw new ApplicationException("Source object does not have a property called " + propertyName);
            }
            var propGetter = sourceProp.GetGetMethod();
            var propSetter = targetProp.GetSetMethod();
            var valueToSet = propGetter.Invoke(source, null);
            propSetter.Invoke(target, new[] { valueToSet });
        }

        private static void ApplyValue(object instance, string propertyName, string propertyValue)
        {
            PropertyInfo pi = instance.GetType().GetProperty(propertyName);
            if (pi != null)
            {
                try
                {
                    if (pi.PropertyType.BaseType == typeof(System.Array))
                    {
                        string[] a = propertyValue.Split(',');
                        Type t = pi.PropertyType.GetElementType();
                        if (t == typeof(string))
                        {
                            pi.SetValue(instance, a, null);
                        }
                        else if (t == typeof(int))
                        {
                            int[] b = new int[a.Length];
                            for (int i = 0; i < a.Length; i++)
                            {
                                b[i] = (int)Convert.ChangeType(a[i], t);
                            }
                            pi.SetValue(instance, b, null);
                        }
                    }
                    else
                    {
                        pi.SetValue(instance, Convert.ChangeType(propertyValue, pi.PropertyType), null);
                    }
                }
                catch
                {

                }
            }
        }
    }
}