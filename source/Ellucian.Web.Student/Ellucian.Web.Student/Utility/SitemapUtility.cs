﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Utility
{
    public static class SitemapUtility
    {
        /// <summary>
        /// Sorts the sitemap
        /// </summary>
        /// <param name="site">The Site model to sort</param>
        /// <returns>A sorted Site model</returns>
        public static Site SortSite(Site site)
        {
            Site sortedSite = new Site();
            var menus = site.Menus.OrderBy(x => x.Order);
            var submenus = site.Submenus.OrderBy(x => x.Order);
            var pages = site.Pages.OrderBy(x => x.Order);
            var links = site.Links.OrderBy(x => x.Order);
            foreach (var menu in menus)
            {
                sortedSite.Menus.Add(SortItems(menu));
            }
            foreach (var submenu in submenus)
            {
                sortedSite.Submenus.Add(SortItems(submenu));
            }
            sortedSite.Pages = pages.ToList();
            sortedSite.Links = links.ToList();

            return sortedSite;
        }

        /// <summary>
        /// Recursively sorts the items that are contained by a menu or submenu (which are both treated as MenuItemMenus)
        /// </summary>
        /// <param name="menu">The Menu model that will be recursively sorted</param>
        /// <returns>A sorted Menu model</returns>
        public static Menu SortItems(Menu menu)
        {
            foreach (var item in menu.Items)
            {
                if (item.ItemType == Mvc.Domain.Entities.MenuItemType.Menu)
                {
                    SortItems(((MenuItemMenu)item).Menu);
                }
            }
            menu.Items = menu.Items.OrderBy(x => x.ItemType == Mvc.Domain.Entities.MenuItemType.Menu ? ((MenuItemMenu)x).Menu.Order : x.ItemType == Mvc.Domain.Entities.MenuItemType.Page ? ((MenuItemPage)x).Page.Order : ((MenuItemLink)x).Link.Order);
            return menu;
        }

        /// <summary>
        /// Returns true when a menu contains visible items. An item is visible if it has at least one page or submenu with visible items.
        /// </summary>
        /// <param name="menu">The menu to check for visible items</param>
        /// <returns>True if the menu contains visible items</returns>
        public static bool MenuContainsVisibleItems(Menu menu)
        {
            foreach (var item in menu.Items)
            {
                switch (item.ItemType)
                {
                    case Ellucian.Web.Mvc.Domain.Entities.MenuItemType.Menu:
                        var m = (MenuItemMenu)item;
                        if (!m.Menu.Hidden && MenuContainsVisibleItems(m.Menu))
                        {
                            // Found a visible menu
                            return true;
                        }
                        break;
                    case Ellucian.Web.Mvc.Domain.Entities.MenuItemType.Page:
                        var p = (MenuItemPage)item;
                        if (!p.Page.Hidden)
                        {
                            // Found a visible page
                            return true;
                        }
                        break;
                    case Ellucian.Web.Mvc.Domain.Entities.MenuItemType.Link:
                        // Links can't be hidden, just return true
                        return true;
                }
            }
            // There were no visible items in the menu
            return false;
        }
    }
}