﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Web.Student.Licensing;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Mvc.Models;

namespace Ellucian.Web.Student.Utility
{
    /// <summary>
    /// Utility class that includes methods to determine if user has access to 
    /// particular modules/pages
    /// </summary>
    public static class SiteAccessUtility
    {
        /// <summary>
        /// Determines whether the specified module is licensed/accessible for the current user
        /// </summary>
        /// <param name="moduleConstant">one of the module constants <see cref="ModuleConstants"/></param>
        /// <returns>boolean indicating whether the specified module is licensed or not</returns>
        public static bool IsModuleAccessible(string moduleConstant)
        {
            if (string.IsNullOrEmpty(moduleConstant))
            {
                throw new ArgumentNullException("moduleConstant cannot be null or empty");
            }
            if (ApplicationMetadata.LicensedAreas.Contains(moduleConstant))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether the specified page is accessible for the current user
        /// </summary>
        /// <param name="site">site object to search</param>
        /// <param name="moduleConstant">one of the module constants <see cref="ModuleConstants"/></param>
        /// <param name="pageId">page id to check accessibility for</param>
        /// <returns>boolean indicating whether the specified page is accessible or not</returns>
        public static bool IsPageAccessible(Site site, string moduleConstant, string pageId)
        {
            if (site == null)
            {
                throw new ArgumentNullException("site cannot be null");
            }
            if (string.IsNullOrEmpty(moduleConstant))
            {
                throw new ArgumentNullException("moduleConstant cannot be null or empty");
            }
            if (string.IsNullOrEmpty(pageId))
            {
                throw new ArgumentNullException("pageId cannot be null or empty");
            }
            if (IsModuleAccessible(moduleConstant))
            {
                return site.Pages.Any(p => p.Id.Equals(pageId) && !p.Hidden);
            }
            return false;
        }

        /// <summary>
        /// Determines whether the current user has access rights to the information protected by the specified page ids
        /// </summary>
        /// <param name="site">Site object to search</param>
        /// <param name="moduleConstant">One of the module constants <see cref="ModuleConstants"/></param>
        /// <param name="pageIds">Page Ids that protect the information of interest</param>
        /// <returns>Boolean indicating whether the current user has access to the information of interest</returns>
        public static bool IsInformationAccessible(Site site, string moduleConstant, List<string> pageIds)
        {
            if (site == null)
            {
                throw new ArgumentNullException("site cannot be null");
            }
            if (string.IsNullOrEmpty(moduleConstant))
            {
                throw new ArgumentNullException("moduleConstant cannot be null or empty");
            }
            if (pageIds == null || !pageIds.Any())
            {
                throw new ArgumentNullException("pageIds cannot be null or empty");
            }
            if (IsModuleAccessible(moduleConstant))
            {
                return site.Pages.Any(p => pageIds.Contains(p.Id) && !p.Hidden);
            }
            return false;
        }
    }
}