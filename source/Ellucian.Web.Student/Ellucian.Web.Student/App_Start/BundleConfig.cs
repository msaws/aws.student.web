﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System.Web;
using System.Web.Optimization;

namespace Ellucian.Web.Student
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // NOTE: Be careful when defining bundle include paths that use wildcards.
            // This is especially true for versioned files, like jQuery. If you're
            // not careful you will end up with multiple versions of the file served up.

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.8.3*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.8.23*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/search").Include(
                        "~/Scripts/Search/search.view.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/polyfills").Include(
                        "~/Scripts/Polyfills/es6-promise.js",
                        "~/Scripts/Polyfills/array.find.polyfill.js",
                        "~/Scripts/Polyfills/console.polyfill.js",
                        "~/Scripts/Polyfills/object.assign.polyfill.js",
                        "~/Scripts/storage.js"));

            bundles.Add(new ScriptBundle("~/bundles/respondjs").Include(
                        "~/Scripts/respond.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/globalscripts").Include(
                        "~/Scripts/jquery.watermark.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/knockout-3.4.0.js",
                        "~/Scripts/knockout.mapping.js",
                        "~/Scripts/knockout-postbox.js",
                        "~/Scripts/knockout.bindings.js",
                        "~/Scripts/knockout.extenders.js",
                        "~/Scripts/json2.js",
                        "~/Scripts/account.js",
                        "~/Scripts/globalize.js",
                        "~/Scripts/global.js",
                        "~/Scripts/jquery.ui.plugin_responsive-table.js",
                        "~/Scripts/jquery.ui.plugin_notification-center.js",
                        "~/Scripts/navigation.js",
                        "~/Scripts/site.js",
                        "~/Scripts/tooltip.js",
                        "~/Scripts/storage.js"));

            bundles.Add(new ScriptBundle("~/bundles/proxyselectionscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Scripts/PersonProxy/proxy.selection.js"));

            bundles.Add(new ScriptBundle("~/bundles/proxyscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Scripts/knockout.validation.debug.js",
                        "~/Scripts/PersonProxy/person.proxy.mapping.js",
                        "~/Scripts/PersonProxy/person.proxy.view.model.js",
                        "~/Scripts/PersonProxy/person.proxy.js"));

            bundles.Add(new ScriptBundle("~/bundles/planningscripts").Include(
                        "~/Scripts/moment.js",
                        "~/Areas/Planning/Scripts/fullcalendar.js",
                        "~/Areas/Planning/Scripts/planning.knockout.bindings.js",
                        "~/Scripts/Courses/planning.global.js",
                        "~/Scripts/tooltip.js",
                        "~/Scripts/jquery.outerhtml.js",
                        "~/Scripts/jquery.ui.multiprogressbar.js",
                        "~/Scripts/activate-accordion.js"));

            bundles.Add(new ScriptBundle("~/bundles/colleaguefinancescripts").Include(
                        "~/Scripts/tooltip.js",
                        "~/Scripts/jquery.outerhtml.js",
                        "~/Scripts/jquery.ui.multiprogressbar.js"));

            bundles.Add(new ScriptBundle("~/bundles/colleaguefinancecostcenterscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/base.cost.center.model.js",
                        "~/Areas/ColleagueFinance/Scripts/base.cost.center.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/colleague.finance.knockout.bindings.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.detail.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.activity.detail.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.subtotal.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.gl.account.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.mapping.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.account.sequence.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.detail.home.js",
                        "~/Areas/ColleagueFinance/Scripts/saved.filter.model.js",
                        "~/Areas/ColleagueFinance/Scripts/general.ledger.component.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.budget.pool.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.transaction.view.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/colleaguefinancecostcentersscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.summary.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.activity.detail.model.js",
                        "~/Areas/ColleagueFinance/Scripts/base.cost.center.model.js",
                        "~/Areas/ColleagueFinance/Scripts/base.cost.center.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.subtotal.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.mapping.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.account.sequence.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.centers.home.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.budget.pool.model.js",
                        "~/Areas/ColleagueFinance/Scripts/colleague.finance.knockout.bindings.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.transaction.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/saved.filter.model.js",
                        "~/Areas/ColleagueFinance/Scripts/general.ledger.component.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/colleaguefinanceglactivitydetailscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.activity.detail.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.activity.detail.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.subtotal.model.js",
                        "~/Areas/ColleagueFinance/Scripts/cost.center.gl.account.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.activity.detail.mapping.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.budget.pool.model.js",
                        "~/Areas/ColleagueFinance/Scripts/colleague.finance.knockout.bindings.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.transaction.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.activity.detail.js"));

            bundles.Add(new ScriptBundle("~/bundles/cfdocumentscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/document.view.model.js",
                        "~/Areas/ColleagueFinance/Scripts/document.model.js",
                        "~/Areas/ColleagueFinance/Scripts/document.line.item.model.js",
                        "~/Areas/ColleagueFinance/Scripts/document.line.item.gl.distribution.model.js",
                        "~/Areas/ColleagueFinance/Scripts/document.approver.model.js",
                        "~/Areas/ColleagueFinance/Scripts/document.mapping.js",
                        "~/Areas/ColleagueFinance/Scripts/colleague.finance.knockout.bindings.js",
                        "~/Areas/ColleagueFinance/Scripts/parse.document.url.js"));

            bundles.Add(new ScriptBundle("~/bundles/cfblanketpurchaseorderscripts").Include(
                        "~/Areas/ColleagueFinance/Scripts/blanket.purchase.order.load.js"));

            bundles.Add(new ScriptBundle("~/bundles/cfjournalentryscripts").Include(
                        "~/Areas/ColleagueFinance/Scripts/journal.entry.load.js"));

            bundles.Add(new ScriptBundle("~/bundles/cfpurchaseorderscripts").Include(
                        "~/Areas/ColleagueFinance/Scripts/purchase.order.load.js"));

            bundles.Add(new ScriptBundle("~/bundles/cfrecurringvoucherscripts").Include(
                        "~/Areas/ColleagueFinance/Scripts/recurring.voucher.load.js"));

            bundles.Add(new ScriptBundle("~/bundles/cfrequisitionscripts").Include(
                        "~/Areas/ColleagueFinance/Scripts/requisition.load.js"));

            bundles.Add(new ScriptBundle("~/bundles/cfvoucherscripts").Include(
                        "~/Areas/ColleagueFinance/Scripts/voucher.load.js"));

            bundles.Add(new ScriptBundle("~/bundles/projectsaccountingscripts").Include(
                        "~/Areas/ProjectsAccounting/Scripts/project.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/project.line.item.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/project.mapping.js"));

            bundles.Add(new ScriptBundle("~/bundles/paprojectsscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/project.summary.view.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/project.status.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/project.type.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/projects.accounting.home.js",
                        "~/Areas/ColleagueFinance/Scripts/colleague.finance.knockout.bindings.js"));

            bundles.Add(new ScriptBundle("~/bundles/paprojectlineitemscripts").Include(
                        "~/Areas/ColleagueFinance/Scripts/colleague.finance.knockout.bindings.js",
                        "~/Areas/ProjectsAccounting/Scripts/project.detail.view.model.js",
                        "~/Scripts/base.view.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/budget.period.model.js",
                        "~/Areas/ColleagueFinance/Scripts/gl.transaction.view.model.js",
                        "~/Areas/ProjectsAccounting/Scripts/project.detail.js"));

            bundles.Add(new ScriptBundle("~/bundles/financialaidscripts").Include(
                        "~/Scripts/moment.js",
                        "~/Areas/FinancialAid/Scripts/financial.aid.knockout.bindings.js",
                        "~/Scripts/base.view.model.js",
                        "~/Scripts/knockout.validation.js",
                        "~/Areas/FinancialAid/Scripts/Shared/year.selector.view.model.js",
                        "~/Areas/FinancialAid/Scripts/Shared/shared.data.view.model.js",
                        "~/Areas/FinancialAid/Scripts/financial.aid.utility.js"));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminscripts").Include(
                        "~/Areas/FinancialAid/Scripts/Shared/admin.search.view.model.js"                        
                        ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidhomescripts").Include(
                        "~/Areas/FinancialAid/Scripts/OutsideAwards/outside.awards.view.model.js",
                        "~/Areas/FinancialAid/Scripts/Home/home.mapping.js",
                        "~/Areas/FinancialAid/Scripts/Home/home.view.model.js",
                        "~/Areas/FinancialAid/Scripts/Home/home.js",                        
                        "~/Scripts/Chart.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminhomescripts").Include(
                        "~/Areas/FinancialAid/Scripts/Home/home.admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidmyawardsscripts").Include(
                        "~/Areas/FinancialAid/Scripts/Awards/awards.mapping.js",
                        "~/Areas/FinancialAid/Scripts/Awards/awards.view.model.js",
                        "~/Areas/FinancialAid/Scripts/Awards/my.awards.js",
                        "~/Scripts/Chart.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminmyawardsscripts").Include(
                        "~/Areas/FinancialAid/Scripts/Awards/my.awards.admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidoutsideawardsscripts").Include(
                        "~/Areas/FinancialAid/Scripts/OutsideAwards/outside.awards.mapping.js",
                        "~/Areas/FinancialAid/Scripts/OutsideAwards/outside.awards.view.model.js",
                        "~/Areas/FinancialAid/Scripts/OutsideAwards/outside.awards.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminoutsideawardsscripts").Include(
                        "~/Areas/FinancialAid/Scripts/OutsideAwards/outside.awards.admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidawardletterscripts").Include(
                        "~/Areas/FinancialAid/Scripts/AwardLetter/award.letter.mapping.js",
                        "~/Areas/FinancialAid/Scripts/AwardLetter/award.letter.view.model.js",
                        "~/Areas/FinancialAid/Scripts/AwardLetter/award.letter.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminawardletterscripts").Include(
                        "~/Areas/FinancialAid/Scripts/AwardLetter/award.letter.admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidsapscripts").Include(
                        "~/Areas/FinancialAid/Scripts/SatisfactoryAcademicProgress/academic.progress.mapping.js",
                        "~/Areas/FinancialAid/Scripts/SatisfactoryAcademicProgress/academic.progress.view.model.js",
                        "~/Areas/FinancialAid/Scripts/SatisfactoryAcademicProgress/academic.progress.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminsapscripts").Include(
                        "~/Areas/FinancialAid/Scripts/SatisfactoryAcademicProgress/academic.progress.admin.js"
                ));

           bundles.Add(new ScriptBundle("~/bundles/financialaidcorrespondencescripts").Include(
                        "~/Areas/FinancialAid/Scripts/CorrespondenceOption/correspondence.option.mapping.js",
                        "~/Areas/FinancialAid/Scripts/CorrespondenceOption/correspondence.option.view.model.js",
                        "~/Areas/FinancialAid/Scripts/CorrespondenceOption/correspondence.option.js"
                ));

           bundles.Add(new ScriptBundle("~/bundles/financialaidadmincorrespondencescripts").Include(
                       "~/Areas/FinancialAid/Scripts/CorrespondenceOption/correspondence.option.admin.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidrequireddocsscripts").Include(
                        "~/Areas/FinancialAid/Scripts/Documents/documents.mapping.js",
                        "~/Areas/FinancialAid/Scripts/Documents/documents.view.model.js",
                        "~/Areas/FinancialAid/Scripts/Documents/required.documents.js"

                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminrequireddocsscripts").Include(
                        "~/Areas/FinancialAid/Scripts/Documents/required.documents.admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidloanrequestscripts").Include(
                        "~/Areas/FinancialAid/Scripts/LoanRequest/loan.request.mapping.js",
                        "~/Areas/FinancialAid/Scripts/LoanRequest/loan.request.view.model.js",
                        "~/Areas/FinancialAid/Scripts/LoanRequest/loan.request.js"

                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminloanrequestscripts").Include(
                        "~/Areas/FinancialAid/Scripts/LoanRequest/loan.request.admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidshoppingsheetscripts").Include(
                        "~/Areas/FinancialAid/Scripts/ShoppingSheet/shopping.sheet.mapping.js",
                        "~/Areas/FinancialAid/Scripts/ShoppingSheet/shopping.sheet.view.model.js",
                        "~/Areas/FinancialAid/Scripts/ShoppingSheet/shopping.sheet.js"

                ));

            bundles.Add(new ScriptBundle("~/bundles/financialaidadminshoppingsheetscripts").Include(
                        "~/Areas/FinancialAid/Scripts/ShoppingSheet/shopping.sheet.admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/studentfinancescripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Areas/Finance/Scripts/Shared/student.finance.js"));

            bundles.Add(new ScriptBundle("~/bundles/accountactivityscripts").Include(
                        "~/Areas/Finance/Scripts/AccountActivity/account.activity.view.model.js",
                        "~/Areas/Finance/Scripts/Payments/make.a.payment.view.model.js",
                        "~/Areas/Finance/Scripts/AccountActivity/account.activity.js",
                        "~/Areas/Finance/Scripts/AccountActivity/account.activity.mapping.js"));

            bundles.Add(new ScriptBundle("~/bundles/financeadminscripts").Include(
                        "~/Areas/Finance/Scripts/Shared/admin.search.view.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/accountactivityadminscripts").Include(
                        "~/Areas/Finance/Scripts/AccountActivity/account.activity.admin.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/makeapaymentadminscripts").Include(
                        "~/Areas/Finance/Scripts/Payments/make.a.payment.admin.js"));

            bundles.Add(new ScriptBundle("~/bundles/registrationactivityadminscripts").Include(
                        "~/Areas/Finance/Scripts/RegistrationActivity/registration.activity.admin.js"));

            bundles.Add(new ScriptBundle("~/bundles/accountsummaryadminscripts").Include(
                        "~/Areas/Finance/Scripts/AccountSummary/account.summary.admin.js"));

            bundles.Add(new ScriptBundle("~/bundles/accountsummaryscripts").Include(
                        "~/Areas/Finance/Scripts/AccountSummary/account.summary.view.model.js",
                        "~/Areas/Finance/Scripts/AccountSummary/account.summary.js"));

            bundles.Add(new ScriptBundle("~/bundles/currency").Include(
                        "~/Scripts/jquery.formatCurrency-1.4.0.min.js",
                        "~/Scripts/jquery.formatCurrency." + System.Threading.Thread.CurrentThread.CurrentCulture.Name + ".js"));

            bundles.Add(new ScriptBundle("~/bundles/makeapaymentscripts").Include(
                        "~/Areas/Finance/Scripts/Payments/make.a.payment.view.model.js",
                        "~/Areas/Finance/Scripts/Payments/make.a.payment.mapping.js"));

            bundles.Add(new ScriptBundle("~/bundles/makeapaymentindexscripts").Include(
                        "~/Areas/Finance/Scripts/Payments/make.a.payment.js"));

            bundles.Add(new ScriptBundle("~/bundles/makeapaymentreviewscripts").Include(
                        "~/Areas/Finance/Scripts/Payments/payment.review.js"));

            bundles.Add(new ScriptBundle("~/bundles/makeapaymentecheckscripts").Include(
                        "~/Areas/Finance/Scripts/Payments/electronic.check.entry.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/makeapaymentacknowledgementscripts").Include(
                        "~/Areas/Finance/Scripts/Payments/payment.acknowledgement.js"));

            bundles.Add(new ScriptBundle("~/bundles/immediatepaymentscripts").Include(
                        "~/Areas/Finance/Scripts/immediate.payments.js"));

            bundles.Add(new ScriptBundle("~/bundles/paymentoptionsscripts").Include(
                        "~/Areas/Finance/Scripts/payment.options.js"));

            bundles.Add(new ScriptBundle("~/bundles/registrationactivityvmscripts").Include(
                        "~/Areas/Finance/Scripts/RegistrationActivity/registration.activity.view.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/registrationactivityscripts").Include(
                        "~/Areas/Finance/Scripts/RegistrationActivity/registration.activity.js"));

            bundles.Add(new ScriptBundle("~/bundles/registrationsummaryscripts").Include(
                        "~/Areas/Finance/Scripts/registration.summary.js"));

            bundles.Add(new ScriptBundle("~/bundles/kovalidationdebugscripts").Include(
                        "~/Scripts/knockout.validation.debug.js"));

            bundles.Add(new ScriptBundle("~/bundles/studentscripts").Include(
                        "~/Scripts/base.view.model.js",                        
                        "~/Scripts/knockout.validation.js",
                        "~/Scripts/Components/AddressEntry/AddressModel.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/studentrequestpaymentscripts").Include(
                        "~/Areas/Student/Scripts/Payments/base.student.request.payment.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/enrverpaymentscripts").Include(
                        "~/Areas/Student/Scripts/Payments/enrollment.verification.request.payment.model.js", 
                        "~/Areas/Student/Scripts/Payments/enrollment.verification.request.payment.js"));

            bundles.Add(new ScriptBundle("~/bundles/enrverscripts").Include(
                        "~/Areas/Student/Scripts/base.student.request.view.model.js",
                        "~/Areas/Student/Scripts/enrollment.verification.requests.view.model.js",
                        "~/Areas/Student/Scripts/enrollment.verification.requests.js",
                        "~/Areas/Student/Scripts/base.student.request.collector.model.js",
                        "~/Areas/Student/Scripts/enrollment.verification.request.collector.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/enrverindexscripts").Include(
                        "~/Areas/Student/Scripts/student.request.info.view.model.js",
                        "~/Areas/Student/Scripts/enrollment.requests.index.js"));

            bundles.Add(new ScriptBundle("~/bundles/graduationscripts").Include(
                        "~/Scripts/knockout.validation.js",
                        "~/Areas/Student/Scripts/graduation.add.view.model.js",
                        "~/Areas/Student/Scripts/Payments/graduation.payment.model.js",
                        "~/Areas/Student/Scripts/graduation.view.model.js",
                        "~/Scripts/Components/AddressEntry/AddressModel.js",
                        "~/Areas/Student/Scripts/graduation.application.js"));

            bundles.Add(new ScriptBundle("~/bundles/graduationindexscripts").Include(
                        "~/Areas/Student/Scripts/graduation.info.view.model.js",
                        "~/Areas/Student/Scripts/graduation.index.js"));

            bundles.Add(new ScriptBundle("~/bundles/graduationsubmissionstatusscripts").Include(
                        "~/Areas/Student/Scripts/graduation.status.view.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/tranrequestindexscripts").Include(
                        "~/Areas/Student/Scripts/transcript.request.info.view.model.js",
                        "~/Areas/Student/Scripts/transcript.requests.index.js"));

            bundles.Add(new ScriptBundle("~/bundles/tranrequestscripts").Include(
                        "~/Areas/Student/Scripts/base.student.request.view.model.js",
                        "~/Areas/Student/Scripts/transcript.requests.view.model.js",
                        "~/Areas/Student/Scripts/transcript.requests.js",
                        "~/Areas/Student/Scripts/base.student.request.collector.model.js",
                        "~/Areas/Student/Scripts/transcript.request.collector.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/tranrequestpaymentscripts").Include(
                        "~/Areas/Student/Scripts/Payments/transcript.request.payment.model.js",
                        "~/Areas/Student/Scripts/Payments/transcript.request.payment.js"));

            bundles.Add(new ScriptBundle("~/bundles/studentgradesindexscripts").Include(
                        "~/Areas/Student/Scripts/student.grades.mapping.js",
                        "~/Areas/Student/Scripts/student.grades.view.model.js",
                        "~/Areas/Student/Scripts/student.grades.index.js"));

            bundles.Add(new ScriptBundle("~/bundles/advisescripts").Include(
                        "~/Areas/Planning/Scripts/Repositories/plan.repository.js",
                        "~/Areas/Planning/Scripts/Repositories/requirements.repository.js",
                        "~/Scripts/base.view.model.js",
                        "~/Areas/Planning/Scripts/load.sample.plan.js",
                        "~/Areas/Planning/Scripts/requirements.mappings.js",
                        "~/Areas/Planning/Scripts/program.requirements.view.model.js",
                        "~/Areas/Planning/Scripts/plan.mapping.js",
                        "~/Areas/Planning/Scripts/plan.view.model.js",
                        "~/Scripts/Courses/catalog.search.subjects.view.model.js",
                        "~/Scripts/Courses/catalog.search.criteria.view.model.js",
                        "~/Scripts/Courses/catalog.result.view.model.js",
                         "~/Scripts/Courses/catalog.result.view.model.factory.js",
                        "~/Areas/Planning/Scripts/advising.view.model.js",
                        "~/Areas/Planning/Scripts/plan.archive.view.model.js",
                        "~/Areas/Planning/Scripts/test.score.view.model.js",
                        "~/Areas/Planning/Scripts/transcripts.view.model.js",
                        "~/Areas/Student/Scripts/student.grades.mapping.js",
                        "~/Areas/Student/Scripts/student.grades.view.model.js",
                        "~/Areas/Planning/Scripts/advising.js",
                        "~/Scripts/Courses/course.section.details.js",
                        "~/Scripts/Courses/course.details.js",
                        "~/Scripts/Courses/section.details.js"));

            bundles.Add(new ScriptBundle("~/bundles/advisorsindexscripts").Include(
                        "~/Areas/Planning/Scripts/advisees.view.model.js",
                        "~/Areas/Planning/Scripts/advisees.js"));

            bundles.Add(new ScriptBundle("~/bundles/coursesindexscripts").Include(
                "~/Scripts/base.view.model.js",
                "~/Scripts/Courses/catalog.search.subjects.view.model.js",
                "~/Scripts/Courses/catalog.advanced.search.keyword.components.model.js",
                        "~/Scripts/Courses/catalog.advanced.search.view.model.js",
                        "~/Scripts/Courses/catalog.search.index.js"));

            bundles.Add(new ScriptBundle("~/bundles/coursessearchresultscripts").Include(
                        "~/Scripts/base.view.model.js", 
                        "~/Areas/Planning/Scripts/plan.mapping.js",
                        "~/Areas/Planning/Scripts/Repositories/plan.repository.js",
                        "~/Areas/Planning/Scripts/Repositories/requirements.repository.js",
                        "~/Scripts/Courses/catalog.result.view.model.js",
                        "~/Scripts/Courses/catalog.result.view.model.factory.js",
                        "~/Areas/Planning/Scripts/program.requirements.subscriber.js",
                        "~/Scripts/Courses/catalog.result.js",
                        "~/Scripts/Courses/course.section.details.js",
                        "~/Scripts/Courses/catalog.search.criteria.view.model.js",
                        "~/Scripts/Courses/course.details.js",
                        "~/Scripts/Courses/section.details.js"));

            bundles.Add(new ScriptBundle("~/bundles/degreeplansindexscripts").Include(
                        "~/Areas/Planning/Scripts/Repositories/plan.repository.js",
                        "~/Areas/Planning/Scripts/Repositories/requirements.repository.js",
                        "~/Areas/Planning/Scripts/load.sample.plan.js", 
                        "~/Areas/Planning/Scripts/plan.mapping.js", 
                        "~/Scripts/base.view.model.js", 
                        "~/Areas/Planning/Scripts/plan.view.model.js",
                        "~/Areas/Planning/Scripts/program.requirements.subscriber.js",
                         "~/Areas/Planning/Scripts/plan.js", 
                        "~/Areas/Planning/Scripts/requirements.mappings.js",
                        "~/Areas/Planning/Scripts/program.requirements.view.model.js",
                        "~/Scripts/Courses/course.section.details.js",
                        "~/Scripts/Courses/course.details.js",
                        "~/Scripts/Courses/section.details.js"));

            bundles.Add(new ScriptBundle("~/bundles/degreeplansprintschedulescripts").Include(
                        "~/Areas/Planning/Scripts/fullcalendar.js",
                        "~/Scripts/knockout-3.4.0.js",
                        "~/Scripts/knockout.mapping.js",
                        "~/Scripts/knockout-postbox.js",
                        "~/Scripts/globalize.js",
                        "~/Scripts/global.js",
                        "~/Scripts/knockout.bindings.js",
                        "~/Areas/Planning/Scripts/plan.mapping.js",
                        "~/Scripts/base.view.model.js",
                        "~/Areas/Planning/Scripts/plan.view.model.js",
                        "~/Areas/Planning/Scripts/plan.print.js"));

            bundles.Add(new ScriptBundle("~/bundles/facultyindexscripts").Include(
                        "~/Areas/Planning/Scripts/faculty.index.js"));

            bundles.Add(new ScriptBundle("~/bundles/facultynavigationscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Scripts/knockout.validation.js",
                        "~/Areas/Planning/Scripts/faculty.grading.final.grade.view.model.js",
                        "~/Areas/Planning/Scripts/faculty.grading.midterm.grade.view.model.js",
                        "~/Areas/Planning/Scripts/faculty.grading.view.model.js",
                        "~/Areas/Planning/Scripts/faculty.navigation.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/planninghomeindexscripts").Include(
                        "~/Areas/Planning/Scripts/Repositories/plan.repository.js",
                        "~/Areas/Planning/Scripts/Repositories/requirements.repository.js",
                        "~/Areas/Planning/Scripts/plan.mapping.js",
                        "~/Scripts/base.view.model.js",
                        "~/Areas/Planning/Scripts/plan.view.model.js",
                        "~/Areas/Planning/Scripts/requirements.mappings.js",
                        "~/Areas/Planning/Scripts/program.requirements.view.model.js",
                        "~/Areas/Planning/Scripts/home.js"));

            bundles.Add(new ScriptBundle("~/bundles/myprogressscripts").Include(
                        "~/Areas/Planning/Scripts/Repositories/plan.repository.js",
                        "~/Areas/Planning/Scripts/Repositories/requirements.repository.js",
                        "~/Scripts/base.view.model.js",
                        "~/Areas/Planning/Scripts/load.sample.plan.js",
                        "~/Areas/Planning/Scripts/requirements.mappings.js",
                        "~/Areas/Planning/Scripts/program.requirements.view.model.js",
                        "~/Areas/Planning/Scripts/my.progress.js",
                        "~/Areas/Planning/Scripts/plan.mapping.js"));

            bundles.Add(new ScriptBundle("~/bundles/testscoresindexscripts").Include(
                        "~/Areas/Planning/Scripts/test.score.view.model.js",
                        "~/Areas/Planning/Scripts/test.scores.index.js"));

            bundles.Add(new ScriptBundle("~/bundles/transcriptsindexscripts").Include(
                        "~/Areas/Planning/Scripts/transcripts.view.model.js",
                        "~/Areas/Planning/Scripts/transcripts.index.js"));

            //bundles.Add(new ScriptBundle("~/bundles/humanresourcesscripts").Include(
            //            "~/Scripts/base.view.model.js",
            //            "~/Areas/HumanResources/Scripts/human.resources.knockout.bindings.js",
            //            "~/Areas/HumanResources/Scripts/human.resources.extenders.js",
            //            "~/Areas/HumanResources/Scripts/human.resources.utilities.js"));

            //bundles.Add(new ScriptBundle("~/bundles/bankinginformationsscripts").Include(
            //            "~/Areas/HumanResources/Scripts/bank.account.usage.view.model.js",
            //            "~/Areas/HumanResources/Scripts/banking.information.view.model.js",
            //            "~/Areas/HumanResources/Scripts/banking.information.mapping.js",
            //            "~/Areas/HumanResources/Scripts/banking.information.js",
            //            "~/Scripts/jquery.ui.touch-punch.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/taxinformationscripts").Include(
                        "~/Scripts/base.view.model.js",
                        "~/Scripts/TaxInformation/tax.information.view.model.js",
                        "~/Scripts/TaxInformation/tax.form.information.js",
                        "~/Scripts/TaxInformation/tax.information.mapping.js",
                        "~/Scripts/TaxInformation/consent.model.js",
                        "~/Scripts/TaxInformation/statement.model.js"));

            bundles.Add(new ScriptBundle("~/bundles/requirejs").Include(
                        "~/Scripts/require.js"));

            bundles.Add(new ScriptBundle("~/bundles/userprofilescripts").Include(
                        "~/Scripts/Components/AddressEntry/AddressModel.js",
                        "~/Scripts/knockout.validation.js",
                        "~/Scripts/UserProfile/user.profile.repository.js",
                        "~/Scripts/UserProfile/user.profile.view.model.js",
                        "~/Scripts/UserProfile/user.profile.js"));

            bundles.Add(new ScriptBundle("~/bundles/emergencyinformationscripts").Include(
                        "~/Scripts/knockout.validation.js",
                        "~/Scripts/UserProfile/emergency.information.repository.js",
                        "~/Scripts/UserProfile/emergency.information.view.model.js",
                        "~/Scripts/UserProfile/emergency.information.js"));

            bundles.Add(new ScriptBundle("~/bundles/viewemergencyinformationscripts").Include(
                        "~/Scripts/UserProfile/view.emergency.information.repository.js",
                        "~/Scripts/UserProfile/view.emergency.information.view.model.js",
                        "~/Scripts/UserProfile/view.emergency.information.js"));

            bundles.Add(new ScriptBundle("~/bundles/emergencyinformationsearchscripts").Include(
                        "~/Scripts/UserProfile/emergency.information.search.view.model.js",
                        "~/Scripts/UserProfile/emergency.information.search.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/organizationalstructurescripts").Include(
                        "~/Scripts/OrganizationalStructure/organizational.structure.repository.js",
                        "~/Scripts/OrganizationalStructure/organizational.structure.view.model.js",
                        "~/Scripts/OrganizationalStructure/organizational.structure.js"));

            bundles.Add(new ScriptBundle("~/bundles/userpreferencesscripts").Include(
                        "~/Scripts/UserProfile/preferences.repository.js",
                        "~/Scripts/UserProfile/preferences.view.model.js",
                        "~/Scripts/UserProfile/preferences.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminscripts").Include(
                        "~/Scripts/admin.js",
                        "~/Scripts/knockout.validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminsettingsscripts").Include(
                        "~/Scripts/admin.settings.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminsitescripts").Include(
                        "~/Scripts/admin.site.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminmenuscripts").Include(
                        "~/Scripts/admin.menu.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminpagescripts").Include(
                        "~/Scripts/admin.page.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminthemescripts").Include(
                        "~/Scripts/spectrum.js"));

            bundles.Add(new ScriptBundle("~/bundles/guesthomecoursecatalogscripts").Include(
                        "~/Scripts/base.view.model.js",
                         "~/Scripts/Courses/catalog.search.subjects.view.model.js",
                        "~/Scripts/Courses/catalog.advanced.search.keyword.components.model.js",
                        "~/Scripts/Courses/catalog.advanced.search.view.model.js",
                        "~/Scripts/Courses/catalog.search.index.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/guesthomecoursessearchresultscripts").Include(
                       "~/Scripts/base.view.model.js",
                        "~/Scripts/Courses/planning.global.js",
                       "~/Scripts/Courses/catalog.result.view.model.js",
                        "~/Scripts/Courses/catalog.result.view.model.factory.js",
                       "~/Scripts/Courses/catalog.result.js",
                       "~/Scripts/Courses/course.section.details.js",
                        "~/Scripts/Courses/catalog.search.criteria.view.model.js",
                        "~/Scripts/Courses/section.details.js"));

            bundles.Add(new ScriptBundle("~/bundles/homepagescripts").Include(
                        "~/Scripts/homepage.js"));

            bundles.Add(new ScriptBundle("~/bundles/loginscripts").Include(
                        "~/Scripts/jquery.watermark.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/resourcefileeditorcripts").Include(
                        "~/Scripts/knockout-3.4.0.js",
                        "~/Scripts/knockout.validation.js",
                        "~/Scripts/ResourceFileEditor/resource.file.editor.view.model.js",
                        "~/Scripts/ResourceFileEditor/resource.file.editor.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                       "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/cssoverrides").Include(
                       "~/Content/Theme.css",
                       "~/Content/Custom.css"));

            bundles.Add(new StyleBundle("~/Areas/Planning/Content/planningcss").Include(
               "~/Areas/Planning/Content/fullcalendar.css"));


            bundles.Add(new StyleBundle("~/Areas/Student/Content/StudentRequestsBundlecss").Include("~/Areas/Student/Content/Requests.css"));

            bundles.Add(new StyleBundle("~/Areas/Student/Content/GraduationBundlecss").Include("~/Areas/Student/Content/Graduation.css"));

            bundles.Add(new StyleBundle("~/Areas/Student/Content/TranscriptRequestsBundlecss").Include("~/Areas/Student/Content/TranscriptRequests.css"));

            bundles.Add(new StyleBundle("~/Areas/Planning/Content/PlanningBundlecss").Include("~/Areas/Planning/Content/Planning.css"));

            bundles.Add(new StyleBundle("~/Areas/Finance/Content/FinanceBundlecss").Include("~/Areas/Finance/Content/Finance.css"));

            bundles.Add(new StyleBundle("~/Areas/ColleagueFinance/Content/ColleagueFinancecss").Include("~/Areas/ColleagueFinance/Content/ColleagueFinance.css"));

            bundles.Add(new StyleBundle("~/Areas/HumanResources/Content/HumanResourcescss").Include("~/Areas/HumanResources/Content/HumanResources.css"));

            bundles.Add(new StyleBundle("~/Areas/FinancialAid/Content/FinancialAidBundlecss").Include("~/Areas/FinancialAid/Content/FinancialAid.css"));

            bundles.Add(new StyleBundle("~/Areas/FinancialAid/Content/FinancialAidShoppingSheetBundlecss").Include("~/Areas/FinancialAid/Content/ShoppingSheet.css"));

            bundles.Add(new StyleBundle("~/Areas/Student/Content/StudentGradesBundlecss").Include("~/Areas/Student/Content/Grades.css"));

            bundles.Add(new StyleBundle("~/Content/adminthemecss").Include("~/Content/spectrum.css"));
        }
    }
}
