﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System.Web.Mvc;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Filters;
using slf4net;

namespace Ellucian.Web.Student
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            // add support for WA3 style sso tokens
            // set order to be less than -1 so that this auth filter happens before any others!!
            filters.Add(new SingleSignOnFilter(
                DependencyResolver.Current.GetService<Settings>(), 
                DependencyResolver.Current.GetService<ILogger>()), -2);

            filters.Add(new SiteMessageFilter(DependencyResolver.Current.GetService<Ellucian.Web.Mvc.Menu.ISiteService>().Get(), DependencyResolver.Current.GetService<Settings>(), DependencyResolver.Current.GetService<ILogger>()));
        }
    }
}