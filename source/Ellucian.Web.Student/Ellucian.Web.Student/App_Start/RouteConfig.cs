﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ellucian.Web.Student
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default_Help_Partial",
                url: "Help/Partial/{contentId}/{contentArea}",
                defaults: new { controller = "Help", action = "Partial", area = string.Empty, contentArea = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default_Help",
                url: "Help/{contentId}/{contentArea}",
                defaults: new { controller = "Help", action = "Index", area = string.Empty, contentArea = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, area = string.Empty },
                namespaces: new string[] { "Ellucian.Web.Student.Controllers" }
            );

            routes.MapRoute(
                  name: "CatchAll",
                  url: "{*any}",
                  defaults: new { controller = "Errors", action = "ErrorOccurred", area = string.Empty }
            );
        }
    }
}