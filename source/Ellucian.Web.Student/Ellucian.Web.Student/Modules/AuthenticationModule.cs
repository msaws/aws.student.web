﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Mvc.Session;
using Ellucian.Web.Student.Infrastructure;
using Hedtech.Identity.Mvc;
using slf4net;

namespace Ellucian.Web.Student.Modules
{
    /// <summary>
    /// Self-Service's custom authentication module
    /// </summary>
    public class AuthenticationModule : IHttpModule
    {
        public const string SsoActionKey = "ssoaction";
        public const string SsoLogoutRequestAction = "ssologoutrequest";
        public const string SsoLogoutResponseAction = "ssologoutresponse";
        public const string ReturnUrlKey = "returnUrl";

        private ILogger logger;
        private Settings appSettings;
        
        public String ModuleName
        {
            get { return "AuthenticationModule"; }
        }

        public void Init(HttpApplication httpApp)
        {
            try
            {
                httpApp.AuthenticateRequest += OnAuthentication;
                var onRequestAsyncWrapper = new EventHandlerTaskAsyncHelper(OnRequestAsync);
                httpApp.AddOnBeginRequestAsync(onRequestAsyncWrapper.BeginEventHandler, onRequestAsyncWrapper.EndEventHandler);
                httpApp.PostRequestHandlerExecute += OnResponse;

                this.appSettings = DependencyResolver.Current.GetService<Settings>();
                this.logger = DependencyResolver.Current.GetService<ILogger>();
            }
            catch (Exception ex)
            {
                this.logger.Error(ex, "AuthenticationModule.Init() Exception");
            }
        }

        async Task OnRequestAsync(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            try
            {
                // handle any pre-request sso processing
                if (application.Request.Params.AllKeys.Contains(SsoActionKey))
                {
                    // logout action
                    if ((application.Request.Params[SsoActionKey] as string).Equals(SsoLogoutRequestAction))
                    {
                        string returnUrl = null;
                        if (application.Request.Params.AllKeys.Contains(ReturnUrlKey) && !string.IsNullOrEmpty(application.Request.Params[ReturnUrlKey] as string))
                        {
                            returnUrl = application.Request.Params[ReturnUrlKey] as string;
                        }

                        // ensure session is signed out of
                        var userAuthentication = new UserAuthentication(new Ellucian.Colleague.Api.Client.ColleagueApiClient(this.appSettings.ApiBaseUrl, this.appSettings.ApiConnectionLimit, this.logger), new HttpContextWrapper(application.Context), this.logger);
                        await userAuthentication.InvalidateUser();

                        // logout of IdP
                        SamlActions.SamlLogoutRedirect(application, returnUrl);
                        application.CompleteRequest();
                    }
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(ex, "AuthenticationModule.OnRequest() Exception");
            }
        }

        void OnResponse(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            try
            {
                // handle any post-request sso processing
                if (application.Context.Items.Contains(SsoActionKey))
                {
                    // logout action
                    if ((application.Context.Items[SsoActionKey] as string).Equals(SsoLogoutResponseAction))
                    {
                        string returnUrl = null;
                        if(application.Response != null && application.Response.RedirectLocation != null)
                        {
                            returnUrl = application.Response.RedirectLocation;
                        }
                        SamlActions.SamlLogoutRedirect(application, returnUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(ex, "AuthenticationModule.OnResponse() Exception");
            }
        }

        void OnAuthentication(object sender, EventArgs a)
        {
            try
            {
                HttpApplication application = (HttpApplication)sender;
                // Set the principal from the session
                application.Context.User = SessionCookieManager.BuildPrincipalFromJwtCookie(application.Context);
            }
            catch (Exception ex)
            {
                this.logger.Error(ex, "AuthenticationModule.OnAuthentication() Exception");
            }
        }

        public void Dispose()
        {
            this.logger = null;
        }
    }
}