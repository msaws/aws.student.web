﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Collections;
using System.Linq.Expressions;
using Ellucian.Web.Student.Helper.Models;

namespace Ellucian.Web.Student.Helpers
{
    public static class AccordionExtensions
    {
        #region Constants

        private const string _ACCORDION_IDS = "AccordionIds";
        private const string _EXPAND_TEXT = "DATATEL_EXPAND_TEXT";
        private const string _COLLAPSE_TEXT = "DATATEL_COLLAPSE_TEXT";

        #endregion

        #region Accordion Extensions

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, AccordionModel model)
        {
            return Accordion(htmlHelper, model.Id, model.Title, model.AdditionalHeader, model.BodyTags);
        }

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, string id, string title, MvcHtmlString additionalHeader, List<MvcHtmlString> bodyTags)
        {
            StringBuilder sb = new StringBuilder();
            bodyTags.ForEach(x => sb.Append(x.ToHtmlString()));
            MvcHtmlString concatenatedHtml = new MvcHtmlString(sb.ToString());
            return Accordion(htmlHelper, id, title, additionalHeader, concatenatedHtml, null);
        }

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, string id, string title, MvcHtmlString additionalHeader, List<MvcHtmlString> bodyTags, string headerId)
        {
            StringBuilder sb = new StringBuilder();
            bodyTags.ForEach(x => sb.Append(x.ToHtmlString()));
            MvcHtmlString concatenatedHtml = new MvcHtmlString(sb.ToString());
            return Accordion(htmlHelper, id, title, additionalHeader, concatenatedHtml, null, headerId);
        }

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, string id, string title, MvcHtmlString body)
        {
            return Accordion(htmlHelper, id, title, null, body, null);
        }

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, string id, string title, MvcHtmlString body, string cssClasses)
        {
            return Accordion(htmlHelper, id, title, null, body, cssClasses);
        }

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, string id, string title, MvcHtmlString additionalHeader, MvcHtmlString body)
        {
            return Accordion(htmlHelper, id, title, additionalHeader, body, null);
        }

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, string id, string title, MvcHtmlString additionalHeader, MvcHtmlString body, string cssClasses)
        {
            return Accordion(htmlHelper, id, title, additionalHeader, body, cssClasses, null);
        }

        public static MvcHtmlString Accordion(this HtmlHelper htmlHelper, string id, string title, MvcHtmlString additionalHeader, MvcHtmlString body, string cssClasses, string headerId)
        {
            if (id == null) throw new Exception("Accordion.Id cannot be null");
            if (body == null) throw new Exception("Accordion.Body cannot be null");

            TagBuilder innerDiv = new TagBuilder("div");
            innerDiv.InnerHtml = body.ToHtmlString();

            TagBuilder link = new TagBuilder("a");
            link.MergeAttribute("href", "#");
            link.InnerHtml = title;

            TagBuilder header = new TagBuilder("h3");
            header.InnerHtml += link.ConvertToHtmlString();
            header.AddCssClass("accordionHeader");
            if (!string.IsNullOrEmpty(headerId))
            {
                header.MergeAttribute("id", headerId);
            }

            if (additionalHeader != null)
            {
                header.InnerHtml += additionalHeader.ToHtmlString();
            }

            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.MergeAttribute("id", TagBuilder.CreateSanitizedId(id));
            outerDiv.MergeAttribute("name", TagBuilder.CreateSanitizedId(id));
            if (!string.IsNullOrEmpty(cssClasses))
            {
                outerDiv.AddCssClass(cssClasses);
            }

            outerDiv.InnerHtml += header.ConvertToHtmlString();
            outerDiv.InnerHtml += innerDiv.ConvertToHtmlString();

            return outerDiv.ConvertToMvcHtmlString(TagRenderMode.Normal);
        }

        #endregion

        #region Accordion Script Extensions

        public static MvcHtmlString AccordionScript(this HtmlHelper htmlHelper, string id)
        {
            return AccordionScript(htmlHelper, id, null);
        }

        public static MvcHtmlString AccordionScript(this HtmlHelper htmlHelper, string id, Dictionary<string, string> attributes)
        {
            List<string> accordionIds = htmlHelper.ViewData[_ACCORDION_IDS] as List<string>;

            if (!string.IsNullOrEmpty(id))
            {
                accordionIds = new List<string>() { id };
            }

            string script = string.Empty;

            if (accordionIds != null)
            {
                foreach (string accordionId in accordionIds)
                {
                    script += "$('#" + accordionId + "').accordion({";

                    if (attributes != null)
                    {
                        foreach (string key in attributes.Keys)
                        {
                            script += key;
                            script += ": ";
                            script += attributes[key];
                            script += ", ";
                        }

                        // Strip the last comma
                        script = script.Substring(0, script.Length - 2);
                    }
                    else
                    {
                        script += "active: false,\n";
                        script += "collapsible: true,\n";
                        script += "autoHeight: false,\n";
                        script += "clearStyle: true,\n";
                        script += "icons: { 'header': 'ui-icon-carat-1-e', 'headerSelected': 'ui-icon-carat-1-s' }\n";
                    }

                    script += "});\n";
                }
            }

            MvcHtmlString accordionScript = new MvcHtmlString(script);

            return accordionScript;
        }

        #endregion

        #region Accordion Expander Extensions

        public static MvcHtmlString AccordionExpander(this HtmlHelper htmlHelper, string id)
        {
            return AccordionExpander(htmlHelper, id, null, null);
        }

        public static MvcHtmlString AccordionExpander(this HtmlHelper htmlHelper, string id, string expandText, string collapseText)
        {
            string localExpandText = expandText ?? "[+] Expand";
            string localCollapseText = collapseText ?? "[-] Collapse";

            if (!htmlHelper.ViewData.ContainsKey(_EXPAND_TEXT))
            {
                htmlHelper.ViewData.Add(_EXPAND_TEXT, localExpandText);
            }

            if (!htmlHelper.ViewData.ContainsKey(_COLLAPSE_TEXT))
            {
                htmlHelper.ViewData.Add(_COLLAPSE_TEXT, localCollapseText);
            }

            TagBuilder expanderLink = new TagBuilder("a");
            expanderLink.MergeAttribute("id", TagBuilder.CreateSanitizedId(id + "_Expand"));
            expanderLink.MergeAttribute("href", "#");
            expanderLink.AddCssClass("expanded");
            expanderLink.InnerHtml = localExpandText;

            TagBuilder collapseLink = new TagBuilder("a");
            collapseLink.MergeAttribute("id", TagBuilder.CreateSanitizedId(id + "_Collapse"));
            collapseLink.MergeAttribute("href", "#");
            collapseLink.AddCssClass("collapsed");
            collapseLink.InnerHtml = localCollapseText;

            MvcHtmlString expandCollapseButtons = MvcHtmlString.Create(expanderLink.ConvertToHtmlString() + collapseLink.ConvertToHtmlString());

            return expandCollapseButtons;
        }

        #endregion

        #region Accordion Expander Script Extensions

        public static MvcHtmlString AccordionExpanderScript(this HtmlHelper htmlHelper, string id)
        {
            return AccordionExpanderScript(htmlHelper, id, null);
        }

        public static MvcHtmlString AccordionExpanderScript(this HtmlHelper htmlHelper, string id, List<string> accordionIdList)
        {
            List<string> accordionIds = accordionIdList ?? htmlHelper.ViewData[_ACCORDION_IDS] as List<string>;
            string localExpandText = htmlHelper.ViewData[_EXPAND_TEXT] as string ?? "[+] Expand";
            string localCollapseText = htmlHelper.ViewData[_COLLAPSE_TEXT] as string ?? "[-] Collapse";

            string expandedState = TagBuilder.CreateSanitizedId(id + "ExpandState");
            string expandId = TagBuilder.CreateSanitizedId(id + "_Expand");
            string collapseId = TagBuilder.CreateSanitizedId(id + "_Collapse");

            string script = string.Empty;

            script += "var " + expandedState + " = false;\n";
            script += "$('#" + expandId + "').click(function() {";
            //script += "alert('State: '+" + expandedState + ");\n";
            script += "var activeAccordion;\n";
            script += expandedState + " = true;\n";
            if (accordionIds != null)
            {
                foreach (string accordionId in accordionIds)
                {
                    script += "activeAccordion = $('#" + accordionId + "').accordion( 'option', 'active' );\n";
                    script += "if( (" + expandedState + " === true && activeAccordion === false) || (" + expandedState + " === false && activeAccordion === 0)) {\n";
                    script += "$('#" + accordionId + "').accordion('activate', 0);\n";
                    script += "}\n";
                }
            }
            script += "$(this).focus();\n";
            script += "});\n\n";

            script += "$('#" + collapseId + "').click(function() {";
            //script += "alert('State: '+" + expandedState + ");\n";
            script += "var activeAccordion;\n";
            script += expandedState + " = false;\n";
            if (accordionIds != null)
            {
                foreach (string accordionId in accordionIds)
                {
                    script += "activeAccordion = $('#" + accordionId + "').accordion( 'option', 'active' );\n";
                    script += "if( (" + expandedState + " === true && activeAccordion === false) || (" + expandedState + " === false && activeAccordion === 0)) {\n";
                    script += "$('#" + accordionId + "').accordion('activate', 0);\n";
                    script += "}\n";
                }
            }
            script += "$(this).focus();\n";
            script += "});\n";

            MvcHtmlString accordionScript = new MvcHtmlString(script);

            return accordionScript;
        }
        #endregion

        public static MvcHtmlString ExpandCollapseButton(this HtmlHelper htmlHelper)
        {
            var defaultText = "Expand All";

            TagBuilder formButton = new TagBuilder("button");
            formButton.InnerHtml = defaultText;
            formButton.MergeAttribute("class", "expand-collapse-all secondary");
            formButton.MergeAttribute("type", "button");

            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.MergeAttribute("class", "expand-collapse-all-wrapper");

            outerDiv.InnerHtml += formButton.ConvertToHtmlString();

            return outerDiv.ConvertToMvcHtmlString(TagRenderMode.Normal);
        }
    }
}
