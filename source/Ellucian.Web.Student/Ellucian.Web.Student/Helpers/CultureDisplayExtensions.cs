﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Helpers
{
    public static class CultureDisplayExtensions
    {
        public static MvcHtmlString CultureDisplayFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return CultureDisplayHelper(htmlHelper,
                                 metadata,
                                 metadata.Model,
                                 ExpressionHelper.GetExpressionText(expression)
                                 );
        }

        private static MvcHtmlString CultureDisplayHelper(this HtmlHelper htmlHelper, ModelMetadata metadata, object value, string name)
        {
            string fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            if (String.IsNullOrEmpty(fullName))
            {
                throw new ArgumentException("Name cannot be null or empty", "name");
            }

            string formatString = metadata.DisplayFormatString ?? string.Empty;

            string valueParameter = string.Format(Thread.CurrentThread.CurrentUICulture, formatString, value);

            return MvcHtmlString.Create(valueParameter);
        }
    }
}
