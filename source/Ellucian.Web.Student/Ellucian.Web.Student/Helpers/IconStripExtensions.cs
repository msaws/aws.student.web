﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Ellucian.Web.Student.Helpers;
using Ellucian.Web.Student.Helper.Models;

namespace Ellucian.Web.Student.Helpers
{
    public static class IconStripExtensions
    {
        public static MvcHtmlString IconStrip(this HtmlHelper htmlHelper, string id, List<IconStripModel> contents)
        {
            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.MergeAttribute("id", TagBuilder.CreateSanitizedId(id));

            foreach (IconStripModel content in contents)
            {
                TagBuilder innerDiv = new TagBuilder("div");

                UrlHelper urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
                if (!string.IsNullOrEmpty(content.Image))
                {
                    innerDiv.AddCssClass("IconBarContent");
                    TagBuilder image = new TagBuilder("img");
                    image.MergeAttribute("src", urlHelper.Content(content.Image));

                    if (!string.IsNullOrEmpty(content.Description))
                    {
                        image.MergeAttribute("alt", content.Description);
                    }

                    innerDiv.InnerHtml += image.ConvertToHtmlString();
                }
                else
                {
                    innerDiv.AddCssClass("IconBarDivider");
                }

                foreach (string line in content.Contents)
                {
                    TagBuilder lineDiv = new TagBuilder("div");
                    lineDiv.AddCssClass("IconBarDescription");
                    lineDiv.InnerHtml += line;

                    innerDiv.InnerHtml += lineDiv.ConvertToHtmlString();
                }

                outerDiv.InnerHtml += innerDiv.ConvertToHtmlString();
            }

            return outerDiv.ConvertToMvcHtmlString(TagRenderMode.Normal);
        }
    }
}
