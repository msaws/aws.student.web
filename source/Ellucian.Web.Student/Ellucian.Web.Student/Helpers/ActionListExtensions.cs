﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using Ellucian.Web.Student.Helper.Models;

namespace Ellucian.Web.Student.Helpers
{
    public static class ActionListExtensions
    {
        public static MvcHtmlString ActionList(this HtmlHelper htmlHelper, ActionListModel model)
        {
            if (model.ContainerName == null) throw new Exception("ActionList.ContainerName cannot be null");
            if (model.ActionName == null) throw new Exception("ActionList.ActionName cannot be null");
            if (model.ControllerName == null) throw new Exception("ActionList.ControllerName cannot be null");

            TagBuilder title = null;
            if (!string.IsNullOrEmpty(model.ListTitle))
            {
                title = new TagBuilder("h2");
                title.SetInnerText(model.ListTitle);
            }

            TagBuilder innerDiv = new TagBuilder("div");
            innerDiv.AddCssClass("actionListWrapper");

            TagBuilder unorderedList = new TagBuilder("ul");

            foreach (ActionListArguments args in model.Arguments)
            {
                TagBuilder listItem = new TagBuilder("li");
                MvcHtmlString actionLink;

                if (model.AjaxEnabled)
                {
                    AjaxHelper Ajax = new AjaxHelper(htmlHelper.ViewContext, htmlHelper.ViewDataContainer);
                    actionLink = Ajax.ActionLink(args.LinkDescription, model.ActionName, model.ControllerName, args.CallArguments, args.CallOptions);
                }
                else
                {
                    actionLink = htmlHelper.ActionLink(args.LinkDescription, model.ActionName, model.ControllerName, args.CallArguments, args.CallOptions);
                }

                listItem.InnerHtml = actionLink.ToString();

                if (!string.IsNullOrEmpty(args.LinkSubtitle))
                {
                    TagBuilder subTitle = new TagBuilder("span");
                    subTitle.AddCssClass("actionListSubtitle");
                    subTitle.SetInnerText(args.LinkSubtitle);
                    listItem.InnerHtml += subTitle.ConvertToHtmlString();
                }

                unorderedList.InnerHtml += listItem.ConvertToHtmlString();
            }

            innerDiv.InnerHtml = unorderedList.ConvertToHtmlString();

            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.MergeAttribute("id", TagBuilder.CreateSanitizedId(model.ContainerName));
            outerDiv.MergeAttribute("name", TagBuilder.CreateSanitizedId(model.ContainerName));

            if (title != null)
            {
                outerDiv.InnerHtml += title.ConvertToHtmlString();
            }
            outerDiv.InnerHtml += innerDiv.ConvertToHtmlString();

            return outerDiv.ConvertToMvcHtmlString(TagRenderMode.Normal);
        }
    }
}
