﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Ajax;

namespace Ellucian.Web.Student.Helper.Models
{
    /// <summary>
    /// Data model for action list arguments
    /// </summary>
    public class ActionListArguments
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ActionListArguments()
        {
            CallOptions = new AjaxOptions();
            CallArguments = new object();
            LinkDescription = string.Empty;
            LinkSubtitle = string.Empty;
        }

        public AjaxOptions CallOptions { get; set; }
        public object CallArguments { get; set; }
        public string LinkDescription { get; set; }
        public string LinkSubtitle { get; set; }
    }
}