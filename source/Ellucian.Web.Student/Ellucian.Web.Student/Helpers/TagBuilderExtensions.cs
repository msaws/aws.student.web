﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Diagnostics;

namespace Ellucian.Web.Student.Helpers
{
    public static class TagBuilderExtensions
    {
        public static MvcHtmlString ConvertToMvcHtmlString(this TagBuilder tagBuilder)
        {
            return ConvertToMvcHtmlString(tagBuilder, TagRenderMode.Normal);
        }

        public static MvcHtmlString ConvertToMvcHtmlString(this TagBuilder tagBuilder, TagRenderMode renderMode)
        {
            return new MvcHtmlString(tagBuilder.ToString(renderMode));
        }

        public static string ConvertToHtmlString(this TagBuilder tagBuilder)
        {
            return ConvertToMvcHtmlString(tagBuilder, TagRenderMode.Normal).ToHtmlString();
        }
    }
}
