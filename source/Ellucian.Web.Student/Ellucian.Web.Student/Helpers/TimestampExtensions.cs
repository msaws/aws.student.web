﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Helpers
{
    public static class TimestampExtensions
    {
        public static MvcHtmlString Timestamp(this HtmlHelper htmlHelper)
        {
            return Timestamp(htmlHelper, false);
        }

        public static MvcHtmlString Timestamp(this HtmlHelper htmlHelper, bool displayText)
        {
            string timeStamp;

            if (displayText)
            {
                timeStamp = "<div class=\"timestamp\">";
                timeStamp += DateTime.Now.ToString();
                timeStamp += "</div>";
            }
            else
            {
                timeStamp = "<!-- ";
                timeStamp += DateTime.Now.ToString();
                timeStamp += " -->";
            }

            return new MvcHtmlString(timeStamp);
        }
    }
}
