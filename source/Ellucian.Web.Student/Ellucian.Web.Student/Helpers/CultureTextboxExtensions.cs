﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Threading;

namespace Ellucian.Web.Student.Helpers
{
    public static class CultureTextboxExtensions
    {
        public static MvcHtmlString CultureTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return CultureTextBoxHelper(htmlHelper,
                                 metadata,
                                 metadata.Model,
                                 ExpressionHelper.GetExpressionText(expression),
                                 htmlAttributes);
        }

        private static MvcHtmlString CultureTextBoxHelper(this HtmlHelper htmlHelper, ModelMetadata metadata, object value, string name, IDictionary<string, object> htmlAttributes)
        {
            string fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            if (String.IsNullOrEmpty(fullName))
            {
                throw new ArgumentException("Name cannot be null or empty", "name");
            }

            TagBuilder tagBuilder = new TagBuilder("input");
            tagBuilder.MergeAttributes(htmlAttributes);
            tagBuilder.MergeAttribute("type", HtmlHelper.GetInputTypeString(InputType.Text));
            tagBuilder.MergeAttribute("name", fullName, true);

            string formatString = metadata.DisplayFormatString ?? string.Empty;

            string valueParameter = string.Format(Thread.CurrentThread.CurrentCulture, formatString, value);

            if (!string.IsNullOrEmpty(valueParameter))
            {
                if (valueParameter.Contains(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol))
                {
                    valueParameter = valueParameter.Replace(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol, string.Empty);
                }

                if (valueParameter.Contains(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator))
                {
                    valueParameter = valueParameter.Replace(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator, string.Empty);
                }
            }

            tagBuilder.MergeAttribute("value", valueParameter);

            tagBuilder.GenerateId(fullName);

            // If there are any errors for a named field, we add the css attribute.
            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(fullName, out modelState))
            {
                if (modelState.Errors.Count > 0)
                {
                    tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);
                }
            }

            tagBuilder.MergeAttributes(htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata));

            return tagBuilder.ConvertToMvcHtmlString(TagRenderMode.SelfClosing);
        }
    }
}
