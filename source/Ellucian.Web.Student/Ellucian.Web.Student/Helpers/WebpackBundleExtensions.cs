﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Helpers
{
    public static class WebpackBundleExtensions
    {
        private const string webpackDevServerHost = "localhost";
        private const int webpackDevServerPort = 9000;

        /// <summary>
        /// Renders a script tag containing the src attribute pointing to the given file path. Method checks if the
        /// webpack dev server is running on port 9000. If so, it renders a script tag to serve the file from the dev server
        /// to take advantage of hot reloads. If not, it renders a script tag to serve the file from the file system. 
        /// In both cases it appends a query parameter containing a random Guid to force the browser not to pull the file from cache.
        /// If HttpContext Debugging is disabled, this method returns an empty script tag.
        ///
        /// </summary>
        /// <param name="this">the HTML Helper</param>
        /// <param name="bundleName">the relative path to the bundle file from the application root, eg. /Areas/HumanResources/Builds/banking.information.bundle.js</param>    
        /// <returns>a script tag, e.g script type=text/javascript src=http://localhost:9000/Areas/HumanResources/Builds/banking.information.bundle.js?123415-12431-1234232-12343232</returns>
        public static MvcHtmlString RenderDevScriptBundle(this HtmlHelper @this, string pathtoFile)
        {
            if (!HttpContext.Current.IsDebuggingEnabled)
            {
                return new TagBuilder("script").ConvertToMvcHtmlString();
            }


            var isDevServerRunning = false;
            using (System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient())
            {
                try
                {
                    client.Connect(webpackDevServerHost, webpackDevServerPort);
                    isDevServerRunning = true;
                }
                catch (System.Net.Sockets.SocketException)
                {
                    isDevServerRunning = false;
                }
                client.Close();
            }


            var scriptSource = "";
            if (isDevServerRunning)
            {
                scriptSource = string.Format("http://{0}:{1}/{2}?v={3}", webpackDevServerHost, webpackDevServerPort, pathtoFile, Guid.NewGuid().ToString());                                
            }
            else
            {
                scriptSource = string.Format("{0}?v={1}",
                    UrlHelper.GenerateContentUrl(pathtoFile.PrependTilde(), new HttpContextWrapper(HttpContext.Current)),
                    Guid.NewGuid());
            }

            var result = buildScriptTag(scriptSource);
            return result;
        }

        /// <summary>
        /// Renders a link tag containing the href attribute pointing to the given file path. Method checks if the
        /// webpack dev server is running on port 9000. If so, it renders a link tag to serve the file from the dev server
        /// to take advantage of hot reloads. If not, it renders a link tag to serve the file from the file system. 
        /// In both cases it appends a query parameter containing a random Guid to force the browser to cache bust.
        /// If HttpContext Debugging is disabled, this method returns an empty link tag.
        ///
        /// </summary>
        /// <param name="this">the HTML Helper</param>
        /// <param name="bundleName">the relative path to the bundle file from the application root, eg. /Areas/HumanResources/Builds/banking.information.bundle.js</param>    
        /// <returns>a link tag, e.g link rel='stylesheet' href=http://localhost:9000/Areas/HumanResources/Builds/banking.information.bundle.js?123415-12431-1234232-12343232</returns>
      
        public static MvcHtmlString RenderDevStyleBundle(this HtmlHelper @this, string pathtoFile)
        {
            if (!HttpContext.Current.IsDebuggingEnabled)
            {
                return new TagBuilder("link").ConvertToMvcHtmlString();
            }


            var isDevServerRunning = false;
            using (System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient())
            {
                try
                {
                    client.Connect(webpackDevServerHost, webpackDevServerPort);
                    isDevServerRunning = true;
                }
                catch (System.Net.Sockets.SocketException)
                {
                    isDevServerRunning = false;
                }
                client.Close();
            }


            var styleSource = "";
            if (isDevServerRunning)
            {
                styleSource = string.Format("http://{0}:{1}/{2}?v={3}", webpackDevServerHost, webpackDevServerPort, pathtoFile, Guid.NewGuid().ToString());
            }
            else
            {
                styleSource = string.Format("{0}?v={1}",
                    UrlHelper.GenerateContentUrl(pathtoFile.PrependTilde(), new HttpContextWrapper(HttpContext.Current)),
                    Guid.NewGuid());
            }

            var result = buildLinkTag(styleSource);
            return result;
        }

        /// <summary>
        /// Renders a script tag to serve the production version of the script bundle
        /// The method appends a query parameter to the bundle containing the current version of Self Service to allow the browser
        /// to cache scripts between releases.
        /// 
        /// If HttpContext Debugging is enabled, this method returns an empty script tag.
        /// 
        /// </summary>
        /// <param name="this">the HtmlHelper object</param>
        /// <param name="pathToFile">the relative path to the file from the application root eg. /Areas/HumanResources/Builds/banking.information.bundle.min.js</param>
        /// <returns></returns>
        public static MvcHtmlString RenderProductionScriptBundle(this HtmlHelper @this, string pathToFile)
        {
            if (HttpContext.Current.IsDebuggingEnabled)
            {
                return new TagBuilder("script").ConvertToMvcHtmlString();
            }

            var contentUrl = UrlHelper.GenerateContentUrl(pathToFile.PrependTilde(), new HttpContextWrapper(HttpContext.Current));
            var scriptSource = string.Format("{0}?v={1}", contentUrl, Ellucian.Web.Student.Infrastructure.ApplicationMetadata.Version);

            var result = buildScriptTag(scriptSource);
            return result;

        }

        /// <summary>
        /// Renders a link rel='stylesheet' tag to serve the production version of the stylesheet bundle.
        /// The method appends a query parameter to the bundle containing the current version of Self Service to allow the browser
        /// to cache stylesheets between releases.
        /// </summary>
        /// <param name="this"></param>
        /// <param name="pathToFile"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderProductionStylesheetBundle(this HtmlHelper @this, string pathToFile)
        {
            if (HttpContext.Current.IsDebuggingEnabled)
            {
                return new TagBuilder("link").ConvertToMvcHtmlString();
            }

            var contentUrl = UrlHelper.GenerateContentUrl(pathToFile.PrependTilde(), new HttpContextWrapper(HttpContext.Current));
            var stylesheetHref = string.Format("{0}?v={1}", contentUrl, Ellucian.Web.Student.Infrastructure.ApplicationMetadata.Version);

            var result = buildLinkTag(stylesheetHref);
            return result;
        }

        private static MvcHtmlString buildScriptTag(string src)
        {
            var script = new TagBuilder("script");
            script.MergeAttribute("type", "text/javascript");
            script.MergeAttribute("src", src);

            return script.ConvertToMvcHtmlString();
        }

        private static MvcHtmlString buildLinkTag(string href)
        {
            var link = new TagBuilder("link");
            link.MergeAttribute("rel", "stylesheet");
            link.MergeAttribute("href", href);

            return link.ConvertToMvcHtmlString();
        }
    }
}