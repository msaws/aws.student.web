﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Helper.Models
{
    public class AccordionModel
    {
        public AccordionModel()
        {
            Id = string.Empty;
            BodyTags = new List<MvcHtmlString>();
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public MvcHtmlString AdditionalHeader { get; set; }
        public List<MvcHtmlString> BodyTags { get; set; }
    }
}
