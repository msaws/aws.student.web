﻿using System.Collections.Generic;
using System.Web.Mvc.Ajax;

namespace Ellucian.Web.Student.Helper.Models
{
    /// <summary>
    /// The AccountController class provides the various actions for the student financial statement. 
    /// </summary>
    public class ActionListModel
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ActionListModel()
        {
            Arguments = new List<ActionListArguments>();
            ListTitle = string.Empty;
            ControllerName = string.Empty;
            ActionName = string.Empty;
            ContainerName = string.Empty;
            AjaxEnabled = false;
        }

        public IList<ActionListArguments> Arguments { get; set; }
        public string ListTitle { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string ContainerName { get; set; }
        public bool AjaxEnabled { get; set; }
    }
}