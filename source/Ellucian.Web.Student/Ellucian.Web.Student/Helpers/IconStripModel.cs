﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Helper.Models
{
    public class IconStripModel
    {
        public IconStripModel()
        {
            Contents = new List<string>();
            Description = string.Empty;
            Image = string.Empty;
        }

        public List<string> Contents { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}
