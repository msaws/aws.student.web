﻿using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Licensing;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Tests.Infrastructure
{
    [TestClass]
    public class ApplicationMetadataTests
    {
        [ClassInitialize]
        public static void InitializeClass(TestContext testContext)
        {
            var logger = new Mock<ILogger>().Object;
            var mockLicensingService = new Mock<Ellucian.Web.Mvc.Licensing.ILicensingService>();
            mockLicensingService.Setup(ls => ls.GetLicensedModules())
                .Returns(new List<string>() { ModuleConstants.Base, ModuleConstants.Student, ModuleConstants.Finance, ModuleConstants.FinancialAid });
            var container = new UnityContainer();
            container.RegisterInstance<ILicensingService>(mockLicensingService.Object);
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        [TestMethod]
        public void ApplicationMetadata_VersionMatchesRegex()
        {
            // Version should look similar to "1.22.3.44"
            var version = ApplicationMetadata.Version;
            var regex = new Regex(@"\d+.\d+.\d+.\d+");
            Assert.IsTrue(regex.IsMatch(version));
        }

        [TestMethod]
        public void ApplicationMetadata_LicensedAreasContainsAreas()
        {
            var licensedAreasList = ApplicationMetadata.LicensedAreas.ToList();
            CollectionAssert.Contains(licensedAreasList, ModuleConstants.ModuleAreas[ModuleConstants.Base].First());
            CollectionAssert.Contains(licensedAreasList, ModuleConstants.ModuleAreas[ModuleConstants.Student].First());
            CollectionAssert.Contains(licensedAreasList, ModuleConstants.ModuleAreas[ModuleConstants.Finance].First());
        }
    }
}
