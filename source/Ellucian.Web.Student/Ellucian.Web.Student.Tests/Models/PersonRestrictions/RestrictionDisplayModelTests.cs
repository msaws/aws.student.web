﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Models.PersonRestrictions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Models.PersonRestrictions
{
    [TestClass]
    public class RestrictionDisplayModelTests
    {
        private List<PersonRestriction> restrictions = new List<PersonRestriction>();

        [TestInitialize]
        public void Initialize()
        {
            restrictions = new List<PersonRestriction>() 
            {
                new PersonRestriction() 
                { 
                    Details = "Open Registration for the current term will start soon!",
                    EndDate = DateTime.Today.AddDays(45),
                    Hyperlink = "http://www.ellucian.com/sn",
                    HyperlinkText = "Register Here!",
                    Id = "123",
                    OfficeUseOnly = false,
                    RestrictionId = "SN",
                    Severity = 5,
                    StartDate = DateTime.Today.AddDays(-15),
                    StudentId = "0001234",
                    Title = "Information"
                },
                new PersonRestriction() 
                { 
                    Details = "Make sure you confirm your User Profile data is current.",
                    EndDate = DateTime.Today.AddDays(55),
                    Hyperlink = null,
                    HyperlinkText = null,
                    Id = "124",
                    OfficeUseOnly = false,
                    RestrictionId = "SN2",
                    Severity = 300,
                    StartDate = DateTime.Today.AddDays(-5),
                    StudentId = "0001234",
                    Title = "Warning"
                },
                new PersonRestriction() 
                { 
                    Details = "<span>You must see the Registrar immediately.<a href='http://www.ellucian.com' target='_blank'>Get more information here</a></span>",
                    EndDate = DateTime.Today.AddDays(30),
                    Hyperlink = "http://www.ellucian.com/hs",
                    HyperlinkText = "Services",
                    Id = "125",
                    OfficeUseOnly = false,
                    RestrictionId = "HS",
                    Severity = 700,
                    StartDate = DateTime.Today.AddDays(-30),
                    StudentId = "0001234",
                    Title = "Critical"
                },
                new PersonRestriction() 
                { 
                    Details = "You must pay your parking ticket in the business office before you can register for classes. See the Registrar for more information about how to pay",
                    EndDate = DateTime.Today.AddDays(30),
                    Hyperlink = null,
                    HyperlinkText = null,
                    Id = "125",
                    OfficeUseOnly = false,
                    RestrictionId = "UC",
                    Severity = 1000,
                    StartDate = DateTime.Today.AddDays(-30),
                    StudentId = "0001234",
                    Title = "Uncategorized"
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            restrictions = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonRestrictionsModel_Constructor_NullRestrictions()
        {
            var model = new RestrictionDisplayModel(null, AlertStyle.Critical);
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_Information()
        {
            var model = new RestrictionDisplayModel(restrictions[0], AlertStyle.Information);
            Assert.AreEqual(restrictions[0].Details, model.Details);
            Assert.AreEqual(restrictions[0].EndDate, model.EndDate);
            Assert.AreEqual(restrictions[0].Hyperlink, model.Hyperlink);
            Assert.AreEqual(restrictions[0].HyperlinkText, string.IsNullOrEmpty(restrictions[0].HyperlinkText) ? "None" : restrictions[0].HyperlinkText);
            Assert.AreEqual(restrictions[0].Id, model.Id);
            Assert.AreEqual(restrictions[0].OfficeUseOnly, model.OfficeUseOnly);
            Assert.AreEqual(restrictions[0].RestrictionId, model.RestrictionId);
            Assert.AreEqual(restrictions[0].Severity, model.Severity);
            Assert.AreEqual(restrictions[0].StartDate, model.StartDate);
            Assert.AreEqual(restrictions[0].StudentId, model.StudentId);
            Assert.AreEqual(restrictions[0].Title, model.Title);
            Assert.AreEqual("info", model.Style);
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_Warning()
        {
            var model = new RestrictionDisplayModel(restrictions[1], AlertStyle.Warning);
            Assert.AreEqual(restrictions[1].Details, model.Details);
            Assert.AreEqual(restrictions[1].EndDate, model.EndDate);
            Assert.AreEqual(restrictions[1].Hyperlink, model.Hyperlink);
            Assert.AreEqual("None", model.HyperlinkText);
            Assert.AreEqual(restrictions[1].Id, model.Id);
            Assert.AreEqual(restrictions[1].OfficeUseOnly, model.OfficeUseOnly);
            Assert.AreEqual(restrictions[1].RestrictionId, model.RestrictionId);
            Assert.AreEqual(restrictions[1].Severity, model.Severity);
            Assert.AreEqual(restrictions[1].StartDate, model.StartDate);
            Assert.AreEqual(restrictions[1].StudentId, model.StudentId);
            Assert.AreEqual(restrictions[1].Title, model.Title);
            Assert.AreEqual("warning", model.Style);
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_Error()
        {
            var model = new RestrictionDisplayModel(restrictions[2], AlertStyle.Critical);
            Assert.AreEqual(restrictions[2].Details, model.Details);
            Assert.AreEqual(restrictions[2].EndDate, model.EndDate);
            Assert.AreEqual(restrictions[2].Hyperlink, model.Hyperlink);
            Assert.AreEqual(restrictions[2].HyperlinkText, string.IsNullOrEmpty(restrictions[2].HyperlinkText) ? "None" : restrictions[2].HyperlinkText);
            Assert.AreEqual(restrictions[2].Id, model.Id);
            Assert.AreEqual(restrictions[2].OfficeUseOnly, model.OfficeUseOnly);
            Assert.AreEqual(restrictions[2].RestrictionId, model.RestrictionId);
            Assert.AreEqual(restrictions[2].Severity, model.Severity);
            Assert.AreEqual(restrictions[2].StartDate, model.StartDate);
            Assert.AreEqual(restrictions[2].StudentId, model.StudentId);
            Assert.AreEqual(restrictions[2].Title, model.Title);
            Assert.AreEqual("error", model.Style);
        }
    }
}
