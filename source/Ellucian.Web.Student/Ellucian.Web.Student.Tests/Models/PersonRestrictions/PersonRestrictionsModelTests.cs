﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Models.PersonRestrictions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Models.PersonRestrictions
{
    [TestClass]
    public class PersonRestrictionsModelTests
    {
        private RestrictionConfiguration configuration = new RestrictionConfiguration();
        private List<PersonRestriction> restrictions = new List<PersonRestriction>();

        [TestInitialize]
        public void Initialize()
        {
            configuration = new RestrictionConfiguration()
            {
                Mapping = new List<SeverityStyleMapping>()
                {
                    new SeverityStyleMapping() { SeverityStart = 0,   SeverityEnd = 100, Style = AlertStyle.Information },
                    new SeverityStyleMapping() { SeverityStart = 101, SeverityEnd = 500, Style = AlertStyle.Warning     },
                    new SeverityStyleMapping() { SeverityStart = 501, SeverityEnd = 999, Style = AlertStyle.Critical    },
                }
            };

            restrictions = new List<PersonRestriction>() 
            {
                new PersonRestriction() 
                { 
                    Details = "Open Registration for the current term will start soon!",
                    EndDate = DateTime.Today.AddDays(45),
                    Hyperlink = "http://www.ellucian.com/sn",
                    HyperlinkText = "Register Here!",
                    Id = "123",
                    OfficeUseOnly = false,
                    RestrictionId = "SN",
                    Severity = 5,
                    StartDate = DateTime.Today.AddDays(-15),
                    StudentId = "0001234",
                    Title = "Information"
                },
                new PersonRestriction() 
                { 
                    Details = "Make sure you confirm your User Profile data is current.",
                    EndDate = DateTime.Today.AddDays(55),
                    Hyperlink = null,
                    HyperlinkText = null,
                    Id = "124",
                    OfficeUseOnly = false,
                    RestrictionId = "SN2",
                    Severity = 300,
                    StartDate = DateTime.Today.AddDays(-5),
                    StudentId = "0001234",
                    Title = "Warning"
                },
                new PersonRestriction() 
                { 
                    Details = "<span>You must see the Registrar immediately.<a href='http://www.ellucian.com' target='_blank'>Get more information here</a></span>",
                    EndDate = DateTime.Today.AddDays(30),
                    Hyperlink = "http://www.ellucian.com/hs",
                    HyperlinkText = "Services",
                    Id = "125",
                    OfficeUseOnly = false,
                    RestrictionId = "HS",
                    Severity = 700,
                    StartDate = DateTime.Today.AddDays(-30),
                    StudentId = "0001234",
                    Title = "Critical"
                },
                new PersonRestriction() 
                { 
                    Details = "You must pay your parking ticket in the business office before you can register for classes. See the Registrar for more information about how to pay",
                    EndDate = DateTime.Today.AddDays(30),
                    Hyperlink = null,
                    HyperlinkText = null,
                    Id = "125",
                    OfficeUseOnly = false,
                    RestrictionId = "UC",
                    Severity = 1000,
                    StartDate = DateTime.Today.AddDays(-30),
                    StudentId = "0001234",
                    Title = "Uncategorized"
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            configuration = null;
            restrictions = null;
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_NullRestrictions()
        {
            var model = new PersonRestrictionsModel(configuration, null);
            Assert.AreEqual(0, model.Restrictions.Count);
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_EmptyRestrictions()
        {
            var model = new PersonRestrictionsModel(configuration, new List<PersonRestriction>());
            Assert.AreEqual(0, model.Restrictions.Count);
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_NullConfiguration()
        {
            var model = new PersonRestrictionsModel(null, restrictions);
            Assert.AreEqual(restrictions.Count, model.Restrictions.Count);
            for(int i = 0; i < model.Restrictions.Count; i++)
            {
                Assert.AreEqual("info", model.Restrictions[i].Style);
            }
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_NullConfigurationMapping()
        {
            configuration.Mapping = null;
            var model = new PersonRestrictionsModel(configuration, restrictions);
            Assert.AreEqual(restrictions.Count, model.Restrictions.Count);
            for (int i = 0; i < model.Restrictions.Count; i++)
            {
                Assert.AreEqual("info", model.Restrictions[i].Style);
            }
        }

        [TestMethod]
        public void PersonRestrictionsModel_Constructor_Valid()
        {
            var model = new PersonRestrictionsModel(configuration, restrictions);
            Assert.AreEqual(restrictions.Count, model.Restrictions.Count);
            Assert.AreEqual("error", model.Restrictions[0].Style);
            Assert.AreEqual("warning", model.Restrictions[1].Style);
            Assert.AreEqual("info", model.Restrictions[2].Style);
            Assert.AreEqual("info", model.Restrictions[3].Style);
        }
    }
}
