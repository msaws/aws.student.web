﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.ResourceFileEditor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Models.ResourceFileEditor
{
    [TestClass]
    public class ResourceFileEntryModelTests
    {
        private string key = "Key";
        private string value = "Value";
        private string comment = "Comment";

        [TestMethod]
        public void ResourceFileEntryModel_BaseConstructor_Key()
        {
            var model = new ResourceFileEntryModel();
            Assert.AreEqual(null, model.Key);
        }

        [TestMethod]
        public void ResourceFileEntryModel_BaseConstructor_Value()
        {
            var model = new ResourceFileEntryModel();
            Assert.AreEqual(null, model.Value);
        }

        [TestMethod]
        public void ResourceFileEntryModel_BaseConstructor_Comment()
        {
            var model = new ResourceFileEntryModel();
            Assert.AreEqual(null, model.Comment);
        }

        [TestMethod]
        public void ResourceFileEntryModel_Constructor_Overload1_Key()
        {
            var model = new ResourceFileEntryModel(key, value);
            Assert.AreEqual(key, model.Key);
        }

        [TestMethod]
        public void ResourceFileEntryModel_Constructor__Overload1_Value()
        {
            var model = new ResourceFileEntryModel(key, value);
            Assert.AreEqual(value, model.Value);
        }

        [TestMethod]
        public void ResourceFileEntryModel_Constructor__Overload1_Comment()
        {
            var model = new ResourceFileEntryModel(key, value);
            Assert.AreEqual(string.Empty, model.Comment);
        }

        [TestMethod]
        public void ResourceFileEntryModel_Constructor_Overload2_Key()
        {
            var model = new ResourceFileEntryModel(key, value, comment);
            Assert.AreEqual(key, model.Key);
        }

        [TestMethod]
        public void ResourceFileEntryModel_Constructor__Overload2_Value()
        {
            var model = new ResourceFileEntryModel(key, value, comment);
            Assert.AreEqual(value, model.Value);
        }

        [TestMethod]
        public void ResourceFileEntryModel_Constructor__Overload2_Comment()
        {
            var model = new ResourceFileEntryModel(key, value, comment);
            Assert.AreEqual(comment, model.Comment);
        }
    }
}
