﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.ResourceFileEditor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Models.ResourceFileEditor
{
    [TestClass]
    public class ResourceFileModelTests
    {
        private string resourceFilePath = "ResourceFilePath";
        private List<ResourceFileEntryModel> resourceFileEntries = new List<ResourceFileEntryModel>()
        {
            new ResourceFileEntryModel("key", "ResourceFileEntries", "comment"),
            new ResourceFileEntryModel("key2", "ResourceFileEntries2")
        };

        [TestMethod]
        public void ResourceFileModel_BaseConstructor_ResourceFilePath()
        {
            var model = new ResourceFileModel();
            Assert.AreEqual(null, model.ResourceFilePath);
        }

        [TestMethod]
        public void ResourceFileModel_BaseConstructor_ResourceFileEntries()
        {
            var model = new ResourceFileModel();
            Assert.AreEqual(null, model.ResourceFileEntries);
        }

        [TestMethod]
        public void ResourceFileModel_Constructor_Overload1_ResourceFilePath()
        {
            var model = new ResourceFileModel(resourceFilePath);
            Assert.AreEqual(resourceFilePath, model.ResourceFilePath);
        }

        [TestMethod]
        public void ResourceFileModel_Constructor_Overload2_ResourceFilePath()
        {
            var model = new ResourceFileModel(resourceFilePath, resourceFileEntries);
            Assert.AreEqual(resourceFilePath, model.ResourceFilePath);
        }

        [TestMethod]
        public void ResourceFileModel_Constructor__Overload2_ResourceFileEntries()
        {
            var model = new ResourceFileModel(resourceFilePath, resourceFileEntries);
            Assert.AreEqual(resourceFileEntries.Count, model.ResourceFileEntries.Count);
        }
    }
}
