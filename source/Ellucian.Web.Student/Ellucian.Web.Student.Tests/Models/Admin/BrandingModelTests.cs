﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Models.Admin;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Models.Admin
{
    [TestClass]
    public class BrandingModelTests
    {
        private Settings settings;

        [TestInitialize]
        public void Initialize()
        {
            settings = new Settings()
            {
                ApiBaseUrl = "http://localhost",
                ApiConnectionLimit = 2
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            settings = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BrandingModel_Constructor_Settings_Null()
        {
            var model = new BrandingModel(null);
        }

        [TestMethod]
        public void BrandingModel_Constructor_Settings_Valid()
        {
            var model = new BrandingModel(settings);
            Assert.AreEqual(settings.ApiBaseUrl, model.ApiBaseUrl);
            Assert.AreEqual(settings.ApiConnectionLimit, model.ApiConnectionLimit);
            Assert.IsNull(model.Favicon);
            Assert.IsFalse(model.Messages.Any());
        }
    }
}
