﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.Admin;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Models.Admin
{
    [TestClass]
    public class AuthenticationConfigurationModelTests
    {
        [TestInitialize]
        public void Initialize()
        {

        }

        [TestMethod]
        public void AuthenticationConfigurationModel_Constructor()
        {
            var authenticationConfigurationModel = new AuthenticationConfigurationModel();
            Assert.IsNotNull(authenticationConfigurationModel.AvailablePrivateKeys);
            Assert.AreEqual(0, authenticationConfigurationModel.AvailablePrivateKeys.Count);

            Assert.IsNotNull(authenticationConfigurationModel.AvailablePublicKeys);
            Assert.AreEqual(0, authenticationConfigurationModel.AvailablePublicKeys.Count);

            Assert.IsNotNull(authenticationConfigurationModel.Messages);
            Assert.AreEqual(0, authenticationConfigurationModel.Messages.Count);
        }
    }
}
