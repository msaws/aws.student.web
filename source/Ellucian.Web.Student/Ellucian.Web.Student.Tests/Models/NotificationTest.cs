﻿using Ellucian.Web.Student.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace Ellucian.Web.Student.Tests
{
    /// <summary>
    ///This is a test class for NotificationTest and is intended
    ///to contain all NotificationTest Unit Tests
    ///</summary>
    [TestClass]
    public class NotificationTest
    {
        string message = "Test Notification";
        NotificationType type = NotificationType.Error;
        Boolean flash = true;

        /// <summary>
        ///A test for Notification Constructor
        ///</summary>
        [TestMethod]
        public void NotificationConstructorTest()
        {
            Notification target = new Notification();
            // Default message is empty
            Assert.AreEqual(target.Message, string.Empty);
            // Default  type is Information
            Assert.AreEqual(target.Type, NotificationType.Information.ToString());
            // Default flash is false
            Assert.AreEqual(target.Flash, false);
        }

        /// <summary>
        ///A test for Notification Constructor
        ///</summary>
        [TestMethod]
        public void NotificationConstructor2Test()
        {
            Notification target = new Notification(message, type);
            Assert.AreEqual(target.Message, message);
            Assert.AreEqual(target.Type, type.ToString());
            // Default flash is false
            Assert.AreEqual(target.Flash, false);
        }

        /// <summary>
        ///A test for Notification Constructor
        ///</summary>
        [TestMethod]
        public void NotificationConstructor3Test()
        {
            Notification target = new Notification(message, type, flash);
            Assert.AreEqual(target.Message, message);
            Assert.AreEqual(target.Type, type.ToString());
            Assert.AreEqual(target.Flash, flash);
        }
    }
}
