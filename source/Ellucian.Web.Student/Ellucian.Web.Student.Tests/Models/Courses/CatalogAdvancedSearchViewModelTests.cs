﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Models.Courses;
using Ellucian.Web.Student.Utility;
using Newtonsoft.Json;

namespace Ellucian.Web.Student.Tests.Models.Courses
{
    [TestClass]
    public class CatalogAdvancedSearchViewModelTests
    {
        CatalogAdvancedSearchViewModel viewModel;
         CourseSearchHelper helper;

        [TestInitialize]
        public  void Initialize()
        {
            viewModel = new CatalogAdvancedSearchViewModel();
            helper = new CourseSearchHelper();
            
        }

        [TestCleanup]
        public  void Cleanup()
        {
            viewModel = null;
        }

        [TestMethod]
        public void InitialValues_after_constructor()
        {
            Assert.IsNull(viewModel.AcademicLevel);
            Assert.IsNotNull(viewModel.Days);
            Assert.IsNull(viewModel.EndDate);
            Assert.IsNull(viewModel.Keyword);
            Assert.IsNotNull(viewModel.KeywordComponents);
            Assert.IsNull(viewModel.Location);
            Assert.IsNull(viewModel.StartDate);
            Assert.IsNull(viewModel.Subject);
            Assert.IsNull(viewModel.Term);
            Assert.IsNull(viewModel.TimeRange);
            Assert.IsTrue(viewModel.Days.Count == 0);
            Assert.IsTrue(viewModel.KeywordComponents.Count == 0);
            
        }
        [TestMethod]
        public void create_search_criteria_model_from_helper_with_null()
        {
           CatalogSearchCriteriaModel searchCriteria= helper.RetrieveCatalogSearchCriteriaFromAdvance(null);
           Assert.IsNotNull(searchCriteria);
        }
        [TestMethod]
        public void create_search_criteria_model_from_helper_with_all()
        {
            string model=@"{""term"":""2017/SP"",""location"":""MC"",""academicLevel"":""UG"",""startDate"":"""",""endDate"":"""",""days"":[""1"",""2""],""timeRange"":{""Item1"":""Morning (8am - Midday)"",""Item2"":480,""Item3"":720},""keywordComponents"":[{""subject"":""ACCT"",""courseNumber"":"""",""section"":""""},{""subject"":""ART"",""courseNumber"":""200"",""section"":""01""}],""isValid"":true}";
            CatalogSearchCriteriaModel searchCriteria = helper.RetrieveCatalogSearchCriteriaFromAdvance(model);
            Assert.IsNotNull(searchCriteria);
            Assert.IsTrue(searchCriteria.Terms.Count==1);
            Assert.IsTrue(searchCriteria.Terms[0] == @"2017/SP");
            Assert.IsTrue(searchCriteria.Locations.Count == 1);
            Assert.IsTrue(searchCriteria.Locations[0] == "MC");
            Assert.IsTrue(searchCriteria.AcademicLevels.Count == 1);
            Assert.IsTrue(searchCriteria.AcademicLevels[0] == "UG");
            Assert.IsNull(searchCriteria.StartDate);
            Assert.IsNull(searchCriteria.EndDate);
            Assert.IsTrue(searchCriteria.Days.Count == 2);
            Assert.IsTrue(searchCriteria.Days[0] == "1");
            Assert.IsTrue(searchCriteria.Days[1] == "2");
            Assert.IsTrue(searchCriteria.StartTime == 480);
            Assert.IsTrue(searchCriteria.EndTime == 720);
            Assert.IsTrue(searchCriteria.KeywordComponents.Count == 2);
            Assert.IsTrue(searchCriteria.KeywordComponents[0].Subject == "ACCT");
            Assert.IsTrue(searchCriteria.KeywordComponents[0].CourseNumber == "");
            Assert.IsTrue(searchCriteria.KeywordComponents[0].Section == "");
            Assert.IsTrue(searchCriteria.KeywordComponents[1].Subject == "ART");
            Assert.IsTrue(searchCriteria.KeywordComponents[1].CourseNumber == "200");
            Assert.IsTrue(searchCriteria.KeywordComponents[1].Section == "01");
        }

        [TestMethod]
        public void create_search_criteria_model_from_helper_with_dates()
        {
            string model = @"{""startDate"":""2/2/2016"",""endDate"":""2/2/2017"",""days"":[],""keywordComponents"":[]}";
            CatalogSearchCriteriaModel searchCriteria = helper.RetrieveCatalogSearchCriteriaFromAdvance(model);
            Assert.IsNotNull(searchCriteria);
            Assert.IsTrue(searchCriteria.Terms.Count == 0);
            Assert.IsTrue(searchCriteria.Locations.Count == 0);
            Assert.IsTrue(searchCriteria.AcademicLevels.Count == 0);
            Assert.IsNotNull(searchCriteria.StartDate);
            Assert.IsNotNull(searchCriteria.EndDate);
            Assert.IsTrue(searchCriteria.StartDate=="2/2/2016");
            Assert.IsTrue(searchCriteria.EndDate=="2/2/2017");
            Assert.IsTrue(searchCriteria.Days.Count == 0);
            Assert.IsNull(searchCriteria.StartTime);
            Assert.IsNull(searchCriteria.EndTime);
            Assert.IsTrue(searchCriteria.KeywordComponents.Count == 0);
        }
        [TestMethod]
        public void serialize_search_criteria_model_to_json_string()
        {
            string model = @"{""term"":""2017/SP"",""location"":""MC"",""academicLevel"":""UG"",""startDate"":"""",""endDate"":"""",""days"":[""1"",""2""],""timeRange"":{""Item1"":""Morning (8am - Midday)"",""Item2"":480,""Item3"":720},""keywordComponents"":[{""subject"":""ACCT"",""courseNumber"":"""",""section"":""""},{""subject"":""ART"",""courseNumber"":""200"",""section"":""01""}],""isValid"":true}";

            string jsonDataShouldBeResult=@"{""subjects"":[],""academicLevels"":[""UG""],""courseLevels"":[],""courseTypes"":[],""topicCodes"":[],""terms"":[""2017/SP""],""days"":[""1"",""2""],""locations"":[""MC""],""faculty"":[],""startDate"":null,""endDate"":null,""startTime"":480,""endTime"":720,""keyword"":null,""requirement"":null,""subrequirement"":null,""group"":null,""courseIds"":null,""sectionIds"":null,""requirementText"":null,""subRequirementText"":null,""onlineCategories"":null,""pageNumber"":1,""quantityPerPage"":30,""openSections"":null,""keywordComponents"":[{""subject"":""ACCT"",""courseNumber"":"""",""section"":""""},{""subject"":""ART"",""courseNumber"":""200"",""section"":""01""}]}";
            CatalogSearchCriteriaModel searchCriteria = helper.RetrieveCatalogSearchCriteriaFromAdvance(model);
            string jsonData = JsonConvert.SerializeObject(searchCriteria, new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
            });
            Assert.IsNotNull(jsonData);
            Assert.AreEqual(jsonData, jsonDataShouldBeResult);
        }
    }
}
