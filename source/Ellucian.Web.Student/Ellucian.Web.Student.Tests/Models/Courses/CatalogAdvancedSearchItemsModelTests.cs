﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Models.Courses;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Models.Courses
{
    [TestClass]
    public class CatalogAdvancedSearchItemsModelTests
    {
       protected CatalogAdvancedSearchItemsModel itemsModel;

        [TestInitialize]
        public void Initialize()
        {
            
        }

        [TestCleanup]
        public void Cleanup()
        {
            itemsModel = null;
        }

        
    }
    [TestClass]
    public class CatalogSearchItems_default_constructor_tests : CatalogAdvancedSearchItemsModelTests
    {
        [TestInitialize]
        public new void Initialize()
        {
            itemsModel = new CatalogAdvancedSearchItemsModel();
            
        }

        [TestCleanup]
        public new void Cleanup()
        {
            itemsModel = null;
        }

        [TestMethod]
        public void InitialValues_after_constructor()
        {
            Assert.IsNotNull(itemsModel.Subjects);
            Assert.IsNotNull(itemsModel.Terms);
            Assert.IsNotNull(itemsModel.Locations);
            Assert.IsNotNull(itemsModel.AcademicLevels);
            Assert.IsNotNull(itemsModel.TimeRanges);
            Assert.IsNotNull(itemsModel.DaysOfWeek);
            Assert.IsNull(itemsModel.EarliestSearchDate);
            Assert.IsNull(itemsModel.LatestSearchDate);
            Assert.IsTrue(itemsModel.Subjects.Count == 0);
            Assert.IsTrue(itemsModel.Terms.Count == 0);
            Assert.IsTrue(itemsModel.Locations.Count == 0);
            Assert.IsTrue(itemsModel.AcademicLevels.Count == 0);
            Assert.IsTrue(itemsModel.TimeRanges.Count == 0);
            Assert.IsTrue(itemsModel.DaysOfWeek.Count == 0);
        }
    }
    [TestClass]
    public class CatalogSearchItems_parameter_constructor_tests : CatalogAdvancedSearchItemsModelTests
    {
        List<Ellucian.Colleague.Dtos.Student.Subject> subjects;
        List<Ellucian.Colleague.Dtos.Student.Term> terms;
        List<Ellucian.Colleague.Dtos.Base.Location> locations;
            List<Ellucian.Colleague.Dtos.Student.AcademicLevel> academicLevels;
            List<Tuple<string, int, int>> timeRanges = new List<Tuple<string, int, int>>();
            List<Tuple<string, string, bool>> daysOfWeek = new List<Tuple<string, string, bool>>();

        Ellucian.Colleague.Dtos.Student.CourseCatalogConfiguration catalogConfiguration ;

        [TestInitialize]
        public new  void  Initialize()
        {
            subjects = new List<Colleague.Dtos.Student.Subject>()
            {
                new Colleague.Dtos.Student.Subject(){ Code="ENGL",Description="english",ShowInCourseSearch=true},
                new Colleague.Dtos.Student.Subject(){ Code="MATH",Description="mathematics",ShowInCourseSearch=true},
                new Colleague.Dtos.Student.Subject(){ Code="ACCTS",Description="accounting",ShowInCourseSearch=true},
                new Colleague.Dtos.Student.Subject(){ Code="CHEM",Description="chemistry",ShowInCourseSearch=true}
            };

            terms = new List<Ellucian.Colleague.Dtos.Student.Term>()
            {
                new Ellucian.Colleague.Dtos.Student.Term(){ Code="2016/FA", Description="2016 fall", IsActive=true},
                new Ellucian.Colleague.Dtos.Student.Term(){ Code="2017/WI", Description="2017 winter", IsActive=true},
                new Ellucian.Colleague.Dtos.Student.Term(){ Code="2017/SU", Description="2017 summer", IsActive=true},
            };

            locations = new List<Ellucian.Colleague.Dtos.Base.Location>()
            {
                new Ellucian.Colleague.Dtos.Base.Location(){ Code="LAB1", Description="LA University - building 1"},
                 new Ellucian.Colleague.Dtos.Base.Location(){ Code="LAB2", Description="LA University - building 2"},
                  new Ellucian.Colleague.Dtos.Base.Location(){ Code="MCU", Description="miller chester university"},
                   new Ellucian.Colleague.Dtos.Base.Location(){ Code="HOME", Description="will come to your home to teach"}
            };

            academicLevels = new List<Ellucian.Colleague.Dtos.Student.AcademicLevel>()
            {
                new Ellucian.Colleague.Dtos.Student.AcademicLevel (){ Code="UG",Description="Undergraduate" },
                 new Ellucian.Colleague.Dtos.Student.AcademicLevel (){ Code="GR",Description="Graduate" }
            };

            timeRanges.Add(new Tuple<string, int, int>("TimeFilterEarlyMorning", 0, 480));
            timeRanges.Add(new Tuple<string, int, int>("TimeFilterMorning", 480, 720));
            timeRanges.Add(new Tuple<string, int, int>("TimeFilterAfternoon", 720, 960));
            timeRanges.Add(new Tuple<string, int, int>("TimeFilterEvening", 960, 1200));
            timeRanges.Add(new Tuple<string, int, int>("TimeFilterNight", 1200, 1440));
          
            daysOfWeek.Add(new Tuple<string, string, bool>("Sunday", "0", false));
            daysOfWeek.Add(new Tuple<string, string, bool>("Monday", "1", false));
            daysOfWeek.Add(new Tuple<string, string, bool>("Tuesday", "2", false));
            daysOfWeek.Add(new Tuple<string, string, bool>("Wednesday", "3", false));
            daysOfWeek.Add(new Tuple<string, string, bool>("Thursday", "4", false));
            daysOfWeek.Add(new Tuple<string, string, bool>("Friday", "5", false));
            daysOfWeek.Add(new Tuple<string, string, bool>("Saturday", "6", false));

            catalogConfiguration = new Colleague.Dtos.Student.CourseCatalogConfiguration();
            catalogConfiguration.EarliestSearchDate = DateTime.Now.AddDays(-2);
            catalogConfiguration.LatestSearchDate = DateTime.Now;
            catalogConfiguration.CatalogFilterOptions = new List<Colleague.Dtos.Student.CatalogFilterOption>();
            catalogConfiguration.CatalogFilterOptions.Add(new Colleague.Dtos.Student.CatalogFilterOption() { Type = Colleague.Dtos.Student.CatalogFilterType.AcademicLevels, IsHidden = false });
            catalogConfiguration.CatalogFilterOptions.Add(new Colleague.Dtos.Student.CatalogFilterOption() { Type = Colleague.Dtos.Student.CatalogFilterType.CourseLevels, IsHidden = false });

            itemsModel = new CatalogAdvancedSearchItemsModel(subjects, terms, locations, academicLevels,null, timeRanges, daysOfWeek, catalogConfiguration);

        }

        [TestCleanup]
        public new void Cleanup()
        {
            itemsModel = null;
        }

        [TestMethod]
        public void Initial_objects_after_parameterized_constructor()
        {
            Assert.IsNotNull(itemsModel.Subjects);
            Assert.AreEqual(itemsModel.TimeRanges,timeRanges);
            Assert.IsNotNull(itemsModel.Locations);
            Assert.IsNotNull(itemsModel.AcademicLevels);
            Assert.IsNotNull(itemsModel.TimeRanges);
            Assert.IsNotNull(itemsModel.DaysOfWeek);
            Assert.IsNotNull(itemsModel.EarliestSearchDate);
            Assert.IsNotNull(itemsModel.LatestSearchDate);
            Assert.IsTrue(itemsModel.Subjects.Count == 4);
            Assert.IsTrue(itemsModel.Terms.Count == 3);
            Assert.IsTrue(itemsModel.Locations.Count == 4);
            Assert.IsTrue(itemsModel.AcademicLevels.Count == 2);
            Assert.IsTrue(itemsModel.TimeRanges.Count == 5);
            Assert.IsTrue(itemsModel.DaysOfWeek.Count == 7);
        }

        [TestMethod]
        public void parameters_are_null()
        {
            subjects = null;
            terms = null;
            academicLevels = null;
            locations = null;
            timeRanges = null;
            daysOfWeek = null;
            catalogConfiguration = null;
            itemsModel = new CatalogAdvancedSearchItemsModel(subjects, terms, locations, academicLevels,null, timeRanges, daysOfWeek, catalogConfiguration);

            Assert.IsNotNull(itemsModel.Subjects);
            Assert.IsNotNull(itemsModel.Terms);
            Assert.IsNotNull(itemsModel.Locations);
            Assert.IsNotNull(itemsModel.AcademicLevels);
            Assert.IsNotNull(itemsModel.TimeRanges);
            Assert.IsNotNull(itemsModel.DaysOfWeek);
            Assert.IsNull(itemsModel.EarliestSearchDate);
            Assert.IsNull(itemsModel.LatestSearchDate);
            Assert.IsTrue(itemsModel.Subjects.Count == 0);
            Assert.IsTrue(itemsModel.Terms.Count == 0);
            Assert.IsTrue(itemsModel.Locations.Count == 0);
            Assert.IsTrue(itemsModel.AcademicLevels.Count == 0);
            Assert.IsTrue(itemsModel.TimeRanges.Count == 0);
            Assert.IsTrue(itemsModel.DaysOfWeek.Count == 0);
        }

        [TestMethod]
        public void parameters_are_empty_lists()
        {
            subjects = new List<Colleague.Dtos.Student.Subject>();
            terms = new List<Colleague.Dtos.Student.Term>();
            academicLevels = new List<Colleague.Dtos.Student.AcademicLevel>();
            locations = new List<Colleague.Dtos.Base.Location>();
            timeRanges = new List<Tuple<string,int,int>>();
            daysOfWeek = new List<Tuple<string,string,bool>>();
            catalogConfiguration = new Colleague.Dtos.Student.CourseCatalogConfiguration();
            itemsModel = new CatalogAdvancedSearchItemsModel(subjects, terms, locations, academicLevels, null,timeRanges, daysOfWeek, catalogConfiguration);

            Assert.IsNotNull(itemsModel.Subjects);
            Assert.IsNotNull(itemsModel.Terms);
            Assert.IsNotNull(itemsModel.Locations);
            Assert.IsNotNull(itemsModel.AcademicLevels);
            Assert.IsNotNull(itemsModel.TimeRanges);
            Assert.IsNotNull(itemsModel.DaysOfWeek);
            Assert.IsNull(itemsModel.EarliestSearchDate);
            Assert.IsNull(itemsModel.LatestSearchDate);
            Assert.IsTrue(itemsModel.Subjects.Count == 0);
            Assert.IsTrue(itemsModel.Terms.Count == 0);
            Assert.IsTrue(itemsModel.Locations.Count == 0);
            Assert.IsTrue(itemsModel.AcademicLevels.Count == 0);
            Assert.IsTrue(itemsModel.TimeRanges.Count == 0);
            Assert.IsTrue(itemsModel.DaysOfWeek.Count == 0);
        }

        [TestMethod]
        public void values_after_parameterized_constructor()
        {
            Assert.AreEqual(itemsModel.Subjects,subjects);
            Assert.AreEqual(itemsModel.TimeRanges, timeRanges);
            Assert.AreEqual(itemsModel.DaysOfWeek, daysOfWeek);
            Assert.AreEqual(itemsModel.EarliestSearchDate, catalogConfiguration.EarliestSearchDate);
            Assert.AreEqual(itemsModel.LatestSearchDate, catalogConfiguration.LatestSearchDate);
            Assert.AreEqual(itemsModel.Terms[0].Item1, terms[0].Code);
            Assert.AreEqual(itemsModel.Terms[0].Item2, terms[0].Description);
            Assert.AreEqual(itemsModel.Terms[1].Item1, terms[1].Code);
            Assert.AreEqual(itemsModel.Terms[1].Item2, terms[1].Description);
            Assert.AreEqual(itemsModel.Terms[2].Item1, terms[2].Code);
            Assert.AreEqual(itemsModel.Terms[2].Item2, terms[2].Description);

            Assert.AreEqual(itemsModel.Locations[0].Item1, locations[0].Code);
            Assert.AreEqual(itemsModel.Locations[0].Item2, locations[0].Description);
            Assert.AreEqual(itemsModel.Locations[1].Item1, locations[1].Code);
            Assert.AreEqual(itemsModel.Locations[1].Item2, locations[1].Description);
            Assert.AreEqual(itemsModel.Locations[2].Item1, locations[2].Code);
            Assert.AreEqual(itemsModel.Locations[2].Item2, locations[2].Description);
            Assert.AreEqual(itemsModel.Locations[3].Item1, locations[3].Code);
            Assert.AreEqual(itemsModel.Locations[3].Item2, locations[3].Description);

            Assert.AreEqual(itemsModel.AcademicLevels[0].Item1, academicLevels[0].Code);
            Assert.AreEqual(itemsModel.AcademicLevels[0].Item2, academicLevels[0].Description);
            Assert.AreEqual(itemsModel.AcademicLevels[1].Item1, academicLevels[1].Code);
            Assert.AreEqual(itemsModel.AcademicLevels[1].Item2, academicLevels[1].Description);

        }
    }
}
