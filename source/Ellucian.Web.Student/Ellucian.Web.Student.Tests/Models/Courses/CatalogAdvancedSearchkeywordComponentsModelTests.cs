﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Models.Courses;
using Newtonsoft.Json;

namespace Ellucian.Web.Student.Tests.Models.Courses
{
    [TestClass]
    public class CatalogAdvancedSearchkeywordComponentsModelTests
    {
        [TestMethod]
        public void default_constructor()
        {
            CatalogAdvancedSearchKeywordComponentsModel obj = new CatalogAdvancedSearchKeywordComponentsModel();
            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Subject);
            Assert.IsNull(obj.CourseNumber);
            Assert.IsNull(obj.Section);
            Assert.IsTrue(obj.IsEmpty());
        }
        [TestMethod]
        public void parameterized_constructor()
        {
            CatalogAdvancedSearchKeywordComponentsModel obj = new CatalogAdvancedSearchKeywordComponentsModel("ART","200","1");
            Assert.IsNotNull(obj);
            Assert.IsTrue(obj.Subject=="ART");
            Assert.AreEqual(obj.CourseNumber,"200");
            Assert.AreEqual(obj.Section, "1");
            Assert.IsFalse(obj.IsEmpty());
        }
        [TestMethod]
        public void parameterized_constructor_with_no_section()
        {
            CatalogAdvancedSearchKeywordComponentsModel obj = new CatalogAdvancedSearchKeywordComponentsModel("ART", "200",null);
            Assert.IsNotNull(obj);
            Assert.IsTrue(obj.Subject == "ART");
            Assert.AreEqual(obj.CourseNumber, "200");
            Assert.IsNull(obj.Section);
            Assert.IsFalse(obj.IsEmpty());
        }
        [TestMethod]
        public void test_jsonproperty_serialize_model()
        {
            string result=@"{""subject"":""ART"",""courseNumber"":""200"",""section"":""1""}";
            CatalogAdvancedSearchKeywordComponentsModel obj = new CatalogAdvancedSearchKeywordComponentsModel("ART", "200", "1");
            string jsonString=JsonConvert.SerializeObject(obj);
            Assert.AreEqual(jsonString, result);
        }
        [TestMethod]
        public void test_jsonproperty_deSerialize_model()
        {
            string jsonString = @"{""subject"":""ART"",""courseNumber"":""200"",""section"":""1""}";
            CatalogAdvancedSearchKeywordComponentsModel obj = JsonConvert.DeserializeObject<CatalogAdvancedSearchKeywordComponentsModel>(jsonString);
            Assert.IsNotNull(obj);
            Assert.IsTrue(obj.Subject == "ART");
            Assert.AreEqual(obj.CourseNumber, "200");
            Assert.AreEqual(obj.Section, "1");
            Assert.IsFalse(obj.IsEmpty());
        }
    }
}

