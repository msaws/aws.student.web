﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Models
{
    [TestClass]
    public class PageHeaderModelTests
    {
        private string title;
        private string subtitle;
        private string staticBackLinkDestination;
        private string staticBackLinkText;

        [TestInitialize]
        public void Initialize()
        {
            title = "Page Title";
            subtitle = "Page Subtitle";
            staticBackLinkDestination = "http://static.backlink.destination";
            staticBackLinkText = "Static Back Link Text";
        }

        [TestCleanup]
        public void TestCleanup()
        {
            title = null;
            subtitle = null;
            staticBackLinkDestination = null;
            staticBackLinkText = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PageHeaderModel_Constructor_Title_Null()
        {
            var model = new PageHeaderModel(null, subtitle, staticBackLinkDestination, staticBackLinkText);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PageHeaderModel_Constructor_Subtitle_with_StaticBackLinkDestination()
        {
            var model = new PageHeaderModel(title, subtitle, staticBackLinkDestination, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PageHeaderModel_Constructor_Subtitle_with_StaticBackLinkText()
        {
            var model = new PageHeaderModel(title, subtitle, null, staticBackLinkText);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PageHeaderModel_Constructor_Subtitle_with_DynamicBackLinkConfiguration()
        {
            var model = new PageHeaderModel(title, subtitle, null, null, new DynamicBackLinkConfiguration());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PageHeaderModel_Constructor_StaticBackLinkDestination_with_DynamicBackLinkConfiguration()
        {
            var model = new PageHeaderModel(title, null, staticBackLinkDestination, null, new DynamicBackLinkConfiguration());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PageHeaderModel_Constructor_StaticBackLinkText_with_DynamicBackLinkConfiguration()
        {
            var model = new PageHeaderModel(title, null, null, staticBackLinkText, new DynamicBackLinkConfiguration());
        }

        [TestMethod]
        public void PageHeaderModel_Constructor_Valid_Subtitle()
        {
            var model = new PageHeaderModel(title, subtitle, null, null);
            Assert.AreEqual(title, model.Title);
            Assert.AreEqual(subtitle, model.Subtitle);
            Assert.IsNull(model.StaticBacklinkDestination);
            Assert.IsNull(model.StaticBacklinkText);
            Assert.IsNull(model.DynamicBackLinkConfiguration);
        }

        [TestMethod]
        public void PageHeaderModel_Constructor_Valid_StaticBackLinkDestination_with_StaticBackLinkText()
        {
            var model = new PageHeaderModel(title, null, staticBackLinkDestination, staticBackLinkText);
            Assert.AreEqual(title, model.Title);
            Assert.IsNull(model.Subtitle);
            Assert.AreEqual(staticBackLinkDestination, model.StaticBacklinkDestination);
            Assert.AreEqual(staticBackLinkText, model.StaticBacklinkText);
            Assert.IsNull(model.DynamicBackLinkConfiguration);
        }

        [TestMethod]
        public void PageHeaderModel_Constructor_Valid_StaticBackLinkDestination_without_StaticBackLinkText()
        {
            var model = new PageHeaderModel(title, null, staticBackLinkDestination, null);
            Assert.AreEqual(title, model.Title);
            Assert.IsNull(model.Subtitle);
            Assert.AreEqual(staticBackLinkDestination, model.StaticBacklinkDestination);
            Assert.IsNull(model.StaticBacklinkText);
            Assert.IsNull(model.DynamicBackLinkConfiguration);
        }

        [TestMethod]
        public void PageHeaderModel_Constructor_Valid_DynamicBackLinkConfiguration()
        {
            var model = new PageHeaderModel(title, null, null, null, new DynamicBackLinkConfiguration());
            Assert.AreEqual(title, model.Title);
            Assert.IsNull(model.Subtitle);
            Assert.IsNull(model.StaticBacklinkDestination);
            Assert.IsNull(model.StaticBacklinkText);
            Assert.IsNotNull(model.DynamicBackLinkConfiguration);
        }
    }
}
