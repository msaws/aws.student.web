﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Models.UserProfile;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Tests.Models
{
    [TestClass]
    public class UserProfileModelTests
    {
        Profile expectedProfileDto = null;

        [TestInitialize]
        public void TestInitialize()
        {
            var expectedProfileAddress1 = new Address
            {
                AddressId = "1",
                AddressLabel = new List<string> 
                    { 
                        "123 Main St",
                        "Fairfax, VA 22033"
                    },
                IsPreferredAddress = true,
                Type = "Home",
                TypeCode = "H"
            };

            var expectedProfileAddress2 = new Address
            {
                AddressId = "2",
                AddressLabel = new List<string>
                    {
                        "Care of Ellucian",
                        "30 Queen's Gate Mews",
                        "London SW7 5QL", 
                        "UNITED KINGDOM" 
                    },
                IsPreferredAddress = false,
                Type = "Business",
                TypeCode = "B"
            };
            var expectedProfileAddress3 = new Address
            {
                AddressId = "3",
                AddressLabel = new List<string>
                    {
                        "43500 Fair Lakes Court",
                        "Fairfax, VA 22033"
                    },
                IsPreferredAddress = false,
                Type = "Business",
                TypeCode = "X"
            };
            var dtoAddresses = new List<Address>()
            {
                expectedProfileAddress1,
                expectedProfileAddress2,
                expectedProfileAddress3
            };

            expectedProfileDto = new Profile()
            {
                Id = "0000123",
                LastName = "Smith",
                FirstName = "Barney",
                PreferredName = "Barney L. Smith II",
                PreferredEmailAddress = "barneysmith@xyzsmithbarney.com",
                BirthDate = new DateTime(1963, 01, 28),
                Addresses = dtoAddresses,
                EmailAddresses = new List<EmailAddress>()
                {
                    new EmailAddress { IsPreferred = true, TypeCode = "SEC", Value = "a@b.c" },
                    new EmailAddress { IsPreferred = false, TypeCode = "PRI", Value = "d@e.f" },
                    new EmailAddress { IsPreferred = false, TypeCode = "AAA", Value = "g@h.i" },
                    new EmailAddress { IsPreferred = false, TypeCode = "XX", Value = "xx@y.z"}
                },
                Phones = new List<Phone>()
                {
                    new Phone { TypeCode = "HO", Number = "111-111-1111" },
                    new Phone { TypeCode = "BU", Number = "222-222-2222", Extension = "12345" },
                    new Phone { TypeCode = "AA", Number = "333-333-3333" } ,
                    new Phone { TypeCode = "ZZ", Number = "864-444-4444" }
                },
                AddressConfirmationDateTime = new DateTimeOffset(2015, 12, 13, 14, 15, 16, TimeSpan.Zero),
                EmailAddressConfirmationDateTime = new DateTimeOffset(2015, 6, 7, 8, 9, 10, TimeSpan.Zero),
                PhoneConfirmationDateTime = new DateTimeOffset(2015, 1, 2, 3, 4, 5, TimeSpan.Zero),
                PersonalPronounCode = "HE"
            };
        }

        [TestMethod]
        public void UserProfileModel_Created()
        {
            var actualUserProfileModel = new UserProfileModel(expectedProfileDto);

            Assert.AreEqual(expectedProfileDto.Id, actualUserProfileModel.Id);
            Assert.AreEqual(expectedProfileDto.BirthDate.Value.ToShortDateString(), actualUserProfileModel.DateOfBirth);
            Assert.AreEqual(expectedProfileDto.FirstName + " " + expectedProfileDto.LastName, actualUserProfileModel.FullName);
            Assert.AreEqual(expectedProfileDto.AddressConfirmationDateTime, actualUserProfileModel.AddressConfirmationDateTime);
            Assert.AreEqual(expectedProfileDto.EmailAddressConfirmationDateTime, actualUserProfileModel.EmailAddressConfirmationDateTime);
            Assert.AreEqual(expectedProfileDto.PhoneConfirmationDateTime, actualUserProfileModel.PhoneConfirmationDateTime);
            Assert.AreEqual(expectedProfileDto.AddressConfirmationDateTime.Value.LocalDateTime.ToShortDateString(), actualUserProfileModel.AddressConfirmationDate);
            Assert.AreEqual(expectedProfileDto.EmailAddressConfirmationDateTime.Value.LocalDateTime.ToShortDateString(), actualUserProfileModel.EmailAddressConfirmationDate);
            Assert.AreEqual(expectedProfileDto.PhoneConfirmationDateTime.Value.LocalDateTime.ToShortDateString(), actualUserProfileModel.PhoneConfirmationDate);
            Assert.AreEqual(expectedProfileDto.LastChangedDateTime, actualUserProfileModel.LastChangedDateTime);
            Assert.AreEqual(expectedProfileDto.PreferredEmailAddress, actualUserProfileModel.PreferredEmailAddress);
            Assert.AreEqual(expectedProfileDto.PersonalPronounCode, actualUserProfileModel.PersonalPronounCode);

            CollectionAssert.AreEqual(expectedProfileDto.Addresses, actualUserProfileModel.Addresses);
            CollectionAssert.AreEqual(expectedProfileDto.EmailAddresses, actualUserProfileModel.EmailAddresses);
            CollectionAssert.AreEqual(expectedProfileDto.Phones, actualUserProfileModel.Phones);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ThrowsExceptionIfProfileEmpty()
        {
            expectedProfileDto = null;
            var actualUserProfileModel = new UserProfileModel(expectedProfileDto);
        }

    }
}
