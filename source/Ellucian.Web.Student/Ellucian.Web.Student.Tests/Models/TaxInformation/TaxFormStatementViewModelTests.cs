﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Models.TaxInformation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.HumanResources;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class TaxFormStatementViewModelTests
    {
        #region Initialize and Cleanup
        public TaxFormStatement2 TaxFormStatementDto;
        public TaxFormStatementsViewModel TaxFormStatementVM;

        [TestInitialize]
        public void Initialize()
        {
            TaxFormStatementDto = new TaxFormStatement2()
            {
                PersonId = "00000001",
                TaxYear = "2012",
                TaxForm = TaxForms.FormW2
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            TaxFormStatementDto = null;
            TaxFormStatementVM = null;
        }
        #endregion

        [TestMethod]
        public void TaxFormStatementViewModel()
        {
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(TaxFormStatementDto.PersonId, TaxFormStatementVM.PersonId);
            Assert.AreEqual(TaxFormStatementDto.TaxForm, TaxFormStatementVM.TaxForm);
            Assert.AreEqual(TaxFormStatementDto.TaxYear, TaxFormStatementVM.TaxYear);
            Assert.AreEqual(true, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_W2NullNotationStatement()
        {
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            string formLabel = GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "FormW2Label");
            string statementLabel = GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "StatementText");
            Assert.AreEqual(TaxFormStatementDto.TaxYear + " " + formLabel + " " + statementLabel, TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(true, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_W2NoneStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.None;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            string formLabel = GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "FormW2Label");
            string statementLabel = GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "StatementText");
            Assert.AreEqual(TaxFormStatementDto.TaxYear + " " + formLabel + " " + statementLabel, TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(true, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_1095CNoneStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.None;
            TaxFormStatementDto.TaxForm = TaxForms.Form1095C;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            string formLabel = GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "Form1095CLabel");
            string statementLabel = GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "StatementText");
            Assert.AreEqual(TaxFormStatementDto.TaxYear + " " + formLabel + " " + statementLabel, TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(true, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_W2NotAvailableStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.NotAvailable;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "W2NotAvailableText"), TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(false, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_1095CNotAvailableStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.NotAvailable;
            TaxFormStatementDto.TaxForm = TaxForms.Form1095C;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "Form1095CNotAvailableText"), TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(false, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_W2CorrectionStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.Correction;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "W2CorrectionText"), TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(false, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_1095CCorrectionStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.Correction;
            TaxFormStatementDto.TaxForm = TaxForms.Form1095C;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "Form1095CCorrectionText"), TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(false, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_W2MultipleFormsStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.MultipleForms;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "W2MultipleFormsText"), TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(true, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_W2IsOverflowStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.IsOverflow;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "W2MultipleFormsText"), TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(true, TaxFormStatementVM.IsLinkable);
        }

        [TestMethod]
        public void TaxFormStatementViewModel_TaxFormDisplay_W2HasOverflowDataStatement()
        {
            TaxFormStatementDto.Notation = TaxFormNotations2.HasOverflowData;
            TaxFormStatementVM = new TaxFormStatementsViewModel(TaxFormStatementDto);
            Assert.AreEqual(GlobalResources.GetString(HumanResourcesResourceFiles.TaxFormsResources, "W2MultipleFormsText"), TaxFormStatementVM.NotationDisplay);
            Assert.AreEqual(true, TaxFormStatementVM.IsLinkable);
        }
    }
}
