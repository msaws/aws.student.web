﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Models.TaxInformation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class TaxInformationViewModelTests
    {
        #region Initialize and Cleanup
        public List<TaxFormConsent> taxFormConsentDtos;
        public TaxFormConfiguration taxFormConfigurationDto;
        public List<TaxFormConsentsViewModel> sortedTaxFormConsentsViewModels;
        public List<TaxFormStatementsViewModel> sortedW2StatementViewModels;
        public TaxFormConsent consentDto;
        public TaxInformationViewModel taxInformationVM;

        [TestInitialize]
        public void Initialize()
        {
            taxFormConsentDtos = null;
            taxFormConfigurationDto = null;
            sortedTaxFormConsentsViewModels = null;
            sortedW2StatementViewModels = null;
            consentDto = null;
            taxInformationVM = null;
        }

        [TestCleanup]
        public void Cleanup()
        {
            taxFormConsentDtos = null;
            taxFormConfigurationDto = null;
            sortedTaxFormConsentsViewModels = null;
            sortedW2StatementViewModels = null;
            consentDto = null;
            taxInformationVM = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void TaxFormConsentsViewModel_Constructor_Base()
        {
            #region Populate DTO data
            taxFormConsentDtos = new List<TaxFormConsent>();
            consentDto = new TaxFormConsent();
            consentDto.PersonId = "0001234";
            consentDto.TaxForm = TaxForms.FormW2;
            consentDto.HasConsented = false;
            consentDto.TimeStamp = new DateTimeOffset(new DateTime(2015,01,01));
            taxFormConsentDtos.Add(consentDto);
            consentDto = null;
            consentDto = new TaxFormConsent();
            consentDto.PersonId = "0001234";
            consentDto.TaxForm = TaxForms.FormW2;
            consentDto.HasConsented = true;
            consentDto.TimeStamp = new DateTimeOffset(new DateTime(2015, 05, 01));
            taxFormConsentDtos.Add(consentDto);
            consentDto = null;
            consentDto = new TaxFormConsent();
            consentDto.PersonId = "0001234";
            consentDto.TaxForm = TaxForms.Form1095C;
            consentDto.HasConsented = true;
            consentDto.TimeStamp = new DateTimeOffset(new DateTime(2015, 05, 01));
            taxFormConsentDtos.Add(consentDto);

            var w2StatementDtos = new List<TaxFormStatement2>()
            {
                new TaxFormStatement2() { PersonId = "0000001", TaxForm = TaxForms.FormW2, TaxYear = "2011" },
                new TaxFormStatement2() { PersonId = "0000001", TaxForm = TaxForms.FormW2, TaxYear = "2012" },
                new TaxFormStatement2() { PersonId = "0000001", TaxForm = TaxForms.FormW2, TaxYear = "2013" },
                new TaxFormStatement2() { PersonId = "0000001", TaxForm = TaxForms.FormW2, TaxYear = "2014" },
            };

            sortedTaxFormConsentsViewModels = taxFormConsentDtos.Where(t => t.TaxForm == TaxForms.FormW2).OrderByDescending(t => t.TimeStamp)
                .Select(t => new TaxFormConsentsViewModel(t)).ToList();

            sortedW2StatementViewModels = w2StatementDtos.OrderByDescending(t => t.TaxYear).Select(t => new TaxFormStatementsViewModel(t)).ToList();

            taxFormConfigurationDto = new TaxFormConfiguration();
            taxFormConfigurationDto.ConsentText = "You have consented.";
            taxFormConfigurationDto.ConsentWithheldText = "You have not consented.";
            #endregion

            taxInformationVM = new TaxInformationViewModel(taxFormConsentDtos, w2StatementDtos, taxFormConfigurationDto, TaxForms.FormW2);

            // Verify that TaxInformationViewModel initialized the appropriate variables

            // Verify that the W-2 form data has been initialized
            Assert.AreEqual(sortedW2StatementViewModels.Count, taxInformationVM.Statements.Count);
            foreach (var viewModel in sortedW2StatementViewModels)
            {
                var taxInformationVMw2Statement = taxInformationVM.Statements.Where(w =>
                    w.PersonId == viewModel.PersonId
                    && w.TaxForm == viewModel.TaxForm
                    && w.TaxYear == viewModel.TaxYear).ToList();
                Assert.AreEqual(1, taxInformationVMw2Statement.Count);
            }

            // Verify that the W-2 form consent data has been initialized
            Assert.AreEqual(sortedTaxFormConsentsViewModels.Count, taxInformationVM.Consents.Count);
            foreach (var consentVM in sortedTaxFormConsentsViewModels)
            {
                var taxInformationVMw2Consent = taxInformationVM.Consents.Where(y =>
                    y.HasConsented == consentVM.HasConsented
                    && y.DisplayDate == consentVM.DisplayDate
                    && y.DisplayTime == consentVM.DisplayTime);
                Assert.AreEqual(1, taxInformationVMw2Consent.Count());
            }
        }

        [TestMethod]
        public void IsW2()
        {
            taxInformationVM = new TaxInformationViewModel(new List<TaxFormConsent>(), new List<TaxFormStatement2>(), new TaxFormConfiguration(), TaxForms.FormW2);
            Assert.IsTrue(taxInformationVM.IsW2);
            Assert.IsFalse(taxInformationVM.Is1095C);
            Assert.IsFalse(taxInformationVM.Is1098);
        }

        [TestMethod]
        public void Is1095C()
        {
            taxInformationVM = new TaxInformationViewModel(new List<TaxFormConsent>(), new List<TaxFormStatement2>(), new TaxFormConfiguration(), TaxForms.Form1095C);
            Assert.IsTrue(taxInformationVM.Is1095C);
            Assert.IsFalse(taxInformationVM.IsW2);
            Assert.IsFalse(taxInformationVM.Is1098);
        }

        [TestMethod]
        public void Is1098()
        {
            taxInformationVM = new TaxInformationViewModel(new List<TaxFormConsent>(), new List<TaxFormStatement2>(), new TaxFormConfiguration(), TaxForms.Form1098);
            Assert.IsTrue(taxInformationVM.Is1098);
            Assert.IsFalse(taxInformationVM.IsW2);
            Assert.IsFalse(taxInformationVM.Is1095C);
        }
        #endregion
    }
}