﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Models.TaxInformation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class TaxFormConsentsViewModelTests
    {
        #region Initialize and Cleanup
        public TaxFormConsent taxFormConsentDto;
        public TaxFormConsentsViewModel taxFormConsentsVM;

        [TestInitialize]
        public void Initialize()
        {
        }

        [TestCleanup]
        public void Cleanup()
        {
            taxFormConsentDto = null;
            taxFormConsentsVM = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void TaxFormConsentsViewModel_Constructor_Base()
        {
            taxFormConsentDto = new TaxFormConsent();
            taxFormConsentDto.PersonId = "0001234";
            taxFormConsentDto.TaxForm = TaxForms.FormW2;
            taxFormConsentDto.HasConsented = true;
            taxFormConsentDto.TimeStamp = new DateTimeOffset(DateTime.Now);

            taxFormConsentsVM = new TaxFormConsentsViewModel(taxFormConsentDto);

            // Verify that TaxFormConsentsViewModel initialized the appropriate variables
            Assert.AreEqual(taxFormConsentDto.HasConsented, taxFormConsentsVM.HasConsented);
            Assert.AreEqual(taxFormConsentDto.TimeStamp.LocalDateTime.ToShortDateString(), taxFormConsentsVM.DisplayDate);
            Assert.AreEqual(taxFormConsentDto.TimeStamp.LocalDateTime.ToLongTimeString(), taxFormConsentsVM.DisplayTime);

        }
        #endregion

    }
}
