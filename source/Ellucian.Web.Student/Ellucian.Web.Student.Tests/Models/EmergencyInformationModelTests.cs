﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using System;
using Ellucian.Web.Student.Models.UserProfile;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Models
{
    [TestClass]
    public class EmergencyInformationModelConstructorTests
    {
        EmergencyInformation EmergencyInformationDto = null;
        List<HealthConditions> HealthConditions = null;


        [TestInitialize]
        public void Initialize()
        {
            EmergencyInformationDto = new EmergencyInformation() { 
                PersonId = "1111111", 
                ConfirmedDate = DateTime.Today, 
                HospitalPreference = "Hospital", 
                InsuranceInformation = "Insurance", 
                AdditionalInformation = "Additional Information", 
                HealthConditions = new List<string>() { "A" }, 
                EmergencyContacts = new List<Ellucian.Colleague.Dtos.Base.EmergencyContact>() { new Ellucian.Colleague.Dtos.Base.EmergencyContact() { Name = "Contact 1", Address = "Address", DaytimePhone = "Daytime", EveningPhone = "Evening", OtherPhone = "Other", IsEmergencyContact = true, Relationship = "Relationship", IsMissingPersonContact = false, EffectiveDate = DateTime.Today.AddDays(2)}}
            }; 
            HealthConditions = new List<HealthConditions>() {
                new HealthConditions() { Code = "A", Description = "Asthma"},
                new HealthConditions() { Code = "B", Description = "Bee Allergy"},
                new HealthConditions() { Code = "C", Description = "Chronic Stuff"},
                new HealthConditions() { Code = "D", Description = "Diabetes"}
            };
        }

        [TestMethod]
        public void EmergencyInformationModelConstructor()
        {
            var emergencyInformationModel = new EmergencyInformationModel(EmergencyInformationDto, HealthConditions, null);
            Assert.AreEqual(EmergencyInformationDto.ConfirmedDate.Value.ToShortDateString(), emergencyInformationModel.ConfirmedDate);
            Assert.AreEqual(EmergencyInformationDto.PersonId, emergencyInformationModel.PersonId);
            Assert.AreEqual(EmergencyInformationDto.InsuranceInformation, emergencyInformationModel.InsuranceInformation);
            Assert.AreEqual(EmergencyInformationDto.HospitalPreference, emergencyInformationModel.HospitalPreference);
            Assert.AreEqual(EmergencyInformationDto.AdditionalInformation, emergencyInformationModel.AdditionalInformation);
            Assert.AreEqual(EmergencyInformationDto.EmergencyContacts.Count, emergencyInformationModel.EmergencyContacts.Count);
            foreach (var condition in HealthConditions)
            {
                var modelCondition = emergencyInformationModel.HealthConditions.Where(h => h.Code == condition.Code).FirstOrDefault();
                Assert.IsNotNull(modelCondition);
                if (modelCondition != null)
                {
                    Assert.AreEqual(condition.Code, modelCondition.Code);
                    Assert.AreEqual(condition.Description, modelCondition.Description);
                    if (condition.Code == "A")
                    {
                        Assert.IsTrue(modelCondition.isSelected);
                    }
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void EmergencyInformationModelConstructor_NullInfo()
        {
            var emergencyInformationModel = new EmergencyInformationModel(null, HealthConditions, null);
        }

        [TestMethod]
        public void EmergencyInformationModelConstructor_NullHealthConditions()
        {
            var emergencyInformationModel = new EmergencyInformationModel(EmergencyInformationDto, null, null);
            Assert.AreEqual(0, emergencyInformationModel.HealthConditions.Count);
        }
    }
}
