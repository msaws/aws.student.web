﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Models;

namespace Ellucian.Web.Student.Tests.Models
{
    [TestClass]
    public class StateProvinceCodeTests
    {
        string code = "TX";
        string desc = "Texas";
        string country = "US";

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void StateProvinceCode_Constructor_NullCode()
        {
            var result = new StateProvinceCode(null, desc, country);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void StateProvinceCode_Constructor_EmptyCode()
        {
            var result = new StateProvinceCode(String.Empty, desc, country);
        }

        [TestMethod]
        public void StateProvinceCode_Constructor_ValidCode()
        {
            var result = new StateProvinceCode(code, desc, country);
            Assert.AreEqual(code, result.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void StateProvinceCode_Constructor_NullDescription()
        {
            var result = new StateProvinceCode(code, null, country);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void StateProvinceCode_Constructor_EmptyDescription()
        {
            var result = new StateProvinceCode(code, String.Empty, country);
        }

        [TestMethod]
        public void StateProvinceCode_Constructor_ValidDescription()
        {
            var result = new StateProvinceCode(code, desc, country);
            Assert.AreEqual(desc, result.Description);
        }

        [TestMethod]
        public void StateProvinceCode_Constructor_NullCountry()
        {
            var result = new StateProvinceCode(code, desc, null);
            Assert.AreEqual("US", result.Country);
        }

        [TestMethod]
        public void StateProvinceCode_Constructor_EmptyCountry()
        {
            var result = new StateProvinceCode(code, desc, String.Empty);
            Assert.AreEqual("US", result.Country);
        }

        [TestMethod]
        public void StateProvinceCode_Constructor_ValidCountry()
        {
            var result = new StateProvinceCode(code, desc, country);
            Assert.AreEqual(country, result.Country);
        }

    }
}
