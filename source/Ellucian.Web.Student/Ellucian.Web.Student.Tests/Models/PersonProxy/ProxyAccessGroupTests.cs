﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.PersonProxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ellucian.Web.Student.Tests.Models.PersonProxy
{
    [TestClass]
    public class ProxyAccessGroupTests
    {
        public string id;
        public string name;
        public ProxyAccessGroup model;


        [TestInitialize]
        public void Initialize()
        {
            id = "SF";
            name = "Student Finance";
        }

        [TestCleanup]
        public void Cleanup()
        {
            id = null;
            name = null;
            model = null;
        }

        [TestClass]
        public class ProxyAccessGroupTests_Build : ProxyAccessGroupTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessGroupTests_Build_NullId()
            {
                model = ProxyAccessGroup.Build(null, name);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessGroupTests_Build_EmptyId()
            {
                model = ProxyAccessGroup.Build(string.Empty, name);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessGroupTests_Build_NullName()
            {
                model = ProxyAccessGroup.Build(id, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessGroupTests_Build_EmptyName()
            {
                model = ProxyAccessGroup.Build(id, string.Empty);
            }

            [TestMethod]
            public void ProxyAccessGroupTests_Build_Id()
            {
                model = ProxyAccessGroup.Build(id, name);
                Assert.AreEqual(id, model.Id);
            }

            [TestMethod]
            public void ProxyAccessGroupTests_Build_Name()
            {
                model = ProxyAccessGroup.Build(id, name);
                Assert.AreEqual(name, model.Name);
            }

            [TestMethod]
            public void ProxyAccessGroupTests_Build_Permissions()
            {
                model = ProxyAccessGroup.Build(id, name);
                Assert.IsNotNull(model.Permissions);
                Assert.AreEqual(0, model.Permissions.Count);
            }
        }
    }
}
