﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.PersonProxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Models.PersonProxy
{
    [TestClass]
    public class ProxyUserTests
    {
        public string id;
        public string name;
        public string relationship;
        public string email;
        public string photoUrl;
        private List<Colleague.Dtos.Base.ProxyWorkflowGroup> workflowGroups;
        private List<Colleague.Dtos.Base.ProxyAccessPermission> accessPermissions;
        private List<Colleague.Dtos.Base.ProxyAccessPermission> reauthorizeAccessPermissions;
        public DateTime? effectiveDate;
        public Ellucian.Colleague.Dtos.Base.ProxyConfiguration config;
        private Colleague.Dtos.Base.ProxyCandidate candidate;
        public ProxyUser model;


        [TestInitialize]
        public void Initialize()
        {
            id = "0001234";
            name = "John Smith";
            relationship = "Father";
            email = "john.smith@fakeemail.com";
            photoUrl = "http://www.ellucianuniversity.edu/photos/0001234.png";
            effectiveDate = DateTime.Today;
            workflowGroups = new List<Colleague.Dtos.Base.ProxyWorkflowGroup>()
            {
                new Colleague.Dtos.Base.ProxyWorkflowGroup()
                {
                    Code = "SF",
                    Description = "Student Finance",
                    Workflows = new List<Colleague.Dtos.Base.ProxyWorkflow>()
                    {
                        new Colleague.Dtos.Base.ProxyWorkflow()
                        {
                            Code = "SFAA",
                            Description = "Account Activity",
                            IsEnabled = true,
                            WorkflowGroupId = "SF"
                        },
                        new Colleague.Dtos.Base.ProxyWorkflow()
                        {
                            Code = "SFMAP",
                            Description = "Make a Payment",
                            IsEnabled = true,
                            WorkflowGroupId = "SF",
                        }
                    }
                },
                new Colleague.Dtos.Base.ProxyWorkflowGroup()
                {
                    Code = "FA",
                    Description = "Financial Aid",
                    Workflows = new List<Colleague.Dtos.Base.ProxyWorkflow>()
                    {
                        new Colleague.Dtos.Base.ProxyWorkflow()
                        {
                            Code = "FACL",
                            Description = "Checklist",
                            IsEnabled = false,
                            WorkflowGroupId = "FA"
                        },
                        new Colleague.Dtos.Base.ProxyWorkflow()
                        {
                            Code = "FARD",
                            Description = "Required Documents",
                            IsEnabled = false,
                            WorkflowGroupId = "FA"
                        }
                    }
                }
            };
            accessPermissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>()
            {
                new Colleague.Dtos.Base.ProxyAccessPermission()
                {
                    Id = "1",
                    IsGranted = true,
                    ProxySubjectId = "0001235",
                    ProxyUserId = "0001234",
                    ProxyWorkflowCode = "SFAA",
                    StartDate = DateTime.Today.AddDays(-3),
                    ReauthorizationDate = DateTime.Today.AddDays(7)
                },
                new Colleague.Dtos.Base.ProxyAccessPermission()
                {
                    Id = "2",
                    IsGranted = true,
                    ProxySubjectId = "0001235",
                    ProxyUserId = "0001234",
                    ProxyWorkflowCode = "SFMAP",
                    StartDate = DateTime.Today.AddDays(-3),
                    ReauthorizationDate = DateTime.Today.AddDays(7)
                }
            };
            reauthorizeAccessPermissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>()
            {
                new Colleague.Dtos.Base.ProxyAccessPermission()
                {
                    Id = "1",
                    IsGranted = true,
                    ProxySubjectId = "0001235",
                    ProxyUserId = "0001234",
                    ProxyWorkflowCode = "SFAA",
                    StartDate = DateTime.Today.AddDays(-3),
                    ReauthorizationDate = DateTime.Today.AddDays(-7)
                },
                new Colleague.Dtos.Base.ProxyAccessPermission()
                {
                    Id = "2",
                    IsGranted = true,
                    ProxySubjectId = "0001235",
                    ProxyUserId = "0001234",
                    ProxyWorkflowCode = "SFMAP",
                    StartDate = DateTime.Today.AddDays(-3),
                    ReauthorizationDate = DateTime.Today.AddDays(-5)
                }
            };
            config = new Ellucian.Colleague.Dtos.Base.ProxyConfiguration()
            {
                ProxyIsEnabled = true,
                DisclosureReleaseDocumentId = "PRX.DISC",
                DisclosureReleaseText = "Line 1",
                ProxyEmailDocumentId = "PRX.EMAIL",
                WorkflowGroups = new List<Colleague.Dtos.Base.ProxyWorkflowGroup>()
                {
                    new Colleague.Dtos.Base.ProxyWorkflowGroup()
                    {
                        Code = "SF",
                        Description = "Student Finance",
                        Workflows = new List<Colleague.Dtos.Base.ProxyWorkflow>() 
                        {
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFAA",
                                Description = "Account Activity",
                                IsEnabled = true,
                                WorkflowGroupId = "SF"
                            },
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFMAP",
                                Description = "Make a Payment",
                                IsEnabled = true,
                                WorkflowGroupId = "SF"
                            }
                        }
                    }
                },
                DemographicFields = new List<Ellucian.Colleague.Dtos.Base.DemographicField>()
                {
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "FIRST_NAME", Description = "First Name", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.FirstName },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "LAST_NAME", Description = "Last Name", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.LastName },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "EMAIL_ADDRESS", Description = "Email", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.EmailAddress },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "SSN", Description = "Government Id", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Optional, Type = Colleague.Dtos.Base.DemographicFieldType.GovernmentId }
                },
            };
            candidate = new Colleague.Dtos.Base.ProxyCandidate()
            {
                BirthDate = new DateTime(1990, 6, 15),
                EmailAddress = "angie@ellucian.edu",
                EmailType = "PRI",
                FormerFirstName = "Angela",
                FormerLastName = "Jones",
                FormerMiddleName = "P",
                FirstName = "Angie",
                Gender = "F",
                GovernmentId = "123-21-2321",
                GrantedPermissions = new List<string>() { "SFMAP" },
                LastName = "Smith",
                MiddleName = "P",
                Phone = "703-261-2358",
                PhoneExtension = "123",
                PhoneType = "HO",
                Prefix = "MS",
                ProxySubject = "0001234",
                RelationType = "PX",
                Suffix = "II"
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            id = null;
            name = null;
            relationship = null;
            email = null;
            photoUrl = null;
            workflowGroups = null;
            accessPermissions = null;
            model = null;
        }

        [TestClass]
        public class ProxyUserTests_Build : ProxyUserTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_Build_NullId()
            {
                model = ProxyUser.Build(null, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_Build_EmptyId()
            {
                model = ProxyUser.Build(string.Empty, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_Build_NullName()
            {
                model = ProxyUser.Build(id, null, relationship, email, effectiveDate, workflowGroups, accessPermissions);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_Build_EmptyName()
            {
                model = ProxyUser.Build(id, string.Empty, relationship, email, effectiveDate, workflowGroups, accessPermissions);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_Build_NullWorkflowGroups()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, null, accessPermissions);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_Build_EmptyWorkflowGroups()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>(), accessPermissions);
            }

            [TestMethod]
            public void ProxyUserTests_Build_NullPhotoUrl()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyuserTests_Build_EmptyEmailAndNullConfig()
            {
                model = ProxyUser.Build(id, name, relationship, "", effectiveDate, workflowGroups, accessPermissions);
            }

            [TestMethod]
            public void ProxyUserTests_Build_Id()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
                Assert.AreEqual(id, model.Id);
            }

            [TestMethod]
            public void ProxyUserTests_Build_Name()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
                Assert.AreEqual(name, model.Name);
            }

            [TestMethod]
            public void ProxyUserTests_Build_Relationship()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
                Assert.AreEqual(relationship, model.Relationship);
            }

            [TestMethod]
            public void ProxyUserTests_Build_Email()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
                Assert.AreEqual(email, model.Email);
                Assert.AreEqual(null, model.EmailType);
                Assert.AreEqual(true, model.HasEmailAddress);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_Build_EmailIsEmpty_NoConfigSupplied_ThrowsException()
            {
                model = ProxyUser.Build(id, name, relationship, string.Empty, effectiveDate, workflowGroups, accessPermissions);
            }

            [TestMethod]
            public void ProxyUserTests_Build_EmailIsEmpty_ButNotPromptingForEmailSoBooleanSetToTrue()
            {
                model = ProxyUser.Build(id, name, relationship, string.Empty, effectiveDate, workflowGroups, accessPermissions, config, false);
                Assert.AreEqual(null, model.Email);
                Assert.AreEqual(true, model.HasEmailAddress); 
            }


            [TestMethod]
            public void ProxyUserTests_Build_EmailIsEmptyAndNotCurrentUserAndCanAddOtherUsers_EmailAddressBooleanFalse()
            {
                config.CanAddOtherUsers = true;
                model = ProxyUser.Build(id, name, relationship, string.Empty, effectiveDate, workflowGroups, accessPermissions, config, false);
                Assert.AreEqual(string.Empty, model.Email);
                Assert.AreEqual(false, model.HasEmailAddress);
            }

            [TestMethod]
            public void ProxyUserTests_Build_EffectiveDate_NotNull()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
                Assert.AreEqual(effectiveDate, model.EffectiveDate);
            }

            [TestMethod]
            public void ProxyUserTests_Build_EffectiveDate_Null()
            {
                id = "0001236";
                model = ProxyUser.Build(id, name, relationship, email, null, workflowGroups, new List<Colleague.Dtos.Base.ProxyAccessPermission>());
                Assert.AreEqual(null, model.EffectiveDate);
            }

            [TestMethod]
            public void ProxyUserTests_Build_PermissionGroups()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, workflowGroups, accessPermissions);
                Assert.AreEqual(1, model.PermissionGroups.Count);
            }

            [TestMethod]
            public void ProxyUserTests_Build_NoPermissionGroups()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[1] }, accessPermissions);
                Assert.AreEqual(0, model.PermissionGroups.Count);
            }

            [TestMethod]
            public void ProxyUserTests_Build_Reauthorization_False()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[1] }, accessPermissions);
                Assert.IsFalse(model.ReauthorizationIsNeeded);
            }

            [TestMethod]
            public void ProxyUserTests_Build_Reauthorization_True()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions);
                Assert.IsTrue(model.ReauthorizationIsNeeded);
            }

            [TestMethod]
            public void ProxyUserTests_Build_ReauthorizationDate()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions);
                Assert.AreEqual(DateTime.Today.AddDays(-7), model.ReauthorizationDate);
            }

            [TestMethod]
            public void ProxyUserTests_Build_ReauthorizationDate_Null()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[1] }, accessPermissions);
                Assert.IsNull(model.ReauthorizationDate);
            }

            [TestMethod]
            public void ProxyUserTests_Build_ActionRequiredText_Null()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[1] }, accessPermissions);
                Assert.IsNull(model.ActionRequiredText);
            }

            [TestMethod]
            public void ProxyUserTests_Build_ActionRequiredText_Past()
            {
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions);
                Assert.IsNull(model.ActionRequiredText);
            }

            [TestMethod]
            public void ProxyUserTests_Build_ActionRequiredText_Today()
            {
                reauthorizeAccessPermissions.ForEach(pap => pap.ReauthorizationDate = DateTime.Today);
                model = ProxyUser.Build(id, name, relationship, email, effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions);
                Assert.IsNull(model.ActionRequiredText);
            }

            [TestMethod]
            public void ProxyUserTests_Build_DemographicInformation_EmailAddress_IfCanAddOtherUsers()
            {
                config.CanAddOtherUsers = true;
                model = ProxyUser.Build(id, name, relationship, "", effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions, config);
                
                // Model has email field/confirmation in demographic information
                var emailAddressDemographicInfo = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailAddress);
                Assert.AreEqual(2, emailAddressDemographicInfo.Count());
                Assert.AreEqual("email_address", emailAddressDemographicInfo.ElementAt(0).Id);
                Assert.AreEqual("confirm_email_address", emailAddressDemographicInfo.ElementAt(1).Id);
                // Model does not have email type in demographic informaiton because it's not configured
                var demoInfoEmailType = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailType);
                Assert.AreEqual(0, demoInfoEmailType.Count());
            }

            [TestMethod]
            public void ProxyUserTests_Build_DemographicInformation_EmailAddress_NotPresentIfCannotAddOtherUsers()
            {
                model = ProxyUser.Build(id, name, relationship, "", effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions, config);

                // Model has email field/confirmation in demographic information
                var emailAddressDemographicInfo = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailAddress);
                Assert.AreEqual(0, emailAddressDemographicInfo.Count());
                // Model does not have email type in demographic informaiton because it's not configured
                var demoInfoEmailType = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailType);
                Assert.AreEqual(0, demoInfoEmailType.Count());
            }

            [TestMethod]
            public void ProxyUserTests_Build_DemographicInformation_EmailTypeConfigured()
            {
                config.CanAddOtherUsers = true;
                // Update configuration to also include email type
                config.DemographicFields = new List<Ellucian.Colleague.Dtos.Base.DemographicField>()
                {
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "FIRST_NAME", Description = "First Name", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.FirstName },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "LAST_NAME", Description = "Last Name", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.LastName },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "EMAIL_ADDRESS", Description = "Email", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.EmailAddress },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "SSN", Description = "Government Id", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Optional, Type = Colleague.Dtos.Base.DemographicFieldType.GovernmentId },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "EMAIL_TYPE", Description = "Email Type", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Optional, Type = Colleague.Dtos.Base.DemographicFieldType.EmailType}
                };
                model = ProxyUser.Build(id, name, relationship, "", effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions, config);

                // Model has email field/confirmation in demographic information
                var demoInfoEmailAddress = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailAddress);
                Assert.AreEqual(2, demoInfoEmailAddress.Count());
                Assert.IsNotNull(demoInfoEmailAddress);
                var demoInfoEmailType = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailType);
                Assert.AreEqual(1, demoInfoEmailType.Count());
                Assert.AreEqual("email_type", demoInfoEmailType.ElementAt(0).Id);
            }

            [TestMethod]
            public void ProxyUserTests_Build_DemographicInformation_EmailTypeHidden()
            {
                config.CanAddOtherUsers = true;
                // Update configuration to also include email type
                config.DemographicFields = new List<Ellucian.Colleague.Dtos.Base.DemographicField>()
                {
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "FIRST_NAME", Description = "First Name", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.FirstName },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "LAST_NAME", Description = "Last Name", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.LastName },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "EMAIL_ADDRESS", Description = "Email", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Required, Type = Colleague.Dtos.Base.DemographicFieldType.EmailAddress },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "SSN", Description = "Government Id", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Optional, Type = Colleague.Dtos.Base.DemographicFieldType.GovernmentId },
                    new Ellucian.Colleague.Dtos.Base.DemographicField() { Code = "EMAIL_TYPE", Description = "Email Type", Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Hidden, Type = Colleague.Dtos.Base.DemographicFieldType.EmailType}
                };
                // Model has email field/confirmation in demographic information
                model = ProxyUser.Build(id, name, relationship, "", effectiveDate, new List<Colleague.Dtos.Base.ProxyWorkflowGroup>() { workflowGroups[0] }, reauthorizeAccessPermissions, config);
                var demoInfoEmailAddress = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailAddress);
                Assert.AreEqual(2, demoInfoEmailAddress.Count());
                // Model does not have email type in demographic informaiton because it's hidden
                var demoInfoEmailType = model.DemographicInformation.Where(d => d.Type == Colleague.Dtos.Base.DemographicFieldType.EmailType);
                Assert.AreEqual(0, demoInfoEmailType.Count());
            }

        }

        [TestClass]
        public class ProxyUserTests_BuildEmpty : ProxyUserTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_BuildEmpty_NullConfiguration()
            {
                model = ProxyUser.BuildEmpty(null);
            }

            [TestMethod]
            public void ProxyUserTests_BuildEmpty_DemographicInformation()
            {
                model = ProxyUser.BuildEmpty(config);
                Assert.AreEqual(config.DemographicFields.Count() + 2, model.DemographicInformation.Count);
            }

            [TestMethod]
            public void ProxyUserTests_BuildEmpty_DemographicInformation_GovernmentIdHidden()
            {
                config.DemographicFields.Last().Requirement = Colleague.Dtos.Base.DemographicFieldRequirement.Hidden;
                model = ProxyUser.BuildEmpty(config);
                Assert.AreEqual(config.DemographicFields.Count() + 1, model.DemographicInformation.Count);
            }

            [TestMethod]
            public void ProxyUserTests_BuildEmpty_DemographicInformation_NoGovernmentId()
            {
                var demographicFields = config.DemographicFields.ToList();
                demographicFields.RemoveAt(config.DemographicFields.Count() - 1);
                config.DemographicFields = demographicFields;

                model = ProxyUser.BuildEmpty(config);
                Assert.AreEqual(config.DemographicFields.Count() + 1, model.DemographicInformation.Count);
            }

            [TestMethod]
            public void ProxyUserTests_BuildEmpty_NullWorkflowGroups()
            {
                config.WorkflowGroups = null;
                model = ProxyUser.BuildEmpty(config);
                Assert.AreEqual(0, model.PermissionGroups.Count);
            }

            [TestMethod]
            public void ProxyUserTests_BuildEmpty_NoWorkflowGroups()
            {
                config.WorkflowGroups = new List<Colleague.Dtos.Base.ProxyWorkflowGroup>();
                model = ProxyUser.BuildEmpty(config);
                Assert.AreEqual(0, model.PermissionGroups.Count);
            }
        }

        [TestClass]
        public class ProxyUserTests_BuildCandidate : ProxyUserTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_BuildCandidate_NullConfiguration()
            {
                model = ProxyUser.BuildCandidate(null, candidate, "Proxy User");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyUserTests_BuildCandidate_NullCandidate()
            {
                model = ProxyUser.BuildCandidate(config, null, "Proxy User");
            }

            [TestMethod]
            public void ProxyUserTests_BuildCandidate_Valid()
            {
                model = ProxyUser.BuildCandidate(config, candidate, "Proxy User");
                Assert.AreEqual("PROXYCANDIDATE", model.Id);
                Assert.AreEqual(candidate.EmailAddress, model.Email);
                Assert.AreEqual(candidate.FirstName + " " + candidate.LastName, model.Name);
                Assert.AreEqual("Proxy User", model.Relationship);
                Assert.AreEqual(config.WorkflowGroups.Count(), model.PermissionGroups.Count);
                Assert.AreEqual(1, model.PermissionGroups.SelectMany(pg => pg.Permissions).Where(p => p.IsAssigned).Count());
                Assert.AreEqual(candidate.GrantedPermissions.ToList()[0], model.PermissionGroups.SelectMany(pg => pg.Permissions).Where(p => p.IsAssigned).First().Id);
            }
        }
    }
}
