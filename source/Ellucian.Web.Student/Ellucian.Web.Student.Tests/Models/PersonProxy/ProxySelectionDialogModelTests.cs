﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Models.PersonProxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Models.PersonProxy
{
    [TestClass]
    public class ProxySelectionDialogModelTests
    {
        public static List<Colleague.Dtos.Base.ProxySubject> proxySubjects;
        public static ICurrentUser currentUser;
        public static ProxySelectionDialogModel model;


        [TestInitialize]
        public void Initialize()
        {
            proxySubjects = new List<Colleague.Dtos.Base.ProxySubject>()
            {
                new Colleague.Dtos.Base.ProxySubject()
                {
                    Id = "0001234",
                    FullName = "John H. Smith",
                    Permissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>()
                    {
                        new Colleague.Dtos.Base.ProxyAccessPermission() {
                            EffectiveDate = DateTime.Today.AddDays(-3),
                            Id = "1",
                            IsGranted = true,
                            ProxySubjectId = "0001234",
                            ProxyUserId = "0001235",
                            ProxyWorkflowCode = "SFAA",
                            StartDate = DateTime.Today.AddDays(-3)
                        },
                        new Colleague.Dtos.Base.ProxyAccessPermission()
                        {
                            EffectiveDate = DateTime.Today.AddDays(-4),
                            Id = "2",
                            IsGranted = false,
                            ProxySubjectId = "0001234",
                            ProxyUserId = "0001235",
                            ProxyWorkflowCode = "SFAA",
                            StartDate = DateTime.Today.AddDays(-5),
                            EndDate = DateTime.Today.AddDays(-4)
                        }
                    },
                    EffectiveDate = DateTime.Today.AddDays(-3)
                }
            };

            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

            // Proxy-For formatted name
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "John H. Smith"));
            // Proxy-For person id
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0001234"));
            // Proxy-For permissions, one line for each
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAA"));
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFMAP"));
            currentUser = new CurrentUser(claim);
        }

        [TestCleanup]
        public void Cleanup()
        {
            proxySubjects = null;
            currentUser = null;
            model = null;
        }

        [TestClass]
        public class ProxySelectionDialogModelTests_Build : ProxySelectionDialogModelTests
        {
            [TestMethod]
            public void ProxySelectionDialogModelTests_Build_NullSubjects()
            {
                var model = ProxySelectionDialogModel.Build(null, null);
                Assert.AreEqual(0, model.ProxySubjects.Count);
            }

            [TestMethod]
            public void ProxySelectionDialogModelTests_Build_EmptySubjects()
            {
                var model = ProxySelectionDialogModel.Build(new List<Colleague.Dtos.Base.ProxySubject>(), null);
                Assert.AreEqual(0, model.ProxySubjects.Count);
            }

            [TestMethod]
            public void ProxySelectionDialogModelTests_Build_NullCurrentUser()
            {
                var model = ProxySelectionDialogModel.Build(proxySubjects, null);
                Assert.AreEqual(1, model.ProxySubjects.Count);
            }

            [TestMethod]
            public void ProxySelectionDialogModelTests_Build_Valid()
            {
                var model = ProxySelectionDialogModel.Build(proxySubjects, currentUser);
                Assert.AreEqual(2, model.ProxySubjects.Count);
            }
        }
    }
}
