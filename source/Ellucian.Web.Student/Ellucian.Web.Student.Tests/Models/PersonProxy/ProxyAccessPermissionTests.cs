﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.PersonProxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ellucian.Web.Student.Tests.Models.PersonProxy
{
    [TestClass]
    public class ProxyAccessPermissionTests
    {
        public string id;
        public string name;
        public DateTime? effectiveDate;
        public bool isEnabled;
        public bool isAssigned;
        public ProxyAccessPermission model;


        [TestInitialize]
        public void Initialize()
        {
            id = "SFMAP";
            name = "Make a Payment";
            effectiveDate = DateTime.Today;
            isEnabled = true;
            isAssigned = true;
        }

        [TestCleanup]
        public void Cleanup()
        {
            id = null;
            name = null;
            model = null;
        }

        [TestClass]
        public class ProxyAccessPermissionTests_Build : ProxyAccessPermissionTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessPermissionTests_Build_NullId()
            {
                model = ProxyAccessPermission.Build(null, name, effectiveDate, isEnabled);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessPermissionTests_Build_EmptyId()
            {
                model = ProxyAccessPermission.Build(string.Empty, name, effectiveDate, isEnabled);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessPermissionTests_Build_NullName()
            {
                model = ProxyAccessPermission.Build(id, null, effectiveDate, isEnabled);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ProxyAccessPermissionTests_Build_EmptyName()
            {
                model = ProxyAccessPermission.Build(id, string.Empty, effectiveDate, isEnabled);
            }

            [TestMethod]
            public void ProxyAccessPermissionTests_Build_IsEnabledIsFalse()
            {
                model = ProxyAccessPermission.Build(id, name, effectiveDate);
                Assert.AreEqual(id, model.Id);
                Assert.AreEqual(name, model.Name);
                Assert.IsFalse(model.IsEnabled);
                Assert.IsFalse(model.IsAssigned);
                Assert.IsFalse(model.IsInitiallyAssigned);
                Assert.IsNull(model.ReauthorizationDate);
            }

            [TestMethod]
            public void ProxyAccessPermissionTests_Build_IsEnabledIsTrue()
            {
                model = ProxyAccessPermission.Build(id, name, effectiveDate, isEnabled);
                Assert.IsTrue(model.IsEnabled);
            }

            [TestMethod]
            public void ProxyAccessPermissionTests_Build_IsAssignedIsTrue()
            {
                model = ProxyAccessPermission.Build(id, name, effectiveDate, isEnabled, isAssigned);
                Assert.IsTrue(model.IsAssigned);
                Assert.IsTrue(model.IsInitiallyAssigned);
            }

            [TestMethod]
            public void ProxyAccessPermissionTests_Build_ReauthorizationNotNeeded()
            {
                model = ProxyAccessPermission.Build(id, name, effectiveDate, true, true, DateTime.Today.AddDays(7));
                Assert.AreEqual(DateTime.Today.AddDays(7), model.ReauthorizationDate);
                Assert.IsFalse(model.ReauthorizationIsNeeded);
            }

            [TestMethod]
            public void ProxyAccessPermissionTests_Build_NullReauthorizationNeeded()
            {
                model = ProxyAccessPermission.Build(id, name, effectiveDate, true, true);
                Assert.IsNull(model.ReauthorizationDate);
                Assert.IsFalse(model.ReauthorizationIsNeeded);
            }

            [TestMethod]
            public void ProxyAccessPermissionTests_Build_NonNullReauthorizationDate()
            {
                model = ProxyAccessPermission.Build(id, name, effectiveDate, true, true, DateTime.Today.AddDays(-7));
                Assert.AreEqual(DateTime.Today.AddDays(-7), model.ReauthorizationDate);
                Assert.IsTrue(model.ReauthorizationIsNeeded);
            }
        }
    }
}
