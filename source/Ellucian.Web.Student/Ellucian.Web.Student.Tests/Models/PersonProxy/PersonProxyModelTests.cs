﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.PersonProxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Tests.Models.PersonProxy
{
    [TestClass]
    public class PersonProxyModelTests
    {
        public string id;
        public string name;
        public bool isEnabled;
        public bool isAssigned;
        public ProxyConfiguration config;
        public ProxyConfiguration noEnabledWorkflowsConfig;
        public ProxyConfiguration restrictedRelationshipTypesConfig;
        private List<Relationship> noRelationships;
        private List<Relationship> relationships;
        private List<Relationship> nonMatchingRelationships;
        private List<RelationshipType> relTypes;
        private List<RelationshipType> nonMatchingRelTypes;
        private List<Prefix> prefixes;
        private List<Suffix> suffixes;
        private List<EmailType> emailTypes;
        private List<PhoneType> phoneTypes;
        public UserProfileConfiguration profileConfig;
        public UserProfileConfiguration allEmailTypesProfileConfig;
        public UserProfileConfiguration noEmailTypesProfileConfig;
        private List<Profile> noProfiles;
        private List<Profile> noEmailProfiles;
        private List<Profile> profiles;
        private List<Colleague.Dtos.Base.ProxyUser> noProxyUsers;
        private List<Colleague.Dtos.Base.ProxyUser> proxyUsers;
        private List<Colleague.Dtos.Base.ProxyUser> reauthProxyUsers;
        private List<Colleague.Dtos.Base.ProxyCandidate> proxyCandidates;
        private List<Colleague.Dtos.Base.DemographicField> demographicFields;
        public PersonProxyModel model;

        [TestInitialize]
        public void Initialize()
        {
            id = "0001234";
            noRelationships = new List<Relationship>();
            relationships = new List<Relationship>()
                {
                    new Relationship() 
                    {
                        IsActive = true,
                        IsPrimaryRelationship = true,
                        OtherEntity = "0001234",
                        PrimaryEntity  ="0001235",
                        RelationshipType = "P",
                        StartDate = DateTime.Today.AddDays(-7)
                    }
                };
            nonMatchingRelationships = new List<Relationship>()
                {
                    new Relationship() 
                    {
                        IsActive = true,
                        IsPrimaryRelationship = true,
                        OtherEntity = "0001234",
                        PrimaryEntity  ="0001235",
                        RelationshipType = "P",
                        StartDate = DateTime.Today.AddDays(-7)
                    },
                    new Relationship() 
                    {
                        IsActive = true,
                        IsPrimaryRelationship = true,
                        OtherEntity = "0001236",
                        PrimaryEntity  ="0001235",
                        RelationshipType = "P",
                        StartDate = DateTime.Today.AddDays(-7)
                    }
                };
            relTypes = new List<RelationshipType>()
                {
                    new RelationshipType() { Code = "P", Description = "Parent", InverseCode = "C"},
                    new RelationshipType() { Code = "C", Description = "Child", InverseCode = "P"}
                };
            nonMatchingRelTypes = new List<RelationshipType>()
                {
                    new RelationshipType() { Code = "S", Description = "Son", InverseCode = "F"},
                    new RelationshipType() { Code = "D", Description = "Daughter", InverseCode = "M"}
                };
            noProfiles = new List<Profile>();
            profiles = new List<Profile>()
                {
                    new Profile()
                    {
                        Id = "0001234",
                        PreferredName = "John H. Smith",
                        EmailAddresses = new List<EmailAddress>() { new EmailAddress() { IsPreferred = true, TypeCode = "PRI", Value = "john.h.smith@ellucianuniv.edu"} }
                    },
                    new Profile()
                    {
                        Id = "0001236",
                        PreferredName = "Lisa M. Smith",
                        EmailAddresses = new List<EmailAddress>() { new EmailAddress() { IsPreferred = true, TypeCode = "PRI", Value = "lisa.m.smith@ellucianuniv.edu"} }
                    },                
                };
            noEmailProfiles = new List<Profile>()
                {
                    new Profile()
                    {
                        Id = "0001234",
                        PreferredName = "John H. Smith",
                        EmailAddresses = new List<EmailAddress>()
                    },
                    new Profile()
                    {
                        Id = "0001236",
                        PreferredName = "Lisa M. Smith",
                        EmailAddresses = new List<EmailAddress>()
                    },                
                };
            noProxyUsers = new List<Colleague.Dtos.Base.ProxyUser>();
            proxyUsers = new List<Colleague.Dtos.Base.ProxyUser>()
            {
                new Colleague.Dtos.Base.ProxyUser()
                {
                    EffectiveDate = DateTime.Today.AddDays(-3),
                    Id = "0001234",
                    Permissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>()
                    {
                        new Colleague.Dtos.Base.ProxyAccessPermission() {
                            EffectiveDate = DateTime.Today.AddDays(-3),
                            Id = "1",
                            IsGranted = true,
                            ProxySubjectId = "0001234",
                            ProxyUserId = "0001235",
                            ProxyWorkflowCode = "SFAA",
                            StartDate = DateTime.Today.AddDays(-3)
                        },
                        new Colleague.Dtos.Base.ProxyAccessPermission()
                        {
                            EffectiveDate = DateTime.Today.AddDays(-4),
                            Id = "2",
                            IsGranted = false,
                            ProxySubjectId = "0001234",
                            ProxyUserId = "0001235",
                            ProxyWorkflowCode = "SFAA",
                            StartDate = DateTime.Today.AddDays(-5),
                            EndDate = DateTime.Today.AddDays(-4)
                        }
                    }
                }
            };
            reauthProxyUsers = new List<Colleague.Dtos.Base.ProxyUser>()
            {
                new Colleague.Dtos.Base.ProxyUser()
                {
                    EffectiveDate = DateTime.Today.AddDays(-3),
                    Id = "0001234",
                    Permissions = new List<Colleague.Dtos.Base.ProxyAccessPermission>()
                    {
                        new Colleague.Dtos.Base.ProxyAccessPermission() {
                            EffectiveDate = DateTime.Today.AddDays(-3),
                            Id = "1",
                            IsGranted = true,
                            ProxySubjectId = "0001235",
                            ProxyUserId = "0001234",
                            ProxyWorkflowCode = "SFMAP",
                            ReauthorizationDate = DateTime.Today.AddDays(-2),
                            StartDate = DateTime.Today.AddDays(-3)
                        },
                        new Colleague.Dtos.Base.ProxyAccessPermission()
                        {
                            EffectiveDate = DateTime.Today.AddDays(-4),
                            Id = "2",
                            IsGranted = true,
                            ProxySubjectId = "0001235",
                            ProxyUserId = "0001234",
                            ProxyWorkflowCode = "SFAA",
                            ReauthorizationDate = DateTime.Today.AddDays(-2),
                            StartDate = DateTime.Today.AddDays(-5),
                            EndDate = DateTime.Today.AddDays(-4)
                        }
                    }
                }
            };
            demographicFields = new List<Colleague.Dtos.Base.DemographicField>() 
                {
                    new Colleague.Dtos.Base.DemographicField()
                    {
                        Code = "prefix",
                        Description = "Prefix",
                        Requirement = DemographicFieldRequirement.Optional,
                        Type = DemographicFieldType.Prefix
                    },
                    new Colleague.Dtos.Base.DemographicField()
                    {
                        Code = "suffix",
                        Description = "Suffix",
                        Requirement = DemographicFieldRequirement.Optional,
                        Type = DemographicFieldType.Suffix
                    },
                    new Colleague.Dtos.Base.DemographicField()
                    {
                        Code = "email_address",
                        Description = "Email Address",
                        Requirement = DemographicFieldRequirement.Required,
                        Type = DemographicFieldType.EmailAddress
                    },
                    new Colleague.Dtos.Base.DemographicField()
                    {
                        Code = "email_type",
                        Description = "Email Type",
                        Requirement = DemographicFieldRequirement.Optional,
                        Type = DemographicFieldType.EmailType
                    },
                    new Colleague.Dtos.Base.DemographicField()
                    {
                        Code = "phone_type",
                        Description = "Phone Type",
                        Requirement = DemographicFieldRequirement.Optional,
                        Type = DemographicFieldType.PhoneType
                    },
                    new Colleague.Dtos.Base.DemographicField()
                    {
                        Code = "gender",
                        Description = "Gender",
                        Requirement = DemographicFieldRequirement.Optional,
                        Type = DemographicFieldType.Gender
                    }
                };
            config = new ProxyConfiguration()
            {
                CanAddOtherUsers = true,
                ProxyIsEnabled = true,
                DisclosureReleaseDocumentId = "PRX.DISC",
                DisclosureReleaseText = "Line 1",
                ProxyEmailDocumentId = "PRX.EMAIL",
                WorkflowGroups = new List<Colleague.Dtos.Base.ProxyWorkflowGroup>()
                {
                    new Colleague.Dtos.Base.ProxyWorkflowGroup()
                    {
                        Code = "SF",
                        Description = "Student Finance",
                        Workflows = new List<Colleague.Dtos.Base.ProxyWorkflow>() 
                        {
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFAA",
                                Description = "Account Activity",
                                IsEnabled = true,
                                WorkflowGroupId = "SF"
                            },
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFMAP",
                                Description = "Make a Payment",
                                IsEnabled = true,
                                WorkflowGroupId = "SF"
                            }
                        }
                    }
                },
                DemographicFields = demographicFields
            };
            noEnabledWorkflowsConfig = new ProxyConfiguration()
            {
                CanAddOtherUsers = true,
                ProxyIsEnabled = true,
                DisclosureReleaseDocumentId = "PRX.DISC",
                DisclosureReleaseText = "Line 1",
                ProxyEmailDocumentId = "PRX.EMAIL",
                WorkflowGroups = new List<Colleague.Dtos.Base.ProxyWorkflowGroup>()
                {
                    new Colleague.Dtos.Base.ProxyWorkflowGroup()
                    {
                        Code = "SF",
                        Description = "Student Finance",
                        Workflows = new List<Colleague.Dtos.Base.ProxyWorkflow>() 
                        {
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFAA",
                                Description = "Account Activity",
                                IsEnabled = false,
                                WorkflowGroupId = "SF"
                            },
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFMAP",
                                Description = "Make a Payment",
                                IsEnabled = false,
                                WorkflowGroupId = "SF"
                            }
                        }
                    }
                }
            };
            restrictedRelationshipTypesConfig = new ProxyConfiguration()
            {
                CanAddOtherUsers = true,
                ProxyIsEnabled = true,
                DisclosureReleaseDocumentId = "PRX.DISC",
                DisclosureReleaseText = "Line 1",
                ProxyEmailDocumentId = "PRX.EMAIL",
                RelationshipTypeCodes = new List<string>() { "P" },
                WorkflowGroups = new List<Colleague.Dtos.Base.ProxyWorkflowGroup>()
                {
                    new Colleague.Dtos.Base.ProxyWorkflowGroup()
                    {
                        Code = "SF",
                        Description = "Student Finance",
                        Workflows = new List<Colleague.Dtos.Base.ProxyWorkflow>() 
                        {
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFAA",
                                Description = "Account Activity",
                                IsEnabled = true,
                                WorkflowGroupId = "SF"
                            },
                            new Colleague.Dtos.Base.ProxyWorkflow()
                            {
                                Code = "SFMAP",
                                Description = "Make a Payment",
                                IsEnabled = true,
                                WorkflowGroupId = "SF"
                            }
                        }
                    }
                },
                DemographicFields = demographicFields
            };
            prefixes = new List<Prefix>()
            {
                new Prefix() { Code = "MR", Description = "Mr." },
                new Prefix() { Code = "MS", Description = "Ms." },
            };
            suffixes = new List<Suffix>()
            {
                new Suffix() { Code = "JR", Description = "Jr." },
                new Suffix() { Code = "II", Description = "II" },
            };
            profileConfig = new UserProfileConfiguration()
            {
                AllEmailTypesAreUpdatable = false,
                UpdatableEmailTypes = new List<string>() { "PRI", "SEC" },
                UpdatablePhoneTypes = new List<string>() { "HO", "BU"}
            };
            allEmailTypesProfileConfig = new UserProfileConfiguration()
            {
                AllEmailTypesAreUpdatable = true,
                UpdatableEmailTypes = new List<string>() { "PRI", "SEC" },
                UpdatablePhoneTypes = new List<string>() { "HO", "BU"}
            };
            noEmailTypesProfileConfig = new UserProfileConfiguration()
            {
                AllEmailTypesAreUpdatable = false,
                UpdatableEmailTypes = new List<string>(),
                UpdatablePhoneTypes = new List<string>()
            };
            emailTypes = new List<EmailType>()
            {
                new EmailType() { Code = "PRI", Description = "Primary" },
                new EmailType() { Code = "SEC", Description = "Secondary" },
            };
            phoneTypes = new List<PhoneType>()
            {
                new PhoneType() { Code = "HO", Description = "Home" },
                new PhoneType() { Code = "BU", Description = "Business" },
            };
            proxyCandidates = new List<ProxyCandidate>()
            {
                new ProxyCandidate() { EmailAddress = "abc@123.com", FirstName = "AB", LastName = "CDEF", RelationType = "P", GrantedPermissions = new List<string>() { "SFAA" } }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            id = null;
            config = null;
        }

        [TestClass]
        public class PersonProxyModelTests_Build : PersonProxyModelTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullId()
            {
                model = PersonProxyModel.Build(null, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_EmptyId()
            {
                model = PersonProxyModel.Build(string.Empty, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullConfig()
            {
                model = PersonProxyModel.Build(id, null, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullRelationships()
            {
                model = PersonProxyModel.Build(id, config, null, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullProfiles()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, null, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullRelationshipTypes()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, null, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullPrefixes()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, null, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullSuffixes()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, null, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullEmailTypes()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, null, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullPhoneTypes()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, null, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullProxyCandidates()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, null, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_NullUserProfileConfiguration()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_UserProfileConfiguration_NullUpdatableEmailTypes()
            {
                profileConfig.UpdatableEmailTypes = null;
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonProxyModelTests_Build_UserProfileConfiguration_NullUpdatablePhoneTypes()
            {
                profileConfig.UpdatablePhoneTypes = null;
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ProxyIsEnabled_NoActiveProxies()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.AreEqual(id, model.Id);
                Assert.AreEqual(config.ProxyIsEnabled, model.PersonProxyIsEnabled);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(noRelationships.Count + 2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ProxyIsEnabled_NullActiveProxies()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, null, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.AreEqual(id, model.Id);
                Assert.AreEqual(config.ProxyIsEnabled, model.PersonProxyIsEnabled);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(noRelationships.Count + 2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ActiveProxies_NoProxiesToAdd()
            {
                model = PersonProxyModel.Build(id, config, relationships, profiles, proxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(proxyUsers.Count + 2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ActiveProxies_NoProxiesToAdd_CannotAddUsers()
            {
                config.CanAddOtherUsers = false;
                proxyCandidates = new List<ProxyCandidate>();
                model = PersonProxyModel.Build(id, config, relationships, profiles, proxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(proxyUsers.Count, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ActiveProxies_ProxiesToAdd()
            {
                model = PersonProxyModel.Build(id, config, nonMatchingRelationships, profiles, proxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(nonMatchingRelationships.Count + 2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ActiveProxies_NoMatchingRelationshipType()
            {
                model = PersonProxyModel.Build(id, config, relationships, profiles, proxyUsers, nonMatchingRelTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(proxyUsers.Count + 2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_NoActiveProxies_ProxyToAddWithNoEmailIsIncluded()
            {
                model = PersonProxyModel.Build(id, config, relationships, noEmailProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(3, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ProxyToAddWithNullEmailAddressesIsIncluded()
            {
                noEmailProfiles[0].EmailAddresses = null;
                model = PersonProxyModel.Build(id, config, relationships, noEmailProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.AreEqual(3, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_NoActiveProxies_ProxyToAddWithNoRelationshipTypeDescription()
            {
                model = PersonProxyModel.Build(id, config, relationships, profiles, noProxyUsers, nonMatchingRelTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_NoActiveProxies_ProxyToAddWithNoRelationshipTypeDescription_2()
            {
                config.RelationshipTypeCodes = new List<string>();
                model = PersonProxyModel.Build(id, config, relationships, profiles, noProxyUsers, nonMatchingRelTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_NoWorkflowsEnabled()
            {
                model = PersonProxyModel.Build(id, noEnabledWorkflowsConfig, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsFalse(model.AnyPermissionsEnabled);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_RelationshipsInAllowedRelationshipTypes()
            {
                model = PersonProxyModel.Build(id, restrictedRelationshipTypesConfig, nonMatchingRelationships, profiles, proxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(nonMatchingRelationships.Count + 2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_NoRelationshipsInAllowedRelationshipTypes()
            {
                restrictedRelationshipTypesConfig.RelationshipTypeCodes = new List<string>() { "X" };
                model = PersonProxyModel.Build(id, restrictedRelationshipTypesConfig, nonMatchingRelationships, profiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ProxyUsers);
                Assert.AreEqual(2, model.ProxyUsers.Count);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_MultipleLinesDisclosureAgreement()
            {
                config.DisclosureReleaseText = "Line1" + Environment.NewLine + "Line2" + Environment.NewLine + "Line3";
                model = PersonProxyModel.Build(id, config, nonMatchingRelationships, profiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.DisclosureAgreementText);
                Assert.AreEqual("Line1<br />Line2<br />Line3", model.DisclosureAgreementText);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_MultipleLinesReauthorizationText()
            {
                config.ReauthorizationText = "Line1" + Environment.NewLine + "Line2" + Environment.NewLine + "Line3";
                model = PersonProxyModel.Build(id, config, nonMatchingRelationships, profiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, profileConfig);
                Assert.IsNotNull(model.ReauthorizationText);
                Assert.AreEqual("Line1<br />Line2<br />Line3", model.ReauthorizationText);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_UserProfileConfiguration_AllEmailTypesUpdatable()
            {
                profileConfig.UpdatableEmailTypes = null;
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, allEmailTypesProfileConfig);
                Assert.IsTrue(model.ProxyUsers[1].DemographicInformation.Where(di => di.Type == DemographicFieldType.EmailType).SelectMany(et => et.Options).Any());
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_UserProfileConfiguration_NoEmailTypes()
            {
                profileConfig.UpdatableEmailTypes = null;
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, noEmailTypesProfileConfig);
                Assert.IsFalse(model.ProxyUsers[1].DemographicInformation.Where(di => di.Type == DemographicFieldType.EmailType).SelectMany(et => et.Options).Any());            
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ReauthorizationIsNeeded_False()
            {
                model = PersonProxyModel.Build(id, config, noRelationships, noProfiles, noProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, noEmailTypesProfileConfig);
                Assert.IsFalse(model.ReauthorizationIsNeeded);
            }

            [TestMethod]
            public void PersonProxyModelTests_Build_ReauthorizationIsNeeded_True()
            {
                model = PersonProxyModel.Build(id, config, relationships, profiles, reauthProxyUsers, relTypes, prefixes, suffixes, emailTypes, phoneTypes, proxyCandidates, noEmailTypesProfileConfig);
                Assert.IsTrue(model.ReauthorizationIsNeeded);
            }
        }
    }
}
