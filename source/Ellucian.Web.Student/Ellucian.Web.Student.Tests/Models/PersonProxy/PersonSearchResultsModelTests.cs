﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.PersonProxy;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Models.PersonProxy
{
    [TestClass]
    public class PersonSearchResultsModelTests
    {
        [TestInitialize]
        public void Initialize()
        {
        }

        [TestCleanup]
        public void Cleanup()
        {

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonSearchResultsModel_Build_NullCriteria()
        {
            var model = PersonSearchResultsModel.Build(null, new List<Ellucian.Colleague.Dtos.Base.PersonMatchResult>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonSearchResultsModel_Build_NullResults()
        {
            var model = PersonSearchResultsModel.Build(new ProxyUser(), null);
        }

        [TestMethod]
        public void PersonSearchResultsModel_Build_SearchCriteria()
        {
            var model = PersonSearchResultsModel.Build(new ProxyUser(), new List<Ellucian.Colleague.Dtos.Base.PersonMatchResult>());
            Assert.IsNotNull(model.SearchCriteria);
        }

        [TestMethod]
        public void PersonSearchResultsModel_Build_Results()
        {
            var model = PersonSearchResultsModel.Build(new ProxyUser(), new List<Ellucian.Colleague.Dtos.Base.PersonMatchResult>());
            Assert.IsNotNull(model.Results);
        }

        [TestMethod]
        public void PersonSearchResultsModel_Build_Message_NoResults()
        {
            var model = PersonSearchResultsModel.Build(new ProxyUser(), new List<Ellucian.Colleague.Dtos.Base.PersonMatchResult>());
            Assert.IsNull(model.Message);
        }

        [TestMethod]
        public void PersonSearchResultsModel_Build_Message_OneDefiniteResult()
        {
            var model = PersonSearchResultsModel.Build(new ProxyUser(), new List<Ellucian.Colleague.Dtos.Base.PersonMatchResult>()
            {
                new Ellucian.Colleague.Dtos.Base.PersonMatchResult()
                {
                    MatchCategory = Ellucian.Colleague.Dtos.Base.PersonMatchCategoryType.Definite,
                    MatchScore = 70,
                    PersonId = "0003315"
                }
            });
            Assert.IsNull(model.Message);
        }

        [TestMethod]
        public void PersonSearchResultsModel_Build_Message_MultipleResults()
        {
            var model = PersonSearchResultsModel.Build(new ProxyUser(), new List<Ellucian.Colleague.Dtos.Base.PersonMatchResult>()
            {
                new Ellucian.Colleague.Dtos.Base.PersonMatchResult()
                {
                    MatchCategory = Ellucian.Colleague.Dtos.Base.PersonMatchCategoryType.Potential,
                    MatchScore = 70,
                    PersonId = "0003315"
                },
                new Ellucian.Colleague.Dtos.Base.PersonMatchResult()
                {
                    MatchCategory = Ellucian.Colleague.Dtos.Base.PersonMatchCategoryType.Potential,
                    MatchScore = 70,
                    PersonId = "0003316"
                },
            }); Assert.IsNull(model.Message);
        }
    }
}
