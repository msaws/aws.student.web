﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.Search;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ellucian.Web.Student.Tests.Models.Search
{
    [TestClass]
    public class SearchActionModelTests
    {
        string action;
        string controller;
        string area;
        SearchActionModel model;

        [TestInitialize]
        public void Initialize()
        {
            action = "Action";
            controller = "Controller";
            area = "Area";
        }

        [TestCleanup]
        public void Cleanup()
        {
            action = null;
            controller = null;
            area = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SearchActionModel_NullAction()
        {
            model = new SearchActionModel(null, controller, area);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SearchActionModel_NullController()
        {
            model = new SearchActionModel(action, null, area);
        }

        [TestMethod]
        public void SearchActionModel_Action()
        {
            model = new SearchActionModel(action, controller, area);
            Assert.AreEqual(action, model.Action);
        }

        [TestMethod]
        public void SearchActionModel_Controller()
        {
            model = new SearchActionModel(action, controller, area);
            Assert.AreEqual(controller, model.Controller);
        }

        [TestMethod]
        public void SearchActionModel_Area_Default()
        {
            model = new SearchActionModel(action, controller);
            Assert.IsNull(model.Area);
        }

        [TestMethod]
        public void SearchActionModel_Area()
        {
            model = new SearchActionModel(action, controller, area);
            Assert.AreEqual(area, model.Area);
        }
    }
}
