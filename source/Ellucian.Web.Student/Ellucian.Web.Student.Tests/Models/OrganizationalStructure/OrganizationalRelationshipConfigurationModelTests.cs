﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Models.OrganizationalStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Models.OrganizationalStructure
{
    [TestClass]
    public class OrganizationalRelationshipConfigurationModelTests
    {
        ICurrentUser currentUser;
        IEnumerable<Role> roles;
        IEnumerable<string> defaultUserRoles;
        IEnumerable<string> privilegedUserRoles;

        [TestInitialize]
        public void Initialize()
        {
            var currentUserMock = new Mock<ICurrentUser>();
            defaultUserRoles = new List<string> { "Employee" };
            privilegedUserRoles = new List<string> { "Employee", "PERSONNEL DIRECTOR" };
            currentUserMock.Setup(c => c.Roles).Returns(defaultUserRoles);

            currentUser = currentUserMock.Object;
            roles = new List<Role>() {
                new Role() {
                    Permissions = new List<Permission> {
                        new Permission() { Code = BasePermissionCodes.UpdateOrganizationalRelationships }
                    },
                    Title = "PERSONNEL DIRECTOR"},
                new Role() {
                    Permissions = new List<Permission>(),
                    Title = "Employee"
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            currentUser = null;
        }

        [TestMethod]
        public void OrganizationalRelationshipConfiguration_Constructor_CreatesList()
        {
            var configModel = new OrganizationalRelationshipConfigurationModel();
            Assert.IsNotNull(configModel.ManagerRelationshipTypes);
            Assert.AreEqual(0, configModel.ManagerRelationshipTypes.Count);
        }

        [TestMethod]
        public void OrganizationalRelationshipConfiguration_Constructor_AcceptsMapping()
        {
            var configDto = new OrganizationalRelationshipConfiguration()
            {
                RelationshipTypeCodeMapping = new Dictionary<OrganizationalRelationshipType, List<string>> {
                    { OrganizationalRelationshipType.Manager, new List<string> { "MGR"} }
                }
            };
            var configModel = new OrganizationalRelationshipConfigurationModel(configDto, roles, currentUser);
            Assert.IsNotNull(configModel.ManagerRelationshipTypes);
            Assert.AreEqual(1, configModel.ManagerRelationshipTypes.Count);
            Assert.AreEqual("MGR", configModel.ManagerRelationshipTypes.First());
            Assert.IsFalse(configModel.UserCanUpdateOrganizationalRelationships);
        }

        [TestMethod]
        public void OrganizationalRelationshipConfiguration_Constructor_SetsPermission()
        {
            var configDto = new OrganizationalRelationshipConfiguration()
            {
                RelationshipTypeCodeMapping = new Dictionary<OrganizationalRelationshipType, List<string>> {
                    { OrganizationalRelationshipType.Manager, new List<string> { "MGR"} }
                }
            };
            var currentUserMock = new Mock<ICurrentUser>();
            currentUserMock.Setup(c => c.Roles).Returns(privilegedUserRoles);
            currentUser = currentUserMock.Object;
            var configModel = new OrganizationalRelationshipConfigurationModel(configDto, roles, currentUser);
            Assert.IsTrue(configModel.UserCanUpdateOrganizationalRelationships);
        }

        [TestMethod]
        public void OrganizationalRelationshipConfiguration_Constructor_DoesNotIncludeNonManagers()
        {
            var configDto = new OrganizationalRelationshipConfiguration()
            {
                RelationshipTypeCodeMapping = new Dictionary<OrganizationalRelationshipType, List<string>> {
                    { OrganizationalRelationshipType.Unknown, new List<string> { "UNK"} }
                }
            };
            var configModel = new OrganizationalRelationshipConfigurationModel(configDto, roles, currentUser);
            Assert.IsNotNull(configModel.ManagerRelationshipTypes);
            Assert.AreEqual(0, configModel.ManagerRelationshipTypes.Count);
        }
    }
}
