﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Services.Dtos;
using Ellucian.Web.Student.Services.Dtos.Requirements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Models.Schedule;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Infrastructure.TestUtil;

namespace Ellucian.Web.Student.Tests.Models
{
    /// <summary>
    /// Encompasses all testing related to models and helpers used in construction and manipulation of a student program model.
    /// <remarks>

    /// </remarks>
    /// </summary>
    [TestClass]
    public class StudentProgramModelTests
    {
        [TestClass]
        public class StudentProgramModelConstructorTests
        {
            TestStudentProgramRepository studentProgramRepository = new TestStudentProgramRepository();
            TestProgramRepository programRepository = new TestProgramRepository();
            TestProgramRequirementsRepository programRequirementsRepository = new TestProgramRequirementsRepository();
            

            

            [TestInitialize]
            public void Initialize()
            {
                fakeTerm1 = new Term() { Code = "ABC", Description = "Fall Term", StartDate = DateTime.Now, EndDate = DateTime.Now.AddDays(120), ReportingYear = 2012 };
                fakeTerm2 = new Term() { Code = "XYZ", Description = "Summer Term", StartDate = DateTime.Now.AddDays(120), EndDate = DateTime.Now.AddDays(240), ReportingYear = 2013 };
                model = new DegreePlanModel();
                helper = new DegreePlanModelHelper();
            }

            [TestCleanup]
            public void CleanUp()
            {
                model = null;
                fakeTerm1 = null;
                fakeTerm2 = null;
            }

            #region Constructor and Setup
            [TestMethod]
            public void StudentProgramModelConstructor()
            {
                
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void AddExceptionIfTermNull()
            {
                helper.AddTermAndCourses(model, null, new List<Course>());
            }
            #endregion

        }

    }
}
