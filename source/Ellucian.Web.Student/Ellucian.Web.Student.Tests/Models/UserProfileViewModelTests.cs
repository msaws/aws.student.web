﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Models.UserProfile;
using Ellucian.Colleague.Dtos.Base;
using System.Collections.Generic;
using Ellucian.Web.Security;
using System.Linq;
using Moq;

namespace Ellucian.Web.Student.Tests.Models
{
    [TestClass]
    public class UserProfileViewModelTests
    {
        Profile expectedProfileDto = null;
        IEnumerable<Ellucian.Colleague.Dtos.Base.PhoneType> expectedPhoneTypes = null;
        IEnumerable<Ellucian.Colleague.Dtos.Base.PhoneType> expectedUpdatablePhoneTypes = null;
        IEnumerable<Ellucian.Colleague.Dtos.Base.EmailType> expectedEmailTypes = null;
        IEnumerable<Ellucian.Colleague.Dtos.Base.EmailType> expectedUpdatableEmailTypes = null;
        IEnumerable<Ellucian.Colleague.Dtos.Base.PersonalPronounType> expectedPersonalPronounTypes = null;
        IEnumerable<Ellucian.Colleague.Dtos.AddressType2> expectedAddressTypes = null;
        IEnumerable<Ellucian.Colleague.Dtos.AddressType2> expectedUpdatableAddressTypes;
        List<ProfilePhoneModel> expectedProfilePhoneModels;
        List<ProfilePhoneModel> expectedProfilePhoneModelsWithoutConfiguration;
        List<ProfileEmailAddressModel> expectedProfileEmailAddressModels;
        List<ProfileEmailAddressModel> expectedProfileEmailAddressModelsWithoutConfiguration;
        List<Country> countries;
        List<State> states;
        UserProfileConfiguration2 configuration;
        UserProfileConfiguration2 configuration2;
        UserProfileConfiguration2 configuration3;
        ICurrentUser currentUser;
        IEnumerable<Role> roles;
        string expectedText;

        [TestInitialize]
        public void TestInitialize()
        {
            var expectedProfileAddress1 = new Address
            {
                AddressId = "1",
                AddressLabel = new List<string>
                    {
                        "123 Main St",
                        "Fairfax, VA 22033"
                    },
                IsPreferredAddress = true,
                Type = "Home",
                TypeCode = "H"
            };

            var expectedProfileAddress2 = new Address
            {
                AddressId = "2",
                AddressLabel = new List<string>
                    {
                        "Care of Ellucian",
                        "30 Queen's Gate Mews",
                        "London SW7 5QL",
                        "UNITED KINGDOM"
                    },
                IsPreferredAddress = false,
                Type = "Business",
                TypeCode = "B"
            };
            var expectedProfileAddress3 = new Address
            {
                AddressId = "3",
                AddressLabel = new List<string>
                    {
                        "43500 Fair Lakes Court",
                        "Fairfax, VA 22033"
                    },
                IsPreferredAddress = false,
                Type = "Business",
                TypeCode = "X"
            };
            var dtoAddresses = new List<Address>()
            {
                expectedProfileAddress1,
                expectedProfileAddress2,
                expectedProfileAddress3
            };

            states = new List<State>();
            states.Add(new State() { Code = "MD", CountryCode = "", Description = "Maryland" });
            states.Add(new State() { Code = "WV", CountryCode = "", Description = "West Virginia" });
            states.Add(new State() { Code = "VA", CountryCode = "", Description = "Virginia" });
            states.Add(new State() { Code = "OH", CountryCode = "", Description = "Ohio" });
            states.Add(new State() { Code = "PA", CountryCode = "", Description = "Pennsylvania" });
            states.Add(new State() { Code = "NB", CountryCode = "CA", Description = "New Brunswick" });

            countries = new List<Country>();
            countries.Add(new Country() { Code = "CA", Description = "CANADA", IsNotInUse = false, IsoCode = "CA" });
            countries.Add(new Country() { Code = "VE", Description = "VENEZUELA", IsNotInUse = false, IsoCode = "VE" });
            countries.Add(new Country() { Code = "HU", Description = "HUNGARY", IsNotInUse = true, IsoCode = "HU" });
            countries.Add(new Country() { Code = "AG", Description = "AFGANISTAN", IsNotInUse = false, IsoCode = "AG" });

            expectedProfileDto = new Profile()
            {
                Id = "0000123",
                LastName = "Smith",
                FirstName = "Barney",
                PreferredName = "Barney L. Smith II",
                PreferredEmailAddress = "barneysmith@xyzsmithbarney.com",
                BirthDate = new DateTime(1963, 01, 28),
                Addresses = dtoAddresses,
                EmailAddresses = new List<EmailAddress>()
                {
                    new EmailAddress { IsPreferred = true, TypeCode = "SEC", Value = "a@b.c" },
                    new EmailAddress { IsPreferred = false, TypeCode = "PRI", Value = "d@e.f" },
                    new EmailAddress { IsPreferred = false, TypeCode = "AAA", Value = "g@h.i" },
                    new EmailAddress { IsPreferred = false, TypeCode = "XX", Value = "xx@y.z"}
                },
                Phones = new List<Phone>()
                {
                    new Phone { TypeCode = "HO", Number = "111-111-1111" },
                    new Phone { TypeCode = "BU", Number = "222-222-2222", Extension = "12345" },
                    new Phone { TypeCode = "AA", Number = "333-333-3333" } ,
                    new Phone { TypeCode = "ZZ", Number = "864-444-4444" }
                },
                PersonalPronounCode = "HE"
            };

            configuration = new UserProfileConfiguration2()
            {
                ViewableAddressTypes = new List<string>() { "H", "B", "WB" },
                ViewableEmailTypes = new List<string>() { "AAA", "PRI", "SEC" },
                ViewablePhoneTypes = new List<string>() { "HO", "AA", "BU" },
                Text = "Contact the office with any issues in your information.\r\n\r\n Thank you.",
                AllAddressTypesAreViewable = false,
                AllEmailTypesAreViewable = false,
                AllEmailTypesAreUpdatable = false,
                AllPhoneTypesAreViewable = false,
                CanUpdateEmailWithoutPermission = false,
                CanUpdatePhoneWithoutPermission = false,
                UpdatableEmailTypes = new List<string>() { "PRI", "SEC" },
                UpdatablePhoneTypes = new List<string>() { "HO" },
                UpdatableAddressTypes = new List<string>() { "WB" },
                CanUpdateAddressWithoutPermission = false,
            };

            configuration2 = new UserProfileConfiguration2()
            {
                ViewableAddressTypes = new List<string>() { "H", "B", "WB" },
                ViewableEmailTypes = new List<string>() { "AAA", "PRI", "SEC" },
                ViewablePhoneTypes = new List<string>() { "HO", "AA", "BU" },
                Text = "Contact the office with any issues in your information.\r\n\r\n Thank you.",
                AllAddressTypesAreViewable = false,
                AllEmailTypesAreViewable = false,
                AllEmailTypesAreUpdatable = false,
                AllPhoneTypesAreViewable = false,
                CanUpdateEmailWithoutPermission = true,
                CanUpdatePhoneWithoutPermission = true,
                UpdatableEmailTypes = new List<string>() { "PRI", "SEC" },
                UpdatablePhoneTypes = new List<string>() { "HO" },
                UpdatableAddressTypes = new List<string>() { "WB" },
                CanUpdateAddressWithoutPermission = true,
            };

            configuration3 = new UserProfileConfiguration2()
            {
                ViewableAddressTypes = new List<string>() { },
                ViewableEmailTypes = new List<string>() { "AAA", "PRI", "SEC" },
                ViewablePhoneTypes = new List<string>() { "HO", "AA", "BU" },
                Text = "Contact the office with any issues in your information.\r\n\r\n Thank you.",
                AllAddressTypesAreViewable = false,
                AllEmailTypesAreViewable = false,
                AllEmailTypesAreUpdatable = false,
                AllPhoneTypesAreViewable = false,
                CanUpdateEmailWithoutPermission = true,
                CanUpdatePhoneWithoutPermission = true,
                UpdatableEmailTypes = new List<string>() { "PRI", "SEC" },
                UpdatablePhoneTypes = new List<string>() { "HO" },
                UpdatableAddressTypes = new List<string>() { },
                CanUpdateAddressWithoutPermission = true,
            };

            expectedText = "Contact the office with any issues in your information.<br /><br /> Thank you.";
            expectedAddressTypes = new List<Ellucian.Colleague.Dtos.AddressType2>()
            {
                new Ellucian.Colleague.Dtos.AddressType2{ Code = "H", Title = "Home"},
                new Ellucian.Colleague.Dtos.AddressType2{ Code = "B", Title = "Business"},
                new Ellucian.Colleague.Dtos.AddressType2{ Code = "WB", Title = "Web change request"},
            };

            expectedUpdatableAddressTypes = new List<Ellucian.Colleague.Dtos.AddressType2>()
            {
                new Ellucian.Colleague.Dtos.AddressType2{ Code = "WB", Title = "Web change request"},
            };

            expectedEmailTypes = new List<Ellucian.Colleague.Dtos.Base.EmailType>()
            {
                new Ellucian.Colleague.Dtos.Base.EmailType{ Code = "PRI", Description = "Primary"},
                new Ellucian.Colleague.Dtos.Base.EmailType{ Code = "SEC", Description = "Secondary"},
                new Ellucian.Colleague.Dtos.Base.EmailType{ Code = "CAM", Description = "Campus"},
            };

            expectedUpdatableEmailTypes = new List<Ellucian.Colleague.Dtos.Base.EmailType>() {
                new Ellucian.Colleague.Dtos.Base.EmailType { Code = "PRI", Description="Primary"},
                new Ellucian.Colleague.Dtos.Base.EmailType { Code = "SEC", Description="Secondary"},
            };

            expectedProfileEmailAddressModels = new List<ProfileEmailAddressModel>()
            {
                new ProfileEmailAddressModel { IsUpdatable = true, EmailAddress = new EmailAddress { IsPreferred = true, TypeCode = "SEC", Value = "a@b.c" }, EmailTypeDescription = "Secondary"},
                new ProfileEmailAddressModel { IsUpdatable = true, EmailAddress = new EmailAddress { IsPreferred = false, TypeCode = "PRI", Value = "d@e.f" }, EmailTypeDescription = "Primary"},
                new ProfileEmailAddressModel { IsUpdatable = false, EmailAddress = new EmailAddress { IsPreferred = false, TypeCode = "AAA", Value = "g@h.i" }, EmailTypeDescription = "AAA" }
            };

            expectedProfileEmailAddressModelsWithoutConfiguration = new List<ProfileEmailAddressModel>()
            {
                new ProfileEmailAddressModel { IsUpdatable = false, EmailAddress = new EmailAddress { IsPreferred = true, TypeCode = "SEC", Value = "a@b.c" }, EmailTypeDescription = "Secondary"},
                new ProfileEmailAddressModel { IsUpdatable = false, EmailAddress = new EmailAddress { IsPreferred = false, TypeCode = "PRI", Value = "d@e.f" }, EmailTypeDescription = "Primary"},
                new ProfileEmailAddressModel { IsUpdatable = false, EmailAddress = new EmailAddress { IsPreferred = false, TypeCode = "AAA", Value = "g@h.i" }, EmailTypeDescription = "AAA" },
                new ProfileEmailAddressModel { IsUpdatable = false, EmailAddress = new EmailAddress { IsPreferred = false, TypeCode = "XX", Value = "xx@y.z" }, EmailTypeDescription = "XX" }
            };

            expectedPhoneTypes = new List<Ellucian.Colleague.Dtos.Base.PhoneType>()
            {
                new Ellucian.Colleague.Dtos.Base.PhoneType{ Code = "BU", Description = "Business"},
                new Ellucian.Colleague.Dtos.Base.PhoneType{ Code = "CP", Description = "Cell Phone"},
                new Ellucian.Colleague.Dtos.Base.PhoneType{ Code = "HO", Description = "Home"},
            };

            expectedUpdatablePhoneTypes = new List<Ellucian.Colleague.Dtos.Base.PhoneType>()
            {
                new Ellucian.Colleague.Dtos.Base.PhoneType{ Code = "HO", Description = "Home"},
            };

            expectedProfilePhoneModels = new List<ProfilePhoneModel>()
            {
                new ProfilePhoneModel { IsUpdatable = true,  Phone = new Phone { TypeCode = "HO", Number = "111-111-1111" }, PhoneTypeDescription = "Home"},
                new ProfilePhoneModel { IsUpdatable = false,  Phone = new Phone { TypeCode = "BU", Number = "222-222-2222", Extension = "12345" }, PhoneTypeDescription = "Business"},
                new ProfilePhoneModel { IsUpdatable = false,  Phone = new Phone { TypeCode = "AA", Number = "333-333-3333" }, PhoneTypeDescription = "AA" },
            };

            expectedProfilePhoneModelsWithoutConfiguration = new List<ProfilePhoneModel>()
            {
                new ProfilePhoneModel { IsUpdatable = false,  Phone = new Phone { TypeCode = "HO", Number = "111-111-1111" }, PhoneTypeDescription = "Home"},
                new ProfilePhoneModel { IsUpdatable = false,  Phone = new Phone { TypeCode = "BU", Number = "222-222-2222", Extension = "12345" }, PhoneTypeDescription = "Business"},
                new ProfilePhoneModel { IsUpdatable = false,  Phone = new Phone { TypeCode = "AA", Number = "333-333-3333" }, PhoneTypeDescription = "AA" },
                new ProfilePhoneModel { IsUpdatable = false,  Phone = new Phone { TypeCode = "ZZ", Number = "864-444-4444" }, PhoneTypeDescription = "ZZ" }
            };

            expectedPersonalPronounTypes = new List<Ellucian.Colleague.Dtos.Base.PersonalPronounType>()
            {
                new Ellucian.Colleague.Dtos.Base.PersonalPronounType{ Code = "HE", Description = "He/him/his"},
                new Ellucian.Colleague.Dtos.Base.PersonalPronounType{ Code = "SHE", Description = "She/her/hers"},
                new Ellucian.Colleague.Dtos.Base.PersonalPronounType{ Code = "ZE", Description = "Ze/Hir/Hirs"},
            };


            var currentUserMock = new Mock<ICurrentUser>();
            currentUserMock.Setup(c => c.Roles).Returns(new List<string>() { "STUDENT", "ADVISOR" });
            currentUser = currentUserMock.Object;

            roles = new List<Role>() {
                new Role() {
                    Permissions = new List<Permission>() { new Permission() { Code="UPDATE.OWN.EMAIL"}, new Permission() { Code="UPDATE.OWN.PHONE"}, new Permission() { Code="UPDATE.OWN.ADDRESS"}},
                    Title = "STUDENT"
                },
                new Role() {
                    Permissions = new List<Permission>() {},
                    Title = "ADVISOR"
                },
            };
        }

        [TestMethod]
        public void UserProfileViewModel_ValidParameters_CreatesViewModel()
        {
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            var expectedUserProfileModel = new UserProfileModel(expectedProfileDto);
            Assert.AreEqual(expectedUserProfileModel.Id, userProfileViewModel.UserProfileModel.Id);
            CollectionAssert.AreEqual(expectedProfileEmailAddressModels, userProfileViewModel.ViewableEmailAddresses, new ProfileEmailAddressModelComparer());
            CollectionAssert.AreEqual(expectedProfilePhoneModels, userProfileViewModel.ViewablePhones, new ProfilePhoneModelComparer());
            Assert.AreEqual(configuration.CanUpdateEmailWithoutPermission, userProfileViewModel.Configuration.CanUpdateEmailWithoutPermission);
            // User should be STUDENT, and STUDENT should have permission
            Assert.IsTrue(userProfileViewModel.UserHasEmailUpdatePermission);
            Assert.AreEqual(expectedText, userProfileViewModel.Text);
            Assert.AreEqual(expectedProfileDto.PersonalPronounCode, userProfileViewModel.UserProfileModel.PersonalPronounCode);
        }

        [TestMethod]
        public void UserProfileViewModel_UpdatableEmailTypesAreCorrect()
        {
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            CollectionAssert.AreEqual(new List<EmailType>(expectedUpdatableEmailTypes), new List<EmailType>(userProfileViewModel.UpdatableEmailTypes), new EmailTypeComparer());
        }

        [TestMethod]
        public void UserProfileViewModel_UpdatablePhoneTypesAreCorrect()
        {
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            CollectionAssert.AreEqual(new List<PhoneType>(expectedUpdatablePhoneTypes), new List<PhoneType>(userProfileViewModel.UpdatablePhoneTypes), new PhoneTypeComparer());
        }

        [TestMethod]
        public void UserProfileViewModel_IfInstitutionDoesNotRequireRoleForEmailUpdatePermission_CanUpdateEmail()
        {
            roles = new List<Role>();
            configuration.CanUpdateEmailWithoutPermission = true;
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            Assert.IsTrue(userProfileViewModel.UserHasEmailUpdatePermission);
        }

        [TestMethod]
        public void UserProfileViewModel_IfUserIsNotInRoleWithEmailUpdatePermission_CannotUpdateEmail()
        {
            roles = new List<Role>();
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            Assert.IsFalse(userProfileViewModel.UserHasEmailUpdatePermission);
        }

        [TestMethod]
        public void UserProfileViewModel_IfInstitutionDoesNotRequireRoleForPhoneUpdatePermission_CanUpdatePhone()
        {
            roles = new List<Role>();
            configuration.CanUpdatePhoneWithoutPermission = true;
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            Assert.IsTrue(userProfileViewModel.UserHasPhoneUpdatePermission);
        }

        [TestMethod]
        public void UserProfileViewModel_IfUserIsNotInRoleWithPhoneUpdatePermission_CannotUpdatePhone()
        {
            roles = new List<Role>();
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            Assert.IsFalse(userProfileViewModel.UserHasPhoneUpdatePermission);
        }

        [TestMethod]
        public void UserProfileViewModel_BuildsIfConfigurationIsNull()
        {
            configuration = null;
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            Assert.AreEqual(expectedProfileDto.Id, userProfileViewModel.UserProfileModel.Id);
            // Configuration was null, assume we can't update at that point
            Assert.AreEqual(false, userProfileViewModel.UserHasEmailUpdatePermission);
            Assert.AreEqual(false, userProfileViewModel.UserHasPhoneUpdatePermission);
            CollectionAssert.AreEqual(expectedProfileEmailAddressModelsWithoutConfiguration, userProfileViewModel.ViewableEmailAddresses, new ProfileEmailAddressModelComparer());
            CollectionAssert.AreEqual(expectedProfilePhoneModelsWithoutConfiguration, userProfileViewModel.ViewablePhones, new ProfilePhoneModelComparer());
        }

        [TestMethod]
        public void UserProfileViewModel_BuildsIfConfigurationTextIsNull()
        {
            configuration.Text = null;
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            Assert.AreEqual(expectedProfileDto.Id, userProfileViewModel.UserProfileModel.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UserProfileViewModel_WhenProfileIsNull_ShouldThrow()
        {
            expectedProfileDto = null;
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UserProfileViewModel_WhenCurrentUserIsNull_ShouldThrow()
        {
            currentUser = null;
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UserProfileViewModel_WhenRolesIsNull_ShouldThrow()
        {
            roles = null;
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
        }

        [TestMethod]
        public void UserProfileViewModel_UpdatableAddressTypesAreCorrect()
        {
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            CollectionAssert.AreEqual(new List<Ellucian.Colleague.Dtos.AddressType2>(expectedUpdatableAddressTypes), new List<Ellucian.Colleague.Dtos.AddressType2>(userProfileViewModel.UpdatableAddressTypes), new AddressType2Comparer());
        }
        [TestMethod]
        public void UserProfileViewModel_UpdatableAddressTypesAreCorrectAltConfigWithoutPermissionSettingsAreTrue()
        {
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration2, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            CollectionAssert.AreEqual(new List<Ellucian.Colleague.Dtos.AddressType2>(expectedUpdatableAddressTypes), new List<Ellucian.Colleague.Dtos.AddressType2>(userProfileViewModel.UpdatableAddressTypes), new AddressType2Comparer());
        }

        [TestMethod]
        public void UserProfileViewModel_NoViewableAddresses_AltConfig_AllAddressTypesViewableIsFalseWithNoAddressTypes()
        {
            var userProfileViewModel = new UserProfileViewModel(expectedProfileDto, configuration3, expectedPhoneTypes, expectedEmailTypes, expectedAddressTypes, currentUser, roles, countries, states, expectedPersonalPronounTypes);
            Assert.AreEqual(0, userProfileViewModel.ViewableAddresses.Count());
        }

        private class AddressType2Comparer : Comparer<Ellucian.Colleague.Dtos.AddressType2>
        {
            public override int Compare(Ellucian.Colleague.Dtos.AddressType2 x, Ellucian.Colleague.Dtos.AddressType2 y)
            {
                if (x.Code != y.Code) return -1;
                if (x.Title != y.Title) return -1;
                return 0;
            }
        }

        private class EmailTypeComparer : Comparer<EmailType>
        {
            public override int Compare(EmailType x, EmailType y)
            {
                if (x.Code != y.Code) return -1;
                if (x.Description != y.Description) return -1;
                return 0;
            }
        }

        private class PhoneTypeComparer : Comparer<PhoneType>
        {
            public override int Compare(PhoneType x, PhoneType y)
            {
                if (x.Code != y.Code) return -1;
                if (x.Description != y.Description) return -1;
                return 0;
            }
        }

        private class ProfileEmailAddressModelComparer : Comparer<ProfileEmailAddressModel>
        {

            public override int Compare(ProfileEmailAddressModel x, ProfileEmailAddressModel y)
            {
                if (x.EmailTypeDescription != y.EmailTypeDescription) return -1;
                if (x.IsUpdatable != y.IsUpdatable) return -1;
                if (x.EmailAddress.IsPreferred != y.EmailAddress.IsPreferred) return -1;
                if (x.EmailAddress.TypeCode != y.EmailAddress.TypeCode) return -1;
                if (x.EmailAddress.Value != y.EmailAddress.Value) return -1;
                return 0;
            }
        }

        private class ProfilePhoneModelComparer : Comparer<ProfilePhoneModel>
        {

            public override int Compare(ProfilePhoneModel x, ProfilePhoneModel y)
            {
                if (x.Phone.Number != y.Phone.Number) return -1;
                if (x.Phone.Extension != y.Phone.Extension) return -1;
                if (x.Phone.TypeCode != y.Phone.TypeCode) return -1;
                if (x.IsUpdatable != y.IsUpdatable) return -1;
                if (x.PhoneTypeDescription != y.PhoneTypeDescription) return -1;
                return 0;
            }
        }
    }
}
