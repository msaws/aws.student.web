﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
var Ellucian = Ellucian || {};
Ellucian.Site = Ellucian.Site || {};
Ellucian.Site.Resources = Ellucian.Site.Resources || {};
Ellucian.Site.Resources = {
    PersonRestrictionsWidgetTitle: '@Resources.SiteResources.PersonRestrictionsWidgetTitle',
    PersonRestrictionsWidgetError: '@Resources.SiteResources.PersonRestrictionsWidgetError'
};

define(['Ellucian.Web.Student/Scripts/Components/PersonRestrictionsWidget/_PersonRestrictionsWidget'],

function (component, events) {

    describe("PersonRestrictionsWidget Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var PersonRestrictionsViewModel = component.viewModel;
        var params = {};
        var past = new Date();
        var today = new Date();
        var future = new Date();
        var data = {}
        var viewModel;
        beforeEach(function () {
            params = { personId: ko.observable("0001234"), isOpen: ko.observable(true) };
            past.setDate(future.getDate() - 30);
            future.setDate(future.getDate() - 30);
            data = {
                Restrictions: [{
                    Details: "Open Registration for the current term will start soon!",
                    EndDate: future,
                    Hyperlink: "http://www.ellucian.com/sn",
                    HyperlinkText: "Register Here!",
                    Id: "123",
                    OfficeUseOnly: false,
                    RestrictionId: "SN",
                    Severity: 5,
                    StartDate: past,
                    StudentId: "0001234",
                    Title: "Information"
                }]
            }
            viewModel = new PersonRestrictionsViewModel(params);
        });

        describe("personId: ", function () {

            it("error thrown if params.personId is undefined", function () {
                params = { isOpen: ko.observable(true) };
                expect(function () { PersonRestrictionsViewModel(params); }).toThrow();
            });

            it("personId set to params.personId value when params.personId is observable", function () {
                expect(viewModel.personId()).toEqual(params.personId());
            });

            it("personId set to params.personId value when params.personId is not observable", function () {
                params = { personId: 0001234, isOpen: ko.observable(true) };
                viewModel = new PersonRestrictionsViewModel(params);
                expect(viewModel.personId()).toEqual(params.personId);
            });
        });

        describe("isOpen: ", function () {

            it("isOpen set to false if params.isOpen is undefined", function () {
                params = { personId: ko.observable("0001234") };
                viewModel = new PersonRestrictionsViewModel(params);
                expect(viewModel.isOpen()).toBeFalsy();
            });

            it("isOpen set to params.isOpen value when params.isOpen is observable", function () {
                expect(viewModel.isOpen()).toEqual(params.isOpen());
            });

            it("isOpen set to params.isOpen value when params.isOpen is not observable", function () {
                params = { personId: 0001234, isOpen: true };
                viewModel = new PersonRestrictionsViewModel(params);
                expect(viewModel.isOpen()).toEqual(params.isOpen);
            });
        });

        describe("Restrictions: ", function () {

            it("is empty array on initialization", function () {
                expect(viewModel.Restrictions()).toEqual([]);
            });
        });

        describe("personIdChange: ", function () {

            it("getRestrictions is called when personId changes", function () {
                spyOn(viewModel, "getRestrictions");
                viewModel.personId("0002468");
                expect(viewModel.getRestrictions).toHaveBeenCalled();
            });
        });

        describe("getRestrictions: ", function () {

            it("calls ko.mapping.fromJS when successful", function () {
                spyOn(ko.mapping, 'fromJS').and.callThrough();
                spyOn($, "ajax").and.callFake(function (e) {
                    return $.Deferred().resolve(data);
                });

                viewModel.getRestrictions();
                expect(ko.mapping.fromJS).toHaveBeenCalledWith(data, {}, viewModel);
            });

            it("getRestrictions calls $.fn.notificationCenter when an error occurs", function () {
                spyOn($.fn, "notificationCenter").and.callThrough();
                spyOn($, "ajax").and.callFake(function (e) {
                    return $.Deferred().reject({ status: "404" });
                });
                viewModel.getRestrictions();
                expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: Ellucian.Site.Resources.PersonRestrictionsWidgetError, type: "error" });
            });
        });

        it("dispose calls personIdChange.dispose when called", function () {
            spyOn(viewModel.personIdChange, "dispose");
            viewModel.dispose();
            expect(viewModel.personIdChange.dispose).toHaveBeenCalled();
        });
    });
});