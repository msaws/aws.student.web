﻿describe("preferencesViewModel", function () {

    var viewModel;

    var preferences = { Homepage: "someUrl" };

    var repository = Ellucian.UserProfile.preferencesRepository;

    beforeEach(function () {
        viewModel = new Ellucian.UserProfile.PreferencesViewModel(repository);
    });

    describe("Constructor", function () {
        it(":should set Homepage to empty string", function () {
            expect(viewModel.Homepage()).toBe("");
        });
        it(":should set isWaiting to false", function () {
            expect(viewModel.isWaiting()).toBeFalsy();
        });
    });

    describe("get", function () {
        it("should update the view model Homepage attribute, set waitingMessage, toggle isWaiting, and call repository get", function (done) {
            var promise = new Promise(function (resolve) { resolve(preferences); });

            spyOn(repository, 'get').and.callFake(function (e) {
                return promise;
            });

            viewModel.get();

            promise.then(function () {

                expect(repository.get).toHaveBeenCalled();
                expect(viewModel.waitingMessage()).toBe(Ellucian.UserProfile.preferencesMessages.loadingMessage);
                expect(viewModel.Homepage()).toBe(preferences.Homepage);

                expect(viewModel.isWaiting()).toBe(false);
                done();
            });

        });

        it(":should not get the Homepage attribute on an error", function (done) {

            var previousValue = viewModel.Homepage();

            var promise = new Promise(function (resolve, reject) { reject(preferences); });

            spyOn(repository, 'get').and.callFake(function (e) {
                return promise;
            });

            viewModel.get();

            promise.catch(function () {
                // no-op
            }).then(function () {

                expect(repository.get).toHaveBeenCalled();
                expect(viewModel.waitingMessage()).toBe(Ellucian.UserProfile.preferencesMessages.loadingMessage);
                expect(viewModel.Homepage()).toBe(previousValue);

                expect(viewModel.isWaiting()).toBe(false);
                done();
            });

        });

    });

    describe("update", function () {
        it(":should update the view model Homepage attribute, set waitingMessage, toggle isWaiting, and call repository update", function (done) {

            var promise = new Promise(function (resolve) { resolve(preferences); });

            spyOn(repository, 'update').and.callFake(function (e) {
                return promise;
            });

            viewModel.update();

            promise.then(function () {

                expect(repository.update).toHaveBeenCalled();
                expect(viewModel.waitingMessage()).toBe(Ellucian.UserProfile.preferencesMessages.waitingUpdateMessage);
                expect(viewModel.Homepage()).toBe(preferences.Homepage);

                expect(viewModel.isWaiting()).toBe(false);
                done();
            });

        });

        it(":should not update the Homepage attribute on an error", function (done) {

            var previousValue = viewModel.Homepage();

            var promise = new Promise(function (resolve, reject) { reject(preferences); });

            spyOn(repository, 'update').and.callFake(function (e) {
                return promise;
            });

            viewModel.update();

            promise.catch(function () {
                // no-op
            }).then(function () {

                expect(repository.update).toHaveBeenCalled();
                expect(viewModel.waitingMessage()).toBe(Ellucian.UserProfile.preferencesMessages.waitingUpdateMessage);
                expect(viewModel.Homepage()).toBe(previousValue);

                expect(viewModel.isWaiting()).toBe(false);
                done();
            });

        });

    });
});