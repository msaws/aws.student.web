﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

describe("preferencesRepository", function () {

    var repository = Ellucian.UserProfile.preferencesRepository;

    var testPreferences = { "id": "12345" }; // Doesn't matter what data is in the object - we're just testing that get retreived something
    var testPreferences2 = { "id": "12345", "foo": "bar" }; // A second test preferences, for testing updates

    beforeEach(function () {
        account = {
            handleInvalidSessionResponse: function (data) {
                return false;
            }
        };
    });

    describe("get", function () {

        it(":gets preferences", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPreferences);
            });

            repository.get().then(function (result) {
                expect(result).toEqual(testPreferences);
                done();
            });
        });

        it(":returns error when unable to retreive preferences", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            repository.get().catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.preferencesMessages.getFailed);

                done();
            });
        });
    });

    describe("update", function () {

        it(":gets updated preferences", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPreferences2);
            });

            repository.update(testPreferences).then(function (result) {

                expect($.ajax).toHaveBeenCalledWith({
                    url: Ellucian.UserProfile.preferencesRepositoryUrls.update,
                    data: JSON.stringify({ preferencesJson: ko.toJSON(testPreferences) }),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                });

                expect(result).toEqual(testPreferences2);

                done();
            });
        });

        it(":returns error when update fails", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            repository.update().catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.preferencesMessages.updateFailed);

                done();
            });
        });

        it(":returns error on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPreferences2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            repository.update().catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.preferencesMessages.updateFailed);

                done();
            });
        });
    });
});