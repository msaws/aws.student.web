﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("UserProfileViewModel", function () {

    var viewModel;
    var data;
    var repository = Ellucian.UserProfile.userProfileRepository

    beforeEach(function () {
        // Sample data used to populate the view model
        data = {
            "UserProfileModel": {
                "FullName": "Ethan Evans",
                "DateOfBirth": "2/2/1990",
                "AddressConfirmationDate": "10/5/2015",
                "EmailAddressConfirmationDate": "10/5/2015",
                "PhoneConfirmationDate": "10/5/2015",
                "Id": "0012578",
                "LastName": "Evans",
                "FirstName": "Ethan",
                "PreferredName": null,
                "BirthDate": "\/Date(633934800000)\/",
                "PersonalPronounCode": "HE",
                "PreferredEmailAddress": "eevans15@ellucian.edu",
                "Addresses": [
                    {
                        "AddressId": "6198",
                        "Type": "Vacation",
                        "AddressLines": ["123 Main St"],
                        "AddressModifier": "",
                        "City": "Anytown",
                        "State": "MA",
                        "PostalCode": "12345",
                        "County": "",
                        "Country": "United States of America",
                        "RouteCode": "",
                        "PhoneNumbers": [{ "Number": "123-456-7892", "Extension": "1", "TypeCode": "CP" }, { "Number": "3213213214", "Extension": "", "TypeCode": "CP" }, { "Number": "987-654-3212", "Extension": "", "TypeCode": "FH" }],
                        "AddressLabel": ["123 Main St", "Anytown, MA 12345", "UNITED STATES OF AMERICA"],
                        "PersonId": "0012558",
                        "EffectiveStartDate": "\/Date(1444363200000)\/",
                        "EffectiveEndDate": null,
                        "IsPreferredAddress": true,
                        "IsPreferredResidence": true,
                        "TypeCode": "V",
                        "CountryCode": null
                    },
                {
                    "AddressId": "6189",
                    "Type": "Home/Permanent",
                    "AddressLines": ["725 Beach Avenue", "Suite J"],
                    "AddressModifier": "",
                    "City": "Virginia Beach",
                    "State": "VA",
                    "PostalCode": "22078",
                    "County": "",
                    "Country": "",
                    "RouteCode": "",
                    "PhoneNumbers": [{ "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" }, { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }],
                    "AddressLabel": ["725 Beach Avenue", "Suite J", "Virginia Beach, VA 22078"],
                    "PersonId": "0012578",
                    "EffectiveStartDate": "\/Date(1444017600000)\/",
                    "EffectiveEndDate": null,
                    "IsPreferredAddress": false,
                    "IsPreferredResidence": false,
                    "TypeCode": "H",
                    "CountryCode": null
                }
                ],
                "EmailAddresses": [{
                    "Value": "eevans15@ellucian.edu",
                    "TypeCode": "COL",
                    "IsPreferred": true
                },
                {
                    "Value": "ethan1990@gmail.com",
                    "TypeCode": "PRI",
                    "IsPreferred": false
                }],
                "Phones": [{
                    "Number": "703-555-5678",
                    "Extension": "",
                    "TypeCode": "CP"
                },
                {
                    "Number": "704-555-0118",
                    "Extension": "",
                    "TypeCode": "HO"
                }],
                "AddressConfirmationDateTime": "\/Date(1444054417000)\/",
                "EmailAddressConfirmationDateTime": "\/Date(1444054429000)\/",
                "PhoneConfirmationDateTime": "\/Date(1444054450000)\/",
                "LastChangedDateTime": "\/Date(1444066337000)\/"
            },
            "Configuration": {
                "ViewableAddressTypes": [],
                "ViewableEmailTypes": ["WEB", "PRI", "SEC", "BUS", "COL"],
                "ViewablePhoneTypes": ["PA", "FH", "CP", "HO"],
                "Text": "Please inform the records office if you have a change to your personal information that needs to be reported. We appreciate your cooperation in this matter, and soon we will be restricting access to other system functionality until you have confirmed that your information is indeed current.\r\n\r\nWe appreciate your cooperation in this matter. For more information about our organization, please google us on \u003ca href=\"http://www.google.com\"\u003eGoogle\u003c/a\u003e",
                "AllAddressTypesAreViewable": true,
                "AllEmailTypesAreViewable": false,
                "AllPhoneTypesAreViewable": false,
                "AllEmailTypesAreUpdatable": false,
                "UpdatableEmailTypes": ["WEB", "PRI", "SEC", "BUS"],
                "CanUpdateEmailWithoutPermission": false,
                "UpdatablePhoneTypes": ["PA", "CP"],
                "CanUpdatePhoneWithoutPermission": false,
                "UpdatableAddressTypes": [],
                "CanUpdateAddressWithoutPermission": false,
            },
            "PhoneTypes": [{ "Code": "BU", "Description": "Business" }, { "Code": "CP", "Description": "Cell Phone" }, { "Code": "FH", "Description": "Fax Home / Permanent" }, { "Code": "HO", "Description": "Home / Permanent" }, { "Code": "CA", "Description": "Campus" }, { "Code": "PA", "Description": "Pager" }, { "Code": "OR", "Description": "Organization" }, { "Code": "OF", "Description": "Office Fax" }, { "Code": "AL", "Description": "Alternate Phone Number" }, { "Code": "FB", "Description": "Fax Business Number" }, { "Code": "UN", "Description": "Unlisted" }, { "Code": "TD", "Description": "Hearing Impaired" }, { "Code": "EM", "Description": "Emergency Phone" }, { "Code": "TF", "Description": "Toll Free" }], "UpdatablePhoneTypes": [{ "Code": "CP", "Description": "Cell Phone" }, { "Code": "PA", "Description": "Pager" }],
            "EmailTypes": [{ "Code": "PRI", "Description": "Primary" }, { "Code": "SEC", "Description": "Secondary (Info Only)" }, { "Code": "WEB", "Description": "Webpage (Info Only)" }, { "Code": "BUS", "Description": "Business / Employment" }, { "Code": "COL", "Description": "College Email" }, { "Code": "EDI", "Description": "EDI Delivered Email" }],
            "AddressTypes": [{ "Code": "LO", "Title": "Local" }, { "Code": "WB", "Title": "Web address change request" }, { "Code": "H", "Title": "Home address" }],
            "UpdatableEmailTypes": [{ "Code": "PRI", "Description": "Primary" }, { "Code": "SEC", "Description": "Secondary (Info Only)" }, { "Code": "WEB", "Description": "Webpage (Info Only)" }, { "Code": "BUS", "Description": "Business / Employment" }],
            "UpdatableAddressTypes": [{ "Code": "WB", "Description": "Web address change request" }],
            "PersonalPronounTypes": [{ "Code": "HE", "Description": "He/Him/His"}, {"Code": "SHE", "Description": "She/Her/Hers"}, { "Code": "ZE", "Description": "Ze/Zir/Zirs" }],
            "ViewableAddresses": [{
                "Address": {
                    "AddressId": "6186",
                    "Type": "Local/Mailing",
                    "AddressLines": ["567 Spring Forest Way"],
                    "AddressModifier": "",
                    "City": "Fairfax",
                    "State": "VA",
                    "PostalCode": "22033",
                    "County": "",
                    "Country": "",
                    "RouteCode": "",
                    "PhoneNumbers": [{ "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" }, { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }],
                    "AddressLabel": ["567 Spring Forest Way", "Fairfax, VA 22033"],
                    "PersonId": "0012578",
                    "EffectiveStartDate": "\/Date(1444017600000)\/",
                    "EffectiveEndDate": null,
                    "IsPreferredAddress": true,
                    "IsPreferredResidence": true,
                    "TypeCode": "LO"
                },
                "IsUpdatable": false,
                "AddressTypeDescription": "Local/Mailing"
            },
            {
                "Address": {
                    "AddressId": "6189",
                    "Type": "Home/Permanent",
                    "AddressLines": ["725 Beach Avenue", "Suite J"],
                    "AddressModifier": "",
                    "City": "Virginia Beach",
                    "State": "VA",
                    "PostalCode": "22078",
                    "County": "",
                    "Country": "",
                    "RouteCode": "",
                    "PhoneNumbers": [{ "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" }, { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }],
                    "AddressLabel": ["725 Beach Avenue", "Suite J", "Virginia Beach, VA 22078"],
                    "PersonId": "0012578",
                    "EffectiveStartDate": "\/Date(1444017600000)\/",
                    "EffectiveEndDate": null,
                    "IsPreferredAddress": false,
                    "IsPreferredResidence": false,
                    "TypeCode": "H"
                },
                "IsUpdatable": false,
                "AddressTypeDescription": "Home/Permanent"
            }],
            "ViewableEmailAddresses": [{
                "EmailAddress": {
                    "Value": "eevans15@ellucian.edu",
                    "TypeCode": "COL",
                    "IsPreferred": true
                },
                "IsUpdatable": false,
                "EmailTypeDescription": "College Email"
            },
            {
                "EmailAddress": {
                    "Value": "ethan1990@gmail.com",
                    "TypeCode": "PRI",
                    "IsPreferred": false
                },
                "IsUpdatable": true,
                "EmailTypeDescription": "Primary"
            }],
            "ViewablePhones": [{
                "IsUpdatable": false,
                "Phone": {
                    "Number": "703-555-5678",
                    "Extension": "",
                    "TypeCode": "CP"
                },
                "PhoneTypeDescription": "Cell Phone"
            },
            {
                "IsUpdatable": false,
                "Phone": {
                    "Number": "704-555-0118",
                    "Extension": "",
                    "TypeCode": "HO"
                },
                "PhoneTypeDescription": "Home / Permanent"
            }],
            "Text": "Please inform the records office if you have a change to your personal information that needs to be reported. We appreciate your cooperation in this matter, and soon we will be restricting access to other system functionality until you have confirmed that your information is indeed current.\u003cbr /\u003e\u003cbr /\u003eWe appreciate your cooperation in this matter. For more information about our organization, please google us on \u003ca href=\"http://www.google.com\"\u003eGoogle\u003c/a\u003e",
            "Notification": null,
            "UserHasEmailUpdatePermission": true,
            "UserHasPhoneUpdatePermission": true,
            "UserHasAddressUpdatePermission": true,
            "Countries": [{ "Code": "AF", "Description": "AFGHANISTAN", "IsoCode": "AF", "IsNotInUse": false }, { "Code": "AL", "Description": "ALBANIA", "IsoCode": "AL", "IsNotInUse": false }, { "Code": "AG", "Description": "ALGERIA", "IsoCode": "AG", "IsNotInUse": false }, { "Code": "AN", "Description": "ANDORRA", "IsoCode": "AN", "IsNotInUse": false }, { "Code": "AO", "Description": "ANGOLA", "IsoCode": "AO", "IsNotInUse": false }, { "Code": "AV", "Description": "ANGUILLA", "IsoCode": "AV", "IsNotInUse": false }, { "Code": "AY", "Description": "ANTARCTICA", "IsoCode": "AY", "IsNotInUse": false }, { "Code": "AC", "Description": "ANTIGUA AND BARBUDA", "IsoCode": "AC", "IsNotInUse": false }, { "Code": "AR", "Description": "ARGENTINA", "IsoCode": "AR", "IsNotInUse": false }, { "Code": "AM", "Description": "ARMENIA", "IsoCode": "AM", "IsNotInUse": false }, { "Code": "AA", "Description": "ARUBA", "IsoCode": "AA", "IsNotInUse": false }, { "Code": "AS", "Description": "AUSTRALIA", "IsoCode": "..", "IsNotInUse": false }, { "Code": "AU", "Description": "AUSTRIA", "IsoCode": "AU", "IsNotInUse": false }, { "Code": "BF", "Description": "BAHAMAS", "IsoCode": "BF", "IsNotInUse": false }, { "Code": "BA", "Description": "BAHRAIN", "IsoCode": "BA", "IsNotInUse": false }, { "Code": "BG", "Description": "BANGLADESH", "IsoCode": "BG", "IsNotInUse": false }, { "Code": "BB", "Description": "BARBADOS", "IsoCode": "BB", "IsNotInUse": false }, { "Code": "BO", "Description": "BELARUS", "IsoCode": "BO", "IsNotInUse": false }, { "Code": "BE", "Description": "BELGIUM", "IsoCode": "BE", "IsNotInUse": false }, { "Code": "BH", "Description": "BELIZE", "IsoCode": "BH", "IsNotInUse": false }, { "Code": "BN", "Description": "BENIN", "IsoCode": "BN", "IsNotInUse": false }, { "Code": "BD", "Description": "BERMUDA", "IsoCode": "BM", "IsNotInUse": false }, { "Code": "BT", "Description": "BHUTAN", "IsoCode": "BT", "IsNotInUse": false }, { "Code": "BL", "Description": "BOLIVIA", "IsoCode": "BL", "IsNotInUse": false }, { "Code": "BK", "Description": "BOSNIA AND HERZEGOVINA", "IsoCode": "BK", "IsNotInUse": false }, { "Code": "BC", "Description": "BOTSWANA", "IsoCode": "BC", "IsNotInUse": false }, { "Code": "BV", "Description": "BOUVET ISLAND", "IsoCode": "BV", "IsNotInUse": false }, { "Code": "BR", "Description": "BRAZIL", "IsoCode": "BR", "IsNotInUse": false }, { "Code": "IO", "Description": "BRITISH INDIAN OCEAN TERRITORY", "IsoCode": "IO", "IsNotInUse": false }, { "Code": "BX", "Description": "BRUNEI", "IsoCode": "BX", "IsNotInUse": false }, { "Code": "BU", "Description": "BULGARIA", "IsoCode": "BU", "IsNotInUse": false }, { "Code": "UV", "Description": "BURKINA FASO", "IsoCode": "UV", "IsNotInUse": false }, { "Code": "BY", "Description": "BURUNDI", "IsoCode": "BY", "IsNotInUse": false }, { "Code": "CB", "Description": "CAMBODIA", "IsoCode": "CB", "IsNotInUse": false }, { "Code": "CM", "Description": "CAMEROON", "IsoCode": "CM", "IsNotInUse": false }, { "Code": "CA", "Description": "CANADA", "IsoCode": "CA", "IsNotInUse": false }, { "Code": "CV", "Description": "CAPE VERDE", "IsoCode": "CV", "IsNotInUse": false }, { "Code": "CJ", "Description": "CAYMAN ISLANDS", "IsoCode": "CJ", "IsNotInUse": false }, { "Code": "CT", "Description": "CENTRAL AFRICAN REPUBLIC", "IsoCode": "CT", "IsNotInUse": false }, { "Code": "CD", "Description": "CHAD", "IsoCode": "CD", "IsNotInUse": false }, { "Code": "CI", "Description": "CHILE", "IsoCode": "CI", "IsNotInUse": false }, { "Code": "CH", "Description": "CHINA", "IsoCode": "CH", "IsNotInUse": false }, { "Code": "KT", "Description": "CHRISTMAS ISLAND", "IsoCode": "KT", "IsNotInUse": false }, { "Code": "IP", "Description": "CLIPPERTON ISLAND", "IsoCode": "IP", "IsNotInUse": false }, { "Code": "CK", "Description": "COCOS (KEELING) ISLANDS", "IsoCode": "CK", "IsNotInUse": false }, { "Code": "CO", "Description": "COLOMBIA", "IsoCode": "CO", "IsNotInUse": false }, { "Code": "CN", "Description": "COMOROS", "IsoCode": "CN", "IsNotInUse": false }, { "Code": "CF", "Description": "CONGO", "IsoCode": "CF", "IsNotInUse": false }, { "Code": "CG", "Description": "CONGO, DEMOCRATIC REPUBLIC", "IsoCode": "CG", "IsNotInUse": false }, { "Code": "CW", "Description": "COOK ISLANDS", "IsoCode": "CW", "IsNotInUse": false }, { "Code": "CS", "Description": "COSTA RICA", "IsoCode": "CS", "IsNotInUse": false }, { "Code": "IV", "Description": "COTE DIVOIRE", "IsoCode": "IV", "IsNotInUse": false }, { "Code": "HR", "Description": "CROATIA", "IsoCode": "HR", "IsNotInUse": false }, { "Code": "CU", "Description": "CUBA", "IsoCode": "CU", "IsNotInUse": false }, { "Code": "CY", "Description": "CYPRUS", "IsoCode": "CY", "IsNotInUse": false }, { "Code": "U1", "Description": "CZECH REPUBLIC", "IsoCode": "U1", "IsNotInUse": false }, { "Code": "DA", "Description": "DENMARK", "IsoCode": "DA", "IsNotInUse": false }, { "Code": "DJ", "Description": "DJIBOUTI", "IsoCode": "DJ", "IsNotInUse": false }, { "Code": "DO", "Description": "DOMINICA", "IsoCode": "DO", "IsNotInUse": false }, { "Code": "DR", "Description": "DOMINICAN REPUBLIC", "IsoCode": "DR", "IsNotInUse": false }, { "Code": "TT", "Description": "EAST TIMOR", "IsoCode": "TT", "IsNotInUse": false }, { "Code": "EC", "Description": "ECUADOR", "IsoCode": "EC", "IsNotInUse": false }, { "Code": "EG", "Description": "EGYPT", "IsoCode": "EG", "IsNotInUse": false }, { "Code": "ES", "Description": "EL SALVADOR", "IsoCode": "ES", "IsNotInUse": false }, { "Code": "EK", "Description": "EQUATORIAL GUINEA", "IsoCode": "EK", "IsNotInUse": false }, { "Code": "ER", "Description": "ERITREA", "IsoCode": "ER", "IsNotInUse": false }, { "Code": "EN", "Description": "ESTONIA", "IsoCode": "EN", "IsNotInUse": false }, { "Code": "ET", "Description": "ETHIOPIA", "IsoCode": "ET", "IsNotInUse": false }, { "Code": "FO", "Description": "FAROE ISLANDS", "IsoCode": "FO", "IsNotInUse": false }, { "Code": "FJ", "Description": "FIJI", "IsoCode": "FJ", "IsNotInUse": false }, { "Code": "FI", "Description": "FINLAND", "IsoCode": "FI", "IsNotInUse": false }, { "Code": "FR", "Description": "FRANCE", "IsoCode": "FR", "IsNotInUse": false }, { "Code": "FD", "Description": "Freedonia", "IsoCode": "FD", "IsNotInUse": false }, { "Code": "FG", "Description": "FRENCH GUIANA", "IsoCode": "FG", "IsNotInUse": false }, { "Code": "FP", "Description": "FRENCH POLYNESIA", "IsoCode": "FP", "IsNotInUse": false }, { "Code": "GB", "Description": "GABON", "IsoCode": "GB", "IsNotInUse": false }, { "Code": "GA", "Description": "GAMBIA", "IsoCode": "GA", "IsNotInUse": false }, { "Code": "GG", "Description": "GEORGIA", "IsoCode": "GG", "IsNotInUse": false }, { "Code": "GM", "Description": "GERMANY", "IsoCode": "GM", "IsNotInUse": false }, { "Code": "GH", "Description": "GHANA", "IsoCode": "GH", "IsNotInUse": false }, { "Code": "GI", "Description": "GIBRALTAR", "IsoCode": "GI", "IsNotInUse": false }, { "Code": "GR", "Description": "GREECE", "IsoCode": "GR", "IsNotInUse": false }, { "Code": "GL", "Description": "GREENLAND", "IsoCode": "GL", "IsNotInUse": false }, { "Code": "GJ", "Description": "GRENADA", "IsoCode": "GJ", "IsNotInUse": false }, { "Code": "GP", "Description": "GUADELOUPE", "IsoCode": "GP", "IsNotInUse": false }, { "Code": "GQ", "Description": "GUAM", "IsoCode": "GQ", "IsNotInUse": false }, { "Code": "GT", "Description": "GUATEMALA", "IsoCode": "GT", "IsNotInUse": false }, { "Code": "GK", "Description": "GUERNSEY", "IsoCode": "GK", "IsNotInUse": false }, { "Code": "GV", "Description": "GUINEA", "IsoCode": "GV", "IsNotInUse": false }, { "Code": "PU", "Description": "GUINEA-BISSAU", "IsoCode": "PU", "IsNotInUse": false }, { "Code": "GY", "Description": "GUYANA", "IsoCode": "GY", "IsNotInUse": false }, { "Code": "HA", "Description": "HAITI", "IsoCode": "HA", "IsNotInUse": false }, { "Code": "HM", "Description": "HEARD MACDONALD ISLANDS", "IsoCode": "HM", "IsNotInUse": false }, { "Code": "HO", "Description": "HONDURA", "IsoCode": "HO", "IsNotInUse": false }, { "Code": "HK", "Description": "HONG KONG", "IsoCode": "HK", "IsNotInUse": false }, { "Code": "HQ", "Description": "HOWLAND ISLAND", "IsoCode": "HQ", "IsNotInUse": false }, { "Code": "HU", "Description": "HUNGARY", "IsoCode": "HU", "IsNotInUse": false }, { "Code": "IC", "Description": "ICELAND", "IsoCode": "IC", "IsNotInUse": false }, { "Code": "BS", "Description": "INDIA", "IsoCode": "BS", "IsNotInUse": false }, { "Code": "IN", "Description": "INDIA", "IsoCode": "IN", "IsNotInUse": false }, { "Code": "ID", "Description": "INDONESIA", "IsoCode": "ID", "IsNotInUse": false }, { "Code": "IR", "Description": "IRAN, ISLAMIC REPUBLIC OF", "IsoCode": "IR", "IsNotInUse": false }, { "Code": "IZ", "Description": "IRAQ", "IsoCode": "IZ", "IsNotInUse": false }, { "Code": "EI", "Description": "IRELAND", "IsoCode": "EI", "IsNotInUse": false }, { "Code": "IM", "Description": "ISLE OF MAN", "IsoCode": "IM", "IsNotInUse": false }, { "Code": "IS", "Description": "ISRAEL", "IsoCode": "IS", "IsNotInUse": false }, { "Code": "IT", "Description": "ITALY", "IsoCode": "IT", "IsNotInUse": false }, { "Code": "JM", "Description": "JAMAICA", "IsoCode": "JM", "IsNotInUse": false }, { "Code": "JA", "Description": "JAPAN", "IsoCode": "JA", "IsNotInUse": false }, { "Code": "JE", "Description": "JERSEY", "IsoCode": "JE", "IsNotInUse": false }, { "Code": "JQ", "Description": "JOHNSTON ATOLL", "IsoCode": "JQ", "IsNotInUse": false }, { "Code": "JO", "Description": "JORDAN", "IsoCode": "JO", "IsNotInUse": false }, { "Code": "KZ", "Description": "KAZAKHSTAN", "IsoCode": "KZ", "IsNotInUse": false }, { "Code": "KE", "Description": "KENYA", "IsoCode": "KE", "IsNotInUse": false }, { "Code": "KR", "Description": "KIRIBATI", "IsoCode": "KR", "IsNotInUse": false }, { "Code": "KN", "Description": "KOREA, REPUBLIC OF", "IsoCode": "KN", "IsNotInUse": false }, { "Code": "KS", "Description": "KOREA, SOUTH", "IsoCode": "KS", "IsNotInUse": false }, { "Code": "KU", "Description": "KUWAIT", "IsoCode": "KU", "IsNotInUse": false }, { "Code": "KG", "Description": "KYRGYZSTAN", "IsoCode": "KG", "IsNotInUse": false }, { "Code": "LG", "Description": "LATVIA", "IsoCode": "LG", "IsNotInUse": false }, { "Code": "LE", "Description": "LEBANON", "IsoCode": "LE", "IsNotInUse": false }, { "Code": "LT", "Description": "LESOTHO", "IsoCode": "LT", "IsNotInUse": false }, { "Code": "LI", "Description": "LIBERIA", "IsoCode": "LI", "IsNotInUse": false }, { "Code": "LY", "Description": "LIBYAN ARAB JAMAHIRIYA", "IsoCode": "LY", "IsNotInUse": false }, { "Code": "LS", "Description": "LIECHTENSTEIN", "IsoCode": "LS", "IsNotInUse": false }, { "Code": "LH", "Description": "LITHUANIA", "IsoCode": "LH", "IsNotInUse": false }, { "Code": "LU", "Description": "LUXEMBOURG", "IsoCode": "LU", "IsNotInUse": false }, { "Code": "MC", "Description": "MACAO", "IsoCode": "MC", "IsNotInUse": false }, { "Code": "MK", "Description": "MACEDONIA, THE REPUBLIC OF", "IsoCode": "MK", "IsNotInUse": false }, { "Code": "MA", "Description": "MADAGASCAR", "IsoCode": "MA", "IsNotInUse": false }, { "Code": "MI", "Description": "MALAWI", "IsoCode": "MI", "IsNotInUse": false }, { "Code": "MY", "Description": "MALAYSIA", "IsoCode": "MY", "IsNotInUse": false }, { "Code": "MV", "Description": "MALDIVES", "IsoCode": "MV", "IsNotInUse": false }, { "Code": "ML", "Description": "MALI", "IsoCode": "ML", "IsNotInUse": false }, { "Code": "MT", "Description": "MALTA", "IsoCode": "MT", "IsNotInUse": false }, { "Code": "RM", "Description": "MARSHALL ISLANDS", "IsoCode": "RM", "IsNotInUse": false }, { "Code": "MB", "Description": "MARTINIQUE", "IsoCode": "MB", "IsNotInUse": false }, { "Code": "MR", "Description": "MAURITANIA", "IsoCode": "MR", "IsNotInUse": false }, { "Code": "MP", "Description": "MAURITIUS", "IsoCode": "MP", "IsNotInUse": false }, { "Code": "MF", "Description": "MAYOTTE", "IsoCode": "MF", "IsNotInUse": false }, { "Code": "MEX", "Description": "MEXICO", "IsoCode": "ME", "IsNotInUse": false }, { "Code": "MX", "Description": "MEXICO", "IsoCode": "MX", "IsNotInUse": false }, { "Code": "MD", "Description": "MOLDOVA, REPUBLIC OF", "IsoCode": "MD", "IsNotInUse": false }, { "Code": "MN", "Description": "MONACO", "IsoCode": "MN", "IsNotInUse": false }, { "Code": "MG", "Description": "MONGOLIA", "IsoCode": "MG", "IsNotInUse": false }, { "Code": "MH", "Description": "MONTSERRAT", "IsoCode": "MH", "IsNotInUse": false }, { "Code": "MO", "Description": "MOROCCO", "IsoCode": "MO", "IsNotInUse": false }, { "Code": "MZ", "Description": "MOZAMBIQUE", "IsoCode": "MZ", "IsNotInUse": false }, { "Code": "BM", "Description": "Myanmar", "IsoCode": "MM", "IsNotInUse": false }, { "Code": "WA", "Description": "NAMIBIA", "IsoCode": "WA", "IsNotInUse": false }, { "Code": "NR", "Description": "NAURU", "IsoCode": "NR", "IsNotInUse": false }, { "Code": "BQ", "Description": "NAVASSA ISLAND", "IsoCode": "BQ", "IsNotInUse": false }, { "Code": "NP", "Description": "NEPAL", "IsoCode": "NP", "IsNotInUse": false }, { "Code": "NL", "Description": "NETHERLANDS", "IsoCode": "NL", "IsNotInUse": false }, { "Code": "NT", "Description": "NETHERLANDS ANTILLES", "IsoCode": "NT", "IsNotInUse": false }, { "Code": "NC", "Description": "NEW CALEDONIA", "IsoCode": "NC", "IsNotInUse": false }, { "Code": "NZ", "Description": "NEW ZEALAND", "IsoCode": "NZ", "IsNotInUse": false }, { "Code": "NU", "Description": "NICARAGUA", "IsoCode": "NU", "IsNotInUse": false }, { "Code": "NG", "Description": "NIGER", "IsoCode": "NG", "IsNotInUse": false }, { "Code": "NI", "Description": "NIGERIA", "IsoCode": "NI", "IsNotInUse": false }, { "Code": "NE", "Description": "NIUE", "IsoCode": "NE", "IsNotInUse": false }, { "Code": "NF", "Description": "NORFOLK ISLAND", "IsoCode": "NF", "IsNotInUse": false }, { "Code": "NO", "Description": "NORWAY", "IsoCode": "NO", "IsNotInUse": false }, { "Code": "456", "Description": "Numeric Test 456", "IsoCode": "45", "IsNotInUse": false }, { "Code": "MU", "Description": "OMAN", "IsoCode": "MU", "IsNotInUse": false }, { "Code": "PK", "Description": "PAKISTAN", "IsoCode": "PK", "IsNotInUse": false }, { "Code": "PS", "Description": "PALAU", "IsoCode": "PS", "IsNotInUse": false }, { "Code": "LQ", "Description": "PALMYRA ATOLL", "IsoCode": "LQ", "IsNotInUse": false }, { "Code": "PM", "Description": "PANAMA", "IsoCode": "PM", "IsNotInUse": false }, { "Code": "PP", "Description": "PAPUA NEW GUINEA", "IsoCode": "PP", "IsNotInUse": false }, { "Code": "PA", "Description": "PARAGUAY", "IsoCode": "PY", "IsNotInUse": false }, { "Code": "PE", "Description": "PERU", "IsoCode": "PE", "IsNotInUse": false }, { "Code": "RP", "Description": "PHILIPPINES", "IsoCode": "RP", "IsNotInUse": false }, { "Code": "PL", "Description": "POLAND", "IsoCode": "PL", "IsNotInUse": false }, { "Code": "PO", "Description": "PORTUGAL", "IsoCode": "PO", "IsNotInUse": false }, { "Code": "RQ", "Description": "PUERTO RICO", "IsoCode": "RQ", "IsNotInUse": false }, { "Code": "QA", "Description": "QATAR", "IsoCode": "QA", "IsNotInUse": false }, { "Code": "RE", "Description": "REUNION", "IsoCode": "RE", "IsNotInUse": false }, { "Code": "RO", "Description": "ROMANIA", "IsoCode": "RO", "IsNotInUse": false }, { "Code": "RS", "Description": "RUSSIAN FEDERATION", "IsoCode": "RS", "IsNotInUse": false }, { "Code": "RW", "Description": "RWANDA", "IsoCode": "RW", "IsNotInUse": false }, { "Code": "SH", "Description": "SAINT HELENA", "IsoCode": "SH", "IsNotInUse": false }, { "Code": "SC", "Description": "SAINT KITTS AND NEVIS", "IsoCode": "SC", "IsNotInUse": false }, { "Code": "ST", "Description": "SAINT LUCIA", "IsoCode": "ST", "IsNotInUse": false }, { "Code": "SB", "Description": "SAINT PIERRE AND MIQUELON", "IsoCode": "SB", "IsNotInUse": false }, { "Code": "AQ", "Description": "SAMOA", "IsoCode": "AQ", "IsNotInUse": false }, { "Code": "WS", "Description": "SAMOA", "IsoCode": "WS", "IsNotInUse": false }, { "Code": "SM", "Description": "SAN MARINO", "IsoCode": "SM", "IsNotInUse": false }, { "Code": "TP", "Description": "SAO TOME AND PRINCIPE", "IsoCode": "TP", "IsNotInUse": false }, { "Code": "SA", "Description": "SAUDI ARABIA", "IsoCode": "SA", "IsNotInUse": false }, { "Code": "SG", "Description": "SENEGAL", "IsoCode": "SG", "IsNotInUse": false }, { "Code": "SE", "Description": "SEYCHELLES", "IsoCode": "SE", "IsNotInUse": false }, { "Code": "SL", "Description": "SIERRA LEONE", "IsoCode": "SL", "IsNotInUse": false }, { "Code": "SN", "Description": "SINGAPORE", "IsoCode": "SN", "IsNotInUse": false }, { "Code": "LO", "Description": "SLOVAKIA", "IsoCode": "LO", "IsNotInUse": false }, { "Code": "SI", "Description": "SLOVENIA", "IsoCode": "SI", "IsNotInUse": false }, { "Code": "BP", "Description": "SOLOMON ISLANDS", "IsoCode": "BP", "IsNotInUse": false }, { "Code": "SO", "Description": "SOMALIA", "IsoCode": "SO", "IsNotInUse": false }, { "Code": "SF", "Description": "SOUTH AFRICA", "IsoCode": "SF", "IsNotInUse": false }, { "Code": "SP", "Description": "Spain", "IsoCode": "SP", "IsNotInUse": false }, { "Code": "CE", "Description": "SRI LANKA", "IsoCode": "CE", "IsNotInUse": false }, { "Code": "U3", "Description": "STATELESS", "IsoCode": "U3", "IsNotInUse": false }, { "Code": "SU", "Description": "SUDAN", "IsoCode": "SU", "IsNotInUse": false }, { "Code": "NS", "Description": "SURINAME", "IsoCode": "NS", "IsNotInUse": false }, { "Code": "WZ", "Description": "SWAZILAND", "IsoCode": "WZ", "IsNotInUse": false }, { "Code": "SW", "Description": "SWEDEN", "IsoCode": "SW", "IsNotInUse": false }, { "Code": "SZ", "Description": "SWITZERLAND", "IsoCode": "SZ", "IsNotInUse": false }, { "Code": "SY", "Description": "SYRIAN ARAB REPUBLIC", "IsoCode": "SY", "IsNotInUse": false }, { "Code": "TW", "Description": "TAIWAN, PROVINCE OF CHINA", "IsoCode": "TW", "IsNotInUse": false }, { "Code": "TI", "Description": "TAJIKISTAN", "IsoCode": "TI", "IsNotInUse": false }, { "Code": "TZ", "Description": "TANZANIA, UNITED REPUBLIC OF", "IsoCode": "TZ", "IsNotInUse": false }, { "Code": "TH", "Description": "THAILAND", "IsoCode": "TH", "IsNotInUse": false }, { "Code": "TO", "Description": "TOGO", "IsoCode": "TO", "IsNotInUse": false }, { "Code": "TL", "Description": "TOKELAU", "IsoCode": "TL", "IsNotInUse": false }, { "Code": "TN", "Description": "TONGA", "IsoCode": "TN", "IsNotInUse": false }, { "Code": "TD", "Description": "TRINIDAD AND TOBAGO", "IsoCode": "TD", "IsNotInUse": false }, { "Code": "TS", "Description": "TUNISIA", "IsoCode": "TS", "IsNotInUse": false }, { "Code": "TU", "Description": "TURKEY", "IsoCode": "TU", "IsNotInUse": false }, { "Code": "TX", "Description": "TURKMENISTAN", "IsoCode": "TX", "IsNotInUse": false }, { "Code": "TK", "Description": "TURKS AND CAICOS ISLANDS", "IsoCode": "TK", "IsNotInUse": false }, { "Code": "TV", "Description": "TUVALU", "IsoCode": "TV", "IsNotInUse": false }, { "Code": "VQ", "Description": "U.S. VIRGIN ISLANDS", "IsoCode": "VQ", "IsNotInUse": false }, { "Code": "UG", "Description": "UGANDA", "IsoCode": "UG", "IsNotInUse": false }, { "Code": "UP", "Description": "UKRAINE", "IsoCode": "UP", "IsNotInUse": false }, { "Code": "TC", "Description": "UNITED ARAB EMIRATES", "IsoCode": "TC", "IsNotInUse": false }, { "Code": "UK", "Description": "UNITED KINGDOM", "IsoCode": "UK", "IsNotInUse": false }, { "Code": "USA", "Description": "United States of America", "IsoCode": "US", "IsNotInUse": false }, { "Code": "UY", "Description": "URUGUAY", "IsoCode": "UY", "IsNotInUse": false }, { "Code": "UZ", "Description": "UZBEKISTAN", "IsoCode": "UZ", "IsNotInUse": false }, { "Code": "NH", "Description": "VANUATU", "IsoCode": "NH", "IsNotInUse": false }, { "Code": "VE", "Description": "VENEZUELA", "IsoCode": "VE", "IsNotInUse": false }, { "Code": "VM", "Description": "VIETNAM", "IsoCode": "VM", "IsNotInUse": false }, { "Code": "VI", "Description": "VIRGIN ISLANDS, BRITISH", "IsoCode": "VI", "IsNotInUse": false }, { "Code": "WF", "Description": "WALLIS AND FUTUNA", "IsoCode": "WF", "IsNotInUse": false }, { "Code": "WE", "Description": "WEST BANK", "IsoCode": "WE", "IsNotInUse": false }, { "Code": "WI", "Description": "WESTERN SAHARA", "IsoCode": "WI", "IsNotInUse": false }, { "Code": "YM", "Description": "YEMEN", "IsoCode": "YM", "IsNotInUse": false }, { "Code": "YUG", "Description": "Yugoslavia", "IsoCode": "YU", "IsNotInUse": false }, { "Code": "ZA", "Description": "ZAMBIA", "IsoCode": "ZA", "IsNotInUse": false }, { "Code": "ZI", "Description": "ZIMBABWE", "IsoCode": "ZI", "IsNotInUse": false }],
            "States": [{ "Code": "AL", "Description": "Alabama", "CountryCode": "" }, { "Code": "AK", "Description": "Alaska", "CountryCode": "" }, { "Code": "AB", "Description": "Alberta", "CountryCode": "CA" }, { "Code": "AZ", "Description": "Arizona", "CountryCode": "" }, { "Code": "AR", "Description": "Arkansas", "CountryCode": "" }, { "Code": "AA", "Description": "Armed Forces Americas", "CountryCode": "" }, { "Code": "AE", "Description": "Base in Europe", "CountryCode": "" }, { "Code": "BC", "Description": "British Columbia", "CountryCode": "CA" }, { "Code": "CA", "Description": "California", "CountryCode": "" }, { "Code": "CZ", "Description": "Canal Zone", "CountryCode": "" }, { "Code": "CO", "Description": "Colorado", "CountryCode": "" }, { "Code": "CT", "Description": "Connecticut", "CountryCode": "" }, { "Code": "DE", "Description": "Delaware", "CountryCode": "" }, { "Code": "DC", "Description": "District Of Columbia", "CountryCode": "" }, { "Code": "FL", "Description": "Florida", "CountryCode": "" }, { "Code": "GA", "Description": "Georgia", "CountryCode": "" }, { "Code": "GU", "Description": "Guam", "CountryCode": "" }, { "Code": "HI", "Description": "Hawaii", "CountryCode": "" }, { "Code": "ID", "Description": "Idaho", "CountryCode": "" }, { "Code": "IL", "Description": "Illinois", "CountryCode": "" }, { "Code": "IN", "Description": "Indiana", "CountryCode": "" }, { "Code": "IA", "Description": "Iowa", "CountryCode": "" }, { "Code": "KS", "Description": "Kansas", "CountryCode": "" }, { "Code": "KY", "Description": "Kentucky", "CountryCode": "" }, { "Code": "LA", "Description": "Louisiana", "CountryCode": "" }, { "Code": "ME", "Description": "Maine", "CountryCode": "" }, { "Code": "MB", "Description": "Manitoba", "CountryCode": "CA" }, { "Code": "MD", "Description": "Maryland", "CountryCode": "" }, { "Code": "MA", "Description": "Massachusetts", "CountryCode": "" }, { "Code": "MI", "Description": "Michigan", "CountryCode": "" }, { "Code": "MN", "Description": "Minnesota", "CountryCode": "" }, { "Code": "MS", "Description": "Mississippi", "CountryCode": "" }, { "Code": "MO", "Description": "Missouri", "CountryCode": "" }, { "Code": "MT", "Description": "Montana", "CountryCode": "" }, { "Code": "NE", "Description": "Nebraska", "CountryCode": "" }, { "Code": "NV", "Description": "Nevada", "CountryCode": "" }, { "Code": "NB", "Description": "New Brunswick", "CountryCode": "CA" }, { "Code": "NH", "Description": "New Hampshire", "CountryCode": "" }, { "Code": "NJ", "Description": "New Jersey", "CountryCode": "" }, { "Code": "NM", "Description": "New Mexico", "CountryCode": "" }, { "Code": "NY", "Description": "New York", "CountryCode": "" }, { "Code": "NL", "Description": "Newfoundland and Labrador", "CountryCode": "CA" }, { "Code": "NC", "Description": "North Carolina", "CountryCode": "" }, { "Code": "ND", "Description": "North Dakota", "CountryCode": "" }, { "Code": "NT", "Description": "Northwest Territories", "CountryCode": "" }, { "Code": "NS", "Description": "Nova Scotia", "CountryCode": "CA" }, { "Code": "NU", "Description": "Nunavut", "CountryCode": "" }, { "Code": "OH", "Description": "Ohio", "CountryCode": "" }, { "Code": "OK", "Description": "Oklahoma", "CountryCode": "" }, { "Code": "ON", "Description": "Ontario", "CountryCode": "CA" }, { "Code": "OR", "Description": "Oregon", "CountryCode": "" }, { "Code": "PA", "Description": "Pennsylvania", "CountryCode": "" }, { "Code": "PE", "Description": "Prince Edward Island", "CountryCode": "CA" }, { "Code": "PR", "Description": "Puerto Rico", "CountryCode": "" }, { "Code": "QC", "Description": "Quebec", "CountryCode": "CA" }, { "Code": "RI", "Description": "Rhode Island", "CountryCode": "" }, { "Code": "SK", "Description": "Saskatchewan", "CountryCode": "CA" }, { "Code": "SC", "Description": "South Carolina", "CountryCode": "" }, { "Code": "SD", "Description": "South Dakota", "CountryCode": "" }, { "Code": "TN", "Description": "Tennessee", "CountryCode": "" }, { "Code": "TX", "Description": "Texas", "CountryCode": "" }, { "Code": "UT", "Description": "Utah", "CountryCode": "" }, { "Code": "VT", "Description": "Vermont", "CountryCode": "" }, { "Code": "VI", "Description": "Virgin Islands", "CountryCode": "" }, { "Code": "VA", "Description": "Virginia", "CountryCode": "" }, { "Code": "WA", "Description": "Washington", "CountryCode": "" }, { "Code": "WV", "Description": "West Virginia", "CountryCode": "" }, { "Code": "WI", "Description": "Wisconsin", "CountryCode": "" }, { "Code": "WY", "Description": "Wyoming", "CountryCode": "" }, { "Code": "YK", "Description": "Yukon", "CountryCode": "" }]
        };
        ko.validation.configure({
            messagesOnModified: true,
            insertMessages: true
        });


        viewModel = new UserProfileViewModel(repository);
        viewModel.errors = ko.validation.group(viewModel);
    });

    describe("constructor", function () {

        it(":isLoaded should be false", function () {
            expect(viewModel.isLoaded()).toBeFalsy();
        });

        it(":isWaiting should be false", function () {
            expect(viewModel.isWaiting()).toBeFalsy();
        });

        it(":ViewableAddresses should be empty", function () {
            expect(viewModel.ViewableAddresses().length).toEqual(0);
        });

        it(":ViewableEmailAddresses should be empty", function () {
            expect(viewModel.ViewableEmailAddresses().length).toEqual(0);
        });

        it(":ViewablePhones should be empty", function () {
            expect(viewModel.ViewablePhones().length).toEqual(0);
        });

        it(":Configuration should be null", function () {
            expect(viewModel.Configuration()).toBe(null);
        });

        it(":UserProfileModel should be undefined", function () {
            expect(viewModel.UserProfileModel()).not.toBeDefined();
        });

        it(":Text should be empty array", function () {
            expect(viewModel.Text()).toEqual([]);
        });

        it(":UpdatableEmailTypes should be empty array", function () {
            expect(viewModel.UpdatableEmailTypes()).toEqual([]);
        });

        it(":UserHasEmailUpdatePermission should be false", function () {
            expect(viewModel.UserHasEmailUpdatePermission()).toBeFalsy();
        });

        it(":UpdatablePhoneTypes should be empty array", function () {
            expect(viewModel.UpdatablePhoneTypes()).toEqual([]);
        });

        it(":UserHasPhoneUpdatePermission should be false", function () {
            expect(viewModel.UserHasPhoneUpdatePermission()).toBeFalsy();
        });

        it(":addressEditDialogIsOpen should be false", function () {
            expect(viewModel.addressEditDialogIsOpen()).toBeFalsy();
        });

        it(":emailEditDialogIsOpen should be false", function () {
            expect(viewModel.emailEditDialogIsOpen()).toBeFalsy();
        });

        it(":emailRemoveDialogIsOpen should be false", function () {
            expect(viewModel.emailRemoveDialogIsOpen()).toBeFalsy();
        });
    });

    it(":UserProfileModel should be populated after mapping", function () {
        ko.mapping.fromJS(data, {}, viewModel);
        expect(viewModel.UserProfileModel()).toBeDefined();
    });

    describe("showEmailNewDialog", function () {
        it(":opens edit dialog and sets values", function () {
            ko.mapping.fromJS(data, {}, viewModel);
            viewModel.showEmailNewDialog();
            expect(viewModel.isNewCandidateEmail()).toBeTruthy();
            expect(viewModel.emailEditDialogIsOpen()).toBeTruthy();
            expect(viewModel.candidateEmail().OriginalIsPreferred()).toEqual(false);
            expect(viewModel.candidateEmail().OriginalTypeCode()).toEqual("");
            expect(viewModel.candidateEmail().OriginalValue()).toEqual("");
            expect(viewModel.candidateEmail().IsPreferred()).toEqual(false);
            expect(viewModel.candidateEmail().TypeCode()).toEqual("");
            expect(viewModel.candidateEmail().Value()).toEqual("");
        });
    });

    describe("showEmailEditDialog", function () {
        it(":opens edit dialog and sets values", function () {
            jasmine.clock().install();
            ko.mapping.fromJS(data, {}, viewModel);
            var viewableEmail = viewModel.ViewableEmailAddresses()[1];
            var emailData = viewableEmail.EmailAddress;
            viewModel.showEmailEditDialog(viewableEmail);
            jasmine.clock().tick(501); // email values are throttled to 500ms updates
            expect(viewModel.emailEditDialogIsOpen()).toBeTruthy();
            expect(viewModel.isNewCandidateEmail()).toBeFalsy();
            expect(viewModel.candidateEmail().OriginalIsPreferred()).toEqual(emailData.IsPreferred());
            expect(viewModel.candidateEmail().OriginalTypeCode()).toEqual(emailData.TypeCode());
            expect(viewModel.candidateEmail().OriginalValue()).toEqual(emailData.Value());
            expect(viewModel.candidateEmail().IsPreferred()).toEqual(emailData.IsPreferred());
            expect(viewModel.candidateEmail().TypeCode()).toEqual(emailData.TypeCode());
            expect(viewModel.candidateEmail().Value()).toEqual(emailData.Value());
            jasmine.clock().uninstall();
        });
    });

    describe("showPhoneNewDialog", function () {
        it(":opens edit dialog and sets values", function () {
            ko.mapping.fromJS(data, {}, viewModel);
            viewModel.showPhoneNewDialog();
            expect(viewModel.isNewCandidatePhone()).toBeTruthy();
            expect(viewModel.phoneEditDialogIsOpen()).toBeTruthy();
            expect(viewModel.candidatePhone().Number()).toEqual("");
            expect(viewModel.candidatePhone().Extension()).toEqual("");
            expect(viewModel.candidatePhone().TypeCode()).toEqual("");
        });
    });

    describe("showPhoneEditDialog", function () {
        it(":opens edit dialog and sets values", function () {
            ko.mapping.fromJS(data, {}, viewModel);
            var viewablePhone = viewModel.ViewablePhones()[1];
            var phoneData = viewablePhone.Phone;

            viewModel.showPhoneEditDialog(viewablePhone);

            expect(viewModel.isNewCandidatePhone()).toBeFalsy();
            expect(viewModel.phoneEditDialogIsOpen()).toBeTruthy();
            expect(viewModel.candidatePhone().Number()).toEqual("704-555-0118");
            expect(viewModel.candidatePhone().Extension()).toEqual("");
            expect(viewModel.candidatePhone().TypeCode()).toEqual("HO");
        });
    });

    describe("closePhoneEditDialog", function () {
        it(":closes edit dialog and resets values", function () {
            ko.mapping.fromJS(data, {}, viewModel);
            viewModel.closePhoneEditDialog();
            expect(viewModel.isNewCandidatePhone()).toBeFalsy();
            expect(viewModel.phoneEditDialogIsOpen()).toBeFalsy();
            expect(viewModel.candidatePhone().Number()).toEqual("");
            expect(viewModel.candidatePhone().Extension()).toEqual("");
            expect(viewModel.candidatePhone().TypeCode()).toEqual("");
        });
    });

    describe("showAddressNewDialog", function () {
        it(":opens edit dialog and sets blank values for new addresses", function () {
            ko.mapping.fromJS(data, {}, viewModel);
            viewModel.showAddressNewDialog();
            expect(viewModel.candidateAddress().Street1()).toEqual("");
            expect(viewModel.candidateAddress().Street2()).toEqual("");
            expect(viewModel.candidateAddress().Street3()).toEqual("");
            expect(viewModel.candidateAddress().Street4()).toEqual("");
            expect(viewModel.candidateAddress().City()).toEqual("");
            expect(viewModel.candidateAddress().State()).toBeNull();
            expect(viewModel.candidateAddress().PostalCode()).toEqual("");
            expect(viewModel.candidateAddress().Country()).toBeNull();
            expect(viewModel.candidateAddress().internationalSelected()).toBeFalsy();
        });

        it(":opens edit dialog and sets existing values for existing domestic addresses (non-Canadian)", function () {
            var address = {
                Address: {
                    "AddressId": ko.observable("7777"),
                    "Type": ko.observable("Home/Permanent"),
                    "AddressLines": ko.observable(["725 Beach Avenue", "Suite J"]),
                    "AddressModifier": ko.observable(""),
                    "City": ko.observable("Virginia Beach"),
                    "State": ko.observable("VA"),
                    "PostalCode": ko.observable("22078"),
                    "County": ko.observable(""),
                    "Country": ko.observable(""),
                    "RouteCode": ko.observable(""),
                    "PhoneNumbers": ko.observable([
                        { "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" },
                        { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }
                    ]),
                    "AddressLabel": ko.observable(["725 Beach Avenue", "Suite J", "Virginia Beach, VA 22078"]),
                    "PersonId": ko.observable("0012578"),
                    "EffectiveStartDate": ko.observable("\/Date(1444017600000)\/"),
                    "EffectiveEndDate": ko.observable(null),
                    "IsPreferredAddress": ko.observable(false),
                    "IsPreferredResidence": ko.observable(false),
                    "TypeCode": ko.observable("WB"),
                    "CountryCode": ko.observable(null)
                }
            };
            ko.mapping.fromJS(data, {}, viewModel);
            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "VA";
            });
            viewModel.showAddressEditDialog(address);
            expect(viewModel.candidateAddress().Street1()).toEqual("725 Beach Avenue");
            expect(viewModel.candidateAddress().Street2()).toEqual("Suite J");
            expect(viewModel.candidateAddress().Street3()).toEqual("");
            expect(viewModel.candidateAddress().Street4()).toEqual("");
            expect(viewModel.candidateAddress().City()).toEqual("Virginia Beach");
            expect(viewModel.candidateAddress().State().Code()).toEqual(stateObj.Code());
            expect(viewModel.candidateAddress().PostalCode()).toEqual("22078");
            expect(viewModel.candidateAddress().Country()).toBeNull();
            expect(viewModel.candidateAddress().internationalSelected()).toBeFalsy();
        });

        it(":opens edit dialog and sets existing values for existing domestic addresses (Canadian)", function () {

            var address = {
                Address: {
                    "AddressId": ko.observable("7777"),
                    "Type": ko.observable("Home/Permanent"),
                    "AddressLines": ko.observable(["725 Beach Avenue", "Suite J"]),
                    "AddressModifier": ko.observable(""),
                    "City": ko.observable("Newport"),
                    "State": ko.observable("NB"),
                    "PostalCode": ko.observable("22078"),
                    "County": ko.observable(""),
                    "Country": ko.observable(""),
                    "RouteCode": ko.observable(""),
                    "PhoneNumbers": ko.observable([{ "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" }, { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }]),
                    "AddressLabel": ko.observable(["725 Beach Avenue", "Suite J", "Virginia Beach, VA 22078"]),
                    "PersonId": ko.observable("0012578"),
                    "EffectiveStartDate": ko.observable("\/Date(1444017600000)\/"),
                    "EffectiveEndDate": ko.observable(null),
                    "IsPreferredAddress": ko.observable(false),
                    "IsPreferredResidence": ko.observable(false),
                    "TypeCode": ko.observable("WB"),
                    "CountryCode": ko.observable("CA")
                }
            };
            ko.mapping.fromJS(data, {}, viewModel);
            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "NB";
            });
            viewModel.showAddressEditDialog(address);
            expect(viewModel.candidateAddress().Street1()).toEqual("725 Beach Avenue");
            expect(viewModel.candidateAddress().Street2()).toEqual("Suite J");
            expect(viewModel.candidateAddress().Street3()).toEqual("");
            expect(viewModel.candidateAddress().Street4()).toEqual("");
            expect(viewModel.candidateAddress().City()).toEqual("Newport");
            expect(viewModel.candidateAddress().State().Code()).toEqual(stateObj.Code());
            expect(viewModel.candidateAddress().PostalCode()).toEqual("22078");
            expect(viewModel.candidateAddress().Country().Code()).toEqual(stateObj.CountryCode());
            expect(viewModel.candidateAddress().OriginalTypeCode()).toEqual("WB");
            expect(viewModel.candidateAddress().internationalSelected()).toBeFalsy();
            expect(viewModel.addressTypeOptions().length).toEqual(1);
            expect(viewModel.addressTypeOptions()[0].Code()).toEqual("WB");
        });

        it(":opens edit dialog and sets existing values for existing intl addresses", function () {
            var address = {
                Address: {
                    "AddressId": ko.observable("7777"),
                    "Type": ko.observable("Home/Permanent"),
                    "AddressLines": ko.observable(["725 Beach Avenue", "Suite J", "Line 3", "Line 4"]),
                    "AddressModifier": ko.observable(""),
                    "City": ko.observable(""),
                    "State": ko.observable(""),
                    "PostalCode": ko.observable(""),
                    "County": ko.observable(""),
                    "Country": ko.observable("VENEZUELA"),
                    "RouteCode": ko.observable(""),
                    "PhoneNumbers": ko.observable([
                        { "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" },
                        { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }
                    ]),
                    "AddressLabel": ko.observable(["725 Beach Avenue", "Suite J", "Line 3", "Line 4"]),
                    "PersonId": ko.observable("0012578"),
                    "EffectiveStartDate": ko.observable("\/Date(1444017600000)\/"),
                    "EffectiveEndDate": ko.observable(null),
                    "IsPreferredAddress": ko.observable(false),
                    "IsPreferredResidence": ko.observable(false),
                    "TypeCode": ko.observable("WB"),
                    "CountryCode": ko.observable("VE")
                }
            };
            ko.mapping.fromJS(data, {}, viewModel);
            var countryObj = ko.utils.arrayFirst(viewModel.Countries(), function (c) {
                return c.Code() === "VE";
            });
            viewModel.showAddressEditDialog(address);
            expect(viewModel.candidateAddress().Street1()).toEqual("725 Beach Avenue");
            expect(viewModel.candidateAddress().Street2()).toEqual("Suite J");
            expect(viewModel.candidateAddress().Street3()).toEqual("Line 3");
            expect(viewModel.candidateAddress().Street4()).toEqual("Line 4");
            expect(viewModel.candidateAddress().City()).toEqual("");
            expect(viewModel.candidateAddress().State()).toBeNull();
            expect(viewModel.candidateAddress().PostalCode()).toEqual("");
            expect(viewModel.candidateAddress().Country().Code()).toEqual(countryObj.Code());
            expect(viewModel.candidateAddress().OriginalTypeCode()).toEqual("WB");
            expect(viewModel.candidateAddress().internationalSelected()).toBeTruthy();
            expect(viewModel.addressTypeOptions().length).toEqual(1);
            expect(viewModel.addressTypeOptions()[0].Code()).toEqual("WB");
        });
    });

    describe("get", function () {
        it(":gets the user profile", function (done) {

            viewModel.UserProfileModel = null;
            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "get").and.callFake(function (e) {
                return promise;
            });

            viewModel.get();

            promise.then(function () {
                var expected = ko.toJS(viewModel.UserProfileModel);
                expect(expected).toEqual(data.UserProfileModel);

                done();
            });
        });

        it(":doesn't update the user profile on error", function (done) {

            viewModel.UserProfileModel = null;
            var promise = new Promise(function (resolve, reject) { reject("error"); });

            spyOn(repository, "get").and.callFake(function (e) {
                return promise;
            });

            spyOn($.fn, "notificationCenter").and.callFake(function () { });

            viewModel.get();

            promise.catch(function () {
                var expected = ko.toJS(viewModel.UserProfileModel);
                expect(expected).toEqual(null);
            })
            .then(function () {
                expect($.fn.notificationCenter).toHaveBeenCalled();

                done();
            });
        });
    });

    describe("removePhone", function () {

        var phone = {
            Phone: {
                Number: ko.observable("703-555-5678"),
                Extension: ko.observable(""),
                TypeCode: ko.observable("CP")
            },
            IsUpdatable: ko.observable(true)
        };

        beforeEach(function () {
            spyOn(viewModel, "updateUserProfile");  // spy to prevent execution. updateUserProfile is tested separately.

            viewModel.phoneToRemove(phone);

            ko.mapping.fromJS(data, {}, viewModel);
        });

        it(":remove phoneToRemove from the profile", function () {
            viewModel.removePhone();

            var index = viewModel.UserProfileModel().Phones.indexOf(
                ko.utils.arrayFirst(ko.utils.unwrapObservable(viewModel.UserProfileModel().Phones), function (obj) {
                    return obj.Number() == phone.Phone.Number();
                }));

            expect(index).toEqual(-1);
        });
    });

    describe("submitPhone", function () {

        var phone = {
            Number: ko.observable("123-123-1234"),
            Extension: ko.observable("5555"),
            TypeCode: ko.observable("TEST")
        };

        var candidatePhone = Object.create(phone);

        candidatePhone.OriginalNumber = ko.observable("703-555-5678");
        candidatePhone.OriginalExtension = ko.observable("");
        candidatePhone.OriginalTypeCode = ko.observable("CP");

        beforeEach(function () {
            spyOn(viewModel, "updateUserProfile");  // spy to prevent execution. updateUserProfile is tested separately.

            viewModel.candidatePhone(candidatePhone);

            ko.mapping.fromJS(data, {}, viewModel);
        });

        it(":adds candidatePhone to profile if it's new", function () {
            viewModel.isNewCandidatePhone(true);

            viewModel.submitPhone();

            var actualPhone = viewModel.UserProfileModel().Phones().pop();

            expect(actualPhone.Number()).toEqual(phone.Number());
            expect(actualPhone.Extension()).toEqual(phone.Extension());
            expect(actualPhone.TypeCode()).toEqual(phone.TypeCode());
        });

        it(":edits existing phone if candidatePhone is not new", function () {
            viewModel.isNewCandidatePhone(false);

            // Find the index of the email that we're going to edit.
            var index = viewModel.UserProfileModel().Phones.indexOf(
                ko.utils.arrayFirst(ko.utils.unwrapObservable(viewModel.UserProfileModel().Phones), function (obj) {
                    return obj.Number() == candidatePhone.OriginalNumber();
                }));

            viewModel.submitPhone();

            var actualPhone = viewModel.UserProfileModel().Phones()[index];

            expect(actualPhone.Number()).toEqual(phone.Number());
            expect(actualPhone.Extension()).toEqual(phone.Extension());
            expect(actualPhone.TypeCode()).toEqual(phone.TypeCode());
        });
    });

    describe("submitEmailAddress", function () {

        var email = {
            IsPreferred: ko.observable(true),
            TypeCode: ko.observable("TEST"),
            Value: ko.observable("test@test")
        };

        var candidateEmail = Object.create(email);

        candidateEmail.OriginalIsPreferred = ko.observable(true);
        candidateEmail.OriginalTypeCode = ko.observable("COL");
        candidateEmail.OriginalValue = ko.observable("eevans15@ellucian.edu");

        beforeEach(function () {
            spyOn(viewModel, "updateUserProfile");  // spy to prevent execution. updateUserProfile is tested separately.

            viewModel.candidateEmail(candidateEmail);

            ko.mapping.fromJS(data, {}, viewModel);
        });

        it(":does not set emailEditDialogIsOpen to false if isNewCandidateEmail is false", function () {
            viewModel.isNewCandidateEmail(false);
            viewModel.emailEditDialogIsOpen(true);

            viewModel.submitEmailAddress();

            expect(viewModel.emailEditDialogIsOpen()).toBeTruthy();
        });

        it(":adds candidateEmail to profile if it's new", function () {
            viewModel.isNewCandidateEmail(true);

            viewModel.submitEmailAddress();

            var actualEmail = viewModel.UserProfileModel().EmailAddresses().pop();

            expect(actualEmail.IsPreferred()).toEqual(email.IsPreferred());
            expect(actualEmail.TypeCode()).toEqual(email.TypeCode());
            expect(actualEmail.Value()).toEqual(email.Value());
        });

        it(":edits existing email if candidateEmail is not new", function () {
            viewModel.isNewCandidateEmail(false);

            // Find the index of the email that we're going to edit.
            var index = viewModel.UserProfileModel().EmailAddresses.indexOf(
                ko.utils.arrayFirst(ko.utils.unwrapObservable(viewModel.UserProfileModel().EmailAddresses), function (obj) {
                    return obj.Value() == candidateEmail.OriginalValue();
                }));

            viewModel.submitEmailAddress();

            var actualEmail = viewModel.UserProfileModel().EmailAddresses()[index];

            expect(actualEmail.IsPreferred()).toEqual(email.IsPreferred());
            expect(actualEmail.TypeCode()).toEqual(email.TypeCode());
            expect(actualEmail.Value()).toEqual(email.Value());
        });
    });

    describe("submitAddress", function () {
        it(":saves new domestic values for domestic address (non-Canadian)", function (done) {
            ko.mapping.fromJS(data, {}, viewModel);
            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "VA";
            });
            var countryObj = ko.utils.arrayFirst(viewModel.Countries(), function (c) {
                return c.Code() === "VE";
            });
            viewModel.showAddressNewDialog();
            viewModel.candidateAddress().Street1("123 Test St");
            viewModel.candidateAddress().Street2("Suite A");
            viewModel.candidateAddress().Street3("Line 3"); // Should be blanked out
            viewModel.candidateAddress().Street4("Line 4"); // Should be blanked out
            viewModel.candidateAddress().City("Somewhere");
            viewModel.candidateAddress().State(stateObj);
            viewModel.candidateAddress().PostalCode("32121");
            viewModel.candidateAddress().Country(countryObj);   // Should be blanked out

            // Add the new address to the object that we expect to be returned by the repository
            data.UserProfileModel.Addresses.push({
                AddressLines: ["123 Test St", "Suite A"],
                City: "Somewhere",
                State: "VA",
                PostalCode: "32121",
                Country: null
            });

            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "update").and.callFake(function (e) {
                return promise;
            });

            viewModel.submitAddress();

            promise.then(function () {
                var address = viewModel.UserProfileModel().Addresses().pop();

                expect(address.AddressLines()[0]).toEqual("123 Test St");
                expect(address.AddressLines()[1]).toEqual("Suite A");
                expect(address.AddressLines().length).toEqual(2);
                expect(address.City()).toEqual("Somewhere");
                expect(address.State()).toEqual("VA");
                expect(address.PostalCode()).toEqual("32121");
                expect(address.Country()).toBeNull();

                done();
            });
        });

        // This test will fail until CountryCode is populated by the API...
        it(":saves new domestic values for domestic address (Canadian)", function (done) {
            ko.mapping.fromJS(data, {}, viewModel);
            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "NB";
            });

            viewModel.showAddressNewDialog();
            viewModel.candidateAddress().Street1("123 Test St");
            viewModel.candidateAddress().Street2("Suite A");
            viewModel.candidateAddress().Street3("Line 3"); // Should be blanked out
            viewModel.candidateAddress().Street4("Line 4"); // Should be blanked out
            viewModel.candidateAddress().City("Somewhere");
            viewModel.candidateAddress().State(stateObj);
            viewModel.candidateAddress().PostalCode("32121");

            // Add the new address to the object that we expect to be returned by the repository
            data.UserProfileModel.Addresses.push({
                AddressLines: ["123 Test St", "Suite A"],
                City: "Somewhere",
                State: "NB",
                PostalCode: "32121",
                CountryCode: "CA"
            });

            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "update").and.callFake(function (e) {
                return promise;
            });

            viewModel.submitAddress();

            promise.then(function () {
                var address = viewModel.UserProfileModel().Addresses().pop();

                expect(address.AddressLines()[0]).toEqual("123 Test St");
                expect(address.AddressLines()[1]).toEqual("Suite A");
                expect(address.AddressLines().length).toEqual(2);
                expect(address.City()).toEqual("Somewhere");
                expect(address.State()).toEqual("NB");
                expect(address.PostalCode()).toEqual("32121");
                expect(address.CountryCode()).toEqual("CA");

                done();
            });
        });

        it(":saves new intl values for intl address", function (done) {
            ko.mapping.fromJS(data, {}, viewModel);
            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "VA";
            });
            var countryObj = ko.utils.arrayFirst(viewModel.Countries(), function (c) {
                return c.Code() === "VE";
            });
            viewModel.showAddressNewDialog();
            viewModel.candidateAddress().internationalSelected(true);
            viewModel.candidateAddress().Street1("123 Test St");
            viewModel.candidateAddress().Street2("Suite A");
            viewModel.candidateAddress().Street3("Line 3");
            viewModel.candidateAddress().Street4("Line 4");
            viewModel.candidateAddress().City("Somewhere"); // Should be blanked out
            viewModel.candidateAddress().State(stateObj);   // Should be blanked out
            viewModel.candidateAddress().PostalCode("32121");   // Should be blanked out
            viewModel.candidateAddress().Country(countryObj);

            // Add the new address to the object that we expect to be returned by the repository
            data.UserProfileModel.Addresses.push({
                AddressLines: ["123 Test St", "Suite A", "Line 3", "Line 4"],
                City: null,
                State: null,
                PostalCode: null,
                CountryCode: countryObj.Code(),
                Country: countryObj.Description()
            });

            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "update").and.callFake(function (e) {
                return promise;
            });

            viewModel.submitAddress();

            promise.then(function () {
                var address = viewModel.UserProfileModel().Addresses().pop();

                expect(address.AddressLines()[0]).toEqual("123 Test St");
                expect(address.AddressLines()[1]).toEqual("Suite A");
                expect(address.AddressLines()[2]).toEqual("Line 3");
                expect(address.AddressLines()[3]).toEqual("Line 4");
                expect(address.City()).toBeNull();
                expect(address.State()).toBeNull();
                expect(address.PostalCode()).toBeNull();
                expect(address.CountryCode()).toEqual(countryObj.Code());
                expect(address.Country()).toEqual(countryObj.Description());

                done();
            });
        });

        it(":updates domestic values for domestic address (non-Canadian)", function (done) {
            data.UserProfileModel.Addresses.push({
                "AddressId": "7777",
                "Type": "Home/Permanent",
                "AddressLines": ["725 Beach Avenue", "Suite J"],
                "AddressModifier": "",
                "City": "Virginia Beach",
                "State": "VA",
                "PostalCode": "22078",
                "County": "",
                "Country": "",
                "RouteCode": "",
                "PhoneNumbers": [{ "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" }, { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }],
                "AddressLabel": ["725 Beach Avenue", "Suite J", "Virginia Beach, VA 22078"],
                "PersonId": "0012578",
                "EffectiveStartDate": "\/Date(1444017600000)\/",
                "EffectiveEndDate": null,
                "IsPreferredAddress": false,
                "IsPreferredResidence": false,
                "TypeCode": "WB",
                "CountryCode": null
            });
            ko.mapping.fromJS(data, {}, viewModel);

            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "VA";
            });
            var countryObj = ko.utils.arrayFirst(viewModel.Countries(), function (c) {
                return c.Code() === "VE";
            });
            viewModel.showAddressNewDialog();
            viewModel.candidateAddress().Street1("123 Test St");
            viewModel.candidateAddress().Street2("Suite A");
            viewModel.candidateAddress().Street3("Line 3"); // Should be blanked out
            viewModel.candidateAddress().Street4("Line 4"); // Should be blanked out
            viewModel.candidateAddress().City("Somewhere");
            viewModel.candidateAddress().State(stateObj);
            viewModel.candidateAddress().PostalCode("32121");
            viewModel.candidateAddress().Country(countryObj);   // Should be blanked out

            // Add the new address to the object that we expect to be returned by the repository
            data.UserProfileModel.Addresses.push({
                AddressLines: ["123 Test St", "Suite A"],
                City: "Somewhere",
                State: "VA",
                PostalCode: "32121",
                Country: null
            });

            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "update").and.callFake(function (e) {
                return promise;
            });

            viewModel.submitAddress();

            promise.then(function () {
                var address = viewModel.UserProfileModel().Addresses().pop();

                expect(address.AddressLines()[0]).toEqual("123 Test St");
                expect(address.AddressLines()[1]).toEqual("Suite A");
                expect(address.AddressLines().length).toEqual(2);
                expect(address.City()).toEqual("Somewhere");
                expect(address.State()).toEqual("VA");
                expect(address.PostalCode()).toEqual("32121");
                expect(address.Country()).toBeNull();

                done();
            });
        });

        it(":updates domestic values for domestic address (Canadian)", function (done) {
            data.UserProfileModel.Addresses.push({
                "AddressId": "7777",
                "Type": "Home/Permanent",
                "AddressLines": ["725 Beach Avenue", "Suite J"],
                "AddressModifier": "",
                "City": "Newport",
                "State": "NB",
                "PostalCode": "22078",
                "County": "",
                "Country": "",
                "RouteCode": "",
                "PhoneNumbers": [{ "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" }, { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }],
                "AddressLabel": ["725 Beach Avenue", "Suite J", "Virginia Beach, VA 22078"],
                "PersonId": "0012578",
                "EffectiveStartDate": "\/Date(1444017600000)\/",
                "EffectiveEndDate": null,
                "IsPreferredAddress": false,
                "IsPreferredResidence": false,
                "TypeCode": "WB",
                "CountryCode": "CA"
            });
            ko.mapping.fromJS(data, {}, viewModel);

            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "NB";
            });

            viewModel.showAddressNewDialog();
            viewModel.candidateAddress().Street1("123 Test St");
            viewModel.candidateAddress().Street2("Suite A");
            viewModel.candidateAddress().Street3("Line 3"); // Should be blanked out
            viewModel.candidateAddress().Street4("Line 4"); // Should be blanked out
            viewModel.candidateAddress().City("Somewhere");
            viewModel.candidateAddress().State(stateObj);
            viewModel.candidateAddress().PostalCode("32121");

            // Add the new address to the object that we expect to be returned by the repository
            data.UserProfileModel.Addresses.push({
                AddressLines: ["123 Test St", "Suite A"],
                City: "Somewhere",
                State: "NB",
                PostalCode: "32121",
                CountryCode: "CA"
            });

            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "update").and.callFake(function (e) {
                return promise;
            });

            viewModel.submitAddress();

            promise.then(function () {
                var address = viewModel.UserProfileModel().Addresses().pop();

                expect(address.AddressLines()[0]).toEqual("123 Test St");
                expect(address.AddressLines()[1]).toEqual("Suite A");
                expect(address.AddressLines().length).toEqual(2);
                expect(address.City()).toEqual("Somewhere");
                expect(address.State()).toEqual("NB");
                expect(address.PostalCode()).toEqual("32121");
                expect(address.CountryCode()).toEqual("CA");

                done();
            });
        });

        it(":updates intl values for intl address", function (done) {
            data.UserProfileModel.Addresses.push({
                "AddressId": "7777",
                "Type": "Home/Permanent",
                "AddressLines": ["725 Beach Avenue", "Suite J", "Line 3", "Line 4"],
                "AddressModifier": "",
                "City": "",
                "State": "",
                "PostalCode": "",
                "County": "",
                "Country": "Venezuela",
                "RouteCode": "",
                "PhoneNumbers": [{ "Number": "703-555-5678", "Extension": "", "TypeCode": "CP" }, { "Number": "704-555-0118", "Extension": "", "TypeCode": "HO" }],
                "AddressLabel": ["725 Beach Avenue", "Suite J", "Line 3", "Line 4"],
                "PersonId": "0012578",
                "EffectiveStartDate": "\/Date(1444017600000)\/",
                "EffectiveEndDate": null,
                "IsPreferredAddress": false,
                "IsPreferredResidence": false,
                "TypeCode": "WB",
                "CountryCode": "VE"
            });
            ko.mapping.fromJS(data, {}, viewModel);

            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "VA";
            });
            var countryObj = ko.utils.arrayFirst(viewModel.Countries(), function (c) {
                return c.Code() === "VE";
            });
            viewModel.showAddressNewDialog();
            viewModel.candidateAddress().internationalSelected(true);
            viewModel.candidateAddress().Street1("123 Test St");
            viewModel.candidateAddress().Street2("Suite A");
            viewModel.candidateAddress().Street3("Line 3");
            viewModel.candidateAddress().Street4("Line 4");
            viewModel.candidateAddress().City("Somewhere"); // Should be blanked out
            viewModel.candidateAddress().State(stateObj);   // Should be blanked out
            viewModel.candidateAddress().PostalCode("32121");   // Should be blanked out
            viewModel.candidateAddress().Country(countryObj);

            // Add the new address to the object that we expect to be returned by the repository
            data.UserProfileModel.Addresses.push({
                AddressLines: ["123 Test St", "Suite A", "Line 3", "Line 4"],
                City: null,
                State: null,
                PostalCode: null,
                CountryCode: countryObj.Code(),
                Country: countryObj.Description()
            });

            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "update").and.callFake(function (e) {
                return promise;
            });

            viewModel.submitAddress();

            promise.then(function () {
                var address = viewModel.UserProfileModel().Addresses().pop();

                expect(address.AddressLines()[0]).toEqual("123 Test St");
                expect(address.AddressLines()[1]).toEqual("Suite A");
                expect(address.AddressLines()[2]).toEqual("Line 3");
                expect(address.AddressLines()[3]).toEqual("Line 4");
                expect(address.City()).toBeNull();
                expect(address.State()).toBeNull();
                expect(address.PostalCode()).toBeNull();
                expect(address.CountryCode()).toEqual(countryObj.Code());
                expect(address.Country()).toEqual(countryObj.Description());

                done();
            });
        });
    });

    describe("closeAddressEditDialog", function () {
        it(":closes edit dialog and resets values", function () {
            ko.mapping.fromJS(data, {}, viewModel);
            var stateObj = ko.utils.arrayFirst(viewModel.States(), function (st) {
                return st.Code() === "VA";
            });
            var countryObj = ko.utils.arrayFirst(viewModel.Countries(), function (c) {
                return c.Code() === "VE";
            });
            viewModel.showAddressNewDialog();
            viewModel.candidateAddress().internationalSelected(true);
            viewModel.candidateAddress().Street1("123 Test St");
            viewModel.candidateAddress().Street2("Suite A");
            viewModel.candidateAddress().Street3("Line 3");
            viewModel.candidateAddress().Street4("Line 4");
            viewModel.candidateAddress().City("Somewhere"); // Should be blanked out
            viewModel.candidateAddress().State(stateObj);   // Should be blanked out
            viewModel.candidateAddress().PostalCode("32121");   // Should be blanked out
            viewModel.candidateAddress().Country(countryObj);

            viewModel.closeAddressEditDialog();

            expect(viewModel.candidateAddress().Street1()).toEqual("");
            expect(viewModel.candidateAddress().Street2()).toEqual("");
            expect(viewModel.candidateAddress().Street3()).toEqual("");
            expect(viewModel.candidateAddress().Street4()).toEqual("");
            expect(viewModel.candidateAddress().City()).toEqual("");
            expect(viewModel.candidateAddress().State()).toBeNull();
            expect(viewModel.candidateAddress().PostalCode()).toEqual("");
            expect(viewModel.candidateAddress().Country()).toBeNull();
            expect(viewModel.candidateAddress().internationalSelected()).toBeFalsy();

        });
    });

    describe("emailEditDialogIsOpen", function () {
        it(":resets candidate email on state change to false", function () {
            viewModel.emailEditDialogIsOpen(true);

            viewModel.candidateEmail().IsPreferred(true);
            viewModel.candidateEmail().TypeCode("x");
            viewModel.candidateEmail().Value("x");
            viewModel.candidateEmail().OriginalIsPreferred(true);
            viewModel.candidateEmail().OriginalTypeCode("x");
            viewModel.candidateEmail().OriginalValue("x");
            viewModel.isNewCandidateEmail(true);

            viewModel.emailEditDialogIsOpen(false);

            expect(viewModel.candidateEmail().IsPreferred()).toBeFalsy();
            expect(viewModel.candidateEmail().TypeCode()).toBe("");
            expect(viewModel.candidateEmail().Value()).toBe("");
            expect(viewModel.candidateEmail().OriginalIsPreferred()).toBeFalsy();
            expect(viewModel.candidateEmail().OriginalTypeCode()).toBe("");
            expect(viewModel.candidateEmail().OriginalValue()).toBe("");

        });
    });

    describe("phoneEditDialogIsOpen", function () {
        it(":resets candidate phone on state change to false", function () {
            viewModel.phoneEditDialogIsOpen(true);

            viewModel.candidatePhone().Number("1");
            viewModel.candidatePhone().Extension("x");
            viewModel.candidatePhone().TypeCode("x");
            viewModel.candidatePhone().OriginalNumber("1");
            viewModel.candidatePhone().OriginalExtension("x");
            viewModel.candidatePhone().OriginalTypeCode("x");
            viewModel.isNewCandidateEmail(true);

            viewModel.phoneEditDialogIsOpen(false);

            expect(viewModel.candidatePhone().Number()).toBe("");
            expect(viewModel.candidatePhone().Extension()).toBe("");
            expect(viewModel.candidatePhone().TypeCode()).toBe("");
            expect(viewModel.candidatePhone().OriginalNumber()).toBe("");
            expect(viewModel.candidatePhone().OriginalExtension()).toBe("");
            expect(viewModel.candidatePhone().OriginalTypeCode()).toBe("");

        });
    });

    describe("emailRemoveDialogIsOpen", function () {

        it(":resets emailToRemove on state change to false", function () {
            viewModel.emailRemoveDialogIsOpen(true);

            viewModel.emailToRemove("x");

            viewModel.emailRemoveDialogIsOpen(false);

            expect(viewModel.emailToRemove()).toBe(null);
        });
    });

    describe("phoneRemoveDialogIsOpen", function () {

        it(":resets phoneToRemove on state change to false", function () {
            viewModel.phoneRemoveDialogIsOpen(true);

            viewModel.phoneToRemove("x");

            viewModel.phoneRemoveDialogIsOpen(false);

            expect(viewModel.phoneToRemove()).toBe(null);
        });
    });

    describe("showEmailRemoveDialog ", function () {

        var email = "test@test.com";

        it(":sets emailToRemove to parameter viewableEmailAddress", function () {
            viewModel.showEmailRemoveDialog(email);

            expect(viewModel.emailToRemove()).toEqual(email);
        });

        it(":sets emailRemoveDialogIsOpen to true", function () {
            viewModel.showEmailRemoveDialog(email);

            expect(viewModel.emailRemoveDialogIsOpen()).toBeTruthy();
        });
    });

    describe("showPhoneRemoveDialog ", function () {

        var phone = "123-123-1234";

        it(":sets phoneToRemove to parameter viewableEmailAddress", function () {
            viewModel.showPhoneRemoveDialog(phone);

            expect(viewModel.phoneToRemove()).toEqual(phone);
        });

        it(":sets phoneRemoveDialogIsOpen to true", function () {
            viewModel.showPhoneRemoveDialog(phone);

            expect(viewModel.phoneRemoveDialogIsOpen()).toBeTruthy();
        });
    });

    describe("removeEmailAddress", function () {

        var email;

        beforeEach(function () {
            spyOn(viewModel, "updateUserProfile"); // spy to avoid running updateUserProfile, which is tested separately

            email = {
                IsUpdatable: ko.observable(true),
                EmailAddress: {
                    Value: ko.observable("test@email"),
                    TypeCode: ko.observable("TEST")
                }
            };
        });

        it(":removes emailToRemove from UserProfileModel.EmailAddresses if IsUpdatable is true", function () {

            ko.mapping.fromJS(data, {}, viewModel);

            viewModel.UserProfileModel().EmailAddresses().push(email.EmailAddress);
            viewModel.emailToRemove(email);


            viewModel.removeEmailAddress();

            expect(viewModel.UserProfileModel().EmailAddresses()).not.toContain(email);
        });

        it(":does not remove emailToRemove from UserProfileModel.EmailAddresses if IsUpdatable is false", function () {

            ko.mapping.fromJS(data, {}, viewModel);

            email.IsUpdatable(false);

            viewModel.UserProfileModel().EmailAddresses().push(email.EmailAddress);
            viewModel.emailToRemove(email);

            viewModel.removeEmailAddress();

            expect(viewModel.UserProfileModel().EmailAddresses()).toContain(email.EmailAddress);
        });

        it(":sets emailRemoveDialogIsOpen to false", function () {

            ko.mapping.fromJS(data, {}, viewModel);

            viewModel.UserProfileModel().EmailAddresses().push(email.EmailAddress);
            viewModel.emailToRemove(email);

            viewModel.emailRemoveDialogIsOpen(true);

            viewModel.removeEmailAddress();

            expect(viewModel.emailRemoveDialogIsOpen()).toBeFalsy();
        });
    });

    describe("verifyAddressInformation", function () {

        beforeEach(function () {
            ko.mapping.fromJS(data, {}, viewModel);
        });

        it("sets isWaiting to true", function () {
            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            viewModel.verifyAddressInformation();

            expect(viewModel.isWaiting()).toBeTruthy();
        });

        it("maps data on success", function (done) {

            data.UserProfileModel.EmailAddresses = [];
            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            viewModel.verifyAddressInformation();


            promise.then(function () {

                expect(viewModel.UserProfileModel().EmailAddresses()).toEqual([]);

                done();
            });
        });

        it(":adds updateFailure message to notification center on failed verification", function (done) {

            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            spyOn($.fn, "notificationCenter");

            viewModel.verifyAddressInformation();


            promise.catch(function () {
                //no-op
            })
            .then(function () {

                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.UserProfile.userProfileMessages.updateFailure, type: "error" });

                done();
            });
        });
    });

    describe("verifyEmailAddressInformation", function () {

        beforeEach(function () {
            ko.mapping.fromJS(data, {}, viewModel);
        });

        it("sets isWaiting to true", function () {
            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            viewModel.verifyEmailAddressInformation();

            expect(viewModel.isWaiting()).toBeTruthy();
        });

        it("maps data on success", function (done) {

            data.UserProfileModel.EmailAddresses = [];
            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            viewModel.verifyEmailAddressInformation();


            promise.then(function () {

                expect(viewModel.UserProfileModel().EmailAddresses()).toEqual([]);

                done();
            });
        });

        it(":adds updateFailure message to notification center on failed verification", function (done) {

            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            spyOn($.fn, "notificationCenter");

            viewModel.verifyEmailAddressInformation();


            promise.catch(function () {
                //no-op
            })
            .then(function () {

                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.UserProfile.userProfileMessages.updateFailure, type: "error" });

                done();
            });
        });
    });

    describe("verifyPhoneInformation", function () {

        beforeEach(function () {
            ko.mapping.fromJS(data, {}, viewModel);
        });

        it("sets isWaiting to true", function () {
            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            viewModel.verifyPhoneInformation();

            expect(viewModel.isWaiting()).toBeTruthy();
        });

        it("maps data on success", function (done) {

            data.UserProfileModel.EmailAddresses = [];
            var promise = new Promise(function (resolve) { resolve(data); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            viewModel.verifyPhoneInformation();


            promise.then(function () {

                expect(viewModel.UserProfileModel().EmailAddresses()).toEqual([]);

                done();
            });
        });

        it(":adds updateFailure message to notification center on failed verification", function (done) {

            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            spyOn($.fn, "notificationCenter");

            viewModel.verifyPhoneInformation();


            promise.catch(function () {
                //no-op
            })
            .then(function () {

                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.UserProfile.userProfileMessages.updateFailure, type: "error" });

                done();
            });
        });
    });

    describe("personalPronouns", function () {

        beforeEach(function () {
            ko.mapping.fromJS(data, {}, viewModel);
        });

        it("personal pronoun is mapped", function () {
            expect(viewModel.personalPronouns().Description()).toEqual("He/Him/His");
        });

        it("unknown personal pronoun maps to null", function () {
            viewModel.UserProfileModel().PersonalPronounCode("")
            expect(viewModel.personalPronouns()).toEqual(null);
        });

    });
});