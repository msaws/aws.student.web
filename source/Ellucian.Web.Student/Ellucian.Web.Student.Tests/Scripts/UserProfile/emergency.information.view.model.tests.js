﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

describe("emergencyInformationViewModel", function () {

    // emergencyInformation is the local instance of the view model we are testing
    var emergencyInformation;

    var repository = Ellucian.UserProfile.emergencyInformationRepository;
    var testData = { "test": "test" };

    beforeEach(function () {
        emergencyInformation = new Ellucian.UserProfile.EmergencyInformationViewModel(repository);
    });

    describe("constructor", function () {

        it(":retrieved is false", function () {
            expect(emergencyInformation.retrieved()).toBeFalsy();
        });

        it(":IsLoading is false", function () {
            expect(emergencyInformation.IsLoading()).toBeFalsy();
        });

        it(":LoadingMessage is empty string", function () {
            expect(emergencyInformation.LoadingMessage()).toEqual("");
        })

        it(":dateFormat contains correct value", function () {
            var expected = Globalize.findClosestCulture().calendar.patterns.d;
            expect(emergencyInformation.dateFormat()).toBe(expected);
        });

        it(":EmergencyContacts is empty array", function () {
            expect(emergencyInformation.EmergencyContacts().constructor).toBe(Array);
            expect(emergencyInformation.EmergencyContacts().length).toBe(0);
        });

        it(":HosptialPreference is empty string", function () {
            expect(typeof emergencyInformation.HospitalPreference()).toBe("string");
            expect(emergencyInformation.HospitalPreference().length).toBe(0);
        });

        it(":HosptialPreference has maximum length of 50", function () {
            var maxLength = 50, badString = new Array(maxLength + 1).join('x');
            emergencyInformation.HospitalPreference(badString);
            expect(emergencyInformation.HospitalPreference().isValid).toBeFalsy();
        });

        it(":InsuranceInformation is empty string", function () {
            expect(typeof emergencyInformation.InsuranceInformation()).toBe("string");
            expect(emergencyInformation.InsuranceInformation().length).toBe(0);
        });

        it(":AdditionalInformation is empty string", function () {
            expect(typeof emergencyInformation.AdditionalInformation()).toBe("string");
            expect(emergencyInformation.AdditionalInformation().length).toBe(0);
        });

        it(":HealthConditions is empty array", function () {
            expect(emergencyInformation.HealthConditions().constructor).toBe(Array);
            expect(emergencyInformation.HealthConditions().length).toBe(0);
        });

        it(":ConfirmedDate is empty string", function () {
            expect(typeof emergencyInformation.ConfirmedDate()).toBe("string");
            expect(emergencyInformation.ConfirmedDate().length).toBe(0);
        });

        it(":OptOut is false", function () {
            expect(emergencyInformation.OptOut()).toBe(false);
        });

        it(":Notification is null", function () {
            expect(emergencyInformation.Notification()).toBe(null);
        });

        it(":PersonId is undefined", function () {
            expect(emergencyInformation.PersonId).not.toBeDefined();
        });

        it(":CurrentDateString is empty array", function () {
            expect(typeof emergencyInformation.CurrentDateString()).toBe("string");
            expect(emergencyInformation.CurrentDateString().length).toBe(0);
        });

        it(":EmergencyInformationConfiguration contains all false values", function () {
            expect(emergencyInformation.EmergencyInformationConfiguration().AllowOptOut()).toBeFalsy();
            expect(emergencyInformation.EmergencyInformationConfiguration().HideOtherInformation()).toBeFalsy();
            expect(emergencyInformation.EmergencyInformationConfiguration().HideHealthConditions()).toBeFalsy();
            expect(emergencyInformation.EmergencyInformationConfiguration().RequireContact()).toBeFalsy();
        });

        it(":selectedContactIndex is undefined", function () {
            expect(emergencyInformation.selectedContactIndex).not.toBeDefined();
        });

        it(":isNewContact is false", function () {
            expect(emergencyInformation.isNewContact).toBeFalsy();
        });

        it(":missingContactType is false", function () {
            expect(emergencyInformation.missingContactType()).toBeFalsy();
        });

        it(":validationMessages is empty array", function () {
            expect(emergencyInformation.validationMessages().constructor).toBe(Array);
            expect(emergencyInformation.validationMessages().length).toBe(0);
        });

        it(":editDialogIsOpen is false", function () {
            expect(emergencyInformation.editDialogIsOpen()).toBeFalsy();
        });

        it(":editDialogButtonLabel is empty string", function () {
            expect(typeof emergencyInformation.editDialogButtonLabel()).toBe("string");
            expect(emergencyInformation.editDialogButtonLabel().length).toBe(0);
        });

        it(":removeDialogIsOpen is false", function () {
            expect(emergencyInformation.removeDialogIsOpen()).toBeFalsy();
        });

        it(":contactToRemove is initialized", function () {
            expect(emergencyInformation.contactToRemove()).toBe(null);
        });

        it(":submitButtonLabel is initialized", function () {
            expect(typeof emergencyInformation.submitButtonLabel()).toBe("string");
            expect(emergencyInformation.submitButtonLabel().length).toBe(0);
        });

        it(":optoutDialogIsOpen is false", function () {
            expect(emergencyInformation.optoutDialogIsOpen()).toBeFalsy();
        });

    });

    describe("addEmergencyContact", function () {

        it(":updates values", function () {
            emergencyInformation.addEmergencyContact();

            expect(emergencyInformation.isNewContact).toBeTruthy();
            expect(emergencyInformation.editDialogIsOpen()).toBeTruthy();

            expect(emergencyInformation.editDialogButtonLabel()).toBe(Ellucian.UserProfile.emergencyInformationMessages.add);
            expect(emergencyInformation.submitButtonLabel()).toBe(Ellucian.UserProfile.emergencyInformationMessages.addAria);

            expect(emergencyInformation.editContact().Name()).toBe("");
            expect(emergencyInformation.editContact().Relationship()).toBe("");
            expect(emergencyInformation.editContact().DaytimePhone()).toBe("");
            expect(emergencyInformation.editContact().EveningPhone()).toBe("");
            expect(emergencyInformation.editContact().OtherPhone()).toBe("");
            expect(emergencyInformation.editContact().Address()).toBe("");
            expect(emergencyInformation.editContact().EffectiveDate()).toBe(emergencyInformation.CurrentDateString());
            expect(emergencyInformation.editContact().IsEmergencyContact()).toBeTruthy();
            expect(emergencyInformation.editContact().IsMissingPersonContact()).toBeFalsy();

        });
    });

    describe("editEmergencyContact", function () {

        it(":edits values", function () {
            var contact = {
                Name: ko.observable("test"),
                Relationship: ko.observable("test"),
                DaytimePhone: ko.observable("test"),
                EveningPhone: ko.observable("test"),
                OtherPhone: ko.observable("test"),
                IsEmergencyContact: ko.observable(true),
                IsMissingPersonContact: ko.observable(false),
                Address: ko.observable("test"),
                EffectiveDate: ko.observable("01/01/2015"),
                errors: ko.observable([]),
                isValid: ko.observable(true),
                isAnyMessageShown: ko.observable(false)
            };

            emergencyInformation.editEmergencyContact(contact);

            expect(emergencyInformation.isNewContact).toBeFalsy();
            expect(emergencyInformation.editDialogIsOpen()).toBeTruthy();

            expect(emergencyInformation.editDialogButtonLabel()).toBe(Ellucian.UserProfile.emergencyInformationMessages.update);
            expect(emergencyInformation.submitButtonLabel()).toBe(Ellucian.UserProfile.emergencyInformationMessages.updateAria);

            expect(emergencyInformation.editContact().Name()).toEqual(contact.Name());
            expect(emergencyInformation.editContact().Relationship()).toEqual(contact.Relationship());
            expect(emergencyInformation.editContact().DaytimePhone()).toEqual(contact.DaytimePhone());
            expect(emergencyInformation.editContact().EveningPhone()).toEqual(contact.EveningPhone());
            expect(emergencyInformation.editContact().OtherPhone()).toEqual(contact.OtherPhone());
            expect(emergencyInformation.editContact().IsEmergencyContact()).toEqual(contact.IsEmergencyContact());
            expect(emergencyInformation.editContact().IsMissingPersonContact()).toEqual(contact.IsMissingPersonContact());
            expect(emergencyInformation.editContact().Address()).toEqual(contact.Address());
            expect(emergencyInformation.editContact().EffectiveDate()).toEqual(contact.EffectiveDate());
            expect(emergencyInformation.editContact().errors()).toEqual(contact.errors());
            expect(emergencyInformation.editContact().isValid()).toEqual(contact.isValid());
            expect(emergencyInformation.editContact().isAnyMessageShown()).toEqual(contact.isAnyMessageShown());

            expect(emergencyInformation.selectedContactIndex).toBe(emergencyInformation.EmergencyContacts.indexOf(contact));
        });
    });

    describe("submitEmergencyInformation", function () {

        it(":adds new contact", function () {
            var previousNumberContacts = emergencyInformation.EmergencyContacts().length;

            emergencyInformation.isValid = ko.observable(true);
            emergencyInformation.isNewContact = true;

            spyOn(emergencyInformation, 'updateEmergencyInformation');

            emergencyInformation.submitEmergencyInformation();

            expect(emergencyInformation.EmergencyContacts().length).toBe(previousNumberContacts + 1);
            expect(emergencyInformation.isNewContact).toBeFalsy();
            expect(emergencyInformation.updateEmergencyInformation).toHaveBeenCalled();
        });

        it(":edits existing contact", function () {
            var previousNumberContacts;
            emergencyInformation.selectedContactIndex = 0;
            emergencyInformation.EmergencyContacts().push({
                Name: ko.observable("Name"),
                Relationship: ko.observable("Relationship"),
                DaytimePhone: ko.observable("DaytimePhone"),
                EveningPhone: ko.observable("EveningPhone"),
                OtherPhone: ko.observable("OtherPhone"),
                Address: ko.observable("Address"),
                EffectiveDate: ko.observable("01/01/2015"),
                IsEmergencyContact: ko.observable(true),
                IsMissingPersonContact: ko.observable(false)
            });
            previousNumberContacts = emergencyInformation.EmergencyContacts().length;
            emergencyInformation.editContact({
                Name: ko.observable("Name2"),
                Relationship: ko.observable("Relationship2"),
                DaytimePhone: ko.observable("DaytimePhone2"),
                EveningPhone: ko.observable("EveningPhone2"),
                OtherPhone: ko.observable("OtherPhone2"),
                Address: ko.observable("Address2"),
                EffectiveDate: ko.observable("01/02/2015"),
                IsEmergencyContact: ko.observable(false),
                IsMissingPersonContact: ko.observable(true)
            });
            emergencyInformation.isValid = ko.observable(true);
            emergencyInformation.isNewContact = false;

            spyOn(emergencyInformation, 'updateEmergencyInformation');

            emergencyInformation.submitEmergencyInformation();

            expect(emergencyInformation.EmergencyContacts().length).toBe(previousNumberContacts);
            expect(emergencyInformation.EmergencyContacts()[emergencyInformation.selectedContactIndex].Name()).toBe(emergencyInformation.editContact().Name());
            expect(emergencyInformation.isNewContact).toBeFalsy();
            expect(emergencyInformation.updateEmergencyInformation).toHaveBeenCalled();
        });

        it(":doesn't update invalid contact", function () {
            emergencyInformation.editContact.isValid = ko.observable(false);

            spyOn(emergencyInformation, 'updateEmergencyInformation');

            emergencyInformation.submitEmergencyInformation();

            expect(emergencyInformation.updateEmergencyInformation).not.toHaveBeenCalled();
        });

        it(":sets OptOut to false if OptOut was true", function () {

            emergencyInformation.isValid = ko.observable(true);
            emergencyInformation.isNewContact = true;

            spyOn(emergencyInformation, 'updateEmergencyInformation');

            emergencyInformation.OptOut(true);

            emergencyInformation.submitEmergencyInformation();

            expect(emergencyInformation.OptOut()).toBeFalsy();
        });
    });

    describe("updateEmergencyInformation", function () {

        beforeAll(function () {
            jasmine.clock().install();
        });

        afterAll(function () {
            jasmine.clock().uninstall();
        });

        it(":does nothing if isValid is false", function () {
            emergencyInformation.editContact.isValid = ko.observable(false);

            emergencyInformation.updateEmergencyInformation();

            expect(emergencyInformation.IsLoading()).toBeFalsy();
            jasmine.clock().tick(200);
            expect(emergencyInformation.IsLoading()).toBeFalsy();
        });

        it(":does something if isValid is true", function (done) {
            emergencyInformation.editContact.isValid = ko.observable(true);

            var promise = new Promise(function (resolve) { resolve(testData); });

            spyOn(repository, "update").and.callFake(function (e) {
                return promise;
            });

            emergencyInformation.updateEmergencyInformation();

            promise.then(function () {
                expect(repository.update).toHaveBeenCalled();

                expect(emergencyInformation.IsLoading()).toBeTruthy();
                jasmine.clock().tick(200);
                expect(emergencyInformation.IsLoading()).toBeFalsy();

                done();
            });
        });
    });

    describe("verifyEmergencyInformation", function () {

        beforeAll(function () {
            jasmine.clock().install();
        });

        afterAll(function () {
            jasmine.clock().uninstall();
        });

        it(":does nothing if isValid is false", function () {
            emergencyInformation.editContact.isValid = ko.observable(false);

            emergencyInformation.verifyEmergencyInformation();

            expect(emergencyInformation.IsLoading()).toBeFalsy();
            jasmine.clock().tick(200);
            expect(emergencyInformation.IsLoading()).toBeFalsy();
        });

        it(":does something if isValid is true", function (done) {
            emergencyInformation.editContact.isValid = ko.observable(true);

            var promise = new Promise(function (resolve) { resolve(testData); });

            spyOn(repository, "verify").and.callFake(function (e) {
                return promise;
            });

            emergencyInformation.verifyEmergencyInformation();

            promise.then(function () {
                expect(repository.verify).toHaveBeenCalled();

                expect(emergencyInformation.IsLoading()).toBeTruthy();
                jasmine.clock().tick(200);
                expect(emergencyInformation.IsLoading()).toBeFalsy();

                done();
            });
        });
    });

    describe("optoutConfirm", function () {

        beforeAll(function () {
            jasmine.clock().install();
        });

        afterAll(function () {
            jasmine.clock().uninstall();
        });

        it(":removes contacts, sets OptOut true, posts an update, and closes the dialog", function () {
            emergencyInformation.EmergencyContacts([{}, {}, {}]);
            emergencyInformation.editContact.isValid = ko.observable(true);

            spyOn(emergencyInformation, "updateEmergencyInformation").and.callFake(function (e) {
                return;
            });

            emergencyInformation.optoutConfirm();

                expect(emergencyInformation.EmergencyContacts().length).toBe(0);
                expect(emergencyInformation.OptOut()).toBeTruthy();
                expect(emergencyInformation.OptoutDialogIsOpen).toBeFalsy();
        });
    });

    describe("updateOptOut", function () {
        it(":sets OptOut to false and opens dialog if OptOut is true and EmergencyContacts contains entries", function () {
            emergencyInformation.OptOut(true);
            emergencyInformation.EmergencyContacts([{}, {}]);

            emergencyInformation.updateOptOut();

            expect(emergencyInformation.OptOut()).toBeFalsy();
            expect(emergencyInformation.optoutDialogIsOpen()).toBeTruthy();
        });

        it(":doesn't open dialog and calls updateEmergencyInformation if OptOut is false and EmergencyContacts contains entries", function () {
            emergencyInformation.OptOut(false);
            emergencyInformation.EmergencyContacts([{}, {}]);

            spyOn(emergencyInformation, "updateEmergencyInformation");

            emergencyInformation.updateOptOut();

            expect(emergencyInformation.updateEmergencyInformation).toHaveBeenCalled();
            expect(emergencyInformation.OptOut()).toBeFalsy();
            expect(emergencyInformation.optoutDialogIsOpen()).toBeFalsy();
        });

        it(":doesn't open dialog and calls updateEmergencyInformation if OptOut is true and EmergencyContacts does not contain entries", function () {
            emergencyInformation.OptOut(true);
            emergencyInformation.EmergencyContacts([]);

            spyOn(emergencyInformation, "updateEmergencyInformation");

            emergencyInformation.updateOptOut();

            expect(emergencyInformation.updateEmergencyInformation).toHaveBeenCalled();
            expect(emergencyInformation.OptOut()).toBeTruthy();
            expect(emergencyInformation.optoutDialogIsOpen()).toBeFalsy();
        });
    });

    describe("showHealthConditions", function() {
        it(":returns true when HideHealthConditions is false and HealthConditions has entries", function () {
            emergencyInformation.EmergencyInformationConfiguration().HideHealthConditions(false);
            emergencyInformation.HealthConditions([{}, {}]);

            expect(emergencyInformation.showHealthConditions()).toBeTruthy();
        });

        it(":returns false when HideHealthConditions is true and HealthConditions has entries", function () {
            emergencyInformation.EmergencyInformationConfiguration().HideHealthConditions(true);
            emergencyInformation.HealthConditions([{}, {}]);

            expect(emergencyInformation.showHealthConditions()).toBeFalsy();
        });

        it(":returns false when HideHealthConditions is false and HealthConditions has no entries", function () {
            emergencyInformation.EmergencyInformationConfiguration().HideHealthConditions(false);
            emergencyInformation.HealthConditions([]);

            expect(emergencyInformation.showHealthConditions()).toBeFalsy();
        });
    });

    describe("showOtherInformation", function() {
        it(":returns true when HideOtherInformation is false", function() {
            emergencyInformation.EmergencyInformationConfiguration().HideOtherInformation(false);
            expect(emergencyInformation.showOtherInformation()).toBeTruthy();
        });

        it(":returns false when HideOtherInformation is true", function() {
            emergencyInformation.EmergencyInformationConfiguration().HideOtherInformation(true);
            expect(emergencyInformation.showOtherInformation()).toBeFalsy();
        });

    });

    describe("openRemoveDialog", function () {

        it(":sets removeDialogIsOpen and contactToRemove", function () {
            var contact = { testAttribute: "test" };

            emergencyInformation.openRemoveDialog(contact);
            expect(emergencyInformation.removeDialogIsOpen()).toBeTruthy();
            expect(emergencyInformation.contactToRemove()).toBe(contact);
        });
    });

    describe("removeEmergencyContact", function () {
        it(":removes contact from view model, calls updateEmergencyInformation, and closes the remove dialog", function () {
            var testContact = { test: "test" };

            spyOn(emergencyInformation, 'updateEmergencyInformation');

            emergencyInformation.EmergencyContacts().push(testContact);
            emergencyInformation.contactToRemove(testContact);

            emergencyInformation.removeEmergencyContact();

            expect(emergencyInformation.EmergencyContacts()).not.toContain(testContact);
            expect(emergencyInformation.updateEmergencyInformation).toHaveBeenCalled();
            expect(emergencyInformation.removeDialogIsOpen()).toBeFalsy();
        });
    });

    describe("closeRemoveDialog", function () {
        it(":sets removeDialogIsOpen to false", function () {
            emergencyInformation.closeRemoveDialog();

            expect(emergencyInformation.removeDialogIsOpen()).toBeFalsy();
        });
    });
});