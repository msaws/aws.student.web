﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
var Ellucian = Ellucian || {};
Ellucian.UserProfile = Ellucian.UserProfile || {};

describe("userProfileRepository", function () {

    var repository = Ellucian.UserProfile.userProfileRepository;

    var testData = { "id": "12345" }; // Doesn't matter what data is in the object - we're just testing that get retreived something
    var testData2 = { "id": "12345", "foo": "bar" }; // A second test data set, for testing updates

    beforeEach(function () {
        account = {
            handleInvalidSessionResponse: function (data) {
                return false;
            }
        };
    });

    describe("get", function () {

        it(":gets profile information", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testData);
            });

            repository.get().then(function (result) {
                expect(result).toEqual(testData);
                done();
            });
        });

        it(":returns error when unable to retreive profile information", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            repository.get().catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.userProfileMessages.retrievalFailure);

                done();
            });
        });
    });

    describe("update", function () {

        it(":gets updated profile information", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testData2);
            });

            repository.update(testData).then(function (result) {

                expect($.ajax).toHaveBeenCalledWith({
                    url: Ellucian.UserProfile.userProfileUrls.update,
                    data: JSON.stringify({ userProfileJson: ko.toJSON(testData) }),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                });

                expect(result).toEqual(testData2);

                done();
            });
        });

        it(":returns error when update fails", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            repository.update().catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.userProfileMessages.updateFailure);

                done();
            });
        });

        it(":returns error on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testData2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            repository.update().catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.userProfileMessages.updateFailure);

                done();
            });
        });
    });

    describe("verify", function () {

        var url = "some.fake.url.com";

        it(":verifies profile information", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testData2);
            });

            repository.verify(testData, url).then(function (result) {

                expect($.ajax).toHaveBeenCalledWith({
                    url: url,
                    data: JSON.stringify({ userProfileJson: ko.toJSON(testData) }),
                    type: "POST",
                    dataType: "json",
                    contentType: jsonContentType
                });

                expect(result).toEqual(testData2);

                done();
            });
        });

        it(":returns error when verify fails", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            repository.verify(testData, url).catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.userProfileMessages.updateFailure);

                done();
            });
        });

        it(":returns error on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testData2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            repository.verify(testData, url).catch(function (result) {
                expect(result).toEqual(Ellucian.UserProfile.userProfileMessages.updateFailure);

                done();
            });
        });
    });
});