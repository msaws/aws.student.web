﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates

var Ellucian = Ellucian || {};
Ellucian.Proxy.ButtonLabels = function () {
    var editProxyAccessDialogSaveButtonLabel = '@Resources.PersonProxyResources.EditProxyAccessDialogSaveButtonLabel';
    var editProxyAccessDialogCancelButtonLabel = '@Resources.PersonProxyResources.EditProxyAccessDialogCancelButtonLabel';
    var reauthorizeConfirmButtonLabel = '@Resources.PersonProxyResources.ReauthorizeConfirmButtonLabel';
    var confirmSelectedPersonDialogButtonLabel = '@Resources.PersonProxyResources.ConfirmSelectedPersonDialogButtonLabel';
};

describe("person.proxy.view.model", function () {

    beforeEach(function () {
        viewModel = new personProxyViewModel();
    });

    it(":showUI defaults to false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading defaults to true", function () {
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":actionThrobberMessage defaults to personProxyActionThrobberMessage after initialization", function () {
        expect(viewModel.actionThrobberMessage()).toBe(personProxyActionThrobberMessage);
    });

    it(":editFormThrobberMessage defaults to personProxyEditProxyAccessThrobberMessage after initialization", function () {
        expect(viewModel.editFormThrobberMessage()).toBe(personProxyEditProxyAccessThrobberMessage);
    });

    it(":searchingThrobberMessage defaults to personProxySearchingThrobberMessage after initialization", function () {
        expect(viewModel.searchingThrobberMessage()).toBe(personProxySearchingThrobberMessage);
    });

    it(":reauthorizingThrobberMessage defaults to loadingReauthorizationSpinnerMessage after initialization", function () {
        expect(viewModel.reauthorizingThrobberMessage()).toBe(loadingReauthorizationSpinnerMessage);
    });

    it(":AnyPermissionsEnabled defaults to false after initialization", function () {
        expect(viewModel.AnyPermissionsEnabled()).toBeFalsy();
    });

    it(":DisclosureAgreementText is undefined after initialization", function () {
        expect(viewModel.DisclosureAgreementText()).toBe(undefined);
    });

    it(":AddProxyHeaderText is undefined after initialization", function () {
        expect(viewModel.AddProxyHeaderText()).toBe(undefined);
    });

    it(":ProxyFormHeaderText is undefined after initialization", function () {
        expect(viewModel.ProxyFormHeaderText()).toBe(undefined);
    });

    it(":ReauthorizationText is undefined after initialization", function () {
        expect(viewModel.ReauthorizationText()).toBe(undefined);
    });

    it(":ReauthorizationIsNeeded is undefined after initialization", function () {
        expect(viewModel.ReauthorizationIsNeeded()).toBeFalsy(undefined);
    });

    it(":PersonProxyIsEnabled defaults to true after initialization", function () {
        expect(viewModel.PersonProxyIsEnabled()).toBeTruthy();
    });

    it(":ProxyUsers defaults to empty after initialization", function () {
        expect(viewModel.ProxyUsers().constructor).toBe(Array);
        expect(viewModel.ProxyUsers().length).toBe(0);
    });

    it(":RelationshipTypes defaults to empty after initialization", function () {
        expect(viewModel.RelationshipTypes().constructor).toBe(Array);
        expect(viewModel.RelationshipTypes().length).toBe(0);
    });

    it(":activeProxyUsers defaults to empty after initialization", function () {
        expect(viewModel.activeProxyUsers().constructor).toBe(Array);
        expect(viewModel.activeProxyUsers().length).toBe(0);
    });

    it(":unassignedProxyUsers defaults to empty after initialization", function () {
        expect(viewModel.unassignedProxyUsers().constructor).toBe(Array);
        expect(viewModel.unassignedProxyUsers().length).toBe(0);
    });

    it(":selectedProxyId is undefined after initialization", function () {
        expect(viewModel.selectedProxyId()).toBe(undefined);
    });

    it(":selectedProxy is undefined after initialization", function () {
        expect(viewModel.selectedProxy()).toBe(undefined);
    });

    it(":selectedProxy is null when selectedProxyId has no matching unassigned proxy user", function () {
        viewModel.selectedProxyId("0001234");
        expect(viewModel.selectedProxy()).toBe(null);
    });

    it(":selectedProxy is not null when selectedProxyId has no matching unassigned proxy user", function () {
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            CanReceiveProxyAccess: ko.observable(true),
            PermissionGroups: ko.observableArray([])
        }]);
        viewModel.selectedProxyId("0001234");
        expect(viewModel.selectedProxy()).toBe(viewModel.unassignedProxyUsers()[0]);
    });

    it(":proxyUserBeingChangedId is undefined after initialization", function () {
        expect(viewModel.proxyUserBeingChangedId()).toBe(undefined);
    });

    it(":proxyUserBeingChanged is undefined after initialization", function () {
        expect(viewModel.proxyUserBeingChanged()).toBe(undefined);
    });

    it(":proxyUserBeingChanged is null when proxyUserBeingChangedId has no matching active proxy user", function () {
        viewModel.proxyUserBeingChangedId("0001234");
        expect(viewModel.proxyUserBeingChanged()).toBe(null);
    });

    it(":proxyUserBeingChanged is not null when selectedProxyId has no matching active proxy user", function () {
        viewModel.activeProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([])
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        expect(viewModel.proxyUserBeingChanged()).toBe(viewModel.activeProxyUsers()[0]);
    });

    it(":setUserBeingChangedAccess sets user's access level to complete when all enabled permissions assigned", function () {
        viewModel.activeProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(true),
                    IsEnabled: ko.observable(true)
                }])
            }]),
            accessLevel: ko.observable('none')
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        viewModel.setUserBeingChangedAccess();
        expect(viewModel.proxyUserBeingChanged().accessLevel()).toBe('complete');
    });

    it(":setUserBeingChangedAccess sets user's access level to complete when no enabled permissions assigned", function () {
        viewModel.activeProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(false),
                    IsInitiallyAssigned: ko.observable(true),
                    IsEnabled: ko.observable(true)
                }])
            }]),
            accessLevel: ko.observable('select')
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        viewModel.setUserBeingChangedAccess();
        expect(viewModel.proxyUserBeingChanged().accessLevel()).toBe('select');
    });

    it(":setUserBeingChangedAccess sets user's access level to partial when some but not all enabled permissions assigned", function () {
        viewModel.activeProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(true),
                    IsEnabled: ko.observable(true)
                },
                {
                    IsAssigned: ko.observable(false),
                    IsInitiallyAssigned: ko.observable(true),
                    IsEnabled: ko.observable(true)
                }])
            }]),
            accessLevel: ko.observable('select')
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        viewModel.setUserBeingChangedAccess();
        expect(viewModel.proxyUserBeingChanged().accessLevel()).toBe('select');
    });

    it(":noActiveProxiesMessage is personProxyNoActiveProxiesMessage when AnyPermissionsEnabled is false", function () {
        expect(viewModel.noActiveProxiesMessage()).toBe(personProxyNoActiveProxiesMessage);
    });

    it(":noActiveProxiesMessage is personProxyNoActiveProxiesMessage when AnyPermissionsEnabled is true", function () {
        viewModel.AnyPermissionsEnabled(true);
        expect(viewModel.noActiveProxiesMessage()).toBe(personProxyNoActiveProxiesMessage + "  " + personProxyNoActiveProxiesAddAProxyMessage);
    });

    it(":hasActiveProxies is false when no active proxy users", function () {
        expect(viewModel.hasActiveProxies()).toBeFalsy();
    });

    it(":hasActiveProxies is true when at least one active proxy users", function () {
        viewModel.activeProxyUsers.push({
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([])
        });
        expect(viewModel.hasActiveProxies()).toBeTruthy();
    });

    it(":hasUnassignedProxies is false when no unassigned proxy users", function () {
        expect(viewModel.hasUnassignedProxies()).toBeFalsy();
    });

    it(":hasUnassignedProxies is true when at least one unassigned proxy users", function () {
        viewModel.unassignedProxyUsers.push({
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([])
        });
        expect(viewModel.hasUnassignedProxies()).toBeTruthy();
    });

    it(":hasAvailableProxies is false when no active or unassigned proxy users", function () {
        expect(viewModel.hasAvailableProxies()).toBeFalsy();
    });

    it(":hasAvailableProxies is true when at least one active or unassigned proxy users", function () {
        viewModel.unassignedProxyUsers.push({
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([])
        });
        expect(viewModel.hasAvailableProxies()).toBeTruthy();
    });

    it(":isUpdating defaults to false after initialization", function () {
        expect(viewModel.isUpdating()).toBeFalsy();
    });

    it(":consentGiven defaults to false after initialization", function () {
        expect(viewModel.consentGiven()).toBeFalsy();
    });

    it(":reauthorizationConsentGiven defaults to false after initialization", function () {
        expect(viewModel.reauthorizationConsentGiven()).toBeFalsy();
    });

    it(":reauthorizationDialogIsDisplayed defaults to false after initialization", function () {
        expect(viewModel.reauthorizationDialogIsDisplayed()).toBeFalsy();
    });

    it(":reauthorizationProcessing defaults to false after initialization", function () {
        expect(viewModel.reauthorizationProcessing()).toBeFalsy();
    });

    it(":openReauthorizationDialog sets reauthorizationDialogIsDisplayed to true when DisclosureAgreementText is set", function () {
        viewModel.DisclosureAgreementText("some text");
        viewModel.openReauthorizationDialog();
        expect(viewModel.reauthorizationDialogIsDisplayed()).toBeTruthy();
    });

    it(":openReauthorizationDialog calls reauthorizeAccess when DisclosureAgreementText is null", function () {
        spyOn(viewModel, "reauthorizeAccess");
        viewModel.DisclosureAgreementText(null);
        viewModel.openReauthorizationDialog();
        expect(viewModel.reauthorizationConsentGiven()).toBeTruthy();
        expect(viewModel.reauthorizeAccess).toHaveBeenCalled();
        expect(viewModel.reauthorizationDialogIsDisplayed()).toBeFalsy();
    });

    it(":verifyingPasswordSuccessFunction calls searchForMatches when called", function () {
        spyOn(viewModel, "searchForMatches");
        viewModel.verifyingPasswordSuccessFunction();
        expect(viewModel.searchForMatches).toHaveBeenCalled();
    });

    it(":reauthorizationButtonLabel is reauthorizeAccessButtonLabel when DisclosureAgreementText is null", function () {
        viewModel.DisclosureAgreementText(null);
        expect(viewModel.reauthorizationButtonLabel()).toBe(reauthorizeAccessButtonLabel);
    });

    it(":reauthorizationButtonLabel is reviewDisclosureAgreementButtonLabel when DisclosureAgreementText is set", function () {
        viewModel.DisclosureAgreementText("some text");
        expect(viewModel.reauthorizationButtonLabel()).toBe(reviewDisclosureAgreementButtonLabel);
    });

    // TODO: SSS needs to fix this broken proxy test
    //it(":getProxyInformation sets isLoading to false, showUI to true, and consentGiven to true when no DisclosureAgreementText", function () {
    //    spyOn($, "ajax").and.callFake(function (e) {
    //        e.beforeSend({});
    //        e.success({
    //            "ProxyUsers": [{
    //                "Id": "0001234",
    //                "Name": "John Smith",
    //                "FirstName": "John",
    //                "LastName": "Smith",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Make a Payment",
    //                        "Id": "SFMAP",
    //                        "IsAssigned": true,
    //                        "IsInitiallyAssigned": true,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Parent",
    //                "IsCandidate": false,
    //                "ReauthorizationIsNeeded": false
    //            },
    //            {
    //                "Id": "0001236",
    //                "Name": "Lisa Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Account Activity",
    //                        "Id:": "SFAA",
    //                        "IsAssigned": false,
    //                        "IsInitiallyAssigned": false,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Child",
    //                "IsCandidate": false,
    //                "ReauthorizationIsNeeded": false
    //            }]
    //        });
    //        e.error({ status: 0 }, "", "");
    //        e.complete({});
    //    });
    //    viewModel.getProxyInformation();
    //    expect(viewModel.isLoading()).toBeFalsy();
    //    expect(viewModel.showUI()).toBeTruthy();
    //    expect(viewModel.consentGiven()).toBeTruthy();
    //});

    // TODO: SSS needs to fix this broken proxy test
    //it(":getProxyInformation sets isLoading to false, showUI to true, and consentGiven to false when DisclosureAgreementText set", function () {
    //    spyOn($, "ajax").and.callFake(function (e) {
    //        e.beforeSend({});
    //        e.success({
    //            "ProxyUsers": [{
    //                "Id": "0001234",
    //                "Name": "John Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Make a Payment",
    //                        "Id": "SFMAP",
    //                        "IsAssigned": true,
    //                        "IsInitiallyAssigned": true,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Parent",
    //                "IsCandidate": false,
    //                "ReauthorizationIsNeeded": false
    //            },
    //            {
    //                "Id": "0001236",
    //                "Name": "Lisa Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Account Activity",
    //                        "Id:": "SFAA",
    //                        "IsAssigned": false,
    //                        "IsInitiallyAssigned": false,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Child",
    //                "IsCandidate": false,
    //                "ReauthorizationIsNeeded": false
    //            }]
    //        });
    //        e.error({ status: 0 }, "", "");
    //        e.complete({});
    //    });
    //    viewModel.DisclosureAgreementText("Some text");
    //    viewModel.getProxyInformation();
    //    expect(viewModel.isLoading()).toBeFalsy();
    //    expect(viewModel.showUI()).toBeTruthy();
    //    expect(viewModel.consentGiven()).toBeFalsy();
    //});

    it(":getProxyInformation posts error to notification center if error occurs", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });
        spyOn($.fn, "notificationCenter");
        spyOn($.fn, "makeTableResponsive");

        var msg = personProxyUnableToLoadMessage;

        viewModel.DisclosureAgreementText("Some text");
        viewModel.getProxyInformation();
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.showUI()).toBeTruthy();
        expect(viewModel.consentGiven()).toBeFalsy();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: msg, type: "error", flash: true });
        expect($.fn.makeTableResponsive).toHaveBeenCalled();

    });

    it(":searchResults defaults to undefined", function () {
        expect(viewModel.searchResults()).toBe(undefined);
    });

    it(":searchCriteriaInvalid defaults to false", function () {
        expect(viewModel.searchCriteriaInvalid()).toBeFalsy();
    });

    it(":noPermissionsAssigned is true after initialization", function () {
        expect(viewModel.noPermissionsAssigned()).toBeTruthy();
    });

    it(":noPermissionsAssigned is false after when 1 or more permissions is assigned", function () {
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("NEWPERSON"),
            CanReceiveProxyAccess: ko.observable(true),
            FirstName: ko.observable("John"),
            LastName: ko.observable("Smith"),
            Relationship: ko.observable("Parent"),
            Email: ko.observable("john.smith@ellucian.com"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            DemographicInformation: ko.observableArray([{
                Id: ko.observable("first_name"),
                Label: ko.observable("First Name"),
                Type: ko.observable(1),
                IsRequired: ko.observable(true),
                IsVisible: ko.observable(true),
                InputType: ko.observable("text"),
                Options: ko.observableArray([]),
                MinimumLength: ko.observable(1),
                MaximumLength: ko.observable(15)
            }]),
            searchCriteria: ko.validatedObservable({
                FirstName: ko.observable("John"),
                LastName: ko.observable("Smith"),
                Relationship: ko.observable("Parent"),
                Email: ko.observable("john.smith@ellucian.com")
            })
        }]);
        viewModel.selectedProxyId("NEWPERSON");
        expect(viewModel.noPermissionsAssigned()).toBeFalsy();
    });

    it(":validateSearchCriteria sets searchCriteriaInvalid to false when consentGiven is false", function () {
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            CanReceiveProxyAccess: ko.observable(true),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.selectedProxyId("0001234");
        viewModel.consentGiven(false);
        viewModel.validateSearchCriteria();
        expect(viewModel.searchCriteriaInvalid()).toBeTruthy();
    });

    it(":validateSearchCriteria sets searchCriteriaInvalid to false when consentGiven is true and noPermissionsAssigned is true", function () {
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            CanReceiveProxyAccess: ko.observable(true),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(false),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.selectedProxyId("0001234");
        viewModel.consentGiven(true);
        viewModel.validateSearchCriteria();
        expect(viewModel.searchCriteriaInvalid()).toBeTruthy();
    });

    it(":searchForMatches sets searchResultsDialogIsLoading to false, searchResultsDialogIsDisplayed to true, updates searchResults and selectedMatch", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                "Id": "NEWPERSON",
                "Name": "John Smith",
                "FirstName": "John",
                "LastName": "Smith",
                "EffectiveDate": new Date(2015, 2, 1),
                "PermissionGroups": [{
                    "Name": "Student Finance",
                    "Permissions": [{
                        "Name": "Make a Payment",
                        "Id": "SFMAP",
                        "IsAssigned": true,
                        "IsInitiallyAssigned": true,
                        "IsEnabled": true,
                        "EffectiveDate": new Date(2015, 2, 1)
                    }]
                }],
                "DemographicInformation": [{
                    "Id": "FirstName",
                    "Label": "First Name",
                    "Type": 1,
                    "IsRequired": true,
                    "IsVisible": true,
                    "InputType": "text",
                    "Options": [],
                    "MinimumLength": 1,
                    "MaximumLength": 15
                }],
                "Relationship": "Parent"
            });
            e.error({ status: 0 }, "", "");
            e.complete({});
        });

        spyOn($.fn, "makeTableResponsive");
        spyOn(ko.mapping, 'fromJS').and.callThrough();

        viewModel.consentGiven(true);
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("NEWPERSON"),
            CanReceiveProxyAccess: ko.observable(true),
            FirstName: ko.observable("John"),
            LastName: ko.observable("Smith"),
            Relationship: ko.observable("Parent"),
            Email: ko.observable("john.smith@ellucian.com"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            DemographicInformation: ko.observableArray([{
                Id: ko.observable("first_name"),
                Label: ko.observable("First Name"),
                Type: ko.observable(1),
                IsRequired: ko.observable(true),
                IsVisible: ko.observable(true),
                InputType: ko.observable("text"),
                Options: ko.observableArray([]),
                MinimumLength: ko.observable(1),
                MaximumLength: ko.observable(15)
            }]),
            searchCriteria: ko.validatedObservable({
                FirstName: ko.observable("John"),
                LastName: ko.observable("Smith"),
                Relationship: ko.observable("Parent"),
                Email: ko.observable("john.smith@ellucian.com")
            })
        }]);
        viewModel.selectedProxyId("NEWPERSON");
        viewModel.searchForMatches();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(viewModel.searchResultsDialogIsLoading()).toBeFalsy();
        expect(viewModel.searchResultsDialogIsDisplayed()).toBeTruthy();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it(":previousSubmit defaults to false after initialization", function () {
        expect(viewModel.previousSubmit()).toBeFalsy();
    });

    it(":startSearchWorkflow calls validateSearchCriteria and showVerifyPasswordDialog when searchCriteriaInvalid is false", function () {
        spyOn(viewModel, 'validateSearchCriteria');
        spyOn(viewModel, 'showVerifyPasswordDialog');

        viewModel.searchCriteriaInvalid(false);
        viewModel.startSearchWorkflow();
        expect(viewModel.validateSearchCriteria).toHaveBeenCalled();
        expect(viewModel.showVerifyPasswordDialog).toHaveBeenCalled();
        expect(viewModel.previousSubmit()).toBeTruthy();
    });

    it(":startSearchWorkflow calls validateSearchCriteria but not showVerifyPasswordDialog when searchCriteriaInvalid is true", function () {
        spyOn(viewModel, 'validateSearchCriteria');
        spyOn(viewModel, 'showVerifyPasswordDialog');

        viewModel.searchCriteriaInvalid(true);
        viewModel.startSearchWorkflow();
        expect(viewModel.validateSearchCriteria).toHaveBeenCalled();
        expect(viewModel.showVerifyPasswordDialog).not.toHaveBeenCalled();
    });

    it(":disableSubmit is true when previousSubmit is true and consentGiven is false", function () {
        viewModel.previousSubmit(true);
        viewModel.consentGiven(false);
        expect(viewModel.disableSubmit()).toBeTruthy();
    });

    it(":disableSubmit is true when previousSubmit is true, consentGiven is true, and noPermissionsAssigned is true", function () {
        viewModel.previousSubmit(true);
        viewModel.consentGiven(true);
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("NEWPERSON"),
            CanReceiveProxyAccess: ko.observable(true),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(false),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.selectedProxyId("NEWPERSON");
        expect(viewModel.disableSubmit()).toBeTruthy();
    });

    it(":disableSubmit is false when previousSubmit is true, consentGiven is true, noPermissionsAssigned is false, and searchCriteria is valid", function () {
        viewModel.previousSubmit(true);
        viewModel.consentGiven(true);
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("NEWPERSON"),
            CanReceiveProxyAccess: ko.observable(true),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            searchCriteria: ko.observable({
                FirstName: ko.observable("John"),
                Relationship: ko.observable("P"),
                isValid: ko.observable(true)
            }),
        }]);
        viewModel.selectedProxyId("NEWPERSON");
        expect(viewModel.disableSubmit()).toBeFalsy();
    });

    it(":disableSubmit is true when previousSubmit is true, consentGiven is true, noPermissionsAssigned is false, and searchCriteria is not valid", function () {
        viewModel.previousSubmit(true);
        viewModel.consentGiven(true);
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("NEWPERSON"),
            CanReceiveProxyAccess: ko.observable(true),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            searchCriteria: ko.observable({
                FirstName: ko.observable("John"),
                Relationship: ko.observable("P"),
                isValid: ko.observable(false)
            }),
        }]);
        viewModel.selectedProxyId("NEWPERSON");
        expect(viewModel.disableSubmit()).toBeTruthy();
    });

    it(":disableSubmit is true when previousSubmit is true, consentGiven is true, noPermissionsAssigned is true on existing user", function () {
        viewModel.previousSubmit(true);
        viewModel.consentGiven(true);
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            CanReceiveProxyAccess: ko.observable(true),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(false),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.selectedProxyId("0001234");
        expect(viewModel.disableSubmit()).toBeTruthy();
    });

    it(":disableSubmit is true when previousSubmit is true, consentGiven is false, noPermissionsAssigned is false on existing user", function () {
        viewModel.previousSubmit(true);
        viewModel.consentGiven(false);
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            CanReceiveProxyAccess: ko.observable(true),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.selectedProxyId("0001234");
        expect(viewModel.disableSubmit()).toBeTruthy();
    });

    it(":disableSubmit is false when previousSubmit is true, consentGiven is true, noPermissionsAssigned is false on existing user", function () {
        viewModel.previousSubmit(true);
        viewModel.consentGiven(true);
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            CanReceiveProxyAccess: ko.observable(true),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.selectedProxyId("0001234");
        expect(viewModel.disableSubmit()).toBeFalsy();
    });

    it(":disableSubmit is false when previousSubmit is false", function () {
        viewModel.previousSubmit(false);
        expect(viewModel.disableSubmit()).toBeFalsy();
    });

    it(":updateEditedProxyInformation calls updateProxyInformation", function () {
        spyOn(viewModel, "updateProxyInformation");

        viewModel.activeProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        viewModel.consentGiven(true);

        viewModel.updateEditedProxyInformation();

        expect(viewModel.updateProxyInformation).toHaveBeenCalledWith(viewModel.proxyUserBeingChanged());

    });

    // TODO: SSS needs to fix this broken proxy test
    //it(":updateProxyInformation sets isLoading to false, showUI to true, and consentGiven to false when DisclosureAgreementText set", function () {
    //    spyOn($, "ajax").and.callFake(function (e) {
    //        e.beforeSend({});
    //        e.success({
    //            "ProxyUsers": [{
    //                "Id": "0001234",
    //                "Name": "John Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Make a Payment",
    //                        "Id": "SFMAP",
    //                        "IsAssigned": true,
    //                        "IsInitiallyAssigned": false,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Parent"
    //            },
    //            {
    //                "Id": "0001236",
    //                "Name": "Lisa Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Account Activity",
    //                        "Id:": "SFAA",
    //                        "IsAssigned": false,
    //                        "IsInitiallyAssigned": false,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Child"
    //            },
    //            {
    //                "Id": "0001236",
    //                "Name": "Lisa Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Account Activity",
    //                        "Id:": "SFAA",
    //                        "IsAssigned": false,
    //                        "IsInitiallyAssigned": false,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Child"
    //            }]
    //        });
    //        e.error({ status: 0 }, "", "");
    //        e.complete({});
    //    });

    //    spyOn(viewModel, "sortProxyUsers");
    //    spyOn($.fn, "notificationCenter");
    //    spyOn($.fn, "makeTableResponsive");

    //    viewModel.unassignedProxyUsers = ko.observableArray([{
    //        Name: ko.observable("John Smith"),
    //        Id: ko.observable("0001234"),
    //        EffectiveDate: ko.observable(new Date(2015, 2, 1)),
    //        PermissionGroups: ko.observableArray([{
    //            Permissions: ko.observableArray([{
    //                IsAssigned: ko.observable(true),
    //                IsInitiallyAssigned: ko.observable(false)
    //            }])
    //        }])
    //    }]);
    //    viewModel.selectedProxyId("0001234");
    //    viewModel.proxyUserBeingChangedId("0001234");
    //    viewModel.consentGiven(true);
    //    viewModel.updateProxyInformation(viewModel.unassignedProxyUsers()[0]);
    //    var msg = personProxySuccessfulUpdateMessage.format(viewModel.unassignedProxyUsers()[0].Name());
    //    expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: msg, type: "success", flash: true });
    //    expect($.fn.makeTableResponsive).toHaveBeenCalled();
    //    expect(viewModel.isUpdating()).toBeFalsy();
    //    expect(viewModel.editDialogIsDisplayed()).toBeFalsy();
    //    expect(viewModel.previousSubmit()).toBeFalsy();
    //});

    // TODO: SSS needs to fix this broken proxy test
    //it(":updateProxyInformation posts error notification to notification center if error occurs", function () {
    //    spyOn($, "ajax").and.callFake(function (e) {
    //        e.beforeSend({});
    //        e.success({});
    //        e.error({ status: 400 }, "", "");
    //        e.complete({});
    //    });

    //    spyOn(viewModel, "sortProxyUsers");
    //    spyOn($.fn, "notificationCenter");

    //    viewModel.unassignedProxyUsers = ko.observableArray([{
    //        Name: ko.observable("John Smith"),
    //        Id: ko.observable("0001234"),
    //        EffectiveDate: ko.observable(new Date(2015, 2, 1)),
    //        PermissionGroups: ko.observableArray([{
    //            Permissions: ko.observableArray([{
    //                IsAssigned: ko.observable(false),
    //                IsInitiallyAssigned: ko.observable(true)
    //            }])
    //        }])
    //    }]);
    //    viewModel.consentGiven(true);
    //    viewModel.selectedProxyId("0001234");
    //    viewModel.proxyUserBeingChangedId("0001234");
    //    viewModel.updateProxyInformation(viewModel.unassignedProxyUsers()[0]);
    //    var msg = personProxyFailedUpdateMessage.format(viewModel.unassignedProxyUsers()[0].Name());
    //    expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: msg, type: "error", flash: true });
    //    expect(viewModel.selectedProxyId()).toBe(null);
    //    expect(viewModel.isUpdating()).toBeFalsy();
    //    expect(viewModel.editDialogIsDisplayed()).toBeFalsy();
    //    expect(viewModel.previousSubmit()).toBeFalsy();
    //});

    it(":reauthorizeAccess sets reauthorizationProcessing to false on success", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                "ProxyUsers": [{
                    "Id": "0001234",
                    "Name": "John Smith",
                    "FirstName": "",
                    "LastName": "",
                    "EffectiveDate": new Date(2015, 2, 1),
                    "PermissionGroups": [{
                        "Name": "Student Finance",
                        "Permissions": [{
                            "Name": "Make a Payment",
                            "Id": "SFMAP",
                            "IsAssigned": true,
                            "IsInitiallyAssigned": true,
                            "IsEnabled": true,
                            "EffectiveDate": new Date(2015, 2, 1)
                        }]
                    }],
                    "Relationship": "Parent"
                },
                {
                    "Id": "0001236",
                    "Name": "Lisa Smith",
                    "FirstName": "",
                    "LastName": "",
                    "EffectiveDate": new Date(2015, 2, 1),
                    "PermissionGroups": [{
                        "Name": "Student Finance",
                        "Permissions": [{
                            "Name": "Account Activity",
                            "Id:": "SFAA",
                            "IsAssigned": false,
                            "IsInitiallyAssigned": false,
                            "IsEnabled": true,
                            "EffectiveDate": new Date(2015, 2, 1)
                        }]
                    }],
                    "Relationship": "Child"
                },
                {
                    "Id": "0001236",
                    "Name": "Lisa Smith",
                    "FirstName": "",
                    "LastName": "",
                    "EffectiveDate": new Date(2015, 2, 1),
                    "PermissionGroups": [{
                        "Name": "Student Finance",
                        "Permissions": [{
                            "Name": "Account Activity",
                            "Id:": "SFAA",
                            "IsAssigned": false,
                            "IsInitiallyAssigned": false,
                            "IsEnabled": true,
                            "EffectiveDate": new Date(2015, 2, 1)
                        }]
                    }],
                    "Relationship": "Child"
                }]
            });
            e.error({ status: 0 }, "", "");
            e.complete({});
        });

        spyOn(viewModel, "sortProxyUsers");
        spyOn($.fn, "notificationCenter");
        spyOn($.fn, "makeTableResponsive");

        viewModel.activeProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            ReauthorizationDate: ko.observable(new Date(2015, 4, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true)
                }])
            }])
        }]);
        viewModel.reauthorizeAccess();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: reauthorizationProcessedMessage, type: "success", flash: true });
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(viewModel.sortProxyUsers).toHaveBeenCalled();
        expect(viewModel.reauthorizationProcessing()).toBeFalsy();
    });

    it(":reauthorizeAccess posts error notification to notification center if error occurs", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });

        spyOn($.fn, "notificationCenter");
        spyOn($.fn, "makeTableResponsive");

        viewModel.activeProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            ReauthorizationDate: ko.observable(new Date(2015, 4, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.reauthorizeAccess();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: reauthorizationFailureMessage, type: "error", flash: true });
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(viewModel.reauthorizationProcessing()).toBeFalsy();
    });


    it(":cancelChanges sets permission assignments to false, blanks out selectedProxyId, and sets consentGiven to false", function () {
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            CanReceiveProxyAccess: ko.observable(true),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            accessLevel: ko.observable('complete')
        }]);
        viewModel.selectedProxyId("0001234");
        spyOn(viewModel, "resetAddProxySection");

        viewModel.cancelChanges();
        expect(viewModel.resetAddProxySection).toHaveBeenCalled();
        expect(viewModel.unassignedProxyUsers()[0].PermissionGroups()[0].Permissions()[0].IsAssigned()).toBeFalsy();
        expect(viewModel.selectedProxyId()).toBe(undefined);
        expect(viewModel.consentGiven()).toBeFalsy();

    });

    it(":editDialogIsLoading defaults to false after initialization", function () {
        expect(viewModel.editDialogIsLoading()).toBeFalsy();
    });

    it(":editDialogIsDisplayed defaults to false after initialization", function () {
        expect(viewModel.editDialogIsDisplayed()).toBeFalsy();
    });

    // TODO: SSS needs to fix this broken proxy test
    //it(":editProxyAccess sets editDialogIsLoading to false and editDialogIsDisplayed to true", function () {
    //    spyOn($, "ajax").and.callFake(function (e) {
    //        e.beforeSend({});
    //        e.success({
    //            "ProxyUsers": [{
    //                "Id": "0001234",
    //                "Name": "John Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": new Date(2015, 2, 1),
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Make a Payment",
    //                        "Id": "SFMAP",
    //                        "IsAssigned": true,
    //                        "IsInitiallyAssigned": true,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Parent",
    //                "IsCandidate": false,
    //                "ReauthorizationIsNeeded": false
    //            },
    //            {
    //                "Id": "0001236",
    //                "Name": "Lisa Smith",
    //                "FirstName": "",
    //                "LastName": "",
    //                "EffectiveDate": null,
    //                "PermissionGroups": [{
    //                    "Name": "Student Finance",
    //                    "Permissions": [{
    //                        "Name": "Account Activity",
    //                        "Id:": "SFAA",
    //                        "IsAssigned": false,
    //                        "IsInitiallyAssigned": false,
    //                        "IsEnabled": true,
    //                        "EffectiveDate": new Date(2015, 2, 1)
    //                    }]
    //                }],
    //                "Relationship": "Child",
    //                "IsCandidate": false,
    //                "ReauthorizationIsNeeded": false
    //            }]
    //        });
    //        e.complete({});
    //    });

    //    spyOn($.fn, "makeTableResponsive");
    //    spyOn(viewModel, "resetAddProxySection");

    //    viewModel.activeProxyUsers = ko.observableArray([{
    //        Name: ko.observable("John Smith"),
    //        Id: ko.observable("0001234"),
    //        EffectiveDate: ko.observable(new Date(2015, 2, 1)),
    //        PermissionGroups: ko.observableArray([{
    //            Permissions: ko.observableArray([{
    //                IsAssigned: ko.observable(false),
    //                IsInitiallyAssigned: ko.observable(false)
    //            }])
    //        }]),
    //        accessLevel: ko.observable('none')
    //    }]);
    //    viewModel.proxyUserBeingChangedId("0001234");
    //    viewModel.editProxyAccess(viewModel.proxyUserBeingChanged());
    //    expect(viewModel.resetAddProxySection).toHaveBeenCalled();
    //    expect(viewModel.selectedProxyId()).toBe(null);
    //    expect($.fn.makeTableResponsive).toHaveBeenCalled();
    //    expect(viewModel.editDialogIsLoading()).toBeFalsy();
    //    expect(viewModel.editDialogIsDisplayed()).toBeTruthy();
    //});

    it(":editProxyAccess posts error notification to notification center if error occurs", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });

        spyOn($.fn, "makeTableResponsive");
        spyOn($.fn, "notificationCenter");

        viewModel.activeProxyUsers = ko.observableArray([{
            Name: ko.observable("John Smith"),
            Id: ko.observable("0001234"),
            EffectiveDate: ko.observable(new Date(2015, 2, 1)),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.selectedProxyId("0001234");
        viewModel.editProxyAccess(viewModel.activeProxyUsers()[0]);
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: personProxyUnableToEditMessage, type: "error", flash: true });
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(viewModel.editDialogIsLoading()).toBeFalsy();
        expect(viewModel.editDialogIsDisplayed()).toBeTruthy();
    });

    it(":closeEditDialog resets permissions assignments, sets editDialogIsDisplayed to false, and sets proxyUserBeingChangedId to null", function () {
        viewModel.activeProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            accessLevel: ko.observable('complete')
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        viewModel.closeEditDialog();
        expect(viewModel.activeProxyUsers()[0].PermissionGroups()[0].Permissions()[0].IsAssigned()).toBe(viewModel.activeProxyUsers()[0].PermissionGroups()[0].Permissions()[0].IsInitiallyAssigned());
        expect(viewModel.editDialogIsDisplayed()).toBeFalsy();
        expect(viewModel.proxyUserBeingChangedId()).toBe(null);
    });

    it(":searchResultsDialogIsLoading defaults to false after initialization", function () {
        expect(viewModel.searchResultsDialogIsLoading()).toBeFalsy();
    });

    it(":searchResultsDialogIsDisplayed defaults to false after initialization", function () {
        expect(viewModel.searchResultsDialogIsDisplayed()).toBeFalsy();
    });

    it(":processSearchResults sorts proxy users and sends notification, calls resetAddProxySection, and nulls out selectedProxyId when successful", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                "Id": "0001234",
                "Name": "John Smith",
                "FirstName": "John",
                "LastName": "Smith",
                "EffectiveDate": new Date(2015, 2, 1),
                "PermissionGroups": [{
                    "Name": "Student Finance",
                    "Permissions": [{
                        "Name": "Make a Payment",
                        "Id": "SFMAP",
                        "IsAssigned": true,
                        "IsInitiallyAssigned": true,
                        "IsEnabled": true,
                        "EffectiveDate": new Date(2015, 2, 1)
                    }]
                }],
                "Relationship": "Parent"
            });
            e.complete({});
        });

        spyOn($.fn, "notificationCenter");
        spyOn(viewModel, "resetAddProxySection");
        spyOn(viewModel, "sortProxyUsers");

        viewModel.searchResults = ko.observable({
            Results: ko.observableArray([{
                Name: ko.observable("John Smith"),
                Id: ko.observable("NEWPERSON"),
                FirstName: ko.observable("John"),
                LastName: ko.observable("Smith"),
                Relationship: ko.observable("Parent"),
                Email: ko.observable("john.smith@ellucian.com"),
                EffectiveDate: ko.observable(new Date(2015, 2, 1)),
                PermissionGroups: ko.observableArray([{
                    Permissions: ko.observableArray([{
                        IsAssigned: ko.observable(true)
                    }])
                }]),
                searchCriteria: ko.validatedObservable({
                    FirstName: ko.observable("John"),
                    LastName: ko.observable("Smith"),
                    Relationship: ko.observable("Parent"),
                    Email: ko.observable("john.smith@ellucian.com")
                })
            }])
        });
        viewModel.processSearchResults();
        expect(viewModel.resetAddProxySection).toHaveBeenCalled();
        expect(viewModel.sortProxyUsers).toHaveBeenCalled();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: proxyRequestSuccessfulMessage, type: "success", flash: true });
        expect(viewModel.selectedProxyId()).toBe(null);
        expect(viewModel.isUpdating()).toBeFalsy();
    });

    it(":processSearchResults posts error notification to notification center if error occurs", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ textStatus: 400, responseText: '"Error occurred"' }, "", "");
            e.complete({});
        });

        spyOn($.fn, "notificationCenter");

        viewModel.searchResults = ko.observable({
            Results: ko.observableArray([{
                Name: ko.observable("John Smith"),
                Id: ko.observable("NEWPERSON"),
                FirstName: ko.observable("John"),
                LastName: ko.observable("Smith"),
                Relationship: ko.observable("Parent"),
                Email: ko.observable("john.smith@ellucian.com"),
                EffectiveDate: ko.observable(new Date(2015, 2, 1)),
                PermissionGroups: ko.observableArray([{
                    Permissions: ko.observableArray([{
                        IsAssigned: ko.observable(true)
                    }])
                }]),
                searchCriteria: ko.validatedObservable({
                    FirstName: ko.observable("John"),
                    LastName: ko.observable("Smith"),
                    Relationship: ko.observable("Parent"),
                    Email: ko.observable("john.smith@ellucian.com")
                })
            }])
        });
        viewModel.DisclosureAgreementText("Some text.");
        viewModel.processSearchResults();
        expect($.fn.notificationCenter).toHaveBeenCalled();
        expect(viewModel.selectedProxyId()).toBe(null);
        expect(viewModel.isUpdating()).toBeFalsy();
        expect(viewModel.searchResultsDialogIsDisplayed()).toBeFalsy();
    });

    it(":saveUpdatesButtonEnabled is false when proxyUserBeingChanged is not set", function () {
        expect(viewModel.saveUpdatesButtonEnabled()).toBeFalsy();
    });

    it(":saveUpdatesButtonEnabled is false when proxyUserBeingChanged is set but no permissions have changed", function () {
        viewModel.activeProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(false),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }])
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        expect(viewModel.saveUpdatesButtonEnabled()).toBeFalsy();
    });

    it(":saveUpdatesButtonEnabled is true when proxyUserBeingChanged is set and permissions have changed", function () {
        viewModel.activeProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            IsCandidate: ko.observable(false)
        }]);
        viewModel.proxyUserBeingChangedId("0001234");
        expect(viewModel.saveUpdatesButtonEnabled()).toBeTruthy();
    });

    it(":sortProxyUsers correctly sorts users into active and unassigned groups", function () {
        viewModel.ProxyUsers = ko.observableArray([{
            Id: ko.observable("0001234"),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(true)
                }])
            }]),
            Relationship: ko.observable("Parent"),
            IsCandidate: ko.observable(false)
        },
            {
                Id: ko.observable("0001236"),
                PermissionGroups: ko.observableArray([{
                    Permissions: ko.observableArray([{
                        IsAssigned: ko.observable(false),
                        IsInitiallyAssigned: ko.observable(false)
                    }])
                }]),
                Relationship: ko.observable("Child"),
                IsCandidate: ko.observable(false)
            }]);
        viewModel.sortProxyUsers();
        expect(viewModel.activeProxyUsers().length).toBe(1);
        expect(viewModel.unassignedProxyUsers().length).toBe(1);
    });

    it(":resetAddProxySection resets permissions to initial values and sets consentGiven to false when DisclosureAgreementText set", function () {
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Id: ko.observable("NEWPERSON"),
            CanReceiveProxyAccess: ko.observable(true),
            HasEmailAddress: ko.observable(false),
            Name: ko.observable("Add Another User"),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            Relationship: ko.observable("P"),
            DemographicInformation: ko.observableArray([{
                Id: ko.observable("first_name"),
                Label: ko.observable("First Name"),
                Type: ko.observable(1),
                IsRequired: ko.observable(true),
                IsVisible: ko.observable(true),
                InputType: ko.observable("text"),
                Options: ko.observableArray([]),
                MinimumLength: ko.observable(1),
                MaximumLength: ko.observable(15),
                Value: ko.observable("Johnny")
            }]),
            searchCriteria: ko.observable({
                FirstName: ko.observable("Johnny"),
                Relationship: ko.observable("P"),
                isValid: ko.observable(true)
            }),
            accessLevel: ko.observable("partial")
        }]);
        viewModel.unassignedProxyUsers()[0].DemographicInformation()[0].Value.isModified = ko.observable(true);
        viewModel.unassignedProxyUsers()[0].Relationship.isModified = ko.observable(true);
        viewModel.DisclosureAgreementText("some text");
        viewModel.selectedProxyId("NEWPERSON");
        viewModel.resetAddProxySection(viewModel);
        expect(viewModel.consentGiven()).toBeFalsy();
        expect(viewModel.searchCriteriaInvalid()).toBeFalsy();
        expect(viewModel.unassignedProxyUsers()[0].PermissionGroups()[0].Permissions()[0].IsAssigned()).toBe(viewModel.unassignedProxyUsers()[0].PermissionGroups()[0].Permissions()[0].IsInitiallyAssigned());
        expect(viewModel.unassignedProxyUsers()[0].DemographicInformation()[0].Value()).toBe(null);
        expect(viewModel.previousSubmit()).toBeFalsy();
    });

    it(":resetAddProxySection resets permissions to initial values and sets consentGiven to true when DisclosureAgreementText not set", function () {
        viewModel.unassignedProxyUsers = ko.observableArray([{
            Id: ko.observable("NEWPERSON"),
            CanReceiveProxyAccess: ko.observable(true),
            HasEmailAddress: ko.observable(false),
            Name: ko.observable("Add Another User"),
            PermissionGroups: ko.observableArray([{
                Permissions: ko.observableArray([{
                    IsAssigned: ko.observable(true),
                    IsInitiallyAssigned: ko.observable(false)
                }])
            }]),
            Relationship: ko.observable("P"),
            DemographicInformation: ko.observableArray([{
                Id: ko.observable("first_name"),
                Label: ko.observable("First Name"),
                Type: ko.observable(1),
                IsRequired: ko.observable(true),
                IsVisible: ko.observable(true),
                InputType: ko.observable("text"),
                Options: ko.observableArray([]),
                MinimumLength: ko.observable(1),
                MaximumLength: ko.observable(15),
                Value: ko.observable("Johnny")
            }]),
            searchCriteria: ko.observable({
                FirstName: ko.observable("Johnny"),
                Relationship: ko.observable("P"),
                isValid: ko.observable(true)
            }),
            accessLevel: ko.observable("partial")
        }]);
        viewModel.unassignedProxyUsers()[0].DemographicInformation()[0].Value.isModified = ko.observable(true);
        viewModel.unassignedProxyUsers()[0].Relationship.isModified = ko.observable(true);
        viewModel.selectedProxyId("NEWPERSON");
        viewModel.resetAddProxySection(viewModel);
        expect(viewModel.consentGiven()).toBeTruthy();
        expect(viewModel.searchCriteriaInvalid()).toBeFalsy();
        expect(viewModel.unassignedProxyUsers()[0].PermissionGroups()[0].Permissions()[0].IsAssigned()).toBe(viewModel.unassignedProxyUsers()[0].PermissionGroups()[0].Permissions()[0].IsInitiallyAssigned());
        expect(viewModel.unassignedProxyUsers()[0].DemographicInformation()[0].Value()).toBe(null);
    });
});