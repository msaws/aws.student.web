﻿//Copyright 2017 Ellucian Company L.P. and its affiliates

describe("person.proxy.mapping.js Tests: ", function () {

    describe("proxyUserModel: ", function () {

        // Tests related to proxyUserModel function

    });

    describe("demographicInfoModel: ", function () {

        // Tests related to demographicInfoModel function

    });

    describe("permissionGroupModel: ", function () {

        var options = {
            parent: {
                Name: ko.observable("John Smith")
            },
            data: {
                Id: "SF",
                Permissions: ko.observableArray([
                    {
                        Id: ko.observable("SF"),
                        IsEnabled: ko.observable(true),
                        IsAssigned: ko.observable(true),
                        IsInitiallyAssigned: ko.observable(false)
                    }
                ])
            }
        };

        var model;

        beforeEach(function () {
            model = new permissionGroupModel(options);
        });

        it("groupOwnerName is options.parent.Name", function () {
            expect(model.groupOwnerName()).toEqual(options.parent.Name());
        });

        describe("isVisible: ", function () {

            it("true when at least one permissions is assigned", function () {
                expect(model.isVisible()).toBeTruthy();
            });

            it("false when no permissions are assigned", function () {

                model.Permissions()[0].IsAssigned(false);

                expect(model.isVisible()).toBeFalsy();
            });

        });

        describe("permissionGroupTooltipText: ", function () {

            it("tooltip for group 'SF' ", function () {
                expect(model.permissionGroupTooltipText()).toEqual(ProxyPermissionGroupTooltipTextSF);
            });

            it("tooltip for group 'FA' ", function () {
                options.data.Id = "FA";
                expect(model.permissionGroupTooltipText()).toEqual(ProxyPermissionGroupTooltipTextFA);
            });

            it("tooltip for group 'CORE' ", function () {
                options.data.Id = "CORE";
                expect(model.permissionGroupTooltipText()).toEqual(ProxyPermissionGroupTooltipTextCore);
            });

        })
    });

});