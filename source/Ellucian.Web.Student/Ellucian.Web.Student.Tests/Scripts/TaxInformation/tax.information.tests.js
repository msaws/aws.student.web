﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
getTaxFormInfoUrl = "getUrl/";
jsonContentType = "json";
taxFormIdFromUrl = "FormW2";
account = {
    handleInvalidSessionResponse: function (data) { return false; }
};

describe("taxInformation", function () {

    it("getTaxInformation executes necessary code upon success", function () {
        
        spyOn(taxInformationViewModelInstance, 'checkForMobile');
        spyOn(ko.mapping, 'fromJS');

        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        getTaxInformation();
        expect(taxInformationViewModelInstance.checkForMobile).toHaveBeenCalled();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
        expect(taxInformationViewModelInstance.showUI()).toBeTruthy();
        expect(taxInformationViewModelInstance.isTabPanelLoading()).toBeTruthy();
    });

    it("getTaxInformation executes necessary code upon completion", function () {

        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
            e.complete();
        });
        getTaxInformation();

        expect(taxInformationViewModelInstance.isTabPanelLoading()).toBeFalsy();
    });
});