﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

var getTaxFormInfoUrl = "getUrl/";
var jsonContentType = "json";
var W2HeadingText = "W-2 Information",
    Form1095CHeadingText = "1095-C Information",
    W2ConsentGivenText = "I give consent for W-2",
    Form1095CConsentGivenText = "I give consent for 1095-C",
    W2ReceiveElectronicOnlyText = "You will recieve your W-2 in electronic only format.",
    Form1095CReceiveElectronicOnlyText = "You will recieve your 1095-C in electronic only format.",
    W2ConsentUnknownText = "Please choose a consent option for W-2.",
    Form1095CConsentUnknownText = "Please choose a consent option for 1095-C.",
    W2StatementHistoryLabel = "W-2 Statements",
    Form1095CStatementHistoryLabel = "1095-C Statements",
    W2ConsentHistoryLabel = "W-2 Consent History",
    Form1095CConsentHistoryLabel = "1095-C Consent History",
    Receive1095CPaperOnlyText = "Receive 1095-C paper only.",
    ReceiveW2PaperOnlyText = "Receive 1095-C paper only.",
    Form1095CConsentWithheldText = "Withhold consent for 1095-C.",
    FormW2ConsentWithheldText = "Withhold consent for W-2.",
    get1095cPdfUrl = "get1095cUrl/",
    getW2PdfUrl = "getW2Url/",
    formW2Id = 'FormW2',
    form1095CId = 'Form1095C',
    getTaxFormInfoUrl = 'taxFormInfoUrl';

describe("taxInformationViewModel", function () {

    // viewModel is the local instance of the view model we are testing
    var viewModel;
    var repository;
    beforeEach(function () {
        viewModel = new taxInformationViewModel();
        repository = new testTaxInformationRepository();
    });

    afterEach(function () {
        viewModel = null;
        repository = null;
    });

    it("Consents is initialized to an empty array", function () {
        expect(viewModel.Consents()).toEqual([]);
    });

    it(":showUI is initialized to false", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it("showConsentOptions is initialized to true", function () {
        expect(viewModel.showConsentOptions()).toBeTruthy();
    });

    it("currentConsentChoice is false when Consents has a length of zero", function () {
        viewModel.Consents([]);
        expect(viewModel.currentConsentChoice()).toBeFalsy();
    });

    it("currentConsentChoice is true when Consents has a first HasConsented value of true", function () {
        var consent1 = { HasConsented: ko.observable(true) },
            consent2 = { HasConsented: ko.observable(false) };
        viewModel.Consents([consent1, consent2]);
        expect(viewModel.currentConsentChoice()).toBeTruthy();
    });

    it("currentConsentChoice is false when Consents has a first HasConsented value of false", function () {
        var consent1 = { HasConsented: ko.observable(false) },
            consent2 = { HasConsented: ko.observable(true) };
        viewModel.Consents([consent1, consent2]);
        expect(viewModel.currentConsentChoice()).toBeFalsy();
    });

    it("changePreferencesButtonClicked should be set to true when changePreferences is called", function () {
        //viewModel = new taxInformationViewModel();
        expect(viewModel.changePreferencesButtonClicked()).toBeFalsy();
        viewModel.changePreferences();
        expect(viewModel.changePreferencesButtonClicked()).toBeTruthy();
    });

    //describe("displayTabs", function () {
    //    it("should set showTabs to true and selectedTaxForm when called.", function () {
    //        expect(viewModel.taxFormSelected()).toBeFalsy();
    //        viewModel.displayTabs(formW2Id);
    //        expect(viewModel.taxFormSelected()).toBeTruthy();
    //        expect(viewModel.selectedTaxForm()).toBe(formW2Id);
    //    });
    //});

    describe("hasConsentHistory", function () {
        it("should return false when the consents list is null", function () {
            viewModel.Consents = null;
            expect(viewModel.hasConsentHistory()).toBeFalsy();
        });

        it("should return false when the consents list is undefined", function () {
            viewModel.Consents = undefined;
            expect(viewModel.hasConsentHistory()).toBeFalsy();
        });

        it("should return false when the consents list is empty", function () {
            expect(viewModel.hasConsentHistory()).toBeFalsy();
        });

        it("should return true when the consents list has data", function () {
            var repositoryConsents = repository.getTaxInformation().Consents;
            for (var i = 0; i < repositoryConsents.length; i++)
                viewModel.Consents.push(new ConsentModel(repositoryConsents[i]));

            expect(viewModel.hasConsentHistory()).toBeTruthy();
        });
    });

    describe("cancelConsentChoice", function () {
        it("should set changePreferencesButtonClicked to false", function () {
            var repositoryConsents = repository.getTaxInformation().Consents;
            for (var i = 0; i < repositoryConsents.length; i++)
                viewModel.Consents.push(new ConsentModel(repositoryConsents[i]));

            viewModel.cancelConsentChoice();
            expect(viewModel.changePreferencesButtonClicked()).toBeFalsy();
            expect(viewModel.consentChoice()).toBe(viewModel.currentConsentChoice())
        });
    });

    describe("taxFormHeadingText", function () {
        it("should return the W-2 heading text when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormHeadingText()).toBe(W2HeadingText);
        });

        it("should return the 1095-C heading text when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.taxFormHeadingText()).toBe(Form1095CHeadingText);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormHeadingText()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormHeadingText()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.taxFormHeadingText()).toBe("");
        });
    });

    describe("taxFormConsentGivenText", function () {
        it("should return the W-2 consent given text when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentGivenText()).toBe(W2ConsentGivenText);
        });

        it("should return the 1095-C consent given text when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.taxFormConsentGivenText()).toBe(Form1095CConsentGivenText);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentGivenText()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentGivenText()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.taxFormConsentGivenText()).toBe("");
        });
    });

    describe("taxFormConsentGivenShortText", function () {
        it("should return the W-2 consent given short text when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentGivenShortText()).toBe(W2ReceiveElectronicOnlyText);
        });

        it("should return the 1095-C consent given short text when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.taxFormConsentGivenShortText()).toBe(Form1095CReceiveElectronicOnlyText);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentGivenShortText()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentGivenShortText()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.taxFormConsentGivenShortText()).toBe("");
        });
    });

    describe("consentUnknownText", function () {
        it("should return the W-2 consent unknown text when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.consentUnknownText()).toBe(W2ConsentUnknownText);
        });

        it("should return the 1095-C consent unknown text when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.consentUnknownText()).toBe(Form1095CConsentUnknownText);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.consentUnknownText()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.consentUnknownText()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.consentUnknownText()).toBe("");
        });
    });

    describe("statementHistoryLabel", function () {
        it("should return the W-2 statement history label when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.statementHistoryLabel()).toBe(W2StatementHistoryLabel);
        });

        it("should return the 1095-C statement history label when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.statementHistoryLabel()).toBe(Form1095CStatementHistoryLabel);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.statementHistoryLabel()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.statementHistoryLabel()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.statementHistoryLabel()).toBe("");
        });
    });

    describe("consentHistoryLabel", function () {
        it("should return the W-2 consent history label when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.consentHistoryLabel()).toBe(W2ConsentHistoryLabel);
        });

        it("should return the 1095-C consent history label when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.consentHistoryLabel()).toBe(Form1095CConsentHistoryLabel);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.consentHistoryLabel()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.consentHistoryLabel()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.consentHistoryLabel()).toBe("");
        });
    });

    describe("receivePaperOnlyLabel", function () {
        it("should return the W-2 paper-only label when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.receivePaperOnlyLabel()).toBe(ReceiveW2PaperOnlyText);
        });

        it("should return the 1095-C paper-only label when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.receivePaperOnlyLabel()).toBe(Receive1095CPaperOnlyText);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.receivePaperOnlyLabel()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.receivePaperOnlyLabel()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.receivePaperOnlyLabel()).toBe("");
        });
    });

    describe("taxFormConsentWithheldText", function () {
        it("should return the W-2 consent withhold text when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentWithheldText()).toBe(FormW2ConsentWithheldText);
        });

        it("should return the 1095-C consent withhold text when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.taxFormConsentWithheldText()).toBe(Form1095CConsentWithheldText);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentWithheldText()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.taxFormConsentWithheldText()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.taxFormConsentWithheldText()).toBe("");
        });
    });

    describe("getPdfData", function () {
        it("should return the W2 Pdf Url when the tax form is a W-2", function () {
            viewModel.IsW2(true);
            viewModel.Is1095C(false);
            expect(viewModel.getPdfData()).toBe(getW2PdfUrl);
        });

        it("should return the 1095-C Pdf Url when the tax form is a 1095-C", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(true);
            expect(viewModel.getPdfData()).toBe(get1095cPdfUrl);
        });

        it("should return blank when the tax form is a none of the accepted forms", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(false);
            expect(viewModel.getPdfData()).toBe("");
        });

        it("should return blank when IsW2 is undefined", function () {
            viewModel.IsW2(undefined);
            viewModel.Is1095C(false);
            expect(viewModel.getPdfData()).toBe("");
        });

        it("should return blank when Is1095C is undefined", function () {
            viewModel.IsW2(false);
            viewModel.Is1095C(undefined);
            expect(viewModel.getPdfData()).toBe("");
        });
    });

    describe("changeTab", function () {
        it("should call cancelConsentChoice()", function () {
            spyOn(viewModel, 'cancelConsentChoice');
            viewModel.changeTab('FormW2');
            expect(viewModel.cancelConsentChoice).toHaveBeenCalled();
        });

        it("should set location.has with the input value", function () {
            spyOn(viewModel, 'cancelConsentChoice');
            var taxFormId = "FormW2";
            viewModel.changeTab(taxFormId);
            expect(location.hash).toBe("#" + taxFormId);
        });
    });
});