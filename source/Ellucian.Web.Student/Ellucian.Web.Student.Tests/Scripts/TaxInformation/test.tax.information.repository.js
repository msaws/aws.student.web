﻿function testTaxInformationRepository() {
    var taxInformationData = {
        "Consents": [
            {
                "DisplayDate": "04/01/2015",
                "DisplayTime": "12:00",
                "HasConsented": true
            },
            {
                "DisplayDate": "03/15/2015",
                "DisplayTime": "12:00",
                "HasConsented": false
            }
        ],
        "Statements": [
            {
                "PersonId": "0001234",
                "TaxYear": "2014",
                "TaxForm": "FormW2"
            }
        ],
        "ConsentGivenText": "I consent.",
        "ConsentWithheldText": "I withhold."
    };

    this.getTaxInformation = function () {
        return taxInformationData;
    }
}