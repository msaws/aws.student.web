﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("BaseViewModel", function () {
    var viewModel;
    var successData;
    var noProxySubjectsData;
    beforeEach(function () {
        Ellucian.Storage.session.clear();
        viewModel = new BaseViewModel();
        successData = {
            ProxySubjects: [{
                EffectiveDate: null,
                FullName: "John Smith",
                Id: "0001234",
                Permissions: null
            },
            {
                EffectiveDate: null,
                FullName: "Lisa Smith",
                Id: "0001235",
                Permissions: [{
                    ApprovalEmailDocumentId: "",
                    DisclosureReleaseDocumentId: "",
                    EffectiveDate: new Date(2015, 1, 2),
                    EndDate: null,
                    Id: "123",
                    IsGranted: true,
                    ProxySubjectId: "0001235",
                    ProxyUserId: "0001234",
                    ProxyWorkflowCode: "SFAA",
                    StartDate: new Date(2015, 1, 2)
                }]
            }]
        };
        noProxySubjectsData = {
            ProxySubjects: [{
                EffectiveDate: null,
                FullName: "John Smith",
                Id: "0001234",
                Permissions: null
            }]
        };
    });

    describe("Constructor", function () {

        it(":mobileScreenWidth defaults to 480", function () {
            expect(viewModel.mobileScreenWidth).toBe(480);
        });

        it(":isMobile defaults to false", function () {
            expect(viewModel.isMobile()).toBeFalsy();
        });

        it(":mobileScreenWidth is set to optional parameter", function () {
            var expectedValue = 500;
            viewModel = new BaseViewModel(expectedValue);
            expect(viewModel.mobileScreenWidth).toBe(expectedValue);
        });

        it(":loadingMessage is personProxyLoadingThrobberMessage", function () {
            expect(viewModel.loadingMessage()).toBe(personProxyLoadingThrobberMessage);
        });

        it(":proxySelectionDialogIsLoading is false", function () {
            expect(viewModel.proxySelectionDialogIsLoading()).toBeFalsy();
        });

        it(":proxySelectionDialogIsDisplayed is false", function () {
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeFalsy();
        });

        it(":ProxySubjects is empty array", function () {
            expect(viewModel.ProxySubjects().constructor).toBe(Array);
            expect(viewModel.ProxySubjects().length).toBe(0);
        });

        it(":selectedProxySubject is null", function () {
            expect(viewModel.selectedProxySubject()).toBe(null);
        });

        it(":changingUserMessage is personProxyChangingThrobberMessage", function () {
            expect(viewModel.changingUserMessage()).toBe(personProxyChangingThrobberMessage);
        });

        it(":changingProxyUsers is false", function () {
            expect(viewModel.changingProxyUsers()).toBeFalsy();
        });

        it(":verifyingPasswordMessage is verifyingPasswordMessage", function () {
            expect(viewModel.verifyingPasswordMessage()).toBe(verifyingPasswordMessage);
        });

        it(":verifyingPasswordAltText is verifyingPasswordAltText", function () {
            expect(viewModel.verifyingPasswordAltText()).toBe(verifyingPasswordAltText);
        });

        it(":verifyingPassword is false", function () {
            expect(viewModel.verifyingPassword()).toBeFalsy();
        });

        it(":userPassword is undefined", function () {
            expect(viewModel.userPassword()).toBe(undefined);
        });

        it(":verifyPasswordMessage is undefined", function () {
            expect(viewModel.verifyPasswordMessage()).toBe(undefined);
        });

        it(":verifyPasswordDialogIsDisplayed is false", function () {
            expect(viewModel.verifyPasswordDialogIsDisplayed()).toBeFalsy();
        });
    });

    describe("checkForMobile", function () {

        it(":sets isMobile to true if window.innerWidth is less than mobileScreenWidth", function () {
            var win = { innerWidth: viewModel.mobileScreenWidth - 1 }, doc = {};

            viewModel.isMobile(false);

            viewModel.checkForMobile(win, doc);

            expect(viewModel.isMobile()).toBeTruthy();

        });

        it(":sets isMobile to true if window.innerWidth is undefined and document.documentElement is less than mobileScreenWidth", function () {
            var win = {}, doc = { documentElement: { clientWidth: viewModel.mobileScreenWidth - 1 } };

            viewModel.isMobile(false);

            viewModel.checkForMobile(win, doc);

            expect(viewModel.isMobile()).toBeTruthy();

        });

        it(":sets isMobile to false if window.innerWidth is greater than mobileScreenWidth", function () {
            var win = { innerWidth: viewModel.mobileScreenWidth + 1 }, doc = {};

            viewModel.isMobile(false);

            viewModel.checkForMobile(win, doc);

            expect(viewModel.isMobile()).toBeFalsy();
        });

        it(":sets isMobile to false if window.innerWidth is undefined and document.documentElement is greater than mobileScreenWidth", function () {
            var win = {}, doc = { documentElement: viewModel.mobileScreenWidth + 1 };

            viewModel.isMobile(false);

            viewModel.checkForMobile(win, doc);

            expect(viewModel.isMobile()).toBeFalsy();

        });
    });

    describe("isMobile", function () {
        it(":subscription triggers changeToMobile if isMobile set to true", function () {
            viewModel.isMobile(false);

            spyOn(viewModel, "changeToMobile");
            spyOn(viewModel, "changeToDesktop");

            viewModel.isMobile(true);

            expect(viewModel.changeToMobile).toHaveBeenCalled();
            expect(viewModel.changeToDesktop).not.toHaveBeenCalled();
        });

        it(":subscription triggers changeToDesktop if isMobile set to false", function () {
            viewModel.isMobile(true);

            spyOn(viewModel, "changeToMobile");
            spyOn(viewModel, "changeToDesktop");

            viewModel.isMobile(false);

            expect(viewModel.changeToMobile).not.toHaveBeenCalled();
            expect(viewModel.changeToDesktop).toHaveBeenCalled();
        });
    });

    describe("changeProxySubject", function () {
        it(":calls sets global hideProxyDialog and calls getProxySubjects when called", function () {
            var global = jasmine.getGlobal();
            global.hideProxyDialog = true;
            spyOn(viewModel, "getProxySubjects");

            viewModel.changeProxySubject();
            expect(viewModel.getProxySubjects).toHaveBeenCalled();
            expect(global.hideProxyDialog).toBeFalsy();
        });
    });

    describe("submitVerifyPasswordDialog", function () {
        describe("beforeSend", function () {
            it(":sets verifyPasswordDialogIsDisplayed to false", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend();
                });

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.verifyPasswordDialogIsDisplayed()).toBeFalsy();
            });

            it(":sets verifyingPassword to true", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend();
                });

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.verifyingPassword()).toBeTruthy();
            });
        });

        describe("success", function () {
            it("sets verifyingPasword to false", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success();
                });

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.verifyingPassword()).toBeFalsy();
            });

            it("calls verifyingPasswordSuccessFunction to false", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success();
                });

                spyOn(viewModel, "verifyingPasswordSuccessFunction");

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.verifyingPasswordSuccessFunction).toHaveBeenCalled();
            });

            it("sets verifyPasswordMessage to null", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success();
                });

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.verifyPasswordMessage()).toBe(null);
            });

            it("sets userPassword to null", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success();
                });

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.userPassword()).toBe(null);
            });
        });

        describe("error", function () {

            var jqXHR = {};

            beforeEach(function () {
                jqXHR.status = 0;
            });

            it("sets verifyingPassword to false", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.error(jqXHR);
                });

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.verifyingPassword()).toBeFalsy();
            });

            it("sets verifyPasswordDialogIsDisplayed to true", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.error(jqXHR);
                });

                viewModel.submitVerifyPasswordDialog();

                expect(viewModel.verifyPasswordDialogIsDisplayed()).toBeTruthy();
            });
        });
    });

    describe("getProxySubjects", function () {

        it(":sets proxySelectionDialogIsLoading to true before sending request", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend();
            });

            viewModel.getProxySubjects();

            expect(viewModel.proxySelectionDialogIsLoading).toBeTruthy();
        });

        it(":sets ProxySubjects when call is successful, and proxySelectionDialogIsDisplayed is true when > 1 proxy subject & global hideProxyDialog is true", function () {
            var global = jasmine.getGlobal();
            global.hideProxyDialog = true;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success(successData);
                e.complete({});
            });
            spyOn(global, "showOrHideElement").and.callThrough();
            spyOn(global, "setFocus").and.callThrough();

            viewModel.getProxySubjects();
            expect(viewModel.ProxySubjects().length).toBe(successData.ProxySubjects.length);
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeFalsy();
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-link"));
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-opsLink"));
            expect(global.setFocus).toHaveBeenCalledWith("#topOfForm");
        });

        it(":sets ProxySubjects when call is successful, and proxySelectionDialogIsDisplayed is false when only 1 proxy subject & global hideProxyDialog is true", function () {
            var global = jasmine.getGlobal();
            global.hideProxyDialog = true;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success(successData);
                e.complete({});
            });
            spyOn(global, "showOrHideElement").and.callThrough();
            spyOn(global, "setFocus").and.callThrough();

            viewModel.getProxySubjects();
            expect(viewModel.ProxySubjects().length).toBe(successData.ProxySubjects.length);
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeFalsy();
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-link"));
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-opsLink"));
            expect(global.setFocus).toHaveBeenCalledWith("#topOfForm");
        });

        it(":sets ProxySubjects when call is successful, and proxySelectionDialogIsDisplayed is true when > 1 proxy subject & global hideProxyDialog is false", function () {
            var global = jasmine.getGlobal();
            global.hideProxyDialog = false;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success(successData);
                e.complete({});
            });
            spyOn(global, "showOrHideElement").and.callThrough();
            spyOn(global, "setFocus").and.callThrough();

            viewModel.getProxySubjects();
            expect(viewModel.ProxySubjects().length).toBe(successData.ProxySubjects.length);
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeTruthy();
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-link"));
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-opsLink"));
            expect(global.setFocus).toHaveBeenCalledWith("input.buttonless");
        });

        it(":sets ProxySubjects when call is successful, and proxySelectionDialogIsDisplayed is false when only 1 proxy subject & global hideProxyDialog is false", function () {
            var global = jasmine.getGlobal();
            global.hideProxyDialog = false;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success(noProxySubjectsData);
                e.complete({});
            });
            spyOn(global, "showOrHideElement").and.callThrough();
            spyOn(global, "setFocus").and.callThrough();

            viewModel.getProxySubjects();
            expect(viewModel.ProxySubjects().length).toBe(noProxySubjectsData.ProxySubjects.length);
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeFalsy();
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-link"));
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-opsLink"));
            expect(global.setFocus).toHaveBeenCalledWith("#topOfForm");
        });

        it(":sets ProxySubjects when call is successful, and proxySelectionDialogIsDisplayed is false when no proxy subject & global hideProxyDialog is true", function () {

            var global = jasmine.getGlobal();
            global.hideProxyDialog = true;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success(noProxySubjectsData);
                e.complete({});
            });
            spyOn(global, "showOrHideElement").and.callThrough();
            spyOn(global, "setFocus").and.callThrough();

            viewModel.getProxySubjects();
            expect(viewModel.ProxySubjects().length).toBe(noProxySubjectsData.ProxySubjects.length);
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeFalsy();
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-link"));
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-opsLink"));
            expect(global.setFocus).toHaveBeenCalledWith("#topOfForm");
        });

        it(":posts error notification to notification center when error occurs", function () {

            var global = jasmine.getGlobal();
            global.hideProxyDialog = false;

            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 400 }, "", "");
                e.complete({});
            });
            spyOn(global, "showOrHideElement").and.callThrough();
            spyOn($.fn, "notificationCenter");

            viewModel.getProxySubjects();
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeFalsy();
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-link"));
            expect(global.showOrHideElement).toHaveBeenCalledWith(viewModel.ProxySubjects(), $("#changeProxyUser-opsLink"));
            expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: proxySelectionUnableToLoadSubjectsMessage, type: "error" });
        });

        it(":caches ProxySubjects when call is successful", function () {
            var global = jasmine.getGlobal();
            global.hideProxyDialog = false;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success(successData);
                e.complete({});
            });


            viewModel.getProxySubjects();
            // The second call should be cached
            viewModel.getProxySubjects();

            expect($.ajax.calls.count()).toBe(1);
        });
    });

    describe("cancelProxySelectionDialog", function () {
        it(":sets proxySelectionDialogIsDisplayed to false when called", function () {
            viewModel.proxySelectionDialogIsDisplayed(true);
            viewModel.cancelProxySelectionDialog();
            expect(viewModel.proxySelectionDialogIsDisplayed()).toBeFalsy();
            expect(viewModel.selectedProxySubject()).toBe(null);
        });
    });

    describe("saveProxySelectionDialog", function () {
        it(":sets changingUserMessage using selectedProxySubject's FullName", function () {
            spyOn($, "ajax");

            currentUserId = "12345";
            viewModel.selectedProxySubject({ FullName: ko.observable("Bob"), Id: ko.observable(currentUserId) });
            var message = personProxyChangingThrobberMessage.format(viewModel.selectedProxySubject().FullName());

            viewModel.saveProxySelectionDialog();

            expect(viewModel.changingUserMessage()).toEqual(message);
        });

        it(":sets changingProxyUsers to true", function () {
            spyOn($, "ajax").and.callFake(function(params) {
                params.beforeSend();
            });

            currentUserId = "12345";
            viewModel.selectedProxySubject({ FullName: ko.observable("Bob"), Id: ko.observable(currentUserId) });

            viewModel.saveProxySelectionDialog();

            expect(viewModel.changingProxyUsers()).toBeTruthy();
        });
    });

    describe("saveProxySelectionEnabled", function () {
        it(":is false when selectedProxySubject is not set", function () {
            expect(viewModel.saveProxySelectionEnabled()).toBeFalsy();
        });

        it(":is true when selectedProxySubject is set", function () {
            viewModel.selectedProxySubject({
                EffectiveDate: ko.observable(null),
                Id: ko.observable("0001234"),
                FullName: ko.observable("John Smith"),
                Permissions: ko.observableArray([{
                    ApprovalEmailDocumentId: ko.observable(null),
                    DisclosureReleaseDocumentId: ko.observable(null),
                    EffectiveDate: ko.observable(new Date(2015, 2, 1)),
                    EndDate: ko.observable(null),
                    Id: ko.observable("1234"),
                    IsGranted: ko.observable(true),
                    ProxySubjectId: ko.observable("0001234"),
                    ProxyUserId: ko.observable("0001235"),
                    ProxyWorkflowCode: ko.observable("SFAA"),
                    StartDate: ko.observable(new Date(2015, 2, 1))
                }])
            });
            expect(viewModel.saveProxySelectionEnabled()).toBeTruthy();
        });
    });

    describe("selectedProxySubject", function () {
        it(":global makeAnnouncement() is called when selectedProxySubject changes", function () {
            var global = jasmine.getGlobal();

            spyOn(global, "makeAnnouncement").and.callThrough();
            viewModel.selectedProxySubject({
                EffectiveDate: ko.observable(null),
                Id: ko.observable("0001234"),
                FullName: ko.observable("John Smith"),
                Permissions: ko.observableArray([{
                    ApprovalEmailDocumentId: ko.observable(null),
                    DisclosureReleaseDocumentId: ko.observable(null),
                    EffectiveDate: ko.observable(new Date(2015, 2, 1)),
                    EndDate: ko.observable(null),
                    Id: ko.observable("1234"),
                    IsGranted: ko.observable(true),
                    ProxySubjectId: ko.observable("0001234"),
                    ProxyUserId: ko.observable("0001235"),
                    ProxyWorkflowCode: ko.observable("SFAA"),
                    StartDate: ko.observable(new Date(2015, 2, 1))
                }])
            });
            expect(global.makeAnnouncement).toHaveBeenCalledWith(proxySelectionUserSelectedMessage.format(viewModel.selectedProxySubject().FullName()));
        });
    });

    describe("showVerifyPasswordDialog", function () {
        it(":sets verifyPasswordDialogIsDisplayed to true when called", function () {
            viewModel.showVerifyPasswordDialog();
            expect(viewModel.verifyPasswordDialogIsDisplayed()).toBeTruthy();
        });
    });

    describe("cancelVerifyPasswordDialog", function () {
        it(":sets verifyPasswordDialogIsDisplayed to false, verifyPasswordMessage to null, and userPassword to null when called", function () {
            viewModel.cancelVerifyPasswordDialog();
            expect(viewModel.verifyPasswordDialogIsDisplayed()).toBeFalsy();
            expect(viewModel.verifyPasswordMessage()).toBe(null);
            expect(viewModel.userPassword()).toBe(null);
        });
    });

    describe("userPassword", function () {
        it(":verifyPasswordButtonEnabled is true when userPassword is set", function () {
            viewModel.userPassword("Passw0rd");
            expect(viewModel.verifyPasswordButtonEnabled()).toBeTruthy();
        });

        it(":verifyPasswordButtonEnabled is false when userPassword is not set", function () {
            expect(viewModel.verifyPasswordButtonEnabled()).toBeFalsy();
        });

        it(":verifyPasswordButtonEnabled is false when userPassword is empty string", function () {
            viewModel.userPassword("");
            expect(viewModel.verifyPasswordButtonEnabled()).toBeFalsy();
        });
    });
});