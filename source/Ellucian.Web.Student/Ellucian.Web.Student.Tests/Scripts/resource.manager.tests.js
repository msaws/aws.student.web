﻿//define(["Ellucian.Web.Student/Scripts/resource.manager"], function (resources) {

//    describe("Test Suite for resource manager", function () {        

//        beforeEach(function () {
//            window.sessionStorage.clear();
//        });

//        describe("resx custom binding", function () {
//            it("should be defined", function () {
//                expect(ko.bindingHandlers.resx).toBeDefined();
//                expect(ko.bindingHandlers.resx.update).toBeDefined();
//            });
//        });

//        describe("get resource string from cache", function () {
//            it("should get string", function (done) {
//                window.sessionStorage.setItem("resource.key", "value");

//                spyOn(window.sessionStorage, "getItem").and.callThrough();

//                resources.get("resource.key").then(function (string) {
//                    expect(string).toEqual("value");
//                    expect(window.sessionStorage.getItem).toHaveBeenCalledWith("resource.key");
//                    done();
//                });
//            });

//            it("should set format parameters", function (done) {
//                window.sessionStorage.setItem("resource.key", "value {0}");

//                spyOn(window.sessionStorage, "getItem").and.callThrough();

//                resources.get("resource.key", ["params"]).then(function (string) {
//                    expect(string).toEqual("value params");
//                    expect(window.sessionStorage.getItem).toHaveBeenCalledWith("resource.key");
//                    done();
//                });
//            });
//        });

//        describe("get resource string from server", function () {
//            it("should get string", function (done) {
//                spyOn($, "ajax").and.returnValue(Promise.resolve({ "key": "value" }));
//                spyOn(window.sessionStorage, "setItem").and.callThrough();
//                spyOn(window.sessionStorage, "getItem").and.callThrough();

//                resources.get("resource.key").then(function (string) {
//                    expect(string).toEqual("value");
//                    expect($.ajax).toHaveBeenCalledWith(getResourceUrl + "/" + "resource");
//                    expect(window.sessionStorage.setItem).toHaveBeenCalledWith("resource.key", "value");
//                    expect(window.sessionStorage.getItem).toHaveBeenCalledWith("resource.key");
//                    done();
//                });

//            });

//            it("should set format parameters", function (done) {
//                spyOn($, "ajax").and.returnValue(Promise.resolve({ "key": "value {0}" }));

//                resources.get("another.key", ["format"]).then(function (string) {
//                    expect(string).toEqual("value format");
//                    done();
//                });

//            });
//        });
//    })
//})