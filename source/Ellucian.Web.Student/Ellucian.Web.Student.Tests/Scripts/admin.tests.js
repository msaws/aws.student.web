﻿data = {}; // global object required to initialize SiteViewModel

describe("SiteViewModel", function () {

    // viewModel is the local instance of the view model we are testing
    var viewModel;


    beforeEach(function () {
        viewModel = new SiteViewModel();
        data.Pages = [];
        data.Menus = [];
    });

    it(":Pages initialized to data.Pages", function () {
        expect(viewModel.Pages()).toEqual(data.Pages);
    });

    it(":Menus initialized to data.Menus", function () {
        expect(viewModel.Menus()).toEqual(data.Menus);
    });

    it(":removePage removes the supplied object from Pages", function () {
        var page = { key: "value" };
        var pagesLengthExpected = viewModel.Pages().length;
        viewModel.Pages.push(page);
        viewModel.removePage(page);

        expect(viewModel.Pages().length).toBe(pagesLengthExpected);
        expect(viewModel.Pages()).not.toContain(page);
    });

    it(":removeSubmenu removes the supplied object from Submenus", function () {
        var submenu = { key: "value" };
        var submenusLengthExpected = viewModel.Submenus().length;
        viewModel.Submenus.push(submenu);
        viewModel.removeSubmenu(submenu);

        expect(viewModel.Submenus().length).toBe(submenusLengthExpected);
        expect(viewModel.Submenus()).not.toContain(submenu);
    });

    it(":removeMenu removes the supplied object from Menus", function () {
        var menu = { key: "value" };
        var menusLengthExpected = viewModel.Menus().length;
        viewModel.Menus.push(menu);
        viewModel.removeMenu(menu);

        expect(viewModel.Menus().length).toBe(menusLengthExpected);
        expect(viewModel.Menus()).not.toContain(menu);
    });

    it(":traverseMenuItems removes submenus from Submenus that are also in Menu", function () {
        var menuItems = [{ "ItemType": 1, "Page": { "Id": "page-id" } }, { "ItemType": 0, "Menu": { "Id": "menu-id", "Items": [{ "ItemType": 1, "Page": { "Id": "page-id2" } }, { "ItemType": 1, "Page": { "Id": "page-id3" } }, { "ItemType": 0, "Menu": { "Id": "menu-id2", "Items": [{ "ItemType": 1, "Page": { "Id": "page-id4" } }, { "ItemType": 1, "Page": { "Id": "page-id5" } }] } }] } }];
        var submenuItems = [{ "Id": "menu-id", "Items": [{ "ItemType": 1, "Page": { "Id": "page-id2" } }, { "ItemType": 1, "Page": { "Id": "page-id3" } }] }];
        var pages = [{ "ItemType": 1, "Page": { "Id": "page-id" } }, { "ItemType": 1, "Page": { "Id": "page-id2" } }, { "ItemType": 1, "Page": { "Id": "page-id3" } }, { "ItemType": 1, "Page": { "Id": "page-id4" } }, { "ItemType": 1, "Page": { "Id": "page-id5" } }]

        viewModel.Submenus(submenuItems);
        viewModel.Menus(menuItems);
        viewModel.Pages(pages);

        viewModel.traverseMenuItems(menuItems);
        expect(viewModel.Submenus().length).toBe(0);
    });

    it(":saveConfig calls saveConfiguration", function () {

        spyOn(window, 'saveConfiguration');
        viewModel.saveConfig();

        expect(window.saveConfiguration).toHaveBeenCalled();
    });
});

describe("PageViewModel", function () {
    // viewModel is the local instance of the view model we are testing
    var viewModel;


    beforeEach(function () {

        viewModel = new PageViewModel();
        data.AllowedRoles = [];
        data.AllowedUsers = [];
    });

    it(":AllowedRoles initialized to data.AllowedRoles", function () {
        expect(viewModel.AllowedRoles()).toEqual(data.AllowedRoles);
    });

    it(":AllowedUsers initialized to data.AllowedUsers", function () {
        expect(viewModel.AllowedUsers()).toEqual(data.AllowedUsers);
    });

    it(":roleToAdd initialized to empty string", function () {
        expect(viewModel.roleToAdd()).toEqual("");
    });

    it(":userToAdd initialized to empty string", function () {
        expect(viewModel.userToAdd()).toEqual("");
    });

    it(":addRole adds roleToAdd to AllowedRoles", function () {
        viewModel.roleToAdd("test");
        viewModel.addRole();

        expect(viewModel.AllowedRoles().length).toEqual(1);
        expect(viewModel.AllowedRoles()[0]).toEqual("test");
    });

    it(":addUser adds userToAdd to AllowedUsers", function () {
        viewModel.userToAdd("test");
        viewModel.addUser();

        expect(viewModel.AllowedUsers().length).toEqual(1);
        expect(viewModel.AllowedUsers()[0]).toEqual("test");
    });

    it(":addRole2 adds role argument to AllowedRoles", function () {
        viewModel.addRole2("test");

        expect(viewModel.AllowedRoles().length).toEqual(1);
        expect(viewModel.AllowedRoles()[0]).toEqual("test");
    });

    it(":addUser2 adds user argument to AllowedUsers", function () {
        viewModel.addUser2("test");

        expect(viewModel.AllowedUsers().length).toEqual(1);
        expect(viewModel.AllowedUsers()[0]).toEqual("test");
    });

    it(":removeRole removes role from AllowedRoles", function () {
        viewModel.addRole2("test");
        viewModel.removeRole("test");

        expect(viewModel.AllowedRoles().length).toEqual(0);
    });

    it(":removeUser removes role from AllowedUsers", function () {
        viewModel.addUser2("test");
        viewModel.removeUser("test");

        expect(viewModel.AllowedUsers().length).toEqual(0);
    });

    it(":saveConfig calls saveConfiguration", function () {

        spyOn(window, 'saveConfiguration');
        viewModel.saveConfig();

        expect(window.saveConfiguration).toHaveBeenCalled();
    });
});

describe("MenuViewModel", function () {
    // viewModel is the local instance of the view model we are testing
    var viewModel;


    beforeEach(function () {

        viewModel = new MenuViewModel();
        data.AllowedRoles = [];
        data.AllowedUsers = [];
    });

    it(":AllowedRoles initialized to data.AllowedRoles", function () {
        expect(viewModel.AllowedRoles()).toEqual(data.AllowedRoles);
    });

    it(":AllowedUsers initialized to data.AllowedUsers", function () {
        expect(viewModel.AllowedUsers()).toEqual(data.AllowedUsers);
    });

    it(":roleToAdd initialized to empty string", function () {
        expect(viewModel.roleToAdd()).toEqual("");
    });

    it(":userToAdd initialized to empty string", function () {
        expect(viewModel.userToAdd()).toEqual("");
    });

    it(":addRole adds roleToAdd to AllowedRoles", function () {
        viewModel.roleToAdd("test");
        viewModel.addRole();

        expect(viewModel.AllowedRoles().length).toEqual(1);
        expect(viewModel.AllowedRoles()[0]).toEqual("test");
    });

    it(":addUser adds userToAdd to AllowedUsers", function () {
        viewModel.userToAdd("test");
        viewModel.addUser();

        expect(viewModel.AllowedUsers().length).toEqual(1);
        expect(viewModel.AllowedUsers()[0]).toEqual("test");
    });

    it(":addRole2 adds role argument to AllowedRoles", function () {
        viewModel.addRole2("test");

        expect(viewModel.AllowedRoles().length).toEqual(1);
        expect(viewModel.AllowedRoles()[0]).toEqual("test");
    });

    it(":addUser2 adds user argument to AllowedUsers", function () {
        viewModel.addUser2("test");

        expect(viewModel.AllowedUsers().length).toEqual(1);
        expect(viewModel.AllowedUsers()[0]).toEqual("test");
    });

    it(":removeRole removes role from AllowedRoles", function () {
        viewModel.addRole2("test");
        viewModel.removeRole("test");

        expect(viewModel.AllowedRoles().length).toEqual(0);
    });

    it(":removeUser removes role from AllowedUsers", function () {
        viewModel.addUser2("test");
        viewModel.removeUser("test");

        expect(viewModel.AllowedUsers().length).toEqual(0);
    });

    it(":saveConfig calls saveConfiguration", function () {

        spyOn(window, 'saveConfiguration');
        viewModel.saveConfig();

        expect(window.saveConfiguration).toHaveBeenCalled();
    });
});

describe("SettingsViewModel", function () {
    // viewModel is the local instance of the view model we are testing
    var viewModel;

    beforeEach(function () {
        data.ApiBaseUrl = "";
        data.UniqueCookieId = "";
        data.Culture = "";
        data.ShortDateFormat = "";
        data.SelectedLogLevel = "";
        data.LogLevels = [];
        data.EnableGoogleAnalytics = true;
        data.EnableCustomTracking = true;
        data.InsertCustomTrackingAfterBody = true;
        data.ProxyDimensionIndex = "1";
        data.TrackingId = "UA-0000-0";
        data.CustomTrackingAnalytics = "&lt;script&gt;console.log('hello world');&lt;/script&gt;";
        data.Favicon = { Name: "favicon.ico" };
        data.Messages = [];

        viewModel = new SettingsViewModel();

    });

    it(":properties initialized to default values", function () {
        expect(viewModel.ApiBaseUrl()).toEqual(data.ApiBaseUrl);
        expect(viewModel.UniqueCookieId()).toEqual(data.UniqueCookieId);
        expect(viewModel.Culture()).toEqual(data.Culture);
        expect(viewModel.ShortDateFormat()).toEqual(data.ShortDateFormat);
        expect(viewModel.SelectedLogLevel()).toEqual(data.SelectedLogLevel);
        expect(viewModel.LogLevels()).toEqual(data.LogLevels);
        expect(viewModel.EnableGoogleAnalytics()).toEqual(data.EnableGoogleAnalytics);
        expect(viewModel.EnableCustomTracking()).toEqual(data.EnableCustomTracking);
        expect(viewModel.InsertCustomTrackingAfterBody()).toEqual(data.InsertCustomTrackingAfterBody);
        expect(viewModel.ProxyDimensionIndex()).toEqual(data.ProxyDimensionIndex);
        expect(viewModel.TrackingId()).toEqual(data.TrackingId);
        expect(viewModel.Favicon()).toEqual(data.Favicon);
        expect(viewModel.Messages()).toEqual(data.Messages);
        expect(viewModel.confirmRemoveDialogIsOpen()).toBeFalsy();
        expect(viewModel.confirmUploadDialogIsOpen()).toBeFalsy();
        expect(viewModel.overwriteOnUpload()).toBe(undefined);

        // $("<div>").html(...).text() decodes trusted strings with html entities.
        // See http://stackoverflow.com/questions/1147359/how-to-decode-html-entities-using-jquery for more info
        expect(viewModel.CustomTrackingAnalytics()).toEqual($("<div>").html(data.CustomTrackingAnalytics).text());
    });

    it(":confirmRemoveItem calls ko.utils.postJson and sets confirmRemoveDialogIsOpen to false", function () {
        spyOn(ko.utils, 'postJson');
        viewModel.confirmRemoveItem();
        expect(ko.utils.postJson).toHaveBeenCalled();
        expect(viewModel.confirmRemoveDialogIsOpen()).toBeFalsy();
    });

    it(":submitConfirmRemoveDialogButton initialized to default values", function () {
        expect(viewModel.submitConfirmRemoveDialogButton.id).toBe("confirm-remove-ok");
        expect(viewModel.submitConfirmRemoveDialogButton.title).toBe(Ellucian.SharedComponents.ButtonLabels.buttonTextOk);
        expect(viewModel.submitConfirmRemoveDialogButton.isPrimary).toBeTruthy();
        expect(viewModel.submitConfirmRemoveDialogButton.callback).toBe(viewModel.confirmRemoveItem);
    });

    it(":confirmRemoveDialogPrompt is null when Favicon is null", function () {
        viewModel.Favicon(null);
        expect(viewModel.confirmRemoveDialogPrompt()).toBe(null);
    });

    it(":confirmRemoveDialogPrompt is null when Favicon.Name is null", function () {
        viewModel.Favicon({ Name: null });
        expect(viewModel.confirmRemoveDialogPrompt()).toBe(null);
    });

    it(":confirmRemoveDialogPrompt is correct when Favicon.Name is set", function () {
        Ellucian.Admin = Ellucian.Admin || {};
        Ellucian.Admin.Resources = Ellucian.Admin.Resources || {};
        Ellucian.Admin.Resources = {
            PermanentDeletePrompt: "Permanently delete {0}?"
        };

        expect(viewModel.confirmRemoveDialogPrompt()).toBe(Ellucian.Admin.Resources.PermanentDeletePrompt.format(viewModel.Favicon().Name));
    });

    it(":confirmUploadItem sets overwriteOnUpload to true and sets confirmUploadDialogIsOpen to false", function () {
        viewModel.confirmUploadItem();
        expect(viewModel.overwriteOnUpload()).toBeTruthy();
        expect(viewModel.confirmUploadDialogIsOpen()).toBeFalsy();
    });

    it(":submitConfirmUploadDialogButton initialized to default values", function () {
        expect(viewModel.submitConfirmUploadDialogButton.id).toBe("confirm-upload-ok");
        expect(viewModel.submitConfirmUploadDialogButton.title).toBe(Ellucian.SharedComponents.ButtonLabels.buttonTextOk);
        expect(viewModel.submitConfirmUploadDialogButton.isPrimary).toBeTruthy();
        expect(viewModel.submitConfirmUploadDialogButton.callback).toBe(viewModel.confirmUploadItem);
    });

    it(":confirmUploadDialogPrompt is null when UploadFilename is null", function () {
        viewModel.UploadFilename(null);
        expect(viewModel.confirmUploadDialogPrompt()).toBe(null);
    });

    it(":confirmUploadDialogPrompt is correct when Favicon.Name is set", function () {
        Ellucian.Admin = Ellucian.Admin || {};
        Ellucian.Admin.Resources = Ellucian.Admin.Resources || {};
        Ellucian.Admin.Resources = {
            OverwriteFilePrompt: "Overwrite {0}?"
        };

        viewModel.UploadFilename("C:\\bogus path\\favicon.ico");
        expect(viewModel.confirmUploadDialogPrompt()).toBe(Ellucian.Admin.Resources.OverwriteFilePrompt.format("favicon.ico"));
    });

    it(":Messages pushed to notification center", function () {
        data.ApiBaseUrl = "";
        data.UniqueCookieId = "";
        data.Culture = "";
        data.ShortDateFormat = "";
        data.SelectedLogLevel = "";
        data.LogLevels = [];
        data.EnableGoogleAnalytics = true;
        data.EnableCustomTracking = true;
        data.InsertCustomTrackingAfterBody = true;
        data.ProxyDimensionIndex = "1";
        data.TrackingId = "UA-0000-0";
        data.CustomTrackingAnalytics = "&lt;script&gt;console.log('hello world');&lt;/script&gt;";
        data.Favicon = { Name: "favicon.ico" };
        data.Messages = ["A favicon file name must be specified when removing a favicon."];
        spyOn($.fn, "notificationCenter");

        viewModel = new SettingsViewModel();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: data.Messages[0], type: 'error' });
    });
    
});