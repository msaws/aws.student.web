﻿// Copyright 2017 Ellucian Company L.P. and its affiliates

describe("OrganizationalStructureRepository", function () {

    var organizationalPositions;
    var repository = Ellucian.OrganizationalStructure.organizationalStructureRepository

    beforeEach(function () {
        // Sample data used to populate the view model
        organizationalPositions =
            [
               {
                   "Id": "1192",
                   "PositionId": "82",
                   "PositionTitle": "ASSOC PROF ENGLISH",
                   "PersonId": "0012239",
                   "PersonName": "Stefan J. Cole",
                   "Relationships": [

                   ]
               },
               {
                   "Id": "1305",
                   "PositionId": "86",
                   "PositionTitle": "DEAN OF ENGLISH DEPARTMENT",
                   "PersonId": "0012239",
                   "PersonName": "Stefan J. Cole",
                   "Relationships": [
                      {
                          "Id": "2",
                          "OrganizationalPersonPositionId": "1305",
                          "RelatedOrganizationalPersonPositionId": "1306",
                          "RelatedPersonId": "0015104",
                          "RelatedPersonName": "Burt Lancaster",
                          "RelatedPositionId": "121",
                          "RelatedPositionTitle": "PROVOST",
                          "Category": "MGR"
                      }
                   ]
               }
            ];
    });

    describe("getOrganizationalRelationships", function () {
        it(":rejects when fails", function (done) {
            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.reject();
                return deferred;
            });

            repository.getOrganizationalRelationships().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationGetRelationshipFailed);

                // Callback to notify Jasmine that test is done
                done();
            });
        });

        it(":rejects when not logged in", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return true; }
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve();
                return deferred;
            });

            repository.getOrganizationalRelationships().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);

                // Callback to notify Jasmine that test is done
                done();
            });
        });

        it(":resolves when done", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return false; }
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve(organizationalPositions);
                return deferred;
            });

            // What is the query? Person ID?
            repository.getOrganizationalRelationships("0012239").then(function (data) {
                expect(data).toEqual(organizationalPositions);

                // Callback to notify Jasmine that test is done
                done();
            });
        });
    });

    describe("createOrganizationalRelationship", function () {
        it(":rejects when fails", function (done) {
            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.reject();
                return deferred;
            });

            repository.createOrganizationalRelationship().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAddFailed);

                done();
            });
        });

        it(":rejects when not logged in", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return true; }
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve();
                return deferred;
            });

            repository.createOrganizationalRelationship().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);

                done();
            });
        });

        it(":resolves when done", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return false; }
            };

            var newRelationship = {
                "Id": "",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "Category": ""
            };

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve(expectedRelationship);
                return deferred;
            });

            repository.createOrganizationalRelationship(newRelationship).then(function (data) {
                expect(data).toEqual(expectedRelationship);

                done();
            });
        });

    });

    describe("updateOrganizationalRelationship", function () {
        it(":rejects when fails", function (done) {
            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.reject();
                return deferred;
            });

            repository.updateOrganizationalRelationship().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipUpdateFailed);

                done();
            });
        });

        it(":rejects when not logged in", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return true; }
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve();
                return deferred;
            });

            repository.updateOrganizationalRelationship().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);

                done();
            });
        });

        it(":resolves when done", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return false; }
            };

            var updatedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve(expectedRelationship);
                return deferred;
            });

            repository.updateOrganizationalRelationship(updatedRelationship).then(function (data) {
                expect(data).toEqual(expectedRelationship);

                done();
            });
        });
    });

    describe("deleteOrganizationalRelationship", function () {
        it(":rejects when fails", function (done) {
            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.reject();
                return deferred;
            });

            repository.deleteOrganizationalRelationship().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemovalFailed);

                done();
            });
        });

        it(":rejects when not logged in", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return true; }
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve();
                return deferred;
            });

            repository.deleteOrganizationalRelationship().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);

                done();
            });
        });

        it(":resolves when done", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return false; }
            };

            var deletedId = "5";
            var expectedRelationship = {};

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve(expectedRelationship);
                return deferred;
            });

            repository.deleteOrganizationalRelationship(deletedId).then(function (data) {
                expect(data).toEqual(expectedRelationship);

                done();
            });
        });
    });

    describe("getOrganizationalRelationshipConfiguration", function () {
        it(":getOrganizationalRelationshipConfiguration success", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return false; }
            };

            var expectedRelationshipConfiguration = { "ManagerRelationshipTypes": ["MGR"] };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve(expectedRelationshipConfiguration);
                return deferred;
            });

            repository.getOrganizationalRelationshipConfiguration().then(function (data) {
                expect(data).toEqual(expectedRelationshipConfiguration);

                done();
            });
        });

        it(":getOrganizationalRelationshipConfiguration rejects when fails", function (done) {
            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.reject();
                return deferred;
            });

            repository.getOrganizationalRelationshipConfiguration().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipConfigurationRetrievalFailed);

                done();
            });
        });
    });

    describe("getOrganizationalPersonPosition", function () {
        it(":getOrganizationalPersonPosition success", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return false; }
            };

            var personPositionId = "5";

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve(personPositionId);
                return deferred;
            });

            repository.getOrganizationalPersonPosition(personPositionId).then(function (data) {
                expect(data).toEqual(personPositionId);

                done();
            });
        });

        it(":rejects when not logged in", function (done) {
            account = {
                handleInvalidSessionResponse: function (data) { return true; }
            };

            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.resolve();
                return deferred;
            });

            repository.getOrganizationalPersonPosition().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationInvalidSession);

                done();
            });
        });

        it(":getOrganizationalPersonPosition rejects when fails", function (done) {
            spyOn($, "ajax").and.callFake(function (e) {
                var deferred = $.Deferred();

                deferred.reject();
                return deferred;
            });

            repository.getOrganizationalPersonPosition().catch(function (error) {
                expect(error).toEqual(Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationPersonPositionRetrievalFailed);

                done();
            });
        });
    });
});