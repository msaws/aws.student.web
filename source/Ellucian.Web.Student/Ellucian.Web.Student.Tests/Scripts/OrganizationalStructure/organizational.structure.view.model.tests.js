﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("OrganizationalStructureViewModel", function () {

    var viewModel;
    var organizationalPositions;
    var repository = Ellucian.OrganizationalStructure.organizationalStructureRepository

    beforeEach(function () {
        // Sample data used to populate the view model
        organizationalPositions =
            [
               {
                   "Id": "1192",
                   "PositionId": "82",
                   "PositionTitle": "ASSOC PROF ENGLISH",
                   "PersonId": "0012239",
                   "PersonName": "Stefan J. Cole",
                   "Relationships": [

                   ]
               },
               {
                   "Id": "1305",
                   "PositionId": "86",
                   "PositionTitle": "DEAN OF ENGLISH DEPARTMENT",
                   "PersonId": "0012239",
                   "PersonName": "Stefan J. Cole",
                   "Relationships": [
                      {
                          "Id": "2",
                          "OrganizationalPersonPositionId": "1305",
                          "PersonId": "0012239",
                          "PersonName": "Stefan J. Cole",
                          "PositionId": "86",
                          "PositionTitle": "DEAN OF ENGLISH DEPARTMENT",
                          "RelatedOrganizationalPersonPositionId": "1306",
                          "RelatedPersonId": "0015104",
                          "RelatedPersonName": "Burt Lancaster",
                          "RelatedPositionId": "121",
                          "RelatedPositionTitle": "PROVOST",
                          "Category": "MGR"
                      },
                      {
                          "Id": "2",
                          "OrganizationalPersonPositionId": "1309",
                          "PersonId": "0012240",
                          "PositionId": "81",
                          "PersonName": "James Cannon",
                          "PositionTitle": "ENGLISH DEPARTMENT ASSISTANT",
                          "RelatedOrganizationalPersonPositionId": "1305",
                          "RelatedPersonId": "0012239",
                          "RelatedPersonName": "Stefan J. Cole",
                          "RelatedPositionId": "86",
                          "RelatedPositionTitle": "DEAN OF ENGLISH DEPARTMENT",
                          "Category": "MGR"
                      },
                      {
                          "Id": "2",
                          "OrganizationalPersonPositionId": "1309",
                          "PersonId": "0012241",
                          "PositionId": "81",
                          "PersonName": "Lily Porter",
                          "PositionTitle": "ENGLISH DEPARTMENT ASSISTANT",
                          "RelatedOrganizationalPersonPositionId": "1305",
                          "RelatedPersonId": "0012239",
                          "RelatedPersonName": "Stefan J. Cole",
                          "RelatedPositionId": "86",
                          "RelatedPositionTitle": "DEAN OF ENGLISH DEPARTMENT",
                          "Category": "MGR"
                      }
                   ]
               }
            ];

        viewModel = new Ellucian.OrganizationalStructure.OrganizationalStructureViewModel(repository);
        // Fake load configuration data
        viewModel.ManagerRelationshipTypes(["MGR"]);
    });

    describe("constructor", function () {
        it(":searchCriteriaForUser should be an empty string", function () {
            expect(viewModel.searchCriteriaForUser()).toBe("");
        });

        it(":resultsForUser should be an empty array by default", function () {
            expect(viewModel.resultsForUser()).toEqual([]);
        });

        it(":selectedUser should be null by default", function () {
            expect(viewModel.selectedUser()).toEqual(null);
        });

        it(":searchCriteriaForManager should be an empty string", function () {
            expect(viewModel.searchCriteriaForManager()).toBe("");
        });

        it(":resultsForManager should be an empty array by default", function () {
            expect(viewModel.resultsForManager()).toEqual([]);
        });

        it(":selectedManager should be null by default", function () {
            expect(viewModel.selectedManager()).toEqual(null);
        });

    });

    describe("loadConfiguration", function () {
        it(":loadConfiguration sets manager relationship types", function (done) {
            spyOn(repository, 'getOrganizationalRelationshipConfiguration').and.callFake(function () {
                return Promise.resolve({ "ManagerRelationshipTypes": ["NEWMGR"] });
            });
            var loadConfigurationPromise = viewModel.loadConfiguration();
            loadConfigurationPromise.then(function () {
                expect(viewModel.ManagerRelationshipTypes()).toEqual(["NEWMGR"]);
                done();
            });
        });
    });

    describe("selectPerson", function () {
        it(":selectUser sets selectedUser and clears the search results", function () {
            var result = organizationalPositions[0];

            viewModel.selectUser(result);
            expect(viewModel.selectedUser()).toEqual(result);
            expect(viewModel.resultsForUser()).toEqual([]);
        });

        it(":userHasManager is true if selectedUser has a relationship with a category of manager", function () {
            // Second item in array has "Category" of "MGR"
            var result = organizationalPositions[1];

            viewModel.selectUser(result);

            expect(viewModel.userHasManager()).toBe(true);
        });

        it(":userHasManager is false if selectedUser does not have a relationship with a category of manager", function () {
            // First item in array has no relationships and therefore no "Category" of "MGR"
            var result = organizationalPositions[0];

            viewModel.selectUser(result);

            expect(viewModel.userHasManager()).toBe(false);
        });
    });

    describe("searchForPerson", function () {
        it(":searchForUser pass search criteria and set the results", function (done) {
            var promise = new Promise(function (resolve) { resolve(organizationalPositions); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            viewModel.searchCriteriaForUser("0012239");
            viewModel.searchForUser();

            promise.then(function () {
                expect(viewModel.resultsForUser()).toEqual(organizationalPositions);

                done();
            });
        });

        it(":searchForUser empty result causes search error", function (done) {
            var promise = new Promise(function (resolve) { resolve([]); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            viewModel.searchCriteriaForUser("bad search data");
            viewModel.searchForUser();

            promise.then(function () {
                expect(viewModel.resultsForUser()).toEqual([]);
                expect(viewModel.userSearchErrorOccurred()).toBe(true);

                done();
            });
        });

        it(":searchForManager empty result causes search error", function (done) {
            var promise = new Promise(function (resolve) { resolve([]); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            viewModel.searchCriteriaForManager("bad search data");
            viewModel.searchForManager();

            promise.then(function () {
                expect(viewModel.resultsForManager()).toEqual([]);
                expect(viewModel.managerSearchErrorOccurred()).toBe(true);
                done();
            });
        });

        it(":searchForManager pass search criteria and set the results", function (done) {
            var promise = new Promise(function (resolve) { resolve(organizationalPositions); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            viewModel.searchCriteriaForManager("0015104");
            viewModel.searchForManager();

            promise.then(function () {
                expect(viewModel.resultsForManager()).toEqual(organizationalPositions);

                done();
            });
        });


        it(":searchForSubordinate empty result causes search error", function (done) {
            var promise = new Promise(function (resolve) { resolve([]); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            viewModel.searchCriteriaForSubordinate("bad search data");
            viewModel.searchForSubordinate();

            promise.then(function () {
                expect(viewModel.resultsForSubordinate()).toEqual([]);
                expect(viewModel.subordinateSearchErrorOccurred()).toBe(true);
                done();
            });
        });

        it(":searchForSubordinate pass search criteria and set the results", function (done) {
            var promise = new Promise(function (resolve) { resolve(organizationalPositions); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            viewModel.searchCriteriaForSubordinate("0015104");
            viewModel.searchForSubordinate();

            promise.then(function () {
                expect(viewModel.resultsForSubordinate()).toEqual(organizationalPositions);

                done();
            });
        });

        it(":userSearchErrorOccurred displays when search fails", function (done) {
            // Repository promise (rejected)
            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            var searchForUserPromise = viewModel.searchForUser();

            searchForUserPromise.then(function (error) {
                expect(viewModel.userSearchErrorOccurred()).toBe(true);

                done();
            });
        });


        it(":managerSearchErrorOccurred displays when search fails", function (done) {
            // Repository promise (rejected)
            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            var searchForManagerPromise = viewModel.searchForManager();

            searchForManagerPromise.then(function (error) {
                expect(viewModel.managerSearchErrorOccurred()).toBe(true);

                done();
            });
        });

        it(":subordinateSearchErrorOccurred displays when search fails", function (done) {
            // Repository promise (rejected)
            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "getOrganizationalRelationships").and.callFake(function (e) {
                return promise;
            });

            var searchForSubordinatePromise = viewModel.searchForSubordinate();

            searchForSubordinatePromise.then(function (error) {
                expect(viewModel.subordinateSearchErrorOccurred()).toBe(true);

                done();
            });
        });
    });

    describe("Page reset", function () {
        it(":resetPage resets search results", function () {
            viewModel.resetPage();
            expect(viewModel.searchCriteriaForUser()).toEqual("");
            expect(viewModel.searchCriteriaForManager()).toEqual("");
            expect(viewModel.searchCriteriaForSubordinate()).toEqual("");
            expect(viewModel.resultsForUser()).toEqual([]);
            expect(viewModel.resultsForManager()).toEqual([]);
            expect(viewModel.resultsForSubordinate()).toEqual([]);
            expect(viewModel.selectedManager()).toEqual(null);
            expect(viewModel.selectedSubordinate()).toEqual(null);

        })
    });

    describe("Edit Supervisor dialog", function () {
        it("editManager opens dialog", function () {
            viewModel.editManager();
            expect(viewModel.editManagerDialogIsVisible()).toEqual(true);
        });

        it("closeEditManagerDialog closes dialog", function () {
            viewModel.closeEditManagerDialog();
            expect(viewModel.editManagerDialogIsVisible()).toEqual(false);
        });
    });

    describe("Edit Subordinates dialog", function () {
        it("editSubordinates opens dialog", function () {
            viewModel.editSubordinates();
            expect(viewModel.editSubordinatesDialogIsVisible()).toEqual(true);
        });

        it("closeEditSubordinatesDialog closes dialog", function () {
            viewModel.closeEditSubordinatesDialog();
            expect(viewModel.editSubordinatesDialogIsVisible()).toEqual(false);
        });
    });

    describe("Removal of manager", function () {
        it(":removeManager shows success notification and resets page on successful removal", function (done) {
            var result = organizationalPositions[1];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            spyOn(repository, 'deleteOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) { resolve({}) });
            })

            // Also need to spy on getOrganizationalPersonPosition because we will call updateSelectedUserData
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[0].RelatedOrganizationalPersonPositionId;
            var newPerson = {
                "Id": "1306",
                "PositionId": "121",
                "PositionTitle": "PROVOST",
                "PersonId": "0015104",
                "PersonName": "Burt Lancaster",
                "Relationships": []
            };

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (positionToChange) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });

            spyOn($.fn, "notificationCenter");

            var removeManagerPromise = viewModel.removeManager();
            removeManagerPromise.then(function () {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemoved, type: 'success', flash: true });
                expect(viewModel.searchCriteriaForUser()).toEqual("");
                expect(viewModel.searchCriteriaForManager()).toEqual("");
                expect(viewModel.searchCriteriaForSubordinate()).toEqual("");
                expect(viewModel.resultsForUser()).toEqual([]);
                expect(viewModel.resultsForManager()).toEqual([]);
                expect(viewModel.resultsForSubordinate()).toEqual([]);
                expect(viewModel.selectedManager()).toEqual(null);
                expect(viewModel.selectedSubordinate()).toEqual(null);
                done();
            });

        });

        it(":removeManager shows error on failure", function (done) {
            var result = organizationalPositions[1];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            spyOn(repository, 'deleteOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) { reject("To do: Failure") });
            })
            spyOn($.fn, "notificationCenter");

            var removeManagerPromise = viewModel.removeManager();
            removeManagerPromise.then(function () {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemovalFailed, type: "error" });
                done();
            });
        });
    });

    describe("Removal of subordinate", function () {
        it(":removeSubordinate shows success notification and resets page on successful removal", function (done) {
            var result = organizationalPositions[1];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            spyOn(repository, 'deleteOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) { resolve({}) });
            })

            // Also need to spy on getOrganizationalPersonPosition because we will call updateSelectedUserData
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[0].RelatedOrganizationalPersonPositionId;
            var newPerson = {
                "Id": "1306",
                "PositionId": "121",
                "PositionTitle": "PROVOST",
                "PersonId": "0015104",
                "PersonName": "Burt Lancaster",
                "Relationships": []
            };

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (positionToChange) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });

            spyOn($.fn, "notificationCenter");

            var removeSubordinatePromise = viewModel.removeSubordinate("1306");
            removeSubordinatePromise.then(function () {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemoved, type: 'success', flash: true });
                expect(viewModel.searchCriteriaForUser()).toEqual("");
                expect(viewModel.searchCriteriaForManager()).toEqual("");
                expect(viewModel.searchCriteriaForSubordinate()).toEqual("");
                expect(viewModel.resultsForUser()).toEqual([]);
                expect(viewModel.resultsForManager()).toEqual([]);
                expect(viewModel.resultsForSubordinate()).toEqual([]);
                expect(viewModel.selectedManager()).toEqual(null);
                expect(viewModel.selectedSubordinate()).toEqual(null);
                done();
            });
        });

        it(":removeSubordinate shows error on failure", function (done) {
            var result = organizationalPositions[1];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            spyOn(repository, 'deleteOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) { reject("To do: Failure") });
            })
            spyOn($.fn, "notificationCenter");

            var removeSubordinatePromise = viewModel.removeSubordinate("1306");
            removeSubordinatePromise.then(function () {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipRemovalFailed, type: "error" });
                done();
            });
        });
    });

    describe("Add or update manager", function () {
        it(":addOrUpdateManager successfully adds a manager when the user has no manager", function (done) {
            // Choose the organization position without a manager relationship
            var result = organizationalPositions[0];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            viewModel.searchCriteriaForManager("0099999");
            var managerPositions = [{
                "Id": "7777",
                "PositionId": "888",
                "PositionTitle": "CHEMISTRY TEACHER",
                "PersonId": "0099999",
                "PersonName": "Walter White",
                "Relationships": []
            }];

            viewModel.resultsForManager(managerPositions);

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };
            spyOn(repository, 'createOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) {
                    resolve(expectedRelationship);
                });
            });

            // Also need to spy on getOrganizationalPersonPosition because we will call updateSelectedUserData
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[0].RelatedOrganizationalPersonPositionId;
            var newPerson = {
                "Id": "1306",
                "PositionId": "121",
                "PositionTitle": "PROVOST",
                "PersonId": "0015104",
                "PersonName": "Burt Lancaster",
                "Relationships": []
            };

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (positionToChange) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });

            spyOn($.fn, "notificationCenter");

            var addManagerPromise = viewModel.addOrUpdateManager(expectedRelationship);
            addManagerPromise.then(function (data) {

                expect(data).toEqual(expectedRelationship);
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAdded, type: 'success', flash: true });

                done();
            });
        });

        it(":addOrUpdateManager adding a manager shows error notification when failing", function (done) {
            // Choose the organization position without a manager relationship
            var result = organizationalPositions[0];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            viewModel.searchCriteriaForManager("0099999");
            var managerPositions = [{
                "Id": "7777",
                "PositionId": "888",
                "PositionTitle": "CHEMISTRY TEACHER",
                "PersonId": "0099999",
                "PersonName": "Walter White",
                "Relationships": []
            }];

            viewModel.resultsForManager(managerPositions);

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };

            spyOn(repository, 'createOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) {
                    reject("To do: Failure");
                });
            });
            spyOn($.fn, "notificationCenter");

            var addManagerPromise = viewModel.addOrUpdateManager(expectedRelationship);
            addManagerPromise.then(function (data) {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAddFailed, type: "error" });

                done();
            });
        });

        it(":addOrUpdateManager successfully updates a manager when the user has a manager", function (done) {
            // Choose the organization position with a manager relationship
            var result = organizationalPositions[1];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            viewModel.searchCriteriaForManager("0099999");
            var managerPositions = [{
                "Id": "7777",
                "PositionId": "888",
                "PositionTitle": "CHEMISTRY TEACHER",
                "PersonId": "0099999",
                "PersonName": "Walter White",
                "Relationships": []
            }];

            viewModel.resultsForManager(managerPositions);

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };
            spyOn(repository, 'updateOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) {
                    resolve(expectedRelationship);
                });
            });

            // Also need to spy on getOrganizationalPersonPosition because we will call updateSelectedUserData
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[0].RelatedOrganizationalPersonPositionId;
            var newPerson = {
                "Id": "1306",
                "PositionId": "121",
                "PositionTitle": "PROVOST",
                "PersonId": "0015104",
                "PersonName": "Burt Lancaster",
                "Relationships": []
            };

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (positionToChange) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });

            spyOn($.fn, "notificationCenter");

            var addManagerPromise = viewModel.addOrUpdateManager(expectedRelationship);
            addManagerPromise.then(function (data) {

                expect(data).toEqual(expectedRelationship);
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipUpdated, type: 'success', flash: true });

                done();
            });
        });

        it(":addOrUpdateManager updating a manager shows error notification when failing", function (done) {
            // Choose the organization position with a manager relationship
            var result = organizationalPositions[1];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            viewModel.searchCriteriaForManager("0099999");
            var managerPositions = [{
                "Id": "7777",
                "PositionId": "888",
                "PositionTitle": "CHEMISTRY TEACHER",
                "PersonId": "0099999",
                "PersonName": "Walter White",
                "Relationships": []
            }];

            viewModel.resultsForManager(managerPositions);

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };

            spyOn(repository, 'updateOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) {
                    reject("To do: Failure");
                });
            });
            spyOn($.fn, "notificationCenter");

            var addManagerPromise = viewModel.addOrUpdateManager(expectedRelationship);
            addManagerPromise.then(function (data) {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipUpdateFailed, type: "error" });

                done();
            });
        });
    });

    describe("Add subordinate", function () {
        it(":addSubordinate successfully adds subordinate", function (done) {
            // Choose the organization position without subordinates
            var result = organizationalPositions[0];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            viewModel.searchCriteriaForSubordinate("0099999");
            var subordinatePositions = [{
                "Id": "7777",
                "PositionId": "888",
                "PositionTitle": "CHEMISTRY TEACHER",
                "PersonId": "0099999",
                "PersonName": "Walter White",
                "Relationships": []
            }];

            viewModel.resultsForSubordinate(subordinatePositions);

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER"
            };
            spyOn(repository, 'createOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) {
                    resolve(expectedRelationship);
                });
            });

            // Also need to spy on getOrganizationalPersonPosition because we will call updateSelectedUserData
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[0].RelatedOrganizationalPersonPositionId;
            var newPerson = {
                "Id": "1306",
                "PositionId": "121",
                "PositionTitle": "PROVOST",
                "PersonId": "0015104",
                "PersonName": "Burt Lancaster",
                "Relationships": []
            };

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (positionToChange) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });

            spyOn($.fn, "notificationCenter");

            var addSubordinatePromise = viewModel.addSubordinate(expectedRelationship);
            addSubordinatePromise.then(function (data) {

                expect(data).toEqual(expectedRelationship);
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAdded, type: 'success', flash: true });

                done();
            });
        });

        it(":addSubordinate shows error notification when failing", function (done) {
            // Choose the organization position with a manager relationship
            var result = organizationalPositions[0];
            viewModel.searchCriteriaForUser("0012239");
            viewModel.resultsForUser(organizationalPositions);
            viewModel.selectUser(result);
            viewModel.searchCriteriaForSubordinate("0099999");
            var subordinatePositions = [{
                "Id": "7777",
                "PositionId": "888",
                "PositionTitle": "CHEMISTRY TEACHER",
                "PersonId": "0099999",
                "PersonName": "Walter White",
                "Relationships": []
            }];

            viewModel.resultsForSubordinate(subordinatePositions);

            var expectedRelationship = {
                "Id": "5",
                "OrganizationalPersonPositionId": "1305",
                "RelatedOrganizationalPersonPositionId": "7777",
                "RelatedPersonId": "0099999",
                "RelatedPersonName": "Walter White",
                "RelatedPositionId": "888",
                "RelatedPositionTitle": "CHEMISTRY TEACHER",
                "Category": "MGR"
            };

            spyOn(repository, 'createOrganizationalRelationship').and.callFake(function () {
                return new Promise(function (resolve, reject) {
                    reject("To do: Failure");
                });
            });
            spyOn($.fn, "notificationCenter");

            var addManagerPromise = viewModel.addOrUpdateManager(expectedRelationship);
            addManagerPromise.then(function (data) {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationRelationshipAddFailed, type: "error" });

                done();
            });
        });
    });

    describe("Update selected user data", function () {
        it(":updateSelectedUserData successfully updates user data", function (done) {
            var newPerson = organizationalPositions[1];

            viewModel.selectUser(newPerson);

            var selectedUserId = viewModel.selectedUser().Id;

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (selectedUserId) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });

            var updateSelectedUserDataPromise = viewModel.updateSelectedUserData();
            updateSelectedUserDataPromise.then(function () {
                expect(newPerson).toEqual(viewModel.selectedUser());

                done();
            });
        });

        it(":updateSelectedUserData shows error notification when failing", function (done) {
            var newPerson = organizationalPositions[1];

            viewModel.selectUser(newPerson);

            // Repository promise (rejected)
            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "getOrganizationalPersonPosition").and.callFake(function (e) {
                return promise;
            });

            spyOn($.fn, "notificationCenter");

            var updateSelectedUserDataPromise = viewModel.updateSelectedUserData();

            updateSelectedUserDataPromise.then(function (error) {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationUserRefreshFailed, type: "error" });

                done();
            });
        });
    });


    describe("Change focus for person position ID", function () {
        it(":focusClickedManager successfully changes person position", function (done) {
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[0].RelatedOrganizationalPersonPositionId;
            var newPerson = {
                "Id": "1306",
                "PositionId": "121",
                "PositionTitle": "PROVOST",
                "PersonId": "0015104",
                "PersonName": "Burt Lancaster",
                "Relationships": []
            };

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (positionToChange) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });
            
            var focusClickedManagerPromise = viewModel.focusClickedManager(positionToChange);

            focusClickedManagerPromise.then(function () {
                expect(newPerson).toEqual(viewModel.selectedUser());
                done();
            });

        });


        it(":focusClickedManager  shows error notification when failing", function (done) {
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[0].RelatedOrganizationalPersonPositionId;

            // Repository promise (rejected)
            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "getOrganizationalPersonPosition").and.callFake(function (e) {
                return promise;
            });

            spyOn($.fn, "notificationCenter");

            var focusClickedManagerPromise = viewModel.focusClickedManager(positionToChange);

            focusClickedManagerPromise.then(function (error) {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationUserNavigationFailed, type: "error" });

                done();
            });
        });

        it(":focusClickedSubordinate successfully changes person position", function (done) {
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[1].OrganizationalPersonPositionId;
            var newPerson = {
                 "Id": "1309",
                "PersonId": "0012240",
                "PositionId": "81",
                "PersonName": "James Cannon",
                "PositionTitle": "ENGLISH DEPARTMENT ASSISTANT",
                "Relationships": []
            };

            spyOn(repository, 'getOrganizationalPersonPosition').and.callFake(function (positionToChange) {
                return new Promise(function (resolve, reject) {
                    resolve(newPerson); // JSON for new person position
                });
            });

            var focusClickedManagerPromise = viewModel.focusClickedSubordinate(positionToChange);

            focusClickedManagerPromise.then(function () {
                expect(newPerson).toEqual(viewModel.selectedUser());
                done();
            });

        });


        it(":focusClickedSubordinate  shows error notification when failing", function (done) {
            var currentPerson = organizationalPositions[1];
            var positionToChange = currentPerson.Relationships[1].OrganizationalPersonPositionId;

            // Repository promise (rejected)
            var promise = new Promise(function (resolve, reject) { reject(); });

            spyOn(repository, "getOrganizationalPersonPosition").and.callFake(function (e) {
                return promise;
            });

            spyOn($.fn, "notificationCenter");

            var focusClickedManagerPromise = viewModel.focusClickedSubordinate(positionToChange);

            focusClickedManagerPromise.then(function (error) {
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.OrganizationalStructure.organizationalStructureMessages.NotificationUserNavigationFailed, type: "error" });

                done();
            });
        });
    });
});