﻿/*This file requires all the tests and source to be bundled by webpack and then run by karma.
 * Notably it bundles site level components and modules, and their tests, that are used by Time Management.
 * If you aren't writing code that's built by webpack, you probably don't need to modify this file.
 */

var testsContext = require.context('.', true, /(auto\.save|collapsible\.group|dropdown|resource\.manager).*tests\.js$/);
testsContext.keys().forEach(testsContext);

var sourceContext = require.context('../../Ellucian.Web.Student/Scripts/', true, /^(_AutoSave|_Dropdown|resource\.manager).*\.js$/);
sourceContext.keys().forEach(sourceContext);
