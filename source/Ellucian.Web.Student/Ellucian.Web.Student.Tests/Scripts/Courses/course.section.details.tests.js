﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
var global;
var dialogContainer;

describe("course.sections.details.js Tests: ", function () {

    beforeEach(function () {
        global = jasmine.getGlobal();
    });

    describe("global variables: ", function () {

        it("addSectionCallback is null", function () {
            expect(global.addSectionCallback).toEqual(null);
        });

        it("addCourseCallback is null", function () {
            expect(global.addCourseCallback).toEqual(null);
        });

        it("updateCourseCallback is null", function () {
            expect(global.updateCourseCallback).toEqual(null);
        });

        it("currentSection is null", function () {
            expect(global.currentSection).toEqual(null);
        });

        it("currentCourse is null", function () {
            expect(global.currentCourse).toEqual(null);
        });

        it("currentCourseOriginalTerm is null", function () {
            expect(global.currentCourseOriginalTerm).toEqual(null);
        });

        it("lastFocus is null", function () {
            expect(global.lastFocus).toEqual(null);
        });

        it("defaultDetailsOptions is initialized", function () {
            expect(global.defaultDetailsOptions).toEqual({
                course: null,
                allowAdding: false,
                allowUpdating: false,
                addTerms: null,
                addCallback: null,
                creditsOverride: null,
                gradingTypeOverride: null
            });
        });

        it("xhr is null", function () {
            expect(global.xhr).toEqual(null);
        });

        it("cvm is a courseDetailsViewModel", function () {
            expect(global.cvm instanceof courseDetailsViewModel).toBeTruthy();
        });

        it("svm is a sectionDetailsViewModel", function () {
            expect(global.svm instanceof sectionDetailsViewModel).toBeTruthy();
        });

    });

    describe("courseDetailsViewModel: ", function () {

        it("FullTitle initialized", function () {
            expect(global.cvm.FullTitle()).toEqual(undefined);
        });

        it("Description initialized", function () {
            expect(global.cvm.Description()).toEqual(undefined);
        });

        it("TermsOffered initialized", function () {
            expect(global.cvm.TermsOffered()).toEqual(undefined);
        });

        it("YearsOffered initialized", function () {
            expect(global.cvm.YearsOffered()).toEqual(undefined);
        });

        it("CreditsStatic initialized", function () {
            expect(global.cvm.CreditsStatic()).toEqual(undefined);
        });

        it("CreditsVariableIncrements initialized", function () {
            expect(global.cvm.CreditsVariableIncrements()).toEqual([]);
        });

        it("IsFreeFormCredits initialized as false", function () {
            expect(global.cvm.IsFreeFormCredits()).toBeFalsy();
        });

        it("IsStaticCredits initialized as false", function () {
            expect(global.cvm.IsStaticCredits()).toBeFalsy();
        });

        it("IsDropDownCredits initialized as false", function () {
            expect(global.cvm.IsDropDownCredits()).toBeFalsy();
        });

        it("FreeFormCreditsValue initialized", function () {
            expect(global.cvm.FreeFormCreditsValue()).toEqual(undefined);
        });

        it("CreditsLabel initialized", function () {
            expect(global.cvm.CreditsLabel()).toEqual(undefined);
        });

        it("AllowAdding initialized", function () {
            expect(global.cvm.AllowAdding()).toEqual(undefined);
        });

        it("Locations initialized", function () {
            expect(global.cvm.Locations()).toEqual([]);
        });

        it("AddTerms initialized", function () {
            expect(global.cvm.AddTerms()).toEqual([]);
        });

        it("AllowUpdating initialized", function () {
            expect(global.cvm.AllowUpdating()).toEqual(undefined);
        });

        it("Requisites initialized", function () {
            expect(global.cvm.Requisites()).toEqual([]);
        });

        it("LocationCycleRestrictionDescriptions initialized", function () {
            expect(global.cvm.LocationCycleRestrictionDescriptions()).toEqual([]);
        });

        it("SelectedTerm initialized as null", function () {
            expect(global.cvm.SelectedTerm()).toEqual(null);
        });

        it("showLocationCycleRestrictionDescriptions initialized as false", function () {
            expect(global.cvm.showLocationCycleRestrictionDescriptions()).toBeFalsy();
        });

        describe("showTermsOffered: ", function () {

            it("false when showLocationCycleRestrictionDescriptions is false and TermsOffered is null", function () {
                expect(global.cvm.showTermsOffered()).toBeFalsy();
            });

            it("true when showLocationCycleRestrictionDescriptions is false and TermsOffered is not null or empty", function () {
                global.cvm.TermsOffered("2017/SP");
                expect(global.cvm.showTermsOffered()).toBeTruthy();
            });

            it("false when showLocationCycleRestrictionDescriptions is true", function () {
                global.cvm.showLocationCycleRestrictionDescriptions(true);
                expect(global.cvm.showTermsOffered()).toBeFalsy();
            });

        });

        describe("showYearsOffered: ", function () {

            it("false when showLocationCycleRestrictionDescriptions is false and YearsOffered is null", function () {
                global.cvm.showLocationCycleRestrictionDescriptions(false);
                expect(global.cvm.showYearsOffered()).toBeFalsy();
            });

            it("true when showLocationCycleRestrictionDescriptions is false and YearsOffered is not null or empty", function () {
                global.cvm.YearsOffered("2017");
                expect(global.cvm.showYearsOffered()).toBeTruthy();
            });

            it("false when showLocationCycleRestrictionDescriptions is true", function () {
                global.cvm.showLocationCycleRestrictionDescriptions(true);
                expect(global.cvm.showYearsOffered()).toBeFalsy();
            });

        });

        it("isLoading initialized as true", function () {
            expect(global.cvm.isLoading()).toBeTruthy();
        });

        it("courseIsVisible initialized as false", function () {
            expect(global.cvm.courseIsVisible()).toBeFalsy();
        });

        describe("disableCourseAddButton: ", function () {

            it("true when SelectedTerm is undefined", function () {
                expect(global.cvm.disableCourseAddButton()).toBeTruthy();
            });

            it("false when SelectedTerm is not null or empty", function () {
                global.cvm.SelectedTerm("2017/SP");
                expect(global.cvm.disableCourseAddButton()).toBeFalsy();
            });

        });

        it("courseDetailsAddButton initialized", function () {
            expect(global.cvm.courseDetailsAddButton).toEqual({
                id: 'coursedetails-add',
                title: Ellucian.Course.SectionCourseCommonDetails.CourseAddButtonLabel,
                isPrimary: true,
                visible: global.cvm.AllowAdding,
                disabled: global.cvm.disableCourseAddButton,
                callback: addCourseDetails
            });
        });

        it("courseDetailsUpdateButton initialized", function () {
            expect(global.cvm.courseDetailsUpdateButton).toEqual({
                id: 'coursedetails-update',
                title: Ellucian.Course.SectionCourseCommonDetails.CourseUpdateButtonLabel,
                visible: global.cvm.AllowUpdating,
                isPrimary: true,
                callback: updateCourseDetails
            });
        });

        it("closeCourseDetailsButton initialized", function () {
            expect(global.cvm.closeCourseDetailsButton).toEqual({
                id: 'coursedetails-close',
                title: Ellucian.Course.SectionCourseCommonDetails.CloseButton,
                aria: Ellucian.Course.SectionCourseCommonDetails.CourseCloseTitle,
                isPrimary: false,
                callback: closeCourseDetails
            });
        });

    });

    describe("sectionDetailsViewModel: ", function () {

        it("FullTitle initialized", function () {
            expect(global.svm.FullTitle()).toEqual(undefined);
        });

        it("Description initialized", function () {
            expect(global.svm.Description()).toEqual(undefined);
        });

        it("CreditsStatic initialized", function () {
            expect(global.svm.CreditsStatic()).toEqual(undefined);
        });

        it("CreditsVariableIncrements initialized", function () {
            expect(global.svm.CreditsVariableIncrements()).toEqual([]);
        });

        it("IsFreeFormCredits initialized as false", function () {
            expect(global.svm.IsFreeFormCredits()).toBeFalsy();
        });

        it("IsStaticCredits initialized as false", function () {
            expect(global.svm.IsStaticCredits()).toBeFalsy();
        });

        it("IsDropDownCredits initialized as false", function () {
            expect(global.svm.IsDropDownCredits()).toBeFalsy();
        });

        it("FreeFormCreditsValue initialized", function () {
            expect(global.svm.FreeFormCreditsValue()).toEqual(undefined);
        });

        it("GradingOptions initialized", function () {
            expect(global.svm.GradingOptions()).toEqual([]);
        });

        it("IsStaticGrading initialized to false", function () {
            expect(global.svm.IsStaticGrading()).toBeFalsy();
        });

        it("GradingStatic initialized", function () {
            expect(global.svm.GradingStatic()).toEqual(undefined);
        });

        it("GradingOptionsMatch initialized to true", function () {
            expect(global.svm.GradingOptionsMatch()).toBeTruthy();
        });

        it("CreditsLabel initialized", function () {
            expect(global.svm.CreditsLabel()).toEqual(undefined);
        });

        it("Corequisites initialized", function () {
            expect(global.svm.Corequisites()).toEqual([]);
        });

        it("Requisites initialized", function () {
            expect(global.svm.Requisites()).toEqual([]);
        });

        it("TimeLocations initialized", function () {
            expect(global.svm.TimeLocations()).toEqual([]);
        });

        it("Dates initialized", function () {
            expect(global.svm.Dates()).toEqual(undefined);
        });

        it("Instructors initialized", function () {
            expect(global.svm.Instructors()).toEqual([]);
        });

        it("BooksTotal initialized", function () {
            expect(global.svm.BooksTotal()).toEqual(undefined);
        });

        it("BookstoreUrl initialized", function () {
            expect(global.svm.BookstoreUrl()).toEqual(undefined);
        });

        it("AllowAdding initialized", function () {
            expect(global.svm.AllowAdding()).toEqual(undefined);
        });

        it("TermDisplay initialized", function () {
            expect(global.svm.TermDisplay()).toEqual(undefined);
        });

        it("Capacity initialized", function () {
            expect(global.svm.Capacity()).toEqual(undefined);
        });

        it("Available initialized", function () {
            expect(global.svm.Available()).toEqual(undefined);
        });

        it("WaitlistAvailable initialized to false", function () {
            expect(global.svm.WaitlistAvailable()).toBeFalsy();
        });

        it("Waitlisted initialized", function () {
            expect(global.svm.Waitlisted()).toEqual(undefined);
        });

        it("TopicCodeDescription initialized", function () {
            expect(global.svm.TopicCodeDescription()).toEqual(undefined);
        });

        it("SeatlistString returns formatted string", function () {
            var previousAvailable = global.svm.Available();
            var previousCapacity = global.svm.Capacity();

            global.svm.Available(3);
            global.svm.Capacity(5);

            expect(global.svm.SeatlistString()).toEqual(Ellucian.Course.SectionCourseCommonDetails.SeatlistFormatString.format(global.svm.Available(), global.svm.Capacity()));

            global.svm.Available(previousAvailable);
            global.svm.Capacity(previousCapacity);
        });

        describe("IsUnlimitedSeating: ", function () {

            it("true when Available and Capacity are null", function () {
                global.svm.Available(null);
                global.svm.Capacity(null);

                expect(global.svm.IsUnlimitedSeating()).toBeTruthy();
            });

            it("true when Available is null", function () {
                global.svm.Available(null);
                global.svm.Capacity(15);

                expect(global.svm.IsUnlimitedSeating()).toBeTruthy();
            });

            it("true when Capacity is null", function () {
                global.svm.Available(10);
                global.svm.Capacity(null);

                expect(global.svm.IsUnlimitedSeating()).toBeTruthy();
            });

            it("false when Available and Capacity are not null", function () {
                global.svm.Available(10);
                global.svm.Capacity(15);

                expect(global.svm.IsUnlimitedSeating()).toBeFalsy();
            });

        });

        it("Comments initialized", function () {
            expect(global.svm.Comments()).toEqual(undefined);
        });

        it("TransferStatusDescription initialized", function () {
            expect(global.svm.TransferStatusDescription()).toEqual(undefined);
        });

        it("canSkipWaitlist initialized to false", function () {
            expect(global.svm.canSkipWaitlist()).toBeFalsy();
        });

        it("isLoading initialized as false", function () {
            expect(global.svm.isLoading()).toBeFalsy();
        });

        it("sectionIsVisible initialized as false", function () {
            expect(global.svm.sectionIsVisible()).toBeFalsy();
        });

        it("addSectionDetailsButton initialized", function () {
            expect(global.svm.addSectionDetailsButton).toEqual({
                id: 'addSectionDetailsButton',
                title: Ellucian.Course.SectionCourseCommonDetails.SectionAddButtonLabel,
                isPrimary: true,
                visible: global.svm.AllowAdding,
                callback: addSectionDetails
            });
        });

        it("closeSectionDetailsButton initialized", function () {
            expect(global.svm.closeSectionDetailsButton).toEqual({
                id: 'closeSectionDetailsButton',
                title: Ellucian.Course.SectionCourseCommonDetails.CloseButton,
                aria: Ellucian.Course.SectionCourseCommonDetails.SectionCloseTitle,
                isPrimary: false,
                callback: closeSectionDetails
            });
        });

        describe("gradingStaticValue pure computed field", function () {

            it("static grading is true and grading static is undefined", function () {
                global.svm.IsStaticGrading(true);
                expect(global.svm.gradingStaticValue()).toBeUndefined();
            });
            it("static grading is true and grading static has value", function () {
                global.svm.IsStaticGrading(true);
                global.svm.GradingStatic("static value");
                expect(global.svm.gradingStaticValue()).toBe("static value");
            });

            it("static grading is false and grading options is empty", function () {
                global.svm.IsStaticGrading(false);
                global.svm.GradingOptions.removeAll();
                expect(global.svm.gradingStaticValue()).toBe("");
            });

            it("static grading is false and grading options length is 1", function () {
                global.svm.IsStaticGrading(false);
                global.svm.GradingOptions.removeAll();
                global.svm.GradingOptions.push({ Description: "trial", Code: 1 });
                expect(global.svm.gradingStaticValue()).toBe("trial");
            });

            it("static grading is false and grading options is empty", function () {
                global.svm.IsStaticGrading(false);
                global.svm.GradingOptions.removeAll();
                expect(global.svm.gradingStaticValue()).toBe("");
            });

            it("static grading is false and grading options length is 1 but has no description", function () {
                global.svm.IsStaticGrading(false);
                global.svm.GradingOptions.removeAll();
                global.svm.GradingOptions.push({ Code: 1 });
                expect(global.svm.gradingStaticValue()).toBe("");
            });

            it("static grading is false and grading options length is 2", function () {
                global.svm.IsStaticGrading(false);
                global.svm.GradingOptions.removeAll();
                global.svm.GradingOptions.push({ Description: "trial", Code: 1 });
                global.svm.GradingOptions.push({ Description: "trial 2", Code: 2 });
                expect(global.svm.gradingStaticValue()).toBe("");
            });

            

        });

    });

    describe("showCourseOrSectionDetails: ", function () {

        it("calls showSectionDetails with options when options.course.Section is not null", function () {
            var options = {
                course: {
                    Section: {
                        Id: ko.observable("456")
                    },
                    SubjectCode: ko.observable("MATH"),
                    Number: ko.observable("01"),
                    Title: ko.observable("Introduction to Math"),
                    Description: ko.observable("An introduction to mathematics"),
                    TermsOffered: ko.observableArray([ko.observable("2017/SP")]),
                    YearsOffered: ko.observableArray([ko.observable("2017")]),
                    CreditsCeusDisplay: ko.observable("3.00"),
                    Id: ko.observable("123")
                },
                section: {

                }
            };
            spyOn(global, 'showSectionDetails');

            showCourseOrSectionDetails(options);

            expect(global.showSectionDetails).toHaveBeenCalled();
        });


        it("calls showCourseDetails with options when options.course is not null or empty and options.course.section is null", function () {
            var options = {
                course: {
                    Section: null
                },
                section: {

                }
            };
            spyOn(global, 'showCourseDetails');

            showCourseOrSectionDetails(options);

            expect(global.showCourseDetails).toHaveBeenCalled();
        });

    });

    describe("showCourseDetails: ", function () {
        //"{"course":{"Id":"101","SubjectCode":"ACCT","Number":"100","MinimumCredits":3,"MaximumCredits":4,"VariableCreditIncrement":1,"Ceus":2,"Title":"Cost Accounting w/Prog","Description":"Course materials will focus on development and communication of cost information to assist in planning, motivating managers, controlling costs, and evaluating performance.\r\n\r\nAt inani detracto vis, cum eu tota probo vulputate, ea mea diam antiopam tincidunt. Partiendo mnesarchum persequeris ex pro. Ex eos aeque ocurreret. Ea ius prompta efficiendi, inani voluptatum eloquentiam eam eu. Qui eros latine no, qui odio posse malorum no. Eu inani accusamus vel, eam ad veritus facilisi, usu case graece iriure ne.\r\n\r\nTe vim repudiare interpretaris, agam aeque eu eam, omnes legere id vis. Usu veri volumus at, feugiat suscipiantur cu est. Sonet pericula ei eum, autem diceret sententiae his et. Eu his salutatus consulatu, sit ei affert prompta intellegam.\r\n\r\nCu ius amet inimicus salutandi. Usu in choro graeci alterum, at atqui facete senserit eam. Eu eos unum aperiam, mutat ridens vituperata an sit, elitr dolores usu cu. Ignota tibique repudiare mea et, an atqui denique apeirian quo.\r\n\r\nPartem utroque tractatos et sea, assum liber expetenda vel in. Latine nonumes has et, splendide tincidunt cu vis. Ei oblique minimum contentiones vel, elit melius eruditi vim ad. Quot fastidii ponderum ea per, id duo alienum oportere conclusionemque. Usu ne platonem mandamus, labores inimicus definiebas sed ad, ubique primis sed id. Mazim equidem deleniti cu mei, his homero offendit te. His suavitate maluisset in, mei ad congue ridens.\r\n\r\nHis ut lucilius accommodare complectitur, ea sea movet iusto aliquip. His feugait liberavisse et, no has sale illum signiferumque, bonorum iracundia accommodare sed eu. Sea ceteros menandri similique in, vis ne tota ridens appetere. Ei vix alii utinam expetendis. Vix partem admodum principes eu.\r\n\r\nSea veritus nominavi offendit at, an munere laboramus suscipiantur pri. Idque nostro vocibus sea ei, ad nihil alterum sit. Eu cum nonumy malorum, cum an agam case porro. Facilisis accommodare cu sit, nec te option evertitur.\r\n\r\nSea tempor scribentur ei. An pro mundi impedit sapientem, ei eos minim noster suscipiantur. Ut usu laoreet detraxit praesent, verear intellegebat ad nam. Nec ex animal recteque constituam, pri modo putent labore ex. Ius aeque inimicus et, at cum alienum expetenda, quo cu eirmod nostrum luptatum.\r\n\r\nTe quo omnis rationibus. Habeo consetetur neglegentur nam ea, dictas assueverit ad eum. Nihil melius fastidii ut vix, ius inani numquam te, vel eu eros ocurreret. His illum audire sensibus id. Impetus oblique ius ea, mea platonem vulputate te.\r\n\r\nNam te diam prompta mnesarchum. Ea cum numquam scriptorem, est eu tibique urbanitas. Ne has tritani dissentias. Ea amet lorem vim.\r\n\r\nVel ea ubique facilisi, eu mei option theophrastus, ei autem velit cum. Vis paulo nonumy ei, facete nostrum cu ius. Te laudem virtute iuvaret sea. Id sit facer aliquid, ut mei commodo denique.       \r\n\r\nAt inani detracto vis, cum eu tota probo vulputate, ea mea diam antiopam tincidunt. Partiendo mnesarchum persequeris ex pro. Ex eos aeque ocurreret. Ea ius prompta efficiendi, inani voluptatum eloquentiam eam eu. Qui eros latine no, qui odio posse malorum no. Eu inani accusamus vel, eam ad veritus facilisi, usu case graece iriure ne.\r\n\r\nTe vim repudiare interpretaris, agam aeque eu eam, omnes legere id vis. Usu veri volumus at, feugiat suscipiantur cu est. Sonet pericula ei eum, autem diceret sententiae his et. Eu his salutatus consulatu, sit ei affert prompta intellegam.\r\n\r\nCu ius amet inimicus salutandi. Usu in choro graeci alterum, at atqui facete senserit eam. Eu eos unum aperiam, mutat ridens vituperata an sit, elitr dolores usu cu. Ignota tibique repudiare mea et, an atqui denique apeirian quo.\r\n\r\nPartem utroque tractatos et sea, assum liber expetenda vel in. Latine nonumes has et, splendide tincidunt cu vis. Ei oblique minimum contentiones vel, elit melius eruditi vim ad. Quot fastidii ponderum ea per, id duo alienum oportere conclusionemque. Usu ne platonem mandamus, labores inimicus definiebas sed ad, ubique primis sed id. Mazim equidem deleniti cu mei, his homero offendit te. His suavitate maluisset in, mei ad congue ridens.\r\n\r\nHis ut lucilius accommodare complectitur, ea sea movet iusto aliquip. His feugait liberavisse et, no has sale illum signiferumque, bonorum iracundia accommodare sed eu. Sea ceteros menandri similique in, vis ne tota ridens appetere. Ei vix alii utinam expetendis. Vix partem admodum principes eu.\r\n\r\nSea veritus nominavi offendit at, an munere laboramus suscipiantur pri. Idque nostro vocibus sea ei, ad nihil alterum sit. Eu cum nonumy malorum, cum an agam case porro. Facilisis accommodare cu sit, nec te option evertitur.\r\n\r\nSea tempor scribentur ei. An pro mundi impedit sapientem, ei eos minim noster suscipiantur. Ut usu laoreet detraxit praesent, verear intellegebat ad nam. Nec ex animal recteque constituam, pri modo putent labore ex. Ius aeque inimicus et, at cum alienum expetenda, quo cu eirmod nostrum luptatum.\r\n\r\nTe quo omnis rationibus. Habeo consetetur neglegentur nam ea, dictas assueverit ad eum. Nihil melius fastidii ut vix, ius inani numquam te, vel eu eros ocurreret. His illum audire sensibus id. Impetus oblique ius ea, mea platonem vulputate te.\r\n\r\nNam te diam prompta mnesarchum. Ea cum numquam scriptorem, est eu tibique urbanitas. Ne has tritani dissentias. Ea amet lorem vim.\r\n\r\nVel ea ubique facilisi, eu mei option theophrastus, ei autem velit cum. Vis paulo nonumy ei, facete nostrum cu ius. Te laudem virtute iuvaret sea. Id sit facer aliquid, ut mei commodo denique.       \r\n\r\nAt inani detracto vis, cum eu tota probo vulputate, ea mea diam antiopam tincidunt. Partiendo mnesarchum persequeris ex pro. Ex eos aeque ocurreret. Ea ius prompta efficiendi, inani voluptatum eloquentiam eam eu. Qui eros latine no, qui odio posse malorum no. Eu inani accusamus vel, eam ad veritus facilisi, usu case graece iriure ne.\r\n\r\nTe vim repudiare interpretaris, agam aeque eu eam, omnes legere id vis. Usu veri volumus at, feugiat suscipiantur cu est. Sonet pericula ei eum, autem diceret sententiae his et. Eu his salutatus consulatu, sit ei affert prompta intellegam.\r\n\r\nCu ius amet inimicus salutandi. Usu in choro graeci alterum, at atqui facete senserit eam. Eu eos unum aperiam, mutat ridens vituperata an sit, elitr dolores usu cu. Ignota tibique repudiare mea et, an atqui denique apeirian quo.\r\n\r\nPartem utroque tractatos et sea, assum liber expetenda vel in. Latine nonumes has et, splendide tincidunt cu vis. Ei oblique minimum contentiones vel, elit melius eruditi vim ad. Quot fastidii ponderum ea per, id duo alienum oportere conclusionemque. Usu ne platonem mandamus, labores inimicus definiebas sed ad, ubique primis sed id. Mazim equidem deleniti cu mei, his homero offendit te. His suavitate maluisset in, mei ad congue ridens.\r\n\r\nHis ut lucilius accommodare complectitur, ea sea movet iusto aliquip. His feugait liberavisse et, no has sale illum signiferumque, bonorum iracundia accommodare sed eu. Sea ceteros menandri similique in, vis ne tota ridens appetere. Ei vix alii utinam expetendis. Vix partem admodum principes eu.\r\n\r\nSea veritus nominavi offendit at, an munere laboramus suscipiantur pri. Idque nostro vocibus sea ei, ad nihil alterum sit. Eu cum nonumy malorum, cum an agam case porro. Facilisis accommodare cu sit, nec te option evertitur.\r\n\r\nSea tempor scribentur ei. An pro mundi impedit sapientem, ei eos minim noster suscipiantur. Ut usu laoreet detraxit praesent, verear intellegebat ad nam. Nec ex animal recteque constituam, pri modo putent labore ex. Ius aeque inimicus et, at cum alienum expetenda, quo cu eirmod nostrum luptatum.\r\n\r\nTe quo omnis rationibus. Habeo consetetur neglegentur nam ea, dictas assueverit ad eum. Nihil melius fastidii ut vix, ius inani numquam te, vel eu eros ocurreret. His illum audire sensibus id. Impetus oblique ius ea, mea platonem vulputate te.\r\n\r\nNam te diam prompta mnesarchum. Ea cum numquam scriptorem, est eu tibique urbanitas. Ne has tritani dissentias. Ea amet lorem vim.\r\n\r\nVel ea ubique facilisi, eu mei option theophrastus, ei autem velit cum. Vis paulo nonumy ei, facete nostrum cu ius. Te laudem virtute iuvaret sea. Id sit facer aliquid, ut mei commodo denique.       ","TermSessionCycle":"F","TermYearlyCycle":"","YearsOffered":null,"TermsOffered":"Fall Only","LocationCodes":[],"IsPseudoCourse":false,"Requisites":[{"RequirementCode":"8488","IsRequired":true,"CompletionOrder":2,"CorequisiteCourseId":null,"IsProtected":false}],"EquatedCourseIds":["102"],"LocationCycleRestrictions":[],"VerifyGrades":null},"section":{"FormattedMeetingTimes":[],"IsNonTermOffering":false,"IsNonStandardDates":false,"StartDateDisplay":"1/19/2017","EndDateDisplay":"5/31/2017","LocationDisplay":"","HasUnlimitedSeats":false,"AllowAudit":true,"AllowPassNoPass":true,"Available":0,"Books":[],"Capacity":5,"Ceus":2,"CourseId":"101","EndDate":"2017-05-31T04:00:00.000Z","FacultyIds":["0014050"],"Id":"20989","IsActive":true,"Location":"","MaximumCredits":4,"Meetings":[],"MinimumCredits":3,"Number":"011","OnlyPassNoPass":false,"OverridesCourseRequisites":false,"Requisites":[],"StartDate":"2017-01-19T05:00:00.000Z","TermId":"2017/SP","Title":"Cost Accounting w/Prog","VariableCreditIncrement":1,"WaitlistAvailable":true,"Waitlisted":0,"ShowWaitlistInfo":true},"allowAdding":true,"addTerms":null,"creditsOverride":null,"studentId":"0011991","canSkipWaitlist":false}"    });
    });
    describe("showSectionDetails: ", function () {

    });

    describe("addSectionDetails: ", function () {

        beforeEach(function () {
            credits = 3;
            grading = "G";
            currentSection = {
                TermId: "2017/SP"
            };
        });

        it("calls computeChosenSectionGrading, computeChosenSectionCredits, and addSectionCallback when addSectionCallback is a function", function () {
            addSectionCallback = new function () { return; };
            spyOn(global, 'computeChosenSectionGrading').and.callFake(function (e) {
                return grading;
            });
            spyOn(global, 'computeChosenSectionCredits').and.callFake(function (e) {
                return credits;
            });
            spyOn(global, "addSectionCallback").and.callFake(function (e) {
                return;
            });

            addSectionDetails();

            expect(global.computeChosenSectionGrading).toHaveBeenCalled();
            expect(global.computeChosenSectionCredits).toHaveBeenCalled();
            expect(global.addSectionCallback).toHaveBeenCalledWith({ chosenCredits: credits, chosenGradingType: grading, chosenTerm: currentSection.TermId });
        });

        it("does not call computeChosenSectionGrading or computeChosenSectionCredits when addSectionCallback is not a function", function () {
            addSectionCallback = 'not a function';
            spyOn(global, 'computeChosenSectionGrading').and.callFake(function (e) {
                return grading;
            });
            spyOn(global, 'computeChosenSectionCredits').and.callFake(function (e) {
                return credits;
            });

            addSectionDetails();

            expect(global.computeChosenSectionGrading).not.toHaveBeenCalled();
            expect(global.computeChosenSectionCredits).not.toHaveBeenCalled();
        });

    });

    describe("computeChosenSectionCredits: ", function () {
        var variableCredits = 2.5;
        var freeFormCredits = 3;
        var staticCredits = 4;

        it("returns variable credits when variable credits is visible", function () {

            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-selectvarcredit":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "val").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-selectvarcredit":
                        return variableCredits;
                    default:
                        return null;
                }
            });

            expect(computeChosenSectionCredits()).toEqual(variableCredits);
        });

        it("returns free form credits when variable credits is not visible and free form credits is visible", function () {

            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-freeformcredits":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "val").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-freeformcredits":
                        return freeFormCredits;
                    default:
                        return null;
                }
            });

            expect(computeChosenSectionCredits()).toEqual(freeFormCredits);
        });

        it("returns static credits when variable credits and free form credits are not visible", function () {
            spyOn($.fn, "text").and.returnValue(staticCredits);
            expect(computeChosenSectionCredits()).toEqual(staticCredits);
        });

    });

    describe("computeChosenSectionGrading: ", function () {
        
        it("returns gradingSelected as undefined when grading options is empty and grading is not selected", function () {
            global.svm.GradingOptions.removeAll();
            expect(global.svm.GradingOptions().length).toBe(0);
            expect(global.svm.gradingSelected()).toBeUndefined();
            expect(computeChosenSectionGrading()).toBeUndefined();
        });

        it("returns gradingselected when grading options is empty", function () {
            global.svm.gradingSelected(1);
            expect(computeChosenSectionGrading()).toBe(1);
        });

        it("returns first grading options when grading options has length == 1", function () {
            global.svm.GradingOptions.removeAll();
            global.svm.GradingOptions.push({ Description: Ellucian.Course.SectionDetails.stringPassFailGrading, Code: 1 });
            expect(global.svm.GradingOptions().length).toBe(1);
            expect(computeChosenSectionGrading()).toBe(1);
        });

        it("returns grading selected  when grading options has length > 1", function () {
            global.svm.GradingOptions.removeAll();
            global.svm.GradingOptions.push({ Description: Ellucian.Course.SectionDetails.stringPassFailGrading, Code: 1 });
            svm.GradingOptions.push({ Description: Ellucian.Course.SectionDetails.stringAuditGrading, Code: 2});
            global.svm.gradingSelected(2);
            expect(global.svm.GradingOptions().length).toBe(2);
            expect(computeChosenSectionGrading()).toBe(2);
        });

    });

    describe("addCourseDetails: ", function () {

        var variableCredits = 2.5;
        var freeFormCredits = 3;
        var staticCredits = 4;
        var term = "2017/SP";

        it("uses variable credits when variable credits is visible", function () {

            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-selectvarcredit":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "val").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-selectvarcredit":
                        return variableCredits;
                    case "#coursedetails-selectterm":
                        return term;
                    default:
                        return null;
                }
            });
            addCourseCallback = new function () { return; };
            spyOn(global, "addCourseCallback").and.callFake(function () {
                return;
            });

            addCourseDetails();
            
            expect(global.addCourseCallback).toHaveBeenCalledWith({ chosenCredits: variableCredits, chosenTerm: term });
            expect(global.cvm.SelectedTerm()).toEqual(null);
        });

        it("uses free form credits when variable credits is not visible and free form credits is visible", function () {

            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-freeformcredits":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "val").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-freeformcredits":
                        return freeFormCredits;
                    case "#coursedetails-selectterm":
                        return term;
                    default:
                        return null;
                }
            });

            addCourseCallback = new function () { return; };
            spyOn(global, "addCourseCallback").and.callFake(function () {
                return;
            });

            addCourseDetails();

            expect(global.addCourseCallback).toHaveBeenCalledWith({ chosenCredits: freeFormCredits, chosenTerm: term });
            expect(global.cvm.SelectedTerm()).toEqual(null);
        });

        it("uses static credits when variable credits and free form credits are not visible", function () {
            spyOn($.fn, "val").and.returnValue(term);
            spyOn($.fn, "text").and.returnValue(staticCredits);
            addCourseCallback = new function () { return; };
            spyOn(global, "addCourseCallback").and.callFake(function() {
                return;
            });

            addCourseDetails();

            expect(global.addCourseCallback).toHaveBeenCalledWith({ chosenCredits: staticCredits, chosenTerm: term });
            expect(global.cvm.SelectedTerm()).toEqual(null);
        });

        it("does not look for credits when addCourseCallback is not a function", function () {

            addCourseCallback = 'not a function';
            spyOn($.fn, "val").and.callThrough();
            spyOn($.fn, "text").and.callThrough();

            addCourseDetails();

            expect($.fn.val).not.toHaveBeenCalled();
            expect($.fn.text).not.toHaveBeenCalled();
        });

    });

    describe("updateCourseDetails: ", function () {

        var term = "2017/SP";

        beforeEach(function () {
            currentCourse = {
                Id: ko.observable("123")
            };
            currentCourseOriginalTerm = "2016/FA";
        });

        afterEach(function () {
            currentCourse = null;
            currentCourseOriginalTerm = null;
        });

        it("calls updateCourseCallback with correct values when updateCourseCallback is a function", function () {

            spyOn($.fn, "val").and.returnValue(term);
            updateCourseCallback = new function () { return; };
            spyOn(global, "updateCourseCallback").and.callFake(function () {
                return;
            });

            updateCourseDetails();

            expect(global.updateCourseCallback).toHaveBeenCalledWith({ courseId: currentCourse.Id(), oldTerm: currentCourseOriginalTerm, newTerm: term });
        });

        it("does not call updateCourseCallback when updateCourseCallback is not a function", function () {
            updateCourseCallback = 'not a function';
            spyOn($.fn, "val").and.callThrough();

            updateCourseDetails();

            expect($.fn.val).not.toHaveBeenCalled();
        });

    });

    describe("focusAddingCourse: ", function () {

        var hasFocus;

        it("focuses on free form credits when free form credits is visible", function () {
            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-freeformcredits":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "focus").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-freeformcredits":
                        hasFocus = "#coursedetails-freeformcredits";
                        return;
                    default:
                        hasFocus = null;
                        return;
                }
            });

            focusAddingCourse();

            expect(hasFocus).toEqual("#coursedetails-freeformcredits")
        });

        it("focuses on variable credits when free form credits is not visible and variable credits is visible", function () {
            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-selectvarcredit":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "focus").and.callFake(function () {
                switch (this.selector) {
                    case "#coursedetails-selectvarcredit":
                        hasFocus = "#coursedetails-selectvarcredit";
                        return;
                    default:
                        hasFocus = null;
                        return;
                }
            });

            focusAddingCourse();

            expect(hasFocus).toEqual("#coursedetails-selectvarcredit")
        });

    });

    describe("focusAddingSection: ", function () {

        var hasFocus;

        it("focuses on free form credits when free form credits is visible", function () {
            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-freeformcredits":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "focus").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-freeformcredits":
                        hasFocus = "#sectiondetails-freeformcredits";
                        return;
                    default:
                        hasFocus = null;
                        return;
                }
            });

            focusAddingSection();

            expect(hasFocus).toEqual("#sectiondetails-freeformcredits")
        });

        it("focuses on variable credits when free form credits is not visible and variable credits is visible", function () {
            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-selectvarcredit":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "focus").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-selectvarcredit":
                        hasFocus = "#sectiondetails-selectvarcredit";
                        return;
                    default:
                        hasFocus = null;
                        return;
                }
            });

            focusAddingSection();

            expect(hasFocus).toEqual("#sectiondetails-selectvarcredit")
        });

        it("focuses on grading when free form credits and variable credits are not visible", function () {
            spyOn($.fn, "is").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-selectgrading":
                        return true;
                    default:
                        return false;
                }
            });
            spyOn($.fn, "focus").and.callFake(function () {
                switch (this.selector) {
                    case "#sectiondetails-selectgrading":
                        hasFocus = "#sectiondetails-selectgrading";
                        return;
                    default:
                        hasFocus = null;
                        return;
                }
            });

            focusAddingSection();

            expect(hasFocus).toEqual("#sectiondetails-selectgrading")
        });
    });

    describe("closeCourseDetails: ", function () {

        it("sets courseIsVisible and showLocationCycleRestrictionDescriptions to false; sets SelectedTerm to null", function () {
            closeCourseDetails();

            expect(global.cvm.courseIsVisible()).toBeFalsy();
            expect(global.cvm.showLocationCycleRestrictionDescriptions()).toBeFalsy();
            expect(global.cvm.SelectedTerm()).toEqual(null);
        });

    });

    describe("closeSectionDetails: ", function () {

        it("sets sectionIsVisible to false", function () {
            closeSectionDetails();

            expect(global.svm.sectionIsVisible()).toBeFalsy();
        });

    });

    describe("closeCourseOrSectionDetails: ", function () {

        beforeEach(function () {
            xhr = {
                abort: function () { return; }
            };
        });

        it("calls dialogContainer.dialog with 'close' and calls xhr.abort when dialogContainer exists and xhr.readyState is not 4", function () {
            dialogContainer = {
                dialog: function () { return; }
            };

            xhr.readyState = 3;

            spyOn(dialogContainer, 'dialog').and.callThrough();
            spyOn(xhr, 'abort');

            closeCourseOrSectionDetails();

            expect(dialogContainer.dialog).toHaveBeenCalledWith("close");
            expect(xhr.abort).toHaveBeenCalled();
        });

        it("calls dialogContainer.dialog with 'close' and but does not call xhr.abort when dialogContainer exists and xhr.readyState = 4", function () {
            dialogContainer = {
                dialog: function () { return; }
            };

            xhr.readyState = 4;

            spyOn(dialogContainer, 'dialog').and.callThrough();
            spyOn(xhr, 'abort');

            closeCourseOrSectionDetails();

            expect(dialogContainer.dialog).toHaveBeenCalledWith("close");
            expect(xhr.abort).not.toHaveBeenCalled();
        });

        it("does not call dialogContainer.dialog with 'close' or xhr.abort when dialogContainer does not exist and xhr.readyState is not 4", function () {
            dialogContainer = undefined;

            xhr.readyState = 4;

            spyOn(xhr, 'abort');

            closeCourseOrSectionDetails();

            expect(xhr.abort).not.toHaveBeenCalled();
        });

    });

    describe("checkForCourseAddEnter: ", function () {

        it("calls addCourseDetails and returns false when keyCode is 13", function () {

            spyOn(global, 'addCourseDetails');

            expect(checkForCourseAddEnter({ keyCode: 13 })).toBeFalsy();
            expect(global.addCourseDetails).toHaveBeenCalled();
        });

        it("does not call addCourseDetails when keyCode is not 13", function () {

            spyOn(global, 'addCourseDetails');

            checkForCourseAddEnter({ keyCode: 15 });
            expect(global.addCourseDetails).not.toHaveBeenCalled();
        });

    });

    describe("checkForSectionAddEnter: ", function () {

        it("calls addSectionDetails and returns false when keyCode is 13", function () {

            spyOn(global, 'addSectionDetails');

            expect(checkForSectionAddEnter({ keyCode: 13 })).toBeFalsy();
            expect(global.addSectionDetails).toHaveBeenCalled();
        });

        it("does not call addSectionDetails when keyCode is not 13", function () {

            spyOn(global, 'addSectionDetails');

            checkForSectionAddEnter({ keyCode: 15 });
            expect(global.addSectionDetails).not.toHaveBeenCalled();
        });

    });

  

});