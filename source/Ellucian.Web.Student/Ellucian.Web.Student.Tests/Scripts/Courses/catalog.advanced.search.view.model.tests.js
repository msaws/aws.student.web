﻿var Ellucian = Ellucian || {};
Ellucian.SharedComponents = Ellucian.SharedComponents || {};
Ellucian.SharedComponents.ButtonLabels = {
    buttonTextCancel: '@Resources.SiteResources.ButtonTextCancel',
    buttonTextClose: '@Resources.SiteResources.ButtonTextClose',
    buttonTextAccept: '@Resources.SiteResources.ButtonTextAccept',
    buttonTextVerifyPassword: '@Resources.SiteResources.VerifyPasswordButtonLabel'
};
Ellucian.Proxy.ButtonLabels = function () {
    var editProxyAccessDialogSaveButtonLabel = '@Resources.PersonProxyResources.EditProxyAccessDialogSaveButtonLabel';
    var editProxyAccessDialogCancelButtonLabel = '@Resources.PersonProxyResources.EditProxyAccessDialogCancelButtonLabel';
    var reauthorizeConfirmButtonLabel = '@Resources.PersonProxyResources.ReauthorizeConfirmButtonLabel';
    var confirmSelectedPersonDialogButtonLabel = '@Resources.PersonProxyResources.ConfirmSelectedPersonDialogButtonLabel';
};

Ellucian.CatalogAdvancedSearch = Ellucian.CatalogAdvancedSearch || {};
Ellucian.CatalogAdvancedSearch.MarkupLabels = {
    academicLevelLabel: '@Resources.CourseCatalogAdvancedSearch.AcademicLevelLabel',
    addCourseButtonLabel: '@Resources.CourseCatalogAdvancedSearch.AddCourseButton',
    clearButtonLabel: '@Resources.CourseCatalogAdvancedSearch.ClearButton',
    courseLabel: '@Resources.CourseCatalogAdvancedSearch.CourseLabel',
    dayOfWeekLabel: '@Resources.CourseCatalogAdvancedSearch.DayOfWeekLabel',
    endMeetingDateLabel: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateLabel',
    headerText: '@Resources.CourseCatalogAdvancedSearch.HeaderText',
    loadingMessageText: '@Resources.CourseCatalogAdvancedSearch.LoadingMessageText',
    locationLabel: '@Resources.CourseCatalogAdvancedSearch.LocationLabel',
    searchButtonLabel: '@Resources.CourseCatalogAdvancedSearch.SearchButton',
    startMeetingDateLabel: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateLabel',
    subjectSelectCaption: '@Resources.CourseCatalogAdvancedSearch.SubjectSelectCaption',
    termLabel: '@Resources.CourseCatalogAdvancedSearch.TermLabel',
    termSelectCaption: '@Resources.CourseCatalogAdvancedSearch.TermSelectCaption',
    timeOfDayLabel: '@Resources.CourseCatalogAdvancedSearch.TimeOfDayLabel',
    timeOfDayCaption: '@Resources.CourseCatalogAdvancedSearch.TimeOfDayCaption',
    spinnerMessageText: '@Resources.CourseCatalogAdvancedSearch.SpinnerMessageText',
    endMeetingDateImproperMessage: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateImproperMessage',
    endMeetingDateRangeMessage: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateRangeMessage',
    endMeetingDateRequiredMessage: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateRequiredMessage',
    invalidDateFormatErrorMessage: '@Resources.CourseCatalogAdvancedSearch.InvalidDateFormatErrorMessage',
    startMeetingDateImproperMessage: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateImproperMessage',
    startMeetingDateRangeMessage: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateRangeMessage',
    startMeetingDateRequiredMessage: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateRequiredMessage',
    locationSelectCaption: '@Resources.CourseCatalogAdvancedSearch.LocationSelectCaption',
    academicLevelSelectCaption: '@Resources.CourseCatalogAdvancedSearch.AcademicLevelSelectCaption',
    subjectLabel: '@Resources.CourseCatalogAdvancedSearch.SubjectLabel',
    courseNumberLabel: '@Resources.CourseCatalogAdvancedSearch.CourseNumberLabel',
    sectionNumberLabel: '@Resources.CourseCatalogAdvancedSearch.SectionNumberLabel',
    addCourseButtonDescription: '@Resources.CourseCatalogAdvancedSearch.AddCourseButtonDescription',
    loadingSubjectsMessageText: '@Resources.CourseCatalogAdvancedSearch.LoadingSubjectsMessageText'

};

Ellucian.Course = Ellucian.Course || {};
Ellucian.Course.ActionUrls = {
    getSubjectsActionUrl: 'subjects url',
    getAdvancedSearchUrl: 'advanced search url',
    postAdvancedSearchUrl: 'post advanced search',
    searchCourseCatalogActionUrl: 'search course catalog',
    searchCourseCatalogAsyncActionUrl: 'search course catalog async',
    getSectionsActionUrl: 'get sections',
    sectionDetailsActionUrl: 'get section details'
};




describe("catalog.advanced.search.view.model.tests", function () {
    var contentType;
    var dataModelInstance;
    var data;
    beforeAll(function () {
    });

    beforeEach(function () {
        dataModelInstance = new Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchViewModel();
        var data = {
            "DaysOfWeek": [
                     { "Item1": "Sunday", "Item2": "0", "Item3": false },
                     { "Item1": "Monday", "Item2": "1", "Item3": false },
                     { "Item1": "Tuesday", "Item2": "2", "Item3": false },
                     { "Item1": "Wednesday", "Item2": "3", "Item3": false },
                     { "Item1": "Thursday", "Item2": "4", "Item3": false },
                     { "Item1": "Friday", "Item2": "5", "Item3": false },
                     { "Item1": "Saturday", "Item2": "6", "Item3": false }]
        }
        ko.mapping.fromJS(data,{},dataModelInstance);
    });


    it(":advanced search data model when mobile view", function () {
        var win = { innerWidth: dataModelInstance.mobileScreenWidth - 1 }, doc = {};
        dataModelInstance.checkForMobile(win, doc);
        expect(dataModelInstance.isMobile()).toBeTruthy();
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance).toBeDefined();
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance).not.toEqual(null);
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents().length).toEqual(1);

    });
    //are days selected
    it(":none of the days of week selected", function () {

        expect(dataModelInstance.areDaysSelected()).toBeFalsy();
    });

    it(":few of the days of week selected", function () {
        dataModelInstance.DaysOfWeek()[2].Item3(true);
        dataModelInstance.DaysOfWeek()[4].Item3(true);
        expect(dataModelInstance.areDaysSelected()).toBeTruthy();
        expect(dataModelInstance.isSearchEnabled()).toBeTruthy();
    });
    //is search enabled
    it(":search button is not enabled", function () {
        expect(dataModelInstance.isSearchEnabled()).toBeFalsy();
    });
    it(":search button is enabled", function () {
        //modify model to make it dirty
        dataModelInstance.catalogAdvancedSearchDataModelInstance.term("2017/SP");
        expect(dataModelInstance.isSearchEnabled()).toBeTruthy();
    });

    //submit
    //clear
    it(":clear all the values in model", function () {
        //give initial value to data model and keyword componenets
        dataModelInstance.catalogAdvancedSearchDataModelInstance.term("2017/SP");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.location("MC");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.academicLevel ("UG");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.startDate("02/03/2017");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.endDate("03/02/2017");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.days.push("01");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.days.push("02");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.timeRange("morning");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].subject("ENG");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].courseNumber("200");
        dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].section("01");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance).toBeDefined();
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance).not.toEqual(null);
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.term()).toEqual("2017/SP");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.location()).toEqual("MC");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.academicLevel()).toEqual("UG");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.startDate()).toEqual("02/03/2017");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.endDate()).toEqual("03/02/2017");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.days()[0]).toEqual("01");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.days()[1]).toEqual("02");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.timeRange()).toEqual("morning");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents().length).toEqual(1);
        expect( dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].subject()).toEqual("ENG");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].courseNumber()).toEqual("200");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].section()).toEqual("01");
        //call clear function
        dataModelInstance.clear();
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance).toBeDefined();
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance).not.toEqual(null);
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.term()).toEqual("");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.location()).toEqual("");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.academicLevel()).toEqual("");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.startDate()).toEqual("");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.endDate()).toEqual("");
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.days().length).toEqual(0);
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.timeRange()).toEqual(null);
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents().length).toEqual(1);
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].subject()).toEqual(undefined);
        expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].courseNumber()).toEqual("");
       expect(dataModelInstance.catalogAdvancedSearchDataModelInstance.keywordComponents()[0].section()).toEqual("");


    });
   
});



//describe("catalog.advanced.search.view.model.tests", function () {
//    var contentType;
//    var searchInstance;
//    var data;
//    beforeAll(function () {
//        jsonContentType = "json";
//        //create json string for data
//        data = {
//            "Subjects": [{ "Code": "ACCT", "Description": "Accounting", "ShowInCourseSearch": true },
//                { "Code": "AOJU", "Description": "Administration of Justice", "ShowInCourseSearch": true },
//                { "Code": "AGBU", "Description": "Agriculture Business", "ShowInCourseSearch": true },
//                { "Code": "AGME", "Description": "Agriculture Mechanics", "ShowInCourseSearch": true }],
//            "Terms": [{ "Item1": "2017/SP", "Item2": "2017 Spring Term" },
//                { "Item1": "2017/S1", "Item2": "2017 Summer 1 Term" },
//                { "Item1": "2017/S2", "Item2": "2017 Summer 2 Term" },
//                { "Item1": "2017/WI", "Item2": "2017 Winterim Term" }],
//            "Locations": [
//                { "Item1": "\"LG,\"", "Item2": "Test for Ldg W/ , and \"" },
//                { "Item1": "500FT", "Item2": "500 Foot House" },
//                { "Item1": "BMA", "Item2": "Test" },
//                { "Item1": "CD", "Item2": "Central District Office" },
//                { "Item1": "COE", "Item2": "Colonial Ohio-East (coe) Campu" },
//                { "Item1": "COEEA", "Item2": "Coe-east" },
//                { "Item1": "CVIL", "Item2": "Loc Description" },
//                { "Item1": "DT", "Item2": "Dalton-Tierney University" },
//                { "Item1": "EC", "Item2": "Evelyn Chambers School of Art" },
//                { "Item1": "H7", "Item2": "Test" },
//                { "Item1": "H8", "Item2": "Bsf Test" },
//                { "Item1": "HC", "Item2": "Herndon College of Music" },
//                { "Item1": "INT", "Item2": "Internet / Web /Online" },
//                { "Item1": "KH", "Item2": "Ken\u0027s Test Location" },
//                { "Item1": "LWKT", "Item2": "LWK Test Location" },
//                { "Item1": "MC", "Item2": "Miller Chester University" },
//                { "Item1": "MRW", "Item2": "Description" },
//                { "Item1": "SA", "Item2": "SC Institute - Aiken Campus" },
//                { "Item1": "SBBHS", "Item2": "Big Bear High School (sbvc)" },
//                { "Item1": "SBBMS", "Item2": "Big Bear Middle School (sbvc)" },
//                { "Item1": "SBSD", "Item2": "Sb Sheriff Dept Train Center" },
//                { "Item1": "SBVC", "Item2": "San Bernardino Valley College" },
//                { "Item1": "SC", "Item2": "SC Institute - Main Campus" },
//                { "Item1": "SW", "Item2": "SC Institute - Western Campus" },
//                { "Item1": "TC", "Item2": "Toronto College of Botany" }],
//            "AcademicLevels": [
//                { "Item1": "AKW", "Item2": "Akw Acad Level Test" },
//                { "Item1": "BMA", "Item2": "Bma Test" },
//                { "Item1": "CE", "Item2": "Continuing Education Level" },
//                { "Item1": "GR", "Item2": "Graduate" },
//                { "Item1": "JD", "Item2": "Law" },
//                { "Item1": "MIS", "Item2": "Marissa\u0027s Academic Level" },
//                { "Item1": "SALAL", "Item2": "Salil Descriptions" },
//                { "Item1": "UG", "Item2": "Undergraduate" }],
//            "TimeRanges": [
//                { "Item1": "Early Morning (Midnight - 8am)", "Item2": 0, "Item3": 480 },
//                { "Item1": "Morning (8am - Midday)", "Item2": 480, "Item3": 720 },
//                { "Item1": "Afternoon (Midday - 4pm)", "Item2": 720, "Item3": 960 },
//                { "Item1": "Evening (4pm - 8pm)", "Item2": 960, "Item3": 1200 },
//                { "Item1": "Night (8pm - Midnight)", "Item2": 1200, "Item3": 1440 }],
//            "DaysOfWeek": [
//                { "Item1": "Sunday", "Item2": "0", "Item3": false },
//                { "Item1": "Monday", "Item2": "1", "Item3": false },
//                { "Item1": "Tuesday", "Item2": "2", "Item3": false },
//                { "Item1": "Wednesday", "Item2": "3", "Item3": false },
//                { "Item1": "Thursday", "Item2": "4", "Item3": false },
//                { "Item1": "Friday", "Item2": "5", "Item3": false },
//                { "Item1": "Saturday", "Item2": "6", "Item3": false }],
//            "EarliestSearchDate": "\/Date(1325394000000)\/",
//            "LatestSearchDate": "\/Date(1577768400000)\/"
//        }

//        //convert json to javascript object
//    });

//    beforeEach(function () {
//        contentType = "json";
//        searchInstance = new catalogSearchViewModel();

//    });

//    //check advanced model before mapping happens
//    //check advanced model after mapping happens
//    //check count of KW componenets when mobile and desktop





//});
