﻿

describe("catalog.advanced.keyword.components.tests", function () {
    var keywordModelInstance;
    beforeAll(function () {
        keywordModelInstance = new Ellucian.CatalogAdvancedSearch.catalogAdvancedSearchKeywordComponentsModel();

    });

    beforeEach(function () {
    });

    //change the values of observable to see values of computed fields

    it(":initial object values", function () {
        expect(keywordModelInstance.subject()).toBe(undefined);
        expect(keywordModelInstance.courseNumber()).toBe("");
        expect(keywordModelInstance.section()).toBe("");
        expect(keywordModelInstance.section.isEnabled()).toBeFalsy();
        expect(keywordModelInstance.courseNumber.isEnabled()).toBeFalsy();
        expect(keywordModelInstance.isNullOrEmpty()).toBeTruthy();
    });

    it(":provide subject a value", function () {
        keywordModelInstance.subject("ACCT");
        expect(keywordModelInstance.subject()).toBe("ACCT");
        expect(keywordModelInstance.courseNumber()).toBe("");
        expect(keywordModelInstance.section()).toBe("");
        expect(keywordModelInstance.section.isEnabled()).toBeFalsy();
        expect(keywordModelInstance.courseNumber.isEnabled()).toBeTruthy();
        expect(keywordModelInstance.isNullOrEmpty()).toBeFalsy();
    });

    it(":provide subject and courseNumber values", function () {
        keywordModelInstance.subject("ACCT");
        keywordModelInstance.courseNumber("200");
        expect(keywordModelInstance.subject()).toBe("ACCT");
        expect(keywordModelInstance.courseNumber()).toBe("200");
        expect(keywordModelInstance.section()).toBe("");
        expect(keywordModelInstance.section.isEnabled()).toBeTruthy();
        expect(keywordModelInstance.courseNumber.isEnabled()).toBeTruthy();
        expect(keywordModelInstance.isNullOrEmpty()).toBeFalsy();
    });

    it(":provide subject, courseNumber and section values", function () {
        keywordModelInstance.subject("ACCT");
        keywordModelInstance.courseNumber("200");
        keywordModelInstance.section("01");
        expect(keywordModelInstance.subject()).toBe("ACCT");
        expect(keywordModelInstance.courseNumber()).toBe("200");
        expect(keywordModelInstance.section()).toBe("01");
        expect(keywordModelInstance.section.isEnabled()).toBeTruthy();
        expect(keywordModelInstance.courseNumber.isEnabled()).toBeTruthy();
        expect(keywordModelInstance.isNullOrEmpty()).toBeFalsy();
    });

    it(":provide subject, courseNumber and section values and delete courseNumber", function () {
        keywordModelInstance.subject("ACCT");
        keywordModelInstance.courseNumber("200");
        keywordModelInstance.section("01");
        expect(keywordModelInstance.subject()).toBe("ACCT");
        expect(keywordModelInstance.courseNumber()).toBe("200");
        expect(keywordModelInstance.section()).toBe("01");
        keywordModelInstance.courseNumber(null);
        expect(keywordModelInstance.subject()).toBe("ACCT");
        expect(keywordModelInstance.courseNumber()).toBe(null);
        expect(keywordModelInstance.section()).toBe("");
        expect(keywordModelInstance.section.isEnabled()).toBeFalsy();
        expect(keywordModelInstance.courseNumber.isEnabled()).toBeTruthy();
        expect(keywordModelInstance.isNullOrEmpty()).toBeFalsy();
    });

    it(":provide subject, courseNumber and section values and delete subject", function () {
        keywordModelInstance.subject("ACCT");
        keywordModelInstance.courseNumber("200");
        keywordModelInstance.section("01");
        expect(keywordModelInstance.subject()).toBe("ACCT");
        expect(keywordModelInstance.courseNumber()).toBe("200");
        expect(keywordModelInstance.section()).toBe("01");
        keywordModelInstance.subject(null);
        expect(keywordModelInstance.subject()).toBe(null);
        expect(keywordModelInstance.courseNumber()).toBe("");
        expect(keywordModelInstance.section()).toBe("");
        expect(keywordModelInstance.section.isEnabled()).toBeFalsy();
        expect(keywordModelInstance.courseNumber.isEnabled()).toBeFalsy();
        expect(keywordModelInstance.isNullOrEmpty()).toBeTruthy();
    });
});

