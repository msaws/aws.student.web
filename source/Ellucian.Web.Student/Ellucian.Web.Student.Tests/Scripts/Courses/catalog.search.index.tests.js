﻿var Ellucian = Ellucian || {};
Ellucian.SharedComponents = Ellucian.SharedComponents || {};
Ellucian.SharedComponents.ButtonLabels = {
    buttonTextCancel: '@Resources.SiteResources.ButtonTextCancel',
    buttonTextClose: '@Resources.SiteResources.ButtonTextClose',
    buttonTextAccept: '@Resources.SiteResources.ButtonTextAccept',
    buttonTextVerifyPassword: '@Resources.SiteResources.VerifyPasswordButtonLabel'
};
Ellucian.Proxy.ButtonLabels = function () {
    var editProxyAccessDialogSaveButtonLabel = '@Resources.PersonProxyResources.EditProxyAccessDialogSaveButtonLabel';
    var editProxyAccessDialogCancelButtonLabel = '@Resources.PersonProxyResources.EditProxyAccessDialogCancelButtonLabel';
    var reauthorizeConfirmButtonLabel = '@Resources.PersonProxyResources.ReauthorizeConfirmButtonLabel';
    var confirmSelectedPersonDialogButtonLabel = '@Resources.PersonProxyResources.ConfirmSelectedPersonDialogButtonLabel';
};

Ellucian.CatalogAdvancedSearch = Ellucian.CatalogAdvancedSearch || {};
Ellucian.CatalogAdvancedSearch.MarkupLabels = {
    academicLevelLabel: '@Resources.CourseCatalogAdvancedSearch.AcademicLevelLabel',
    addCourseButtonLabel: '@Resources.CourseCatalogAdvancedSearch.AddCourseButton',
    clearButtonLabel: '@Resources.CourseCatalogAdvancedSearch.ClearButton',
    courseLabel: '@Resources.CourseCatalogAdvancedSearch.CourseLabel',
    dayOfWeekLabel: '@Resources.CourseCatalogAdvancedSearch.DayOfWeekLabel',
    endMeetingDateLabel: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateLabel',
    headerText: '@Resources.CourseCatalogAdvancedSearch.HeaderText',
    loadingMessageText: '@Resources.CourseCatalogAdvancedSearch.LoadingMessageText',
    locationLabel: '@Resources.CourseCatalogAdvancedSearch.LocationLabel',
    searchButtonLabel: '@Resources.CourseCatalogAdvancedSearch.SearchButton',
    startMeetingDateLabel: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateLabel',
    subjectSelectCaption: '@Resources.CourseCatalogAdvancedSearch.SubjectSelectCaption',
    termLabel: '@Resources.CourseCatalogAdvancedSearch.TermLabel',
    termSelectCaption: '@Resources.CourseCatalogAdvancedSearch.TermSelectCaption',
    timeOfDayLabel: '@Resources.CourseCatalogAdvancedSearch.TimeOfDayLabel',
    timeOfDayCaption: '@Resources.CourseCatalogAdvancedSearch.TimeOfDayCaption',
    spinnerMessageText: '@Resources.CourseCatalogAdvancedSearch.SpinnerMessageText',
    endMeetingDateImproperMessage: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateImproperMessage',
    endMeetingDateRangeMessage: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateRangeMessage',
    endMeetingDateRequiredMessage: '@Resources.CourseCatalogAdvancedSearch.EndMeetingDateRequiredMessage',
    invalidDateFormatErrorMessage: '@Resources.CourseCatalogAdvancedSearch.InvalidDateFormatErrorMessage',
    startMeetingDateImproperMessage: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateImproperMessage',
    startMeetingDateRangeMessage: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateRangeMessage',
    startMeetingDateRequiredMessage: '@Resources.CourseCatalogAdvancedSearch.StartMeetingDateRequiredMessage',
    locationSelectCaption: '@Resources.CourseCatalogAdvancedSearch.LocationSelectCaption',
    academicLevelSelectCaption: '@Resources.CourseCatalogAdvancedSearch.AcademicLevelSelectCaption',
    subjectLabel: '@Resources.CourseCatalogAdvancedSearch.SubjectLabel',
    courseNumberLabel: '@Resources.CourseCatalogAdvancedSearch.CourseNumberLabel',
    sectionNumberLabel: '@Resources.CourseCatalogAdvancedSearch.SectionNumberLabel',
    addCourseButtonDescription: '@Resources.CourseCatalogAdvancedSearch.AddCourseButtonDescription',
    loadingSubjectsMessageText: '@Resources.CourseCatalogAdvancedSearch.LoadingSubjectsMessageText'

};

Ellucian.Course = Ellucian.Course || {};
Ellucian.Course.ActionUrls = {
    getSubjectsActionUrl: 'subjects url',
    getAdvancedSearchUrl: 'advanced search url',
    postAdvancedSearchUrl: 'post advanced search',
    searchCourseCatalogActionUrl: 'search course catalog',
    searchCourseCatalogAsyncActionUrl: 'search course catalog async',
    getSectionsActionUrl: 'get sections',
    sectionDetailsActionUrl: 'get section details'
};


describe("catalog.search.index.tests", function () {
    var contentType;
    var searchInstance;
    var data;
    beforeAll(function () {
        jsonContentType = "json";
       //create json string for data
        data = {
            "Subjects": [{ "Code": "ACCT", "Description": "Accounting", "ShowInCourseSearch": true },
                { "Code": "AOJU", "Description": "Administration of Justice", "ShowInCourseSearch": true },
                { "Code": "AGBU", "Description": "Agriculture Business", "ShowInCourseSearch": true },
                { "Code": "AGME", "Description": "Agriculture Mechanics", "ShowInCourseSearch": true }],
            "Terms": [{ "Item1": "2017/SP", "Item2": "2017 Spring Term" },
                { "Item1": "2017/S1", "Item2": "2017 Summer 1 Term" },
                { "Item1": "2017/S2", "Item2": "2017 Summer 2 Term" },
                { "Item1": "2017/WI", "Item2": "2017 Winterim Term" }],
            "Locations": [
                { "Item1": "\"LG,\"", "Item2": "Test for Ldg W/ , and \"" },
                { "Item1": "500FT", "Item2": "500 Foot House" },
                { "Item1": "BMA", "Item2": "Test" },
                { "Item1": "CD", "Item2": "Central District Office" },
                { "Item1": "COE", "Item2": "Colonial Ohio-East (coe) Campu" },
                { "Item1": "COEEA", "Item2": "Coe-east" },
                { "Item1": "CVIL", "Item2": "Loc Description" },
                { "Item1": "DT", "Item2": "Dalton-Tierney University" },
                { "Item1": "EC", "Item2": "Evelyn Chambers School of Art" },
                { "Item1": "H7", "Item2": "Test" },
                { "Item1": "H8", "Item2": "Bsf Test" },
                { "Item1": "HC", "Item2": "Herndon College of Music" },
                { "Item1": "INT", "Item2": "Internet / Web /Online" },
                { "Item1": "KH", "Item2": "Ken\u0027s Test Location" },
                { "Item1": "LWKT", "Item2": "LWK Test Location" },
                { "Item1": "MC", "Item2": "Miller Chester University" },
                { "Item1": "MRW", "Item2": "Description" },
                { "Item1": "SA", "Item2": "SC Institute - Aiken Campus" },
                { "Item1": "SBBHS", "Item2": "Big Bear High School (sbvc)" },
                { "Item1": "SBBMS", "Item2": "Big Bear Middle School (sbvc)" },
                { "Item1": "SBSD", "Item2": "Sb Sheriff Dept Train Center" },
                { "Item1": "SBVC", "Item2": "San Bernardino Valley College" },
                { "Item1": "SC", "Item2": "SC Institute - Main Campus" },
                { "Item1": "SW", "Item2": "SC Institute - Western Campus" },
                { "Item1": "TC", "Item2": "Toronto College of Botany" }],
            "AcademicLevels": [
                { "Item1": "AKW", "Item2": "Akw Acad Level Test" },
                { "Item1": "BMA", "Item2": "Bma Test" },
                { "Item1": "CE", "Item2": "Continuing Education Level" },
                { "Item1": "GR", "Item2": "Graduate" },
                { "Item1": "JD", "Item2": "Law" },
                { "Item1": "MIS", "Item2": "Marissa\u0027s Academic Level" },
                { "Item1": "SALAL", "Item2": "Salil Descriptions" },
                { "Item1": "UG", "Item2": "Undergraduate" }],
            "TimeRanges": [
                { "Item1": "Early Morning (Midnight - 8am)", "Item2": 0, "Item3": 480 },
                { "Item1": "Morning (8am - Midday)", "Item2": 480, "Item3": 720 },
                { "Item1": "Afternoon (Midday - 4pm)", "Item2": 720, "Item3": 960 },
                { "Item1": "Evening (4pm - 8pm)", "Item2": 960, "Item3": 1200 },
                { "Item1": "Night (8pm - Midnight)", "Item2": 1200, "Item3": 1440 }],
            "DaysOfWeek": [
                { "Item1": "Sunday", "Item2": "0", "Item3": false },
                { "Item1": "Monday", "Item2": "1", "Item3": false },
                { "Item1": "Tuesday", "Item2": "2", "Item3": false },
                { "Item1": "Wednesday", "Item2": "3", "Item3": false },
                { "Item1": "Thursday", "Item2": "4", "Item3": false },
                { "Item1": "Friday", "Item2": "5", "Item3": false },
                { "Item1": "Saturday", "Item2": "6", "Item3": false }],
            "EarliestSearchDate": "\/Date(1325394000000)\/",
            "LatestSearchDate": "\/Date(1577768400000)\/"
        }

        //convert json to javascript object
    });

    beforeEach(function () {
        contentType = "json";
        searchInstance = new catalogSearchViewModel();
    });

    //related to document ready ajax calls
    //related to objects created

    it(":tab selected is first one  after initialization", function () {
        expect(searchInstance.TabSelected()).toBe(1);
    });

    it(":catalog advanced search model is not null or undefined after initialization", function () {
        expect(searchInstance.catalogAdvancedSearchViewModelInstance).toBeDefined();
        expect(searchInstance.catalogAdvancedSearchViewModelInstance).not.toEqual(null);

    });

    it(":catalog subject search model is not null or undefined after initialization", function () {
        expect(searchInstance.catalogSearchSubjectViewModelInstance).toBeDefined();
        expect(searchInstance.catalogSearchSubjectViewModelInstance).not.toEqual(null);
    });

    it(":on ajax success ko.mapping is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(data);
        });

        spyOn(ko.mapping, "fromJS").and.callThrough();
        searchInstance.initialization();
        expect(ko.mapping.fromJS).toHaveBeenCalled();

    });

    it(":on ajax success proper mapping happens for advanced search model", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(data);
        });
        searchInstance.initialization();
        var model = searchInstance.catalogAdvancedSearchViewModelInstance;
        expect(model).toBeDefined();
        expect(model).not.toEqual(null);
        expect(model.Subjects().length).toEqual(4);
        expect(ko.mapping.toJS(model.Subjects())).toEqual(data.Subjects);
        expect(ko.mapping.toJS(model.Subjects())).toContain(data.Subjects[2]);
        expect(model.Terms().length).toEqual(4);
        expect(ko.mapping.toJS(model.Terms())).toEqual(data.Terms);
        expect(ko.mapping.toJS(model.Terms())).toContain(data.Terms[2]);
        expect(model.Locations().length).toEqual(25);
        expect(ko.mapping.toJS(model.Locations())).toEqual(data.Locations);
        expect(ko.mapping.toJS(model.Locations())).toContain(data.Locations[2]);
        expect(model.AcademicLevels().length).toEqual(8);
        expect(ko.mapping.toJS(model.AcademicLevels())).toEqual(data.AcademicLevels);
        expect(ko.mapping.toJS(model.AcademicLevels())).toContain(data.AcademicLevels[2]);
        expect(model.TimeRanges().length).toEqual(5);
        expect(ko.mapping.toJS(model.TimeRanges())).toEqual(data.TimeRanges);
        expect(ko.mapping.toJS(model.TimeRanges())).toContain(data.TimeRanges[2]);
        expect(model.DaysOfWeek().length).toEqual(7);
        expect(ko.mapping.toJS(model.DaysOfWeek())).toEqual(data.DaysOfWeek);
        expect(ko.mapping.toJS(model.DaysOfWeek())).toContain(data.DaysOfWeek[2]);
        expect(model.EarliestSearchDate()).toEqual(data.EarliestSearchDate);
        expect(model.LatestSearchDate()).toEqual(data.LatestSearchDate);
    });
      

    it(":on ajax success proper mapping happens for subject search model", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(data);
        });
        searchInstance.initialization();
        var model = searchInstance.catalogSearchSubjectViewModelInstance;
        expect(model).toBeDefined();
        expect(model).not.toEqual(null);
        expect(model.subjects().length).toEqual(4);
        expect(model.subjects()[0].Code()).toEqual("ACCT");
        expect(model.subjects()[0].Description()).toEqual("Accounting");
        expect(model.subjects()[0].ShowInCourseSearch()).toEqual(true);
        var searchUrl=Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl + "?subjects=" + encodeURIComponent((model.subjects()[0].Code()));
        expect(model.subjects()[0].searchUrl()).toEqual(searchUrl);

        expect(model.subjects()[1].Code()).toEqual("AOJU");
        expect(model.subjects()[1].Description()).toEqual("Administration of Justice");
        expect(model.subjects()[1].ShowInCourseSearch()).toEqual(true);
        var searchUrl = Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl + "?subjects=" + encodeURIComponent((model.subjects()[1].Code()));
        expect(model.subjects()[1].searchUrl()).toEqual(searchUrl);

        expect(model.subjects()[2].Code()).toEqual("AGBU");
        expect(model.subjects()[2].Description()).toEqual("Agriculture Business");
        expect(model.subjects()[2].ShowInCourseSearch()).toEqual(true);
        var searchUrl = Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl + "?subjects=" + encodeURIComponent((model.subjects()[2].Code()));
        expect(model.subjects()[2].searchUrl()).toEqual(searchUrl);

        expect(model.subjects()[3].Code()).toEqual("AGME");
        expect(model.subjects()[3].Description()).toEqual("Agriculture Mechanics");
        expect(model.subjects()[3].ShowInCourseSearch()).toEqual(true);
        var searchUrl = Ellucian.Course.ActionUrls.searchCourseCatalogActionUrl + "?subjects=" + encodeURIComponent((model.subjects()[3].Code()));
        expect(model.subjects()[3].searchUrl()).toEqual(searchUrl);
       
    });

    it(":subject model has proper filtered subjects", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(data);
        });
        searchInstance.initialization();
        var model = searchInstance.catalogSearchSubjectViewModelInstance;
        model.filter("Agriculture");
        expect(model).toBeDefined();
        expect(model).not.toEqual(null);
        expect(model.filteredSubjects().length).toEqual(2);
        expect(model.filteredSubjects()[0].Code()).toEqual("AGBU");
        expect(model.filteredSubjects()[1].Code()).toEqual("AGME");
    });

});

