﻿// Copyright 2016 Ellucian Company L.P. and its affiliates

/*
 * Initialize a KO custom binding to be executed as an automated unit test.
 * 
 * This function takes in four arguments:
 *   1 - tag.........: String representing a DOM element to create. For example, "div" results in
 *                     the Jasmine framework adding a "<div />" element to the DOM of the "virtual" page.
 *   2 - dataBinding.: Custom data binding to be tested. Specify the argument as an object that contains
 *                     the binding and use syntax as if you were using the binding in HTML, for example
 *                     if you wanted to test a custom binding named 'toggleElement' that takes in no arguments
 *                     then use the following syntax: { toggleElement: {} }
 *   3 - viewModel...: A view model to include as the context of the binding. Include a view model if the custom
 *                     binding calls a function within the view model.
 *   4 - testCallback: The test code you want executed after the custom binding is initiazed. This inludes
 *                     data setup, initialization of jasmin spies, and expect statements.
 * 
 * Here's a sample call to this function:
 * 
 *     var viewModel = new projectDetailViewModel();
 *     ko.initializeCustomBinding("div", { toggleGlDetailView: {} }, viewModel, function (bindingElement) {
 *         bindingElement.click();
 *         expect(viewModel.toggleView).toHaveBeenCalled();
 *     });
 */
ko.initializeCustomBinding = function (tag, dataBinding, viewModel, testCallback) {
    // Create an element to which to bind the binding being tested and add it to the document body.
    var bindingElement = $('<' + tag + ' />');
    bindingElement.appendTo("body");

    // Bind the view model and test binding to the DOM element.
    ko.applyBindingsToNode(bindingElement[0], dataBinding, viewModel);

    // Define a clean function to remove the element from the DOM after the test is finished.
    var args = {
        clean: function () {
            bindingElement.remove();
        }
    };

    // Execute the test callback function...basically, execute the test.
    testCallback(bindingElement, args);

    // Clean up the document now that the test is complete.
    if (!args.async) {
        args.clean();
    }
};