﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/Badge/_Badge'],

function (component, events) {

    describe("Badge Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var BadgeViewModel = component.viewModel;
        var params = {};
        var viewModel;
        beforeEach(function () {
            params = { number: ko.observable(123), type: ko.observable('draft') };
            viewModel = new BadgeViewModel(params);
        });

        it("error thrown if params.number is undefined", function () {
            params = { type: ko.observable('draft') };
            expect(function () { BadgeViewModel(params); }).toThrow();
        });

        it("number set to params.number value when params.number is observable", function () {
            expect(viewModel.number()).toEqual(params.number());
        });

        it("number set to params.number value when params.number is not observable", function () {
            params = { number: 123, type: ko.observable('draft') };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.number()).toEqual(params.number);
        });

        it("type set to null when params.type is undefined", function () {
            params = { number: 123 };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.type()).toEqual(null);
        });

        it("type set to params.type value when params.type is observable", function () {
            expect(viewModel.type()).toEqual(params.type());
        });

        it("type set to params.type value when params.type is not observable", function () {
            params = { number: 123, type: 'draft' };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.type()).toEqual(params.type);
        });

        it("badgeClasses set to 'esg-badge' when params.type is not a valid type", function () {
            params = { number: 123 };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.badgeClasses()).toEqual('esg-badge');
        });

        it("badgeClasses set to 'esg-badge esg-badge--error' when params.type is 'error'", function () {
            params = { number: 123, type: 'error' };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.badgeClasses()).toEqual('esg-badge esg-badge--error');
        });

        it("badgeClasses set to 'esg-badge esg-badge--success' when params.type is 'success'", function () {
            params = { number: 123, type: 'success' };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.badgeClasses()).toEqual('esg-badge esg-badge--success');
        });

        it("badgeClasses set to 'esg-badge esg-badge--pending' when params.type is 'pending'", function () {
            params = { number: 123, type: 'pending' };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.badgeClasses()).toEqual('esg-badge esg-badge--pending');
        });

        it("badgeClasses set to 'esg-badge esg-badge--draft' when params.type is 'draft'", function () {
            params = { number: 123, type: 'draft' };
            viewModel = new BadgeViewModel(params);
            expect(viewModel.badgeClasses()).toEqual('esg-badge esg-badge--draft');
        });

        it("dispose calls badgeClasses.dispose when called", function () {
            spyOn(viewModel.badgeClasses, "dispose");
            viewModel.dispose();
            expect(viewModel.badgeClasses.dispose).toHaveBeenCalled();
        });
    });
});