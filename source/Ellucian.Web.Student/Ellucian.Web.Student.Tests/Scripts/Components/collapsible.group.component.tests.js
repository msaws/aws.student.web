﻿const component = require("Site/Components/CollapsibleGroup/_CollapsibleGroup");

describe("Test Suite for CollapsibleGroup Component", () => {
    const viewModel = component.viewModel;
    var params = {};

    const innerHtml = document.body.innerHTML;    

    beforeEach(() => {
        params = {
            titleText: 'los huevos nuevos',
            headerId: "foo",
            bodyId: "bar",
            isOpen: ko.observable(false)
        };        

        //document.body.innerHTML = innerHtml.concat("<collapsible-group params=\"headerId: 'foo', bodyId: 'bar' \"><div id='foo'>This is the header</div><div id='bar'>This is the body</div></collapsible-group>");
        //ko.components.register('collapsible-group', component);
        document.body.innerHTML = innerHtml.concat(component.template);
        

    });

    afterEach(() => {
        //ko.components.unregister("collapsible-group");
    })
/*
    describe("Body and Header Ids", () => {
     
        it("should be applied to group ids", () => {
            let model = new viewModel(params);
            expect(model.groupBodyId).toEqual(params.bodyId + "-collapseBody");
            expect(model.groupHeaderId).toEqual(params.headerId + "-groupHeading");
        });
        it("should be observable too", () => {
            params.headerId = ko.observable("foo");
            params.bodyId = ko.observable("bar");
            let model = new viewModel(params);
            expect(model.groupBodyId).toEqual(params.bodyId() + "-collapseBody");
            expect(model.groupHeaderId).toEqual(params.headerId() + "-groupHeading");
        });
        it("should be required", () => {
            params.headerId = null;
            let errorNum = 0;
            try {                
                let model = new viewModel(params);
            }
            catch(error) {
                expect(error).toBeDefined();
                errorNum++;
            }
            params.headerId = "foo";
            params.bodyId = null;
            try {
                let model = new viewModel(params);
            }
            catch(error) {
                expect(error).toBeDefined();
                errorNum++;
            }
            expect(errorNum).toEqual(2);
            
        });
    });
    */
    describe("After component template nodes are rendered", () => {
        afterEach(() => {
            //ko.cleanNode(document);
        })
        xit("should move header nodes to appropriate location", () => {             
            //not sure how to write a good test for this.
            //i want to get the markup into the document and applyBindings, and then verify that the afterRender function was invoked
            let model = new viewModel(params);
            
        });
    });

    describe("When Collapsible Group is toggled", () => {
        let model;
        beforeEach(() => {
            params.isOpen = ko.observable(false);
            model = new viewModel(params);
            
        })
        it("should update isOpen parameter" ,() => {
            model.toggleCollapsible();
            expect(params.isOpen()).toBeTruthy();
            model.toggleCollapsible();
            expect(params.isOpen()).toBeFalsy();
        });

        it("should not update isOpen parameter if not observable", () => {
            params.isOpen = false;
            model = new viewModel(params);
            model.toggleCollapsible();
            expect(params.isOpen).toBeFalsy();
        });

        it("should not update isOpen parameter if computed observable", () => {
            params.isOpen = ko.computed(() => true);
            model = new viewModel(params);
            model.toggleCollapsible();
            expect(params.isOpen()).toBeTruthy();
        });
        
        it("should update collapsibleClass computed", () => {
            expect(model.collapsibleClass()).toEqual("esg-is-collapsed");
            model.toggleCollapsible();
            expect(model.collapsibleClass()).toEqual("esg-is-open");
        });

        it("should update iconClass computed", () => {
            expect(model.arrowIconClass()).toEqual('esg-icon--down');
            model.toggleCollapsible();
            expect(model.arrowIconClass()).toEqual('esg-icon--up');
        })
    })

});