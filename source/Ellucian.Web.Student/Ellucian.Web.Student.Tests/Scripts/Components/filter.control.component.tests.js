﻿define(['Ellucian.Web.Student/Scripts/Components/FilterControl/_FilterControl'],

        function (component, events) {

            describe("FilterControl Component :", function () {

                jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

                var FilterControlViewModel = component.viewModel;
                var params = {};
                var viewModel;
                beforeEach(function () {
                    params = { name: "Fund", criteriaArray: ko.observableArray([]) };
                    viewModel = new FilterControlViewModel(params);
                });

                describe("initialization", function () {
                    it("should set default values", function () {
                        expect(viewModel.ComponentName()).toEqual(params.name);
                    });
                });

                describe("functions: ", function () {
                    it("toggleCriteria changes the state of the expanded observable", function () {
                        viewModel.toggleCriteria();
                        expect(viewModel.expanded()).toBeTruthy();
                    });

                    it("addFilterCriteria doesn't add blank workingCriteria values", function () {
                        viewModel.addFilterCriteria();
                        expect(viewModel.tokenizedFilterCriteria().length).toEqual(0);
                    });

                    it("addFilterCriteria calls validateFilterCriteria when workingCriteria is not blank", function () {
                        spyOn(viewModel, "validateFilterCriteria").and.callFake(function () {
                            return true;
                        });

                        viewModel.workingCriteria = ko.observable("10");
                        viewModel.addFilterCriteria();
                        expect(viewModel.validateFilterCriteria).toHaveBeenCalled();
                        expect(viewModel.tokenizedFilterCriteria().length).toEqual(1);
                    });

                    it("removeFilterCriteria clears the argument from tokenizedFilterCriteria", function () {
                        viewModel.tokenizedFilterCriteria.push("10");
                        viewModel.tokenizedFilterCriteria.push("20");

                        viewModel.removeFilterCriteria("10");
                        expect(viewModel.tokenizedFilterCriteria().length).toEqual(1);
                    });

                    it("uniqueCriteria returns an array of unique tokenized criteria", function () {
                        var val = viewModel.uniqueCriteria("1, 1, 1");
                        expect(val.length).toEqual(1);
                    });

                    it("validateFilterCriteria returns true when no validation function is passed as a param", function () {
                        var val = viewModel.validateFilterCriteria();
                        expect(val).toBeTruthy();
                    });

                    it("validateFilterCriteria calls the validation function passed as a param", function () {
                        params.validate = function (criteria, errorMessages) {
                            return false;
                        };
                        
                        spyOn(params, "validate");
                        var val = viewModel.validateFilterCriteria();
                        expect(params.validate).toHaveBeenCalled();
                    });

                    it("setValidationTimer is called when workingCriteria is changed", function () {
                        viewModel.workingCriteria("01");
                        viewModel.workingCriteria("");
                        expect(viewModel.errorMessages().length).toEqual(0);
                    });
                });
                
            });

        });