﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/Icon/_Icon'],

function (component, events) {

    describe("Icon Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var IconViewModel = component.viewModel;
        var params = {};
        var viewModel;
        beforeEach(function () {
            params = {
                isVisible: ko.observable(true),
                icon: ko.observable("error")
            };
            viewModel = new IconViewModel(params);
        });

        describe("isVisible property: ", function () {

            it("error logged to console if params.isVisible is undefined", function () {
                params = {
                    icon: ko.observable("error")
                };
                spyOn(console, 'log').and.callThrough();
                viewModel = new IconViewModel(params);
                expect(console.log).toHaveBeenCalledWith("Please provide a valid isVisible value.");
            });

            it("set to params.isVisible value when params.isVisible is observable", function () {
                expect(viewModel.isVisible()).toEqual(params.isVisible());
            });

            it("set to params.isVisible value when params.isVisible is not observable", function () {
                params = {
                    isVisible: true,
                    icon: ko.observable("error")
                };
                viewModel = new IconViewModel(params);
                expect(viewModel.isVisible()).toEqual(params.isVisible);
            });
        });

        describe("iconId property: ", function () {

            iconList = [ "add",
                         "arrow",
                         "arrow-double",
                         "avatar",
                         "calendar",
                         "calendar-term",
                         "cards",
                         "check",
                         "clock",
                         "clear",
                         "close",
                         "comment",
                         "delete",
                         "document",
                         "drag",
                         "edit",
                         "email",
                         "error",
                         "export",
                         "favorite",
                         "filter",
                         "folder-open",
                         "folder-closed",
                         "graduation",
                         "group",
                         "help",
                         "hide",
                         "home",
                         "image",
                         "info",
                         "list",
                         "location",
                         "lookup",
                         "menu",
                         "more",
                         "new-document",
                         "notification",
                         "print",
                         "refresh",
                         "save",
                         "search",
                         "settings",
                         "share",
                         "skip",
                         "swap",
                         "view"];

            it("error logged to console if params.icon is undefined", function () {
                params = {
                    isVisible: ko.observable(true),
                };
                spyOn(console, 'log').and.callThrough();
                viewModel = new IconViewModel(params);
                expect(console.log).toHaveBeenCalledWith("Please provide a valid icon value.");
            });

            it("error logged to console if params.icon is not one of the allowed values", function () {
                params = {
                    isVisible: ko.observable(true),
                    icon: ko.observable("INVALID")
                };
                spyOn(console, 'log').and.callThrough();
                viewModel = new IconViewModel(params);
                expect(console.log).toHaveBeenCalledWith("Invalid icon value provided!");
            });

            // Function to test individual dictionary key/value pairs
            function test_iconId_property(input, output) {
                it("set to '" + output + "' when params.icon is '" + input + "'", function () {
                    params = {
                        isVisible: ko.observable(true),
                        icon: ko.observable(input)
                    };
                    viewModel = new IconViewModel(params);
                    expect(viewModel.iconId()).toEqual(output);
                })
            }

            // Iterate over icon dictionary members
            for (var i = 0; i < iconList.length; i++) {
                test_iconId_property(iconList[i], "#icon-" + iconList[i]);
            }
        });

        describe("iconClasses property: ", function () {

            describe("using 'direction' parameter: ", function () {

                directionList = [ "right",
                                  "down",
                                  "left",
                                  "up"];

                it("error logged to console if params.direction is not one of the allowed values", function () {
                    params = {
                        isVisible: ko.observable(true),
                        icon: ko.observable("error"),
                        direction: ko.observable("INVALID")
                    };
                    spyOn(console, 'log').and.callThrough();
                    viewModel = new IconViewModel(params);
                    expect(console.log).toHaveBeenCalledWith("Invalid direction value provided!");
                });

                // Function to test individual dictionary key/value pairs
                function test_iconClasses_direction_property(input, output) {
                    it("set to '" + output + "' when params.direction is '" + input + "'", function () {
                        params = {
                            isVisible: ko.observable(true),
                            icon: ko.observable("error"),
                            direction: ko.observable(input)
                        };
                        viewModel = new IconViewModel(params);
                        expect(viewModel.iconClasses()).toEqual(output);
                    })
                }

                // Iterate over icon dictionary members
                for (var i = 0; i < directionList.length; i++) {
                    test_iconClasses_direction_property(directionList[i], " esg-icon--" + directionList[i]);
                }
            });

            describe("using 'size' parameter: ", function () {

                sizeList = [ "xsmall", 
                             "small", 
                             "medium", 
                             "large"];

                it("error logged to console if params.size is not one of the allowed values", function () {
                    params = {
                        isVisible: ko.observable(true),
                        icon: ko.observable("error"),
                        size: ko.observable("INVALID")
                    };
                    spyOn(console, 'log').and.callThrough();
                    viewModel = new IconViewModel(params);
                    expect(console.log).toHaveBeenCalledWith("Invalid size value provided!");
                });

                // Function to test individual dictionary key/value pairs
                function test_iconClasses_size_property(input, output) {
                    it("set to '" + output + "' when params.size is '" + input + "'", function () {
                        params = {
                            isVisible: ko.observable(true),
                            icon: ko.observable("error"),
                            size: ko.observable(input)
                        };
                        viewModel = new IconViewModel(params);
                        expect(viewModel.iconClasses()).toEqual(output);
                    })
                }

                // Iterate over icon dictionary members
                for (var i = 0; i < sizeList.length; i++) {
                    test_iconClasses_size_property(sizeList[i], " esg-icon--" + sizeList[i]);
                }
            });

        });

        describe("iconContainerClasses property: ", function () {

            colorList = [ "light",
                          "neutral",
                          "info",
                          "info-dark",
                          "warning",
                          "warning-dark",
                          "error",
                          "error-dark",
                          "success",
                          "success-dark"];

            it("error logged to console if params.color is not one of the allowed values", function () {
                params = {
                    isVisible: ko.observable(true),
                    icon: ko.observable("error"),
                    color: ko.observable("INVALID")
                };
                spyOn(console, 'log').and.callThrough();
                viewModel = new IconViewModel(params);
                expect(console.log).toHaveBeenCalledWith("Invalid container color value provided!");
            });

            // Function to test individual dictionary key/value pairs
            function test_iconContainerClasses_property(input, output) {
                it("set to '" + output + "' when params.color is '" + input + "'", function () {
                    params = {
                        isVisible: ko.observable(true),
                        icon: ko.observable("error"),
                        color: ko.observable(input)
                    };
                    viewModel = new IconViewModel(params);
                    expect(viewModel.iconContainerClasses()).toEqual(output);
                })
            }

            // Iterate over icon dictionary members
            for (var i = 0; i < colorList.length; i++) {
                test_iconContainerClasses_property(colorList[i], " esg-icon__container--" + colorList[i]);
            }
        });

    });
});