﻿//define(['Ellucian.Web.Student/Scripts/Components/AutoSave/_AutoSave',
//        'Ellucian.Web.Student/Scripts/Components/AutoSave/_AutoSaveEvents'],
       
//        function (component, events) {

//            getResourceUrl = "Resources/GetResources";

//            describe("Test Suite for AutoSave Component", function () {

//                jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

//                var AutoSaveViewModel = component.viewModel;
//                var params = {};

//                beforeEach(function () {
//                    params = {
//                        initialLastSaveDate: null,
//                        isMobile: ko.observable(false),
//                    };
//                });

//                describe("initialization", function () {
//                    it("should initialize isMobile to params value", function () {
//                        expect(new AutoSaveViewModel(params).isMobile()).toEqual(params.isMobile());
//                    });

//                    it("should set saveError to false", function () {
//                        expect(new AutoSaveViewModel(params).saveError()).toBeFalsy();
//                    });
//                });

//                describe("saveText labels", function () {

//                    it("should initialize if last save date does not exist", function () {
//                        var viewModel = new AutoSaveViewModel(params);
//                        expect(viewModel.saveTextResx()).toEqual("AutoSave.InitialMessage");
//                        expect(viewModel.shortSaveTextResx()).toEqual("AutoSave.InitialMessageShort");
//                    });

//                    it("should initialize if last save date does exist", function () {
//                        params.initialLastSaveDate = new Date();
//                        var viewModel = new AutoSaveViewModel(params);
//                        expect(viewModel.saveTextResx()[0]).toEqual("AutoSave.LastSavedMessage");
//                        expect(viewModel.shortSaveTextResx()[0]).toEqual("AutoSave.LastSavedMessageShort");
//                    });

//                    it("should change on SAVESTART event", function (done) {
//                        var viewModel = new AutoSaveViewModel(params);
//                        ko.postbox.publish(events.AUTOSAVE_SAVESTART);
//                        window.setTimeout(function () {
//                            expect(viewModel.saveTextResx()).toEqual("AutoSave.SavingMessage");
//                            expect(viewModel.shortSaveTextResx()).toEqual("AutoSave.SavingMessageShort");
//                            done();
//                        }, 50);
//                    });

//                    it("should change on SAVECOMPLETE event", function (done) {
//                        var viewModel = new AutoSaveViewModel(params);
//                        ko.postbox.publish(events.AUTOSAVE_SAVECOMPLETE);
//                        window.setTimeout(function () {
//                            expect(viewModel.saveTextResx()).toEqual("AutoSave.SavedJustNowMessage");
//                            expect(viewModel.shortSaveTextResx()).toEqual("AutoSave.SavedJustNowMessageShort");
//                            done();
//                        }, 50);
//                    });

//                    it("should change on SAVEERROR event", function (done) {
//                        var viewModel = new AutoSaveViewModel(params);
//                        ko.postbox.publish(events.AUTOSAVE_SAVEERROR);
//                        window.setTimeout(function () {
//                            expect(viewModel.saveTextResx()).toEqual("AutoSave.ErrorSavingMessage");
//                            expect(viewModel.shortSaveTextResx()).toEqual("AutoSave.ErrorSavingMessageShort");
//                            expect(viewModel.saveError()).toBeTruthy();
//                            done();
//                        }, 50);
//                    });

//                    it("should change after 15 seconds after SAVECOMPLETE event", function (done) {
//                        var viewModel = new AutoSaveViewModel(params);
//                        ko.postbox.publish(events.AUTOSAVE_SAVECOMPLETE);
//                        window.setTimeout(function () {
//                            expect(viewModel.saveTextResx()[0]).toEqual("AutoSave.LastSavedMessage");
//                            expect(viewModel.shortSaveTextResx()[0]).toEqual("AutoSave.LastSavedMessageShort");
//                            done();
//                        }, 15100);
//                    }, 20000);

//                });

//                describe("save button", function () {
//                    it("should publish SAVEBUTTONPRESSED event when click is handled", function () {
//                        spyOn(ko.postbox, "publish");
//                        var viewModel = new AutoSaveViewModel(params);
//                        viewModel.saveButtonClickHandler();
//                        expect(ko.postbox.publish).toHaveBeenCalledWith(events.AUTOSAVE_SAVEBUTTONPRESSED, jasmine.any(Object));
//                    });

//                    it("should be enabled by default", function () {
//                        expect(new AutoSaveViewModel(params).saveButtonEnabled()).toBeTruthy();
//                    });

//                    it("should be enabled/disabled based on published value of SETBUTTONSTATUS event", function () {
//                        var viewModel = new AutoSaveViewModel(params);
//                        ko.postbox.publish(events.AUTOSAVE_SETBUTTONSTATUS, false);
//                        expect(viewModel.saveButtonEnabled()).toBeFalsy();

//                        ko.postbox.publish(events.AUTOSAVE_SETBUTTONSTATUS, true);
//                        expect(viewModel.saveButtonEnabled()).toBeTruthy();
//                    });
//                });

//                describe("timestamp calculation", function () {
//                    it("should display the saved time when last save is less than 24 hrs ago", function () {
//                        params.initialLastSaveDate = new Date();
//                        spyOn(Globalize, "format");
//                        var viewModel = new AutoSaveViewModel(params);
//                        expect(Globalize.format).toHaveBeenCalledWith(params.initialLastSaveDate, "t");
//                    });

//                    it("should display the saved date when last save is more than 24 hrs ago", function () {
//                        params.initialLastSaveDate = new Date(2011, 1, 1);
//                        spyOn(Globalize, "format");
//                        var viewModel = new AutoSaveViewModel(params);
//                        expect(Globalize.format).toHaveBeenCalledWith(params.initialLastSaveDate, "M");

//                    })
//                })

//            });

//        });