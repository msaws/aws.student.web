﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/Spinner/_Spinner'],

function (component, events) {

    describe("Spinner Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var SpinnerViewModel = component.viewModel;
        var params = {};
        var viewModel;
        beforeEach(function () {
            params = {
                isVisible: ko.observable(true),
                message: ko.observable("Loading page data...")
            };
            viewModel = new SpinnerViewModel(params);
        });

        describe("isVisible property: ", function () {

            it("error logged to console if params.isVisible is undefined", function () {
                params = {
                    message: ko.observable("Loading page data...")
                };
                spyOn(console, 'log').and.callThrough();
                viewModel = new SpinnerViewModel(params);
                expect(console.log).toHaveBeenCalledWith("Please provide a valid isVisible parameter.");
            });

            it("set to params.isVisible value when params.isVisible is observable", function () {
                expect(viewModel.isVisible()).toEqual(params.isVisible());
            });

            it("set to params.isVisible value when params.isVisible is not observable", function () {
                params = {
                    isVisible: true,
                    message: ko.observable("Loading page data...")
                };
                viewModel = new SpinnerViewModel(params);
                expect(viewModel.isVisible()).toEqual(params.isVisible);
            });
        });

        describe("message property: ", function () {

            it("set to Ellucian.Site.Resources.SpinnerDefaultMessage when params.message is undefined", function () {
                params = {
                    isVisible: ko.observable(true)
                };
                viewModel = new SpinnerViewModel(params);
                expect(viewModel.message()).toEqual(Ellucian.Site.Resources.SpinnerDefaultMessage);
            });

            it("set to params.message value when params.message is observable", function () {
                expect(viewModel.message()).toEqual(params.message());
            });

            it("set to params.message value when params.message is not observable", function () {
                params = {
                    isVisible: ko.observable(true),
                    message: "Spinner Message..."
                };
                viewModel = new SpinnerViewModel(params);

                expect(viewModel.message()).toEqual(params.message);
            });

        });

        describe("showMessage property: ", function () {

            it("set to true if message is defined with length > 0", function () {
                expect(viewModel.showMessage()).toBeTruthy();
            });

            it("set to false if message property length === 0", function () {
                params = {
                    isVisible: true,
                    message: ko.observable("")
                };
                viewModel = new SpinnerViewModel(params);
                expect(viewModel.showMessage()).toBeFalsy();
            });
        });

        describe("wrapperClass property: ", function () {

            it("set to 'css-spinner-wrapper' if message is defined with length > 0", function () {
                expect(viewModel.wrapperClass()).toEqual('css-spinner-wrapper');
            });

            it("set to 'css-spinner-wrapper' if message property length === 0", function () {
                params = {
                    isVisible: true,
                    message: ko.observable("")
                };
                viewModel = new SpinnerViewModel(params);
                expect(viewModel.wrapperClass()).toEqual('css-spinner-wrapper css-spinner-wrapper--no-padding-top');
            });
        });
    });
});