﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/CsvDownloadLink/_CsvDownloadLink'],

function (component, events) {

    describe("CSV Download Link: ", function () {
        var singleCostCenter1 = {
            "Id": ko.observable("0000"),
            "Name": ko.observable("Record name 1"),
            "Amount1": ko.observable("$0.00"),
            "Amount2": ko.observable("$36,540.00"),
            "Amount3": ko.observable("$500.00"),
        };

        var singleCostCenter2 = {
            "Id": ko.observable("1"),
            "Name": ko.observable("Record name 2"),
            "Amount1": ko.observable(""),
            "Amount2": ko.observable(""),
            "Amount3": ko.observable("$0.00"),
        };

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var CsvDownloadLinkViewModel = component.viewModel;
        var params = {};
        var viewModel;
        var sourceData;
        var csvColumns;
        var csvColumnHeaders;
        beforeEach(function () {
            sourceData = [singleCostCenter1, singleCostCenter2];

            csvColumns = ko.observableArray(
                ['Id',
                'Name',
                'Amount1',
                'Amount2',
                'Amount3']);

            csvColumnHeaders = ko.observableArray(
                ['Cost Center ID',
                'Cost Center Description',
                'Amount 1',
                'Amount 2',
                'Amount 3']);

            params = {
                data: sourceData,
                linkText: "Download CSV",
                fileName: "cost-centers-2017.csv",
                includeKeys: true,
                limitToKeys: csvColumns(),
                keyTextReplacements: csvColumnHeaders()
            };
            viewModel = new CsvDownloadLinkViewModel(params);
        });

        describe("limitToKeys", function () {
            it("should return the value specified if populated", function () {
                expect(viewModel.limitToKeys()).toBe(csvColumns());
            });

            it("should return an empty array if null", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: null,
                    keyTextReplacements: []
                };
                var viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.limitToKeys().length).toBe(0);
            });

            it("should return an empty array if empty", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: [],
                    keyTextReplacements: []
                };
                var viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.limitToKeys().length).toBe(0);
            });
        });

        describe("keyTextReplacements", function () {
            it("should return the value specified if populated", function () {
                expect(viewModel.keyTextReplacements()).toBe(csvColumnHeaders());
            });

            it("should return an empty array if null", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: null
                };
                var viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.keyTextReplacements().length).toBe(0);
            });

            it("should return an empty array if empty", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: [],
                    keyTextReplacements: []
                };
                var viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.keyTextReplacements().length).toBe(0);
            });
        });

        describe("includeKeys", function () {
            it("should return the value specified (true) if populated", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.includeKeys()).toBeTruthy();
            });

            it("should return the value specified (false) if populated", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: false,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.includeKeys()).toBeFalsy();
            });

            it("should return true if parameter is undefined", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: "undefined",
                    limitToKeys: csvColumns(),
                    keyTextReplacements: null
                };
                var viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.includeKeys()).toBeTruthy();
            });

            it("should return true if parameter is null", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: null,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: null
                };
                var viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.includeKeys()).toBeTruthy();
            });
        });

        describe("linkText", function () {
            it("should return the value specified if populated", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.linkText()).toBe("Download CSV");
            });

            it("should return the default link text if parameter is null", function () {
                params = {
                    data: [],
                    linkText: null,
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.linkText()).toBe("Download File");
            });

            it("should return the default link text if parameter is empty", function () {
                params = {
                    data: [],
                    linkText: "",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.linkText()).toBe("Download File");
            });
        });

        describe("jsData", function () {
            it("should return the raw version of the KO input data", function () {
                var rawData = viewModel.jsData();
                for (var i = 0; i < sourceData.length; i++) {
                    currentCostCenterModel = sourceData[i];
                    for (var key in currentCostCenterModel) {
                        if (csvColumns.indexOf(key) != -1) {
                            expect(currentCostCenterModel[key]()).toBe(rawData[i][key]);
                        }
                    }
                }
            });
        });

        describe("overrideHref", function () {
            it("should default to blank", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                expect(viewModel.overrideHref()).toBe("");
            });
        });

        describe("href", function () {
            it("should return the overrideHref if overrideHref has a value", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                viewModel.overrideHref("foo");
                expect(viewModel.href()).toBe(viewModel.overrideHref());
            });

            it("should return the fileName if overrideHref is blank", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                viewModel.overrideHref("");
                expect(viewModel.href()).toBe(params.fileName);
            });

            it("should return the fileName if overrideHref is null", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                viewModel.overrideHref(null);
                expect(viewModel.href()).toBe(params.fileName);
            });

            it("should set the overrideHref to blank when set to the filename", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                viewModel.href("cost-centers-2017.csv");
                expect(viewModel.overrideHref()).toBe("");
                expect(viewModel.href()).toBe(params.fileName);
            });

            it("should set the overrideHref to the input value - foo", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                var newHrefValue = "foo";
                viewModel.href(newHrefValue);
                expect(viewModel.overrideHref()).toBe(newHrefValue);
                expect(viewModel.href()).toBe(newHrefValue);
            });

            it("should set the overrideHref to the input value - blank", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                var newHrefValue = "";
                viewModel.href(newHrefValue);
                expect(viewModel.overrideHref()).toBe(newHrefValue);
                expect(viewModel.href()).toBe(params.fileName);
            });

            it("should set the overrideHref to the input value - null", function () {
                params = {
                    data: [],
                    linkText: "Download CSV",
                    fileName: "cost-centers-2017.csv",
                    includeKeys: true,
                    limitToKeys: csvColumns(),
                    keyTextReplacements: csvColumnHeaders()
                };
                viewModel = new CsvDownloadLinkViewModel(params);
                var newHrefValue = null;
                viewModel.href(newHrefValue);
                expect(viewModel.overrideHref()).toBe(newHrefValue);
                expect(viewModel.href()).toBe(params.fileName);
            });
        });

        describe("convertToCsv", function () {
            function splitCsvIntoArray(inputCsv) {
                var inputCsAvrray = inputCsv.split('","');
                var outputCsvArray = [];
                for (var j = 0; j < inputCsAvrray.length; j++) {
                    inputCsAvrray[j] = inputCsAvrray[j].replace('"', '');
                    inputCsAvrray[j] = inputCsAvrray[j].replace('"', '');
                    outputCsvArray.push(inputCsAvrray[j]);
                }
                return outputCsvArray;
            }

            it("should return CSV data with no headers - data only", function () {
                var expectedCsv = "";

                // Insert the observable data into CSV format.
                for (var i = 0; i < sourceData.length; i++) {
                    if (i > 0) { expectedCsv += "\n"; }

                    expectedCsv += '"' + sourceData[i].Id() + '"'
                        + ',"' + sourceData[i].Name() + '"'
                        + ',"' + sourceData[i].Amount1() + '"'
                        + ',"' + sourceData[i].Amount2() + '"'
                        + ',"' + sourceData[i].Amount3() + '"';
                }

                var newParams = {
                    data: sourceData,
                    includeKeys: false
                };
                viewModel = new CsvDownloadLinkViewModel(newParams);

                var expectedCsvArray = expectedCsv.split('\n');
                var actualCsvArray = viewModel.convertToCsv().split('\n');
                
                for (var i = 0; i < expectedCsvArray.length; i++) {
                    expect(expectedCsvArray[i].length).toBe(actualCsvArray[i].length);

                    var expectedCsvRow = splitCsvIntoArray(expectedCsvArray[i]);
                    var actualCsvRow = splitCsvIntoArray(actualCsvArray[i]);
                    for (var j = 0; j < expectedCsvRow.length; j++) {
                        expect(expectedCsvRow[j]).toBe(actualCsvRow[j]);
                    }
                }
            });

            it("should return CSV data with headers and data", function () {
                csvColumns = ko.observableArray(
                    ['Id',
                    'Name',
                    'Amount1',
                    'Amount2',
                    'Amount3']);

                // Insert the headers
                var expectedCsv = "";
                for (var i = 0; i < csvColumns().length; i++) {
                    if (i > 0) { expectedCsv += ","; }
                    expectedCsv += '"' + csvColumns()[i] + '"';
                }
                expectedCsv += "\n";

                // Insert the observable data into CSV format.
                for (var i = 0; i < sourceData.length; i++) {
                    if (i > 0) { expectedCsv += "\n"; }

                    expectedCsv += '"' + sourceData[i].Id() + '"'
                        + ',"' + sourceData[i].Name() + '"'
                        + ',"' + sourceData[i].Amount1() + '"'
                        + ',"' + sourceData[i].Amount2() + '"'
                        + ',"' + sourceData[i].Amount3() + '"';
                }

                var newParams = {
                    data: sourceData,
                };
                var newViewModel = new CsvDownloadLinkViewModel(newParams);

                var expectedCsvArray = expectedCsv.split('\n');
                var actualCsvArray = newViewModel.convertToCsv().split('\n');

                for (var i = 0; i < expectedCsvArray.length; i++) {
                    expect(expectedCsvArray[i].length).toBe(actualCsvArray[i].length);

                    var expectedCsvRow = splitCsvIntoArray(expectedCsvArray[i]);
                    var actualCsvRow = splitCsvIntoArray(actualCsvArray[i]);
                    for (var j = 0; j < expectedCsvRow.length; j++) {
                        expect(expectedCsvRow[j]).toBe(actualCsvRow[j]);
                    }
                }
            });

            it("should return CSV data with headers and data in a different sorted order", function () {
                csvColumns = ko.observableArray(
                    ['Name',
                    'Id',
                    'Amount2',
                    'Amount1',
                    'Amount3']);

                // Insert the headers
                var expectedCsv = "";
                for (var i = 0; i < csvColumns().length; i++) {
                    if (i > 0) { expectedCsv += ","; }
                    expectedCsv += '"' + csvColumns()[i] + '"';
                }
                expectedCsv += "\n";

                // Insert the observable data into CSV format.
                for (var i = 0; i < sourceData.length; i++) {
                    if (i > 0) { expectedCsv += "\n"; }

                    expectedCsv += '"' + sourceData[i].Name() + '"'
                        + ',"' + sourceData[i].Id() + '"'
                        + ',"' + sourceData[i].Amount2() + '"'
                        + ',"' + sourceData[i].Amount1() + '"'
                        + ',"' + sourceData[i].Amount3() + '"';
                }

                var newParams = {
                    data: sourceData,
                    limitToKeys: csvColumns
                };
                
                var newViewModel = new CsvDownloadLinkViewModel(newParams);

                var expectedCsvArray = expectedCsv.split('\n');
                var actualCsvArray = newViewModel.convertToCsv().split('\n');

                for (var i = 0; i < expectedCsvArray.length; i++) {
                    expect(expectedCsvArray[i].length).toBe(actualCsvArray[i].length);

                    var expectedCsvRow = splitCsvIntoArray(expectedCsvArray[i]);
                    var actualCsvRow = splitCsvIntoArray(actualCsvArray[i]);
                    for (var j = 0; j < expectedCsvRow.length; j++) {
                        expect(expectedCsvRow[j]).toBe(actualCsvRow[j]);
                    }
                }
            });

            it("should return CSV data with replacement headers and data in a different sorted order", function () {
                csvColumns = ko.observableArray(
                    ['Name',
                    'Amount2',
                    'Amount3',
                    'Id',
                    'Amount1',]);

                csvColumnHeaders = ko.observableArray(
                    ['Name override',
                    'Amount 2 override',
                    'Amount 3 override',
                    'ID override',
                    'Amount 1 override']);

                // Insert the headers
                var expectedCsv = "";
                for (var i = 0; i < csvColumnHeaders().length; i++) {
                    if (i > 0) { expectedCsv += ","; }
                    expectedCsv += '"' + csvColumnHeaders()[i] + '"';
                }
                expectedCsv += "\n";

                // Insert the observable data into CSV format.
                for (var i = 0; i < sourceData.length; i++) {
                    if (i > 0) { expectedCsv += "\n"; }

                    expectedCsv += '"' + sourceData[i].Name() + '"'
                        + ',"' + sourceData[i].Amount2() + '"'
                        + ',"' + sourceData[i].Amount3() + '"'
                        + ',"' + sourceData[i].Id() + '"'
                        + ',"' + sourceData[i].Amount1() + '"';
                }

                var newParams = {
                    data: sourceData,
                    includeKeys: true,
                    limitToKeys: csvColumns,
                    keyTextReplacements: csvColumnHeaders
                };
                var newViewModel = new CsvDownloadLinkViewModel(newParams);

                var expectedCsvArray = expectedCsv.split('\n');
                var actualCsvArray = newViewModel.convertToCsv().split('\n');

                for (var i = 0; i < expectedCsvArray.length; i++) {
                    expect(expectedCsvArray[i].length).toBe(actualCsvArray[i].length);

                    var expectedCsvRow = splitCsvIntoArray(expectedCsvArray[i]);
                    var actualCsvRow = splitCsvIntoArray(actualCsvArray[i]);
                    for (var j = 0; j < expectedCsvRow.length; j++) {
                        expect(expectedCsvRow[j]).toBe(actualCsvRow[j]);
                    }
                }
            });

            it("should return CSV data with fewer columns than originally available", function () {
                csvColumns = ko.observableArray(
                    ['Id','Amount1']);

                // Insert the headers
                var expectedCsv = "";
                for (var i = 0; i < csvColumns().length; i++) {
                    if (i > 0) { expectedCsv += ","; }
                    expectedCsv += '"' + csvColumns()[i] + '"';
                }
                expectedCsv += "\n";

                // Insert the observable data into CSV format.
                for (var i = 0; i < sourceData.length; i++) {
                    if (i > 0) { expectedCsv += "\n"; }

                    expectedCsv += '"' + sourceData[i].Id() + '"'
                        + ',"' + sourceData[i].Amount1() + '"';
                }

                var newParams = {
                    data: sourceData,
                    limitToKeys: csvColumns,
                };
                var newViewModel = new CsvDownloadLinkViewModel(newParams);

                var expectedCsvArray = expectedCsv.split('\n');
                var actualCsvArray = newViewModel.convertToCsv().split('\n');
                
                for (var i = 0; i < expectedCsvArray.length; i++) {
                    expect(expectedCsvArray[i].length).toBe(actualCsvArray[i].length);

                    var expectedCsvRow = splitCsvIntoArray(expectedCsvArray[i]);
                    var actualCsvRow = splitCsvIntoArray(actualCsvArray[i]);
                    for (var j = 0; j < expectedCsvRow.length; j++) {
                        expect(expectedCsvRow[j]).toBe(actualCsvRow[j]);
                    }
                }
            });
        });

        describe("downloadFile", function () {
            it("should return false when 'window.navigator.msSaveOrOpenBlob' exists", function () {
                window = window || {};
                window.navigator = window.navigator || {}
                window.navigator.msSaveOrOpenBlob = window.navigator.msSaveOrOpenBlob || {};

                spyOn(window.navigator, "msSaveOrOpenBlob");
                expect(viewModel.downloadFile()).toBeFalsy();
                expect(window.navigator.msSaveOrOpenBlob).toHaveBeenCalled();
            });

            it("should return true when 'window.navigator.msSaveOrOpenBlob' does not exist", function () {
                window = window || {};
                window.navigator = window.navigator || {}
                window.navigator.msSaveOrOpenBlob = null;

                spyOn(viewModel, "href");
                expect(viewModel.downloadFile()).toBeTruthy();
                expect(viewModel.href).toHaveBeenCalled();
            });
        });
    });
});