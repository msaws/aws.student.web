﻿//const component = require("Site/Components/Dropdown/_Dropdown");

//describe("Test Suite for Dropdown Component", () => {

//    const viewModel = component.viewModel;
//    var params = {};

//    const innerHtml = document.body.innerHTML;    

//    beforeEach(() => {
//        params = {
//            buttonId: ko.observable(),
//            buttonCaption: undefined,
//            items: ko.observableArray(["1", "2","3","4"]),
//            itemsText: undefined,
//            value: ko.observable(),
//            afterRender: undefined
//        };        

//        document.body.innerHTML = innerHtml.concat(component.template);
        
//    });

    
//    describe("User iteracting with Dropdown", () => {
    
//        let model;        
//        beforeEach(() => {
//            model = new viewModel(params);
//            ko.applyBindings(model);
//        });

//        afterEach(() => {
//            ko.cleanNode(document.body);
//        })

//        it("should toggle dropdown open on button click ", () => {

//            spyOn(model, "toggleDropdown").and.callThrough();
        
//            $("#"+ko.unwrap(model.buttonId)).click();
//            expect(model.toggleDropdown).toHaveBeenCalled();
//            expect(model.openClass()).toEqual("esg-is-open");
//            expect(model.iconClass()).toEqual('esg-icon--up');
//            expect($("#"+ko.unwrap(model.buttonId)).hasClass('esg-is-open')).toBeTruthy(); 
//        });
    
//        it("should toggle dropdown closed on button click ", () => {
//            spyOn(model, "toggleDropdown").and.callThrough();
        
//            $("#"+ko.unwrap(model.buttonId)).click();
//            $("#"+ko.unwrap(model.buttonId)).click();
//            expect(model.toggleDropdown.calls.count()).toEqual(2);
//            expect(model.openClass()).toEqual("");
//            expect(model.iconClass()).toEqual('esg-icon--down');
//            expect($("#"+ko.unwrap(model.buttonId)).hasClass('esg-is-open')).toBeFalsy();  
//        });

//        it("should toggle dropdown closed when clicking outside dropdown", () => {
 
//            $("#"+ko.unwrap(model.buttonId)).click();

//            $(document).click();
//            expect(model.openClass()).toEqual("");
//            expect(model.iconClass()).toEqual('esg-icon--down');
//            expect($("#"+ko.unwrap(model.buttonId)).hasClass('esg-is-open')).toBeFalsy();               
//        });

//        it("should update value and close dropdown when item is clicked", () => {
//            $("#"+model.buttonId()).click();

//            $(".esg-dropdown__menu-item:first-child").click();
//            expect(params.value()).toEqual(params.items()[0]);
//            expect(model.openClass()).toEqual("");
//            expect(model.iconClass()).toEqual('esg-icon--down');
//            expect($("#"+ko.unwrap(model.buttonId)).hasClass('esg-is-open')).toBeFalsy();  
//        });
//    });

//    describe("ButtonId parameter", () => {

//        let model;
//        function createModel() {
//            model = new viewModel(params);
//            ko.applyBindings(model);
//        }

//        afterEach(() => {
//            ko.cleanNode(document.body);
//        })

//        it("should set element id with non-observable", () => {
//            params.buttonId = "foobar";
//            createModel();
//            expect(model.buttonId()).toEqual(params.buttonId);
//            expect($('#'+model.buttonId()).is('.esg-button')).toBeTruthy();        
//        });

//        it("should calculate element id if not supplied", () => {                
//            createModel();         
//            expect(model.buttonId()).toEqual("1-dropdownButton"); 
//            expect(model.buttonId()).toEqual(params.buttonId());               
//        });

//    });

//    describe("Parameter error checking", () => {
//        it("should throw error if params.value is undefined", () => {
//            params.value = undefined;
//            try {
//                new viewModel(params);
//            }
//            catch(error) {
//                expect(error).toBeDefined();
//            }
//        });
//        it("should throw error if params.value is null", () => {
//            params.value = null;
//            try {
//                new viewModel(params);
//            }
//            catch(error) {
//                expect(error).toBeDefined();
//            }
//        });
//        it("should throw error if params.value not an observable", () => {
//            params.value = "foobar";
//            try {
//                new viewModel(params);
//            }
//            catch(error) {
//                expect(error).toBeDefined();
//            }
//        });

//        it("should throw error if params.value is undefined", () => {
//            params.items = undefined;
//            try {
//                new viewModel(params);
//            }
//            catch(error) {
//                expect(error).toBeDefined();
//            }
//        });
//        it("should throw error if params.value is null", () => {
//            params.items = null;
//            try {
//                new viewModel(params);
//            }
//            catch(error) {
//                expect(error).toBeDefined();
//            }
//        });
//    })


//    describe("ButtonCaption parameter", () => {
//        let model;
//        function createModel() {
//            model = new viewModel(params);
//            ko.applyBindings(model);
//        }

//        afterEach(() => {
//            ko.cleanNode(document.body);
//        });
    
//        it("should set model property and text content of button", () => {
//            params.buttonCaption = "foobar";
//            createModel();
//            expect(model.buttonCaption).toEqual(params.buttonCaption);
//            expect($('#'+model.buttonId()+" span:first-child").text()).toBe(params.buttonCaption);
//        });
    
//        it("should add undefined item to front of the original items when set", () => {
//            params.buttonCaption = "foobar";
//            createModel();
//            expect(model.dropdownItems()[0].originalItem).not.toBeDefined();
//        });

//        it("should cause undefined value parameter to be set to first original item when not set", () => {
//            params.buttonCaption = undefined;
//            params.value(undefined);
//            createModel();
//            expect(params.value()).toEqual(params.items()[0]);
//        });
//    });
    
//    describe("Selected Text Displayed", () => {
//        let model;
//        function createModel() {
//            model = new viewModel(params);
//            ko.applyBindings(model);
//        }

//        afterEach(() => {
//            ko.cleanNode(document.body);
//        });
//        it("should display button caption if nothing selected", () => {
//            params.buttonCaption = "foobar";
//            createModel(); 
//            expect(model.selectedText()).toEqual(params.buttonCaption);
//        });

//        it("should display undefined if no caption and no items", () => {        
//            params.items.removeAll();
//            createModel();
//            expect(model.selectedText()).not.toBeDefined();
//        });
//        it("should display text of first item if no caption and value is undefined", () => {
//            createModel();
//            params.value(undefined);
//            expect(model.selectedText()).toEqual(params.items()[0]);
//        });
//        it("should display text of params.value", () => {
//            params.buttonCaption = "foobar";
//            createModel();
//            params.value(params.items()[2]);
//            expect(model.selectedText()).toEqual(params.items()[2]);
//        });
//        it("should display toString of arbitrary object when no itemsText parameter", () => {
//            params.items = ko.observableArray([{name: "1", val: "foo"}, {name: "2", val: "bar"}]);
//            params.value(params.items()[0]);
//            createModel();
//            expect(model.selectedText()).toEqual(params.items()[0].toString());
//        });
//        it("should display attribute of arbitrary object when itemsText is string", () => {
//            params.items = ko.observableArray([{name: "1", val: "foo"}, {name: "2", val: "bar"}]);
//            params.value(params.items()[0]);
//            params.itemsText = "name";
//            createModel();
//            expect(model.selectedText()).toEqual(params.items()[0].name);
//        });
//        it("should display result of itemsText function", () => {
//            params.items = ko.observableArray([{name: "1", val: "foo"}, {name: "2", val: "bar"}]);
//            params.value(params.items()[0]);
//            params.itemsText = function(item) {
//                return item.val;
//            }
//            createModel();
//            expect(model.selectedText()).toEqual(params.itemsText(params.items()[0]));
//        });

//    });

//    describe("postProcess function", () => {
//        it("should invoke afterRender callback", () => {
//            params.afterRender = function() {
//                expect(true).toBeTruthy();
//            }
//            let model = new viewModel(params);
//            model.postProcess();
//        });
//    });

//});