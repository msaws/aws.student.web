﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/StepProgress/_StepProgress'],

function (component, events) {

    describe("StepProgress Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var StepProgressViewModel = component.viewModel;
        var params = {};
        var viewModel;
        beforeEach(function () {
            params = {
                steps: ko.observableArray([
                    { text: ko.observable('Step 1'), href: ko.observable('http://www.ellucian.edu/step1') },
                    { text: ko.observable('Step 2'), href: ko.observable('http://www.ellucian.edu/step2') },
                    { text: ko.observable('Step 3'), href: ko.observable('http://www.ellucian.edu/step3') },
                    { text: ko.observable('Step 4'), href: ko.observable('http://www.ellucian.edu/step4') },
                    { text: ko.observable('Step 5') }
                ]),
                activeStep: ko.observable(3)
            };
            viewModel = new StepProgressViewModel(params);
        });

        it("error thrown if params.steps is undefined", function () {
            params = { activeStep: ko.observable(3) };
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("error thrown if params.steps is not a function or object", function () {
            params = { steps: 'invalid', activeStep: ko.observable(3) };
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("error thrown if params.activeStep is undefined", function () {
            params = {
                steps: ko.observableArray([
                        { text: ko.observable('Step 1'), href: ko.observable('http://www.ellucian.edu/step1') },
                        { text: ko.observable('Step 2'), href: ko.observable('http://www.ellucian.edu/step2') },
                        { text: ko.observable('Step 3'), href: ko.observable('http://www.ellucian.edu/step3') },
                        { text: ko.observable('Step 4'), href: ko.observable('http://www.ellucian.edu/step4') },
                        { text: ko.observable('Step 5'), href: ko.observable('http://www.ellucian.edu/step5') }
                ])
            };
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("error thrown if params.activeStep is not a function or number", function () {
            params.activeStep = 'invalid';
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("error thrown if params.activeStep is observable and too big", function () {
            params.activeStep = ko.observable(params.steps().length);
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("error thrown if params.activeStep is observable and too small", function () {
            params.activeStep = ko.observable(-1);
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("error thrown if params.activeStep is a number and too big", function () {
            params.activeStep = params.steps().length;
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("error thrown if params.activeStep is a number and too small", function () {
            params.activeStep = -1;
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("activeStep set to params.activeStep value when params.activeStep is observable", function () {
            expect(viewModel.activeStep()).toEqual(params.activeStep());
        });

        it("activeStep set to params.activeStep value when params.activeStep is not observable", function () {
            params.activeStep = 3;
            viewModel = new StepProgressViewModel(params);
            expect(viewModel.activeStep()).toEqual(params.activeStep);
        });

        it("error thrown if any step.text is undefined", function () {
            params.steps().push({ href: ko.observable('http://www.ellucian.edu/step6') });
            expect(function () { StepProgressViewModel(params); }).toThrow();
        });

        it("step.text set correctly for each step", function () {
            viewModel = new StepProgressViewModel(params);
            expect(viewModel.steps()[0].text()).toEqual(params.steps()[0].text());
            expect(viewModel.steps()[1].text()).toEqual(params.steps()[1].text());
            expect(viewModel.steps()[2].text()).toEqual(params.steps()[2].text());
            expect(viewModel.steps()[3].text()).toEqual(params.steps()[3].text());
            expect(viewModel.steps()[4].text()).toEqual(params.steps()[4].text());
        });

        it("step.href set correctly for each step", function () {
            viewModel = new StepProgressViewModel(params);
            expect(viewModel.steps()[0].href()).toEqual(params.steps()[0].href());
            expect(viewModel.steps()[1].href()).toEqual(params.steps()[1].href());
            expect(viewModel.steps()[2].href()).toEqual(params.steps()[2].href());
            expect(viewModel.steps()[3].href()).toEqual(params.steps()[3].href());
            expect(viewModel.steps()[4].href()).toEqual('#');
        });

        it("step.classes set correctly for each step", function () {
            viewModel = new StepProgressViewModel(params);
            expect(viewModel.steps()[0].classes()).toEqual('esg-step-progress__item esg-is-previous');
            expect(viewModel.steps()[1].classes()).toEqual('esg-step-progress__item esg-is-previous');
            expect(viewModel.steps()[2].classes()).toEqual('esg-step-progress__item esg-is-previous');
            expect(viewModel.steps()[3].classes()).toEqual('esg-step-progress__item esg-is-active');
            expect(viewModel.steps()[4].classes()).toEqual('esg-step-progress__item');
        });

        it("step.dispose calls statusLabelClasses.dispose when called", function () {
            spyOn(viewModel.steps()[0].classes, "dispose");
            spyOn(viewModel.steps()[0].href, "dispose");
            viewModel.steps()[0].dispose();
            expect(viewModel.steps()[0].classes.dispose).toHaveBeenCalled();
            expect(viewModel.steps()[0].href.dispose).toHaveBeenCalled();
        });
    });
});