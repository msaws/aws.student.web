﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/ComplexTable/_ComplexTable'],

function (component, events) {

    describe("Complex Table Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var ComplexTableViewModel = component.viewModel;
        var params = {};
        var viewModel,
            memory;

        beforeAll(function () {
            memory = window.localStorage ||
                         (window.UserDataStorage && new UserDataStorage()) ||
                         new CookieStorage();
        });

        beforeEach(function () {
            params = {
                rows: ko.observableArray([
                {
                    isCollapsibleRow: true,
                    isRowVisible: true,
                    rowCells:
                        {
                            Description: "ACT Composite",
                            DateTaken: "3/19/2016",
                            Percentile: null,
                            Score: "36",
                            Status: "Accepted"
                        }
                },
                {
                    isCollapsibleRow: false,
                    isRowVisible: false,
                    rowCells:
                        {
                            Description: "ACT English",
                            DateTaken: "3/19/2016",
                            Percentile: null,
                            Score: "20",
                            Status: "Accepted"
                        }
                },
                {
                    isCollapsibleRow: false,
                    isRowVisible: false,
                    rowCells:
                        {
                            Description: "ACT MATH",
                            DateTaken: "3/19/2016",
                            Percentile: null,
                            Score: "36",
                            Status: "Accepted"
                        }
                },
                {
                    isCollapsibleRow: false,
                    isRowVisible: false,
                    rowCells:
                    {
                        Description: "ACT Reading",
                        DateTaken: "3/19/2016",
                        Percentile: null,
                        Score: "36 out of 40",
                        Status: "Accepted"
                    }
                },
                {
                    isCollapsibleRow: false,
                    isRowVisible: false,
                    rowCells:
                    {
                        Description: "ACT Science",
                        DateTaken: "3/19/2016",
                        Percentile: null,
                        Score: "20",
                        Status: "Accepted"
                    }
                },
                {
                    isCollapsibleRow: false,
                    isRowVisible: false,
                    rowCells:
                    {
                        Description: "ACT Writing",
                        DateTaken: "3/19/2016",
                        Percentile: null,
                        Score: "30 out of 40",
                        Status: "Notational Only"
                    }
                }
                ]),
                columnProperties: ko.observableArray([
                {
                    headerText: 'Test',
                    headerId: 'Description',
                    order: 0,
                    isVisible: true,
                    templateName: null
                },
                {
                    headerText: 'Date Taken',
                    headerId: 'DateTaken',
                    order: 1,
                    isVisible: true,
                    templateName: null
                },
                {
                    headerText: 'Percentile',
                    headerId: 'Percentile',
                    order: 2,
                    isVisible: true,
                    templateName: null
                },
                {
                    headerText: 'Score',
                    headerId: 'Score',
                    order: 3,
                    isVisible: true,
                    templateName: null
                },
                {
                    headerText: 'Status',
                    headerId: 'Status',
                    order: 4,
                    isVisible: true,
                    templateName: 'icon-template' 
                }
                ]),
                tableCaption: ko.observable('Admissions Tests'),
                tableId: ko.observable('test-scores')
            };
            viewModel = new ComplexTableViewModel(params);
        });

        describe("rows parameter", function () {
            it("error thrown if params.rows is undefined", function () {
                params = {
                    columnProperties: ko.observableArray([]),
                    tableCaption: 'Admissions Tests',
                    tableId: 'test-scores'
                };

                try {
                    viewModel = new ComplexTableViewModel(params);
                }
                catch (error) {
                    expect(error).toBeDefined();

                }
            });

            it("set to params.rows value when params.rows is observableArray", function () {
                expect(viewModel.rows()).toEqual(params.rows());
            });

            it("set to params.rows value when params.rows is not observableArray", function () {
                params = {
                    rows: [],
                    columnProperties: ko.observableArray([]),
                    tableCaption: 'Admissions Tests',
                    tableId: 'test-scores'
                };
                viewModel = new ComplexTableViewModel(params);
                expect(viewModel.rows()).toEqual(params.rows);
            }); 
        });

        describe("columnProperties parameter", function () {
            it("error thrown if params.columnProperties is undefined", function () {
                params = {
                    rows: ko.observableArray([]),
                    tableCaption: 'Admissions Tests',
                    tableId: 'test-scores'
                };
                try {
                    viewModel = new ComplexTableViewModel(params);
                }
                catch (error) {
                    expect(error).toBeDefined();

                }
            });

            it("set to params.columnProperties values when params.columnProperties is observableArray", function () {
                expect(viewModel.columnProperties()[0].headerText()).toEqual(params.columnProperties()[0].headerText);
                expect(viewModel.columnProperties()[0].headerId()).toEqual(params.columnProperties()[0].headerId);
                expect(viewModel.columnProperties()[0].isVisible()).toEqual(params.columnProperties()[0].isVisible);
                expect(viewModel.columnProperties()[0].order()).toEqual(params.columnProperties()[0].order);
                expect(viewModel.columnProperties()[0].templateName()).toEqual(params.columnProperties()[0].templateName);
            });
        });

        describe("tableId parameter", function () {
            it("error thrown if params.tableId is undefined", function () {
                params = {
                    rows: ko.observableArray([]),
                    columnProperties: ko.observableArray([]),
                    tableCaption: 'Admissions Tests'
                };

                try {
                    viewModel = new ComplexTableViewModel(params);
                }
                catch (error) {
                    expect(error).toBeDefined();

                }
            });

            it("set to params.tableId value when params.tableId is observableArray", function () {
                expect(viewModel.tableId()).toEqual(params.tableId());
            });

            it("set to params.tableId value when params.tableId is not observableArray", function () {
                params = {
                    rows: [],
                    columnProperties: ko.observableArray([]),
                    tableCaption: 'Admissions Tests',
                    tableId: 'test-scores'
                };
                viewModel = new ComplexTableViewModel(params);
                expect(viewModel.tableId()).toEqual(params.tableId);
            });
        });

        describe("save column properties", function () {

            it("column properties set successfully after clicking saveColumnProperties", function () {
                spyOn(memory, "setItem");
                spyOn($.fn, "notificationCenter");

                viewModel.saveColumnProperties();
                expect(memory.setItem).toHaveBeenCalled();
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.SharedComponents.ComplexTable.ComplexTableSaveSuccess, type: "success", flash: true });
            });
        });

        describe("load column properties", function () {

            it("set params to columns from memory if memory item exists", function () {
                var memoryToSet = ko.toJSON(viewModel.columnProperties());
                memory.setItem(viewModel.tableId(), memoryToSet);
                viewModel = new ComplexTableViewModel(params);
                expect(viewModel.columnPropertiesFromMemory).toEqual(JSON.parse(memoryToSet));
                expect(params.columnProperties).toEqual(viewModel.columnPropertiesFromMemory);
            });
        });

        describe("reset column properties", function () {

            it("column properties reset successfully after clicking resetColumnProperties", function () {
                spyOn($.fn, "notificationCenter");

                viewModel.resetColumnProperties();
                for (i = 0; i < viewModel.columnProperties().length; i++) {
                    expect(viewModel.columnProperties()[i].headerId()).toEqual(viewModel.columnPropertiesDefault()[i].headerId());
                    expect(viewModel.columnProperties()[i].headerText()).toEqual(viewModel.columnPropertiesDefault()[i].headerText());
                    expect(viewModel.columnProperties()[i].isVisible()).toEqual(viewModel.columnPropertiesDefault()[i].isVisible());
                    expect(viewModel.columnProperties()[i].order()).toEqual(viewModel.columnPropertiesDefault()[i].order());
                    expect(viewModel.columnProperties()[i].templateName()).toEqual(viewModel.columnPropertiesDefault()[i].templateName());
                }

                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.SharedComponents.ComplexTable.ComplexTableResetSuccess, type: "success", flash: true });
            });
        });

        describe("dropdown toggle", function () {
            it("dropdown is inactive when page loads", function () {
                expect(viewModel.dropdownIsActive()).toBeFalsy();
            });

            it("dropdown is active when toggled", function () {
                viewModel.toggleDropdown(viewModel);
                expect(viewModel.dropdownIsActive()).toBeTruthy();
            });
        });

        describe("move column order", function () {

            it("moveColumnDown swaps order with next item", function () {
                viewModel.moveColumnDown(viewModel.columnProperties()[1], 'click');
                expect(viewModel.columnToMoveDown()).toEqual(viewModel.columnProperties()[1].order());
            });

            it("moveColumnUp swaps order with previous item", function () {
                viewModel.moveColumnUp(viewModel.columnProperties()[1], 'click');
                expect(viewModel.columnToMoveUp()).toEqual(viewModel.columnProperties()[1].order());
            });

        });

        describe("first visible column", function () {
            it("first visible column returns true when a visible column is found", function () {
                viewModel.firstVisibleColumn();
                expect(viewModel.firstVisibleColumn()).toBeTruthy();
            });

            it("first visible column returns false when there are no visible columns", function () {
                params = {
                    rows: ko.observableArray([]),
                    columnProperties: ko.observableArray([]),
                    tableCaption: 'Admissions Tests',
                    tableId: 'test-scores'
                };

                viewModel = new ComplexTableViewModel(params);
                viewModel.firstVisibleColumn();
                expect(viewModel.firstVisibleColumn()).toBeFalsy();
            });
        });
    });
});