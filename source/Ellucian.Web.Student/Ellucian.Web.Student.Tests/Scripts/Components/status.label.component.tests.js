﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/StatusLabel/_StatusLabel'],

function (component, events) {

    describe("StatusLabel Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var StatusLabelViewModel = component.viewModel;
        var params = {};
        var viewModel;
        beforeEach(function () {
            params = { text: ko.observable('text'), type: ko.observable('draft') };
            viewModel = new StatusLabelViewModel(params);
        });

        it("error thrown if params.text is undefined", function () {
            params = { type: ko.observable('draft') };
            expect(function () { StatusLabelViewModel(params); }).toThrow();
        });

        it("text set to params.text value when params.text is observable", function () {
            expect(viewModel.text()).toEqual(params.text());
        });

        it("text set to params.text value when params.text is not observable", function () {
            params = { text: 'text', type: ko.observable('draft') };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.text()).toEqual(params.text);
        });

        it("type set to null when params.type is undefined", function () {
            params = { text: 'text' };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.type()).toEqual(null);
        });

        it("type set to params.type value when params.type is observable", function () {
            expect(viewModel.type()).toEqual(params.type());
        });

        it("type set to params.type value when params.type is not observable", function () {
            params = { text: 'text', type: 'draft' };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.type()).toEqual(params.type);
        });

        it("statusLabelClasses set to 'esg-label' when params.type is not a valid type", function () {
            params = { text: 'text' };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.statusLabelClasses()).toEqual('esg-label');
        });

        it("statusLabelClasses set to 'esg-label esg-label--error' when params.type is 'error'", function () {
            params = { text: 'text', type: 'error' };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.statusLabelClasses()).toEqual('esg-label esg-label--error');
        });

        it("statusLabelClasses set to 'esg-label esg-label--success' when params.type is 'success'", function () {
            params = { text: 'text', type: 'success' };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.statusLabelClasses()).toEqual('esg-label esg-label--success');
        });

        it("statusLabelClasses set to 'esg-label esg-label--pending' when params.type is 'pending'", function () {
            params = { text: 'text', type: 'pending' };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.statusLabelClasses()).toEqual('esg-label esg-label--pending');
        });

        it("statusLabelClasses set to 'esg-label esg-label--draft' when params.type is 'draft'", function () {
            params = { text: 'text', type: 'draft' };
            viewModel = new StatusLabelViewModel(params);
            expect(viewModel.statusLabelClasses()).toEqual('esg-label esg-label--draft');
        });

        it("dispose calls statusLabelClasses.dispose when called", function () {
            spyOn(viewModel.statusLabelClasses, "dispose");
            viewModel.dispose();
            expect(viewModel.statusLabelClasses.dispose).toHaveBeenCalled();
        });
    });
});