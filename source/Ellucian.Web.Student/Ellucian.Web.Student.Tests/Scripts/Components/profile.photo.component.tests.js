﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/ProfilePhoto/_ProfilePhoto'],

function (component, events) {

    describe("ProfilePhoto Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var ProfilePhotoViewModel = component.viewModel;
        var params = {};
        var viewModel;
        beforeEach(function () {
            params = {
                id: ko.observable("0001234"),
                altText: ko.observable("Photo of 0001234"),
                size: ko.observable("small"),
                usedInTable: ko.observable(true)
            };
            viewModel = new ProfilePhotoViewModel(params);
        });

        describe("id property: ", function () {

            it("error thrown if params.id is undefined", function () {
                params = {
                    altText: ko.observable("Photo of 0001234"),
                    size: ko.observable("small"),
                    usedInTable: ko.observable(true)
                };
                expect(function () { ProfilePhotoViewModel(params); }).toThrow();
            });

            it("set to params.id value when params.id is observable", function () {
                expect(viewModel.id()).toEqual(params.id());
            });

            it("set to params.id value when params.id is not observable", function () {
                params = {
                    id: "0001234",
                    altText: ko.observable("Photo of 0001234"),
                    size: ko.observable("small"),
                    usedInTable: ko.observable(true)
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.id()).toEqual(params.id);
            });
        });

        describe("altText property: ", function () {

            it("set to Ellucian.Site.Resources.PersonProfilePhotoDefaultAltText when params.altText is undefined", function () {
                params = {
                    id: ko.observable("0001234"),
                    size: ko.observable("small"),
                    usedInTable: ko.observable(true)
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.altText()).toEqual(Ellucian.Site.Resources.PersonProfilePhotoDefaultAltText);
            });

            it("set to params.altText value when params.altText is observable", function () {
                expect(viewModel.altText()).toEqual(params.altText());
            });

            it("set to params.altText value when params.altText is not observable", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: "Photo of 0001234",
                    size: ko.observable("small"),
                    usedInTable: ko.observable(true)
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.altText()).toEqual(params.altText);
            });
        });

        describe("usedInTable property: ", function () {

            it("set to false when params.usedInTable is undefined", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    size: ko.observable("small"),
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.usedInTable()).toBeFalsy();
            });

            it("set to params.usedInTable value when params.usedInTable is observable", function () {
                expect(viewModel.usedInTable()).toEqual(params.usedInTable());
            });

            it("set to params.usedInTable value when params.usedInTable is not observable", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    size: ko.observable("small"),
                    usedInTable: true
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.usedInTable()).toEqual(params.usedInTable);
            });
        });

        describe("classes property: ", function () {

            it("set to esg-avatar esg-avatar--medium when params.size is undefined", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    usedInTable: ko.observable(false)

                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.classes()).toEqual("esg-avatar esg-avatar--medium");
            });

            it("set to esg-avatar esg-avatar--medium esg-avatar--vertical-align-middle when params.usedInTable is true", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    usedInTable: ko.observable(true)
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.classes()).toEqual("esg-avatar esg-avatar--medium esg-avatar--vertical-align-middle");
            });

            it("set to esg-avatar esg-avatar--small when params.size is small", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    usedInTable: ko.observable(false),
                    size: ko.observable("small"),
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.classes()).toEqual("esg-avatar esg-avatar--small");
            });

            it("set to esg-avatar esg-avatar--medium when params.size is medium", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    usedInTable: ko.observable(false),
                    size: ko.observable("medium"),
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.classes()).toEqual("esg-avatar esg-avatar--medium");
            });

            it("set to esg-avatar esg-avatar--large when params.size is large", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    usedInTable: ko.observable(false),
                    size: ko.observable("large"),
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.classes()).toEqual("esg-avatar esg-avatar--large");
            });


            it("set to esg-avatar esg-avatar--medium when params.size is not small, medium, or large", function () {
                params = {
                    id: ko.observable("0001234"),
                    altText: ko.observable("Photo of 0001234"),
                    usedInTable: ko.observable(false),
                    size: ko.observable("extra-large"),
                };
                viewModel = new ProfilePhotoViewModel(params);
                expect(viewModel.classes()).toEqual("esg-avatar esg-avatar--medium");
            });
        });

        describe("source property: ", function () {
             
            it("set to userPhotoBaseUrl + self.id", function () {
                expect(viewModel.source()).toEqual(userPhotoBaseUrl + viewModel.id());
            });

        });

        describe("dispose method: ", function () {

            it("calls classes.dispose and source.dispose when called", function () {
                spyOn(viewModel.classes, "dispose");
                spyOn(viewModel.source, "dispose");
                viewModel.dispose();
                expect(viewModel.classes.dispose).toHaveBeenCalled();
                expect(viewModel.source.dispose).toHaveBeenCalled();
            });
        });
    });
});