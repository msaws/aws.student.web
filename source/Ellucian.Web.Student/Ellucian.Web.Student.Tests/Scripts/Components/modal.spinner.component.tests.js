﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
define(['Ellucian.Web.Student/Scripts/Components/ModalSpinner/_ModalSpinner'],

function (component, events) {

    describe("ModalSpinner Component: ", function () {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

        var ModalSpinnerViewModel = component.viewModel;
        var params = {};
        var viewModel;
        beforeEach(function () {
            params = {
                isVisible: ko.observable(true),
                message: ko.observable("Modal Spinner Message...")
            };
            viewModel = new ModalSpinnerViewModel(params);
        });

        describe("isVisible property ", function () {

            it("error logged to console if params.isVisible is undefined", function () {
                params = {
                    message: ko.observable("Modal Spinner Message...")
                };
                spyOn(console, 'log').and.callThrough();
                viewModel = new ModalSpinnerViewModel(params);
                expect(console.log).toHaveBeenCalledWith("Please provide a valid isVisible parameter.");
            });

            it("set to params.isVisible value when params.isVisible is observable", function () {
                expect(viewModel.isVisible()).toEqual(params.isVisible());
            });

            it("set to params.isVisible value when params.isVisible is not observable", function () {
                params = {
                    isVisible: true,
                    message: ko.observable("Modal Spinner Message...")
                };
                viewModel = new ModalSpinnerViewModel(params);
                expect(viewModel.isVisible()).toEqual(params.isVisible);
            });
        });

        describe("message property ", function () {

            it("set to Ellucian.Site.Resources.SpinnerDefaultMessage when params.message is undefined", function () {
                params = {
                    isVisible: ko.observable(true)
                };
                viewModel = new ModalSpinnerViewModel(params);
                expect(viewModel.message()).toEqual(Ellucian.Site.Resources.SpinnerDefaultMessage);
            });

            it("set to params.message value when params.message is observable", function () {
                expect(viewModel.message()).toEqual(params.message());
            });

            it("set to params.message value when params.message is not observable", function () {
                params = {
                    isVisible: ko.observable(true),
                    message: "Modal Spinner Message..."
                };
                viewModel = new ModalSpinnerViewModel(params);

                expect(viewModel.message()).toEqual(params.message);
            });
        });
    });
});