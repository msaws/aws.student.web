﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("search.view.model", function () {
    beforeEach(function () {
        viewModel = new searchViewModel();
    });

    it(":showUI defaults to false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading defaults to true after initialization", function () {
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":calls BaseViewModel when called", function () {
        spyOn(BaseViewModel, "call");
        viewModel = new searchViewModel();

        expect(BaseViewModel.call).toHaveBeenCalled();
    });

    it(":changeToDesktop does nothing ", function () {
        spyOn(viewModel, 'executeSearch');

        viewModel.changeToDesktop();
        expect(viewModel.executeSearch).not.toHaveBeenCalled();
    });

    it(":changeToMobile does nothing ", function () {
        spyOn(viewModel, 'executeSearch');

        viewModel.changeToMobile();
        expect(viewModel.executeSearch).not.toHaveBeenCalled();
    });

    it(":searching defaults to false after initialization", function () {
        expect(viewModel.searching()).toBeFalsy();
    });

    it(":SearchPrompt is undefined after initialization", function () {
        expect(viewModel.SearchPrompt()).toBe(undefined);
    });

    it(":PlaceholderText is undefined after initialization", function () {
        expect(viewModel.PlaceholderText()).toBe(undefined);
    });

    it(":BeforeSearchText is undefined after initialization", function () {
        expect(viewModel.BeforeSearchText()).toBe(undefined);
    });

    it(":ErrorOccurred is undefined after initialization", function () {
        expect(viewModel.ErrorOccurred()).toBe(undefined);
    });

    it(":ErrorMessage is undefined after initialization", function () {
        expect(viewModel.ErrorMessage()).toBe(undefined);
    });

    it(":SearchFieldLabel is undefined after initialization", function () {
        expect(viewModel.SearchFieldLabel()).toBe(undefined);
    });

    it(":SearchSubmitLabel is undefined after initialization", function () {
        expect(viewModel.SearchSubmitLabel()).toBe(undefined);
    });

    it(":Search is undefined after initialization", function () {
        expect(viewModel.Search()).toBe(undefined);
    });

    it(":Select is undefined after initialization", function () {
        expect(viewModel.Select()).toBe(undefined);
    });

    it(":PointOfOriginControllerName is undefined after initialization", function () {
        expect(viewModel.PointOfOriginControllerName()).toBe(undefined);
    });

    it(":DynamicBackLinkDefault is undefined after initialization", function () {
        expect(viewModel.DynamicBackLinkDefault()).toBe(undefined);
    });

    it(":BackLinkExclusions is an empty array after initialization", function () {
        expect(viewModel.BackLinkExclusions().length).toBe(0);
    });

    it(":SearchString is undefined after initialization", function () {
        expect(viewModel.SearchString()).toBe(undefined);
    });

    it(":SearchResultsViewName is undefined after initialization", function () {
        expect(viewModel.SearchResultsViewName()).toBe(undefined);
    });

    it(":SearchResults is an empty array after initialization", function () {
        expect(viewModel.SearchResults().length).toBe(0);
    });

    it(":SpinnerAlternateText is undefined after initialization", function () {
        expect(viewModel.SpinnerAlternateText()).toBe(undefined);
    });

    it(":SpinnerText is undefined after initialization", function () {
        expect(viewModel.SpinnerText()).toBe(undefined);
    });

    it(":searchResultsCount is (0) when SearchResults is null", function () {
        viewModel.SearchResults(null);
        expect(viewModel.searchResultsCount()).toBe("(0)");
    });

    it(":searchResultsCount is (0) when SearchResults is empty", function () {
        expect(viewModel.searchResultsCount()).toBe("(0)");
    });

    it(":searchResultsCount is (0) when SearchResults is not empty", function () {
        viewModel.SearchResults([{"Id":ko.observable("12345")}]);
        expect(viewModel.searchResultsCount()).toBe("(1)");
    });

    it(":searchHasOccurred defaults to false after initialization", function () {
        expect(viewModel.searchHasOccurred()).toBeFalsy();
    });

    it(":executeSearch throws an alert when called", function () {
        var global = jasmine.getGlobal();
        spyOn(global, "alert").and.callThrough();

        viewModel.executeSearch();
        expect(global.alert).toHaveBeenCalledWith("Executing search (override me)...");
    });

    it(":triggerSearch sets ErrorOccurred to false, Error Message to call, and calls execute search and sets searchHasOccurred to true when called", function () {
        spyOn(viewModel, "executeSearch");

        viewModel.triggerSearch();
        expect(viewModel.ErrorOccurred()).toBeFalsy();
        expect(viewModel.ErrorMessage()).toBe(null);
        expect(viewModel.executeSearch).toHaveBeenCalled();
        expect(viewModel.searchHasOccurred()).toBeTruthy();
    });

    it(":resetSearch sets SearchString and SearchResults to null, searchHasOccurred to false when called", function () {
        viewModel.resetSearch();

        expect(viewModel.SearchString()).toBe(null);
        expect(viewModel.SearchResults()).toBe(null);
        expect(viewModel.searchHasOccurred()).toBeFalsy();
    });
});