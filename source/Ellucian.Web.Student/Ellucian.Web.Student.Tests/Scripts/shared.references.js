﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
personProxyActionThrobberMessage = "Loading...";

personProxyEditProxyAccessThrobberMessage = "Loading..."

personProxySearchingThrobberMessage = 'Searching...';

personProxyNoActiveProxiesMessage = "No active proxies.";
personProxyNoActiveProxiesAddAProxyMessage = "No proxies to add.";

personProxyUnableToLoadMessage = "Unable to load.";
personProxySuccessfulUpdateMessage = "Success. {0}";
personProxyFailedUpdateMessage = "Failure. {0}";
personProxyUnableToUpdateMessage = "Unable to update.";

personProxyUnableToEditMessage = "Unable to edit access.";
personProxyAddProxySelectedMessage = "{0} successfully added.";

reauthorizationProcessedMessage = "Reauthorization success.";
reauthorizationFailureMessage = "Reauthorization failed.";

reviewDisclosureAgreementButtonLabel = 'Review Disclosure Agreeement';
reauthorizeAccessButtonLabel = 'Reauthorize Access';

personProxyActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/GetPersonProxyModel";
updatePersonProxyInformationActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/UpdatePersonProxyInformation";
getProxyInfoForEditActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/GetProxyInformationForEdit";
searchForMatchesActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/QueryMatchingPersonsAsync";
processSearchResultsActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/ProcessResultsAsync";
reauthorizeAccessActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/ReauthorizeAccessAsync";

personProxyPhotoTitleText = "Photo of {0}.";
personProxyEditAccessButtonText = "Update access for {0}.";

requiredFieldMessageString = "Required";
maximumLengthMessageString = "Too long";
minimumLengthMessageString = "Too short";
invalidEmailAddressMessageString = "Invalid Email";

proxySearchNoResultsMessage = "No matches found.";
proxyAccessLevelLegend = "Set proxy access for {0} to ";
personProxyLoadingThrobberMessage = "Loading...";
personProxyLoadingThrobberAltText = "Alt Loading";
personProxyChangingThrobberMessage = "Changing...";
personProxyChangingThrobberAltText = "Alt Changing";
verifyingPasswordMessage = 'Verifying password...';
verifyingPasswordAltText = 'Alt Verifying';
loadingReauthorizationSpinnerMessage = "Loading...";
proxyRequestSuccessfulMessage = "Success!";

getProxySubjectsActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/GetProxySubjectsAsync";
setProxySubjectActionUrl = "http://www.ellucianuniversity.edu/Student/PersonProxy/SetProxySubjectAsync";
homeUrl = "http://www.ellucianuniversity.edu/Student";
proxySelectionUnableToLoadSubjectsMessage = "Unable to load proxy subjects.";
proxySelectionUserSelectedMessage = "{0} selected.";

getResourceUrl = "http://www.ellucianuniversity.edu/Student/Resources/GetResources";

// Manage scope of string constants
var Ellucian = Ellucian || {};
Ellucian.Proxy = Ellucian.Proxy || {};
Ellucian.Proxy.ButtonLabels = {
    editProxyAccessDialogSaveButtonLabel : '@Resources.PersonProxyResources.EditProxyAccessDialogSaveButtonLabel',
    editProxyAccessDialogCancelButtonLabel : '@Resources.PersonProxyResources.EditProxyAccessDialogCancelButtonLabel',
    reauthorizeConfirmButtonLabel : '@Resources.PersonProxyResources.ReauthorizeConfirmButtonLabel',
    confirmSelectedPersonDialogButtonLabel : '@Resources.PersonProxyResources.ConfirmSelectedPersonDialogButtonLabel',
    saveProxySelectionDialogButtonLabel : '@Resources.PersonProxyResources.SaveProxySelectionDialogButtonLabel',
    cancelProxySelectionDialogButtonLabel : '@Resources.PersonProxyResources.CancelProxySelectionDialogButtonLabel'
};

Ellucian.SharedComponents = Ellucian.SharedComponents || {};
Ellucian.SharedComponents.ButtonLabels = {
    buttonTextCancel: '@Resources.SiteResources.ButtonTextCancel',
    buttonTextClose: '@Resources.SiteResources.ButtonTextClose',
    buttonTextAccept: '@Resources.SiteResources.ButtonTextAccept',
    buttonTextVerifyPassword: '@Resources.SiteResources.VerifyPasswordButtonLabel'
};