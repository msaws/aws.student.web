﻿describe("Ellucian.Storage", function () {
    describe("session", function () {
        beforeEach(function () {
            if ('sessionStorage' in window && typeof window.sessionStorage !== "undefined") {
                sessionStorage.clear();
                storage = Ellucian.Storage.session;
            }
        });

        describe("Setting",
            function () {

                it(":sets primitives",
                    function () {
                        var number = 3;
                        var string = "3 is three";

                        storage.setItem("test1", number);
                        storage.setItem("test2", string)

                        expect(storage.getItem("test1")).toEqual(number);
                        expect(storage.getItem("test2")).toEqual(string);
                    });

                it(":sets object",
                    function () {
                        var data = { number: 3 };
                        storage.setItem("test", data);

                        expect(storage.getItem("test")).toEqual(data);
                    });

                it(":sets array",
                    function () {
                        var data = [1, 2, 3];
                        storage.setItem("test", data);

                        expect(storage.getItem("test")).toEqual(data);
                    });

                it(":handles QUOTA_EXCEEDED_ERR",
                    function () {

                        // Add a small value to storage
                        var smallString = "a";
                        storage.setItem("test", smallString);

                        var exception;
                        var repeat = function (str, x) { return new Array(x + 1).join(str); };
                        var largeString = repeat("x", 12 * 1024 * 1024 / 2); // each JS character is 2 bytes

                        // Writing this value might fail* 
                        // but we expect the storage object to fail gracefully
                        // * we can't really be sure, as storage space varies by browser
                        storage.setItem("test", largeString);

                        // Get the value - storage should return the previously valid value.
                        var result = storage.getItem("test");

                        // The value could be the old value, or the new large value, depending on browser
                        result = result === smallString || result === largeString;

                        expect(result).toEqual(true);

                    });
            });

        describe("Getting",
            function () {
                it(":gets primitives",
                    function () {
                        var number = 3;
                        var string = "3 is three";

                        storage.setItem("test1", number);
                        storage.setItem("test2", string)

                        expect(storage.getItem("test1")).toEqual(number);
                        expect(storage.getItem("test2")).toEqual(string);
                    });

                it(":gets object",
                    function () {
                        var data = { number: 3, character: 'a' };
                        storage.setItem("test", data);

                        expect(storage.getItem("test")).toEqual(jasmine.any(Object));
                        expect(storage.getItem("test").number).toEqual(data.number);
                        expect(storage.getItem("test").character).toEqual(data.character);
                    });

                it(":gets array",
                    function () {
                        var data = [1, '2', 3];
                        storage.setItem("test", data);

                        expect(storage.getItem("test")).toEqual(jasmine.any(Array));
                        expect(storage.getItem("test")[0]).toEqual(data[0]);
                        expect(storage.getItem("test")[1]).toEqual(data[1]);
                        expect(storage.getItem("test")[2]).toEqual(data[2]);
                    });
            });

        describe("Removing",
            function () {
                it(":removes item",
                    function () {
                        var data = 3;

                        storage.setItem("test", data);
                        expect(storage.getItem("test")).toEqual(data);

                        storage.removeItem("test");
                        expect(storage.getItem("test")).toEqual(null);
                    });
            });

        describe("Clearing",
            function () {

                it(":clears",
                    function () {
                        var data1 = 3;
                        var data2 = "test";

                        storage.setItem("test1", data1);
                        expect(storage.getItem("test1")).toEqual(data1);

                        storage.setItem("test2", data2);
                        expect(storage.getItem("test2")).toEqual(data2);

                        storage.clear();
                        expect(storage.getItem("test1")).toEqual(null);
                        expect(storage.getItem("test2")).toEqual(null);

                    });
            });
    });

    describe("local", function () {
        beforeEach(function () {
            localStorage.clear();

            storage = Ellucian.Storage.local;
        });

        describe("Setting", function () {

            it(":sets primitives", function () {
                var number = 3;
                var string = "3 is three";

                storage.setItem("test1", number);
                storage.setItem("test2", string)

                expect(storage.getItem("test1")).toEqual(number);
                expect(storage.getItem("test2")).toEqual(string);
            });

            it(":sets object", function () {
                var data = { number: 3 };
                storage.setItem("test", data);

                expect(storage.getItem("test")).toEqual(data);
            });

            it(":sets array", function () {
                var data = [1, 2, 3];
                storage.setItem("test", data);

                expect(storage.getItem("test")).toEqual(data);
            });

            it(":handles QUOTA_EXCEEDED_ERR", function () {

                // Add a small value to storage
                var smallString = "a";
                storage.setItem("test", smallString);

                var exception;
                var repeat = function (str, x) { return new Array(x + 1).join(str); };
                var largeString = repeat("x", 12 * 1024 * 1024 / 2); // each JS character is 2 bytes

                // Writing this value might fail* 
                // but we expect the storage object to fail gracefully
                // * we can't really be sure, as storage space varies by browser
                storage.setItem("test", largeString);

                // Get the value - storage should return the previously valid value.
                var result = storage.getItem("test");

                // The value could be the old value, or the new large value, depending on browser
                result = result === smallString || result === largeString;

                expect(result).toEqual(true);

            });
        });

        describe("Getting", function () {
            it(":gets primitives", function () {
                var number = 3;
                var string = "3 is three";

                storage.setItem("test1", number);
                storage.setItem("test2", string)

                expect(storage.getItem("test1")).toEqual(number);
                expect(storage.getItem("test2")).toEqual(string);
            });

            it(":gets object", function () {
                var data = { number: 3, character: 'a' };
                storage.setItem("test", data);

                expect(storage.getItem("test")).toEqual(jasmine.any(Object));
                expect(storage.getItem("test").number).toEqual(data.number);
                expect(storage.getItem("test").character).toEqual(data.character);
            });

            it(":gets array", function () {
                var data = [1, '2', 3];
                storage.setItem("test", data);

                expect(storage.getItem("test")).toEqual(jasmine.any(Array));
                expect(storage.getItem("test")[0]).toEqual(data[0]);
                expect(storage.getItem("test")[1]).toEqual(data[1]);
                expect(storage.getItem("test")[2]).toEqual(data[2]);
            });
        });

        describe("Removing", function () {
            it(":removes item", function () {
                var data = 3;

                storage.setItem("test", data);
                expect(storage.getItem("test")).toEqual(data);

                storage.removeItem("test");
                expect(storage.getItem("test")).toEqual(null);
            });
        });

        describe("Clearing", function () {

            it(":clears", function () {
                var data1 = 3;
                var data2 = "test";

                storage.setItem("test1", data1);
                expect(storage.getItem("test1")).toEqual(data1);

                storage.setItem("test2", data2);
                expect(storage.getItem("test2")).toEqual(data2);

                storage.clear();
                expect(storage.getItem("test1")).toEqual(null);
                expect(storage.getItem("test2")).toEqual(null);

            });
        });
    });
});