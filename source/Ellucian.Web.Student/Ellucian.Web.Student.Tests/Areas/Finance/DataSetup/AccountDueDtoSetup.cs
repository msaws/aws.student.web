﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;

namespace Ellucian.Web.Student.Tests.Areas.Finance.DataSetup
{
    // Build an AccountDue data structure named 'accountDue'
    public static class AccountDueDtoSetup
    {
        const string paymentGroup1 = "PaymentGroup1";
        const string paymentGroup2 = "PaymentGroup2";
        const string paymentGroup3 = "PaymentGroup3";

        public static AccountDue emptyAccountDue = new AccountDue() { AccountTerms = new List<AccountTerm>() };
        public static AccountDue accountDue = new AccountDue()
        {
            StartDate = DateTime.Today.AddDays(-50),
            EndDate = DateTime.Today.AddDays(50),
            AccountTerms = new List<AccountTerm>()
                {
                    new AccountTerm()
                    {
                        TermId = "Term1",
                        Description = "Term1 Description",
                        GeneralItems = new List<AccountsReceivableDueItem>()
                        {
                            // selected because is first payment group
                            new AccountsReceivableDueItem(){AmountDue= 3, DueDate = DateTime.Today, Distribution = paymentGroup1, AccountType = "01"},
                            // unselected because overdue and first payment group but is a credit
                            new AccountsReceivableDueItem(){AmountDue= -3, DueDate = DateTime.Today.AddDays(-1), Distribution = paymentGroup1, AccountType = "01"},
                        },
                        DepositDueItems = new List<DepositDue>()
                        {
                            // unselected because due but not first payment group
                            new DepositDue(){AmountDue = 3000, DueDate = DateTime.Today, Distribution = paymentGroup3, Id = "DepositDue1.1",},
                            // unselected because due but not first payment group
                            new DepositDue(){AmountDue = 3000, DueDate = DateTime.Today, Distribution = paymentGroup2, Id = "DepositDue1.2",},
                        },
                        InvoiceItems = new List<InvoiceDueItem>()
                        {
                            // unselected because due but not first payment group
                            new InvoiceDueItem(){AmountDue = 30,  Distribution = paymentGroup2, DueDate = DateTime.Today, InvoiceId = "Invoice2.1", AccountType = "01"},
                            // selected because first payment group
                            new InvoiceDueItem(){AmountDue = 30,  Distribution = paymentGroup1, DueDate = DateTime.Today.AddDays(1), InvoiceId = "Invoice2.2", AccountType = "01"},
                        },
                        PaymentPlanItems = new List<PaymentPlanDueItem>()
                        {
                            // unselected because first plan item but not first payment group
                            new PaymentPlanDueItem(){AmountDue = 300, Distribution = paymentGroup2, DueDate = DateTime.Today, PaymentPlanId = "PayPlan1.1", AccountType = "01"},
                            // selected because overdue and first payment group
                            new PaymentPlanDueItem(){AmountDue = 300, Distribution = paymentGroup1, DueDate= DateTime.Today.AddDays(-1), PaymentPlanId = "PayPlan1.2", AccountType = "01"},
                            // selected because first in plan and first payment group
                            new PaymentPlanDueItem(){AmountDue = 300, Distribution = paymentGroup1, DueDate= DateTime.Today.AddDays(1), PaymentPlanId = "PayPlan1.2", AccountType = "01"},
                        },
                    },
                    new AccountTerm()
                    {
                        TermId = "Term2",
                        Description = "Term2 Description",
                        GeneralItems = new List<AccountsReceivableDueItem>()
                        {
                            // unselected because wrong payment group
                            new AccountsReceivableDueItem(){AmountDue= 4, Distribution = paymentGroup2, DueDate = DateTime.Today, AccountType = "01"},
                            // selected because first payment group
                            new AccountsReceivableDueItem(){AmountDue=4, Distribution = paymentGroup1, DueDate = DateTime.Today, AccountType = "01" },
                        },
                        DepositDueItems = new List<DepositDue>()
                        {
                            // selected because first payment group
                            new DepositDue(){AmountDue = 4000, Distribution = paymentGroup1, DueDate = DateTime.Today, Id = "DepositDue2.1",},
                            // unselected because not first payment group
                            new DepositDue(){AmountDue = 4000, Distribution = paymentGroup3, DueDate = DateTime.Today, Id = "DepositDue2.2",},
                        },
                        InvoiceItems = new List<InvoiceDueItem>()
                        {
                            // unselected because not first payment group
                            new InvoiceDueItem(){AmountDue = 40, Distribution = paymentGroup3, DueDate = DateTime.Today, InvoiceId = "Invoice2.1", AccountType = "01"},
                            // unselected because first payment group but is a credit
                            new InvoiceDueItem(){AmountDue = -40, Distribution = paymentGroup1, DueDate = DateTime.Today, InvoiceId = "Invoice2.2", AccountType = "01"},
                        },
                        PaymentPlanItems = new List<PaymentPlanDueItem>()
                        {
                            // selected because overdue and correct payment group
                            new PaymentPlanDueItem(){AmountDue = 400, Distribution = paymentGroup1, DueDate=DateTime.Today.AddDays(-2), PaymentPlanId = "PayPlan2.1", AccountType = "01"},
                            // selected because overdue and correct payment group
                            new PaymentPlanDueItem(){AmountDue = 400, Distribution = paymentGroup1, DueDate=DateTime.Today.AddDays(-1), PaymentPlanId = "PayPlan2.1", AccountType = "01"},
                            // selected because first non-overdue item and correct payment group
                            new PaymentPlanDueItem(){AmountDue = 400, Distribution = paymentGroup1, DueDate=DateTime.Today.AddDays(21), PaymentPlanId = "PayPlan2.1", AccountType = "01"},
                            // unselected because correct payment group but not first non-overdue item
                            new PaymentPlanDueItem(){AmountDue = 400, Distribution = paymentGroup1, DueDate=DateTime.Today.AddDays(22), PaymentPlanId = "PayPlan2.1", AccountType = "01"},
                        },
                    },
                    new AccountTerm()
                    {
                        TermId = "Term3",
                        Description = "Term3 Description",
                        GeneralItems = new List<AccountsReceivableDueItem>()
                        {
                            // selected because first payment group
                            new AccountsReceivableDueItem(){AmountDue= 5, Distribution = paymentGroup1, DueDate = DateTime.Today, AccountType = "01" },
                            // unselected because not first payment group
                            new AccountsReceivableDueItem(){AmountDue=5, Distribution = paymentGroup3, DueDate = DateTime.Today, AccountType = "01" },
                        },
                        DepositDueItems = new List<DepositDue>()
                        {
                            // unselected because not first payment group
                            new DepositDue(){AmountDue = 5000, Distribution = paymentGroup2, DueDate = DateTime.Today, Id = "DepositDue3.1"},
                            // unselected because not first payment group
                            new DepositDue(){AmountDue = 5000, Distribution = paymentGroup2, DueDate = DateTime.Today, Id = "DepositDue3.2"},
                        },
                        InvoiceItems = new List<InvoiceDueItem>()
                        {
                            // selected because first payment group
                            new InvoiceDueItem(){AmountDue = 50, Distribution = paymentGroup1, DueDate = DateTime.Today, InvoiceId = "Invoice3.1", AccountType = "01"},
                            // unselected because not first payment group
                            new InvoiceDueItem(){AmountDue = 50, Distribution = paymentGroup2, DueDate = DateTime.Today, InvoiceId = "Invoice3.2", AccountType = "01"},
                        },
                        PaymentPlanItems = new List<PaymentPlanDueItem>()
                        {
                            // unselected because first in plan but wrong group
                            new PaymentPlanDueItem(){AmountDue = 500, Distribution = paymentGroup2, DueDate = DateTime.Today, PaymentPlanId = "PayPlan3.1", AccountType = "01"},
                            // selected because first in plan and first group
                            new PaymentPlanDueItem(){AmountDue = 500, Distribution = paymentGroup1, DueDate = DateTime.Today, PaymentPlanId = "PayPlan3.2", AccountType = "01"},
                        },
                    }
                }
        };

        public static AccountDue accountDueNullTerms = new AccountDue()
        {
            StartDate = DateTime.Today.AddDays(-50),
            EndDate = DateTime.Today.AddDays(50),
            AccountTerms = null,
            Balance = 0,
            PersonName = "John Smith"
        };

        public static AccountDue accountDueZeroBalance = new AccountDue()
        {
            StartDate = DateTime.Today.AddDays(-50),
            EndDate = DateTime.Today.AddDays(50),
            AccountTerms = new List<AccountTerm>()
            {
                new AccountTerm()
                {
                    Amount = 0m,
                    GeneralItems = new List<AccountsReceivableDueItem>()
                    {
                        new AccountsReceivableDueItem()
                        {
                            AmountDue = 0m,
                            AccountDescription = "Student Receivables",
                            AccountType = "01",
                            Description = "Charge",
                            Distribution = "BANK",
                            DueDate = DateTime.Today.AddDays(3),
                            Overdue = false,
                            Term = "2014/FA",
                            TermDescription = "2014 Fall Term"
                        }
                    }
                }
            },
            Balance = 0,
            PersonName = "John Smith"
        };

        public static AccountDue PastAccountDue = new AccountDue() { AccountTerms = accountDue.AccountTerms, StartDate = null, EndDate = DateTime.Today.AddDays(-31), PersonName = "Joe Smith" };
        public static AccountDue CurrentAccountDue = new AccountDue() { AccountTerms = accountDue.AccountTerms, StartDate = DateTime.Today.AddDays(-30), EndDate = DateTime.Today.AddDays(-30), PersonName = "Joe Smith" };
        public static AccountDue FutureAccountDue = new AccountDue() { AccountTerms = accountDue.AccountTerms, StartDate = DateTime.Today.AddDays(31), EndDate = null, PersonName = "Joe Smith" };
        public static AccountDue EmptyAccountDue = new AccountDue() { StartDate = DateTime.Today.AddDays(-30), EndDate = DateTime.Today.AddDays(-30), };
    }
}
