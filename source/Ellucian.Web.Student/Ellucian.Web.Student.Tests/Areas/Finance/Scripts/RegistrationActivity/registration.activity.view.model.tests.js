﻿// Copyright 2013-2017 Ellucian Company L.P. and its affiliates. 
//Ellucian = Ellucian || {};
//Ellucian.Finance = Ellucian.Finance || {};
//Ellucian.Finance.RegistrationActivity = Ellucian.Finance.RegistrationActivity || {};

//registrationSummaryString = "View Registration Summary";
//payForRegistrationString = "Pay for Registration";
//goToAccountActivityString = "Go to Account Activity";
//paymentRequirementsMetString = "Complete";
//paymentRequirementsNotMetString = "Incomplete";
//noActiveClassesText = "No active classes";
//registrationActivityNoActionString = "Registration not completed; payment requirements not met.";
//actionHeaderLabel = "Action";
//multipleActionsHeaderLabel = "Actions";
//raReprintSummaryAdminActionUrl = "http://www.ellucianuniversity.edu/Finance/RegistrationActivity/Admin/ReprintSummary";
//raReprintSummaryActionUrl = "http://www.ellucianuniversity.edu/Finance/RegistrationActivity/ReprintSummary";
//accountActivityAdminActionUrl = "http://www.ellucianuniversity.edu/Finance/AccountActivity/Admin";
//accountActivityActionUrl = "http://www.ellucianuniversity.edu/Finance/AccountActivity";
//ipcRegistrationSummaryActionUrl = "http://www.ellucianuniversity.edu/Finance/ImmediatePayments";
//raDisplayPaymentPlanSummaryActionUrl = "http://www.ellucianuniversity.edu/Finance/RegistrationActivity/DisplayPaymentPlanSummary";
//raPrintPaymentPlanSummaryActionUrl = "http://www.ellucianuniversity.edu/Finance/RegistrationActivity/PrintPaymentPlanSummary";
//registrationActivityAdminActionUrl = "http://www.ellucianuniversity.edu/Finance/RegistrationActivity/Admin";
//registrationActivityActionUrl = "http://www.ellucianuniversity.edu/Finance/RegistrationActivity";
//Ellucian.Finance.RegistrationActivity.ActionUrls.registrationActivityChangeUserUrl = "changeUserUrl";
//weeklyFrequencyDescriptionString = "Weekly";
//monthlyFrequencyDescriptionString = "Monthly";
//biweeklyFrequencyDescriptionString = "Biweekly";
//yearlyFrequencyDescriptionString = "Yearly";
//customFrequencyDescriptionString = "Custom";
//unknownFrequencyDescriptionString = "Unknown";
//registrationActivityUnableToLoadMessage = "Could not load activity.";
//raDisplayPaymentPlanSummaryActionUrl = "http://www.ellucianuniversity.edu/Finance/RegistrationActivity/DisplayPlanSummary";

describe("registrationActivity.RegistrationActivityViewModel", function () {
    var viewModel, data;

    beforeEach(function () {
        data = {
            "Message": "Some message",
            "IsAdminUser": false,
            "StudentId": "0001234",
            "StudentName": "John Smith",
            "PaymentControl": {
                "AcademicCredits": [ "101", "102" ],
                "Id": "201",
                "InvoiceIds": [ "301", "302" ],
                "LastPlanApprovalId": "401",
                "LastTermsApprovalId": "501",
                "PaymentPlanId": "601",
                "Payments": [ "701", "702" ],
                "PaymentStatus": "Complete",
                "RegisteredSectionIds": ["801", "802"],
                "StudentId": "0001234",
                "TermId": "2014/FA"
            },
            "RegistrationTerms": [
                {
                    "PaymentControl": {
                        "AcademicCredits": ["101", "102"],
                        "Id": "201",
                        "InvoiceIds": ["301", "302"],
                        "LastPlanApprovalId": "401",
                        "LastTermsApprovalId": "501",
                        "PaymentPlanId": "601",
                        "Payments": ["701", "702"],
                        "PaymentStatus": "Complete",
                        "RegisteredSectionIds": ["801", "802"],
                        "StudentId": "0001234",
                        "TermId": "2014/FA"
                    },
                    "TermDescription": "2014 Fall Term",
                    "Status": "Complete",
                    "FinancialPeriod": {
                        "End": new Date(2014, 11, 15),
                        "Start": new Date(2014, 7, 15),
                        "Type": "Current"
                    },
                    "PeriodCode": "CUR"
                },
                {
                    "PaymentControl": {
                        "AcademicCredits": ["103"],
                        "Id": "202",
                        "InvoiceIds": ["303"],
                        "LastPlanApprovalId": null,
                        "LastTermsApprovalId": null,
                        "PaymentPlanId": null,
                        "Payments": [],
                        "PaymentStatus": "New",
                        "RegisteredSectionIds": ["803"],
                        "StudentId": "0001234",
                        "TermId": "2015/SP"
                    },
                    "TermDescription": "2015 Spring Term",
                    "Status": "New",
                    "FinancialPeriod": {
                        "End": new Date(2015, 0, 15),
                        "Start": new Date(2014, 4, 15),
                        "Type": "Future"
                    },
                    "PeriodCode": "FTR"
                },
            ],
            "AccountActivityDisplayByTerm": true,
        };

        viewModel = new registrationActivity.RegistrationActivityViewModel(data);
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading is true after initialization", function () {
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":Message is undefined after initialization", function () {
        expect(viewModel.Message()).toBe(undefined);
    });

    it(":PaymentControl is undefined after initialization", function () {
        expect(viewModel.PaymentControl()).toBe(undefined);
    });

    it(":IsAdminUser is false after initialization", function () {
        expect(viewModel.IsAdminUser()).toBeFalsy();
    });

    it(":PersonId is undefined after initialization", function () {
        expect(viewModel.PersonId()).toBe(undefined);
    });

    it(":PersonName is undefined after initialization", function () {
        expect(viewModel.PersonName()).toBe(undefined);
    });

    it(":RegistrationTerms is empty after initialization", function () {
        expect(viewModel.RegistrationTerms()).not.toBe(undefined);
        expect(viewModel.RegistrationTerms().length).toBe(0);
    });

    it(":AccountActivityDisplayByTerm is undefined after initialization", function () {
        expect(viewModel.AccountActivityDisplayByTerm()).toBe(undefined);
    });

    it(":HasPrivacyRestriction is false after initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBeFalsy();
    });

    it(":PrivacyMessage is undefined after initialization", function () {
        expect(viewModel.PrivacyMessage()).toBe(undefined);
    });

    it(":changeUserUrl set correctly", function () {
        viewModel.IsAdminUser(true);
        viewModel.PersonId("0001234");
        expect(viewModel.changeUserUrl()).toBe(Ellucian.Finance.RegistrationActivity.ActionUrls.registrationActivityChangeUserUrl);
    });

    it(":loadRegistrationActivityData calls ko.mapping.fromJS on success", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({"IsAdminUser":false});
            e.error({});
            e.complete({});
        });
        spyOn(ko.mapping, 'fromJS').and.callThrough();
        spyOn(viewModel, 'checkForMobile');

        viewModel.loadRegistrationActivityData(registrationActivityActionUrl);
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.showUI()).toBeTruthy();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
        expect(viewModel.checkForMobile).toHaveBeenCalled();
    });

    it(":loadRegistrationActivityData posts message to notification center on error", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });
        spyOn($.fn, "notificationCenter");

        viewModel.loadRegistrationActivityData(registrationActivityActionUrl);
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.showUI()).toBeTruthy();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: registrationActivityUnableToLoadMessage, type: "error" });
    });

    it(":processPlanLink calls ko.utils.postJson", function () {
        spyOn(ko.utils, "postJson").and.callThrough();

        viewModel.processPlanLink(data.RegistrationTerms[0]);
        expect(ko.utils.postJson).toHaveBeenCalled();
    });
});
