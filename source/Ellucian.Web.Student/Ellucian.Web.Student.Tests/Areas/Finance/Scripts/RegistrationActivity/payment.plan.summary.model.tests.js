﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
describe("registrationActivity.PaymentPlanSummaryModel", function () {
    var viewModel, data;
    Ellucian = Ellucian || {};
    Ellucian.Finance = Ellucian.Finance || {};
    Ellucian.Finance.RegistrationActivity = Ellucian.Finance.RegistrationActivity || {};
    Ellucian.Finance.RegistrationActivity.ActionUrls = {
        registrationActivityChangeUserUrl : "changeUserUrl"
    };

    beforeEach(function () {
        data = {
            "WorkflowTitle": "PaymentPlanAcknowledgement",
            "WorkflowStep": "PaymentAcknowledgement",
            "Notification": null,
            "IsAdminUser": false,
            "ReturnUrl": "http://www.ellucianuniversity.edu/Finance/RegistrationActivity",
            "PaymentControl": {
                "AcademicCredits": ["101", "102"],
                "Id": "201",
                "InvoiceIds": ["301", "302"],
                "LastPlanApprovalId": "401",
                "LastTermsApprovalId": "501",
                "PaymentPlanId": "601",
                "Payments": ["701", "702"],
                "PaymentStatus": "Complete",
                "RegisteredSectionIds": ["801", "802"],
                "StudentId": "0001234",
                "TermId": "2014/FA"
            },
            "PersonName": "John Smith",
            "PersonId": "0001234",
            "PlanDetails": {
                "DisplayTimestamp": Date(2014, 11, 15, 12, 30, 45, 50),
                "AcknowledgementText": ["This is some...", "...acknowledgement text"],
                "PaymentPlan": {
                    "CurrentAmount": 10200,
                    "CurrentStatus": "Open",
                    "CurrentStatusDate": Date(2014, 11, 15, 12, 32, 25, 10),
                    "DownPaymentAmount": 500,
                    "DownPaymentAmountPaid": 0,
                    "DownPaymentDate": null,
                    "DownPaymentPercentage": 5,
                    "FirstDueDate": Date(2015, 0, 15),
                    "Frequency": "Monthly",
                    "GraceDays": 3,
                    "Id": "601",
                    "LateChargeAmount": 10,
                    "LateChargePercentage": 1,
                    "NumberOfPayments": 5,
                    "OriginalAmount": 10200,
                    "PersonId": "0001234",
                    "PlanCharges": [
                        {
                            "Amount": 200,
                            "Charge": {
                                "Amount": 200,
                                "BaseAmount": 200,
                                "Code": "SETUP",
                                "Description": "Payment Plan Setup Fee",
                                "Id": "822",
                                "InvoiceId": "301",
                                "PaymentPlanIds": ["601"],
                                "TaxAmount": 0
                            },
                            "Id": "812",
                            "IsAutomaticallyModifiable": true,
                            "IsSetupCharge": true,
                            "PlanId": "601"
                        },
                        {
                            "Amount": 10000,
                            "Charge": {
                                "Amount": 10000,
                                "BaseAmount": 9500,
                                "Code": "TUITN",
                                "Description": "Semester Tuition",
                                "Id": "823",
                                "InvoiceId": "302",
                                "PaymentPlanIds": ["601"],
                                "TaxAmount": 500
                            },
                            "Id": "811",
                            "IsAutomaticallyModifiable": true,
                            "IsSetupCharge": false,
                            "PlanId": "601"
                        }],
                    "ReceivableTypeCode": "01",
                    "ScheduledPayments": [
                        {
                            "Amount": 2200,
                            "AmountPaid": 0,
                            "DueDate": Date(2015, 0, 15),
                            "Id": "901",
                            "IsPastDue": false,
                            "LastPaidDate": null,
                            "PlanId": "601"
                        },
                        {
                            "Amount": 2000,
                            "AmountPaid": 0,
                            "DueDate": Date(2015, 1, 15),
                            "Id": "902",
                            "IsPastDue": false,
                            "LastPaidDate": null,
                            "PlanId": "601"
                        },
                        {
                            "Amount": 2000,
                            "AmountPaid": 0,
                            "DueDate": Date(2015, 2, 15),
                            "Id": "903",
                            "IsPastDue": false,
                            "LastPaidDate": null,
                            "PlanId": "601"
                        },
                         {
                             "Amount": 2000,
                             "AmountPaid": 0,
                             "DueDate": Date(2015, 3, 15),
                             "Id": "904",
                             "IsPastDue": false,
                             "LastPaidDate": null,
                             "PlanId": "601"
                         },
                        {
                            "Amount": 2000,
                            "AmountPaid": 0,
                            "DueDate": Date(2015, 4, 15),
                            "Id": "905",
                            "IsPastDue": false,
                            "LastPaidDate": null,
                            "PlanId": "601"
                        }],
                    "SetupAmount": 100,
                    "SetupPercentage": 1,
                    "Statuses": [
                        {
                            "Date": Date(2014, 11, 15),
                            "Status": "Open"
                        }],
                    "StudentName": "John Smith",
                    "TemplateId": "DEFAULT",
                    "TermId": "2014/FA",
                    "TotalSetupChargeAmount": 200
                },
                "StudentName": "John Smith",
                "StudentId": "0001234",
                "TermsAndConditionsText": ["This is some...", "...terms and conditions text"],
                "ReceivedDownPayment": 0,
                "ApprovalTimestamp": Date(2014, 11, 15, 12, 32, 15, 20),
                "ApprovalUserId": "0001234",
                "IsPrintable": false
            },
            "IsPrintable": false
        };
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
    });

    it(":AcceptTermsAndConditions is true after initialization", function () {
        expect(viewModel.AcceptTermsAndConditions()).toBeTruthy();
    });

    it(":EnablePlanDetails is true after initialization", function () {
        expect(viewModel.EnablePlanDetails()).toBeTruthy();
    });

    it(":StudentIdName is viewModel.PaymentControl.StudentId + viewModel.PlanDetails.StudentName after initialization", function () {
        expect(viewModel.StudentIdName()).toBe(data.PaymentControl.StudentId + " " + data.PlanDetails.StudentName);
    });

    it(":StudentIdName is viewModel.PlanDetails.StudentName when viewModel.PaymentControl.StudentId is not set", function () {
        data.PaymentControl = {
            "AcademicCredits": ["101", "102"],
            "Id": "201",
            "InvoiceIds": ["301", "302"],
            "LastPlanApprovalId": "401",
            "LastTermsApprovalId": "501",
            "PaymentPlanId": "601",
            "Payments": ["701", "702"],
            "PaymentStatus": "Complete",
            "RegisteredSectionIds": ["801", "802"],
            "TermId": "2014/FA"
        };
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.StudentIdName()).toBe(" " + data.PlanDetails.StudentName);
    });

    it(":FrequencyDisplay is '' when viewModel.PlanDetails.PaymentPlan is null", function () {
        data.PlanDetails.PaymentPlan = null;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe("");
    });

    it(":FrequencyDisplay is weeklyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is Weekly", function () {
        data.PlanDetails.PaymentPlan.Frequency = "Weekly";
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(weeklyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is weeklyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is 0", function () {
        data.PlanDetails.PaymentPlan.Frequency = 0;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(weeklyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is biweeklyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is Biweekly", function () {
        data.PlanDetails.PaymentPlan.Frequency = "Biweekly";
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(biweeklyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is biweeklyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is 1", function () {
        data.PlanDetails.PaymentPlan.Frequency = 1;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(biweeklyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is monthlyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is Monthly", function () {
        data.PlanDetails.PaymentPlan.Frequency = "Monthly";
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(monthlyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is monthlyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is 2", function () {
        data.PlanDetails.PaymentPlan.Frequency = 2;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(monthlyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is yearlyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is Yearly", function () {
        data.PlanDetails.PaymentPlan.Frequency = "Yearly";
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(yearlyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is yearlyFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is 3", function () {
        data.PlanDetails.PaymentPlan.Frequency = 3;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(yearlyFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is customFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is Custom", function () {
        data.PlanDetails.PaymentPlan.Frequency = "Custom";
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(customFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is customFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is 4", function () {
        data.PlanDetails.PaymentPlan.Frequency = 4;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(customFrequencyDescriptionString);
    });

    it(":FrequencyDisplay is unknownFrequencyDescriptionString when viewModel.PlanDetails.PaymentPlan.Frequency is not a valid value", function () {
        data.PlanDetails.PaymentPlan.Frequency = 5;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.FrequencyDisplay()).toBe(unknownFrequencyDescriptionString);
    });

    it(":DisplayPlanApprovalMessage is true when plan has ApprovalTimestamp and ApprovalUserId", function () {
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.DisplayPlanApprovalMessage()).toBeTruthy();
    });

    it(":DisplayPlanApprovalMessage is true when plan does not have ApprovalTimestamp", function () {
        data.PlanDetails.ApprovalTimestamp = null;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.DisplayPlanApprovalMessage()).toBeFalsy();
    });

    it(":DisplayPlanApprovalMessage is true when plan does not have ApprovalUserId", function () {
        data.PlanDetails.ApprovalUserId = null;
        viewModel = new registrationActivity.PaymentPlanSummaryModel(data);
        expect(viewModel.DisplayPlanApprovalMessage()).toBeFalsy();
    });

    it(":printPlanAcknowledgement calls window.open with viewModel.PrintPlanAcknowledgementUrl and _blank", function () {
        spyOn(window,"open").and.callThrough();

        viewModel.printPlanAcknowledgement();
        expect(window.open).toHaveBeenCalledWith(viewModel.PrintPlanAcknowledgementUrl(), "_blank");
    });
});