﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates. 
baseSiteUrl = "http://localhost/"

describe("accountActivityViewModel", function () {
    var viewModel, data;
               
    beforeEach(function () {
        data = {
            "DisplayTermPeriodCode": "2015/SP",
            "DisplayTermPeriodDescription": "2015 Spring Term",
            "DropdownLabel": "Term",
            "TermPeriodBalances": [{
                "Id": "2015/SP",
                "Description": "2015 Spring Term",
                "Balance": 4114.70,
                "AssociatedTerms": ["2015/SP"],
                "DisplayText": "2015 Spring Term - Balance: $4,114.70",
                "ProcessUrl": "/Ellucian.Web.Student/Finance/AccountActivity/GetAccountActivityViewModel?PersonId=0011521\u0026timeframeId=2015%2FSP"
            },
            {
                "Id": "2014/FA",
                "Description": "2014 Fall Term",
                "Balance": -750.00,
                "AssociatedTerms": ["2014/FA"],
                "DisplayText": "2014 Fall Term - Balance: -$750.00",
                "ProcessUrl": "/Ellucian.Web.Student/Finance/AccountActivity/GetAccountActivityViewModel?PersonId=0011521\u0026timeframeId=2014%2FFA"
            },
            {
                "Id": "NON-TERM",
                "Description": "Other",
                "Balance": 945.00,
                "AssociatedTerms": [],
                "DisplayText": "Other - Balance: $945.00",
                "ProcessUrl": "/Ellucian.Web.Student/Finance/AccountActivity/GetAccountActivityViewModel?PersonId=0011521\u0026timeframeId=NON-TERM"
            }],
            "FormulaCategories": [{
                "Id": "2015/SP-0",
                "CategoryType": 0,
                "Order": 0,
                "Description": "Charges",
                "ImageUrl": "Areas/Finance/Content/Images/Icon-Charges.png",
                "ImageAltText": "Charges",
                "PrefixImageUrl": "Content/images/icon_plus_sign.png",
                "PrefixImageAltText": "Plus",
                "CategoryStyle": null,
                "IncludeInFormula": true,
                "Transactions": [{
                    "Id": "Tuition",
                    "Description": "Tuition",
                    "ChargeType": 0,
                    "Order": 1,
                    "ChargeDetails": [{
                        "Name": "ARTH-100-01",
                        "Title": "Beginning Art History",
                        "BillingCredits": 3.00,
                        "CEUs": null,
                        "MeetingDays": "MWF",
                        "MeetingTimes": "11:00-11:50 AM",
                        "MeetingRoom": "Georgia O\u0027Keefe Center 110",
                        "Instructor": "G. Dahle",
                        "Status": "New",
                        "TermId": "2015/SP",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 4125.00
                    },
                    {
                        "Name": "HIST-100-01",
                        "Title": "Western Civilization I",
                        "BillingCredits": 3.00,
                        "CEUs": 6.00,
                        "MeetingDays": "MWF",
                        "MeetingTimes": "7:00-7:50 PM",
                        "MeetingRoom": "Toni Morrison Hall 201",
                        "Instructor": "E. Loadwick",
                        "Status": "New",
                        "TermId": "2015/SP",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 4125.00
                    },
                    {
                        "Name": "MATH-099-01",
                        "Title": "Developmental Mathematics",
                        "BillingCredits": 0.00,
                        "CEUs": null,
                        "MeetingDays": "Sa",
                        "MeetingTimes": "9:00 AM-5:00 PM",
                        "MeetingRoom": "Jerry Yang Hall 120",
                        "Instructor": "T. Hamilton",
                        "Status": "New",
                        "TermId": "2015/SP",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 0.00
                    },
                    {
                        "Name": "Other Tuition Activity",
                        "Title": "",
                        "BillingCredits": null,
                        "CEUs": null,
                        "MeetingDays": "",
                        "MeetingTimes": "TBD",
                        "MeetingRoom": "",
                        "Instructor": "",
                        "Status": "",
                        "TermId": "2015/SP",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 555.55
                    }],
                    "IsVisible": true,
                    "Amount": 8805.55
                },
                {
                    "Id": "Registration_Fee",
                    "Description": "Registration Fee",
                    "ChargeType": 2,
                    "Order": 2,
                    "ChargeDetails": [{
                        "Description": "Athletic Fee",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 25.00
                    },
                    {
                        "Description": "Health Service Fee",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 1225.00
                    },
                    {
                        "Description": "Technology Fee",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 175.00
                    }],
                    "IsVisible": true,
                    "Amount": 1425.00
                },
                {
                    "Id": "Bookstore_Charges",
                    "Description": "Bookstore Charges",
                    "ChargeType": 4,
                    "Order": 3,
                    "ChargeDetails": [{
                        "InvoiceNumber": "000007778",
                        "Date": "\/Date(1418706000000)\/",
                        "Description": "Books",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 385.00
                    }],
                    "IsVisible": true,
                    "Amount": 385.00
                },
                {
                    "Id": "Non-Credit_Tuition",
                    "Description": "Non-Credit Tuition",
                    "ChargeType": 1,
                    "Order": 4,
                    "ChargeDetails": [{
                        "Name": "CNED-CE100-JPM",
                        "Title": "Crocheting",
                        "BillingCredits": 1.50,
                        "CEUs": 1.50,
                        "MeetingDays": "",
                        "MeetingTimes": "TBD",
                        "MeetingRoom": " ",
                        "Instructor": "TBA",
                        "Status": "Dropped",
                        "TermId": "2015/SP",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 80.00
                    },
                    {
                        "Name": "Other Tuition Activity",
                        "Title": "",
                        "BillingCredits": null,
                        "CEUs": null,
                        "MeetingDays": "",
                        "MeetingTimes": "TBD",
                        "MeetingRoom": "",
                        "Instructor": "",
                        "Status": "",
                        "TermId": "2015/SP",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 555.55
                    }],
                    "IsVisible": true,
                    "Amount": 635.55
                },
                {
                    "Id": "Activities_Fee",
                    "Description": "Activities Fee",
                    "ChargeType": 2,
                    "Order": 5,
                    "ChargeDetails": [{
                        "Description": "Student Activity Fee",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 325.00
                    }],
                    "IsVisible": true,
                    "Amount": 325.00
                },
                {
                    "Id": "Room_and_Board",
                    "Description": "Room \u0026 Board",
                    "ChargeType": 3,
                    "Order": 6,
                    "ChargeDetails": [{
                        "Description": "Terry Fox Suite Rates",
                        "Building": "Terry Fox Suites",
                        "Date": "\/Date(1418706000000)\/",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 2850.00
                    },
                    {
                        "Description": "14 Meals a Week",
                        "Building": "",
                        "Date": "\/Date(1418706000000)\/",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 1500.00
                    }],
                    "IsVisible": true,
                    "Amount": 4350.00
                },
                {
                    "Id": "Miscellaneous",
                    "Description": "Miscellaneous",
                    "ChargeType": 9,
                    "Order": 99,
                    "ChargeDetails": [{
                        "InvoiceNumber": "Pending",
                        "Date": "\/Date(1428033600000)\/",
                        "Description": "Late Charge",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 535.60
                    },
                    {
                        "InvoiceNumber": "000007872",
                        "Date": "\/Date(1421902800000)\/",
                        "Description": "Late Charge",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 75.90
                    },
                    {
                        "InvoiceNumber": "000007871",
                        "Date": "\/Date(1421902800000)\/",
                        "Description": "Late Charge",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 121.40
                    },
                    {
                        "InvoiceNumber": "000007870",
                        "Date": "\/Date(1421902800000)\/",
                        "Description": "Late Charge",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 121.40
                    },
                    {
                        "InvoiceNumber": "000007832",
                        "Date": "\/Date(1421125200000)\/",
                        "Description": "Payment Plan Setup Charge",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 100.00
                    },
                    {
                        "InvoiceNumber": "000007779",
                        "Date": "\/Date(1418706000000)\/",
                        "Description": "Payment Plan Setup Charge",
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "Amount": 100.00
                    }],
                    "IsVisible": true,
                    "Amount": 1054.30
                }],
                "Amount": 16980.40,
                "IsVisible": true,
                "IsDebit": true,
                "IsCredit": false
            },
            {
                "Id": "2015/SP-1",
                "CategoryType": 1,
                "Order": 1,
                "Description": "Payments",
                "ImageUrl": "Areas/Finance/Content/Images/Icon-Payments.png",
                "ImageAltText": "Payments",
                "PrefixImageUrl": "Content/images/icon_minus_sign.png",
                "PrefixImageAltText": "Minus",
                "CategoryStyle": null,
                "IncludeInFormula": true,
                "Transactions": [{
                    "Id": "000001463",
                    "ReceiptNumber": "000001463",
                    "Date": "\/Date(1386046800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 576.00,
                    "PaymentMethodCode": "Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": ""
                },
                {
                    "Id": "000001464",
                    "ReceiptNumber": "000001464",
                    "Date": "\/Date(1386046800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 714.00,
                    "PaymentMethodCode": "e-Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": "4132"
                },
                {
                    "Id": "000001471",
                    "ReceiptNumber": "000001471",
                    "Date": "\/Date(1421902800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 8700.00,
                    "PaymentMethodCode": "e-Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": ""
                },
                {
                    "Id": "000001471",
                    "ReceiptNumber": "000001471",
                    "Date": "\/Date(1421902800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 835.40,
                    "PaymentMethodCode": "e-Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": ""
                },
                {
                    "Id": "000001471",
                    "ReceiptNumber": "000001471",
                    "Date": "\/Date(1421902800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 835.40,
                    "PaymentMethodCode": "e-Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": ""
                },
                {
                    "Id": "000001471",
                    "ReceiptNumber": "000001471",
                    "Date": "\/Date(1421902800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 334.90,
                    "PaymentMethodCode": "e-Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": ""
                },
                {
                    "Id": "000001471",
                    "ReceiptNumber": "000001471",
                    "Date": "\/Date(1421902800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 714.00,
                    "PaymentMethodCode": "e-Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": ""
                },
                {
                    "Id": "000001471",
                    "ReceiptNumber": "000001471",
                    "Date": "\/Date(1421902800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Amount": 232.00,
                    "PaymentMethodCode": "e-Check",
                    "PaymentMethodDescription": null,
                    "PaymentReferenceNumber": ""
                }],
                "Amount": 12941.70,
                "IsVisible": true,
                "IsDebit": false,
                "IsCredit": true
            },
            {
                "Id": "2015/SP-2",
                "CategoryType": 2,
                "Order": 2,
                "Description": "Financial Aid",
                "ImageUrl": "Areas/Finance/Content/Images/Icon-FinancialAid.png",
                "ImageAltText": "Financial Aid",
                "PrefixImageUrl": "Content/images/icon_minus_sign.png",
                "PrefixImageAltText": "Minus",
                "CategoryStyle": null,
                "IncludeInFormula": true,
                "Transactions": [{
                    "Id": "15/SP*AWDA",
                    "Description": "Unrestricted D7 Scholarship",
                    "AwardAmount": 1000.00,
                    "OtherAmount": null,
                    "OtherTermIds": null,
                    "LoanFeeAmount": null,
                    "IneligibleAmount": 1000.00,
                    "Comment": "Failed Eligibility",
                    "AwardTerms": [{
                        "TermId": "2015 Spring Term",
                        "TermDescription": "2015 Spring Term",
                        "DisbursementAmount": null,
                        "AnticipatedAmount": null
                    }],
                    "AnticipatedAidAmount": 0,
                    "DisbursedAidAmount": 0,
                    "Amount": 0
                }],
                "Amount": 0,
                "IsVisible": true,
                "IsDebit": false,
                "IsCredit": true
            },
            {
                "Id": "2015/SP-3",
                "CategoryType": 3,
                "Order": 3,
                "Description": "Sponsorships",
                "ImageUrl": "Areas/Finance/Content/Images/Icon-Sponsorships.png",
                "ImageAltText": "Sponsorships",
                "PrefixImageUrl": "Content/images/icon_minus_sign.png",
                "PrefixImageAltText": "Minus",
                "CategoryStyle": null,
                "IncludeInFormula": true,
                "Transactions": [],
                "Amount": null,
                "IsVisible": false,
                "IsDebit": false,
                "IsCredit": true
            },
            {
                "Id": "2015/SP-4",
                "CategoryType": 4,
                "Order": 4,
                "Description": "Deposits",
                "ImageUrl": "Areas/Finance/Content/Images/Icon-Deposits.png",
                "ImageAltText": "Deposits",
                "PrefixImageUrl": "Content/images/icon_minus_sign.png",
                "PrefixImageAltText": "Minus",
                "CategoryStyle": null,
                "IncludeInFormula": true,
                "Transactions": [{
                    "Id": "292",
                    "TypeDescription": "Tuition/Fees Deposit",
                    "Date": "\/Date(1386046800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "DepositAmount": 300.00,
                    "AppliedAmount": 0.00,
                    "OtherAmount": 0.0,
                    "RefundAmount": 0.00,
                    "NetAmount": 300.00,
                    "Amount": 300.00
                },
                {
                    "Id": "297",
                    "TypeDescription": "Tuition/Fees Deposit",
                    "Date": "\/Date(1421902800000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "DepositAmount": 200.00,
                    "AppliedAmount": 0.00,
                    "OtherAmount": 0.0,
                    "RefundAmount": 0.00,
                    "NetAmount": 200.00,
                    "Amount": 200.00
                }],
                "Amount": 500.00,
                "IsVisible": true,
                "IsDebit": false,
                "IsCredit": true
            },
            {
                "Id": "2015/SP-9",
                "CategoryType": 9,
                "Order": 9,
                "Description": "Refunds",
                "ImageUrl": "Areas/Finance/Content/Images/Icon-Refunds.png",
                "ImageAltText": "Refunds",
                "PrefixImageUrl": "Content/images/icon_plus_sign.png",
                "PrefixImageAltText": "Plus",
                "CategoryStyle": null,
                "IncludeInFormula": true,
                "Transactions": [{
                    "Id": "V0000615",
                    "Date": "\/Date(1418706000000)\/",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "Description": "Overcharged",
                    "Amount": 576.00,
                    "DisbursementMethod": null
                }],
                "Amount": 576.00,
                "IsVisible": true,
                "IsDebit": true,
                "IsCredit": false
            }],
            "NonFormulaCategories": [{
                "Id": "2015/SP-11",
                "CategoryType": 11,
                "Order": 11,
                "Description": "Deposits Due",
                "ImageUrl": null,
                "ImageAltText": null,
                "PrefixImageUrl": null,
                "PrefixImageAltText": null,
                "CategoryStyle": null,
                "IncludeInFormula": false,
                "Transactions": [{
                    "Id": "44",
                    "TypeCode": "TUIFE",
                    "TypeDescription": "Tuition/Fees Deposit",
                    "DueDate": "\/Date(1420088400000)\/",
                    "TermId": "2015/SP",
                    "TermDescription": "2015 Spring Term",
                    "OriginalAmount": 500.00,
                    "PaidAmount": 500.00,
                    "Amount": 0.00
                },
                {
                    "Id": "45",
                    "TypeCode": "MEALS",
                    "TypeDescription": "Meal Plan Deposit",
                    "DueDate": "\/Date(1426651200000)\/",
                    "TermId": "2015/SP",
                    "TermDescription": "2015 Spring Term",
                    "OriginalAmount": 500.00,
                    "PaidAmount": 0.0,
                    "Amount": 500.00
                }],
                "Amount": null,
                "IsVisible": true,
                "IsDebit": false,
                "IsCredit": true
            },
            {
                "Id": "2015/SP-12",
                "CategoryType": 12,
                "Order": 12,
                "Description": "Payment Plans",
                "ImageUrl": null,
                "ImageAltText": null,
                "PrefixImageUrl": null,
                "PrefixImageAltText": null,
                "CategoryStyle": null,
                "IncludeInFormula": false,
                "Transactions": [{
                    "Id": "1858",
                    "Amount": 4760.00,
                    "TypeDescription": "Student Receivable",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "OriginalAmount": 4760.00,
                    "CurrentBalance": 1670.80,
                    "AmountDue": 1670.80,
                    "Schedules": [{
                        "PlanId": "1858",
                        "DueDate": "\/Date(1419051600000)\/",
                        "DueAmount": 476.00,
                        "SetupAmount": 100.00,
                        "LateAmount": 0.00,
                        "PaidAmount": 676.00,
                        "LastPaidDate": "\/Date(1421902800000)\/",
                        "Amount": -100.00
                    },
                    {
                        "PlanId": "1858",
                        "DueDate": "\/Date(1420088400000)\/",
                        "DueAmount": 714.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 0.00,
                        "PaidAmount": 714.00,
                        "LastPaidDate": "\/Date(1386046800000)\/",
                        "Amount": 0.00
                    },
                    {
                        "PlanId": "1858",
                        "DueDate": "\/Date(1420693200000)\/",
                        "DueAmount": 714.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 121.40,
                        "PaidAmount": 835.40,
                        "LastPaidDate": "\/Date(1421902800000)\/",
                        "Amount": 0.00
                    },
                    {
                        "PlanId": "1858",
                        "DueDate": "\/Date(1421298000000)\/",
                        "DueAmount": 714.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 121.40,
                        "PaidAmount": 835.40,
                        "LastPaidDate": "\/Date(1421902800000)\/",
                        "Amount": 0.00
                    },
                    {
                        "PlanId": "1858",
                        "DueDate": "\/Date(1421902800000)\/",
                        "DueAmount": 714.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 0.00,
                        "PaidAmount": 614.00,
                        "LastPaidDate": null,
                        "Amount": 100.00
                    },
                    {
                        "PlanId": "1858",
                        "DueDate": "\/Date(1422507600000)\/",
                        "DueAmount": 714.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 121.40,
                        "PaidAmount": 0.00,
                        "LastPaidDate": null,
                        "Amount": 835.40
                    },
                    {
                        "PlanId": "1858",
                        "DueDate": "\/Date(1423112400000)\/",
                        "DueAmount": 714.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 121.40,
                        "PaidAmount": 0.00,
                        "LastPaidDate": null,
                        "Amount": 835.40
                    }]
                },
                {
                    "Id": "1866",
                    "Amount": 1551.00,
                    "TypeDescription": "Student Receivable",
                    "TermId": "2015 Spring Term",
                    "TermDescription": "2015 Spring Term",
                    "OriginalAmount": 1551.00,
                    "CurrentBalance": 1452.80,
                    "AmountDue": 1452.80,
                    "Schedules": [{
                        "PlanId": "1866",
                        "DueDate": "\/Date(1421470800000)\/",
                        "DueAmount": 159.00,
                        "SetupAmount": 100.00,
                        "LateAmount": 75.90,
                        "PaidAmount": 334.90,
                        "LastPaidDate": "\/Date(1421902800000)\/",
                        "Amount": 0.00
                    },
                    {
                        "PlanId": "1866",
                        "DueDate": "\/Date(1422680400000)\/",
                        "DueAmount": 232.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 0.00,
                        "PaidAmount": 232.00,
                        "LastPaidDate": "\/Date(1421902800000)\/",
                        "Amount": 0.00
                    },
                    {
                        "PlanId": "1866",
                        "DueDate": "\/Date(1423285200000)\/",
                        "DueAmount": 232.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 73.20,
                        "PaidAmount": 0.00,
                        "LastPaidDate": null,
                        "Amount": 305.20
                    },
                    {
                        "PlanId": "1866",
                        "DueDate": "\/Date(1423890000000)\/",
                        "DueAmount": 232.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 73.20,
                        "PaidAmount": 0.00,
                        "LastPaidDate": null,
                        "Amount": 305.20
                    },
                    {
                        "PlanId": "1866",
                        "DueDate": "\/Date(1426910400000)\/",
                        "DueAmount": 232.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 73.20,
                        "PaidAmount": 0.00,
                        "LastPaidDate": null,
                        "Amount": 305.20
                    },
                    {
                        "PlanId": "1866",
                        "DueDate": "\/Date(1427515200000)\/",
                        "DueAmount": 232.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 73.20,
                        "PaidAmount": 0.00,
                        "LastPaidDate": null,
                        "Amount": 305.20
                    },
                    {
                        "PlanId": "1866",
                        "DueDate": "\/Date(1428206400000)\/",
                        "DueAmount": 232.00,
                        "SetupAmount": 0.00,
                        "LateAmount": 0.00,
                        "PaidAmount": 0.00,
                        "LastPaidDate": null,
                        "Amount": 232.00
                    }]
                }],
                "Amount": null,
                "IsVisible": true,
                "IsDebit": false,
                "IsCredit": true
            }],
            "DisclaimerText": "The following section identifies transactions you have with the institution.  They do not affect the balance above, only the amount currently due.",
            "Balance": 4114.70,
            "BalanceImageUrl": "Areas/Finance/Content/Images/Icon-Balance.png",
            "BalanceImageAltText": "Balance",
            "ShowDepositsDueLink": true,
            "ShowPaymentPlansLink": true,
            "HasData": true,
            "NoDataMessage": "There is no account activity to display.",
            "AlertMessage": "All invoices must be paid by \u003cb\u003e3/31/14\u003c/b\u003e to avoid late charges.",
            "IsAdminUser": false,
            "PersonId": "0011521",
            "PersonName": "Lillie E. Puckett",
            "IsTermDisplay": true
        };
        viewModel = new accountActivityViewModel(data);
    });

    afterEach(function () {
        data = null;
        viewModel = null;
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading is true after initialization", function () {
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":receiptIsLoading is false after initialization", function () {
        expect(viewModel.receiptIsLoading()).toBeFalsy();
    });

    it(":receiptIsDisplayed is false after initialization", function () {
        expect(viewModel.receiptIsDisplayed()).toBeFalsy();
    });

    it(":isPrintable is false after initialization", function () {
        expect(viewModel.isPrintable()).toBeFalsy();
    });

    it(":changeUserUrl set correctly", function () {
        viewModel.IsAdminUser(true);
        viewModel.PersonId("0001234");
        expect(viewModel.changeUserUrl()).toBe(Ellucian.Finance.AccountActivity.ActionUrls.accountActivityChangeUserUrl);
    });

    it(":receiptDialogOptions is defined", function () {
        expect(viewModel.receiptDialogOptions()).not.toBe(undefined);
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":changeToDesktop does nothing when isMobile is false", function () {
        viewModel.isMobile(false);

        spyOn(viewModel, 'reloadPage');

        viewModel.changeToDesktop();
        expect(viewModel.reloadPage).not.toHaveBeenCalled();
    });

    it(":changeToDesktop does nothing when showUI is false", function () {
        viewModel.isMobile(true);

        spyOn(viewModel, 'reloadPage');

        viewModel.changeToDesktop();
        expect(viewModel.reloadPage).not.toHaveBeenCalled();
    });

    it(":changeToDesktop calls reloadPage when showUI is true isMobile is false", function () {
        viewModel.isMobile(false);
        viewModel.showUI(true);
        var window = {
            location: {
                href: "http://localhost/Ellucian.Web.Student/Finance/AccountActivity?personId=0011521&timeframeId=2015%2FSP"
            }
        };

        spyOn(viewModel, 'reloadPage');

        viewModel.changeToDesktop();
        expect(viewModel.reloadPage).toHaveBeenCalled();
    });

    it(":changeToMobile does nothing when isMobile is true", function () {
        viewModel.isMobile(false);

        spyOn(viewModel, 'reloadPage');

        viewModel.changeToMobile();
        expect(viewModel.reloadPage).not.toHaveBeenCalled();
    });

    it(":changeToMobile does nothing when showUI is false", function () {
        viewModel.isMobile(true);

        spyOn(viewModel, 'reloadPage');

        viewModel.changeToMobile();
        expect(viewModel.reloadPage).not.toHaveBeenCalled();
    });

    it(":changeToMobile calls reloadPage when showUI isMobile are true", function () {
        viewModel.isMobile(true);
        viewModel.showUI(true);
        var window = {
            location: {
                href: "http://localhost/Ellucian.Web.Student/Finance/AccountActivity?personId=0011521&timeframeId=2015%2FSP"
            }
        };

        spyOn(viewModel, 'reloadPage');

        viewModel.changeToMobile();
        expect(viewModel.reloadPage).toHaveBeenCalled();
    });

    it(":actionThrobberMessage set to accountActivityActionThrobberMessage value after initialization", function () {
        expect(viewModel.actionThrobberMessage()).toBe(accountActivityActionThrobberMessage);
    });

    it(":receiptThrobberMessage set to accountActivityReceiptThrobberMessage value after initialization", function () {
        expect(viewModel.receiptThrobberMessage()).toBe(accountActivityReceiptThrobberMessage);
    });

    it(":AlertMessage is undefined after initialization", function () {
        expect(viewModel.AlertMessage()).toBe(undefined);
    });

    it(":Balance is undefined after initialization", function () {
        expect(viewModel.Balance()).toBe(undefined);
    });

    it(":BalanceImageUrl is undefined after initialization", function () {
        expect(viewModel.BalanceImageUrl()).toBe(undefined);
    });

    it(":BalanceImageAltText is undefined after initialization", function () {
        expect(viewModel.BalanceImageAltText()).toBe(undefined);
    });

    it(":DisclaimerText is undefined after initialization", function () {
        expect(viewModel.DisclaimerText()).toBe(undefined);
    });

    it(":DisplayTermPeriodCode is undefined after initialization", function () {
        expect(viewModel.DisplayTermPeriodCode()).toBe(undefined);
    });

    it(":DisplayTermPeriodDescription is undefined after initialization", function () {
        expect(viewModel.DisplayTermPeriodDescription()).toBe(undefined);
    });

    it(":DropdownLabel is undefined after initialization", function () {
        expect(viewModel.DropdownLabel()).toBe(undefined);
    });

    it(":FormulaCategories is empty array after initialization", function () {
        expect(viewModel.FormulaCategories().constructor).toBe(Array);
        expect(viewModel.FormulaCategories().length).toBe(0);
    });

    it(":HasData is false after initialization", function () {
        expect(viewModel.HasData()).toBeFalsy();
    });

    it(":IsAdminUser is false after initialization", function () {
        expect(viewModel.IsAdminUser()).toBeFalsy();
    });

    it(":IsTermDisplay is undefined after initialization", function () {
        expect(viewModel.IsTermDisplay()).toBe(undefined);
    });

    it(":NoDataMessage is undefined after initialization", function () {
        expect(viewModel.NoDataMessage()).toBe(undefined);
    });

    it(":NonFormulaCategories is empty array after initialization", function () {
        expect(viewModel.NonFormulaCategories().constructor).toBe(Array);
        expect(viewModel.NonFormulaCategories().length).toBe(0);
    });

    it(":PersonId is undefined after initialization", function () {
        expect(viewModel.PersonId()).toBe(undefined);
    });

    it(":PersonName is undefined after initialization", function () {
        expect(viewModel.PersonName()).toBe(undefined);
    });

    it(":ReceiptData is undefined after initialization", function () {
        expect(viewModel.ReceiptData()).toBe(undefined);
    });

    it(":ShowDepositsDueLink false after initialization", function () {
        expect(viewModel.ShowDepositsDueLink()).toBeFalsy();
    });

    it(":ShowPaymentPlansLink false after initialization", function () {
        expect(viewModel.ShowPaymentPlansLink()).toBeFalsy();
    });

    it(":TermPeriodBalances is empty array after initialization", function () {
        expect(viewModel.TermPeriodBalances().constructor).toBe(Array);
        expect(viewModel.TermPeriodBalances().length).toBe(0);
    });

    it(":HasPrivacyRestriction is false after initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBeFalsy();
    });

    it(":PrivacyMessage is undefined after initialization", function () {
        expect(viewModel.PrivacyMessage()).toBe(undefined);
    });

    it(":currentStudentInfo is blank when IsAdminUser is true and PersonId/PersonName are initialized", function () {
        viewModel.IsAdminUser(true);
        expect(viewModel.currentStudentInfo()).toBe(undefined + " " + undefined);
    });

    it(":currentStudentInfo is PersonId + ' ' + PersonName when IsAdminUser is true and PersonId/PersonName are set", function () {
        viewModel.IsAdminUser(true);
        viewModel.PersonId(data.PersonId);
        viewModel.PersonName(data.PersonName);
        expect(viewModel.currentStudentInfo()).toBe(data.PersonId + " " + data.PersonName);
    });

    it(":balanceImagePath is null after initialization", function () {
        expect(viewModel.balanceImagePath()).toBe(null);
    });

    it(":balanceImagePath is baseSiteUrl + BalanceImageUrl when baseSiteUrl and BalanceImageUrl are defined", function () {
        viewModel.BalanceImageUrl(data.BalanceImageUrl);
        var expected = baseSiteUrl + data.BalanceImageUrl;

        expect(viewModel.balanceImagePath()).toBe(expected);
    });

    it(":selectedTermPeriod is undefined after initialization", function () {
        expect(viewModel.selectedTermPeriod()).toBe(undefined);
    });

    it(":viewStatementLink is getStudentStatementReportActionUrl + '?personId=' + self.PersonId() + '&timeframeId=' + self.DisplayTermPeriodCode() after initialization", function () {
        viewModel.PersonId(data.PersonId);
        viewModel.DisplayTermPeriodCode(data.DisplayTermPeriodCode);

        var expected = getStudentStatementReportActionUrl + "?personId=" + viewModel.PersonId() + "&timeframeId=" + viewModel.DisplayTermPeriodCode()
        expect(viewModel.viewStatementLink()).toBe(expected);
    });

    it(":depositDueClickHandler calls global clickHandler() with '#deposits-due'", function () {

        var global = jasmine.getGlobal();
        spyOn(global, 'clickHandler');

        viewModel.depositDueClickHandler();
        expect(global.clickHandler).toHaveBeenCalledWith('#deposits-due');
    });

    it(":paymentPlansClickHandler calls global clickHandler() with '#payment-plans'", function () {

        var global = jasmine.getGlobal();
        spyOn(global, 'clickHandler');

        viewModel.paymentPlansClickHandler();
        expect(global.clickHandler).toHaveBeenCalledWith('#payment-plans');
    });

    it(":iconBarClickHandler calls global clickHandler() with '#' + CategoryType() + '-accordion'", function () {

        var global = jasmine.getGlobal();
        spyOn(global, 'clickHandler');

        viewModel.FormulaCategories = ko.observableArray([
            {
                CategoryType: ko.observable("0"),
            }
        ]);
        viewModel.iconBarClickHandler(viewModel.FormulaCategories()[0], event);
        expect(global.clickHandler).toHaveBeenCalledWith('#0-accordion');
    });

    it(":printReceiptUrl is undefined after initialization", function () {
        expect(viewModel.printReceiptUrl()).toBe(undefined);
    });

    it(":getAccountActivity does nothing when isLoading is true", function () {
        viewModel.isLoading(true);
        spyOn(viewModel, 'loadAccountActivityData');

        viewModel.getAccountActivity();
        expect(viewModel.loadAccountActivityData).not.toHaveBeenCalled();
    });

    it(":getAccountActivity calls loadAccountActivityData with selected term period URL when isLoading is false and selectedTermPeriod ProcessUrl is set", function () {
        var stpb = {
            Id: ko.observable("2015/SP"),
            Description: ko.observable("2015 Spring Term"),
            Balance: ko.observable(4114.70),
            AssociatedTerms: ko.observableArray(["2015/SP"]),
            DisplayText: ko.observable("2015 Spring Term - Balance: $4,114.70"),
        };
        viewModel.selectedTermPeriod(stpb);
        viewModel.isLoading(false);

        spyOn(viewModel, 'loadAccountActivityData');

        viewModel.getAccountActivity();
        expect(viewModel.loadAccountActivityData).toHaveBeenCalledWith(getAccountActivityActionUrl, stpb.Id());
    });

    it(":getAccountActivity calls loadAccountActivityData with selected term period URL when isLoading is false and selectedTermPeriod ProcessUrl is set", function () {
        var stpb = {
            Id: ko.observable("2015/SP"),
            Description: ko.observable("2015 Spring Term"),
            Balance: ko.observable(4114.70),
            AssociatedTerms: ko.observableArray(["2015/SP"]),
            DisplayText: ko.observable("2015 Spring Term - Balance: $4,114.70"),
            ProcessUrl: ko.observable("/Ellucian.Web.Student/Finance/AccountActivity/GetAccountActivityViewModel?PersonId=0011521\u0026timeframeId=2015%2FSP")
        };
        viewModel.selectedTermPeriod(stpb);
        viewModel.isLoading(false);

        spyOn(viewModel, 'loadAccountActivityData');

        viewModel.getAccountActivity();
        expect(viewModel.loadAccountActivityData).toHaveBeenCalledWith(getAccountActivityActionUrl, stpb.Id());
    });

    it(":closeReceipt sets receiptIsDisplayed to false when called", function () {
        viewModel.closeReceipt();
        expect(viewModel.receiptIsDisplayed()).toBeFalsy();
    });

    it(":printReceipt opens new window for printReceiptUrl", function () {
        spyOn(window, "open");
        viewModel.printReceipt();
        expect(window.open).toHaveBeenCalledWith(viewModel.printReceiptUrl(), '_blank');
    });

    it(":printCashReceiptButton set correctly on initialization", function () {
        expect(viewModel.printCashReceiptButton).toEqual({
            id: "print-cash-receipt-dialog-button",
            isPrimary: true,
            title: Ellucian.AccountActivity.ButtonLabels.cashReceiptPrintButtonLabel,
            callback: viewModel.printReceipt
        });
    });

    it(":closeCashReceiptDialogButton set correctly on initialization", function () {
        expect(viewModel.closeCashReceiptDialogButton).toEqual({
            id: "close-cash-receipt-dialog-button",
            isPrimary: false,
            title: Ellucian.AccountActivity.ButtonLabels.cashReceiptCloseButtonLabel,
            callback: viewModel.closeReceipt
        });
    });
});