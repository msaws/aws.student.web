﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
baseUrl = "http://localhost/Ellucian.Web.Student";
getFinanceAdminSearchViewModelUrl = "http://localhost/Ellucian.Web.Student/Finance/Admin/GetAdminViewModel";
searchForAccountholdersAsyncUrl = "http://localhost/Ellucian.Web.Student/Finance/Admin/Search";
adminLoadingText = "Loading...";
unableToLoadAdminSearch = "Unable to load person search.";
searchFailedMessage = "Search for {0} failed.";
searchErrorMessage = "Search for {0} returned no matches";

describe("admin.search.view.model", function () {
    beforeEach(function () {
        viewModel = new adminSearchViewModel();
        data = { "Id": "0001234", "HasPrivacyRestriction": false };
    });

    it(":calls searchViewModel when called", function () {
        spyOn(searchViewModel, "call").and.callThrough();
        viewModel = new adminSearchViewModel();

        expect(searchViewModel.call).toHaveBeenCalledWith(viewModel);
    });

    it(":getFinanceAdminSearchViewModel calls checkForMobile and makeTableResponsive, ko.mapping.fromJS, sets isLoading to false and showUI to true on success,", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn(viewModel, "checkForMobile");
        spyOn(ko.mapping, 'fromJS').and.callThrough();
        var data = [
                {"Id":"0012345","Name":"Result 1"},
                {"Id":"0012346","Name":"Result 2"}
        ];
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(data);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });

        viewModel.getFinanceAdminSearchViewModel();
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.showUI()).toBeTruthy();
        expect(viewModel.checkForMobile).toHaveBeenCalled();
        expect(ko.mapping.fromJS).toHaveBeenCalledWith(data, {}, viewModel);
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":getFinanceAdminSearchViewModel calls checkForMobile and makeTableResponsive, posts error to notification center, sets isLoading to false and showUI to true on error,", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn(viewModel, "checkForMobile");
        spyOn($.fn, "notificationCenter");
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });

        viewModel.getFinanceAdminSearchViewModel();
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.showUI()).toBeTruthy();
        expect(viewModel.checkForMobile).toHaveBeenCalled();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: unableToLoadAdminSearch, type: "error", flash: true });
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":executeSearch calls makeTableResponsive, posts error to notification center, sets searching to false, SearchResults to null, ErrorOccurred to true, and ErrorMessage on error,", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn($.fn, "notificationCenter");
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });

        viewModel.SearchString("Smith, J");
        viewModel.executeSearch();
        expect(viewModel.searching()).toBeFalsy();
        expect(viewModel.ErrorOccurred()).toBeTruthy();
        expect(viewModel.SearchResults()).toBe(null);
        expect(viewModel.ErrorMessage()).toBe(searchErrorMessage.format(viewModel.SearchString()));
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: searchFailedMessage.format(viewModel.SearchString()), type: "error", flash: true });
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":executeSearch calls makeTableResponsive, sets SearchResults to data, and sets searching to false on success,", function () {
        spyOn($.fn, "makeTableResponsive");
        var data = [
                { "Id": "0012345", "Name": "Result 1" },
                { "Id": "0012346", "Name": "Result 2" }
        ];
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(data);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });

        viewModel.SearchString("Smith, J");
        viewModel.executeSearch();
        expect(viewModel.searching()).toBeFalsy();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":selectResult calls openUrl when necessary fields are set", function () {
        var global = jasmine.getGlobal();
        spyOn(global, "openUrl").and.callThrough();

        viewModel.Select({
            Action: ko.observable("SearchForAccountholdersAsync"),
            Controller: ko.observable("Admin"),
            Area: ko.observable("Finance")
        });
        viewModel.selectResult(data);
        expect(global.openUrl).toHaveBeenCalledWith(baseUrl + viewModel.Select().Area() + '/' + viewModel.Select().Controller() + '/' + viewModel.Select().Action() + '/' + data.Id);
    });

    it(":selectResult does not call openUrl when HasPrivacyRestriction is true", function () {
        var global = jasmine.getGlobal();
        spyOn(global, "openUrl").and.callThrough();

        viewModel.Select({
            Action: ko.observable("SearchForAccountholdersAsync"),
            Controller: ko.observable("Admin"),
            Area: ko.observable("Finance")
        });
        data.HasPrivacyRestriction = true;
        viewModel.selectResult(data);
        expect(global.openUrl).not.toHaveBeenCalled();
    });
});