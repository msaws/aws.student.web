﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("makeAPayment.PaymentsViewModel", function () {
    var viewModel, data;
               
    beforeEach(function () {
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        data = {
            "AvailablePaymentMethods": [{
                "InternalCode": "CC",
                "Description": "Credit Card",
                "Type": "CC"
            },
            {
                "InternalCode": "DISC",
                "Description": "Discover",
                "Type": "CC"
            },
            {
                "InternalCode": "ECHK",
                "Description": "e-Check",
                "Type": "CK"
            },
            {
                "InternalCode": "MAST",
                "Description": "MasterCard",
                "Type": "CC"
            },
            {
                "InternalCode": "TNCC",
                "Description": "TouchNet Credit Card",
                "Type": "CC"
            },
            {
                "InternalCode": "TNCK",
                "Description": "TouchNet e-Check",
                "Type": "CK"
            },
            {
                "InternalCode": "VISA",
                "Description": "VISA",
                "Type": "CC"
            }],
            "TermModels": [{
                "TermCode": "2014/FA",
                "TermDescription": "2014 Fall Term",
                "PaymentsDue": [{
                    "IsSelected": false,
                    "Description": "Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": null,
                    "DueAmount": -750.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": true,
                    "IsOverdue": false,
                    "IsEditable": false,
                    "MinimumPayment": null,
                    "Id": "Student Receivable_-750.00_BANK_01___"
                },
                {
                    "IsSelected": false,
                    "Description": "Room Deposit",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1451538000000)\/",
                    "DueAmount": 300.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": null,
                    "InvoiceId": "",
                    "PaymentPlanId": "",
                    "DepositDueId": "46",
                    "IsCredit": false,
                    "IsOverdue": false,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Room Deposit_300.00_BANK____46_12/31/2015"
                }],
                "DueAmount": -450.00,
                "PaidAmount": 0
            },
            {
                "TermCode": "2015/SP",
                "TermDescription": "2015 Spring Term",
                "PaymentsDue": [{
                    "IsSelected": true,
                    "Description": "Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1427688000000)\/",
                    "DueAmount": 911.10,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Student Receivable_911.10_BANK_01____3/30/2015"
                },
                {
                    "IsSelected": false,
                    "Description": "Continuing Ed Receivable",
                    "PaymentGroup": "TRAV",
                    "DueDate": "\/Date(1427688000000)\/",
                    "DueAmount": 80.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "02",
                    "InvoiceId": "",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Continuing Ed Receivable_80.00_TRAV_02____3/30/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1858 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1422507600000)\/",
                    "DueAmount": 835.40,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1858",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 835.40,
                    "Id": "Payment Plan 1858 - Student Receivable_835.40_BANK_01__1858__1/29/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1858 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1423112400000)\/",
                    "DueAmount": 835.40,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1858",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 835.40,
                    "Id": "Payment Plan 1858 - Student Receivable_835.40_BANK_01__1858__2/5/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1866 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1423285200000)\/",
                    "DueAmount": 305.20,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1866",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 305.20,
                    "Id": "Payment Plan 1866 - Student Receivable_305.20_BANK_01__1866__2/7/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1866 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1423890000000)\/",
                    "DueAmount": 305.20,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1866",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 305.20,
                    "Id": "Payment Plan 1866 - Student Receivable_305.20_BANK_01__1866__2/14/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1866 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1426910400000)\/",
                    "DueAmount": 305.20,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1866",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 305.20,
                    "Id": "Payment Plan 1866 - Student Receivable_305.20_BANK_01__1866__3/21/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1866 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1427515200000)\/",
                    "DueAmount": 305.20,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1866",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 305.20,
                    "Id": "Payment Plan 1866 - Student Receivable_305.20_BANK_01__1866__3/28/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1866 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1428206400000)\/",
                    "DueAmount": 232.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1866",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": false,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Payment Plan 1866 - Student Receivable_232.00_BANK_01__1866__4/5/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Meal Plan Deposit",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1426651200000)\/",
                    "DueAmount": 500.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": null,
                    "InvoiceId": "",
                    "PaymentPlanId": "",
                    "DepositDueId": "45",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Meal Plan Deposit_500.00_BANK____45_3/18/2015"
                }],
                "DueAmount": 4614.70,
                "PaidAmount": 0
            },
            {
                "TermCode": "NON-TERM",
                "TermDescription": "Other",
                "PaymentsDue": [{
                    "IsSelected": false,
                    "Description": "Registration - CNED-CE200-JPM",
                    "PaymentGroup": "TRAV",
                    "DueDate": "\/Date(1421384400000)\/",
                    "DueAmount": 80.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "02",
                    "InvoiceId": "14712",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Registration - CNED-CE200-JPM_80.00_TRAV_02_14712___1/16/2015"
                },
                {
                    "IsSelected": false,
                    "Description": "Registration - CNED-CE100-STMT",
                    "PaymentGroup": "TRAV",
                    "DueDate": "\/Date(1421384400000)\/",
                    "DueAmount": 80.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "02",
                    "InvoiceId": "14711",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Registration - CNED-CE100-STMT_80.00_TRAV_02_14711___1/16/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Non-Term Plan Invoice",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1426737600000)\/",
                    "DueAmount": 500.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "15117",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Non-Term Plan Invoice_500.00_BANK_01_15117___3/19/2015"
                },
                {
                    "IsSelected": false,
                    "Description": "Registration - CNED-CE300-STMT",
                    "PaymentGroup": "TRAV",
                    "DueDate": "\/Date(1421730000000)\/",
                    "DueAmount": 85.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "02",
                    "InvoiceId": "14713",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Registration - CNED-CE300-STMT_85.00_TRAV_02_14713___1/20/2015"
                },
                {
                    "IsSelected": false,
                    "Description": "Registration - CNED-CE400-STMT",
                    "PaymentGroup": "TRAV",
                    "DueDate": "\/Date(1423976400000)\/",
                    "DueAmount": 80.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "02",
                    "InvoiceId": "14714",
                    "PaymentPlanId": "",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Registration - CNED-CE400-STMT_80.00_TRAV_02_14714___2/15/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1894 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1426737600000)\/",
                    "DueAmount": 160.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1894",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 160.00,
                    "Id": "Payment Plan 1894 - Student Receivable_160.00_BANK_01__1894__3/19/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1894 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1427342400000)\/",
                    "DueAmount": 160.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1894",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 160.00,
                    "Id": "Payment Plan 1894 - Student Receivable_160.00_BANK_01__1894__3/26/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1894 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1427947200000)\/",
                    "DueAmount": 100.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1894",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": false,
                    "MinimumPayment": 100.00,
                    "Id": "Payment Plan 1894 - Student Receivable_100.00_BANK_01__1894__4/2/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Payment Plan 1894 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1428552000000)\/",
                    "DueAmount": 100.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1894",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": false,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Payment Plan 1894 - Student Receivable_100.00_BANK_01__1894__4/9/2015"
                },
                {
                    "IsSelected": false,
                    "Description": "Payment Plan 1894 - Student Receivable",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1429156800000)\/",
                    "DueAmount": 100.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": "01",
                    "InvoiceId": "",
                    "PaymentPlanId": "1894",
                    "DepositDueId": "",
                    "IsCredit": false,
                    "IsOverdue": false,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Payment Plan 1894 - Student Receivable_100.00_BANK_01__1894__4/16/2015"
                },
                {
                    "IsSelected": true,
                    "Description": "Room Deposit",
                    "PaymentGroup": "BANK",
                    "DueDate": "\/Date(1427947200000)\/",
                    "DueAmount": 300.00,
                    "PaidAmount": null,
                    "ReceivableTypeCode": null,
                    "InvoiceId": "",
                    "PaymentPlanId": "",
                    "DepositDueId": "47",
                    "IsCredit": false,
                    "IsOverdue": true,
                    "IsEditable": true,
                    "MinimumPayment": 1,
                    "Id": "Room Deposit_300.00_BANK____47_4/2/2015"
                }],
                "DueAmount": 1745.00,
                "PaidAmount": 0
            }],
            "PeriodModels": null,
            "PaymentsAllowed": true,
            "PartialAccountPaymentsAllowed": true,
            "PartialDepositPaymentsAllowed": true,
            "PartialPlanPaymentsAllowed": 2,
            "DueAmount": 5909.70,
            "PaidAmount": 0,
            "HasData": true,
            "NoDataMessage": "No account balance to pay.",
            "ShowCreditAmounts": true,
            "AlertMessage": "All invoices must be paid by \u003cb\u003e3/31/14\u003c/b\u003e to avoid late charges.",
            "IsAdminUser": false,
            "PersonId": "0011521",
            "PersonName": "Lillie E. Puckett",
            "IsTermDisplay": true,
            "UserPaymentPlanCreationEnabled": false
        };
        viewModel = new makeAPayment.PaymentsViewModel(data);
    });

    afterEach(function () {
        data: null;
        viewModel: null;
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading is true after initialization", function () {
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":actionThrobberMessage set to makePaymentActionThrobberMessage value after initialization", function () {
        expect(viewModel.actionThrobberMessage()).toBe(makePaymentActionThrobberMessage);
    });

    it(":AlertMessage is undefined after initialization", function () {
        expect(viewModel.AlertMessage()).toBe(undefined);
    });

    it(":AvailablePaymentMethods is empty array after initialization", function () {
        expect(viewModel.AvailablePaymentMethods().constructor).toBe(Array);
        expect(viewModel.AvailablePaymentMethods().length).toBe(0);
    });

    it(":DueAmount is set to 0 after initialization", function () {
        expect(viewModel.DueAmount()).toBe(0);
    });

    it(":HasData is false after initialization", function () {
        expect(viewModel.HasData()).toBeFalsy();
    });

    it(":IsAdminUser is false after initialization", function () {
        expect(viewModel.IsAdminUser()).toBeFalsy();
    });

    it(":IsTermDisplay is undefined after initialization", function () {
        expect(viewModel.IsTermDisplay()).toBe(undefined);
    });

    it(":NoDataMessage is undefined after initialization", function () {
        expect(viewModel.NoDataMessage()).toBe(undefined);
    });

    it(":PaymentsAllowed is true after initialization", function () {
        expect(viewModel.PaymentsAllowed()).toBeTruthy();
    });

    it(":PeriodModels is empty array after initialization", function () {
        expect(viewModel.PeriodModels().constructor).toBe(Array);
        expect(viewModel.PeriodModels().length).toBe(0);
    });

    it(":PersonId is undefined after initialization", function () {
        expect(viewModel.PersonId()).toBe(undefined);
    });

    it(":PersonName is undefined after initialization", function () {
        expect(viewModel.PersonName()).toBe(undefined);
    });

    it(":SelectedPaymentMethod is undefined after initialization", function () {
        expect(viewModel.SelectedPaymentMethod()).toBe(undefined);
    });

    it(":ShowCreditAmounts is true after initialization", function () {
        expect(viewModel.ShowCreditAmounts()).toBeTruthy();
    });

    it(":TermModels is empty array after initialization", function () {
        expect(viewModel.TermModels().constructor).toBe(Array);
        expect(viewModel.TermModels().length).toBe(0);
    });

    it(":UserPaymentPlanCreationEnabled is false after initialization", function () {
        expect(viewModel.UserPaymentPlanCreationEnabled()).toBeFalsy();
    });

    it(":PaymentPlanTermModels is empty array after initialization", function () {
        expect(viewModel.PaymentPlanTermModels().constructor).toBe(Array);
        expect(viewModel.PaymentPlanTermModels().length).toBe(0);
    });

    it(":PaymentPlanPeriodModels is empty array after initialization", function () {
        expect(viewModel.PaymentPlanPeriodModels().constructor).toBe(Array);
        expect(viewModel.PaymentPlanPeriodModels().length).toBe(0);
    });

    it(":PaymentPlanEligibilityText is undefined after initialization", function () {
        expect(viewModel.PaymentPlanEligibilityText()).toBe(undefined);
    });

    it(":HasPrivacyRestriction is false after initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBeFalsy();
    });

    it(":PrivacyMessage is undefined after initialization", function () {
        expect(viewModel.PrivacyMessage()).toBe(undefined);
    });

    it(":isChoosingPlanOption is false after initialization", function () {
        expect(viewModel.isChoosingPlanOption()).toBeFalsy();
    });

    it(":isLoadingPlanDetails is false after initialization", function () {
        expect(viewModel.isLoadingPlanDetails()).toBeFalsy();
    });

    it(":proposedPlanHasError is false after initialization", function () {
        expect(viewModel.proposedPlanHasError()).toBeFalsy();
    });
    
    it(":changeUserUrl set correctly", function () {
        viewModel.IsAdminUser(true);
        viewModel.PersonId("0001234");
        expect(viewModel.changeUserUrl()).toBe(Ellucian.Finance.MakeAPayment.ActionUrls.mapChangeUserUrl);
    });

    it(":hasErrors is false after initialization", function () {
        expect(viewModel.hasErrors()).toBeFalsy();
    });

    it(":errorChange is undefined after initialization", function () {
        expect(viewModel.errorChange()).toBe(undefined);
    });

    it(":currentStudentInfo is null after initialization", function () {
        expect(viewModel.currentStudentInfo()).toBe(null);
    });

    it(":currentStudentInfo is blank when IsAdminUser is true, PersonId and PersonName are initialized", function () {
        viewModel.IsAdminUser(true);
        expect(viewModel.currentStudentInfo()).toBe(undefined + " " + undefined);
    });

    it(":currentStudentInfo is PersonId + ' ' + PersonName when IsAdminUser is true and PersonId/PersonName are set", function () {
        viewModel.IsAdminUser(true);
        viewModel.PersonId(data.PersonId);
        viewModel.PersonName(data.PersonName);
        expect(viewModel.currentStudentInfo()).toBe(data.PersonId + " " + data.PersonName);
    });

    it(":currencySymbol is null after initialization", function () {
        var expected = Globalize.culture().numberFormat.currency.symbol;
        expect(viewModel.currencySymbol()).toBe(expected);
    });

    it(":validPaymentMethodSelected is false after initialization", function () {
        expect(viewModel.validPaymentMethodSelected()).toBeFalsy();
    });

    it(":validPaymentMethodSelected is true when selectedPaymentMethod is set", function () {
        viewModel.SelectedPaymentMethod(data.AvailablePaymentMethods[0]);
        expect(viewModel.validPaymentMethodSelected()).toBeTruthy();
    });

    it(":globalErrorMessage is null after initialization", function () {
        expect(viewModel.globalErrorMessage()).toBe(null);
    });

    it(":globalErrorMessage is empty array when there are no errors", function () {
        viewModel.showUI(true);
        expect(viewModel.globalErrorMessage().constructor).toBe(Array);
        expect(viewModel.globalErrorMessage().length).toBe(0);
    });

    it(":globalErrorMessage contains multiple payment groups error message when multiple payment groups are selected", function () {
        viewModel.showUI(true);

        spyOn(viewModel, 'errorChange').and.returnValue(true);
        spyOn(viewModel, 'checkForMultiplePaymentGroups').and.returnValue(true);
        spyOn(viewModel, 'checkForInputErrors').and.returnValue(false);

        viewModel.hasErrors(true);

        expect(viewModel.globalErrorMessage().constructor).toBe(Array);
        expect(viewModel.globalErrorMessage().length).toBe(1);
        expect(viewModel.globalErrorMessage()[0]).toBe(multipleDistributionsMessage);
    });

    it(":globalErrorMessage contains input error message when there are input errors", function () {
        viewModel.showUI(true);

        spyOn(viewModel, 'errorChange').and.returnValue(true);
        spyOn(viewModel, 'checkForMultiplePaymentGroups').and.returnValue(false);
        spyOn(viewModel, 'checkForInputErrors').and.returnValue(true);

        viewModel.hasErrors(true);

        expect(viewModel.globalErrorMessage().constructor).toBe(Array);
        expect(viewModel.globalErrorMessage().length).toBe(1);
        expect(viewModel.globalErrorMessage()[0]).toBe(correctErrorsMessage);
    });

    it(":globalErrorMessage contains multiple payment groups and input error messages when multiple payment groups are selected and there are input errors", function () {
        viewModel.showUI(true);

        spyOn(viewModel, 'errorChange').and.returnValue(true);
        spyOn(viewModel, 'checkForMultiplePaymentGroups').and.returnValue(true);
        spyOn(viewModel, 'checkForInputErrors').and.returnValue(true);

        viewModel.hasErrors(true);

        expect(viewModel.globalErrorMessage().constructor).toBe(Array);
        expect(viewModel.globalErrorMessage().length).toBe(2);
        expect(viewModel.globalErrorMessage()[0]).toBe(multipleDistributionsMessage);
        expect(viewModel.globalErrorMessage()[1]).toBe(correctErrorsMessage);
    });

    it(":totalPayment returns 0 after initialization", function () {
        expect(viewModel.totalPayment()).toBe(0);
    });

    it(":totalPayment returns 0 for PCF mode when there are no payments due", function () {
        viewModel.showUI(true);
        expect(viewModel.totalPayment()).toBe(0);
    });

    it(":totalPayment returns 0 for PCF mode there are no selected payments due", function () {
        viewModel.showUI(true);
        viewModel.IsTermDisplay(false);
        viewModel.PeriodModels = ko.observableArray([{
            Terms: ko.observableArray([{
                PaymentsDue: ko.observableArray([
                    {
                        IsSelected: ko.observable(false),
                        PaymentGroup: ko.observable("ABCDEFG"),
                        Description: ko.observable("Student Receivable"),
                        DueDate: ko.observable(null),
                        DueAmount: ko.observable(250.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("01"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable(""),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(250.00),
                        Id: ko.observable("Payment 1"),
                    },
                    {
                        IsSelected: ko.observable(false),
                        PaymentGroup: ko.observable("ABCDEFG"),
                        Description: ko.observable("Continuing Ed Receivable"),
                        DueDate: ko.observable(null),
                        DueAmount: ko.observable(500.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("02"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable(""),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(500.00),
                        Id: ko.observable("Payment 2"),
                    }
                ])
            }])
        }]);

        var global = jasmine.getGlobal();
        spyOn(global, 'parseDollarAmount').and.callThrough();

        expect(global.parseDollarAmount).not.toHaveBeenCalled();
        expect(viewModel.totalPayment()).toBe(0);
    });

    it(":totalPayment returns 0 for Term mode there are no payments due", function () {
        viewModel.showUI(true);
        viewModel.IsTermDisplay(true);
        expect(viewModel.totalPayment()).toBe(0);
    });

    it(":totalPayment returns 0 for Term mode there are no selected payments due", function () {
        viewModel.showUI(true);
        viewModel.IsTermDisplay(true);
        viewModel.TermModels = ko.observableArray([{
            PaymentsDue: ko.observableArray([
                {
                    IsSelected: ko.observable(false),
                    PaymentGroup: ko.observable("ABCDEFG"),
                    Description: ko.observable("Student Receivable"),
                    DueDate: ko.observable(null),
                    DueAmount: ko.observable(250.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("01"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable(""),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(250.00),
                    Id: ko.observable("Payment 1"),
                    amountToPay: ko.observable(250.00)
                },
                {
                    IsSelected: ko.observable(false),
                    PaymentGroup: ko.observable("ABCDEFG"),
                    Description: ko.observable("Continuing Ed Receivable"),
                    DueDate: ko.observable(null),
                    DueAmount: ko.observable(500.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("02"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable(""),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(500.00),
                    Id: ko.observable("Payment 2"),
                    amountToPay: ko.observable(500.00)
                }
            ])
        }]);

        var global = jasmine.getGlobal();
        spyOn(global, 'parseDollarAmount').and.callThrough();

        expect(global.parseDollarAmount).not.toHaveBeenCalled();
        expect(viewModel.totalPayment()).toBe(0);
    });

    it(":checkForMultiplePaymentGroups returns false after initialization", function () {
        var multipleGroups = viewModel.checkForMultiplePaymentGroups();
        expect(multipleGroups).toBeFalsy();
    });

    it(":checkForMultiplePaymentGroups returns true for PCF mode when multiple payment groups are selected", function () {
        viewModel.IsTermDisplay(false);
        viewModel.PeriodModels = ko.observableArray([{
            Terms: ko.observableArray([{
                PaymentsDue: ko.observableArray([
                    {
                        IsSelected: ko.observable(true),
                        PaymentGroup: ko.observable("HIJKLMN"),
                        Description: ko.observable("Student Receivable"),
                        DueDate: ko.observable(null),
                        DueAmount: ko.observable(250.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("01"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable(""),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(250.00),
                        Id: ko.observable("Payment 1"),
                    }
                ])
            },
            {
                PaymentsDue: ko.observableArray([{
                        IsSelected: ko.observable(true),
                        PaymentGroup: ko.observable("ABCDEFG"),
                        Description: ko.observable("Continuing Ed Receivable"),
                        DueDate: ko.observable(null),
                        DueAmount: ko.observable(500.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("02"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable(""),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(500.00),
                        Id: ko.observable("Payment 2"),
                    }
                ])
            }])
        }]);

        var global = jasmine.getGlobal();
        spyOn(global, 'onlyUnique').and.callThrough();

        var multipleGroups = viewModel.checkForMultiplePaymentGroups();
        expect(global.onlyUnique).toHaveBeenCalled();
        expect(multipleGroups).toBeTruthy();
    });

    it(":checkForMultiplePaymentGroups returns false for PCF mode when only one payment group is selected", function () {
        viewModel.IsTermDisplay(false);
        viewModel.PeriodModels = ko.observableArray([{
            Terms: ko.observableArray([{
                PaymentsDue: ko.observableArray([
                    {
                        IsSelected: ko.observable(true),
                        PaymentGroup: ko.observable("HIJKLMN"),
                        Description: ko.observable("Student Receivable"),
                        DueDate: ko.observable(null),
                        DueAmount: ko.observable(250.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("01"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable(""),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(250.00),
                        Id: ko.observable("Payment 1"),
                    }
                ])
            },
            {
                PaymentsDue: ko.observableArray([{
                    IsSelected: ko.observable(true),
                    PaymentGroup: ko.observable("HIJKLMN"),
                    Description: ko.observable("Continuing Ed Receivable"),
                    DueDate: ko.observable(null),
                    DueAmount: ko.observable(500.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("02"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable(""),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(500.00),
                    Id: ko.observable("Payment 2"),
                }
                ])
            }])
        }]);

        var global = jasmine.getGlobal();
        spyOn(global, 'onlyUnique').and.callThrough();

        var multipleGroups = viewModel.checkForMultiplePaymentGroups();
        expect(global.onlyUnique).toHaveBeenCalled();
        expect(multipleGroups).toBeFalsy();
    });

    it(":checkForMultiplePaymentGroups returns true for Term mode when multiple payment groups are selected", function () {
        viewModel.IsTermDisplay(true);
        viewModel.TermModels = ko.observableArray([{
                PaymentsDue: ko.observableArray([
                    {
                        IsSelected: ko.observable(true),
                        PaymentGroup: ko.observable("HIJKLMN"),
                        Description: ko.observable("Student Receivable"),
                        DueDate: ko.observable(null),
                        DueAmount: ko.observable(250.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("01"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable(""),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(250.00),
                        Id: ko.observable("Payment 1"),
                    },
                    {
                        IsSelected: ko.observable(true),
                        PaymentGroup: ko.observable("ABCDEFG"),
                        Description: ko.observable("Continuing Ed Receivable"),
                        DueDate: ko.observable(null),
                        DueAmount: ko.observable(500.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("02"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable(""),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(500.00),
                        Id: ko.observable("Payment 2"),
                    }
                ])
            }]
        );

        var global = jasmine.getGlobal();
        spyOn(global, 'onlyUnique').and.callThrough();

        var multipleGroups = viewModel.checkForMultiplePaymentGroups();
        expect(global.onlyUnique).toHaveBeenCalled();
        expect(multipleGroups).toBeTruthy();
    });

    it(":checkForMultiplePaymentGroups returns false for Term mode when only one payment group is selected", function () {
        viewModel.IsTermDisplay(true);
        viewModel.TermModels = ko.observableArray([{
            PaymentsDue: ko.observableArray([
                {
                    IsSelected: ko.observable(true),
                    PaymentGroup: ko.observable("HIJKLMN"),
                    Description: ko.observable("Student Receivable"),
                    DueDate: ko.observable(null),
                    DueAmount: ko.observable(250.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("01"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable(""),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(250.00),
                    Id: ko.observable("Payment 1"),
                },
                {
                    IsSelected: ko.observable(true),
                    PaymentGroup: ko.observable("HIJKLMN"),
                    Description: ko.observable("Continuing Ed Receivable"),
                    DueDate: ko.observable(null),
                    DueAmount: ko.observable(500.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("02"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable(""),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(500.00),
                    Id: ko.observable("Payment 2"),
                }
            ])
        }]
        );

        var global = jasmine.getGlobal();
        spyOn(global, 'onlyUnique').and.callThrough();

        var multipleGroups = viewModel.checkForMultiplePaymentGroups();
        expect(global.onlyUnique).toHaveBeenCalled();
        expect(multipleGroups).toBeFalsy();
    });

    it(":checkForInputErrors returns false after initialization", function () {
        var inputErrors = viewModel.checkForInputErrors();
        expect(inputErrors).toBeFalsy();
    });

    it(":preventPayPlanOutOfOrderErrors for PCF mode disables future payment plan payments when an earlier payment plan payment is not selected", function () {
        viewModel.IsTermDisplay(false);
        viewModel.PeriodModels = ko.observableArray([{
            Terms: ko.observableArray([{
                PaymentsDue: ko.observableArray([
                    {
                        IsSelected: ko.observable(false),
                        isDisabled: ko.observable(false),
                        PaymentGroup: ko.observable("HIJKLMN"),
                        Description: ko.observable("Student Receivable"),
                        DueDate: ko.observable(new Date(2015, 1, 7)),
                        DueAmount: ko.observable(250.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("01"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable("123"),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(250.00),
                        Id: ko.observable("Payment 1"),
                    },
                    {
                        IsSelected: ko.observable(true),
                        isDisabled: ko.observable(false),
                        PaymentGroup: ko.observable("HIJKLMN"),
                        Description: ko.observable("Student Receivable"),
                        DueDate: ko.observable(new Date(2015, 1, 14)),
                        DueAmount: ko.observable(250.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("01"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable("123"),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(250.00),
                        Id: ko.observable("Payment 1"),
                    },
                    {
                        IsSelected: ko.observable(true),
                        isDisabled: ko.observable(false),
                        PaymentGroup: ko.observable("HIJKLMN"),
                        Description: ko.observable("Student Receivable"),
                        DueDate: ko.observable(new Date(2015, 1, 21)),
                        DueAmount: ko.observable(250.00),
                        PaidAmount: ko.observable(null),
                        ReceivableTypeCode: ko.observable("01"),
                        InvoiceId: ko.observable(""),
                        PaymentPlanId: ko.observable("123"),
                        DepositDueId: ko.observable(""),
                        IsCredit: ko.observable(false),
                        IsOverdue: ko.observable(false),
                        IsEditable: ko.observable(false),
                        MinimumPayment: ko.observable(250.00),
                        Id: ko.observable("Payment 1"),
                    }
                ])
            }])
        }]);

        var global = jasmine.getGlobal();
        spyOn(global, 'getTermPlanItems').and.callThrough();

        viewModel.preventPayPlanOutOfOrderErrors();
        expect(global.getTermPlanItems).toHaveBeenCalled();
        expect(viewModel.PeriodModels()[0].Terms()[0].PaymentsDue()[0].isDisabled()).toBeFalsy();
        expect(viewModel.PeriodModels()[0].Terms()[0].PaymentsDue()[1].isDisabled()).toBeTruthy();
        expect(viewModel.PeriodModels()[0].Terms()[0].PaymentsDue()[1].IsSelected()).toBeFalsy();
        expect(viewModel.PeriodModels()[0].Terms()[0].PaymentsDue()[2].isDisabled()).toBeTruthy();
        expect(viewModel.PeriodModels()[0].Terms()[0].PaymentsDue()[2].IsSelected()).toBeFalsy();
    });

    it(":preventPayPlanOutOfOrderErrors for Term mode disables future payment plan payments when an earlier payment plan payment is not selected", function () {
        viewModel.IsTermDisplay(true);
        viewModel.TermModels = ko.observableArray([{
            PaymentsDue: ko.observableArray([
                {
                    IsSelected: ko.observable(false),
                    isDisabled: ko.observable(false),
                    PaymentGroup: ko.observable("HIJKLMN"),
                    Description: ko.observable("Student Receivable"),
                    DueDate: ko.observable(new Date(2015, 1, 7)),
                    DueAmount: ko.observable(250.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("01"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable("123"),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(250.00),
                    Id: ko.observable("Payment 1"),
                },
                {
                    IsSelected: ko.observable(true),
                    isDisabled: ko.observable(false),
                    PaymentGroup: ko.observable("HIJKLMN"),
                    Description: ko.observable("Student Receivable"),
                    DueDate: ko.observable(new Date(2015, 1, 14)),
                    DueAmount: ko.observable(250.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("01"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable("123"),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(250.00),
                    Id: ko.observable("Payment 1"),
                },
                {
                    IsSelected: ko.observable(true),
                    isDisabled: ko.observable(false),
                    PaymentGroup: ko.observable("HIJKLMN"),
                    Description: ko.observable("Student Receivable"),
                    DueDate: ko.observable(new Date(2015, 1, 21)),
                    DueAmount: ko.observable(250.00),
                    PaidAmount: ko.observable(null),
                    ReceivableTypeCode: ko.observable("01"),
                    InvoiceId: ko.observable(""),
                    PaymentPlanId: ko.observable("123"),
                    DepositDueId: ko.observable(""),
                    IsCredit: ko.observable(false),
                    IsOverdue: ko.observable(false),
                    IsEditable: ko.observable(false),
                    MinimumPayment: ko.observable(250.00),
                    Id: ko.observable("Payment 1"),
                }
            ])
        }]);

        var global = jasmine.getGlobal();
        spyOn(global, 'getTermPlanItems').and.callThrough();

        viewModel.preventPayPlanOutOfOrderErrors();
        expect(global.getTermPlanItems).toHaveBeenCalled();
        expect(viewModel.TermModels()[0].PaymentsDue()[0].isDisabled()).toBeFalsy();
        expect(viewModel.TermModels()[0].PaymentsDue()[1].isDisabled()).toBeTruthy();
        expect(viewModel.TermModels()[0].PaymentsDue()[1].IsSelected()).toBeFalsy();
        expect(viewModel.TermModels()[0].PaymentsDue()[2].isDisabled()).toBeTruthy();
        expect(viewModel.TermModels()[0].PaymentsDue()[2].IsSelected()).toBeFalsy();
    });

    it(":checkForErrors sets hasErrors to false when there are no payment group or input errors", function () {

        var global = jasmine.getGlobal();

        spyOn(viewModel, 'checkForMultiplePaymentGroups');
        spyOn(viewModel, 'checkForInputErrors');
        spyOn(global, 'formatAmounts');

        viewModel.checkForErrors();

        expect(viewModel.checkForMultiplePaymentGroups).toHaveBeenCalled();
        expect(viewModel.checkForInputErrors).toHaveBeenCalled();
        expect(global.formatAmounts).toHaveBeenCalled();
        expect(viewModel.hasErrors()).toBeFalsy();
    });

    it(":checkForErrors sets hasErrors to true when there are payment group errors", function () {

        var global = jasmine.getGlobal();

        spyOn(viewModel, 'checkForMultiplePaymentGroups').and.returnValue(true);
        spyOn(viewModel, 'checkForInputErrors').and.returnValue(false);
        spyOn(global, 'formatAmounts');

        viewModel.checkForErrors();

        expect(viewModel.checkForMultiplePaymentGroups).toHaveBeenCalled();
        expect(viewModel.checkForInputErrors).not.toHaveBeenCalled();
        expect(global.formatAmounts).toHaveBeenCalled();
        expect(viewModel.hasErrors()).toBeTruthy();
    });

    it(":checkForErrors sets hasErrors to true when there are input errors", function () {

        var global = jasmine.getGlobal();

        spyOn(viewModel, 'checkForMultiplePaymentGroups').and.returnValue(false);
        spyOn(viewModel, 'checkForInputErrors').and.returnValue(true);
        spyOn(global, 'formatAmounts');

        viewModel.checkForErrors();

        expect(viewModel.checkForMultiplePaymentGroups).toHaveBeenCalled();
        expect(viewModel.checkForInputErrors).toHaveBeenCalled();
        expect(global.formatAmounts).toHaveBeenCalled();
        expect(viewModel.hasErrors()).toBeTruthy();
    });

    it(":beginPayment sets isLoading to true and calls ko.toJS with viewModel when called", function () {
        var global = jasmine.getGlobal();

        spyOn(ko.mapping, 'toJS').and.callThrough();
        spyOn(ko.utils, 'postJson').and.callThrough();
        spyOn(global, 'sanitizeFormData').and.callThrough();

        viewModel.beginPayment();
        expect(ko.mapping.toJS).toHaveBeenCalled();
        expect(global.sanitizeFormData).toHaveBeenCalledWith(viewModel.AlertMessage());
        expect(global.sanitizeFormData).toHaveBeenCalledWith(viewModel.PaymentPlanEligibilityText());
        expect(ko.utils.postJson).toHaveBeenCalled();
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":beginPaymentPlan sets payPlanDialogIsDisplayed to true on success when Options is not null and has values", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                "Message":null,
                "Options":[{
                    "TermCode": "2016/FA",
                    "TermDescription": "2016 Fall Term",
                    "DueAmount": 1000,
                    "PaidAmount": 0,
                    "SanitizedId": "2016_FA",
                    "PaymentsDue": [
                        {
                            "IsSelected": false,
                            "Description": "Student Receivables",
                            "PaymentGroup": "BANK",
                            "DueDate": "12-31-2016",
                            "DueAmount": 1000,
                            "PaidAmount": 0,
                            "ReceivableTypeCode": "01",
                            "InvoiceId": null,
                            "DepositDueId": null,
                            "PaymentPlanId": null,
                            "IsCredit": false,
                            "IsOverdue": false,
                            "IsEditable": true,
                            "MinimumPayment": 1,
                            "Id": "2016_FA_01",
                            "IsPayable": true,
                            "IsEligibleForPaymentPlan": true,
                            "PaymentPlanTemplateId": "DEFAULT"
                        }
                    ]
                }]
            });
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        spyOn(viewModel, "checkForMobile");
        spyOn(ko.mapping, "fromJS");
        var global = jasmine.getGlobal();
        viewModel.beginPaymentPlan();

        expect(viewModel.checkForMobile).toHaveBeenCalled();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
        expect(viewModel.isChoosingPlanOption()).toBeTruthy();
        expect(viewModel.payPlanDialogIsDisplayed()).toBeTruthy();
        expect(viewModel.payPlanDialogIsLoading()).toBeFalsy();
        expect(viewModel.noPaymentPlanOptionsMessage()).toBe(null);
    });

    it(":beginPaymentPlan sets noPaymentPlanOptionsMessage to true on success when Options is null", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                "Message": "No options present",
                "Options": null
            });
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        spyOn(viewModel, "checkForMobile");
        spyOn(ko.mapping, "fromJS");
        var global = jasmine.getGlobal();
        viewModel.beginPaymentPlan();

        expect(viewModel.checkForMobile).toHaveBeenCalled();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(ko.mapping.fromJS).not.toHaveBeenCalled();
        expect(viewModel.isChoosingPlanOption()).toBeTruthy();
        expect(viewModel.payPlanDialogIsDisplayed()).toBeTruthy();
        expect(viewModel.payPlanDialogIsLoading()).toBeFalsy();
        expect(viewModel.noPaymentPlanOptionsMessage()).toBe("No options present");
    });

    it(":beginPaymentPlan sets noPaymentPlanOptionsMessage to true on success when Options is empty", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                "Message": "No options present",
                "Options": []
            });
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        spyOn(viewModel, "checkForMobile");
        spyOn(ko.mapping, "fromJS");
        var global = jasmine.getGlobal();
        viewModel.beginPaymentPlan();

        expect(viewModel.checkForMobile).toHaveBeenCalled();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(ko.mapping.fromJS).not.toHaveBeenCalled();
        expect(viewModel.isChoosingPlanOption()).toBeTruthy();
        expect(viewModel.payPlanDialogIsDisplayed()).toBeTruthy();
        expect(viewModel.payPlanDialogIsLoading()).toBeFalsy();
        expect(viewModel.noPaymentPlanOptionsMessage()).toBe("No options present");
    });

    it(":beginPaymentPlan sets payPlanDialogIsDisplayed to true on success when Options is undefined", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                    "TermCode": "2016/FA",
                    "TermDescription": "2016 Fall Term",
                    "DueAmount": 1000,
                    "PaidAmount": 0,
                    "SanitizedId": "2016_FA",
                    "PaymentsDue": [
                        {
                            "IsSelected": false,
                            "Description": "Student Receivables",
                            "PaymentGroup": "BANK",
                            "DueDate": "12-31-2016",
                            "DueAmount": 1000,
                            "PaidAmount": 0,
                            "ReceivableTypeCode": "01",
                            "InvoiceId": null,
                            "DepositDueId": null,
                            "PaymentPlanId": null,
                            "IsCredit": false,
                            "IsOverdue": false,
                            "IsEditable": true,
                            "MinimumPayment": 1,
                            "Id": "2016_FA_01",
                            "IsPayable": true,
                            "IsEligibleForPaymentPlan": true,
                            "PaymentPlanTemplateId": "DEFAULT"
                        }
                    ]
                });
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        spyOn(viewModel, "checkForMobile");
        spyOn(ko.mapping, "fromJS");
        var global = jasmine.getGlobal();
        viewModel.beginPaymentPlan();

        expect(viewModel.checkForMobile).toHaveBeenCalled();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
        expect(viewModel.isChoosingPlanOption()).toBeTruthy();
        expect(viewModel.payPlanDialogIsDisplayed()).toBeTruthy();
        expect(viewModel.payPlanDialogIsLoading()).toBeFalsy();
        expect(viewModel.noPaymentPlanOptionsMessage()).toBe(null);
    });


    it(":beginPaymentPlan throws error on failure", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });
        spyOn($.fn, "notificationCenter");

        viewModel.beginPaymentPlan();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: createPaymentPlanUnableToLoadMessage, type: "error" });
        expect(viewModel.payPlanDialogIsDisplayed()).toBeTruthy();
        expect(viewModel.payPlanDialogIsLoading()).toBeFalsy();
    });

    it(":submitPaymentPlan sets payPlanDialogIsDisplayed to true on success", function () {
        spyOn($.fn, "makeTableResponsive");
        spyOn(ko.utils, "postJson");
        var global = jasmine.getGlobal();
        viewModel.selectedPaymentPlanOption(ko.observable({
            IsSelected: ko.observable(false),
            Description: ko.observable("Student Receivables"),
            PaymentGroup: ko.observable("BANK"),
            DueDate: ko.observable("12-31-2016"),
            DueAmount: ko.observable(1000),
            PaidAmount: ko.observable(0),
            ReceivableTypeCode: ko.observable("01"),
            IsCredit: ko.observable(false),
            IsOverdue: ko.observable(false),
            IsEditable: ko.observable(true),
            MinimumPayment: ko.observable(1),
            Id: ko.observable("2016_FA_01"),
            IsPayable: ko.observable(true),
            IsEligibleForPaymentPlan: ko.observable(true),
            PaymentPlanTemplateId: ko.observable("DEFAULT")
        }));
        viewModel.submitPaymentPlan();
        expect(ko.utils.postJson).toHaveBeenCalled();
        expect(viewModel.isLoadingPlanDetails()).toBeTruthy();
        expect(viewModel.payPlanDialogIsDisplayed()).toBeFalsy();
    });

    it(":cancelPaymentPlan resets view model properties appropriately", function () {
        viewModel.cancelPaymentPlan();
        expect(viewModel.payPlanDialogIsDisplayed()).toBeFalsy();
        expect(viewModel.proposedPlanHasError()).toBeFalsy();
        expect(viewModel.selectedPaymentPlanOption()).toBe(null);
        expect(viewModel.isChoosingPlanOption()).toBeFalsy();
    });

    it(":payPlanDialogIsLoading is false after initialization", function () {
        expect(viewModel.payPlanDialogIsLoading()).toBeFalsy();
    });

    it(":payPlanDialogIsDisplayed is false after initialization", function () {
        expect(viewModel.payPlanDialogIsDisplayed()).toBeFalsy();
    });

    it(":selectedPaymentPlanOption is undefined after initialization", function () {
        expect(viewModel.selectedPaymentPlanOption()).toBe(undefined);
    });

    it(":hasPaymentPlanOptions is false when PaymentPlanTermModels is empty", function () {
        viewModel.PaymentPlanTermModels([]);
        viewModel.PaymentPlanPeriodModels(null);
        expect(viewModel.hasPaymentPlanOptions()).toBeFalsy();
    });

    it(":hasPaymentPlanOptions is true when PaymentPlanTermModels is not empty", function () {
        viewModel.PaymentPlanTermModels([{"Id":"ABC"}]);
        viewModel.PaymentPlanPeriodModels(null);
        expect(viewModel.hasPaymentPlanOptions()).toBeTruthy();
    });

    it(":hasPaymentPlanOptions is false when PaymentPlanPeriodModels is empty", function () {
        viewModel.PaymentPlanTermModels(null);
        viewModel.PaymentPlanPeriodModels([]);
        expect(viewModel.hasPaymentPlanOptions()).toBeFalsy();
    });

    it(":hasPaymentPlanOptions is true when PaymentPlanPeriodModels is not empty", function () {
        viewModel.PaymentPlanPeriodModels([{ "Id": "ABC" }]);
        viewModel.PaymentPlanTermModels(null);
        expect(viewModel.hasPaymentPlanOptions()).toBeTruthy();
    });

    it(":paymentPlanCancelButtonLabel is createPaymentPlanDialogCancelButtonLabel when hasPaymentPlanOptions is true", function () {
        viewModel.PaymentPlanPeriodModels([{ "Id": "ABC" }]);
        viewModel.PaymentPlanTermModels(null);
        expect(viewModel.paymentPlanCancelButtonLabel()).toBe(createPaymentPlanDialogCancelButtonLabel);
    });

    it(":paymentPlanCancelButtonLabel is createPaymentPlanDialogCancelButtonLabel when hasPaymentPlanOptions is false", function () {
        viewModel.PaymentPlanTermModels(null);
        viewModel.PaymentPlanPeriodModels([]);
        expect(viewModel.paymentPlanCancelButtonLabel()).toBe(createPaymentPlanOkButtonLabel);
    });

    it(":showContinueButton is true when hasPaymentPlanOptions is true and isChoosingPlanOption is true", function () {
        viewModel.PaymentPlanPeriodModels([{ "Id": "ABC" }]);
        viewModel.PaymentPlanTermModels(null);
        viewModel.isChoosingPlanOption(true);
        expect(viewModel.showContinueButton()).toBeTruthy();
    });

    it(":showContinueButton is false when hasPaymentPlanOptions is false and isChoosingPlanOption is true", function () {
        viewModel.PaymentPlanPeriodModels([]);
        viewModel.PaymentPlanTermModels(null);
        viewModel.isChoosingPlanOption(true);
        expect(viewModel.showContinueButton()).toBeFalsy();
    });

    it(":showContinueButton is true when hasPaymentPlanOptions is true and isChoosingPlanOption is false", function () {
        viewModel.PaymentPlanPeriodModels([{ "Id": "ABC" }]);
        viewModel.PaymentPlanTermModels(null);
        viewModel.isChoosingPlanOption(false);
        expect(viewModel.showContinueButton()).toBeFalsy();
    });

    it(":submitPaymentPlanButton set correctly on initialization", function () {
        expect(viewModel.submitPaymentPlanButton).toEqual({
            id: "submit-payment-plan-dialog-button",
            isPrimary: true,
            title: Ellucian.MakePayment.ButtonLabels.createPaymentPlanContinueButtonLabel,
            enabled: viewModel.selectedPaymentPlanOption,
            visible: viewModel.showContinueButton,
            callback: viewModel.submitPaymentPlan
        });
    });

    it(":cancelPaymentPlanButton set correctly on initialization", function () {
        expect(viewModel.cancelPaymentPlanButton).toEqual({
            id: "close-payment-plan-dialog-button",
            isPrimary: false,
            title: viewModel.paymentPlanCancelButtonLabel,
            callback: viewModel.cancelPaymentPlan
        });
    });
});

describe("makeAPayment.ECheckEntryViewModel", function () {
    var viewModel, data;
               
    beforeEach(function () {
        data = {
            "Payment": {
                "AmountToPay": 1000,
                "CheckDetails": {
                    "FirstName": "John",
                    "LastName": "Smith",
                    "BillingAddress1": "123 Main Street",
                    "City": "Fairfax",
                    "State": "VA",
                    "ZipCode": "22033",
                    "EmailAddress": "john.smith@ellucianuniversity.edu",
                },
                "ConvenienceFee": "CF5",
                "ConvenienceFeeAmount": 5,
                "ConvenienceFeeGeneralLedgerNumber": "110101000000010100",
                "Distribution": "BANK",
                "PaymentItems": [
                ],
                "PayMethod": "ECHK",
                "PersonId": "0003315",
                "ProviderAccount": "PROVCC",
                "ReturnToOriginUrl": "http://www.ellucianuniversity.edu/return-to-origin",
                "ReturnUrl": "http://www.ellucianuniversity.edu/return",
            },
            "StateList": [
                {
                    "Code": "OH",
                    "Description": "Ohio",
                    "Country": "USA"
                },
                {
                    "Code": "VA",
                    "Description": "Virginia",
                    "Country": "USA"
                }
            ],
            "UseGuaranteedChecks": false,
        };
        viewModel = new makeAPayment.ECheckEntryViewModel(data);
    });

    afterEach(function () {
        data = null;
        viewModel = null;
    });

    it(":isLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":actionThrobberMessage set to makePaymentEcheckActionThrobberMessage value after initialization", function () {
        expect(viewModel.actionThrobberMessage()).toBe(makePaymentEcheckActionThrobberMessage);
    });

    it(":payerName is payer first name + ' ' + payer last name", function () {
        viewModel.Payment.CheckDetails.FirstName(data.Payment.FirstName);
        viewModel.Payment.CheckDetails.LastName(data.Payment.LastName);

        expect(viewModel.payerName()).toBe(data.Payment.FirstName + " " + data.Payment.LastName);
    });

    it(":paymentAmount is currency formatted string for AmountToPay", function () {
        var expected = formatAsCurrency(data.Payment.AmountToPay);
        viewModel.Payment.AmountToPay(data.Payment.AmountToPay);

        expect(viewModel.paymentAmount()).toBe(expected);
    });

    it(":userRoutingNumber is routingNumberPlaceholderString when AbaRoutingNumber is null", function () {
        viewModel.Payment.CheckDetails.AbaRoutingNumber(null);
        expect(viewModel.userRoutingNumber()).toBe(routingNumberPlaceholderString);
    });

    it(":userRoutingNumber is routingNumberPlaceholderString when AbaRoutingNumber is empty string", function () {
        viewModel.Payment.CheckDetails.AbaRoutingNumber("");
        expect(viewModel.userRoutingNumber()).toBe(routingNumberPlaceholderString);
    });

    it(":userRoutingNumber is AbaRoutingNumber when AbaRoutingNumber is not null or empty", function () {
        viewModel.Payment.CheckDetails.AbaRoutingNumber("062203298");
        expect(viewModel.userRoutingNumber()).toBe("062203298");
    });

    it(":userBankAccountNumber is bankAccountPlaceholderString when AbaRoutingNumber is null", function () {
        viewModel.Payment.CheckDetails.BankAccountNumber(null);
        expect(viewModel.userBankAccountNumber()).toBe(bankAccountPlaceholderString);
    });

    it(":userBankAccountNumber is bankAccountPlaceholderString when AbaRoutingNumber is empty string", function () {
        viewModel.Payment.CheckDetails.BankAccountNumber("");
        expect(viewModel.userBankAccountNumber()).toBe(bankAccountPlaceholderString);
    });

    it(":userBankAccountNumber is BankAccountNumber when BankAccountNumber is not null or empty", function () {
        viewModel.Payment.CheckDetails.BankAccountNumber("1234554321");
        expect(viewModel.userBankAccountNumber()).toBe("1234554321");
    });

    it(":userBankAccountNumberLast4Digits is bankAccountPlaceholderString when AbaRoutingNumber is null", function () {
        viewModel.Payment.CheckDetails.BankAccountNumber(null);
        expect(viewModel.userBankAccountNumberLast4Digits()).toBe(last4DigitsPlaceholderString);
    });

    it(":userBankAccountNumberLast4Digits is bankAccountPlaceholderString when AbaRoutingNumber is less than 4 characters", function () {
        viewModel.Payment.CheckDetails.BankAccountNumber("");
        expect(viewModel.userBankAccountNumberLast4Digits()).toBe(last4DigitsPlaceholderString);
    });

    it(":userBankAccountNumberLast4Digits is BankAccountNumber when BankAccountNumber is not null or empty", function () {
        viewModel.Payment.CheckDetails.BankAccountNumber("1234554321");
        expect(viewModel.userBankAccountNumberLast4Digits()).toBe("4321");
    });

    it(":achAuthorizationText uses computed properties in its computation", function () {
        var todaysDate = new Date();
        var month = todaysDate.getMonth();
        var day = todaysDate.getDate();
        var year = todaysDate.getFullYear();
        var todayString = Globalize.format(new Date(year, month, day), "d");
        var tomorrowsDate = new Date(todaysDate.getTime() + 24 * 60 * 60 * 1000);
        var tomorrowString = Globalize.format(tomorrowsDate, "d");
        var expected = achAuthorizationTextString.format(
                viewModel.payerName(),
                viewModel.paymentAmount(),
                todayString,
                viewModel.userRoutingNumber(),
                viewModel.userBankAccountNumber(),
                viewModel.userBankAccountNumberLast4Digits(),
                tomorrowString);
        expect(viewModel.achAuthorizationText()).toBe(expected);
    });

    it(":allFieldsValid is false after initialization", function () {
        expect(viewModel.allFieldsValid()).toBeFalsy();
    });

    it(":allFieldsValid is true when there are no errors", function () {
        viewModel.Payment.CheckDetails.BankAccountNumber("1234554321");
        viewModel.Payment.CheckDetails.AbaRoutingNumber("062203298");
        expect(viewModel.allFieldsValid()).toBeTruthy();
    });

    it(":processECheck sets isLoading to true and calls ko.toJS with viewModel when called", function () {
        spyOn(ko, 'toJS').and.callThrough();
        spyOn(ko.utils, 'postJson').and.callThrough();

        viewModel.processECheck();
        expect(ko.toJS).toHaveBeenCalledWith(viewModel);
        expect(ko.utils.postJson).toHaveBeenCalled();
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":bankingInformationCloseButton set correctlt on initialization", function () {
        expect(viewModel.bankingInformationCloseButton).toEqual({
            id: 'close-banking-information-dialog-button',
            isPrimary: false,
            title: Ellucian.MakePayment.ButtonLabels.bankingInformationCloseButtonLabel,
            callback: viewModel.closeBankingInformation
        });
    });
});

describe("makeAPayment.PaymentReviewViewModel", function () {
    var viewModel, data;

    beforeEach(function () {
        data = {
            "LineItems": [{
                "Description": "Student Payment",
                "Amount": 500.00
            }],
            "PaymentMethod": {
                "Description": "Electronic Check",
                "InternalCode": "ECHK",
                "Type": "CK"
            },
            "Message": "This is a message",
            "PaymentNumber": 0,
            "PaymentCount": 0,
            "Payment": {
                "AmountToPay": 505.00,
                "ConvenienceFee": "CF5",
                "ConvenienceFeeAmount": 5.00,
                "ConvenienceFeeGeneralLedgerNumber": "110101000000010100",
                "Distribution": "BANK",
                "PayMethod": "ECHK",
                "PaymentItems": [{
                    "AccountType": "01",
                    "DepositDueId": "",
                    "Description": "Student Payment",
                    "InvoiceId": "1234",
                    "Overdue": false,
                    "PaymentAmount": 500.00,
                    "PaymentComplete": true,
                    "PaymentControlId": "",
                    "PaymentPlanId": "",
                    "Term": "2014/FA"
                }],
                "PersonId": "0001234",
                "ProviderAccount": "PPCC",
                "ReturnUrl": "http://www.ellucianuniversity.edu/Finance/Payments/Acknowledgement",
                "ReturnToOriginUrl": "http://www.ellucianuniversity.edu/Finance/Payments/Acknowledgement2"
            }
        };
        viewModel = new makeAPayment.PaymentReviewViewModel(data);
    });

    afterEach(function () {
        data = null;
        viewModel = null;
    });

    it(":isLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":actionThrobberMessage set to makePaymentReviewThrobberMessage value after initialization", function () {
        expect(viewModel.actionThrobberMessage()).toBe(makePaymentReviewActionThrobberMessage);
    });

    it(":PaymentCountMessage set to formatted paymentReviewCountMessage after initialization", function () {
        expect(viewModel.PaymentCountMessage()).toBe(paymentReviewCountMessage.format(data.PaymentNumber, data.PaymentCount));
    });

    it(":ProcessPayment sets viewModel.Message to empty string, sets isLoading to true, and calls ko.toJS with viewModel when called", function () {
        spyOn(ko, 'toJS').and.callThrough();
        spyOn(ko.utils, 'postJson').and.callThrough();

        viewModel.ProcessPayment();
        expect(ko.toJS).toHaveBeenCalledWith(viewModel);
        expect(ko.utils.postJson).toHaveBeenCalled();
        expect(viewModel.isLoading()).toBeTruthy();
        expect(viewModel.Message).toBe("");
    });
});

describe("makeAPayment.PaymentAcknowledgementViewModel", function () {
    var viewModel, data;
               
    beforeEach(function () {
        data = {
            "Payments": [
                {
                    Description : "Payment 1",
                    Location : "MC",
                    LocationDescription : "Main Campus",
                    NetAmount : "500",
                    PaymentControlId : "",
                    PaymentDescription : "Student Receivables",
                    PersonId : "0011521",
                    PersonName : "Lillie E. Puckett",
                    Term : "2015/FA",
                    TermDescription : "2015 Fall Term",
                    Type : "01",
                },
                {
                    Description: "Payment 2",
                    Location: "MC",
                    LocationDescription: "Main Campus",
                    NetAmount: "300",
                    PaymentControlId: "",
                    PaymentDescription: "Continuing Ed Receivables",
                    PersonId: "0011521",
                    PersonName: "Lillie E. Puckett",
                    Term: "2015/FA",
                    TermDescription: "2015 Fall Term",
                    Type: "02",
                }],
            "Deposits": [
                {
                    Description: "Deposit 1",
                    Location: "MC",
                    LocationDescription: "Main Campus",
                    NetAmount: "500",
                    PersonId: "0011521",
                    PersonName: "Lillie E. Puckett",
                    Term: "2015/FA",
                    TermDescription: "2015 Fall Term",
                    Type: "MEALS",
                },
                {
                    Description: "Deposit 2",
                    Location: "MC",
                    LocationDescription: "Main Campus",
                    NetAmount: "300",
                    PersonId: "0011521",
                    PersonName: "Lillie E. Puckett",
                    Term: "2015/FA",
                    TermDescription: "2015 Fall Term",
                    Type: "TUIFE",
                }],
            "OtherItems": [
                {
                    Code: "BKNEW",
                    Description: "New Books",
                    Location: "MC",
                    LocationDescription: "Main Campus",
                    NetAmount: "375",
                },
                {
                    Code: "BKUSE",
                    Description: "Used Books",
                    Location: "MC",
                    LocationDescription: "Main Campus",
                    NetAmount: "225",
                }],
            "Fees": [
                {
                    Amount: "5",
                    Code: "CF5",
                    Description: "$5.00 Flat Fee"
                }],
            "PaymentsTendered": [
                {
                    ConfirmationNumber: "CONF1234",
                    ControlNumber: "1234",
                    PayMethodCode: "ECHK",
                    PayMethodDescription: "Electronic Check",
                    TransactionAmount: "2205",
                    TransactionDescription: "Check Payment",
                    TransactionNumber: "12345",
                }],
            "TransactionId": "123",
            "ReceiptId": "124",
            "ChangeReturned": 0,
        };
        viewModel = new makeAPayment.PaymentAcknowledgementViewModel(data);
    });

    afterEach(function () {
        data = null;
        viewModel = null;
    });

    it(":isLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":actionThrobberMessage set to makePaymentAcknowledgementActionThrobberMessage value after initialization", function () {
        expect(viewModel.actionThrobberMessage()).toBe(makePaymentAcknowledgementActionThrobberMessage);
    });

    it(":displayPayments is false when viewModel.Payments is null", function () {
        viewModel.Payments(null);
        expect(viewModel.displayPayments()).toBeFalsy();
    });

    it(":displayPayments is true when viewModel.Payments is not empty", function () {
        viewModel.Payments = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.displayPayments()).toBeTruthy();
    });

    it(":displayDeposits is false when viewModel.Deposits is null", function () {
        viewModel.Deposits(null);
        expect(viewModel.displayDeposits()).toBeFalsy();
    });

    it(":displayDeposits is true when viewModel.Deposits is not empty", function () {
        viewModel.Deposits = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.displayDeposits()).toBeTruthy();
    });

    it(":displayOtherItems is false when viewModel.OtherItems is null", function () {
        viewModel.OtherItems(null);
        expect(viewModel.displayOtherItems()).toBeFalsy();
    });

    it(":displayOtherItems is true when viewModel.OtherItems is not empty", function () {
        viewModel.OtherItems = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.displayOtherItems()).toBeTruthy();
    });

    it(":displayFees is false when viewModel.Fees is null", function () {
        viewModel.Fees(null);
        expect(viewModel.displayFees()).toBeFalsy();
    });

    it(":displayFees is true when viewModel.Fees is not empty", function () {
        viewModel.Fees = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.displayFees()).toBeTruthy();
    });

    it(":displayPaymentsTendered is false when viewModel.PaymentsTendered is null", function () {
        viewModel.PaymentsTendered(null);
        expect(viewModel.displayPaymentsTendered()).toBeFalsy();
    });

    it(":displayFees is true when viewModel.PaymentsTendered is not empty", function () {
        viewModel.PaymentsTendered = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.displayPaymentsTendered()).toBeTruthy();
    });

    it(":showPaymentsLocation is false when viewModel.Payments is null", function () {
        viewModel.Payments(null);
        expect(viewModel.showPaymentsLocation()).toBeFalsy();
    });

    it(":showPaymentsLocation is true when viewModel.Payments contains at least one item with a location", function () {
        viewModel.Payments = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.showPaymentsLocation()).toBeTruthy();
    });

    it(":showDepositsLocation is false when viewModel.Deposits is null", function () {
        viewModel.Deposits(null);
        expect(viewModel.showDepositsLocation()).toBeFalsy();
    });

    it(":showDepositsLocation is true when viewModel.Deposits contains at least one item with a location", function () {
        viewModel.Deposits = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.showDepositsLocation()).toBeTruthy();
    });

    it(":showOtherItemsLocation is false when viewModel.OtherItems is null", function () {
        viewModel.OtherItems(null);
        expect(viewModel.showOtherItemsLocation()).toBeFalsy();
    });

    it(":showOtherItemsLocation is true when viewModel.OtherItems contains at least one item with a location", function () {
        viewModel.OtherItems = ko.observableArray([{ Location: ko.observable("MC") }]);
        expect(viewModel.showOtherItemsLocation()).toBeTruthy();
    });

    it(":printAcknowledgementUrl is mapPrintAcknowledgementActionUrl + ?ecPayTransId= + viewModel.TransactionId when viewModel.TransactionId is not null", function () {
        viewModel.TransactionId(data.TransactionId);
        var expected = mapPrintAcknowledgementActionUrl + "?ecPayTransId=" + data.TransactionId;
        expect(viewModel.printAcknowledgementUrl()).toBe(expected);
    });

    it(":printAcknowledgementUrl is mapPrintAcknowledgementActionUrl + ?cashRcptsId= + viewModel.ReceiptId when viewModel.TransactionId is null", function () {
        viewModel.TransactionId(null);
        viewModel.ReceiptId = ko.observable(data.ReceiptId);
        var expected = mapPrintAcknowledgementActionUrl + "?cashRcptsId=" + data.ReceiptId;
        expect(viewModel.printAcknowledgementUrl()).toBe(expected);
    });

    it(":buttonAriaLabel is makePaymentAcknowledgementAriaLabel after initialization", function () {
        expect(viewModel.buttonAriaLabel()).toBe(makePaymentAcknowledgementAriaLabel);
    });

    it(":paymentComplete sets isLoading to true and calls ko.toJS with viewModel when called", function () {

        spyOn(ko.mapping, 'toJS').and.callThrough();
        spyOn(ko.utils, 'postJson').and.callThrough();

        viewModel.paymentComplete();
        expect(ko.mapping.toJS).toHaveBeenCalled();
        expect(ko.utils.postJson).toHaveBeenCalled();
        expect(viewModel.isLoading()).toBeTruthy();
    });
});

describe("makeAPayment.PaymentPlanViewModel", function () {

    var viewModel, data;
    beforeEach(function () {
        data = {
            "DisplayPaymentPlan": {
                "AcknowledgementText": ["Acknowledge this text."],
                "TermsAndConditionsText": ["Terms text here."]
            },
            "ErrorMessage": "",
            "SelectedPaymentMethod": ""
        };
        viewModel = new makeAPayment.PaymentPlanViewModel(data);
    });

    afterEach(function () {
        data: null;
        viewModel: null;
    });

    it(":ko.mapping.toJS is called on initialization", function () {
        spyOn(ko.mapping, 'fromJS').and.callThrough();
        viewModel = new makeAPayment.PaymentPlanViewModel(data);
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":actionThrobberMessage is loadingPaymentPlanDetailsMessage after initialization", function () {
        expect(viewModel.actionThrobberMessage()).toBe(loadingPaymentPlanDetailsMessage);
    });

    it(":isProcessingAcceptance is false after initialization", function () {
        expect(viewModel.isProcessingAcceptance()).toBeFalsy();
    });

    it(":termsAndConditionsAccepted is false after initialization", function () {
        expect(viewModel.termsAndConditionsAccepted()).toBeFalsy();
    });

    it(":returnToMapButtonDisplayed is true when consentProcessed is true", function () {
        viewModel.consentProcessed(true);
        expect(viewModel.returnToMapButtonDisplayed()).toBeTruthy();
    });

    it(":returnToMapButtonDisplayed is true when ErrorMessage is not null or undefined", function () {
        viewModel.ErrorMessage("An error occurred");
        expect(viewModel.returnToMapButtonDisplayed()).toBeTruthy();
    });

    it(":returnToMapButtonDisplayed is false when consentProcessed is false and ErrorMessage is null", function () {
        viewModel.consentProcessed(false);
        viewModel.ErrorMessage(null);
        expect(viewModel.returnToMapButtonDisplayed()).toBeFalsy();
    });

    it(":consentProcessed is false after initialization", function () {
        expect(viewModel.consentProcessed()).toBeFalsy();
    });

    it(":ApprovalInformationMessage is undefined after initialization", function () {
        expect(viewModel.ApprovalInformationMessage()).toBe(undefined);
    });

    it(":acceptTermsAndConditions success", function () {
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": {
                    "Id": "1234"
                }
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        spyOn($.fn, "notificationCenter");
        spyOn(ko.mapping, 'fromJS').and.callThrough();
        viewModel.acceptTermsAndConditions();
        expect(viewModel.ApprovalInformationMessage()).toBe(successData.ApprovalInformationMessage);
        expect(ko.mapping.fromJS).toHaveBeenCalled();
        expect(viewModel.DisplayPaymentPlan.Id()).toBe(successData.PlanDetails.DisplayPaymentPlan.Id);
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: successData.ApprovalInformationMessage, type: "success", flash: true });
        expect(viewModel.consentProcessed()).toBeTruthy();
        expect(viewModel.isLoading()).toBeFalsy();

    });

    it(":acceptTermsAndConditions error", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });
        spyOn(account, "handleInvalidSessionResponse").and.callFake(function () {
            return true;
        });
        spyOn($.fn, "notificationCenter");
        viewModel.acceptTermsAndConditions();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: createPaymentPlanUnableToProcessAcceptanceMessage, type: "error" });
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":downPaymentDialogIsDisplayed is set to false on initialization", function () {
        expect(viewModel.downPaymentDialogIsDisplayed()).toBeFalsy();
    })

    it(":downPaymentWarning is null if DisplayPaymentPlan is null", function () {
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": null
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        viewModel.acceptTermsAndConditions();
        viewModel.DisplayPaymentPlan = null;
        expect(viewModel.downPaymentWarning()).toBe(null);
    });

    it(":downPaymentWarning is null if DisplayPaymentPlan.DownPaymentAmount is null", function () {
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": {
                    "Id": "1234",
                    "DownPaymentAmount": null
                }
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        viewModel.acceptTermsAndConditions();
        expect(viewModel.downPaymentWarning()).toBe(null);
    });

    it(":downPaymentWarning is null if DisplayPaymentPlan.DownPaymentAmount is zero", function () {
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": {
                    "Id": "1234",
                    "DownPaymentAmount": 0
                }
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        viewModel.acceptTermsAndConditions();
        expect(viewModel.downPaymentWarning()).toBe(null);
    });

    it(":paymentPlanWarningMessage is formatted string", function () {
        expect(viewModel.paymentPlanWarningMessage()).toBe(createPaymentPlanWarningMessage.format(createPaymentPlanAgreeCheckboxLabel, Ellucian.MakePayment.ButtonLabels.createPaymentPlanContinueButtonLabel));
    });

    it(":errorOccurred is set to false on initialization", function () {
        expect(viewModel.errorOccurred()).toBeFalsy();
    })

    it(":goToMakeAPayment calls openUrl if errorOccurred is true", function () {
        var global = jasmine.getGlobal();
        spyOn(global, 'openUrl').and.callThrough();

        viewModel.errorOccurred(true);
        viewModel.goToMakeAPayment();
        expect(global.openUrl).toHaveBeenCalledWith(mapActionUrl);
    });

    it(":goToMakeAPayment calls openUrl if DisplayPaymentPlan is null", function () {
        var global = jasmine.getGlobal();
        spyOn(global, 'openUrl').and.callThrough();
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": null
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        viewModel.acceptTermsAndConditions();
        viewModel.DisplayPaymentPlan = null;
        viewModel.goToMakeAPayment();
        expect(global.openUrl).toHaveBeenCalledWith(mapActionUrl);
    });

    it(":goToMakeAPayment calls openUrl if DisplayPaymentPlan.DownPaymentAmount is null", function () {
        var global = jasmine.getGlobal();
        spyOn(global, 'openUrl').and.callThrough();
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": {
                    "Id": "1234",
                    "DownPaymentAmount":null
                }
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        viewModel.acceptTermsAndConditions();
        viewModel.goToMakeAPayment();
        expect(global.openUrl).toHaveBeenCalledWith(mapActionUrl);
    });

    it(":goToMakeAPayment calls openUrl if DisplayPaymentPlan.DownPaymentAmount is 0", function () {
        var global = jasmine.getGlobal();
        spyOn(global, 'openUrl').and.callThrough();
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": {
                    "Id": "1234",
                    "DownPaymentAmount": 0
                }
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        viewModel.acceptTermsAndConditions();
        viewModel.goToMakeAPayment();
        expect(global.openUrl).toHaveBeenCalledWith(mapActionUrl);
    });

    it(":goToMakeAPayment sets downPaymentDialogIsDisplayed to true if DisplayPaymentPlan.DownPaymentAmount > 0", function () {
        var successData = {
            "ApprovalInformationMessage": "Terms and conditions accepted 11/15/2016 at 12:00pm by jmansfield",
            "PlanDetails": {
                "DisplayPaymentPlan": {
                    "Id": "1234",
                    "DownPaymentAmount": 1
                }
            }
        }
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        viewModel.acceptTermsAndConditions();
        viewModel.goToMakeAPayment();
        expect(viewModel.downPaymentDialogIsDisplayed()).toBeTruthy();
    });

    it(":beginDownPayment success", function () {
        var successData = {
            "AlertMessage": "Here is an alert.",
            "PaymentPlanEligibilityText": "Here is some other text.",
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(successData);
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        spyOn(ko.mapping, 'toJS').and.callThrough();
        spyOn(ko.utils, 'postJson').and.callThrough();
        viewModel.DisplayPaymentPlan = { Id: ko.observable("123") };
        viewModel.SelectedPaymentMethod({ InternalCode: ko.observable("CK") });
        viewModel.beginDownPayment();
        expect(ko.mapping.toJS).toHaveBeenCalledWith(successData, {
            'ignore': ['Restrictions']
        });
        expect(ko.utils.postJson).toHaveBeenCalled();
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.downPaymentDialogIsDisplayed()).toBeFalsy();
    });

    it(":beginDownPayment error", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });
        spyOn(account, "handleInvalidSessionResponse").and.callFake(function () {
            return true;
        });
        spyOn($.fn, "notificationCenter");
        viewModel.DisplayPaymentPlan = { Id: ko.observable("123") };
        viewModel.SelectedPaymentMethod({ InternalCode: ko.observable("CK") });
        viewModel.beginDownPayment();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: paymentPlanDownPaymentGenericError, type: "error" });
        expect(viewModel.errorOccurred).toBeTruthy();
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.downPaymentDialogIsDisplayed()).toBeFalsy();
    });


    it(":downPaymentPayMethodSelected is false if SelectedPaymentMethod is undefined " + mapActionUrl, function () {
        viewModel.SelectedPaymentMethod = undefined;
        expect(viewModel.downPaymentPayMethodSelected()).toBeFalsy();
    });

    it(":downPaymentPayMethodSelected is false if SelectedPaymentMethod is not a function " + mapActionUrl, function () {
        viewModel.SelectedPaymentMethod = "CHK";
        expect(viewModel.downPaymentPayMethodSelected()).toBeFalsy();
    });

    it(":downPaymentPayMethodSelected is false if SelectedPaymentMethod is a function that returns null" + mapActionUrl, function () {
        viewModel.SelectedPaymentMethod = ko.observable(null);
        expect(viewModel.downPaymentPayMethodSelected()).toBeFalsy();
    });

    it(":downPaymentPayMethodSelected is true if SelectedPaymentMethod is a function that returns non-null value" + mapActionUrl, function () {
        viewModel.SelectedPaymentMethod = ko.observable({});
        expect(viewModel.downPaymentPayMethodSelected()).toBeTruthy();
    });

    it(":exitPage calls openUrl with " + mapActionUrl, function () {
        var global = jasmine.getGlobal();
        spyOn(global, 'openUrl').and.callThrough();

        viewModel.exitPage();
        expect(global.openUrl).toHaveBeenCalledWith(mapActionUrl);
    });

    it(":printAdvisoryMessage is formatted string", function () {
        expect(viewModel.printAdvisoryMessage()).toBe(paymentPlanPleasePrintMessage.format(paymentPlanSummaryHeaderLabel, paymentPlanScheduleHeaderLabel,
                paymentPlanTermsAndConditionsLabel));
    });

    it(":workflowState is paymentPlanAcknowledgementTitle when consent is processed", function () {
        viewModel.consentProcessed(true);
        expect(viewModel.workflowState()).toBe(paymentPlanAcknowledgementTitle);
    });

    it(":workflowState is paymentPlanPreviewTitle when consent is not processed", function () {
        viewModel.consentProcessed(false);
        expect(viewModel.workflowState()).toBe(paymentPlanPreviewTitle);
    });

    it(":makeDownPaymentButton set correctly on initialization", function () {
        expect(viewModel.makeDownPaymentButton).toEqual({
            id: "make-down-payment-dialog-button",
            isPrimary: true,
            enabled: viewModel.downPaymentPayMethodSelected,
            title: Ellucian.MakePayment.ButtonLabels.paymentPlanAcknowledgementDownPaymentLabel,
            callback: viewModel.beginDownPayment
        });
    });

    it(":payLaterButton set correctly on initialization", function () {
        expect(viewModel.payLaterButton).toEqual({
            id: "pay-later-dialog-button",
            isPrimary: false,
            title: Ellucian.MakePayment.ButtonLabels.paymentPlanPayDownPaymentLaterButtonLabel,
            callback: viewModel.exitPage
        });
    });
});