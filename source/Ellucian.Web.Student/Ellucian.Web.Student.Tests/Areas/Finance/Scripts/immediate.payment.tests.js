﻿var paymentReviewCountMessage = "PaymentReviewCountMessage{0}{1}";
var ipcECheckEntryActionUrl = "fakeUrl";

describe("immediatePayments.PaymentReviewModel", function () {
    var viewModel,
        data = { Message: "Message", PaymentNumber: 1, PaymentCount: 1, IsCreatingPaymentPlan: true };
        
    beforeEach(function () {
        viewModel = new immediatePayments.PaymentReviewViewModel(data);
    });

    it(":ProcessPayment sets Message to empty string", function () {
        viewModel.ProcessPayment();

        expect(viewModel.Message).toBe("");
    });
});