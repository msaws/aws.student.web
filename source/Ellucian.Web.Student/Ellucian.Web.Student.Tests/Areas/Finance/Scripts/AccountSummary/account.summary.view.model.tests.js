﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates. 

describe("accountSummaryViewModel", function () {
    var viewModel, data;

    beforeEach(function () {
        viewModel = new accountSummaryViewModel(data);
    });

    afterEach(function () {
        data = null;
        viewModel = null;
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading is true after initialization", function () {
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":mobileScreenWidth is set to 768 after initialization", function () {
        expect(viewModel.mobileScreenWidth).toBe(768);
    });

    it(":changeToDesktop does nothing", function () {
        viewModel.isMobile(false);

        spyOn(viewModel, 'getAccountSummaryInformation');

        viewModel.changeToDesktop();
        expect(viewModel.getAccountSummaryInformation).not.toHaveBeenCalled();
    });

    it(":changeToDesktop does nothing when showUI is false", function () {
        viewModel.isMobile(true);

        spyOn(viewModel, 'getAccountSummaryInformation');

        viewModel.changeToDesktop();
        expect(viewModel.getAccountSummaryInformation).not.toHaveBeenCalled();
    });

    it(":NextPaymentDueDate is undefined after initialization", function () {
        expect(viewModel.NextPaymentDueDate()).toBe(undefined);
    });

    it(":AmountDue is undefined after initialization", function () {
        expect(viewModel.AmountDue()).toBe(undefined);
    });

    it(":PersonName is undefined after initialization", function () {
        expect(viewModel.PersonName()).toBe(undefined);
    });

    it(":PersonId is undefined after initialization", function () {
        expect(viewModel.PersonId()).toBe(undefined);
    });

    it(":IsAdminUser is false after initialization", function () {
        expect(viewModel.IsAdminUser()).toBeFalsy();
    });

    it(":HasPrivacyRestriction is false after initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBeFalsy();
    });

    it(":OverdueAmount is undefined after initialization", function () {
        expect(viewModel.OverdueAmount()).toBe(undefined);
    });

    it(":TotalAmountDue is undefined after initialization", function () {
        expect(viewModel.TotalAmountDue()).toBe(undefined);
    });

    it(":AccountBalance is undefined after initialization", function () {
        expect(viewModel.AccountBalance()).toBe(undefined);
    });

    it(":HelpfulLinks is empty array after initialization", function () {
        expect(viewModel.HelpfulLinks().constructor).toBe(Array);
        expect(viewModel.HelpfulLinks().length).toBe(0);
    });

    it(":TimeframeBalances is empty array after initialization", function () {
        expect(viewModel.TimeframeBalances().constructor).toBe(Array);
        expect(viewModel.TimeframeBalances().length).toBe(0);
    });

    it(":ShowAccountActivityLink is undefined after initialization", function () {
        expect(viewModel.ShowAccountActivityLink()).toBe(undefined);
    });

    it(":ShowMakeAPaymentLink is undefined after initialization", function () {
        expect(viewModel.ShowMakeAPaymentLink()).toBe(undefined);
    });

    it(":PrivacyMessage is undefined after initialization", function () {
        expect(viewModel.PrivacyMessage()).toBe(undefined);
    });

    it(":changeUserUrl set correctly", function () {
        viewModel.IsAdminUser(true);
        viewModel.PersonId("0001234");
        expect(viewModel.changeUserUrl()).toBe(Ellucian.Finance.AccountSummary.ActionUrls.accountSummaryChangeUserUrl);
    });

    it(":getAccountSummaryInformation sets showUI to true and sets isLoading to false when it completes", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({
                "AccountBalance":3302,
                "AlertMessage":"This is an <b>alert</b> message.  It <i>may</i> say something important.",
                "AmountDue":6772.4,
                "HelpfulLinks":[],
                "IsAdminUser":false,
                "IsTermDisplay":false,
                "NextPaymentDueDate": new Date(2015, 2, 1),
                "OverdueAmount":"6772.4",
                "PersonId":"0011521",
                "PersonName":"Lillie E. Puckett",
                "TimeframeBalances":[],
                "TotalAmountDue":13544.8
                });
            e.error({ status: 0 }, "", "");
            e.complete({});
        });
        spyOn(viewModel, "checkForMobile");
        spyOn(ko.mapping, "fromJS");

        var global = jasmine.getGlobal();
        spyOn(global, "addQueryParamsToUrl");

        viewModel.getAccountSummaryInformation();
        expect(viewModel.showUI()).toBeTruthy();
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.checkForMobile).toHaveBeenCalled();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it(":getAccountSummaryInformation posts notification on error", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success({});
            e.error({ status: 400 }, "", "");
            e.complete({});
        });
        spyOn($.fn, "notificationCenter");

        viewModel.getAccountSummaryInformation();
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.showUI()).toBeTruthy();
        expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: accountSummaryUnableToLoadMessage, type: "error" });
    });
});