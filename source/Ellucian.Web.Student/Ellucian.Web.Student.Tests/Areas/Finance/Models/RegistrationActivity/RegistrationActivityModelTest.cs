﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.Finance.Models.RegistrationActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.RegistrationActivity
{
    /// <summary>
    ///This is a test class for RegistrationActivityModelTest and is intended
    ///to contain all RegistrationActivityModelTest Unit Tests
    ///</summary>
    [TestClass]
    public class RegistrationActivityModelTest
    {
        List<RegistrationPaymentControl> paymentControls = new List<RegistrationPaymentControl>();
        List<Term> terms = new List<Term>();
        List<FinancialPeriod> periods = new List<FinancialPeriod>();
        TermPeriodsUtility termPeriodsUtility;
        AccountHolder accountHolder;
        ICurrentUser currentUser;

        bool displayByTerm = true;

        [TestInitialize]
        public void Initialize()
        {
            RegistrationPaymentControl paymentControl1 = new RegistrationPaymentControl();
                paymentControl1.AcademicCredits = new List<string>() { "201", "202", "203" };
                paymentControl1.Id = "123";
                paymentControl1.InvoiceIds = new List<string>() { "12345", "67890", "23456" };
                paymentControl1.LastTermsApprovalId = "2345";
                paymentControl1.Payments = new List<string>() { "34567", "45678" };
                paymentControl1.PaymentStatus = RegistrationPaymentStatus.Complete;
                paymentControl1.RegisteredSectionIds = new List<string>() { "234", "456", "678", "890" };
                paymentControl1.StudentId = "0003315";
                paymentControl1.TermId = "2013/FA";
                paymentControls.Add(paymentControl1);

            RegistrationPaymentControl paymentControl2 = new RegistrationPaymentControl();
                paymentControl2.AcademicCredits = new List<string>() { "102", "022", "302" };
                paymentControl2.Id = "321";
                paymentControl2.InvoiceIds = new List<string>() { "54321", "09876", "65432" };
                paymentControl2.LastTermsApprovalId = "5432";
                paymentControl2.Payments = new List<string>() { "76543", "87654" };
                paymentControl2.PaymentStatus = RegistrationPaymentStatus.New;
                paymentControl2.RegisteredSectionIds = new List<string>() { "432", "654", "876", "098" };
                paymentControl2.StudentId = "0000927";
                paymentControl2.TermId = "2014/SP";
                paymentControls.Add(paymentControl2);

            Term term1 = new Term();
                term1.Code = "2013/FA";
                term1.DefaultOnPlan = false;
                term1.Description = "2013 Fall Term";
                term1.EndDate = DateTime.Parse("12/31/2013");
                term1.FinancialPeriod = PeriodType.Past;
                term1.ReportingTerm = "2013/FA";
                term1.ReportingYear = 2013;
                term1.Sequence = 1;
                term1.StartDate = DateTime.Parse("08/01/2013");
                terms.Add(term1);

            Term term2 = new Term();
                term2.Code = "2014/SP";
                term2.DefaultOnPlan = true;
                term2.Description = "2014 Spring Term";
                term2.EndDate = DateTime.Parse("01/01/2014");
                term2.FinancialPeriod = PeriodType.Current;
                term2.ReportingTerm = "2014/SP";
                term2.ReportingYear = 2013;
                term2.Sequence = 2;
                terms.Add(term2);

            FinancialPeriod pastPeriod = new FinancialPeriod();
                pastPeriod.End = DateTime.Parse("12/31/2013");
                pastPeriod.Start = DateTime.MinValue;
                pastPeriod.Type = PeriodType.Past;
                periods.Add(pastPeriod);

            FinancialPeriod currentPeriod = new FinancialPeriod();
                currentPeriod.End = DateTime.Parse("05/31/2014");
                currentPeriod.Start = DateTime.Parse("01/01/2014");
                currentPeriod.Type = PeriodType.Current;
                periods.Add(currentPeriod);

            FinancialPeriod futurePeriod = new FinancialPeriod();
                futurePeriod.End = DateTime.MaxValue;
                futurePeriod.Start = DateTime.Parse("06/01/2014");
                futurePeriod.Type = PeriodType.Future;
                periods.Add(futurePeriod);

            termPeriodsUtility = new TermPeriodsUtility(terms, periods);

            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

            // Proxy-For formatted name
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
            // Proxy-For person id
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
            // Proxy-For permissions, one line for each
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAS"));

            currentUser = new CurrentUser(claim);

            accountHolder = new AccountHolder()
            {
                Id = "HOLDER1",
                PreferredName = "My Name",
                DepositsDue = BuildDetailedAccountPeriod.BuildDepositsDue()
            };
        }

        [TestClass]
        public class RegistrationActivityModel_Build : RegistrationActivityModelTest
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationActivityModel_Build_NullAccountHolder()
            {
                var model = RegistrationActivityModel.Build(null, currentUser);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationActivityModel_Build_NullCurrentUser()
            {
                var model = RegistrationActivityModel.Build(accountHolder, null);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_PersonId()
            {
                var model = RegistrationActivityModel.Build(accountHolder, currentUser);
                Assert.AreEqual(accountHolder.Id, model.PersonId);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_PersonName()
            {
                var model = RegistrationActivityModel.Build(accountHolder, currentUser);
                Assert.AreEqual(accountHolder.PreferredName, model.PersonName);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_IsAdminUser_False()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(accountHolder, currentUser);
                Assert.IsFalse(registrationActivityModel.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_IsAdminUser_False2()
            {
                accountHolder.Id = "0003315";
                var registrationActivityModel = RegistrationActivityModel.Build(accountHolder, currentUser);
                Assert.IsFalse(registrationActivityModel.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_IsAdminUser_False3()
            {
                accountHolder.Id = "0003315";
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);
                var model = RegistrationActivityModel.Build(accountHolder, currentUser);

                Assert.IsFalse(model.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_IsAdminUser_True()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);
                var model = RegistrationActivityModel.Build(accountHolder, currentUser);

                Assert.IsTrue(model.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_HasPrivacyRestriction_False()
            {
                accountHolder.PrivacyStatusCode = null;
                var model = RegistrationActivityModel.Build(accountHolder, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_HasPrivacyRestriction_False2()
            {
                accountHolder.PrivacyStatusCode = "X";
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
                currentUser = new CurrentUser(claim); 

                var model = RegistrationActivityModel.Build(accountHolder, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_PrivacyStatusCode_Set()
            {
                accountHolder.PrivacyStatusCode = "X";
                var model = RegistrationActivityModel.Build(accountHolder, currentUser);
                Assert.AreEqual(accountHolder.PrivacyStatusCode, model.PrivacyStatusCode);
            }
        }

        [TestClass]
        public class RegistrationActivityModel_Build_Overload : RegistrationActivityModelTest
        {

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationActivityModel_Build_Overload_NullPaymentControls()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(null, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationActivityModel_Build_Overload_NoPaymentControls()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(new List<RegistrationPaymentControl>(), termPeriodsUtility, displayByTerm, accountHolder, currentUser);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationActivityModel_Build_Overload_NullTermPeriodsUtility()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(paymentControls, null, displayByTerm, accountHolder, currentUser);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationActivityModel_Build_Overload_NullAccountHolder()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, null, currentUser);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationActivityModel_Build_Overload_NullCurrentUser()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, null);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_Term()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.AreEqual(displayByTerm, registrationActivityModel.AccountActivityDisplayByTerm);
                Assert.AreEqual(paymentControls.Count(), registrationActivityModel.RegistrationTerms.Count());
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_PCF()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, false, accountHolder, currentUser);
                Assert.IsFalse(registrationActivityModel.AccountActivityDisplayByTerm);
                Assert.AreEqual(paymentControls.Count(), registrationActivityModel.RegistrationTerms.Count());
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_PersonId()
            {
                var model = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.AreEqual(accountHolder.Id, model.PersonId);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_PersonName()
            {
                var model = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.AreEqual(accountHolder.PreferredName, model.PersonName);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_IsAdminUser_False()
            {
                var registrationActivityModel = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.IsFalse(registrationActivityModel.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_IsAdminUser_False2()
            {
                accountHolder.Id = "0003315";
                var registrationActivityModel = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.IsFalse(registrationActivityModel.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_IsAdminUser_False3()
            {
                accountHolder.Id = "0003315";
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);
                var model = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);

                Assert.IsFalse(model.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_IsAdminUser_True()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);
                var model = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);

                Assert.IsTrue(model.IsAdminUser);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_HasPrivacyRestriction_False()
            {
                accountHolder.PrivacyStatusCode = null;
                var model = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_HasPrivacyRestriction_False2()
            {
                accountHolder.PrivacyStatusCode = "X";
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
                currentUser = new CurrentUser(claim); 

                var model = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void RegistrationActivityModel_Build_Overload_PrivacyStatusCode_Set()
            {
                accountHolder.PrivacyStatusCode = "X";
                var model = RegistrationActivityModel.Build(paymentControls, termPeriodsUtility, displayByTerm, accountHolder, currentUser);
                Assert.AreEqual(accountHolder.PrivacyStatusCode, model.PrivacyStatusCode);
            }
        }
    }
}
