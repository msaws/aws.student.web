﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Finance.Models.RegistrationActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.RegistrationActivity
{
    [TestClass]
    public class RegistrationTermDisplayModelTests
    {
        RegistrationPaymentControl paymentControl = new RegistrationPaymentControl();
        RegistrationPaymentControl planPaymentControl = new RegistrationPaymentControl();
        List<Term> terms = new List<Term>();
        List<FinancialPeriod> periods = new List<FinancialPeriod>();
        TermPeriodsUtility termPeriodsUtility;
        AccountHolder accountHolder;
        ICurrentUser currentUser;

        bool displayByTerm = true;

        [TestInitialize]
        public void Initialize()
        {
            paymentControl = new RegistrationPaymentControl();
            paymentControl.AcademicCredits = new List<string>() { "201", "202", "203" };
            paymentControl.Id = "123";
            paymentControl.InvoiceIds = new List<string>() { "12345", "67890", "23456" };
            paymentControl.LastTermsApprovalId = "2345";
            paymentControl.Payments = new List<string>() { "34567", "45678" };
            paymentControl.PaymentStatus = RegistrationPaymentStatus.Complete;
            paymentControl.RegisteredSectionIds = new List<string>() { "234", "456", "678", "890" };
            paymentControl.StudentId = "0003315";
            paymentControl.TermId = "2013/FA";

            planPaymentControl = new RegistrationPaymentControl();
            planPaymentControl.AcademicCredits = new List<string>() { "201", "202", "203" };
            planPaymentControl.Id = "123";
            planPaymentControl.InvoiceIds = new List<string>() { "12345", "67890", "23456" };
            planPaymentControl.LastTermsApprovalId = "2345";
            planPaymentControl.LastPlanApprovalId = "369";
            planPaymentControl.PaymentPlanId = "246";
            planPaymentControl.Payments = new List<string>() { "34567", "45678" };
            planPaymentControl.PaymentStatus = RegistrationPaymentStatus.Complete;
            planPaymentControl.RegisteredSectionIds = new List<string>() { "234", "456", "678", "890" };
            planPaymentControl.StudentId = "0003315";
            planPaymentControl.TermId = "2013/FA";
            
            Term term1 = new Term();
            term1.Code = "2013/FA";
            term1.DefaultOnPlan = false;
            term1.Description = "2013 Fall Term";
            term1.EndDate = DateTime.Parse("12/31/2013");
            term1.FinancialPeriod = PeriodType.Past;
            term1.ReportingTerm = "2013/FA";
            term1.ReportingYear = 2013;
            term1.Sequence = 1;
            term1.StartDate = DateTime.Parse("08/01/2013");
            terms.Add(term1);

            FinancialPeriod pastPeriod = new FinancialPeriod();
            pastPeriod.End = DateTime.Parse("12/31/2013");
            pastPeriod.Start = DateTime.MinValue;
            pastPeriod.Type = PeriodType.Past;
            periods.Add(pastPeriod);

            FinancialPeriod currentPeriod = new FinancialPeriod();
            currentPeriod.End = DateTime.Parse("05/31/2014");
            currentPeriod.Start = DateTime.Parse("01/01/2014");
            currentPeriod.Type = PeriodType.Current;
            periods.Add(currentPeriod);

            FinancialPeriod futurePeriod = new FinancialPeriod();
            futurePeriod.End = DateTime.MaxValue;
            futurePeriod.Start = DateTime.Parse("06/01/2014");
            futurePeriod.Type = PeriodType.Future;
            periods.Add(futurePeriod);

            termPeriodsUtility = new TermPeriodsUtility(terms, periods);

            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

            // Proxy-For formatted name
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
            // Proxy-For person id
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
            // Proxy-For permissions, one line for each
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAS"));

            currentUser = new CurrentUser(claim);

            accountHolder = new AccountHolder()
            {
                Id = "HOLDER1",
                PreferredName = "My Name",
                DepositsDue = BuildDetailedAccountPeriod.BuildDepositsDue()
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            paymentControl = null;
            terms = null;
            periods = null;
            termPeriodsUtility = null;
            accountHolder = null;
            currentUser = null;
        }

        [TestClass]
        public class RegistrationTermDisplayModel_Constructor : RegistrationTermDisplayModelTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationTermDisplayModel_NullRegistrationPaymentControl()
            {
                var model = RegistrationTermDisplayModel.Build(null, termPeriodsUtility, accountHolder.Id, displayByTerm, false);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationTermDisplayModel_NullTermPeriodsUtility()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, null, accountHolder.Id, displayByTerm, false);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationTermDisplayModel_NullPersonId()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, null, displayByTerm, false);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_PaymentControl()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, displayByTerm, false);
                Assert.AreEqual(paymentControl.Id, model.PaymentControl.Id);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_FinancialPeriod_Term()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, displayByTerm, false);
                Assert.IsNull(model.FinancialPeriod);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_FinancialPeriod_Pcf()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, false, false);
                Assert.IsNotNull(model.FinancialPeriod);
                Assert.AreEqual(PeriodType.Past, model.FinancialPeriod.Type);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_PeriodCode_Term()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, displayByTerm, false);
                Assert.IsNull(model.PeriodCode);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_PeriodCode_Pcf()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, false, false);
                Assert.AreEqual("PAST", model.PeriodCode);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_NoActionText_NonAdmin()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, false, false);
                Assert.AreEqual(string.Empty, model.NoActionText);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_NoActionText_Admin_NonNewStatus()
            {
                paymentControl.PaymentStatus = RegistrationPaymentStatus.Accepted;
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.AreEqual(string.Empty, model.NoActionText);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_NoActionText_Admin_NewStatus()
            {
                paymentControl.PaymentStatus = RegistrationPaymentStatus.New;
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.AreEqual(null, model.NoActionText);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_DisplayPaymentPlanLink_NonComplete()
            {
                paymentControl.PaymentStatus = RegistrationPaymentStatus.New;
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsFalse(model.DisplayPaymentPlanLink);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_DisplayPaymentPlanLink_Complete_NoLastPlanApprovalId()
            {
                var model = RegistrationTermDisplayModel.Build(paymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsFalse(model.DisplayPaymentPlanLink);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_DisplayPaymentPlanLink_Complete_LastPlanApprovalId_NoPlanId()
            {
                planPaymentControl.PaymentPlanId = null;
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsFalse(model.DisplayPaymentPlanLink);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_DisplayPaymentPlanLink_Complete_LastPlanApprovalId_PlanId()
            {
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsTrue(model.DisplayPaymentPlanLink);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_ActionHeaderLabel_OneAction()
            {
                planPaymentControl.PaymentPlanId = null;
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsNull(model.ActionHeaderLabel);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_ActionHeaderLabel_MultipleAction()
            {
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsNull(model.ActionHeaderLabel);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_AccountActivityUrlSuffix_Term()
            {
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, displayByTerm, true);
                Assert.IsNull(model.ActionHeaderLabel);
                Assert.AreEqual("/" + accountHolder.Id + "?timeframeId=" + planPaymentControl.TermId, model.AccountActivityUrlSuffix);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_AccountActivityUrlSuffix_Pcf_Past()
            {
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, false);
                Assert.IsNull(model.ActionHeaderLabel);
                Assert.AreEqual("?timeframeId=PAST&EndDate=" + DateTime.Parse("12/31/2013").ToShortDateString(), model.AccountActivityUrlSuffix);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_AccountActivityUrlSuffix_Pcf_Current()
            {
                planPaymentControl.TermId = "2016/S1";

                Term term1 = new Term();
                term1.Code = "2016/S1";
                term1.DefaultOnPlan = false;
                term1.Description = "2016 Fall Term";
                term1.EndDate = DateTime.Today.AddDays(30);
                term1.FinancialPeriod = PeriodType.Current;
                term1.ReportingTerm = "2016/SU";
                term1.ReportingYear = 2016;
                term1.Sequence = 1;
                term1.StartDate = DateTime.Today.AddDays(-30);
                terms.Add(term1);

                termPeriodsUtility = new TermPeriodsUtility(terms, periods);
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, false);
                Assert.IsNull(model.ActionHeaderLabel);
                Assert.AreEqual("?timeframeId=CUR&StartDate=" + DateTime.Parse("1/1/2014").ToShortDateString() + "&EndDate=" + DateTime.Parse("5/31/2014").ToShortDateString(), model.AccountActivityUrlSuffix);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_AccountActivityUrlSuffix_Pcf_Future()
            {
                planPaymentControl.TermId = "2016/FA";

                Term term1 = new Term();
                term1.Code = "2016/FA";
                term1.DefaultOnPlan = false;
                term1.Description = "2016 Fall Term";
                term1.EndDate = DateTime.Today.AddDays(30);
                term1.FinancialPeriod = PeriodType.Future;
                term1.ReportingTerm = "2016/FA";
                term1.ReportingYear = 2016;
                term1.Sequence = 1;
                term1.StartDate = DateTime.Today.AddDays(-30);
                terms.Add(term1);

                termPeriodsUtility = new TermPeriodsUtility(terms, periods);

                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsNull(model.ActionHeaderLabel);
                Assert.AreEqual("/" + accountHolder.Id + "?timeframeId=FTR&StartDate=" + DateTime.Parse("6/1/2014").ToShortDateString(), model.AccountActivityUrlSuffix);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_IsComplete_True()
            {
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsTrue(model.IsComplete);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_IsComplete_False()
            {
                planPaymentControl.PaymentStatus = RegistrationPaymentStatus.New;
                var model = RegistrationTermDisplayModel.Build(planPaymentControl, termPeriodsUtility, accountHolder.Id, false, true);
                Assert.IsFalse(model.IsComplete);
            }
        }
        
        [TestClass]
        public class RegistrationTermDisplayModel_GetLinkLabel : RegistrationTermDisplayModelTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationTermDisplayModel_GetLinkLabel_NullPaymentControl()
            {
                var suffix = RegistrationTermDisplayModel.GetLinkLabel(null, false);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetLinkLabel_NoRegisteredSections_Null()
            {
                paymentControl.RegisteredSectionIds = null;
                var label = RegistrationTermDisplayModel.GetLinkLabel(paymentControl, false);
                Assert.AreEqual(null, label);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetLinkLabel_NoRegisteredSections_Empty()
            {
                paymentControl.RegisteredSectionIds = new List<string>();
                var label = RegistrationTermDisplayModel.GetLinkLabel(paymentControl, false);
                Assert.AreEqual(null, label);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetLinkLabel_CompleteStatus()
            {
                paymentControl.PaymentStatus = RegistrationPaymentStatus.Complete;
                var label = RegistrationTermDisplayModel.GetLinkLabel(paymentControl, false);
                Assert.AreEqual(null, label);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetLinkLabel_NonCompleteStatus_NonAdmin()
            {
                paymentControl.PaymentStatus = RegistrationPaymentStatus.Accepted;
                var label = RegistrationTermDisplayModel.GetLinkLabel(paymentControl, false);
                Assert.AreEqual(null, label);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetLinkLabel_NonCompleteStatus_Admin()
            {
                paymentControl.PaymentStatus = RegistrationPaymentStatus.Accepted;
                var label = RegistrationTermDisplayModel.GetLinkLabel(paymentControl, true);
                Assert.AreEqual(null, label);
            }
        }

        [TestClass]
        public class RegistrationTermDisplayModel_GetUrlSuffix : RegistrationTermDisplayModelTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void RegistrationTermDisplayModel_GetUrlSuffix_NullPaymentControl()
            {
                var suffix = RegistrationTermDisplayModel.GetUrlSuffix(null, true, true, paymentControl.StudentId, paymentControl.TermId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void RegistrationTermDisplayModel_GetUrlSuffix_Pcf_NoDates()
            {
                var suffix = RegistrationTermDisplayModel.GetUrlSuffix(paymentControl, false, true, paymentControl.StudentId, paymentControl.TermId);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetUrlSuffix_Term_NoPersonId()
            {
                var suffix = RegistrationTermDisplayModel.GetUrlSuffix(paymentControl, true, true, null, paymentControl.TermId);
                Assert.AreEqual("?timeframeId=" + paymentControl.TermId, suffix);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetUrlSuffix_Term_PersonId()
            {
                var suffix = RegistrationTermDisplayModel.GetUrlSuffix(paymentControl, true, true, paymentControl.StudentId, paymentControl.TermId);
                Assert.AreEqual("/" + paymentControl.StudentId + "?timeframeId=" + paymentControl.TermId, suffix);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetUrlSuffix_Pcf_NoPersonId()
            {
                var suffix = RegistrationTermDisplayModel.GetUrlSuffix(paymentControl, false, true, null, paymentControl.TermId, DateTime.Today.AddDays(30), DateTime.Today.AddDays(-30));
                Assert.AreEqual("?timeframeId=" + paymentControl.TermId + "&StartDate=" + DateTime.Today.AddDays(-30).ToShortDateString(), suffix);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetUrlSuffix_Pcf_PersonId()
            {
                var suffix = RegistrationTermDisplayModel.GetUrlSuffix(paymentControl, false, true, paymentControl.StudentId, paymentControl.TermId, DateTime.Today.AddDays(30), DateTime.Today.AddDays(-30));
                Assert.AreEqual("/" + paymentControl.StudentId + "?timeframeId=" + paymentControl.TermId + "&StartDate=" + DateTime.Today.AddDays(-30).ToShortDateString(), suffix);
            }
        }

        [TestClass]
        public class RegistrationTermDisplayModel_GetStyle : RegistrationTermDisplayModelTests
        {
            [TestMethod]
            public void RegistrationTermDisplayModel_GetStyle_NoRegisteredSections()
            {
                var style = RegistrationTermDisplayModel.GetStyle(RegistrationPaymentStatus.Complete, 0);
                Assert.AreEqual("noActiveClasses", style);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetStyle_Complete()
            {
                var style = RegistrationTermDisplayModel.GetStyle(RegistrationPaymentStatus.Complete, 1);
                Assert.AreEqual("requirementMet", style);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetStyle_NotComplete()
            {
                var style = RegistrationTermDisplayModel.GetStyle(RegistrationPaymentStatus.Accepted, 1);
                Assert.AreEqual("requirementNotMet", style);
            }
        }

        [TestClass]
        public class RegistrationTermDisplayModel_GetStatus : RegistrationTermDisplayModelTests
        {
            [TestMethod]
            public void RegistrationTermDisplayModel_GetStatus_NoRegisteredSections()
            {
                var status = RegistrationTermDisplayModel.GetStatus(RegistrationPaymentStatus.Complete, 0);
                Assert.IsNull(status);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetStatus_Complete()
            {
                var status = RegistrationTermDisplayModel.GetStatus(RegistrationPaymentStatus.Complete, 1);
                Assert.IsNull(status);
            }

            [TestMethod]
            public void RegistrationTermDisplayModel_GetStatus_NotComplete()
            {
                var status = RegistrationTermDisplayModel.GetStatus(RegistrationPaymentStatus.Accepted, 1);
                Assert.IsNull(status);
            }
        }

    }
}
