﻿using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityRoomAndBoardItem
    {
        private static DateTime date = new DateTime(2014, 9, 15, 0, 0, 0);
        private static string room = "ABC*123";

        public static ActivityRoomAndBoardItem Build()
        {
            var ati = BuildActivityTermItem.Build();

            var model = new ActivityRoomAndBoardItem()
            {
                Id = ati.Id,
                Description = ati.Description,
                Date = date,
                TermId = ati.TermId,
                Amount = ati.Amount,
                Room = room
            };

            return model;
        }
    }
}
