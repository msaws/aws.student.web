﻿using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivitySponsorPaymentItem
    {
        private static DateTime date = new DateTime(2014, 9, 15, 0, 0, 0);
        private static string sponsorship = "123";

        public static ActivitySponsorPaymentItem Build()
        {
            var ati = BuildActivityDateTermItem.Build();

            var model = new ActivitySponsorPaymentItem()
            {
                Id = ati.Id,
                Description = ati.Description,
                Date = date,
                TermId = ati.TermId,
                Amount = ati.Amount,
                Sponsorship = sponsorship
            };

            return model;
        }
    }
}
