﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityTuitionItem
    {
        private const decimal amt = 500m;
        private const decimal bc = 3m;
        private const string room = "Classroom";
        private const string desc = "Tuition Description";
        private const string end = "10:00 AM";
        private const string start = "9:00 AM";
        private const string instructor = "Professor Jones";
        private const string status = "Active";

        public static ActivityTuitionItem Build(string id = null, string termId = "2014/FA")
        {
            var model = new ActivityTuitionItem()
            {
                Amount = amt,
                BillingCredits = bc,
                Ceus = null,
                Classroom = room,
                Credits = bc,
                Days = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday },
                Description = desc + " " + id,
                EndTime = end,
                Id = id,
                Instructor = instructor,
                StartTime = start,
                Status = status,
                TermId = termId
            };

            return model;
        }
    }
}
