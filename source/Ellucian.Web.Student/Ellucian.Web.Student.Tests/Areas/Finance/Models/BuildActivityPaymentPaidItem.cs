﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using System;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityPaymentPaidItem
    {
        private const string id = "12345";
        private const string description = "Deposit Description";
        private const string termId = "2014/FA";
        private const decimal amount = 123.45m;
        private const string method = "ECHK";
        private const string refNo = "1234";

        public static ActivityPaymentPaidItem Build()
        {
            var model = new ActivityPaymentPaidItem()
            {
                Id = id,
                Description = description,
                TermId = termId,
                Amount = amount,
                Date = DateTime.Today.AddDays(-3),
                Method = method,
                ReferenceNumber = refNo
            };

            return model;
        }
    }
}
