﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityFinancialAidTerm
    {
        private const string termId = "2014/FA";
        private const decimal antAmount = 500m;
        private const decimal disbAmount = 1000m;

        public static ActivityFinancialAidTerm Build()
        {
            var model = new ActivityFinancialAidTerm()
            {
                AwardTerm = termId,
                AnticipatedAmount = antAmount,
                DisbursedAmount = disbAmount
            };

            return model;
        }
    }
}
