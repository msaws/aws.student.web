﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityTermItem
    {
        private const string id = "12345";
        private const string description = "Technology Fee";
        private const string termId = "2014/FA";
        private const decimal amount = 123.45m;

        public static ActivityTermItem Build()
        {
            var model = new ActivityTermItem()
            {
                Id = id,
                Description = description,
                TermId = termId,
                Amount = amount
            };

            return model;
        }
    }
}
