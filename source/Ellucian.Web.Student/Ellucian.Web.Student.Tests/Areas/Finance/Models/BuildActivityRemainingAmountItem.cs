﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using System;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityRemainingAmountItem
    {
        private const string id = "12345";
        private const string description = "Deposit Description";
        private const string termId = "2014/FA";
        private const decimal amount = 123.45m;
        private const decimal otherAmt = 26.55m;
        private const decimal paidAmt = 50m;
        private const decimal rfdAmt = 100m;
        private const decimal remaining = 200m;

        public static ActivityRemainingAmountItem Build()
        {
            var model = new ActivityRemainingAmountItem()
            {
                Id = id,
                Description = description,
                TermId = termId,
                Amount = amount,
                Date = DateTime.Today.AddDays(-3),
                OtherAmount = otherAmt,
                PaidAmount = paidAmt,
                RefundAmount = rfdAmt,
                RemainingAmount = remaining
            };

            return model;
        }
    }
}
