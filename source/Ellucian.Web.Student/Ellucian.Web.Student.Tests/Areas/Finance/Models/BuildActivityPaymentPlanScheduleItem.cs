﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityPaymentPlanScheduleItem
    {
        private const string id = "12345";
        private const string description = "Technology Fee";
        private const string termId = "2014/FA";
        private const decimal amount = 123.45m;
        private const decimal paid = 23.45m;
        private const decimal late = 10m;
        private const decimal setup = 15m;

        public static ActivityPaymentPlanScheduleItem Build(DateTime date)
        {
            var model = new ActivityPaymentPlanScheduleItem()
            {
                Id = id,
                Description = description,
                TermId = termId,
                Amount = amount,
                AmountPaid = paid,
                Date = date.AddDays(7),
                DatePaid = date,
                LateCharge = late,
                NetAmountDue = amount - paid,
                SetupCharge = setup
            };

            return model;
        }
    }
}
