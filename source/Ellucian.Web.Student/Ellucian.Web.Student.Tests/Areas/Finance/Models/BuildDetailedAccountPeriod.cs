﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildDetailedAccountPeriod
    {
        public static List<string> BuildAssociatedPeriods(string displayTermPeriodCode = "TermId")
        {
            return new List<string>() { displayTermPeriodCode };
        }
        public static ChargesCategory BuildChargesCategory(string displayTermPeriodCode = "TermId")
        {
            return new ChargesCategory()
            {
                FeeGroups = new List<FeeType>()
                    {
                        new FeeType() { DisplayOrder = 1, Name = "Fee Charges 1", FeeCharges = new List<ActivityDateTermItem>()
                            {
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                            }
                        },
                        new FeeType() { DisplayOrder = 2, Name = "Fee Charges 2", FeeCharges = new List<ActivityDateTermItem>()
                            {
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                            }
                        }
                    },
                Miscellaneous = new OtherType()
                {
                    DisplayOrder = 99,
                    Name = "Miscellaneous",
                    OtherCharges = new List<ActivityDateTermItem>()
                        {
                            BuildActivityDateTermItem.Build(),
                            BuildActivityDateTermItem.Build(),
                            BuildActivityDateTermItem.Build(),
                        }
                },
                OtherGroups = new List<OtherType>()
                    {
                        new OtherType() { DisplayOrder = 3, Name = "Other Charges 1", OtherCharges = new List<ActivityDateTermItem>()
                            {
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                            }
                        },
                        new OtherType() { DisplayOrder = 4, Name = "Other Charges 2", OtherCharges = new List<ActivityDateTermItem>()
                            {
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                                BuildActivityDateTermItem.Build(),
                            }
                        }
                    },
                RoomAndBoardGroups = new List<RoomAndBoardType>()
                    {
                        new RoomAndBoardType() { DisplayOrder = 5, Name = "Room and Board Charges 1", RoomAndBoardCharges = new List<ActivityRoomAndBoardItem>()
                            {
                                BuildActivityRoomAndBoardItem.Build(),
                                BuildActivityRoomAndBoardItem.Build(),
                                BuildActivityRoomAndBoardItem.Build(),
                            }
                        },
                        new RoomAndBoardType() { DisplayOrder = 6, Name = "Room and Board Charges 2", RoomAndBoardCharges = new List<ActivityRoomAndBoardItem>()
                            {
                                BuildActivityRoomAndBoardItem.Build(),
                                BuildActivityRoomAndBoardItem.Build(),
                                BuildActivityRoomAndBoardItem.Build(),
                            }
                        }
                    },
                TuitionBySectionGroups = new List<TuitionBySectionType>()
                    {
                        new TuitionBySectionType() { DisplayOrder = 7, Name = "Tuition by Section Charges 1", SectionCharges = new List<ActivityTuitionItem>()
                            {
                                BuildActivityTuitionItem.Build("101", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("102", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("103", displayTermPeriodCode),
                            }
                        },
                        new TuitionBySectionType() { DisplayOrder = 8, Name = "Tuition by Section Charges 2", SectionCharges = new List<ActivityTuitionItem>()
                            {
                                BuildActivityTuitionItem.Build("104", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("105", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("106", displayTermPeriodCode),
                            }
                        }
                    },
                TuitionByTotalGroups = new List<TuitionByTotalType>()
                    {
                        new TuitionByTotalType() { DisplayOrder = 9, Name = "Tuition by Total Charges 1", TotalCharges = new List<ActivityTuitionItem>()
                            {
                                BuildActivityTuitionItem.Build("201", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("202", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("203", displayTermPeriodCode),
                            }
                        },
                        new TuitionByTotalType() { DisplayOrder = 10, Name = "Tuition by Total Charges 2", TotalCharges = new List<ActivityTuitionItem>()
                            {
                                BuildActivityTuitionItem.Build("204", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("205", displayTermPeriodCode),
                                BuildActivityTuitionItem.Build("206", displayTermPeriodCode),
                            }
                        }
                    }
            };
        }
        public static StudentPaymentCategory BuildStudentPaymentCategory()
        {
            return new StudentPaymentCategory()
            {
                StudentPayments = new List<ActivityPaymentPaidItem>()
                    {
                        BuildActivityPaymentPaidItem.Build(),
                        BuildActivityPaymentPaidItem.Build(),
                        BuildActivityPaymentPaidItem.Build(),
                    }
            };
        }
        public static FinancialAidCategory BuildFinancialAidCategory()
        {
            return new FinancialAidCategory()
            {
                AnticipatedAid = new List<ActivityFinancialAidItem>() 
                    {
                        BuildActivityFinancialAidItem.Build(),
                        BuildActivityFinancialAidItem.Build(),
                        BuildActivityFinancialAidItem.Build()
                    },
                DisbursedAid = new List<ActivityDateTermItem>() 
                    {
                        BuildActivityDateTermItem.Build(),
                        BuildActivityDateTermItem.Build(),
                        BuildActivityDateTermItem.Build()
                    }
            };
        }
        public static SponsorshipCategory BuildSponsorshipcategory()
        {
            return new SponsorshipCategory()
            {
                SponsorItems = new List<ActivitySponsorPaymentItem>() 
                    {
                        BuildActivitySponsorPaymentItem.Build(),
                        BuildActivitySponsorPaymentItem.Build(),
                        BuildActivitySponsorPaymentItem.Build()
                    }
            };
        }
        public static DepositCategory BuildDepositCategory()
        {
            return new DepositCategory()
            {
                Deposits = new List<ActivityRemainingAmountItem>() 
                    {
                        BuildActivityRemainingAmountItem.Build(),
                        BuildActivityRemainingAmountItem.Build(),
                        BuildActivityRemainingAmountItem.Build()
                    }
            };
        }
        public static RefundCategory BuildRefundCategory()
        {
            return new RefundCategory()
            {
                Refunds = new List<ActivityPaymentMethodItem>() 
                    {
                        BuildActivityPaymentMethodItem.Build(),
                        BuildActivityPaymentMethodItem.Build(),
                        BuildActivityPaymentMethodItem.Build()
                    }
            };
        }
        public static List<DepositDue> BuildDepositsDue()
        {
            return new List<DepositDue>()
            {
                BuildDepositDue.Build(),
                BuildDepositDue.Build(),
                BuildDepositDue.Build()
            };
        }
        public static PaymentPlanCategory BuildPaymentPlanCategory()
        {
            return new PaymentPlanCategory()
            {
                PaymentPlans = new List<ActivityPaymentPlanDetailsItem>() 
                    {
                        BuildActivityPaymentPlanDetailsItem.Build(),
                        BuildActivityPaymentPlanDetailsItem.Build(),
                        BuildActivityPaymentPlanDetailsItem.Build()
                    }
            };
        }

        public static DetailedAccountPeriod Build(string displayTermPeriodCode)
        {
            return new DetailedAccountPeriod()
            {
                AssociatedPeriods = BuildAssociatedPeriods(displayTermPeriodCode),
                Charges = BuildChargesCategory(displayTermPeriodCode),
                Deposits = BuildDepositCategory(),
                FinancialAid = BuildFinancialAidCategory(),
                PaymentPlans = BuildPaymentPlanCategory(),
                Refunds = BuildRefundCategory(),
                Sponsorships = BuildSponsorshipcategory(),
                StudentPayments = BuildStudentPaymentCategory(),
                AmountDue = 1000m,
                Balance = 10000m,
                Description = displayTermPeriodCode,
                DueDate = DateTime.Today.AddDays(3),
                EndDate = DateTime.Today.AddDays(30),
                Id = displayTermPeriodCode,
                StartDate = DateTime.Today.AddDays(-30)
            };
        }
    }
}
