﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class CategoryModelTests
    {
        TermPeriodsUtility termUtility;
        string displayTermPeriodCode;
        CategoryModel result;

        [TestInitialize]
        public void Initialize()
        {
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            displayTermPeriodCode = "2014/FA";
        }

        [TestClass]
        public class CategoryModelTests_Build_ChargesCategory : CategoryModelTests
        {
            ChargesCategory category;

            [TestInitialize]
            public void CategoryModelTests_Build_ChargesCategory_Initialize()
            {
                category = BuildDetailedAccountPeriod.BuildChargesCategory(displayTermPeriodCode);
                result = CategoryModel.Build(category, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_ChargesCategory_NullCategory()
            {
                result = CategoryModel.Build((ChargesCategory)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_ChargesCategory_NullTermUtility()
            {
                result = CategoryModel.Build(category, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_ChargesCategory_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_ChargesCategory_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_CategoryType()
            {
                Assert.AreEqual(CategoryType.Charge, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_Order()
            {
                Assert.AreEqual((int)CategoryType.Charge, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.Charge).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_IncludeInFormula()
            {
                Assert.IsTrue(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_Description()
            {
                Assert.AreEqual(CategoryType.Charge.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_Transactions()
            {
                Assert.AreEqual(11, result.Transactions.Count);
                Assert.AreEqual(category.FeeGroups[0].DisplayOrder, (result.Transactions[0] as ChargeGroupModel).Order);
                Assert.AreEqual(category.FeeGroups[1].DisplayOrder, (result.Transactions[1] as ChargeGroupModel).Order);
                Assert.AreEqual(category.OtherGroups[0].DisplayOrder, (result.Transactions[2] as ChargeGroupModel).Order);
                Assert.AreEqual(category.OtherGroups[1].DisplayOrder, (result.Transactions[3] as ChargeGroupModel).Order);
                Assert.AreEqual(category.RoomAndBoardGroups[0].DisplayOrder, (result.Transactions[4] as ChargeGroupModel).Order);
                Assert.AreEqual(category.RoomAndBoardGroups[1].DisplayOrder, (result.Transactions[5] as ChargeGroupModel).Order);
                Assert.AreEqual(category.TuitionBySectionGroups[0].DisplayOrder, (result.Transactions[6] as ChargeGroupModel).Order);
                Assert.AreEqual(category.TuitionBySectionGroups[1].DisplayOrder, (result.Transactions[7] as ChargeGroupModel).Order);
                Assert.AreEqual(category.TuitionByTotalGroups[0].DisplayOrder, (result.Transactions[8] as ChargeGroupModel).Order);
                Assert.AreEqual(category.TuitionByTotalGroups[1].DisplayOrder, (result.Transactions[9] as ChargeGroupModel).Order);
                Assert.AreEqual(category.Miscellaneous.DisplayOrder, (result.Transactions[10] as ChargeGroupModel).Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_Transactions_NullGroups()
            {
                result = CategoryModel.Build(new ChargesCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_Amount()
            {
                var expected = category.FeeGroups.SelectMany(f => f.FeeCharges).Sum(fc => fc.Amount ?? 0) +
                    category.Miscellaneous.OtherCharges.Sum(m => m.Amount ?? 0) +
                    category.OtherGroups.SelectMany(o => o.OtherCharges).Sum(oc => oc.Amount ?? 0) +
                    category.RoomAndBoardGroups.SelectMany(r => r.RoomAndBoardCharges).Sum(rbc => rbc.Amount ?? 0) +
                    category.TuitionBySectionGroups.SelectMany(t => t.SectionCharges).Sum(sc => sc.Amount ?? 0) +
                    category.TuitionByTotalGroups.SelectMany(t => t.TotalCharges).Sum(tc => tc.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_Amount_NotVisible()
            {
                result = CategoryModel.Build(new ChargesCategory(), termUtility, displayTermPeriodCode);
                Assert.IsNull(result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_IsVisible_False()
            {
                result = CategoryModel.Build(new ChargesCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_IsDebit()
            {
                Assert.IsTrue(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_IsCredit()
            {
                Assert.IsFalse(result.IsCredit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_NullChargesWithinGroups()
            {
                result = CategoryModel.Build(new ChargesCategory() { 
                    FeeGroups = new List<FeeType>() { new FeeType() { FeeCharges = null } },
                    TuitionBySectionGroups = new List<TuitionBySectionType>() { new TuitionBySectionType() { SectionCharges = null } },
                    TuitionByTotalGroups = new List<TuitionByTotalType>() { new TuitionByTotalType() { TotalCharges = null } },
                    RoomAndBoardGroups = new List<RoomAndBoardType>() { new RoomAndBoardType() { RoomAndBoardCharges = null } },
                    OtherGroups = new List<OtherType>() { new OtherType() { OtherCharges = null } }
                }, termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_ChargesCategory_EmptyChargesWithinGroups()
            {
                result = CategoryModel.Build(new ChargesCategory()
                {
                    FeeGroups = new List<FeeType>() { new FeeType() { FeeCharges = new List<ActivityDateTermItem>() } },
                    TuitionBySectionGroups = new List<TuitionBySectionType>() { new TuitionBySectionType() { SectionCharges = new List<ActivityTuitionItem>() } },
                    TuitionByTotalGroups = new List<TuitionByTotalType>() { new TuitionByTotalType() { TotalCharges = new List<ActivityTuitionItem>() } },
                    RoomAndBoardGroups = new List<RoomAndBoardType>() { new RoomAndBoardType() { RoomAndBoardCharges = new List<ActivityRoomAndBoardItem>() } },
                    OtherGroups = new List<OtherType>() { new OtherType() { OtherCharges = new List<ActivityDateTermItem>() } }
                }, termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }
        }

        [TestClass]
        public class CategoryModelTests_Build_StudentPaymentCategory : CategoryModelTests
        {
            List<AvailablePaymentMethod> payMethods;
            StudentPaymentCategory category;

            [TestInitialize]
            public void CategoryModelTests_Build_StudentPaymentCategory_Initialize()
            {
                payMethods = new List<AvailablePaymentMethod>()
                {
                    new AvailablePaymentMethod() { Description = "Visa", InternalCode = "CCVI", Type = "CC" },
                    new AvailablePaymentMethod() { Description = "MasterCard", InternalCode = "CCMC", Type = "CC" },
                    new AvailablePaymentMethod() { Description = "American Express", InternalCode = "CCAX", Type = "CC" },
                    new AvailablePaymentMethod() { Description = "Discover", InternalCode = "CCDS", Type = "CC" },
                    new AvailablePaymentMethod() { Description = "Electronic Check / ACH", InternalCode = "ECHK", Type = "CK" },
                };
                category = BuildDetailedAccountPeriod.BuildStudentPaymentCategory();
                result = CategoryModel.Build(category, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_StudentPaymentCategory_NullCategory()
            {
                result = CategoryModel.Build((StudentPaymentCategory)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_StudentPaymentCategory_NullTermUtility()
            {
                result = CategoryModel.Build(category, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_StudentPaymentCategory_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_StudentPaymentCategory_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_CategoryType()
            {
                Assert.AreEqual(CategoryType.ReceiptPayment, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_Order()
            {
                Assert.AreEqual((int)CategoryType.ReceiptPayment, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.ReceiptPayment).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_IncludeInFormula()
            {
                Assert.IsTrue(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_Description()
            {
                Assert.AreEqual(CategoryType.ReceiptPayment.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_Transactions()
            {
                Assert.AreEqual(category.StudentPayments.Count, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_Transactions_NullStudentPayments()
            {
                result = CategoryModel.Build(new StudentPaymentCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_Amount()
            {
                var expected = category.StudentPayments.Sum(sp => sp.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_Amount_NotVisible()
            {
                result = CategoryModel.Build(new StudentPaymentCategory(), termUtility, displayTermPeriodCode);
                Assert.IsNull(result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_IsVisible_False()
            {
                result = CategoryModel.Build(new StudentPaymentCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_IsDebit()
            {
                Assert.IsFalse(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_StudentPaymentCategory_IsCredit()
            {
                Assert.IsTrue(result.IsCredit);
            }
        }

        [TestClass]
        public class CategoryModelTests_Build_FinancialAidCategory : CategoryModelTests
        {
            FinancialAidCategory category;

            [TestInitialize]
            public void CategoryModelTests_Build_FinancialAidCategory_Initialize()
            {
                category = BuildDetailedAccountPeriod.BuildFinancialAidCategory();
                result = CategoryModel.Build(category, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_FinancialAidCategory_NullCategory()
            {
                result = CategoryModel.Build((FinancialAidCategory)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_FinancialAidCategory_NullTermUtility()
            {
                result = CategoryModel.Build(category, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_FinancialAidCategory_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_FinancialAidCategory_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_CategoryType()
            {
                Assert.AreEqual(CategoryType.FinancialAidPayment, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_Order()
            {
                Assert.AreEqual((int)CategoryType.FinancialAidPayment, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.FinancialAidPayment).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_IncludeInFormula()
            {
                Assert.IsTrue(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_Description()
            {
                Assert.AreEqual(CategoryType.FinancialAidPayment.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_Transactions()
            {
                Assert.AreEqual(3, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_Transactions_NullAnticipatedAid()
            {
                result = CategoryModel.Build(new FinancialAidCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_Amount()
            {
                Assert.AreEqual(4500, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_Amount_NotVisible()
            {
                result = CategoryModel.Build(new FinancialAidCategory(), termUtility, displayTermPeriodCode);
                Assert.IsNull(result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_IsVisible_False()
            {
                result = CategoryModel.Build(new FinancialAidCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_IsDebit()
            {
                Assert.IsFalse(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_FinancialAidCategory_IsCredit()
            {
                Assert.IsTrue(result.IsCredit);
            }
        }

        [TestClass]
        public class CategoryModelTests_Build_SponsorshipCategory : CategoryModelTests
        {
            SponsorshipCategory category;

            [TestInitialize]
            public void CategoryModelTests_Build_SponsorshipCategory_Initialize()
            {
                category = BuildDetailedAccountPeriod.BuildSponsorshipcategory();
                result = CategoryModel.Build(category, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_SponsorshipCategory_NullCategory()
            {
                result = CategoryModel.Build((SponsorshipCategory)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_SponsorshipCategory_NullTermUtility()
            {
                result = CategoryModel.Build(category, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_SponsorshipCategory_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_SponsorshipCategory_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_CategoryType()
            {
                Assert.AreEqual(CategoryType.Sponsorship, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_Order()
            {
                Assert.AreEqual((int)CategoryType.Sponsorship, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.Sponsorship).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_IncludeInFormula()
            {
                Assert.IsTrue(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_Description()
            {
                Assert.AreEqual(CategoryType.Sponsorship.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_Transactions()
            {
                Assert.AreEqual(3, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_Transactions_NullSponsorItems()
            {
                result = CategoryModel.Build(new SponsorshipCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_Amount()
            {
                var expected = category.SponsorItems.Sum(si => si.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_Amount_NotVisible()
            {
                result = CategoryModel.Build(new SponsorshipCategory(), termUtility, displayTermPeriodCode);
                Assert.IsNull(result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_IsVisible_False()
            {
                result = CategoryModel.Build(new SponsorshipCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_IsDebit()
            {
                Assert.IsFalse(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_SponsorshipCategory_IsCredit()
            {
                Assert.IsTrue(result.IsCredit);
            }
        }

        [TestClass]
        public class CategoryModelTests_Build_DepositCategory : CategoryModelTests
        {
            DepositCategory category;

            [TestInitialize]
            public void CategoryModelTests_Build_DepositCategory_Initialize()
            {
                category = BuildDetailedAccountPeriod.BuildDepositCategory();
                result = CategoryModel.Build(category, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositCategory_NullCategory()
            {
                result = CategoryModel.Build((DepositCategory)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositCategory_NullTermUtility()
            {
                result = CategoryModel.Build(category, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositCategory_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositCategory_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_CategoryType()
            {
                Assert.AreEqual(CategoryType.Deposit, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_Order()
            {
                Assert.AreEqual((int)CategoryType.Deposit, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.Deposit).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_IncludeInFormula()
            {
                Assert.IsTrue(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_Description()
            {
                Assert.AreEqual(CategoryType.Deposit.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_Transactions()
            {
                Assert.AreEqual(3, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_Transactions_NullDeposits()
            {
                result = CategoryModel.Build(new DepositCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_Amount()
            {
                var expected = category.Deposits.Sum(d => (d.PaidAmount ?? 0) + d.RemainingAmount);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_Amount_NotVisible()
            {
                result = CategoryModel.Build(new DepositCategory(), termUtility, displayTermPeriodCode);
                Assert.IsNull(result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_IsVisible_False()
            {
                result = CategoryModel.Build(new DepositCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_IsDebit()
            {
                Assert.IsFalse(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositCategory_IsCredit()
            {
                Assert.IsTrue(result.IsCredit);
            }
        }

        [TestClass]
        public class CategoryModelTests_Build_RefundCategory : CategoryModelTests
        {
            RefundCategory category;

            [TestInitialize]
            public void CategoryModelTests_Build_RefundCategory_Initialize()
            {
                category = BuildDetailedAccountPeriod.BuildRefundCategory();
                result = CategoryModel.Build(category, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_RefundCategory_NullCategory()
            {
                result = CategoryModel.Build((RefundCategory)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_RefundCategory_NullTermUtility()
            {
                result = CategoryModel.Build(category, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_RefundCategory_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_RefundCategory_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_CategoryType()
            {
                Assert.AreEqual(CategoryType.Refund, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_Order()
            {
                Assert.AreEqual((int)CategoryType.Refund, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.Refund).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_IncludeInFormula()
            {
                Assert.IsTrue(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_Description()
            {
                Assert.AreEqual(CategoryType.Refund.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_Transactions()
            {
                Assert.AreEqual(3, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_Transactions_NullRefunds()
            {
                result = CategoryModel.Build(new RefundCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_Amount()
            {
                var expected = category.Refunds.Sum(d => d.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_Amount_NotVisible()
            {
                result = CategoryModel.Build(new RefundCategory(), termUtility, displayTermPeriodCode);
                Assert.IsNull(result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_IsVisible_False()
            {
                result = CategoryModel.Build(new RefundCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_IsDebit()
            {
                Assert.IsTrue(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_RefundCategory_IsCredit()
            {
                Assert.IsFalse(result.IsCredit);
            }
        }

        [TestClass]
        public class CategoryModelTests_Build_DepositsDue : CategoryModelTests
        {
            List<DepositDue> depsDue;

            [TestInitialize]
            public void CategoryModelTests_Build_DepositsDue_Initialize()
            {
                depsDue = BuildDetailedAccountPeriod.BuildDepositsDue();
                result = CategoryModel.Build(depsDue, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositsDue_NullDepositsDue()
            {
                result = CategoryModel.Build((IEnumerable<DepositDue>)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositsDue_NullTermUtility()
            {
                result = CategoryModel.Build(depsDue, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositsDue_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(depsDue, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_DepositsDue_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(depsDue, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_CategoryType()
            {
                Assert.AreEqual(CategoryType.DepositDue, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_Order()
            {
                Assert.AreEqual((int)CategoryType.DepositDue, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.DepositDue).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_IncludeInFormula()
            {
                Assert.IsFalse(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_Description()
            {
                Assert.AreEqual(CategoryType.DepositDue.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_Transactions()
            {
                Assert.AreEqual(3, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_NoTransactions()
            {
                result = CategoryModel.Build(new List<DepositDue>(), termUtility, displayTermPeriodCode);
                Assert.AreEqual(0, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_Amount()
            {
                Assert.AreEqual(null, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_IsVisible_False()
            {
                result = CategoryModel.Build(new List<DepositDue>(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_IsDebit()
            {
                Assert.IsFalse(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_DepositsDue_IsCredit()
            {
                Assert.IsTrue(result.IsCredit);
            }
        }

        [TestClass]
        public class CategoryModelTests_Build_PaymentPlanCategory : CategoryModelTests
        {
            PaymentPlanCategory category;

            [TestInitialize]
            public void CategoryModelTests_Build_PaymentPlanCategory_Initialize()
            {
                category = BuildDetailedAccountPeriod.BuildPaymentPlanCategory();
                result = CategoryModel.Build(category, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_PaymentPlanCategory_NullCategory()
            {
                result = CategoryModel.Build((PaymentPlanCategory)null, termUtility, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_PaymentPlanCategory_NullTermUtility()
            {
                result = CategoryModel.Build(category, null, displayTermPeriodCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_PaymentPlanCategory_NullDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CategoryModelTests_Build_PaymentPlanCategory_EmptyDisplayTermPeriodCode()
            {
                result = CategoryModel.Build(category, termUtility, string.Empty);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_CategoryType()
            {
                Assert.AreEqual(CategoryType.PaymentPlan, result.CategoryType);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_Order()
            {
                Assert.AreEqual((int)CategoryType.PaymentPlan, result.Order);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_Id()
            {
                Assert.AreEqual(displayTermPeriodCode + "-" + ((int)CategoryType.PaymentPlan).ToString(), result.Id);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_IncludeInFormula()
            {
                Assert.IsFalse(result.IncludeInFormula);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_Description()
            {
                Assert.AreEqual(CategoryType.PaymentPlan.ToString(), result.Description);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_Transactions()
            {
                Assert.AreEqual(3, result.Transactions.Count);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_Transactions_NullPaymentPlans()
            {
                result = CategoryModel.Build(new PaymentPlanCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.Transactions.Any());
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_Amount()
            {
                Assert.AreEqual(null, result.Amount);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_IsVisible_False()
            {
                result = CategoryModel.Build(new PaymentPlanCategory(), termUtility, displayTermPeriodCode);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_IsDebit()
            {
                Assert.IsFalse(result.IsDebit);
            }

            [TestMethod]
            public void CategoryModelTests_Build_PaymentPlanCategory_IsCredit()
            {
                Assert.IsTrue(result.IsCredit);
            }
        }
    }
}
