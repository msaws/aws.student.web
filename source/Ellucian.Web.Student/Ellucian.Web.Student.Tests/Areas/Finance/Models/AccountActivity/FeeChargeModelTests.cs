﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class FeeChargeModelTests
    {
        ActivityDateTermItem dateTermItem;
        TermPeriodsUtility termUtility;
        FeeChargeModel result;

        [TestInitialize]
        public void Initialize()
        {
            dateTermItem = BuildActivityDateTermItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = FeeChargeModel.Build(dateTermItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FeeChargeModel_Build_NullDetail()
        {
            result = FeeChargeModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FeeChargeModel_Build_NullTermUtility()
        {
            result = FeeChargeModel.Build(dateTermItem, null);
        }

        [TestMethod]
        public void FeeChargeModel_Build_Description()
        {
            Assert.AreEqual(dateTermItem.Description, result.Description);
        }

        [TestMethod]
        public void FeeChargeModel_Build_TermId()
        {
            Assert.AreEqual(dateTermItem.TermId, result.TermId);
        }

        [TestMethod]
        public void FeeChargeModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(dateTermItem.TermId), result.TermDescription);
        }

        [TestMethod]
        public void FeeChargeModel_Build_Amount()
        {
            Assert.AreEqual(dateTermItem.Amount, result.Amount);
        }
    }
}
