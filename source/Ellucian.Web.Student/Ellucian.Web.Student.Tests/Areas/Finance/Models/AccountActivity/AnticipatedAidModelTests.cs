﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class AnticipatedAidModelTests
    {
        ActivityFinancialAidTerm faItem;
        TermPeriodsUtility termUtility;
        AnticipatedAidModel result;

        [TestInitialize]
        public void Initialize()
        {
            faItem = BuildActivityFinancialAidTerm.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = AnticipatedAidModel.Build(faItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AnticipatedAidModel_Build_NullDetail()
        {
            result = AnticipatedAidModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AnticipatedAidModel_Build_NullTermUtility()
        {
            result = AnticipatedAidModel.Build(faItem, null);
        }

        [TestMethod]
        public void AnticipatedAidModel_Build_TermId()
        {
            Assert.AreEqual(faItem.AwardTerm, result.TermId);
        }

        [TestMethod]
        public void AnticipatedAidModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(faItem.AwardTerm), result.TermDescription);
        }

        [TestMethod]
        public void AnticipatedAidModel_Build_AnticipatedAmount()
        {
            Assert.AreEqual(faItem.AnticipatedAmount, result.AnticipatedAmount);
        }

        [TestMethod]
        public void AnticipatedAidModel_Build_DisbursementAmount()
        {
            Assert.AreEqual(faItem.DisbursedAmount, result.DisbursementAmount);
        }
    }
}
