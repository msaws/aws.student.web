﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Tests.Utility;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Web;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class TermPeriodBalanceModelTests
    {
        AccountPeriod period;
        TermPeriodBalanceModel result;

        [TestInitialize]
        public void Initialize()
        {
            period = new AccountPeriod()
            {
                Balance = 1000m,
                Description = "Current Period",
                Id = "CUR",
                EndDate = DateTime.Today.AddDays(30),
                StartDate = DateTime.Today.AddDays(-30),
                AssociatedPeriods = new List<string>() { "2014/FA" }
            };
            HttpContext.Current = BuildHttpContext.FakeHttpContext();
            result = TermPeriodBalanceModel.Build(period, "0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TermPeriodBalanceModel_Build_NullPeriod()
        {
            var result = TermPeriodBalanceModel.Build(null, "0001234");
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_Id()
        {
            Assert.AreEqual(period.Id, result.Id);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_Description()
        {
            Assert.AreEqual(period.Description, result.Description);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_DisplayText()
        {
            Assert.AreEqual(period.Description + ": " + FormatUtility.FormatAsCurrency(period.Balance), result.DisplayText);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_Balance()
        {
            Assert.AreEqual(period.Balance, result.Balance);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_AssociatedPeriods()
        {
            Assert.AreEqual(period.AssociatedPeriods.Count, result.AssociatedTerms.Count);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_ProcessUrl()
        {
            Assert.AreEqual(null, result.ProcessUrl);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_ProcessUrl_NullPersonId()
        {
            Assert.AreEqual(null, result.ProcessUrl);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_AccountActivityUrl()
        {
            Assert.AreEqual(null, result.AccountActivityUrl);
        }

        [TestMethod]
        public void TermPeriodBalanceModel_Build_AccountActivityUrl_NullPersonId()
        {
            Assert.AreEqual(null, result.AccountActivityUrl);
        }
    }
}
