﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class ChargeGroupModelTests
    {
        TermPeriodsUtility termUtility;
        ChargeGroupModel result;

        [TestInitialize]
        public void Initialize()
        {
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
        }

        [TestClass]
        public class ChargeGroupModelTests_Build_TuitionByTotal : ChargeGroupModelTests
        {
            TuitionByTotalType type;

            [TestInitialize]
            public void ChargeGroupModelTests_Build_TuitionByTotal_Initialize()
            {
                type = new TuitionByTotalType()
                {
                    DisplayOrder = 1,
                    Name = "Tuition by Total Charges",
                    TotalCharges = new List<ActivityTuitionItem>()
                    {
                        BuildActivityTuitionItem.Build("101", "2015/FA"),
                        BuildActivityTuitionItem.Build("102", "2013/FA"),
                        BuildActivityTuitionItem.Build("Other Tuition Activity"),
                        BuildActivityTuitionItem.Build("103", "2014/FA"),
                    }
                };
                result = ChargeGroupModel.Build(type, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_TuitionByTotal_NullGroup()
            {
                result = ChargeGroupModel.Build((TuitionByTotalType)null, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_TuitionByTotal_NullUtility()
            {
                result = ChargeGroupModel.Build(type, null);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_Id()
            {
                Assert.AreEqual(type.Name.Replace(' ', '_'), result.Id);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_IdWithInvalidChars()
            {
                var type2 = new TuitionByTotalType()
                {
                    DisplayOrder = 1,
                    Name = @"Tui!tion/by@Total.Char \ges",
                    TotalCharges = new List<ActivityTuitionItem>()
                    {
                        BuildActivityTuitionItem.Build("101", "2015/FA"),
                        BuildActivityTuitionItem.Build("102", "2013/FA"),
                        BuildActivityTuitionItem.Build("Other Tuition Activity"),
                        BuildActivityTuitionItem.Build("103", "2014/FA"),
                    }
                };
                result = ChargeGroupModel.Build(type2, termUtility);
                Assert.AreEqual("Tui_tion_by_Total_Char__ges", result.Id);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_Description()
            {
                Assert.AreEqual(type.Name, result.Description);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_Order()
            {
                Assert.AreEqual(type.DisplayOrder, result.Order);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_ChargeType()
            {
                Assert.AreEqual(ChargeType.TuitionByTotal, result.ChargeType);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_ChargeDetails()
            {
                Assert.AreEqual(type.TotalCharges.Count, result.ChargeDetails.Count);
                Assert.AreEqual(type.TotalCharges[1].TermId, result.ChargeDetails[0].TermId);
                Assert.AreEqual(type.TotalCharges[3].TermId, result.ChargeDetails[1].TermId);
                Assert.AreEqual(type.TotalCharges[0].TermId, result.ChargeDetails[2].TermId);
                Assert.AreEqual(type.TotalCharges[2].TermId, result.ChargeDetails[3].TermId);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_IsVisible_False_NullCharges()
            {
                result = ChargeGroupModel.Build(new TuitionByTotalType() { DisplayOrder = 1, Name = "Tuition by Total Charges", TotalCharges = null }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_IsVisible_False_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new TuitionByTotalType() { DisplayOrder = 1, Name = "Tuition by Total Charges", TotalCharges = new List<ActivityTuitionItem>() }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_Amount()
            {
                var expected = type.TotalCharges.Sum(tc => tc.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_Amount_NullCharges()
            {
                result = ChargeGroupModel.Build(new TuitionByTotalType() { DisplayOrder = 1, Name = "Tuition by Total Charges", TotalCharges = null }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionByTotal_Amount_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new TuitionByTotalType() { DisplayOrder = 1, Name = "Tuition by Total Charges", TotalCharges = new List<ActivityTuitionItem>() }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }
        }

        [TestClass]
        public class ChargeGroupModelTests_Build_TuitionBySection : ChargeGroupModelTests
        {
            TuitionBySectionType type;

            [TestInitialize]
            public void ChargeGroupModelTests_Build_TuitionBySection_Initialize()
            {
                type = new TuitionBySectionType()
                {
                    DisplayOrder = 1,
                    Name = "Tuition by Section Charges",
                    SectionCharges = new List<ActivityTuitionItem>()
                    {
                        BuildActivityTuitionItem.Build("101", "2015/FA"),
                        BuildActivityTuitionItem.Build("102", "2013/FA"),
                        BuildActivityTuitionItem.Build("Other Tuition Activity"),
                        BuildActivityTuitionItem.Build("103", "2014/FA"),
                    }
                };
                result = ChargeGroupModel.Build(type, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_TuitionBySection_NullGroup()
            {
                result = ChargeGroupModel.Build((TuitionBySectionType)null, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_TuitionBySection_NullUtility()
            {
                result = ChargeGroupModel.Build(type, null);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_Id()
            {
                Assert.AreEqual(type.Name.Replace(' ', '_'), result.Id);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_Description()
            {
                Assert.AreEqual(type.Name, result.Description);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_Order()
            {
                Assert.AreEqual(type.DisplayOrder, result.Order);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_ChargeType()
            {
                Assert.AreEqual(ChargeType.TuitionBySection, result.ChargeType);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_ChargeDetails()
            {
                Assert.AreEqual(type.SectionCharges.Count, result.ChargeDetails.Count);
                Assert.AreEqual(type.SectionCharges[1].TermId, result.ChargeDetails[0].TermId);
                Assert.AreEqual(type.SectionCharges[3].TermId, result.ChargeDetails[1].TermId);
                Assert.AreEqual(type.SectionCharges[0].TermId, result.ChargeDetails[2].TermId);
                Assert.AreEqual(type.SectionCharges[2].TermId, result.ChargeDetails[3].TermId);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_IsVisible_False_NullCharges()
            {
                result = ChargeGroupModel.Build(new TuitionBySectionType() { DisplayOrder = 1, Name = "Tuition by Section Charges", SectionCharges = null }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_IsVisible_False_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new TuitionBySectionType() { DisplayOrder = 1, Name = "Tuition by Section Charges", SectionCharges = new List<ActivityTuitionItem>() }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_Amount()
            {
                var expected = type.SectionCharges.Sum(tc => tc.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_Amount_NullCharges()
            {
                result = ChargeGroupModel.Build(new TuitionBySectionType() { DisplayOrder = 1, Name = "Tuition by Section Charges", SectionCharges = null }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_TuitionBySection_Amount_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new TuitionBySectionType() { DisplayOrder = 1, Name = "Tuition by Section Charges", SectionCharges = new List<ActivityTuitionItem>() }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }
        }
        
        [TestClass]
        public class ChargeGroupModelTests_Build_Fee : ChargeGroupModelTests
        {
            FeeType type;

            [TestInitialize]
            public void ChargeGroupModelTests_Build_Fee_Initialize()
            {
                type = new FeeType()
                {
                    DisplayOrder = 1,
                    Name = "Fee Charges",
                    FeeCharges = new List<ActivityDateTermItem>()
                    {
                        BuildActivityDateTermItem.Build(),
                        BuildActivityDateTermItem.Build(),
                        BuildActivityDateTermItem.Build()
                    }
                };
                result = ChargeGroupModel.Build(type, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_Fee_NullGroup()
            {
                result = ChargeGroupModel.Build((FeeType)null, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_Fee_NullUtility()
            {
                result = ChargeGroupModel.Build(type, null);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_Id()
            {
                Assert.AreEqual(type.Name.Replace(' ', '_'), result.Id);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_Description()
            {
                Assert.AreEqual(type.Name, result.Description);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_Order()
            {
                Assert.AreEqual(type.DisplayOrder, result.Order);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_ChargeType()
            {
                Assert.AreEqual(ChargeType.Fees, result.ChargeType);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_ChargeDetails()
            {
                Assert.AreEqual(type.FeeCharges.Count, result.ChargeDetails.Count);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_IsVisible_False_NullCharges()
            {
                result = ChargeGroupModel.Build(new FeeType() { DisplayOrder = 1, Name = "Fee Charges", FeeCharges = null }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_IsVisible_False_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new FeeType() { DisplayOrder = 1, Name = "Fee Charges", FeeCharges = new List<ActivityDateTermItem>() }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_Amount()
            {
                var expected = type.FeeCharges.Sum(tc => tc.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_Amount_NullCharges()
            {
                result = ChargeGroupModel.Build(new FeeType() { DisplayOrder = 1, Name = "Fee Charges", FeeCharges = null }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Fee_Amount_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new FeeType() { DisplayOrder = 1, Name = "Fee Charges", FeeCharges = new List<ActivityDateTermItem>() }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }
        }

        [TestClass]
        public class ChargeGroupModelTests_Build_RoomAndBoard : ChargeGroupModelTests
        {
            RoomAndBoardType type;

            [TestInitialize]
            public void ChargeGroupModelTests_Build_RoomAndBoard_Initialize()
            {
                type = new RoomAndBoardType()
                {
                    DisplayOrder = 1,
                    Name = "Room and Board Charges",
                    RoomAndBoardCharges = new List<ActivityRoomAndBoardItem>()
                    {
                        BuildActivityRoomAndBoardItem.Build(),
                        BuildActivityRoomAndBoardItem.Build(),
                        BuildActivityRoomAndBoardItem.Build()
                    }
                };
                result = ChargeGroupModel.Build(type, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_RoomAndBoard_NullGroup()
            {
                result = ChargeGroupModel.Build((RoomAndBoardType)null, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_RoomAndBoard_NullUtility()
            {
                result = ChargeGroupModel.Build(type, null);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_Id()
            {
                Assert.AreEqual(type.Name.Replace(' ', '_'), result.Id);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_Description()
            {
                Assert.AreEqual(type.Name, result.Description);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_Order()
            {
                Assert.AreEqual(type.DisplayOrder, result.Order);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_ChargeType()
            {
                Assert.AreEqual(ChargeType.RoomAndBoard, result.ChargeType);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_ChargeDetails()
            {
                Assert.AreEqual(type.RoomAndBoardCharges.Count, result.ChargeDetails.Count);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_IsVisible_False_NullCharges()
            {
                result = ChargeGroupModel.Build(new RoomAndBoardType() { DisplayOrder = 1, Name = "Room and Board Charges", RoomAndBoardCharges = null }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_IsVisible_False_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new RoomAndBoardType() { DisplayOrder = 1, Name = "Room and Board Charges", RoomAndBoardCharges = new List<ActivityRoomAndBoardItem>() }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_Amount()
            {
                var expected = type.RoomAndBoardCharges.Sum(tc => tc.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_Amount_NullCharges()
            {
                result = ChargeGroupModel.Build(new RoomAndBoardType() { DisplayOrder = 1, Name = "Room and Board Charges", RoomAndBoardCharges = null }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_RoomAndBoard_Amount_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new RoomAndBoardType() { DisplayOrder = 1, Name = "Room and Board Charges", RoomAndBoardCharges = new List<ActivityRoomAndBoardItem>() }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }
        }

        [TestClass]
        public class ChargeGroupModelTests_Build_Other : ChargeGroupModelTests
        {
            OtherType type;

            [TestInitialize]
            public void ChargeGroupModelTests_Build_Other_Initialize()
            {
                type = new OtherType()
                {
                    DisplayOrder = 1,
                    Name = "Other Charges",
                    OtherCharges = new List<ActivityDateTermItem>()
                    {
                        BuildActivityDateTermItem.Build(),
                        BuildActivityDateTermItem.Build(),
                        BuildActivityDateTermItem.Build()
                    }
                };
                result = ChargeGroupModel.Build(type, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_Other_NullGroup()
            {
                result = ChargeGroupModel.Build((OtherType)null, termUtility);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ChargeGroupModelTests_Build_Other_NullUtility()
            {
                result = ChargeGroupModel.Build(type, null);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_Id()
            {
                Assert.AreEqual(type.Name.Replace(' ', '_'), result.Id);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_Description()
            {
                Assert.AreEqual(type.Name, result.Description);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_Order()
            {
                Assert.AreEqual(type.DisplayOrder, result.Order);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_ChargeType_Default()
            {
                Assert.AreEqual(ChargeType.Other, result.ChargeType);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_ChargeType_MiscType()
            {
                result = ChargeGroupModel.Build(type, termUtility, true);
                Assert.AreEqual(ChargeType.Miscellaneous, result.ChargeType);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_ChargeDetails()
            {
                Assert.AreEqual(type.OtherCharges.Count, result.ChargeDetails.Count);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_IsVisible_True()
            {
                Assert.IsTrue(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_IsVisible_False_NullCharges()
            {
                result = ChargeGroupModel.Build(new OtherType() { DisplayOrder = 1, Name = "Other Charges", OtherCharges = null }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_IsVisible_False_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new OtherType() { DisplayOrder = 1, Name = "Other Charges", OtherCharges = new List<ActivityDateTermItem>() }, termUtility);
                Assert.IsFalse(result.IsVisible);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_Amount()
            {
                var expected = type.OtherCharges.Sum(tc => tc.Amount ?? 0);
                Assert.AreEqual(expected, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_Amount_NullCharges()
            {
                result = ChargeGroupModel.Build(new OtherType() { DisplayOrder = 1, Name = "Other Charges", OtherCharges = null }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }

            [TestMethod]
            public void ChargeGroupModelTests_Build_Other_Amount_EmptyCharges()
            {
                result = ChargeGroupModel.Build(new OtherType() { DisplayOrder = 1, Name = "Other Charges", OtherCharges = new List<ActivityDateTermItem>() }, termUtility);
                Assert.AreEqual(0, result.Amount);
            }
        }
    }
}
