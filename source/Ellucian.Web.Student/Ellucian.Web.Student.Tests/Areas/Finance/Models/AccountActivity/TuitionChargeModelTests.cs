﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class TuitionChargeModelTests
    {
        ActivityTuitionItem tuitionItem;
        TermPeriodsUtility termUtility;
        TuitionChargeModel result;

        [TestInitialize]
        public void Initialize()
        {
            tuitionItem = BuildActivityTuitionItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = TuitionChargeModel.Build(tuitionItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TuitionChargeModel_Build_NullDetail()
        {
            result = TuitionChargeModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TuitionChargeModel_Build_NullTermUtility()
        {
            result = TuitionChargeModel.Build(tuitionItem, null);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_Name()
        {
            Assert.AreEqual(tuitionItem.Id, result.Name);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_Title()
        {
            Assert.AreEqual(tuitionItem.Description, result.Title);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_TermId()
        {
            Assert.AreEqual(termUtility.GetTermIdForTermDescription(tuitionItem.TermId), result.TermId);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_TermDescription()
        {
            Assert.AreEqual(tuitionItem.TermId, result.TermDescription);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_Amount()
        {
            Assert.AreEqual(tuitionItem.Amount, result.Amount);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_BillingCredits()
        {
            Assert.AreEqual(tuitionItem.BillingCredits, result.BillingCredits);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_CEUs()
        {
            Assert.AreEqual(tuitionItem.Ceus, result.CEUs);
        }
        
        [TestMethod]
        public void TuitionChargeModel_Build_MeetingDays()
        {
            Assert.AreEqual(DayOfWeekConverter.EnumCollectionToString(tuitionItem.Days), result.MeetingDays);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_MeetingTimes()
        {
            Assert.AreEqual(SectionUtility.GetMeetingTimes(tuitionItem.StartTime, tuitionItem.EndTime), result.MeetingTimes);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_MeetingRoom_NotNull()
        {
            Assert.AreEqual(tuitionItem.Classroom.Replace('*', ' '), result.MeetingRoom);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_MeetingRoom_Null()
        {
            var nullRoomItem = BuildActivityTuitionItem.Build();
            nullRoomItem.Classroom = null;
            result = TuitionChargeModel.Build(nullRoomItem, termUtility);
            Assert.AreEqual(null, result.MeetingRoom);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_Instructor()
        {
            Assert.AreEqual(tuitionItem.Instructor, result.Instructor);
        }

        [TestMethod]
        public void TuitionChargeModel_Build_Status()
        {
            Assert.AreEqual(tuitionItem.Status, result.Status);
        }
    }
}
