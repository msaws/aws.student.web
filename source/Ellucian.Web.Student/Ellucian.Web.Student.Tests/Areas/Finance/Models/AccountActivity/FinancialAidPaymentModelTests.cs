﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class FinancialAidPaymentModelTests
    {
        ActivityFinancialAidItem faItem;
        TermPeriodsUtility termUtility;
        FinancialAidPaymentModel result;

        [TestInitialize]
        public void Initialize()
        {
            faItem = BuildActivityFinancialAidItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = FinancialAidPaymentModel.Build(faItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FinancialAidPaymentModel_Build_NullDetail()
        {
            result = FinancialAidPaymentModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FinancialAidPaymentModel_Build_NullTermUtility()
        {
            result = FinancialAidPaymentModel.Build(faItem, null);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_Id()
        {
            Assert.AreEqual(faItem.PeriodAward, result.Id);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_Description()
        {
            Assert.AreEqual(faItem.AwardDescription, result.Description);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_AwardAmount()
        {
            Assert.AreEqual(faItem.AwardAmount, result.AwardAmount);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_Comment()
        {
            Assert.AreEqual(faItem.Comments, result.Comment);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_IneligibleAmount()
        {
            Assert.AreEqual(faItem.IneligibleAmount, result.IneligibleAmount);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_LoanFeeAmount()
        {
            Assert.AreEqual(faItem.LoanFee, result.LoanFeeAmount);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_OtherAmount()
        {
            Assert.AreEqual(faItem.OtherTermAmount, result.OtherAmount);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_AwardTerms_NullDetailAwardTerms()
        {
            var item = faItem;
            item.AwardTerms = null;
            result = FinancialAidPaymentModel.Build(item, termUtility);
            Assert.AreEqual(0, result.AwardTerms.Count);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_AwardTerms_AtLeastOneAwardTerm()
        {
            Assert.AreEqual(faItem.AwardTerms.Count, result.AwardTerms.Count);
            for (int i = 0; i < result.AwardTerms.Count; i++)
            {
                Assert.AreEqual(faItem.AwardTerms[i].AnticipatedAmount, result.AwardTerms[i].AnticipatedAmount);
                Assert.AreEqual(faItem.AwardTerms[i].DisbursedAmount, result.AwardTerms[i].DisbursementAmount);
                Assert.AreEqual(faItem.AwardTerms[i].AwardTerm, result.AwardTerms[i].TermId);
                Assert.AreEqual(termUtility.GetTermDescription(faItem.AwardTerms[i].AwardTerm), result.AwardTerms[i].TermDescription);
            }
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_AwardTerms_OneAwardTermWithNullAwardTerm()
        {
            var item = faItem;
            item.AwardTerms[0].AwardTerm = null;
            result = FinancialAidPaymentModel.Build(item, termUtility);
            Assert.AreEqual(0, result.AwardTerms.Count);
        }

        [TestMethod]
        public void FinancialAidPaymentModel_Build_AwardTerms_OneAwardTermWithEmptyAwardTerm()
        {
            var item = faItem;
            item.AwardTerms[0].AwardTerm = string.Empty;
            result = FinancialAidPaymentModel.Build(item, termUtility);
            Assert.AreEqual(0, result.AwardTerms.Count);
        }
    }
}
