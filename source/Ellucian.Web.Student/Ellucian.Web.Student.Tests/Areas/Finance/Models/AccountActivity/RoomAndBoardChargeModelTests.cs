﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class RoomAndBoardChargeModelTests
    {
        ActivityRoomAndBoardItem rbItem;
        TermPeriodsUtility termUtility;
        RoomAndBoardChargeModel result;

        [TestInitialize]
        public void Initialize()
        {
            rbItem = BuildActivityRoomAndBoardItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = RoomAndBoardChargeModel.Build(rbItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RoomAndBoardChargeModel_Build_NullDetail()
        {
            result = RoomAndBoardChargeModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RoomAndBoardChargeModel_Build_NullTermUtility()
        {
            result = RoomAndBoardChargeModel.Build(rbItem, null);
        }

        [TestMethod]
        public void RoomAndBoardChargeModel_Build_Amount()
        {
            Assert.AreEqual(rbItem.Amount, result.Amount);
        }

        [TestMethod]
        public void RoomAndBoardChargeModel_Build_Building()
        {
            Assert.AreEqual(rbItem.Room, result.Building);
        }

        [TestMethod]
        public void RoomAndBoardChargeModel_Build_Description()
        {
            Assert.AreEqual(rbItem.Description, result.Description);
        }

        [TestMethod]
        public void RoomAndBoardChargeModel_Build_Date()
        {
            Assert.AreEqual(rbItem.Date, result.Date);
        }

        [TestMethod]
        public void RoomAndBoardChargeModel_Build_TermId()
        {
            Assert.AreEqual(rbItem.TermId, result.TermId);
        }

        [TestMethod]
        public void RoomAndBoardChargeModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(rbItem.TermId), result.TermDescription);
        }

    }
}
