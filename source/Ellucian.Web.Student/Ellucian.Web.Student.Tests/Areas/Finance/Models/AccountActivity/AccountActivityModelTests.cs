﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Tests.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class AccountActivityModelTests
    {
        AccountActivityPeriods allPeriods;
        DetailedAccountPeriod selectedPeriod;
        AccountHolder accountHolder;
        FinanceConfiguration configuration;
        ICurrentUser currentUser;
        string displayTermPeriodCode;
        List<Term> terms;
        AccountActivityModel model;

        [TestInitialize]
        public void Initialize()
        {
            displayTermPeriodCode = "2014/FA";

            allPeriods = new AccountActivityPeriods()
            {
                NonTermActivity = new AccountPeriod(),
                Periods = new List<AccountPeriod>() 
                {
                    new AccountPeriod()
                    {
                        AssociatedPeriods = new List<string>() { displayTermPeriodCode },
                        Balance = 10000m,
                        Description = displayTermPeriodCode,
                        EndDate = DateTime.Today.AddDays(30),
                        Id = displayTermPeriodCode,
                        StartDate = DateTime.Today.AddDays(-30)
                    }
                }
            };

            selectedPeriod = BuildDetailedAccountPeriod.Build(displayTermPeriodCode);
            
            accountHolder = new AccountHolder()
            {
                Id = "HOLDER1",
                PreferredName = "My Name",
                DepositsDue = BuildDetailedAccountPeriod.BuildDepositsDue()
            };

            configuration = new FinanceConfiguration()
            {
                PaymentMethods = new List<AvailablePaymentMethod>(){
                    new AvailablePaymentMethod(){
                        Description = "Payment Method 1",
                        InternalCode = "PM1",
                    },
                    new AvailablePaymentMethod(){
                        Description = "Payment Method 2",
                        InternalCode = "PM2",
                    }
                },
                NotificationText = "My notification",
                ActivityDisplay = ActivityDisplay.DisplayByTerm,
                PaymentDisplay = PaymentDisplay.DisplayByTerm,
                PartialAccountPaymentsAllowed = true,
                PartialDepositPaymentsAllowed = true,
                PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed,
                ECommercePaymentsAllowed = true,
                SelfServicePaymentsAllowed = true,
                Periods = new List<FinancialPeriod>() 
                {
                    new FinancialPeriod() { End = DateTime.Today.AddDays(-31), Type = PeriodType.Past },
                    new FinancialPeriod() { End = DateTime.Today.AddDays(30), Start = DateTime.Today.AddDays(-30), Type = PeriodType.Current },
                    new FinancialPeriod() { End = DateTime.Today.AddDays(31), Type = PeriodType.Future },
                }
            };

            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

            // Proxy-For formatted name
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
            // Proxy-For person id
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
            // Proxy-For permissions, one line for each
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAA"));

            currentUser = new CurrentUser(claim);


            terms = BuildTermPeriodsUtility.BuildTerms;
            HttpContext.Current = BuildHttpContext.FakeHttpContext();
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, terms, currentUser);
        }

        [TestMethod]
        public void AccountActivityModel_DefaultConstructor()
        {
            model = new AccountActivityModel();
            Assert.IsFalse(model.FormulaCategories.Any());
            Assert.IsFalse(model.NonFormulaCategories.Any());
            Assert.IsNull(model.TermPeriodBalances);
            Assert.IsNotNull(model.ReceiptData);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccountActivityModel_Build_NullAccountActivityPeriods()
        {
            model = AccountActivityModel.Build(null, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, terms, currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccountActivityModel_Build_NullDetailedAccountPeriod()
        {
            model = AccountActivityModel.Build(allPeriods, null, accountHolder, configuration, displayTermPeriodCode, terms, currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccountActivityModel_Build_NullAccountHolder()
        {
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, null, configuration, displayTermPeriodCode, terms, currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccountActivityModel_Build_NullFinanceConfiguration()
        {
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, null, displayTermPeriodCode, terms, currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccountActivityModel_Build_NullTerms()
        {
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, null, currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccountActivityModel_Build_NoTerms()
        {
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, new List<Term>(), currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccountActivityModel_Build_NullCurrentUser()
        {
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, terms, null);
        }

        [TestMethod]
        public void AccountActivityModel_Build_AlertMessage()
        {
            Assert.AreEqual(configuration.NotificationText, model.AlertMessage);
        }

        [TestMethod]
        public void AccountActivityModel_Build_IsTermDisplay()
        {
            Assert.AreEqual(configuration.ActivityDisplay == ActivityDisplay.DisplayByTerm, model.IsTermDisplay);
        }

        [TestMethod]
        public void AccountActivityModel_Build_DisplayTermPeriodCode()
        {
            Assert.AreEqual(displayTermPeriodCode, model.DisplayTermPeriodCode);
        }

        [TestMethod]
        public void AccountActivityModel_Build_DisplayTermPeriodDescription()
        {
            Assert.AreEqual(BuildTermPeriodsUtility.TermPeriodsUtility.GetTermDescription(displayTermPeriodCode), model.DisplayTermPeriodDescription);
        }

        [TestMethod]
        public void AccountActivityModel_Build_PersonId()
        {
            Assert.AreEqual(accountHolder.Id, model.PersonId);
        }

        [TestMethod]
        public void AccountActivityModel_Build_PersonName()
        {
            Assert.AreEqual(accountHolder.PreferredName, model.PersonName);
        }

        [TestMethod]
        public void AccountActivityModel_Build_IsAdminUser_False()
        {
            Assert.IsFalse(model.IsAdminUser);
        }

        [TestMethod]
        public void AccountActivityModel_Build_IsAdminUser_True()
        {
            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

            currentUser = new CurrentUser(claim);
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, terms, currentUser);

            Assert.IsTrue(model.IsAdminUser);
        }

        [TestMethod]
        public void AccountActivityModel_Build_FormulaCategories()
        {
            Assert.AreEqual(6, model.FormulaCategories.Count);
        }

        [TestMethod]
        public void AccountActivityModel_Build_NonFormulaCategories()
        {
            Assert.AreEqual(2, model.NonFormulaCategories.Count);
        }

        [TestMethod]
        public void AccountActivityModel_Build_DisclaimerText()
        {
            Assert.IsNull(model.DisclaimerText);
        }

        [TestMethod]
        public void AccountActivityModel_Build_BalanceImageUrl()
        {
            Assert.IsNull(model.BalanceImageUrl);
        }

        [TestMethod]
        public void AccountActivityModel_Build_BalanceImageAltText()
        {
            Assert.IsNull(model.BalanceImageAltText);
        }

        [TestMethod]
        public void AccountActivityModel_Build_HasPrivacyRestriction_False()
        {
            accountHolder.PrivacyStatusCode = null;
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, terms, currentUser);
            Assert.IsFalse(model.HasPrivacyRestriction);
        }

        [TestMethod]
        public void AccountActivityModel_Build_HasPrivacyRestriction_False2()
        {
            accountHolder.PrivacyStatusCode = "X";
            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
            currentUser = new CurrentUser(claim);

            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, terms, currentUser);
            Assert.IsFalse(model.HasPrivacyRestriction);
        }

        [TestMethod]
        public void AccountActivityModel_Build_PrivacyStatusCode_Set()
        {
            accountHolder.PrivacyStatusCode = "X";
            model = AccountActivityModel.Build(allPeriods, selectedPeriod, accountHolder, configuration, displayTermPeriodCode, terms, currentUser);
            Assert.AreEqual(model.PrivacyStatusCode, accountHolder.PrivacyStatusCode);
        }
    }
}
