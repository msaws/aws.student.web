﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class OtherChargeModelTests
    {
        ActivityDateTermItem dateTermItem;
        TermPeriodsUtility termUtility;
        OtherChargeModel result;

        [TestInitialize]
        public void Initialize()
        {
            dateTermItem = BuildActivityDateTermItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = OtherChargeModel.Build(dateTermItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void OtherChargeModel_Build_NullDetail()
        {
            result = OtherChargeModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void OtherChargeModel_Build_NullTermUtility()
        {
            result = OtherChargeModel.Build(dateTermItem, null);
        }

        [TestMethod]
        public void OtherChargeModel_Build_Amount()
        {
            Assert.AreEqual(dateTermItem.Amount, result.Amount);
        }

        [TestMethod]
        public void OtherChargeModel_Build_InvoiceNumber()
        {
            Assert.AreEqual(dateTermItem.Id, result.InvoiceNumber);
        }

        [TestMethod]
        public void OtherChargeModel_Build_Description()
        {
            Assert.AreEqual(dateTermItem.Description, result.Description);
        }

        [TestMethod]
        public void OtherChargeModel_Build_Date()
        {
            Assert.AreEqual(dateTermItem.Date, result.Date);
        }

        [TestMethod]
        public void OtherChargeModel_Build_TermId()
        {
            Assert.AreEqual(dateTermItem.TermId, result.TermId);
        }

        [TestMethod]
        public void OtherChargeModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(dateTermItem.TermId), result.TermDescription);
        }
    }
}
