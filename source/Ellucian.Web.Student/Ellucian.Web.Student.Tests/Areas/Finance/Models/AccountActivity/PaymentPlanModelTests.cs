﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class PaymentPlanModelTests
    {
        ActivityPaymentPlanDetailsItem planItem;
        TermPeriodsUtility termUtility;
        PaymentPlanModel result;

        [TestInitialize]
        public void Initialize()
        {
            planItem = BuildActivityPaymentPlanDetailsItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = PaymentPlanModel.Build(planItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentPlanModel_Build_NullDetail()
        {
            result = PaymentPlanModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentPlanModel_Build_NullTermUtility()
        {
            result = PaymentPlanModel.Build(planItem, null);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_Id()
        {
            Assert.AreEqual(planItem.Id, result.Id);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_Amount()
        {
            Assert.AreEqual(planItem.OriginalAmount, result.Amount);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_OriginalAmount()
        {
            Assert.AreEqual(planItem.OriginalAmount, result.OriginalAmount);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_CurrentBalance()
        {
            Assert.AreEqual(planItem.PaymentPlanSchedules.Sum(ps => ps.Amount + (ps.SetupCharge ?? 0) + (ps.LateCharge ?? 0) - (ps.AmountPaid ?? 0)), result.CurrentBalance);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_TypeDescription()
        {
            Assert.AreEqual(planItem.Type, result.TypeDescription);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_TermId()
        {
            Assert.AreEqual(planItem.TermId, result.TermId);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(planItem.TermId), result.TermDescription);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_AmountDue()
        {
            Assert.AreEqual(planItem.Amount, result.AmountDue);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_PaymentPlanApproval()
        {
            Assert.AreEqual(planItem.PaymentPlanApproval, result.PaymentPlanApproval);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_Schedules_NullPaymentPlanSchedules()
        {
            var item = planItem;
            item.PaymentPlanSchedules = null;
            result = PaymentPlanModel.Build(item, termUtility);
            Assert.AreEqual(0, result.Schedules.Count);
        }

        [TestMethod]
        public void PaymentPlanModel_Build_Schedules_AtLeastOnePlanSchedule()
        {
            Assert.AreEqual(planItem.PaymentPlanSchedules.Count, result.Schedules.Count);
        }
    }
}
