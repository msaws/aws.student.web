﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class SponsorshipModelTests
    {
        ActivitySponsorPaymentItem sponsorItem;
        TermPeriodsUtility termUtility;
        SponsorshipModel result;

        [TestInitialize]
        public void Initialize()
        {
            sponsorItem = BuildActivitySponsorPaymentItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;

            result = SponsorshipModel.Build(sponsorItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SponsorshipModel_Build_NullDetail()
        {
            result = SponsorshipModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SponsorshipModel_Build_NullTermUtility()
        {
            result = SponsorshipModel.Build(sponsorItem, null);
        }

        [TestMethod]
        public void SponsorshipModel_Build_Id()
        {
            Assert.AreEqual(sponsorItem.Id, result.Id);
        }

        [TestMethod]
        public void SponsorshipModel_Build_Amount()
        {
            Assert.AreEqual(sponsorItem.Amount, result.Amount);
        }

        [TestMethod]
        public void SponsorshipModel_Build_SponsorId()
        {
            Assert.AreEqual(sponsorItem.Id, result.SponsorId);
        }

        [TestMethod]
        public void SponsorshipModel_Build_SponsorName()
        {
            Assert.AreEqual(sponsorItem.Sponsorship, result.SponsorName);
        }

        [TestMethod]
        public void SponsorshipModel_Build_TermId()
        {
            Assert.AreEqual(sponsorItem.TermId, result.TermId);
        }

        [TestMethod]
        public void SponsorshipModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermIdForTermDescription(sponsorItem.TermId), result.TermDescription);
        }

        [TestMethod]
        public void SponsorshipModel_Build_Date()
        {
            Assert.AreEqual(sponsorItem.Date, result.Date);
        }
    }
}
