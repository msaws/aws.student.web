﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Colleague.Dtos.Finance;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class DepositDueModelTests
    {
        DepositDue depositDue;
        TermPeriodsUtility termUtility;
        DepositDueModel result;

        [TestInitialize]
        public void Initialize()
        {
            depositDue = BuildDepositDue.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = DepositDueModel.Build(depositDue, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DepositDueModel_Build_NullDetail()
        {
            result = DepositDueModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DepositDueModel_Build_NullTermUtility()
        {
            result = DepositDueModel.Build(depositDue, null);
        }

        [TestMethod]
        public void DepositDueModel_Build_Id()
        {
            Assert.AreEqual(depositDue.Id, result.Id);
        }

        [TestMethod]
        public void DepositDueModel_Build_TypeCode()
        {
            Assert.AreEqual(depositDue.DepositType, result.TypeCode);
        }

        [TestMethod]
        public void DepositDueModel_Build_TypeDescription()
        {
            Assert.AreEqual(depositDue.DepositTypeDescription, result.TypeDescription);
        }

        [TestMethod]
        public void DepositDueModel_Build_TermId()
        {
            Assert.AreEqual(depositDue.TermId, result.TermId);
        }

        [TestMethod]
        public void DepositDueModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(depositDue.TermId), result.TermDescription);
        }

        [TestMethod]
        public void DepositDueModel_Build_DueDate()
        {
            Assert.AreEqual(depositDue.DueDate, result.DueDate);
        }

        [TestMethod]
        public void DepositDueModel_Build_OriginalAmount()
        {
            Assert.AreEqual(depositDue.Amount, result.OriginalAmount);
        }

        [TestMethod]
        public void DepositDueModel_Build_PaidAmount()
        {
            Assert.AreEqual(depositDue.AmountPaid, result.PaidAmount);
        }

        [TestMethod]
        public void DepositDueModel_Build_Amount()
        {
            Assert.AreEqual(depositDue.Amount - depositDue.AmountPaid, result.Amount);
        }
    }
}
