﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class RefundModelTests
    {
        ActivityPaymentMethodItem payMethodItem;
        TermPeriodsUtility termUtility;
        RefundModel result;

        [TestInitialize]
        public void Initialize()
        {
            payMethodItem = BuildActivityPaymentMethodItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;

            result = RefundModel.Build(payMethodItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RefundModel_Build_NullDetail()
        {
            result = RefundModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RefundModel_Build_NullTermUtility()
        {
            result = RefundModel.Build(payMethodItem, null);
        }

        [TestMethod]
        public void RefundModel_Build_Id()
        {
            Assert.AreEqual(payMethodItem.Id, result.Id);
        }

        [TestMethod]
        public void RefundModel_Build_Description()
        {
            Assert.AreEqual(payMethodItem.Description, result.Description);
        }

        [TestMethod]
        public void RefundModel_Build_Date()
        {
            Assert.AreEqual(payMethodItem.Date, result.Date);
        }

        [TestMethod]
        public void RefundModel_Build_Amount()
        {
            Assert.AreEqual(payMethodItem.Amount, result.Amount);
        }

        [TestMethod]
        public void RefundModel_Build_TermId()
        {
            Assert.AreEqual(payMethodItem.TermId, result.TermId);
        }

        [TestMethod]
        public void RefundModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(payMethodItem.TermId), result.TermDescription);
        }

        [TestMethod]
        public void RefundModel_Build_DisbursementMethod()
        {
            Assert.AreEqual(payMethodItem.Method, result.DisbursementMethod);
        }

        [TestMethod]
        public void RefundModel_Build_TransactionNumber()
        {
            Assert.AreEqual(payMethodItem.TransactionNumber, result.TransactionNumber);
        }

        [TestMethod]
        public void RefundModel_Build_StatusDate()
        {
            Assert.AreEqual(payMethodItem.CheckDate, result.StatusDate);
        }

        [TestMethod]
        public void RefundModel_Build_StatusDate_NoCheckDate()
        {
            payMethodItem = new ActivityPaymentMethodItem() { StatusDate = DateTime.Today };
            result = RefundModel.Build(payMethodItem, termUtility);
            Assert.AreEqual(payMethodItem.StatusDate, result.StatusDate);
        }

        [TestMethod]
        public void RefundModel_Build_ReferenceNumber()
        {
            Assert.AreEqual(payMethodItem.CheckNumber, result.PaymentReferenceNumber);
        }

        [TestMethod]
        public void RefundModel_Build_ReferenceNumber_PaidStatus()
        {
            payMethodItem = new ActivityPaymentMethodItem() { Status = Colleague.Dtos.Finance.RefundVoucherStatus.Paid };
            result = RefundModel.Build(payMethodItem, termUtility);
            Assert.AreEqual(payMethodItem.CheckNumber, result.PaymentReferenceNumber);
        }

        [TestMethod]
        public void RefundModel_Build_ReferenceNumber_NoCheckNumber()
        {
            payMethodItem = new ActivityPaymentMethodItem() { Status = Colleague.Dtos.Finance.RefundVoucherStatus.Paid, CreditCardLastFourDigits = "8932" };
            result = RefundModel.Build(payMethodItem, termUtility);
            Assert.AreEqual(payMethodItem.CreditCardLastFourDigits, result.PaymentReferenceNumber);
        }

        [TestMethod]
        public void RefundModel_Build_ReferenceNumber_OutstandingStatus()
        {
            payMethodItem = new ActivityPaymentMethodItem() { Status = Colleague.Dtos.Finance.RefundVoucherStatus.Outstanding };
            result = RefundModel.Build(payMethodItem, termUtility);
            Assert.AreEqual(null, result.PaymentReferenceNumber);
        }

        [TestMethod]
        public void RefundModel_Build_ReferenceNumber_NotApprovedStatus()
        {
            payMethodItem = new ActivityPaymentMethodItem() { Status = Colleague.Dtos.Finance.RefundVoucherStatus.NotApproved };
            result = RefundModel.Build(payMethodItem, termUtility);
            Assert.AreEqual(null, result.PaymentReferenceNumber);
        }


        [TestMethod]
        public void RefundModel_Build_ReferenceNumber_CancelledStatus()
        {
            payMethodItem = new ActivityPaymentMethodItem() { Status = Colleague.Dtos.Finance.RefundVoucherStatus.Cancelled };
            result = RefundModel.Build(payMethodItem, termUtility);
            Assert.AreEqual(string.Empty, result.PaymentReferenceNumber);
        }
    }
}
