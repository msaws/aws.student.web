﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Colleague.Dtos.Finance;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class DepositModelTests
    {
        ActivityRemainingAmountItem detail;
        TermPeriodsUtility termUtility;
        DepositModel result;

        [TestInitialize]
        public void Initialize()
        {
            detail = BuildActivityRemainingAmountItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;
            result = DepositModel.Build(detail, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DepositModel_Build_NullDetail()
        {
            result = DepositModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DepositModel_Build_NullTermUtility()
        {
            result = DepositModel.Build(detail, null);
        }

        [TestMethod]
        public void DepositModel_Build_Id()
        {
            Assert.AreEqual(detail.Id, result.Id);
        }

        [TestMethod]
        public void DepositModel_Build_TypeDescription()
        {
            Assert.AreEqual(detail.Description, result.TypeDescription);
        }

        [TestMethod]
        public void DepositModel_Build_Date()
        {
            Assert.AreEqual(detail.Date, result.Date);
        }

        [TestMethod]
        public void DepositModel_Build_TermId()
        {
            Assert.AreEqual(detail.TermId, result.TermId);
        }

        [TestMethod]
        public void DepositModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(detail.TermId), result.TermDescription);
        }

        [TestMethod]
        public void DepositModel_Build_DepositAmount()
        {
            Assert.AreEqual(detail.Amount, result.DepositAmount);
        }

        [TestMethod]
        public void DepositModel_Build_AppliedAmount()
        {
            Assert.AreEqual(detail.PaidAmount, result.AppliedAmount);
        }

        [TestMethod]
        public void DepositModel_Build_OtherAmount()
        {
            Assert.AreEqual(detail.OtherAmount, result.OtherAmount);
        }

        [TestMethod]
        public void DepositModel_Build_RefundAmount()
        {
            Assert.AreEqual(detail.RefundAmount, result.RefundAmount);
        }

        [TestMethod]
        public void DepositModel_Build_NetAmount()
        {
            Assert.AreEqual(detail.RemainingAmount, result.NetAmount);
        }

        [TestMethod]
        public void DepositModel_Build_Amount()
        {
            Assert.AreEqual(detail.PaidAmount + detail.RemainingAmount, result.Amount);
        }

        [TestMethod]
        public void DepositModel_Build_ReceiptId()
        {
            Assert.AreEqual(detail.ReceiptId, result.ReceiptId);
        }
    }
}
