﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class PlanScheduleModelTests
    {
        string planId;
        ActivityPaymentPlanScheduleItem planItem;
        PlanScheduleModel result;

        [TestInitialize]
        public void Initialize()
        {
            planId = "101";
            planItem = BuildActivityPaymentPlanScheduleItem.Build(DateTime.Today);
            result = PlanScheduleModel.Build(planId, planItem);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PlanScheduleModel_Build_NullPlanId()
        {
            result = PlanScheduleModel.Build(null, planItem);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PlanScheduleModel_Build_EmptyPlanId()
        {
            result = PlanScheduleModel.Build(string.Empty, planItem);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PlanScheduleModel_Build_NullPlanItem()
        {
            result = PlanScheduleModel.Build(planId, null);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_PlanId()
        {
            Assert.AreEqual(planId, result.PlanId);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_DueAmount()
        {
            Assert.AreEqual(planItem.Amount, result.DueAmount);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_DueDate()
        {
            Assert.AreEqual(planItem.Date, result.DueDate);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_SetupAmount()
        {
            Assert.AreEqual(planItem.SetupCharge, result.SetupAmount);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_LateAmount()
        {
            Assert.AreEqual(planItem.LateCharge, result.LateAmount);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_PaidAmount()
        {
            Assert.AreEqual(planItem.AmountPaid, result.PaidAmount);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_LastPaidDate()
        {
            Assert.AreEqual(planItem.DatePaid, result.LastPaidDate);
        }

        [TestMethod]
        public void PlanScheduleModel_Build_Amount()
        {
            Assert.AreEqual((planItem.Amount + planItem.SetupCharge + planItem.LateCharge - planItem.AmountPaid), result.Amount);
        }
    }
}
