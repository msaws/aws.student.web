﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance;
using Ellucian.Web.Student.Areas.Finance.Models.AccountActivity;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountActivity
{
    [TestClass]
    public class ReceiptPaymentModelTests
    {
        ActivityPaymentPaidItem paidItem;
        TermPeriodsUtility termUtility;
        ReceiptPaymentModel result;

        [TestInitialize]
        public void Initialize()
        {
            paidItem = BuildActivityPaymentPaidItem.Build();
            termUtility = BuildTermPeriodsUtility.TermPeriodsUtility;

            result = ReceiptPaymentModel.Build(paidItem, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ReceiptPaymentModel_Build_NullDetail()
        {
            result = ReceiptPaymentModel.Build(null, termUtility);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ReceiptPaymentModel_Build_NullTermUtility()
        {
            result = ReceiptPaymentModel.Build(paidItem, null);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_Id()
        {
            Assert.AreEqual(paidItem.Id, result.Id);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_Amount()
        {
            Assert.AreEqual(paidItem.Amount, result.Amount);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_ReceiptNumber()
        {
            Assert.AreEqual(paidItem.ReceiptNumber, result.ReceiptNumber);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_Date()
        {
            Assert.AreEqual(paidItem.Date, result.Date);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_TermId()
        {
            Assert.AreEqual(paidItem.TermId, result.TermId);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_TermDescription()
        {
            Assert.AreEqual(termUtility.GetTermDescription(paidItem.TermId), result.TermDescription);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_PaymentMethodCode()
        {
            Assert.AreEqual(paidItem.Method, result.PaymentMethodCode);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_PaymentMethodCode_ReturnedCheck()
        {
            var returnedItem = BuildActivityPaymentPaidItem.Build();
            returnedItem.Method = "Returned Check";
            result = ReceiptPaymentModel.Build(returnedItem, termUtility);

            Assert.AreEqual(GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "CheckReturnedText"), result.PaymentMethodCode);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_PaymentMethodCode_Cash()
        {
            var returnedItem = BuildActivityPaymentPaidItem.Build();
            returnedItem.Method = "Cash";
            result = ReceiptPaymentModel.Build(returnedItem, termUtility);

            Assert.AreEqual(returnedItem.Method.Replace("Cash", GlobalResources.GetString(FinanceResourceFiles.AccountActivityResources, "CashPayMethodText")), result.PaymentMethodCode);
        }

        [TestMethod]
        public void ReceiptPaymentModel_Build_PaymentReferenceNumber()
        {
            Assert.AreEqual(paidItem.ReferenceNumber, result.PaymentReferenceNumber);
        }
    }
}
