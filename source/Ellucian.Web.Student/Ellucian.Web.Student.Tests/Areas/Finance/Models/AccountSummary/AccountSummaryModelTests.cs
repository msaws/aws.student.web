﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Finance.Models.AccountSummary;
using Ellucian.Web.Student.Tests.Areas.Finance.DataSetup;
using Ellucian.Web.Student.Tests.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.AccountSummary
{
    [TestClass]
    public class AccountSummaryModelTests
    {
        AccountSummaryModel model;
        AccountHolder ah;
        AccountDuePeriod adp;
        AccountDue ad;
        AccountActivityPeriods aap;
        FinanceConfiguration termConfig;
        FinanceConfiguration pcfConfig;
        IEnumerable<Term> terms;
        ICurrentUser currentUser;

        [TestInitialize]
        public void Initialize()
        {
            ah = new AccountHolder()
            {
                Id = "HOLDER1",
                PreferredName = "My Name",
                DepositsDue = BuildDetailedAccountPeriod.BuildDepositsDue()
            };

            adp = new AccountDuePeriod()
            {
                Past = AccountDueDtoSetup.PastAccountDue,
                Current = AccountDueDtoSetup.CurrentAccountDue,
                Future = AccountDueDtoSetup.FutureAccountDue,
            };

            ad = AccountDueDtoSetup.accountDue;

            aap = new AccountActivityPeriods()
            {
                NonTermActivity = new AccountPeriod()
                {
                    AssociatedPeriods = new List<string>(),
                    Balance = 10000m,
                    Description = "Other",
                    EndDate = null,
                    Id = "NON-TERM",
                    StartDate = null
                },
                Periods = new List<AccountPeriod>() 
                {
                    new AccountPeriod()
                    {
                        AssociatedPeriods = new List<string>() { "2014/FA" },
                        Balance = 10000m,
                        Description = "2014 Fall Term",
                        EndDate = DateTime.Today.AddDays(30),
                        Id = "2014/FA",
                        StartDate = DateTime.Today.AddDays(-30)
                    }
                }
            };

            termConfig = new FinanceConfiguration()
            {
                Links = new List<StudentFinanceLink>()
                {
                    new StudentFinanceLink()
                    {
                        Title = "Ellucian University",
                        Url = "http://www.ellucian.edu"
                    }
                },
                PaymentMethods = new List<AvailablePaymentMethod>(){
                    new AvailablePaymentMethod(){
                        Description = "Payment Method 1",
                        InternalCode = "PM1",
                    },
                    new AvailablePaymentMethod(){
                        Description = "Payment Method 2",
                        InternalCode = "PM2",
                    }
                },
                NotificationText = "My notification",
                ActivityDisplay = ActivityDisplay.DisplayByTerm,
                PaymentDisplay = PaymentDisplay.DisplayByTerm,
                PartialAccountPaymentsAllowed = true,
                PartialDepositPaymentsAllowed = true,
                PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed,
                ECommercePaymentsAllowed = true,
                SelfServicePaymentsAllowed = true,
                Periods = new List<FinancialPeriod>() 
                {
                    new FinancialPeriod() { End = DateTime.Today.AddDays(-31), Type = PeriodType.Past },
                    new FinancialPeriod() { End = DateTime.Today.AddDays(30), Start = DateTime.Today.AddDays(-30), Type = PeriodType.Current },
                    new FinancialPeriod() { End = DateTime.Today.AddDays(31), Type = PeriodType.Future },
                },
                DisplayedReceivableTypes = new List<PayableReceivableType>()
            };

            pcfConfig = termConfig;
            pcfConfig.ActivityDisplay = ActivityDisplay.DisplayByPeriod;
            pcfConfig.PaymentDisplay = PaymentDisplay.DisplayByPeriod;

            terms = BuildTermPeriodsUtility.BuildTerms;
            HttpContext.Current = BuildHttpContext.FakeHttpContext();

            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

            // Proxy-For formatted name
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
            // Proxy-For person id
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
            // Proxy-For permissions, one line for each
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAS"));

            currentUser = new CurrentUser(claim);
        }

        [TestCleanup]
        public void Cleanup()
        {
            model = null;
            ah = null;
            adp = null;
            ad = null;
            aap = null;
            termConfig = null;
            terms = null;
            currentUser = null;
        }

        [TestClass]
        public class AccountSummaryModel_BuildForPaymentsDueByPeriod : AccountSummaryModelTests
        {
            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_NullAccountHolder()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(null, adp, aap, pcfConfig, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_NullAccountDue()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, null, aap, pcfConfig, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_NullAccountActivityPeriods()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, null, pcfConfig, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_NullFinanceConfiguration()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, null, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_NullTerms()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, null, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_NullICurrentUser()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, null);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_AccountBalance()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(aap.NonTermActivity.Balance + aap.Periods.Sum(p => p.Balance), model.AccountBalance);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_AlertMessage()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(pcfConfig.NotificationText, model.AlertMessage);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_AmountDue()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(79743m, model.AmountDue);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_HelpfulLinks()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                CollectionAssert.AreEqual(pcfConfig.Links, model.HelpfulLinks);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_IsAdminUser_False()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsFalse(model.IsAdminUser);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_IsAdminUser_True()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsTrue(model.IsAdminUser);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_ShowAccountActivityLink_False1()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsFalse(model.ShowAccountActivityLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_ShowAccountActivityLink_True1()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                // Proxy-For formatted name
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
                // Proxy-For person id
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
                // Proxy-For permissions, one line for each
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAS"));
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAA"));

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsTrue(model.ShowAccountActivityLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_ShowAccountActivityLink_True2()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsTrue(model.ShowAccountActivityLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_ShowMakeAPaymentLink_False1()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsFalse(model.ShowMakeAPaymentLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_ShowMakeAPaymentLink_True1()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                // Proxy-For formatted name
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
                // Proxy-For person id
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
                // Proxy-For permissions, one line for each
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAS"));
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFMAP"));

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsTrue(model.ShowMakeAPaymentLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_ShowMakeAPaymentLink_True2()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsTrue(model.ShowMakeAPaymentLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_IsTermDisplay()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsFalse(model.IsTermDisplay);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_NextPaymentDueDate()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(DateTime.Today.ToShortDateString(), model.NextPaymentDueDate);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_OverdueAmount()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(3291m, model.OverdueAmount);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_PersonId()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(ah.Id, model.PersonId);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_PersonName()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(ah.PreferredName, model.PersonName);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_TimeframeBalances()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(3, model.TimeframeBalances.Count);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_TimeframeBalances_DepositsDue()
            {
                ah.DepositsDue = new List<DepositDue>()
                {
                    new DepositDue()
                    {
                        Amount = 100m,
                        AmountDue = 100m,
                        AmountPaid = 0m,
                        Balance = 100m,
                        Deposits = null,
                        DepositType = "MEALS",
                        DepositTypeDescription = "Meal Plan",
                        Distribution = "BANK",
                        DueDate = DateTime.Today.AddDays(3),
                        Id = "123",
                        Overdue = false,
                        PersonId = ah.Id,
                        TermDescription = "2001 Fall Term",
                        TermId = "2001/FA"
                    }
                };
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(3, model.TimeframeBalances.Count);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_TotalAmountDue()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(83034m, model.TotalAmountDue);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_HasPrivacyRestriction_False()
            {
                ah.PrivacyStatusCode = null;
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_HasPrivacyRestriction_False2()
            {
                ah.PrivacyStatusCode = "X";
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByPeriod_PrivacyStatusCode_Set()
            {
                ah.PrivacyStatusCode = "X";
                model = AccountSummaryModel.BuildForPaymentsDueByPeriod(ah, adp, aap, pcfConfig, terms, currentUser);
                Assert.AreEqual(ah.PrivacyStatusCode, model.PrivacyStatusCode);
            }
        }

        [TestClass]
        public class AccountSummaryModel_BuildForPaymentsDueByTerm : AccountSummaryModelTests
        {
            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_NullAccountHolder()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(null, ad, aap, termConfig, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_NullAccountDue()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, null, aap, termConfig, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_NullAccountActivityPeriods()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, null, termConfig, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_NullFinanceConfiguration()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, null, terms, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_NullTerms()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, null, currentUser);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_NullICurrentUser()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, null);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_AccountBalance()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(aap.NonTermActivity.Balance + aap.Periods.Sum(p => p.Balance), model.AccountBalance);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_AlertMessage()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(termConfig.NotificationText, model.AlertMessage);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_AmountDue()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(26581m, model.AmountDue);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_HelpfulLinks()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                CollectionAssert.AreEqual(termConfig.Links, model.HelpfulLinks);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_IsAdminUser_False()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsFalse(model.IsAdminUser);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_IsAdminUser_True()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsTrue(model.IsAdminUser);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_ShowAccountActivityLink_False1()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsFalse(model.ShowAccountActivityLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_ShowAccountActivityLink_True1()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                // Proxy-For formatted name
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
                // Proxy-For person id
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
                // Proxy-For permissions, one line for each
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAS"));
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAA"));

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsTrue(model.ShowAccountActivityLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_ShowAccountActivityLink_True2()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsTrue(model.ShowAccountActivityLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_ShowMakeAPaymentLink_False1()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsFalse(model.ShowMakeAPaymentLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_ShowMakeAPaymentLink_True1()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                // Proxy-For formatted name
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
                // Proxy-For person id
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
                // Proxy-For permissions, one line for each
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFAS"));
                claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFMAP"));

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsTrue(model.ShowMakeAPaymentLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_ShowMakeAPaymentLink_True2()
            {
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsTrue(model.ShowMakeAPaymentLink);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_IsTermDisplay()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsFalse(model.IsTermDisplay);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_NextPaymentDueDate()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(DateTime.Today.ToShortDateString(), model.NextPaymentDueDate);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_OverdueAmount()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(1097m, model.OverdueAmount);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_PersonId()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(ah.Id, model.PersonId);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_PersonName()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(ah.PreferredName, model.PersonName);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_TimeframeBalances()
            {
                ah.DepositsDue = null;
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(2, model.TimeframeBalances.Count);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_TimeframeBalances_DepositsDue()
            {
                ah.DepositsDue = new List<DepositDue>()
                {
                    new DepositDue()
                    {
                        Amount = 100m,
                        AmountDue = 100m,
                        AmountPaid = 0m,
                        Balance = 100m,
                        Deposits = null,
                        DepositType = "MEALS",
                        DepositTypeDescription = "Meal Plan",
                        Distribution = "BANK",
                        DueDate = DateTime.Today.AddDays(3),
                        Id = "123",
                        Overdue = false,
                        PersonId = ah.Id,
                        TermDescription = "2001 Fall Term",
                        TermId = "2001/FA"
                    }
                };
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(3, model.TimeframeBalances.Count);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_TimeframeBalances_DepositsDue_NonTerm()
            {
                ah.DepositsDue = new List<DepositDue>()
                {
                    new DepositDue()
                    {
                        Amount = 100m,
                        AmountDue = 100m,
                        AmountPaid = 0m,
                        Balance = 100m,
                        Deposits = null,
                        DepositType = "MEALS",
                        DepositTypeDescription = "Meal Plan",
                        Distribution = "BANK",
                        DueDate = DateTime.Today.AddDays(3),
                        Id = "123",
                        Overdue = false,
                        PersonId = ah.Id,
                        TermDescription = string.Empty,
                        TermId = string.Empty
                    }
                };
                termConfig.ActivityDisplay = ActivityDisplay.DisplayByTerm;
                aap.NonTermActivity = null;
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(2, model.TimeframeBalances.Count);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_TimeframeBalances_DepositsDue_Term()
            {
                ah.DepositsDue = new List<DepositDue>()
                {
                    new DepositDue()
                    {
                        Amount = 100m,
                        AmountDue = 100m,
                        AmountPaid = 0m,
                        Balance = 100m,
                        Deposits = null,
                        DepositType = "MEALS",
                        DepositTypeDescription = "Meal Plan",
                        Distribution = "BANK",
                        DueDate = DateTime.Today.AddDays(3),
                        Id = "123",
                        Overdue = false,
                        PersonId = ah.Id,
                        TermDescription = "2014 Fall Term",
                        TermId = "2014/FA"
                    }
                };
                termConfig.ActivityDisplay = ActivityDisplay.DisplayByTerm;
                aap.Periods = new List<AccountPeriod>();
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(2, model.TimeframeBalances.Count);
            }


            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_TotalAmountDue()
            {
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(27678m, model.TotalAmountDue);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_HasPrivacyRestriction_False()
            {
                ah.PrivacyStatusCode = null;
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_HasPrivacyRestriction_False2()
            {
                ah.PrivacyStatusCode = "X";
                var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
                var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
                currentUser = new CurrentUser(claim);

                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.IsFalse(model.HasPrivacyRestriction);
            }

            [TestMethod]
            public void AccountSummaryModel_BuildForPaymentsDueByTerm_PrivacyStatusCode_Set()
            {
                ah.PrivacyStatusCode = "X";
                model = AccountSummaryModel.BuildForPaymentsDueByTerm(ah, ad, aap, termConfig, terms, currentUser);
                Assert.AreEqual(ah.PrivacyStatusCode, model.PrivacyStatusCode);
            }
        }
    }
}
