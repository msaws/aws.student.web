﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildDepositDue
    {
        private const decimal amt = 500m;
        private const decimal amtDue = 200m;
        private const decimal amtPaid = 300m;
        private const string depType = "MEALS";
        private const string depDesc = "Meal Plan Deposit";
        private const string dist = "BANK";
        private const string depDueId = "101";
        private const string personId = "0001234";
        private const string depDueTermId = "2014/FA";
        private const string depDueTermDesc = "2014 Fall Term";
        private const string order = "1";


        public static DepositDue Build()
        {
            var model = new DepositDue()
            {
                Amount = amt,
                AmountDue = amtDue,
                AmountPaid = amtPaid,
                Balance = amt - amtPaid,
                Deposits = new List<Deposit>()
                {
                    new Deposit()
                    {
                        Amount = amtPaid,
                        Date = DateTime.Today.AddDays(-3),
                        DepositType = depType,
                        Id = "102",
                        PersonId = personId,
                        ReceiptId = "103",
                        TermId = depDueTermId
                    }
                },
                DepositType = depType,
                DepositTypeDescription = depDesc,
                Distribution = dist,
                DueDate = DateTime.Today.AddDays(3),
                Id = depDueId,
                Overdue = false,
                PersonId = personId,
                SortOrder = order,
                TermDescription = depDueTermDesc,
                TermId = depDueTermId
            };

            return model;
        }
    }
}
