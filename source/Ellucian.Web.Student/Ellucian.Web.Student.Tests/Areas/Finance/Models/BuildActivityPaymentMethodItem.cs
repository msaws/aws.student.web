﻿using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityPaymentMethodItem
    {
        private static DateTime date = new DateTime(2014, 9, 15, 0, 0, 0);
        private static string method = "ECHK";

        public static ActivityPaymentMethodItem Build()
        {
            var ati = BuildActivityDateTermItem.Build();

            var model = new ActivityPaymentMethodItem()
            {
                Id = ati.Id,
                Description = ati.Description,
                Date = date,
                TermId = ati.TermId,
                Amount = ati.Amount,
                Method = method,
                CheckDate = DateTime.Today.AddDays(-1),
                CheckNumber = "E0000556",
                CreditCardLastFourDigits = "4326",
                TransactionNumber = "A23V32790B",
                Status = Colleague.Dtos.Finance.RefundVoucherStatus.Reconciled,
                StatusDate = DateTime.Today
            };

            return model;
        }
    }
}
