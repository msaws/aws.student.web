﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Payments
{
    [TestClass]
    public class TermSummaryModelTest
    {
        const string termId = "TermId";
        const string termDescription = "Term Description";
        const string overDue = "OverDue";
        const string notOverDue = "NotOverDue";
        const string invoiceItem = "InvoiceItem";
        const string paymentPlanId = "PaymentPlanId";
        const string selectedPlanItem = "SelectedPlanItem";
        const string unselectedPlanItem = "UnselectedPlanItem";
        const string depositDue = "DepositDue";
        FinanceConfiguration config = new FinanceConfiguration() { DisplayedReceivableTypes = new List<PayableReceivableType>() };

        static List<AccountsReceivableDueItem> generalItems = new List<AccountsReceivableDueItem>()
        {
            new AccountsReceivableDueItem(){Description = overDue, DueDate = DateTime.Today.AddDays(-1), AmountDue= 3, AccountType = "01"},
            new AccountsReceivableDueItem(){Description = notOverDue, DueDate = DateTime.Today.AddDays(1), AmountDue=3, AccountType = "01"},
        };

        static List<InvoiceDueItem> invoiceItems = new List<InvoiceDueItem>(){
            new InvoiceDueItem(){Description = invoiceItem, InvoiceId = "Invoice1", AmountDue = 30, AccountType = "01"},
            new InvoiceDueItem(){Description = invoiceItem, InvoiceId = "Invoice2", AmountDue = 30, AccountType = "01"},
        };

        static List<PaymentPlanDueItem> paymentPlanItems = new List<PaymentPlanDueItem>(){
            new PaymentPlanDueItem(){PaymentPlanId = paymentPlanId, Description = selectedPlanItem, DueDate = DateTime.Today, AmountDue = 300, AccountType = "01"},
            new PaymentPlanDueItem(){PaymentPlanId = paymentPlanId, Description = unselectedPlanItem, DueDate = DateTime.Today.AddDays(1), AmountDue = 300, AccountType = "01"},
        };

        static List<DepositDue> depositDueItems = new List<DepositDue>(){
            new DepositDue(){DepositTypeDescription = depositDue, Id = "Due1", AmountDue = 3000},
            new DepositDue(){DepositTypeDescription = depositDue, Id = "Due2", AmountDue = 3000},
        };
        AccountTerm term = new AccountTerm()
        {
            TermId = termId,
            Description = termDescription,
            GeneralItems = generalItems,
            InvoiceItems = invoiceItems,
            PaymentPlanItems = paymentPlanItems,
            DepositDueItems = depositDueItems,
        };

        TermSummaryModel model;

        [TestInitialize]
        public void Initialize()
        {
            model = TermSummaryModel.Build(term, config);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TermSummaryModel_Build_NullArgument()
        {
            TermSummaryModel.Build(null as AccountTerm, config);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TermSummaryModel_Build_NullConfigArgument()
        {
            TermSummaryModel.Build(term, null);
        }

        [TestMethod]
        public void TermSummaryModel_Term_Valid()
        {
            Assert.AreEqual(termId, model.TermCode);
        }

        [TestMethod]
        public void TermSummaryModel_TermDescription_Valid()
        {
            Assert.AreEqual(termDescription, model.TermDescription);
        }

        [TestMethod]
        public void TermSummaryModel_PaymentsDue_Count()
        {
            Assert.AreEqual(generalItems.Count + invoiceItems.Count + paymentPlanItems.Count + depositDueItems.Count,
                model.PaymentsDue.Count);
        }

        [TestMethod]
        public void TermSummaryModel_DueAmount_Valid()
        {
            decimal dueAmount = 0;
            foreach (PaymentsDueModel x in model.PaymentsDue)
            {
                dueAmount += x.DueAmount;
            }

            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void TermSummaryModel_PaidAmount_Valid()
        {
            foreach (PaymentsDueModel x in model.PaymentsDue)
            {
                x.PaidAmount = 2;
            }

            Assert.AreEqual(2 * model.PaymentsDue.Count, model.PaidAmount);
        }

        [TestMethod]
        public void TermSummaryModel_PaymentsDue_FirstPaymentPlanItemIsSelected()
        {
            var items = model.PaymentsDue.Where(x => x.PaymentPlanId == paymentPlanId && x.Description == selectedPlanItem && x.IsSelected == true);
            Assert.IsNotNull(items);
            Assert.AreEqual(1, items.Count());
        }

        [TestMethod]
        public void TermSummaryModel_PaymentsDue_SecondPaymentPlanItemIsUnselected()
        {
            var items = model.PaymentsDue.Where(x => x.PaymentPlanId == paymentPlanId && x.Description == unselectedPlanItem && x.IsSelected == false);
            Assert.IsNotNull(items);
            Assert.AreEqual(1, items.Count());
        }

        [TestMethod]
        public void TermSummaryModel_PaymentsDue_IsSelectedValid()
        {
            var source = new AccountTerm()
            {
                PaymentPlanItems = new List<PaymentPlanDueItem>()
                {
                    // selected because overdue
                    new PaymentPlanDueItem(){Description = selectedPlanItem, DueDate = DateTime.Today.AddDays(-5), PaymentPlanId = paymentPlanId, AccountType = "01"},
                    // selected because overdue
                    new PaymentPlanDueItem(){Description = selectedPlanItem, DueDate = DateTime.Today.AddDays(-3), PaymentPlanId = paymentPlanId, AccountType = "01"},
                    // selected because next due
                    new PaymentPlanDueItem(){Description = selectedPlanItem, DueDate = DateTime.Today.AddDays(1), PaymentPlanId = paymentPlanId, AccountType = "01"},
                    // unselected
                    new PaymentPlanDueItem(){Description = unselectedPlanItem, DueDate = DateTime.Today.AddDays(3), PaymentPlanId = paymentPlanId, AccountType = "01"},
                }
            };
            var model = TermSummaryModel.Build(source, config);
            // get the number of selected and unselected and count the results.
            var selected = model.PaymentsDue.Where(x => x.IsSelected);
            Assert.IsNotNull(selected);
            Assert.AreEqual(3, selected.Count());
        }

        [TestMethod]
        public void TermSummaryModel_PaymentsDue_IsUnselectedValid()
        {
            var source = new AccountTerm()
            {
                PaymentPlanItems = new List<PaymentPlanDueItem>()
                {
                    // selected because overdue
                    new PaymentPlanDueItem(){Description = selectedPlanItem, DueDate = DateTime.Today.AddDays(-5), PaymentPlanId = paymentPlanId, AccountType = "01"},
                    // selected because overdue
                    new PaymentPlanDueItem(){Description = selectedPlanItem, DueDate = DateTime.Today.AddDays(-3), PaymentPlanId = paymentPlanId, AccountType = "01"},
                    // selected because next due
                    new PaymentPlanDueItem(){Description = selectedPlanItem, DueDate = DateTime.Today.AddDays(1), PaymentPlanId = paymentPlanId, AccountType = "01"},
                    // unselected
                    new PaymentPlanDueItem(){Description = unselectedPlanItem, DueDate = DateTime.Today.AddDays(3), PaymentPlanId = paymentPlanId, AccountType = "01"},
                }
            };
            var model = TermSummaryModel.Build(source, config);
            // get the number of selected and unselected and count the results.
            var unselected = model.PaymentsDue.Where(x => !x.IsSelected);
            Assert.IsNotNull(unselected);
            Assert.AreEqual(1, unselected.Count());
        }

        [TestMethod]
        public void TermSummaryModel_PaymentsDue_TermIds()
        {
            foreach (PaymentsDueModel x in model.PaymentsDue)
            {
                Assert.AreEqual(model.TermCode, x.TermId);
            }
        }
    }
}
