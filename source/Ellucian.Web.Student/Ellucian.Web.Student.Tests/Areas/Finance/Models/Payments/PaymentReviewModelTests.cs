﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Payments
{
    [TestClass]
    public class PaymentReviewModelTests
    {
        Payment payment;
        AvailablePaymentMethod selectedPaymentMethod;
        List<ReceivableType> receivableTypes;
        List<Colleague.Dtos.Base.ConvenienceFee> convenienceFees;
        string paymentReviewMessage;

        string message;
        NotificationType type;

        [TestInitialize]
        public void Initialize()
        {
            payment = new Payment()
            {
                AmountToPay = 505m,
                ConvenienceFee = "CF5",
                ConvenienceFeeAmount = 5m,
                ConvenienceFeeGeneralLedgerNumber = "110101000000010100",
                Distribution = "BANK",
                PayMethod = "ECHK",
                PaymentItems = new List<PaymentItem>()
                {
                    new PaymentItem()
                    {
                        AccountType = "01",
                        DepositDueId = string.Empty,
                        Description = "Student Payment",
                        InvoiceId = "1234",
                        Overdue = false,
                        PaymentAmount = 500m,
                        PaymentComplete = true,
                        PaymentControlId = string.Empty,
                        PaymentPlanId = string.Empty,
                        Term = "2014/FA"
                    }
                },
                PersonId = "0001234",
                ProviderAccount = "PPCC",
                ReturnUrl = "http://www.ellucianuniversity.edu/Finance/Payments/Acknowledgement",
                ReturnToOriginUrl = "http://www.ellucianuniversity.edu/Finance/Payments/Acknowledgement2",
            };

            selectedPaymentMethod = new AvailablePaymentMethod()
            {
                Description = "Electronic Check",
                InternalCode = "ECHK",
                Type = "CK"
            };

            receivableTypes = new List<ReceivableType>()
            {
                new ReceivableType()
                {
                    Code = "01",
                    Description = "Student Receivables"
                },
                new ReceivableType()
                {
                    Code = "02",
                    Description = "Continuing Ed Receivables"
                },
            };

            convenienceFees = new List<Colleague.Dtos.Base.ConvenienceFee>()
            {
                new Colleague.Dtos.Base.ConvenienceFee()
                {
                    Code = "CF5",
                    Description = "$5.00 Flat Fee"
                }
            };
            
            paymentReviewMessage = "You selected the following items for payment.";

            message = "Unable to load Electronic Check Entry information.";
            type = NotificationType.Information;
        }

        [TestClass]
        public class PaymentReviewModel_Build : PaymentReviewModelTests
        {
            [TestInitialize]
            public void PaymentReviewModel_Build_Initialize()
            {
                Initialize();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_NullPayment()
            {
                var model = PaymentReviewModel.Build(null, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_PaymentWithNullPaymentItems()
            {
                var newPayment = payment;
                newPayment.PaymentItems = null;
                var model = PaymentReviewModel.Build(newPayment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_PaymentWithNoPaymentItems()
            {
                var newPayment = payment;
                newPayment.PaymentItems = new List<PaymentItem>();
                var model = PaymentReviewModel.Build(newPayment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_NullSelectedPaymentMethod()
            {
                var model = PaymentReviewModel.Build(payment, null, receivableTypes, convenienceFees, paymentReviewMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_NullReceivableTypes()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, null, convenienceFees, paymentReviewMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_EmptyReceivableTypes()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, new List<ReceivableType>(), convenienceFees, paymentReviewMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_PaymentWithConvenienceFee_NullConvenienceFees()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, receivableTypes, null, paymentReviewMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentReviewModel_Build_PaymentWithConvenienceFee_EmptyConvenienceFees()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, receivableTypes, new List<Colleague.Dtos.Base.ConvenienceFee>(), paymentReviewMessage);
            }

            [TestMethod]
            public void PaymentReviewModel_Build_Message()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
                Assert.AreEqual(paymentReviewMessage, model.Message);
            }

            [TestMethod]
            public void PaymentReviewModel_Build_Payment()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
                Assert.AreEqual(payment.AmountToPay, model.Payment.AmountToPay);
                Assert.AreEqual(payment.CheckDetails, model.Payment.CheckDetails);
                Assert.AreEqual(payment.ConvenienceFee, model.Payment.ConvenienceFee);
                Assert.AreEqual(payment.ConvenienceFeeAmount, model.Payment.ConvenienceFeeAmount);
                Assert.AreEqual(payment.ConvenienceFeeGeneralLedgerNumber, model.Payment.ConvenienceFeeGeneralLedgerNumber);
                Assert.AreEqual(payment.Distribution, model.Payment.Distribution);
                Assert.AreEqual(payment.PaymentItems.Count, model.Payment.PaymentItems.Count);
                for (int i = 0; i < model.Payment.PaymentItems.Count; i++)
                {
                    Assert.AreEqual(payment.PaymentItems[i].AccountType, model.Payment.PaymentItems[i].AccountType);
                    Assert.AreEqual(payment.PaymentItems[i].DepositDueId, model.Payment.PaymentItems[i].DepositDueId);
                    Assert.AreEqual(payment.PaymentItems[i].Description, model.Payment.PaymentItems[i].Description);
                    Assert.AreEqual(payment.PaymentItems[i].InvoiceId, model.Payment.PaymentItems[i].InvoiceId);
                    Assert.AreEqual(payment.PaymentItems[i].Overdue, model.Payment.PaymentItems[i].Overdue);
                    Assert.AreEqual(payment.PaymentItems[i].PaymentAmount, model.Payment.PaymentItems[i].PaymentAmount);
                    Assert.AreEqual(payment.PaymentItems[i].PaymentComplete, model.Payment.PaymentItems[i].PaymentComplete);
                    Assert.AreEqual(payment.PaymentItems[i].PaymentControlId, model.Payment.PaymentItems[i].PaymentControlId);
                    Assert.AreEqual(payment.PaymentItems[i].PaymentPlanId, model.Payment.PaymentItems[i].PaymentPlanId);
                    Assert.AreEqual(payment.PaymentItems[i].Term, model.Payment.PaymentItems[i].Term);
                }
                Assert.AreEqual(payment.PayMethod, model.Payment.PayMethod);
                Assert.AreEqual(payment.PersonId, model.Payment.PersonId);
                Assert.AreEqual(payment.ProviderAccount, model.Payment.ProviderAccount);
                Assert.AreEqual(payment.ReturnToOriginUrl, model.Payment.ReturnToOriginUrl);
                Assert.AreEqual(payment.ReturnUrl, model.Payment.ReturnUrl);
            }

            [TestMethod]
            public void PaymentReviewModel_Build_SelectedPaymentMethod()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
                Assert.AreEqual(selectedPaymentMethod.Description, model.PaymentMethod.Description);
                Assert.AreEqual(selectedPaymentMethod.InternalCode, model.PaymentMethod.InternalCode);
                Assert.AreEqual(selectedPaymentMethod.Type, model.PaymentMethod.Type);
            }

            [TestMethod]
            public void PaymentReviewModel_Build_LineItems_ConvenienceFee()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
                Assert.AreEqual(3, model.LineItems.Count);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount, model.LineItems[0].Amount);
                Assert.AreEqual(payment.PaymentItems[0].Description, model.LineItems[0].Description);

                Assert.AreEqual(payment.ConvenienceFeeAmount.Value, model.LineItems[1].Amount);
                Assert.AreEqual(convenienceFees[0].Description, model.LineItems[1].Description);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount + payment.ConvenienceFeeAmount.Value, model.LineItems[2].Amount); 
                Assert.AreEqual(null, model.LineItems[2].Description);
            }

            [TestMethod]
            public void PaymentReviewModel_Build_LineItems_NoMatchingConvenienceFee()
            {
                var model = PaymentReviewModel.Build(payment, selectedPaymentMethod, receivableTypes, new List<Colleague.Dtos.Base.ConvenienceFee>() { new Colleague.Dtos.Base.ConvenienceFee() { Code = "CF4", Description = "$4.00 Fee" } }, paymentReviewMessage);
                Assert.AreEqual(2, model.LineItems.Count);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount, model.LineItems[0].Amount);
                Assert.AreEqual(payment.PaymentItems[0].Description, model.LineItems[0].Description);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount, model.LineItems[1].Amount);
                Assert.AreEqual(null, model.LineItems[1].Description);
            }

            [TestMethod]
            public void PaymentReviewModel_Build_LineItems_NullConvenienceFee()
            {
                var newPayment = payment;
                newPayment.ConvenienceFeeAmount = null;
                var model = PaymentReviewModel.Build(newPayment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
                Assert.AreEqual(2, model.LineItems.Count);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount, model.LineItems[0].Amount);
                Assert.AreEqual(payment.PaymentItems[0].Description, model.LineItems[0].Description);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount, model.LineItems[1].Amount);
                Assert.AreEqual(null, model.LineItems[1].Description);
            }

            [TestMethod]
            public void PaymentReviewModel_Build_LineItems_ZeroConvenienceFee()
            {
                var newPayment = payment;
                newPayment.ConvenienceFeeAmount = 0m;
                var model = PaymentReviewModel.Build(newPayment, selectedPaymentMethod, receivableTypes, convenienceFees, paymentReviewMessage);
                Assert.AreEqual(2, model.LineItems.Count);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount, model.LineItems[0].Amount);
                Assert.AreEqual(payment.PaymentItems[0].Description, model.LineItems[0].Description);

                Assert.AreEqual(payment.PaymentItems[0].PaymentAmount, model.LineItems[1].Amount);
                Assert.AreEqual(null, model.LineItems[1].Description);
            }
        }

        [TestClass]
        public class PaymentReviewModel_BuildWithErrors : PaymentReviewModelTests
        {
            [TestInitialize]
            public void PaymentReviewModel_BuildWithErrors_Initialize()
            {
                Initialize();
            }

            [TestMethod]
            public void PaymentReviewModel_BuildWithErrors_NullMessage()
            {
                var model = PaymentReviewModel.BuildWithErrors(null, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void PaymentReviewModel_BuildWithErrors_EmptyMessage()
            {
                var model = PaymentReviewModel.BuildWithErrors(string.Empty, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void PaymentReviewModel_BuildWithErrors_NoType()
            {
                var model = PaymentReviewModel.BuildWithErrors(message);
                Assert.AreEqual(message, model.Notification.Message);
                Assert.AreEqual("Error", model.Notification.Type);
            }
        }

    }
}
