﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Payments
{
    [TestClass]
    public class PaymentAcknowledgementModelTests
    {
        PaymentReceipt receipt;
        string message;
        NotificationType type;

        [TestInitialize]
        public void Initialize()
        {
            receipt = new PaymentReceipt()
            {
                AcknowledgeFooterImageUrl = new Uri("http://www.ellucianuniversity.edu/Payments/footer.png"),
                AcknowledgeFooterText = new List<string>() { "This is some...", "...footer text." },
                ChangeReturned = 5m,
                ConvenienceFees = new List<ConvenienceFee>() 
                {
                    new ConvenienceFee() { Amount = 5m, Code = "CF5", Description = "$5.00 Flat Fee"}
                },
                Deposits = new List<AccountsReceivableDeposit>() 
                {
                    new AccountsReceivableDeposit() 
                    { 
                        Description = "Deposit",
                        Location = "MC",
                        LocationDescription = "Main Campus",
                        NetAmount = 500m,
                        PersonId = "0001234",
                        PersonName = "John Smith",
                        Term = "2014/FA",
                        TermDescription = "2014 Fall Term",
                        Type = "MEALS"
                    }
                },
                ErrorMessage = "This is an error that occurred.",
                OtherItems = new List<GeneralPayment>() 
                {
                    new GeneralPayment()
                    {
                        Code = "BOOKS",
                        Description = "Book Charges",
                        Location = "MC",
                        LocationDescription = "Main Campus",
                        NetAmount = 300m
                    }
                },
                MerchantEmail = "merchant@ellucianuniversity.edu",
                MerchantNameAddress = new List<string>() { "Ellucian University", "123 Main Street", "Fairfax, VA 22033" },
                MerchantPhone = "(703) 968-3000 x1234",
                PaymentMethods = new List<PaymentMethod>() 
                {
                    new PaymentMethod()
                    {
                        ConfirmationNumber = "CONF1234",
                        ControlNumber = "1234",
                        PayMethodCode = "ECHK",
                        PayMethodDescription = "Electronic Check",
                        TransactionAmount = 995m,
                        TransactionDescription = "Student Payment",
                        TransactionNumber = "TRANS1234"

                    },
                    new PaymentMethod()
                    {
                        ConfirmationNumber = "",
                        ControlNumber = "",
                        PayMethodCode = "Cash",
                        PayMethodDescription = "",
                        TransactionAmount = 10m,
                        TransactionDescription = "",
                        TransactionNumber = ""

                    },
                },
                ReceiptAcknowledgeText = new List<string>() { "This is some...", "...acknowledgement text" },
                Payments = new List<AccountsReceivablePayment>() 
                {
                    new AccountsReceivablePayment()
                    {
                        Description = "Receivable Payment",
                        Location = "MC",
                        LocationDescription = "Main Campus",
                        NetAmount = 200m,
                        PaymentDescription = "Payment on Account",
                        PersonId = "0001234",
                        PersonName = "John Smith",
                        Term = "2014/FA",
                        TermDescription = "2014 Fall Term",
                        Type = "01"
                    }
                },
                ReceiptDate = DateTime.Today,
                ReceiptNo = "1234",
                ReceiptPayerId = "0001234",
                ReceiptPayerName = "John Smith",
                ReceiptTime = DateTime.Now,
            };

            message = "Unable to load Electronic Check Entry information.";
            type = NotificationType.Information;
        }

        [TestClass]
        public class PaymentAcknowledgementModel_Build : PaymentAcknowledgementModelTests
        {
            [TestInitialize]
            public void PaymentAcknowledgementModel_Build_Initialize()
            {
                Initialize();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentAcknowledgementModel_Build_NullReceipt()
            {
                var model = PaymentAcknowledgementModel.Build(null);
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_Build_Valid()
            {
                var model = PaymentAcknowledgementModel.Build(receipt);

                Assert.AreEqual(receipt.ReceiptAcknowledgeText.Count, model.AcknowledgementText.Count);
                for (int a = 0; a < model.AcknowledgementText.Count; a++)
                {
                    Assert.AreEqual(receipt.ReceiptAcknowledgeText[a], model.AcknowledgementText[a]);
                }
                Assert.AreEqual(receipt.ChangeReturned, model.ChangeReturned);
                Assert.AreEqual(receipt.Deposits.Count, model.Deposits.Count);
                for (int b = 0; b < model.Deposits.Count; b++)
                {
                    Assert.AreEqual(receipt.Deposits[b].Description, model.Deposits[b].Description);
                    Assert.AreEqual(receipt.Deposits[b].Location, model.Deposits[b].Location);
                    Assert.AreEqual(receipt.Deposits[b].LocationDescription, model.Deposits[b].LocationDescription);
                    Assert.AreEqual(receipt.Deposits[b].NetAmount, model.Deposits[b].NetAmount);
                    Assert.AreEqual(receipt.Deposits[b].PersonId, model.Deposits[b].PersonId);
                    Assert.AreEqual(receipt.Deposits[b].PersonName, model.Deposits[b].PersonName);
                    Assert.AreEqual(receipt.Deposits[b].Term, model.Deposits[b].Term);
                    Assert.AreEqual(receipt.Deposits[b].TermDescription, model.Deposits[b].TermDescription);
                    Assert.AreEqual(receipt.Deposits[b].Type, model.Deposits[b].Type);
                }
                Assert.AreEqual(receipt.ErrorMessage, model.ErrorMessage);
                Assert.AreEqual(receipt.ConvenienceFees.Count, model.Fees.Count);
                for (int c = 0; c < model.Fees.Count; c++)
                {
                    Assert.AreEqual(receipt.ConvenienceFees[c].Amount, model.Fees[c].Amount);
                    Assert.AreEqual(receipt.ConvenienceFees[c].Code, model.Fees[c].Code);
                    Assert.AreEqual(receipt.ConvenienceFees[c].Description, model.Fees[c].Description);
                }
                Assert.AreEqual(receipt.AcknowledgeFooterImageUrl, model.FooterImage);
                Assert.AreEqual(receipt.AcknowledgeFooterText.Count, model.FooterText.Count);
                for (int d = 0; d < model.FooterText.Count; d++)
                {
                    Assert.AreEqual(receipt.AcknowledgeFooterText[d], model.FooterText[d]);
                }
                for (int e = 0; e < receipt.MerchantNameAddress.Count; e++)
                {
                    Assert.AreEqual(receipt.MerchantNameAddress[e], model.Merchant[e]);
                }
                Assert.AreEqual(receipt.MerchantNameAddress.Count + 3, model.Merchant.Count);
                Assert.AreEqual(" ", model.Merchant[receipt.MerchantNameAddress.Count]);
                Assert.AreEqual(receipt.MerchantEmail, model.Merchant[receipt.MerchantNameAddress.Count + 1]);
                Assert.AreEqual(receipt.MerchantPhone, model.Merchant[receipt.MerchantNameAddress.Count + 2]);
                Assert.AreEqual(receipt.OtherItems.Count, model.OtherItems.Count);
                for (int e = 0; e < model.OtherItems.Count; e++)
                {
                    Assert.AreEqual(receipt.OtherItems[e].Code, model.OtherItems[e].Code);
                    Assert.AreEqual(receipt.OtherItems[e].Description, model.OtherItems[e].Description);
                    Assert.AreEqual(receipt.OtherItems[e].Location, model.OtherItems[e].Location);
                    Assert.AreEqual(receipt.OtherItems[e].LocationDescription, model.OtherItems[e].LocationDescription);
                    Assert.AreEqual(receipt.OtherItems[e].NetAmount, model.OtherItems[e].NetAmount);
                }
                Assert.AreEqual(receipt.ReceiptPayerId, model.PayerId);
                Assert.AreEqual(receipt.ReceiptPayerName, model.PayerName);
                Assert.AreEqual(receipt.Payments.Count, model.Payments.Count);
                for (int f = 0; f < model.Payments.Count; f++)
                {
                    Assert.AreEqual(receipt.Payments[f].Description, model.Payments[f].Description);
                    Assert.AreEqual(receipt.Payments[f].Location, model.Payments[f].Location);
                    Assert.AreEqual(receipt.Payments[f].LocationDescription, model.Payments[f].LocationDescription);
                    Assert.AreEqual(receipt.Payments[f].NetAmount, model.Payments[f].NetAmount);
                    Assert.AreEqual(receipt.Payments[f].PaymentControlId, model.Payments[f].PaymentControlId);
                    Assert.AreEqual(receipt.Payments[f].PaymentDescription, model.Payments[f].PaymentDescription);
                    Assert.AreEqual(receipt.Payments[f].PersonId, model.Payments[f].PersonId);
                    Assert.AreEqual(receipt.Payments[f].PersonName, model.Payments[f].PersonName);
                    Assert.AreEqual(receipt.Payments[f].Term, model.Payments[f].Term);
                    Assert.AreEqual(receipt.Payments[f].TermDescription, model.Payments[f].TermDescription);
                    Assert.AreEqual(receipt.Payments[f].Type, model.Payments[f].Type);
                }
                Assert.AreEqual(receipt.PaymentMethods.Count, model.PaymentsTendered.Count);
                for (int f = 0; f < model.PaymentsTendered.Count; f++)
                {
                    Assert.AreEqual(receipt.PaymentMethods[f].ConfirmationNumber, model.PaymentsTendered[f].ConfirmationNumber);
                    Assert.AreEqual(receipt.PaymentMethods[f].ControlNumber, model.PaymentsTendered[f].ControlNumber);
                    Assert.AreEqual(receipt.PaymentMethods[f].PayMethodCode, model.PaymentsTendered[f].PayMethodCode);
                    Assert.AreEqual(receipt.PaymentMethods[f].PayMethodDescription, model.PaymentsTendered[f].PayMethodDescription);
                    Assert.AreEqual(receipt.PaymentMethods[f].TransactionAmount, model.PaymentsTendered[f].TransactionAmount);
                    Assert.AreEqual(receipt.PaymentMethods[f].TransactionDescription, model.PaymentsTendered[f].TransactionDescription);
                    Assert.AreEqual(receipt.PaymentMethods[f].TransactionNumber, model.PaymentsTendered[f].TransactionNumber);
                }
                Assert.AreEqual(receipt.CashReceiptsId, model.ReceiptId);
                Assert.AreEqual(receipt.ReceiptDate.Value.Add(new TimeSpan(receipt.ReceiptTime.Value.Hour, receipt.ReceiptTime.Value.Minute, receipt.ReceiptTime.Value.Second)), model.ReceiptDate);
                Assert.AreEqual(receipt.ReceiptNo, model.ReceiptNumber);
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_Build_NullCollections()
            {
                var rcpt = new PaymentReceipt();
                var model = PaymentAcknowledgementModel.Build(rcpt);

                Assert.AreEqual(0, model.AcknowledgementText.Count);
                Assert.AreEqual(0, model.Deposits.Count);
                Assert.AreEqual(0, model.Fees.Count);
                Assert.AreEqual(rcpt.AcknowledgeFooterImageUrl, model.FooterImage);
                Assert.AreEqual(0, model.FooterText.Count);
                Assert.AreEqual(3, model.Merchant.Count);
                Assert.AreEqual(" ", model.Merchant[0]);
                Assert.AreEqual(rcpt.MerchantEmail, model.Merchant[1]);
                Assert.AreEqual(rcpt.MerchantPhone, model.Merchant[2]);
                Assert.AreEqual(0, model.OtherItems.Count);
                Assert.AreEqual(rcpt.ReceiptPayerId, model.PayerId);
                Assert.AreEqual(rcpt.ReceiptPayerName, model.PayerName);
                Assert.AreEqual(0, model.Payments.Count);
                Assert.AreEqual(0, model.PaymentsTendered.Count);
                Assert.AreEqual(rcpt.ReceiptDate, model.ReceiptDate);
                Assert.AreEqual(rcpt.ReceiptNo, model.ReceiptNumber);
            }
        }

        [TestClass]
        public class PaymentAcknowledgementModel_BuildWithErrors : PaymentAcknowledgementModelTests
        {
            [TestInitialize]
            public void PaymentAcknowledgementModel_BuildWithErrors_Initialize()
            {
                Initialize();
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_BuildWithErrors_NullMessage()
            {
                var model = PaymentAcknowledgementModel.BuildWithErrors(null, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_BuildWithErrors_EmptyMessage()
            {
                var model = PaymentAcknowledgementModel.BuildWithErrors(string.Empty, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_BuildWithErrors_NoType()
            {
                var model = PaymentAcknowledgementModel.BuildWithErrors(message);
                Assert.AreEqual(message, model.Notification.Message);
                Assert.AreEqual("Error", model.Notification.Type);
            }
        }

        [TestClass]
        public class PaymentAcknowledgementModel_BuildForCanceledPayment : PaymentAcknowledgementModelTests
        {
            [TestInitialize]
            public void PaymentAcknowledgementModel_BuildForCanceledPayment_Initialize()
            {
                Initialize();
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_BuildForCanceledPayment_NullMessage()
            {
                var model = PaymentAcknowledgementModel.BuildForCanceledPayment(null, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_BuildForCanceledPayment_EmptyMessage()
            {
                var model = PaymentAcknowledgementModel.BuildForCanceledPayment(string.Empty, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void PaymentAcknowledgementModel_BuildForCanceledPayment_NoType()
            {
                var model = PaymentAcknowledgementModel.BuildForCanceledPayment(message);
                Assert.AreEqual(message, model.Notification.Message);
                Assert.AreEqual("Success", model.Notification.Type);
            }
        }
    }
}
