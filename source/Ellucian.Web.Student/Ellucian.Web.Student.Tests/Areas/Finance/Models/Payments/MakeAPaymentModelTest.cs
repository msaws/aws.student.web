﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Tests.Areas.Finance.DataSetup;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using Microsoft.IdentityModel.Claims;
using Ellucian.Web.Student.Areas.Finance.Models.ImmediatePayments;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Payments
{
    [TestClass]
    public class MakeAPaymentModelTest
    {
        ICurrentUser currentUser;

        [TestInitialize]
        public void Initialize()
        {
            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IjAwMDMzMTUiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);

            // Proxy-For formatted name
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectFormattedName, "Lillie E. Puckett"));
            // Proxy-For person id
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPersonId, "0011521"));
            // Proxy-For permissions, one line for each
            claim.Identities.First().Claims.Add(new Microsoft.IdentityModel.Claims.Claim(Ellucian.Web.Security.ClaimConstants.ProxySubjectPermissions, "SFMAP"));

            currentUser = new CurrentUser(claim);
        }

        AccountDuePeriod accountDuePeriod = new AccountDuePeriod()
        { 
            Past = AccountDueDtoSetup.PastAccountDue,
            Current = AccountDueDtoSetup.CurrentAccountDue,
            Future = AccountDueDtoSetup.FutureAccountDue,
        };

        #region Create a term and a PCF finance configuration
        FinanceConfiguration termConfig = new FinanceConfiguration()
        {
            PaymentMethods = new List<AvailablePaymentMethod>(){
                new AvailablePaymentMethod(){
                    Description = "Payment Method 1",
                    InternalCode = "PM1",
                },
                new AvailablePaymentMethod(){
                    Description = "Payment Method 2",
                    InternalCode = "PM2",
                }
            },
            DisplayedReceivableTypes = new List<PayableReceivableType>(),
            NotificationText = "My notification",
            PaymentDisplay = PaymentDisplay.DisplayByTerm,
            PartialAccountPaymentsAllowed = true,
            PartialDepositPaymentsAllowed = true,
            PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed,
            ECommercePaymentsAllowed = true,
            SelfServicePaymentsAllowed = true
        };

        FinanceConfiguration pcfConfig = new FinanceConfiguration()
        {
            PaymentMethods = new List<AvailablePaymentMethod>(){
                new AvailablePaymentMethod(){
                    Description = "Payment Method 1",
                    InternalCode = "PM1",
                },
                new AvailablePaymentMethod(){
                    Description = "Payment Method 2",
                    InternalCode = "PM2",
                }
            },
            DisplayedReceivableTypes = new List<PayableReceivableType>(),
            NotificationText = "My notification",
            PaymentDisplay = PaymentDisplay.DisplayByPeriod,
            PartialAccountPaymentsAllowed = true,
            PartialDepositPaymentsAllowed = true,
            PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed,
            ECommercePaymentsAllowed = true,
            SelfServicePaymentsAllowed = true
        };
        #endregion

        AccountHolder holder = new AccountHolder()
        {
            Id = "HOLDER1",
            PreferredName = "My Name",
        };
        AccountHolder holder2 = new AccountHolder()
        {
            Id = "HOLDER2",
            PreferredName = "My Name2",
        };

        List<PersonRestriction> restrictions = new List<PersonRestriction>()
        {
            new PersonRestriction()
            {
                Details = "Restriction text.",
                EndDate = DateTime.Today.AddDays(3),
                Hyperlink = "http://www.ellucianuniversity.edu",
                HyperlinkText = "Click here.",
                Id = "123",
                OfficeUseOnly = false,
                RestrictionId = "BH",
                StartDate = DateTime.Today.AddDays(-3),
                StudentId = "HOLDER1",
                Title = "Business Office Hold"
            }
        };


        #region Term
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDue_NullSourceArgument()
        {
            MakeAPaymentModel.Build((AccountDue)null, termConfig, holder,  currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDue_NullConfigArgument()
        {
            MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, null, holder,  currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDue_NullHolderArgument()
        {
            MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, null,  currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDue_NullUserArgument()
        {
            MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void MakeAPaymentModel_AccountDuePeriod_TermPaymentDataAndPeriodDisplayMode()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, pcfConfig, holder,  currentUser);
            Assert.AreEqual(0, model.PaidAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_NoTerms()
        {
            var model = MakeAPaymentModel.Build(new AccountDue(), termConfig, holder,  currentUser);
            Assert.AreEqual(0, model.TermModels.Count);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PaymentMethodsValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreSame(termConfig.PaymentMethods, model.AvailablePaymentMethods);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_NotificationValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(termConfig.NotificationText, model.AlertMessage);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PaymentsAllowedTrue()
        {
            var model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.IsTrue(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PaymentsAllowedFalse1()
        {
            var myConfig = new FinanceConfiguration() { ECommercePaymentsAllowed = true, SelfServicePaymentsAllowed = false, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            var model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, myConfig, holder,  currentUser);
            Assert.IsFalse(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PaymentsAllowedFalse2()
        {
            var myConfig = new FinanceConfiguration() { ECommercePaymentsAllowed = false, SelfServicePaymentsAllowed = false, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            var model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, myConfig, holder,  currentUser);
            Assert.IsFalse(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PaymentsAllowedFalse3()
        {
            var myConfig = new FinanceConfiguration() { ECommercePaymentsAllowed = false, SelfServicePaymentsAllowed = true, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            var model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, myConfig, holder,  currentUser);
            Assert.IsFalse(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_AdminUserFalse()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.IsFalse(model.IsAdminUser);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PersonIdValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(holder.Id, model.PersonId);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PersonNameValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(holder.PreferredName, model.PersonName);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_TermDisplayTrue()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.IsTrue(model.IsTermDisplay);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PartialAccountPaymentsAllowed_True()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(termConfig.PartialAccountPaymentsAllowed, model.PartialAccountPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PartialAccountPaymentsAllowed_False()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialAccountPaymentsAllowed = false, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialAccountPaymentsAllowed, model.PartialAccountPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PartialDepositPaymentsAllowed_True()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(termConfig.PartialDepositPaymentsAllowed, model.PartialDepositPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PartialDepositPaymentsAllowed_False()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialDepositPaymentsAllowed = false, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialDepositPaymentsAllowed, model.PartialDepositPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PartialPlanPaymentsAllowed_Allowed()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(termConfig.PartialPlanPaymentsAllowed, model.PartialPlanPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PartialPlanPaymentsAllowed_Denied()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialPlanPaymentsAllowed = PartialPlanPayments.Denied, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialPlanPaymentsAllowed, model.PartialPlanPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PartialPlanPaymentsAllowed_AllowedWhenNotOverdue()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialPlanPaymentsAllowed = PartialPlanPayments.AllowedWhenNotOverdue, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialPlanPaymentsAllowed, model.PartialPlanPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_TermCountValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(AccountDueDtoSetup.accountDue.AccountTerms.Count, model.TermModels.Count);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_DueAmountValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            decimal dueAmount = CalcAmountDue(AccountDueDtoSetup.accountDue);
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PaidAmountValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            decimal paidAmount = SetAndReturnPaidAmount(model.TermModels);
            Assert.AreEqual(paidAmount, model.PaidAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_HasDataTrueWhenTermDisplay()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.IsTrue(model.HasData);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_HasDataFalseWhenNoTerms()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDueNullTerms, termConfig, holder,  currentUser);
            Assert.IsFalse(model.HasData);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_NoDataMessage()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.emptyAccountDue, termConfig, holder,  currentUser);
            Assert.IsNull(model.NoDataMessage);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_SelectedItemsValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(11, CountSelectedItems(model.TermModels));
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_UnselectedItemsValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder,  currentUser);
            Assert.AreEqual(16, CountUnselectedItems(model.TermModels));
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_HasPrivacyRestriction_False()
        {
            holder.PrivacyStatusCode = null;
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder, currentUser);
            Assert.IsFalse(model.HasPrivacyRestriction);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_HasPrivacyRestriction_False2()
        {
            holder.PrivacyStatusCode = "X";
            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
            currentUser = new CurrentUser(claim);

            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder, currentUser);
            Assert.IsFalse(model.HasPrivacyRestriction);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PrivacyStatusCode_Set()
        {
            holder.PrivacyStatusCode = "X";
            MakeAPaymentModel model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder, currentUser);
            Assert.AreEqual(model.PrivacyStatusCode, holder.PrivacyStatusCode);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_UserPaymentPlanCreationEnabled_True()
        {
            termConfig.UserPaymentPlanCreationEnabled = true;
            var model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder, currentUser);
            Assert.IsTrue(model.UserPaymentPlanCreationEnabled);
            termConfig.UserPaymentPlanCreationEnabled = false;
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_UserPaymentPlanCreationEnabled_False()
        {
            termConfig.UserPaymentPlanCreationEnabled = false;
            var model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder, currentUser);
            Assert.IsFalse(model.UserPaymentPlanCreationEnabled);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDue_PaymentPlanEligibilityText()
        {
            termConfig.PaymentPlanEligibilityText = "Here is some text.";
            var model = MakeAPaymentModel.Build(AccountDueDtoSetup.accountDue, termConfig, holder, currentUser);
            Assert.AreEqual(termConfig.PaymentPlanEligibilityText, model.PaymentPlanEligibilityText);
        }

        #endregion

        #region PCF
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDuePeriod_NullSourceArgument()
        {
            MakeAPaymentModel.Build((AccountDuePeriod)null, pcfConfig, holder,  currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDuePeriod_NullConfigArgument()
        {
            MakeAPaymentModel.Build(accountDuePeriod, null, holder,  currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDuePeriod_NullHolderArgument()
        {
            MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, null,  currentUser);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_AccountDuePeriod_NullUserArgument()
        {
            MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  null);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaymentMethodsValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.AreSame(pcfConfig.PaymentMethods, model.AvailablePaymentMethods);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_NotificationValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.AreEqual(pcfConfig.NotificationText, model.AlertMessage);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaymentsAllowedTrue()
        {
            var model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.IsTrue(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaymentsAllowedFalse1()
        {
            var myConfig = new FinanceConfiguration() { ECommercePaymentsAllowed = true, SelfServicePaymentsAllowed = false, PaymentDisplay = PaymentDisplay.DisplayByPeriod, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            var model = MakeAPaymentModel.Build(accountDuePeriod, myConfig, holder,  currentUser);
            Assert.IsFalse(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaymentsAllowedFalse2()
        {
            var myConfig = new FinanceConfiguration() { ECommercePaymentsAllowed = false, SelfServicePaymentsAllowed = false, PaymentDisplay = PaymentDisplay.DisplayByPeriod, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            var model = MakeAPaymentModel.Build(accountDuePeriod, myConfig, holder,  currentUser);
            Assert.IsFalse(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaymentsAllowedFalse3()
        {
            var myConfig = new FinanceConfiguration() { ECommercePaymentsAllowed = false, SelfServicePaymentsAllowed = true, PaymentDisplay = PaymentDisplay.DisplayByPeriod, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            var model = MakeAPaymentModel.Build(accountDuePeriod, myConfig, holder,  currentUser);
            Assert.IsFalse(model.PaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_AdminUserFalse()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.IsFalse(model.IsAdminUser);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PersonIdValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.AreEqual(holder.Id, model.PersonId);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PersonNameValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.AreEqual(holder.PreferredName, model.PersonName);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PartialAccountPaymentsAllowed_True()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.AreEqual(pcfConfig.PartialAccountPaymentsAllowed, model.PartialAccountPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PartialAccountPaymentsAllowed_False()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialAccountPaymentsAllowed = false, PaymentDisplay = PaymentDisplay.DisplayByPeriod, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialAccountPaymentsAllowed, model.PartialAccountPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PartialDepositPaymentsAllowed_True()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.AreEqual(pcfConfig.PartialDepositPaymentsAllowed, model.PartialDepositPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PartialDepositPaymentsAllowed_False()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialDepositPaymentsAllowed = false, PaymentDisplay = PaymentDisplay.DisplayByPeriod, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialDepositPaymentsAllowed, model.PartialDepositPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PartialPlanPaymentsAllowed_Allowed()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            Assert.AreEqual(pcfConfig.PartialPlanPaymentsAllowed, model.PartialPlanPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PartialPlanPaymentsAllowed_Denied()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialPlanPaymentsAllowed = PartialPlanPayments.Denied, PaymentDisplay = PaymentDisplay.DisplayByPeriod, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialPlanPaymentsAllowed, model.PartialPlanPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PartialPlanPaymentsAllowed_AllowedWhenNotOverdue()
        {
            FinanceConfiguration myConfig = new FinanceConfiguration() { PartialPlanPaymentsAllowed = PartialPlanPayments.AllowedWhenNotOverdue, PaymentDisplay = PaymentDisplay.DisplayByPeriod, DisplayedReceivableTypes = new List<PayableReceivableType>() };
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, myConfig, holder,  currentUser);
            Assert.AreEqual(myConfig.PartialPlanPaymentsAllowed, model.PartialPlanPaymentsAllowed);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_TermCountValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            var sourceTermCounts = accountDuePeriod.Past.AccountTerms.Count
                + accountDuePeriod.Current.AccountTerms.Count
                + accountDuePeriod.Future.AccountTerms.Count;
            var modelTermCounts = model.PeriodModels.SelectMany(pm => pm.Terms).Count();
            Assert.AreEqual(sourceTermCounts, modelTermCounts);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_DueAmountValid_AllPeriods()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            decimal dueAmount = CalcAmountDue(accountDuePeriod.Past)
                + CalcAmountDue(accountDuePeriod.Current)
                + CalcAmountDue(accountDuePeriod.Future);
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_DueAmountValid_PastPeriod()
        {
            AccountDuePeriod myAcctDuePrd = new AccountDuePeriod() { Past = AccountDueDtoSetup.accountDue, };
            MakeAPaymentModel model = MakeAPaymentModel.Build(myAcctDuePrd, pcfConfig, holder,  currentUser);
            decimal dueAmount = CalcAmountDue(myAcctDuePrd.Past);
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_DueAmountValid_CurrentPeriod()
        {
            AccountDuePeriod myAcctDuePrd = new AccountDuePeriod() { Current = AccountDueDtoSetup.accountDue, };
            MakeAPaymentModel model = MakeAPaymentModel.Build(myAcctDuePrd, pcfConfig, holder,  currentUser);
            decimal dueAmount = CalcAmountDue(myAcctDuePrd.Current);
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_DueAmountValid_FuturePeriod()
        {
            AccountDuePeriod myAcctDuePrd = new AccountDuePeriod() { Future = AccountDueDtoSetup.accountDue, };
            MakeAPaymentModel model = MakeAPaymentModel.Build(myAcctDuePrd, pcfConfig, holder,  currentUser);
            decimal dueAmount = CalcAmountDue(myAcctDuePrd.Future);
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaidAmountValid_AllPeriods()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            var modelTermCounts = model.PeriodModels.Select(pm => pm.Terms).Count();
            decimal paidAmount = 0;
            foreach (var period in model.PeriodModels)
            {
                paidAmount += SetAndReturnPaidAmount(period.Terms);
            }
            Assert.AreEqual(paidAmount, model.PaidAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaidAmountValid_PastPeriod()
        {
            AccountDuePeriod myAcctDuePrd = new AccountDuePeriod() { Past = AccountDueDtoSetup.accountDue, };
            MakeAPaymentModel model = MakeAPaymentModel.Build(myAcctDuePrd, pcfConfig, holder,  currentUser);
            decimal paidAmount = SetAndReturnPaidAmount(model.PeriodModels[0].Terms);
            Assert.AreEqual(paidAmount, model.PaidAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaidAmountValid_CurrentPeriod()
        {
            AccountDuePeriod myAcctDuePrd = new AccountDuePeriod() { Current = AccountDueDtoSetup.accountDue, };
            MakeAPaymentModel model = MakeAPaymentModel.Build(myAcctDuePrd, pcfConfig, holder,  currentUser);
            decimal paidAmount = SetAndReturnPaidAmount(model.PeriodModels[0].Terms);
            Assert.AreEqual(paidAmount, model.PaidAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaidAmountValid_FuturePeriod()
        {
            AccountDuePeriod myAcctDuePrd = new AccountDuePeriod() { Future = AccountDueDtoSetup.accountDue, };
            MakeAPaymentModel model = MakeAPaymentModel.Build(myAcctDuePrd, pcfConfig, holder,  currentUser);
            decimal paidAmount = SetAndReturnPaidAmount(model.PeriodModels[0].Terms);
            Assert.AreEqual(paidAmount, model.PaidAmount);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void MakeAPaymentModel_AccountDuePeriod_PeriodPaymentDataAndTermDisplayMode()
        {
            AccountDuePeriod myAcctDuePrd = new AccountDuePeriod() { Past = AccountDueDtoSetup.PastAccountDue, Current = AccountDueDtoSetup.CurrentAccountDue, Future = AccountDueDtoSetup.FutureAccountDue, };
            MakeAPaymentModel model = MakeAPaymentModel.Build(myAcctDuePrd, termConfig, holder,  currentUser);
            Assert.AreEqual(0, model.PaidAmount);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_SelectedItemsValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            int selectedItems = 0;
            foreach (var period in model.PeriodModels)
            {
                selectedItems += CountSelectedItems(period.Terms);
            }
            Assert.AreEqual(33, selectedItems);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_UnselectedItemsValid()
        {
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder,  currentUser);
            int unselectedItems = 0;
            foreach (var period in model.PeriodModels)
            {
                unselectedItems += CountUnselectedItems(period.Terms);
            }
            Assert.AreEqual(48, unselectedItems);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_HasPrivacyRestriction_False()
        {
            holder.PrivacyStatusCode = null;
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder, currentUser);
            Assert.IsFalse(model.HasPrivacyRestriction);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_HasPrivacyRestriction_False2()
        {
            holder.PrivacyStatusCode = "X";
            var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
            var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
            currentUser = new CurrentUser(claim); 
            
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder, currentUser);
            Assert.IsFalse(model.HasPrivacyRestriction);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PrivacyStatusCode_Set()
        {
            holder.PrivacyStatusCode = "X";
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder, currentUser);
            Assert.AreEqual(holder.PrivacyStatusCode, model.PrivacyStatusCode);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_UserPaymentPlanCreationEnabled_True()
        {
            pcfConfig.UserPaymentPlanCreationEnabled = true;
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder, currentUser);
            Assert.IsTrue(model.UserPaymentPlanCreationEnabled);
            pcfConfig.UserPaymentPlanCreationEnabled = false;
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_UserPaymentPlanCreationEnabled_False()
        {
            pcfConfig.UserPaymentPlanCreationEnabled = false;
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder, currentUser);
            Assert.IsFalse(model.UserPaymentPlanCreationEnabled);
        }

        [TestMethod]
        public void MakeAPaymentModel_AccountDuePeriod_PaymentPlanEligibilityText()
        {
            pcfConfig.PaymentPlanEligibilityText = "Here is some text.";
            MakeAPaymentModel model = MakeAPaymentModel.Build(accountDuePeriod, pcfConfig, holder, currentUser);
            Assert.AreEqual(pcfConfig.PaymentPlanEligibilityText, model.PaymentPlanEligibilityText);
        }

        #endregion

        #region InvoicesByTerm
    
           [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MakeAPaymentModel_InvoiceDue_NullSourceArgument()
        {
            MakeAPaymentModel.Build((InvoiceDue)null, termConfig, holder,  currentUser.PersonId);
        }

           [TestMethod]
           [ExpectedException(typeof(ArgumentNullException))]
           public void MakeAPaymentModel_InvoiceDue_NullConfigArgument()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               MakeAPaymentModel.Build(source, null, holder,  currentUser.PersonId);
           }

           [TestMethod]
           [ExpectedException(typeof(ArgumentNullException))]
           public void MakeAPaymentModel_InvoiceDue_NullHolderArgument()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               MakeAPaymentModel.Build(source, termConfig, null,  currentUser.PersonId);
           }

           [TestMethod]
           [ExpectedException(typeof(ArgumentNullException))]
           public void MakeAPaymentModel_InvoiceDue_NullUserArgument()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               MakeAPaymentModel.Build(source, termConfig, holder,  null);
           }

           [TestMethod]
           [ExpectedException(typeof(ArgumentNullException))]
           public void MakeAPaymentModel_InvoiceDue_EmptyUserArgument()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               MakeAPaymentModel.Build(source, termConfig, holder,  string.Empty);
           }

           [TestMethod]
           [ExpectedException(typeof(ArgumentNullException))]
           public void MakeAPaymentModel_InvoiceDue_EmptyDistributionCodeInSource()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               source.DistributionCode = null;
               MakeAPaymentModel.Build(source, termConfig, holder,  currentUser.PersonId);
           }

           [TestMethod]
           [ExpectedException(typeof(ArgumentNullException))]
           public void MakeAPaymentModel_InvoiceDue_EmptyListOfInvoicesInSource()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               source.Invoices = new List<Invoice>();
               MakeAPaymentModel.Build(source, termConfig, holder,  currentUser.PersonId);
           }

           [TestMethod]
           public void MakeAPaymentModel_InvoiceDue_ValidMakeAPaymentModel()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               MakeAPaymentModel model = MakeAPaymentModel.Build(source, termConfig, holder,  currentUser.PersonId);
               Assert.AreEqual(2, model.TermModels.Count);
               Assert.AreEqual(2, model.TermModels[0].PaymentsDue.Count);
               Assert.AreEqual(2, model.TermModels[1].PaymentsDue.Count);

               //first term collection
               Assert.AreEqual("2015/FA", model.TermModels[0].TermCode);
               Assert.AreEqual(source.DistributionCode, model.TermModels[0].PaymentsDue[0].PaymentGroup);
               Assert.AreEqual("01", model.TermModels[0].PaymentsDue[0].ReceivableTypeCode);
               Assert.AreEqual("11", model.TermModels[0].PaymentsDue[0].InvoiceId);
               Assert.AreEqual(source.DistributionCode, model.TermModels[0].PaymentsDue[1].PaymentGroup);
               Assert.AreEqual("03", model.TermModels[0].PaymentsDue[1].ReceivableTypeCode);
               Assert.AreEqual("13", model.TermModels[0].PaymentsDue[1].InvoiceId);

               //second term collection
               Assert.AreEqual("2016/FA", model.TermModels[1].TermCode);
               Assert.AreEqual(source.DistributionCode, model.TermModels[1].PaymentsDue[0].PaymentGroup);
               Assert.AreEqual("02", model.TermModels[1].PaymentsDue[0].ReceivableTypeCode);
               Assert.AreEqual("12", model.TermModels[1].PaymentsDue[0].InvoiceId);
               Assert.AreEqual(source.DistributionCode, model.TermModels[1].PaymentsDue[1].PaymentGroup);
               Assert.AreEqual("04", model.TermModels[1].PaymentsDue[1].ReceivableTypeCode);
               Assert.AreEqual("14", model.TermModels[1].PaymentsDue[1].InvoiceId);
           }

           [TestMethod]
           public void MakeAPaymentModel_InvoiceDue_HasPrivacyRestriction_False()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               holder.PrivacyStatusCode = null;
               MakeAPaymentModel model = MakeAPaymentModel.Build(source, termConfig, holder, currentUser.PersonId);
               Assert.IsFalse(model.HasPrivacyRestriction);
           }

           [TestMethod]
           public void MakeAPaymentModel_InvoiceDue_HasPrivacyRestriction_False2()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               holder.PrivacyStatusCode = "X";
               var userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJpc3MiOiJDb2xsZWFndWUiLCJhdWQiOiJodHRwOi8vZWxsdWNpYW4uY29tLyIsInBpZCI6IkhPTERFUjEiLCJzaWQiOiI3MDI5NDEzNDExMTg5NjkqNzkwMjk4NTIwIiwidG8iOiI5OTk5OSIsInVuIjoiam1hbnNmaWVsZCIsIm5hbWUiOiJKYXNvbiBNYW5zZmllbGQiLCJmeGlkIjoiRzM5MTQwSTQ4STM5Iiwicm9sZSI6IkZJTkFOQ0UgQURNSU5JU1RSQVRPUixTRUxGLVNFUlZJQ0UgQURNSU5JU1RSQVRPUixJTlRFR1JBVElPTiBIVUIgU1lOQyxTVFVERU5ULFBMQU5OSU5HVklFV09OTFksU1RVREVOVF9ST0xFIiwicGEiOiIiLCJhZiI6IjM3MzAzMjM5MzQzMTMzMzQzMTMxMzEzODM5MzYzOTJBMzczOTMwMzIzOTM4MzUzMjMwQ0I2QTBDRTEzNjQ5QzNDMTQxMDE3MjgzNTBGODQxNjBDQkVBOUVFMTFGNjk1QkFBMjMzNTBBNjA3NjRENUU4RSJ9";
               var claim = (Microsoft.IdentityModel.Claims.IClaimsPrincipal)Ellucian.Web.Security.JwtHelper.CreatePrincipal(userToken);
               
               currentUser = new CurrentUser(claim); 
               MakeAPaymentModel model = MakeAPaymentModel.Build(source, termConfig, holder, currentUser.PersonId);
               Assert.IsFalse(model.HasPrivacyRestriction);
           }

           [TestMethod]
           public void MakeAPaymentModel_InvoiceDue_PrivacyStatusCode_Set()
           {
               InvoiceDue source = CreateValidInvoiceDueModel();
               holder.PrivacyStatusCode = "X";
               MakeAPaymentModel model = MakeAPaymentModel.Build(source, termConfig, holder, currentUser.PersonId);
               Assert.AreEqual(holder.PrivacyStatusCode, model.PrivacyStatusCode);
           }

           [TestMethod]
           public void MakeAPaymentModel_InvoiceDue_UserPaymentPlanCreationEnabled()
           {
               pcfConfig.UserPaymentPlanCreationEnabled = false;
               InvoiceDue source = CreateValidInvoiceDueModel();
               MakeAPaymentModel model = MakeAPaymentModel.Build(source, termConfig, holder, currentUser.PersonId);
               Assert.IsFalse(model.UserPaymentPlanCreationEnabled);
           }

           private InvoiceDue CreateValidInvoiceDueModel()
           {
               List<Invoice> invoices = new List<Invoice>();
               invoices.Add(new Invoice() { Amount=5, Id="11" , ReceivableTypeCode="01", TermId="2015/FA" , Description="Graduation Application Fees" });
               invoices.Add(new Invoice() { Amount = 5, Id = "12", ReceivableTypeCode = "02", TermId = "2016/FA", Description = "Graduation Application Fees" });
               invoices.Add(new Invoice() { Amount = 5, Id = "13", ReceivableTypeCode = "03", TermId = "2015/FA", Description = "Graduation Application Fees" });
               invoices.Add(new Invoice() { Amount = 5, Id = "14", ReceivableTypeCode = "04", TermId = "2016/FA", Description = "Graduation Application Fees" });
               InvoiceDue invoice = new InvoiceDue("BANK", null, invoices);
               return invoice;
           }
        #endregion
        
        #region helper methods
        // traverses the data structure and accumulates all of the due amounts.  Purposefully does not use Lync.
        private decimal CalcAmountDue(AccountDue accountDue)
        {
            if (accountDue == null|| accountDue.AccountTerms == null) { return 0; }
            decimal amount = 0;

            foreach (AccountTerm x in accountDue.AccountTerms)
            {
                if (x.GeneralItems != null) foreach (AccountsReceivableDueItem y in x.GeneralItems) { amount += y.AmountDue.GetValueOrDefault(); }
                if (x.InvoiceItems != null) foreach (InvoiceDueItem y in x.InvoiceItems) { amount += y.AmountDue.GetValueOrDefault(); }
                if (x.PaymentPlanItems != null) foreach (PaymentPlanDueItem y in x.PaymentPlanItems) { amount += y.AmountDue.GetValueOrDefault(); }
                if (x.DepositDueItems != null) foreach (DepositDue y in x.DepositDueItems) { amount += y.AmountDue; }
            }
            return amount;
        }

        // traverses the data structure, updates the paid amounts, and accumulates the total paid.  Purposefully does not use Lync.
        private decimal SetAndReturnPaidAmount(List<TermSummaryModel> terms)
        {
            if (terms == null ) return 0;
            decimal amount = 0;
            foreach (TermSummaryModel x in terms)
            {
                foreach (PaymentsDueModel y in x.PaymentsDue)
                {
                    y.PaidAmount = 2;
                    amount += 2;
                }
            }
            return amount;
        }

        private int CountSelectedItems(List<TermSummaryModel> terms)
        {
            int selectedCount = 0;
            foreach (var term in terms)
            {
                var items = term.PaymentsDue.Where(x => x.IsSelected == true);
                selectedCount += (items == null) ? 0 : items.Count();
            }
            return selectedCount;
        }

        private int CountUnselectedItems(List<TermSummaryModel> terms)
        {
            int unselectedCount = 0;
            foreach (var term in terms)
            {
                var items = term.PaymentsDue.Where(x => x.IsSelected == false);
                unselectedCount += (items == null) ? 0 : items.Count();
            }
            return unselectedCount;
        }
        #endregion
    }
}
