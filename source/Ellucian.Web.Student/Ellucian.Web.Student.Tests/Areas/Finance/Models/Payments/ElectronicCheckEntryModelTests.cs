﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Tests.Areas.Finance.DataSetup;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.Payments;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Models;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Payments
{
    [TestClass]
    public class ElectronicCheckEntryModelTests
    {
        FinanceConfiguration config;
        PaymentReviewModel reviewModel;
        ElectronicCheckPayer payer;
        List<State> statesProvinces;

        string message;
        NotificationType type;

        [TestInitialize]
        public void Initialize()
        {
            config = new FinanceConfiguration()
            {
                PaymentMethods = new List<AvailablePaymentMethod>(){
                new AvailablePaymentMethod(){
                    Description = "Payment Method 1",
                    InternalCode = "PM1",
                },
                new AvailablePaymentMethod(){
                    Description = "Payment Method 2",
                    InternalCode = "PM2",
                }
            },
                NotificationText = "My notification",
                PaymentDisplay = PaymentDisplay.DisplayByTerm,
                PartialAccountPaymentsAllowed = true,
                PartialDepositPaymentsAllowed = true,
                PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed,
                ECommercePaymentsAllowed = true,
                SelfServicePaymentsAllowed = true,
                UseGuaranteedChecks = false
            };

            reviewModel = new PaymentReviewModel()
            {
                Payment = new Colleague.Dtos.Finance.Payments.Payment()
                {
                    AmountToPay = 1000m,
                    ConvenienceFee = "CF5",
                    ConvenienceFeeAmount = 5m,
                    ConvenienceFeeGeneralLedgerNumber = "110101000000010100",
                    Distribution = "BANK",
                    PayMethod = "ECHK",
                    PaymentItems = new List<Colleague.Dtos.Finance.Payments.PaymentItem>()
                {
                    new Colleague.Dtos.Finance.Payments.PaymentItem()
                    {
                        AccountType = "01",
                        DepositDueId = "123",
                        Description = "Meal Plan Deposit",
                        InvoiceId = string.Empty,
                        Overdue = false,
                        PaymentAmount = 100m,
                        PaymentComplete = false,
                        PaymentControlId = string.Empty,
                        PaymentPlanId = string.Empty,
                        Term = "2014/FA"
                    },
                    new Colleague.Dtos.Finance.Payments.PaymentItem()
                    {
                        AccountType = "01",
                        DepositDueId = string.Empty,
                        Description = "Payment Plan 124 - Payment #1",
                        InvoiceId = string.Empty,
                        Overdue = true,
                        PaymentAmount = 200m,
                        PaymentComplete = false,
                        PaymentControlId = string.Empty,
                        PaymentPlanId = "124",
                        Term = "2014/FA"
                    },
                    new Colleague.Dtos.Finance.Payments.PaymentItem()
                    {
                        AccountType = "01",
                        DepositDueId = string.Empty,
                        Description = "Invoice 125",
                        InvoiceId = "125",
                        Overdue = true,
                        PaymentAmount = 700m,
                        PaymentComplete = false,
                        PaymentControlId = string.Empty,
                        PaymentPlanId = string.Empty,
                        Term = "2014/FA"
                    },
                },
                    PersonId = "0003315",
                    ProviderAccount = "PROVCC",
                    ReturnToOriginUrl = "http://www.ellucianuniversity.edu/return-to-origin",
                    ReturnUrl = "http://www.ellucianuniversity.edu/return",
                }
            };

            payer = new ElectronicCheckPayer()
            {
                City = "Fairfax",
                Country = "USA",
                Email = "john.smith@ellucianuniversity.edu",
                FirstName = "John",
                LastName = "Smith",
                MiddleName = "Patrick",
                PostalCode = "22033-1234",
                State = "VA",
                Street = "123 Main Street",
                Telephone = "7039681234"
            };

            statesProvinces = new List<State>() {
                new State() { Code = "OH", CountryCode = "US", Description = "Ohio" },
                new State() { Code = "VA", CountryCode = "US", Description = "Virginia" }
            };

            message = "Unable to load Electronic Check Entry information.";
            type = NotificationType.Information;
        }

        [TestClass]
        public class ElectronicCheckEntryModel_Build : ElectronicCheckEntryModelTests
        {
            [TestInitialize]
            public void ElectronicCheckEntryModel_Build_Initialize()
            {
                Initialize();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ElectronicCheckEntryModel_Build_NullFinanceConfiguration()
            {
                var model = ElectronicCheckEntryModel.Build(null, reviewModel, payer, statesProvinces);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ElectronicCheckEntryModel_Build_NullPaymentReviewModel()
            {
                var model = ElectronicCheckEntryModel.Build(config, null, payer, statesProvinces);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ElectronicCheckEntryModel_Build_NullPaymentReviewModelPayment()
            {
                var model = ElectronicCheckEntryModel.Build(config, new PaymentReviewModel() { Payment = null }, payer, statesProvinces);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ElectronicCheckEntryModel_Build_NullElectronicCheckPayer()
            {
                var model = ElectronicCheckEntryModel.Build(config, reviewModel, null, statesProvinces);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ElectronicCheckEntryModel_Build_NullStates()
            {
                var model = ElectronicCheckEntryModel.Build(config, reviewModel, payer, null);
            }

            [TestMethod]
            public void ElectronicCheckEntryModel_Build_Valid()
            {
                payer.MiddleName = string.Empty;
                var model = ElectronicCheckEntryModel.Build(config, reviewModel, payer, statesProvinces);

                Assert.IsNotNull(model.Payment);
                Assert.AreEqual(reviewModel.Payment.AmountToPay, model.Payment.AmountToPay);
                Assert.IsNotNull(model.Payment.CheckDetails);
                Assert.IsNull(model.Payment.CheckDetails.AbaRoutingNumber);
                Assert.IsNull(model.Payment.CheckDetails.BankAccountNumber);
                Assert.AreEqual(payer.Street, model.Payment.CheckDetails.BillingAddress1);
                Assert.IsNull(model.Payment.CheckDetails.BillingAddress2);
                Assert.IsNull(model.Payment.CheckDetails.CheckNumber);
                Assert.AreEqual(payer.City, model.Payment.CheckDetails.City);
                Assert.IsNull(model.Payment.CheckDetails.DriversLicenseNumber);
                Assert.IsNull(model.Payment.CheckDetails.DriversLicenseState);
                Assert.AreEqual(payer.Email, model.Payment.CheckDetails.EmailAddress);
                Assert.AreEqual(payer.FirstName, model.Payment.CheckDetails.FirstName);
                Assert.AreEqual(payer.LastName, model.Payment.CheckDetails.LastName);
                Assert.AreEqual(payer.State, model.Payment.CheckDetails.State);
                Assert.AreEqual(payer.PostalCode.Replace("-", ""), model.Payment.CheckDetails.ZipCode);
                Assert.AreEqual(reviewModel.Payment.ConvenienceFee, model.Payment.ConvenienceFee);
                Assert.AreEqual(reviewModel.Payment.ConvenienceFeeAmount, model.Payment.ConvenienceFeeAmount);
                Assert.AreEqual(reviewModel.Payment.ConvenienceFeeGeneralLedgerNumber, model.Payment.ConvenienceFeeGeneralLedgerNumber);
                Assert.AreEqual(reviewModel.Payment.Distribution, model.Payment.Distribution);
                Assert.AreEqual(reviewModel.Payment.PaymentItems.Count, model.Payment.PaymentItems.Count);
                for (int i = 0; i < model.Payment.PaymentItems.Count; i++)
                {
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].AccountType, model.Payment.PaymentItems[i].AccountType);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].DepositDueId, model.Payment.PaymentItems[i].DepositDueId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].Description, model.Payment.PaymentItems[i].Description);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].InvoiceId, model.Payment.PaymentItems[i].InvoiceId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].Overdue, model.Payment.PaymentItems[i].Overdue);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentAmount, model.Payment.PaymentItems[i].PaymentAmount);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentComplete, model.Payment.PaymentItems[i].PaymentComplete);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentControlId, model.Payment.PaymentItems[i].PaymentControlId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentPlanId, model.Payment.PaymentItems[i].PaymentPlanId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].Term, model.Payment.PaymentItems[i].Term);
                }
                Assert.AreEqual(reviewModel.Payment.PayMethod, model.Payment.PayMethod);
                Assert.AreEqual(reviewModel.Payment.PersonId, model.Payment.PersonId);
                Assert.AreEqual(reviewModel.Payment.ProviderAccount, model.Payment.ProviderAccount);
                Assert.AreEqual(reviewModel.Payment.ReturnToOriginUrl, model.Payment.ReturnToOriginUrl);
                Assert.AreEqual(reviewModel.Payment.ReturnUrl, model.Payment.ReturnUrl);

                Assert.AreEqual(statesProvinces.Count, model.StateList.Count);
                for (int j = 0; j < model.StateList.Count; j++)
                {
                    Assert.AreEqual(statesProvinces[j].Code, model.StateList[j].Code);
                    Assert.AreEqual(statesProvinces[j].CountryCode, model.StateList[j].Country);
                    Assert.AreEqual(statesProvinces[j].Description, model.StateList[j].Description);
                }

                Assert.AreEqual(config.UseGuaranteedChecks, model.UseGuaranteedChecks);
            }

            [TestMethod]
            public void ElectronicCheckEntryModel_Build_Valid_With_MiddleName()
            {
                payer.MiddleName = "Henry";
                var model = ElectronicCheckEntryModel.Build(config, reviewModel, payer, statesProvinces);

                Assert.IsNotNull(model.Payment);
                Assert.AreEqual(reviewModel.Payment.AmountToPay, model.Payment.AmountToPay);
                Assert.IsNotNull(model.Payment.CheckDetails);
                Assert.IsNull(model.Payment.CheckDetails.AbaRoutingNumber);
                Assert.IsNull(model.Payment.CheckDetails.BankAccountNumber);
                Assert.AreEqual(payer.Street, model.Payment.CheckDetails.BillingAddress1);
                Assert.IsNull(model.Payment.CheckDetails.BillingAddress2);
                Assert.IsNull(model.Payment.CheckDetails.CheckNumber);
                Assert.AreEqual(payer.City, model.Payment.CheckDetails.City);
                Assert.IsNull(model.Payment.CheckDetails.DriversLicenseNumber);
                Assert.IsNull(model.Payment.CheckDetails.DriversLicenseState);
                Assert.AreEqual(payer.Email, model.Payment.CheckDetails.EmailAddress);
                Assert.AreEqual(payer.FirstName, model.Payment.CheckDetails.FirstName);
                Assert.AreEqual(payer.MiddleName + " " + payer.LastName, model.Payment.CheckDetails.LastName);
                Assert.AreEqual(payer.State, model.Payment.CheckDetails.State);
                Assert.AreEqual(payer.PostalCode.Replace("-", ""), model.Payment.CheckDetails.ZipCode);
                Assert.AreEqual(reviewModel.Payment.ConvenienceFee, model.Payment.ConvenienceFee);
                Assert.AreEqual(reviewModel.Payment.ConvenienceFeeAmount, model.Payment.ConvenienceFeeAmount);
                Assert.AreEqual(reviewModel.Payment.ConvenienceFeeGeneralLedgerNumber, model.Payment.ConvenienceFeeGeneralLedgerNumber);
                Assert.AreEqual(reviewModel.Payment.Distribution, model.Payment.Distribution);
                Assert.AreEqual(reviewModel.Payment.PaymentItems.Count, model.Payment.PaymentItems.Count);
                for (int i = 0; i < model.Payment.PaymentItems.Count; i++)
                {
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].AccountType, model.Payment.PaymentItems[i].AccountType);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].DepositDueId, model.Payment.PaymentItems[i].DepositDueId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].Description, model.Payment.PaymentItems[i].Description);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].InvoiceId, model.Payment.PaymentItems[i].InvoiceId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].Overdue, model.Payment.PaymentItems[i].Overdue);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentAmount, model.Payment.PaymentItems[i].PaymentAmount);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentComplete, model.Payment.PaymentItems[i].PaymentComplete);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentControlId, model.Payment.PaymentItems[i].PaymentControlId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].PaymentPlanId, model.Payment.PaymentItems[i].PaymentPlanId);
                    Assert.AreEqual(reviewModel.Payment.PaymentItems[i].Term, model.Payment.PaymentItems[i].Term);
                }
                Assert.AreEqual(reviewModel.Payment.PayMethod, model.Payment.PayMethod);
                Assert.AreEqual(reviewModel.Payment.PersonId, model.Payment.PersonId);
                Assert.AreEqual(reviewModel.Payment.ProviderAccount, model.Payment.ProviderAccount);
                Assert.AreEqual(reviewModel.Payment.ReturnToOriginUrl, model.Payment.ReturnToOriginUrl);
                Assert.AreEqual(reviewModel.Payment.ReturnUrl, model.Payment.ReturnUrl);

                Assert.AreEqual(statesProvinces.Count, model.StateList.Count);
                for (int j = 0; j < model.StateList.Count; j++)
                {
                    Assert.AreEqual(statesProvinces[j].Code, model.StateList[j].Code);
                    Assert.AreEqual(statesProvinces[j].CountryCode, model.StateList[j].Country);
                    Assert.AreEqual(statesProvinces[j].Description, model.StateList[j].Description);
                }

                Assert.AreEqual(config.UseGuaranteedChecks, model.UseGuaranteedChecks);
            }

        }

        [TestClass]
        public class ElectronicCheckEntryModel_BuildWithErrors : ElectronicCheckEntryModelTests
        {
            [TestInitialize]
            public void ElectronicCheckEntryModel_BuildWithErrors_Initialize()
            {
                Initialize();
            }

            [TestMethod]
            public void ElectronicCheckEntryModel_BuildWithErrors_NullMessage()
            {
                var model = ElectronicCheckEntryModel.BuildWithErrors(null, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void ElectronicCheckEntryModel_BuildWithErrors_EmptyMessage()
            {
                var model = ElectronicCheckEntryModel.BuildWithErrors(string.Empty, type);

                // Null is expected because resource files have not been rendered
                Assert.AreEqual(null, model.Notification.Message);
                Assert.AreEqual("Information", model.Notification.Type);
            }

            [TestMethod]
            public void ElectronicCheckEntryModel_BuildWithErrors_NoType()
            {
                var model = ElectronicCheckEntryModel.BuildWithErrors(message);
                Assert.AreEqual(message, model.Notification.Message);
                Assert.AreEqual("Error", model.Notification.Type);
            }
        }
    }
}
