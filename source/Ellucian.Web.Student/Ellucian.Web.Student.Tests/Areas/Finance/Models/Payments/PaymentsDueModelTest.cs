﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Payments
{
    [TestClass]
    public class PaymentsDueModelTest
    {
        const string description = "Description";
        const string distribution = "Distribution";
        const string accountType = "AccountType";
        const string invoiceId = "InvoiceId";
        const string paymentPlanId = "PaymentPlanId";
        const string depositDueId = "DepositDueId";
        static decimal? dueAmount = 1000;
        static decimal? nullDueAmount = null;
        static decimal minimumPaymentAmount = 1m;
        static DateTime dueDate = DateTime.Today;
        static DateTime? nullDueDate = null;

        FinanceConfiguration config = new FinanceConfiguration()
            {
                DisplayedReceivableTypes = new List<PayableReceivableType>()
            };

        AccountsReceivableDueItem generalItemDue = new AccountsReceivableDueItem()
        {
            Description = description,
            AmountDue = dueAmount,
            DueDate = dueDate,
            Distribution = distribution,
            AccountType = accountType,
        };


        DepositDue depositDue = new DepositDue()
        {
            DepositTypeDescription = description,
            AmountDue = dueAmount.GetValueOrDefault(),
            DueDate = dueDate,
            Distribution = distribution,
            Id = depositDueId,
        };
        PaymentPlanDueItem paymentPlanDueItem = new PaymentPlanDueItem()
            {
                AccountType = accountType
            };
        PaymentsDueModel model;

        // No Initialize needed
        #region 'Build' argument tests
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentsDueModel_Build_NullAccountsReceivableDueItemArgument()
        {
            model = PaymentsDueModel.Build((AccountsReceivableDueItem)null, config);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentsDueModel_Build_NullConfigArgument()
        {
            model = PaymentsDueModel.Build(generalItemDue, (FinanceConfiguration)null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentsDueModel_Build_NullDepositDueArgument()
        {
            model = PaymentsDueModel.Build((DepositDue)null, config);
        }
        #endregion

        #region AccountsReceivableDueItem
        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_Description()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.AreEqual(description, model.Description);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_DueAmount()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_NullDueAmount()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = nullDueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.AreEqual(default(decimal), model.DueAmount);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_DueDate()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.AreEqual(dueDate, model.DueDate);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_NullDueDate()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = nullDueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.AreEqual(default(DateTime?), model.DueDate);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_NullDueDate_IsOverdueFalse()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = nullDueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.IsFalse(model.IsOverdue);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_NullDueDate_IsOverdueFalse2()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = dueDate.AddDays(3),
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.IsFalse(model.IsOverdue);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_PastDueDate_IsOverdueTrue()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = DateTime.Today.AddDays(-3),
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.IsTrue(model.IsOverdue);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_Distribution()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.AreEqual(distribution, model.PaymentGroup);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_AccountType()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.AreEqual(accountType, model.ReceivableTypeCode);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsOverDue_True()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = dueDate.AddDays(-1),
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.IsTrue(model.IsOverdue);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsOverDue_False()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.IsFalse(model.IsOverdue);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsCredit_True()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = -dueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.IsTrue(model.IsCredit);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_MinimumPaymentNullForCredit()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = -dueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.IsNull(model.MinimumPayment);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsCredit_False()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.IsFalse(model.IsCredit);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsSelected_False1()
        {
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = -dueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.IsFalse(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsSelected_True()
        {
            var itemDueToday = generalItemDue;
            itemDueToday.DueDate = DateTime.Today;
            model = PaymentsDueModel.Build(itemDueToday, config);
            Assert.IsTrue(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsSelectedTrue()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.IsTrue(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_IsSelectedFalseWhenCredit()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = -dueAmount, AccountType = accountType }, config);
            Assert.IsFalse(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsEditable_FalseWhenCreditItem()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = -dueAmount, AccountType = accountType }, config);
            Assert.IsFalse(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsEditable_FalseWhenConfigFalse()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AccountType = accountType },
                new FinanceConfiguration() { PartialAccountPaymentsAllowed = false, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.IsFalse(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsEditable_TrueWhenConfigTrue()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AccountType = accountType },
                new FinanceConfiguration() { PartialAccountPaymentsAllowed = true, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.IsTrue(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_MinimumPayment_NullWhenCreditItem()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = -dueAmount, AccountType = accountType }, config);
            Assert.IsFalse(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_MinimumPayment_DueAmountWhenNotEditable()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = accountType },
                new FinanceConfiguration() { PartialAccountPaymentsAllowed = false, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_MinimumPayment_MinimumWhenEditable()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = accountType },
                new FinanceConfiguration() { PartialAccountPaymentsAllowed = true, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.AreEqual(minimumPaymentAmount, model.MinimumPayment);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsPayable_Empty_DisplayedReceivableTypes()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = accountType },
                new FinanceConfiguration() { PartialAccountPaymentsAllowed = true, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.IsTrue(model.IsPayable);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentsDueModel_GeneralItemDue_IsPayable_Null_DisplayedReceivableTypes()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = accountType },
                new FinanceConfiguration() { PartialAccountPaymentsAllowed = true, DisplayedReceivableTypes = null });
            Assert.IsTrue(model.IsPayable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsPayable_True()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = "01" },
                new FinanceConfiguration()
                {
                    PartialAccountPaymentsAllowed = true,
                    DisplayedReceivableTypes = new List<PayableReceivableType>() 
                    { 
                        new PayableReceivableType() { Code = "01", IsPayable = true }
                    }
                });
            Assert.IsTrue(model.IsPayable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsPayable_False()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = "01" },
                new FinanceConfiguration()
                {
                    PartialAccountPaymentsAllowed = true,
                    DisplayedReceivableTypes = new List<PayableReceivableType>() 
                    { 
                        new PayableReceivableType() { Code = "01", IsPayable = false }
                    }
                });
            Assert.IsFalse(model.IsPayable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsPayable_False2()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = "01" },
                new FinanceConfiguration()
                {
                    PartialAccountPaymentsAllowed = true,
                    DisplayedReceivableTypes = new List<PayableReceivableType>() 
                    { 
                        new PayableReceivableType() { Code = "02", IsPayable = false }
                    }
                });
            Assert.IsFalse(model.IsPayable);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsPayable_False_CreditItem()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = -10m, AccountType = "01" },
                new FinanceConfiguration()
                {
                    PartialAccountPaymentsAllowed = true,
                    DisplayedReceivableTypes = new List<PayableReceivableType>() 
                    { 
                        new PayableReceivableType() { Code = "01", IsPayable = true }
                    }
                });
            Assert.IsFalse(model.IsPayable);
        }
        
        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsEligibleForPaymentPlan_True()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = dueAmount, AccountType = "01" },
                new FinanceConfiguration()
                {
                    PartialAccountPaymentsAllowed = true,
                    DisplayedReceivableTypes = new List<PayableReceivableType>() 
                    { 
                        new PayableReceivableType() { Code = "01", IsPayable = true }
                    }
                });
            Assert.IsTrue(model.IsEligibleForPaymentPlan);
        }


        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_IsEligibleForPaymentPlan_False_CreditItem()
        {
            model = PaymentsDueModel.Build(new AccountsReceivableDueItem() { AmountDue = -10m, AccountType = "01" },
                new FinanceConfiguration()
                {
                    PartialAccountPaymentsAllowed = true,
                    DisplayedReceivableTypes = new List<PayableReceivableType>() 
                    { 
                        new PayableReceivableType() { Code = "01", IsPayable = true }
                    }
                });
            Assert.IsFalse(model.IsEligibleForPaymentPlan);
        }

        [TestMethod]
        public void PaymentsDueModel_GeneralItemDue_PaymentPlanTemplateId()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.IsNull(model.PaymentPlanTemplateId);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_GeneralItemDue_TermId()
        {
            model = PaymentsDueModel.Build(generalItemDue, config);
            Assert.IsNull(model.TermId);
        }

        #endregion

        #region InvoiceItemDue
        [TestMethod]
        public void PaymentsDueModel_Build_InvoiceItemDue_InvoiceId()
        {
            InvoiceDueItem invoiceDueItem = new InvoiceDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            };

            model = PaymentsDueModel.Build(invoiceDueItem, config);
            Assert.AreEqual(invoiceId, model.InvoiceId);
            Assert.AreEqual(string.Empty, model.PaymentPlanId);
            Assert.AreEqual(string.Empty, model.DepositDueId);
            Assert.IsFalse(model.IsEligibleForPaymentPlan);
        }
        #endregion

        #region PaymentPlanItemDue
        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_PaymentPlanId()
        {
            paymentPlanDueItem = new PaymentPlanDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                PaymentPlanId = paymentPlanId,
            };

            model = PaymentsDueModel.Build(paymentPlanDueItem, config);
            Assert.AreEqual(paymentPlanId, model.PaymentPlanId);
            Assert.AreEqual(string.Empty, model.InvoiceId);
            Assert.AreEqual(string.Empty, model.DepositDueId);
            Assert.IsFalse(model.IsEligibleForPaymentPlan);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_MinimumPayment_AmountDue()
        {
            config.PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed;
            model = PaymentsDueModel.Build(new InvoiceDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                InvoiceId = invoiceId,
            }, config);
            Assert.AreEqual(minimumPaymentAmount, model.MinimumPayment);
        }


        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_IsSelectedFalse()
        {
            paymentPlanDueItem = new PaymentPlanDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = dueDate,
                Distribution = distribution,
                AccountType = accountType,
                PaymentPlanId = paymentPlanId,
            };

            model = PaymentsDueModel.Build(paymentPlanDueItem, config);
            Assert.IsFalse(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_IsSelectedTrueWhenOverdue()
        {
            paymentPlanDueItem = new PaymentPlanDueItem()
            {
                Description = description,
                AmountDue = dueAmount,
                DueDate = dueDate.AddDays(-1),
                Distribution = distribution,
                AccountType = accountType,
                PaymentPlanId = paymentPlanId,
            };

            model = PaymentsDueModel.Build(paymentPlanDueItem, config);
            Assert.IsTrue(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_IsEditable_TrueWhenAllowed()
        {
            paymentPlanDueItem = new PaymentPlanDueItem() { PaymentPlanId = paymentPlanId, AccountType = accountType };

            model = PaymentsDueModel.Build(paymentPlanDueItem, 
                new FinanceConfiguration { PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.IsTrue(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_IsEditable_TrueWhenNotOverdue()
        {
            paymentPlanDueItem = new PaymentPlanDueItem() { PaymentPlanId = paymentPlanId, DueDate = DateTime.Today, AccountType = accountType};

            model = PaymentsDueModel.Build(paymentPlanDueItem,
                new FinanceConfiguration { PartialPlanPaymentsAllowed = PartialPlanPayments.AllowedWhenNotOverdue, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.IsTrue(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_IsEditable_FalseWhenOverdue()
        {
            paymentPlanDueItem = new PaymentPlanDueItem() { PaymentPlanId = paymentPlanId, DueDate = DateTime.Today.AddDays(-1), AccountType = accountType };

            model = PaymentsDueModel.Build(paymentPlanDueItem,
                new FinanceConfiguration { PartialPlanPaymentsAllowed = PartialPlanPayments.AllowedWhenNotOverdue, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.IsFalse(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_PaymentPlanItemDue_IsEditable_FalseWhenDenied()
        {
            paymentPlanDueItem = new PaymentPlanDueItem() { PaymentPlanId = paymentPlanId, AccountType = accountType };

            model = PaymentsDueModel.Build(paymentPlanDueItem,
                new FinanceConfiguration { PartialPlanPaymentsAllowed = PartialPlanPayments.Denied, DisplayedReceivableTypes = new List<PayableReceivableType>() });
            Assert.IsFalse(model.IsEditable);
        }
        #endregion

        #region DepositDueItem
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentsDueModel_Build_NullDepositDueItem()
        {
            model = PaymentsDueModel.Build((DepositDue)null, config);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentsDueModel_Build_NullDepositDueConfigItem()
        {
            model = PaymentsDueModel.Build(depositDue, (FinanceConfiguration) null);

        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_Description()
        {
            model = PaymentsDueModel.Build(depositDue, config);
            Assert.AreEqual(description, model.Description);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_DueAmount()
        {
            model = PaymentsDueModel.Build(depositDue, config);
            Assert.AreEqual(dueAmount, model.DueAmount);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_DueDate()
        {
            model = PaymentsDueModel.Build(depositDue, config);
            Assert.AreEqual(dueDate, model.DueDate);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_Distribution()
        {
            model = PaymentsDueModel.Build(depositDue, config);
            Assert.AreEqual(distribution, model.PaymentGroup);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_DepositDueId()
        {
            model = PaymentsDueModel.Build(depositDue, config);
            Assert.AreEqual(depositDueId, model.DepositDueId);
            Assert.AreEqual(string.Empty, model.InvoiceId);
            Assert.AreEqual(string.Empty, model.PaymentPlanId);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDue_IsSelectedTrue()
        {
            model = PaymentsDueModel.Build(depositDue, config);
            Assert.IsTrue(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_IsSelectedFalseWhenCredit()
        {
            model = PaymentsDueModel.Build(new DepositDue() { AmountDue = (decimal)-dueAmount, }, config);
            Assert.IsFalse(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_IsSelectedFalseWhenFutureDueDate()
        {
            model = PaymentsDueModel.Build(new DepositDue() { AmountDue = (decimal)dueAmount, DueDate = dueDate.AddDays(5)}, config);
            Assert.IsFalse(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_IsSelectedTrueWhenPastDueDate()
        {
            model = PaymentsDueModel.Build(new DepositDue() { AmountDue = (decimal)dueAmount, DueDate = DateTime.Today.AddDays(-5) }, config);
            Assert.IsTrue(model.IsSelected);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_IsEditable_FalseWhenNotAllowed()
        {
            model = PaymentsDueModel.Build(new DepositDue(), 
                new FinanceConfiguration() { PartialDepositPaymentsAllowed = false, });
            Assert.IsFalse(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_IsEditable_TrueWhenAllowed()
        {
            model = PaymentsDueModel.Build(new DepositDue(), 
                new FinanceConfiguration() { PartialDepositPaymentsAllowed = true, });
            Assert.IsTrue(model.IsEditable);
        }

        [TestMethod]
        public void PaymentsDueModel_Build_DepositDueItem_IsEligibleForPaymentPlan()
        {
            model = PaymentsDueModel.Build(new DepositDue(), 
                new FinanceConfiguration() { PartialDepositPaymentsAllowed = true, });
            Assert.IsFalse(model.IsEligibleForPaymentPlan);
        }

        #endregion
    }
}
