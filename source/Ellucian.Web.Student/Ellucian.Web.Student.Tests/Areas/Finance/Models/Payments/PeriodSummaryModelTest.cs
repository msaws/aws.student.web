﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Payments;
using Ellucian.Web.Student.Tests.Areas.Finance.DataSetup;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Payments
{
    [TestClass]
    public class PeriodSummaryModelTest
    {
        AccountDue source = AccountDueDtoSetup.CurrentAccountDue;
        FinanceConfiguration config = new FinanceConfiguration() { DisplayedReceivableTypes = new List<PayableReceivableType>() };

        PeriodSummaryModel item;

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PeriodSummaryModel_Build_NullAccountDueArgument()
        {
            item = PeriodSummaryModel.Build(null, config);
        }

        [ExpectedException(typeof(ArgumentNullException))]
        public void PeriodSummaryModel_Build_NullConfigArgument()
        {
            item = PeriodSummaryModel.Build(new AccountDue(), null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PeriodSummaryModel_Build_NullAccountDueAccountTerms()
        {
            item = PeriodSummaryModel.Build(new AccountDue() { AccountTerms = null }, config);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PeriodSummaryModel_Build_NoDates()
        {
            item = PeriodSummaryModel.Build(new AccountDue() { AccountTerms = new List<AccountTerm>() }, config);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PeriodSummaryModel_Build_NoTerms()
        {
            item = PeriodSummaryModel.Build(new AccountDue(), config);
        }

        [TestMethod]
        public void PeriodSummaryModel_Terms_Count()
        {
            item = PeriodSummaryModel.Build(source, config);
            Assert.AreEqual(source.AccountTerms.Count, item.Terms.Count);
        }

        [TestMethod]
        public void PeriodSummaryModel_HasItems_False_NullTerms()
        {
            item = PeriodSummaryModel.Build(source, config);
            item.Terms = null;
            Assert.IsFalse(item.HasItems);
        }

        [TestMethod]
        public void PeriodSummaryModel_HasItems_False_NoTerms()
        {
            item = PeriodSummaryModel.Build(source, config);
            item.Terms = new List<TermSummaryModel>();
            Assert.IsFalse(item.HasItems);
        }

        [TestMethod]
        public void PeriodSummaryModel_HasItems_True()
        {
            item = PeriodSummaryModel.Build(source, config);
            Assert.IsTrue(item.HasItems);
        }

        [TestMethod]
        public void PeriodSummaryModel_DueAmount_Valid()
        {
            item = PeriodSummaryModel.Build(source, config);
            decimal dueAmount = 0;
            foreach (TermSummaryModel x in item.Terms)
            {
                dueAmount += x.DueAmount;
            }
            Assert.AreEqual(dueAmount, item.DueAmount);
        }

        [TestMethod]
        public void PeriodSummaryModel_PaidAmount_Valid()
        {
            item = PeriodSummaryModel.Build(source, config);
            decimal paidAmount = 0;
            foreach (TermSummaryModel x in item.Terms)
            {
                foreach (PaymentsDueModel y in x.PaymentsDue)
                {
                    y.PaidAmount = 2;
                    paidAmount += 2;
                }
            }

            Assert.AreEqual(paidAmount, item.PaidAmount);
        }

        [TestMethod]
        public void PeriodSummaryModel_PeriodInformation_PastIsValid()
        {
            var model = PeriodSummaryModel.Build(AccountDueDtoSetup.PastAccountDue, config);
            Assert.AreEqual("PAST", model.PeriodCode);
        }

        [TestMethod]
        public void PeriodSummaryModel_PeriodInformation_CurrentIsValid()
        {
            var model = PeriodSummaryModel.Build(AccountDueDtoSetup.CurrentAccountDue, config);
            Assert.AreEqual("CUR", model.PeriodCode);
        }

        [TestMethod]
        public void PeriodSummaryModel_PeriodInformation_FutureIsValid()
        {
            var model = PeriodSummaryModel.Build(AccountDueDtoSetup.FutureAccountDue, config);
            Assert.AreEqual("FTR", model.PeriodCode);
        }

        [TestMethod]
        public void PeriodSummaryModel_Term_PaymentsDue_TermId()
        {
            var model = PeriodSummaryModel.Build(AccountDueDtoSetup.FutureAccountDue, config);
            foreach (TermSummaryModel x in model.Terms)
            {
                foreach (PaymentsDueModel y in x.PaymentsDue)
                {
                    Assert.AreEqual(x.TermCode, y.TermId);
                }
            }

        }
    }

}
