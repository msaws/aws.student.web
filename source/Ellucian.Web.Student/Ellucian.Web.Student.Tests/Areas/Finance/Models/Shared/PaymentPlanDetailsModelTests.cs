﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class PaymentPlanDetailsModelTests
    {
        PaymentPlan paymentPlan;
        PaymentPlanDisplayModel displayModel;
        AccountHolder accountHolder;
        List<string> ackText;
        List<string> termsText;
        FinanceConfiguration config;
        string termDescription;

        [TestInitialize]
        public void Initialize()
        {
            paymentPlan = new PaymentPlan()
            {
                CurrentAmount = 1000m,
                CurrentStatus = PlanStatusType.Open,
                CurrentStatusDate = DateTime.Today,
                DownPaymentAmount = 25m,
                DownPaymentAmountPaid = 0m,
                DownPaymentDate = DateTime.Today.AddDays(3),
                DownPaymentPercentage = 2.5m,
                FirstDueDate = DateTime.Today.AddDays(3),
                Frequency = PlanFrequency.Weekly,
                GraceDays = 3,
                Id = "1234",
                LateChargeAmount = 5m,
                LateChargePercentage = 1m,
                NumberOfPayments = 5,
                OriginalAmount = 1020m,
                PersonId = "0003315",
                ReceivableTypeCode = "01",
                SetupAmount = 10m,
                SetupPercentage = 1m,
                TemplateId = "DEFAULT",
                TermId = "2017/FA",
                TotalSetupChargeAmount = 20m,
                PlanCharges = new List<PlanCharge>()
                {
                    new PlanCharge() {
                        Amount = 600m,
                        Id = "1234*123",
                        IsAutomaticallyModifiable = true,
                        IsSetupCharge = false,
                        PlanId = "1234",
                        Charge = new Charge() {
                            Amount = 600m,
                            BaseAmount = 600m,
                            Code = "ACTFE",
                            Description = new List<string>() { "Student Activities Fee" },
                            InvoiceId = "135",
                            TaxAmount = 0m,
                            PaymentPlanIds = new List<string>() { "1234" },
                            Id = "123"
                        }
                    },
                    new PlanCharge() {
                        Amount = 400m,
                        Id = "1234*456",
                        IsAutomaticallyModifiable = true,
                        IsSetupCharge = false,
                        PlanId = "1234",
                        Charge = new Charge() {
                            Amount = 400m,
                            BaseAmount = 375m,
                            Code = "HLTFE",
                            Description = new List<string>() { "Student Health Fee" },
                            InvoiceId = "136",
                            TaxAmount = 25m,
                            PaymentPlanIds = new List<string>() { "1234" },
                            Id = "456"
                        }
                    },
                    new PlanCharge() {
                        Amount = 600m,
                        Id = "1234*789",
                        IsAutomaticallyModifiable = true,
                        IsSetupCharge = true,
                        PlanId = "1234",
                        Charge = new Charge() {
                            Amount = 20m,
                            BaseAmount = 20m,
                            Code = "SETUP",
                            Description = new List<string>() { "Setup Fee" },
                            InvoiceId = "136",
                            TaxAmount = 0m,
                            PaymentPlanIds = new List<string>() { "1234" },
                            Id = "789"
                        }
                    }
                },
                ScheduledPayments = new List<ScheduledPayment>() {
                    new ScheduledPayment() {
                        Amount = 220m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(3),
                        Id = "2345",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(13),
                        Id = "2346",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(23),
                        Id = "2347",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(33),
                        Id = "2348",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(43),
                        Id = "2349",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    }
                }
            };

            displayModel = PaymentPlanDisplayModel.Build(paymentPlan);

            accountHolder = new AccountHolder()
            {
                Id = "0001234",
                PreferredName = "Rick Astley"
            };

            ackText = new List<string>()
            {
                "This is some acknowledgement text.",
                "Here is some more text."
            };

            termsText = new List<string>()
            {
                "This is some terms & conditions text.",
                "Here is even more text."
            };

            config = new FinanceConfiguration()
            {
                PaymentMethods = new List<AvailablePaymentMethod>(){
                    new AvailablePaymentMethod(){
                        Description = "Payment Method 1",
                        InternalCode = "PM1",
                    },
                    new AvailablePaymentMethod(){
                        Description = "Payment Method 2",
                        InternalCode = "PM2",
                    }
                },
                NotificationText = "My notification",
                ActivityDisplay = ActivityDisplay.DisplayByTerm,
                PaymentDisplay = PaymentDisplay.DisplayByTerm,
                PartialAccountPaymentsAllowed = true,
                PartialDepositPaymentsAllowed = true,
                PartialPlanPaymentsAllowed = PartialPlanPayments.Allowed,
                ECommercePaymentsAllowed = true,
                SelfServicePaymentsAllowed = true,
                Periods = new List<FinancialPeriod>() 
                {
                    new FinancialPeriod() { End = DateTime.Today.AddDays(-31), Type = PeriodType.Past },
                    new FinancialPeriod() { End = DateTime.Today.AddDays(30), Start = DateTime.Today.AddDays(-30), Type = PeriodType.Current },
                    new FinancialPeriod() { End = DateTime.Today.AddDays(31), Type = PeriodType.Future },
                }
            };

            termDescription = "2017 Fall Term";
        }

        [TestCleanup]
        public void Cleanup()
        {
            paymentPlan = null;
            accountHolder = null;
            ackText = null;
            termsText = null;
        }

        [TestClass]
        public class BuildDisplayPlanPreviewTests : PaymentPlanDetailsModelTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Null_PaymentPlan()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(null, accountHolder, ackText, termsText, config, termDescription);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Null_AccountHolder()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(displayModel, null, ackText, termsText, config, termDescription);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Null_AcknowledgementText()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(displayModel, accountHolder, null, termsText, config, termDescription);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Empty_AcknowledgementText()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(displayModel, accountHolder, new List<string>(), termsText, config, termDescription);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Null_TermsText()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(displayModel, accountHolder, ackText, null, config, termDescription);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Empty_TermsText()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(displayModel, accountHolder, ackText, new List<string>(), config, termDescription);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Null_Configuration()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(displayModel, accountHolder, ackText, termsText, null, termDescription);
            }

            [TestMethod]
            public void PaymentPlanDetailsModel_BuildDisplayPlanPreview_Valid()
            {
                var model = PaymentPlanDetailsModel.BuildDisplayPlanPreview(displayModel, accountHolder, ackText, termsText, config, termDescription);
                Assert.AreEqual(DateTime.Now.Date, model.DisplayTimestamp.Date);
                CollectionAssert.AreEqual(ackText, model.AcknowledgementText);
                Assert.IsNotNull(model.DisplayPaymentPlan);
                Assert.AreEqual(accountHolder.PreferredName, model.StudentName);
                CollectionAssert.AreEqual(termsText, model.TermsAndConditionsText);
                Assert.IsNull(model.ApprovalTimestamp);
                Assert.IsNull(model.ApprovalUserId);
                Assert.IsFalse(model.IsPrintable);
                Assert.AreEqual(String.Format("{0} {1}", accountHolder.Id, accountHolder.PreferredName), model.DisplayPaymentPlan.PlanDisplayName);
                Assert.AreEqual(String.Format("{0} {1}", accountHolder.Id, accountHolder.PreferredName), model.DisplayPaymentPlan.PlanDisplayName);
                Assert.AreEqual(string.Join(Environment.NewLine, termsText), model.PlanTermsAndConditionsText);
                Assert.AreEqual(config.PaymentMethods.Count, model.AvailablePaymentMethods.Count);
            }
        }
    }
}
