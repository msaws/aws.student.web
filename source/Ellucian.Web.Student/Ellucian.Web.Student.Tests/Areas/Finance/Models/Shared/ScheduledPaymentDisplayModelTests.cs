﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class ScheduledPaymentDisplayModelTests
    {
        ScheduledPayment scheduledPayment;
        ScheduledPaymentDisplayModel model;

        [TestInitialize]
        public void Initialize()
        {
            scheduledPayment = new ScheduledPayment() {
                Amount = 220m,
                AmountPaid = 30m,
                DueDate = DateTime.Today.AddDays(3),
                Id = "2345",
                IsPastDue = false,
                LastPaidDate = DateTime.Today.AddDays(-2),
                PlanId = "1234"
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            scheduledPayment = null;
            model = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ScheduledPaymentDisplayModel_Null_ScheduledPayment()
        {
            model = ScheduledPaymentDisplayModel.Build(null);
        }

        [TestMethod]
        public void ScheduledPaymentDisplayModel_Valid()
        {
            model = ScheduledPaymentDisplayModel.Build(scheduledPayment);
            Assert.AreEqual(scheduledPayment.Amount, model.Amount);
            Assert.AreEqual(scheduledPayment.AmountPaid, model.AmountPaid);
            Assert.AreEqual(scheduledPayment.DueDate.ToShortDateString(), model.DueDate);
            Assert.AreEqual(scheduledPayment.Id, model.Id);
            Assert.AreEqual(scheduledPayment.IsPastDue, model.IsPastDue);
            Assert.AreEqual(scheduledPayment.LastPaidDate.Value.ToShortDateString(), model.LastPaidDate);
            Assert.AreEqual(scheduledPayment.PlanId, model.PlanId);
        }

        [TestMethod]
        public void ScheduledPaymentDisplayModel_Valid_Null_LastPaidDate()
        {
            scheduledPayment.LastPaidDate = null;
            model = ScheduledPaymentDisplayModel.Build(scheduledPayment);
            Assert.AreEqual(string.Empty, model.LastPaidDate);
        }
    }
}
