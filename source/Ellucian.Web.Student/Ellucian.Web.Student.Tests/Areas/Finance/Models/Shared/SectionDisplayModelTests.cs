﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class SectionDisplayModelTests
    {
        [TestMethod]
        public void SectionDisplayModel_Constructor()
        {
            var model = new SectionDisplayModel();
            Assert.IsNotNull(model.MeetingInformation);
            Assert.IsFalse(model.MeetingInformation.Any());
            Assert.IsNotNull(model.Faculty);
            Assert.IsFalse(model.Faculty.Any());
            Assert.IsNotNull(model.Classrooms);
            Assert.IsFalse(model.Classrooms.Any());
        }
    }
}
