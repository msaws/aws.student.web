﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.Person;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class PersonSearchResultModelTests
    {
        string name;
        string id;
        string privacyStatusCode;
        PersonSearchResultModel model;

        [TestInitialize]
        public void Initialize()
        {
            name = "Account Holder";
            id = "0012345";
            privacyStatusCode = "A";
        }

        [TestCleanup]
        public void Cleanup()
        {
            name = null;
            id = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonSearchResultModel_NullName()
        {
            model = new PersonSearchResultModel(null, id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonSearchResultModel_NullId()
        {
            model = new PersonSearchResultModel(name, null);
        }

        [TestMethod]
        public void PersonSearchResultModel_Name()
        {
            model = new PersonSearchResultModel(name, id);
            Assert.AreEqual(name, model.Name);
        }

        [TestMethod]
        public void PersonSearchResultModel_Id()
        {
            model = new PersonSearchResultModel(name, id);
            Assert.AreEqual(id, model.Id);
        }

        [TestMethod]
        public void PersonSearchResultModel_PrivacyStatusCode_Default()
        {
            model = new PersonSearchResultModel(name, id);
            Assert.IsNull(model.PrivacyStatusCode);
        }

        [TestMethod]
        public void PersonSearchResultModel_PrivacyStatusCode_Set()
        {
            model = new PersonSearchResultModel(name, id, privacyStatusCode);
            Assert.AreEqual(privacyStatusCode, model.PrivacyStatusCode);
        }

        [TestMethod]
        public void PersonSearchResultModel_MobileHeader_Name_When_PrivacyMessage_Null()
        {
            model = new PersonSearchResultModel(name, id, privacyStatusCode);
            model.PrivacyMessage = null;
            Assert.AreEqual(name, model.MobileHeader);
        }

        [TestMethod]
        public void PersonSearchResultModel_MobileHeader_Name_and_PrivacyMessage_When_HasPrivacyRestriction_True()
        {
            model = new PersonSearchResultModel(name, id, privacyStatusCode);
            model.PrivacyMessage = "This is a private record.";
            Assert.AreEqual(name + Environment.NewLine + model.PrivacyMessage, model.MobileHeader);
        }
    }
}
