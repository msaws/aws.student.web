﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class PaymentPlanDisplayModelTests
    {
        PaymentPlan paymentPlan;
        PaymentPlanDisplayModel model;

        [TestInitialize]
        public void Initialize()
        {
            paymentPlan = new PaymentPlan()
            {
                CurrentAmount = 1000m,
                CurrentStatus = PlanStatusType.Open,
                CurrentStatusDate = DateTime.Today,
                DownPaymentAmount = 25m,
                DownPaymentAmountPaid = 0m,
                DownPaymentDate = DateTime.Today.AddDays(3),
                DownPaymentPercentage = 2.5m,
                FirstDueDate = DateTime.Today.AddDays(3),
                Frequency = PlanFrequency.Weekly,
                GraceDays = 3,
                Id = "1234",
                LateChargeAmount = 5m,
                LateChargePercentage = 1m,
                NumberOfPayments = 5,
                OriginalAmount = 1020m,
                PersonId = "0003315",
                ReceivableTypeCode = "01",
                SetupAmount = 10m,
                SetupPercentage = 1m,
                TemplateId = "DEFAULT",
                TermId = "2017/FA",
                TotalSetupChargeAmount = 20m,
                PlanCharges = new List<PlanCharge>()
                {
                    new PlanCharge() {
                        Amount = 600m,
                        Id = "1234*123",
                        IsAutomaticallyModifiable = true,
                        IsSetupCharge = false,
                        PlanId = "1234",
                        Charge = new Charge() {
                            Amount = 600m,
                            BaseAmount = 600m,
                            Code = "ACTFE",
                            Description = new List<string>() { "Student Activities Fee" },
                            InvoiceId = "135",
                            TaxAmount = 0m,
                            PaymentPlanIds = new List<string>() { "1234" },
                            Id = "123"
                        }
                    },
                    new PlanCharge() {
                        Amount = 400m,
                        Id = "1234*456",
                        IsAutomaticallyModifiable = true,
                        IsSetupCharge = false,
                        PlanId = "1234",
                        Charge = new Charge() {
                            Amount = 400m,
                            BaseAmount = 375m,
                            Code = "HLTFE",
                            Description = new List<string>() { "Student Health Fee" },
                            InvoiceId = "136",
                            TaxAmount = 25m,
                            PaymentPlanIds = new List<string>() { "1234" },
                            Id = "456"
                        }
                    },
                    new PlanCharge() {
                        Amount = 600m,
                        Id = "1234*789",
                        IsAutomaticallyModifiable = true,
                        IsSetupCharge = true,
                        PlanId = "1234",
                        Charge = new Charge() {
                            Amount = 20m,
                            BaseAmount = 20m,
                            Code = "SETUP",
                            Description = new List<string>() { "Setup Fee" },
                            InvoiceId = "136",
                            TaxAmount = 0m,
                            PaymentPlanIds = new List<string>() { "1234" },
                            Id = "789"
                        }
                    }
                },
                ScheduledPayments = new List<ScheduledPayment>() {
                    new ScheduledPayment() {
                        Amount = 220m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(3),
                        Id = "2345",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(13),
                        Id = "2346",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(23),
                        Id = "2347",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(33),
                        Id = "2348",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    },
                    new ScheduledPayment() {
                        Amount = 200m,
                        AmountPaid = 0m,
                        DueDate = DateTime.Today.AddDays(43),
                        Id = "2349",
                        IsPastDue = false,
                        LastPaidDate = null,
                        PlanId = "1234"
                    }
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            paymentPlan = null;
            model = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentPlanDisplayModel_Null_PaymentPlan()
        {
            model = PaymentPlanDisplayModel.Build(null);
        }

        [TestMethod]
        public void PaymentPlanDisplayModel_Valid()
        {
            model = PaymentPlanDisplayModel.Build(paymentPlan);
            Assert.AreEqual(paymentPlan.CurrentAmount, model.CurrentAmount);
            Assert.AreEqual(paymentPlan.CurrentStatus.ToString(), model.CurrentStatus);
            Assert.AreEqual(paymentPlan.CurrentStatusDate.ToShortDateString(), model.CurrentStatusDate);
            Assert.AreEqual(paymentPlan.DownPaymentAmount, model.DownPaymentAmount);
            Assert.AreEqual(paymentPlan.DownPaymentAmountPaid, model.DownPaymentAmountPaid);
            Assert.AreEqual(paymentPlan.DownPaymentDate.Value.ToShortDateString(), model.DownPaymentDate);
            Assert.AreEqual(paymentPlan.DownPaymentPercentage, model.DownPaymentPercentage);
            Assert.AreEqual(paymentPlan.FirstDueDate.ToShortDateString(), model.FirstDueDate);
            Assert.AreEqual(paymentPlan.Frequency.ToString(), model.Frequency);
            Assert.AreEqual(paymentPlan.GraceDays, model.GraceDays);
            Assert.AreEqual(paymentPlan.Id, model.Id);
            Assert.AreEqual(paymentPlan.LateChargeAmount, model.LateChargeAmount);
            Assert.AreEqual(paymentPlan.LateChargePercentage, model.LateChargePercentage);
            Assert.AreEqual(paymentPlan.NumberOfPayments, model.NumberOfPayments);
            Assert.AreEqual(paymentPlan.OriginalAmount, model.OriginalAmount);
            Assert.IsNotNull(model.PaymentPlan);
            Assert.AreEqual(paymentPlan.PersonId, model.PersonId);
            Assert.IsNull(model.PlanDisplayName);
            Assert.AreEqual(paymentPlan.CurrentAmount + paymentPlan.TotalSetupChargeAmount, model.PlanTotalAmount);
            Assert.AreEqual(paymentPlan.ReceivableTypeCode, model.ReceivableTypeCode);
            Assert.AreEqual(paymentPlan.SetupAmount, model.SetupAmount);
            Assert.AreEqual(paymentPlan.SetupPercentage, model.SetupPercentage);
            Assert.AreEqual(paymentPlan.TotalSetupChargeAmount, model.TotalSetupChargeAmount);
            Assert.AreEqual(paymentPlan.PlanCharges.Count(), model.PlanCharges.Count);
            Assert.AreEqual(paymentPlan.ScheduledPayments.Count(), model.ScheduledPayments.Count);
        }

        [TestMethod]
        public void PaymentPlanDisplayModel_Valid_Null_PlanCharges()
        {
            paymentPlan.PlanCharges = null;
            model = PaymentPlanDisplayModel.Build(paymentPlan);
            Assert.IsFalse(model.PlanCharges.Any());
        }

        [TestMethod]
        public void PaymentPlanDisplayModel_Valid_Null_ScheduledPayments()
        {
            paymentPlan.ScheduledPayments = null;
            model = PaymentPlanDisplayModel.Build(paymentPlan);
            Assert.IsFalse(model.ScheduledPayments.Any());
        }

        [TestMethod]
        public void PaymentPlanDisplayModel_Valid_Null_DownPaymentDate()
        {
            paymentPlan.DownPaymentDate = null;
            model = PaymentPlanDisplayModel.Build(paymentPlan);
            Assert.AreEqual(string.Empty, model.DownPaymentDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentPlanDisplayModel_Null_PlanCharge()
        {
            paymentPlan.PlanCharges = new List<PlanCharge>() { null };
            model = PaymentPlanDisplayModel.Build(paymentPlan);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PaymentPlanDisplayModel_Null_ScheduledPayment()
        {
            paymentPlan.ScheduledPayments = new List<ScheduledPayment>() { null };
            model = PaymentPlanDisplayModel.Build(paymentPlan);
        }

        [TestMethod]
        public void PaymentPlanDisplayModel_DownPaymentMessage_Null_DownPaymentDate()
        {
            paymentPlan.DownPaymentDate = null;
            paymentPlan.DownPaymentAmount = 100m;
            model = PaymentPlanDisplayModel.Build(paymentPlan);
            Assert.IsNull(model.DownPaymentMessage);
        }

        [TestMethod]
        public void PaymentPlanDisplayModel_DownPaymentMessage_Zero_DownPaymentAmount()
        {
            paymentPlan.DownPaymentDate = DateTime.Today.AddDays(3);
            paymentPlan.DownPaymentAmount = 0m;
            model = PaymentPlanDisplayModel.Build(paymentPlan);
            Assert.IsNull(model.DownPaymentMessage);
        }
    }
}
