﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Student.Models.Person;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class PersonSearchModelTests
    {
        string searchPrompt;
        string beforeSearchText;
        string searchResultsViewName;
        PersonSearchModel model;

        [TestInitialize]
        public void Initialize()
        {
            searchPrompt = "Who would you like to work with?";
            beforeSearchText = "Enter a name or ID and press Enter to begin.";
            searchResultsViewName = "_PersonSearchResults";
        }

        [TestCleanup]
        public void Cleanup()
        {
            searchPrompt = null;
            beforeSearchText = null;
            searchResultsViewName = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonSearchModel_NullSearchPrompt()
        {
            model = new PersonSearchModel(null, beforeSearchText, searchResultsViewName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonSearchModel_NullBeforeSearchText()
        {
            model = new PersonSearchModel(searchPrompt, null, searchResultsViewName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersonSearchModel_NullSearchResultsViewName()
        {
            model = new PersonSearchModel(searchPrompt, beforeSearchText, null);
        }

        [TestMethod]
        public void PersonSearchModel_SearchPrompt()
        {
            model = new PersonSearchModel(searchPrompt, beforeSearchText, searchResultsViewName);
            Assert.AreEqual(searchPrompt, model.SearchPrompt);
        }

        [TestMethod]
        public void PersonSearchModel_BeforeSearchText()
        {
            model = new PersonSearchModel(searchPrompt, beforeSearchText, searchResultsViewName);
            Assert.AreEqual(beforeSearchText, model.BeforeSearchText);
        }

        [TestMethod]
        public void PersonSearchModel_SearchResultsViewName()
        {
            model = new PersonSearchModel(searchPrompt, beforeSearchText, searchResultsViewName);
            Assert.AreEqual(searchResultsViewName, model.SearchResultsViewName);
        }

        [TestMethod]
        public void PersonSearchModel_DynamicBackLinkDefault_Default()
        {
            model = new PersonSearchModel(searchPrompt, beforeSearchText, searchResultsViewName);
            Assert.AreEqual(null, model.DynamicBackLinkDefault);
        }

        [TestMethod]
        public void PersonSearchModel_BackLinkExclusions_Default()
        {
            model = new PersonSearchModel(searchPrompt, beforeSearchText, searchResultsViewName);
            Assert.AreEqual(null, model.BackLinkExclusions);
        }
    }
}
