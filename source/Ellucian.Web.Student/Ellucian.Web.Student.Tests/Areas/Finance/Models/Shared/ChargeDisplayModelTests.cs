﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class ChargeDisplayModelTests
    {
        Charge charge;
        ChargeDisplayModel model;

        [TestInitialize]
        public void Initialize()
        {
            charge = new Charge()
            {
                Amount = 600m,
                BaseAmount = 600m,
                Code = "ACTFE",
                Description = new List<string>() { "Student Activities Fee" },
                InvoiceId = "135",
                TaxAmount = 0m,
                PaymentPlanIds = new List<string>() { "1234" },
                Id = "123"
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            charge = null;
            model = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ChargeDisplayModel_Null_Charge()
        {
            model = ChargeDisplayModel.Build(null);
        }

        [TestMethod]
        public void ChargeDisplayModel_Valid()
        {
            model = ChargeDisplayModel.Build(charge);
            Assert.AreEqual(charge.Amount, model.Amount);
            Assert.AreEqual(charge.Id, model.Id);
            Assert.AreEqual(charge.BaseAmount, model.BaseAmount);
            Assert.AreEqual(charge.Code, model.Code);
            Assert.AreEqual(charge.Description, model.Description);
            Assert.AreEqual(charge.Id, model.Id);
            Assert.AreEqual(charge.InvoiceId, model.InvoiceId);
            Assert.AreEqual(charge.TaxAmount, model.TaxAmount);
            Assert.AreEqual(charge.PaymentPlanIds.Count, model.PaymentPlanIds.Count);
        }

        [TestMethod]
        public void ChargeDisplayModel_Duplicate_PaymentPlanId()
        {
            charge.PaymentPlanIds.Add(charge.PaymentPlanIds.First());
            model = ChargeDisplayModel.Build(charge);
            Assert.AreEqual(charge.PaymentPlanIds.Count-1, model.PaymentPlanIds.Count);
        }

        [TestMethod]
        public void ChargeDisplayModel_Null_PaymentPlanIds()
        {
            charge.PaymentPlanIds = null;
            model = ChargeDisplayModel.Build(charge);
            Assert.AreEqual(charge.Amount, model.Amount);
            Assert.AreEqual(charge.Id, model.Id);
            Assert.AreEqual(charge.BaseAmount, model.BaseAmount);
            Assert.AreEqual(charge.Code, model.Code);
            Assert.AreEqual(charge.Description, model.Description);
            Assert.AreEqual(charge.Id, model.Id);
            Assert.AreEqual(charge.InvoiceId, model.InvoiceId);
            Assert.AreEqual(charge.TaxAmount, model.TaxAmount);
            Assert.IsFalse(model.PaymentPlanIds.Any());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ChargeDisplayModel_Null_PaymentPlanId()
        {
            charge.PaymentPlanIds = new List<string>() { null };
            model = ChargeDisplayModel.Build(charge);
        }


    }
}
