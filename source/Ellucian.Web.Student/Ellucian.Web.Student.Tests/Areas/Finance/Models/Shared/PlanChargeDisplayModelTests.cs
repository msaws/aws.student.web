﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Student.Areas.Finance.Models.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models.Shared
{
    [TestClass]
    public class PlanChargeDisplayModelTests
    {
        PlanCharge planCharge;
        PlanChargeDisplayModel model;

        [TestInitialize]
        public void Initialize()
        {
            planCharge = new PlanCharge()
            {
                Amount = 600m,
                Id = "1234*123",
                IsAutomaticallyModifiable = true,
                IsSetupCharge = false,
                PlanId = "1234",
                Charge = new Charge()
                {
                    Amount = 600m,
                    BaseAmount = 600m,
                    Code = "ACTFE",
                    Description = new List<string>() { "Student Activities Fee" },
                    InvoiceId = "135",
                    TaxAmount = 0m,
                    PaymentPlanIds = new List<string>() { "1234" },
                    Id = "123"
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            planCharge = null;
            model = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PlanChargeDisplayModel_Null_PlanCharge()
        {
            model = PlanChargeDisplayModel.Build(null);
        }

        [TestMethod]
        public void PlanChargeDisplayModel_Valid()
        {
            model = PlanChargeDisplayModel.Build(planCharge);
            Assert.AreEqual(planCharge.Amount, model.Amount);
            Assert.AreEqual(planCharge.Id, model.Id);
            Assert.AreEqual(planCharge.IsAutomaticallyModifiable, model.IsAutomaticallyModifiable);
            Assert.AreEqual(planCharge.IsSetupCharge, model.IsSetupCharge);
            Assert.AreEqual(planCharge.PlanId, model.PlanId);
            Assert.IsNotNull(planCharge.Charge);
        }
    }
}
