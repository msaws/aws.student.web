﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityFinancialAidItem
    {
        private const string termId = "2014/FA";
        private const decimal awdAmt = 500m;
        private const string awdDesc = "Award";
        private const string comments = "Comments";
        private const decimal ineligible = 300m;
        private const decimal loanFee = 50m;
        private const decimal otherTerm = 100m;
        private const string periodAwd = "CUR";
        public static ActivityFinancialAidItem Build()
        {
            var model = new ActivityFinancialAidItem()
            {
                AwardAmount = awdAmt,
                AwardDescription = awdDesc,
                AwardTerms = new System.Collections.Generic.List<ActivityFinancialAidTerm>()
                {
                    BuildActivityFinancialAidTerm.Build()
                },
                Comments = comments,
                IneligibleAmount = ineligible,
                LoanFee = loanFee,
                OtherTermAmount = otherTerm,
                PeriodAward = periodAwd,
            };

            return model;
        }
    }
}
