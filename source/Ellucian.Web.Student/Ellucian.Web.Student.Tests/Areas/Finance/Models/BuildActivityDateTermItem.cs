﻿using System;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityDateTermItem
    {
        private static DateTime date = new DateTime(2014, 9, 15, 0, 0, 0);

        public static ActivityDateTermItem Build()
        {
            var ati = BuildActivityTermItem.Build();

            var model = new ActivityDateTermItem()
            {
                Id = ati.Id,
                Description = ati.Description,
                Date = date,
                TermId = ati.TermId,
                Amount = ati.Amount
            };

            return model;
        }
    }
}
