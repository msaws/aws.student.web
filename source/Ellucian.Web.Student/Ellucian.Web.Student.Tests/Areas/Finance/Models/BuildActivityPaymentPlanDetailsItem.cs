﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Models
{
    public static class BuildActivityPaymentPlanDetailsItem
    {
        private const string id = "12345";
        private const string description = "Technology Fee";
        private const string termId = "2014/FA";
        private const decimal amount = 123.45m;
        private const decimal bal = 223.45m;
        private const decimal orig = 500m;
        private const string type = "Student Receivables";
        private const string approval = "123";

        public static ActivityPaymentPlanDetailsItem Build()
        {
            var model = new ActivityPaymentPlanDetailsItem()
            {
                Id = id,
                Description = description,
                TermId = termId,
                Amount = amount,
                CurrentBalance = bal,
                OriginalAmount = orig,
                Type = type,
                PaymentPlanApproval = approval,
                PaymentPlanSchedules = new List<ActivityPaymentPlanScheduleItem>()
                {
                    BuildActivityPaymentPlanScheduleItem.Build(DateTime.Today.AddDays(-7)),
                    BuildActivityPaymentPlanScheduleItem.Build(DateTime.Today),
                    BuildActivityPaymentPlanScheduleItem.Build(DateTime.Today.AddDays(7)),
                    BuildActivityPaymentPlanScheduleItem.Build(DateTime.Today.AddDays(14)),
                    BuildActivityPaymentPlanScheduleItem.Build(DateTime.Today.AddDays(21)),
                }
            };

            return model;
        }
    }
}
