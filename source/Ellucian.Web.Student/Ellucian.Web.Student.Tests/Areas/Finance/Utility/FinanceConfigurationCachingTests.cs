﻿// RTM FIX
//using System;
//using System.Text;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Ellucian.Web.Student.Utility;
//using Ellucian.Colleague.Api.Client;
//using Moq;
//using Ellucian.Utility.Logging;
//using System.Net;
//using System.Net.Http;
//using Ellucian.Web.Student.Configuration;
//using Newtonsoft.Json;
//using Ellucian.Colleague.Dtos.Finance.Configuration;
//using Ellucian.Web.Student.Areas.Finance.Utility;

//namespace Ellucian.Web.Student.Tests.Utility
//{
//    [TestClass]
//    public class FinanceConfigurationCachingTests
//    {
//        private const string _serviceUrl = "http://fake.url";
//        private ILogger _logger = new Mock<ILogger>().Object;
//        private string _configurationJsonResponse;

//        [TestInitialize]
//        public void Initialize()
//        {
//            var configuration = new FinanceConfiguration();
//            configuration.ActivityDisplay = ActivityDisplay.DisplayByTerm;
//            configuration.ECommercePaymentsAllowed = true;

//            _configurationJsonResponse = JsonConvert.SerializeObject(configuration);

//            FinanceConfigurationCaching.ClearConfiguration();
//        }

//        [TestMethod]
//        public void GetConfiguration_NoCachedConfiguration()
//        {
//            // Arrange
//            var response = new HttpResponseMessage(HttpStatusCode.OK);
//            response.Content = new StringContent(_configurationJsonResponse, Encoding.UTF8, "application/json");
//            var mockHandler = new MockHandler();
//            mockHandler.Responses.Enqueue(response);

//            var testHttpClient = new HttpClient(mockHandler);
//            testHttpClient.BaseAddress = new Uri(_serviceUrl);

//            var client = new ColleagueApiClient(testHttpClient, _logger);

//            // Act
//            var configuration = FinanceConfigurationCaching.GetConfiguration(client);

//            // Assert
//            Assert.AreEqual(ActivityDisplay.DisplayByTerm, configuration.ActivityDisplay);
//            Assert.AreEqual(true, configuration.ECommercePaymentsAllowed);
//        }

//        [TestMethod]
//        public void GetConfiguration_CachedConfiguration()
//        {
//            // Arrange
//            var response = new HttpResponseMessage(HttpStatusCode.OK);
//            response.Content = new StringContent(_configurationJsonResponse, Encoding.UTF8, "application/json");
//            var mockHandler = new MockHandler();
//            mockHandler.Responses.Enqueue(response);

//            var testHttpClient = new HttpClient(mockHandler);
//            testHttpClient.BaseAddress = new Uri(_serviceUrl);

//            var client = new ColleagueApiClient(testHttpClient, _logger);

//            // Act
//            var configuration = FinanceConfigurationCaching.GetConfiguration(client);

//            // If the configuration is cached, then a null ColleagueApiClient will not matter
//            configuration = FinanceConfigurationCaching.GetConfiguration(null);

//            // Assert
//            Assert.AreEqual(ActivityDisplay.DisplayByTerm, configuration.ActivityDisplay);
//            Assert.AreEqual(true, configuration.ECommercePaymentsAllowed);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(NullReferenceException))]
//        public void GetConfiguration_NullServiceClient()
//        {
//            var configuration = FinanceConfigurationCaching.GetConfiguration(null);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(AggregateException))]
//        public void GetConfiguration_ServiceClientException()
//        {
//            // Arrange
//            var loggerMock = new Mock<ILogger>();

//            var response = new HttpResponseMessage(HttpStatusCode.NotFound);
//            response.Content = new StringContent(string.Empty, Encoding.UTF8, "application/json");
//            var mockHandler = new MockHandler();
//            mockHandler.Responses.Enqueue(response);

//            var testHttpClient = new HttpClient(mockHandler);
//            testHttpClient.BaseAddress = new Uri(_serviceUrl);

//            var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);

//            // Act
//            var configuration = FinanceConfigurationCaching.GetConfiguration(client);
//        }
//    }
//}
