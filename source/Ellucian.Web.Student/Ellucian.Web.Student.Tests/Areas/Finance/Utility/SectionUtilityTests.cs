﻿using System;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Finance.Utility;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Utility
{
    [TestClass]
    public class SectionUtilityTests
    {
        static CultureInfo currentCulture = null;
        private string ToBeDeterminedText;
        private string SectionDatesDelimiter;
        private string CourseDelimiter;

        SectionMeeting2 meeting1, meeting2;
        List<DayOfWeek> days1, days2;
        DateTime startDate1, startDate2, endDate1, endDate2;
        DateTimeOffset startTime1, startTime2, endTime1, endTime2;
        string instMethod1, instMethod2, room1, room2, freq, meeting1Out, meeting2Out;
        List<SectionMeeting2> meetings;
        TimeSpan offset = new TimeSpan(-5, 0, 0);
        TimeSpan offsetDST = new TimeSpan(-4, 0, 0);

        //Mock<HttpContext> resourceMock;

        [TestInitialize]
        public void Initialize()
        {
            if (currentCulture == null)
            {
                currentCulture = CultureInfo.CurrentCulture.Clone() as CultureInfo;
                // Force the date and time output formats so the expected results are accurate
                currentCulture.DateTimeFormat.ShortDatePattern = "M/d/yyyy";
                currentCulture.DateTimeFormat.ShortTimePattern = "h:mm tt";
                currentCulture.DateTimeFormat.AMDesignator = "AM";
                currentCulture.DateTimeFormat.PMDesignator = "PM";
            }

            //resourceMock = new Mock<HttpContext>();
            //resourceMock.Setup<string>(x => x.GetGlobalResourceObject(It.IsAny<string>(), It.IsAny<string>()))
            //    .Returns("foo");
                //.Returns<string, string>((file, value) =>
                //{
                //    switch (value)
                //    {
                //        case "ToBeDeterminedText": return "TBA";
                //        case "SectionDatesDelimiter": return "-";
                //        case "CourseDelimiter": return "-";
                //        case "MondayAbbreviation": return "M";
                //        case "TuesdayAbbreviation": return "Tu";
                //        case "WednesdayAbbreviation": return "W";
                //        case "ThursdayAbbreviation": return "Th";
                //        case "FridayAbbreviation": return "F";
                //        case "SaturdayAbbreviation": return "Sa";
                //        case "SundayAbbreviation": return "Su";
                //    }
                //});

            ToBeDeterminedText = GlobalResources.GetString(GlobalResourceFiles.SharedConstants, "ToBeDeterminedText");
            SectionDatesDelimiter = GlobalResources.GetString(GlobalResourceFiles.SharedConstants, "SectionDatesDelimiter");
            CourseDelimiter = GlobalResources.GetString(GlobalResourceFiles.SharedConstants, "CourseDelimiter");

            days1 = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday };
            days2 = new List<DayOfWeek>() { DayOfWeek.Tuesday, DayOfWeek.Thursday };
            startDate1 = new DateTime(2013, 8, 28, 9, 0, 0);
            startTime1 = new DateTimeOffset(startDate1, offsetDST);
            endDate1 = new DateTime(2013, 12, 15, 9, 50, 0);
            endTime1 = new DateTimeOffset(endDate1, offset);
            startDate2 = new DateTime(2013, 9, 5, 18, 30, 0);
            startTime2 = new DateTimeOffset(startDate2, offsetDST);
            endDate2 = new DateTime(2013, 11, 20, 21, 0, 0);
            endTime2 = new DateTimeOffset(endDate2, offset);
            meeting1Out = "MWF 9:00-9:50 AM";
            meeting2Out = "TuTh 6:30-9:00 PM";

            instMethod1 = "LEC";
            instMethod2 = "LAB";
            room1 = "EIN*301";
            room2 = "EIN*315";
            freq = "W";

            meeting1 = new SectionMeeting2() 
                { 
                    Days = days1, 
                    StartDate = startDate1, 
                    EndDate = endDate1, 
                    StartTime = startTime1,
                    EndTime = endTime1,
                    Frequency = freq,
                    InstructionalMethodCode = instMethod1,
                    Room = room1 
                };
            meeting2 = new SectionMeeting2()
            {
                Days = days2,
                StartDate = startDate2,
                EndDate = endDate2,
                StartTime = startTime2,
                EndTime = endTime2,
                Frequency = freq,
                InstructionalMethodCode = instMethod2,
                Room = room2
            };

            meetings = new List<SectionMeeting2>() { meeting1, meeting2 };
        
        }

        [TestMethod]
        public void SectionUtility_TBA()
        {
            string expected = ToBeDeterminedText;
            string result = SectionUtility.TBA;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_SectionDatesDelimiter()
        {
            string expected = SectionDatesDelimiter;
            string result = SectionUtility.SectionDatesDelimiter;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_CourseDelimiter()
        {
            string expected = CourseDelimiter;
            string result = SectionUtility.CourseDelimiter;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetSectionDates_NullEndDate()
        {
            var section = new Section3();
            section.StartDate = new DateTime(2013, 1, 14);
            section.EndDate = null;
            string expected = section.StartDate.ToShortDateString() + SectionUtility.SectionDatesDelimiter + SectionUtility.TBA;
            string result = SectionUtility.GetSectionDates(section);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetSectionDates_ValidEndDate()
        {
            var section = new Section3();
            section.StartDate = new DateTime(2013, 1, 14);
            section.EndDate = new DateTime(2013, 5, 1);
            string expected = section.StartDate.ToShortDateString() + SectionUtility.SectionDatesDelimiter + section.EndDate.Value.ToShortDateString();
            string result = SectionUtility.GetSectionDates(section);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_NullStartNullEnd()
        {
            string startTime = null;
            string endTime = null;
            string expected = String.Empty;
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_EmptyStartValidEndAm()
        {
            string startTime = null;
            string endTime = "08:50:00";
            string expected = SectionUtility.TBA + "- 8:50 AM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_EmptyStartValidEndPm()
        {
            string startTime = null;
            string endTime = "18:50:00";
            string expected = SectionUtility.TBA + "- 6:50 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_ValidStartAmEmptyEnd()
        {
            string startTime = "08:50:00";
            string endTime = null;
            string expected = "8:50 AM-" + SectionUtility.TBA;
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_ValidStartPmEmptyEnd()
        {
            string startTime = "18:50:00";
            string endTime = null;
            string expected = "6:50 PM-" + SectionUtility.TBA;
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_AmStartAmEnd()
        {
            string startTime = "10:00:00";
            string endTime = "10:50:00";
            string expected = "10:00-10:50 AM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_AmStartAmEndMilitary()
        {
            string startTime = "10:00:00";
            string endTime = "10:50:00";
            string expected = "10:00-10:50 AM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_AmStartPmEnd()
        {
            string startTime = "11:30:00";
            string endTime = "13:00:00";
            string expected = "11:30 AM-1:00 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_AmStartPmEndMilitary()
        {
            string startTime = "11:30:00";
            string endTime = "13:00:00";
            string expected = "11:30 AM-1:00 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_PmStartPmEnd()
        {
            string startTime = "13:00:00";
            string endTime = "14:30:00";
            string expected = "1:00-2:30 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Strings_PmStartPmEndMilitary()
        {
            string startTime = "13:00:00";
            string endTime = "14:30:00";
            string expected = "1:00-2:30 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_NullSectionMeetings()
        {
            string expected = ToBeDeterminedText;
            var result = SectionUtility.GetMeetingTimes(null);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_ValidSectionMeetings()
        {
            var meeting = new SectionMeeting2();
            meeting.StartTime = new DateTimeOffset(2013, 1, 1, 8, 0, 0, offset);
            meeting.EndTime = new DateTimeOffset(2013, 1, 1, 8, 50, 0, offset);
            string expected = "8:00-8:50 AM";
            var result = SectionUtility.GetMeetingTimes(meeting);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_NullStartNullEnd()
        {
            DateTimeOffset? startTime = null;
            DateTimeOffset? endTime = null;
            string expected = String.Empty;
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_EmptyStartValidEndAm()
        {
            DateTimeOffset? startTime = null;
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 8, 50, 0, offset);
            string expected = SectionUtility.TBA + "- 8:50 AM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_EmptyStartValidEndPm()
        {
            DateTimeOffset? startTime = null;
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 18, 50, 0, offset);
            string expected = SectionUtility.TBA + "- 6:50 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_ValidStartAmEmptyEnd()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 8, 50, 0, offset);
            DateTimeOffset? endTime = null;
            string expected = "8:50 AM-" + SectionUtility.TBA;
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_ValidStartPmEmptyEnd()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 18, 50, 0, offset);
            DateTimeOffset? endTime = null;
            string expected = "6:50 PM-" + SectionUtility.TBA;
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_AmStartAmEnd()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 10, 0, 0, offset);
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 10, 50, 0, offset);
            string expected = "10:00-10:50 AM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_AmStartAmEndMilitary()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 10, 0, 0, offset);
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 10, 50, 0, offset);
            string expected = "10:00-10:50 AM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_AmStartPmEnd()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 11, 30, 0, offset);
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 13, 0, 0, offset);
            string expected = "11:30 AM-1:00 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_AmStartPmEndMilitary()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 11, 30, 0, offset);
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 13, 0, 0, offset);
            string expected = "11:30 AM-1:00 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_PmStartPmEnd()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 13, 0, 0, offset);
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 14, 30, 0, offset);
            string expected = "1:00-2:30 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingTimes_Times_PmStartPmEndMilitary()
        {
            DateTimeOffset? startTime = new DateTimeOffset(1, 1, 1, 13, 00, 0, offset);
            DateTimeOffset? endTime = new DateTimeOffset(1, 1, 1, 14, 30, 0, offset);
            string expected = "1:00-2:30 PM";
            string result = SectionUtility.GetMeetingTimes(startTime, endTime);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingInformation_NullMeetings()
        {
            string expected = ToBeDeterminedText;
            var result = SectionUtility.GetMeetingInformation(null);
            Assert.AreEqual(expected, result[0]);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingInformation_NoMeetings()
        {
            string expected = ToBeDeterminedText;
            var result = SectionUtility.GetMeetingInformation(new List<SectionMeeting2>());
            Assert.AreEqual(expected, result[0]);
        }

        [TestMethod]
        public void SectionUtility_GetMeetingInformation_Meetings()
        {
            var result = SectionUtility.GetMeetingInformation(meetings);
            Assert.AreEqual(meetings.Count, result.Count);
            // Can't compare days because of issue getting resource values, so
            // just compare the time portions - the part after the space
            //Assert.AreEqual(meeting1Out, result[0]);
            //Assert.AreEqual(meeting2Out, result[1]);
            Assert.AreEqual(meeting1Out.Split(' ')[1], result[0].Split(' ')[1]);
            Assert.AreEqual(meeting2Out.Split(' ')[1], result[1].Split(' ')[1]);
        }
    }
}
