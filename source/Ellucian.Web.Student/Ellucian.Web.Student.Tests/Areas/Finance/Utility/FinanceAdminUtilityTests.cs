﻿//// Copyright 2016 Ellucian Company L.P. and its affiliates.
//using Ellucian.Web.Student.Areas.Finance.Models.Shared;
//using Ellucian.Web.Student.Areas.Finance.Utility;
//using Microsoft.QualityTools.Testing.Fakes;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
//using System.Linq;

//namespace Ellucian.Web.Student.Tests.Areas.Finance.Utility
//{
//    [TestClass]
//    public class FinanceAdminUtilityTests
//    {
//        PersonSearchModel model;
//        string controller = "AccountActivity";

//        [TestMethod]
//        [ExpectedException(typeof(ArgumentNullException))]
//        public void FinanceAdminUtility_NullControllerName()
//        {
//            model = FinanceAdminUtility.CreatePersonSearchModel(null);
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SearchPrompt()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };
//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("PersonSearchSearchPrompt", model.SearchPrompt);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_BeforeSearchText()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };
//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("PersonSearchBeforeSearchText", model.BeforeSearchText);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SearchResultsViewName()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("_PersonSearchResults", model.SearchResultsViewName);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_DynamicBackLinkDefault()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("SearchForAccountholdersAsync", model.BackLinkExclusions.First().Action);
//                Assert.AreEqual("Admin", model.BackLinkExclusions.First().Controller);
//                Assert.AreEqual("Finance", model.BackLinkExclusions.First().Area);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_BackLinkExclusions()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };
//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual(1, model.BackLinkExclusions.Count());
//                Assert.AreEqual("SearchForAccountholdersAsync", model.BackLinkExclusions.First().Action);
//                Assert.AreEqual("Admin", model.BackLinkExclusions.First().Controller);
//                Assert.AreEqual("Finance", model.BackLinkExclusions.First().Area);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_ErrorMessage()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.IsNull(model.ErrorMessage);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_ErrorOccurred()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.IsFalse(model.ErrorOccurred);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_PlaceholderText()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("PersonSearchPlaceholderText", model.PlaceholderText);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_PointOfOriginControllerName()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual(controller, model.PointOfOriginControllerName);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_Search()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("SearchForAccountholdersAsync", model.Search.Action);
//                Assert.AreEqual("Admin", model.Search.Controller);
//                Assert.AreEqual("Finance", model.Search.Area);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SearchFieldLabel()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("PersonSearchSearchFieldLabel", model.SearchFieldLabel);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SearchString()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.IsNull(model.SearchString);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SearchResults()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.IsNull(model.SearchResults);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SearchSubmitLabel()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("PersonSearchSearchSubmitLabel", model.SearchSubmitLabel);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_Select()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("Admin", model.Select.Action);
//                Assert.AreEqual(controller, model.Select.Controller);
//                Assert.AreEqual("Finance", model.Select.Area);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SpinnerText()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("PersonSearchSpinnerText", model.SpinnerText);
//            }
//        }

//        [TestMethod]
//        public void FinanceAdminUtility_SpinnerAlternateText()
//        {
//            using (ShimsContext.Create())
//            {
//                Ellucian.Web.Student.Utility.Fakes.ShimGlobalResources.GetStringStringString =
//                (string resourceFile, string resourceKey) =>
//                {
//                    return resourceKey;
//                };

//                model = FinanceAdminUtility.CreatePersonSearchModel(controller);
//                Assert.AreEqual("PersonSearchSpinnerAlternateText", model.SpinnerAlternateText);
//            }
//        }
//    }
//}
