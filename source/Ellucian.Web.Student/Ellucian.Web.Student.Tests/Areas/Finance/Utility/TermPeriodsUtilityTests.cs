﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Finance.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Finance.Utility
{
    [TestClass]
    public class TermPeriodsUtilityTests
    {
        private static Term term1, term2, term3;
        private static FinancialPeriod past, current, future;
        private static IEnumerable<Term> terms;
        private static IEnumerable<FinancialPeriod> periods;
        private static TermPeriodsUtility util;
        private DateTime? date1, date2;
        private PeriodType? period;

        #region Initialization

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [ClassInitialize()]
        public static void ClassInitialize(TestContext testContext)
        {
            term1 = new Term()
            {
                Code = "2012/SP",
                Description = "Spring 2012",
                StartDate = new DateTime(2012, 01, 14),
                EndDate = new DateTime(2012, 05, 14),
                FinancialPeriod = PeriodType.Past,
                ReportingYear = 2011,
                Sequence = 5,
                ReportingTerm = "2012SPR"
            };
            term2 = new Term()
            {
                Code = "2012/FA",
                Description = "Fall 2012",
                StartDate = new DateTime(2012, 08, 25),
                EndDate = new DateTime(2012, 12, 14),
                FinancialPeriod = PeriodType.Current,
                ReportingYear = 2012,
                Sequence = 1,
                ReportingTerm = "2012FAR"
            };
            term3 = new Term()
            {
                Code = "2013/SP",
                Description = "Spring 2013",
                StartDate = new DateTime(2013, 01, 15),
                EndDate = new DateTime(2013, 05, 15),
                FinancialPeriod = PeriodType.Future,
                ReportingYear = 2012,
                Sequence = 5,
                ReportingTerm = "2013/SP"
            };

            past = new FinancialPeriod()
            {
                Type = PeriodType.Past,
                Start = null,
                End = new DateTime(2012, 08, 24)
            };
            current = new FinancialPeriod()
            {
                Type = PeriodType.Current,
                Start = new DateTime(2012, 08, 25),
                End = new DateTime(2012, 12, 14)
            };
            future = new FinancialPeriod()
            {
                Type = PeriodType.Future,
                Start = new DateTime(2012, 12, 15),
                End = null
            };
            terms = new List<Term> { term1, term2, term3 };
            periods = new List<FinancialPeriod> { past, current, future };
            util = new TermPeriodsUtility(terms, periods);
        }
        
        #endregion

        #region Constructor tests

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void TermPeriodsUtility_Constructor_NullTerms()
        {
            var result = new TermPeriodsUtility(null, null);
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void TermPeriodsUtility_Constructor_NoTerms()
        {
            var source = new List<Term>();
            var result = new TermPeriodsUtility(source, null);
        }

        [TestMethod]
        public void TermPeriodsUtility_ConstructorNullPeriods_Valid()
        {
            var source = new List<Term>{term1};
            var result = new TermPeriodsUtility(source, null);
            Assert.AreNotEqual(result, null);
        }

        [TestMethod]
        public void TermPeriodsUtility_ConstructorNoPeriods_Valid()
        {
            var source = new List<Term> { term1 };
            var result = new TermPeriodsUtility(source, new List<FinancialPeriod>());
            Assert.AreNotEqual(result, null);
        }

        [TestMethod]
        public void TermPeriodsUtility_ConstructorPeriods_Valid()
        {
            var result = new TermPeriodsUtility(terms, periods);
            Assert.AreNotEqual(result, null);
        }

        #endregion

        #region GetTerm tests

        [TestMethod]
        public void TermPeriodsUtility_GetTerm_ValidTerm()
        {
            string code = term2.Code;
            var term = util.GetTerm(code);
            Assert.AreEqual(term2, term);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTerm_InvalidTerm()
        {
            string code = "XYZ";
            var term = util.GetTerm(code);
            Assert.AreEqual(null, term);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTerm_NullTerm()
        {
            string code = null;
            var term = util.GetTerm(code);
            Assert.AreEqual(null, term);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTerm_EmptyTerm()
        {
            string code = String.Empty;
            var term = util.GetTerm(code);
            Assert.AreEqual(null, term);
        }

        #endregion

        #region GetTermDescription tests

        [TestMethod]
        public void TermPeriodsUtility_GetTermDescription_ValidTerm()
        {
            string term = term2.Code;
            var desc = util.GetTermDescription(term);
            Assert.AreEqual(term2.Description, desc);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermDescription_InvalidTerm()
        {
            string term = "XYZ";
            var desc = util.GetTermDescription(term);
            Assert.AreEqual(term, desc);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermDescription_NullTerm()
        {
            string term = null;
            var desc = util.GetTermDescription(term);
            Assert.AreEqual(term, desc);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermDescription_EmptyTerm()
        {
            string term = String.Empty;
            var desc = util.GetTermDescription(term);
            Assert.AreEqual(term, desc);
        }

        #endregion

        #region GetReportingTerm tests

        [TestMethod]
        public void TermPeriodsUtility_GetReportingTerm_NullTerm()
        {
            string termId = null;
            var result = util.GetReportingTerm(termId);
            Assert.AreEqual(String.Empty, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetReportingTerm_EmptyTerm()
        {
            string termId = String.Empty;
            var result = util.GetReportingTerm(termId);
            Assert.AreEqual(String.Empty, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetReportingTerm_TermNotReportingTerm()
        {
            string termId = term1.Code;
            var result = util.GetReportingTerm(termId);
            Assert.AreEqual(term1.ReportingTerm, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetReportingTerm_TermIsReportingTerm()
        {
            string termId = term3.Code;
            var result = util.GetReportingTerm(termId);
            Assert.AreEqual(term3.ReportingTerm, result);
        }

        #endregion

        #region IsReportingTerm tests

        [TestMethod]
        public void TermPeriodsUtility_IsReportingTerm_NullTerm()
        {
            string termId = null;
            var result = util.IsReportingTerm(termId);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsReportingTerm_EmptyTerm()
        {
            string termId = String.Empty;
            var result = util.IsReportingTerm(termId);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsReportingTerm_TermNotReportingTerm()
        {
            string termId = term1.Code;
            var result = util.IsReportingTerm(termId);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsReportingTerm_TermIsReportingTerm()
        {
            string termId = term3.Code;
            var result = util.IsReportingTerm(termId);
            Assert.IsTrue(result);
        }

        #endregion

        #region IsInReportingTerm tests

        [TestMethod]
        public void TermPeriodsUtility_IsInReportingTerm_NullTerm()
        {
            string termId = null;
            string reportingTerm = "2012FAR";
            var result = util.IsInReportingTerm(termId, reportingTerm);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInReportingTerm_NullReportingTerm()
        {
            string termId = "2012/FA";
            string reportingTerm = null;
            var result = util.IsInReportingTerm(termId, reportingTerm);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInReportingTerm_TermInReportingTerm()
        {
            string termId = term1.Code;
            string reportingTerm = term1.ReportingTerm;
            var result = util.IsInReportingTerm(termId, reportingTerm);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInReportingTerm_TermNotInReportingTerm()
        {
            string termId = term1.Code;
            string reportingTerm = term2.ReportingTerm;
            var result = util.IsInReportingTerm(termId, reportingTerm);
            Assert.IsFalse(result);
        }

        #endregion

        #region GetTermPeriod tests

        [TestMethod]
        public void TermPeriodsUtility_GetTermPeriod_ValidTerm()
        {
            string code = term2.Code;
            var period = util.GetTermPeriod(code);
            Assert.AreEqual(term2.FinancialPeriod, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermPeriod_InvalidTerm()
        {
            string code = "XYZ";
            var period = util.GetTermPeriod(code);
            Assert.AreEqual(null, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermPeriod_NullTerm()
        {
            string code = null;
            var period = util.GetTermPeriod(code);
            Assert.AreEqual(null, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermPeriod_EmptyTerm()
        {
            string code = String.Empty;
            var period = util.GetTermPeriod(code);
            Assert.AreEqual(null, period);
        }

        #endregion

        #region GetTermSortOrder tests

        [TestMethod]
        public void TermPeriodsUtility_GetTermSortOrder_ValidTerm()
        {
            string code = term2.Code;
            var codeOrder = term2.StartDate.ToString("s") + term2.Sequence.ToString().PadLeft(3, '0') + term2.EndDate.ToString("s") + term2.Code;
            var sortOrder = util.GetTermSortOrder(code);
            Assert.AreEqual(codeOrder, sortOrder);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermSortOrder_InvalidTerm()
        {
            string code = "XYZ";
            var codeOrder = DateTime.MaxValue.ToString("s") + "999" + DateTime.MaxValue.ToString("s") + "zzzzzzz";
            var sortOrder = util.GetTermSortOrder(code);
            Assert.AreEqual(codeOrder, sortOrder);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermSortOrder_NullTerm()
        {
            string code = null;
            var codeOrder = DateTime.MaxValue.ToString("s") + "999" + DateTime.MaxValue.ToString("s") + "zzzzzzz";
            var sortOrder = util.GetTermSortOrder(code);
            Assert.AreEqual(codeOrder, sortOrder);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetTermSortOrder_EmptyTerm()
        {
            string code = String.Empty;
            var codeOrder = DateTime.MaxValue.ToString("s") + "999" + DateTime.MaxValue.ToString("s") + "zzzzzzz";
            var sortOrder = util.GetTermSortOrder(code);
            Assert.AreEqual(codeOrder, sortOrder);
        }

        #endregion

        #region CompareTerms tests

        #region Tests with Term arguments

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_TermsBothNull()
        {
            Term termA = null;
            Term termB = null;
            var result = util.CompareTerms(termA, termB);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_TermsNullNonNull()
        {
            Term termA = null;
            Term termB = term1;
            var result = util.CompareTerms(termA, termB);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_TermsNonNullNull()
        {
            Term termA = term1;
            Term termB = null;
            var result = util.CompareTerms(termA, termB);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_TermsEqual()
        {
            Term termA = term1;
            Term termB = term1;
            var result = util.CompareTerms(termA, termB);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_TermsNotEqual()
        {
            Term termA = term1;
            Term termB = term2;
            var result = util.CompareTerms(termA, termB);
            Assert.IsFalse(result);
        }

        #endregion

        #region Tests with string arguments

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_EqualTerms()
        {
            string code1 = "2012/FA";
            string code2 = "2012/FA";
            var result = util.CompareTerms(code1, code2);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_NotEqualTerms()
        {
            string code1 = "2012/FA";
            string code2 = "2013/FA";
            var result = util.CompareTerms(code1, code2);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_EqualTermsNull()
        {
            string code1 = null;
            string code2 = null;
            var result = util.CompareTerms(code1, code2);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_NotEqualTermsNull1()
        {
            string code1 = null;
            string code2 = "2012/FA";
            var result = util.CompareTerms(code1, code2);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_NotEqualTermsNull2()
        {
            string code1 = "2012/FA";
            string code2 = null;
            var result = util.CompareTerms(code1, code2);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_EqualTermsEmptyNull()
        {
            string code1 = String.Empty;
            string code2 = null;
            var result = util.CompareTerms(code1, code2);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_CompareTerms_EqualTermsNullEmpty()
        {
            string code1 = null;
            string code2 = String.Empty;
            var result = util.CompareTerms(code1, code2);
            Assert.IsTrue(result);
        }

        #endregion

        #endregion

        #region GetPeriod tests

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_NoPeriods()
        {
            var utility = new TermPeriodsUtility(terms, null);
            var period = PeriodType.Past;
            var result = utility.GetPeriod(period);
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_PastPeriod()
        {
            var period = PeriodType.Past;
            var result = util.GetPeriod(period);
            Assert.AreEqual(past, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_CurrentPeriod()
        {
            var period = PeriodType.Current;
            var result = util.GetPeriod(period);
            Assert.AreEqual(current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_FuturePeriod()
        {
            var period = PeriodType.Future;
            var result = util.GetPeriod(period);
            Assert.AreEqual(future, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_PastCode()
        {
            var period = "PAST";
            var result = util.GetPeriod(period);
            Assert.AreEqual(past, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_CurrentCode()
        {
            var period = "CUR";
            var result = util.GetPeriod(period);
            Assert.AreEqual(current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_FutureCode()
        {
            var period = "FTR";
            var result = util.GetPeriod(period);
            Assert.AreEqual(future, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_NullCode()
        {
            string period = null;
            var result = util.GetPeriod(period);
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriod_EmptyCode()
        {
            string period = String.Empty;
            var result = util.GetPeriod(period);
            Assert.AreEqual(null, result);
        }

        #endregion

        #region GetPeriodType tests

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_PastTerm()
        {
            string code = term1.Code;
            var period = util.GetPeriodType(code, DateTime.Now);
            Assert.AreEqual(term1.FinancialPeriod, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_CurrentTerm()
        {
            string code = term2.Code;
            var period = util.GetPeriodType(code, DateTime.Now);
            Assert.AreEqual(term2.FinancialPeriod, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_FutureTerm()
        {
            string code = term3.Code;
            var period = util.GetPeriodType(code, DateTime.Now);
            Assert.AreEqual(term3.FinancialPeriod, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_PastDate()
        {
            string code = null;
            DateTime date = new DateTime(2012, 01, 01);  // Past period date
            var period = util.GetPeriodType(code, date);
            Assert.AreEqual(PeriodType.Past, period.Value);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_CurrentDate()
        {
            string code = null;
            DateTime date = new DateTime(2012, 10, 01);  // Current period date
            var period = util.GetPeriodType(code, date);
            Assert.AreEqual(PeriodType.Current, period.Value);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_FutureDate()
        {
            string code = null;
            DateTime date = new DateTime(2013, 01, 01);  // Future period date
            var period = util.GetPeriodType(code, date);
            Assert.AreEqual(PeriodType.Future, period.Value);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_NoPeriods()
        {
            var utility = new TermPeriodsUtility(terms, null);
            string termId = term3.Code;
            DateTime date = new DateTime(2013, 01, 01);  // Future period date
            var period = utility.GetPeriodType(termId, date);
            Assert.AreEqual(null, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_PastCode()
        {
            string code = "PAST";
            var period = util.GetPeriodType(code);
            Assert.AreEqual(past.Type, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_CurrentCode()
        {
            string code = "CUR";
            var period = util.GetPeriodType(code);
            Assert.AreEqual(current.Type, period);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodType_FutureCode()
        {
            string code = "FTR";
            var period = util.GetPeriodType(code);
            Assert.AreEqual(future.Type, period);
        }

        #endregion

        #region GetPeriodTerms tests

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodTerms_NullPeriod()
        {
            PeriodType? period = null;
            var terms = util.GetPeriodTerms(period);
            Assert.AreEqual(0, terms.Count());
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodTerms_PastPeriod()
        {
            PeriodType? period = PeriodType.Past;
            var terms = util.GetPeriodTerms(period);
            Assert.AreEqual(term1.Code, terms.ToList()[0]);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodTerms_CurrentPeriod()
        {
            PeriodType? period = PeriodType.Current;
            var terms = util.GetPeriodTerms(period);
            Assert.AreEqual(term2.Code, terms.ToList()[0]);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodTerms_FuturePeriod()
        {
            PeriodType? period = PeriodType.Future;
            var terms = util.GetPeriodTerms(period);
            Assert.AreEqual(term3.Code, terms.ToList()[0]);
        }

        #endregion

        #region GetPeriodCode tests

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodCode_NullPeriod()
        {
            period = null;
            var code = util.GetPeriodCode(period);
            Assert.AreEqual(String.Empty, code);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodCode_PastPeriod()
        {
            period = PeriodType.Past;
            var code = util.GetPeriodCode(period);
            Assert.AreEqual("PAST", code);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodCode_CurrentPeriod()
        {
            period = PeriodType.Current;
            var code = util.GetPeriodCode(period);
            Assert.AreEqual("CUR", code);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodCode_FuturePeriod()
        {
            period = PeriodType.Future;
            var code = util.GetPeriodCode(period);
            Assert.AreEqual("FTR", code);
        }

        #endregion

        #region GetDatePeriod tests

        [TestMethod]
        public void TermPeriodsUtility_GetDatePeriod_NoPeriod()
        {
            var utility = new TermPeriodsUtility(terms, null);
            var date = new DateTime(2012, 1, 1);
            var result = utility.GetDatePeriod(date);
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDatePeriod_PastDate()
        {
            var date = new DateTime(2012, 1, 1);
            var result = util.GetDatePeriod(date);
            Assert.AreEqual(past.Type, result.Value);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDatePeriod_CurrentDate()
        {
            var date = new DateTime(2012, 10, 15);
            var result = util.GetDatePeriod(date);
            Assert.AreEqual(current.Type, result.Value);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDatePeriod_FutureDate()
        {
            var date = new DateTime(2013, 1, 1);
            var result = util.GetDatePeriod(date);
            Assert.AreEqual(future.Type, result.Value);
        }

        #endregion

        #region ComparePeriod tests

        #region Tests with 3 input arguments

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_NoPeriods()
        {
            var utility = new TermPeriodsUtility(terms, null);
            string term = term1.Code;
            DateTime date = term1.StartDate;
            var periodType = PeriodType.Past;
            var result = utility.ComparePeriod(term, date, periodType);
            Assert.IsFalse(result);
        }

        #region Past Period tests

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_PastPeriod_TermMatch()
        {
            Term term = term1;
            string termId = term.Code;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_PastPeriod_TermNotMatch()
        {
            Term term = term1;
            string termId = term3.Code;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_PastPeriod_DateMatch()
        {
            Term term = term1;
            string termId = null;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_PastPeriod_DateNotMatch()
        {
            Term term = term1;
            string termId = null;
            DateTime date = term2.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsFalse(result);
        }

        #endregion

        #region Current Period tests

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_CurrentPeriod_TermMatch()
        {
            Term term = term2;
            string termId = term.Code;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_CurrentPeriod_TermNotMatch()
        {
            Term term = term2;
            string termId = term3.Code;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_CurrentPeriod_DateMatch()
        {
            Term term = term2;
            string termId = null;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_CurrentPeriod_DateNotMatch()
        {
            Term term = term2;
            string termId = null;
            DateTime date = term1.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsFalse(result);
        }

        #endregion

        #region Future Period tests

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_FuturePeriod_TermMatch()
        {
            Term term = term3;
            string termId = term.Code;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_FuturePeriod_TermNotMatch()
        {
            Term term = term3;
            string termId = term2.Code;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_FuturePeriod_DateMatch()
        {
            Term term = term3;
            string termId = null;
            DateTime date = term.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_FuturePeriod_DateNotMatch()
        {
            Term term = term3;
            string termId = null;
            DateTime date = term2.StartDate;
            var periodType = term.FinancialPeriod;
            var result = util.ComparePeriod(termId, date, periodType);
            Assert.IsFalse(result);
        }

        #endregion

        #endregion

        #region Tests with 5 input arguments

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_NoTermInPastDateRange()
        {
            string term = null;
            DateTime date = new DateTime(2012, 01, 01);
            var periodTerms = new List<string> { term1.Code };
            var result = util.ComparePeriod(term, date, periodTerms, past.Start, past.End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_NoTermNotInPastDateRange()
        {
            string term = null;
            DateTime date = new DateTime(2013, 01, 01);
            var periodTerms = new List<string> { term1.Code };
            var result = util.ComparePeriod(term, date, periodTerms, past.Start, past.End);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_NoTermInCurrentDateRange()
        {
            string term = null;
            DateTime date = new DateTime(2012, 10, 01);
            var periodTerms = new List<string> { term2.Code };
            var result = util.ComparePeriod(term, date, periodTerms, current.Start, current.End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_NoTermNotInCurrentDateRange()
        {
            string term = null;
            DateTime date = new DateTime(2012, 01, 01);
            var periodTerms = new List<string> { term2.Code };
            var result = util.ComparePeriod(term, date, periodTerms, current.Start, current.End);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_NoTermInFutureDateRange()
        {
            string term = null;
            DateTime date = new DateTime(2013, 01, 01);
            var periodTerms = new List<string> { term3.Code };
            var result = util.ComparePeriod(term, date, periodTerms, future.Start, future.End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_NoTermNotInFutureDateRange()
        {
            string term = null;
            DateTime date = new DateTime(2012, 01, 01);
            var periodTerms = new List<string> { term1.Code };
            var result = util.ComparePeriod(term, date, periodTerms, future.Start, future.End);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_TermInPeriodTerms()
        {
            string term = term2.Code;
            DateTime date = new DateTime(2012, 01, 01);
            var periodTerms = new List<string> { term1.Code, term2.Code, term3.Code };
            var result = util.ComparePeriod(term, date, periodTerms, future.Start, future.End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_ComparePeriod_TermNotInPeriodTerms()
        {
            string term = "XYZ";
            DateTime date = new DateTime(2012, 01, 01);
            var periodTerms = new List<string> { term1.Code, term2.Code, term3.Code };
            var result = util.ComparePeriod(term, date, periodTerms, future.Start, future.End);
            Assert.IsFalse(result);
        }

        #endregion

        #endregion

        #region IsInPeriod tests with single date

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_SingleDate_NoPeriods()
        {
            var utility = new TermPeriodsUtility(terms, null);
            var date = DateTime.Now;
            var result1 = utility.IsInPeriod(date, PeriodType.Past);
            var result2 = utility.IsInPeriod(date, PeriodType.Current);
            var result3 = utility.IsInPeriod(date, PeriodType.Future);
            Assert.IsFalse(result1);
            Assert.IsFalse(result2);
            Assert.IsFalse(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_SingleDate_NullPeriod()
        {
            var date = DateTime.Now;
            var result = util.IsInPeriod(date, null);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_SingleDate_PastDate()
        {
            var date = past.End.Value.AddYears(-1);
            var result1 = util.IsInPeriod(date, PeriodType.Past);
            var result2 = util.IsInPeriod(date, PeriodType.Current);
            var result3 = util.IsInPeriod(date, PeriodType.Future);
            Assert.IsTrue(result1);
            Assert.IsFalse(result2);
            Assert.IsFalse(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_SingleDate_CurrentDate()
        {
            var date = current.Start.Value.AddDays(1);
            var result1 = util.IsInPeriod(date, PeriodType.Past);
            var result2 = util.IsInPeriod(date, PeriodType.Current);
            var result3 = util.IsInPeriod(date, PeriodType.Future);
            Assert.IsFalse(result1);
            Assert.IsTrue(result2);
            Assert.IsFalse(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_SingleDate_FutureDate()
        {
            var date = future.Start.Value.AddDays(1);
            var result1 = util.IsInPeriod(date, PeriodType.Past);
            var result2 = util.IsInPeriod(date, PeriodType.Current);
            var result3 = util.IsInPeriod(date, PeriodType.Future);
            Assert.IsFalse(result1);
            Assert.IsFalse(result2);
            Assert.IsTrue(result3);
        }

        #endregion

        #region IsInPeriod tests with date range

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_NoPeriods()
        {
            var utility = new TermPeriodsUtility(terms, null);
            var date1 = DateTime.Now;
            var date2 = date1.AddDays(1);
            var result1 = utility.IsInPeriod(date1, date2, PeriodType.Past);
            var result2 = utility.IsInPeriod(date1, date2, PeriodType.Current);
            var result3 = utility.IsInPeriod(date1, date2, PeriodType.Future);
            Assert.IsFalse(result1);
            Assert.IsFalse(result2);
            Assert.IsFalse(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_NullPeriod()
        {
            var date1 = DateTime.Now;
            var date2 = date1.AddDays(1);
            var result = util.IsInPeriod(date1, date2, null);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_PastDates()
        {
            var date1 = past.End.Value.AddYears(-1);
            var date2 = date1.AddDays(1);
            var result1 = util.IsInPeriod(date1, date2, PeriodType.Past);
            var result2 = util.IsInPeriod(date1, date2, PeriodType.Current);
            var result3 = util.IsInPeriod(date1, date2, PeriodType.Future);
            Assert.IsTrue(result1);
            Assert.IsFalse(result2);
            Assert.IsFalse(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_CurrentDates()
        {
            var date1 = current.Start.Value.AddDays(1);
            var date2 = date1.AddDays(1);
            var result1 = util.IsInPeriod(date1, date2, PeriodType.Past);
            var result2 = util.IsInPeriod(date1, date2, PeriodType.Current);
            var result3 = util.IsInPeriod(date1, date2, PeriodType.Future);
            Assert.IsFalse(result1);
            Assert.IsTrue(result2);
            Assert.IsFalse(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_FutureDates()
        {
            var date1 = future.Start.Value.AddDays(1);
            var date2 = date1.AddDays(1);
            var result1 = util.IsInPeriod(date1, date2, PeriodType.Past);
            var result2 = util.IsInPeriod(date1, date2, PeriodType.Current);
            var result3 = util.IsInPeriod(date1, date2, PeriodType.Future);
            Assert.IsFalse(result1);
            Assert.IsFalse(result2);
            Assert.IsTrue(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_PastCurrentOverlap()
        {
            var date1 = past.End.Value.AddYears(-1);
            var date2 = current.Start.Value.AddDays(1);
            var result1 = util.IsInPeriod(date1, date2, PeriodType.Past);
            var result2 = util.IsInPeriod(date1, date2, PeriodType.Current);
            var result3 = util.IsInPeriod(date1, date2, PeriodType.Future);
            Assert.IsTrue(result1);
            Assert.IsTrue(result2);
            Assert.IsFalse(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_PastFutureOverlap()
        {
            var date1 = past.End.Value.AddYears(-1);
            var date2 = future.Start.Value.AddDays(1);
            var result1 = util.IsInPeriod(date1, date2, PeriodType.Past);
            var result2 = util.IsInPeriod(date1, date2, PeriodType.Current);
            var result3 = util.IsInPeriod(date1, date2, PeriodType.Future);
            Assert.IsTrue(result1);
            Assert.IsTrue(result2);
            Assert.IsTrue(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_CurrentFutureOverlap()
        {
            var date1 = current.End.Value.AddDays(-1);
            var date2 = future.Start.Value.AddDays(1);
            var result1 = util.IsInPeriod(date1, date2, PeriodType.Past);
            var result2 = util.IsInPeriod(date1, date2, PeriodType.Current);
            var result3 = util.IsInPeriod(date1, date2, PeriodType.Future);
            Assert.IsFalse(result1);
            Assert.IsTrue(result2);
            Assert.IsTrue(result3);
        }

        [TestMethod]
        public void TermPeriodsUtility_IsInPeriod_DateRange_NullDates()
        {
            var date1 = current.End.Value.AddDays(-1);
            var date2 = future.Start.Value.AddDays(1);
            var result1 = util.IsInPeriod(null, null, PeriodType.Past);
            var result2 = util.IsInPeriod(null, null, PeriodType.Current);
            var result3 = util.IsInPeriod(null, null, PeriodType.Future);
            Assert.IsTrue(result1);
            Assert.IsTrue(result2);
            Assert.IsTrue(result3);
        }

        #endregion

        #region GetDateRangePeriod tests

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_NoPeriods()
        {
            var utility = new TermPeriodsUtility(terms, null);
            date1 = DateTime.Now;
            date2 = date1.Value.AddDays(1);
            var result = utility.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastDates()
        {
            date1 = past.End.Value.AddYears(-1);
            date2 = date1.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Past, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastDatesNullStart()
        {
            date1 = null;
            date2 = past.End.Value.AddYears(-1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Past, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_CurrentDates()
        {
            date1 = current.Start.Value.AddDays(1);
            date2 = date1.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_FutureDates()
        {
            date1 = future.Start.Value.AddDays(1);
            date2 = date1.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Future, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_FutureDatesNullEnd()
        {
            date1 = future.Start.Value.AddDays(1);
            date2 = null;
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Future, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastCurrentOverlap()
        {
            date1 = past.End.Value.AddYears(-1);
            date2 = current.Start.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastCurrentOverlapNullStart()
        {
            date1 = null;
            date2 = current.Start.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastFutureOverlap()
        {
            date1 = past.End.Value.AddYears(-1);
            date2 = future.Start.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastFutureOverlapNullStart()
        {
            date1 = null;
            date2 = future.Start.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastFutureOverlapNullStartNullEnd()
        {
            date1 = null;
            date2 = null;
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_PastFutureOverlapNullEnd()
        {
            date1 = past.End.Value.AddYears(-1);
            date2 = null;
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_CurrentFutureOverlap()
        {
            date1 = current.End.Value.AddDays(-1);
            date2 = future.Start.Value.AddDays(1);
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetDateRangePeriod_CurrentFutureOverlapNullEnd()
        {
            date1 = current.End.Value.AddDays(-1);
            date2 = null;
            var result = util.GetDateRangePeriod(date1, date2);
            Assert.AreEqual(PeriodType.Current, result);
        }

        #endregion

        #region GetPeriodDescription tests

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodDescription_NullPeriod()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = null;
            var result = utility.GetPeriodDescription(periodId);
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodDescription_EmptyPeriod()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = string.Empty;
            var result = utility.GetPeriodDescription(periodId);
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodDescription_PastPeriod()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "PAST";
            var result = utility.GetPeriodDescription(periodId);
            Assert.AreEqual(result, "Past");
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodDescription_CurrentPeriod()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "CUR";
            var result = utility.GetPeriodDescription(periodId);
            Assert.AreEqual(result, "Current");
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodDescription_FuturePeriod()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "FTR";
            var result = utility.GetPeriodDescription(periodId);
            Assert.AreEqual(result, "Future");
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodDescription_InvalidPeriod()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "INVALID";
            var result = utility.GetPeriodDescription(periodId);
            Assert.AreEqual(result, null);
        }

        #endregion

        #region GetPeriodSortOrder tests

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodSortOrder_NullPeriodTitle()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodTitle = null;
            var result = utility.GetPeriodSortOrder(periodTitle);
            Assert.AreEqual(result, int.MaxValue);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodSortOrder_EmptyPeriodTitle()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodTitle = string.Empty;
            var result = utility.GetPeriodSortOrder(periodTitle);
            Assert.AreEqual(result, int.MaxValue);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodSortOrder_PastPeriodTitle()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "Past";
            var result = utility.GetPeriodSortOrder(periodId);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodSortOrder_CurrentPeriodTitle()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "Current";
            var result = utility.GetPeriodSortOrder(periodId);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodSortOrder_FuturePeriodTitle()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "Future";
            var result = utility.GetPeriodSortOrder(periodId);
            Assert.AreEqual(result, 2);
        }

        [TestMethod]
        public void TermPeriodsUtility_GetPeriodSortOrder_InvalidPeriodTitle()
        {
            var utility = new TermPeriodsUtility(terms, periods);
            string periodId = "Invalid";
            var result = utility.GetPeriodSortOrder(periodId);
            Assert.AreEqual(result, 0);
        }

        #endregion
    }
}
