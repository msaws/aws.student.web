﻿//Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Adapters;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Web.Mvc.Menu;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Controllers
{
    [TestClass]
    public class AwardsControllerTests
    {
        Settings settings { get; set; }
        ILogger logger { get; set; }
        IAdapterRegistry adapterRegistry { get; set; }
        ISiteService siteService;

        AwardsController awardsController { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            settings = new Mock<Settings>().Object;
            settings.ApiBaseUrl = "http://localhost";
            settings.ApiConnectionLimit = 2;

            logger = new Mock<ILogger>().Object;
            adapterRegistry = new Mock<IAdapterRegistry>().Object;
            siteService = new Mock<ISiteService>().Object;

            awardsController = new AwardsController(adapterRegistry, settings, logger, siteService);
        }

        [TestMethod]
        public void AwardsController_Index()
        {
            var result = awardsController.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ViewBag);
            Assert.IsNotNull(result.ViewBag.Title);
        }

        [TestMethod]
        public void AwardsController_Admin()
        {
            var studentId = "0003914";
            var result = awardsController.Admin(studentId) as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ViewBag);
            Assert.IsNotNull(result.ViewBag.Title);
            Assert.IsNotNull(result.ViewBag.IsAdmin);
            Assert.IsTrue(result.ViewBag.IsAdmin);
            Assert.IsNotNull(result.ViewBag.StudentId);
            Assert.AreEqual(studentId, result.ViewBag.StudentId);
        }

        //[TestMethod]
        //[ExpectedException(typeof (HttpException))]
        //public void AwardsControllerStudentIdNull_Admin()
        //{
        //    var studentId = "";
        //    try
        //    {
        //        var result = awardsController.Admin(studentId) as ViewResult;
        //    }
        //    catch (Exception ex){
        //        var hex = ex as HttpException;
        //        Assert.AreEqual(400, hex.GetHttpCode());
        //        throw;
        //    }
            
        //}
    }
}
