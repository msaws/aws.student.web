﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ShoppingSheet
{
    /// <summary>
    /// Financial Aid Office test class
    /// </summary>
    [TestClass]
    public class FAOfficeTests
    {
        public string studentId = "0003914";
        public string institutionName = "Ellucian";
        public FinancialAidOffice3 financialAidOfficeDto;
        public FAOffice faOffice;

        public TestModelData testModelData;

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData(studentId);
            financialAidOfficeDto = testModelData.financialAidOfficesData.First();

            faOffice = new FAOffice(financialAidOfficeDto, institutionName);
        }

        [TestMethod]
        public void ObjectIsNotNullTest()
        {
            Assert.IsNotNull(faOffice);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FinancialAidDtoIsNull_ExceptionThrownTest()
        {
            faOffice = new FAOffice(null, institutionName);
        }

        [TestMethod]
        public void FAOfficeAttributes_AreEqualTest()
        {
            Assert.AreEqual(institutionName, faOffice.Name);
            Assert.AreEqual(financialAidOfficeDto.EmailAddress, faOffice.EmailAddress);
            Assert.AreEqual(financialAidOfficeDto.PhoneNumber, faOffice.PhoneNumber);
            Assert.AreEqual(financialAidOfficeDto.AddressLabel, faOffice.Address);
        }

        [TestMethod]
        public void FaOfficeName_EqualsOfficeDtoNameTest()
        {
            faOffice = new FAOffice(financialAidOfficeDto, null);
            Assert.AreEqual(financialAidOfficeDto.Name, faOffice.Name);
        }

        [TestMethod]
        public void FaOfficeContactName_EqualsOfficeDtoName()
        {
            Assert.AreEqual(financialAidOfficeDto.Name, faOffice.ContactName);
        }
    }
}
