﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ShoppingSheet
{
    /// <summary>
    /// StudentShoppingSheet test class
    /// </summary>
    [TestClass]
    public class StudentShoppingSheetTests
    {
        public string studentId;
        public TestModelData testModelData;
        public StudentShoppingSheet studentShoppingSheet;
        public List<Colleague.Dtos.FinancialAid.ShoppingSheet> shoppingSheetDtos;
        public List<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeDtos;
        public Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto;
        public Colleague.Dtos.Base.Institution institutionDto;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);

            shoppingSheetDtos = testModelData.shoppingSheetData;
            financialAidOfficeDtos = testModelData.financialAidOfficesData;
            studentAwardYearDto = testModelData.studentAwardYearsData.First();
            institutionDto = testModelData.institutionData;

            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);
        }

        [TestMethod]
        public void ObjectIsNotNullTest()
        {
            Assert.IsNotNull(studentShoppingSheet);
        }

        [TestMethod]
        public void NoInstitutionData_ObjectIsNotNullTest()
        {
            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, null);
            Assert.IsNotNull(studentShoppingSheet);
            Assert.IsNotNull(studentShoppingSheet.FinancialAidOffice);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AwardYearDtoIsNull_ExceptionThrownTest()
        {
            studentShoppingSheet = new StudentShoppingSheet(null, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ShoppingSheetDtosListIsNull_ExceptionThrownTest()
        {
            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, null, financialAidOfficeDtos, institutionDto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FinanciaAidOfficeDtosListIsNull_ExceptionThrownTest()
        {
            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, null, institutionDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void NoShoppingSheetForYearFound_ExceptionThrownTest()
        {
            var index = shoppingSheetDtos.FindIndex(ss => ss.AwardYear == studentAwardYearDto.Code && ss.StudentId == studentAwardYearDto.StudentId);
            shoppingSheetDtos.RemoveAt(index);
            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void NoFinancialAidOfficeForYearFound_ExceptionThrownTest()
        {
            var index = financialAidOfficeDtos.FindIndex(fao => fao.Id == studentAwardYearDto.FinancialAidOfficeId);
            financialAidOfficeDtos.RemoveAt(index);
            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void NoFinancialAidConfigurationForYear_ExceptionThrownTest()
        {
            var index = financialAidOfficeDtos.FindIndex(fao => fao.Id == studentAwardYearDto.FinancialAidOfficeId);
            financialAidOfficeDtos[index].Configurations = new List<FinancialAidConfiguration3>();
            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ShoppingSheetNotActiveForYear_ExceptionThrownTest()
        {
            var index = financialAidOfficeDtos.FindIndex(fao => fao.Id == studentAwardYearDto.FinancialAidOfficeId);
            financialAidOfficeDtos[index].Configurations.ForEach(c => c.IsShoppingSheetActive = false);
            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);
        }

        /// <summary>
        /// Tests the attributes that were not grouped in the dtos
        /// </summary>
        [TestMethod]
        public void ShoppingSheet_UngroupedAttributesEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);

            Assert.AreEqual(studentAwardYearDto.Code, studentShoppingSheet.AwardYearCode);
            Assert.AreEqual(((int)shoppingSheetDto.TotalEstimatedCost).ToString("C0"), studentShoppingSheet.TotalEstimatedCost);
            Assert.AreEqual(((int)shoppingSheetDto.TotalGrantsAndScholarships).ToString("C0"), studentShoppingSheet.TotalGrantsAndScholarships);
            Assert.AreEqual(((int)shoppingSheetDto.FamilyContribution).ToString("C0"), studentShoppingSheet.FamilyContribution);
            Assert.AreEqual(shoppingSheetDto.NetCosts, studentShoppingSheet.NetCosts);

            Assert.IsNotNull(studentShoppingSheet.FinancialAidOffice);
            Assert.AreEqual(shoppingSheetDto.CustomMessages, studentShoppingSheet.CustomMessagesSet1);
        }

        [TestMethod]
        public void ShoppingSheet_BooksAndSuppliesEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var costs = shoppingSheetDto.Costs;
            var amount = costs.Where(c => c.BudgetGroup == ShoppingSheetBudgetGroup.BooksAndSupplies).Sum(c => c.Cost);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.BooksAndSupplies);
        }

        [TestMethod]
        public void ShoppingSheet_TuitionAndFeesEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var costs = shoppingSheetDto.Costs;
            var amount = costs.Where(c => c.BudgetGroup == ShoppingSheetBudgetGroup.TuitionAndFees).Sum(c => c.Cost);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.TuitionAndFees);
        }

        [TestMethod]
        public void ShoppingSheet_HousingAndMealsEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var costs = shoppingSheetDto.Costs;
            var amount = costs.Where(c => c.BudgetGroup == ShoppingSheetBudgetGroup.HousingAndMeals).Sum(c => c.Cost);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.HousingAndMeals);
        }

        [TestMethod]
        public void ShoppingSheet_TransportationEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var costs = shoppingSheetDto.Costs;
            var amount = costs.Where(c => c.BudgetGroup == ShoppingSheetBudgetGroup.Transportation).Sum(c => c.Cost);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.Transportation);
        }

        [TestMethod]
        public void ShoppingSheet_OtherCostsEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var costs = shoppingSheetDto.Costs;
            var amount = costs.Where(c => c.BudgetGroup == ShoppingSheetBudgetGroup.OtherCosts).Sum(c => c.Cost);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.OtherCosts);
        }

        [TestMethod]
        public void ShoppingSheet_SchoolGrantsEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var grants = shoppingSheetDto.GrantsAndScholarships;
            var amount = grants.Where(g => g.AwardGroup == ShoppingSheetAwardGroup.SchoolGrants).Sum(c => c.Amount);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.SchoolGrants);
        }

        [TestMethod]
        public void ShoppingSheet_StateGrantsEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var grants = shoppingSheetDto.GrantsAndScholarships;
            var amount = grants.Where(g => g.AwardGroup == ShoppingSheetAwardGroup.StateGrants).Sum(c => c.Amount);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.StateGrants);
        }

        [TestMethod]
        public void ShoppingSheet_PellGrantEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var grants = shoppingSheetDto.GrantsAndScholarships;
            var amount = grants.Where(g => g.AwardGroup == ShoppingSheetAwardGroup.PellGrants).Sum(c => c.Amount);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.FederalPellGrant);
        }

        [TestMethod]
        public void ShoppingSheet_OtherGrantsEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var grants = shoppingSheetDto.GrantsAndScholarships;
            var amount = grants.Where(g => g.AwardGroup == ShoppingSheetAwardGroup.OtherGrants).Sum(c => c.Amount);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.OtherGrants);
        }

        [TestMethod]
        public void ShoppingSheet_SubsidizedLoansEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var loans = shoppingSheetDto.LoanOptions;
            var amount = loans.Where(g => g.AwardGroup == ShoppingSheetAwardGroup.SubsidizedLoans).Sum(c => c.Amount);

            Assert.AreEqual(amount, studentShoppingSheet.SubsidizedLoans);
        }

        [TestMethod]
        public void ShoppingSheet_UnsubsidizedLoansEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var loans = shoppingSheetDto.LoanOptions;
            var amount = loans.Where(g => g.AwardGroup == ShoppingSheetAwardGroup.UnsubsidizedLoans).Sum(c => c.Amount);

            Assert.AreEqual(amount, studentShoppingSheet.UnsubsidizedLoans);
        }

        [TestMethod]
        public void ShoppingSheet_PerkinsLoansEqualTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var loans = shoppingSheetDto.LoanOptions;
            var amount = loans.Where(g => g.AwardGroup == ShoppingSheetAwardGroup.PerkinsLoans).Sum(c => c.Amount);

            Assert.AreEqual(amount, studentShoppingSheet.PerkinsLoans);
        }

        [TestMethod]
        public void ShoppingSheet_WorkStudyEqualsTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            var workStudy = shoppingSheetDto.WorkOptions;
            var amount = workStudy.Where(w => w.AwardGroup == ShoppingSheetAwardGroup.WorkStudy).Sum(w => w.Amount);

            Assert.AreEqual(amount.ToString("N0"), studentShoppingSheet.WorkStudy);
        }

        [TestMethod]
        public void UngroupedAmountAttributes_EqualNAOrEmptyStringTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            shoppingSheetDto.FamilyContribution = null;
            shoppingSheetDto.TotalEstimatedCost = null;
            shoppingSheetDto.TotalGrantsAndScholarships = null;

            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);
            Assert.AreEqual("N/A", studentShoppingSheet.TotalEstimatedCost);
            Assert.AreEqual("N/A", studentShoppingSheet.TotalGrantsAndScholarships);
            Assert.AreEqual(string.Empty, studentShoppingSheet.FamilyContribution);
        }

        [TestMethod]
        public void CostAmountAttributes_EqualNATest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            shoppingSheetDto.Costs = new List<ShoppingSheetCostItem>();

            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);

            Assert.AreEqual("N/A", studentShoppingSheet.BooksAndSupplies);
            Assert.AreEqual("N/A", studentShoppingSheet.HousingAndMeals);
            Assert.AreEqual("N/A", studentShoppingSheet.TuitionAndFees);
            Assert.AreEqual("N/A", studentShoppingSheet.Transportation);
            Assert.AreEqual("N/A", studentShoppingSheet.OtherCosts);
        }

        [TestMethod]
        public void GrantAmountAttributes_EqualNATest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            shoppingSheetDto.GrantsAndScholarships = new List<ShoppingSheetAwardItem>();

            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);

            Assert.AreEqual("N/A", studentShoppingSheet.OtherGrants);
            Assert.AreEqual("N/A", studentShoppingSheet.SchoolGrants);
            Assert.AreEqual("N/A", studentShoppingSheet.StateGrants);
            Assert.AreEqual("N/A", studentShoppingSheet.FederalPellGrant);
        }

        [TestMethod]
        public void LoanAmountAttributes_EqualZeroTest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            shoppingSheetDto.LoanOptions = new List<ShoppingSheetAwardItem>();

            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);

            Assert.AreEqual(0, studentShoppingSheet.PerkinsLoans);
            Assert.AreEqual(0, studentShoppingSheet.SubsidizedLoans);
            Assert.AreEqual(0, studentShoppingSheet.UnsubsidizedLoans);
        }

        [TestMethod]
        public void WorkStudyAmountAttribute_EqualsNATest()
        {
            var shoppingSheetDto = shoppingSheetDtos.FirstOrDefault(ss => ss.AwardYear == studentAwardYearDto.Code);
            shoppingSheetDto.WorkOptions = new List<ShoppingSheetAwardItem>();

            studentShoppingSheet = new StudentShoppingSheet(studentAwardYearDto, shoppingSheetDtos, financialAidOfficeDtos, institutionDto);

            Assert.AreEqual("N/A", studentShoppingSheet.WorkStudy);
        }

        [TestMethod]
        public void AwardYearCodeNull_NotIsPrior2017Test()
        {
            studentShoppingSheet.AwardYearCode = null;
            Assert.IsFalse(studentShoppingSheet.IsPrior2017);
        }

        [TestMethod]
        public void AwardYearPrior2017_IsPrior2017Test()
        {
            studentShoppingSheet.AwardYearCode = "2016";
            Assert.IsTrue(studentShoppingSheet.IsPrior2017);
        }

        [TestMethod]
        public void AwardYearIs2017_NotIsPrior2017Test()
        {
            studentShoppingSheet.AwardYearCode = "2017";
            Assert.IsFalse(studentShoppingSheet.IsPrior2017);
        }

        [TestMethod]
        public void AwardYearPost2017_NotIsPrior2017Test()
        {
            studentShoppingSheet.AwardYearCode = "2018";
            Assert.IsFalse(studentShoppingSheet.IsPrior2017);
        }
    }
}
