﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ShoppingSheet
{
    /// <summary>
    /// ShoppingSheetViewModel test class
    /// </summary>
    [TestClass]
    public class ShoppingSheetViewModelTests
    {
        public TestModelData testModelData;
        public ShoppingSheetViewModel shoppingSheetViewModel;
        public string studentId;
        private ICurrentUser currentUser;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);
            currentUser = testModelData.currentUser;
            BuildShoppingSheetViewModel();
        }

        /// <summary>
        /// Test if the shoppingSheetViewModel was created along with
        /// its properties
        /// </summary>
        [TestMethod]
        public void DtoData_ShoppingSheetViewModel()
        {
            Assert.IsNotNull(shoppingSheetViewModel);
            Assert.IsNotNull(shoppingSheetViewModel.AwardYears);
            Assert.IsNotNull(shoppingSheetViewModel.StudentShoppingSheets);
           
        }

        /// <summary>
        /// Test if the shoppingSheetViewModel was created when there was no data passed
        /// </summary>
        [TestMethod]
        public void NoData_ShoppingSheetViewModel()
        {
            testModelData.shoppingSheetData = new List<Colleague.Dtos.FinancialAid.ShoppingSheet>();
            testModelData.studentAwardYearsData = new List<StudentAwardYear2>();
            testModelData.financialAidOfficesData = new List<FinancialAidOffice3>();
            testModelData.institutionData = null;
            

            BuildShoppingSheetViewModel();

            Assert.IsNotNull(shoppingSheetViewModel);
            Assert.IsNotNull(shoppingSheetViewModel.AwardYears);
            Assert.IsNotNull(shoppingSheetViewModel.StudentShoppingSheets);
        }

        /// <summary>
        /// Test if the student property was created even though there was
        /// no person data passed
        /// </summary>
        [TestMethod]
        public void NoPersonData_StudentIsNotNullTest()
        {
            testModelData.studentData = null;
            BuildShoppingSheetViewModel();

            Assert.IsNotNull(shoppingSheetViewModel.Student);
        }        

        
        /// <summary>
        /// Test if the number of award years in the shoppingSheetViewModel
        /// equals the number of the studentAwardYear dtos
        /// </summary>
        [TestMethod]
        public void NumberAwardYears_EqualsTest()
        {
            var studentAwardYearDtosCount = testModelData.studentAwardYearsData.Count;
            Assert.AreEqual(studentAwardYearDtosCount, shoppingSheetViewModel.AwardYears.Count());
        }

        /// <summary>
        /// Test if the number of the shopping sheets equals the number of the all the years
        /// that are both in the studentAwardYearDtos list and the shoppingSheetDtos list
        /// </summary>
        [TestMethod]
        public void NumberShoppingSheets_EqualsTest()
        {
            var commonAwardYears = testModelData.shoppingSheetData.FindAll(ss => testModelData.studentAwardYearsData.Any(say => say.Code == ss.AwardYear));
            Assert.AreEqual(commonAwardYears.Count, shoppingSheetViewModel.StudentShoppingSheets.Count());
        }

        [TestMethod]
        public void CounselorCreatedForEachYear_ShoppingSheetViewModel()
        {
            shoppingSheetViewModel.AwardYears.ToList().ForEach(y => Assert.IsNotNull(y.Counselor));
        }

        [TestMethod]
        public void CounselorNotCreated_NoExceptions_ShoppingSheetViewModel()
        {
            testModelData.financialAidCounselorData.Name = string.Empty;
            BuildShoppingSheetViewModel();

            shoppingSheetViewModel.AwardYears.ToList().ForEach(y => Assert.IsNull(y.Counselor));
        }

        /// <summary>
        /// Test if award years are arranged in descending order
        /// </summary>
        [TestMethod]
        public void AwardYearsDescendingOrder_ShoppingSheetViewModel()
        {
            var awardYears = shoppingSheetViewModel.AwardYears.ToList();

            for (int i = 0; i < awardYears.Count() - 1; i++)
            {
                Assert.IsTrue(awardYears[i].Code.CompareTo(awardYears[i + 1].Code) > 0);
            }
        }

        [TestMethod]
        public void IsProxyView_FalseWhenCurrentUserIsNullTest()
        {
            currentUser = null;
            BuildShoppingSheetViewModel();
            Assert.IsFalse(shoppingSheetViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyView_FalseWhenCurrentUserIsNotProxyTest()
        {
            Assert.IsFalse(shoppingSheetViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyView_TrueWhenCurrentUserIsProxyTest()
        {
            currentUser = testModelData.currentUserWithProxy;
            BuildShoppingSheetViewModel();
            Assert.IsTrue(shoppingSheetViewModel.IsProxyView);
        }

        [TestMethod]
        public void NoExceptionsThrown_WhenFinancialAidOfficeDataIsNullTest()
        {
            testModelData.financialAidOfficesData = null;
            bool exceptionThrown = false;
            try
            {
                BuildShoppingSheetViewModel();
            }
            catch { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void NoPersonData_PersonIdIsnullTest()
        {
            testModelData.studentData = null;
            BuildShoppingSheetViewModel();
            Assert.IsNull(shoppingSheetViewModel.PersonId);
        }

        [TestMethod]
        public void PersonId_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelData.studentData.Id, shoppingSheetViewModel.PersonId);
        }

        [TestMethod]
        public void NoPersonData_PersonNameIsnullTest()
        {
            testModelData.studentData = null;
            BuildShoppingSheetViewModel();
            Assert.IsNull(shoppingSheetViewModel.PersonName);
        }

        [TestMethod]
        public void PersonName_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelData.studentData.PreferredName, shoppingSheetViewModel.PersonName);
        }

        [TestMethod]
        public void NoPersonData_PrivacyStatusCodeIsnullTest()
        {
            testModelData.studentData = null;
            BuildShoppingSheetViewModel();
            Assert.IsNull(shoppingSheetViewModel.PrivacyStatusCode);
        }

        [TestMethod]
        public void PrivacyStatusCode_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelData.studentData.PrivacyStatusCode, shoppingSheetViewModel.PrivacyStatusCode);
        }

        private void BuildShoppingSheetViewModel()
        {
            shoppingSheetViewModel = new ShoppingSheetViewModel(testModelData.studentData,
                testModelData.financialAidCounselorData, testModelData.studentAwardYearsData,
                testModelData.financialAidOfficesData, testModelData.shoppingSheetData, 
                testModelData.institutionData, currentUser);
        }
    }
}
