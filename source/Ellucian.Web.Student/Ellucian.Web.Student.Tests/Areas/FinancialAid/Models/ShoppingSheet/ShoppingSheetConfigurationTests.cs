﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using System.Globalization;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ShoppingSheet
{

    [TestClass]
    public class ShoppingSheetConfiguration_ShoppingSheetDtoConstructorTests
    {
        public TestModelData testModelData;

        public Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration configurationDto;

        public string officeName;

        public ShoppingSheetConfiguration shoppingSheetConfiguration
        {
            get
            { return new ShoppingSheetConfiguration(configurationDto, officeName); }
        }

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0003914");
            configurationDto = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations.Select(c => c.ShoppingSheetConfiguration)).FirstOrDefault(c => c != null);
            officeName = "OFFICE";
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullConfigurationTest()
        {
            new ShoppingSheetConfiguration(null, officeName);
        }

        [TestMethod]
        public void CustomMessageRuleTableIdTest()
        {
            Assert.AreEqual(configurationDto.CustomMessageRuleTableId, shoppingSheetConfiguration.CustomMessageRuleTableId);
        }

        [TestMethod]
        public void EfcOptionTest()
        {
            Assert.AreEqual(configurationDto.EfcOption, shoppingSheetConfiguration.EfcOption);
        }

        [TestMethod]
        public void GraduationRate100PercentTest()
        {
            configurationDto.GraduationRate = 1;
            Assert.AreEqual((decimal).01, shoppingSheetConfiguration.GraduationRate);
        }

        [TestMethod]
        public void GraduationRateLessThan100PercentTest()
        {
            configurationDto.GraduationRate = (decimal)1.01;
            Assert.AreEqual(configurationDto.GraduationRate, shoppingSheetConfiguration.GraduationRate);
        }

        [TestMethod]
        public void LoanDefaultRate100PercentTest()
        {
            configurationDto.LoanDefaultRate = 1;
            Assert.AreEqual((decimal).01, shoppingSheetConfiguration.LoanDefaultRate);
        }

        [TestMethod]
        public void LoanDefaultRateLessThan100PercentTest()
        {
            configurationDto.LoanDefaultRate = (decimal).99;
            Assert.AreEqual((decimal).0099, shoppingSheetConfiguration.LoanDefaultRate);
        }

        [TestMethod]
        public void NationalLoanDefaultRate100PercentTest()
        {
            configurationDto.NationalLoanDefaultRate = 1;
            Assert.AreEqual((decimal).01, shoppingSheetConfiguration.NationalLoanDefaultRate);
        }

        [TestMethod]
        public void NationalLoanDefaultRateLessThan100PercentTest()
        {
            configurationDto.NationalLoanDefaultRate = (decimal)1000;
            Assert.AreEqual(configurationDto.NationalLoanDefaultRate, shoppingSheetConfiguration.NationalLoanDefaultRate);
        }

        [TestMethod]
        public void InstitutionRepaymentRate_LessThanOneValueTest()
        {
            configurationDto.InstitutionRepaymentRate = 0.6m;
            Assert.AreEqual(0.006m, shoppingSheetConfiguration.InstitutionRepaymentRate);
        }

        [TestMethod]
        public void InstitutionRepaymentRate_GreaterThanOneValueTest()
        {
            configurationDto.InstitutionRepaymentRate = 67.6m;
            Assert.AreEqual(configurationDto.InstitutionRepaymentRate, shoppingSheetConfiguration.InstitutionRepaymentRate);
        }

        [TestMethod]
        public void NullInstitutionRepaymentRateTest()
        {
            configurationDto.InstitutionRepaymentRate = null;
            Assert.IsNull(shoppingSheetConfiguration.InstitutionRepaymentRate);
        }

        [TestMethod]
        public void NationalRepaymentRate_LessThanOneValueTest()
        {
            configurationDto.NationalRepaymentRateAverage = 0.6m;
            Assert.AreEqual(0.006m, shoppingSheetConfiguration.NationalRepaymentRateAverage);
        }

        [TestMethod]
        public void NationalRepaymentRate_GreaterThanOneValueTest()
        {
            configurationDto.NationalRepaymentRateAverage = 67.6m;
            Assert.AreEqual(configurationDto.NationalRepaymentRateAverage, shoppingSheetConfiguration.NationalRepaymentRateAverage);
        }

        [TestMethod]
        public void NullNationalRepaymentRateAverageTest()
        {
            configurationDto.NationalRepaymentRateAverage = null;
            Assert.IsNull(shoppingSheetConfiguration.NationalRepaymentRateAverage);
        }

        [TestMethod]
        public void MedianBorrowingAmountHasValueTest()
        {
            configurationDto.MedianBorrowingAmount = 5;
            Assert.AreEqual(((int)configurationDto.MedianBorrowingAmount).ToString("C0", CultureInfo.CurrentCulture), shoppingSheetConfiguration.MedianBorrowingAmount);
        }

        [TestMethod]
        public void MedianBorrowingAmountHasNoValueTest()
        {
            configurationDto.MedianBorrowingAmount = null;
            Assert.AreEqual(string.Empty, shoppingSheetConfiguration.MedianBorrowingAmount);
        }

        [TestMethod]
        public void MedianMonthlyPaymentAmountHasValueTest()
        {
            configurationDto.MedianMonthlyPaymentAmount = 5;
            Assert.AreEqual(((int)configurationDto.MedianMonthlyPaymentAmount).ToString("C0", CultureInfo.CurrentCulture), shoppingSheetConfiguration.MedianMonthlyPaymentAmount);
        }

        [TestMethod]
        public void MedianMonthlyPaymentAmountHasNoValueTest()
        {
            configurationDto.MedianMonthlyPaymentAmount = null;
            Assert.AreEqual(string.Empty, shoppingSheetConfiguration.MedianMonthlyPaymentAmount);
        }

        [TestMethod]
        public void OfficeTypeTest()
        {
            Assert.AreEqual(configurationDto.OfficeType, shoppingSheetConfiguration.OfficeType);
        }

        [TestMethod]
        public void LoanDefaultRateHasValueTest()
        {
            configurationDto.LoanDefaultRate = 5;
            configurationDto.NationalLoanDefaultRate = 6;

            //unable to test resource strings right now
            //Assert.IsTrue(!string.IsNullOrEmpty(shoppingSheetConfiguration.LoanDefaultRateDetail));
        }

        [TestMethod]
        public void LoanDefaultRateDoesNotHaveValueTest()
        {
            configurationDto.LoanDefaultRate = null;
            configurationDto.NationalLoanDefaultRate = 6;

            // unable to test resource strings right now
            //Assert.IsTrue(!string.IsNullOrEmpty(shoppingSheetConfiguration.LoanDefaultRateDetail));
        }

        [TestMethod]
        public void NationalLoanDefaultRateDoesNotHaveValueTest()
        {
            configurationDto.LoanDefaultRate = 6;
            configurationDto.NationalLoanDefaultRate = null;

            //unable to test resource strings right now
            //Assert.IsTrue(!string.IsNullOrEmpty(shoppingSheetConfiguration.LoanDefaultRateDetail));
        }
    }

    [TestClass]
    public class GraduationRateLevelTests
    {
        public TestModelData testModelData;

        public Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration configurationDto;

        public ShoppingSheetConfiguration shoppingSheetConfiguration
        {
            get
            { return new ShoppingSheetConfiguration(configurationDto, string.Empty); }
        }

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0003914");
            configurationDto = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations.Select(c => c.ShoppingSheetConfiguration)).FirstOrDefault(c => c != null);
        }

        [TestMethod]
        public void NullIfNoGraduationRateTest()
        {
            configurationDto.GraduationRate = null;
            Assert.IsNull(shoppingSheetConfiguration.GraduationRateLevel);
        }

        [TestMethod]
        public void NullIfNoLowMediumBoundaryTest()
        {
            configurationDto.LowToMediumBoundary = null;
            Assert.IsNull(shoppingSheetConfiguration.GraduationRateLevel);
        }

        [TestMethod]
        public void NullIfNoMediumHighBoundaryTest()
        {
            configurationDto.MediumToHighBoundary = null;
            Assert.IsNull(shoppingSheetConfiguration.GraduationRateLevel);
        }

        [TestMethod]
        public void LowRateLevelTest()
        {
            configurationDto.LowToMediumBoundary = 33.3m;
            configurationDto.GraduationRate = 33.29m;
            Assert.AreEqual(RateLevel.Low, shoppingSheetConfiguration.GraduationRateLevel);
        }

        [TestMethod]
        public void LowMediumRateLevelTest()
        {
            configurationDto.LowToMediumBoundary = 33.3m;
            configurationDto.GraduationRate = 33.3m;
            Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        }

        [TestMethod]
        public void MediumRateLevelTest()
        {
            configurationDto.MediumToHighBoundary = 66.6m;
            configurationDto.GraduationRate = 66.59m;
            Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        }

        [TestMethod]
        public void MediumHighRateLevelTest()
        {
            configurationDto.MediumToHighBoundary = 66.6m;
            configurationDto.GraduationRate = 66.6m;
            Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.GraduationRateLevel);
        }

        [TestMethod]
        public void HighRateLevelTest()
        {
            configurationDto.MediumToHighBoundary = 66.6m;
            configurationDto.GraduationRate = 100.1m;
            Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.GraduationRateLevel);
        }

        //[TestMethod]
        //public void BachelorTypeLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.BachelorDegreeGranting;
        //    configurationDto.GraduationRate = 37.39m;
        //    Assert.AreEqual(RateLevel.Low, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void BachelorTypeMedLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.BachelorDegreeGranting;
        //    configurationDto.GraduationRate = 37.4m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void BachelorTypeMedHighEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.BachelorDegreeGranting;
        //    configurationDto.GraduationRate = 66.79m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void BachelorTypeHighMedEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.BachelorDegreeGranting;
        //    configurationDto.GraduationRate = 66.8m;
        //    Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        ////AssociateType
        //[TestMethod]
        //public void AssociateTypeLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.AssociateDegreeGranting;
        //    configurationDto.GraduationRate = 17.79m;
        //    Assert.AreEqual(RateLevel.Low, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void AssociateTypeMedLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.AssociateDegreeGranting;
        //    configurationDto.GraduationRate = 17.8m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void AssociateTypeMedHighEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.AssociateDegreeGranting;
        //    configurationDto.GraduationRate = 32.29m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void AssociateTypeHighMedEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.AssociateDegreeGranting;
        //    configurationDto.GraduationRate = 32.3m;
        //    Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        ////CertificateType
        //[TestMethod]
        //public void CertificateTypeLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.CertificateGranting;
        //    configurationDto.GraduationRate = 24.19m;
        //    Assert.AreEqual(RateLevel.Low, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void CertificateTypeMedLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.CertificateGranting;
        //    configurationDto.GraduationRate = 24.2m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void CertificateTypeMedHighEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.CertificateGranting;
        //    configurationDto.GraduationRate = 50.19m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void CertificateTypeHighMedEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.CertificateGranting;
        //    configurationDto.GraduationRate = 50.2m;
        //    Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        ////GraduateDegreeGranting
        //[TestMethod]
        //public void GraduateTypeNullRateLevelTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.GraduateDegreeGranting;
        //    configurationDto.GraduationRate = 10m;
        //    Assert.IsNull(shoppingSheetConfiguration.GraduationRateLevel);
        //}

        ////NonDegreeGranting
        //[TestMethod]
        //public void NonDegreeTypeLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.NonDegreeGranting;
        //    configurationDto.GraduationRate = 62.59m;
        //    Assert.AreEqual(RateLevel.Low, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void NonDegreeTypeMedLowEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.NonDegreeGranting;
        //    configurationDto.GraduationRate = 62.6m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void NonDegreeTypeMedHighEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.NonDegreeGranting;
        //    configurationDto.GraduationRate = 77.69m;
        //    Assert.AreEqual(RateLevel.Medium, shoppingSheetConfiguration.GraduationRateLevel);
        //}

        //[TestMethod]
        //public void NonDegreeTypeHighMedEdgeTest()
        //{
        //    configurationDto.OfficeType = Colleague.Dtos.FinancialAid.ShoppingSheetOfficeType.NonDegreeGranting;
        //    configurationDto.GraduationRate = 77.7m;
        //    Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.GraduationRateLevel);
        //}

    }

    [TestClass]
    public class LoanDefaultRateLevelTests
    {
        public TestModelData testModelData;

        public Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration configurationDto;

        public ShoppingSheetConfiguration shoppingSheetConfiguration
        {
            get
            { return new ShoppingSheetConfiguration(configurationDto, string.Empty); }
        }

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0003914");
            configurationDto = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations.Select(c => c.ShoppingSheetConfiguration)).FirstOrDefault(c => c != null);
        }

        [TestMethod]
        public void NullLoanDefaultRate_NullLoanDefaultRateLevelTest(){
            configurationDto.LoanDefaultRate = null;
            Assert.IsNull(shoppingSheetConfiguration.LoanDefaultRateLevel);
        }

        [TestMethod]
        public void NullNationalLoanDefaultRate_NullLoanDefaultRateLevelTest()
        {
            configurationDto.NationalLoanDefaultRate = null;
            Assert.IsNull(shoppingSheetConfiguration.LoanDefaultRateLevel);
        }

        [TestMethod]
        public void LoanDefaultRateLevel_IsLowTest()
        {
            configurationDto.NationalLoanDefaultRate = 100;
            configurationDto.LoanDefaultRate = 70;
            Assert.IsTrue(shoppingSheetConfiguration.LoanDefaultRateLevel == RateLevel.Low);
        }

        [TestMethod]
        public void LoanDefaultRateLevel_IsHighTest()
        {
            configurationDto.NationalLoanDefaultRate = 55;
            configurationDto.LoanDefaultRate = 65;
            Assert.IsTrue(shoppingSheetConfiguration.LoanDefaultRateLevel == RateLevel.High);
        }

        [TestMethod]
        public void LoanDefaultRateLevel_IsMediumTest()
        {
            configurationDto.NationalLoanDefaultRate = 50;
            configurationDto.LoanDefaultRate = 50;
            Assert.IsTrue(shoppingSheetConfiguration.LoanDefaultRateLevel == RateLevel.Medium);
        }
    }

    [TestClass]
    public class NationalLoanDefaultRateLevelTests{
        public TestModelData testModelData;

        public Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration configurationDto;

        public ShoppingSheetConfiguration shoppingSheetConfiguration
        {
            get
            { return new ShoppingSheetConfiguration(configurationDto, string.Empty); }
        }

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0003914");
            configurationDto = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations.Select(c => c.ShoppingSheetConfiguration)).FirstOrDefault(c => c != null);
        }

        [TestMethod]
        public void NullLoanDefaultRate_NullNationalLoanDefaultRateLevelTest()
        {
            configurationDto.LoanDefaultRate = null;
            Assert.IsNull(shoppingSheetConfiguration.NationalLoanDefaultRateLevel);
        }

        [TestMethod]
        public void NullNationalLoanDefaultRate_NullNationalLoanDefaultRateLevelTest()
        {
            configurationDto.NationalLoanDefaultRate = null;
            Assert.IsNull(shoppingSheetConfiguration.NationalLoanDefaultRateLevel);
        }

        [TestMethod]
        public void NationalLoanDefaultRateLevel_IsLowTest()
        {
            configurationDto.NationalLoanDefaultRate = 46;
            configurationDto.LoanDefaultRate = 67;
            Assert.IsTrue(shoppingSheetConfiguration.NationalLoanDefaultRateLevel == RateLevel.Low);
        }

        [TestMethod]
        public void NationalLoanDefaultRateLevel_IsHighTest()
        {
            configurationDto.NationalLoanDefaultRate = 87;
            configurationDto.LoanDefaultRate = 34;
            Assert.IsTrue(shoppingSheetConfiguration.NationalLoanDefaultRateLevel == RateLevel.High);
        }

        [TestMethod]
        public void LoanDefaultRateLevel_IsMediumTest()
        {
            configurationDto.NationalLoanDefaultRate = (decimal)0.4;
            configurationDto.LoanDefaultRate = (decimal)0.4;
            Assert.IsTrue(shoppingSheetConfiguration.NationalLoanDefaultRateLevel == RateLevel.Medium);
        }
    }

    [TestClass]
    public class RepaymentRateLevelTests
    {
        public TestModelData testModelData;

        public Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration configurationDto;

        public ShoppingSheetConfiguration shoppingSheetConfiguration
        {
            get
            { return new ShoppingSheetConfiguration(configurationDto, string.Empty); }
        }

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0003914");
            configurationDto = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations.Select(c => c.ShoppingSheetConfiguration)).FirstOrDefault(c => c != null);
        }

        [TestMethod]
        public void NullInstitutionRepaymentRate_NullInstitutionRepaymentRateLevelTest()
        {
            configurationDto.InstitutionRepaymentRate = null;
            Assert.IsNull(shoppingSheetConfiguration.InstitutionRepaymentRateLevel);
        }

        [TestMethod]
        public void NullNationalRepaymentRateAverage_NullNationalRepaymentRateLevelTest()
        {
            configurationDto.NationalRepaymentRateAverage = null;
            Assert.IsNull(shoppingSheetConfiguration.NationalRepaymentRateLevel);
        }

        [TestMethod]
        public void RepaymentRatesEqual_NullNationaAndInstitutionalRepaymentRateLevelsTest()
        {
            configurationDto.InstitutionRepaymentRate = 50;
            configurationDto.NationalRepaymentRateAverage = 50;
            Assert.IsNull(shoppingSheetConfiguration.NationalRepaymentRateLevel);
            Assert.IsNull(shoppingSheetConfiguration.InstitutionRepaymentRateLevel);
        }

        [TestMethod]
        public void LowerInstitutionalRepaymentRate_LowInstitutionalRepaymentRateLevelTest()
        {
            configurationDto.InstitutionRepaymentRate = 40;
            configurationDto.NationalRepaymentRateAverage = 50;
            Assert.AreEqual(RateLevel.Low, shoppingSheetConfiguration.InstitutionRepaymentRateLevel);
        }

        [TestMethod]
        public void HigherInstitutionalRepaymentRate_HighInstitutionalRepaymentRateLevelTest()
        {
            configurationDto.InstitutionRepaymentRate = 60;
            configurationDto.NationalRepaymentRateAverage = 50;
            Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.InstitutionRepaymentRateLevel);
        }

        [TestMethod]
        public void LowerNationalRepaymentRate_LowNationalRepaymentRateLevelTest()
        {
            configurationDto.InstitutionRepaymentRate = 40;
            configurationDto.NationalRepaymentRateAverage = 5.8m;
            Assert.AreEqual(RateLevel.Low, shoppingSheetConfiguration.NationalRepaymentRateLevel);
        }

        [TestMethod]
        public void HigherNationalRepaymentRate_HighNationalRepaymentRateLevelTest()
        {
            configurationDto.InstitutionRepaymentRate = 40;
            configurationDto.NationalRepaymentRateAverage = 99.9m;
            Assert.AreEqual(RateLevel.High, shoppingSheetConfiguration.NationalRepaymentRateLevel);
        }
    }
}
