﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Awards
{

    [TestClass]
    public class LoanRequirementsTests
    {
        private TestModelData testModelDataObject;
        private AwardsViewModel awardsViewModel;

        [TestInitialize]
        public void Initialize()
        {
            testModelDataObject = new TestModelData("0003914");
            BuildAwardsViewModel();

        }

        /// <summary>
        /// Test if the loanSummary object is not null and created with correct attrs (passed)
        /// </summary>
        [TestMethod]
        public void DtoData_LoanRequirements()
        {
            Assert.IsNotNull(awardsViewModel.LoanRequirements);
            Assert.AreEqual(awardsViewModel.LoanRequirements.StudentId, testModelDataObject.studentLoanSummaryData.StudentId);
            Assert.AreEqual(awardsViewModel.LoanRequirements.ActiveDirectLoanMpnExpirationDate, testModelDataObject.studentLoanSummaryData.DirectLoanMpnExpirationDate.Value.ToShortDateString());
            Assert.AreEqual(awardsViewModel.LoanRequirements.DirectLoanEntranceInterviewDate, testModelDataObject.studentLoanSummaryData.DirectLoanEntranceInterviewDate.Value.ToShortDateString());
        }

        /// <summary>
        /// Test if the loan summary object is created without any data
        /// </summary>
        [TestMethod]
        public void NoData_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary();

            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.LoanRequirements);
        }

        /// <summary>
        /// Test if the entrance interview date sets correctly (no date)
        /// </summary>
        [TestMethod]
        public void EntranceInterviewDateNoData_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary();

            BuildAwardsViewModel();

            Assert.IsTrue(awardsViewModel.LoanRequirements.DirectLoanEntranceInterviewDate == null);
        }

        /// <summary>
        /// Test if entrance interview date sets correctly (with the date provided)
        /// </summary>
        [TestMethod]
        public void EntranceInterviewDateProvided_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary()
            {
                DirectLoanMpnExpirationDate = null,
                StudentId = "0003914",
                DirectLoanEntranceInterviewDate = new DateTime(2014, 01, 17)
            };

            BuildAwardsViewModel();

            var date = new DateTime(2014, 01, 17);
            Assert.AreEqual(date.ToShortDateString(), awardsViewModel.LoanRequirements.DirectLoanEntranceInterviewDate);
        }

        /// <summary>
        /// Test if the mpn expiration date sets correctly (no date)
        /// </summary>
        [TestMethod]
        public void MpnExpirationDateNoData_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary();

            BuildAwardsViewModel();

            Assert.IsTrue(awardsViewModel.LoanRequirements.ActiveDirectLoanMpnExpirationDate == null);
        }

        /// <summary>
        /// Test if mpn expiration date sets correctly (with the date provided)
        /// </summary>
        [TestMethod]
        public void MpnExpirationDateProvided_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary()
            {
                DirectLoanMpnExpirationDate = new DateTime(2014, 08, 15),
                StudentId = "0003914",
                DirectLoanEntranceInterviewDate = new DateTime(2014, 01, 17)
            };

            BuildAwardsViewModel();

            var date = new DateTime(2014, 08, 15);
            Assert.AreEqual(date.ToShortDateString(), awardsViewModel.LoanRequirements.ActiveDirectLoanMpnExpirationDate);
        }

        /// <summary>
        /// Test if interview status is calculated correctly (Incomplete)
        /// </summary>
        [TestMethod]
        public void InterviewStatusIncomplete_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary();

            BuildAwardsViewModel();

            Assert.IsTrue(awardsViewModel.LoanRequirements.DirectLoanEntranceInterviewStatus == "Incomplete");

        }

        /// <summary>
        /// Test if interview status is calculated correctly (Complete)
        /// </summary>
        [TestMethod]
        public void InterviewStatusComplete_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary()
            {
                DirectLoanMpnExpirationDate = null,
                StudentId = "0003914",
                DirectLoanEntranceInterviewDate = new DateTime(2014, 01, 13)
            };

            BuildAwardsViewModel();

            Assert.IsTrue(awardsViewModel.LoanRequirements.DirectLoanEntranceInterviewStatus == "Complete");
        }

        /// <summary>
        /// Test if mpn status is calculated correctly ('incomplete': no date at all)
        /// </summary>
        [TestMethod]

        public void MpnStatusIncompleteNoExpirationDate_LoanRequirements()
        {

            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary()
            {
                DirectLoanMpnExpirationDate = null,
                StudentId = "0003914",
                DirectLoanEntranceInterviewDate = null
            };

            BuildAwardsViewModel();

            Assert.IsTrue(awardsViewModel.LoanRequirements.ActiveDirectLoanMpnStatus == "Incomplete");

        }

        /// <summary>
        /// Test if mpn status is calculated correctly (date exists but earlier than present date)
        /// </summary>
        [TestMethod]
        public void MpnStatusIncompleteExpirationDateEarlierThanToday_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary()
            {
                DirectLoanMpnExpirationDate = new DateTime(2013, 01, 01),
                StudentId = "0003914",
                DirectLoanEntranceInterviewDate = null
            };

            BuildAwardsViewModel();

            Assert.IsTrue(awardsViewModel.LoanRequirements.ActiveDirectLoanMpnStatus == "Incomplete");

        }

        /// <summary>
        /// Test if mpn status is calculated correctly (date exists and is greater than present date)
        /// </summary>
        [TestMethod]
        public void MpnStatusComplete_LoanRequirements()
        {
            testModelDataObject.studentLoanSummaryData = new Colleague.Dtos.FinancialAid.StudentLoanSummary()
            {
                DirectLoanMpnExpirationDate = new DateTime(2020, 01, 01),
                StudentId = "0003914",
                DirectLoanEntranceInterviewDate = null
            };

            BuildAwardsViewModel();

            Assert.IsTrue(awardsViewModel.LoanRequirements.ActiveDirectLoanMpnStatus == "Complete");
        }

        private void BuildAwardsViewModel()
        {
            awardsViewModel = new AwardsViewModel(
                testModelDataObject.applicantData,
                testModelDataObject.studentAwardsData,
                testModelDataObject.studentLoanLimitationsData,
                testModelDataObject.studentLoanSummaryData,
                testModelDataObject.awardsData,
                testModelDataObject.awardPeriodsData,
                testModelDataObject.awardStatusData,
                testModelDataObject.studentAwardYearsData,
                testModelDataObject.studentDocumentsData,
                new List<AwardLetter2>(),
                testModelDataObject.communicationCodesData,
                testModelDataObject.officeCodesData,
                testModelDataObject.studentFaApplicationsData,
                testModelDataObject.financialAidCounselorData,
                testModelDataObject.financialAidOfficesData,
                testModelDataObject.studentLoanRequestsData,
                testModelDataObject.awardPackageChangeRequestData,
                testModelDataObject.studentChecklistsData,
                testModelDataObject.financialAidChecklistItemsData,
                testModelDataObject.academicProgressEvaluationsData,
                testModelDataObject.academicProgressStatusesData,
                testModelDataObject.currentUser,
                false);
        }
    }
}