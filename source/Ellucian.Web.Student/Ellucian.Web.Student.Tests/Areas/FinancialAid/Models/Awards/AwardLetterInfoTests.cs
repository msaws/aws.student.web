﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Awards
{
    [TestClass]
    public class AwardLetterInfoTests
    {
        private Ellucian.Web.Student.Areas.FinancialAid.Models.AwardYear awardYearModel;
        private FinancialAidConfiguration3 configuration;
        private List<Colleague.Dtos.FinancialAid.StudentAward> studentAwards;
        private List<AwardStatus> awardStatuses;
        private AwardLetter2 awardLetter;

        private TestModelData testModelData;

        private AwardLetterInfo awardLetterInfo;

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0004791");

            awardYearModel = new Student.Areas.FinancialAid.Models.AwardYear(){
                Code = "2015"
            };

            configuration = new FinancialAidConfiguration3()
            {
                IsAwardLetterActive = true
            };

            studentAwards = testModelData.studentAwardsData.Where(a => a.AwardYearId == awardYearModel.Code).ToList();

            awardStatuses = testModelData.awardStatusData;

            awardLetter = testModelData.awardLetterHistoryRecordsData.First(al => al.AwardLetterYear == awardYearModel.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullAwardYear_ThrowsArgumentNullExceptionTest()
        {
            new AwardLetterInfo(null, configuration, studentAwards, awardStatuses, awardLetter);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NullAwardYearCode_ThrowsArgumentExceptionTest()
        {
            awardYearModel.Code = null;
            new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
        }

        [TestMethod]
        public void AwardYearCode_EqualsExpectedTest()
        {
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.AreEqual(awardYearModel.Code, awardLetterInfo.AwardYearCode);
        }

        [TestMethod]
        public void NoConfigurationForTheYear_IsAwardLetterAvailableIsFalseTest(){
            awardLetterInfo = new AwardLetterInfo(awardYearModel, null, studentAwards, awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.IsAwardLetterAvailable);
        }

        [TestMethod]
        public void NotIsAwardLetterActive_NotIsAwardLetterAvailableTest()
        {
            configuration.IsAwardLetterActive = false;
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.IsAwardLetterAvailable);
        }

        [TestMethod]
        public void NotArePrerequisitesSatisfied_NotIsAwardLetterAvailableTest()
        {
            awardYearModel.ArePrerequisitesSatisfied = false;
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.IsAwardLetterAvailable);
        }

        [TestMethod]
        public void IsAwardLetterActiveAndArePrerequisitesSatisfied_IsAwardLetterAvailableTest()
        {
            awardYearModel.ArePrerequisitesSatisfied = true;
            configuration.IsAwardLetterActive = true;
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsTrue(awardLetterInfo.IsAwardLetterAvailable);
        }

        [TestMethod]
        public void NoAwardLetter_NotIsAwardLetterAcceptedTest()
        {
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, null);
            Assert.IsFalse(awardLetterInfo.IsAwardLetterAccepted);
        }

        [TestMethod]
        public void NoAcceptedDate_NotIsAwardLetterAcceptedTest()
        {
            awardLetter.AcceptedDate = null;
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.IsAwardLetterAccepted);
        }

        [TestMethod]
        public void AcceptedDatePresent_IsAwardLetterAcceptedTest()
        {
            awardLetter.AcceptedDate = DateTime.Today;
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsTrue(awardLetterInfo.IsAwardLetterAccepted);
        }

        [TestMethod]
        public void NoneOfPendingAwardsAreIgnored_DoesAnyAwardRequireActionReturnsTrueTest()
        {
            studentAwards.ForEach(a => a.StudentAwardPeriods.ForEach(ap => { ap.AwardStatusId = "P"; ap.IsIgnoredOnAwardLetter = false; }));
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsTrue(awardLetterInfo.DoesAnyAwardRequireAction);
        }

        [TestMethod]
        public void AllOfPendingAwardsAreIgnored_DoesAnyAwardRequireActionReturnsFalseTest()
        {
            studentAwards.ForEach(a => a.StudentAwardPeriods.ForEach(ap => { ap.AwardStatusId = "P"; ap.IsIgnoredOnAwardLetter = true; }));
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.DoesAnyAwardRequireAction);
        }

        [TestMethod]
        public void NoPendingAwards_DoesAnyAwardRequireActionReturnsFalseTest()
        {
            studentAwards.ForEach(a => a.StudentAwardPeriods.ForEach(ap => ap.AwardStatusId = "A"));
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.DoesAnyAwardRequireAction);
        }

        [TestMethod]
        public void OnePendingNonIgnoredAward_DoesAnyAwardRequireActionReturnsTrueTest()
        {
            studentAwards.First(a => a.AwardYearId == awardYearModel.Code).StudentAwardPeriods.First().AwardStatusId = "E"; 
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsTrue(awardLetterInfo.DoesAnyAwardRequireAction);
        }

        [TestMethod]
        public void OnePendingIgnoredAward_DoesAnyAwardRequireActionReturnsFalseTest()
        {
            studentAwards.ForEach(a => a.StudentAwardPeriods.ForEach(ap => ap.AwardStatusId = "A"));
            var awardPeriod = studentAwards.First(a => a.AwardYearId == awardYearModel.Code).StudentAwardPeriods.First();
            awardPeriod.IsIgnoredOnAwardLetter = true;
            awardPeriod.AwardStatusId = "P";
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.DoesAnyAwardRequireAction);
        }

        [TestMethod]
        public void NoStudentAwardsForYear_DoesAnyAwardRequireActionReturnsFalseTest()
        {
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, new List<Colleague.Dtos.FinancialAid.StudentAward>(), awardStatuses, awardLetter);
            Assert.IsFalse(awardLetterInfo.DoesAnyAwardRequireAction);
        }

        [TestMethod]
        public void NoAwardStatuses_DoesAnyAwardRequireActionReturnsFalseTest()
        {
            awardLetterInfo = new AwardLetterInfo(awardYearModel, configuration, studentAwards, new List<AwardStatus>(), awardLetter);
            Assert.IsFalse(awardLetterInfo.DoesAnyAwardRequireAction);
        }
    }
}
