﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Awards
{
    [TestClass]
    public class LoanCollectionModelTests
    {
        private string studentId;
        private TestModelData testModelData;

        private string awardYearCode;
        private List<StudentAward> loans;
        private bool isStatusModifiable;
        private bool IsLoanCollectionInReview;
        private int maximumAmount;
        private List<LoanPeriod> loanPeriods;

        private string unknownStatusDescription;

        private LoanCollection loanCollection;
        
        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);

            awardYearCode = testModelData.studentAwardYearsData.First().Code;
            loans = new List<StudentAward>();

            var studentAwardYearDto = testModelData.studentAwardYearsData.First();
            var configurationForYear = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code);
            foreach (var studentAwardDto in testModelData.studentAwardsData.Where(y => y.AwardYearId == awardYearCode))
            {
                loans.Add(new StudentAward(
                    new AwardYear(studentAwardYearDto),
                    studentAwardDto,
                    testModelData.awardsData.First(a => a.Code == studentAwardDto.AwardId),
                    testModelData.awardPeriodsData,
                    testModelData.awardStatusData));
            }
            isStatusModifiable = true;
            IsLoanCollectionInReview = true;
            maximumAmount = 666;
            loanPeriods = new List<LoanPeriod>()
            {
                new LoanPeriod() {Code = "Period1", Description="Period One", IsActive = true, IsStatusModifiable = true, StartDate = DateTime.Today.AddDays(-1)},
                new LoanPeriod() {Code = "Period2", Description = "Period Two", IsActive = true, IsStatusModifiable = true, StartDate = DateTime.Today}
            };

            unknownStatusDescription = "Unknown";

            loanCollection = new LoanCollection();
        }

        [TestMethod]
        public void LoansListInitializedTest()
        {
            Assert.IsNotNull(loanCollection.Loans);
            Assert.AreEqual(0, loanCollection.Loans.Count());
        }

        [TestMethod]
        public void AwardYearCodeGetSetTest()
        {
            loanCollection.AwardYearCode = awardYearCode;
            Assert.AreEqual(awardYearCode, loanCollection.AwardYearCode);
        }

        [TestMethod]
        public void StudentIdGetSetTest()
        {
            loanCollection.StudentId = studentId;
            Assert.AreEqual(studentId, loanCollection.StudentId);
        }

        [TestMethod]
        public void LoansGetSetTest()
        {
            loanCollection.Loans = loans;
            CollectionAssert.AreEqual(loans.ToArray(), loanCollection.Loans.ToArray());
        }

        [TestMethod]
        public void IsStatusModifiableGetSetTest()
        {
            loanCollection.IsStatusModifiable = isStatusModifiable;
            Assert.AreEqual(isStatusModifiable, loanCollection.IsStatusModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_NullLoansReturnsFalseTest()
        {
            loanCollection.Loans = null;
            Assert.IsFalse(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_EmptyLoansReturnsFalseTest()
        {
            loanCollection.Loans = new List<StudentAward>();
            Assert.IsFalse(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_NullAwardPeriodsReturnsFalseTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods = null);
            loanCollection.Loans = loans;
            Assert.IsFalse(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_EmptyAwardPeriodsReturnsFalseTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods = new List<StudentAwardPeriod>());
            loanCollection.Loans = loans;
            Assert.IsFalse(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_NoAwardPeriodsAreModifiableTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods.ForEach(p => p.IsAmountModifiable = false));
            loanCollection.Loans = loans;
            Assert.IsFalse(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_OneAwardPeriodIsModifiableTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods.ForEach(p => p.IsAmountModifiable = false));
            loans.First().StudentAwardPeriods.First().IsAmountModifiable = true;
            loanCollection.Loans = loans;
            Assert.IsTrue(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_OneSetOfAwardPeriodsIsEmptyTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods.ForEach(p => p.IsAmountModifiable = false));
            loans.First().StudentAwardPeriods = new List<StudentAwardPeriod>();
            loans.Last().StudentAwardPeriods.Last().IsAmountModifiable = true;
            loanCollection.Loans = loans;
            Assert.IsTrue(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_OneSetOfAwardPeriodsIsNullTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods.ForEach(p => p.IsAmountModifiable = false));
            loans.First().StudentAwardPeriods = null;
            loans.Last().StudentAwardPeriods.Last().IsAmountModifiable = true;
            loanCollection.Loans = loans;
            Assert.IsTrue(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsAnyAmountModifiable_OneAwardPeriodIsNullTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods.ForEach(p => p.IsAmountModifiable = false));
            loans.First().StudentAwardPeriods[0] = null;
            loans.Last().StudentAwardPeriods.Last().IsAmountModifiable = true;
            loanCollection.Loans = loans;
            Assert.IsTrue(loanCollection.IsAnyAmountModifiable);
        }

        [TestMethod]
        public void IsLoanCollectionInReviewGetSetTest()
        {
            loanCollection.IsLoanCollectionInReview = IsLoanCollectionInReview;
            Assert.AreEqual(IsLoanCollectionInReview, loanCollection.IsLoanCollectionInReview);
        }

        [TestMethod]
        public void StatusDescription_NullLoansReturnsUnknownTest()
        {
            loanCollection.Loans = null;
            Assert.AreEqual(unknownStatusDescription, loanCollection.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_EmptyLoansReturnsUnknownTest()
        {
            loanCollection.Loans = new List<StudentAward>();
            Assert.AreEqual(unknownStatusDescription, loanCollection.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_NullStudentAwardPeriodsReturnsUnknownTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods = null);
            foreach (var loan in loans)
            {
                loan.StatusDescription = "";
            }
            loanCollection.Loans = loans;
            Assert.AreEqual(unknownStatusDescription, loanCollection.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_EmptyStudentAwardPeriodsReturnsUnknownTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods = new List<StudentAwardPeriod>());
            foreach (var loan in loans)
            {
                loan.StatusDescription = "";
            }
            loanCollection.Loans = loans;
            Assert.AreEqual(unknownStatusDescription, loanCollection.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_DescriptionFromAwardStatusEvaluatorTest()
        {
            foreach (var loan in loans)
            {
                loan.StatusDescription = "D";
            }
            var expectedDescription = AwardStatusEvaluator.CalculateAwardStatus(loans.SelectMany(l => l.StudentAwardPeriods)).Category.ToString();
            loanCollection.Loans = loans;
            Assert.AreEqual(expectedDescription, loanCollection.StatusDescription);
        }

        /// <summary>
        /// Tests if loanCollection Status description is set to "Completed" if the loan
        /// category has two separate loans statuses of which are mixed: accepted and rejected
        /// </summary>
        [TestMethod]
        public void StatusDescription_ReturnsCompletedStatusTest()
        {
            loanCollection.Loans.Add(new StudentAward()
            {
                Code = "Sub1",
                StudentAwardPeriods = new List<StudentAwardPeriod>()
                {
                    new StudentAwardPeriod()
                    {
                        AwardId = "Sub1",
                        AwardStatus = new Colleague.Dtos.FinancialAid.AwardStatus(){ Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Accepted }
                    }
                },
                StatusDescription = ""
            });

            loanCollection.Loans.Add(new StudentAward()
            {
                Code = "Sub2",
                StudentAwardPeriods = new List<StudentAwardPeriod>()
                {
                    new StudentAwardPeriod()
                    {
                        AwardId = "Sub2",
                        AwardStatus = new Colleague.Dtos.FinancialAid.AwardStatus(){ Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Denied }
                    }
                },
                StatusDescription = ""
            });

            var expectedDescription = "Completed";
            Assert.AreEqual(expectedDescription, loanCollection.StatusDescription);
        }

        /// <summary>
        /// Tests if loanCollection Status description is set to Accepted if the loan
        /// category has one loan statuses of award periods of which are mixed: accepted and rejected
        /// </summary>
        [TestMethod]
        public void StatusDescription_ReturnsCorrectStatusTest()
        {
            loanCollection.Loans.Add(new StudentAward()
            {
                Code = "Sub1",
                StudentAwardPeriods = new List<StudentAwardPeriod>()
                {
                    new StudentAwardPeriod()
                    {
                        AwardId = "Sub1",
                        AwardStatus = new Colleague.Dtos.FinancialAid.AwardStatus(){ Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Accepted }
                    },

                    new StudentAwardPeriod()
                    {
                        AwardId = "Sub1",
                        AwardStatus = new Colleague.Dtos.FinancialAid.AwardStatus(){ Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Denied }
                    }
                },
                StatusDescription = ""
            });

            var expectedDescription = Colleague.Dtos.FinancialAid.AwardStatusCategory.Accepted.ToString();
            Assert.AreEqual(expectedDescription, loanCollection.StatusDescription);
        }

        [TestMethod]
        public void MaximumAmountGetSetTest()
        {
            loanCollection.MaximumAmount = maximumAmount;
            Assert.AreEqual(maximumAmount, loanCollection.MaximumAmount);
        }

        [TestMethod]
        public void LoanPeriodsGetSetTest()
        {
            loanCollection.LoanPeriods = loanPeriods;
            CollectionAssert.AreEqual(loanPeriods.ToArray(), loanCollection.LoanPeriods.ToArray());
        }

        [TestMethod]
        public void LoanCollectionType_NullLoansReturnsNullTypeTest()
        {
            loanCollection.Loans = null;
            Assert.AreEqual(null, loanCollection.LoanCollectionType);
        }

        [TestMethod]
        public void LoanCollectionType_EmptyLoansReturnsNullTypeTest()
        {
            loanCollection.Loans = new List<StudentAward>();
            Assert.AreEqual(null, loanCollection.LoanCollectionType);
        }

        [TestMethod]
        public void LoanCollectionType_ReturnTypeOfFirstLoanTest()
        {
            var expectedLoanType = Ellucian.Colleague.Dtos.FinancialAid.LoanType.GraduatePlusLoan;
            loans.First().LoanType = expectedLoanType;
            loanCollection.Loans = loans;

            Assert.AreEqual(expectedLoanType, loanCollection.LoanCollectionType.Value);
        }

        [TestMethod]
        public void LoanCollectionType_FirstLoanTypeIsNullTest()
        {
            loans.First().LoanType = null;
            loanCollection.Loans = loans;
            Assert.AreEqual(null, loanCollection.LoanCollectionType);
        }

        [TestMethod]
        public void IsAccepted_ReturnsTrueTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods.ForEach(ap => ap.AwardStatus.Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Accepted));
            loanCollection.Loans = loans;
            Assert.IsTrue(loanCollection.IsAccepted);
        }

        [TestMethod]
        public void IsAccepted_ReturnsFalseTest()
        {
            loans.ForEach(l => l.StudentAwardPeriods.ForEach(ap => ap.AwardStatus.Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Estimated));
            loanCollection.Loans = loans;
            Assert.IsFalse(loanCollection.IsAccepted);
        }
    }
}
