﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Awards
{
    [TestClass]
    public class AwardsViewModelTests
    {
        private TestModelData testModelDataObject;
        private AwardsViewModel awardsViewModel;
        private List<StudentDocumentCollection> studentDocumentCollections;
        private ICurrentUser currentUser;
        private bool isUserSelf;

        [TestInitialize]
        public void Initialize()
        {
            testModelDataObject = new TestModelData("0003914");
            currentUser = testModelDataObject.currentUser;
            isUserSelf = true;
            BuildAwardsViewModel();

        }

        [TestMethod]
        public void NoData_InitializedModel()
        {
            testModelDataObject.studentAwardsData = new List<Colleague.Dtos.FinancialAid.StudentAward>();
            testModelDataObject.studentLoanLimitationsData = new List<Colleague.Dtos.FinancialAid.StudentLoanLimitation>();
            testModelDataObject.awardsData = new List<Colleague.Dtos.FinancialAid.Award2>();
            testModelDataObject.awardStatusData = new List<Colleague.Dtos.FinancialAid.AwardStatus>();
            testModelDataObject.awardYearsData = new List<Colleague.Dtos.FinancialAid.AwardYear>();
            testModelDataObject.studentAwardYearsData = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            testModelDataObject.academicProgressEvaluationsData = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            testModelDataObject.academicProgressStatusesData = new List<AcademicProgressStatus>();
            testModelDataObject.financialAidOfficesData = new List<FinancialAidOffice3>();

            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardYears);
            Assert.IsNotNull(awardsViewModel.StudentAwards);
            Assert.IsNotNull(awardsViewModel.AwardPercentageCharts);
            Assert.IsNotNull(awardsViewModel.StudentSubLoans);
            Assert.IsNotNull(awardsViewModel.StudentUnsubLoans);
            Assert.IsNotNull(awardsViewModel.AwardPrerequisites);
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        [TestMethod]
        public void NoStudentAwardData_InitializedModel()
        {
            testModelDataObject.studentAwardsData = new List<Colleague.Dtos.FinancialAid.StudentAward>();

            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardYears);
            Assert.IsNotNull(awardsViewModel.StudentAwards);
            Assert.IsNotNull(awardsViewModel.StudentSubLoans);
            Assert.IsNotNull(awardsViewModel.StudentUnsubLoans);
            Assert.IsNotNull(awardsViewModel.AwardPercentageCharts);
            Assert.IsNotNull(awardsViewModel.SAPStatus);
        }

        [TestMethod]
        public void StudentAwardYearExists_AwardYearDataObjectExistsInModelTest()
        {
            foreach (var year in testModelDataObject.studentAwardYearsData)
            {
                Assert.IsTrue(awardsViewModel.AwardYears.Any(ay => ay.Code == year.Code));
            }
        }

        /// <summary>
        /// Test if adding an active award year to the studentAwardYearsData makes
        /// that year to be added to the top of the award years list in the
        /// created awardsViewModel
        /// </summary>
        [TestMethod]
        public void StudentAwardYearNotExist_NewAwardYearObjectInModelTest()
        {
            testModelDataObject.studentAwardYearsData.Add(new StudentAwardYear2()
            {
                Code = "2020",
                FinancialAidOfficeId = testModelDataObject.financialAidOfficesData.First().Id,
                IsApplicationReviewed = true,
                PendingLoanRequestId = "4",
                StudentId = "0003914"
            });
            testModelDataObject.financialAidOfficesData.First().Configurations.Add(new FinancialAidConfiguration3()
                {
                    OfficeId = testModelDataObject.financialAidOfficesData.First().Id,
                    AwardYear = "2020",
                    IsSelfServiceActive = true
                });

            BuildAwardsViewModel();

            var awardYearModel = awardsViewModel.AwardYears.First();
            Assert.AreEqual("2020", awardYearModel.Code);
            Assert.AreEqual("2020", awardYearModel.Description);
        }

        [TestMethod]
        public void CounselorCreatedForEachYear_AwardsViewModel()
        {
            awardsViewModel.AwardYears.ToList().ForEach(y => Assert.IsNotNull(y.Counselor));
        }

        [TestMethod]
        public void CounselorNotCreated_NoExceptions_AwardsViewModel()
        {
            testModelDataObject.financialAidCounselorData.Name = string.Empty;
            BuildAwardsViewModel();

            awardsViewModel.AwardYears.ToList().ForEach(y => Assert.IsNull(y.Counselor));
        }

        [TestMethod]
        public void SAPStatusCreated_AwardsViewModelTest()
        {
            Assert.IsNotNull(awardsViewModel.SAPStatus);
        }

        [TestMethod]
        public void DistinctPeriods_PeriodDataExistsTest()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var awardPeriods = year.DistinctAwardPeriods;
                foreach (var period in awardPeriods)
                {
                    Assert.IsTrue(testModelDataObject.awardPeriodsData.Any(ap => ap.Code == period.Code));
                }
            }
        }

        [TestMethod]
        public void DistinctPeriods_SamePeriodCodeOneDistinctPeriodTest()
        {
            var anotherStudentAwardPeriod = new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
            {
                StudentId = "0003914",
                AwardYearId = "2014",
                AwardId = "ZEBRA",
                AwardPeriodId = "14/FA",
                AwardStatusId = "A",
                AwardAmount = 3999,
                IsFrozen = false,
                IsTransmitted = false,
                IsAmountModifiable = false
            };

            var award = testModelDataObject.studentAwardsData.FirstOrDefault(sa => (sa.AwardYearId == anotherStudentAwardPeriod.AwardYearId) &&
                                                            sa.AwardId == anotherStudentAwardPeriod.AwardId);
            if (award != null)
            {
                award.StudentAwardPeriods.Add(anotherStudentAwardPeriod);
            }

            BuildAwardsViewModel();

            var awardYearModel = awardsViewModel.AwardYears.FirstOrDefault(ay => ay.Code == anotherStudentAwardPeriod.AwardYearId);
            if (awardYearModel != null)
            {
                Assert.IsTrue(awardYearModel.DistinctAwardPeriods.Distinct().Count() == awardYearModel.DistinctAwardPeriods.Count());
            }
        }

        [TestMethod]
        public void DistinctPeriods_DifferentPeriodCodeDistinctPeriodsTest()
        {
            var awardYearModel = awardsViewModel.AwardYears.FirstOrDefault(ay => ay.Code == "2014");
            var distinctAwardPeriodCountBefore = awardYearModel.DistinctAwardPeriods.Count();

            var anotherStudentAwardPeriod = new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
            {
                StudentId = "0003914",
                AwardYearId = "2014",
                AwardId = "ZEBRA",
                AwardPeriodId = "14/WI",
                AwardStatusId = "A",
                AwardAmount = 3999,
                IsFrozen = false,
                IsTransmitted = false,
                IsAmountModifiable = false
            };

            var anotherPeriod = new Colleague.Dtos.FinancialAid.AwardPeriod()
            {
                Code = "14/WI",
                Description = "Description",
                StartDate = new DateTime(2014, 01, 21)
            };

            testModelDataObject.studentAwardsData.First().StudentAwardPeriods.Add(anotherStudentAwardPeriod);
            testModelDataObject.awardPeriodsData.Add(anotherPeriod);

            BuildAwardsViewModel();

            awardYearModel = awardsViewModel.AwardYears.FirstOrDefault(ay => ay.Code == "2014");

            Assert.AreEqual(distinctAwardPeriodCountBefore, awardYearModel.DistinctAwardPeriods.Count());
            Assert.IsNotNull(awardYearModel.DistinctAwardPeriods.FirstOrDefault(dap => dap.Code == "14/FA"));
            Assert.IsNotNull(awardYearModel.DistinctAwardPeriods.FirstOrDefault(dap => dap.Code == "14/SP"));
        }

        [TestMethod]
        public void DistinctPeriods_SortedTest()
        {
            var studentAwardPeriod1 = new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
            {
                StudentId = "0003914",
                AwardYearId = "2014",
                AwardId = "ZEBRA",
                AwardPeriodId = "14/SP",
                AwardStatusId = "A",
                AwardAmount = 3999,
                IsFrozen = false,
                IsTransmitted = false,
                IsAmountModifiable = false
            };

            var period1 = new Colleague.Dtos.FinancialAid.AwardPeriod()
            {
                Code = "14/SP",
                Description = "Description",
                StartDate = new DateTime(2014, 01, 21)
            };

            var studentAwardPeriod2 = new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
            {
                StudentId = "0003914",
                AwardYearId = "2014",
                AwardId = "ZEBRA",
                AwardPeriodId = "15/SP",
                AwardStatusId = "A",
                AwardAmount = 3999,
                IsFrozen = false,
                IsTransmitted = false,
                IsAmountModifiable = false
            };

            var period2 = new Colleague.Dtos.FinancialAid.AwardPeriod()
            {
                Code = "15/SP",
                Description = "Description",
                StartDate = new DateTime(2015, 01, 21)
            };

            testModelDataObject.studentAwardsData.First().StudentAwardPeriods.Add(studentAwardPeriod1);
            testModelDataObject.awardPeriodsData.Add(period1);

            testModelDataObject.studentAwardsData.First().StudentAwardPeriods.Add(studentAwardPeriod2);
            testModelDataObject.awardPeriodsData.Add(period2);

            BuildAwardsViewModel();

            var awardPeriods = awardsViewModel.AwardYears.First().DistinctAwardPeriods;

            for (int i = 0; i < awardPeriods.Count() - 1; i++)
            {
                var date1 = awardPeriods.ElementAt(i).StartDate;
                var date2 = awardPeriods.ElementAt(i + 1).StartDate;
                var compareValue = DateTime.Compare(date1, date2);

                Assert.IsTrue(compareValue < 0);
            }

        }

        /// <summary>
        /// Test if award period attributes are set correctly
        /// </summary>
        [TestMethod]
        public void StudentAwardPeriod_AttributesTest()
        {
            var studentAwardPeriodDto = testModelDataObject.studentAwardsData.FirstOrDefault(sad => sad.AwardId == "ZEBRA").StudentAwardPeriods.First();
            var awardPeriodDto = testModelDataObject.awardPeriodsData.FirstOrDefault(apd => apd.Code == studentAwardPeriodDto.AwardPeriodId);
            var awardStatusDto = testModelDataObject.awardStatusData.FirstOrDefault(asd => asd.Code == studentAwardPeriodDto.AwardStatusId);

            var studentAwardPeriodModel = awardsViewModel.GetAllStudentAwards()
                .First(say => say.Code == "ZEBRA" && say.AwardYearId == studentAwardPeriodDto.AwardYearId)
                .StudentAwardPeriods.FirstOrDefault(sap => sap.Code == studentAwardPeriodDto.AwardPeriodId);

            if (studentAwardPeriodDto != null && awardPeriodDto != null && awardStatusDto != null && studentAwardPeriodModel != null)
            {
                Assert.AreEqual(studentAwardPeriodDto.AwardPeriodId, studentAwardPeriodModel.Code);
                Assert.AreEqual(studentAwardPeriodDto.IsFrozen, studentAwardPeriodModel.IsFrozen);
                Assert.AreEqual(studentAwardPeriodDto.IsTransmitted, studentAwardPeriodModel.IsTransmitted);
                Assert.AreEqual(studentAwardPeriodDto.IsAmountModifiable, studentAwardPeriodModel.IsAmountModifiable);

                Assert.IsFalse(studentAwardPeriodModel.IsDummy);

                Assert.AreEqual(awardPeriodDto.Description, studentAwardPeriodModel.Description);
                Assert.AreEqual(awardPeriodDto.StartDate, studentAwardPeriodModel.StartDate);

                Assert.AreEqual(awardStatusDto.Code, studentAwardPeriodModel.AwardStatus.Code);
                Assert.AreEqual(awardStatusDto.Description, studentAwardPeriodModel.AwardStatus.Description);
                Assert.AreEqual(awardStatusDto.Category, studentAwardPeriodModel.AwardStatus.Category);
            }

        }

        [TestMethod]
        public void StudentAwardPeriod_AwardAmountHasValueTest()
        {
            var studentAwardPeriodDto = testModelDataObject.studentAwardsData.First().StudentAwardPeriods.First();
            Assert.IsTrue(studentAwardPeriodDto.AwardAmount.HasValue);

            var studentAwardPeriodModel = awardsViewModel.StudentAwards.First().StudentAwardsForYear.First().StudentAwardPeriods.First();
            Assert.AreEqual(studentAwardPeriodDto.AwardAmount.Value, studentAwardPeriodModel.AwardAmount);
        }

        /// <summary>
        /// Test if amount gets set to 0 when the AwardAmount is set to null 
        /// </summary>
        [TestMethod]
        public void StudentAwardPeriod_AwardAmountIsNullTest()
        {
            var studentAwardPeriodDto = testModelDataObject.studentAwardsData.FirstOrDefault(sad => sad.AwardId == "ZEBRA").StudentAwardPeriods.First();
            studentAwardPeriodDto.AwardAmount = null;

            BuildAwardsViewModel();

            var studentAwardPeriodModel = awardsViewModel.GetAllStudentAwards()
                .FirstOrDefault(say => say.Code == "ZEBRA" && say.AwardYearId == studentAwardPeriodDto.AwardYearId)
                .StudentAwardPeriods.First(p => p.Code == studentAwardPeriodDto.AwardPeriodId);

            Assert.AreEqual((decimal)0, studentAwardPeriodModel.AwardAmount);
        }

        [TestMethod]
        public void StudentAwardPeriod_NotRejectedIsActiveTest()
        {
            var awardStatusDto = testModelDataObject.awardStatusData.First();
            Assert.IsTrue(awardStatusDto.Category != AwardStatusCategory.Rejected && awardStatusDto.Category != AwardStatusCategory.Denied);

            var studentAwardPeriodModel = awardsViewModel.StudentAwards.First().StudentAwardsForYear.First().StudentAwardPeriods.First();
            Assert.IsTrue(studentAwardPeriodModel.IsActive);
        }

        /// <summary>
        /// Test if setting an award period's status to "Rejected" makes that
        /// award period in the created student award period model !IsActive
        /// </summary>
        [TestMethod]
        public void StudentAwardPeriod_RejectedIsNotActiveTest()
        {
            var studentAwardModel = awardsViewModel.StudentAwards.First().StudentAwardsForYear.First();
            var studentAwardDto = testModelDataObject.studentAwardsData.First(sad => sad.AwardId == studentAwardModel.Code);

            var studentAwardPeriodModel = studentAwardModel.StudentAwardPeriods.First();
            var studentAwardPeriodDto = studentAwardDto.StudentAwardPeriods.FirstOrDefault(sap => sap.AwardPeriodId == studentAwardPeriodModel.Code);

            studentAwardPeriodDto.AwardStatusId = "D";
            BuildAwardsViewModel();

            studentAwardPeriodModel = awardsViewModel.StudentAwards.First().StudentAwardsForYear.First().StudentAwardPeriods.First();
            Assert.IsFalse(studentAwardPeriodModel.IsActive);
        }

        [TestMethod]
        public void StudentAward_AttributesTest()
        {
            var studentAwardDto = testModelDataObject.studentAwardsData.First(sad => sad.AwardId == "ZEBRA");
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var awardDto = testModelDataObject.awardsData.FirstOrDefault(ad => ad.Code == "ZEBRA");
            var studentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == awardYearDto.Code).StudentAwardsForYear.FirstOrDefault(sa => sa.Code == "ZEBRA");

            Assert.AreEqual(studentAwardDto.AwardYearId, studentAwardModel.AwardYearId);
            Assert.AreEqual(studentAwardDto.StudentId, studentAwardModel.StudentId);
            Assert.AreEqual(studentAwardDto.AwardId, studentAwardModel.Code);
            Assert.AreEqual(studentAwardDto.IsEligible, studentAwardModel.IsEligible);
            Assert.AreEqual(studentAwardDto.IsAmountModifiable, studentAwardModel.IsAmountModifiable);

            Assert.AreEqual(awardDto.Description, studentAwardModel.Description);
            Assert.AreEqual(awardDto.Explanation, studentAwardModel.Explanation);
            Assert.AreEqual(awardDto.LoanType, studentAwardModel.LoanType);
        }

        /// <summary>
        /// Tests if the StatusDescription property of the StudentAward equals to its AwardStatus description -
        /// there are no pending change requests for award
        /// </summary>
        [TestMethod]
        public void StudentAwardStatusDescription_EqualsAwardStatusDescriptionTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var studentAwardDto = testModelDataObject.studentAwardsData.First(sad => sad.AwardId == "UNSUBDL2" && sad.AwardYearId == awardYearDto.Code);

            testModelDataObject.awardPackageChangeRequestData.FirstOrDefault(cr => cr.AwardPeriodChangeRequests.Any(p => p.Status == AwardPackageChangeRequestStatus.Pending) &&
                                    studentAwardDto.AwardId == cr.AwardId && studentAwardDto.AwardYearId == cr.AwardYearId).AwardPeriodChangeRequests.ToList()
                                    .ForEach(p => p.Status = AwardPackageChangeRequestStatus.Accepted);

            BuildAwardsViewModel();
            var studentAwardModel = awardsViewModel.StudentUnsubLoans.FirstOrDefault(ssl => ssl.AwardYearCode == awardYearDto.Code).Loans.FirstOrDefault(l => l.Code == studentAwardDto.AwardId);
            var awardStatus = studentAwardModel.AwardStatus;
            Assert.AreEqual(awardStatus.Description, studentAwardModel.StatusDescription);
        }

        /// <summary>
        /// Tests if the StatusDescription property of the Student award does not equal its awardStatus description - 
        /// there is a pending change request for award
        /// </summary>
        [TestMethod]
        public void StudentAwardStatusDescription_NotEqualsAwardStatusDescriptionTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var studentAwardDto = testModelDataObject.studentAwardsData.First(sad => sad.AwardId == "UNSUBDL2" && sad.AwardYearId == awardYearDto.Code);
            testModelDataObject.awardPackageChangeRequestData.FirstOrDefault(cr => cr.AwardPeriodChangeRequests.Any(p => p.Status == AwardPackageChangeRequestStatus.Pending) &&
                                    studentAwardDto.AwardId == cr.AwardId && studentAwardDto.AwardYearId == cr.AwardYearId).AwardPeriodChangeRequests.ToList()
                                    .ForEach(p => p.Status = AwardPackageChangeRequestStatus.Pending);

            BuildAwardsViewModel();
            var studentAwardModel = awardsViewModel.StudentUnsubLoans.FirstOrDefault(ssl => ssl.AwardYearCode == awardYearDto.Code).Loans.FirstOrDefault(l => l.Code == studentAwardDto.AwardId);
            var awardStatus = studentAwardModel.AwardStatus;
            Assert.AreNotEqual(awardStatus.Description, studentAwardModel.StatusDescription);
        }

        /// <summary>
        /// Tests if StatusDescription property equals AwardStatus description property if there were no
        /// pending change requests
        /// </summary>
        [TestMethod]
        public void NoChangeRequestsData_StatusDescriptionEqualsAwardStatusDescriptionTest()
        {
            testModelDataObject.awardPackageChangeRequestData = new List<AwardPackageChangeRequest>();
            BuildAwardsViewModel();

            foreach (var year in awardsViewModel.StudentAwards)
            {
                foreach (var award in year.StudentAwardsForYear)
                {
                    Assert.AreEqual(award.AwardStatus.Description, award.StatusDescription);
                }
            }
            foreach (var year in awardsViewModel.StudentGradPlusLoans)
            {
                foreach (var loan in year.Loans)
                {
                    Assert.AreEqual(loan.AwardStatus.Description, loan.StatusDescription);
                }
            }
            foreach (var year in awardsViewModel.StudentSubLoans)
            {
                foreach (var loan in year.Loans)
                {
                    Assert.AreEqual(loan.AwardStatus.Description, loan.StatusDescription);
                }
            }
            foreach (var year in awardsViewModel.StudentUnsubLoans)
            {
                foreach (var loan in year.Loans)
                {
                    Assert.AreEqual(loan.AwardStatus.Description, loan.StatusDescription);
                }
            }
        }

        /// <summary>
        /// Test if Loan period exists and was created correctly
        /// </summary>
        [TestMethod]
        public void LoanPeriodCreated_LoanPeriods()
        {

            var actualPeriod = awardsViewModel.StudentUnsubLoans.First().LoanPeriods.First();
            var expectedPeriod = testModelDataObject.awardPeriodsData.FirstOrDefault(p => p.Code == actualPeriod.Code);

            Assert.IsNotNull(expectedPeriod);

            Assert.AreEqual(expectedPeriod.Code, actualPeriod.Code);
            Assert.AreEqual(expectedPeriod.Description, actualPeriod.Description);
        }

        [TestMethod]
        public void EmptyAwardPeriodsAdded_StudentAwardsEachYear()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var distinctPeriodsCount = year.DistinctAwardPeriods.Count();
                var awards = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == year.Code);
                if (awards != null)
                {
                    foreach (var award in awards.StudentAwardsForYear)
                    {
                        Assert.AreEqual(distinctPeriodsCount, award.StudentAwardPeriods.Count());
                        Assert.IsTrue(award.StudentAwardPeriods.Distinct().Count() == award.StudentAwardPeriods.Count());
                    }
                }
            }
        }

        [TestMethod]
        public void EmptyAwardPeriodsAdded_StudentWorkAwardsEachYear()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var distinctPeriodsCount = year.DistinctAwardPeriods.Count();
                var awards = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == year.Code);
                if (awards != null)
                {
                    foreach (var award in awards.StudentWorkAwardsForYear)
                    {
                        Assert.AreEqual(distinctPeriodsCount, award.StudentAwardPeriods.Count());
                        Assert.IsTrue(award.StudentAwardPeriods.Distinct().Count() == award.StudentAwardPeriods.Count());
                    }
                }
            }
        }

        [TestMethod]
        public void EmptyAwardPeriodsAdded_OtherLoansEachYear()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var distinctPeriodsCount = year.DistinctAwardPeriods.Count();
                var awards = awardsViewModel.OtherLoans.FirstOrDefault(sa => sa.AwardYearCode == year.Code);
                if (awards != null)
                {
                    foreach (var award in awards.StudentAwardsForYear)
                    {
                        Assert.AreEqual(distinctPeriodsCount, award.StudentAwardPeriods.Count());
                        Assert.IsTrue(award.StudentAwardPeriods.Distinct().Count() == award.StudentAwardPeriods.Count());
                    }
                }
            }
        }

        [TestMethod]
        public void EmptyAwardPeriodsAdded_StudentSubLoansEachYear()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var distinctPeriodsCount = year.DistinctAwardPeriods.Count();
                var subLoans = awardsViewModel.StudentSubLoans.FirstOrDefault(ssl => ssl.AwardYearCode == year.Code);
                if (subLoans != null)
                {
                    foreach (var award in subLoans.Loans)
                    {
                        Assert.AreEqual(distinctPeriodsCount, award.StudentAwardPeriods.Count());
                        Assert.IsTrue(award.StudentAwardPeriods.Distinct().Count() == award.StudentAwardPeriods.Count());
                    }
                }
            }
        }

        [TestMethod]
        public void EmptyAwardPeriodsAdded_StudentUnsubLoansEachYear()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var distinctPeriodsCount = year.DistinctAwardPeriods.Count();
                var unsubLoans = awardsViewModel.StudentUnsubLoans.FirstOrDefault(ssl => ssl.AwardYearCode == year.Code);
                if (unsubLoans != null)
                {
                    foreach (var award in unsubLoans.Loans)
                    {
                        Assert.AreEqual(distinctPeriodsCount, award.StudentAwardPeriods.Count());
                        Assert.IsTrue(award.StudentAwardPeriods.Distinct().Count() == award.StudentAwardPeriods.Count());
                    }
                }
            }
        }

        [TestMethod]
        public void EmptyAwardPeriodsAdded_SubLoanCollectionLoanPeriodsEachYear()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var distinctPeriodsCount = year.DistinctAwardPeriods.Count();
                var subLoans = awardsViewModel.StudentSubLoans.FirstOrDefault(ssl => ssl.AwardYearCode == year.Code);
                if (subLoans != null)
                {
                    Assert.AreEqual(distinctPeriodsCount, subLoans.LoanPeriods.Count());
                    Assert.IsTrue(subLoans.LoanPeriods.Distinct().Count() == subLoans.LoanPeriods.Count());
                }
            }
        }

        [TestMethod]
        public void EmptyAwardPeriodsAdded_UnsubLoanCollectionLoanPeriodsEachYear()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var distinctPeriodsCount = year.DistinctAwardPeriods.Count();
                var unsubLoans = awardsViewModel.StudentUnsubLoans.FirstOrDefault(sul => sul.AwardYearCode == year.Code);
                if (unsubLoans != null)
                {
                    Assert.AreEqual(distinctPeriodsCount, unsubLoans.LoanPeriods.Count());
                    Assert.IsTrue(unsubLoans.LoanPeriods.Distinct().Count() == unsubLoans.LoanPeriods.Count());
                }
            }
        }

        [TestMethod]
        public void DistinctAwardPeriodsOrdered_StudentAwardYears()
        {
            foreach (var year in awardsViewModel.AwardYears)
            {
                var periods = year.DistinctAwardPeriods.ToList();
                for (int i = 0; i < periods.Count - 1; i++)
                {
                    Assert.IsTrue(periods[i].StartDate <= periods[i + 1].StartDate);
                }
            }
        }

        [TestMethod]
        public void StudentAwardPeriodsOrdered_StudentAwardsEachYear()
        {
            foreach (var year in awardsViewModel.StudentAwards)
            {
                if (year != null)
                {
                    var awards = year.StudentAwardsForYear;
                    foreach (var award in awards)
                    {
                        var periods = award.StudentAwardPeriods.ToList();
                        for (int i = 0; i < periods.Count - 1; i++)
                        {
                            Assert.IsTrue(periods[i].StartDate <= periods[i + 1].StartDate);
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void StudentAwardPeriodsOrdered_StudentOtherLoansEachYear()
        {
            foreach (var year in awardsViewModel.OtherLoans)
            {
                if (year != null)
                {
                    var awards = year.StudentAwardsForYear;
                    foreach (var award in awards)
                    {
                        var periods = award.StudentAwardPeriods.ToList();
                        for (int i = 0; i < periods.Count - 1; i++)
                        {
                            Assert.IsTrue(periods[i].StartDate <= periods[i + 1].StartDate);
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void StudentAwardPeriodsOrdered_StudentSubLoansEachYear()
        {
            foreach (var year in awardsViewModel.StudentSubLoans)
            {
                if (year != null)
                {
                    var awards = year.Loans;
                    foreach (var award in awards)
                    {
                        var periods = award.StudentAwardPeriods.ToList();
                        for (int i = 0; i < periods.Count - 1; i++)
                        {
                            Assert.IsTrue(periods[i].StartDate <= periods[i + 1].StartDate);
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void StudentAwardPeriodsOrdered_StudentUnsubLoansEachYear()
        {
            foreach (var year in awardsViewModel.StudentUnsubLoans)
            {
                if (year != null)
                {
                    var awards = year.Loans;
                    foreach (var award in awards)
                    {
                        var periods = award.StudentAwardPeriods.ToList();
                        for (int i = 0; i < periods.Count - 1; i++)
                        {
                            Assert.IsTrue(periods[i].StartDate <= periods[i + 1].StartDate);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tests if the boolean AreAllAwardsSatisfied is set correctly,
        /// set all checklist items' control statuses to "Skipped",
        /// not all items are completed
        /// </summary>
        [TestMethod]
        public void AreAwardPrerequisitesSatisfied_Satisfied()
        {
            testModelDataObject.studentFaApplicationsData = new List<FinancialAidApplication2>();
            testModelDataObject.studentAwardYearsData.ForEach(year =>
            {
                testModelDataObject.financialAidOfficesData
                    .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == year.FinancialAidOfficeId && c.AwardYear == year.Code).IsProfileActive = false;
                year.IsApplicationReviewed = false;
                testModelDataObject.studentFaApplicationsData.Add(new Fafsa() { AwardYear = year.Code });
                testModelDataObject.studentFaApplicationsData.Add(new ProfileApplication() { AwardYear = year.Code });
            });
            testModelDataObject.studentDocumentsData.ForEach(doc => doc.Status = DocumentStatus.Received);
            testModelDataObject.studentChecklistsData.ForEach(sc => sc.ChecklistItems.ForEach(checklistItem =>
                {
                    checklistItem.ControlStatus = ChecklistItemControlStatus.CompletionRequiredLater;
                }));

            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardPrerequisites);
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), awardsViewModel.AwardPrerequisites.Count());

            Assert.IsTrue(awardsViewModel.AwardPrerequisites.All(prereq => prereq.AreAllPrerequisitesSatisfied));
        }

        /// <summary>
        /// Tests if the boolean AreAllAwardsSatisfied is set correctly,
        /// set all checklist items' control statuses to "Required",
        /// all of the items are completed
        /// </summary>
        [TestMethod]
        public void AreAwardPrerequisitesSatisfied_Satisfied1()
        {
            //set up data so that all the studentawardyear's prereqs are satisfied
            testModelDataObject.studentFaApplicationsData = new List<FinancialAidApplication2>();
            testModelDataObject.studentAwardYearsData.ForEach(year =>
            {
                testModelDataObject.financialAidOfficesData
                    .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == year.FinancialAidOfficeId && c.AwardYear == year.Code).IsProfileActive = true;
                year.IsApplicationReviewed = true;
                testModelDataObject.studentFaApplicationsData.Add(new Fafsa() { AwardYear = year.Code });
                testModelDataObject.studentFaApplicationsData.Add(new ProfileApplication() { AwardYear = year.Code });
            });
            testModelDataObject.studentDocumentsData.ForEach(doc => doc.Status = DocumentStatus.Received);
            testModelDataObject.studentChecklistsData.ForEach(sc => sc.ChecklistItems.ForEach(checklistItem =>
            {
                checklistItem.ControlStatus = ChecklistItemControlStatus.CompletionRequired;
            }));

            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardPrerequisites);
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), awardsViewModel.AwardPrerequisites.Count());

            Assert.IsTrue(awardsViewModel.AwardPrerequisites.All(prereq => prereq.AreAllPrerequisitesSatisfied));
        }

        /// <summary>
        /// Tests that if the Application not reviewed by the office and required, prerequisites are not met,
        /// application reviewed item is set to "Required"
        /// </summary>
        [TestMethod]
        public void AreAwardPrerequisitesSatisfied_ApplicationNotReviewedNotSatisfied()
        {
            testModelDataObject.studentFaApplicationsData = new List<FinancialAidApplication2>();
            testModelDataObject.studentAwardYearsData.ForEach(year =>
                {
                    testModelDataObject.financialAidOfficesData
                    .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == year.FinancialAidOfficeId && c.AwardYear == year.Code).IsProfileActive = true;
                    year.IsApplicationReviewed = false;
                    testModelDataObject.studentFaApplicationsData.Add(new Fafsa() { AwardYear = year.Code });
                    testModelDataObject.studentFaApplicationsData.Add(new ProfileApplication() { AwardYear = year.Code });
                });
            testModelDataObject.studentDocumentsData.ForEach(doc => doc.Status = DocumentStatus.Received);
            testModelDataObject.studentChecklistsData.ForEach(sc => sc.ChecklistItems.Find(cli => cli.Code == "APPLRVW").ControlStatus = ChecklistItemControlStatus.CompletionRequired);

            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardPrerequisites);
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), awardsViewModel.AwardPrerequisites.Count());

            Assert.IsTrue(awardsViewModel.AwardPrerequisites.All(prereq => !prereq.AreAllPrerequisitesSatisfied));
        }

        /// <summary>
        /// Tests that the if Fafsa is not complete and required - prereqs are not satisfied
        /// </summary>
        [TestMethod]
        public void AreAwardPrerequisitesSatisfied_FafsaNotCompleteNotSatisfied()
        {
            testModelDataObject.studentFaApplicationsData = new List<FinancialAidApplication2>();
            testModelDataObject.studentAwardYearsData.ForEach(year =>
            {
                testModelDataObject.financialAidOfficesData
                    .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == year.FinancialAidOfficeId && c.AwardYear == year.Code).IsProfileActive = true;
                year.IsApplicationReviewed = true;
                testModelDataObject.studentFaApplicationsData.Add(new ProfileApplication() { AwardYear = year.Code });
            });
            testModelDataObject.studentDocumentsData.ForEach(doc => doc.Status = DocumentStatus.Received);
            testModelDataObject.studentChecklistsData.ForEach(sc => sc.ChecklistItems.Find(cli => cli.Code == "FAFSA").ControlStatus = ChecklistItemControlStatus.CompletionRequired);
            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardPrerequisites);
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), awardsViewModel.AwardPrerequisites.Count());

            Assert.IsTrue(awardsViewModel.AwardPrerequisites.All(prereq => !prereq.AreAllPrerequisitesSatisfied));
        }

        /// <summary>
        /// Tests that if the Profile is required and incomplete, prereqs are not satisfied
        /// </summary>
        [TestMethod]
        public void AreAwardPrerequisitesSatisfied_ProfileNotCompleteNotSatisfied()
        {
            testModelDataObject.studentFaApplicationsData = new List<FinancialAidApplication2>();
            testModelDataObject.studentAwardYearsData.ForEach(year =>
            {
                testModelDataObject.financialAidOfficesData
                    .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == year.FinancialAidOfficeId && c.AwardYear == year.Code).IsProfileActive = true;
                year.IsApplicationReviewed = true;
                testModelDataObject.studentFaApplicationsData.Add(new Fafsa() { AwardYear = year.Code });
            });
            testModelDataObject.studentDocumentsData.ForEach(doc => doc.Status = DocumentStatus.Received);
            testModelDataObject.studentChecklistsData.ForEach(sc => sc.ChecklistItems.Find(cli => cli.Code == "PROFILE").ControlStatus = ChecklistItemControlStatus.CompletionRequired);
            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardPrerequisites);
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), awardsViewModel.AwardPrerequisites.Count());

            Assert.IsTrue(awardsViewModel.AwardPrerequisites.All(prereq => !prereq.AreAllPrerequisitesSatisfied));
        }

        /// <summary>
        /// Tests that if the Documents are incomplete and required completion, prereqs are not satisfied
        /// </summary>
        [TestMethod]
        public void AreAwardPrerequisitesSatisfied_DocumentsNotCompleteNotSatisfied()
        {
            //set up data so that all the studentawardyear's prereqs are not satisfied
            testModelDataObject.studentFaApplicationsData = new List<FinancialAidApplication2>();
            testModelDataObject.studentAwardYearsData.ForEach(year =>
            {
                testModelDataObject.financialAidOfficesData
                    .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == year.FinancialAidOfficeId && c.AwardYear == year.Code).IsProfileActive = true;
                year.IsApplicationReviewed = true;
                testModelDataObject.studentFaApplicationsData.Add(new Fafsa() { AwardYear = year.Code });
                testModelDataObject.studentFaApplicationsData.Add(new ProfileApplication() { AwardYear = year.Code });
            });

            testModelDataObject.studentDocumentsData.ForEach(doc => doc.Status = DocumentStatus.Incomplete);
            testModelDataObject.studentChecklistsData.ForEach(sc => sc.ChecklistItems.Find(cli => cli.Code == "CMPLREQDOC").ControlStatus = ChecklistItemControlStatus.CompletionRequired);

            BuildAwardsViewModel();

            Assert.IsNotNull(awardsViewModel.AwardPrerequisites);
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), awardsViewModel.AwardPrerequisites.Count());

            Assert.IsTrue(awardsViewModel.AwardPrerequisites.All(prereq => !prereq.AreAllPrerequisitesSatisfied));
        }

        /// <summary>
        /// Tests if GetAllLoanCollections method returns all of the loan collections
        /// </summary>
        [TestMethod]
        public void GetAllLoanCollections_ReturnsAllLoanCollectionsTest()
        {
            var loanCollectionCount = awardsViewModel.StudentGradPlusLoans.Count();
            loanCollectionCount += awardsViewModel.StudentSubLoans.Count();
            loanCollectionCount += awardsViewModel.StudentUnsubLoans.Count();

            Assert.AreEqual(loanCollectionCount, awardsViewModel.GetAllStudentLoanCollections().Count());
        }

        /// <summary>
        /// Tests if the IsStatusModifiable of a studentAwardModel is set to true whenever at least one award period has the 
        /// same property set to true
        /// </summary>
        [TestMethod]
        public void StudentAwardModelModifiableStatus_SetCorrectlyTest()
        {
            var studentAwardDto = testModelDataObject.studentAwardsData.First(sa => sa.AwardId == "WOOFY");
            foreach (var period in studentAwardDto.StudentAwardPeriods)
            {
                period.IsStatusModifiable = false;
            }
            var newStudentAwardPeriod = new Ellucian.Colleague.Dtos.FinancialAid.StudentAwardPeriod()
            {
                AwardId = studentAwardDto.AwardId,
                AwardPeriodId = "new period",
                IsStatusModifiable = true,
                AwardStatusId = "P",
                StudentId = studentAwardDto.StudentId,
                AwardYearId = studentAwardDto.AwardYearId
            };
            studentAwardDto.StudentAwardPeriods.Add(newStudentAwardPeriod);
            testModelDataObject.awardPeriodsData.Add(new AwardPeriod()
            {
                Code = "new period",
                Description = "description",
                StartDate = DateTime.Today
            });
            BuildAwardsViewModel();
            var correspondingStudentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == studentAwardDto.AwardYearId)
                .StudentAwardsForYear.FirstOrDefault(sa => sa.Code == studentAwardDto.AwardId);

            Assert.IsTrue(correspondingStudentAwardModel.IsStatusModifiable);
        }

        #region SAP Status tests
        /// <summary>
        /// Tests if SAP Status object is not created in case there is no award years data
        /// </summary>
        [TestMethod]
        public void NoAwardYearsData_SAPStatusIsNullTest()
        {
            testModelDataObject.studentAwardYearsData = new List<StudentAwardYear2>();
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if SAP Status object is not created if there is no fa office data
        /// </summary>
        [TestMethod]
        public void NoFinancialAidOfficeData_SAPStatusIsNullTest()
        {
            testModelDataObject.financialAidOfficesData = new List<FinancialAidOffice3>();
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if SAP Status object is not created if fa office data is null
        /// </summary>
        [TestMethod]
        public void NullFinancialAidOfficeData_SAPStatusIsNullTest()
        {
            testModelDataObject.financialAidOfficesData = null;
            BuildAwardsViewModel();
            Assert.IsNotNull(awardsViewModel);
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        [TestMethod]
        public void NoAcademicEvaluations_SAPStatusIsNullTest()
        {
            testModelDataObject.academicProgressEvaluationsData = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        [TestMethod]
        public void NullAcademicEvaluations_SAPStatusIsNullTest()
        {
            testModelDataObject.academicProgressEvaluationsData = null;
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if no SAP Status object was created when SAP flag is off
        /// </summary>
        [TestMethod]
        public void SAPStatusIsNull_WhenSAPFlagIsFalseTest()
        {
            var mostRecentAwardYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId).AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = false;
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if SAP Status object was created when SAP flag is on
        /// and no specific types are specified for display
        /// </summary>
        [TestMethod]
        public void SAPStatusIsNotNull_WhenSAPFlagIsTrueTest()
        {
            var mostRecentAwardYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId).AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId).AcademicProgressConfiguration.AcademicProgressTypesToDisplay = new List<string>();
            BuildAwardsViewModel();
            Assert.IsNotNull(awardsViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if no SAP Status was created if all existing evaluations do not match the specified type to display
        /// </summary>
        [TestMethod]
        public void SAPDisplayTypeNoMatch_SAPStatusIsNullTest()
        {
            var mostRecentAwardYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var faOffice = testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);
            faOffice.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            faOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay = new List<string>() { "foo" };
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.SAPStatus);
        }

        [TestMethod]
        public void SAPDisplayTypeMatches_SAPStatusIsNotNullTest()
        {
            var existingType = testModelDataObject.academicProgressEvaluationsData.First().AcademicProgressTypeCode;
            var mostRecentAwardYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var faOffice = testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);
            faOffice.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            faOffice.AcademicProgressConfiguration.AcademicProgressTypesToDisplay = new List<string>() { existingType };

            BuildAwardsViewModel();
            Assert.IsNotNull(awardsViewModel.SAPStatus);
        }

        #endregion

        [TestMethod]
        public void AddDummyAwardPeriodsToWorkAwards_DummyPeriodsAddedTest()
        {
            var awardYear = testModelDataObject.studentAwardYearsData.First();
            
            var newWorkAward = new Award2()
            {
                AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Work,
                Code = "CWS",
                Description = "CWS Description"
            };
            testModelDataObject.awardsData.Add(newWorkAward);

            var newStudentAward = new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
            {
                AwardId = newWorkAward.Code,
                AwardYearId = awardYear.Code,
                IsAmountModifiable = false,
                StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>(){
                    new Ellucian.Colleague.Dtos.FinancialAid.StudentAwardPeriod(){
                        AwardPeriodId = "15/FA",
                        AwardAmount = 2345,
                        AwardYearId = awardYear.Code,
                        IsAmountModifiable = false,
                        StudentId = "0003914",
                        IsStatusModifiable = true,
                        AwardId = "CWS",
                        AwardStatusId = "P"
                    }
                }
            };
            testModelDataObject.studentAwardsData.Add(newStudentAward);
            BuildAwardsViewModel();

            awardsViewModel.AddDummyStudentAwardPeriods(new List<Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod>()
            {
                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = newWorkAward.Code,
                    AwardStatus = new AwardStatus(){},
                    AwardYearId = awardYear.Code,
                    Code = "15GH",
                    IsActive = true,
                    StartDate = new DateTime(2000, 12, 12)
                },

                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = newWorkAward.Code,
                    AwardStatus = new AwardStatus(){},
                    AwardYearId = awardYear.Code,
                    Code = "15GF",
                    IsActive = true,
                    StartDate = new DateTime(2045, 12, 12)
                }
            });
            var distinctPeriodCount = awardsViewModel.AwardYears.First(y => y.Code == awardYear.Code).DistinctAwardPeriods.Count();
            var awardsForYear = awardsViewModel.StudentAwards.First(a => a.AwardYearCode == awardYear.Code);
            var workAward = awardsForYear.StudentWorkAwardsForYear.First(wa => wa.Code == newWorkAward.Code);
            Assert.IsNotNull(workAward);
            Assert.AreEqual(distinctPeriodCount, workAward.StudentAwardPeriods.Count);
        }

        [TestMethod]
        public void AddDummyAwardPeriodsToRandomAward_DummyPeriodsAddedTest()
        {
            var awardYear = testModelDataObject.studentAwardYearsData.First();
            var awardToTest = testModelDataObject.studentAwardsData.First(a => a.AwardYearId == awardYear.Code);

            awardsViewModel.AddDummyStudentAwardPeriods(new List<Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod>()
            {
                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = awardToTest.AwardId,
                    AwardStatus = new AwardStatus(){},
                    AwardYearId = awardYear.Code,
                    Code = "15GH",
                    IsActive = true,
                    StartDate = new DateTime(2000, 12, 12)
                },

                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = awardToTest.AwardId,
                    AwardStatus = new AwardStatus(){},
                    AwardYearId = awardYear.Code,
                    Code = "15GF",
                    IsActive = true,
                    StartDate = new DateTime(2045, 12, 12)
                }
            });

            var distinctPeriodCount = awardsViewModel.AwardYears.First(y => y.Code == awardYear.Code).DistinctAwardPeriods.Count();
            var actualAward = awardsViewModel.GetAllStudentAwards().First(a => a.Code == awardToTest.AwardId);
            Assert.AreEqual(distinctPeriodCount, actualAward.StudentAwardPeriods.Count);
        }

        [TestMethod]
        public void IsProxyView_IsFalseWhenCurrentUserIsNullTest()
        {
            currentUser = null;
            BuildAwardsViewModel();
            Assert.IsFalse(awardsViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyView_IsFalseWhenCurrentUserIsNotProxyTest()
        {
            Assert.IsFalse(awardsViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyView_IsTrueWhenCurrentUserIsProxyTest()
        {
            currentUser = testModelDataObject.currentUserWithProxy;
            BuildAwardsViewModel();
            Assert.IsTrue(awardsViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsUserSelf_IsTrueTest()
        {
            Assert.IsTrue(awardsViewModel.IsUserSelf);
        }

        [TestMethod]
        public void IsUserSelf_IsFalseTest()
        {
            isUserSelf = false;
            BuildAwardsViewModel();
            Assert.IsFalse(awardsViewModel.IsUserSelf);
        }

        [TestMethod]
        public void AwardLetterDataCount_EqualsNumberOfYearsTest()
        {
            Assert.AreEqual(awardsViewModel.AwardYears.Count(), awardsViewModel.AwardLetterData.Count());
        }

        [TestMethod]
        public void AwardLetterData_IsInitializedToEmptyListTest()
        {
            testModelDataObject.studentAwardYearsData = new List<StudentAwardYear2>();
            BuildAwardsViewModel();
            Assert.IsFalse(awardsViewModel.AwardLetterData.Any());
        }

        [TestMethod]
        public void NoPersonData_PersonIdIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.PersonId);
        }

        [TestMethod]
        public void PersonId_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.Id, awardsViewModel.PersonId);
        }

        [TestMethod]
        public void NoPersonData_PersonNameIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.PersonName);
        }

        [TestMethod]
        public void PersonName_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PreferredName, awardsViewModel.PersonName);
        }

        [TestMethod]
        public void NoPersonData_PrivacyStatusCodeIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildAwardsViewModel();
            Assert.IsNull(awardsViewModel.PrivacyStatusCode);
        }

        [TestMethod]
        public void PrivacyStatusCode_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PrivacyStatusCode, awardsViewModel.PrivacyStatusCode);
        }

        #region FormatAwardExplanationTests

        [TestMethod]
        public void LinkOnly_AwardExplanation_ExpectedValueAssignedTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var awardDto = testModelDataObject.awardsData.FirstOrDefault(ad => ad.Code == "ZEBRA");
            awardDto.Explanation = "<a href='https://www.google.com'>here</a>";
            BuildAwardsViewModel();

            var expectedExplanation = "<a href='https://www.google.com' target=_blank>here</a>";
            var studentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == awardYearDto.Code).StudentAwardsForYear.FirstOrDefault(sa => sa.Code == "ZEBRA");
            Assert.AreEqual(expectedExplanation, studentAwardModel.Explanation);
        }

        [TestMethod]
        public void TwoLinksInARow_AwardExplanation_ExpectedValueAssignedTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var awardDto = testModelDataObject.awardsData.FirstOrDefault(ad => ad.Code == "ZEBRA");
            awardDto.Explanation = "<a href='https://www.google.com'>here</a> <a href='https://www.yahoo.com'>there</a>";
            BuildAwardsViewModel();

            var expectedExplanation = "<a href='https://www.google.com' target=_blank>here</a> <a href='https://www.yahoo.com' target=_blank>there</a>";
            var studentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == awardYearDto.Code).StudentAwardsForYear.FirstOrDefault(sa => sa.Code == "ZEBRA");
            Assert.AreEqual(expectedExplanation, studentAwardModel.Explanation);
        }

        [TestMethod]
        public void LinkAtTheBeginningOfText_AwardExplanation_ExpectedValueAssignedTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var awardDto = testModelDataObject.awardsData.FirstOrDefault(ad => ad.Code == "ZEBRA");
            awardDto.Explanation = "<a href='https://www.google.com'>here</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus.";
            BuildAwardsViewModel();

            var expectedExplanation = "<a href='https://www.google.com' target=_blank>here</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus.";
            var studentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == awardYearDto.Code).StudentAwardsForYear.FirstOrDefault(sa => sa.Code == "ZEBRA");
            Assert.AreEqual(expectedExplanation, studentAwardModel.Explanation);
        }

        [TestMethod]
        public void LinkAtTheEndOfText_AwardExplanation_ExpectedValueAssignedTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var awardDto = testModelDataObject.awardsData.FirstOrDefault(ad => ad.Code == "ZEBRA");
            awardDto.Explanation = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus. <a href='https://www.google.com'>here</a>";
            BuildAwardsViewModel();

            var expectedExplanation = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus. <a href='https://www.google.com' target=_blank>here</a>";
            var studentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == awardYearDto.Code).StudentAwardsForYear.FirstOrDefault(sa => sa.Code == "ZEBRA");
            Assert.AreEqual(expectedExplanation, studentAwardModel.Explanation);
        }

        [TestMethod]
        public void LinkInTheMiddleOfText_AwardExplanation_ExpectedValueAssignedTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var awardDto = testModelDataObject.awardsData.FirstOrDefault(ad => ad.Code == "ZEBRA");
            awardDto.Explanation = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. <a href='https://www.google.com'>here</a> Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus.";
            BuildAwardsViewModel();

            var expectedExplanation = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. <a href='https://www.google.com' target=_blank>here</a> Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus.";
            var studentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == awardYearDto.Code).StudentAwardsForYear.FirstOrDefault(sa => sa.Code == "ZEBRA");
            Assert.AreEqual(expectedExplanation, studentAwardModel.Explanation);
        }

        [TestMethod]
        public void MultipleLinks_AwardExplanation_ExpectedValueAssignedTest()
        {
            var awardYearDto = testModelDataObject.awardYearsData.First();
            var awardDto = testModelDataObject.awardsData.FirstOrDefault(ad => ad.Code == "ZEBRA");
            awardDto.Explanation = "<a href='http://m019229:8080/tfs/ProdCollection/ColleagueWebAPI'>click here</a>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. <a href='https://www.google.com'>here</a> Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus. <a href='https://iris2010.ellucian.com/team/technology/Pages/Home.aspx'>or here</a>";
            BuildAwardsViewModel();

            var expectedExplanation = "<a href='http://m019229:8080/tfs/ProdCollection/ColleagueWebAPI' target=_blank>click here</a>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum ante sed blandit convallis. <a href='https://www.google.com' target=_blank>here</a> Phasellus volutpat sapien interdum, scelerisque lectus at, blandit purus. <a href='https://iris2010.ellucian.com/team/technology/Pages/Home.aspx' target=_blank>or here</a>";
            var studentAwardModel = awardsViewModel.StudentAwards.FirstOrDefault(sa => sa.AwardYearCode == awardYearDto.Code).StudentAwardsForYear.FirstOrDefault(sa => sa.Code == "ZEBRA");
            Assert.AreEqual(expectedExplanation, studentAwardModel.Explanation);
        }


        #endregion

        private void BuildAwardsViewModel()
        {
            awardsViewModel = new AwardsViewModel(
                testModelDataObject.studentData,
                testModelDataObject.studentAwardsData,
                testModelDataObject.studentLoanLimitationsData,
                testModelDataObject.studentLoanSummaryData,
                testModelDataObject.awardsData,
                testModelDataObject.awardPeriodsData,
                testModelDataObject.awardStatusData,
                testModelDataObject.studentAwardYearsData,
                testModelDataObject.studentDocumentsData,
                testModelDataObject.awardLetterHistoryRecordsData,
                testModelDataObject.communicationCodesData,
                testModelDataObject.officeCodesData,
                testModelDataObject.studentFaApplicationsData,
                testModelDataObject.financialAidCounselorData,
                testModelDataObject.financialAidOfficesData,
                testModelDataObject.studentLoanRequestsData,
                testModelDataObject.awardPackageChangeRequestData,
                testModelDataObject.studentChecklistsData,
                testModelDataObject.financialAidChecklistItemsData,
                testModelDataObject.academicProgressEvaluationsData,
                testModelDataObject.academicProgressStatusesData,
                currentUser, isUserSelf);
        }

        private void BuildStudentDocumentCollections()
        {
            studentDocumentCollections = new List<StudentDocumentCollection>();
            foreach (var studentAwardYearDto in testModelDataObject.studentAwardYearsData)
            {
                var configurationForYear = testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code);
                studentDocumentCollections.Add(new StudentDocumentCollection(studentAwardYearDto, testModelDataObject.studentDocumentsData,
                    testModelDataObject.communicationCodesData, testModelDataObject.officeCodesData, configurationForYear));

            }
        }
    }
}
