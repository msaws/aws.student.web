﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Security;


namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models
{
    public class TestModelData
    {
        public string studentId;

        public List<Colleague.Dtos.FinancialAid.StudentAward> studentAwardsData;
        public List<StudentAwardYear2> studentAwardYearsData;
        public List<StudentLoanLimitation> studentLoanLimitationsData;
        public StudentLoanSummary studentLoanSummaryData;
        public List<AwardLetter> studentAwardLettersData;
        public List<Award2> awardsData;
        public List<AwardStatus> awardStatusData;
        public List<Colleague.Dtos.FinancialAid.AwardYear> awardYearsData;
        public List<AwardPeriod> awardPeriodsData;
        public Colleague.Dtos.Student.Student studentData;
        public Colleague.Dtos.Student.Applicant applicantData;
        public FinancialAidCounselor financialAidCounselorData;
        public List<FinancialAidOffice3> financialAidOfficesData;
        public List<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentsData;
        public List<CommunicationCode2> communicationCodesData;
        public List<OfficeCode> officeCodesData;
        public List<FinancialAidApplication2> studentFaApplicationsData;
        public List<AverageAwardPackage> averageAwardPackageData;
        public List<Link> usefulLinksData;
        public List<IpedsInstitution> ipedsInstitutionData;
        public List<StudentLoanRequest> studentLoanRequestsData;
        public List<Colleague.Dtos.FinancialAid.LoanRequest> pendingStudentLoanRequestsData;
        public CorrespondenceOptionData correspondenceOptionData;
        public List<Colleague.Dtos.FinancialAid.ShoppingSheet> shoppingSheetData;
        public Colleague.Dtos.Base.Institution institutionData;
        public List<Colleague.Dtos.FinancialAid.AwardPackageChangeRequest> awardPackageChangeRequestData;
        public List<Colleague.Dtos.FinancialAid.StudentFinancialAidChecklist> studentChecklistsData;
        public List<Colleague.Dtos.FinancialAid.ChecklistItem> financialAidChecklistItemsData;
        public List<Colleague.Dtos.Base.PhoneNumber> counselorsPhoneNumbersData;
        public List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluationsData;
        public List<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatusesData;
        public List<Colleague.Dtos.Student.AcademicProgram> academicProgramsData;
        public List<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode> academicProgressAppealCodesData;
        public List<Colleague.Dtos.FinancialAid.FinancialAidCounselor> appealsCounselorsData;
        public List<Colleague.Dtos.FinancialAid.AwardLetter2> awardLetterHistoryRecordsData;
        public List<Colleague.Dtos.FinancialAid.AwardLetterConfiguration> awardLetterConfigurationsData;
        public ICurrentUser currentUser;
        public ICurrentUser currentUserWithProxy;
        public List<Colleague.Dtos.FinancialAid.StudentDefaultAwardPeriod> studentDefaultAwardPeriodData;
        public List<OutsideAward> outsideAwardsData;
        public StudentNsldsInformation studentNsldsData;
        public Colleague.Dtos.Finance.AccountDue.AccountDue studentAccountDueData;

        public TestModelData(string studentId)
        {
            this.studentId = studentId;

            #region StudentData

            studentData = new Colleague.Dtos.Student.Student()
            {
                Id = studentId,
                FirstName = "Matt",
                MiddleName = "Whatever",
                LastName = "DeDiana",
                PreferredName = "Matt DeDiana",
                PreferredAddress = new List<String>() { "Address Line 1", "AddressLine2" },
                PreferredEmailAddress = "matt.dediana@ellucian.com",
                FinancialAidCounselorId = "0010743",
                PrivacyStatusCode = "S"
            };
            #endregion

            #region ApplicantData

            applicantData = new Colleague.Dtos.Student.Applicant()
            {
                Id = studentId,
                FirstName = "Matt",
                MiddleName = "Whatever",
                LastName = "DeDiana",
                PreferredName = "Matt DeDiana",
                PreferredAddress = new List<String>() { "Address Line 1", "AddressLine2" },
                PreferredEmailAddress = "matt.dediana@ellucian.com",
                FinancialAidCounselorId = "0010743",
                PrivacyStatusCode = ""
            };
            #endregion

            #region FinancialAidCounselorData

            financialAidCounselorData = new FinancialAidCounselor()
            {
                Id = "0010743",
                Name = "Mr Counselor",
                EmailAddress = "mr.counselor@ellucian.edu"
            };
            #endregion

            #region FinancialAidOfficesData

            financialAidOfficesData = new List<FinancialAidOffice3>()
            {
                new FinancialAidOffice3()
                {
                    Id = "MAIN",
                    Name = "Ellucian Main Fairfax Campus",
                    AddressLabel = new List<string>() {"4375 Fair Lakes Court", "Fairfax, VA 22033"},
                    DirectorName = "FA Director",
                    EmailAddress = "fa.director@ellucian.edu",
                    PhoneNumber = "800-ELLUCIAN",
                    Configurations = new List<FinancialAidConfiguration3>()
                    {
                        new FinancialAidConfiguration3()
                        {
                            AwardYear = "2012",
                            AwardYearDescription = "2012-2013 Award Year for Main Office",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsShoppingSheetActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            OfficeId = "MAIN",      
                            PaperCopyOptionText = "By selecting this option, I understand...",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 23,
                                LoanDefaultRate = 10,
                                NationalLoanDefaultRate = 46,
                                MedianBorrowingAmount = 25000,
                                MedianMonthlyPaymentAmount = 450,
                                OfficeType = ShoppingSheetOfficeType.AssociateDegreeGranting,
                                EfcOption = ShoppingSheetEfcOption.IsirEfc,
                                CustomMessageRuleTableId = "CustomMessage",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 45.8m,
                                NationalRepaymentRateAverage = 23.8m
                            },
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="BU",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                            
                        },

                        new FinancialAidConfiguration3()
                        {
                            AwardYear = "2013",
                            AwardYearDescription = "2013-2014 Award Year for Main Office",
                            OfficeId = "MAIN",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsShoppingSheetActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            PaperCopyOptionText = "PCO1",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 68,
                                LoanDefaultRate = 28,
                                NationalLoanDefaultRate = 56,
                                MedianBorrowingAmount = 35000,
                                MedianMonthlyPaymentAmount = 678,
                                OfficeType = ShoppingSheetOfficeType.BachelorDegreeGranting,
                                EfcOption = ShoppingSheetEfcOption.ProfileEfc,
                                CustomMessageRuleTableId = "CustomMessage1",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 77.9m,
                                NationalRepaymentRateAverage = 15.0m
                            },                            
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="CP",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                        },

                        new FinancialAidConfiguration3()
                        {
                            AwardYear = "2014",
                            AwardYearDescription = "2014-2015 Award Year for Main Office",
                            OfficeId = "MAIN",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsShoppingSheetActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            PaperCopyOptionText = "PCO2",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 45,
                                LoanDefaultRate = 20,
                                NationalLoanDefaultRate = 35,
                                MedianBorrowingAmount = 10000,
                                MedianMonthlyPaymentAmount = 345,
                                OfficeType = ShoppingSheetOfficeType.CertificateGranting,
                                EfcOption = ShoppingSheetEfcOption.ProfileEfcUntilIsirExists,
                                CustomMessageRuleTableId = "CustomMessage2",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 39.0m,
                                NationalRepaymentRateAverage = 22.0m
                            },
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="CP",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                        },

                         new FinancialAidConfiguration3()
                        {
                            AwardYear = "2015",
                            AwardYearDescription = "2014-2015 Award Year for Main Office",
                            OfficeId = "MAIN",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsShoppingSheetActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            PaperCopyOptionText = "PCO3",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 45,
                                LoanDefaultRate = 20,
                                NationalLoanDefaultRate = 35,
                                MedianBorrowingAmount = 10000,
                                MedianMonthlyPaymentAmount = 345,
                                OfficeType = ShoppingSheetOfficeType.CertificateGranting,
                                EfcOption = ShoppingSheetEfcOption.ProfileEfcUntilIsirExists,
                                CustomMessageRuleTableId = "CustomMessage2",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 98.7m,
                                NationalRepaymentRateAverage = 56.2m
                            },
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="HM",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                        }
                    },
                    AcademicProgressConfiguration = new AcademicProgressConfiguration()
                    {
                        IsSatisfactoryAcademicProgressActive = true,
                        IsSatisfactoryAcademicProgressHistoryActive = true,
                        DetailPropertyConfigurations = new List<AcademicProgressPropertyConfiguration>(){
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Cumulative attempted description",
                                Label = "Cumulative Attempted",
                                Type = AcademicProgressPropertyType.CumulativeAttemptedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Cumulative completed description",
                                Label = "Cumulative Completed",
                                Type = AcademicProgressPropertyType.CumulativeCompletedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Evaluation attempted description",
                                Label = "Evaluation Attempted",
                                Type = AcademicProgressPropertyType.EvaluationPeriodAttemptedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Evaluation completed description",
                                Label = "Evaluation completed",
                                Type = AcademicProgressPropertyType.EvaluationPeriodCompletedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeAttemptedCreditsExcludingRemedial",
                                Label = "CumulativeAttemptedCreditsExcludingRemedial",
                                Type = AcademicProgressPropertyType.CumulativeAttemptedCreditsExcludingRemedial
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeCompletedCreditsExcludingRemedial",
                                Label = "CumulativeCompletedCreditsExcludingRemedial",
                                Type = AcademicProgressPropertyType.CumulativeCompletedCreditsExcludingRemedial
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeRateOfCompletion",
                                Label = "CumulativeRateOfCompletion",
                                Type = AcademicProgressPropertyType.CumulativeRateOfCompletion
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeRateOfCompletionExcludingRemedial",
                                Label = "CumulativeRateOfCompletionExcludingRemedial",
                                Type = AcademicProgressPropertyType.CumulativeRateOfCompletionExcludingRemedial
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "EvaluationPeriodRateOfCompletion",
                                Label = "EvaluationPeriodRateOfCompletion",
                                Type = AcademicProgressPropertyType.EvaluationPeriodRateOfCompletion
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "MaximumProgramCredits",
                                Label = "MaximumProgramCredits",
                                Type = AcademicProgressPropertyType.MaximumProgramCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeOverallGpa",
                                Label = "CumulativeOverallGpa",
                                Type = AcademicProgressPropertyType.CumulativeOverallGpa
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "EvaluationPeriodOverallGpa",
                                Label = "EvaluationPeriodOverallGpa",
                                Type = AcademicProgressPropertyType.EvaluationPeriodOverallGpa
                            }
                            
                        },
                        NumberOfAcademicProgressHistoryRecordsToDisplay = 4,
                        AcademicProgressTypesToDisplay = new List<string>(){
                            "FEDERAL",
                            "FEDERALC"
                        }
                    }
                },
                new FinancialAidOffice3()
                {
                    Id = "LAW",
                    Name = "Ellucian Law Malvern Campus",
                    AddressLabel = new List<string>() {"4 Country View Road", "Malvern, PA 19355"},
                    DirectorName = "JD Director",
                    EmailAddress = "jd.director@ellucian.edu",
                    PhoneNumber = "703-968-9000",
                    Configurations = new List<FinancialAidConfiguration3>()
                    {
                        new FinancialAidConfiguration3()
                        {
                            AwardYear = "2012",
                            AwardYearDescription = "2012-2013 Award Year for Law Office",
                            OfficeId = "LAW",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            IsShoppingSheetActive = true,
                            PaperCopyOptionText = "By selecting this option, I understand...",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 23,
                                LoanDefaultRate = 10,
                                NationalLoanDefaultRate = 46,
                                MedianBorrowingAmount = 25000,
                                MedianMonthlyPaymentAmount = 450,
                                OfficeType = ShoppingSheetOfficeType.GraduateDegreeGranting,
                                EfcOption = ShoppingSheetEfcOption.IsirEfc,
                                CustomMessageRuleTableId = "CM1",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 45.8m,
                                NationalRepaymentRateAverage = 23.8m
                            },
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="CP",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                        },

                        new FinancialAidConfiguration3()
                        {
                            AwardYear = "2013",
                            AwardYearDescription = "2013-2014 Award Year for Law Office",
                            OfficeId = "LAW",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsShoppingSheetActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            PaperCopyOptionText = "Paper copy option text",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 68,
                                LoanDefaultRate = 28,
                                NationalLoanDefaultRate = 56,
                                MedianBorrowingAmount = 35000,
                                MedianMonthlyPaymentAmount = 678,
                                OfficeType = ShoppingSheetOfficeType.NonDegreeGranting,
                                EfcOption = ShoppingSheetEfcOption.ProfileEfc,
                                CustomMessageRuleTableId = "CM2",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 23.8m,
                                NationalRepaymentRateAverage = 65.8m
                            },
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="BU",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                        },

                        new FinancialAidConfiguration3()
                        {
                            AwardYear = "2014",
                            AwardYearDescription = "2014-2015 Award Year for Law Office",
                            OfficeId = "LAW",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsShoppingSheetActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            PaperCopyOptionText = "Paper copy option text1",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 45,
                                LoanDefaultRate = 20,
                                NationalLoanDefaultRate = 35,
                                MedianBorrowingAmount = 10000,
                                MedianMonthlyPaymentAmount = 345,
                                OfficeType = ShoppingSheetOfficeType.AssociateDegreeGranting,
                                EfcOption = ShoppingSheetEfcOption.ProfileEfcUntilIsirExists,
                                CustomMessageRuleTableId = "CM3",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 76.5m,
                                NationalRepaymentRateAverage = 54.8m
                            },
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="CE",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                        },

                        new FinancialAidConfiguration3()
                        {
                            AwardYear = "2015",
                            AwardYearDescription = "2015-2016 Award Year for Law Office",
                            OfficeId = "LAW",
                            IsProfileActive = true,
                            IsSelfServiceActive = true,
                            IsShoppingSheetActive = true,
                            IsAwardingActive = true,
                            IsAwardLetterActive = true,
                            PaperCopyOptionText = "Paper copy option text2",
                            ShoppingSheetConfiguration = new Ellucian.Colleague.Dtos.FinancialAid.ShoppingSheetConfiguration()
                            {
                                GraduationRate = 45,
                                LoanDefaultRate = 20,
                                NationalLoanDefaultRate = 35,
                                MedianBorrowingAmount = 10000,
                                MedianMonthlyPaymentAmount = 345,
                                OfficeType = ShoppingSheetOfficeType.AssociateDegreeGranting,
                                EfcOption = ShoppingSheetEfcOption.ProfileEfcUntilIsirExists,
                                CustomMessageRuleTableId = "CM3",
                                LowToMediumBoundary = 45.5m,
                                MediumToHighBoundary = 55.65m,
                                InstitutionRepaymentRate = 45.8m,
                                NationalRepaymentRateAverage = 23.8m
                            },
                            AcceptedAwardStatusCode = "A",
                            RejectedAwardStatusCode = "R",
                            IsDeclinedStatusChangeRequestRequired = true,
                            IsLoanAmountChangeRequestRequired = true,
                            CounselorPhoneType="CP",
                            AreAwardChangesAllowed = true,
                            AreLoanRequestsAllowed = true,
                            IsAwardLetterHistoryActive = true,
                            SuppressInstanceData = false,
                            SuppressMaximumLoanLimits = false,
                            UseDocumentStatusDescription = true,
                            DisplayPellLifetimeEarningsUsed = true,
                            SuppressAccountSummaryDisplay = true,
                            SuppressAverageAwardPackageDisplay = true,
                            AllowDeclineZeroOfAcceptedLoans = false
                        }
                    },
                    AcademicProgressConfiguration = new AcademicProgressConfiguration()
                    {
                        IsSatisfactoryAcademicProgressActive = true,
                        IsSatisfactoryAcademicProgressHistoryActive = true,
                         DetailPropertyConfigurations = new List<AcademicProgressPropertyConfiguration>(){
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Cumulative attempted description",
                                Label = "Cumulative Attempted",
                                Type = AcademicProgressPropertyType.CumulativeAttemptedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Cumulative completed description",
                                Label = "Cumulative Completed",
                                Type = AcademicProgressPropertyType.CumulativeCompletedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Evaluation attempted description",
                                Label = "Evaluation Attempted",
                                Type = AcademicProgressPropertyType.EvaluationPeriodAttemptedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "Evaluation completed description",
                                Label = "Evaluation completed",
                                Type = AcademicProgressPropertyType.EvaluationPeriodCompletedCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeAttemptedCreditsExcludingRemedial",
                                Label = "CumulativeAttemptedCreditsExcludingRemedial",
                                Type = AcademicProgressPropertyType.CumulativeAttemptedCreditsExcludingRemedial
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeCompletedCreditsExcludingRemedial",
                                Label = "CumulativeCompletedCreditsExcludingRemedial",
                                Type = AcademicProgressPropertyType.CumulativeCompletedCreditsExcludingRemedial
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeRateOfCompletion",
                                Label = "CumulativeRateOfCompletion",
                                Type = AcademicProgressPropertyType.CumulativeRateOfCompletion
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeRateOfCompletionExcludingRemedial",
                                Label = "CumulativeRateOfCompletionExcludingRemedial",
                                Type = AcademicProgressPropertyType.CumulativeRateOfCompletionExcludingRemedial
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "EvaluationPeriodRateOfCompletion",
                                Label = "EvaluationPeriodRateOfCompletion",
                                Type = AcademicProgressPropertyType.EvaluationPeriodRateOfCompletion
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "MaximumProgramCredits",
                                Label = "MaximumProgramCredits",
                                Type = AcademicProgressPropertyType.MaximumProgramCredits
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "CumulativeOverallGpa",
                                Label = "CumulativeOverallGpa",
                                Type = AcademicProgressPropertyType.CumulativeOverallGpa
                            },
                            new AcademicProgressPropertyConfiguration()
                            {
                                IsHidden = false,
                                Description = "EvaluationPeriodOverallGpa",
                                Label = "EvaluationPeriodOverallGpa",
                                Type = AcademicProgressPropertyType.EvaluationPeriodOverallGpa
                            }
                        },
                        NumberOfAcademicProgressHistoryRecordsToDisplay = 2,
                        AcademicProgressTypesToDisplay = new List<string>(){
                            "federal",
                            "federalc",
                            "institutional"
                        }
                    }
                }
            };
            #endregion

            #region StudentDocuments, Comm Codes, Office Codes

            studentDocumentsData = new List<Colleague.Dtos.FinancialAid.StudentDocument>() 
            { 
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "doc1",
                    Status = DocumentStatus.Received,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "docNoYear",
                    Status = DocumentStatus.Received,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocNoDueDate",
                    DueDate = null,
                    Status = DocumentStatus.Incomplete,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocCode1",
                    StatusDate = new DateTime(2014, 02, 15),
                    Status = DocumentStatus.Waived,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocCode2",
                    StatusDate = new DateTime(2014, 02, 15),
                    Status = DocumentStatus.Waived,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocCode3",
                    StatusDate = new DateTime(2014, 02, 15),
                    Status = DocumentStatus.Waived,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocCode4",
                    StatusDate = new DateTime(2014, 03, 15),
                    Status = DocumentStatus.Waived,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocCode5",
                    StatusDate = null,
                    Status = DocumentStatus.Waived,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocPastDueDate",
                    DueDate = new DateTime(2001, 1, 1),
                    Status = DocumentStatus.Incomplete,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocCloserDueDate",
                    DueDate = new DateTime(2014, 07, 31),
                    Status = DocumentStatus.Incomplete,
                    StudentId = studentId
                },
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "DocFurtherDueDate",
                    DueDate = new DateTime(2015, 07, 31),
                    Status = DocumentStatus.Incomplete,
                    StudentId = studentId
                }

            };
            communicationCodesData = new List<CommunicationCode2>()
            {
                new CommunicationCode2()
                {
                    Code = "DocFurtherDueDate",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }

                },
                new CommunicationCode2()
                {
                    Code = "DocCloserDueDate",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = false,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "DocPastDueDate",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "DocCode3",
                    AwardYear = "2012",
                    OfficeCodeId = "FA",
                    IsStudentViewable = false,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "DocCode4",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "DocCode5",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "DocCode2",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>(),
                },
                new CommunicationCode2()
                {
                    Code = "DocCode1",
                    AwardYear = "2013",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "doc1",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "docNoYear",
                    AwardYear = null,
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = null
                },
                new CommunicationCode2()
                {
                    Code = "docNotFa",
                    AwardYear = null,
                    OfficeCodeId = "AR",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
                },
                new CommunicationCode2()
                {
                    Code = "DocNoDueDate",
                    AwardYear = "2014",
                    OfficeCodeId = "FA",
                    IsStudentViewable = true,
                    Description = "Description",
                    Explanation = "Explanation",
                    Hyperlinks = new List<CommunicationCodeHyperlink>()
                    {
                        new CommunicationCodeHyperlink() {Url = "www.ellucian.com", Title = "Ellucian Inc"},
                        new CommunicationCodeHyperlink() {Url = "luci/", Title = "Ellucian Internal"}
                    }
}
            };
            officeCodesData = new List<Colleague.Dtos.Base.OfficeCode>()
            {
                new Colleague.Dtos.Base.OfficeCode()
                {
                    Code = "FA",
                    Type = OfficeCodeType.FinancialAid
                },
                new Colleague.Dtos.Base.OfficeCode()
                {
                    Code = "AR",
                    Type = OfficeCodeType.Other
                }
            };
            #endregion

            #region StudentFaApplicationsData

            studentFaApplicationsData = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>() 
            { 
                new Colleague.Dtos.FinancialAid.Fafsa()
                {
                    AwardYear = "2013",
                    StudentId = studentId,                    
                },
                new Colleague.Dtos.FinancialAid.ProfileApplication()
                {
                    AwardYear = "2013",
                    StudentId = studentId,
                },
                new Colleague.Dtos.FinancialAid.Fafsa()
                {
                    AwardYear = "2014",
                    StudentId = studentId,                    
                },
                new Colleague.Dtos.FinancialAid.ProfileApplication()
                {
                    AwardYear = "2014",
                    StudentId = studentId,
                },
                new Colleague.Dtos.FinancialAid.Fafsa()
                {
                    AwardYear = "2015",
                    StudentId = studentId,                    
                },
                new Colleague.Dtos.FinancialAid.ProfileApplication()
                {
                    AwardYear = "2015",
                    StudentId = studentId,
                }            
            };
            #endregion

            #region StudentLoanLimitationData

            studentLoanLimitationsData = new List<StudentLoanLimitation>()
            {
                new StudentLoanLimitation()
                {
                    StudentId = studentId,
                    AwardYear = "2014",
                    SubsidizedMaximumAmount = 1500,
                    UnsubsidizedMaximumAmount = 2000
                }
            };
            #endregion

            #region StudentLoanSummaryData

            studentLoanSummaryData = new StudentLoanSummary()
            {
                StudentId = studentId,
                DirectLoanMpnExpirationDate = new DateTime(2014, 1, 1),
                DirectLoanEntranceInterviewDate = new DateTime(2013, 12, 31),
                GraduatePlusLoanEntranceInterviewDate = null,
                PlusLoanMpnExpirationDate = new DateTime(2013, 12, 31),
                StudentLoanHistory = new List<StudentLoanHistory>()
                {
                    new StudentLoanHistory()
                    {
                        OpeId = "00100200",                       
                        TotalLoanAmount = 12565
                    },
                    new StudentLoanHistory()
                    {
                        OpeId = "00600898",
                        TotalLoanAmount = 8967
                    }
                },
                StudentLoanCombinedTotalAmount = 25000
            };
            #endregion

            #region IpedsInstitutionData

            ipedsInstitutionData = new List<IpedsInstitution>()
            {
                new IpedsInstitution()
                {
                    OpeId = "00100200",
                    Name = "Amarillo College",
                    UnitId = "123456543"
                },
                new IpedsInstitution()
                {
                    OpeId = "00600898",
                    Name = "Beta University",
                    UnitId = "987654343"
                }
            };
            #endregion

            #region StudentAwards

            //Award codes
            //UNSUBDL
            //SUBDL
            //ZEBRA
            //FWS

            studentAwardsData = new List<Colleague.Dtos.FinancialAid.StudentAward>()
            {
                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                {
                    StudentId = studentId,
                    AwardYearId = "2013",
                    AwardId = "WOOFY",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2013",
                            AwardId = "WOOFY",
                            AwardPeriodId = "13/FA",
                            AwardStatusId = "P",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2013",
                            AwardId = "WOOFY",
                            AwardPeriodId = "14/SP",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        }
                    },
                    PendingChangeRequestId ="1"
                },
                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                {
                    StudentId = studentId,
                    AwardYearId = "2013",
                    AwardId = "PELL",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2013",
                            AwardId = "PELL",
                            AwardPeriodId = "13/FA",
                            AwardStatusId = "P",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2013",
                            AwardId = "PELL",
                            AwardPeriodId = "14/SP",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        }
                    },
                    PendingChangeRequestId = "C"
                },
                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                {
                    StudentId = studentId,
                    AwardYearId = "2014",
                    AwardId = "UNSUBDL",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "UNSUBDL",
                            AwardPeriodId = "14/FA",
                            AwardStatusId = "P",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "UNSUBDL",
                            AwardPeriodId = "15/SP",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        }
                    },
                    PendingChangeRequestId = "1D"
                },

                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                    {
                    StudentId = studentId,
                    AwardYearId = "2015",
                    AwardId = "UNSUBDL2",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2015",
                            AwardId = "UNSUBDL2",
                            AwardPeriodId = "15/FA",
                            AwardStatusId = "P",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2015",
                            AwardId = "UNSUBDL2",
                            AwardPeriodId = "16/SP",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2015",
                            AwardId = "UNSUBDL2",
                            AwardPeriodId = "16/SU",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        }
                    },
                    PendingChangeRequestId = "14"
                },

                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                    {
                    StudentId = studentId,
                    AwardYearId = "2014",
                    AwardId = "UNSUBDL2",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "UNSUBDL2",
                            AwardPeriodId = "14/FA",
                            AwardStatusId = "P",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "UNSUBDL2",
                            AwardPeriodId = "15/SP",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        }
                    },
                    PendingChangeRequestId = "12e"
                },
                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                {
                    StudentId = studentId,
                    AwardYearId = "2014",
                    AwardId = "SUBDL",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "SUBDL",
                            AwardPeriodId = "14/WI",
                            AwardStatusId = "R",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        }                    
                    },
                    PendingChangeRequestId = ""
                    
                },
                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                {
                    StudentId = studentId,
                    AwardYearId = "2014",
                    AwardId = "ZEBRA",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "ZEBRA",
                            AwardPeriodId = "14/FA",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "ZEBRA",
                            AwardPeriodId = "14/SP",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        },

                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2014",
                            AwardId = "ZEBRA",
                            AwardPeriodId = "14/ST",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = true
                        }
                    }
                },

                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward()
                {
                    StudentId = studentId,
                    AwardYearId = "2015",
                    AwardId = "FWS",
                    IsEligible = false,
                    IsAmountModifiable = false,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2015",
                            AwardId = "FWS",
                            AwardPeriodId = "15/FA",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = false
                        }
                    }
                },

                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward(){
                    StudentId = studentId,
                    AwardYearId = "2015",
                    AwardId = "GradPlus",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2015",
                            AwardId = "GradPlus",
                            AwardPeriodId = "15/FA",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = false
                        }
                    }
                },
                new Ellucian.Colleague.Dtos.FinancialAid.StudentAward(){
                    StudentId = studentId,
                    AwardYearId = "2015",
                    AwardId = "OtherLoan",
                    IsEligible = true,
                    IsAmountModifiable = true,
                    StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>() 
                    {
                        new Colleague.Dtos.FinancialAid.StudentAwardPeriod()
                        {
                            StudentId = studentId,
                            AwardYearId = "2015",
                            AwardId = "OtherLoan",
                            AwardPeriodId = "15/FA",
                            AwardStatusId = "A",
                            AwardAmount = 3999,
                            IsFrozen = false,
                            IsTransmitted = false,
                            IsAmountModifiable = false
                        }
                    }
                }
            };


            #endregion

            #region StudentAwardYearsData

            studentAwardYearsData = new List<StudentAwardYear2>()
            {
                new StudentAwardYear2()
                {
                    Code = "2014",
                    FinancialAidOfficeId = "MAIN",
                    StudentId = studentId,
                    IsApplicationReviewed = true,
                    PendingLoanRequestId = "3",
                    EstimatedCostOfAttendance = 20000,
                    TotalAwardedAmount = 10000,
                    IsPaperCopyOptionSelected = true,
                    AwardLetterHistoryItemsForYear = new List<Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem>(){
                        new Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem(){
                            Id = "1",
                            CreatedDate = new DateTime(2014, 08, 09)
                        },
                        new Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem(){
                            Id = "2",
                            CreatedDate = new DateTime(2014, 09, 10)
                        },
                        new Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem(){
                            Id = "r2",
                            CreatedDate = new DateTime(2014, 09, 10)
                        }
                    }
                    
                },
                new StudentAwardYear2()
                {
                    Code = "2013",
                    FinancialAidOfficeId = "MAIN",
                    StudentId = studentId,
                    IsApplicationReviewed = true,
                    PendingLoanRequestId = "2",
                    EstimatedCostOfAttendance = 30000,
                    TotalAwardedAmount = 40000,
                    IsPaperCopyOptionSelected = false,
                    AwardLetterHistoryItemsForYear = new List<Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem>(){
                        new Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem(){
                            Id = "23",
                            CreatedDate = new DateTime(2013, 10, 12)
                        },
                        new Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem(){
                            Id = "24",
                            CreatedDate = new DateTime(2013, 09, 01)
                        }
                    }
                    
                },
                new StudentAwardYear2()
                {
                    Code = "2015",
                    FinancialAidOfficeId = "LAW",
                    StudentId = studentId,
                    IsApplicationReviewed = false,
                    EstimatedCostOfAttendance = 20000,
                    TotalAwardedAmount = 20000,
                    IsPaperCopyOptionSelected = true,
                    AwardLetterHistoryItemsForYear = new List<Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem>(){
                        new Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem(){
                            Id = "15",
                            CreatedDate = new DateTime(2015, 01, 25)
                        },
                        new Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem(){
                            Id = "20",
                            CreatedDate = new DateTime(2015, 12, 12)
                        }
                    }
                    
                }
            };
            #endregion

            #region AwardLetterData
            studentAwardLettersData = new List<AwardLetter>()
            {
                new AwardLetter()
                {
                    AwardYearCode = "2013",
                    StudentId = studentId,
                    AcceptedDate = null,
                    AwardColumnHeader = "Award",
                    AwardPeriod1ColumnHeader = "Column1",
                    AwardPeriod2ColumnHeader = "Column2",
                    AwardPeriod3ColumnHeader = "Column3",
                    AwardPeriod4ColumnHeader = "Column4",
                    AwardPeriod5ColumnHeader = "Column5",
                    AwardPeriod6ColumnHeader = "Column6",
                    AwardTableRows = new List<AwardLetterAward>()
                    {
                        new AwardLetterAward()
                        {
                            AwardDescription = "Description",
                            AwardId = "Award",
                            GroupName = "Group",
                            GroupNumber = 0,
                            Period1Amount = 123,
                            Period2Amount = 234,
                            Period3Amount = 345,
                            Period4Amount = 456,
                            Period5Amount = 567,
                            Period6Amount = 678,
                            Total = (decimal)12345.67
                        },
                        new AwardLetterAward()
                        {
                            AwardDescription = "Total",
                            AwardId = "Total",
                            GroupName = "Group2",
                            GroupNumber = 1,
                            Period1Amount = 123,
                            Period2Amount = 234,
                            Period3Amount = 345,
                            Period4Amount = 456,
                            Period5Amount = 567,
                            Period6Amount = 678,
                            Total = (decimal)12345.67
                        }
                    },
                    AwardYearDescription = "2013/2014 Award Year",
                    BudgetAmount = 1234,
                    ClosingParagraph = "This is the closing Paragraph",
                    ContactAddress = new List<AwardLetterAddress>() 
                    {
                        new AwardLetterAddress() {AddressLine = "Main Office"},
                        new AwardLetterAddress() {AddressLine = "Address Line 1"},
                        new AwardLetterAddress() {AddressLine = "Address Line 2"}
                    },
                    Date = DateTime.Today,
                    EstimatedFamilyContributionAmount = 4321,
                    IsContactBlockActive = true,
                    IsNeedBlockActive = true,
                    NeedAmount = 9876,
                    NumberAwardPeriodColumns = 3,
                    OpeningParagraph = "This is the opening paragraph",
                    StudentName = "Name",
                    StudentAddress = new List<AwardLetterAddress>()
                    {
                        new AwardLetterAddress() {AddressLine = "Student Name"},
                        new AwardLetterAddress() {AddressLine = "Address Line 1"},
                        new AwardLetterAddress() {AddressLine = "Address Line 2"},
                    },

                    TotalColumnHeader = "Total",
                    IsHousingCodeActive = true,
                    HousingCode = Colleague.Dtos.FinancialAid.HousingCode.OffCampus
                },

                new AwardLetter()
                {
                    AwardYearCode = "2014",
                    StudentId = studentId,
                    AcceptedDate = null,
                    AwardColumnHeader = "Award",
                    AwardPeriod1ColumnHeader = "Column1",
                    AwardPeriod2ColumnHeader = "Column2",
                    AwardPeriod3ColumnHeader = "Column3",
                    AwardPeriod4ColumnHeader = "Column4",
                    AwardPeriod5ColumnHeader = "Column5",
                    AwardPeriod6ColumnHeader = "Column6",
                    AwardTableRows = new List<AwardLetterAward>()
                    {
                        new AwardLetterAward()
                        {
                            AwardDescription = "Description",
                            AwardId = "Award",
                            GroupName = "Group",
                            GroupNumber = 0,
                            Period1Amount = 123,
                            Period2Amount = 234,
                            Period3Amount = 345,
                            Period4Amount = 456,
                            Period5Amount = 567,
                            Period6Amount = 678,
                            Total = (decimal)12345.67
                        },
                        new AwardLetterAward()
                        {
                            AwardDescription = "Total",
                            AwardId = "Total",
                            GroupName = "Group2",
                            GroupNumber = 3,
                            Period1Amount = 123,
                            Period2Amount = 234,
                            Period3Amount = 345,
                            Period4Amount = 456,
                            Period5Amount = 567,
                            Period6Amount = 678,
                            Total = (decimal)12345.67
                        }
                    },
                    AwardYearDescription = "2014/2015 Award Year",
                    BudgetAmount = 1234,
                    ClosingParagraph = "This is the closing Paragraph",
                    ContactAddress = new List<AwardLetterAddress>() 
                    {
                        new AwardLetterAddress() {AddressLine = "Address Line 1"},
                        new AwardLetterAddress() {AddressLine = "Address Line 2"}
                    },
                    Date = DateTime.Today,
                    EstimatedFamilyContributionAmount = 4321,
                    IsContactBlockActive = true,
                    IsNeedBlockActive = true,
                    NeedAmount = 9876,
                    NumberAwardPeriodColumns = 3,
                    OpeningParagraph = "This is the opening paragraph",
                    StudentAddress = new List<AwardLetterAddress>()
                    {
                        new AwardLetterAddress() {AddressLine = "Address Line 1"},
                        new AwardLetterAddress() {AddressLine = "Address Line 2"},
                    },
                    StudentName = "Name",
                    TotalColumnHeader = "Total",
                    IsHousingCodeActive = true,
                    HousingCode = Colleague.Dtos.FinancialAid.HousingCode.OnCampus
                },

                new AwardLetter()
                {
                    AwardYearCode = "2015",
                    StudentId = studentId,
                    AcceptedDate = null,
                    AwardColumnHeader = "Award",
                    AwardPeriod1ColumnHeader = "Column1",
                    AwardPeriod2ColumnHeader = "Column2",
                    AwardPeriod3ColumnHeader = "Column3",
                    AwardPeriod4ColumnHeader = "Column4",
                    AwardPeriod5ColumnHeader = "Column5",
                    AwardPeriod6ColumnHeader = "Column6",
                    AwardTableRows = new List<AwardLetterAward>()
                    {
                        new AwardLetterAward()
                        {
                            AwardDescription = "Description",
                            AwardId = "Award",
                            GroupName = "Group",
                            GroupNumber = 0,
                            Period1Amount = 123,
                            Period2Amount = 234,
                            Period3Amount = 345,
                            Period4Amount = 456,
                            Period5Amount = 567,
                            Period6Amount = 678,
                            Total = (decimal)12345.67
                        },
                        new AwardLetterAward()
                        {
                            AwardDescription = "Total",
                            AwardId = "Total",
                            GroupName = "Group2",
                            GroupNumber = 1,
                            Period1Amount = 123,
                            Period2Amount = 234,
                            Period3Amount = 345,
                            Period4Amount = 456,
                            Period5Amount = 567,
                            Period6Amount = 678,
                            Total = (decimal)12345.67
                        }
                    },
                    AwardYearDescription = "2015/2016 Award Year",
                    BudgetAmount = 1234,
                    ClosingParagraph = "This is the closing Paragraph",
                    ContactAddress = new List<AwardLetterAddress>() 
                    {
                        new AwardLetterAddress() {AddressLine = "Address Line 1"},
                        new AwardLetterAddress() {AddressLine = "Address Line 2"}
                    },
                    Date = DateTime.Today,
                    EstimatedFamilyContributionAmount = 4321,
                    IsContactBlockActive = true,
                    IsNeedBlockActive = true,
                    NeedAmount = 9876,
                    NumberAwardPeriodColumns = 3,
                    OpeningParagraph = "This is the opening paragraph",
                    StudentAddress = new List<AwardLetterAddress>()
                    {
                        new AwardLetterAddress() {AddressLine = "Address Line 1"},
                        new AwardLetterAddress() {AddressLine = "Address Line 2"},
                    },
                    StudentName = "Name",
                    TotalColumnHeader = "Total",
                    IsHousingCodeActive = false,
                    HousingCode = Colleague.Dtos.FinancialAid.HousingCode.OffCampus
                }
            };
            #endregion

            #region AverageAwardPackageData

            averageAwardPackageData = new List<AverageAwardPackage>()
            {
                new AverageAwardPackage(){
                    AverageGrantAmount = 5000,
                    AverageLoanAmount = 6000,
                    AverageScholarshipAmount = 7000,
                    AwardYearCode = "2014"
                },

                new AverageAwardPackage(){
                    AverageGrantAmount = 1000,
                    AverageLoanAmount = 2000,
                    AverageScholarshipAmount = 3000,
                    AwardYearCode = "2013"
                },

                new AverageAwardPackage(){
                    AverageGrantAmount = 500,
                    AverageLoanAmount = 600,
                    AverageScholarshipAmount = 700,
                    AwardYearCode = "2012"
                }
            };
            #endregion

            #region UsefulLinksData

            usefulLinksData = new List<Link>()
            {
                new Link()
                {
                    LinkType = LinkTypes.PROFILE,
                    LinkUrl = "https://studentloans.gov/myDirectLoan/index.action",
                    Title = "Complete profile"
                },

                new Link()
                {
                    LinkType = LinkTypes.FAFSA,
                    LinkUrl = "https://fafsa.ed.gov/",
                    Title = "Complete FAFSA"
                },

                new Link()
                {
                    LinkType = LinkTypes.Forecaster,
                    LinkUrl = "https://fafsa.ed.gov/FAFSA/FAFSA/app/f4cForm",
                    Title = "FAFSA4Caster"
                },
                new Link()
                {
                    LinkType = LinkTypes.Form,
                    LinkUrl = "localhost:8080/document",
                    Title = "FA Document"
                },
                new Link()
                {
                    LinkType = LinkTypes.SatisfactoryAcademicProgress,
                    LinkUrl = "localhost:8080/document",
                    Title = "SAP Policy"
                },
                new Link()
                {
                    LinkType = LinkTypes.SatisfactoryAcademicProgress,
                    LinkUrl = "http://google.com",
                    Title = "SAP Appeals Form"
                }
            };
            #endregion

            #region AwardData
            awardsData = new List<Award2>()
            {
                new Award2()
                {
                    Code = "UNSUBDL",
                    Description = "UNSUBDL Description",
                    Explanation = "Explanation",
                    LoanType = LoanType.UnsubsidizedLoan
                },

                new Award2()
                {
                    Code = "UNSUBDL2",
                    Description = "UNSUBDL2 Description",
                    Explanation = "Explanation",
                    LoanType = LoanType.UnsubsidizedLoan
                },

                new Award2()
                {
                    Code = "SUBDL",
                    Description = "SUBDL Description",
                    Explanation = "Explanation",
                    LoanType = LoanType.SubsidizedLoan,
                    AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Loan
                },

                new Award2()
                {
                    Code = "ZEBRA",
                    Description = "ZEBRA Description",
                    Explanation = "Explanation",
                    LoanType = null,
                    AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Scholarship
                },
                new Award2()
                {
                    Code = "WOOFY",
                    Description = "WOOFY Description",
                    Explanation = "Explanation",
                    LoanType = null,
                    AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Scholarship
                },
                new Award2()
                {
                    Code = "PELL",
                    Description = "PELL Description",
                    Explanation = "Explanation",
                    LoanType = null,
                    AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Grant
                },
                new Award2()
                {
                    Code = "FWS",
                    Description = "FWS Description",
                    Explanation = "Explanation",
                    LoanType = null,
                    AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Work
                },

                new Award2(){
                    Code = "GradPlus",
                    Description = "Grad Plus Description",
                    LoanType = LoanType.GraduatePlusLoan,
                    AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Loan
                },

                new Award2(){
                    Code = "OtherLoan",
                    Description = "Other loan Description",
                    LoanType = LoanType.OtherLoan,
                    AwardCategoryType = Ellucian.Colleague.Dtos.FinancialAid.AwardCategoryType.Loan
                }

            };
            #endregion

            #region AwardStatusData
            awardStatusData = new List<AwardStatus>()
            {
                new AwardStatus()
                {
                    Code = "P",
                    Description = "Pending",
                    Category = AwardStatusCategory.Pending
                },
                new AwardStatus()
                {
                    Code = "E",
                    Description = "Estimated",
                    Category = AwardStatusCategory.Estimated
                },
                new AwardStatus()
                {
                    Code = "O",
                    Description = "Offered",
                    Category = AwardStatusCategory.Pending
                },
                new AwardStatus()
                {
                    Code = "Y",
                    Description = "Almost there",
                    Category = AwardStatusCategory.Estimated
                },
                new AwardStatus()
                {
                    Code = "Z",
                    Description = "Zzzzz",
                    Category = AwardStatusCategory.Estimated
                },
                new AwardStatus()
                {
                    Code = "A",
                    Description = "Accepted",
                    Category = AwardStatusCategory.Accepted
                },
                new AwardStatus()
                {
                    Code = "D",
                    Description = "Denied",
                    Category = AwardStatusCategory.Denied
                },
                new AwardStatus()
                {
                    Code = "R",
                    Description = "Rejected",
                    Category = AwardStatusCategory.Rejected
                }                
            };
            #endregion

            #region AwardYearsData

            awardYearsData = new List<Colleague.Dtos.FinancialAid.AwardYear>()
            {
                new Colleague.Dtos.FinancialAid.AwardYear()
                {
                    Code = "2014",
                    Description = "2013/2014 Award Year"
                },
                new Colleague.Dtos.FinancialAid.AwardYear()
                {
                    Code = "2013",
                    Description = "2012/2013 Award Year"
                },
                new Colleague.Dtos.FinancialAid.AwardYear()
                {
                    Code = "2015",
                    Description = "2014/2015 Award Year"
                }
            };
            #endregion

            #region AwardPeriodsData

            awardPeriodsData = new List<AwardPeriod>()
            {
                new AwardPeriod()
                {
                    Code = "13/FA",
                    Description = "13-14 Fall",
                    StartDate = new DateTime(2013, 8, 15)
                },
                new AwardPeriod()
                {
                    Code = "14/SP",
                    Description = "13-14 Spring",
                    StartDate = new DateTime(2014, 1, 15 )
                },
                new AwardPeriod()
                {
                    Code = "14/FA",
                    Description = "14-15 Fall",
                    StartDate = new DateTime(2014, 9, 1)
                },

                new AwardPeriod()
                {
                    Code = "15/SP",
                    Description = "14-15 Spring",
                    StartDate = new DateTime(2015, 1, 1)
                },

                new AwardPeriod()
                {
                    Code = "15/FA",
                    Description = "15-16 Fall",
                    StartDate = new DateTime(2015, 9, 1)
                },

                new AwardPeriod()
                {
                    Code = "16/SP",
                    Description = "15-16 Spring",
                    StartDate = new DateTime(2016, 1, 1)
                },

                new AwardPeriod()
                {
                    Code = "14/ST",
                    Description = "14-15 Summer",
                    StartDate = new DateTime(2014, 6, 1)
                },

                new AwardPeriod()
                {
                    Code = "14/WI",
                    Description = "14-15 Winter",
                    StartDate = new DateTime(2014, 12, 1)
                },

                new AwardPeriod()
                {
                    Code = "16/SU",
                    Description = "15-16 Summer",
                    StartDate = new DateTime(2016, 06, 1)
                }
            };

            #endregion

            #region PendingLoanRequests

            pendingStudentLoanRequestsData = new List<Colleague.Dtos.FinancialAid.LoanRequest>()
            {            
                new Colleague.Dtos.FinancialAid.LoanRequest(){
                    AwardYear = "2013",
                    Id = "2",
                    AssignedToId = "24",
                    RequestDate = new DateTime(2013, 5, 26),
                    Status = LoanRequestStatus.Pending,
                    StatusDate = new DateTime(2013, 7, 1),
                    StudentComments = "Student comment",
                    StudentId = "0003914",
                    TotalRequestAmount = 2000,
                    LoanRequestPeriods = new List<LoanRequestPeriod>()
                    {
                        new LoanRequestPeriod() { Code = "13/FA", LoanAmount = 667},
                        new LoanRequestPeriod() { Code = "13/WI", LoanAmount = 667},
                        new LoanRequestPeriod() { Code = "14/SP", LoanAmount = 666}
                    }
                }, 
   
                new Colleague.Dtos.FinancialAid.LoanRequest(){
                    AwardYear = "2014",
                    Id = "3",
                    AssignedToId = "25",
                    RequestDate = new DateTime(2014, 3, 21),
                    Status = LoanRequestStatus.Pending,
                    StatusDate = new DateTime(2014, 4, 1),
                    StudentComments = "Student comment",
                    StudentId = "0003914",
                    TotalRequestAmount = 5000,
                    LoanRequestPeriods = new List<LoanRequestPeriod>()
                    {
                        new LoanRequestPeriod() { Code = "14/FA", LoanAmount = 1667},
                        new LoanRequestPeriod() { Code = "15/WI", LoanAmount = 1667},
                        new LoanRequestPeriod() { Code = "16/SP", LoanAmount = 1666}
                    }

                }     
            };
            #endregion

            #region ShoppingSheetData
            shoppingSheetData = new List<Colleague.Dtos.FinancialAid.ShoppingSheet>
            {
                new Colleague.Dtos.FinancialAid.ShoppingSheet()
                {
                    AwardYear = "2012",
                    TotalEstimatedCost = 23000,
                    TotalGrantsAndScholarships = 10000,
                    FamilyContribution = 3000,
                    NetCosts = 10000,
                    StudentId = "0003914",
                    Costs = new List<ShoppingSheetCostItem>()
                    {
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.BooksAndSupplies, Cost = 3000},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.HousingAndMeals, Cost = 15000},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.OtherCosts, Cost = 500},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.Transportation, Cost = 1000},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.TuitionAndFees, Cost = 23000},
                    },
                    GrantsAndScholarships = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.OtherGrants, Amount = 0},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.PellGrants, Amount = 3500},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.SchoolGrants, Amount = 2000},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.StateGrants, Amount = 4000},
                    },
                    LoanOptions = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.PerkinsLoans, Amount =2356},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.SubsidizedLoans, Amount = 0},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.UnsubsidizedLoans, Amount = 2000},
                    },
                    WorkOptions = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.WorkStudy, Amount = 6000},
                    }

                },

                new Colleague.Dtos.FinancialAid.ShoppingSheet()
                {
                    AwardYear = "2013",
                    TotalEstimatedCost = 50000,
                    TotalGrantsAndScholarships = 12000,
                    FamilyContribution = 20000,
                    NetCosts = 14000,
                    StudentId = "0003914",
                    Costs = new List<ShoppingSheetCostItem>()
                    {
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.BooksAndSupplies, Cost = 2000},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.HousingAndMeals, Cost = 20000},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.OtherCosts, Cost = 0},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.Transportation, Cost = 234},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.TuitionAndFees, Cost = 1234},
                    },
                    GrantsAndScholarships = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.OtherGrants, Amount = 12345},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.PellGrants, Amount = 0},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.SchoolGrants, Amount = 0},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.StateGrants, Amount = 0},
                    },
                    LoanOptions = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.PerkinsLoans, Amount = 765},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.SubsidizedLoans, Amount = 6785},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.UnsubsidizedLoans, Amount = 876},
                    },
                    WorkOptions = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.WorkStudy, Amount = 0},
                    }
                },

                new Colleague.Dtos.FinancialAid.ShoppingSheet()
                {
                    AwardYear = "2014",
                    TotalEstimatedCost = 15000,
                    TotalGrantsAndScholarships = 0,
                    FamilyContribution = 7500,
                    NetCosts = 3000,
                    StudentId = "0003914",
                                        Costs = new List<ShoppingSheetCostItem>()
                    {
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.BooksAndSupplies, Cost = 1000},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.HousingAndMeals, Cost = 0},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.OtherCosts, Cost = 1423},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.Transportation, Cost = 300},
                        new ShoppingSheetCostItem() {BudgetGroup = ShoppingSheetBudgetGroup.TuitionAndFees, Cost = 12000},
                    },
                    GrantsAndScholarships = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.OtherGrants, Amount = 0},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.PellGrants, Amount = 1000},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.SchoolGrants, Amount = 2354},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.StateGrants, Amount = 876},
                    },
                    LoanOptions = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.PerkinsLoans, Amount = 0},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.SubsidizedLoans, Amount = 0},
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.UnsubsidizedLoans, Amount = 1250},
                    },
                    WorkOptions = new List<ShoppingSheetAwardItem>()
                    {
                        new ShoppingSheetAwardItem() {AwardGroup = ShoppingSheetAwardGroup.WorkStudy, Amount = 4000},
                    }
                }
            };
            #endregion

            #region InstitutionData
            institutionData = new Institution()
            {
                FinancialAidInstitutionName = "Ellucian College",
                IsHostInstitution = true
            };

            #endregion

            #region AwardPackageChangeRequestData

            awardPackageChangeRequestData = new List<AwardPackageChangeRequest>() 
            { 
                new AwardPackageChangeRequest()
                {
                    AwardId = "UNSUBDL2",
                    AwardYearId = "2014",
                    Id = "12e",
                    AwardPeriodChangeRequests = new List<AwardPeriodChangeRequest>(){
                        new AwardPeriodChangeRequest(){
                            AwardPeriodId = "14/FA",
                            NewAmount = 2356,
                            Status = AwardPackageChangeRequestStatus.Pending
                        },
                        new AwardPeriodChangeRequest(){
                            AwardPeriodId = "15/SP",
                            NewAmount = 1000,
                            Status = AwardPackageChangeRequestStatus.Pending
                        }
                    }
                },

                new AwardPackageChangeRequest()
                {
                    AwardId = "UNSUBDL2",
                    AwardYearId = "2015",
                    Id = "14",
                    AwardPeriodChangeRequests = new List<AwardPeriodChangeRequest>(){
                        new AwardPeriodChangeRequest(){
                            AwardPeriodId = "15/FA",
                            NewAmount = 500,
                            Status = AwardPackageChangeRequestStatus.Pending
                        },
                        new AwardPeriodChangeRequest(){
                            AwardPeriodId = "16/SP",
                            NewAmount = 600,
                            Status = AwardPackageChangeRequestStatus.Pending
                        }
                    }
                },

                new AwardPackageChangeRequest()
                {
                    AwardId = "PELL",
                    AwardYearId = "2013",
                    Id = "C",
                    AwardPeriodChangeRequests = new List<AwardPeriodChangeRequest>(){
                        new AwardPeriodChangeRequest(){
                            AwardPeriodId = "13/FA",
                            Status = AwardPackageChangeRequestStatus.Pending
                        },
                        new AwardPeriodChangeRequest(){
                            AwardPeriodId = "14/SP",
                            Status = AwardPackageChangeRequestStatus.Pending
                        }
                    }
                }
            };
            #endregion

            #region StudentChecklistItemsData

            studentChecklistsData = new List<StudentFinancialAidChecklist>()
            {
                new StudentFinancialAidChecklist(){
                    AwardYear = "2014",
                    StudentId = studentId,
                    ChecklistItems = new List<StudentChecklistItem>()
                    {
                        new StudentChecklistItem()
                        {
                            Code = "FAFSA",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired                    
                        },

                        new StudentChecklistItem()
                        {
                            Code = "PROFILE",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequiredLater                    
                        },

                        new StudentChecklistItem()
                        {
                            Code = "CMPLREQDOC",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired                    
                        },

                        new StudentChecklistItem()
                        {
                            Code = "APPLRVW",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired                    
                        },

                        new StudentChecklistItem()
                        {
                            Code = "ACCAWDPKG",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired                    
                        },

                        new StudentChecklistItem()
                        {
                            Code = "SIGNAWDLTR",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired                    
                        }
                    }
                },
                
                new StudentFinancialAidChecklist()
                {
                    AwardYear = "2015",
                    StudentId = studentId,
                    ChecklistItems = new List<StudentChecklistItem>()
                    {
                        new StudentChecklistItem()
                        {
                            Code = "FAFSA",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "PROFILE",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequiredLater
                        },

                        new StudentChecklistItem()
                        {
                            Code = "CMPLREQDOC",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "APPLRVW",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "ACCAWDPKG",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "SIGNAWDLTR",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },
                    }
                },
           
                new StudentFinancialAidChecklist()
                {
                    AwardYear = "2013",
                    StudentId = studentId,
                    ChecklistItems = new List<StudentChecklistItem>()
                    {
                        new StudentChecklistItem()
                        {
                            Code = "FAFSA",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "PROFILE",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequiredLater
                        },

                        new StudentChecklistItem()
                        {
                            Code = "CMPLREQDOC",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "APPLRVW",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "ACCAWDPKG",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        },

                        new StudentChecklistItem()
                        {
                            Code = "SIGNAWDLTR",
                            ControlStatus = ChecklistItemControlStatus.CompletionRequired
                        }
                    }
                }
            };
            #endregion

            #region FinancialAidChecklistItems
            financialAidChecklistItemsData = new List<Colleague.Dtos.FinancialAid.ChecklistItem>()
            {
                new Ellucian.Colleague.Dtos.FinancialAid.ChecklistItem()
                {
                    ChecklistItemCode = "PROFILE",
                    ChecklistItemType = Colleague.Dtos.FinancialAid.ChecklistItemType.PROFILE
                },

                new Ellucian.Colleague.Dtos.FinancialAid.ChecklistItem()
                {
                    ChecklistItemCode = "FAFSA",
                    ChecklistItemType = Colleague.Dtos.FinancialAid.ChecklistItemType.FAFSA
                },

                new Ellucian.Colleague.Dtos.FinancialAid.ChecklistItem()
                {
                    ChecklistItemCode = "APPLRVW",
                    ChecklistItemType = Colleague.Dtos.FinancialAid.ChecklistItemType.ApplicationReview
                },

                new Ellucian.Colleague.Dtos.FinancialAid.ChecklistItem()
                {
                    ChecklistItemCode = "CMPLREQDOC",
                    ChecklistItemType = Colleague.Dtos.FinancialAid.ChecklistItemType.CompletedDocuments
                },

                new Ellucian.Colleague.Dtos.FinancialAid.ChecklistItem()
                {
                    ChecklistItemCode = "ACCAWDPKG",
                    ChecklistItemType = Colleague.Dtos.FinancialAid.ChecklistItemType.ReviewAwardPackage
                },

                new Ellucian.Colleague.Dtos.FinancialAid.ChecklistItem()
                {
                    ChecklistItemCode = "SIGNAWDLTR",
                    ChecklistItemType = Colleague.Dtos.FinancialAid.ChecklistItemType.ReviewAwardLetter
                }

            };

            #endregion

            #region CounselorPhoneNumbers
            counselorsPhoneNumbersData = new List<PhoneNumber>()
            {
                new PhoneNumber()
                {
                    PersonId = financialAidCounselorData.Id,
                    PhoneNumbers = new List<Colleague.Dtos.Base.Phone>()
                    {
                        new Phone(){
                            Extension = "02",
                            Number = "703 000 0000",
                            TypeCode = "BU"
                        },
                        new Phone(){
                            Extension = "",
                            Number = "777 777 7777",
                            TypeCode = "HM"
                        },
                        new Phone(){
                            Extension = "",
                            Number = "555 555 5555",
                            TypeCode = "CP"
                        }
                    }
                },
                new PhoneNumber()
                {
                    PersonId = "0000001",
                    PhoneNumbers = new List<Colleague.Dtos.Base.Phone>()
                    {
                        new Phone(){
                            Extension = "098",
                            Number = "703 259 0000",
                            TypeCode = "BU"
                        },
                        
                        new Phone(){
                            Extension = "",
                            Number = "555 000 5555",
                            TypeCode = "HM"
                        }
                    }
                },
                new PhoneNumber()
                {
                    PersonId = "0000002",
                    PhoneNumbers = new List<Colleague.Dtos.Base.Phone>()
                    {
                        new Phone(){
                            Extension = "02",
                            Number = "000 000 0000",
                            TypeCode = "CP"
                        },
                        new Phone(){
                            Extension = "",
                            Number = "456 567 3745",
                            TypeCode = "BU"
                        },
                        new Phone(){
                            Extension = "",
                            Number = "123 456 7891",
                            TypeCode = "HM"
                        }
                    }
                }
            };
            #endregion

            #region AcademicProgressEvaluations

            academicProgressEvaluationsData = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>()
            {
                new Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2()
                {
                    Detail = new AcademicProgressEvaluationDetail(){
                        CumulativeAttemptedCredits = 7,
                        CumulativeCompletedCredits = 7,                        
                        EvaluationPeriodAttemptedCredits = 7,
                        EvaluationPeriodCompletedCredits = 7,
                        CumulativeAttemptedCreditsExcludingRemedial = 3,
                        CumulativeCompletedCreditsExcludingRemedial = 3,
                        CumulativeCompletedGradePoints = 56,
                        CumulativeRateOfCompletion = 98,
                        CumulativeRateOfCompletionExcludingRemedial = 67,                        
                        EvaluationPeriodCompletedGradePoints = 45,
                        EvaluationPeriodRateOfCompletion = 67,
                        CumulativeOverallGpa = 3.5m,
                        EvaluationPeriodOverallGpa = 3.0m
                    },
                    ProgramDetail = new Colleague.Dtos.FinancialAid.AcademicProgressProgramDetail(){
                        ProgramCode = "CS",
                        ProgramMaxCredits = 120,
                        ProgramMinCredits = 120
                    },
                    EvaluationDateTime = new DateTime(2015, 02, 03),
                    EvaluationPeriodEndDate = null,
                    EvaluationPeriodEndTerm = "2014/FA",
                    EvaluationPeriodStartDate = null,
                    EvaluationPeriodStartTerm = "2013/SP",
                    Id = "456",
                    StatusCode = "P",
                    StudentId = "0003419",
                    ResultAppeals = new List<AcademicProgressAppeal>(){
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "456",
                            Id = "67",
                            AppealCounselorId = "0000001",
                            AppealDate = new DateTime(2015, 07, 20),
                            AppealStatusCode = "CANCELED",
                            StudentId = "0003914"
                        },
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "456",
                            Id = "68",
                            AppealCounselorId = "0000001",
                            AppealDate = new DateTime(2015, 07, 20),
                            AppealStatusCode = "REQUESTED",
                            StudentId = "0003914"
                        }
                    },
                    AcademicProgressTypeCode = "INSTITUTIONAL"
                },
                new Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2()
                {
                    Detail = new AcademicProgressEvaluationDetail(){
                        CumulativeAttemptedCredits = 9,
                        CumulativeCompletedCredits = 5,                        
                        EvaluationPeriodAttemptedCredits = 6,
                        EvaluationPeriodCompletedCredits = 4,
                        CumulativeAttemptedCreditsExcludingRemedial = 0,
                        CumulativeCompletedCreditsExcludingRemedial = 0,
                        CumulativeCompletedGradePoints = 100,
                        CumulativeRateOfCompletion = 67,
                        CumulativeRateOfCompletionExcludingRemedial = 78,                        
                        EvaluationPeriodCompletedGradePoints = 35,
                        EvaluationPeriodRateOfCompletion = 88,
                        CumulativeOverallGpa = 2.3m,
                        EvaluationPeriodOverallGpa = 4m
                    },
                    ProgramDetail = new Colleague.Dtos.FinancialAid.AcademicProgressProgramDetail(){
                        ProgramCode = "CH",
                        ProgramMaxCredits = 160,
                        ProgramMinCredits = 150
                    },
                    EvaluationDateTime = new DateTime(2014, 10, 03),
                    EvaluationPeriodEndDate = new DateTime(2014, 01, 01),
                    EvaluationPeriodEndTerm = null,
                    EvaluationPeriodStartDate = new DateTime(2013, 09, 23),
                    EvaluationPeriodStartTerm = null,
                    Id = "457",
                    StatusCode = "U",
                    StudentId = "0003419",
                    ResultAppeals = new List<AcademicProgressAppeal>(){
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "457",
                            Id = "78",
                            AppealCounselorId = "0000002",
                            AppealDate = new DateTime(2014, 07, 27),
                            AppealStatusCode = "GRANTED",
                            StudentId = "0003914"
                        },
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "457",
                            Id = "56",
                            AppealCounselorId = "0000002",
                            AppealDate = new DateTime(2015, 01, 20),
                            AppealStatusCode = "REQUESTED",
                            StudentId = "0003914"
                        }
                    },
                    AcademicProgressTypeCode = "FEDERAL"
                },
                new Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2()
                {
                    Detail = new AcademicProgressEvaluationDetail(){
                        CumulativeAttemptedCredits = 59,
                        CumulativeCompletedCredits = 59,                        
                        EvaluationPeriodAttemptedCredits = 15,
                        EvaluationPeriodCompletedCredits = 15,
                        CumulativeAttemptedCreditsExcludingRemedial = 2,
                        CumulativeCompletedCreditsExcludingRemedial = 1,
                        CumulativeCompletedGradePoints = 456,
                        CumulativeRateOfCompletion = 99,
                        CumulativeRateOfCompletionExcludingRemedial = 50,                        
                        EvaluationPeriodCompletedGradePoints = 134,
                        EvaluationPeriodRateOfCompletion = 98,
                        CumulativeOverallGpa = 2.9m,
                        EvaluationPeriodOverallGpa = 1m
                    },
                    ProgramDetail = new Colleague.Dtos.FinancialAid.AcademicProgressProgramDetail(){
                        ProgramCode = "PH",
                        ProgramMaxCredits = 125,
                        ProgramMinCredits = 120
                    },
                    EvaluationDateTime = new DateTime(2015, 05, 03),
                    EvaluationPeriodEndDate = new DateTime(2014, 01, 01),
                    EvaluationPeriodEndTerm = "2015/SP",
                    EvaluationPeriodStartDate = new DateTime(2013, 09, 23),
                    EvaluationPeriodStartTerm = "2015/SP",
                    Id = "45",
                    StatusCode = "S",
                    StudentId = "0003419",
                    ResultAppeals = new List<AcademicProgressAppeal>(){
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "45",
                            Id = "99",
                            AppealCounselorId = "0000002",
                            AppealDate = new DateTime(2014, 07, 27),
                            AppealStatusCode = "REQUESTED",
                            StudentId = "0003914"
                        },
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "45",
                            Id = "101",
                            AppealCounselorId = "0000002",
                            AppealDate = new DateTime(2015, 01, 20),
                            AppealStatusCode = "DENIED",
                            StudentId = "0003914"
                        }
                    },
                    AcademicProgressTypeCode = "federalC"
                },
                new Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2()
                {
                    Detail = new AcademicProgressEvaluationDetail(){
                        CumulativeAttemptedCredits = 45,
                        CumulativeCompletedCredits = 39,                        
                        EvaluationPeriodAttemptedCredits = 10,
                        EvaluationPeriodCompletedCredits = 10,
                        CumulativeAttemptedCreditsExcludingRemedial = 2,
                        CumulativeCompletedCreditsExcludingRemedial = 1,
                        CumulativeCompletedGradePoints = 500,
                        CumulativeRateOfCompletion = 90,
                        CumulativeRateOfCompletionExcludingRemedial = 70,                        
                        EvaluationPeriodCompletedGradePoints = 560,
                        EvaluationPeriodRateOfCompletion = 89,
                        CumulativeOverallGpa = 3.5m,
                        EvaluationPeriodOverallGpa = 2.7m
                    },
                    ProgramDetail = new Colleague.Dtos.FinancialAid.AcademicProgressProgramDetail(){
                        ProgramCode = "PSY",
                        ProgramMaxCredits = 125,
                        ProgramMinCredits = 120
                    },
                    EvaluationDateTime = new DateTime(2015, 05, 06),
                    EvaluationPeriodEndDate = new DateTime(2014, 01, 01),
                    EvaluationPeriodEndTerm = "2015/WI",
                    EvaluationPeriodStartDate = new DateTime(2013, 09, 23),
                    EvaluationPeriodStartTerm = "2015/WI",
                    Id = "48",
                    StatusCode = "W",
                    StudentId = "0003419",
                    ResultAppeals = new List<AcademicProgressAppeal>(){
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "48",
                            Id = "12",
                            AppealCounselorId = "0000001",
                            AppealDate = new DateTime(2014, 07, 27),
                            AppealStatusCode = "ACCEPTED",
                            StudentId = "0003914"
                        },
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "48",
                            Id = "13",
                            AppealCounselorId = "0000001",
                            AppealDate = new DateTime(2015, 01, 20),
                            AppealStatusCode = "CANCELED",
                            StudentId = "0003914"
                        }
                    },
                    AcademicProgressTypeCode = "FeDeRaL"
                },
                new Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2()
                {
                    Detail = new AcademicProgressEvaluationDetail(){
                        CumulativeAttemptedCredits = 68,
                        CumulativeCompletedCredits = 67,                        
                        EvaluationPeriodAttemptedCredits = 17,
                        EvaluationPeriodCompletedCredits = 15,
                        CumulativeAttemptedCreditsExcludingRemedial = 2,
                        CumulativeCompletedCreditsExcludingRemedial = 1,
                        CumulativeCompletedGradePoints = 345,
                        CumulativeRateOfCompletion = 79,
                        CumulativeRateOfCompletionExcludingRemedial = 60,                        
                        EvaluationPeriodCompletedGradePoints = 564,
                        EvaluationPeriodRateOfCompletion = 78,
                        CumulativeOverallGpa = 2.9m,
                        EvaluationPeriodOverallGpa = 3m
                    },
                    ProgramDetail = new Colleague.Dtos.FinancialAid.AcademicProgressProgramDetail(){
                        ProgramCode = "MU",
                        ProgramMaxCredits = 125,
                        ProgramMinCredits = 120
                    },
                    EvaluationDateTime = new DateTime(2015, 06, 03),
                    EvaluationPeriodEndDate = new DateTime(2014, 01, 01),
                    EvaluationPeriodEndTerm = "2014/SP",
                    EvaluationPeriodStartDate = new DateTime(2013, 09, 23),
                    EvaluationPeriodStartTerm = "2015/SP",
                    Id = "49",
                    StatusCode = "P",
                    StudentId = "0003419",
                    ResultAppeals = new List<AcademicProgressAppeal>(){
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "49",
                            Id = "56",
                            AppealCounselorId = "0000002",
                            AppealDate = new DateTime(2014, 07, 27),
                            AppealStatusCode = "REQUESTED",
                            StudentId = "0003914"
                        },
                        new AcademicProgressAppeal(){
                            AcademicProgressEvaluationId = "49",
                            Id = "79",
                            AppealCounselorId = "0000001",
                            AppealDate = new DateTime(2015, 01, 20),
                            AppealStatusCode = "GRANTED",
                            StudentId = "0003914"
                        }
                    },
                    AcademicProgressTypeCode = "instiTutioNal"
                }
            };
            #endregion

            #region AcademicProgressStatuses

            academicProgressStatusesData = new List<AcademicProgressStatus>()
            {
                new AcademicProgressStatus()
                {
                    Code = "U",
                    Description = "Unsatisfactory",
                    Category = AcademicProgressStatusCategory.Unsatisfactory
                },

                new AcademicProgressStatus()
                {
                    Code = "S",
                    Description = "Satisfactory",
                    Category = AcademicProgressStatusCategory.Satisfactory
                },
                new AcademicProgressStatus()
                {
                    Code = "P",
                    Description = "Probation",
                    Category = AcademicProgressStatusCategory.Unsatisfactory
                },
                new AcademicProgressStatus()
                {
                    Code = "X",
                    Description = "Unable to calculate",
                    Category = AcademicProgressStatusCategory.DoNotDisplay
                },
                new AcademicProgressStatus()
                {
                    Code = "W",
                    Description = "Warning",
                    Category = AcademicProgressStatusCategory.Warning
                }
            };
            #endregion

            #region AcademicPrograms

            academicProgramsData = new List<AcademicProgram>()
            {
                new AcademicProgram()
                {
                    Code = "CS",
                    Description = "Computer Science"
                },
                new AcademicProgram()
                {
                    Code = "CH",
                    Description = "Chemistry"
                },
                new AcademicProgram()
                {
                    Code = "PH",
                    Description = "Physics"
                },
                new AcademicProgram()
                {
                    Code = "PSY",
                    Description = "Psychology"
                },
                new AcademicProgram()
                {
                    Code = "MU",
                    Description = "Music"
                }
            };

            #endregion

            #region AcademicProgressAppealCodes

            academicProgressAppealCodesData = new List<AcademicProgressAppealCode>()
            {
                new AcademicProgressAppealCode(){
                    Code = "CANCELED",
                    Description = "Appeal Canceled"
                },
                new AcademicProgressAppealCode(){
                    Code = "DENIED",
                    Description = "Appeal Denied"
                },
                new AcademicProgressAppealCode(){
                    Code = "ACCEPTED",
                    Description = "Appeal Accepted"
                },
                new AcademicProgressAppealCode(){
                    Code = "REQUESTED",
                    Description = "Appeal Requested"
                },
                new AcademicProgressAppealCode(){
                    Code = "IN REVIEW",
                    Description = "Appeal in Review"
                }
            };

            #endregion

            #region AppealsCounselorsData

            appealsCounselorsData = new List<FinancialAidCounselor>(){
                new FinancialAidCounselor(){
                    Id = "0000001",
                    Name = "Noah Thorne",
                    EmailAddress = "noah.thorne@university.edu"
                },
                new FinancialAidCounselor(){
                    Id = "0000002",
                    Name="Victoria Hadock",
                    EmailAddress = "victoria.hadock@institution.edu"
                }
            };
            #endregion

            #region AwardLetterHistoryRecords

            awardLetterHistoryRecordsData = new List<AwardLetter2>()
            {
                new AwardLetter2(){
                    Id = "r1",
                    AwardLetterParameterId = "config1",
                    StudentId = "0004791",
                    AwardLetterYear = "2015",
                    AwardYearDescription = "2015/2016 award year",
                    HousingCode = HousingCode.OffCampus,
                    EstimatedFamilyContributionAmount = 15000,
                    AwardLetterAnnualAwards = new List<AwardLetterAnnualAward>()
                    {
                         new AwardLetterAnnualAward()
                         {
                             AwardId = "ZEBRA",
                             AnnualAwardAmount = 9268,
                             AwardDescription = "Zebra description",
                             AwardLetterAwardPeriods = new List<AwardLetterAwardPeriod>(){
                                new AwardLetterAwardPeriod(){
                                    AwardId = "ZEBRA",
                                    GroupName = "Scholarships and grants",
                                    GroupNumber = 1,
                                    AwardPeriodAmount = 256,
                                    ColumnName = "Fall",
                                    ColumnNumber = 1
                                },

                                new AwardLetterAwardPeriod(){
                                    AwardId = "ZEBRA",
                                    GroupName = "Scholarships and grants",
                                    GroupNumber = 1,
                                    AwardPeriodAmount = 256,
                                    ColumnName = "Spring",
                                    ColumnNumber = 2
                                },

                                new AwardLetterAwardPeriod(){
                                    AwardId = "ZEBRA",
                                    GroupName = "Scholarships and grants",
                                    GroupNumber = 1,
                                    AwardPeriodAmount = 256,
                                    ColumnName = "Summer",
                                    ColumnNumber = 3
                                },

                                new AwardLetterAwardPeriod(){
                                    AwardId = "FWS",
                                    GroupName = "Work",
                                    GroupNumber = 2,
                                    AwardPeriodAmount = 5000,
                                    ColumnName = "Fall",
                                    ColumnNumber = 1
                                },

                                new AwardLetterAwardPeriod(){
                                    AwardId = "GradPlus",
                                    GroupName = "Loans",
                                    GroupNumber = 3,
                                    AwardPeriodAmount = 3500,
                                    ColumnName = "Summer",
                                    ColumnNumber = 3
                                }
                            }
                        }
                    },
                    BudgetAmount = 40000,
                    ClosingParagraph = "Closing paragraph",
                    OpeningParagraph = "Opening paragraph",
                    AwardLetterGroups = new List<AwardLetterGroup>(),
                    StudentAddress = new List<AwardLetterAddress>(){
                        new AwardLetterAddress(){
                            AddressLine = "4375 Fairfax Ct"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "Fairfax, VA"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "22033"
                        }
                    },
                    ContactAddress = new List<AwardLetterAddress>(){
                        new AwardLetterAddress(){
                            AddressLine = "140 Van Cortlandt Park"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "Bronx, NY"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "10463"
                        }
                    }
                },

                new AwardLetter2(){
                    Id = "r2",
                    AwardLetterParameterId = "config2",
                    StudentId = "0004791",
                    AwardLetterYear = "2014",
                    AwardYearDescription = "2014/2015 award year",
                    HousingCode = HousingCode.OnCampus,
                    EstimatedFamilyContributionAmount = 10000,
                    AwardLetterAnnualAwards = new List<AwardLetterAnnualAward>(){
                        new AwardLetterAnnualAward(){
                            AwardId = "ZEBRA",
                            AnnualAwardAmount = 4012,
                            AwardDescription = "Zebra description",
                            AwardLetterAwardPeriods = new List<AwardLetterAwardPeriod>(){
                        
                                new AwardLetterAwardPeriod(){
                                    AwardId = "ZEBRA",
                                    GroupName = "Scholarships and grants",
                                    GroupNumber = 1,
                                    AwardPeriodAmount = 256,
                                    ColumnName = "Spring",
                                    ColumnNumber = 2
                                },

                                new AwardLetterAwardPeriod(){
                                    AwardId = "ZEBRA",
                                    GroupName = "Scholarships and grants",
                                    GroupNumber = 1,
                                    AwardPeriodAmount = 256,
                                    ColumnName = "Summer",
                                    ColumnNumber = 3
                                },
                        
                                new AwardLetterAwardPeriod(){
                                    AwardId = "GradPlus",
                                    GroupName = "Loans",
                                    GroupNumber = 3,
                                    AwardPeriodAmount = 3500,
                                    ColumnName = "Summer",
                                    ColumnNumber = 3
                                }
                            },
                            GroupName = "Group 1",
                            GroupNumber = 1
                        }
                    },
                    BudgetAmount = 30000,
                    ClosingParagraph = "Closing paragraph 1",
                    OpeningParagraph = "Opening paragraph 1",
                    AwardLetterGroups = new List<AwardLetterGroup>(){
                        new AwardLetterGroup(){
                            GroupName = "Group 1",
                            GroupNumber = 1,
                            GroupType = GroupType.AwardCategories
                        }
                    },
                    StudentAddress = new List<AwardLetterAddress>(){
                        new AwardLetterAddress(){
                            AddressLine = "4375 Fairfax Ct"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "Fairfax, VA"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "22033"
                        }
                    },
                    ContactAddress = new List<AwardLetterAddress>(){
                        new AwardLetterAddress(){
                            AddressLine = "140 Van Cortlandt Park"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "Bronx, NY"
                        },
                        new AwardLetterAddress(){
                            AddressLine = "10463"
                        }
                    }
                }
            };

            #endregion

            #region AwardLetterConfigurations

            awardLetterConfigurationsData = new List<AwardLetterConfiguration>()
            {
                new AwardLetterConfiguration(){
                    Id = "config1",
                    IsContactBlockActive = true,
                    IsHousingBlockActive = false,
                    IsNeedBlockActive = true,
                    AwardTableTitle = "Awards",
                    AwardTotalTitle = "Total",
                    ParagraphSpacing = "2"
                },

                new AwardLetterConfiguration(){
                    Id = "config2",
                    IsContactBlockActive = false,
                    IsHousingBlockActive = false,
                    IsNeedBlockActive = true,
                    AwardTableTitle = "Awarding",
                    AwardTotalTitle = "Total amount",
                    ParagraphSpacing = "1"
                },

                new AwardLetterConfiguration(){
                    Id = "config3",
                    IsContactBlockActive = true,
                    IsHousingBlockActive = true,
                    IsNeedBlockActive = true,
                    AwardTableTitle = "Awards and Loans",
                    AwardTotalTitle = "Totals",
                    ParagraphSpacing = ""
                }
            };

            #endregion

            #region CurrentUser 
            currentUser = new CurrentUser(new Claims()
            {
                ControlId = "231",
                PersonId = studentId,
                SecurityToken = "fs546",
                Name = studentData.FirstName,
                SessionTimeout = 30,
                UserName = "student",
                SessionFixationId = "786",
                Roles = new List<string>() { "STUDENT"}
            });

            currentUserWithProxy = new CurrentUser(new Claims()
            {
                ControlId = "231",
                PersonId = studentId,
                SecurityToken = "fs546",
                Name = studentData.FirstName,
                SessionTimeout = 30,
                UserName = "student",
                SessionFixationId = "786",
                Roles = new List<string>() { "STUDENT" },
                ProxySubjectClaims = new ProxySubjectClaims()
                {
                    PersonId = "foo"
                }
            });
            #endregion

            #region StudentDefaultAwardPeriodData

            studentDefaultAwardPeriodData = new List<StudentDefaultAwardPeriod>()
            {
                new StudentDefaultAwardPeriod(){
                    StudentId = studentId,
                    AwardYear = "2015",
                    DefaultAwardPeriods = new List<string>(){"15/FA", "15/SU", "16/SP"}
                },

                new StudentDefaultAwardPeriod(){
                    StudentId = studentId,
                    AwardYear = "2014",
                    DefaultAwardPeriods = new List<string>(){"14/FA", "14/SU", "15/SP", "14/WI"}
                },

                new StudentDefaultAwardPeriod(){
                    StudentId = studentId,
                    AwardYear = "2013",
                    DefaultAwardPeriods = new List<string>(){"13/FA", "13/SU", "14/SP", "13/WI"}
                }
            };

            #endregion

            studentLoanRequestsData = new List<StudentLoanRequest>();

            #region OutsideAwardsData

            outsideAwardsData = new List<OutsideAward>()
            {
                new OutsideAward(){
                    Id = "1",
                    StudentId = "0003914",
                    AwardName = "Outside award 1",
                    AwardType = "scholarship",
                    AwardAmount = 456.78m,
                    AwardFundingSource = "Free people",
                    AwardYearCode = "2016"
                },
                new OutsideAward(){
                    Id = "16",
                    StudentId = "0003914",
                    AwardName = "Outside award 2",
                    AwardType = "grant",
                    AwardAmount = 10000m,
                    AwardFundingSource = "Grant central",
                    AwardYearCode = "2016"
                },
                new OutsideAward(){
                    Id = "5",
                    StudentId = "0003914",
                    AwardName = "Cool award",
                    AwardType = "scholarship",
                    AwardAmount = 777.77m,
                    AwardFundingSource = "Awarding place",
                    AwardYearCode = "2016"
                },
                new OutsideAward(){
                    Id = "9",
                    StudentId = "0003914",
                    AwardName = "Outside award for 2015",
                    AwardType = "loan",
                    AwardAmount = 15000m,
                    AwardFundingSource = "Some bank",
                    AwardYearCode = "2015"
                },
                new OutsideAward(){
                    Id = "67",
                    StudentId = "0003914",
                    AwardName = "Outside grant",
                    AwardType = "grant",
                    AwardAmount = 123.67m,
                    AwardFundingSource = "Girl scouts",
                    AwardYearCode = "2015"
                }
            };
            #endregion

            #region StudentNsldsData

            studentNsldsData = new StudentNsldsInformation()
            {
                StudentId = studentId,
                PellLifetimeEligibilityUsedPercentage = 23.567m
            };

            #endregion

            #region studentAccountDueData

            studentAccountDueData = new Colleague.Dtos.Finance.AccountDue.AccountDue()
            {
                AccountTerms = new List<Colleague.Dtos.Finance.AccountDue.AccountTerm>()
                {
                    new Colleague.Dtos.Finance.AccountDue.AccountTerm(){
                        GeneralItems = new List<Colleague.Dtos.Finance.AccountDue.AccountsReceivableDueItem>(){
                            new Colleague.Dtos.Finance.AccountDue.AccountsReceivableDueItem(){
                                AmountDue = 2500,
                                DueDate = new DateTime(2016, 08, 29)
                            },
                            new Colleague.Dtos.Finance.AccountDue.AccountsReceivableDueItem(){
                                AmountDue = 123,
                                DueDate = new DateTime(2016, 07, 02)
                            }
                        },
                        DepositDueItems = new List<Colleague.Dtos.Finance.DepositDue>(){
                            new Colleague.Dtos.Finance.DepositDue(){
                                AmountDue = 1000,
                                DueDate = new DateTime(2015, 06, 06)
                            },
                            new Colleague.Dtos.Finance.DepositDue(){
                                AmountDue = 300,
                                DueDate = new DateTime(2017, 06, 06)
                            }
                        },
                        PaymentPlanItems = new List<Colleague.Dtos.Finance.AccountDue.PaymentPlanDueItem>(){
                            new Colleague.Dtos.Finance.AccountDue.PaymentPlanDueItem(){
                                AmountDue = 5004,
                                DueDate = null
                            },
                            new Colleague.Dtos.Finance.AccountDue.PaymentPlanDueItem(){
                                AmountDue = 432,
                                DueDate = new DateTime(2017, 01, 01)
                            }
                        },
                        InvoiceItems = new List<Colleague.Dtos.Finance.AccountDue.InvoiceDueItem>(){
                            new Colleague.Dtos.Finance.AccountDue.InvoiceDueItem(){
                                AmountDue = 655,
                                DueDate = new DateTime(2016, 09, 30)
                            }
                        }
                    }
                }
            };
            #endregion
        }
    }
}
