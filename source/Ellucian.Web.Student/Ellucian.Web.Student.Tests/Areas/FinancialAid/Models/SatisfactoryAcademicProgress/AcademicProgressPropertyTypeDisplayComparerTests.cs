﻿using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.SatisfactoryAcademicProgress
{
    [TestClass]
    public class AcademicProgressPropertyTypeDisplayComparerTests
    {
        public List<AcademicProgressPropertyType> orderedTypeList;

        [TestInitialize]
        public void Initialize()
        {
            orderedTypeList = new List<AcademicProgressPropertyType>()
            {
                AcademicProgressPropertyType.MaximumProgramCredits,
                AcademicProgressPropertyType.EvaluationPeriodAttemptedCredits,
                AcademicProgressPropertyType.EvaluationPeriodCompletedCredits,
                AcademicProgressPropertyType.EvaluationPeriodOverallGpa,
                AcademicProgressPropertyType.EvaluationPeriodRateOfCompletion,
                AcademicProgressPropertyType.CumulativeAttemptedCredits,
                AcademicProgressPropertyType.CumulativeCompletedCredits,
                AcademicProgressPropertyType.CumulativeOverallGpa,
                AcademicProgressPropertyType.CumulativeRateOfCompletion,
                AcademicProgressPropertyType.CumulativeAttemptedCreditsExcludingRemedial,
                AcademicProgressPropertyType.CumulativeCompletedCreditsExcludingRemedial,
                AcademicProgressPropertyType.CumulativeRateOfCompletionExcludingRemedial,                
            };
        }

        [TestMethod]
        public void OrderedListEqualsExpectedTest()
        {
            var actualOrderedList = orderedTypeList.OrderBy(t => t, new AcademicProgressPropertyTypeDisplayComparer());
            CollectionAssert.AreEqual(orderedTypeList, actualOrderedList.ToList());
        }
    }
}
