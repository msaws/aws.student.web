﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;


namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.SatisfactoryAcademicProgress
{
    [TestClass]
    public class AcademicProgressEvaluationTests
    {
        private TestModelData testModelData;
        private Ellucian.Web.Student.Areas.FinancialAid.Models.AcademicProgressEvaluation academicProgressEvaluation;
        private List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluationDtos;
        private List<AcademicProgressStatus> academicProgressStatusDtos;        
        private FinancialAidOffice3 financialAidOfficeDto;
        private List<AcademicProgram> academicProgramDtos;
        private List<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode> academicProgressAppealCodesDtos;
        private List<Colleague.Dtos.FinancialAid.FinancialAidCounselor> appealsCounselorDtos;
        private List<Colleague.Dtos.Base.PhoneNumber> counselorsPhoneNumberDtos;
        private Colleague.Dtos.FinancialAid.StudentAwardYear2 mostRecentAwardYear;
        
        
        public void TestInitialize()
        {
            testModelData = new TestModelData("0003914");
            financialAidOfficeDto = testModelData.financialAidOfficesData.First();
            academicProgressEvaluationDtos = testModelData.academicProgressEvaluationsData;
            academicProgressStatusDtos = testModelData.academicProgressStatusesData;
            academicProgramDtos = testModelData.academicProgramsData;
            academicProgressAppealCodesDtos = testModelData.academicProgressAppealCodesData;
            appealsCounselorDtos = testModelData.appealsCounselorsData;
            counselorsPhoneNumberDtos = testModelData.counselorsPhoneNumbersData;
            mostRecentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(ay => ay.Code).First();
        }
        
        public void TestCleanup()
        {
            testModelData = null;
            academicProgressEvaluation = null;
            financialAidOfficeDto = null;
            academicProgramDtos = null;
            academicProgressEvaluationDtos = null;
            academicProgressStatusDtos = null;
            appealsCounselorDtos = null;
            mostRecentAwardYear = null;
        }          

        [TestClass]
        public class LatestAcademicProgressEvaluationConstructorTests : AcademicProgressEvaluationTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TestInitialize();
                BuildLatestAcademicProgressEvaluation();
            }

            [TestCleanup]
            public void Cleanup()
            {
                TestCleanup();
            }

            [TestMethod]
            public void AcademicProgressEvaluationInitializedTest()
            {
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() > 0);
                Assert.IsNotNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.SAPAppeal);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NullAcademicProgressEvaluations_AcademicProgressEvaluationInitializedTest()
            {
                academicProgressEvaluationDtos = null;
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() == 0);
                Assert.IsNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
                Assert.IsNull(academicProgressEvaluation.AcademicProgressEvaluationId);

            }

            [TestMethod]
            public void NoAcademicProgressEvaluations_AcademicProgressEvaluationInitializedTest()
            {
                academicProgressEvaluationDtos = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() == 0);
                Assert.IsNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
                Assert.IsNull(academicProgressEvaluation.AcademicProgressEvaluationId);
                
            }

            [TestMethod]
            public void NoAcademicProgressStatuses_AcademicProgressEvaluationInitializedTest()
            {                
                academicProgressStatusDtos = new List<AcademicProgressStatus>();
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() == 0);
                Assert.IsNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
            }

            [TestMethod]
            public void NoAcademicProgramsData_AcademicProgressEvaluationInitializedTest()
            {
                academicProgramDtos = new List<AcademicProgram>();
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() > 0);
                Assert.IsNotNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.SAPAppeal);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NoFinancialAidOfficeDto_AcademicProgressEvaluationInitializedTest()
            {
                financialAidOfficeDto = null;
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() == 0);
                Assert.IsNotNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.SAPAppeal);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void SAPStatusProperties_MatchLatestEvaluationDataTest()
            {                
                var expectedEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault();
                var actualEvaluationSapStatus = academicProgressEvaluation.SAPStatus;

                Assert.AreEqual(expectedEvaluation.EvaluationDateTime, actualEvaluationSapStatus.EvaluationDate);
                Assert.AreEqual(expectedEvaluation.EvaluationPeriodStartDate, actualEvaluationSapStatus.PeriodStartDate);
                Assert.AreEqual(expectedEvaluation.EvaluationPeriodEndDate, actualEvaluationSapStatus.PeriodEndDate);
                Assert.AreEqual(expectedEvaluation.EvaluationPeriodStartTerm, actualEvaluationSapStatus.PeriodStartTerm);
                Assert.AreEqual(expectedEvaluation.EvaluationPeriodEndTerm, actualEvaluationSapStatus.PeriodEndTerm);
                Assert.AreEqual(expectedEvaluation.StatusCode, actualEvaluationSapStatus.AcademicProgressStatus.Code);
            }

            [TestMethod]
            public void AcademicProgressStatusInitializationTest()
            {
                Assert.IsNotNull(academicProgressStatusDtos);
                Assert.IsTrue(academicProgressStatusDtos.Count == 5);
                CollectionAssert.AllItemsAreNotNull(academicProgressStatusDtos);
            }

            [TestMethod]
            public void AcademicProgressStatusDoNoDisplayTest()
            {
                var expectedAcademicProgressStatusRecord = testModelData.academicProgressStatusesData.FirstOrDefault(apsd => apsd.Category == AcademicProgressStatusCategory.DoNotDisplay);
                Assert.AreEqual(expectedAcademicProgressStatusRecord.Category, AcademicProgressStatusCategory.DoNotDisplay);
                Assert.IsNotNull(expectedAcademicProgressStatusRecord.Code);
                Assert.IsNotNull(expectedAcademicProgressStatusRecord.Description);
            }

            [TestMethod]
            public void AcademicProgressStatus_DoNotDisplayReturnsNoEvaluationRecordsTest()
            {
                var academicProgressStatusDtos = testModelData.academicProgressStatusesData.FirstOrDefault(apsd => apsd.Category == AcademicProgressStatusCategory.DoNotDisplay);
                var expectedEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault(aped => aped.StatusCode == academicProgressStatusDtos.Code);

                Assert.IsNull(expectedEvaluation);
            }

            [TestMethod]
            public void SAPDetailItems_MatchLatestEvaluationDataTest()
            {
                var expectedEvaluationDetails = testModelData.academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault().Detail;
                var expectedEvaluationConfiguration = financialAidOfficeDto.AcademicProgressConfiguration.DetailPropertyConfigurations;
                foreach (var propertyConfig in expectedEvaluationConfiguration)
                {
                    if (!propertyConfig.IsHidden)
                    {
                        var matchingDetailItem = academicProgressEvaluation.SAPDetailItems.FirstOrDefault(sdi => sdi.Code == propertyConfig.Type.ToString());
                        if (matchingDetailItem.Code != AcademicProgressPropertyType.MaximumProgramCredits.ToString())
                        {
                            var expectedProperty = expectedEvaluationDetails.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyConfig.Type.ToString());
                            Assert.IsNotNull(matchingDetailItem);
                            Assert.AreEqual(expectedProperty.GetValue(expectedEvaluationDetails), matchingDetailItem.Value);
                            Assert.AreEqual(propertyConfig.Description, matchingDetailItem.Explanation);
                            if (!string.IsNullOrEmpty(propertyConfig.Label))
                            {
                                Assert.AreEqual(propertyConfig.Label, matchingDetailItem.Description);
                            }                            
                        }                        
                    }                    
                }
            }

            [TestMethod]
            public void MaximumCreditsSAPDetailItem_MatchesExpectedDataTest()
            {
                var expectedMaximumCreditsValue = testModelData.academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault().ProgramDetail.ProgramMaxCredits;
                var expectedMaximumCreditsItemProperties = financialAidOfficeDto.AcademicProgressConfiguration.DetailPropertyConfigurations.First(dpc => dpc.Type == AcademicProgressPropertyType.MaximumProgramCredits);
                var actualMaximumCreditsItem = academicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == AcademicProgressPropertyType.MaximumProgramCredits.ToString());

                Assert.AreEqual(expectedMaximumCreditsValue, actualMaximumCreditsItem.Value);
                Assert.AreEqual(expectedMaximumCreditsItemProperties.Label, actualMaximumCreditsItem.Description);
                Assert.AreEqual(expectedMaximumCreditsItemProperties.Description, actualMaximumCreditsItem.Explanation);
            }

            [TestMethod]
            public void MaximumCreditsSapDetailItemValue_EqualsMinimumCreditsValueTest()
            {
                var expectedEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault();
                expectedEvaluation.ProgramDetail.ProgramMaxCredits = null;
                BuildLatestAcademicProgressEvaluation();
                var actualMaximumCreditsItem = academicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == AcademicProgressPropertyType.MaximumProgramCredits.ToString());
                Assert.IsNotNull(actualMaximumCreditsItem);
                Assert.AreEqual(expectedEvaluation.ProgramDetail.ProgramMinCredits, actualMaximumCreditsItem.Value);
            }

            [TestMethod]
            public void NullResultAppeals_NullSapAppealTest()
            {
                var mostRecentAcademicProgressEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(e => e.EvaluationDateTime).First();
                mostRecentAcademicProgressEvaluation.ResultAppeals = null;
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
            }

            [TestMethod]
            public void EmptyResultAppeals_NullSapAppealTest()
            {
                var mostRecentAcademicProgressEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(e => e.EvaluationDateTime).First();
                mostRecentAcademicProgressEvaluation.ResultAppeals = new List<AcademicProgressAppeal>();
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
            }

            [TestMethod]
            public void NullAppealCodes_NoSapAppealCreatedExceptionCaughtTest()
            {
                academicProgressAppealCodesDtos = null;
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
            }

            [TestMethod]
            public void EmptyAppealCodes_NoSapAppealCreatedExceptionCaughtTest()
            {
                academicProgressAppealCodesDtos = new List<AcademicProgressAppealCode>();
                BuildLatestAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
            }

            [TestMethod]
            public void AcademicProgressEvaluationProperties_EqualExpectedValuesTest()
            {
                var expectedEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault();
                Assert.AreEqual(expectedEvaluation.AcademicProgressTypeCode, academicProgressEvaluation.AcademicProgressEvaluationType);
                Assert.AreEqual(expectedEvaluation.Id, academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            private void BuildLatestAcademicProgressEvaluation()
            {
                academicProgressEvaluation = new Student.Areas.FinancialAid.Models.AcademicProgressEvaluation(academicProgressEvaluationDtos,
                        academicProgressStatusDtos, academicProgressAppealCodesDtos, appealsCounselorDtos, counselorsPhoneNumberDtos, academicProgramDtos, financialAidOfficeDto, mostRecentAwardYear);
            }      
        }

        [TestClass]
        public class SAPHistoryAcademicProgressEvaluationConstructorTests : AcademicProgressEvaluationTests
        {
            private Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicProgressEvaluationDto;

            [TestInitialize]
            public void Initialize()
            {
                TestInitialize();
                academicProgressEvaluationDto = testModelData.academicProgressEvaluationsData.First();
                BuildAcademicProgressEvaluation();
            }            

            [TestCleanup]
            public void Cleanup()
            {
                TestCleanup();
            }

            [TestMethod]
            public void AcademicProgressEvaluationInitializedTest()
            {
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() > 0);
                Assert.IsNotNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NoAcademicProgressEvaluation_AcademicProgressEvaluationInitializedTest()
            {
                academicProgressEvaluationDto = null;
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() == 0);
                Assert.IsNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NoAcademicProgressStatuses_AcademicProgressEvaluationInitializedTest()
            {
                academicProgressStatusDtos = new List<AcademicProgressStatus>();
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() > 0);
                Assert.IsNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NullAcademicProgressStatuses_AcademicProgressEvaluationInitializedTest()
            {
                academicProgressStatusDtos = null;
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() > 0);
                Assert.IsNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NoAcademicProgramsData_AcademicProgressEvaluationInitializedTest()
            {
                academicProgramDtos = new List<AcademicProgram>();
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() > 0);
                Assert.IsNotNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NoFinancialAidOfficeDto_AcademicProgressEvaluationInitializedTest()
            {
                financialAidOfficeDto = null;
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() == 0);
                Assert.IsNotNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            [TestMethod]
            public void NoAcademicProgramRequirements_AcademicProgressEvaluationInitializedTest()
            {
                academicProgressEvaluationDto.ProgramDetail = null;
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNotNull(academicProgressEvaluation.SAPDetailItems);
                Assert.IsTrue(academicProgressEvaluation.SAPDetailItems.Count() == 0);
                Assert.IsNotNull(academicProgressEvaluation.SAPStatus);
                Assert.IsNotNull(academicProgressEvaluation.AcademicProgressEvaluationId);

            }

            [TestMethod]
            public void SAPStatusProperties_MatchExpectedTest()
            {
                var sapStatus = academicProgressEvaluation.SAPStatus;
                Assert.AreEqual(academicProgressEvaluationDto.EvaluationDateTime, sapStatus.EvaluationDate);
                Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodStartDate, sapStatus.PeriodStartDate);
                Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodEndDate, sapStatus.PeriodEndDate);
                Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodStartTerm, sapStatus.PeriodStartTerm);
                Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodEndTerm, sapStatus.PeriodEndTerm);
                Assert.AreEqual(academicProgressEvaluationDto.StatusCode, sapStatus.AcademicProgressStatus.Code);
            }

            [TestMethod]
            public void SAPDetailItems_MatchAcademicProgressEvaluationDataTest()
            {
                var expectedEvaluationDetails = academicProgressEvaluationDto.Detail;
                var expectedEvaluationConfiguration = financialAidOfficeDto.AcademicProgressConfiguration.DetailPropertyConfigurations;
                foreach (var propertyConfig in expectedEvaluationConfiguration)
                {
                    if (!propertyConfig.IsHidden)
                    {
                        var matchingDetailItem = academicProgressEvaluation.SAPDetailItems.FirstOrDefault(sdi => sdi.Code == propertyConfig.Type.ToString());
                        if (matchingDetailItem.Code != AcademicProgressPropertyType.MaximumProgramCredits.ToString())
                        {
                            var expectedProperty = expectedEvaluationDetails.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyConfig.Type.ToString());
                            Assert.IsNotNull(matchingDetailItem);
                            Assert.AreEqual(expectedProperty.GetValue(expectedEvaluationDetails), matchingDetailItem.Value);
                            Assert.AreEqual(propertyConfig.Description, matchingDetailItem.Explanation);
                            if (!string.IsNullOrEmpty(propertyConfig.Label))
                            {
                                Assert.AreEqual(propertyConfig.Label, matchingDetailItem.Description);
                            }
                        }
                    }
                }
            }

            [TestMethod]
            public void MaximumCreditsSAPDetailItem_MatchesExpectedDataTest()
            {
                var expectedMaximumCreditsValue = academicProgressEvaluationDto.ProgramDetail.ProgramMaxCredits;
                var expectedMaximumCreditsItemProperties = financialAidOfficeDto.AcademicProgressConfiguration.DetailPropertyConfigurations.First(dpc => dpc.Type == AcademicProgressPropertyType.MaximumProgramCredits);
                var actualMaximumCreditsItem = academicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == AcademicProgressPropertyType.MaximumProgramCredits.ToString());

                Assert.AreEqual(expectedMaximumCreditsValue, actualMaximumCreditsItem.Value);
                Assert.AreEqual(expectedMaximumCreditsItemProperties.Label, actualMaximumCreditsItem.Description);
                Assert.AreEqual(expectedMaximumCreditsItemProperties.Description, actualMaximumCreditsItem.Explanation);
            }

            [TestMethod]
            public void MaximumCreditsSapDetailItemValue_EqualsMinimumCreditsValueTest()
            {
                academicProgressEvaluationDto.ProgramDetail.ProgramMaxCredits = null;
                BuildAcademicProgressEvaluation();
                var actualMaximumCreditsItem = academicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == AcademicProgressPropertyType.MaximumProgramCredits.ToString());
                Assert.IsNotNull(actualMaximumCreditsItem);
                Assert.AreEqual(academicProgressEvaluationDto.ProgramDetail.ProgramMinCredits, actualMaximumCreditsItem.Value);
            }

            [TestMethod]
            public void MaximumCreditsSapDetailItemValue_EqualsZeroTest()
            {
                academicProgressEvaluationDto.ProgramDetail.ProgramMaxCredits = null;
                academicProgressEvaluationDto.ProgramDetail.ProgramMinCredits = null;
                BuildAcademicProgressEvaluation();
                var actualMaximumCreditsItem = academicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == AcademicProgressPropertyType.MaximumProgramCredits.ToString());
                Assert.IsNotNull(actualMaximumCreditsItem);
                Assert.AreEqual(0, actualMaximumCreditsItem.Value);
            }

            [TestMethod]
            public void NullResultAppeals_NullSapAppealTest()
            {
                academicProgressEvaluationDto.ResultAppeals = null;
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
            }

            [TestMethod]
            public void EmptyResultAppeals_NullSapAppealTest()
            {
                academicProgressEvaluationDto.ResultAppeals = new List<AcademicProgressAppeal>();
                BuildAcademicProgressEvaluation();
                Assert.IsNotNull(academicProgressEvaluation);
                Assert.IsNull(academicProgressEvaluation.SAPAppeal);
            }

            [TestMethod]
            public void AcademicProgressEvaluationProperties_EqualExpectedValuesTest()
            {
                Assert.AreEqual(academicProgressEvaluationDto.AcademicProgressTypeCode, academicProgressEvaluation.AcademicProgressEvaluationType);
                Assert.AreEqual(academicProgressEvaluationDto.Id, academicProgressEvaluation.AcademicProgressEvaluationId);
            }

            private void BuildAcademicProgressEvaluation()
            {
                academicProgressEvaluation = new Student.Areas.FinancialAid.Models.AcademicProgressEvaluation(academicProgressEvaluationDto,
                    academicProgressStatusDtos, academicProgressAppealCodesDtos, appealsCounselorDtos, counselorsPhoneNumberDtos, academicProgramDtos, financialAidOfficeDto, mostRecentAwardYear);
            }
        }
       
    }
}
