﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.SatisfactoryAcademicProgress
{
    /// <summary>
    /// SAPStatus class tests
    /// </summary>
    [TestClass]
    public class SAPStatusTests
    {
        private SAPStatus sapStatus;
        private Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicProgressEvaluationDto;
        private AcademicProgressStatus academicProgressStatusDto;
        private AcademicProgram studentAcademicProgram;
        private TestModelData testModelData;

        [TestInitialize]
        public void Initalize()
        {
            testModelData = new TestModelData("0003914");
            academicProgressEvaluationDto = testModelData.academicProgressEvaluationsData.First();

            academicProgressStatusDto = testModelData.academicProgressStatusesData.First(aps => aps.Code == academicProgressEvaluationDto.StatusCode);

            studentAcademicProgram = testModelData.academicProgramsData.First(ap => ap.Code == academicProgressEvaluationDto.ProgramDetail.ProgramCode);

            sapStatus = new SAPStatus(academicProgressEvaluationDto, academicProgressStatusDto, studentAcademicProgram);
        }

        [TestCleanup]
        public void Cleanup()
        {
            sapStatus = null;
        }

        [TestMethod]
        public void SAPStatusInitializedTest()
        {
            Assert.IsNotNull(sapStatus);
        }

        [TestMethod]
        public void SAPStatusInitialized_DefaultConstructorTest()
        {
            sapStatus = new SAPStatus();
            Assert.IsNotNull(sapStatus);
            Assert.IsNull(sapStatus.AcademicProgressStatus);
        }

        [TestMethod]
        [ExpectedException (typeof(ArgumentNullException))]
        public void NullAcademicProgressEvaluationDto_ExceptionThrownTest()
        {
            sapStatus = new SAPStatus(null, academicProgressStatusDto, studentAcademicProgram);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullAcademicProgressStatusDto_ExceptionThrownTest()
        {
            sapStatus = new SAPStatus(academicProgressEvaluationDto, null, studentAcademicProgram);
        }

        [TestMethod]
        public void NoAcademicProgram_SAPStatusCreatedTest()
        {
            sapStatus = new SAPStatus(academicProgressEvaluationDto, academicProgressStatusDto, null);
            Assert.IsNotNull(sapStatus);
            Assert.IsNull(sapStatus.StudentAcademicProgram);
            Assert.IsNotNull(sapStatus.AcademicProgressStatus);
            Assert.IsNotNull(sapStatus.AcademicProgressStatus.Code);
            Assert.IsNotNull(sapStatus.AcademicProgressStatus.Category);
        }

        [TestMethod]
        public void SAPStatusAttributes_MatchExpectedTest()
        {
            Assert.AreEqual(academicProgressEvaluationDto.EvaluationDateTime, sapStatus.EvaluationDate);
            Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodStartDate, sapStatus.PeriodStartDate);
            Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodEndDate, sapStatus.PeriodEndDate);
            Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodStartTerm, sapStatus.PeriodStartTerm);
            Assert.AreEqual(academicProgressEvaluationDto.EvaluationPeriodEndTerm, sapStatus.PeriodEndTerm);
            Assert.AreEqual(academicProgressEvaluationDto.StatusCode, sapStatus.AcademicProgressStatus.Code);
            Assert.AreEqual(academicProgressStatusDto.Category, sapStatus.AcademicProgressStatus.Category);
            Assert.AreEqual(academicProgressStatusDto.Explanation, sapStatus.AcademicProgressStatus.Explanation);
            Assert.AreEqual(academicProgressStatusDto.Code, sapStatus.AcademicProgressStatus.Code);
            Assert.AreEqual(academicProgressStatusDto.Description, sapStatus.AcademicProgressStatus.Description);
            Assert.AreEqual(studentAcademicProgram.Description, sapStatus.StudentAcademicProgram);
        }
    }
}
