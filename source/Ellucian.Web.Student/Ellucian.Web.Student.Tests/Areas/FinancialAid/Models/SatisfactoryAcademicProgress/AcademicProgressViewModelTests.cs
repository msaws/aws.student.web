﻿/*Copyright 2015-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.SatisfactoryAcademicProgress
{
    /// <summary>
    /// AcademicProgressViewModelClass
    /// </summary>
    [TestClass]
    public class AcademicProgressViewModelTests
    {
        public string studentId;
        public TestModelData testModelData;
        
        public AcademicProgressViewModel academicProgressViewModel;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);

            BuildAcademicProgressViewModel();
        }

        [TestCleanup]
        public void Cleanup()
        {
            academicProgressViewModel = null;
        }

        [TestMethod]
        public void InitializedViewModelTest()
        {
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems);
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.IsNotNull(academicProgressViewModel.FinancialAidCounselor);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
        }

        [TestMethod]
        public void NoAcademicEvaluationsData_InitializedModelTest()
        {
            testModelData.academicProgressEvaluationsData = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNull(academicProgressViewModel.LatestAcademicProgressEvaluation);
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
            Assert.IsFalse(academicProgressViewModel.IsProxyView);
        }

        [TestMethod]
        public void NullAcademicEvaluationsData_InitializedModelTest()
        {
            testModelData.academicProgressEvaluationsData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
            Assert.IsNotNull(academicProgressViewModel.SAPHistoryItems);

        }

        [TestMethod]
        public void NoAcademicProgressStatusesData_InitializedModelTest()
        {
            testModelData.academicProgressStatusesData = new List<Colleague.Dtos.FinancialAid.AcademicProgressStatus>();
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);            
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
            Assert.IsNotNull(academicProgressViewModel.SAPHistoryItems);
        }

        [TestMethod]
        public void NullAcademicProgressStatusesData_InitializedModelTest()
        {
            testModelData.academicProgressStatusesData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
            Assert.IsNotNull(academicProgressViewModel.SAPHistoryItems);
        }

        [TestMethod]
        public void PersonIsFaStudentTest()
        {
            Assert.AreEqual(typeof(FAStudent), academicProgressViewModel.Person.GetType());
        }

        [TestMethod]
        public void NullPersonDataPassed_PersonAttributeIsNotNullTest()
        {
            testModelData.studentData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel.Person);
        }

        [TestMethod]
        public void NoFinancialAidOfficesPassed_ModelInitializedTest()
        {
            var exceptionThrown = false;
            testModelData.financialAidOfficesData = null;
            try { BuildAcademicProgressViewModel(); }
            catch (Exception) { exceptionThrown = true; }

            Assert.IsFalse(exceptionThrown);
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.IsNotNull(academicProgressViewModel.SAPHistoryItems);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
        }

        [TestMethod]
        public void NoStudentAwardYearsDataPassed_ModelInitializedTest()
        {
            testModelData.studentAwardYearsData = null;
            BuildAcademicProgressViewModel();

            Assert.IsNotNull(academicProgressViewModel);            
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.IsNull(academicProgressViewModel.FinancialAidCounselor);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
            Assert.IsNotNull(academicProgressViewModel.SAPHistoryItems);
        }

        [TestMethod]
        public void NoSAPLinksDataPassed_ModelInitializedTest()
        {
            testModelData.usefulLinksData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.LatestAcademicProgressEvaluation.SAPStatus);
            Assert.IsNotNull(academicProgressViewModel.FinancialAidCounselor);
            Assert.IsNotNull(academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems);
            Assert.IsNotNull(academicProgressViewModel.SAPLinks);
            Assert.IsNotNull(academicProgressViewModel.Person);
        }
       
        [TestMethod]
        public void ApplicantDataPassed_PersonCreatedAndAttributesMatchTest()
        {
            academicProgressViewModel = new AcademicProgressViewModel(testModelData.applicantData, 
                testModelData.academicProgressEvaluationsData, testModelData.academicProgressStatusesData,
                testModelData.academicProgressAppealCodesData,
                testModelData.appealsCounselorsData,
                testModelData.financialAidCounselorData, testModelData.counselorsPhoneNumbersData,
                testModelData.studentAwardYearsData, testModelData.financialAidOfficesData, testModelData.usefulLinksData,
                testModelData.academicProgramsData,testModelData.currentUser);
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.Person);
            Assert.AreEqual(testModelData.applicantData.FirstName, academicProgressViewModel.Person.FirstName);
            Assert.AreEqual(testModelData.applicantData.LastName, academicProgressViewModel.Person.LastName);
            Assert.AreEqual(testModelData.applicantData.Id, academicProgressViewModel.Person.Id);
        }

        [TestMethod]
        public void SAPStatusAttributes_MatchMostRecentEvaluationAttributesTest()
        {
            var evaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(ap => ap.EvaluationDateTime).First();
            Assert.AreEqual(evaluation.EvaluationPeriodEndDate, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPStatus.PeriodEndDate);
            Assert.AreEqual(evaluation.EvaluationPeriodStartDate, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPStatus.PeriodStartDate);
            Assert.AreEqual(evaluation.EvaluationPeriodEndTerm, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPStatus.PeriodEndTerm);
            Assert.AreEqual(evaluation.EvaluationPeriodStartTerm, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPStatus.PeriodStartTerm);
            Assert.AreEqual(evaluation.StatusCode, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Code);
        }

        [TestMethod]
        public void NotIsSAPStatusActive_NoPropertiesHaveValuesTest()
        {
            foreach (var office in testModelData.financialAidOfficesData)
            {
                office.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = false;
            }
            BuildAcademicProgressViewModel();
            Assert.IsNull(academicProgressViewModel.FinancialAidCounselor);            
            Assert.IsTrue(academicProgressViewModel.SAPLinks.Count() == 0);
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count() == 0);
            Assert.IsFalse(academicProgressViewModel.IsAcademicProgressActive);
        }

        [TestMethod]
        public void IsSAPStatusActive_AllPropertiesHaveValuesTest()
        {
            foreach (var office in testModelData.financialAidOfficesData)
            {
                office.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            }
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel.FinancialAidCounselor);
            Assert.IsNotNull(academicProgressViewModel.LatestAcademicProgressEvaluation.SAPStatus);
            Assert.IsTrue(academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems.Count() > 0);
            Assert.IsTrue(academicProgressViewModel.SAPLinks.Count() > 0);
            Assert.IsTrue(academicProgressViewModel.IsAcademicProgressActive);
        }

        [TestMethod]
        public void NotIsSapHistoryActive_NoHistoryItemsCreatedTest()
        {
            foreach (var office in testModelData.financialAidOfficesData)
            {
                office.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressHistoryActive = false;
            }
            BuildAcademicProgressViewModel();
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count() == 0);
        }

        [TestMethod]
        public void IsSapHistoryActive_HistoryItemsCreatedTest()
        {
            foreach (var office in testModelData.financialAidOfficesData)
            {
                office.AcademicProgressConfiguration.IsSatisfactoryAcademicProgressHistoryActive = true;
            }
            BuildAcademicProgressViewModel();
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count() > 0);
        }        
        
        [TestMethod]
        public void SAPLinksCount_MatchesPassedCountTest()
        {
            var sapLinks = testModelData.usefulLinksData.Where(l => l.LinkType == Colleague.Dtos.FinancialAid.LinkTypes.SatisfactoryAcademicProgress);
            Assert.AreEqual(sapLinks.Count(), academicProgressViewModel.SAPLinks.Count());
        }       

        [TestMethod]
        public void SAPLinksMatchPassedLinksTest()
        {
            var sapLinks = testModelData.usefulLinksData.Where(l => l.LinkType == Colleague.Dtos.FinancialAid.LinkTypes.SatisfactoryAcademicProgress);
            foreach (var link in sapLinks)
            {
                Assert.IsTrue(academicProgressViewModel.SAPLinks.Any(sl => sl.Title == link.Title));
                Assert.IsTrue(academicProgressViewModel.SAPLinks.Any(sl => sl.LinkUrl == link.LinkUrl));
            }
        }

        [TestMethod]
        public void SAPDetailsItemsCount_EqualsExpectedCountTest()
        {
            var mostRecentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var faOffice = testModelData.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);
            var expectedItemsCount = faOffice.AcademicProgressConfiguration.DetailPropertyConfigurations.Count();
            Assert.AreEqual(expectedItemsCount, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems.Count());            
        }

        [TestMethod]
        public void SAPDetailsItemsCount_NotEqualsExpectedWhenSomeIsHiddenTest()
        {
            var mostRecentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var faOffice = testModelData.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);

            var expectedItemsCount = faOffice.AcademicProgressConfiguration.DetailPropertyConfigurations.Count();

            faOffice.AcademicProgressConfiguration.DetailPropertyConfigurations.ToList()[0].IsHidden = true;
            BuildAcademicProgressViewModel();
            Assert.AreNotEqual(expectedItemsCount, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems.Count());
        }

        [TestMethod]
        public void SAPDetailsItemsCount_EqualsZeroWhenAllIsHiddenTest()
        {
            var mostRecentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var faOffice = testModelData.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);

            foreach (var config in faOffice.AcademicProgressConfiguration.DetailPropertyConfigurations)
            {
                config.IsHidden = true;
            }
            BuildAcademicProgressViewModel();
            Assert.IsTrue(academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems.Count() == 0);
        }

        [TestMethod]
        public void SAPMaximumCreditsItem_GetsValueFromMinimumCreditsPropertyTest()
        {
            var mostRecentEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(ped => ped.EvaluationDateTime).First();
            mostRecentEvaluation.ProgramDetail.ProgramMaxCredits = null;
            BuildAcademicProgressViewModel();
            Assert.AreEqual(mostRecentEvaluation.ProgramDetail.ProgramMinCredits, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == "MaximumProgramCredits").Value);
        }        

        [TestMethod]
        public void SAPMaximumCreditsItem_GetsValueFromMaximumCreditsPropertyTest()
        {
            var mostRecentEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(ped => ped.EvaluationDateTime).First();
            
            Assert.AreEqual(mostRecentEvaluation.ProgramDetail.ProgramMaxCredits, academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == "MaximumProgramCredits").Value);
        }

        [TestMethod]
        public void SAPMaximumCreditsItem_PropertiesMatchTest()
        {
            var mostRecentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var faOffice = testModelData.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);
            var mostRecentEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(ped => ped.EvaluationDateTime).First();
            var maximumCreditsConfiguration = faOffice.AcademicProgressConfiguration.DetailPropertyConfigurations.First(dpc => dpc.Type == AcademicProgressPropertyType.MaximumProgramCredits);

            var maxCreditsDetailItem = academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems.First(sdi => sdi.Code == "MaximumProgramCredits");

            Assert.AreEqual(maximumCreditsConfiguration.Description, maxCreditsDetailItem.Explanation);
            Assert.AreEqual(maximumCreditsConfiguration.Type.ToString(), maxCreditsDetailItem.Code);
            Assert.AreEqual(maximumCreditsConfiguration.Label, maxCreditsDetailItem.Description);
        }

        /// <summary>
        /// Tests that all but MaximimCreditsItem SAPDetailsItems have their properties set to correct values
        /// </summary>
        [TestMethod]
        public void SAPDetailsItemsProperties_MatchExpectedTest()
        {
            var mostRecentEvaluation = testModelData.academicProgressEvaluationsData.OrderByDescending(ped => ped.EvaluationDateTime).First();
            var mostRecentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var faOffice = testModelData.financialAidOfficesData.First(fao => fao.Id == mostRecentAwardYear.FinancialAidOfficeId);
            foreach (var detailItem in academicProgressViewModel.LatestAcademicProgressEvaluation.SAPDetailItems)
            {
                var correspondingConfig = faOffice.AcademicProgressConfiguration.DetailPropertyConfigurations.FirstOrDefault(dpc => dpc.Type.ToString() == detailItem.Code);
                var correspondingDetailProperty = mostRecentEvaluation.Detail.GetType().GetProperty(detailItem.Code);
                if (correspondingDetailProperty != null && correspondingConfig != null)
                {
                    var value = correspondingDetailProperty.GetValue(mostRecentEvaluation.Detail);
                    Assert.AreEqual(correspondingConfig.Description, detailItem.Explanation);
                    Assert.AreEqual(correspondingConfig.Label, detailItem.Description);
                    Assert.AreEqual(value, detailItem.Value);
                }                
            }
        }

        [TestMethod]
        public void NumberOfHistoryItemsCreated_EqualsNumberOfSAPRecordsToDisplayTest()
        {
            var mostRecentEvaluationType = testModelData.academicProgressEvaluationsData.OrderByDescending(e => e.EvaluationDateTime).First().AcademicProgressTypeCode.ToUpper();
            testModelData.academicProgressEvaluationsData.ForEach(e => e.AcademicProgressTypeCode = mostRecentEvaluationType);
            BuildAcademicProgressViewModel();
            var expectedNumberOfRecords = testModelData.financialAidOfficesData.First(fao => fao.Id == testModelData.studentAwardYearsData.OrderByDescending(ay => ay.Code)
                .First().FinancialAidOfficeId).AcademicProgressConfiguration.NumberOfAcademicProgressHistoryRecordsToDisplay;
            Assert.AreEqual(expectedNumberOfRecords, academicProgressViewModel.SAPHistoryItems.Count());
        }

        [TestMethod]
        public void ZeroNumberHistoryRecordsToDisplay_NoSAPHistoryItemsCreatedTest()
        {
            testModelData.financialAidOfficesData.First(fao => fao.Id == testModelData.studentAwardYearsData.OrderByDescending(ay => ay.Code)
                .First().FinancialAidOfficeId).AcademicProgressConfiguration.NumberOfAcademicProgressHistoryRecordsToDisplay = 0;
            BuildAcademicProgressViewModel();
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count() == 0);
        }

        [TestMethod]
        public void NumberHistoryRecordsToDisplay_GreaterThanAvailableRecordsTest()
        {
            testModelData.financialAidOfficesData.First(fao => fao.Id == testModelData.studentAwardYearsData.OrderByDescending(ay => ay.Code)
                .First().FinancialAidOfficeId).AcademicProgressConfiguration.NumberOfAcademicProgressHistoryRecordsToDisplay = 9;
            BuildAcademicProgressViewModel();
            var desiredNumberOfRecordsToDisplay = testModelData.financialAidOfficesData.First(fao => fao.Id == testModelData.studentAwardYearsData.OrderByDescending(ay => ay.Code)
                .First().FinancialAidOfficeId).AcademicProgressConfiguration.NumberOfAcademicProgressHistoryRecordsToDisplay;
            // -1 since one of the evaluations is the LatestAcademicProgressEvaluation 
            // and will not be included in the SAPHistoryItems list
            var availableEvaluationsCount = testModelData.academicProgressEvaluationsData.Count() - 1;
            Assert.AreNotEqual(desiredNumberOfRecordsToDisplay, availableEvaluationsCount);
        }

        [TestMethod]
        public void SAPHistoryItemsAreSortedTest()
        {
            var mostRecentEvaluationType = testModelData.academicProgressEvaluationsData.OrderByDescending(e => e.EvaluationDateTime).First().AcademicProgressTypeCode.ToUpper();
            testModelData.academicProgressEvaluationsData.ForEach(e => e.AcademicProgressTypeCode = mostRecentEvaluationType);
            BuildAcademicProgressViewModel();
            //Sort all academic progress evaluations and get one more than what the parameter says 
            //since the first evaluation in the list will be the latest evaluation not included in the SAPHistory records
            var sortedSAPHistoryItems = testModelData.academicProgressEvaluationsData.OrderByDescending(ape => ape.EvaluationDateTime).Take(testModelData.financialAidOfficesData.First(fao => fao.Id == testModelData.studentAwardYearsData.OrderByDescending(ay => ay.Code)
                .First().FinancialAidOfficeId).AcademicProgressConfiguration.NumberOfAcademicProgressHistoryRecordsToDisplay + 1).ToArray();
            //Start from 1 - first evaluation is the LatestAcademicProgressEvaluation
            for (int i = 1; i < sortedSAPHistoryItems.Count(); i++)
            {
                Assert.AreEqual(sortedSAPHistoryItems[i].EvaluationDateTime, academicProgressViewModel.SAPHistoryItems.ToArray()[i - 1].SAPStatus.EvaluationDate);
                Assert.AreEqual(sortedSAPHistoryItems[i].EvaluationPeriodEndDate, academicProgressViewModel.SAPHistoryItems.ToArray()[i - 1].SAPStatus.PeriodEndDate);
                Assert.AreEqual(sortedSAPHistoryItems[i].EvaluationPeriodStartDate, academicProgressViewModel.SAPHistoryItems.ToArray()[i - 1].SAPStatus.PeriodStartDate);
                Assert.AreEqual(sortedSAPHistoryItems[i].EvaluationPeriodEndTerm, academicProgressViewModel.SAPHistoryItems.ToArray()[i - 1].SAPStatus.PeriodEndTerm);
                Assert.AreEqual(sortedSAPHistoryItems[i].EvaluationPeriodStartTerm, academicProgressViewModel.SAPHistoryItems.ToArray()[i - 1].SAPStatus.PeriodStartTerm);
            }
        }

        [TestMethod]
        public void SAPHistoryItemsInDoNotDisplayCategory_NotIncludedTest()
        {
            //Get all evaluations, first one will be LatestAcademicProgressEvaluation and will not be included in the SAPHistoryItems list
            var allEvaluationsSorted = testModelData.academicProgressEvaluationsData.OrderByDescending(ape => ape.EvaluationDateTime).ToArray();

            //Get the status code of the first sap history item to set the appropriate category to "Do not Display"
            var sapHistoryItemStatus = allEvaluationsSorted[1].StatusCode;

            testModelData.academicProgressStatusesData.First(aps => aps.Code == sapHistoryItemStatus).Category = AcademicProgressStatusCategory.DoNotDisplay;
            BuildAcademicProgressViewModel();
            Assert.AreNotEqual(allEvaluationsSorted.Count() - 1, academicProgressViewModel.SAPHistoryItems.Count());
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.All(shi => shi.SAPStatus.AcademicProgressStatus.Code != sapHistoryItemStatus));

        }

        [TestMethod]
        public void AllEvaluationsInDoNotDisplayCategory_NoSAPHistoryItemsCreatedTest()
        {
            testModelData.academicProgressStatusesData.ForEach(aps => aps.Category = AcademicProgressStatusCategory.DoNotDisplay);
            BuildAcademicProgressViewModel();
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count() == 0);
        }

        [TestMethod]
        public void NoAcademicProgressStatuses_NoSAPHistoryItemsCreatedTest()
        {
            testModelData.academicProgressStatusesData = null;
            BuildAcademicProgressViewModel();
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count() == 0);
        }

        [TestMethod]
        public void NoPastEvaluationsAvailable_NoSAPHistoryItemsCreatedTest()
        {
            var allEvaluationsSorted = testModelData.academicProgressEvaluationsData.OrderByDescending(ape => ape.EvaluationDateTime).ToArray();
            for (var i = 1; i < allEvaluationsSorted.Count(); i++)
            {
                testModelData.academicProgressStatusesData.First(aps => aps.Code == allEvaluationsSorted[i].StatusCode).Category = AcademicProgressStatusCategory.DoNotDisplay;
            }
            BuildAcademicProgressViewModel();
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count() == 0);
        }

        [TestMethod]
        public void NullFinancialAidCounselor_FinancialAidCounselorNotNullAndExceptionCaughtTest()
        {
            testModelData.financialAidCounselorData = null;
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.FinancialAidCounselor);
        }

        [TestMethod]
        public void EmptyFinancialAidCounselor_FACounselorNotNullAndExceptionCaughtTest()
        {
            testModelData.financialAidCounselorData = new FinancialAidCounselor();
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNull(academicProgressViewModel.FinancialAidCounselor);
        }

        [TestMethod]
        public void NullCounselorPhoneNumbers_FACounselorCreatedTest()
        {
            testModelData.counselorsPhoneNumbersData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.FinancialAidCounselor);
        }

        [TestMethod]
        public void NoCounselorPhoneNumbers_FACounselorCreatedTest()
        {
            testModelData.counselorsPhoneNumbersData = new List<Colleague.Dtos.Base.PhoneNumber>();
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.FinancialAidCounselor);
        }

        [TestMethod]
        public void NoAppealCounselorsData_AppealAdvisorMatchesFACounselorTest()
        {
            testModelData.appealsCounselorsData = null;
            BuildAcademicProgressViewModel();
            var expectedCounselor = academicProgressViewModel.FinancialAidCounselor;
            var actualAppealAdvisor = academicProgressViewModel.LatestAcademicProgressEvaluation.SAPAppeal.AppealAdvisor;
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(actualAppealAdvisor);
            Assert.AreEqual(expectedCounselor.Name, actualAppealAdvisor.Name);
            Assert.AreEqual(expectedCounselor.PhoneNumber, actualAppealAdvisor.PhoneNumber);
            Assert.AreEqual(expectedCounselor.IsCounselorInfo, actualAppealAdvisor.IsCounselorInfo);
            Assert.AreEqual(expectedCounselor.EmailAddress, actualAppealAdvisor.EmailAddress);
        }

        [TestMethod]
        public void NoDoNotDisplayStatuses_AllExpectedSAPHistoryItemsDisplayTest()
        {
            var mostRecentEvaluationType = testModelData.academicProgressEvaluationsData.OrderByDescending(e => e.EvaluationDateTime).First().AcademicProgressTypeCode.ToUpper();
            var evaluationsCount = testModelData.academicProgressEvaluationsData.Where(e => e.AcademicProgressTypeCode.ToUpper() == mostRecentEvaluationType).Count();
            testModelData.academicProgressStatusesData.ForEach(s => s.Category = AcademicProgressStatusCategory.Satisfactory);
            testModelData.financialAidOfficesData.First(o => o.Id == testModelData.studentAwardYearsData.OrderByDescending(y => y.Code)
                .First().FinancialAidOfficeId).AcademicProgressConfiguration.NumberOfAcademicProgressHistoryRecordsToDisplay = evaluationsCount;
           
            BuildAcademicProgressViewModel();
            Assert.AreEqual(evaluationsCount - 1, academicProgressViewModel.SAPHistoryItems.Count);
        }

        [TestMethod]
        public void AllStatusesDoNotDisplay_NoEvaluationsTest()
        {
            testModelData.academicProgressStatusesData.ForEach(s => s.Category = AcademicProgressStatusCategory.DoNotDisplay);
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNull(academicProgressViewModel.LatestAcademicProgressEvaluation);
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count == 0);
        }

        [TestMethod]
        public void NoSameTypeEvaluations_NoSAPHistoryItemsTest()
        {
            testModelData.academicProgressEvaluationsData = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>() { 
                new Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2(){
                    EvaluationDateTime = new DateTime(2015, 01, 01),
                    EvaluationPeriodStartDate = new DateTime(2015, 08, 31),
                    EvaluationPeriodEndDate = new DateTime(2016, 01, 03),
                    AcademicProgressTypeCode = "Federal"
                }
            };
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count == 0);
        }

        [TestMethod]
        public void WrongAcademicProgressStatus_NoEvaluationsCreatedTest()
        {
            testModelData.academicProgressEvaluationsData.ForEach(e => e.StatusCode = "GH");
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNull(academicProgressViewModel.LatestAcademicProgressEvaluation);
            Assert.IsTrue(academicProgressViewModel.SAPHistoryItems.Count == 0);
        }

        [TestMethod]
        public void NoAppealCounselorsData_AppealCounselorEqualsFACounselorTest()
        {
            testModelData.appealsCounselorsData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            var expectedCounselor = academicProgressViewModel.FinancialAidCounselor;
            foreach (var item in academicProgressViewModel.SAPHistoryItems)
            {
                Assert.AreEqual(expectedCounselor.Name, item.SAPAppeal.AppealAdvisor.Name);
                Assert.AreEqual(expectedCounselor.IsCounselorInfo, item.SAPAppeal.AppealAdvisor.IsCounselorInfo);
                Assert.AreEqual(expectedCounselor.EmailAddress, item.SAPAppeal.AppealAdvisor.EmailAddress);
                Assert.AreEqual(expectedCounselor.PhoneNumber, item.SAPAppeal.AppealAdvisor.PhoneNumber);
            }
        }

        [TestMethod]
        public void NoAcademicProgressEvaluationsTypesToDisplay_AllEvaluationsOfSameTypeReturnedTest()
        {
            var mostRecentYear = testModelData.studentAwardYearsData.OrderByDescending(y => y.Code).First();
            var mostRecentEvaluationType = testModelData.academicProgressEvaluationsData.OrderByDescending(e => e.EvaluationDateTime).First().AcademicProgressTypeCode.ToUpper();
            testModelData.financialAidOfficesData.First(o => o.Id == mostRecentYear.FinancialAidOfficeId).AcademicProgressConfiguration.AcademicProgressTypesToDisplay = new List<string>();
            BuildAcademicProgressViewModel();
            
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNotNull(academicProgressViewModel.LatestAcademicProgressEvaluation);
            Assert.AreEqual(mostRecentEvaluationType, academicProgressViewModel.LatestAcademicProgressEvaluation.AcademicProgressEvaluationType.ToUpper());

            var expectedCount = testModelData.academicProgressEvaluationsData.Where(e => e.AcademicProgressTypeCode.ToUpper() == mostRecentEvaluationType).Count() - 1;
            Assert.AreEqual(expectedCount, academicProgressViewModel.SAPHistoryItems.Count);
        }

        [TestMethod]
        public void SAPHistoryEvaluationsType_MatchesLatestEvaluationTypeTest()
        {
            var expectedEvaluationType = academicProgressViewModel.LatestAcademicProgressEvaluation.AcademicProgressEvaluationType;
            foreach (var item in academicProgressViewModel.SAPHistoryItems)
            {
                Assert.AreEqual(expectedEvaluationType.ToUpper(), item.AcademicProgressEvaluationType.ToUpper());
            }
        }

        [TestMethod]
        public void CurrentUserIsNull_IsProxyViewSetToFalseTest()
        {
            testModelData.currentUser = null;
            BuildAcademicProgressViewModel();
            Assert.IsFalse(academicProgressViewModel.IsProxyView);
        }

        [TestMethod]
        public void CurrentUserNoProxy_IsProxyViewSetToFalseTest()
        {
            Assert.IsFalse(academicProgressViewModel.IsProxyView);
        }

        [TestMethod]
        public void CurrentUserWithProxy_IsProxyViewSetToTrueTest()
        {
            academicProgressViewModel = new AcademicProgressViewModel(testModelData.studentData, 
                testModelData.academicProgressEvaluationsData, testModelData.academicProgressStatusesData,
                testModelData.academicProgressAppealCodesData,
                testModelData.appealsCounselorsData,
                testModelData.financialAidCounselorData, testModelData.counselorsPhoneNumbersData,
                testModelData.studentAwardYearsData, testModelData.financialAidOfficesData, testModelData.usefulLinksData,
                testModelData.academicProgramsData,testModelData.currentUserWithProxy);

            Assert.IsTrue(academicProgressViewModel.IsProxyView);
        }

        /// <summary>
        /// Tests if SAP history items count equals expected count - only those history items
        /// are created that are of the same type as latest evaluation
        /// </summary>
        [TestMethod]
        public void SAPHistoryItemsCount_EqualsExpectedTest()
        {
            var expectedEvaluationType = academicProgressViewModel.LatestAcademicProgressEvaluation.AcademicProgressEvaluationType.ToUpper();
            var expectedCount = testModelData.academicProgressEvaluationsData.Where(e => e.AcademicProgressTypeCode.ToUpper() == expectedEvaluationType).Count() - 1;
            Assert.AreEqual(expectedCount, academicProgressViewModel.SAPHistoryItems.Count);
        }

        [TestMethod]
        public void NonMatchingAcademicProgressEvaluationType_NoEvaluationsReturnedTest()
        {
            testModelData.academicProgressEvaluationsData.ForEach(e => e.AcademicProgressTypeCode = "foo");
            BuildAcademicProgressViewModel();
            Assert.IsNotNull(academicProgressViewModel);
            Assert.IsNull(academicProgressViewModel.LatestAcademicProgressEvaluation);
            Assert.IsFalse(academicProgressViewModel.SAPHistoryItems.Any());
        }

        [TestMethod]
        public void NoPersonData_PersonIdIsnullTest()
        {
            testModelData.studentData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNull(academicProgressViewModel.PersonId);
        }

        [TestMethod]
        public void PersonId_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelData.studentData.Id, academicProgressViewModel.PersonId);
        }

        [TestMethod]
        public void NoPersonData_PersonNameIsnullTest()
        {
            testModelData.studentData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNull(academicProgressViewModel.PersonName);
        }

        [TestMethod]
        public void PersonName_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelData.studentData.PreferredName, academicProgressViewModel.PersonName);
        }

        [TestMethod]
        public void NoPersonData_PrivacyStatusCodeIsnullTest()
        {
            testModelData.studentData = null;
            BuildAcademicProgressViewModel();
            Assert.IsNull(academicProgressViewModel.PrivacyStatusCode);
        }

        [TestMethod]
        public void PrivacyStatusCode_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelData.studentData.PrivacyStatusCode, academicProgressViewModel.PrivacyStatusCode);
        }

        private void BuildAcademicProgressViewModel()
        {
            academicProgressViewModel = new AcademicProgressViewModel(testModelData.studentData, 
                testModelData.academicProgressEvaluationsData, testModelData.academicProgressStatusesData,
                testModelData.academicProgressAppealCodesData,
                testModelData.appealsCounselorsData,
                testModelData.financialAidCounselorData, testModelData.counselorsPhoneNumbersData,
                testModelData.studentAwardYearsData, testModelData.financialAidOfficesData, testModelData.usefulLinksData,
                testModelData.academicProgramsData,testModelData.currentUser);
        }
    }
}
