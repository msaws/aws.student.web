﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.FinancialAid.Models;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.SatisfactoryAcademicProgress
{
    [TestClass]
    public class SAPDetailItemTests
    {
        private string code;
        private string description;
        private string explanation;
        private decimal value;

        private SAPDetailItem sapDetailItem;

        [TestInitialize]
        public void Initialize()
        {
            code = "CompletedCredits";
            description = "Completed";
            explanation = "Completed credits explanation";
            value = 45m;

            sapDetailItem = new SAPDetailItem();

        }

        [TestCleanup]
        public void Cleanup()
        {
            sapDetailItem = null;
        }

        [TestMethod]
        public void CreditsItemInitializedTest()
        {
            Assert.IsNotNull(sapDetailItem);
            Assert.IsNull(sapDetailItem.Code);
            Assert.IsNull(sapDetailItem.Description);
            Assert.IsNull(sapDetailItem.Explanation);
            Assert.AreEqual(0, sapDetailItem.Value);            
        }

        [TestMethod]
        public void CodeAttributeGetSetTest()
        {
            sapDetailItem.Code = code;
            Assert.AreEqual(code, sapDetailItem.Code);
        }

        [TestMethod]
        public void DescriptionAttributeGetSetTest()
        {
            sapDetailItem.Description = description;
            Assert.AreEqual(description, sapDetailItem.Description);
        }

        [TestMethod]
        public void ExplanationAttributeGetSetTest()
        {
            sapDetailItem.Explanation = explanation;
            Assert.AreEqual(explanation, sapDetailItem.Explanation);
        }

        [TestMethod]
        public void ValueAttributeGetSetTest()
        {
            sapDetailItem.Value = value;
            Assert.AreEqual(value, sapDetailItem.Value);
        }
        
    }
}

