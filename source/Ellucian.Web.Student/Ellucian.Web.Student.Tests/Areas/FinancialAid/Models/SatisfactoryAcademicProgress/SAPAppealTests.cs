﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.SatisfactoryAcademicProgress
{
    [TestClass]
    public class SAPAppealTests
    {
        private TestModelData testModelData;
        private string studentId = "0003914";
        private Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2 academicProgressEvaluation;
        private IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressAppealCode> appealCodes;
        private IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidCounselor> appealCounselors;
        private IEnumerable<Colleague.Dtos.Base.PhoneNumber> counselorsPhoneNumbers;
        private Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYear;
        private Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOffice;
        private SAPAppeal actualSapAppeal;

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData(studentId);
            academicProgressEvaluation = testModelData.academicProgressEvaluationsData.First();
            appealCodes = testModelData.academicProgressAppealCodesData;
            appealCounselors = testModelData.appealsCounselorsData;
            counselorsPhoneNumbers = testModelData.counselorsPhoneNumbersData;
            studentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(y => y.Code).First();
            financialAidOffice = testModelData.financialAidOfficesData.First(o => o.Id == studentAwardYear.FinancialAidOfficeId);
            CreateSapAppeal();
        }
        
        [TestCleanup]
        public void Cleanup()
        {
            testModelData = null;
            academicProgressEvaluation = null;
            appealCodes = null;
            appealCounselors = null;
            counselorsPhoneNumbers = null;
            studentAwardYear = null;
            financialAidOffice = null;
        }

        /// <summary>
        /// Tests if sap appeal was created and its properties are not null
        /// </summary>
        [TestMethod]
        public void SapAppealInitializedTest()
        {
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNotNull(actualSapAppeal.AppealAdvisor);
            Assert.IsNotNull(actualSapAppeal.AppealDate);
            Assert.IsNotNull(actualSapAppeal.Status);
        }

        /// <summary>
        /// Test if appeal date equals expected date
        /// </summary>
        [TestMethod]
        public void SAPAppeal_AppealDateEqualsExpectedTest()
        {
            var expectedAppeal = academicProgressEvaluation.ResultAppeals.First();
            Assert.AreEqual(expectedAppeal.AppealDate, actualSapAppeal.AppealDate);
        }

        /// <summary>
        /// Test if appeal status equals expected status
        /// </summary>
        [TestMethod]
        public void SAPAppeal_AppealStatusEqualsExpectedTest()
        {
            var expectedAppealStatus = appealCodes.First( ac => ac.Code == academicProgressEvaluation.ResultAppeals.First().AppealStatusCode).Description;
            Assert.AreEqual(expectedAppealStatus, actualSapAppeal.Status);
        }

        /// <summary>
        /// Tests if appeal advisor name and email equal expected
        /// </summary>
        [TestMethod]
        public void SapAppeal_AppealCounselorEqualsExpectedTest()
        {
            var expectedAppeal = academicProgressEvaluation.ResultAppeals.First();
            var expectedAppealCounselor = appealCounselors.First(c => c.Id == expectedAppeal.AppealCounselorId);            
            Assert.AreEqual(expectedAppealCounselor.Name, actualSapAppeal.AppealAdvisor.Name);
            Assert.AreEqual(expectedAppealCounselor.EmailAddress, actualSapAppeal.AppealAdvisor.EmailAddress);
        }

        /// <summary>
        /// Tests if ArgumentNullException is thrown when passed academic evaluation is null
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullAcademicProgressEvaluation_ExceptionThrownTest()
        {
            academicProgressEvaluation = null;
            CreateSapAppeal();
        }

        /// <summary>
        /// Tests if ArgumentNullException is thrown when appealCodes argument is null
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public void NullAppealCodesList_ExceptionThrownTest()
        {
            appealCodes = null;
            CreateSapAppeal();
        }

        /// <summary>
        /// Tests if ArgumentNullException is thrown when appeal codes list is empty
        /// </summary>
        [TestMethod]
        [ExpectedException (typeof(ArgumentNullException))]
        public void EmptyAppealCodesList_ExceptionThrownTest()
        {
            appealCodes = new List<AcademicProgressAppealCode>();
            CreateSapAppeal();
        }

        /// <summary>
        /// Tests if empty academic progress evaluation forces created sap appeal's properties to their defaults
        /// </summary>
        [TestMethod]
        public void EmptyAcademicProgressEvaluation_SapAppealPropertiesSetToDefaultsTest()
        {
            academicProgressEvaluation = new Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2();
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            //Gets set to a default date
            Assert.IsNotNull(actualSapAppeal.AppealDate);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
            Assert.IsNull(actualSapAppeal.Status);
        }

        /// <summary>
        /// Tests if null result appeals force created sap appeal's props to their defaults
        /// </summary>
        [TestMethod]
        public void NullResultAppeals_SapAppealPropertiesSetToDefaultsTest()
        {
            academicProgressEvaluation.ResultAppeals = null;
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            //Gets set to a default date
            Assert.IsNotNull(actualSapAppeal.AppealDate);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
            Assert.IsNull(actualSapAppeal.Status);
        }

        /// <summary>
        /// Tests if empty result appeals force created sap appeal's props to their defaults
        /// </summary>
        [TestMethod]
        public void EmptyResultAppeals_SapAppealPropertiesSetToDefaultsTest()
        {
            academicProgressEvaluation.ResultAppeals = new List<AcademicProgressAppeal>();
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            //Gets set to a default date
            Assert.IsNotNull(actualSapAppeal.AppealDate);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
            Assert.IsNull(actualSapAppeal.Status);
        }

        /// <summary>
        /// Tests if invalid sap appeal status code on most recent ebaluation forces created sap 
        /// appeal status to be null
        /// </summary>
        [TestMethod]
        public void InvalidAppealStatusCode_SapAppealStatusIsNullTest()
        {
            academicProgressEvaluation.ResultAppeals.First().AppealStatusCode = "false";
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNull(actualSapAppeal.Status);
        }

        /// <summary>
        /// Tests if sap appeal counselor is not created if no counselor data is passed
        /// </summary>
        [TestMethod]
        public void NullAppealCounselorsData_SapAppealCounselorNotNullTest()
        {
            appealCounselors = null;
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
        }

        /// <summary>
        /// Tests if sap appeal counselor is not created if empty counselor data list is passed
        /// </summary>
        [TestMethod]
        public void EmptyAppealsCounselorsData_SapAppealCounselorNotNullTest()
        {
            appealCounselors = new List<Colleague.Dtos.FinancialAid.FinancialAidCounselor>();
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
        }

        /// <summary>
        /// Tests if sap appeal counselor is created even if counselor phone numbers list is null
        /// </summary>
        [TestMethod]
        public void NullCounselorPhoneNumbers_SapAppealCounselorNotNullTest()
        {
            counselorsPhoneNumbers = null;
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNotNull(actualSapAppeal.AppealAdvisor);
        }

        /// <summary>
        /// Tests if sap appeal counselor is created even if counselor phone numbers list is empty
        /// </summary>
        [TestMethod]
        public void EmptyCounselorPhoneNumbers_SapAppealCounselorNotNullTest()
        {
            counselorsPhoneNumbers = new List<Colleague.Dtos.Base.PhoneNumber>();
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNotNull(actualSapAppeal.AppealAdvisor);
        }

        /// <summary>
        /// Tests if null student award year does not cause sap appeal from being created, but appeal counselor is null
        /// </summary>
        [TestMethod]
        public void NullStudentAwardYear_AppealAdvisorIsNullTest()
        {
            studentAwardYear = null;
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNotNull(actualSapAppeal.Status);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
        }

        /// <summary>
        /// Tests if empty student award year does not cause sap appeal from being created, but appeal counselor is null
        /// </summary>
        [TestMethod]
        public void EmptyStudentAwardYear_AppealAdvisorIsNullTest()
        {
            studentAwardYear = new StudentAwardYear2();
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNotNull(actualSapAppeal.Status);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
        }

        /// <summary>
        /// Tests if null fa office does not cause sap appeal from being created, but appeal counselor is null
        /// </summary>
        [TestMethod]
        public void NullFinancialAidOffice_AppealAdvisorIsNullTest()
        {
            financialAidOffice = null;
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNotNull(actualSapAppeal.Status);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
        }

        /// <summary>
        /// Tests if empty fa office does not cause sap appeal from being created, but appeal counselor is null
        /// </summary>
        [TestMethod]
        public void EmptyFinancialAidOffice_AppealAdvisorIsNullTest()
        {
            financialAidOffice = new FinancialAidOffice3();
            CreateSapAppeal();
            Assert.IsNotNull(actualSapAppeal);
            Assert.IsNotNull(actualSapAppeal.Status);
            Assert.IsNull(actualSapAppeal.AppealAdvisor);
        }

        private void CreateSapAppeal()
        {
            actualSapAppeal = new SAPAppeal(academicProgressEvaluation, appealCodes, appealCounselors, counselorsPhoneNumbers, studentAwardYear, financialAidOffice);
        }
    }
}
