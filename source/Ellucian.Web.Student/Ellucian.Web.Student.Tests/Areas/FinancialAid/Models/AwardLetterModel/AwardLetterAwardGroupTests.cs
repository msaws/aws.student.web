﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.AwardLetterModel
{

    [TestClass]
    public class AwardLetterAwardGroupTests
    {
        public string studentId;
        public TestModelData testModelData;
        public AwardLetterAwardGroup awardLetterAwardGroup;

        public void BaseInitialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);
        }

        [TestClass]
        public class AwardLetterAwardGroupDefaultConstructorTests : AwardLetterAwardGroupTests
        {
            [TestInitialize]
            public void Initialize()
            {
                awardLetterAwardGroup = new AwardLetterAwardGroup();
            }

            [TestMethod]
            public void ObjectInitializedTest()
            {
                Assert.IsNotNull(awardLetterAwardGroup);
                Assert.IsNotNull(awardLetterAwardGroup.AwardLetterStudentAwards);
            }

            [TestMethod]
            public void GroupNameGetSetTest()
            {
                var expectedName = "Name";
                awardLetterAwardGroup.Name = expectedName;
                Assert.AreEqual(expectedName, awardLetterAwardGroup.Name);
            }

            [TestMethod]
            public void SequenceNumberGetSetTest()
            {
                var expectedNum = 1;
                awardLetterAwardGroup.SequenceNumber = expectedNum;
                Assert.AreEqual(expectedNum, awardLetterAwardGroup.SequenceNumber);
            }

            [TestMethod]
            public void AwardLetterStudentAwardsGetSetTest()
            {
                var expectedAwards = new List<AwardLetterStudentAward>()
            {
                new AwardLetterStudentAward() {AwardYearId = "foo", Code = "bar"}
            };
                awardLetterAwardGroup.AwardLetterStudentAwards = expectedAwards;
                Assert.AreEqual(expectedAwards, awardLetterAwardGroup.AwardLetterStudentAwards);
            }
        }

        /// <summary>
        /// Test class for AwardLetterAwardGroup constructor accepting group name, sequence number,
        /// group awards, and all award periods for the year
        /// </summary>
        [TestClass]
        public class AwardLetterAwardGroupMainConstructorTests : AwardLetterAwardGroupTests
        {
            private string name;
            private int sequenceNumber;
            private List<AwardLetterAnnualAward> groupAwards;
            private List<AwardLetterAwardPeriod> awardPeriods;
            
            [TestInitialize]
            public void Initialize()
            {
                name = "foobar";
                sequenceNumber = 99999;                

                awardPeriods = new List<AwardLetterAwardPeriod>(){
                    new AwardLetterAwardPeriod()
                    {
                        AwardDescription = "Award desc",
                        AwardId = "AWARD",
                        AwardPeriodAmount = 2000,
                        ColumnName = "Fall",
                        ColumnNumber = 1,
                        GroupName = "Awards",
                        GroupNumber = 1
                        
                    },
                    new AwardLetterAwardPeriod()
                    {
                        AwardDescription = "Award desc",
                        AwardId = "AWARD",
                        AwardPeriodAmount = 2000,
                        ColumnName = "Spring",
                        ColumnNumber = 2,
                        GroupName = "Awards",
                        GroupNumber = 1
                        
                    }
                };

                groupAwards = new List<AwardLetterAnnualAward>()
                {
                    new AwardLetterAnnualAward(){
                        AnnualAwardAmount = 2453,
                        AwardDescription = "Award desc",
                        AwardId = "AWARD",
                        GroupName = "Awards",
                        GroupNumber = 1,
                        AwardLetterAwardPeriods = awardPeriods
                    }
                };
                
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, new List<AwardLetterAwardPeriod>());
            }

            [TestMethod]
            public void NameTest()
            {
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, new List<AwardLetterAwardPeriod>());
                Assert.AreEqual(name, awardLetterAwardGroup.Name);
            }

            [TestMethod]
            public void SequenceNumberTest()
            {
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, new List<AwardLetterAwardPeriod>());
                Assert.AreEqual(sequenceNumber, awardLetterAwardGroup.SequenceNumber);
            }

            [TestMethod]
            public void AwardLetterStudentAwardsTest()
            {
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, new List<AwardLetterAwardPeriod>());
                foreach (var studentAward in groupAwards)
                {
                    var actualStudentAward = awardLetterAwardGroup.AwardLetterStudentAwards.First(a => a.Code == studentAward.AwardId);
                    Assert.AreEqual(studentAward.AwardDescription, actualStudentAward.Description);                    
                }
            }
            
            [TestMethod]
            public void AwardLetterStudentAwardPeriodsTest()
            {
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, awardPeriods);
                Assert.IsTrue(awardLetterAwardGroup.AwardLetterStudentAwards.Count() > 0);
                Assert.IsTrue(awardLetterAwardGroup.AwardLetterStudentAwards.SelectMany(a => a.StudentAwardPeriods).Count() > 0);
                
            }

            [TestMethod]
            [ExpectedException (typeof(ArgumentNullException))]
            public void NoGroupAwards_ExceptionThrownTest()
            {
                new AwardLetterAwardGroup(name, sequenceNumber, new List<AwardLetterAnnualAward>(), new List<AwardLetterAwardPeriod>());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullGroupAwards_ExceptionThrownTest()
            {
                new AwardLetterAwardGroup(name, sequenceNumber, null, new List<AwardLetterAwardPeriod>());
            }

            [TestMethod]
            public void AwardLetterAwards_HaveCorrectNumberOfPeriodsTest()
            {
                var expectedCount = awardPeriods.Count;
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, awardPeriods);
                foreach (var award in awardLetterAwardGroup.AwardLetterStudentAwards)
                {
                    Assert.AreEqual(expectedCount, award.StudentAwardPeriods.Count);
                }
            }

            [TestMethod]
            public void AwardLetterAwardPeriods_AreOrderedByColumnNumberTest()
            {
                var expectedOrder = awardPeriods.Distinct().OrderBy(p => p.ColumnNumber).ToList();
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, awardPeriods);
                foreach (var award in awardLetterAwardGroup.AwardLetterStudentAwards)
                {
                    var awardLetterAward = groupAwards.First(a => a.AwardId == award.Code);
                    for (int i = 0; i < expectedOrder.Count; i++)
                    {
                        var awardLetterAwardPeriod = awardLetterAward.AwardLetterAwardPeriods.First(p => p.ColumnNumber == expectedOrder[i].ColumnNumber);
                        Assert.AreEqual(awardLetterAwardPeriod.AwardPeriodAmount, award.StudentAwardPeriods[i].AwardAmount);
                    }
                }
            }

            [TestMethod]
            public void NoDistinctAwardPeriods_NoAwardLetterAwardPeriodsCreatedTest()
            {
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, null);
                foreach (var award in awardLetterAwardGroup.AwardLetterStudentAwards)
                {
                    Assert.IsTrue(award.StudentAwardPeriods.Count == 0);
                }
            }

            [TestMethod]
            public void NoMatchingAwardLetterAward_AwardPeriodAmountSetToNullTest()
            {
                #region newAwardPeriodsList
                var newAwardPeriodsList = new List<AwardLetterAwardPeriod>()
                {
                    new AwardLetterAwardPeriod()
                    {
                        AwardDescription = "Award desc",
                        AwardId = "AWARD",
                        AwardPeriodAmount = 2000,
                        ColumnName = "Fall",
                        ColumnNumber = 1,
                        GroupName = "Awards",
                        GroupNumber = 1
                        
                    },
                    new AwardLetterAwardPeriod()
                    {
                        AwardDescription = "Award desc",
                        AwardId = "AWARD",
                        AwardPeriodAmount = 2000,
                        ColumnName = "Spring",
                        ColumnNumber = 2,
                        GroupName = "Awards",
                        GroupNumber = 1
                        
                    },
                    new AwardLetterAwardPeriod(){
                        AwardDescription = "Award desc",
                        AwardId = "AWARD",                       
                        ColumnName = "Summer",
                        ColumnNumber = 3,
                        GroupName = "Awards",
                        GroupNumber = 1
                    }
                };
                #endregion
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, newAwardPeriodsList);

                Assert.IsTrue(awardLetterAwardGroup.AwardLetterStudentAwards.Any(a => a.StudentAwardPeriods.Any(p => p.AwardAmount == null)));
            }

            [TestMethod]
            public void AwardLetterAwardTotal_EqualsAnnualAwardAmountTest()
            {
                groupAwards.First().AwardLetterAwardPeriods = new List<AwardLetterAwardPeriod>();
                awardLetterAwardGroup = new AwardLetterAwardGroup(name, sequenceNumber, groupAwards, null); 
                Assert.AreEqual(groupAwards.First().AnnualAwardAmount, awardLetterAwardGroup.AwardLetterStudentAwards.First().TotalAwardAmount);
            }
        }

        /// <summary>
        /// Test class for AwardLetterAwardGroup constructor accepting award letter dto 
        /// </summary>
        [TestClass]
        public class AwardLetterAwardGroupConstructorTests : AwardLetterAwardGroupTests
        {
            private AwardLetter2 awardLetterDto;
            
            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();
                awardLetterDto = testModelData.awardLetterHistoryRecordsData.First();
                awardLetterAwardGroup = new AwardLetterAwardGroup(awardLetterDto);
            }

            [TestCleanup]
            public void Cleanup()
            {
                awardLetterDto = null;
                awardLetterAwardGroup = null;
            }

            [TestMethod]
            public void AwardLetterAwardGroupInitializedTest()
            {
                Assert.IsNotNull(awardLetterAwardGroup);                
                Assert.IsNotNull(awardLetterAwardGroup.AwardLetterStudentAwards);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullAwardLetterDto_ExceptionThrownTest()
            {
                awardLetterAwardGroup = new AwardLetterAwardGroup(null);
            }

            [TestMethod]
            public void AwardPeriodCount_MatchesExpectedTest()
            {
                var expectedCount = awardLetterDto.AwardLetterAnnualAwards.SelectMany(a => a.AwardLetterAwardPeriods).Select(p => p.ColumnNumber).Distinct().Count();
                foreach (var award in awardLetterAwardGroup.AwardLetterStudentAwards)
                {
                    Assert.AreEqual(expectedCount, award.StudentAwardPeriods.Count);
                }
            }

            [TestMethod]
            public void TotalAwardAmount_EqualsAwardPeriodAwardAmountsSumTest()
            {
                var allAwardPeriods = awardLetterDto.AwardLetterAnnualAwards.SelectMany(a => a.AwardLetterAwardPeriods);
                Assert.AreEqual(allAwardPeriods.Sum(ap => ap.AwardPeriodAmount), awardLetterAwardGroup.AwardLetterStudentAwards.First().TotalAwardAmount);
            }

            [TestMethod]
            public void TotalAwardAmount_EqualsAnnualAwardAmountsSumTest()
            {
                foreach (var award in awardLetterDto.AwardLetterAnnualAwards)
                {
                    award.AwardLetterAwardPeriods = new List<AwardLetterAwardPeriod>();
                }
                awardLetterAwardGroup = new AwardLetterAwardGroup(awardLetterDto);
                Assert.AreEqual(awardLetterDto.AwardLetterAnnualAwards.Sum(a => a.AnnualAwardAmount), awardLetterAwardGroup.AwardLetterStudentAwards.First().TotalAwardAmount);
            }

            [TestMethod]
            public void NoAwardLetterAnnualAwards_TotalEqualsZeroTest()
            {
                awardLetterDto.AwardLetterAnnualAwards = new List<AwardLetterAnnualAward>();
                awardLetterAwardGroup = new AwardLetterAwardGroup(awardLetterDto);
                Assert.IsTrue(awardLetterAwardGroup.AwardLetterStudentAwards.First().TotalAwardAmount == 0);
            }
        }
    }
}
