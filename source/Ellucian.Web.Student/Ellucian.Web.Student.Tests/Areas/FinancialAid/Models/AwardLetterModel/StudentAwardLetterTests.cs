﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.AwardLetterModel
{
    [TestClass]
    public class StudentAwardLetterTests
    {
        public string studentId;
        public TestModelData testModelData;
        public StudentAwardLetter studentAwardLetter;

        public void BaseInitialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);
        }

        [TestClass]
        public class StudentAwardLetterDefaultConstructorTests : StudentAwardLetterTests
        {
            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();
                studentAwardLetter = new StudentAwardLetter();
            }

            [TestMethod]
            public void ObjectExistsTest()
            {
                Assert.IsNotNull(studentAwardLetter);
            }

            [TestMethod]
            public void AwardYearCodeGetSetTest()
            {
                var AwardYearCode = "2014";
                studentAwardLetter.AwardYearCode = AwardYearCode;
                Assert.AreEqual(AwardYearCode, studentAwardLetter.AwardYearCode);
            }
            [TestMethod]
            public void AwardYearDescriptionGetSetTest()
            {
                var AwardYearDescription = "Description";
                studentAwardLetter.AwardYearDescription = AwardYearDescription;
                Assert.AreEqual(AwardYearDescription, studentAwardLetter.AwardYearDescription);
            }
            [TestMethod]
            public void StudentIdGetSetTest()
            {
                var StudentId = "foobar";
                studentAwardLetter.StudentId = StudentId;
                Assert.AreEqual(StudentId, studentAwardLetter.StudentId);
            }
            [TestMethod]
            public void StudentNameGetSetTest()
            {
                var StudentName = "foobar";
                studentAwardLetter.StudentName = StudentName;
                Assert.AreEqual(StudentName, studentAwardLetter.StudentName);
            }
            [TestMethod]
            public void AcceptedDateGetSetTest()
            {
                var AcceptedDate = DateTime.Today;
                studentAwardLetter.AcceptedDate = AcceptedDate;
                Assert.AreEqual(AcceptedDate, studentAwardLetter.AcceptedDate);
            }
            [TestMethod]
            public void OpeningParagraphGetSetTest()
            {
                var OpeningParagraph = "This is the the opening paragraph";
                studentAwardLetter.OpeningParagraph = OpeningParagraph;
                Assert.AreEqual(OpeningParagraph, studentAwardLetter.OpeningParagraph);
            }
            [TestMethod]
            public void ClosingParagraphGetSetTest()
            {
                var ClosingParagraph = "This is the the closing paragraph";
                studentAwardLetter.ClosingParagraph = ClosingParagraph;
                Assert.AreEqual(ClosingParagraph, studentAwardLetter.ClosingParagraph);
            }
            [TestMethod]
            public void IsContactBlockActiveGetSetTest()
            {
                var IsContactBlockActive = true;
                studentAwardLetter.IsContactBlockActive = IsContactBlockActive;
                Assert.AreEqual(IsContactBlockActive, studentAwardLetter.IsContactBlockActive);
            }
            [TestMethod]
            public void ContactAddressGetSetTest()
            {
                Assert.IsNotNull(studentAwardLetter.ContactAddress);
                var ContactAddress = new List<string>() { "Main Office","AddressLine1", "AddressLine2" };
                studentAwardLetter.ContactAddress = ContactAddress;
                Assert.AreEqual(ContactAddress, studentAwardLetter.ContactAddress);
            }
            [TestMethod]
            public void StudentAddressGetSetTest()
            {
                Assert.IsNotNull(studentAwardLetter.StudentAddress);
                var StudentAddress = new List<string>() {"Student Name", "AddressLine1", "AddressLine2" };
                studentAwardLetter.StudentAddress = StudentAddress;
                Assert.AreEqual(StudentAddress, studentAwardLetter.StudentAddress);
            }
            [TestMethod]
            public void IsNeedBlockActiveGetSetTest()
            {
                var IsNeedBlockActive = true;
                studentAwardLetter.IsNeedBlockActive = IsNeedBlockActive;
                Assert.AreEqual(IsNeedBlockActive, studentAwardLetter.IsNeedBlockActive);
            }
            [TestMethod]
            public void BudgetAmountGetSetTest()
            {
                var BudgetAmount = 1234;
                studentAwardLetter.BudgetAmount = BudgetAmount;
                Assert.AreEqual(BudgetAmount, studentAwardLetter.BudgetAmount);
            }
            [TestMethod]
            public void EstimatedFamilyContributionAmountGetSetTest()
            {
                var EstimatedFamilyContributionAmount = 1234;
                studentAwardLetter.EstimatedFamilyContributionAmount = EstimatedFamilyContributionAmount;
                Assert.AreEqual(EstimatedFamilyContributionAmount, studentAwardLetter.EstimatedFamilyContributionAmount);
            }
            [TestMethod]
            public void NeedAmountGetSetTest()
            {
                var NeedAmount = 1234;
                studentAwardLetter.NeedAmount = NeedAmount;
                Assert.AreEqual(NeedAmount, studentAwardLetter.NeedAmount);
            }

            [TestMethod]
            public void IsHousingCodeActiveGetSetTest()
            {
                var isHousingCodeActive = true;
                studentAwardLetter.IsHousingCodeActive = isHousingCodeActive;
                Assert.AreEqual(isHousingCodeActive, studentAwardLetter.IsHousingCodeActive);
            }

            [TestMethod]
            public void HousingCodeGetSetTest()
            {
                var housingCode = "With parent";
                studentAwardLetter.HousingCode = housingCode;
                Assert.AreEqual(housingCode, studentAwardLetter.HousingCode);
            }

            [TestMethod]
            public void StudentAwardGroupsGetSetTest()
            {
                var StudentAwardGroups = new List<AwardLetterAwardGroup>() { new AwardLetterAwardGroup() { Name = "Group1", SequenceNumber = 0 } };
                studentAwardLetter.StudentAwardGroups = StudentAwardGroups;
                Assert.AreEqual(StudentAwardGroups, studentAwardLetter.StudentAwardGroups);
            }
            [TestMethod]
            public void AwardColumnHeaderGetSetTest()
            {
                var AwardColumnHeader = "Award Column";
                studentAwardLetter.AwardColumnHeader = AwardColumnHeader;
                Assert.AreEqual(AwardColumnHeader, studentAwardLetter.AwardColumnHeader);
            }
            [TestMethod]
            public void TotalColumnHeaderGetSetTest()
            {
                var TotalColumnHeader = "Total Column";
                studentAwardLetter.TotalColumnHeader = TotalColumnHeader;
                Assert.AreEqual(TotalColumnHeader, studentAwardLetter.TotalColumnHeader);
            }
            [TestMethod]
            public void AwardPeriodColumnHeadersGetSetTest()
            {
                Assert.IsNotNull(studentAwardLetter.AwardPeriodColumnHeaders);
                var AwardPeriodColumnHeaders = new List<string>() { "Column1", "Column2" };
                studentAwardLetter.AwardPeriodColumnHeaders = AwardPeriodColumnHeaders;
                Assert.AreEqual(AwardPeriodColumnHeaders, studentAwardLetter.AwardPeriodColumnHeaders);
            }
            
            [TestMethod]
            public void AreAnyAwardsPendingGetSetTest()
            {
                var AreAnyAwardsPending = true;
                studentAwardLetter.AreAnyAwardsPending = AreAnyAwardsPending;
                Assert.AreEqual(AreAnyAwardsPending, studentAwardLetter.AreAnyAwardsPending);
            }
            [TestMethod]
            public void AreAwardsPresentGetTest()
            {
                Assert.IsFalse(studentAwardLetter.AreAwardsPresent);                
            }
            [TestMethod]
            public void IsLetterAcceptedGetTest()
            {
                studentAwardLetter.AcceptedDate = null;
                Assert.IsFalse(studentAwardLetter.IsLetterAccepted);

                studentAwardLetter.AcceptedDate = DateTime.Today;
                Assert.IsTrue(studentAwardLetter.IsLetterAccepted);
            }

            [TestMethod]
            public void AreAnyAwardsGetSetTest()
            {
                Assert.IsFalse(studentAwardLetter.AreAnyAwardsToDisplay);

                var areAnyAwards = true;
                studentAwardLetter.AreAnyAwardsToDisplay = areAnyAwards;

                Assert.AreEqual(areAnyAwards, studentAwardLetter.AreAnyAwardsToDisplay);
            }

            [TestMethod]
            public void AwardLetterAwardsTotal_GetSetTest()
            {
                var total = 2000;
                studentAwardLetter.AwardLetterAwardsTotal = total;
                Assert.AreEqual(total, studentAwardLetter.AwardLetterAwardsTotal);
            }
            
        }

        [TestClass]
        public class StudentAwardLetterConstructorTests : StudentAwardLetterTests
        {
            private StudentAwardYear2 studentAwardYearDto;
            private AwardLetter2 studentAwardLetterDto;
            private FAStudent personData;
            private List<Colleague.Dtos.FinancialAid.StudentAward> studentAwardDtos;
            private List<AwardStatus> awardStatusDtos;
            private IEnumerable<AwardLetterConfiguration> awardLetterConfigurationsList;
            private FinancialAidConfiguration3 configuration;

            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();

                studentAwardYearDto = testModelData.studentAwardYearsData.First();
                studentAwardLetterDto = testModelData.awardLetterHistoryRecordsData.First(l => l.AwardLetterYear == studentAwardYearDto.Code);
                personData = new FAStudent(testModelData.studentData);
                studentAwardDtos = testModelData.studentAwardsData.Where(sa => sa.AwardYearId == studentAwardYearDto.Code).ToList();
                awardStatusDtos = testModelData.awardStatusData;
                awardLetterConfigurationsList = testModelData.awardLetterConfigurationsData;
                configuration = testModelData.financialAidOfficesData.First(o => o.Id == studentAwardYearDto.FinancialAidOfficeId).Configurations.First(c => c.AwardYear == studentAwardYearDto.Code);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullAwardYearDtoThrowsExceptionTest()
            {
                new StudentAwardLetter(null, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullAwardLetterDtoThrowsExceptionTest()
            {
                new StudentAwardLetter(studentAwardYearDto, null, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
            }
            
            [TestMethod]
            [ExpectedException (typeof(ArgumentNullException))]
            public void NullAwardLetterConfigurationDtos_ExceptionThrownTest()
            {
                new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, null, configuration);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void EmptyAwardLetterConfigurationDtosList_ExceptionThrownTest()
            {
                new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, new List<AwardLetterConfiguration>(), configuration);
            }

            [TestMethod]
            [ExpectedException (typeof(Exception))]
            public void NoAwardLetterConfigurationForAwardLetter_ExceptionThrownTest()
            {
                studentAwardLetterDto.AwardLetterParameterId = "test";
                new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
            }

            [TestMethod]
            public void AwardLetterPropertiesTest()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                var awardLetterConfigurationForYear = awardLetterConfigurationsList.First(c => c.Id == studentAwardLetterDto.AwardLetterParameterId);
                
                Assert.AreEqual(studentAwardYearDto.Code, studentAwardLetter.AwardYearCode);
                Assert.AreEqual(studentAwardYearDto.Description, studentAwardLetter.AwardYearDescription);
                Assert.AreEqual(studentAwardLetterDto.StudentId, studentAwardLetter.StudentId);
                Assert.AreEqual(studentAwardLetterDto.StudentName, studentAwardLetter.StudentName);                
                Assert.AreEqual(studentAwardLetterDto.AcceptedDate, studentAwardLetter.AcceptedDate);
                Assert.AreEqual(studentAwardLetterDto.BudgetAmount, studentAwardLetter.BudgetAmount);
                Assert.AreEqual(studentAwardLetterDto.ClosingParagraph, studentAwardLetter.ClosingParagraph);
                Assert.AreEqual(studentAwardLetterDto.EstimatedFamilyContributionAmount, studentAwardLetter.EstimatedFamilyContributionAmount);
                Assert.AreEqual(studentAwardLetterDto.NeedAmount, studentAwardLetter.NeedAmount);
                Assert.AreEqual(studentAwardLetterDto.OpeningParagraph, studentAwardLetter.OpeningParagraph);
                Assert.AreEqual(studentAwardLetterDto.Id, studentAwardLetter.AwardLetterRecordId);
                Assert.AreEqual(studentAwardLetterDto.AcceptedDate.HasValue, studentAwardLetter.IsLetterAccepted);

                Assert.AreEqual(awardLetterConfigurationForYear.IsContactBlockActive, studentAwardLetter.IsContactBlockActive);
                Assert.AreEqual(awardLetterConfigurationForYear.IsHousingBlockActive, studentAwardLetter.IsHousingCodeActive);
                Assert.AreEqual(awardLetterConfigurationForYear.IsNeedBlockActive, studentAwardLetter.IsNeedBlockActive);
                Assert.AreEqual(awardLetterConfigurationForYear.AwardTableTitle, studentAwardLetter.AwardColumnHeader);
                Assert.AreEqual(awardLetterConfigurationForYear.AwardTotalTitle, studentAwardLetter.TotalColumnHeader);

                Assert.IsTrue(studentAwardLetter.AreAnyAwardsToDisplay);

            } 
           
            [TestMethod]
            public void StudentAddressEqualsExpectedTest()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                for(int i = 0; i < studentAwardLetterDto.StudentAddress.Count; i++)
                {
                    var actualLine = studentAwardLetter.StudentAddress[i];
                    Assert.AreEqual(studentAwardLetterDto.StudentAddress[i].AddressLine, actualLine);
                }
            }

            [TestMethod]
            public void NoStudentAddress_NoneAssignedTest()
            {
                studentAwardLetterDto.StudentAddress = null;
                personData.PreferredName = null;
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.StudentAddress.Any());
            }

            [TestMethod]
            public void ContactAddressEqualsExpectedTest()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                for (int i = 0; i < studentAwardLetterDto.ContactAddress.Count; i++)
                {
                    var actualLine = studentAwardLetter.ContactAddress[i];
                    Assert.AreEqual(studentAwardLetterDto.ContactAddress[i].AddressLine, actualLine);
                }
            }

            [TestMethod]
            public void NoContactAddress_NoneAssignedTest()
            {
                studentAwardLetterDto.ContactAddress = null;
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.ContactAddress.Any());
            }

            [TestMethod]
            public void AreAnyAwardsToDisplay_IsTrueTest()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsTrue(studentAwardLetter.AreAnyAwardsToDisplay);
            }

            [TestMethod]
            public void AreAnyAwardsToDisplay_IsFalseTest()
            {
                studentAwardLetterDto.AwardLetterAnnualAwards = new List<AwardLetterAnnualAward>();
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.AreAnyAwardsToDisplay);
            }

            [TestMethod]
            public void AreAwardsPresent_IsTrueTest()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsTrue(studentAwardLetter.AreAwardsPresent);
            }

            [TestMethod]
            public void AreAwardsPresent_IsFalseTest1()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, null, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.AreAwardsPresent);
            }

            [TestMethod]
            public void AreAwardsPresent_IsFalseTest2()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, new List<Ellucian.Colleague.Dtos.FinancialAid.StudentAward>(), awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.AreAwardsPresent);
            }

            [TestMethod]
            public void AwardPeriodColumnHeaders_EqualExpectedTest()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                var awardPeriodsForYear = studentAwardLetterDto.AwardLetterAnnualAwards.SelectMany(a => a.AwardLetterAwardPeriods)
                    .OrderBy(ap => ap.ColumnNumber).Select(ap => ap.ColumnName).Distinct().ToList();
                for (int i = 0; i < awardPeriodsForYear.Count; i++)
                {
                    Assert.AreEqual(awardPeriodsForYear[i], studentAwardLetter.AwardPeriodColumnHeaders[i]);
                }
            }

            [TestMethod]
            public void NoAwardPeriodsForYear_EmptyAwardPeriodColumnHeadersListTest()
            {
                foreach(var award in studentAwardLetterDto.AwardLetterAnnualAwards){
                    award.AwardLetterAwardPeriods = new List<AwardLetterAwardPeriod>();
                }
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.AwardPeriodColumnHeaders.Any());
            }

            [TestMethod]
            public void StudentAwardGroupsCount_EqualsExpectedTest()
            {
                //+1 for the total row
                var expectedCount = studentAwardLetterDto.AwardLetterAnnualAwards.Select(a => a.GroupNumber).Distinct().Count() + 1;
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.AreEqual(expectedCount, studentAwardLetter.StudentAwardGroups.Count);
            }

            [TestMethod]
            public void StudentAwardGroups_NoAwardGroupsAddedIfNoAwardsTest()
            {
                studentAwardLetterDto.AwardLetterAnnualAwards = new List<AwardLetterAnnualAward>();
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                //Only the total row was added
                Assert.IsTrue(studentAwardLetter.StudentAwardGroups.Count == 1);
            }

            [TestMethod]
            public void AreAnyAwardsPending_IsTrueTest1() {
                studentAwardDtos.First().StudentAwardPeriods.First().AwardStatusId = "P";
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsTrue(studentAwardLetter.AreAnyAwardsPending);
            }

            [TestMethod]
            public void AreAnyAwardsPending_IsTrueTest2()
            {
                studentAwardDtos.First().StudentAwardPeriods.First().AwardStatusId = "E";
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsTrue(studentAwardLetter.AreAnyAwardsPending);
            }

            [TestMethod]
            public void AreAnyAwardsPendingIsFalse_NoAwardPeriodsForYearTest()
            {
                studentAwardDtos.ForEach(sa => sa.StudentAwardPeriods = new List<Colleague.Dtos.FinancialAid.StudentAwardPeriod>());
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.AreAnyAwardsPending);
            }

            [TestMethod]
            public void AreAnyAwardsPendingIsFalse_NoAwardStatusesPassedTest()
            {
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, null, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.AreAnyAwardsPending);
            }

            [TestMethod]
            public void PendingAwardIsIgnored_AreAnyAwardsPendingIsFalseTest()
            {
                studentAwardDtos.ForEach(a => a.StudentAwardPeriods.ForEach(p => p.AwardStatusId = "A"));
                studentAwardDtos.First().StudentAwardPeriods.First().AwardStatusId = "E";
                studentAwardDtos.First().StudentAwardPeriods.First().IsIgnoredOnAwardLetter = true;
                studentAwardLetter = new StudentAwardLetter(studentAwardYearDto, studentAwardLetterDto, studentAwardDtos, awardStatusDtos, awardLetterConfigurationsList, configuration);
                Assert.IsFalse(studentAwardLetter.AreAnyAwardsPending);
            }
            
        }
    }
}
