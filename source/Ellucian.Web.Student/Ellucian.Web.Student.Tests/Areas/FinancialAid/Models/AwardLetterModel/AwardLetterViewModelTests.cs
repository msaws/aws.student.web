﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.AwardLetterModel
{
    [TestClass]
    public class AwardLetterViewModelTests
    {
        private TestModelData testModelDataObject;
        private AwardLetterViewModel awardLetterViewModel;
        private string studentId;
        private FAStudent student;

        public void BaseInitialize()
        {
            studentId = "0003914";
            testModelDataObject = new TestModelData(studentId);
            BuildAwardLetterViewModel();
        }

        private void BuildAwardLetterViewModel()
        {

            awardLetterViewModel = new AwardLetterViewModel(
                testModelDataObject.studentAwardYearsData,
                testModelDataObject.studentFaApplicationsData,
                testModelDataObject.studentDocumentsData,
                testModelDataObject.communicationCodesData,
                testModelDataObject.officeCodesData,
                testModelDataObject.studentData,
                testModelDataObject.financialAidCounselorData,
                testModelDataObject.financialAidOfficesData,
                testModelDataObject.studentChecklistsData,
                testModelDataObject.financialAidChecklistItemsData,
                testModelDataObject.currentUser);
        }

        private void UpdateAwardLetterViewModel()
        {
            var studentAwardYearDto = testModelDataObject.studentAwardYearsData.First();
            var method = awardLetterViewModel.GetType().GetMethod("UpdateViewModel", BindingFlags.NonPublic | BindingFlags.Instance)
                .Invoke(awardLetterViewModel, new object[] { testModelDataObject.awardStatusData, testModelDataObject.awardLetterConfigurationsData, 
                testModelDataObject.financialAidOfficesData, testModelDataObject.financialAidChecklistItemsData, 
                testModelDataObject.awardLetterHistoryRecordsData.First(r => r.AwardLetterYear == studentAwardYearDto.Code),
                testModelDataObject.studentAwardsData.Where(sa => sa.AwardYearId == studentAwardYearDto.Code), 
                studentAwardYearDto, testModelDataObject.studentChecklistsData});
        }

        [TestClass]
        public class AwardLetterViewModelConstructorTests : AwardLetterViewModelTests
        {

            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();                
            }

            [TestMethod]
            public void ModelCreated_InitializedModel()
            {
                Assert.IsNotNull(awardLetterViewModel);
                Assert.IsNotNull(awardLetterViewModel.Student);
                Assert.IsNotNull(awardLetterViewModel.AwardYears);
                Assert.IsNotNull(awardLetterViewModel.AwardLetterHistoryItems);
                Assert.IsNull(awardLetterViewModel.AwardPackageChecklistItemControlStatus);
                Assert.IsNull(awardLetterViewModel.StudentAwardLetter);
                Assert.IsFalse(awardLetterViewModel.IsProxyView);

            }

            [TestMethod]
            public void NoData_InitializedModel()
            {

                testModelDataObject.studentAwardYearsData = new List<StudentAwardYear2>();
                testModelDataObject.awardYearsData = new List<Colleague.Dtos.FinancialAid.AwardYear>();
                testModelDataObject.communicationCodesData = new List<Colleague.Dtos.Base.CommunicationCode2>();
                testModelDataObject.officeCodesData = new List<Colleague.Dtos.Base.OfficeCode>();
                testModelDataObject.studentData = new Colleague.Dtos.Student.Student();
                testModelDataObject.financialAidCounselorData = new FinancialAidCounselor();
                testModelDataObject.financialAidOfficesData = new List<FinancialAidOffice3>();
                testModelDataObject.studentFaApplicationsData = new List<FinancialAidApplication2>();
                testModelDataObject.studentDocumentsData = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
                testModelDataObject.studentChecklistsData = new List<StudentFinancialAidChecklist>();
                testModelDataObject.financialAidChecklistItemsData = new List<Colleague.Dtos.FinancialAid.ChecklistItem>();

                BuildAwardLetterViewModel();

                Assert.IsNotNull(awardLetterViewModel);
                Assert.IsNotNull(awardLetterViewModel.Student);
                Assert.IsNotNull(awardLetterViewModel.AwardYears);
                Assert.IsNotNull(awardLetterViewModel.AwardLetterHistoryItems);
            }

            [TestMethod]
            public void NoPersonData_StudentCreatedTest()
            {
                testModelDataObject.studentData = null;
                BuildAwardLetterViewModel();
                Assert.IsNotNull(awardLetterViewModel.Student);
                Assert.IsTrue(string.IsNullOrEmpty(awardLetterViewModel.Student.Id));
            }

            [TestMethod]
            public void NoFinancialAidOfficesData_NoAwardYearsCreatedTest()
            {
                testModelDataObject.financialAidOfficesData = null;
                BuildAwardLetterViewModel();
                Assert.IsFalse(awardLetterViewModel.AwardYears.Any());
            }

            [TestMethod]
            public void NoStudentChecklists_ArePrerequisitesSatisfiedIsFalseTest()
            {
                testModelDataObject.studentChecklistsData = new List<StudentFinancialAidChecklist>();
                BuildAwardLetterViewModel();
                foreach (var year in awardLetterViewModel.AwardYears)
                {
                    Assert.IsFalse(year.ArePrerequisitesSatisfied);
                }
            }

            [TestMethod]
            public void NoReferenceChecklistData_ArePrerequisitesSatisfiedIsFalseTest()
            {
                testModelDataObject.financialAidChecklistItemsData = null;
                BuildAwardLetterViewModel();
                foreach (var year in awardLetterViewModel.AwardYears)
                {
                    Assert.IsFalse(year.ArePrerequisitesSatisfied);
                }
            }            

            [TestMethod]
            public void AwardYearExistsForEachStudentAwardYearDtoTest()
            {
                var studentAwardYears = testModelDataObject.studentAwardYearsData;
                Assert.AreEqual(studentAwardYears.Count(), awardLetterViewModel.AwardYears.Count());
            }

            [TestMethod]
            public void AwardYearModelDescriptionTest()
            {
                var yearWithDescription = testModelDataObject.studentAwardYearsData[0];
                var yearSansDescription = testModelDataObject.studentAwardYearsData[1];

                var description = "foobar";
                yearWithDescription.Description = description;
                yearSansDescription.Description = string.Empty;

                BuildAwardLetterViewModel();

                var actualYearWithDescription = awardLetterViewModel.AwardYears.First(y => y.Code == yearWithDescription.Code);
                Assert.AreEqual(description, actualYearWithDescription.Description);

                var actualYearSansDescription = awardLetterViewModel.AwardYears.First(y => y.Code == yearSansDescription.Code);
                Assert.AreEqual(yearSansDescription.Code, actualYearSansDescription.Description);
            }

            [TestMethod]
            public void CounselorCreatedForEachYear_AwardLetterViewModel()
            {
                awardLetterViewModel.AwardYears.ToList().ForEach(y => Assert.IsNotNull(y.Counselor));
            }

            [TestMethod]
            public void CounselorNotCreated_NoExceptions_AwardLetterViewModel()
            {
                testModelDataObject.financialAidCounselorData.Name = string.Empty;
                BuildAwardLetterViewModel();
                awardLetterViewModel.AwardYears.ToList().ForEach(y => Assert.IsNull(y.Counselor));
            }

            [TestMethod]
            public void AwardYearsAreSortedTest()
            {
                var sortedYears = testModelDataObject.studentAwardYearsData.OrderByDescending(y => y.Code).ToList();

                for (int i = 0; i < sortedYears.Count(); i++)
                {
                    Assert.AreEqual(sortedYears[i].Code, awardLetterViewModel.AwardYears.ElementAt(i).Code);
                }
            }

            [TestMethod]
            public void FaStudentBuiltFromPersonTest()
            {
                Assert.AreEqual(testModelDataObject.studentData.Id, awardLetterViewModel.Student.Id);
                Assert.AreEqual(testModelDataObject.studentData.FirstName, awardLetterViewModel.Student.FirstName);
                Assert.AreEqual(testModelDataObject.studentData.LastName, awardLetterViewModel.Student.LastName);
                Assert.AreEqual(testModelDataObject.studentData.MiddleName, awardLetterViewModel.Student.MiddleName);
                CollectionAssert.AreEqual(testModelDataObject.studentData.PreferredAddress.ToArray(), awardLetterViewModel.Student.PreferredAddress.ToArray());
                Assert.AreEqual(testModelDataObject.studentData.PreferredName, awardLetterViewModel.Student.PreferredName);
            }

            [TestMethod]
            public void CurrentUserIsNull_IsProxyViewSetToFalseTest()
            {
                testModelDataObject.currentUser = null;
                BuildAwardLetterViewModel();
                Assert.IsFalse(awardLetterViewModel.IsProxyView);
            }

            [TestMethod]
            public void CurrentUserNoProxy_IsProxyViewSetToFalseTest()
            {
                Assert.IsFalse(awardLetterViewModel.IsProxyView);
            }

            [TestMethod]
            public void CurrentUserWithProxy_IsProxyViewSetToTrueTest()
            {
                awardLetterViewModel = new AwardLetterViewModel(
                    testModelDataObject.studentAwardYearsData,
                    testModelDataObject.studentFaApplicationsData,
                    testModelDataObject.studentDocumentsData,
                    testModelDataObject.communicationCodesData,
                    testModelDataObject.officeCodesData,
                    testModelDataObject.studentData,
                    testModelDataObject.financialAidCounselorData,
                    testModelDataObject.financialAidOfficesData,
                    testModelDataObject.studentChecklistsData,
                    testModelDataObject.financialAidChecklistItemsData,
                    testModelDataObject.currentUserWithProxy);
                Assert.IsTrue(awardLetterViewModel.IsProxyView);
            }

            [TestMethod]
            public void NoPersonData_PersonIdIsnullTest()
            {
                testModelDataObject.studentData = null;
                BuildAwardLetterViewModel();
                Assert.IsNull(awardLetterViewModel.PersonId);
            }

            [TestMethod]
            public void PersonId_EqualsExpectedTest()
            {
                Assert.AreEqual(testModelDataObject.studentData.Id, awardLetterViewModel.PersonId);
            }

            [TestMethod]
            public void NoPersonData_PersonNameIsnullTest()
            {
                testModelDataObject.studentData = null;
                BuildAwardLetterViewModel();
                Assert.IsNull(awardLetterViewModel.PersonName);
            }

            [TestMethod]
            public void PersonName_EqualsExpectedTest()
            {
                Assert.AreEqual(testModelDataObject.studentData.PreferredName, awardLetterViewModel.PersonName);
            }

            [TestMethod]
            public void NoPersonData_PrivacyStatusCodeIsnullTest()
            {
                testModelDataObject.studentData = null;
                BuildAwardLetterViewModel();
                Assert.IsNull(awardLetterViewModel.PrivacyStatusCode);
            }

            [TestMethod]
            public void PrivacyStatusCode_EqualsExpectedTest()
            {
                Assert.AreEqual(testModelDataObject.studentData.PrivacyStatusCode, awardLetterViewModel.PrivacyStatusCode);
            }
        }

        [TestClass]
        public class UpdateAwardLetterViewModelTests : AwardLetterViewModelTests
        {
            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();
                student = new FAStudent(testModelDataObject.studentData);
                UpdateAwardLetterViewModel();
            }

            [TestMethod]
            public void AwardLetterViewModelUpdatedTest()
            {
                Assert.IsNotNull(awardLetterViewModel.StudentAwardLetter);
                Assert.IsNotNull(awardLetterViewModel.AwardPackageChecklistItemControlStatus);
                Assert.IsNotNull(awardLetterViewModel.AwardLetterHistoryItems);
            }

            [TestMethod]
            public void AwardPackageControlStatus_CompletionRequiredTest()
            {

                var studentAwardYearDto = testModelDataObject.studentAwardYearsData.First();
                testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations)
                    .FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code).IsAwardingActive = true;
                testModelDataObject.studentChecklistsData.FirstOrDefault(sc => sc.AwardYear == studentAwardYearDto.Code)
                    .ChecklistItems.FirstOrDefault(cli => cli.Code == "ACCAWDPKG").ControlStatus = ChecklistItemControlStatus.CompletionRequired;
                UpdateAwardLetterViewModel();
                Assert.IsTrue(awardLetterViewModel.AwardPackageChecklistItemControlStatus == ChecklistItemControlStatus.CompletionRequired);
            }

            [TestMethod]
            public void AwardPackageControlStatus_SkippedTest()
            {
                var studentAwardYearDto = testModelDataObject.studentAwardYearsData.First();
                testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations)
                    .FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code).IsAwardingActive = true;
                testModelDataObject.studentChecklistsData.FirstOrDefault(sc => sc.AwardYear == studentAwardYearDto.Code).ChecklistItems.FirstOrDefault(cli => cli.Code == "ACCAWDPKG").ControlStatus = ChecklistItemControlStatus.CompletionRequiredLater;
                UpdateAwardLetterViewModel();
                
                Assert.IsTrue(awardLetterViewModel.AwardPackageChecklistItemControlStatus == ChecklistItemControlStatus.CompletionRequiredLater);
            }

            [TestMethod]
            public void AwardPackageControlStatus_NullBasedOnConfigurationTest()
            {
                var studentAwardYearDto = testModelDataObject.studentAwardYearsData.First();
                testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations)
                    .FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code).IsAwardingActive = false;
                testModelDataObject.studentChecklistsData.FirstOrDefault(sc => sc.AwardYear == studentAwardYearDto.Code).ChecklistItems.FirstOrDefault(cli => cli.Code == "ACCAWDPKG").ControlStatus = ChecklistItemControlStatus.CompletionRequired;
                UpdateAwardLetterViewModel();
                
                Assert.IsNull(awardLetterViewModel.AwardPackageChecklistItemControlStatus);
            }
            
            [TestMethod]
            public void AwardLetterHistoryItems_ContainsExpectedNumberOfItemsTest()
            {
                var studentAwardYearDto = testModelDataObject.studentAwardYearsData.FirstOrDefault();
                var studentAwardLetterDto = testModelDataObject.awardLetterHistoryRecordsData.First(r => r.AwardLetterYear == studentAwardYearDto.Code);                
                var expectedCount = studentAwardYearDto.AwardLetterHistoryItemsForYear.Count() - 1;
                Assert.AreEqual(expectedCount, awardLetterViewModel.AwardLetterHistoryItems.Count());

            }

            [TestMethod]
            public void NotIsAwardLetterHistoryActive_NoAwardLetterHistoryItemsForYearTest()
            {
                var year = testModelDataObject.studentAwardYearsData.First();
                var configurationForYear = testModelDataObject.financialAidOfficesData.First(fo => fo.Id == year.FinancialAidOfficeId).Configurations.First(c => c.AwardYear == year.Code);
                configurationForYear.IsAwardLetterHistoryActive = false;
                UpdateAwardLetterViewModel();

                Assert.IsFalse(awardLetterViewModel.AwardLetterHistoryItems.Any());
            }

            [TestMethod]
            public void NoConfigurationForYear_NoAwardLetterHistoryItemsForYearTest()
            {
                var year = testModelDataObject.studentAwardYearsData.First();
                testModelDataObject.financialAidOfficesData.First(fo => fo.Id == year.FinancialAidOfficeId).Configurations = new List<FinancialAidConfiguration3>();
                UpdateAwardLetterViewModel();

                Assert.IsFalse(awardLetterViewModel.AwardLetterHistoryItems.Any());
            }

            [TestMethod]
            public void NoHistoryItemsForYear_NoAwardLetterHistoryItemsTest()
            {
                var year = testModelDataObject.studentAwardYearsData.First().Code;
                testModelDataObject.studentAwardYearsData.First().AwardLetterHistoryItemsForYear = new List<Ellucian.Colleague.Dtos.FinancialAid.AwardLetterHistoryItem>();

                UpdateAwardLetterViewModel();
                Assert.IsFalse(awardLetterViewModel.AwardLetterHistoryItems.Any());
            }

            [TestMethod]
            public void NoStudentChecklistItems_AwardPackageControlStatusIsNullTest()
            {
                testModelDataObject.studentChecklistsData = new List<StudentFinancialAidChecklist>();
                UpdateAwardLetterViewModel();
                Assert.IsNull(awardLetterViewModel.AwardPackageChecklistItemControlStatus);
            }

            [TestMethod]
            public void NoReferenceChecklistItems_AwardPackageControlStatusIsNullTest()
            {
                testModelDataObject.financialAidChecklistItemsData = new List<Colleague.Dtos.FinancialAid.ChecklistItem>();
                UpdateAwardLetterViewModel();
                Assert.IsNull(awardLetterViewModel.AwardPackageChecklistItemControlStatus);
            }

            [TestMethod]
            public void NoFinancialAidOfficeData_AwardPackageChecklistItemContrlStatusIsNullTest()
            {
                testModelDataObject.financialAidOfficesData = null;
                UpdateAwardLetterViewModel();
                Assert.IsNull(awardLetterViewModel.AwardPackageChecklistItemControlStatus);
            }

            [TestMethod]
            public void NoFinancialAidOfficeData_AwardLetterHistoryItemsListEmptyTest()
            {
                testModelDataObject.financialAidOfficesData = null;
                UpdateAwardLetterViewModel();
                Assert.IsFalse(awardLetterViewModel.AwardLetterHistoryItems.Any());
            }
        }
    }
}