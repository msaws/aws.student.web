﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.AwardLetterModel
{
    [TestClass]
    public class AwardLetterStudentAwardTests
    {
        private List<StudentAwardPeriod> studentAwardPeriods;
        private decimal expectedTotal;
        private AwardLetterStudentAward awardLetterStudentAward;

        [TestInitialize]
        public void Initialize()
        {
            studentAwardPeriods = new List<StudentAwardPeriod>()
            {
                new StudentAwardPeriod()
                {
                    AwardAmount = 5,
                },
                new StudentAwardPeriod()
                {
                    AwardAmount = 10
                }
            };

            expectedTotal = 2600;

            awardLetterStudentAward = new AwardLetterStudentAward();
        }

        [TestMethod]
        public void ObjectExistsTest()
        {
            Assert.IsNotNull(awardLetterStudentAward);
        }

        [TestMethod]
        public void TotalAmountTest()
        {
            awardLetterStudentAward.TotalAwardAmount = expectedTotal;
            Assert.AreEqual(expectedTotal, awardLetterStudentAward.TotalAwardAmount);
        }

        [TestMethod]
        public void NullAwardPeriodsExpectZeroTotalTest()
        {
            studentAwardPeriods = null;
            awardLetterStudentAward.StudentAwardPeriods = studentAwardPeriods;
            Assert.AreEqual(null, awardLetterStudentAward.TotalAwardAmount);
        }

        [TestMethod]
        public void EmptyAwardPeriodsExpectZeroTotalTest()
        {
            studentAwardPeriods = new List<StudentAwardPeriod>();
            awardLetterStudentAward.StudentAwardPeriods = studentAwardPeriods;
            Assert.AreEqual(null, awardLetterStudentAward.TotalAwardAmount);
        }
    }
}
