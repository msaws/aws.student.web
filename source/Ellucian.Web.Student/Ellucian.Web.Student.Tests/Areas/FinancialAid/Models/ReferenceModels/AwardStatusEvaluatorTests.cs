﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ReferenceModels
{
    [TestClass]
    public class AwardStatusEvaluatorTests
    {
        private List<Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod> periodsList;
        private List<AwardStatus> awardStatuses;
        private FinancialAidConfiguration3 configuration;

        private TestModelData testModeldata;

        public void TestInitialize()
        {
            testModeldata = new TestModelData("0004791");
            periodsList = new List<Student.Areas.FinancialAid.Models.StudentAwardPeriod>()
            {
                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = "ZEBRA",
                    AwardYearId = "2016",
                    IsStatusModifiable = true,
                    IsAmountModifiable = true,
                    AwardAmount = 2000,
                    AwardStatus = new AwardStatus(){
                        Category = AwardStatusCategory.Pending,
                        Code = "P"
                    },
                    Code = "15/FA"
                },
                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = "ZEBRA",
                    AwardYearId = "2016",
                    IsStatusModifiable = true,
                    IsAmountModifiable = true,
                    AwardAmount = 2000,
                    AwardStatus = new AwardStatus(){
                        Category = AwardStatusCategory.Pending,
                        Code = "P"
                    },
                    Code = "16/SP"
                },
                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = "SUB",
                    AwardYearId = "2016",
                    IsStatusModifiable = true,
                    IsAmountModifiable = true,
                    AwardAmount = 5000,
                    AwardStatus = new AwardStatus(){
                        Category = AwardStatusCategory.Pending,
                        Code = "P"
                    },
                    Code = "16/SP"
                },
                new Ellucian.Web.Student.Areas.FinancialAid.Models.StudentAwardPeriod(){
                    AwardId = "AMERC",
                    AwardYearId = "2016",
                    IsStatusModifiable = true,
                    IsAmountModifiable = true,
                    AwardAmount = 500,
                    AwardStatus = new AwardStatus(){
                        Category = AwardStatusCategory.Pending,
                        Code = "P"
                    },
                    Code = "16/SP"
                }
            };
            awardStatuses = testModeldata.awardStatusData;
            configuration = testModeldata.financialAidOfficesData.First().Configurations.First();
        }

        public void TestCleanup()
        {
            testModeldata = null;
            periodsList = null;
            awardStatuses = null;
            configuration = null;
        }

        [TestClass]
        public class CalculateAwardStatusWithPeriodsListArgTests : AwardStatusEvaluatorTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TestInitialize();
            }

            [TestCleanup]
            public void Cleanup()
            {
                TestCleanup();
            }

            [TestMethod]
            public void NullPeriodsList_CalculateAwardStatusReturnsNullTest()
            {
                Assert.IsNull(AwardStatusEvaluator.CalculateAwardStatus(null));
            }

            [TestMethod]
            public void EmptyPeriodsList_CalculateAwardStatusReturnsNullTest()
            {
                periodsList = new List<Student.Areas.FinancialAid.Models.StudentAwardPeriod>();
                Assert.IsNull(AwardStatusEvaluator.CalculateAwardStatus(periodsList));
            }

            [TestMethod]
            public void AllStatusesPending_CalculateAwardStatusReturnsPendingStatusTest()
            {
                Assert.AreEqual(AwardStatusCategory.Pending, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AllStatusesEstimated_CalculateAwardStatusReturnsEstimatedStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Estimated });
                Assert.AreEqual(AwardStatusCategory.Estimated, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AllStatusesAccepted_CalculateAwardStatusReturnsAcceptedStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Accepted });
                Assert.AreEqual(AwardStatusCategory.Accepted, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AllStatusesRejected_CalculateAwardStatusReturnsRejectedStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Rejected });
                Assert.AreEqual(AwardStatusCategory.Rejected, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AllStatusesDenied_CalculateAwardStatusReturnsDeniedStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Denied });
                Assert.AreEqual(AwardStatusCategory.Denied, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AllStatusesRejectedOrDenied_CalculateAwardStatusReturnsExpectedStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Denied });
                periodsList.Last().AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Rejected };
                Assert.AreEqual(AwardStatusCategory.Denied, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AtLeastOneStatusPending_CalculateAwardStatusReturnsPendingStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Denied });
                periodsList.First().AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Pending };
                Assert.AreEqual(AwardStatusCategory.Pending, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AtLeastOneStatusEstimated_CalculateAwardStatusReturnsEstimatedStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Accepted });
                periodsList.First().AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Estimated};
                Assert.AreEqual(AwardStatusCategory.Estimated, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }

            [TestMethod]
            public void AtLeastOneStatusAcceptedWithRestRejected_CalculateAwardStatusReturnsAcceptedStatusTest()
            {
                periodsList.ForEach(p => p.AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Denied });
                periodsList.First().AwardStatus = new AwardStatus() { Category = AwardStatusCategory.Accepted };
                Assert.AreEqual(AwardStatusCategory.Accepted, AwardStatusEvaluator.CalculateAwardStatus(periodsList).Category);
            }
        }

        [TestClass]
        public class CalculateAwardStatusWithOptionalExcludeIgnoredArgTests : AwardStatusEvaluatorTests
        {
            private List<Ellucian.Colleague.Dtos.FinancialAid.StudentAwardPeriod> studentAwardPeriods;

            [TestInitialize]
            public void Initialize()
            {
                TestInitialize();
                studentAwardPeriods = testModeldata.studentAwardsData.SelectMany(a => a.StudentAwardPeriods).ToList();
            }

            [TestCleanup]
            public void Cleanup()
            {
                TestCleanup();
            }

            [TestMethod]
            public void NoExcludeIgnoredArgument_ExpectedAwardStatusReturnedTest()
            {
                Assert.AreEqual(AwardStatusCategory.Pending, AwardStatusEvaluator.CalculateAwardStatus(studentAwardPeriods, awardStatuses).Category);
            }

            [TestMethod]
            public void NoIgnoredStudentAwardPeriods_ExpectedAwardStatusReturnedTest()
            {
                studentAwardPeriods.ForEach(ap => ap.IsIgnoredOnAwardLetter = false);
                Assert.AreEqual(AwardStatusCategory.Pending, AwardStatusEvaluator.CalculateAwardStatus(studentAwardPeriods, awardStatuses, true).Category);
            }

            [TestMethod]
            public void PendingActionStatusIgnored_ExpectedAwardStatusReturnedTest()
            {
                studentAwardPeriods.Where(p => p.AwardStatusId == "P").ToList().ForEach(p => p.IsIgnoredOnAwardLetter = true);
                Assert.AreEqual(AwardStatusCategory.Accepted, AwardStatusEvaluator.CalculateAwardStatus(studentAwardPeriods, awardStatuses, true).Category);
            }

            [TestMethod]
            public void OfferedActionStatusIgnored_ExpectedAwardStatusReturnedTest()
            {
                studentAwardPeriods.Where(p => p.AwardStatusId == "O").ToList().ForEach(p => p.IsIgnoredOnAwardLetter = true);
                Assert.AreEqual(AwardStatusCategory.Pending, AwardStatusEvaluator.CalculateAwardStatus(studentAwardPeriods, awardStatuses, true).Category);
            }

            [TestMethod]
            public void OfferedAndPendingActionStatusesIgnored_ExpectedAwardStatusReturnedTest()
            {
                studentAwardPeriods.Where(p => p.AwardStatusId == "O" || p.AwardStatusId == "P").ToList().ForEach(p => p.IsIgnoredOnAwardLetter = true);
                Assert.AreEqual(AwardStatusCategory.Accepted, AwardStatusEvaluator.CalculateAwardStatus(studentAwardPeriods, awardStatuses, true).Category);
            }

            [TestMethod]
            public void AllExistingActionStatusesIgnored_NullReturnedTest()
            {
                studentAwardPeriods.ForEach(ap => ap.IsIgnoredOnAwardLetter = true);
                Assert.IsNull(AwardStatusEvaluator.CalculateAwardStatus(studentAwardPeriods, awardStatuses, true));
            }
        }
        
    }
}
