﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ReferenceModels
{
    /// <summary>
    /// Tests class for PercentageChartCollection class
    /// </summary>
    [TestClass]
    public class PercentageChartCollectionTests
    {
        private List<PercentageByAwardTypeChartModel> percentageChartModels;
        
        [TestInitialize]
        public void Initialize()
        {
            percentageChartModels = new List<PercentageByAwardTypeChartModel>()
            {
                new PercentageByAwardTypeChartModel(15000, 4500, AwardCategoryType.Award),
                new PercentageByAwardTypeChartModel(15000, 9500, AwardCategoryType.Loan),
                new PercentageByAwardTypeChartModel(15000, 1000, AwardCategoryType.Work)
            };
        }
    }
}
