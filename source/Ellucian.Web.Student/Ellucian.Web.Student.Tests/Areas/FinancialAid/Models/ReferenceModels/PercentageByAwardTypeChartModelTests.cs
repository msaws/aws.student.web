﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ReferenceModels
{
    /// <summary>
    /// Test class for PercentageByAwardTypeChartModel
    /// </summary>
    [TestClass]
    public class PercentageByAwardTypeChartModelTests
    {
        private PercentageByAwardTypeChartModel percentageChartModel;
        private decimal totalAwardedAmount;
        private decimal totalAmountForAwardType;
        private AwardCategoryType awardCategoryType;

        [TestInitialize]
        public void Initialize()
        {
            totalAwardedAmount = 2000;
            totalAmountForAwardType = 765;
            awardCategoryType = AwardCategoryType.Award;
            percentageChartModel = new PercentageByAwardTypeChartModel(totalAwardedAmount, totalAmountForAwardType, awardCategoryType);
        }

        [TestCleanup]
        public void Cleanup()
        {
            percentageChartModel = null;
        }

        [TestMethod]
        public void PercentageChartModelNotNullTest()
        {
            Assert.IsNotNull(percentageChartModel);
            Assert.IsNotNull(percentageChartModel.ChartData);
            Assert.IsNotNull(percentageChartModel.ChartOptions);
        }

        [TestMethod]
        public void PercentageChartModel_AllPropertiesMatchTest()
        {
            decimal awardPercentage = (int)Math.Round((totalAmountForAwardType/totalAwardedAmount * 100), 0);
            decimal otherAwardsPercentage = 100 - awardPercentage;
            Assert.AreEqual(awardPercentage, percentageChartModel.PercentageForAwardType);
            Assert.AreEqual(otherAwardsPercentage, percentageChartModel.PercentageForAllOtherAwards);
            Assert.AreEqual(AwardCategoryType.Award, percentageChartModel.AwardCategoryType);
        }

        //[TestMethod]
        //[ExpectedException (typeof(ApplicationException))]
        //public void TotalAwardedAmountEqualsZero_ExceptionThrownTest()
        //{
        //    percentageChartModel = new PercentageByAwardTypeChartModel(0, totalAmountForAwardType, awardCategoryType);
        //}

        //[TestMethod]
        //[ExpectedException (typeof(ApplicationException))]
        //public void TotalAmountForAwardTypeEqualsZero_ExceptionThrownTest()
        //{
        //    percentageChartModel = new PercentageByAwardTypeChartModel(totalAwardedAmount, 0, awardCategoryType);
        //}

        //[TestMethod]
        //[ExpectedException (typeof(ApplicationException))]
        //public void TotalAwardedAndAmountForAwardTypeNgtZero_ExceptionThrownTest()
        //{
        //    percentageChartModel = new PercentageByAwardTypeChartModel(0, -2, awardCategoryType);
        //}

        [TestMethod]
        public void WorkAwardCategoryType_GetSetTest()
        {
            percentageChartModel = new PercentageByAwardTypeChartModel(totalAwardedAmount, totalAmountForAwardType, AwardCategoryType.Work);
            Assert.AreEqual(AwardCategoryType.Work, percentageChartModel.AwardCategoryType);
        }

        [TestMethod]
        public void LoanAwardCategoryType_GetSetTest()
        {
            percentageChartModel = new PercentageByAwardTypeChartModel(totalAwardedAmount, totalAmountForAwardType, AwardCategoryType.Loan);
            Assert.AreEqual(AwardCategoryType.Loan, percentageChartModel.AwardCategoryType);
        }
        
        [TestMethod]
        public void DefaultAwardCategoryType_GetSetTest()
        {
            percentageChartModel = new PercentageByAwardTypeChartModel(totalAwardedAmount, totalAmountForAwardType, null);
            Assert.AreEqual(AwardCategoryType.Award, percentageChartModel.AwardCategoryType);
        }
    }
}
