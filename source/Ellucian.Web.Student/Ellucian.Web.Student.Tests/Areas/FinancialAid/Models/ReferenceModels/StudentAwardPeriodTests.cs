﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates*/
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ReferenceModels
{
    [TestClass]
    public class StudentAwardPeriodTests
    {
        private string awardId, awardYearId, code, description;
        private DateTime startDate;

        private Colleague.Dtos.FinancialAid.AwardStatus awardStatus;
        private decimal? originalAwardAmount, awardAmount;
        private bool isTransmitted, isActive, isFrozen, isAmountModifiable, 
            isStatusModifiable, isIgnoredOnAwardLetter, isViewableOnAwardLetter;

        private StudentAwardPeriod studentAwardPeriod;

        public void TestInitialize()
        {
            awardId = "Woofy";
            awardYearId = "2016";
            code = "16/FA";
            description = "2016 Fall";
            startDate = new DateTime(2016, 09, 01);
            awardStatus = new Colleague.Dtos.FinancialAid.AwardStatus()
            {
                Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Pending,
                Code = "P",
                Description = "Pending"
            };
            originalAwardAmount = 2500m;
            awardAmount = 2500m;
            isTransmitted = false;
            isActive = true;
            isFrozen = false;
            isAmountModifiable = false;
            isStatusModifiable = true;
            isIgnoredOnAwardLetter = false;
            isViewableOnAwardLetter = true;
        }

        [TestClass]
        public class StudentAwardPeriodDefaultConstructorTests : StudentAwardPeriodTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TestInitialize();
                studentAwardPeriod = new StudentAwardPeriod();
            }

            [TestMethod]
            public void ObjectIsNotNullTest()
            {
                Assert.IsNotNull(studentAwardPeriod);
            }

            [TestMethod]
            public void AwardStatusProperty_IsNotNullTest()
            {
                Assert.IsNotNull(studentAwardPeriod.AwardStatus);
            }

            [TestMethod]
            public void IsActive_InitializedToTrueTest()
            {
                Assert.IsTrue(studentAwardPeriod.IsActive);
            }
        }

        [TestClass]
        public class StudentAwardPeriodConstructorTests : StudentAwardPeriodTests
        {
            Colleague.Dtos.FinancialAid.StudentAwardPeriod studentAwardPeriodDto;
            Colleague.Dtos.FinancialAid.AwardPeriod awardPeriodDto;
            Colleague.Dtos.FinancialAid.AwardStatus awardStatusDto;

            [TestInitialize]
            public void Initialize()
            {
                TestInitialize();
                studentAwardPeriodDto = new Colleague.Dtos.FinancialAid.StudentAwardPeriod(){
                    AwardId = awardId,
                    AwardYearId = awardYearId,
                    AwardAmount = awardAmount,
                    IsTransmitted = isTransmitted,
                    IsAmountModifiable = isAmountModifiable,
                    IsStatusModifiable = isStatusModifiable,
                    IsFrozen = isFrozen,
                    IsIgnoredOnAwardLetter = isIgnoredOnAwardLetter,
                    IsViewableOnAwardLetterAndShoppingSheet = isViewableOnAwardLetter                    
                };

                awardPeriodDto = new Colleague.Dtos.FinancialAid.AwardPeriod(){
                    Code = code,
                    Description = description,
                    StartDate = startDate
                };

                awardStatusDto = awardStatus;

                studentAwardPeriod = new StudentAwardPeriod(studentAwardPeriodDto, awardPeriodDto, awardStatusDto);
            }

            [TestMethod]
            public void ObjectIsNotNullTest()
            {
                Assert.IsNotNull(studentAwardPeriod);
            }

            [TestMethod]
            public void AllProperties_EqualExpectedTest()
            {
                Assert.AreEqual(awardId, studentAwardPeriod.AwardId);
                Assert.AreEqual(awardYearId, studentAwardPeriod.AwardYearId);
                Assert.AreEqual(awardAmount, studentAwardPeriod.AwardAmount);
                Assert.AreEqual(awardAmount, studentAwardPeriod.OriginalAwardAmount);
                Assert.AreEqual(code, studentAwardPeriod.Code);
                Assert.AreEqual(description, studentAwardPeriod.Description);
                Assert.AreEqual(startDate, studentAwardPeriod.StartDate);
                Assert.IsTrue(studentAwardPeriod.IsActive);
                Assert.AreEqual(isTransmitted, studentAwardPeriod.IsTransmitted);
                Assert.AreEqual(isFrozen, studentAwardPeriod.IsFrozen);
                Assert.AreEqual(isAmountModifiable, studentAwardPeriod.IsAmountModifiable);
                Assert.AreEqual(isStatusModifiable, studentAwardPeriod.IsStatusModifiable);
                Assert.AreEqual(isIgnoredOnAwardLetter, studentAwardPeriod.IsIgnoredOnAwardLetter);
                Assert.AreEqual(isViewableOnAwardLetter, studentAwardPeriod.IsViewableOnAwardLetter);
                Assert.IsFalse(studentAwardPeriod.IsDummy);
            }

            [TestMethod]
            public void DeniedAwardStatus_IsActive_SetToFalseTest()
            {
                awardStatus.Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Denied;
                studentAwardPeriod = new StudentAwardPeriod(studentAwardPeriodDto, awardPeriodDto, awardStatus);
                Assert.IsFalse(studentAwardPeriod.IsActive);
            }

            [TestMethod]
            public void RejectedAwardStatus_IsActive_SetToFalseTest()
            {
                awardStatus.Category = Colleague.Dtos.FinancialAid.AwardStatusCategory.Rejected;
                studentAwardPeriod = new StudentAwardPeriod(studentAwardPeriodDto, awardPeriodDto, awardStatus);
                Assert.IsFalse(studentAwardPeriod.IsActive);
            }
        }

        [TestClass]
        public class StudentAwardPeriodCreateDummyTests : StudentAwardPeriodTests
        {
            public static StudentAwardPeriod awardPeriodDummy;
            Colleague.Dtos.FinancialAid.AwardPeriod awardPeriodDto;

            [TestInitialize]
            public void Initialize()
            {
                TestInitialize();

                awardPeriodDto = new Colleague.Dtos.FinancialAid.AwardPeriod()
                {
                    Code = code,
                    Description = description,
                    StartDate = startDate
                };
                awardPeriodDummy = StudentAwardPeriod.CreateDummy(awardId, awardYearId, awardPeriodDto, awardStatus);
            }

            [TestMethod]
            public void ObjectIsNotNullTest()
            {
                Assert.IsNotNull(awardPeriodDummy);
            }

            [TestMethod]
            public void AllProperties_EqualExpectedTest()
            {
                Assert.AreEqual(awardId, awardPeriodDummy.AwardId);
                Assert.AreEqual(awardYearId, awardPeriodDummy.AwardYearId);
                Assert.AreEqual(0, awardPeriodDummy.AwardAmount);
                Assert.AreEqual(code, awardPeriodDummy.Code);
                Assert.AreEqual(description, awardPeriodDummy.Description);
                Assert.AreEqual(startDate, awardPeriodDummy.StartDate);
                Assert.AreEqual(awardStatus, awardPeriodDummy.AwardStatus);
                Assert.IsTrue(awardPeriodDummy.IsDummy);
                Assert.IsFalse(awardPeriodDummy.IsTransmitted);
                Assert.IsFalse(awardPeriodDummy.IsActive);
                Assert.IsFalse(awardPeriodDummy.IsAmountModifiable);
                Assert.IsFalse(awardPeriodDummy.IsFrozen);
            }
        }
    }
}
