﻿/*Copyright 2015-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ReferenceModels
{
    [TestClass]
    public class AwardYearConfigurationTests
    {
        private TestModelData testData;
        private string studentId;
        private AwardYearConfiguration configuration;
        private FinancialAidOffice3 officeDto;
        private FinancialAidConfiguration3 configurationDto;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0004791";
            testData = new TestModelData(studentId);
            officeDto = testData.financialAidOfficesData.First();
            configurationDto = officeDto.Configurations.First();
        }

        [TestCleanup]
        public void Cleanup()
        {
            testData = null;
            officeDto = null;
            configurationDto = null;
        }

        #region Tests for Default constructor
        [TestMethod]
        public void AwardYearConfiguration_DefaultConstructorTest()
        {
            configuration = new AwardYearConfiguration();
            Assert.IsNotNull(configuration);
            Assert.IsFalse(configuration.AllowAnnualAwardUpdatesOnly);
            Assert.IsFalse(configuration.AreAwardingChangesAllowed);
            Assert.IsFalse(configuration.IsDeclinedStatusChangeReviewRequired);
            Assert.IsFalse(configuration.IsLoanAmountChangeReviewRequired);
            Assert.IsFalse(configuration.IsLoanRequestEnabled);
            Assert.IsFalse(configuration.SuppressMaximumLoanLimits);
            Assert.IsNull(configuration.AwardYearCode);
            Assert.IsNull(configuration.ShoppingSheetConfiguration);
            Assert.IsFalse(configuration.DisplayPellLifetimeEligibilityUsedPercentage);
        }
        #endregion

        #region Tests for constructor with config dto argument
        [TestMethod]
        public void AwardYearConfiguration_ConstructorWithConfigurationDtoTest()
        {
            configuration = new AwardYearConfiguration(configurationDto);
            Assert.IsNotNull(configuration);
            Assert.AreEqual(configurationDto.AwardYear, configuration.AwardYearCode);
            Assert.AreEqual(configurationDto.AllowAnnualAwardUpdatesOnly, configuration.AllowAnnualAwardUpdatesOnly);
            Assert.AreEqual(configurationDto.AreAwardChangesAllowed, configuration.AreAwardingChangesAllowed);
            Assert.AreEqual(configurationDto.AreLoanRequestsAllowed, configuration.IsLoanRequestEnabled);
            Assert.AreEqual(configurationDto.IsLoanAmountChangeRequestRequired, configuration.IsLoanAmountChangeReviewRequired);
            Assert.AreEqual(configurationDto.IsDeclinedStatusChangeRequestRequired, configuration.IsDeclinedStatusChangeReviewRequired);
            Assert.AreEqual(configurationDto.SuppressMaximumLoanLimits, configuration.SuppressMaximumLoanLimits);
            Assert.IsNotNull(configuration.ShoppingSheetConfiguration);
            Assert.AreEqual(configurationDto.DisplayPellLifetimeEarningsUsed, configuration.DisplayPellLifetimeEligibilityUsedPercentage);
        }

        [TestMethod]
        [ExpectedException (typeof(ArgumentNullException))]
        public void AwardYearConfiguration_NullConfigurationDtoPassedTest()
        {
            configuration = new AwardYearConfiguration(null);
        }

        [TestMethod]
        public void AwardYearConfiguration_ShoppingSheetConfigurationIsNotNullTest()
        {
            configurationDto.ShoppingSheetConfiguration = null;
            configuration = new AwardYearConfiguration(configurationDto);
            Assert.IsNotNull(configuration.ShoppingSheetConfiguration);
        }
        #endregion

        #region Tests for constructor with officeDto and configurationDto arguments

        [TestMethod]
        public void AwardYearConfiguration_ConstructorTest()
        {
            configuration = new AwardYearConfiguration(officeDto, configurationDto);
            Assert.IsNotNull(configuration);
            Assert.AreEqual(configurationDto.AwardYear, configuration.AwardYearCode);
            Assert.AreEqual(configurationDto.AllowAnnualAwardUpdatesOnly, configuration.AllowAnnualAwardUpdatesOnly);
            Assert.AreEqual(configurationDto.AreAwardChangesAllowed, configuration.AreAwardingChangesAllowed);
            Assert.AreEqual(configurationDto.AreLoanRequestsAllowed, configuration.IsLoanRequestEnabled);
            Assert.AreEqual(configurationDto.IsLoanAmountChangeRequestRequired, configuration.IsLoanAmountChangeReviewRequired);
            Assert.AreEqual(configurationDto.IsDeclinedStatusChangeRequestRequired, configuration.IsDeclinedStatusChangeReviewRequired);
            Assert.AreEqual(configurationDto.SuppressMaximumLoanLimits, configuration.SuppressMaximumLoanLimits);
            Assert.AreEqual(configurationDto.DisplayPellLifetimeEarningsUsed, configuration.DisplayPellLifetimeEligibilityUsedPercentage);
            Assert.AreEqual(configurationDto.AllowDeclineZeroOfAcceptedLoans, configuration.AllowDeclineZeroOfAcceptedLoans);
            Assert.IsNotNull(configuration.ShoppingSheetConfiguration);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AwardYearConfiguration_NullOfficeAndConfigurationDtoPassedTest()
        {
            configuration = new AwardYearConfiguration(null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullConfigurationDtoPassed_ExceptionThrownTest()
        {
            configuration = new AwardYearConfiguration(officeDto, null);
        }

        [TestMethod]
        public void NullOfficeDtoPassed_NoExceptionThrownTest()
        {
            configuration = new AwardYearConfiguration(null, configurationDto);
            Assert.IsNotNull(configuration);
        }

        #endregion
    }
}
