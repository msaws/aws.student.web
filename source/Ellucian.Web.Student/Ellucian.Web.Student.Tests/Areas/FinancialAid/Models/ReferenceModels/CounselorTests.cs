﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.ReferenceModels
{
    [TestClass]
    public class CounselorConstructorTests
    {
        public TestModelData testModelData;

        public StudentAwardYear2 inputStudentAwardYear;

        public Counselor actualCounselor;        

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0003914");

            //use an input year that has a configuration
            inputStudentAwardYear = testModelData.studentAwardYearsData.FirstOrDefault(y => testModelData.financialAidOfficesData.SelectMany(o => o.Configurations).GetConfigurationDtoOrDefault(y) != null);
        }

        [TestMethod]
        public void SingleAwardYearConstructorUsesSetPropertiesMethodTest()
        {
            var expectedCounselor = new Counselor();
            var phoneNumber = testModelData.counselorsPhoneNumbersData.First(pnd => pnd.PersonId == testModelData.financialAidCounselorData.Id);
            expectedCounselor.SetCounselorPropertiesFromDtos(testModelData.financialAidCounselorData, phoneNumber, inputStudentAwardYear, testModelData.financialAidOfficesData, false);

            actualCounselor = new Counselor(testModelData.financialAidCounselorData, phoneNumber, inputStudentAwardYear, testModelData.financialAidOfficesData);
            Assert.AreEqual(expectedCounselor, actualCounselor);
        }

        [TestMethod]
        public void MultiAwardYearConstructor_GetMostRecentAwardYear_UsesSetPropertiesMethodTest()
        {
            var mostRecentAwardYear = testModelData.studentAwardYearsData.OrderByDescending(y => y.Code).First();

            var expectedCounselor = new Counselor();
            var phoneNumber = testModelData.counselorsPhoneNumbersData.First(pnd => pnd.PersonId == testModelData.financialAidCounselorData.Id);
            expectedCounselor.SetCounselorPropertiesFromDtos(testModelData.financialAidCounselorData, phoneNumber, mostRecentAwardYear, testModelData.financialAidOfficesData, false);

            actualCounselor = new Counselor(testModelData.financialAidCounselorData, testModelData.studentAwardYearsData, testModelData.financialAidOfficesData, testModelData.counselorsPhoneNumbersData.First(p => p.PersonId == testModelData.financialAidCounselorData.Id));
            Assert.AreEqual(expectedCounselor, actualCounselor);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullStudentAwardYearDtoListTest()
        {
            new Counselor(testModelData.financialAidCounselorData, null, testModelData.financialAidOfficesData, testModelData.counselorsPhoneNumbersData.First(p => p.PersonId == testModelData.financialAidCounselorData.Id));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void EmptyStudentAwardYearDtoListTest()
        {
            new Counselor(testModelData.financialAidCounselorData, new List<StudentAwardYear2>(), testModelData.financialAidOfficesData, testModelData.counselorsPhoneNumbersData.First(p => p.PersonId == testModelData.financialAidCounselorData.Id));
        }        

    }

    [TestClass]
    public class SetCounselorPropertiesFromDtosTests
    {
        public TestModelData testModelData;

        public StudentAwardYear2 inputStudentAwardYear;

        public bool isAppealCounselor = false;

        public FinancialAidOffice3 calculatedOfficeDto
        { get { return testModelData.financialAidOfficesData.FirstOrDefault(o => o.Id == inputStudentAwardYear.FinancialAidOfficeId); } }

        public Counselor counselor
        {
            get
            {
                var counselorObj = new Counselor();
                var phoneNumber = testModelData.counselorsPhoneNumbersData != null ? testModelData.counselorsPhoneNumbersData.First() : null;
                counselorObj.SetCounselorPropertiesFromDtos(testModelData.financialAidCounselorData, phoneNumber, inputStudentAwardYear, testModelData.financialAidOfficesData, isAppealCounselor);
                return counselorObj;
            }
        }

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0003914");

            //use an input year that has a configuration
            inputStudentAwardYear = testModelData.studentAwardYearsData.FirstOrDefault(y => testModelData.financialAidOfficesData.SelectMany(o => o.Configurations).GetConfigurationDtoOrDefault(y) != null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullOfficeListTest()
        {
            testModelData.financialAidOfficesData = null;
            var test = counselor;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullStudentAwardYearTest()
        {
            inputStudentAwardYear = null;
            var test = counselor;
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void NoOfficeWithSpecifiedIdTest()
        {
            inputStudentAwardYear.FinancialAidOfficeId = "FOOBAR";
            var test = counselor;
        }

        [TestMethod]
        public void NullCounselorDtoUsesOfficeDtoDataTest()
        {
            testModelData.financialAidCounselorData = null;
            Assert.AreEqual(calculatedOfficeDto.DirectorName, counselor.Name);
            Assert.AreEqual(calculatedOfficeDto.EmailAddress, counselor.EmailAddress);
            Assert.AreEqual(calculatedOfficeDto.PhoneNumber, counselor.PhoneNumber);
            Assert.IsFalse(counselor.IsCounselorInfo);
        }
        
        [TestMethod]
        public void CounselorDtoNameTest()
        {
            Assert.AreEqual(testModelData.financialAidCounselorData.Name, counselor.Name);
        }

        [TestMethod]
        public void CounselorDtoEmailAddressTest()
        {
            Assert.AreEqual(testModelData.financialAidCounselorData.EmailAddress, counselor.EmailAddress);
        }

        [TestMethod]
        public void NullCounselorDtoEmail_OfficeEmailAssignedTest()
        {
            testModelData.financialAidCounselorData.EmailAddress = null;
            Assert.AreEqual(calculatedOfficeDto.EmailAddress, counselor.EmailAddress);
        }

        [TestMethod]
        public void EmptyCounselorDtoEmail_OfficeEmailAssignedTest()
        {
            testModelData.financialAidCounselorData.EmailAddress = string.Empty;
            Assert.AreEqual(calculatedOfficeDto.EmailAddress, counselor.EmailAddress);
        }

        [TestMethod]
        public void CounselorDtoNotNull_IsCounselorInfoTrueTest()
        {
            Assert.IsTrue(counselor.IsCounselorInfo);
        }
        
        [TestMethod]
        public void NullConfiguration_UsesOfficePhoneEmptyExtensionTest()
        {
            inputStudentAwardYear = testModelData.studentAwardYearsData.First();
            testModelData.financialAidOfficesData.ForEach(o => o.Configurations = null);

            Assert.AreEqual(calculatedOfficeDto.PhoneNumber, counselor.PhoneNumber);
            Assert.AreEqual(string.Empty, counselor.Extension);
        }

        [TestMethod]
        public void NullPhoneNumberDto_UsesOfficePhoneEmptyExtensionTest()
        {
            testModelData.counselorsPhoneNumbersData = null;
            Assert.AreEqual(calculatedOfficeDto.PhoneNumber, counselor.PhoneNumber);
            Assert.AreEqual(string.Empty, counselor.Extension);
        }

        [TestMethod]
        public void NullPhoneNumberNumbers_UsesOfficePhoneEmptyExtensionTest()
        {
            testModelData.counselorsPhoneNumbersData.First(p => p.PersonId == testModelData.financialAidCounselorData.Id).PhoneNumbers = null;
            Assert.AreEqual(calculatedOfficeDto.PhoneNumber, counselor.PhoneNumber);
            Assert.AreEqual(string.Empty, counselor.Extension);
        }

        [TestMethod]
        public void NoNumberWithConfigurationType_UsesOfficePhoneEmptyExtensionTest()
        {
            testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.CounselorPhoneType = "FOOBAR"));
            Assert.AreEqual(calculatedOfficeDto.PhoneNumber, counselor.PhoneNumber);
            Assert.AreEqual(string.Empty, counselor.Extension);
        }

        [TestMethod]
        public void UseNumberAndExtensionWithConfigurationTypeTest()
        {
            testModelData.counselorsPhoneNumbersData.First(p => p.PersonId == testModelData.financialAidCounselorData.Id).PhoneNumbers.Add(
                new Colleague.Dtos.Base.Phone()
                {
                    Number = "foo-foo-foo",
                    Extension = "bar",
                    TypeCode = "FOOBAR"
                });
            testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.CounselorPhoneType = "FOOBAR"));

            Assert.AreEqual("foo-foo-foo", counselor.PhoneNumber);
            Assert.AreEqual("bar", counselor.Extension);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void NoCounselorNameTest()
        {
            testModelData.financialAidCounselorData.Name = string.Empty;
            var test = counselor;
        }

        [TestMethod]
        public void NoCounselorPhoneTest()
        {
            testModelData.financialAidCounselorData = null;
            testModelData.financialAidOfficesData.ForEach(o => o.PhoneNumber = string.Empty);
            Assert.AreEqual(string.Empty, counselor.PhoneNumber);
        }

        [TestMethod]
        public void NoCounselorEmailTest()
        {
            testModelData.financialAidCounselorData = null;
            testModelData.financialAidOfficesData.ForEach(o => o.EmailAddress = string.Empty);
            Assert.AreEqual(string.Empty, counselor.EmailAddress);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void NoCounselorPhoneAndEmailTest()
        {
            testModelData.financialAidCounselorData = null;
            testModelData.financialAidOfficesData.ForEach(o => { o.EmailAddress = string.Empty; o.PhoneNumber = string.Empty; });
            var test = counselor;
        }

        [TestMethod]
        [ExpectedException (typeof(ArgumentException))]
        public void NullCounselorDtoAndIsAppealCounselor_ExceptionThrownTest()
        {
            testModelData.financialAidCounselorData = null;
            isAppealCounselor = true;
            var nullCounselor = counselor;
        }
    }

    [TestClass]
    public class CounselorEqualsTests
    {
        public string name;
        public string phoneNumber;
        public string email;

        public Counselor counselor;

        [TestInitialize]
        public void Initialize()
        {
            name = "Matt";
            phoneNumber = "555-555-5555";
            email = "matt@ellucian.com";

            counselor = new Counselor()
            {
                Name = name,
                PhoneNumber = phoneNumber,
                EmailAddress = email
            };
        }

        [TestMethod]
        public void NullNotEqualTest()
        {
            Assert.IsFalse(counselor.Equals(null));
        }

        [TestMethod]
        public void NotCounselorTypeNotEqualTest()
        {
            Assert.IsFalse(counselor.Equals(new Colleague.Dtos.FinancialAid.FinancialAidCounselor()));
        }

        [TestMethod]
        public void EqualsTest()
        {
            var test = new Counselor()
            {
                Name = name,
                PhoneNumber = phoneNumber,
                EmailAddress = email
            };
            Assert.IsTrue(counselor.Equals(test));
        }

        [TestMethod]
        public void Equal_SameHashCodeTest()
        {
            var test = new Counselor()
            {
                Name = name,
                PhoneNumber = phoneNumber,
                EmailAddress = email
            };
            Assert.AreEqual(test.GetHashCode(), counselor.GetHashCode());
        }

        [TestMethod]
        public void DiffNameNotEqualTest()
        {
            var test = new Counselor()
            {
                Name = "foo",
                PhoneNumber = phoneNumber,
                EmailAddress = email
            };
            Assert.IsFalse(counselor.Equals(test));
        }

        [TestMethod]
        public void DiffNameNotSameHashCodeTest()
        {
            var test = new Counselor()
            {
                Name = "foo",
                PhoneNumber = phoneNumber,
                EmailAddress = email
            };
            Assert.AreNotEqual(test.GetHashCode(), counselor.GetHashCode());
        }

        [TestMethod]
        public void DiffPhoneNotEqualTest()
        {
            var test = new Counselor()
            {
                Name = name,
                PhoneNumber = "foo",
                EmailAddress = email
            };
            Assert.IsFalse(counselor.Equals(test));
        }

        [TestMethod]
        public void DiffPhoneNotSameHashCodeTest()
        {
            var test = new Counselor()
            {
                Name = name,
                PhoneNumber = "foo",
                EmailAddress = email
            };
            Assert.AreNotEqual(test.GetHashCode(), counselor.GetHashCode());
        }

        [TestMethod]
        public void DiffEmailNotEqualTest()
        {
            var test = new Counselor()
            {
                Name = name,
                PhoneNumber = phoneNumber,
                EmailAddress = "foo"
            };
            Assert.IsFalse(counselor.Equals(test));
        }

        [TestMethod]
        public void DiffEmailNotSameHashCodeTest()
        {
            var test = new Counselor()
            {
                Name = name,
                PhoneNumber = phoneNumber,
                EmailAddress = "foo"
            };
            Assert.AreNotEqual(test.GetHashCode(), counselor.GetHashCode());
        }
    }

    [TestClass]
    public class CounselorInfoAssignmentWithUseDefaultContactFlagTests
    {
        private TestModelData testModelData;
        private Counselor counselor;

        private FinancialAidCounselor counselorDto;
        private PhoneNumber phoneNumberDto;
        private StudentAwardYear2 studentAwardYearDto;
        private List<FinancialAidOffice3> officeDtos;

        [TestInitialize]
        public void TestInitialize()
        {
            testModelData = new TestModelData("0004791");
            counselorDto = testModelData.financialAidCounselorData;
            phoneNumberDto = testModelData.counselorsPhoneNumbersData.First(d => d.PersonId == counselorDto.Id);
            studentAwardYearDto = testModelData.studentAwardYearsData.First();
            officeDtos = testModelData.financialAidOfficesData;
        }

        [TestCleanup]
        public void Cleanup()
        {
            counselor = null;
            counselorDto = null;
            phoneNumberDto = null;
            studentAwardYearDto = null;
            officeDtos = null;
        }

        [TestMethod]
        public void UseDefaultContactFlagSetTrue_FaCounselorInfoIsDefaultTest()
        {
            var faOffice = officeDtos.First(o => o.Id == studentAwardYearDto.FinancialAidOfficeId);
            setUseDefaultContactFlag(true, faOffice);

            counselor = new Counselor(counselorDto, phoneNumberDto, studentAwardYearDto, officeDtos);
            Assert.AreEqual(faOffice.DirectorName, counselor.Name);
            Assert.AreEqual(faOffice.EmailAddress, counselor.EmailAddress);
            Assert.AreEqual(faOffice.PhoneNumber, counselor.PhoneNumber);
        }        

        [TestMethod]
        public void UseDefaultContactFlagSetFalse_FaCounselorInfoIsNotDefaultTest()
        {
            var faOffice = officeDtos.First(o => o.Id == studentAwardYearDto.FinancialAidOfficeId);
            var configuration = setUseDefaultContactFlag(false, faOffice);

            counselor = new Counselor(counselorDto, phoneNumberDto, studentAwardYearDto, officeDtos);
            Assert.AreEqual(counselorDto.Name, counselor.Name);
            Assert.AreEqual(counselorDto.EmailAddress, counselor.EmailAddress);
            Assert.AreEqual(phoneNumberDto.PhoneNumbers.First(n => n.TypeCode == configuration.CounselorPhoneType).Number, counselor.PhoneNumber);
        }

        [TestMethod]
        public void UseDefaultContactFlagSetTrue_SAPAppealsCounselorIsDefaultTest()
        {
            var faOffice = officeDtos.First(o => o.Id == studentAwardYearDto.FinancialAidOfficeId);
            setUseDefaultContactFlag(true, faOffice);

            counselor = new Counselor(counselorDto, phoneNumberDto, studentAwardYearDto, officeDtos, true);
            Assert.AreEqual(faOffice.DirectorName, counselor.Name);
            Assert.AreEqual(faOffice.EmailAddress, counselor.EmailAddress);
            Assert.AreEqual(faOffice.PhoneNumber, counselor.PhoneNumber);
        }

        [TestMethod]
        public void UseDefaultContactFlagSetFalse_SAPAppealsCounselorIsNotDefaultTest()
        {
            var faOffice = officeDtos.First(o => o.Id == studentAwardYearDto.FinancialAidOfficeId);
            var configuration =  setUseDefaultContactFlag(false, faOffice);

            counselor = new Counselor(counselorDto, phoneNumberDto, studentAwardYearDto, officeDtos, true);
            Assert.AreEqual(counselorDto.Name, counselor.Name);
            Assert.AreEqual(counselorDto.EmailAddress, counselor.EmailAddress);
            Assert.AreEqual(phoneNumberDto.PhoneNumbers.First(n => n.TypeCode == configuration.CounselorPhoneType).Number, counselor.PhoneNumber);
        }

        [TestMethod]
        [ExpectedException (typeof(ArgumentException))]
        public void UseDefaultContactFlagSetTrueAndSAPAppealsCounselorDtoNull_ThrowsExceptionTest()
        {
            var faOffice = officeDtos.First(o => o.Id == studentAwardYearDto.FinancialAidOfficeId);
            setUseDefaultContactFlag(true, faOffice);

            counselor = new Counselor(null, phoneNumberDto, studentAwardYearDto, officeDtos, true);
        }

        private FinancialAidConfiguration3 setUseDefaultContactFlag(bool flagValue, FinancialAidOffice3 faOffice)
        {
            var configuration = faOffice.Configurations.First(c => c.AwardYear == studentAwardYearDto.Code);
            configuration.UseDefaultContact = flagValue;
            return configuration;
        }
    }
}
