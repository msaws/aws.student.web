﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.LoanRequest
{
    [TestClass]
    public class LoanRequestViewModelTests
    {
        public string studentId;
        public TestModelData testModelData;

        public Colleague.Dtos.Base.Person personData;
        public List<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearsData;
        public List<Colleague.Dtos.FinancialAid.StudentDocument> studentDocumentsData;
        public List<Colleague.Dtos.Base.CommunicationCode2> communicationCodesData;
        public List<Colleague.Dtos.Base.OfficeCode> officeCodesData;
        public List<Colleague.Dtos.FinancialAid.FinancialAidApplication2> studentFaApplicationsData;
        public Colleague.Dtos.FinancialAid.FinancialAidCounselor financialAidCounselorData;
        public List<Colleague.Dtos.FinancialAid.FinancialAidOffice3> financialAidOfficeData;
        public List<StudentLoanRequest> pendingStudentLoanRequestModels;
        public Colleague.Dtos.Base.PhoneNumber counselorPhoneNumbers;

        private ICurrentUser currentUser;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);
            personData = testModelData.studentData;
            studentAwardYearsData = testModelData.studentAwardYearsData;
            studentDocumentsData = testModelData.studentDocumentsData;
            communicationCodesData = testModelData.communicationCodesData;
            officeCodesData = testModelData.officeCodesData;
            studentFaApplicationsData = testModelData.studentFaApplicationsData;
            financialAidCounselorData = testModelData.financialAidCounselorData;
            financialAidOfficeData = testModelData.financialAidOfficesData;
            pendingStudentLoanRequestModels = testModelData.pendingStudentLoanRequestsData.Select(loanRequestDto => new StudentLoanRequest(loanRequestDto)).ToList();
            counselorPhoneNumbers = testModelData.counselorsPhoneNumbersData.First(cpn => cpn.PersonId == financialAidCounselorData.Id);
            currentUser = testModelData.currentUser;
        }

        [TestMethod]
        public void FAStudentFromStudentTest()
        {
            var actualFAStudent = BuildLoanRequestViewModel().Person;
            Assert.AreEqual(testModelData.studentData.Id, actualFAStudent.Id);
            Assert.AreEqual(testModelData.studentData.PreferredName, actualFAStudent.PreferredName);
        }

        [TestMethod]
        public void FAStudentFromApplicantTest()
        {
            personData = testModelData.applicantData;
            var actualFAStudent = BuildLoanRequestViewModel().Person;
            Assert.AreEqual(testModelData.studentData.Id, actualFAStudent.Id);
            Assert.AreEqual(testModelData.studentData.PreferredName, actualFAStudent.PreferredName);
        }

        [TestMethod]
        public void LoanRequestListsAreEqualTest()
        {
            var actualLoanRequests = BuildLoanRequestViewModel().LoanRequests;
            Assert.IsNotNull(actualLoanRequests);
            CollectionAssert.AreEqual(pendingStudentLoanRequestModels.ToList(), actualLoanRequests);
        }

        [TestMethod]
        public void CounselorCreatedForEachYear_LoanRequestViewModel()
        {
            BuildLoanRequestViewModel().AwardYears.ToList().ForEach(y => Assert.IsNotNull(y.Counselor));
        }

        [TestMethod]
        public void CounselorNotCreated_NoExceptions_LoanRequestViewModel()
        {
            financialAidCounselorData.Name = string.Empty;
            BuildLoanRequestViewModel().AwardYears.ToList().ForEach(y => Assert.IsNull(y.Counselor));
        }

        [TestMethod]
        public void AwardYearsInSortedOrderTest()
        {
            var actualAwardYearCodes = BuildLoanRequestViewModel().AwardYears.Select(y => y.Code);
            var expectedSortedAwardYearCodes = studentAwardYearsData.Select(y => y.Code).OrderByDescending(code => code);

            CollectionAssert.AreEqual(expectedSortedAwardYearCodes.ToList(), actualAwardYearCodes.ToList());
        }

        [TestMethod]
        public void NoFinancialAidDataPassed_NoExceptionThrownTest()
        {
            bool exceptionThrown = false;
            financialAidOfficeData = null;
            try
            {
                BuildLoanRequestViewModel();
            }
            catch { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void IsProxyView_FalseWhenCurrentUserIsNullTest()
        {
            currentUser = null;
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.IsFalse(loanRequestViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyView_FalseWhenCurrentUserIsNotProxyTest()
        {
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.IsFalse(loanRequestViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyView_TrueWhenCurrentUserIsProxyTest()
        {
            currentUser = testModelData.currentUserWithProxy;
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.IsTrue(loanRequestViewModel.IsProxyView);
        }

        [TestMethod]
        public void NoPersonData_PersonIdIsnullTest()
        {
            testModelData.studentData = null;
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.IsNull(loanRequestViewModel.PersonId);
        }

        [TestMethod]
        public void PersonId_EqualsExpectedTest()
        {
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.AreEqual(testModelData.studentData.Id, loanRequestViewModel.PersonId);
        }

        [TestMethod]
        public void NoPersonData_PersonNameIsnullTest()
        {
            testModelData.studentData = null;
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.IsNull(loanRequestViewModel.PersonName);
        }

        [TestMethod]
        public void PersonName_EqualsExpectedTest()
        {
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.AreEqual(testModelData.studentData.PreferredName, loanRequestViewModel.PersonName);
        }

        [TestMethod]
        public void NoPersonData_PrivacyStatusCodeIsnullTest()
        {
            testModelData.studentData = null;
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.IsNull(loanRequestViewModel.PrivacyStatusCode);
        }

        [TestMethod]
        public void PrivacyStatusCode_EqualsExpectedTest()
        {
            var loanRequestViewModel = BuildLoanRequestViewModel();
            Assert.AreEqual(testModelData.studentData.PrivacyStatusCode, loanRequestViewModel.PrivacyStatusCode);
        }
        

        private LoanRequestViewModel BuildLoanRequestViewModel()
        {
            return new LoanRequestViewModel(
                testModelData.studentData,
                studentAwardYearsData,
                studentDocumentsData,
                communicationCodesData,
                officeCodesData,
                studentFaApplicationsData,
                financialAidCounselorData,
                financialAidOfficeData,
                pendingStudentLoanRequestModels,
                counselorPhoneNumbers,
                currentUser);
        }
    }
}
