﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.LoanRequest
{
    [TestClass]
    public class StudentLoanRequestModelTests
    {
        public string studentId;
        public TestModelData testModelData;

        public Colleague.Dtos.FinancialAid.LoanRequest loanRequestData;
        public Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearData;
        public List<Colleague.Dtos.FinancialAid.StudentDefaultAwardPeriod> studentDefaultAwardPeriodData;
        public List<Colleague.Dtos.FinancialAid.AwardPeriod> awardPeriodData;

        public StudentLoanRequest actualStudentLoanRequest;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelData = new TestModelData(studentId);

            loanRequestData = testModelData.pendingStudentLoanRequestsData.First();
            studentAwardYearData = testModelData.studentAwardYearsData.First(y => y.Code == loanRequestData.AwardYear);
            studentDefaultAwardPeriodData = testModelData.studentDefaultAwardPeriodData;
            awardPeriodData = testModelData.awardPeriodsData;
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest();
            Assert.IsNotNull(actualStudentLoanRequest.LoanRequestAwardPeriods);
        }

        [TestMethod]
        public void UnmetCostTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest();
            Assert.AreEqual(0, actualStudentLoanRequest.UnmetCost);

            var coa = 1000;
            var total = 1200;
            actualStudentLoanRequest.EstimatedCostOfAttendance = coa;
            actualStudentLoanRequest.TotalAwardedAmount = total;

            Assert.AreEqual(coa - total, actualStudentLoanRequest.UnmetCost);
        }

        [TestMethod]
        public void MaxRequestAmountIsUnmetCostTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest();

            actualStudentLoanRequest.EstimatedCostOfAttendance = 5000;
            actualStudentLoanRequest.TotalAwardedAmount = 3000;

            Assert.AreEqual(actualStudentLoanRequest.UnmetCost, actualStudentLoanRequest.MaximumRequestAmount);
        }

        [TestMethod]
        public void MaxRequestAmountIsConstantIfUnmetCostNotGreaterThanZeroTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest();

            actualStudentLoanRequest.EstimatedCostOfAttendance = 0;
            actualStudentLoanRequest.TotalAwardedAmount = 0;

            Assert.AreEqual(99999, actualStudentLoanRequest.MaximumRequestAmount);
        }

        [TestMethod]
        public void PendingLoanRequestDtoConstructorTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(loanRequestData);
            Assert.AreEqual(loanRequestData.AwardYear, actualStudentLoanRequest.AwardYearCode);
            Assert.AreEqual(loanRequestData.RequestDate.ToShortDateString(), actualStudentLoanRequest.RequestDate);
            Assert.AreEqual(loanRequestData.TotalRequestAmount, actualStudentLoanRequest.TotalRequestAmount);
            Assert.AreEqual(true, actualStudentLoanRequest.IsRequestPending);
            Assert.AreEqual(loanRequestData.StudentId, actualStudentLoanRequest.StudentId);

            var expectedLoanRequestPeriods = loanRequestData.LoanRequestPeriods.Select(pDto => new StudentLoanPeriod() { Code = pDto.Code, AwardAmount = pDto.LoanAmount });
            CollectionAssert.AreEqual(expectedLoanRequestPeriods.ToList(), actualStudentLoanRequest.LoanRequestAwardPeriods.ToList());
        }

        [TestMethod]
        public void NewLoanRequestConstructor_Test()
        {
            actualStudentLoanRequest = new StudentLoanRequest(studentAwardYearData, studentId, studentDefaultAwardPeriodData, awardPeriodData);

            Assert.AreEqual(studentAwardYearData.Code, actualStudentLoanRequest.AwardYearCode);
            Assert.AreEqual(false, actualStudentLoanRequest.IsRequestPending);
            Assert.AreEqual(studentId, actualStudentLoanRequest.StudentId);

            Assert.AreEqual(studentAwardYearData.EstimatedCostOfAttendance.Value, actualStudentLoanRequest.EstimatedCostOfAttendance);
            Assert.AreEqual((int)Math.Round(studentAwardYearData.TotalAwardedAmount), actualStudentLoanRequest.TotalAwardedAmount);
        }

        [TestMethod]
        public void NewLoanRequestConstructor_CoaHasNoValueTest()
        {
            studentAwardYearData.EstimatedCostOfAttendance = null;
            actualStudentLoanRequest = new StudentLoanRequest(studentAwardYearData, studentId, studentDefaultAwardPeriodData, awardPeriodData);

            Assert.AreEqual(0, actualStudentLoanRequest.EstimatedCostOfAttendance);
        }

        [TestMethod]
        public void NewLoanRequestConstructor_DistinctLoanRequestAwardPeriodsTest()
        {
            //find an award period to add a duplicate of that to the list
            var studentAwardPeriodId = studentDefaultAwardPeriodData.First(d => d.AwardYear == studentAwardYearData.Code).DefaultAwardPeriods.First();
            studentDefaultAwardPeriodData.First(d => d.AwardYear == studentAwardYearData.Code).DefaultAwardPeriods.Add(studentAwardPeriodId);            

            actualStudentLoanRequest = new StudentLoanRequest(studentAwardYearData, studentId, studentDefaultAwardPeriodData, awardPeriodData);

            CollectionAssert.AllItemsAreNotNull(actualStudentLoanRequest.LoanRequestAwardPeriods);
            CollectionAssert.AllItemsAreUnique(actualStudentLoanRequest.LoanRequestAwardPeriods);
        }

        [TestMethod]
        public void NewLoanRequestConstructor_OrderedLoanRequestAwardPeriodsTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(studentAwardYearData, studentId, studentDefaultAwardPeriodData, awardPeriodData);

            var orderedPeriods = actualStudentLoanRequest.LoanRequestAwardPeriods.OrderBy(p => p.StartDate);
            CollectionAssert.AreEqual(orderedPeriods.ToList(), actualStudentLoanRequest.LoanRequestAwardPeriods);
        }

        [TestMethod]
        public void CommentsPropertyIsNullTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest();
            Assert.IsNull(actualStudentLoanRequest.Comments);
        }

        [TestMethod]
        public void CommentPropertyGetSetTest()
        {
            string newComment = "This is a comment";
            actualStudentLoanRequest = new StudentLoanRequest();
            actualStudentLoanRequest.Comments = newComment;
            Assert.AreEqual(newComment, actualStudentLoanRequest.Comments);
        }

        [TestMethod]
        public void EqualsMethodRetunsFalse_WhenAwardYearCodesDoNotMatchTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(studentAwardYearData, studentId, studentDefaultAwardPeriodData, awardPeriodData);
            var expectedLoanRequest = new StudentLoanRequest(new Colleague.Dtos.FinancialAid.StudentAwardYear2() { Code="foo" }, studentId, studentDefaultAwardPeriodData, awardPeriodData);
            Assert.IsFalse(actualStudentLoanRequest.Equals(expectedLoanRequest));
        }

        [TestMethod]
        public void EqualsMethodRetunsFalse_WhenStudentIdsDoNotMatchTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(studentAwardYearData, studentId, studentDefaultAwardPeriodData, awardPeriodData);
            var expectedLoanRequest = new StudentLoanRequest(studentAwardYearData, "id", studentDefaultAwardPeriodData, awardPeriodData);
            Assert.IsFalse(actualStudentLoanRequest.Equals(expectedLoanRequest));
        }

        [TestMethod]
        public void EqualsMethodRetunsFalse_WhenRequestDatesDoNotMatchTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(loanRequestData);

            loanRequestData.RequestDate = DateTime.Today;
            var expectedLoanRequest = new StudentLoanRequest(loanRequestData);
            Assert.IsFalse(actualStudentLoanRequest.Equals(expectedLoanRequest));
        }

        [TestMethod]
        public void EqualsMethodRetunsFalse_WhenCompareToObjectIsNullTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(loanRequestData);            
            Assert.IsFalse(actualStudentLoanRequest.Equals(null));
        }

        [TestMethod]
        public void EqualsMethodRetunsFalse_WhenCompareToObjectIsOfDifferentTypeTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(loanRequestData);
            Assert.IsFalse(actualStudentLoanRequest.Equals(new StudentAwardYear2()));
        }

        [TestMethod]
        public void EqualsMethodRetunsTrue_WhenAllDataMatchesTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(loanRequestData);            
            var expectedLoanRequest = new StudentLoanRequest(loanRequestData);

            Assert.IsTrue(actualStudentLoanRequest.Equals(expectedLoanRequest));
        }

        [TestMethod]
        public void GetHashMethodReturnsExpectedValueTest()
        {
            actualStudentLoanRequest = new StudentLoanRequest(loanRequestData);
            int expectedHashCode = actualStudentLoanRequest.AwardYearCode.GetHashCode() ^ actualStudentLoanRequest.StudentId.GetHashCode() ^ actualStudentLoanRequest.RequestDate.GetHashCode();
            Assert.AreEqual(expectedHashCode, actualStudentLoanRequest.GetHashCode());
        }
    }
}
