﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.CorrespondenceOption
{
    /// <summary>
    /// Test class for CorrespondenceOptionViewModel class
    /// </summary>
    [TestClass]
    public class CorrespondenceOptionViewModelTests
    {
        private TestModelData testModelDataObject;

        private CorrespondenceOptionViewModel correspondenceOptionViewModel;

        private string studentId;
        private ICurrentUser currentUser;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelDataObject = new TestModelData(studentId);
            currentUser = null;
            BuildCorrespondenceOptionViewModel();
        }

        /// <summary>
        /// Tests if the correspondenceOptionViewModel
        /// was created
        /// </summary>
        [TestMethod]
        public void ModelCreated_InitializedModelTest()
        {
            Assert.IsNotNull(correspondenceOptionViewModel);
            Assert.IsNotNull(correspondenceOptionViewModel.CorrespondenceOptionData);
            Assert.IsNotNull(correspondenceOptionViewModel.Person);
            
        }
        
        /// <summary>
        /// Tests if the view model was created even though the person object did not contain data
        /// </summary>
        [TestMethod]
        public void NoPersonData_ModelInitializedTest()
        {
            testModelDataObject.studentData = new Colleague.Dtos.Student.Student();
            BuildCorrespondenceOptionViewModel();

            Assert.IsNotNull(correspondenceOptionViewModel);
            Assert.IsNotNull(correspondenceOptionViewModel.Person);
            Assert.IsNotNull(correspondenceOptionViewModel.CorrespondenceOptionData);
            
        }        

        /// <summary>
        /// Tests if no exception is thrown when there is no 
        /// studentAwardYearsData
        /// </summary>
        [TestMethod]
        public void NoStudentAwardYearsData_NoExceptionThrownTest()
        {
            testModelDataObject.studentAwardYearsData = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
            var exceptionThrown = false;
            try
            {
                BuildCorrespondenceOptionViewModel();
            }
            catch (Exception) { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        }

        /// <summary>
        /// Tests if no exception is thrown when there is no
        /// financialAidOfficesData
        /// </summary>
        [TestMethod]
        public void NoFinancialAidOfficesData_NoExceptionThrownTest()
        {
            testModelDataObject.financialAidOfficesData = null;
            var exceptionThrown = false;
            try
            {
                BuildCorrespondenceOptionViewModel();
            }
            catch (Exception) { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        }

        /// <summary>
        /// Tests if no exception is rethrown if there is no FAOffice for the year
        /// </summary>
        [TestMethod]
        public void NoFinancialAidOfficeForYear_ExceptionCaughtAndRethrownTest()
        {
            var sortedAwardYears = testModelDataObject.studentAwardYearsData.OrderByDescending(ay => ay.Code);
            var latestYear = sortedAwardYears.First();
            var index = testModelDataObject.financialAidOfficesData.FindIndex(fao => fao.Id == latestYear.FinancialAidOfficeId);
            testModelDataObject.financialAidOfficesData.RemoveAt(index);
            var exceptionThrown = false;
            try
            {
                BuildCorrespondenceOptionViewModel();
            }
            catch (Exception) { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        }

        /// <summary>
        /// Tests if no exception is rethrown if no configurations were found for the office
        /// </summary>
        [TestMethod]
        public void NoConfigurationsForYear_NoExceptionRethrownTest()
        {
            var sortedAwardYears = testModelDataObject.studentAwardYearsData.OrderByDescending(ay => ay.Code);
            var latestYear = sortedAwardYears.First();
            var faOfficeForYear = testModelDataObject.financialAidOfficesData.First(fao => fao.Id == latestYear.FinancialAidOfficeId);
            faOfficeForYear.Configurations.RemoveAll(c => c != null);
            var exceptionThrown = false;
            try
            {
                BuildCorrespondenceOptionViewModel();
            }
            catch (Exception) { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        }

        /// <summary>
        /// Tests if no exception is rethrown when there is no current configuration for the office
        /// </summary>
        [TestMethod]        
        public void NoCurrentConfiguration_ExceptionCaughtAndRethrownTest()
        {
            var sortedAwardYears = testModelDataObject.studentAwardYearsData.OrderByDescending(ay => ay.Code);
            var latestYear = sortedAwardYears.First();
            var faOfficeForYear = testModelDataObject.financialAidOfficesData.First(fao => fao.Id == latestYear.FinancialAidOfficeId);
            faOfficeForYear.Configurations.RemoveAll(c => c.AwardYear == latestYear.Code);
            var exceptionThrown = false;
            try
            {
                BuildCorrespondenceOptionViewModel();
            }
            catch (Exception) { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        }
        

        /// <summary>
        /// Tests if the resulting CorrespondenceOptionData does not use the most recent award year 
        /// that does not have a configuration but instead uses the next available year that has a configuration
        /// </summary>
        [TestMethod]
        public void CorrespondenceOptionData_UsesYearWithConfigurationTest()
        {
            var awardYearsSortedByDesc = testModelDataObject.studentAwardYearsData.OrderByDescending(ay => ay.Code).ToList();
            awardYearsSortedByDesc[0].FinancialAidOfficeId = null;
            BuildCorrespondenceOptionViewModel();

            Assert.AreNotEqual(correspondenceOptionViewModel.CorrespondenceOptionData.AwardYearCode, awardYearsSortedByDesc[0].Code);
            Assert.IsNotNull(correspondenceOptionViewModel.CorrespondenceOptionData.AwardYearCode);
            Assert.AreEqual(correspondenceOptionViewModel.CorrespondenceOptionData.AwardYearCode, awardYearsSortedByDesc[1].Code);
            Assert.AreEqual(correspondenceOptionViewModel.CorrespondenceOptionData.IsCorrespondenceOptionChecked, awardYearsSortedByDesc[1].IsPaperCopyOptionSelected);
        }

        [TestMethod]
        public void IsProxyViewIsFalse_WhenCurrentUserIsNullTest()
        {
            Assert.IsFalse(correspondenceOptionViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyViewIsFalse_WhenCurrentUserIsNotProxyTest()
        {
            currentUser = testModelDataObject.currentUser;
            BuildCorrespondenceOptionViewModel();
            Assert.IsFalse(correspondenceOptionViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyViewIsTrue_WhenCurrentUserIsProxyTest()
        {
            currentUser = testModelDataObject.currentUserWithProxy;
            BuildCorrespondenceOptionViewModel();
            Assert.IsTrue(correspondenceOptionViewModel.IsProxyView);
        }

        [TestMethod]
        public void NoPersonData_PersonIdIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildCorrespondenceOptionViewModel();
            Assert.IsNull(correspondenceOptionViewModel.PersonId);
        }

        [TestMethod]
        public void PersonId_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.Id, correspondenceOptionViewModel.PersonId);
        }

        [TestMethod]
        public void NoPersonData_PersonNameIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildCorrespondenceOptionViewModel();
            Assert.IsNull(correspondenceOptionViewModel.PersonName);
        }

        [TestMethod]
        public void PersonName_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PreferredName, correspondenceOptionViewModel.PersonName);
        }

        [TestMethod]
        public void NoPersonData_PrivacyStatusCodeIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildCorrespondenceOptionViewModel();
            Assert.IsNull(correspondenceOptionViewModel.PrivacyStatusCode);
        }

        [TestMethod]
        public void PrivacyStatusCode_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PrivacyStatusCode, correspondenceOptionViewModel.PrivacyStatusCode);
        }
        
        private void BuildCorrespondenceOptionViewModel()
        {
            correspondenceOptionViewModel = new CorrespondenceOptionViewModel(
                testModelDataObject.studentData, 
                testModelDataObject.studentAwardYearsData, 
                testModelDataObject.financialAidOfficesData, 
                currentUser);
        }
    }
}
