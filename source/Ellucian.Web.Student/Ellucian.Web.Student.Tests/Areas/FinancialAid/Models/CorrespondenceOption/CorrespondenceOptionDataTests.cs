﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.CorrespondenceOption
{
    /// <summary>
    /// Test class for the CorrespondenceOptionData class
    /// </summary>
    [TestClass]
    public class CorrespondenceOptionDataTests
    {
        private bool isCorrOptionChecked;
        private string optionMessage;
        private string awardYearCode;
        private string officeId;
        private CorrespondenceOptionData correspondenceOptionData;
        private Colleague.Dtos.FinancialAid.StudentAwardYear2 awardYear;
        private Colleague.Dtos.FinancialAid.FinancialAidOffice3 financialAidOffice;

        [TestInitialize]
        public void Initialize()
        {
            isCorrOptionChecked = false;
            optionMessage = "option message";
            awardYearCode = "2015";
            officeId = "office";
            awardYear = new Colleague.Dtos.FinancialAid.StudentAwardYear2() { IsPaperCopyOptionSelected = isCorrOptionChecked, Code = awardYearCode, FinancialAidOfficeId = officeId };
            financialAidOffice = new Colleague.Dtos.FinancialAid.FinancialAidOffice3()
            {
                Id = officeId,
                Configurations = new List<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3>()
                {
                    new Colleague.Dtos.FinancialAid.FinancialAidConfiguration3(){
                        AwardYear = awardYearCode,
                        PaperCopyOptionText = optionMessage
                    },
                    new Colleague.Dtos.FinancialAid.FinancialAidConfiguration3(){
                        AwardYear = "diffYear",
                        PaperCopyOptionText = optionMessage
                    }
                }
            };
            correspondenceOptionData = new CorrespondenceOptionData(awardYear, financialAidOffice);
        }

        /// <summary>
        /// Tests if the correspondenceOptionData object is not null
        /// </summary>
        [TestMethod]
        public void ObjectIsNotNullTest()
        {
            Assert.IsNotNull(correspondenceOptionData);
        }

        /// <summary>
        /// Tests if the attributes were set correctly
        /// </summary>
        [TestMethod]
        public void AttributesGetSetTest()
        {
            Assert.AreEqual(isCorrOptionChecked, correspondenceOptionData.IsCorrespondenceOptionChecked);
            Assert.AreEqual(optionMessage, correspondenceOptionData.OptionMessage);
            Assert.AreEqual(awardYearCode, correspondenceOptionData.AwardYearCode);
        }

        /// <summary>
        /// Tests if attributes are set to correct values in a default constructor
        /// </summary>
        [TestMethod]
        public void DefaultConstructor_DefaultAttributesTest()
        {
            correspondenceOptionData = new CorrespondenceOptionData();
            Assert.IsNotNull(correspondenceOptionData);

            Assert.AreEqual(false, correspondenceOptionData.IsCorrespondenceOptionChecked);
            Assert.AreEqual(string.Empty, correspondenceOptionData.OptionMessage);
            Assert.AreEqual(string.Empty, correspondenceOptionData.AwardYearCode);
        }
        
        /// <summary>
        /// Tests if ArgumentNullException is thrown when the studentAwardYearDto argument
        /// is null
        /// </summary>
        [TestMethod]
        [ExpectedException (typeof(ArgumentNullException))]
        public void NullStudentAwardYearArgument_ExceptionThrownTest()
        {
            correspondenceOptionData = new CorrespondenceOptionData(null, financialAidOffice);
        }

        /// <summary>
        /// Tests if ArgumentNullException is thrown when FaOfficeData argument is null
        /// </summary>
        [TestMethod]
        [ExpectedException (typeof(ArgumentNullException))]
        public void NullFAOfficeData_ExceptionThrownTest()
        {
            correspondenceOptionData = new CorrespondenceOptionData(awardYear, null);
        }

        /// <summary>
        /// Tests if ApplicationException is thrown when there are no configurations
        /// for the fa office
        /// </summary>
        [TestMethod]
        [ExpectedException (typeof(ApplicationException))]
        public void NoFinancialAidOfficeConfigurations_ExceptionThrownTest()
        {
            financialAidOffice.Configurations = null;
            correspondenceOptionData = new CorrespondenceOptionData(awardYear, financialAidOffice);
        }

        /// <summary>
        /// Tests if ApplicationException is thrown when there is no current configuration for the year
        /// </summary>
        [TestMethod]
        [ExpectedException (typeof(ApplicationException))]
        public void NoCurrentConfiguration_ExceptionThrownTest()
        {
            financialAidOffice.Configurations.RemoveAll(c => c.AwardYear == awardYear.Code);
            correspondenceOptionData = new CorrespondenceOptionData(awardYear, financialAidOffice);
        }
    }
}
