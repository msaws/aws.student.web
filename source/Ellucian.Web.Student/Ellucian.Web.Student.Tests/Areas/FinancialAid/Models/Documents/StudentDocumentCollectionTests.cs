﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using System;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Documents
{
    [TestClass]
    public class StudentDocumentCollectionTests
    {
        public TestModelData testModelData;
        public StudentDocumentCollection studentDocumentCollection
        { get { return new StudentDocumentCollection(inputStudentAwardYear, inputStudentDocuments, inputCommunicationCodes, inputOfficeCodes, inputConfiguration); } }

        public Colleague.Dtos.FinancialAid.StudentAwardYear2 inputStudentAwardYear
        { get { return testModelData.studentAwardYearsData.FirstOrDefault(); } }

        public Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 inputConfiguration
        { get { return testModelData.financialAidOfficesData.SelectMany(o => o.Configurations).GetConfigurationDtoOrDefault(inputStudentAwardYear); } }

        public IEnumerable<Colleague.Dtos.FinancialAid.StudentDocument> inputStudentDocuments
        { get { return testModelData.studentDocumentsData; } }

        public IEnumerable<Colleague.Dtos.Base.CommunicationCode2> inputCommunicationCodes
        { get { return testModelData.communicationCodesData; } }

        public IEnumerable<Colleague.Dtos.Base.OfficeCode> inputOfficeCodes
        { get { return testModelData.officeCodesData; } }


        [TestClass]
        public class OtherStudentDocumentCollectionTests : StudentDocumentCollectionTests
        {
            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");
            }

            [TestMethod]
            public void DefaultConstructorTests()
            {
                var collection = new StudentDocumentCollection();
                Assert.IsNotNull(collection.GetStudentDocuments());
            }

            [TestMethod]
            public void IncompleteDocumentsAreAllIncompleteTest()
            {
                Assert.IsTrue(studentDocumentCollection.IncompleteDocuments.All(d => d.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete));
            }

            [TestMethod]
            public void IncompleteDocumentsSortedByDocumentDisplayComparerTest()
            {
                var sortedIncompleteDocuments = studentDocumentCollection.IncompleteDocuments.ToList().OrderBy(d => d, new DocumentDisplayComparer());

                CollectionAssert.AreEqual(sortedIncompleteDocuments.ToList(), studentDocumentCollection.IncompleteDocuments);
            }

            [TestMethod]
            public void CompleteDocumentsAreAllCompleteTest()
            {
                Assert.IsTrue(studentDocumentCollection.CompleteDocuments.All(d => d.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Received || d.Status == Colleague.Dtos.FinancialAid.DocumentStatus.Waived));
            }

            [TestMethod]
            public void CompleteDocumentsSortedByDocumentDisplayComparerTest()
            {
                var sortedCompleteDocuments = studentDocumentCollection.CompleteDocuments.ToList().OrderBy(d => d, new DocumentDisplayComparer());

                CollectionAssert.AreEqual(sortedCompleteDocuments.ToList(), studentDocumentCollection.CompleteDocuments);
            }

            [TestMethod]
            public void DocumentsExistTest()
            {
                Assert.IsTrue(studentDocumentCollection.DocumentsExist);
                testModelData.studentDocumentsData = null;
                Assert.IsFalse(studentDocumentCollection.DocumentsExist);
            }

            [TestMethod]
            public void HasOutstandingDocumentsFalseTest()
            {
                testModelData.studentDocumentsData.ForEach(d => d.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Received);
                Assert.IsFalse(studentDocumentCollection.HasOutstandingDocuments);
            }

            [TestMethod]
            public void HasOutstandingDocumentsTrueTest()
            {
                var collection = studentDocumentCollection;
                collection.GetStudentDocuments().First().Status = Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete;

                Assert.IsTrue(collection.HasOutstandingDocuments);
            }

        }



        [TestClass]
        public class StudentDocumentCollectionConstructorTests : StudentDocumentCollectionTests
        {
            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void InputStudentAwardYearRequiredTest()
            {
                testModelData.studentAwardYearsData = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();

                var test = studentDocumentCollection;
            }

            [TestMethod]
            public void AwardYearCodeTest()
            {
                Assert.AreEqual(inputStudentAwardYear.Code, studentDocumentCollection.AwardYearCode);
            }

            [TestMethod]
            public void DocumentsEmptyIfNullStudentDocumentsTest()
            {
                testModelData.studentDocumentsData = null;
                Assert.IsNotNull(studentDocumentCollection.GetStudentDocuments());
                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void DocumentsEmptyIfEmptyStudentDocumentsTest()
            {
                testModelData.studentDocumentsData = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
                Assert.IsNotNull(studentDocumentCollection.GetStudentDocuments());
                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void DocumentsEmptyIfNullCommunicationCodesTest()
            {
                testModelData.communicationCodesData = null;
                Assert.IsNotNull(studentDocumentCollection.GetStudentDocuments());
                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void DocumentsEmptyIfNullOfficeCodesTest()
            {
                testModelData.officeCodesData = null;
                Assert.IsNotNull(studentDocumentCollection.GetStudentDocuments());
                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void NoFinancialAidOfficeCodes_NoDocumentsTest()
            {
                testModelData.officeCodesData = new List<Colleague.Dtos.Base.OfficeCode>() {
                new Colleague.Dtos.Base.OfficeCode() { Code = "NOTFA", Description = "Not FA", Type = Colleague.Dtos.Base.OfficeCodeType.Other}
            };

                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void OnlyAddDocumentsAssignedToFinancialAidOfficeTest()
            {
                var faOfficeCodes = testModelData.officeCodesData.Where(o => o.Type == Colleague.Dtos.Base.OfficeCodeType.FinancialAid).Select(o => o.Code);
                Assert.IsTrue(faOfficeCodes.Count() > 0);

                var faCommCodes = testModelData.communicationCodesData.Where(cc => faOfficeCodes.Contains(cc.OfficeCodeId)).Select(cc => cc.Code);

                Assert.IsTrue(studentDocumentCollection.GetStudentDocuments().Count() > 0);

                foreach (var studentDocument in studentDocumentCollection.GetStudentDocuments())
                {
                    Assert.IsTrue(faCommCodes.Contains(studentDocument.Code));
                }
            }

            [TestMethod]
            public void OnlyAddDocumentsIfIsStudentViewableTest()
            {
                testModelData.communicationCodesData.ForEach(cc => cc.IsStudentViewable = false);

                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void OnlyAddDocumentsIfAssignedToInputStudentAwardYearTest()
            {
                testModelData.communicationCodesData.ForEach(cc => cc.AwardYear = "foobar");

                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void AddDocumentWithEmptyYearTest()
            {
                testModelData.communicationCodesData.Add(new Colleague.Dtos.Base.CommunicationCode2()
                    {
                        Code = "FOOBAR",
                        AwardYear = null,
                        OfficeCodeId = "FA",
                        IsStudentViewable = true
                    });
                testModelData.studentDocumentsData.Add(new Colleague.Dtos.FinancialAid.StudentDocument()
                    {
                        Code = "FOOBAR",
                        StudentId = "0003914",
                        Status = Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete
                    });

                var docNoYear = studentDocumentCollection.GetStudentDocuments().FirstOrDefault(d => d.Code == "FOOBAR");
                Assert.IsNotNull(docNoYear);
            }

            [TestMethod]
            public void NoStudentDocumentsInViewableFinancialAidCommCodes()
            {
                testModelData.studentDocumentsData = new List<Colleague.Dtos.FinancialAid.StudentDocument>()
            {
                new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "FOOBAR",
                    StudentId = testModelData.studentId,
                    Status = Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete
                }
            };

                Assert.AreEqual(0, studentDocumentCollection.GetStudentDocuments().Count());
            }

            [TestMethod]
            public void NumberOfDocumentsTest()
            {
                //extract the financial aid office codes
                var financialAidOfficeCodes = testModelData.officeCodesData.Where(o => o.Type == Colleague.Dtos.Base.OfficeCodeType.FinancialAid);

                //extract the communication codes assigned to the FA office 
                //and that are assigned to the specific award year (or no award year), and that
                //are viewable by a student (on CCWP form)
                var financialAidCommunicationCodeDtoList = testModelData.communicationCodesData.Where(c =>
                    (c.AwardYear == inputStudentAwardYear.Code || string.IsNullOrEmpty(c.AwardYear)) &&
                    financialAidOfficeCodes.Select(o => o.Code).Contains(c.OfficeCodeId) && c.IsStudentViewable);

                //extract the student documents contained in the finAidCommCodes 
                var documentDtoList = testModelData.studentDocumentsData.Where(d => financialAidCommunicationCodeDtoList.Select(c => c.Code).Contains(d.Code));

                Assert.AreEqual(documentDtoList.Count(), studentDocumentCollection.GetStudentDocuments().Count());
            }
        }
    }
}
