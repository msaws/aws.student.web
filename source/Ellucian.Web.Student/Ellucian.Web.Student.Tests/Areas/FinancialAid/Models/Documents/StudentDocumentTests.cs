﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Documents
{
    [TestClass]
    public class StudentDocumentTests
    {
        public TestModelData testModelData;
        public StudentDocument studentDocument;

        public Colleague.Dtos.FinancialAid.StudentDocument inputStudentDocument
        {
            get { return testModelData.studentDocumentsData.First(); }
        }
        public IEnumerable<Colleague.Dtos.Base.CommunicationCode2> inputCommunicationCodes
        {
            get { return testModelData.communicationCodesData; }
        }

        public bool suppressDocumentInstance;
        private bool useStatusDescription;

        public FunctionEqualityComparer<Colleague.Dtos.Base.CommunicationCodeHyperlink> communicationCodeHyperlinkDtoComparer = 
            new FunctionEqualityComparer<Colleague.Dtos.Base.CommunicationCodeHyperlink>
                ((h1, h2) => h1.Url == h2.Url && h1.Title == h2.Title, h => h.GetHashCode());

        [TestClass]
        public class StudentDocumentConstructorTests : StudentDocumentTests
        {            
            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");
                suppressDocumentInstance = false;
                useStatusDescription = false;
                
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void InputStudentDocumentRequiredTest()
            {                
                studentDocument = new StudentDocument(null, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void InputCommCodeListRequiredTest()
            {
                studentDocument = new StudentDocument(inputStudentDocument, null, suppressDocumentInstance, useStatusDescription);
            }

            [TestMethod]
            public void DocumentComesFromCommunicationCodesListTest()
            {
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsTrue(inputCommunicationCodes.Select(c => c.Code).Contains(studentDocument.Code));
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void DocumentDoesNotExistInCommunicationCodesListTest()
            {          
                studentDocument = new StudentDocument(inputStudentDocument, new List<Colleague.Dtos.Base.CommunicationCode2>(), suppressDocumentInstance, useStatusDescription);                
            }

            [TestMethod]
            public void CodeTest()
            {
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(inputStudentDocument.Code, studentDocument.Code);
            }

            [TestMethod]
            public void DescriptionTest()
            {
                var commCodeDto = inputCommunicationCodes.First(c => c.Code == inputStudentDocument.Code);
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(commCodeDto.Description, studentDocument.Description);
            }

            [TestMethod]
            public void ExplanationTest()
            {
                var commCodeDto = inputCommunicationCodes.First(c => c.Code == inputStudentDocument.Code);
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(commCodeDto.Explanation, studentDocument.Explanation);
            }

            [TestMethod]
            public void ExplanationWithCarriageReturn_EqualsExpectedValueTest()
            {
                var inputCodes = new List<CommunicationCode2>()
                {
                    new CommunicationCode2()
                    {
                        Code = "newCMC",
                        Explanation="This is an explanation\r with a carriage return"
                    }
                };
                var inputDocument = new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "newCMC"
                };
                studentDocument = new StudentDocument(inputDocument, inputCodes, suppressDocumentInstance, useStatusDescription);

                string expectedExplanation = "This is an explanation<br /> with a carriage return";
                Assert.AreEqual(expectedExplanation, studentDocument.Explanation);
            }

            [TestMethod]
            public void ExplanationWithNewLine_EqualsExpectedValueTest()
            {
                var inputCodes = new List<CommunicationCode2>()
                {
                    new CommunicationCode2()
                    {
                        Code = "newCMC",
                        Explanation="This is an explanation\n with a new line"
                    }
                };
                var inputDocument = new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "newCMC"
                };
                studentDocument = new StudentDocument(inputDocument, inputCodes, suppressDocumentInstance, useStatusDescription);

                string expectedExplanation = "This is an explanation<br /> with a new line";
                Assert.AreEqual(expectedExplanation, studentDocument.Explanation);
            }

            [TestMethod]
            public void ExplanationWithEnvironmentNewLine_EqualsExpectedValueTest()
            {
                var inputCodes = new List<CommunicationCode2>()
                {
                    new CommunicationCode2()
                    {
                        Code = "newCMC",
                        Explanation="This is an explanation\r\n with an environment new line"
                    }
                };
                var inputDocument = new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "newCMC"
                };
                studentDocument = new StudentDocument(inputDocument, inputCodes, suppressDocumentInstance, useStatusDescription);

                string expectedExplanation = "This is an explanation<br /> with an environment new line";
                Assert.AreEqual(expectedExplanation, studentDocument.Explanation);
            }

            [TestMethod]
            public void ExplanationWithMultipleEnvironmentNewLines_EqualsExpectedValueTest()
            {
                var inputCodes = new List<CommunicationCode2>()
                {
                    new CommunicationCode2()
                    {
                        Code = "newCMC",
                        Explanation="This is an explanation\r\n\r\n with an environment new lines"
                    }
                };
                var inputDocument = new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "newCMC"
                };
                studentDocument = new StudentDocument(inputDocument, inputCodes, suppressDocumentInstance, useStatusDescription);

                string expectedExplanation = "This is an explanation<br /><br /> with an environment new lines";
                Assert.AreEqual(expectedExplanation, studentDocument.Explanation);
            }

            [TestMethod]
            public void ExplanationWithMultipleEnvironmentNewLinesAndCarriageReturns_EqualsExpectedValueTest()
            {
                var inputCodes = new List<CommunicationCode2>()
                {
                    new CommunicationCode2()
                    {
                        Code = "newCMC",
                        Explanation="This is an explanation\r\n\r\n\r\r\r with an environment new lines"
                    }
                };
                var inputDocument = new Colleague.Dtos.FinancialAid.StudentDocument()
                {
                    Code = "newCMC"
                };
                studentDocument = new StudentDocument(inputDocument, inputCodes, suppressDocumentInstance, useStatusDescription);

                string expectedExplanation = "This is an explanation<br /><br /><br /><br /><br /> with an environment new lines";
                Assert.AreEqual(expectedExplanation, studentDocument.Explanation);
            }

            [TestMethod]
            public void NullExplanation_ReturnsEmptyStringTest()
            {
                var commCodeDto = inputCommunicationCodes.First(c => c.Code == inputStudentDocument.Code);
                commCodeDto.Explanation = null;
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(string.Empty, studentDocument.Explanation);
            }

            [TestMethod]
            public void StatusTest()
            {
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(inputStudentDocument.Status, studentDocument.Status);
            }

            [TestMethod]
            public void NotUseStatusDescription_OverdueStatusDisplayTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete;
                inputStudentDocument.DueDate = DateTime.Now.AddDays(-1);
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual("Overdue", studentDocument.StatusDisplay);
            }

            [TestMethod]
            public void UseStatusDescription_OverdueStatusDisplayTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete;
                inputStudentDocument.DueDate = DateTime.Now.AddDays(-1);
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, true);
                Assert.AreEqual("Overdue", studentDocument.StatusDisplay);
            }

            [TestMethod]
            public void NotOverdueStatusDisplay_NoDueDateTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Received;
                inputStudentDocument.DueDate = null;
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(inputStudentDocument.Status.ToString(), studentDocument.StatusDisplay);
            }

            [TestMethod]           
            public void NotOverdueStatusDisplay_GreaterThanDueDateTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Received;
                inputStudentDocument.DueDate = DateTime.Now.AddDays(1);
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(inputStudentDocument.Status.ToString(), studentDocument.StatusDisplay);
            }

            [TestMethod]
            public void StatusDate_ReceivedTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Received;
                inputStudentDocument.StatusDate = DateTime.Today;
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(inputStudentDocument.StatusDate, studentDocument.Date);
                Assert.IsFalse(studentDocument.IsDueDate);
            }

            [TestMethod]
            public void MultipleUrlsTest()
            {
                var matchingCommunicationCode = inputCommunicationCodes.First(cc => cc.Code == inputStudentDocument.Code);
                matchingCommunicationCode.Hyperlinks.ToList().Add(new Colleague.Dtos.Base.CommunicationCodeHyperlink()
                    {
                        Url = "FOO",
                        Title = "BAR"
                    });
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(matchingCommunicationCode.Hyperlinks.Count(), studentDocument.Hyperlinks.Count());

                
                CollectionAssert.AreEqual(matchingCommunicationCode.Hyperlinks.ToList(), studentDocument.Hyperlinks.ToList(), communicationCodeHyperlinkDtoComparer);
                
            }

            [TestMethod]
            public void StatusDate_WaivedTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Waived;
                inputStudentDocument.StatusDate = DateTime.Today;
                inputStudentDocument.DueDate = DateTime.Today.AddDays(1);
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(inputStudentDocument.StatusDate, studentDocument.Date);
                Assert.IsFalse(studentDocument.IsDueDate);
            }

            [TestMethod]
            public void DueDateTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete;
                inputStudentDocument.StatusDate = DateTime.Today;
                inputStudentDocument.DueDate = DateTime.Today.AddDays(1);
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.AreEqual(inputStudentDocument.DueDate, studentDocument.Date);
                Assert.IsTrue(studentDocument.IsDueDate);
            }

            [TestMethod]
            public void UseStatusDescription_StatusDescriptionIsUsedTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Received;
                inputStudentDocument.StatusDescription = "Sent by mail";
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, true);
                Assert.AreEqual(inputStudentDocument.StatusDescription, studentDocument.StatusDisplay);
            }

            [TestMethod]
            public void UseStatusDescription_StatusDescriptionNotUsedTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Received;
                inputStudentDocument.StatusDescription = null;
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, true);
                Assert.AreEqual(inputStudentDocument.Status.ToString(), studentDocument.StatusDisplay);
            }

            [TestMethod]
            public void IsOutstandingTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Incomplete;                
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsTrue(studentDocument.IsOutstanding);
            }

            [TestMethod]
            public void NotIsOutstandingTest()
            {
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Waived;
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsFalse(studentDocument.IsOutstanding);
            }

            [TestMethod]
            public void EqualsReturnsTrueTest()
            {
                var studentDocument1 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                var studentDocument2 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsTrue(studentDocument1.Equals(studentDocument2));
            }

            [TestMethod]
            public void DifferentDocumentCode_EqualsReturnsFalseTest()
            {
                var studentDocument1 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                inputStudentDocument.Code = testModelData.studentDocumentsData.Last().Code;
                var studentDocument2 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsFalse(studentDocument1.Equals(studentDocument2));
            }

            [TestMethod]
            public void DifferentInstance_EqualsReturnsFalseTest()
            {
                var studentDocument1 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                inputStudentDocument.Instance = "instance_foo";
                var studentDocument2 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsFalse(studentDocument1.Equals(studentDocument2));
            }

            [TestMethod]
            public void DifferentDate_EqualsReturnsFalseTest()
            {
                var studentDocument1 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                inputStudentDocument.StatusDate = DateTime.Today;
                var studentDocument2 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsFalse(studentDocument1.Equals(studentDocument2));
            }

            [TestMethod]
            public void DifferentStatus_EqualsReturnsFalseTest()
            {
                var studentDocument1 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                inputStudentDocument.Status = Colleague.Dtos.FinancialAid.DocumentStatus.Waived;
                var studentDocument2 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                Assert.IsFalse(studentDocument1.Equals(studentDocument2));
            }

            [TestMethod]
            public void ComparingToNull_EqualsReturnsFalseTest()
            {
                StudentDocument studentDocument1 = new StudentDocument(inputStudentDocument, inputCommunicationCodes, suppressDocumentInstance, useStatusDescription);
                StudentDocument studentDocument2 = null;
                Assert.IsFalse(studentDocument1.Equals(studentDocument2));
            }

            [TestMethod]
            public void NotSuppressInstance_InstanceEqualsExpectedTest()
            {
                string expectedInstanceText = "InstanceText";
                inputStudentDocument.Instance = expectedInstanceText;
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, false, true);
                Assert.AreEqual(expectedInstanceText, studentDocument.Instance);
            }

            [TestMethod]
            public void SuppressInstance_InstanceEqualsExpectedTest()
            {
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, true, true);
                Assert.AreEqual(string.Empty, studentDocument.Instance);
            }

            [TestMethod]
            public void NoDocumentHyperlinks_EmptyListAssignedToPropertyTest()
            {
                inputCommunicationCodes.First(c => c.Code == inputStudentDocument.Code).Hyperlinks = null;
                studentDocument = new StudentDocument(inputStudentDocument, inputCommunicationCodes, false, true);
                Assert.IsFalse(studentDocument.Hyperlinks.Any());
            }
            
        }

    }
}