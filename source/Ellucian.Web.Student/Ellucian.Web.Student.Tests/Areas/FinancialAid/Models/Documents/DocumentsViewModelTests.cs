﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Documents
{
    [TestClass]
    public class DocumentsViewModelTests
    {
        private TestModelData testModelDataObject;
        private DocumentsViewModel documentsViewModel;
        private ICurrentUser currentUser;

        [TestInitialize]
        public void Initialize()
        {
            testModelDataObject = new TestModelData("0003914");
            currentUser = null;
            BuildDocumentsViewModel();

        }

        private void BuildDocumentsViewModel()
        {

            documentsViewModel = new DocumentsViewModel(
                testModelDataObject.studentData,
                testModelDataObject.financialAidCounselorData,
                testModelDataObject.studentDocumentsData,
                testModelDataObject.communicationCodesData,
                testModelDataObject.officeCodesData,
                testModelDataObject.studentAwardYearsData,
                testModelDataObject.financialAidOfficesData,
                currentUser);
        }

        [TestMethod]
        public void NonNullPersonDataTest()
        {
            Assert.AreEqual(testModelDataObject.studentData, documentsViewModel.Person);
        }

        [TestMethod]
        public void NullPersonDataTest()
        {
            testModelDataObject.studentData = null;
            BuildDocumentsViewModel();
            Assert.IsNull(documentsViewModel.Person);
        }

        [TestMethod]
        public void CounselorCreatedForEachYear_DocumentsViewModel()
        {
            documentsViewModel.AwardYears.ToList().ForEach(y => Assert.IsNotNull(y.Counselor));
        }

        [TestMethod]
        public void CounselorNotCreated_NoExceptions_DocumentsViewModel()
        {
            testModelDataObject.financialAidCounselorData.Name = string.Empty;
            BuildDocumentsViewModel();

            documentsViewModel.AwardYears.ToList().ForEach(y => Assert.IsNull(y.Counselor));
        }

        [TestMethod]
        public void NumberOfAwardYearsTest()
        {
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), documentsViewModel.AwardYears.Count());
        }

        [TestMethod]
        public void NumberOfDocumentCollectionsTest()
        {
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), documentsViewModel.StudentDocumentCollections.Count());
        }

        [TestMethod]
        public void NullFinancialAidOfficesTest()
        {
            testModelDataObject.financialAidOfficesData = null;
            BuildDocumentsViewModel();

            Assert.AreEqual(0, documentsViewModel.StudentDocumentCollections.Count());

        }

        [TestMethod]
        public void NoStudentAwardYearsTest()
        {
            testModelDataObject.studentAwardYearsData = new List<StudentAwardYear2>();
            BuildDocumentsViewModel();
            Assert.AreEqual(0, documentsViewModel.AwardYears.Count());
            Assert.AreEqual(0, documentsViewModel.StudentDocumentCollections.Count());
        }

        [TestMethod]
        public void NullStudentAwardYearsTest()
        {
            testModelDataObject.studentAwardYearsData = null;
            BuildDocumentsViewModel();
            Assert.AreEqual(0, documentsViewModel.AwardYears.Count());
            Assert.AreEqual(0, documentsViewModel.StudentDocumentCollections.Count());
        }

        [TestMethod]
        public void AwardYearsInDescendingOrderTest()
        {
            var expectedAwardYears = documentsViewModel.AwardYears.ToList().OrderByDescending(a => a.Code);

            CollectionAssert.AreEqual(expectedAwardYears.ToList(), documentsViewModel.AwardYears.ToList());
        }

        [TestMethod]
        public void NullStudentDocumentsDataTest()
        {
            testModelDataObject.studentDocumentsData = null;
            BuildDocumentsViewModel();

            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), documentsViewModel.StudentDocumentCollections.Count());
            Assert.IsTrue(documentsViewModel.StudentDocumentCollections.All(c => c.GetStudentDocuments().Count() == 0));
        }

        [TestMethod]
        public void NullCommunicationCodesDataTest()
        {
            testModelDataObject.communicationCodesData = null;
            BuildDocumentsViewModel();

            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), documentsViewModel.StudentDocumentCollections.Count());
            Assert.IsTrue(documentsViewModel.StudentDocumentCollections.All(c => c.GetStudentDocuments().Count() == 0));
        }

        [TestMethod]
        public void NullOfficeCodesDataTest()
        {
            testModelDataObject.officeCodesData = null;
            BuildDocumentsViewModel();

            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), documentsViewModel.StudentDocumentCollections.Count());
            Assert.IsTrue(documentsViewModel.StudentDocumentCollections.All(c => c.GetStudentDocuments().Count() == 0));
        }

        [TestMethod]
        public void IsProxyViewIsFalse_WhenCurrentUserIsNullTest()
        {
            Assert.IsFalse(documentsViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyViewIsFalse_WhenCurrentUserIsNotProxyTest()
        {
            currentUser = testModelDataObject.currentUser;
            BuildDocumentsViewModel();
            Assert.IsFalse(documentsViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyViewIsTrue_WhenCurrentUserIsProxyTest()
        {
            currentUser = testModelDataObject.currentUserWithProxy;
            BuildDocumentsViewModel();
            Assert.IsTrue(documentsViewModel.IsProxyView);
        }

        [TestMethod]
        public void NoPersonData_PersonIdIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildDocumentsViewModel();
            Assert.IsNull(documentsViewModel.PersonId);
        }

        [TestMethod]
        public void PersonId_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.Id, documentsViewModel.PersonId);
        }

        [TestMethod]
        public void NoPersonData_PersonNameIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildDocumentsViewModel();
            Assert.IsNull(documentsViewModel.PersonName);
        }

        [TestMethod]
        public void PersonName_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PreferredName, documentsViewModel.PersonName);
        }

        [TestMethod]
        public void NoPersonData_PrivacyStatusCodeIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildDocumentsViewModel();
            Assert.IsNull(documentsViewModel.PrivacyStatusCode);
        }

        [TestMethod]
        public void PrivacyStatusCode_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PrivacyStatusCode, documentsViewModel.PrivacyStatusCode);
        }
    }

    /// <summary>
    /// Most of these tests were written before DocumentsViewModel was refactored to use the constructors of other objects.
    /// These tests are still valid, but they test code that executes in other objects. For instance, IncompleteDocuments_CorrectNumber,
    /// is a test for the StudentDocumentCollection class. 
    /// </summary>
    [TestClass]
    public class DocumentsViewModelTestsOld
    {

        private TestModelData testModelDataObject;
        private DocumentsViewModel documentsViewModel;

        [TestInitialize]
        public void Initialize()
        {
            testModelDataObject = new TestModelData("0003914");
            BuildDocumentsViewModel();

        }

        /// <summary>
        /// Test if the documentsViewModel object is not null and created with correct attrs
        /// </summary>
        [TestMethod]
        public void DtoData_DocumentsViewModel()
        {
            Assert.IsNotNull(documentsViewModel);
            Assert.IsNotNull(documentsViewModel.StudentDocumentCollections);
            Assert.IsNotNull(documentsViewModel.AwardYears);

            var studentDocuments = documentsViewModel.StudentDocumentCollections;

            foreach (var collection in studentDocuments)
            {
                var documents = collection.IncompleteDocuments.Concat(collection.CompleteDocuments);
                foreach (var studentDocument in documents)
                {
                    var documentDto = testModelDataObject.studentDocumentsData.First(sdd => sdd.Code == studentDocument.Code);
                    var cCodeDto = testModelDataObject.communicationCodesData.FirstOrDefault(ccd => ccd.Code == studentDocument.Code);
                    //Assert.AreEqual(documentDto.DueDate, studentDocument.Date);
                    Assert.AreEqual(documentDto.Status, studentDocument.Status);
                    Assert.AreEqual(cCodeDto.Description, studentDocument.Description);
                    Assert.AreEqual(cCodeDto.Explanation, studentDocument.Explanation);
                }
            }

        }

        /// <summary>
        /// Test if the documentsViewModel object is created without any data
        /// </summary>
        [TestMethod]
        public void NoData_DocumentsViewModel()
        {
            testModelDataObject.studentDocumentsData = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
            testModelDataObject.communicationCodesData = new List<Colleague.Dtos.Base.CommunicationCode2>();
            testModelDataObject.awardYearsData = new List<Colleague.Dtos.FinancialAid.AwardYear>();

            BuildDocumentsViewModel();

            Assert.IsNotNull(documentsViewModel);
            Assert.IsNotNull(documentsViewModel.StudentDocumentCollections);
            Assert.IsNotNull(documentsViewModel.AwardYears);
        }

        /// <summary>
        /// Test if a document with no year, given it is viewable, gets added to 
        /// each year on student's record
        /// </summary>
        [TestMethod]
        public void StudentDocumentNoYear_DocumentsViewModel()
        {
            if (testModelDataObject.communicationCodesData.FirstOrDefault(ccd => ccd.Code == "docNoYear").IsStudentViewable)
            {
                Assert.IsTrue(documentsViewModel.StudentDocumentCollections.Where(sd => sd.AwardYearCode == "2013").Any(sd => sd.GetStudentDocuments().Any(d => d.Code == "docNoYear")));
                Assert.IsTrue(documentsViewModel.StudentDocumentCollections.Where(sd => sd.AwardYearCode == "2014").Any(sd => sd.GetStudentDocuments().Any(d => d.Code == "docNoYear")));
            }
        }

        /// <summary>
        /// Test if the AwardYears object and StudentDocuments objects have distinct award years
        /// </summary>
        [TestMethod]
        public void DistinctAwardYears_DocumentsViewModel()
        {
            Assert.AreEqual(testModelDataObject.awardYearsData.Count(), documentsViewModel.AwardYears.Count());
            Assert.AreEqual(testModelDataObject.awardYearsData.Count(), documentsViewModel.StudentDocumentCollections.Count());

            foreach (var year in documentsViewModel.StudentDocumentCollections)
            {
                Assert.IsTrue(testModelDataObject.awardYearsData.Any(ay => ay.Code == year.AwardYearCode));
            }
        }

        /// <summary>
        /// Test if award years are arranged in descending order
        /// </summary>
        [TestMethod]
        public void AwardYearsDescendingOrder_DocumentsViewModel()
        {
            var awardYears = documentsViewModel.AwardYears.ToList();

            for (int i = 0; i < awardYears.Count() - 1; i++)
            {
                Assert.IsTrue(awardYears[i].Code.CompareTo(awardYears[i + 1].Code) > 0);
            }
        }

        /// <summary>
        /// Test if correct number of incomplete documents are sorted by date
        /// </summary>
        [TestMethod]
        public void IncompleteDocuments_CorrectNumber()
        {
            var yearCodes = documentsViewModel.AwardYears.Select(y => y.Code).ToList();
            List<Ellucian.Web.Student.Areas.FinancialAid.Models.StudentDocument> expectedDocs;
            List<Ellucian.Web.Student.Areas.FinancialAid.Models.StudentDocument> incompleteDocs;
            foreach (var year in yearCodes)
            {
                expectedDocs = new List<Student.Areas.FinancialAid.Models.StudentDocument>(
                    documentsViewModel.StudentDocumentCollections.FirstOrDefault(sd => sd.AwardYearCode == year).GetStudentDocuments().
                    Where(doc => doc.Status == DocumentStatus.Incomplete));

                incompleteDocs = new List<Student.Areas.FinancialAid.Models.StudentDocument>(
                    documentsViewModel.StudentDocumentCollections.FirstOrDefault(sd => sd.AwardYearCode == year).IncompleteDocuments);

                Assert.AreEqual(expectedDocs.Count(), incompleteDocs.Count());
            }
        }

        /// <summary>
        /// Test if the due dates for complete documents show in correct order:
        /// further due date - closer due date - past due date - no due date
        /// </summary>
        [TestMethod]
        public void CompleteDocumentsDueDatesOrder_DocumentsViewModel()
        {
            var yearCodes = documentsViewModel.AwardYears.Select(y => y.Code).ToList();
            List<Ellucian.Web.Student.Areas.FinancialAid.Models.StudentDocument> completedDocs;
            foreach (var year in yearCodes)
            {
                completedDocs = new List<Student.Areas.FinancialAid.Models.StudentDocument>(documentsViewModel.StudentDocumentCollections.Where(sd => sd.AwardYearCode == year).First().CompleteDocuments.Where(cd => cd.Status != DocumentStatus.Incomplete));

                var noDueDateCount = completedDocs.FindAll(d => !d.Date.HasValue).Count();
                var currentDate = DateTime.Today;
                var dueDateCount = completedDocs.FindAll(d => d.Date.HasValue).Count();

                if (completedDocs.Count > 1)
                {
                    if (dueDateCount < completedDocs.Count)
                    {
                        for (int n = 0; n < dueDateCount; n++)
                        {
                            Assert.IsTrue(!completedDocs[n + 1].Date.HasValue || (completedDocs[n + 1].Date < completedDocs[n].Date));
                        }
                    }

                    for (int rest = dueDateCount; rest < completedDocs.Count(); rest++)
                    {
                        Assert.IsTrue(completedDocs[rest].Date == null);
                    }
                }
            }
        }

        /// <summary>
        /// Test if correct number of incomplete documents are sorted by date
        /// </summary>
        [TestMethod]
        public void DocumentsExist_DocumentsViewModel()
        {
            if (documentsViewModel.StudentDocumentCollections != null && documentsViewModel.StudentDocumentCollections.Count > 0)
            {
                foreach (var sdc in documentsViewModel.StudentDocumentCollections)
                {
                    if (sdc.GetStudentDocuments().Count > 0)
                    {
                        Assert.IsTrue(sdc.DocumentsExist);
                    }
                    else
                    {
                        Assert.IsFalse(sdc.DocumentsExist);
                    }
                }
            }
        }

        /// <summary>
        /// Test if all the student documents in the list correspond to communication codes
        /// that have a flag IsStudentViewable set to true
        /// </summary>
        [TestMethod]
        public void StudentDocumentCodes_EqualViewableCommunicationCodes()
        {
            var viewableCommunicationCodes = testModelDataObject.communicationCodesData.FindAll(ccd => ccd.IsStudentViewable);
            foreach (var docCollection in documentsViewModel.StudentDocumentCollections)
            {
                foreach (var document in docCollection.GetStudentDocuments())
                {
                    Assert.IsTrue(viewableCommunicationCodes.Select(vcc => vcc.Code).Contains(document.Code));
                }
            }
        }

        [TestMethod]
        public void InstanceDataExistsWhenNotSuppressedTest()
        {
            var expectedInstance = "foobar";
            var studentAwardYearDto = testModelDataObject.studentAwardYearsData[0];
            var configurationForYear = testModelDataObject.financialAidOfficesData
                .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code);
            configurationForYear.SuppressInstanceData = false;
            testModelDataObject.studentDocumentsData[0].Instance = expectedInstance;

            BuildDocumentsViewModel();

            var actualDocument = documentsViewModel.StudentDocumentCollections
                .First(dc => dc.AwardYearCode == studentAwardYearDto.Code)
                .GetStudentDocuments()
                .First(d => d.Code == testModelDataObject.studentDocumentsData[0].Code);

            Assert.AreEqual(expectedInstance, actualDocument.Instance);
        }

        [TestMethod]
        public void InstanceDataDoesNotExistsWhenSuppressedTest()
        {

            var studentAwardYearDto = testModelDataObject.studentAwardYearsData[0];
            var configurationForYear = testModelDataObject.financialAidOfficesData
                .SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code);
            configurationForYear.SuppressInstanceData = true;
            testModelDataObject.studentDocumentsData[0].Instance = "foobar";

            BuildDocumentsViewModel();

            var actualDocument = documentsViewModel.StudentDocumentCollections
                .First(dc => dc.AwardYearCode == testModelDataObject.studentAwardYearsData[0].Code)
                .GetStudentDocuments()
                .First(d => d.Code == testModelDataObject.studentDocumentsData[0].Code);

            Assert.AreEqual(string.Empty, actualDocument.Instance);
        }        

        private void BuildDocumentsViewModel()
        {

            documentsViewModel = new DocumentsViewModel(
                testModelDataObject.studentData,
                testModelDataObject.financialAidCounselorData,
                testModelDataObject.studentDocumentsData,
                testModelDataObject.communicationCodesData,
                testModelDataObject.officeCodesData,
                testModelDataObject.studentAwardYearsData,
                testModelDataObject.financialAidOfficesData,
                null);
        }
    }


}
