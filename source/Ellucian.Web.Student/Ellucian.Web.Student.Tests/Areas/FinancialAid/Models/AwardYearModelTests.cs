﻿//Copyright 2014-2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models
{
    [TestClass]
    public class AwardYearModelTests
    {
        private string studentId;
        private TestModelData testModelDataObject;

        private Colleague.Dtos.FinancialAid.StudentAwardYear2 studentAwardYearDto;
        private Colleague.Dtos.FinancialAid.AwardPeriod awardPeriodDto;
        private AwardYear awardYearModel;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0003914";
            testModelDataObject = new TestModelData(studentId);
            awardPeriodDto = new Colleague.Dtos.FinancialAid.AwardPeriod()
            {
                Code = "Period1",
                Description = "This is period 1",
                StartDate = DateTime.Today.AddDays(-1)
            };

            studentAwardYearDto = new Colleague.Dtos.FinancialAid.StudentAwardYear2()
            {
                Code = "2014",
                Description = "Three Thousand Fourteen",
                StudentId = studentId,
                FinancialAidOfficeId = "MAIN"
            };
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            awardYearModel = new AwardYear();
            Assert.IsNotNull(awardYearModel.DistinctAwardPeriods);
        }

        [TestMethod]
        public void CodeGetSetTest()
        {
            awardYearModel = new AwardYear();
            awardYearModel.Code = studentAwardYearDto.Code;
            Assert.AreEqual(studentAwardYearDto.Code, awardYearModel.Code);
        }

        [TestMethod]
        public void DescriptionGetSetTest()
        {
            awardYearModel = new AwardYear();
            awardYearModel.Description = studentAwardYearDto.Description;
            Assert.AreEqual(studentAwardYearDto.Description, awardYearModel.Description);
        }

        [TestMethod]
        public void AddNullDistinctAwardPeriodTest()
        {
            awardYearModel = new AwardYear();
            awardPeriodDto = null;
            var result = awardYearModel.AddDistinctAwardPeriod(awardPeriodDto);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AddExistingAwardPeriodTest()
        {
            awardYearModel = new AwardYear();
            awardYearModel.AddDistinctAwardPeriod(awardPeriodDto);

            var newAwardPeriod = new Colleague.Dtos.FinancialAid.AwardPeriod()
            {
                Code = "Period1",
                Description = "This is period with the same code",
                StartDate = DateTime.Today.AddDays(-1)
            };

            var result = awardYearModel.AddDistinctAwardPeriod(newAwardPeriod);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AddAwardPeriodTest()
        {
            awardYearModel = new AwardYear();
            awardYearModel.AddDistinctAwardPeriod(awardPeriodDto);

            Assert.AreEqual(1, awardYearModel.DistinctAwardPeriods.Count());

            Assert.AreEqual(awardPeriodDto, awardYearModel.DistinctAwardPeriods[0]);
        }

        [TestMethod]
        public void AwardPeriodsSortedByStartDateTest()
        {
            awardYearModel = new AwardYear();
            var otherAwardPeriod = new Colleague.Dtos.FinancialAid.AwardPeriod()
            {
                Code = "Period2",
                Description = "This is period 2",
                StartDate = DateTime.Today.AddDays(1)
            };

            awardYearModel.AddDistinctAwardPeriod(otherAwardPeriod);
            awardYearModel.AddDistinctAwardPeriod(awardPeriodDto);

            Assert.AreEqual(2, awardYearModel.DistinctAwardPeriods.Count());

            Assert.AreEqual(awardPeriodDto, awardYearModel.DistinctAwardPeriods[0]);
            Assert.AreEqual(otherAwardPeriod, awardYearModel.DistinctAwardPeriods[1]);
        }

        [TestMethod]
        public void StudentAwardYearDtoConstructorTest()
        {
            var configurationForYear = testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code);
            awardYearModel = new AwardYear(studentAwardYearDto);
            Assert.AreEqual(studentAwardYearDto.Code, awardYearModel.Code);
            Assert.AreEqual(studentAwardYearDto.Description, awardYearModel.Description);

            Assert.IsNotNull(awardYearModel.DistinctAwardPeriods);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullArgumentToConstructorThrowsExceptionTest()
        {
            new AwardYear(null);
        }

        [TestMethod]
        public void StudentAwardYearDtoHasNoDescriptionTest()
        {
            studentAwardYearDto.Description = null;
            var configurationForYear = testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code);
            awardYearModel = new AwardYear(studentAwardYearDto);
            Assert.AreEqual(studentAwardYearDto.Code, awardYearModel.Description);
        }

        [TestMethod]
        public void PrerequisiteFunctionConstructorTest()
        {
            var prereqSatisified = true;
            var configurationForYear = testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYearDto.FinancialAidOfficeId && c.AwardYear == studentAwardYearDto.Code);
            awardYearModel = new AwardYear(studentAwardYearDto, (() => prereqSatisified), false);
            Assert.AreEqual(studentAwardYearDto.Code, awardYearModel.Code);
            Assert.AreEqual(studentAwardYearDto.Description, awardYearModel.Description);
            Assert.IsNotNull(awardYearModel.DistinctAwardPeriods);
            Assert.AreEqual(prereqSatisified, awardYearModel.ArePrerequisitesSatisfied);
            Assert.IsFalse(awardYearModel.AreChecklistItemsAssigned);
        }

        [TestMethod]
        public void CounselorInitializedToNullTest_DefaultConstructor()
        {
            awardYearModel = new AwardYear();
            Assert.IsNull(awardYearModel.Counselor);
        }

        [TestMethod]
        public void CounselorInitializedToNullTest_StudentAwardYearConstructor()
        {
            awardYearModel = new AwardYear(studentAwardYearDto);
            Assert.IsNull(awardYearModel.Counselor);
        }

        [TestMethod]
        public void CounselorInitializedToNullTest_PrerequisiteCalcConstructor()
        {
            awardYearModel = new AwardYear(studentAwardYearDto, () => true, true);
            Assert.IsNull(awardYearModel.Counselor);
        }

        [TestMethod]
        public void CounselorGetSetTest()
        {
            awardYearModel = new AwardYear();
            var phoneNumber = testModelDataObject.counselorsPhoneNumbersData.First(pnd => pnd.PersonId == testModelDataObject.financialAidCounselorData.Id);
            var counselor = new Counselor(testModelDataObject.financialAidCounselorData, phoneNumber, studentAwardYearDto, testModelDataObject.financialAidOfficesData);
            awardYearModel.Counselor = counselor;

            Assert.IsTrue(counselor.Equals(awardYearModel.Counselor));
        }
    }
}
