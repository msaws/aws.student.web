﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using System.Reflection;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.OutsideAwards
{
    [TestClass]
    public class OutsideAwardsViewModelTests
    {
        private string studentId;
        private OutsideAwardsViewModel outsideAwardsViewModel;
        private TestModelData testModelData;
        private bool isUserSelf;
       
        public void BaseInitialize()
        {
            studentId = "0003914";
            isUserSelf = true;
            testModelData = new TestModelData(studentId);
            BuildOutsideAwardsViewModel();
        }

        private void BuildOutsideAwardsViewModel()
        {
            outsideAwardsViewModel = new OutsideAwardsViewModel
                (testModelData.studentData,
                testModelData.studentAwardYearsData,
                testModelData.financialAidOfficesData,
                testModelData.financialAidCounselorData,
                isUserSelf);
        }

        private void UpdateOutsideAwardsViewModel(string awardYear)
        {
            outsideAwardsViewModel.GetType().GetMethod("UpdateOutsideAwardsViewModel", BindingFlags.NonPublic | BindingFlags.Instance)
                .Invoke(outsideAwardsViewModel, new object[] { testModelData.outsideAwardsData.Where(a => a.AwardYearCode == awardYear) });
        }
        
        public void BaseCleanup()
        {
            outsideAwardsViewModel = null;
            testModelData = null;
            studentId = null;
        }

        [TestClass]
        public class OutsideAwardsViewModelConstructorTests : OutsideAwardsViewModelTests
        {
            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();
            }

            [TestCleanup]
            public void Cleanup()
            {
                BaseCleanup();
            }

            [TestMethod]
            public void OutsideAwardsViewModelInitializedTest()
            {
                Assert.IsNotNull(outsideAwardsViewModel);
                Assert.IsNotNull(outsideAwardsViewModel.Student);
                Assert.IsNotNull(outsideAwardsViewModel.AwardYears);
                Assert.IsNotNull(outsideAwardsViewModel.OutsideAwards);
                Assert.IsNotNull(outsideAwardsViewModel.OutsideAwardTypes);
                Assert.IsTrue(outsideAwardsViewModel.IsUserSelf);
            }

            [TestMethod]
            public void NoData_OutsideAwardsViewModelInitializedTest()
            {
                outsideAwardsViewModel = new OutsideAwardsViewModel(null, null, null, null, false);
                Assert.IsNotNull(outsideAwardsViewModel);
                Assert.IsNotNull(outsideAwardsViewModel.Student);
                Assert.IsNotNull(outsideAwardsViewModel.AwardYears);
                Assert.IsNotNull(outsideAwardsViewModel.OutsideAwards);
                Assert.IsNotNull(outsideAwardsViewModel.OutsideAwardTypes);
                Assert.IsFalse(outsideAwardsViewModel.IsUserSelf);
            }

            [TestMethod]
            public void StudentAwardYearsCount_EqualsExpectedTest()
            {
                Assert.AreEqual(testModelData.studentAwardYearsData.Count, outsideAwardsViewModel.AwardYears.Count());
            }

            [TestMethod]
            public void StudentAwardYears_AreSortedTest()
            {
                var sortedExpectedYears = testModelData.studentAwardYearsData.OrderByDescending(y => y.Code).ToList();
                var actualYears = outsideAwardsViewModel.AwardYears.ToList();
                for (var i = 0; i < sortedExpectedYears.Count; i++)
                {
                    Assert.AreEqual(sortedExpectedYears[i].Code, actualYears[i].Code);
                }
            }

            [TestMethod]
            public void StudentProperty_MatchesExpectedTest()
            {
                var expectedStudent = testModelData.studentData;
                Assert.AreEqual(expectedStudent.Id, outsideAwardsViewModel.Student.Id);
                Assert.AreEqual(expectedStudent.PreferredName, outsideAwardsViewModel.Student.PreferredName);
            }

            [TestMethod]
            public void NoPersonData_PersonIdIsnullTest()
            {
                testModelData.studentData = null;
                BuildOutsideAwardsViewModel();
                Assert.IsNull(outsideAwardsViewModel.PersonId);
            }

            [TestMethod]
            public void PersonId_EqualsExpectedTest()
            {
                Assert.AreEqual(testModelData.studentData.Id, outsideAwardsViewModel.PersonId);
            }

            [TestMethod]
            public void NoPersonData_PersonNameIsnullTest()
            {
                testModelData.studentData = null;
                BuildOutsideAwardsViewModel();
                Assert.IsNull(outsideAwardsViewModel.PersonName);
            }

            [TestMethod]
            public void PersonName_EqualsExpectedTest()
            {
                Assert.AreEqual(testModelData.studentData.PreferredName, outsideAwardsViewModel.PersonName);
            }

            [TestMethod]
            public void NoPersonData_PrivacyStatusCodeIsnullTest()
            {
                testModelData.studentData = null;
                BuildOutsideAwardsViewModel();
                Assert.IsNull(outsideAwardsViewModel.PrivacyStatusCode);
            }

            [TestMethod]
            public void PrivacyStatusCode_EqualsExpectedTest()
            {
                Assert.AreEqual(testModelData.studentData.PrivacyStatusCode, outsideAwardsViewModel.PrivacyStatusCode);
            }
        }

        [TestClass]
        public class UpadteOutsideAwardsViewModelTests : OutsideAwardsViewModelTests
        {
            private string awardYearCode = "2016";

            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();                
            }

            [TestCleanup]
            public void Cleanup()
            {
                BaseCleanup();
                awardYearCode = null;
            }

            [TestMethod]
            public void UpdateOutsideAwardsViewModel_OutsideAwardsEqualExpectedTest()
            {
                UpdateOutsideAwardsViewModel(awardYearCode);
                var expectedAwards = testModelData.outsideAwardsData.Where(a => a.AwardYearCode == awardYearCode).ToList();
                var actualAwards = outsideAwardsViewModel.OutsideAwards.ToList();
                CollectionAssert.AreEqual(expectedAwards, actualAwards);
            }

            [TestMethod]
            public void ExistingOutsideAwards_AreClearedOnUpdateTest()
            {
                outsideAwardsViewModel.OutsideAwards.Add(testModelData.outsideAwardsData.First(a => a.AwardYearCode == awardYearCode));
                UpdateOutsideAwardsViewModel(awardYearCode);
                var expectedAwards = testModelData.outsideAwardsData.Where(a => a.AwardYearCode == awardYearCode);
                Assert.AreEqual(expectedAwards.Count(), outsideAwardsViewModel.OutsideAwards.Count());
            }

            [TestMethod]
            public void OutsideAwardTypes_EqualExpectedTest()
            {
                UpdateOutsideAwardsViewModel(awardYearCode);
                var expectedTypes = new List<string>() { "Scholarship", "Grant", "Loan" };
                CollectionAssert.AreEqual(expectedTypes, outsideAwardsViewModel.OutsideAwardTypes);
            }
        }
    }
}
