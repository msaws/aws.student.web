﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Home
{
    [TestClass]
    public class AverageAwardPackageChartTests
    {
        private TestModelData testModelData;
        private AverageAwardPackage averageAwardPackageDto;
        private AverageAwardPackage averageAwardPackage;
 
        public int AverageGrantAmount { get; set; }
        public int AverageLoanAmount { get; set; }
        public int AverageScholarshipAmount { get; set; }
        public string AwardYearCode { get; set; }

        public AverageAwardPackageChartModel averageAwardPackageChart;

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0004791");
            averageAwardPackageDto = testModelData.averageAwardPackageData.First();
            AverageGrantAmount = averageAwardPackageDto.AverageGrantAmount;
            AverageLoanAmount = averageAwardPackageDto.AverageLoanAmount;
            AverageScholarshipAmount = averageAwardPackageDto.AverageScholarshipAmount;
            AwardYearCode = averageAwardPackageDto.AwardYearCode;
            averageAwardPackageChart = BuildAverageAwardPackageChart(AverageGrantAmount,AverageLoanAmount,AverageScholarshipAmount,AwardYearCode);
        }

        [TestCleanup]
        public void Cleanup()
        {
            averageAwardPackageDto = null;
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            averageAwardPackage = new AverageAwardPackage();
            Assert.IsNotNull(averageAwardPackage);
        }

        [TestMethod]
        public void AverageAwardPackageDto_PropertiesSetTest()
        {
            averageAwardPackage = averageAwardPackageDto;

            Assert.IsNotNull(averageAwardPackage);
            Assert.IsFalse(averageAwardPackage.AverageGrantAmount == 0);
            Assert.IsFalse(averageAwardPackage.AverageLoanAmount == 0);
            Assert.IsFalse(averageAwardPackage.AverageScholarshipAmount == 0);
            Assert.IsNotNull(averageAwardPackage.AwardYearCode);
        }

        [TestMethod]
        public void AverageAwardPackageChart_PropertiesNotSetTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);
            Assert.IsNotNull(averageAwardPackage);
            Assert.IsFalse(averageAwardPackage.AverageGrantAmount == 0);
            Assert.IsFalse(averageAwardPackage.AverageLoanAmount == 0);
            Assert.IsFalse(averageAwardPackage.AverageScholarshipAmount == 0);
            Assert.IsNotNull(averageAwardPackage.AwardYearCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AverageAwardPackageChart_NegativeAverageGrantAmountTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            averageAwardPackage.AverageGrantAmount = -1000;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AverageAwardPackageChart_NegativeAverageLoanAmountTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            averageAwardPackage.AverageLoanAmount = -1000;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AverageAwardPackageChart_NegativeAverageScholarshipAmountTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            averageAwardPackage.AverageScholarshipAmount = -1000;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AverageAwardPackageChart_AllZerosTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            averageAwardPackage.AverageGrantAmount = 0;
            averageAwardPackage.AverageLoanAmount = 0;
            averageAwardPackage.AverageScholarshipAmount = 0;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void AverageAwardPackageChart_NullAwardYearTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            averageAwardPackage.AwardYearCode = null;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);
        }

        [TestMethod]
        public void AverageAwardPackage_ChartNotNullTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);

            Assert.IsNotNull(averageChartData.ChartData);
        }

        [TestMethod]
        public void AverageAwardPackage_ChartValuesMatchTest()
        {
            averageAwardPackage = averageAwardPackageDto;
            var averageChartData = BuildAverageAwardPackageChart(averageAwardPackage.AverageGrantAmount, averageAwardPackage.AverageLoanAmount, averageAwardPackage.AverageScholarshipAmount, averageAwardPackage.AwardYearCode);

            Assert.IsTrue(averageChartData.ChartData[0].value == 5000);
            Assert.IsTrue(averageChartData.ChartData[0].color == "#88ddfd");
            Assert.IsTrue(averageChartData.ChartData[1].value == 6000);
            Assert.IsTrue(averageChartData.ChartData[1].color == "#6aca3f");
            Assert.IsTrue(averageChartData.ChartData[2].value == 7000);
            Assert.IsTrue(averageChartData.ChartData[2].color == "#fc361d");
        }

        [TestMethod]
        public void SingleNonZeroAverage_SetsShowStrikeFalseTest1()
        {
            var chartModel = new AverageAwardPackageChartModel(0, 0, 35, "2015");
            Assert.IsFalse(chartModel.ChartOptions.segmentShowStroke);
        }

        [TestMethod]
        public void SingleNonZeroAverage_SetsShowStrikeFalseTest2()
        {
            var chartModel = new AverageAwardPackageChartModel(0, 40, 0, "2015");
            Assert.IsFalse(chartModel.ChartOptions.segmentShowStroke);
        }

        [TestMethod]
        public void SingleNonZeroAverage_SetsShowStrikeFalseTest3()
        {
            var chartModel = new AverageAwardPackageChartModel(500, 0, 0, "2015");
            Assert.IsFalse(chartModel.ChartOptions.segmentShowStroke);
        }

        [TestMethod]
        public void MultipleNonZeroAverage_SetsShowStrikeTrueTest1()
        {
            var chartModel = new AverageAwardPackageChartModel(0, 45, 35, "2015");
            Assert.IsTrue(chartModel.ChartOptions.segmentShowStroke);
        }

        [TestMethod]
        public void MultipleNonZeroAverage_SetsShowStrikeTrueTest2()
        {
            var chartModel = new AverageAwardPackageChartModel(234, 45, 0, "2015");
            Assert.IsTrue(chartModel.ChartOptions.segmentShowStroke);
        }

        [TestMethod]
        public void MultipleNonZeroAverage_SetsShowStrikeTrueTest3()
        {
            var chartModel = new AverageAwardPackageChartModel(234, 0, 100, "2015");
            Assert.IsTrue(chartModel.ChartOptions.segmentShowStroke);
        }

        private AverageAwardPackageChartModel BuildAverageAwardPackageChart(int averageGrantAmount, int averageLoanAmount, int averageScholarshipAmount, string awardYearCode)
        {
            return new AverageAwardPackageChartModel(averageGrantAmount, averageLoanAmount, averageScholarshipAmount, awardYearCode);
        }
    }
}
