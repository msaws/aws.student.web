﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Home
{
    [TestClass]
    public class LoanDetailsTests
    {
        private TestModelData testModelData;

        private StudentLoanHistory studentLoanHistoryDto;
        private IpedsInstitution ipedsInstitutionDto;

        private LoanDetails loanDetails;

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0004791");
            studentLoanHistoryDto = testModelData.studentLoanSummaryData.StudentLoanHistory.First();
            ipedsInstitutionDto = testModelData.ipedsInstitutionData.First(d => d.OpeId == studentLoanHistoryDto.OpeId);            
        }

        [TestCleanup]
        public void Cleanup()
        {
            studentLoanHistoryDto = null;
            ipedsInstitutionDto = null;
            loanDetails = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullStudentHistoryDto_ThrowsExceptionTest()
        {
            loanDetails = new LoanDetails(null, ipedsInstitutionDto);
        }

        [TestMethod]
        public void NullIpedsInstitutionDto_SchoolNameIsOpeIdTest()
        {
            loanDetails = new LoanDetails(studentLoanHistoryDto, null);
            Assert.AreEqual(studentLoanHistoryDto.OpeId, loanDetails.SchoolName);
        }

        [TestMethod]
        public void NullIpedsInstitutionName_SchoolNameIsOpeIdTest()
        {
            ipedsInstitutionDto.Name = null;
            loanDetails = new LoanDetails(studentLoanHistoryDto, ipedsInstitutionDto);
            Assert.AreEqual(studentLoanHistoryDto.OpeId, loanDetails.SchoolName);
        }

        [TestMethod]
        public void SchoolName_EqualsIpedsInstitutionNameTest()
        {
            loanDetails = new LoanDetails(studentLoanHistoryDto, ipedsInstitutionDto);
            Assert.AreEqual(ipedsInstitutionDto.Name, loanDetails.SchoolName);
        }

        [TestMethod]
        public void TotalLoanAmount_EqualsStudentLoanHistoryTotalTest()
        {
            loanDetails = new LoanDetails(studentLoanHistoryDto, ipedsInstitutionDto);
            Assert.AreEqual(studentLoanHistoryDto.TotalLoanAmount, loanDetails.TotalLoanAmount);
        }
    }
}
