﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Home
{
    [TestClass]
    public class AwardYearConfigurationTests
    {
        public TestModelData testModelData;

        [TestClass]
        public class AwardYearConfigurationConstructorTests : AwardYearConfigurationTests
        {
            public Colleague.Dtos.FinancialAid.FinancialAidConfiguration3 configurationDto;

            public AwardYearConfiguration awardYearConfiguration
            { get { return new AwardYearConfiguration(configurationDto); } }

            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");
                configurationDto = testModelData.financialAidOfficesData.First().Configurations.First();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullConfigurationTest()
            {
                configurationDto = null;
                var test = awardYearConfiguration;
            }

            [TestMethod]
            public void AwardYearTest()
            {
                Assert.AreEqual(configurationDto.AwardYear, awardYearConfiguration.AwardYearCode);
            }

            [TestMethod]
            public void IsDeclinedStatusChangeRequestRequiredTest()
            {
                Assert.AreEqual(configurationDto.IsDeclinedStatusChangeRequestRequired, awardYearConfiguration.IsDeclinedStatusChangeReviewRequired);
            }

            [TestMethod]
            public void IsLoanAMountChangeRequestRequiredTest()
            {
                Assert.AreEqual(configurationDto.IsLoanAmountChangeRequestRequired, awardYearConfiguration.IsLoanAmountChangeReviewRequired);
            }

            [TestMethod]
            public void IsLoanRequestEnabledTest()
            {
                Assert.AreEqual(configurationDto.AreLoanRequestsAllowed, awardYearConfiguration.IsLoanRequestEnabled);
            }

            [TestMethod]
            public void AreAwardingChangesAllowedTest()
            {
                Assert.AreEqual(configurationDto.AreAwardChangesAllowed, awardYearConfiguration.AreAwardingChangesAllowed);
            }

            [TestMethod]
            public void ShoppingSheetConfigurationNotNullTest()
            {
                configurationDto.ShoppingSheetConfiguration = null;
                Assert.IsNotNull(awardYearConfiguration.ShoppingSheetConfiguration);
            }

            [TestMethod]
            public void ShoppingSheetConfigurationIsNotNullTest()
            {
                Assert.IsNotNull(configurationDto.ShoppingSheetConfiguration);
                Assert.IsNotNull(awardYearConfiguration.ShoppingSheetConfiguration);
            }

            [TestMethod]
            public void SuppressStudentAccountSummaryDisplay_EqualsExpectedTest()
            {
                Assert.AreEqual(configurationDto.SuppressAccountSummaryDisplay, awardYearConfiguration.SuppressStudentAccountSummaryDisplay);
            }

            [TestMethod]
            public void SuppressStudentAccountSummaryDisplay_ReturnsFalseTest()
            {
                configurationDto.SuppressAccountSummaryDisplay = false;
                Assert.IsFalse(awardYearConfiguration.SuppressStudentAccountSummaryDisplay);
            }
            [TestMethod]
            public void SuppressAverageAwardPackageDisplay_EqualsExpectedTest()
            {
                Assert.AreEqual(configurationDto.SuppressAverageAwardPackageDisplay, awardYearConfiguration.SuppressAverageAwardPackageDisplay);
            }

            [TestMethod]
            public void SuppressAverageAwardPackageDisplay_ReturnsFalseTest()
            {
                configurationDto.SuppressAverageAwardPackageDisplay = false;
                Assert.IsFalse(awardYearConfiguration.SuppressAverageAwardPackageDisplay);
            }
        }
    }
}
