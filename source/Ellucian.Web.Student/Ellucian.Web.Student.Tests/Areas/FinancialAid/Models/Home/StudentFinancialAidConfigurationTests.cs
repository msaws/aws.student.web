﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Home
{
    [TestClass]
    public class StudentFinancialAidConfigurationTests
    {
        [TestClass]
        public class FinancialAidConfigurationConstructorsTests
        {
            public TestModelData testModelData;
            public StudentFinancialAidConfiguration configuration;

            public IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3> configurationDtos;

            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");
                configurationDtos = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations);
            }

            [TestMethod]
            public void DefaultConstructorTest()
            {
                configuration = new StudentFinancialAidConfiguration();
                Assert.IsFalse(configuration.IsConfigurationComplete);
                Assert.IsNotNull(configuration.MissingConfigurationMessages);
                Assert.AreEqual(0, configuration.MissingConfigurationMessages.Count());
                Assert.IsNotNull(configuration.AwardYearConfigurations);
                Assert.AreEqual(0, configuration.AwardYearConfigurations.Count());
            }

            [TestMethod]
            public void MissingMessagesInitializedTest()
            {
                configuration = new StudentFinancialAidConfiguration(null, null);
                Assert.IsNotNull(configuration.MissingConfigurationMessages);
            }

            [TestMethod]
            public void AwardYearConfigurationsInitializedTest()
            {
                configuration = new StudentFinancialAidConfiguration(null, null);
                Assert.IsNotNull(configuration.AwardYearConfigurations);
            }

            [TestMethod]
            public void IsConfigurationCompleteIsSetToTrueTest()
            {
                configuration = new StudentFinancialAidConfiguration(testModelData.studentAwardYearsData, configurationDtos);
                Assert.IsTrue(configuration.IsConfigurationComplete);
            }

            [TestMethod]
            public void IsConfigurationCompleteIsSetToFalseTest()
            {
                configuration = new StudentFinancialAidConfiguration(testModelData.studentAwardYearsData, null);
                Assert.IsFalse(configuration.IsConfigurationComplete);
            }

            [TestMethod]
            public void AwardYearConfigurationsTest()
            {
                configuration = new StudentFinancialAidConfiguration(testModelData.studentAwardYearsData, configurationDtos);
                var expectedConfigurations = testModelData.studentAwardYearsData.Where(y => configurationDtos.GetConfigurationDtoOrDefault(y) != null);
                Assert.AreEqual(expectedConfigurations.Count(), configuration.AwardYearConfigurations.Count());
            }

            [TestMethod]
            public void NoStudentAwardYears_NoAwardYearConfigurationsTest()
            {
                configuration = new StudentFinancialAidConfiguration(null, configurationDtos);
                Assert.AreEqual(0, configuration.AwardYearConfigurations.Count());
            }

            [TestMethod]
            public void NoConfigurationDtos_NoAwardYearConfigurationsTest()
            {
                configuration = new StudentFinancialAidConfiguration(testModelData.studentAwardYearsData, null);
                Assert.AreEqual(0, configuration.AwardYearConfigurations.Count());
            }
        }

        [TestClass]
        public class SetIsConfigurationCompleteTests
        {
            public TestModelData testModelData;
            public StudentFinancialAidConfiguration configuration;

            public IEnumerable<Colleague.Dtos.FinancialAid.StudentAwardYear2> studentAwardYearDtos
            { get { return testModelData.studentAwardYearsData; } }

            public IEnumerable<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3> configurationDtos
            { get { return testModelData.financialAidOfficesData.SelectMany(o => o.Configurations); } }

            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");
                configuration = new StudentFinancialAidConfiguration();
            }

            [TestMethod]
            public void NullConfigurationTest()
            {
                configuration.SetIsConfigurationComplete(studentAwardYearDtos, null);
                Assert.IsFalse(configuration.IsConfigurationComplete);
                Assert.IsTrue(configuration.MissingConfigurationMessages.Contains("No configurations exist."));
            }

            [TestMethod]
            public void EmptyConfigurationTest()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations = new List<Colleague.Dtos.FinancialAid.FinancialAidConfiguration3>());
                configuration.SetIsConfigurationComplete(studentAwardYearDtos, configurationDtos);
                Assert.IsFalse(configuration.IsConfigurationComplete);
                Assert.IsTrue(configuration.MissingConfigurationMessages.Contains("No configurations exist."));
            }

            [TestMethod]
            public void NullStudentAwardYearDtosTest()
            {
                configuration.SetIsConfigurationComplete(null, configurationDtos);
                Assert.IsTrue(configuration.IsConfigurationComplete);
            }

            [TestMethod]
            public void EmptyStudentAwardYearDtosTest()
            {
                testModelData.studentAwardYearsData = new List<Colleague.Dtos.FinancialAid.StudentAwardYear2>();
                configuration.SetIsConfigurationComplete(null, configurationDtos);
                Assert.IsTrue(configuration.IsConfigurationComplete);
            }

            [TestMethod]
            public void NoConfigurationsExistForOfficesTest()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.OfficeId = "FOOBAR"));
                configuration.SetIsConfigurationComplete(studentAwardYearDtos, configurationDtos);
                Assert.IsFalse(configuration.IsConfigurationComplete);
                Assert.IsTrue(configuration.MissingConfigurationMessages.Any(m => m.StartsWith("No configurations exist for any of the student's awardYear & office combinations:")));
            }

            [TestMethod]
            public void NoConfigurationsExistForAwardYearsTest()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.AwardYear = "FOOBAR"));
                configuration.SetIsConfigurationComplete(studentAwardYearDtos, configurationDtos);
                Assert.IsFalse(configuration.IsConfigurationComplete);
                Assert.IsTrue(configuration.MissingConfigurationMessages.Any(m => m.StartsWith("No configurations exist for any of the student's awardYear & office combinations:")));
            }

            [TestMethod]
            public void NoActiveAwardYearsTest()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.IsSelfServiceActive = false));
                configuration.SetIsConfigurationComplete(studentAwardYearDtos, configurationDtos);
                Assert.IsFalse(configuration.IsConfigurationComplete);
                Assert.IsTrue(configuration.MissingConfigurationMessages.Any(m => m.StartsWith("Self-Service is deactivated for all student's configurations:")));
            }

            [TestMethod]
            public void ConfigurationCompleteTest()
            {
                configuration.SetIsConfigurationComplete(studentAwardYearDtos, configurationDtos);
                Assert.IsTrue(configuration.IsConfigurationComplete);
                Assert.IsNotNull(configuration.MissingConfigurationMessages);
                Assert.AreEqual(0, configuration.MissingConfigurationMessages.Count());
            }
        }
    }
}
