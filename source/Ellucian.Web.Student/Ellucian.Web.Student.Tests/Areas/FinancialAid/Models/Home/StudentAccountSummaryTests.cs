﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Finance.AccountDue;
using Ellucian.Web.Student.Areas.FinancialAid.Models;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Home
{
    [TestClass]
    public class StudentAccountSummaryTests
    {
        private AccountDue studentAccountDueData;
        private StudentFinanceAccountSummary studentAccountSummary;
        private TestModelData testModelData;
        private string studentId;

        [TestInitialize]
        public void Initialize()
        {
            studentId = "0004791";
            testModelData = new TestModelData(studentId);
            studentAccountDueData = testModelData.studentAccountDueData;
            studentAccountSummary = new StudentFinanceAccountSummary(studentAccountDueData);
        }

        [TestMethod]
        public void ObjectCreatedTest()
        {
            Assert.IsNotNull(studentAccountSummary);
        }

        [TestMethod]
        public void StudentAccountDueDataIsNull_ObjectedCreateTest()
        {
            studentAccountSummary = new StudentFinanceAccountSummary(null);
            Assert.IsNotNull(studentAccountSummary);
            Assert.IsTrue(studentAccountSummary.AmountDue == 0);
            Assert.IsTrue(studentAccountSummary.AmountOverdue == 0);
            Assert.IsTrue(studentAccountSummary.TotalAmountDue == 0);
        }

        [TestMethod]
        public void AmountDueEqualsExpectedTest()
        {
            var amountDue = 0m;
            var today = DateTime.Today;
            var accountTerms = studentAccountDueData.AccountTerms;
            foreach (var term in accountTerms)
            {
                if (term.DepositDueItems != null)
                {
                    term.DepositDueItems.ForEach(dd => { if (dd.DueDate >= today) { amountDue += dd.AmountDue; } });
                }
                if(term.GeneralItems != null)
                {
                    term.GeneralItems.ForEach(gi => { if (!gi.DueDate.HasValue || (gi.DueDate.HasValue && gi.DueDate.Value >= today) && gi.AmountDue.HasValue) { amountDue += gi.AmountDue.Value; } });
                }
                if (term.InvoiceItems != null)
                {
                    term.InvoiceItems.ForEach(ii => { if (!ii.DueDate.HasValue || (ii.DueDate.HasValue && ii.DueDate.Value >= today) && ii.AmountDue.HasValue) { amountDue += ii.AmountDue.Value; } });
                }
                if (term.PaymentPlanItems != null)
                {
                    term.PaymentPlanItems.ForEach(ppi => { if (!ppi.DueDate.HasValue || (ppi.DueDate.HasValue && ppi.DueDate.Value >= today) && ppi.AmountDue.HasValue) { amountDue += ppi.AmountDue.Value; } });
                }
            }

            Assert.AreEqual(amountDue, studentAccountSummary.AmountDue);
        }

        [TestMethod]
        public void AmountOverdueEqualsExpectedTest()
        {
            var amountOverdue = 0m;
            var today = DateTime.Today;
            var accountTerms = studentAccountDueData.AccountTerms;
            foreach (var term in accountTerms)
            {
                if (term.DepositDueItems != null)
                {
                    term.DepositDueItems.ForEach(dd => { if (dd.DueDate < today) { amountOverdue += dd.AmountDue; } });
                }
                if (term.GeneralItems != null)
                {
                    term.GeneralItems.ForEach(gi => { if (gi.DueDate.HasValue && gi.DueDate.Value < today && gi.AmountDue.HasValue) { amountOverdue += gi.AmountDue.Value; } });
                }
                if (term.InvoiceItems != null)
                {
                    term.InvoiceItems.ForEach(ii => { if (ii.DueDate.HasValue && ii.DueDate.Value < today && ii.AmountDue.HasValue) { amountOverdue += ii.AmountDue.Value; } });
                }
                if (term.PaymentPlanItems != null)
                {
                    term.PaymentPlanItems.ForEach(ppi => { if (ppi.DueDate.HasValue && ppi.DueDate.Value < today && ppi.AmountDue.HasValue) { amountOverdue += ppi.AmountDue.Value; } });
                }
            }

            Assert.AreEqual(amountOverdue, studentAccountSummary.AmountOverdue);
        }

        [TestMethod]
        public void TotalAmountDueEqualsExpectedTest()
        {
            var amountOverdue = 0m;
            var amountDue = 0m;
            var today = DateTime.Today;
            var accountTerms = studentAccountDueData.AccountTerms;
            foreach (var term in accountTerms)
            {
                if (term.DepositDueItems != null)
                {
                    term.DepositDueItems.ForEach(dd => { if (dd.DueDate < today) { amountOverdue += dd.AmountDue; } else { amountDue += dd.AmountDue; } });
                }
                if (term.GeneralItems != null)
                {
                    term.GeneralItems.ForEach(gi => { if (gi.DueDate.HasValue && gi.DueDate.Value < today && gi.AmountDue.HasValue) { amountOverdue += gi.AmountDue.Value; } else { amountDue += gi.AmountDue.Value; } });
                }
                if (term.InvoiceItems != null)
                {
                    term.InvoiceItems.ForEach(ii => { if (ii.DueDate.HasValue && ii.DueDate.Value < today && ii.AmountDue.HasValue) { amountOverdue += ii.AmountDue.Value; } else { amountDue += ii.AmountDue.Value; } });
                }
                if (term.PaymentPlanItems != null)
                {
                    term.PaymentPlanItems.ForEach(ppi => { if (ppi.DueDate.HasValue && ppi.DueDate.Value < today && ppi.AmountDue.HasValue) { amountOverdue += ppi.AmountDue.Value; } else { amountDue += ppi.AmountDue.Value; } });
                }
            }

            var totalAmountDue = amountDue + amountOverdue;

            Assert.AreEqual(totalAmountDue, studentAccountSummary.TotalAmountDue);
        }

        [TestMethod]
        public void NoDueItems_AmountsDueAreZeroTest()
        {
            testModelData.studentAccountDueData.AccountTerms.ForEach(at =>
            {
                at.GeneralItems = new List<AccountsReceivableDueItem>();
                at.DepositDueItems = new List<Colleague.Dtos.Finance.DepositDue>();
                at.InvoiceItems = new List<InvoiceDueItem>();
                at.PaymentPlanItems = new List<PaymentPlanDueItem>();
            });

            studentAccountSummary = new StudentFinanceAccountSummary(testModelData.studentAccountDueData);
            Assert.IsTrue(studentAccountSummary.TotalAmountDue == 0);
        }

        [TestMethod]
        public void NullDueItemLists_AmountsDueAreZeroTest()
        {
            testModelData.studentAccountDueData.AccountTerms.ForEach(at =>
            {
                at.GeneralItems = null;
                at.DepositDueItems = null;
                at.InvoiceItems = null;
                at.PaymentPlanItems = null;
            });

            studentAccountSummary = new StudentFinanceAccountSummary(testModelData.studentAccountDueData);
            Assert.IsTrue(studentAccountSummary.TotalAmountDue == 0);
        }

        [TestMethod]
        public void NoAmounts_TotalAmountDueIsZeroTest()
        {
            testModelData.studentAccountDueData.AccountTerms.ForEach(at =>
            {
                at.DepositDueItems.ForEach(dd => dd.AmountDue = 0);
                at.GeneralItems.ForEach(gi => gi.AmountDue = null);
                at.InvoiceItems.ForEach(ii => ii.AmountDue = null);
                at.PaymentPlanItems.ForEach(ppi => ppi.AmountDue = null);
            });

            studentAccountSummary = new StudentFinanceAccountSummary(testModelData.studentAccountDueData);
            Assert.IsTrue(studentAccountSummary.TotalAmountDue == 0);
        }

    }
}
