﻿//Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Security;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Home
{
    [TestClass]
    public class HomeViewModelTests
    {
        private TestModelData testModelDataObject;
        private HomeViewModel homeViewModel;
        private ICurrentUser currentUser;
        private bool isUserAdminOnly;

        /// <summary>
        /// Initialize with a set of data 
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            testModelDataObject = new TestModelData("0003914");
            BuildHomeViewModel();
            currentUser = testModelDataObject.currentUser;
            isUserAdminOnly = false;
        }

        /// <summary>
        /// Test if created model and its attributes are not null
        /// </summary>
        [TestMethod]
        public void ModelCreated_InitializedModel()
        {
            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.HelpfulLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
            Assert.IsNotNull(homeViewModel.StudentAccountSummary);
        }

        /// <summary>
        /// Test if a model created with no data received is not null
        /// </summary>
        [TestMethod]
        public void NoData_InitializedModel()
        {
            testModelDataObject.studentAwardYearsData = new List<StudentAwardYear2>();
            testModelDataObject.studentDocumentsData = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
            testModelDataObject.communicationCodesData = new List<Colleague.Dtos.Base.CommunicationCode2>();
            testModelDataObject.studentAwardsData = new List<Colleague.Dtos.FinancialAid.StudentAward>();
            testModelDataObject.awardStatusData = new List<Colleague.Dtos.FinancialAid.AwardStatus>();
            testModelDataObject.studentAwardLettersData = new List<AwardLetter>();
            testModelDataObject.studentFaApplicationsData = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>();
            testModelDataObject.usefulLinksData = new List<Link>();
            testModelDataObject.averageAwardPackageData = new List<AverageAwardPackage>();
            testModelDataObject.studentChecklistsData = new List<StudentFinancialAidChecklist>();
            testModelDataObject.academicProgressEvaluationsData = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            testModelDataObject.academicProgressStatusesData = new List<AcademicProgressStatus>();
            testModelDataObject.financialAidOfficesData = new List<FinancialAidOffice3>();
            testModelDataObject.studentAccountDueData = null;

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNull(homeViewModel.SAPStatus);
            Assert.IsNull(homeViewModel.StudentAccountSummary);
        }

        [TestMethod]
        public void AwardYearsAreSortedTest()
        {
            BuildHomeViewModel();
            Assert.AreEqual(testModelDataObject.studentAwardYearsData.Count(), homeViewModel.AwardYears.Count());

            var sortedTestData = testModelDataObject.studentAwardYearsData.OrderByDescending(y => y.Code).ToArray();
            for (int i = 0; i < sortedTestData.Count(); i++)
            {
                Assert.AreEqual(sortedTestData[i].Code, homeViewModel.AwardYears.ToArray()[i].Code);
            }

            CollectionAssert.AreEqual(sortedTestData.Select(y => y.Code).ToArray(),
                homeViewModel.AwardYears.Select(y => y.Code).ToArray());
        }

        /// <summary>
        /// Test if the model is created along with its attributes and is not null when there is no 
        /// FA Application data received
        /// </summary>
        [TestMethod]
        public void NoFaApplicationData_InitializedModel()
        {
            testModelDataObject.studentFaApplicationsData = new List<Colleague.Dtos.FinancialAid.FinancialAidApplication2>();

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
        }

        /// <summary>
        /// Test if the model is created along with its attributes and is not null when there is no 
        /// student documents data received
        /// </summary>
        [TestMethod]
        public void NoDocumentsData_InitializedModel()
        {
            testModelDataObject.studentDocumentsData = new List<Colleague.Dtos.FinancialAid.StudentDocument>();
            //documentsData = new List<Colleague.Dtos.FinancialAid.Document>();

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
        }

        /// <summary>
        /// Test if the model is created along with its attributes and is not null when there is no 
        /// student awards data received
        /// </summary>
        [TestMethod]
        public void NoAwardsData_InitializedModel()
        {
            testModelDataObject.studentAwardsData = new List<Colleague.Dtos.FinancialAid.StudentAward>();
            //awardStatusesData = new List<Colleague.Dtos.FinancialAid.AwardStatus>();

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
        }

        /// <summary>
        /// Test if the model is created along with its attributes and is not null when there is no 
        /// student award letters data received
        /// </summary>
        [TestMethod]
        public void NoAwardLettersData_InitializedModel()
        {
            testModelDataObject.studentAwardLettersData = new List<AwardLetter>();

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if the model is created and is not null when no links are passed
        /// </summary>
        [TestMethod]
        public void NoLinksData_InitializedModel()
        {
            testModelDataObject.usefulLinksData = new List<Link>();

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if the model is created and is not null when no average award packages are 
        /// passed
        /// </summary>
        [TestMethod]
        public void NoAverageAwardPackageData_InitializedModel()
        {
            testModelDataObject.averageAwardPackageData = new List<AverageAwardPackage>();

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);

        }

        /// <summary>
        /// Tests if the model is created and is not null when no studentChecklists are 
        /// passed, also makes sure that the number of created checklist items is zero
        /// </summary>
        [TestMethod]
        public void NoStudentChecklistsData_InitializedModel()
        {
            testModelDataObject.studentChecklistsData = new List<StudentFinancialAidChecklist>();

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
            Assert.IsTrue(homeViewModel.StudentChecklists.Count == 0);


        }

        /// <summary>
        /// Tests if the model is created and is not null when no financialAidChecklistItems
        /// are passed, also makes sure that the number of created checklist items is zero
        /// </summary>
        [TestMethod]
        public void NoFinancialAidChecklistItemsData_InitializedModel()
        {
            testModelDataObject.financialAidChecklistItemsData = null;

            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNotNull(homeViewModel.SAPStatus);
            Assert.IsTrue(homeViewModel.StudentChecklists.Count == 0);
        }

        /// <summary>
        /// Tests if the model is created and is not null when no academicProgressEvaluationsData is passed,
        /// makes sure that no SAPStatus object was created
        /// </summary>
        [TestMethod]
        public void NoAcademicProgressEvaluationsData_InitializedModel()
        {
            testModelDataObject.academicProgressEvaluationsData = new List<Ellucian.Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNull(homeViewModel.SAPStatus);

        }

        /// <summary>
        /// Tests if the model is created and is not null when no academicProgressStatusesData is passed,
        /// makes sure that no SAPStatus object was created
        /// </summary>
        [TestMethod]
        public void NoAcademicProgressStatusesData_InitializedModel()
        {
            testModelDataObject.academicProgressStatusesData = new List<AcademicProgressStatus>();
            BuildHomeViewModel();

            Assert.IsNotNull(homeViewModel);
            Assert.IsNotNull(homeViewModel.AwardYears);
            Assert.IsNotNull(homeViewModel.StudentChecklists);
            Assert.IsNotNull(homeViewModel.FormLinks);
            Assert.IsNotNull(homeViewModel.AverageAwardPackages);
            Assert.IsNull(homeViewModel.SAPStatus);

        }
        
        /// <summary>
        /// Test if a checklist is created for each FA year on student's record:
        /// Counts of FA Award years and checklists' years should be equal
        /// </summary>
        [TestMethod]
        public void ChecklistExistsForEachFAYear_HomeViewModel()
        {
            var awardYearsCount = homeViewModel.AwardYears.Count();
            var studentChecklistYearsCount = homeViewModel.StudentChecklists.Count();

            Assert.AreEqual(awardYearsCount, studentChecklistYearsCount);
        }

        [TestMethod]
        public void CounselorCreatedForEachYear_HomeViewModel()
        {
            homeViewModel.AwardYears.ToList().ForEach(y => Assert.IsNotNull(y.Counselor));
        }

        [TestMethod]
        public void CounselorNotCreated_NoExceptions_HomeViewModel()
        {
            testModelDataObject.financialAidCounselorData.Name = string.Empty;
            BuildHomeViewModel();

            homeViewModel.AwardYears.ToList().ForEach(y => Assert.IsNull(y.Counselor));
        }

        /// <summary>
        /// Test if all checklists' years are the same as all FA years:
        /// iterate through all FA years and see if there is a match for each among the checklists' years
        /// </summary>
        [TestMethod]
        public void ChecklistYearsSameAsFAYears_HomeViewModel()
        {
            foreach (var year in homeViewModel.AwardYears)
            {
                Assert.IsNotNull(homeViewModel.StudentChecklists.First(scl => scl.AwardYearCode == year.Code));
            }
        }

        /// <summary>
        /// Test if attributes get set and are not null
        /// </summary>
        [TestMethod]
        public void EachChecklistItem_AttributesTest()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                foreach (var item in checklist.ChecklistItems)
                {
                    Assert.IsNotNull(item.Type);
                    Assert.IsNotNull(item.IsActiveItem);
                    Assert.IsNotNull(item.Status);
                }
            }
        }

        /// <summary>
        /// Tests if profile checklist item is created when IsProfileActive field
        /// of student's FA Applications is set to true
        /// </summary>
        [TestMethod]
        public void IsProfileActive_ProfileChecklistItem()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var studentAwardYear = testModelDataObject.studentAwardYearsData.First(y => y.Code == checklist.AwardYearCode);
                var configurationForYear = testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYear.FinancialAidOfficeId && c.AwardYear == studentAwardYear.Code);

                if (configurationForYear.IsProfileActive)
                {
                    Assert.IsNotNull(checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Profile));
                }
            }
        }

        /// <summary>
        /// Tests if profile checklist item is not created when student has none assigned
        /// </summary>
        [TestMethod]
        public void NoProfileChecklistItem_NoProfileChecklistAddedItem()
        {
            testModelDataObject.studentChecklistsData.ForEach(c => c.ChecklistItems.Remove(c.ChecklistItems.First(i => i.Code == "PROFILE")));
            BuildHomeViewModel();
            foreach (var checklist in homeViewModel.StudentChecklists)
            {                
                Assert.IsNull(checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Profile));
            }
        }

        /// <summary>
        /// Test if there is no profile checklist item created when the corresponding field
        /// of FA Applications is set to false
        /// </summary>
        [TestMethod]
        public void NotIsProfileActive_NoProfileChecklistItem()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var studentAwardYear = testModelDataObject.studentAwardYearsData.First(y => y.Code == checklist.AwardYearCode);
                var configurationForYear = testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).FirstOrDefault(c => c.OfficeId == studentAwardYear.FinancialAidOfficeId && c.AwardYear == studentAwardYear.Code);
                if (!configurationForYear.IsProfileActive)
                {
                    Assert.IsNull(checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Profile));
                }
            }
        }

        /// <summary>
        /// Test if Fafsa item's status is available if there is no profile requirement
        /// </summary>
        [TestMethod]
        public void NoProfile_FafsaAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var profile = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Profile);
                if (profile == null)
                {
                    var fafsa = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.FAFSA);
                    Assert.IsTrue(fafsa.Status.ToString() != "NotAvailable");
                }
            }
        }

        /// <summary>
        /// Test if Fafsa item's status is available if profile item's status is complete
        /// </summary>
        [TestMethod]
        public void ProfileComplete_FafsaAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var profile = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Profile);
                if (profile != null)
                {
                    if (profile.Status.ToString() == "Complete")
                    {
                        var fafsa = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.FAFSA);
                        Assert.IsTrue(fafsa.Status.ToString() != "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if Fafsa item's status is not available if profile is incomplete
        /// </summary>
        [TestMethod]
        public void ProfileIncomplete_FafsaNotAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var profile = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Profile);
                if (profile != null)
                {
                    if (profile.Status.ToString() == "Incomplete")
                    {
                        var fafsa = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.FAFSA);
                        Assert.IsTrue(fafsa.Status.ToString() == "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if Fafsa item is not created if not assigned to student
        /// </summary>
        [TestMethod]
        public void NoFafsaItem_NoFafsaCreatedTest()
        {
            testModelDataObject.studentChecklistsData.ForEach(c => c.ChecklistItems.Remove(c.ChecklistItems.First(i => i.Code == "FAFSA")));
            BuildHomeViewModel();
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                Assert.IsNull(checklist.ChecklistItems.FirstOrDefault(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.FAFSA));
            }
        }

        /// <summary>
        /// Tests if documents item's status is available if Fafsa is complete
        /// </summary>
        [TestMethod]
        public void FafsaComplete_DocumentsItemAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var fafsa = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.FAFSA);
                if (fafsa != null)
                {
                    if (fafsa.Status.ToString() == "Complete")
                    {
                        var documents = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Documents);
                        Assert.IsTrue(documents.Status.ToString() != "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if documents item's status is not available if Fafsa is incomplete
        /// </summary>
        [TestMethod]
        public void FafsaIncomplete_DocumentsItemNotAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var fafsa = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.FAFSA);
                if (fafsa != null)
                {
                    if (fafsa.Status.ToString() == "Incomplete")
                    {
                        var documents = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Documents);
                        Assert.IsTrue(documents.Status.ToString() == "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if fa Application item's status is available if Documents are complete
        /// </summary>
        [TestMethod]
        public void DocumentsItemComplete_FaApplicationsItemAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var documents = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Documents);
                if (documents != null)
                {
                    if (documents.Status.ToString() == "Complete")
                    {
                        var faApplication = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Application);
                        Assert.IsTrue(faApplication.Status.ToString() != "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if fa Application item's status is not available if Documents are incomplete
        /// </summary>
        [TestMethod]
        public void DocumentsItemIncomplete_FaApplicationsItemNotAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var documents = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Documents);
                if (documents != null)
                {
                    if (documents.Status.ToString() == "Incomplete")
                    {
                        var faApplication = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Application);
                        Assert.IsTrue(faApplication.Status.ToString() == "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if no documents checklist item was created if none was assigned to the student
        /// </summary>
        [TestMethod]
        public void NoDocumentItemAssigned_NoDocumentItemCreatedTest()
        {
            testModelDataObject.studentChecklistsData.ForEach(c => c.ChecklistItems.Remove(c.ChecklistItems.First(i => i.Code == "CMPLREQDOC")));
            BuildHomeViewModel();
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                Assert.IsNull(checklist.ChecklistItems.FirstOrDefault(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Documents));
            }
        }

        /// <summary>
        /// Tests if awards item's status is available if fa Application is complete
        /// </summary>
        [TestMethod]
        public void FaApplicationItemComplete_AwardsItemAvailable()
        {
            testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).ToList().ForEach(c => c.IsAwardingActive = true);
            BuildHomeViewModel();

            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var faApplication = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Application);
                if (faApplication != null)
                {
                    if (faApplication.Status.ToString() == "Complete")
                    {
                        var awardsItem = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Awards);
                        Assert.IsTrue(awardsItem.Status.ToString() != "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if awards item's status is not available if fa Application is incomplete
        /// </summary>
        [TestMethod]
        public void FaApplicationItemIncomplete_AwardsItemNotAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var faApplication = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Application);
                if (faApplication != null)
                {
                    if (faApplication.Status.ToString() == "Incomplete")
                    {
                        var awardsItem = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Awards);
                        Assert.IsTrue(awardsItem.Status.ToString() == "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if award letter item's status is not available if fa application item is incomplete
        /// </summary>
        [TestMethod]
        public void FaApplicationItemIncomplete_AwardLetterItemNotAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var faApplication = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Application);
                var awardLetterForYear = testModelDataObject.studentAwardLettersData.FirstOrDefault(sl => sl.AwardYearCode == checklist.AwardYearCode);
                if (faApplication != null)
                {
                    if ((faApplication.Status.ToString() == "Incomplete") && (awardLetterForYear != null))
                    {
                        var awardLetterItem = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.AwardLetter);
                        Assert.IsTrue(awardLetterItem.Status.ToString() == "NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if award letter item's status is not complete if fa application item is complete but 
        /// there is no award letter for the year
        /// </summary>
        [TestMethod]
        public void FaApplicationItemCompleteNoAwardLetterForYear_AwardLetterItemNotAvailable()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var faApplication = checklist.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Application);
                var awardLetterForYear = testModelDataObject.studentAwardLettersData.FirstOrDefault(sl => sl.AwardYearCode == checklist.AwardYearCode);
                if (faApplication != null)
                {
                    if ((faApplication.Status.ToString() == "Complete") && (awardLetterForYear == null))
                    {
                        var awardLetterItem = checklist.ChecklistItems.First(ch => ch.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.AwardLetter);
                        Assert.IsTrue(awardLetterItem.Status.ToString() == "Incomplete");
                    }
                }
            }
        }

        /// <summary>
        /// Tests if no fa application item is created if none assigned to the student
        /// </summary>
        [TestMethod]
        public void NoFaApplicationItemAssigned_NoFaApplicationItemCreatedTest()
        {
            testModelDataObject.studentChecklistsData.ForEach(c => c.ChecklistItems.Remove(c.ChecklistItems.First(i => i.Code == "APPLRVW")));
            BuildHomeViewModel();
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                Assert.IsNull(checklist.ChecklistItems.FirstOrDefault(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Application));
            }
        }

        /// <summary>
        /// Tests if no award package checklist item is created when none assigned
        /// </summary>
        [TestMethod]
        public void NoAwardPackageItemAssigned_NoAwardPackageItemCreatedTest()
        {
            testModelDataObject.studentChecklistsData.ForEach(c => c.ChecklistItems.Remove(c.ChecklistItems.First(i => i.Code == "ACCAWDPKG")));
            BuildHomeViewModel();
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                Assert.IsNull(checklist.ChecklistItems.FirstOrDefault(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Awards));
            }
        }

        /// <summary>
        /// Tests if no award letter checklist item is created when none assigned
        /// </summary>
        [TestMethod]
        public void NoAwardLetterItemAssigned_NoAwardLetterItemCreatedTest()
        {
            testModelDataObject.studentChecklistsData.ForEach(c => c.ChecklistItems.Remove(c.ChecklistItems.First(i => i.Code == "SIGNAWDLTR")));
            BuildHomeViewModel();
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                Assert.IsNull(checklist.ChecklistItems.FirstOrDefault(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.AwardLetter));
            }
        }

        //TODO mcd: Write unit tests for entrance interview checklist items

        /// <summary>
        /// Test if the direct loan MPN checklist item is created when there are
        /// any sub/unsub loans on student's record for that year
        /// As data is currently set up, award year 2014 has UNSUBDL, 2013: GPLUS and ZEBRA
        /// Thre should be DirectLoanMpnChecklistItem created for 2014
        /// </summary>
        [TestMethod]
        public void DirectLoanMpnItemCreated_HomeViewModel()
        {
            testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).ToList().ForEach(c => c.IsAwardingActive = true);
            BuildHomeViewModel();

            foreach (var studentAward in testModelDataObject.studentAwardsData)
            {
                var awardType = testModelDataObject.awardsData.FirstOrDefault(a => a.Code == studentAward.AwardId).LoanType;
                if ((awardType == LoanType.SubsidizedLoan) || (awardType == LoanType.UnsubsidizedLoan))
                {
                    var year = studentAward.AwardYearId;
                    var checklistForYear = homeViewModel.StudentChecklists.FirstOrDefault(cl => cl.AwardYearCode == year);
                    Assert.IsNotNull(checklistForYear.ChecklistItems.FirstOrDefault(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.MPN));
                }
            }
        }

        [TestMethod]
        public void DirectLoanMpnItemNotCreatedBasedOnConfiguration_HomeViewModel()
        {
            testModelDataObject.financialAidOfficesData.SelectMany(o => o.Configurations).ToList().ForEach(c => c.IsAwardingActive = false);
            BuildHomeViewModel();

            foreach (var studentAward in testModelDataObject.studentAwardsData)
            {
                var awardType = testModelDataObject.awardsData.FirstOrDefault(a => a.Code == studentAward.AwardId).LoanType;
                if ((awardType == LoanType.SubsidizedLoan) || (awardType == LoanType.UnsubsidizedLoan))
                {
                    var year = studentAward.AwardYearId;
                    var checklistForYear = homeViewModel.StudentChecklists.FirstOrDefault(cl => cl.AwardYearCode == year);
                    Assert.IsNull(checklistForYear.ChecklistItems.FirstOrDefault(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.MPN));
                }
            }
        }

        /// <summary>
        /// Test if IsActiveItem flag is set appropriately: will be true only if item's
        /// status is incomplete or "in progress" (may change)
        /// </summary>
        [TestMethod]
        public void IsActiveOrNotIsActive_ChecklistItems()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                foreach (var item in checklist.ChecklistItems)
                {
                    if ((item.Status == ChecklistItemStatus.Incomplete) || (item.Status == ChecklistItemStatus.InProgress))
                    {
                        Assert.IsTrue(item.IsActiveItem);
                    }
                    else
                    {
                        Assert.IsTrue(!item.IsActiveItem);
                    }
                }
            }
        }

        /// <summary>
        /// Tests if Active checklist item attribute of a checklist is set correctly:
        ///     type of the first item in the list (there may be few) which IsActive 
        ///     should match the ActiveChecklistItem's one
        /// </summary>
        [TestMethod]
        public void ActiveChecklistItem_HomeViewModel()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var activeItem = checklist.ChecklistItems.FirstOrDefault(i => i.IsActiveItem);
                Assert.AreEqual(activeItem.Type, checklist.ActiveChecklistItem.Type);
            }
        }

        /// <summary>
        /// Tests if MPN expiration data is set based on the MPN status
        /// </summary>
        [TestMethod]
        public void MPNChecklistItem_ExpirationDateIsSet()
        {
            foreach (var checklist in homeViewModel.StudentChecklists)
            {
                var mpns = checklist.ChecklistItems.FindAll(i => i.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.MPN);
                foreach (var mpn in mpns)
                {
                    if (mpn.Status == ChecklistItemStatus.Complete)
                    {
                        Assert.IsNotNull(mpn.ExpirationDate);
                    }
                    else
                    {
                        Assert.IsNull(mpn.ExpirationDate);
                    }
                }

            }
        }
        
        /// <summary>
        /// Tests if the number of the links added to the Links attribute of the homeViewModel
        /// equals the number of links passed
        /// </summary>
        [TestMethod]
        public void NumberOfLinksInModel_UsefulLinksReceived()
        {
            var passedLinksCount = testModelDataObject.usefulLinksData.Where(l => l.LinkType != LinkTypes.SatisfactoryAcademicProgress).Count();
            var createdLinksCount = homeViewModel.FormLinks.Count() + homeViewModel.HelpfulLinks.Count();
            Assert.AreEqual(passedLinksCount, createdLinksCount);
        }

        /// <summary>
        /// Tests if the profile information created matches with one received from usefulLinksData
        /// </summary>
        [TestMethod]
        public void ProfileInfoMatches_PassedUsefulLinks()
        {
            var profileInfo = testModelDataObject.usefulLinksData.FirstOrDefault(l => l.LinkType == LinkTypes.PROFILE);
            foreach (var year in homeViewModel.StudentChecklists)
            {
                var profileChecklistItem = year.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Profile);
                if ((profileInfo != null) && (profileChecklistItem != null))
                {
                    Assert.AreEqual(profileInfo.LinkUrl, profileChecklistItem.Link);
                    Assert.AreEqual(profileInfo.Title, profileChecklistItem.Title);
                }
            }
        }

        /// <summary>
        /// Tests if the fafsa information created matches with one received from usefulLinksData
        /// </summary>
        [TestMethod]
        public void FafsaInfoMatches_PassedUsefulLinks()
        {
            var fafsaInfo = testModelDataObject.usefulLinksData.FirstOrDefault(l => l.LinkType == LinkTypes.FAFSA);
            foreach (var year in homeViewModel.StudentChecklists)
            {
                var fafsaChecklistItem = year.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.FAFSA);
                if ((fafsaInfo != null) && (fafsaChecklistItem != null))
                {
                    Assert.AreEqual(fafsaInfo.LinkUrl, fafsaChecklistItem.Link);
                    Assert.AreEqual(fafsaInfo.Title, fafsaChecklistItem.Title);
                }
            }
        }

        /// <summary>
        /// Tests if the direct loan entrance interview information created matches with one 
        /// received from usefulLinksData
        /// </summary>
        [TestMethod]
        public void DLEntranceInterviewInfoMatches_PassedUsefulLinks()
        {
            var dlEntranceInterviewInfo = testModelDataObject.usefulLinksData.FirstOrDefault(l => l.LinkType == LinkTypes.EntranceInterview);
            foreach (var year in homeViewModel.StudentChecklists)
            {
                //It will appear first in the list of interviews - so we should be ok
                var dlEntranceInterviewChecklistItem = year.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Interview);
                if ((dlEntranceInterviewInfo != null) && (dlEntranceInterviewChecklistItem != null))
                {
                    Assert.AreEqual(dlEntranceInterviewInfo.LinkUrl, dlEntranceInterviewChecklistItem.Link);
                    Assert.AreEqual(dlEntranceInterviewInfo.Title, dlEntranceInterviewChecklistItem.Title);
                }
            }
        }

        /// <summary>
        /// Tests if the direct loan mpn information created matches with one 
        /// received from usefulLinksData
        /// </summary>
        [TestMethod]
        public void DLMPNInfoMatches_PassedUsefulLinks()
        {
            var dlMPNInfo = testModelDataObject.usefulLinksData.FirstOrDefault(l => l.LinkType == LinkTypes.EntranceInterview);
            foreach (var year in homeViewModel.StudentChecklists)
            {
                //It will appear first in the list of MPNs - so we should be ok
                var dlMPNChecklistItem = year.ChecklistItems.FirstOrDefault(cl => cl.Type == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.MPN);
                if ((dlMPNInfo != null) && (dlMPNChecklistItem != null))
                {
                    Assert.AreEqual(dlMPNInfo.LinkUrl, dlMPNChecklistItem.Link);
                    Assert.AreEqual(dlMPNInfo.Title, dlMPNChecklistItem.Title);
                }
            }
        }

        /// <summary>
        /// Tests if no links of SAP type were included in the Helpful links list
        /// </summary>
        [TestMethod]
        public void NoSAPLinksIncluded_HelpfulLinksTest()
        {
            foreach (var link in homeViewModel.HelpfulLinks)
            {
                Assert.IsTrue(link.LinkType != LinkTypes.SatisfactoryAcademicProgress);
            }
        }

        /// <summary>
        /// Tests if the number of created award packages equals the number of received 
        /// aap dtos given each dto has at least one amount set to > 0 and awardYearCode is
        /// not null or empty
        /// </summary>
        [TestMethod]
        public void AverageAwardPackageCount_EqualsAverageAwardPackageDtosCount()
        {
            var expectedAapCount = testModelDataObject.averageAwardPackageData.Count();
            var actualAapCount = homeViewModel.AverageAwardPackages.Count();

            Assert.AreEqual(expectedAapCount, actualAapCount);
        }

        /// <summary>
        /// Tests if the count of average award packages does not equal the count of passed
        /// dtos if one of the dtos does not have its award year set
        /// </summary>
        [TestMethod]
        public void AverageAwardPackageCount_NotEqualsAverageAwardPackageDtosCount()
        {
            testModelDataObject.averageAwardPackageData.First().AwardYearCode = string.Empty;

            BuildHomeViewModel();

            var expectedAapCount = testModelDataObject.averageAwardPackageData.Count();
            var actualAapCount = homeViewModel.AverageAwardPackages.Count();

            Assert.AreNotEqual(expectedAapCount, actualAapCount);

        }

        /// <summary>
        /// Tests if SAP Status object was not created if there were no award years
        /// </summary>
        [TestMethod]
        public void NoAwardYearsData_SAPStatusIsNullTest()
        {
            testModelDataObject.studentAwardYearsData = new List<StudentAwardYear2>();
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if SAP Status object was not created if there is no FA office data
        /// </summary>
        [TestMethod]
        public void NoFinancialAidOfficeData_SAPStatusIsNullTest()
        {
            testModelDataObject.financialAidOfficesData = new List<FinancialAidOffice3>();
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.SAPStatus);
        }

        #region SAPStatus tests
        /// <summary>
        /// Tests if SAP Status object was not created if SAP flag is set to false
        /// </summary>
        [TestMethod]
        public void SAPStatusIsNull_WhenSAPFlagIsFalseTest()
        {
            var mostRecentYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentYear.FinancialAidOfficeId).AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = false;
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.SAPStatus);
        }

        /// <summary>
        /// Tests if SAP Status was created when SAP flag is set to true
        /// and no specific display types are specified
        /// </summary>
        [TestMethod]
        public void SAPStatusIsNotNull_WhenSAPFlagIsTrueTest()
        {
            var mostRecentYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var academicProgressConfiguration = testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentYear.FinancialAidOfficeId).AcademicProgressConfiguration;
            academicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            academicProgressConfiguration.AcademicProgressTypesToDisplay = new List<string>();
            BuildHomeViewModel();
            Assert.IsNotNull(homeViewModel.SAPStatus);
        }

        [TestMethod]
        public void NoAcademicProgressEvaluations_SAPStatusIsNullTest()
        {
            var mostRecentYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentYear.FinancialAidOfficeId).AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            testModelDataObject.academicProgressEvaluationsData = new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>();
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.SAPStatus);
        }

        [TestMethod]
        public void NullAcademicProgressEvaluations_SAPStatusIsNullTest()
        {
            var mostRecentYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentYear.FinancialAidOfficeId).AcademicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            testModelDataObject.academicProgressEvaluationsData = null;
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.SAPStatus);
        }

        [TestMethod]
        public void NoMatchingAcademicProgressEvalType_SAPStatusIsNullTest()
        {
            var mostRecentYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var academicProgressConfiguration = testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentYear.FinancialAidOfficeId).AcademicProgressConfiguration;
            academicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;
            academicProgressConfiguration.AcademicProgressTypesToDisplay = new List<string> { "not a matching type" };
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.SAPStatus);
        }

        [TestMethod]
        public void MatchingAcademicProgressEvalType_SAPStatusNotNullTest()
        {
            var mostRecentYear = testModelDataObject.studentAwardYearsData.OrderByDescending(say => say.Code).First();
            var academicProgressConfiguration = testModelDataObject.financialAidOfficesData.First(fao => fao.Id == mostRecentYear.FinancialAidOfficeId).AcademicProgressConfiguration;
            academicProgressConfiguration.IsSatisfactoryAcademicProgressActive = true;

            var existingType = testModelDataObject.academicProgressEvaluationsData.Last().AcademicProgressTypeCode;
            academicProgressConfiguration.AcademicProgressTypesToDisplay = new List<string> { existingType };
            BuildHomeViewModel();
            Assert.IsNotNull(homeViewModel.SAPStatus);
        }

        #endregion

        [TestMethod]
        public void NotIsProxyView_WhenNullCurrentUserTest()
        {
            testModelDataObject.currentUser = null;
            BuildHomeViewModel();
            Assert.IsFalse(homeViewModel.IsProxyView);
        }

        [TestMethod]
        public void NotIsProxyView_WhenNoProxySubjectsTest()
        {
            Assert.IsFalse(homeViewModel.IsProxyView);
        }

        [TestMethod]
        public void IsProxyViewIsTrueTest()
        {
            currentUser = testModelDataObject.currentUserWithProxy;
            BuildHomeViewModel();
            Assert.IsTrue(homeViewModel.IsProxyView);
        }

        [TestMethod]
        public void NotIsReadOnly_WhenNotIsProxyViewTest()
        {
            foreach (var item in homeViewModel.StudentChecklists.First().ChecklistItems)
            {
                Assert.IsFalse(item.IsReadOnly);
            }
        }

        [TestMethod]
        public void RequiredDocumentsIsReadOnly_WhenIsProxyViewTest()
        {
            currentUser = testModelDataObject.currentUserWithProxy;
            BuildHomeViewModel();
            Assert.IsTrue(homeViewModel.StudentChecklists.First().ChecklistItems
                .First(i => i.Type.Value == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Documents).IsReadOnly);
        }

        [TestMethod]
        public void AwardPackageIsReadOnly_WhenIsProxyViewTest()
        {
            currentUser = testModelDataObject.currentUserWithProxy;
            BuildHomeViewModel();
            Assert.IsTrue(homeViewModel.StudentChecklists.First().ChecklistItems
                .First(i => i.Type.Value == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.Awards).IsReadOnly);
        }

        [TestMethod]
        public void AwardLetterIsReadOnly_WhenIsProxyViewTest()
        {
            currentUser = testModelDataObject.currentUserWithProxy;
            BuildHomeViewModel();
            Assert.IsTrue(homeViewModel.StudentChecklists.First().ChecklistItems
                .First(i => i.Type.Value == Ellucian.Web.Student.Areas.FinancialAid.Models.ChecklistItemType.AwardLetter).IsReadOnly);
        }

        [TestMethod]
        public void NoStudentLoanSummaryDto_StudentLoansSummaryInitializedTest()
        {
            testModelDataObject.studentLoanSummaryData = null;
            BuildHomeViewModel();
            Assert.IsNotNull(homeViewModel.StudentLoansSummary);
            Assert.IsNotNull(homeViewModel.StudentLoansSummary.LoanHistory);
        }

        [TestMethod]
        public void StudentLoanSummaryDto_StudentLoansSummaryInitializedTest()
        {
            Assert.IsNotNull(homeViewModel.StudentLoansSummary);
            Assert.IsNotNull(homeViewModel.StudentLoansSummary.LoanHistory);
            Assert.IsTrue(homeViewModel.StudentLoansSummary.LoanHistory.Any());
        }

        [TestMethod]
        public void NoStudentNsldsInformationDto_PellLifetimeEligibilityUsedPercentageIsnullTest()
        {
            testModelDataObject.studentNsldsData = null;
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.PellLifetimeEligibilityUsedPercentage);
        }

        [TestMethod]
        public void PellLifetimeEligibilityUsedPercentage_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentNsldsData.PellLifetimeEligibilityUsedPercentage, homeViewModel.PellLifetimeEligibilityUsedPercentage);
        }

        [TestMethod]
        public void IsAdminView_ReturnsFalseTest()
        {
            Assert.IsFalse(homeViewModel.IsAdminView);
        }

        [TestMethod]
        public void IsAdminView_ReturnsTrueTest()
        {
            isUserAdminOnly = true;
            BuildHomeViewModel();
            Assert.IsTrue(homeViewModel.IsAdminView);
        }

        [TestMethod]
        public void NoPersonData_PersonIdIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.PersonId);
        }

        [TestMethod]
        public void PersonId_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.Id, homeViewModel.PersonId);
        }

        [TestMethod]
        public void NoPersonData_PersonNameIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.PersonName);
        }

        [TestMethod]
        public void PersonName_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PreferredName, homeViewModel.PersonName);
        }

        [TestMethod]
        public void NoPersonData_PrivacyStatusCodeIsnullTest()
        {
            testModelDataObject.studentData = null;
            BuildHomeViewModel();
            Assert.IsNull(homeViewModel.PrivacyStatusCode);
        }

        [TestMethod]
        public void PrivacyStatusCode_EqualsExpectedTest()
        {
            Assert.AreEqual(testModelDataObject.studentData.PrivacyStatusCode, homeViewModel.PrivacyStatusCode);
        }

        private void BuildHomeViewModel()
        {
            homeViewModel = new HomeViewModel(
                testModelDataObject.studentData,
                testModelDataObject.studentAwardYearsData,
                testModelDataObject.studentDocumentsData,
                testModelDataObject.communicationCodesData,
                testModelDataObject.officeCodesData,
                testModelDataObject.studentAwardsData,
                testModelDataObject.awardsData,
                testModelDataObject.awardStatusData,
                testModelDataObject.awardLetterHistoryRecordsData,
                testModelDataObject.studentFaApplicationsData,
                testModelDataObject.studentLoanSummaryData,
                testModelDataObject.ipedsInstitutionData,
                testModelDataObject.averageAwardPackageData,
                testModelDataObject.usefulLinksData,
                testModelDataObject.financialAidCounselorData,
                testModelDataObject.financialAidOfficesData,
                testModelDataObject.studentChecklistsData,
                testModelDataObject.financialAidChecklistItemsData,
                testModelDataObject.counselorsPhoneNumbersData.First(p => p.PersonId == testModelDataObject.financialAidCounselorData.Id),
                testModelDataObject.academicProgressEvaluationsData,
                testModelDataObject.academicProgressStatusesData,
                testModelDataObject.studentNsldsData,
                testModelDataObject.studentAccountDueData,
                currentUser, isUserAdminOnly);
        }
    }
}
