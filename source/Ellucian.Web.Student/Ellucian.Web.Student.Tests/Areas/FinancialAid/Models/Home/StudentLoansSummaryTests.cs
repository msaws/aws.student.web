﻿using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Models.Home
{
    [TestClass]
    public class StudentLoansSummaryTests
    {
        private StudentLoanSummary studentLoanSummaryDto;
        private List<IpedsInstitution> ipedsInstitutions;
        private TestModelData testModelData;

        private StudentLoansSummary studentLoanSummary;

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData("0004791");
            studentLoanSummaryDto = testModelData.studentLoanSummaryData;
            ipedsInstitutions = testModelData.ipedsInstitutionData;
        }

        [TestCleanup]
        public void Cleanup()
        {
            testModelData = null;
            studentLoanSummaryDto = null;
            ipedsInstitutions = null;
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            studentLoanSummary = new StudentLoansSummary();
            Assert.IsNotNull(studentLoanSummary);
            Assert.IsNotNull(studentLoanSummary.LoanHistory);
        }

        [TestMethod]
        public void NullStudentLoanSummaryDto_PropertiesNotSetTest()
        {
            studentLoanSummary = new StudentLoansSummary(null, ipedsInstitutions);
            Assert.IsNotNull(studentLoanSummary.LoanHistory);
            Assert.IsTrue(studentLoanSummary.TotalLoanAmountBorrowed == 0);
            Assert.IsTrue(studentLoanSummary.AggregateTotalLoanAmount == 0);
            Assert.IsTrue(studentLoanSummary.OtherLoansAmount == 0);
        }

        [TestMethod]
        public void AggregateTotalLoanAmountGetSetTest()
        {
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            Assert.AreEqual(studentLoanSummaryDto.StudentLoanCombinedTotalAmount, studentLoanSummary.AggregateTotalLoanAmount);
        }

        [TestMethod]
        public void NullStudentLoanHistory_NoLoanDetailsCreatedTest()
        {
            studentLoanSummaryDto.StudentLoanHistory = null;
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            Assert.IsFalse(studentLoanSummary.LoanHistory.Any());
        }

        [TestMethod]
        public void EmptyStudentLoanHistory_NoLoanDetailsCreatedTest()
        {
            studentLoanSummaryDto.StudentLoanHistory = new List<StudentLoanHistory>();
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            Assert.IsFalse(studentLoanSummary.LoanHistory.Any());
        }

        [TestMethod]
        public void LoanDetailsCount_EqualsExpectedTest()
        {
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            Assert.AreEqual(studentLoanSummaryDto.StudentLoanHistory.Count, studentLoanSummary.LoanHistory.Count);
        }

        [TestMethod]
        public void NoIpedsInstitutionDtos_NoExceptionThrownTest()
        {
            bool exceptionThrown = false;
            try
            {
                new StudentLoansSummary(studentLoanSummaryDto, null);
            }
            catch (Exception) { exceptionThrown = true; }
            Assert.IsFalse(exceptionThrown);
        } 
       
        [TestMethod]
        public void NoLoanHistory_TotalLoanAmountBorrowedIsZeroTest()
        {
            studentLoanSummaryDto.StudentLoanHistory = new List<StudentLoanHistory>();
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            Assert.IsTrue(studentLoanSummary.TotalLoanAmountBorrowed == 0);
        }

        [TestMethod]
        public void NullLoanHistory_TotalLoanAmountBorrowedIsZeroTest()
        {
            studentLoanSummaryDto.StudentLoanHistory = null;
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            Assert.IsTrue(studentLoanSummary.TotalLoanAmountBorrowed == 0);
        }

        [TestMethod]
        public void ZeroAggregateLoanAmount_OtherLoansAmountIsZeroTest()
        {
            studentLoanSummaryDto.StudentLoanCombinedTotalAmount = 0;
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            Assert.IsTrue(studentLoanSummary.OtherLoansAmount == 0);
        }

        [TestMethod]
        public void OtherLoansAmount_EqualsExpectedTest()
        {
            studentLoanSummary = new StudentLoansSummary(studentLoanSummaryDto, ipedsInstitutions);
            int difference = studentLoanSummary.AggregateTotalLoanAmount - studentLoanSummary.TotalLoanAmountBorrowed;
            Assert.AreEqual(difference, studentLoanSummary.OtherLoansAmount);
        }
    }
}
