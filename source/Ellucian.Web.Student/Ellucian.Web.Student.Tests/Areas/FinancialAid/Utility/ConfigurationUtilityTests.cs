﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Tests.Areas.FinancialAid.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Utility
{
    [TestClass]
    public class ConfigurationUtilityTests
    {
        public TestModelData testModelData;

        [TestClass]
        public class GetActiveStudentAwardYearDtosTests : ConfigurationUtilityTests
        {
            public IEnumerable<StudentAwardYear2> studentAwardYearDtos;

            public IEnumerable<FinancialAidConfiguration3> configurationDtos
            { get { return testModelData.financialAidOfficesData.SelectMany(o => o.Configurations); } }

            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");

                studentAwardYearDtos = testModelData.studentAwardYearsData;
            }

            [TestMethod]
            public void NullStudentAwardYearsTest()
            {
                studentAwardYearDtos = null;
                var activeYears = studentAwardYearDtos.GetActiveStudentAwardYearDtos(configurationDtos);
                Assert.IsNotNull(activeYears);
                Assert.AreEqual(0, activeYears.Count());
            }

            [TestMethod]
            public void EmptyStudentAwardYearsTest()
            {
                studentAwardYearDtos = new List<StudentAwardYear2>();
                var activeYears = studentAwardYearDtos.GetActiveStudentAwardYearDtos(configurationDtos);
                Assert.IsNotNull(activeYears);
                Assert.AreEqual(0, activeYears.Count());
            }

            [TestMethod]
            public void NullConfigurationsTest()
            {
                var activeYears = studentAwardYearDtos.GetActiveStudentAwardYearDtos(null);
                Assert.IsNotNull(activeYears);
                Assert.AreEqual(0, activeYears.Count());
            }

            [TestMethod]
            public void EmptyConfigurationsTest()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations = new List<FinancialAidConfiguration3>());
                var activeYears = studentAwardYearDtos.GetActiveStudentAwardYearDtos(configurationDtos);
                Assert.IsNotNull(activeYears);
                Assert.AreEqual(0, activeYears.Count());
            }

            [TestMethod]
            public void NoConfigurationsForAwardYear()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.AwardYear = "FOOBAR"));
                var activeYears = studentAwardYearDtos.GetActiveStudentAwardYearDtos(configurationDtos);
                Assert.IsNotNull(activeYears);
                Assert.AreEqual(0, activeYears.Count());
            }

            [TestMethod]
            public void ActiveAwardYearsTest()
            {
                var activeYears = studentAwardYearDtos.GetActiveStudentAwardYearDtos(configurationDtos);
                Assert.IsNotNull(activeYears);
                Assert.IsTrue(activeYears.Count() > 0);

                foreach (var year in activeYears)
                {
                    Assert.IsNotNull(configurationDtos.GetConfigurationDtoOrDefault(year));
                    Assert.IsTrue(configurationDtos.GetConfigurationDtoOrDefault(year).IsSelfServiceActive);
                }
            }

            [TestMethod]
            public void InactiveAwardYearsTest()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.IsSelfServiceActive = false));
                Assert.AreEqual(0, studentAwardYearDtos.GetActiveStudentAwardYearDtos(configurationDtos).Count());
            }
        }




        [TestClass]
        public class GetCOnfigurationOrDefaultTests : ConfigurationUtilityTests
        {
            public StudentAwardYear2 studentAwardYearDto
            { get { return testModelData.studentAwardYearsData.First(); } }

            public IEnumerable<FinancialAidConfiguration3> configurationDtos;

            [TestInitialize]
            public void Initialize()
            {
                testModelData = new TestModelData("0003914");
                configurationDtos = testModelData.financialAidOfficesData.SelectMany(o => o.Configurations);
            }

            [TestMethod]
            public void NullConfigurationsTest()
            {
                configurationDtos = null;
                var config = configurationDtos.GetConfigurationDtoOrDefault(studentAwardYearDto);
                Assert.IsNull(config);
            }

            [TestMethod]
            public void NullStudentAwardYearTest()
            {
                var config = configurationDtos.GetConfigurationDtoOrDefault(null);
                Assert.IsNull(config);
            }

            [TestMethod]
            public void ConfigurationHasSameAwardYearAsStudentAwardYear()
            {
                var config = configurationDtos.GetConfigurationDtoOrDefault(studentAwardYearDto);
                Assert.AreEqual(studentAwardYearDto.Code, config.AwardYear);
            }

            [TestMethod]
            public void ConfigurationHasSameOfficeAsStudentAwardYear()
            {
                var config = configurationDtos.GetConfigurationDtoOrDefault(studentAwardYearDto);
                Assert.AreEqual(studentAwardYearDto.FinancialAidOfficeId, config.OfficeId);
            }

            [TestMethod]
            public void NoConfigurationWithSameAwardYearAsStudentAwardYear()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.AwardYear = "FOOBAR"));
                var config = configurationDtos.GetConfigurationDtoOrDefault(studentAwardYearDto);
                Assert.IsNull(config);
            }

            [TestMethod]
            public void NoConfigurationWithSameOfficeAsStudentAwardYear()
            {
                testModelData.financialAidOfficesData.ForEach(o => o.Configurations.ForEach(c => c.OfficeId = "FOOBAR"));
                var config = configurationDtos.GetConfigurationDtoOrDefault(studentAwardYearDto);
                Assert.IsNull(config);
            }
        }
    }
}
