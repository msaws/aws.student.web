﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Tests.Areas.FinancialAid.Models;
using Ellucian.Web.Student.Areas.FinancialAid.Utility;
using Ellucian.Web.Student.Areas.FinancialAid.Models;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Web.Student.Tests.Areas.FinancialAid.Utility
{
    /// <summary>
    /// SAPUtility test class
    /// </summary>
    [TestClass]
    public class SAPUtilityTests
    {
        private TestModelData testModelData;
        private IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2> academicProgressEvaluations;
        private IEnumerable<Colleague.Dtos.FinancialAid.AcademicProgressStatus> academicProgressStatuses;
        private IEnumerable<Colleague.Dtos.Student.AcademicProgram> academicProgramsData;

        [TestInitialize]
        public void Initalize()
        {
            testModelData = new TestModelData("0003914");
            academicProgressEvaluations = testModelData.academicProgressEvaluationsData;
            academicProgressStatuses = testModelData.academicProgressStatusesData;
            academicProgramsData = testModelData.academicProgramsData;
        }

        [TestCleanup]
        public void Cleanup()
        {
            testModelData = null;
            academicProgressEvaluations = null;
            academicProgressStatuses = null;
            academicProgramsData = null;
        }

        /// <summary>
        /// Tests if SAPStatus object was not created when no academic progress evaluation data
        /// was passed
        /// </summary>
        [TestMethod]        
        public void NullAcademicProgressEvaluations_NullReturnedTest()
        {
            Assert.IsNull(SAPUtility.GetLatestSAPStatus(null, academicProgressStatuses, academicProgramsData));
        }

        /// <summary>
        /// Tests if SAPStatus object was not created when no progress statuses data was passed
        /// </summary>
        [TestMethod]
        public void NullAcademicProgressStatuses_NullReturnedTest()
        {
            Assert.IsNull(SAPUtility.GetLatestSAPStatus(academicProgressEvaluations, null, academicProgramsData));
        }

        /// <summary>
        /// Tests if SAPStatus object was created when academicProgramsData is not available
        /// </summary>
        [TestMethod]
        public void NullAcademicProgramsData_SAPStatusReturnedTest()
        {
            var sapStatus = SAPUtility.GetLatestSAPStatus(academicProgressEvaluations, academicProgressStatuses, null);
            Assert.IsNotNull(sapStatus);
            Assert.AreEqual(typeof(SAPStatus), sapStatus.GetType());
        }

        /// <summary>
        /// Tests if SAPStatus object was not created if there is no recent evaluation
        /// </summary>
        [TestMethod]
        public void NullMostRecentEvaluation_NullReturnedTest()
        {
            Assert.IsNull(SAPUtility.GetLatestSAPStatus(new List<Colleague.Dtos.FinancialAid.AcademicProgressEvaluation2>(), academicProgressStatuses, academicProgramsData));
        }

        /// <summary>
        /// Tests if SAPStatus object was not created if no matching academic progress status was found
        /// </summary>
        [TestMethod]
        public void NullProgressStatus_NullReturnedTest()
        {
            Assert.IsNull(SAPUtility.GetLatestSAPStatus(academicProgressEvaluations, new List<AcademicProgressStatus>(), academicProgramsData));
        } 
       
        /// <summary>
        /// Tests if SAPStatus StudentAcademicProgram code equals the passed one
        /// </summary>
        [TestMethod]
        public void StudentAcademicProgram_EqualsTest()
        {
            var mostRecentEvaluation = academicProgressEvaluations.OrderByDescending(aped => aped.EvaluationDateTime).FirstOrDefault();
            var academicProgram = academicProgramsData.FirstOrDefault(ap => ap.Code == mostRecentEvaluation.ProgramDetail.ProgramCode);

            var sapStatus = SAPUtility.GetLatestSAPStatus(academicProgressEvaluations, academicProgressStatuses, academicProgramsData);
            Assert.AreEqual(academicProgram.Description, sapStatus.StudentAcademicProgram);
        }
    }
}
