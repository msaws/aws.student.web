﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("outside.awards.js", function () {

    var outsideAwardsUrl,
        contentType;

    beforeAll(function () {
        getGeneralOutsideAwardsDataActionUrl = "url";
        getOutsideAwardsActionUrl = "specific url";
        jsonContentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        outsideAwardsAdminActionUrl = "adminUrl";
        outsideAwardsViewModelInstance = new outsideAwardsViewModel();
        faNotAuthorizedMessage = "Not authorized";
        faMissingStudentIdMessage = "missing id";
        faInvalidStudentIdMessage = "invalid id";
        faOutsideAwardsUnableToLoadMessage = "unable to load";        
    });

    beforeEach(function () {
        outsideAwardsUrl = "url";
        contentType = "json";
    });

    it(":ajax call uses correct url", function () {
        spyOn($, "ajax");
        getOutsideAwards();
        expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(outsideAwardsUrl);
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getOutsideAwards();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":showUI has been called with true on success", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(outsideAwardsViewModelInstance, "showUI");
        getOutsideAwards();
        expect(outsideAwardsViewModelInstance.showUI).toHaveBeenCalledWith(true);
    });

    //it(":selectedYear is set with expected value on successful ajax request", function () {
    //    var awardYear = testData().awardYearsData[1];
    //    memory.setItem("yearCode", "2016");
    //    outsideAwardsViewModelInstance.AwardYears(testData().awardYearsData);
    //    outsideAwardsViewModelInstance.selectedYear = ko.observable({
    //        Code: ko.observable()
    //    });
    //    spyOn($, "ajax").and.callFake(function (e) {
    //        e.success({});
    //    });
    //    spyOn(outsideAwardsViewModelInstance, "selectedYear");
    //    getOutsideAwards();
    //    expect(outsideAwardsViewModelInstance.selectedYear).toHaveBeenCalled();
    //});

    it(":getOutsideAwardsData is being invoked on successful ajax request", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(utility, "getOutsideAwardsData");
        getOutsideAwards();
        expect(utility.getOutsideAwardsData).toHaveBeenCalled();
    });

    it(":on error with status of 403 correct message is being sent", function () {
        var jqXHR = {
            status: 403
        };
        var secondArg = {
            message: "Not authorized",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getOutsideAwards();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":correct message is being sent when 400 error occurs", function () {
        var jqXHR = {
            status: 400
        };
        var secondArg = {
            message: "missing id",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getOutsideAwards();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":correct message is being sent when 404 error occurs", function () {
        var jqXHR = {
            status: 404
        };
        var secondArg = {
            message: "invalid id",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getOutsideAwards();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":correct message is set when an unknown error occurs", function () {
        var jqXHR = {
            status: 500
        };
        var secondArg = {
            message: "unable to load",
            type: "error"
        }

        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getOutsideAwards();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":notification center does not get called when error status is 0", function () {
        var jqXHR = {
            status: 0
        };

        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getOutsideAwards();
        expect($.fn.notificationCenter).not.toHaveBeenCalled();
    });
});