﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("documents.mapping.js", function () {
    var documentsViewModelData,
        documentsViewModelInstance;

    beforeAll(function () {
        documentsViewModelData = testData().documentsData;
        documentsAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        documentsViewModelInstance = new documentsViewModel();
        ko.mapping.fromJS(documentsViewModelData, documentsViewModelMapping, documentsViewModelInstance);
    });

    describe("studentDocumentModelTests", function () {
        var documentModel,
            documentTestData;

        beforeEach(function () {
            documentTestData = documentsViewModelData.StudentDocumentCollections[0].CompleteDocuments[0];
            documentModel = new studentDocumentModel(documentTestData);
        });

        it(":documentTestData maps correctly to documentModel", function () {
            expect(documentModel.Code()).toEqual(documentTestData.Code());
            expect(documentModel.Description()).toEqual(documentTestData.Description());
            expect(documentModel.Status()).toEqual(documentTestData.Status());
            expect(documentModel.StatusDisplay()).toEqual(documentTestData.StatusDisplay());
        });

        it(":statusStyle returns 'document-received' if status equals 'Received'", function () {
            documentTestData.Status("Received");
            documentModel = new studentDocumentModel(documentTestData);
            expect(documentModel.statusStyle()).toEqual('document-received');
        });

        it(":statusStyle returns 'document-waived' if status equals 'Waived'", function () {
            documentTestData.Status("Waived");
            documentModel = new studentDocumentModel(documentTestData);
            expect(documentModel.statusStyle()).toEqual('document-waived');
        });

        it(":statusStyle returns 'document-overdue' if status equals 'Incomplete' and status display equals 'Overdue'", function () {
            documentTestData.Status("Incomplete");
            documentTestData.StatusDisplay("Overdue");
            documentModel = new studentDocumentModel(documentTestData);
            expect(documentModel.statusStyle()).toEqual('document-overdue');
        });

        it(":statusStyle returns 'document-overdue' if status is blank and status display equals 'Overdue'", function () {
            documentTestData.Status("");
            documentTestData.StatusDisplay("Overdue");
            documentModel = new studentDocumentModel(documentTestData);
            expect(documentModel.statusStyle()).toEqual('document-overdue');
        });

        it(":statusStyle returns 'document-incomplete' if status equals 'Incomplete'", function () {
            documentTestData.Status("Incomplete");
            documentTestData.StatusDisplay("");
            documentModel = new studentDocumentModel(documentTestData);
            expect(documentModel.statusStyle()).toEqual('document-incomplete');
        });

        it(":statusStyle returns 'document-incomplete' if status is blank", function () {
            documentTestData.Status("");
            documentTestData.StatusDisplay("Complete");
            documentModel = new studentDocumentModel(documentTestData);
            expect(documentModel.statusStyle()).toEqual('document-incomplete');
        });
    });
});