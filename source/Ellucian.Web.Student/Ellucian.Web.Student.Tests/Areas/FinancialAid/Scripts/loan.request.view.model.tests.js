﻿//Copyright 2015 Ellucian Company L.P. and its affiliates

describe("loan.request.view.model.js", function () {
    var viewModel,           //local instance of loanRequestViewModel under test
    personDataModel,         //person data
    loanRequestDataModel,    //loan request data
    awardYearDataModel       //award year data

    beforeAll(function () {
        createNewLoanRequestActionUrl = "url",
        faLoanRequestSuccessMessage = "Success!",
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        },
        faLoanRequestUnableToSubmitLoanRequestMessage = "Unable to submit";
        loanRequestAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        viewModel = new loanRequestViewModel();
        viewModel.IsProxyView = ko.observable();
        personDataModel = {
            Id: ko.observable("0004791")
        },

        loanRequestDataModel = {
            AwardYearCode: ko.observable("2015"),
            TotalRequestAmount: ko.observable(2000),
            AssignedToCounselor: ko.observable({
                Id: ko.observable("0003914")
            }),
            IsRequestPending: ko.observable(false),
            StudentId: ko.observable("0004791"),
            LoanRequestAwardPeriods: ko.observable([
                {
                    Code: ko.observable("15/FA"),
                    Description: ko.observable("Fall 2015")
                },
                {
                    Code: ko.observable("16/SP"),
                    Description: ko.observable("Spring 2016")
                }
            ]),
            requestSubmitted: ko.observable(false),
            currentWorkFlowStep: ko.observable(),
            selectedRequestOption: ko.observable('foo')
        },

        awardYearDataModel = {
            Code: ko.observable("2015")
        }
    });

    afterEach(function () {
        viewModel = null;
        personDataModel = null;
        loanRequestDataModel = null;
        awardYearDataModel = null;
    });

    it(":isLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":Configuration is undefined after initialization", function () {
        expect(viewModel.Configuration()).toBeUndefined();
    });

    it(":showUi is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":loanRequestSubmissionInProgress is false after initialization", function () {
        expect(viewModel.loanRequestSubmissionInProgress()).toBeFalsy();
    });

    it(":Person is undefined after initialization", function () {
        expect(viewModel.Person()).toBeUndefined();
    });

    it(":LoanRequests array is empty after initialization", function () {
        expect(viewModel.LoanRequests()).toEqual([]);
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(loanRequestAdminActionUrl);
    });

    it(":selectedLoanRequestForYear is null if no selectedYear", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedLoanRequestForYear()).toBeNull();
    });

    it(":selectedLoanRequestForYear is null if no loan request for selectedYear", function () {
        viewModel.selectedYear(awardYearDataModel.Code("foo"));
        viewModel.LoanRequests().push(loanRequestDataModel);
        expect(viewModel.selectedLoanRequestForYear()).toBeNull();
    });

    it(":selectedLoanRequestForYear returns expected loan request", function () {
        viewModel.selectedYear(awardYearDataModel);
        viewModel.LoanRequests.push(loanRequestDataModel);        
        expect(viewModel.selectedLoanRequestForYear()).toEqual(loanRequestDataModel);
        
    });

    it(":currencySymbol returns expected symbol", function () {
        var expectedCurrencySymbol = Globalize.culture().numberFormat.currency.symbol;
        expect(viewModel.currencySymbol()).toEqual(expectedCurrencySymbol);
    });

    it(":createNewLoanRequest sets loanRequestSubmissionInProgress to true", function () {
        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect(viewModel.loanRequestSubmissionInProgress()).toBeTruthy();
    });

    it(":isLoading is set to true when onCreatNewLoanRequest is triggered", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });

        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":notificationCenter has been called with correct arguments on createNewLoanRequest success", function () {
        viewModel.LoanRequests.push(loanRequestDataModel);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(loanRequestDataModel);
        });

        spyOn($.fn, "notificationCenter");
        
        var secondArg = {
            message: "Success!",
            type: "success",
            flash: true
        };

        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":isLoading is set to false on createNewLoanRequest error", function () {
        var jqXHR = { status: 403 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":notificationCenter has been called with correct arguments on createNewLoanRequest error", function () {
        var jqXHR = { status: 403 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");

        var secondArg = {
            message: "Unable to submit",
            type: "error"
        };

        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":isLoading is set to false on createNewLoanRequest complete", function () {
        viewModel.LoanRequests.push(loanRequestDataModel);
        viewModel.selectedYear(awardYearDataModel);
        var jqXHR = { status: 200 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete(jqXHR, undefined);
        });
                
        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":loanRequestSubmissionInProgress is set to false on createNewLoanRequest complete", function () {
        viewModel.LoanRequests.push(loanRequestDataModel);
        viewModel.selectedYear(awardYearDataModel);
        var jqXHR = { status: 200 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete(jqXHR, undefined);
        });

        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect(viewModel.loanRequestSubmissionInProgress()).toBeFalsy();
    });

    it(":requestSubmitted is set to true on createNewLoanRequest complete", function () {
        viewModel.LoanRequests.push(loanRequestDataModel);
        viewModel.selectedYear(awardYearDataModel);
        var jqXHR = { status: 200 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete(jqXHR, undefined);
        });

        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect(viewModel.selectedLoanRequestForYear().requestSubmitted()).toBeTruthy();
    });

    it(":currentWorkFlowStep is set to 'Confirmation' on createNewLoanRequest complete", function () {
        viewModel.LoanRequests.push(loanRequestDataModel);
        viewModel.selectedYear(awardYearDataModel);
        var jqXHR = { status: 200 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete(jqXHR, undefined);
        });

        viewModel.createNewLoanRequest(loanRequestDataModel);
        expect(viewModel.selectedLoanRequestForYear().currentWorkFlowStep()).toEqual("Confirmation");
    });

    it(":selectCustomRequestOption sets selectedRequestOption to 'customAmount' when person is not proxy", function () {
        viewModel.IsProxyView(false);
        viewModel.LoanRequests.push(loanRequestDataModel);
        viewModel.selectedYear(awardYearDataModel);
        viewModel.selectCustomRequestOption();
        expect(viewModel.selectedLoanRequestForYear().selectedRequestOption()).toEqual('customAmount');
    });

    it(":selectCustomRequestOption does not set selectedRequestOption to 'customAmount' when person is proxy", function () {
        viewModel.IsProxyView(true);
        viewModel.LoanRequests.push(loanRequestDataModel);
        viewModel.selectedYear(awardYearDataModel);
        viewModel.selectCustomRequestOption();
        expect(viewModel.selectedLoanRequestForYear().selectedRequestOption()).not.toEqual('customAmount');
    });
    
});