﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
var personProxyLoadingThrobberMessage = "Loading...";
var personProxyLoadingThrobberAltText = "Alt Loading";
var personProxyChangingThrobberMessage = "Changing...";
var personProxyChangingThrobberAltText = "Alt Changing";

describe("home.js", function () {
    beforeAll(function () {        
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        faNotAuthorizedMessage = "Not authorized";
        faMissingStudentIdMessage = "Missing id";
        faHomeUnableToLoadMessage = "Unable to load";
        getHomeDataActionUrl = "getHomeDataUrl";
        faInvalidStudentIdMessage = "Invalid id";
        faHomeNoFinAidDataMessage = "No fin aid data available";
        faHomeNoChecklistAssignedStudentMessage = "You have no checklist items";
        faHomeNoChecklistAssignedMessage = "Student has no checklist items";
        homeAdminActionUrl = "adminUrl";
        homeViewModelInstance = new homeViewModel();
        homeViewModelInstance.selectedYear = ko.observable({ Code: ko.observable() });
        homeViewModelInstance.Configuration = ko.observable(testData().configurationData);        
    });

    it(":ajax is invoked with expected url", function () {
        spyOn($, "ajax");
        getHomeData();
        expect($.ajax.calls.mostRecent().args[0]['url']).toEqual("getHomeDataUrl");
    });

    it(":ajax is invoked with expected dataType", function () {
        spyOn($, "ajax");
        getHomeData();
        expect($.ajax.calls.mostRecent().args[0]['dataType']).toEqual("json");
    });

    it(":ajax is invoked with expected action type", function () {
        spyOn($, "ajax");
        getHomeData();
        expect($.ajax.calls.mostRecent().args[0]['type']).toEqual("GET");
    });

    it(":ajax succes callback invokes ko.mapping.fromJS", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(ko.mapping, "fromJS");
        getHomeData();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it(":ajax succes callback does not invoke ko.mapping.fromJS if handleInvalidSessionResponse flag is true", function () {
        account.handleInvalidSessionResponse = function (data) { return true; };
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(ko.mapping, "fromJS");
        getHomeData();
        expect(ko.mapping.fromJS).not.toHaveBeenCalled();
    });

    it(":ajax success callback sets selectedYear to expected value", function () {
        account.handleInvalidSessionResponse = function (data) { return false; };
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        memory.setItem("yearCode", "2015");
        homeViewModelInstance.AwardYears([{ Code: ko.observable("2015") }]);
        getHomeData();
        expect(homeViewModelInstance.selectedYear().Code()).toEqual("2015");
    });

    it(":ajax success callback sets 'newPage' value in memory to 0", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        getHomeData();
        expect(memory.getItem('newPage')).toEqual('0');
    });

    it(":ajax success callback sets showUI flag to true", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });        
        getHomeData();
        expect(homeViewModelInstance.showUI()).toEqual(true);
    });

    it(":ajax error callback sends expected message to notification center when 403 error occurs", function () {
        var xhr = {
            status: 403
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getHomeData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Not authorized", type: "error" });
    });

    it(":ajax error callback sends expected message to notification center when 400 error occurs", function () {
        var xhr = {
            status: 400
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getHomeData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Missing id", type: "error" });
    });

    it(":ajax error callback sends expected message to notification center when 404 error occurs", function () {
        var xhr = {
            status: 404
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getHomeData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Invalid id", type: "error" });
    });

    it(":ajax error callback sends expected message to notification center when non-400/403 error occurs", function () {
        var xhr = {
            status: 500
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getHomeData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Unable to load", type: "error" });
    });

    it(":ajax complete callback invokes changeToMobile function when isMobile is true", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        homeViewModelInstance.isMobile(true);
        spyOn(homeViewModelInstance, "changeToMobile");
        getHomeData();
        expect(homeViewModelInstance.changeToMobile).toHaveBeenCalled();
    });

    it(":ajax complete callback invokes checkForMobile function when isMobile is false", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        homeViewModelInstance.isMobile(false);
        spyOn(homeViewModelInstance, "checkForMobile");
        getHomeData();
        expect(homeViewModelInstance.checkForMobile).toHaveBeenCalled();
    });
});