﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
var personProxyLoadingThrobberMessage = "Loading...";
var personProxyLoadingThrobberAltText = "Alt Loading";
var personProxyChangingThrobberMessage = "Changing...";
var personProxyChangingThrobberAltText = "Alt Changing";

describe("loan.request.js", function () {
    beforeAll(function () {
        getLoanRequestActionUrl = "getLoanRequestUrl";
        loanRequestAdminActionUrl = "adminUrl";
        loanRequestViewModelInstance = new loanRequestViewModel();
        loanRequestViewModelInstance.selectedYear = ko.observable({ Code: ko.observable() });
        faNotAuthorizedMessage = "Not authorized";
        faMissingStudentIdMessage = "Missing id";
        faLoanRequestUnableToLoadMessage = "Unable to load";
        faInvalidStudentIdMessage = "Invalid id";        
    });

    beforeEach(function () {
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    it(":ajax request is invoked with expected url", function () {
        spyOn($, "ajax");
        getLoanRequestData();
        expect($.ajax.calls.mostRecent().args[0]['url']).toEqual("getLoanRequestUrl");
    });

    it(":ajax request is invoked with expected dataType", function () {
        spyOn($, "ajax");
        getLoanRequestData();
        expect($.ajax.calls.mostRecent().args[0]['dataType']).toEqual("json");
        expect(loanRequestViewModelInstance.showUI()).toBe(false);
    });

    it(":ajax request is invoked with expected action type", function () {
        spyOn($, "ajax");
        getLoanRequestData();
        expect($.ajax.calls.mostRecent().args[0]['type']).toEqual("GET");
    });

    it(":ajax success callback triggers ko.mapping.fromJS when handleInvalidSessionResponse flag is false", function () {        
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(ko.mapping, "fromJS");
        getLoanRequestData();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it(":ajax success callback does not trigger ko.mapping.fromJS when handleInvalidSessionResponse flag is true", function () {
        account.handleInvalidSessionResponse = function (data) { return true; };
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(ko.mapping, "fromJS");
        getLoanRequestData();
        expect(ko.mapping.fromJS).not.toHaveBeenCalled();
    });

    it(":ajax success callback sets selectedYear to expected value", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        memory.setItem("yearCode", "2015");
        loanRequestViewModelInstance.AwardYears = ko.observable([{ Code: ko.observable("2015") }]);
        getLoanRequestData();
        expect(loanRequestViewModelInstance.selectedYear().Code()).toEqual("2015");
    });

    it(":ajax success callback sets showUI flag to true", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        getLoanRequestData();
        expect(loanRequestViewModelInstance.showUI()).toBe(true);
    });    

    it(":ajax success callback sets memory 'newPage' value to 0", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        getLoanRequestData();
        expect(memory.getItem('newPage')).toEqual('0');
    });

    it(":ajax error callback sets expected message in notificationCenter when error code is 403", function () {
        var xhr = {
            status: 403
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getLoanRequestData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Not authorized", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when error code is 404", function () {
        var xhr = {
            status: 404
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getLoanRequestData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Invalid id", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when error code is 400", function () {
        var xhr = {
            status: 400
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getLoanRequestData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Missing id", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when error code is not 400/403", function () {
        var xhr = {
            status: 500
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getLoanRequestData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Unable to load", type: "error" });
    });

    it(":ajax complete callback invokes checkForMobile function", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn(loanRequestViewModelInstance, "checkForMobile");
        getLoanRequestData();
        expect(loanRequestViewModelInstance.checkForMobile).toHaveBeenCalled();
    });
});