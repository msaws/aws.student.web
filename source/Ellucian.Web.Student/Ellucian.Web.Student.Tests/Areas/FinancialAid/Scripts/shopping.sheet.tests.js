﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
var personProxyLoadingThrobberMessage = "Loading...";
var personProxyLoadingThrobberAltText = "Alt Loading";
var personProxyChangingThrobberMessage = "Changing...";
var personProxyChangingThrobberAltText = "Alt Changing";
var shoppingSheetAdminActionUrl = "adminUrl";

describe("shopping.sheet.js", function () {
    beforeAll(function () {
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        faNotAuthorizedMessage = "Not authorized";
        faMissingStudentIdMessage = "Missing id";
        faHomeUnableToLoadMessage = "Unable to load";
        faInvalidStudentIdMessage = "Invalid id";
        faShoppingSheetUnableToLoadMessage = "Unable to load";
        faShoppingSheetBestViewedOnLargerDeviceMessage = "View in desktop";
        getShoppingSheetDataActionUrl = "getShoppingSheetUrl";
        shoppingSheetViewModelInstance = new shoppingSheetViewModel();
    });

    it(":ajax is invoked with expected url", function () {
        spyOn($, "ajax");
        getShoppingSheetData();
        expect($.ajax.calls.mostRecent().args[0]['url']).toEqual("getShoppingSheetUrl");
    });

    it(":ajax is invoked with expected dataType", function () {
        spyOn($, "ajax");
        getShoppingSheetData();
        expect($.ajax.calls.mostRecent().args[0]['dataType']).toEqual("json");
    });

    it(":ajax is invoked with expected action type", function () {
        spyOn($, "ajax");
        getShoppingSheetData();
        expect($.ajax.calls.mostRecent().args[0]['type']).toEqual("GET");
    });

    it(":ajax succes callback invokes ko.mapping.fromJS", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(ko.mapping, "fromJS");
        getShoppingSheetData();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it(":ajax succes callback does not invoke ko.mapping.fromJS if handleInvalidSessionResponse flag is true", function () {
        account.handleInvalidSessionResponse = function (data) { return true; };
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(ko.mapping, "fromJS");
        getShoppingSheetData();
        expect(ko.mapping.fromJS).not.toHaveBeenCalled();
    });

    it(":ajax success callback sets selectedYear to expected value", function () {
        account.handleInvalidSessionResponse = function (data) { return false; };
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        memory.setItem("yearCode", "2016");
        shoppingSheetViewModelInstance.AwardYears([{ Code: ko.observable("2016") }]);
        getShoppingSheetData();
        expect(shoppingSheetViewModelInstance.selectedYear().Code()).toEqual("2016");
    });

    it(":ajax success callback sets 'newPage' value in memory to 0", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        getShoppingSheetData();
        expect(memory.getItem('newPage')).toEqual('0');
    });

    it(":ajax success callback sets showUI flag to true", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        getShoppingSheetData();
        expect(shoppingSheetViewModelInstance.showUI()).toEqual(true);
    });

    it(":ajax success callback invokes checkForMobile function", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(shoppingSheetViewModelInstance, "checkForMobile");
        getShoppingSheetData();
        expect(shoppingSheetViewModelInstance.checkForMobile).toHaveBeenCalled();
    });

    it(":ajax error callback sends expected message to notification center when 403 error occurs", function () {
        var xhr = {
            status: 403
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getShoppingSheetData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Not authorized", type: "error" });
    });

    it(":ajax error callback sends expected message to notification center when 400 error occurs", function () {
        var xhr = {
            status: 400
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getShoppingSheetData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Missing id", type: "error" });
    });

    it(":ajax error callback sends expected message to notification center when 404 error occurs", function () {
        var xhr = {
            status: 404
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getShoppingSheetData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Invalid id", type: "error" });
    });

    it(":ajax error callback sends expected message to notification center when unknown error occurs", function () {
        var xhr = {
            status: 500
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr);
        });
        spyOn($.fn, "notificationCenter");
        getShoppingSheetData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Unable to load", type: "error" });
    });
});