﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("correspondence.option.mapping.js", function () {

    var corrOptionData,
        corrOptionViewModelInstance;

    beforeAll(function () {
        corrOptionData = testData().correspondenceOptionData;
        correspondenceOptionAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        corrOptionViewModelInstance = new correspondenceOptionViewModel();
        ko.mapping.fromJS(corrOptionData, correspondenceOptionViewModelMapping, corrOptionViewModelInstance);
    });

    describe("correspondenceOptionDataModelTests", function () {
        var corrOptionDataModel;

        it(":isOriginalOptionChecked is set to the value of IsCorrespondenceOptionChecked flag", function () {
            corrOptionData.IsCorrespondenceOptionChecked(true);
            corrOptionDataModel = new correspondenceOptionDataModel(corrOptionData);
            expect(corrOptionDataModel.isOriginalOptionChecked()).toEqual(corrOptionData.IsCorrespondenceOptionChecked);
        });
    });
});