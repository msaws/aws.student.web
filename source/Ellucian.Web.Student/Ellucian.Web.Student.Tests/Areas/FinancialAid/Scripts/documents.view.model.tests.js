﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("documents.view.model.js", function () {
    var viewModel,                   //documents view model instance under test
        personDataModel,             //person data
        documentsCollection,         //documents data
        anotherDocumentsCollection,  //another documents collection data
        awardYearDataModel,          //award year data
        documents,
        singleDocument;

    beforeAll(function () {
        documentsAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        viewModel = new documentsViewModel();

        personDataModel = {
            Id: ko.observable("0004791")
        },

        documents = [
            { Code: ko.observable("doc1") },
            { Code: ko.observable("doc2") },
            { Code: ko.observable("doc3") }
        ],

        documentsCollection = {
            AwardYearCode: ko.observable("2015"),
            Documents: ko.observable([]),
            IncompleteDocuments: ko.observable(documents),
            CompleteDocuments: ko.observable(documents),
            DocumentsExist: ko.observable(true),
            HasOutstandingDocuments: ko.observable(true)
        },

        anotherDocumentsCollection = {
            AwardYearCode: ko.observable("2014"),
            Documents: ko.observable([]),
            IncompleteDocuments: ko.observable([]),
            CompleteDocuments: ko.observable([]),
            DocumentsExist: ko.observable(false),
            HasOutstandingDocuments: ko.observable(true)
        },

        awardYearDataModel = {
            Code: ko.observable("2015")
        },

        singleDocument = {
            Code: ko.observable("document")
        };
    });

    afterEach(function () {
        viewModel = null;
        personDataModel = null;
        documents = null;
        documentsCollection = null;
        awardYearDataModel = null;
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":Person is undefined after initialization", function () {
        expect(viewModel.Person()).toBeUndefined();
    });

    it(":StudentDocumentCollections list is empty after initialization", function () {
        expect(viewModel.StudentDocumentCollections()).toEqual([]);
    });

    it(":selectedDocumentCollection is null if selectedYear is undefined", function () {
        expect(viewModel.selectedDocumentCollection()).toEqual(null);
    });

    it(":activeDocument is undefined after initialization", function () {
        expect(viewModel.activeDocument()).toBeUndefined();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(documentsAdminActionUrl);
    });

    it(":selectedDocumentCollection is null if selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedDocumentCollection()).toEqual(null);
    });

    it(":selectedDocumentCollection is null if no collections exist", function () {
        viewModel.selectedYear(awardYearDataModel);
        expect(viewModel.selectedDocumentCollection()).toEqual(null);
    });

    it(":selectedDocumentCollection is null if no matching collection exists", function () {
        viewModel.selectedYear(awardYearDataModel.Code("foo"));
        viewModel.StudentDocumentCollections.push(documentsCollection);
        expect(viewModel.selectedDocumentCollection()).toEqual(null);
    });

    it(":selectedDocumentCollection is not null if a matching collection exists", function () {
        viewModel.selectedYear(awardYearDataModel);
        viewModel.StudentDocumentCollections.push(documentsCollection);
        expect(viewModel.selectedDocumentCollection()).not.toEqual(null);
    });

    it(":selectedDocumentCollection returns the expected documents collection", function () {
        viewModel.selectedYear(awardYearDataModel);
        viewModel.StudentDocumentCollections.push(documentsCollection);
        viewModel.StudentDocumentCollections.push(anotherDocumentsCollection);
        expect(viewModel.selectedDocumentCollection()).toEqual(documentsCollection);
    });

    it("documentsExistForYear returns false if there is no selectedDocumentCollection", function () {
        viewModel.selectedYear(awardYearDataModel);
        expect(viewModel.documentsExistForYear()).toBeFalsy();
    });

    it("documentsExistForYear returns false if DocumentsExist field on the collection is false", function () {
        viewModel.selectedYear(awardYearDataModel);
        documentsCollection.DocumentsExist(false);
        viewModel.StudentDocumentCollections.push(documentsCollection);
        expect(viewModel.documentsExistForYear()).toBeFalsy();
    });

    it("documentsExistForYear returns true if DocumentsExist field on the collection is true", function () {
        viewModel.selectedYear(awardYearDataModel);
        documentsCollection.DocumentsExist(true);
        viewModel.StudentDocumentCollections.push(documentsCollection);
        expect(viewModel.documentsExistForYear()).toBeTruthy();
    });

    it(":activateDocument does not update activeDocument observable if isMobile is false", function () {
        viewModel.isMobile(false);
        viewModel.activateDocument(singleDocument);
        expect(viewModel.activeDocument()).toBeUndefined();
    });

    it(":activateDocument updates activeDocument observable if isMobile is true", function () {
        viewModel.isMobile(true);
        viewModel.activateDocument(singleDocument);
        expect(viewModel.activeDocument()).toEqual(singleDocument);
    });

    it(":activateDocument retuns true", function () {
        expect(viewModel.activateDocument(singleDocument)).toBeTruthy();
    });

    it(":deactivateDocument nullifies activeDocument observable", function () {
        viewModel.isMobile(true);
        viewModel.activateDocument(singleDocument);
        expect(viewModel.activeDocument()).toEqual(singleDocument);

        viewModel.deactivateDocument();
        expect(viewModel.activeDocument()).toBe(null);
    });

    it(":changeToMobile triggers 'each' function", function () {
        spyOn($.fn, "each").and.callThrough();        
        viewModel.changeToMobile();
        expect($.fn.each).toHaveBeenCalled();        
    });    

    it(":changeToMobile triggers 'addClass' function with correct attribute", function () {
        spyOn($.fn, "addClass");
        viewModel.changeToMobile();
        expect($.fn.addClass).toHaveBeenCalledWith('offScreen');
    });

    it(":changeToMobile sets 'attr' with correct values", function () {
        spyOn($.fn, "attr");
        viewModel.changeToMobile();
        expect($.fn.attr).toHaveBeenCalledWith('aria-role', 'button');
    });

    it(":changeToDesktop triggers 'each' function", function () {
        spyOn($.fn, "each");
        viewModel.changeToDesktop();
        expect($.fn.each).toHaveBeenCalled();
    });

    it(":changeToDesktop triggers 'removeClass' function with expected attributes", function () {
        spyOn($.fn, "removeClass");
        viewModel.changeToDesktop();
        expect($.fn.removeClass).toHaveBeenCalledWith('offScreen');
    });

    it(":changeToDesktop triggers 'removeAttr' function with expected attributes", function () {
        spyOn($.fn, "removeAttr");
        viewModel.changeToDesktop();
        expect($.fn.removeAttr).toHaveBeenCalledWith('aria-role');
    });

    it(":changeToDesktop triggers 'deactivateDocument' function", function () {
        spyOn(viewModel, "deactivateDocument");
        viewModel.changeToDesktop();
        expect(viewModel.deactivateDocument).toHaveBeenCalled();
    });

});