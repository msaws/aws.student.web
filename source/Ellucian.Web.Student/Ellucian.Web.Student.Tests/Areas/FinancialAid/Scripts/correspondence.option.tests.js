﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
describe("correspondence.option.js", function () {

    beforeAll(function () {        
        getCorrespondenceOptionActionUrl = "correspOptionUrl";
        account = {
            handleInvalidSessionResponse: function(data){return false;}
        };
        faMissingStudentIdMessage = "Missing id";
        faNotAuthorizedMessage = "Not authorized";
        faCorrespondenceOptionUnableToLoadMessage = "Unable to load";
        faInvalidStudentIdMessage = "Invalid id";
        correspondenceOptionAdminActionUrl = "adminUrl";
        correspondenceOptionViewModelInstance = new correspondenceOptionViewModel();
    });

    beforeEach(function () {

    });   

    afterEach(function () {

    });

    it(":ajax is invoked with expected url", function () {
        spyOn($, "ajax");
        getCorrespondenceOptionData();
        expect($.ajax.calls.mostRecent().args[0]['url']).toEqual("correspOptionUrl");
    });

    it(":ajax is invoked with expected dataType", function () {
        spyOn($, "ajax");
        getCorrespondenceOptionData();
        expect($.ajax.calls.mostRecent().args[0]['dataType']).toEqual("json");
    });

    it(":ajax is invoked with expected action type", function () {
        spyOn($, "ajax");
        getCorrespondenceOptionData();
        expect($.ajax.calls.mostRecent().args[0]['type']).toEqual("GET");
    });

    it(":ajax success callback sets showUI flag to true", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        getCorrespondenceOptionData();
        expect(correspondenceOptionViewModelInstance.showUI()).toEqual(true);
    });

    it(":ajax error callback places expected message in console", function () {
        var xhr = {
            status: 400,
            responseText: "text"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn(console, "debug");
        getCorrespondenceOptionData();
        expect(console.debug).toHaveBeenCalledWith("Correpondence option data retrieval failed: 400 text");
    });

    it(":ajax error callback sets expected message in notificationCenter when 403", function () {
        var xhr = {
            status: 403
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getCorrespondenceOptionData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Not authorized", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when 404", function () {
        var xhr = {
            status: 404
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getCorrespondenceOptionData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Invalid id", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when 400", function () {
        var xhr = {
            status: 400
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getCorrespondenceOptionData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Missing id", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when error status other than 400/403", function () {
        var xhr = {
            status: 500
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(xhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getCorrespondenceOptionData();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Unable to load", type: "error" });
    });
});