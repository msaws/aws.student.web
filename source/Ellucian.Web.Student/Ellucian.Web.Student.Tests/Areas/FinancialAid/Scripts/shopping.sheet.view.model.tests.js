﻿//Copyright 2015 - 2017 Ellucian Company L.P. and its affiliates
describe("shopping.sheet.view.model.js", function () {

    personProxyLoadingThrobberMessage = "Loading...";
    personProxyLoadingThrobberAltText = "Alt Loading";
    personProxyChangingThrobberMessage = "Changing...";
    personProxyChangingThrobberAltText = "Alt Changing";

    var viewModel,
        studentShoppingSheets,
        awardYear;

    var global = jasmine.getGlobal();

    beforeAll(function () {
        faShoppingSheetBestViewedOnLargerDeviceMessage = "message";
        shoppingSheetAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        viewModel = new shoppingSheetViewModel();
        studentShoppingSheets = testData().shoppingSheetsData;
        awardYear = testData().awardYearsData[0];
    });


    afterEach(function () {
        viewModel = null;
        studentShoppingSheets = null;
    });

    it(":showUI is false after initalization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":Student is undefined after initalization", function () {
        expect(viewModel.Student()).toBeUndefined();
    });

    it(":StudentShoppingSheets list is empty after initalization", function () {
        expect(viewModel.StudentShoppingSheets()).toEqual([]);
    });

    it(":Configuration is undefined after initalization", function () {
        expect(viewModel.Configuration()).toBeUndefined();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(shoppingSheetAdminActionUrl);
    });

    it(":selectedShoppingSheetForYear returns null if selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedShoppingSheetForYear()).toEqual(null);
    });

    it(":selectedShoppingSheetForYear returns null if selectedYear is undefined", function () {        
        expect(viewModel.selectedShoppingSheetForYear()).toEqual(null);
    });

    it(":selectedShoppingSheetForYear returns null if there is no shopping sheet data", function () {
        viewModel.selectedYear(awardYear);
        viewModel.StudentShoppingSheets([]);
        expect(viewModel.selectedShoppingSheetForYear()).toEqual(null);
    });

    it(":selectedShoppingSheetForYear returns null if there is no matching shopping sheet for the year", function () {
        awardYear.Code("foo");
        viewModel.StudentShoppingSheets(studentShoppingSheets);
        expect(viewModel.selectedShoppingSheetForYear()).toEqual(null);
    });

    it(":selectedShoppingSheetForYear returns expected shopping sheet", function () {
        viewModel.selectedYear(awardYear);
        viewModel.StudentShoppingSheets(studentShoppingSheets);
        var expectedShoppingSheet = studentShoppingSheets[0];
        expect(viewModel.selectedShoppingSheetForYear()).toEqual(expectedShoppingSheet);
    });

    it(":changeToMobile invokes an alert message to be shown to the user", function () {
        spyOn(global, "alert");
        viewModel.changeToMobile();
        expect(global.alert).toHaveBeenCalled();
    });

    it(":changeToMobile invokes an alert with expected message", function () {        
        spyOn(global, "alert");
        viewModel.changeToMobile();
        expect(global.alert).toHaveBeenCalledWith("message");
    });
});
