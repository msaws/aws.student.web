﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("outside.awards.mapping.js", function () {
    var outsideAwardsViewModelData,
        outsideAwardsViewModelInstance;

    beforeAll(function () {
        outsideAwardsViewModelData = testData().outsideAwardsViewModelData;
        outsideAwardsAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        outsideAwardsViewModelInstance = new outsideAwardsViewModel();
        ko.mapping.fromJS(outsideAwardsViewModelData, outsideAwardsViewModelMapping, outsideAwardsViewModelInstance);
    });

    describe("outsideAwardItemModelTests", function () {
        var outsideAwardTestData,
            outsideAwardsModel;

        beforeEach(function () {
            outsideAwardTestData = outsideAwardsViewModelData.OutsideAwards[0];
        });

        it(":outsideAwardItemModel properties are mapped correctly", function () {            
            outsideAwardsModel = new outsideAwardItemModel(outsideAwardTestData);
            expect(outsideAwardsModel.AwardName()).toEqual(outsideAwardTestData.AwardName());
            expect(outsideAwardsModel.AwardType()).toEqual(outsideAwardTestData.AwardType());
            expect(outsideAwardsModel.AwardAmount()).toEqual('1,000.00');
            expect(outsideAwardsModel.AwardFundingSource()).toEqual(outsideAwardTestData.AwardFundingSource());
            expect(outsideAwardsModel.AwardYearCode()).toEqual(outsideAwardTestData.AwardYearCode());
            expect(outsideAwardsModel.Id()).toEqual(outsideAwardTestData.Id());
            expect(outsideAwardsModel.StudentId()).toEqual(outsideAwardTestData.StudentId());
        });
    });
    
});