﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
var personProxyLoadingThrobberMessage = "Loading...";
var personProxyLoadingThrobberAltText = "Alt Loading";
var personProxyChangingThrobberMessage = "Changing...";
var personProxyChangingThrobberAltText = "Alt Changing";

describe("awardsViewModel", function () {
    var viewModel,                                //local instance of the view model we are testing        
        submittingChangeMessage,                  //initial spinner message
        awardPrerequisites,
        awardYear,
        anotherAwardYear,
        studentAwardCollection,
        anotherStudentAwardCollection,
        studentAwardsForYear,
        studentWorkAwardsForYear,
        loanCollection,
        anotherLoanCollection,
        awardYearConfigurations,
        finAidConfiguration,
        percentageChartCollection1,
        percentageChartCollection2,
        awardPeriod,
        award = null,
        loanCollection = null,
        event,
        awardLetterInfoForYear;

    var global = jasmine.getGlobal();
    
    totalAwardPeriodAmount = function (period, award) {
        var total = 0;
        if (award === null || typeof award === 'undefined') return total;
        else {
            var matchingStudentAwardPeriods = ko.utils.arrayFilter(award.StudentAwardPeriods(), function (studentAwardPeriod) {
                return (studentAwardPeriod.Code() === awardPeriod.Code());
            });

            ko.utils.arrayForEach(matchingStudentAwardPeriods, function (studentAwardPeriod) {
                var amt = parseFloat(studentAwardPeriod.AwardAmount());
                if (amt === null) { amt = 0; }
                total += amt;
            });

            return total;
        }
    };

    totalLoanCollectionPeriodAmount = function (period, loanCollection) {
        var total = 0;
        if (loanCollection === null || typeof loanCollection === 'undefined') return total;
        else {
            ko.utils.arrayForEach(loanCollection.Loans(), function (loan) {
                var amt = loan.totalAwardPeriodAmount(period);
                if (amt === null) { amt = 0; }
                total += amt;
            });
            return total;
        }
    };

    beforeAll(function () {        
        faAwardsSubmittingChangesMessage = "Submitting change...";
        faAwardsAcceptSpinnerMessage = "Accepting award(s)...";
        faAwardsRejectSpinnerMessage = "Rejecting award(s)...";
        faAwardsAcceptAllSpinnerMessage = "Accepting all";
        faAwardsRejectAllSpinnerMessage = "Rejecting all";
        faAwardsDeclineAllSuccessMessage = "Declined all";
        faAwardsInsufficientAmountMessage = "Insufficient amount";
        submittingChangeMessage = faAwardsSubmittingChangesMessage;
        updateLoanCollectionActionUrl = "updateLoanCollectionActionUrl/";
        acceptStudentAwardActionUrl = "acceptStudentAwardActionUrl/";
        updateAllAwardsAndLoansActionUrl = "updateAllAwardsAndLoansActionUrl";
        jsonContentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        faAwardsSubsidizedLoanType = "SubLoan";
        faAwardsUnsubsidizedLoanType = "UnsubLoan";
        faAwardsGradPlusLoanType = "GradPlusLoan";
        faAwardsLoanAmountSuccessfullyChanged = "Changed!";
        faAwardsLoanAcceptedMessage = "Accepted!";
        faAwardsLoanPlacedInReviewMessage = "Placed in Review!";
        faAwardsLoanDeclinedMessage = "Declined!";
        faAwardsErrorUpdatingAwardMessage = "Error updating";
        faAwardsAwardAcceptedMessage = "Award accepted!"
        faAwardsAcceptAllSpinnerMessage = "Submitting change...";
        faAwardsAcceptAllSuccessMessage = "Success!";
        awardsAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {        
        viewModel = new awardsViewModel();

        awardYear = {
            Code: ko.observable("2015"),
            AreChecklistItemsAssigned: ko.observable(true),
            DistinctAwardPeriods: ko.observable([
                {
                    Code: ko.observable("15/FA"),
                    IsActive: ko.observable(true),
                    originalIsActive: ko.observable(true)
                },
                {
                    Code: ko.observable("15/SP"),
                    IsActive: ko.observable(true),
                    originalIsActive: ko.observable(true)
                }
            ])
        };

        anotherAwardYear = {
            Code: ko.observable("2016"),
            AreChecklistItemsAssigned: ko.observable(true),
            DistinctAwardPeriods: ko.observable([
                {
                    Code: ko.observable("16/FA")
                },
                {
                    Code: ko.observable("16/SP")
                },
                {
                    Code: ko.observable("16/WI")
                }
            ])
        };

        awardPeriod = {
            Code: ko.observable("15/FA"),
            AwardId: ko.observable(),
            AwardAmount: ko.observable(),
        }

        awardPrerequisites = [
            {
                AwardYearCode: ko.observable("2015"),
                AreAllPrerequisitesSatisfied: ko.observable(true)
            }
        ];

        studentAwardsForYear = [
            {
                AwardYearId: ko.observable("2015"),
                Code: ko.observable("Award 1"),
                Description: ko.observable(),
                totalAmount: ko.observable(200),
                AwardStatus: {
                    Category: ko.observable("Accepted"),
                    Code: ko.observable()
                },
                IsStatusModifiable: ko.observable(),
                IsAmountModifiable: ko.observable(),
                StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(100),
                        originalAmount: ko.observable(100),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Accepted")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(100),
                        originalAmount: ko.observable(100),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Accepted")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                ]),
                awardUpdated: ko.observable(),
                flipAccordion: function () { },
                isAwardOnHoldOrInReview: ko.observable()
            },
            {
                AwardYearId: ko.observable("2015"),
                Code: ko.observable("Award 2"),
                Description: ko.observable(),
                totalAmount: ko.observable(500),
                AwardStatus: {
                    Category: ko.observable("Accepted"),
                    Code: ko.observable()
                },
                IsStatusModifiable: ko.observable(),
                IsAmountModifiable: ko.observable(),                
                StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(500),
                        originalAmount: ko.observable(500),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Accepted")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                ]),
                awardUpdated: ko.observable(),
                flipAccordion: function () { },
                isAwardOnHoldOrInReview: ko.observable()
            },
            {
                AwardYearId: ko.observable("2015"),
                Code: ko.observable("Award 3"),
                Description: ko.observable(),
                totalAmount: ko.observable(300),
                AwardStatus: {
                    Category: ko.observable("Rejected"),
                    Code: ko.observable()
                },
                IsStatusModifiable: ko.observable(),
                IsAmountModifiable: ko.observable(),
                StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(150),
                        originalAmount: ko.observable(150),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Accepted")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(150),
                        originalAmount: ko.observable(150),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Accepted")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                ]),
                awardUpdated: ko.observable(),
                flipAccordion: function () { },
                isAwardOnHoldOrInReview: ko.observable()
            }
        ];

        studentWorkAwardsForYear = [
            {
                AwardYearId: ko.observable("2015"),
                Code: ko.observable("Work Award 1"),
                totalAmount: ko.observable(1100),
                AwardStatus: {
                    Category: ko.observable("Pending"),
                    Code: ko.observable()
                },
                IsStatusModifiable: ko.observable(),
                IsAmountModifiable: ko.observable(),
                StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(1000),
                        originalAmount: ko.observable(1000),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Estimated")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(100),
                        originalAmount: ko.observable(100),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Pending")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                ]),
                awardUpdated: ko.observable(),
                flipAccordion: function () { },
                isAwardOnHoldOrInReview: ko.observable()
            },
            {
                AwardYearId: ko.observable("2015"),
                Code: ko.observable("Work Award 2"),
                totalAmount: ko.observable(1200),
                AwardStatus: {
                    Category: ko.observable("Denied"),
                    Code: ko.observable()
                },
                IsStatusModifiable: ko.observable(),
                IsAmountModifiable: ko.observable(),
                StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(700),
                        originalAmount: ko.observable(700),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Estimated")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(500),
                        originalAmount: ko.observable(500),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        AwardStatus: {
                            Category: ko.observable("Pending")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                ]),
                awardUpdated: ko.observable(),
                flipAccordion: function () { },
                isAwardOnHoldOrInReview: ko.observable()
            }
        ];

        for (var i = 0; i < studentAwardsForYear.length; i++) {
            var obj = studentAwardsForYear[i];
            studentAwardsForYear[i].totalAwardPeriodAmount = function (awardPeriod) { return totalAwardPeriodAmount(awardPeriod, obj); }
        }

        for (var i = 0; i < studentWorkAwardsForYear.length; i++) {
            var obj = studentWorkAwardsForYear[i];
            studentWorkAwardsForYear[i].totalAwardPeriodAmount = function (awardPeriod) { return totalAwardPeriodAmount(awardPeriod, obj); }
        }

        studentAwardCollection = {
            AwardYearCode: ko.observable("2015"),
            StudentAwardsForYear: ko.observable(studentAwardsForYear),
            StudentWorkAwardsForYear: ko.observable(studentWorkAwardsForYear)
        };

        anotherStudentAwardCollection = {
            AwardYearCode: ko.observable("2016"),
            StudentAwardsForYear: ko.observable([
                {
                    AwardYearId: ko.observable("2016"),
                    Code: ko.observable("Award A"),
                    totalAmount: ko.observable(500),
                    AwardStatus: {
                        Category: ko.observable("Estimated"),
                        Code: ko.observable()
                    },
                    IsStatusModifiable: ko.observable(),
                    IsAmountModifiable: ko.observable(),
                    StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(500),
                        AwardStatus: {
                            Category: ko.observable("Estimated")
                        },
                        IsIgnoredOnAwardLetter: ko.observable(true),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }]),
                    isAwardOnHoldOrInReview: ko.observable()
            }
            ]),
            StudentWorkAwardsForYear: ko.observable([])
        };

        for (var i = 0; i < anotherStudentAwardCollection.StudentAwardsForYear().length; i++) {
            var obj = anotherStudentAwardCollection.StudentAwardsForYear()[i];
            anotherStudentAwardCollection.StudentAwardsForYear()[i].totalAwardPeriodAmount = function (awardPeriod) { return totalAwardPeriodAmount(awardPeriod, obj); }
        }

        loanCollection = {
            AwardYearCode: ko.observable("2015"),
            Loans: ko.observable([
                {
                    AwardYearId: ko.observable("2015"),
                    Code: ko.observable("Loan 1"),
                    totalAmount: ko.observable(2000),
                    AwardStatus: {
                        Category: ko.observable("Rejected"),
                        Code: ko.observable()
                    },
                    IsStatusModifiable: ko.observable(),
                    IsAmountModifiable: ko.observable(true),
                    StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(1000),
                        originalAmount: ko.observable(1000),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        AwardId: ko.observable(),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(1000),
                        originalAmount: ko.observable(1000),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                    ]),
                    hasInputErrors: ko.observable(),
                    inputErrorMessage: ko.observable(),
                    isAwardOnHoldOrInReview: ko.observable()
                },
                {
                    AwardYearId: ko.observable("2015"),
                    Code: ko.observable("Loan 2"),
                    totalAmount: ko.observable(1500),
                    AwardStatus: {
                        Category: ko.observable("Accepted"),
                        Code: ko.observable()
                    },
                    IsStatusModifiable: ko.observable(),
                    IsAmountModifiable: ko.observable(),
                    StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(800),
                        originalAmount: ko.observable(800),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(700),
                        originalAmount: ko.observable(700),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                    ]),
                    hasInputErrors: ko.observable(),
                    inputErrorMessage: ko.observable(),
                    isAwardOnHoldOrInReview: ko.observable()
                },
                {
                    AwardYearId: ko.observable("2015"),
                    Code: ko.observable("Loan 3"),
                    totalAmount: ko.observable(900),
                    AwardStatus: {
                        Category: ko.observable("Accepted"),
                        Code: ko.observable()
                    },
                    IsStatusModifiable: ko.observable(),
                    IsAmountModifiable: ko.observable(true),
                    StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(450),
                        originalAmount: ko.observable(450),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(450),
                        originalAmount: ko.observable(450),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                    ]),
                    hasInputErrors: ko.observable(),
                    inputErrorMessage: ko.observable(),
                    isAwardOnHoldOrInReview: ko.observable()
                },
                {
                    AwardYearId: ko.observable("2015"),
                    Code: ko.observable("Loan 4"),
                    totalAmount: ko.observable(200),
                    AwardStatus: {
                        Category: ko.observable("Rejected"),
                        Code: ko.observable()
                    },
                    IsStatusModifiable: ko.observable(),
                    IsAmountModifiable: ko.observable(),
                    StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(100),
                        originalAmount: ko.observable(100),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    },
                    {
                        Code: ko.observable("15/SP"),
                        AwardAmount: ko.observable(100),
                        originalAmount: ko.observable(100),
                        IsActive: ko.observable(true),
                        originalIsActive: ko.observable(true),
                        loanChangeAmount: ko.observable(),
                        AwardStatus: {
                            Category: ko.observable("Accepted"),
                            Code: ko.observable()
                        },
                        IsAmountModifiable: ko.observable(true),
                        IsIgnoredOnAwardLetter: ko.observable(),
                        IsViewableOnAwardLetter: ko.observable(true)
                    }
                    ]),
                    hasInputErrors: ko.observable(),
                    inputErrorMessage: ko.observable(),
                    isAwardOnHoldOrInReview: ko.observable()
                }
            ]),
            StatusDescription: ko.observable(),
            hasPendingLoans: ko.observable(),
            passesLimitationCriteria: ko.observable(),
            IsStatusModifiable: ko.observable(true),
            IsAnyAmountModifiable: ko.observable(true),
            totalAmount: ko.observable(2500),
            MaximumAmount: ko.observable(10000),
            isLoan: true,
            LoanPeriods: ko.observable(awardYear.DistinctAwardPeriods()),
            loanCollectionUpdated: ko.observable(false),
            LoanCollectionType: ko.observable("SubsidizedLoan"),
            flipAccordion: function () { },
            IsLoanCollectionInReview: ko.observable(false),
            hasInputErrors: ko.observable()
        };

        for (var i = 0; i < loanCollection.Loans().length; i++) {
            var obj = loanCollection.Loans()[i];
            loanCollection.Loans()[i].totalAwardPeriodAmount = function (awardPeriod) { return totalAwardPeriodAmount(awardPeriod, obj); }
        }

        //These are being calculated in the mapping file
        loanCollection.totalAwardPeriodAmount = function (awardPeriod) { return totalLoanCollectionPeriodAmount(awardPeriod, loanCollection); };
        loanCollection.hasPendingLoans = ko.computed(function () {
            var pendingLoan = null;
            ko.utils.arrayFirst(loanCollection.Loans(), function (loan) {
                var category = ko.utils.unwrapObservable(loan.AwardStatus.Category());
                if (category === "Pending" || category === "Estimated") {
                    pendingLoan = loan;
                }
            });

            if (pendingLoan !== null) return true;
            else return false;
        });
        loanCollection.passesLimitationCriteria = ko.computed(function () {

            //if there are no pending loans or no amounts are modifiable, then we don't care if the amounts
            //do not meet limitations
            if (!loanCollection.hasPendingLoans() && !loanCollection.IsStatusModifiable() && !loanCollection.IsAnyAmountModifiable()) return true;

            if (loanCollection.totalAmount() > loanCollection.MaximumAmount()) {
                return false;
            }

            return true;
        });

        anotherLoanCollection = {
            AwardYearCode: ko.observable("2014"),
            Loans: ko.observable([
                {
                    AwardYearId: ko.observable("2014"),
                    Code: ko.observable("Loan a"),
                    totalAmount: ko.observable(200),
                    AwardStatus: {
                        Category: ko.observable("Accepted"),
                        Code: ko.observable()
                    },
                    IsStatusModifiable: ko.observable(),
                    IsAmountModifiable: ko.observable(),
                    StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(200),
                        AwardStatus: {
                            Category: ko.observable("Pending")
                        },
                        IsIgnoredOnAwardLetter: ko.observable()
                    }]),
                    isAwardOnHoldOrInReview: ko.observable()
                },
                {
                    AwardYearId: ko.observable("2014"),
                    Code: ko.observable("Loan b"),
                    totalAmount: ko.observable(200),
                    AwardStatus: {
                        Category: ko.observable("Denied"),
                        Code: ko.observable()
                    },
                    IsStatusModifiable: ko.observable(),
                    IsAmountModifiable: ko.observable(),
                    StudentAwardPeriods: ko.observable([
                    {
                        Code: ko.observable("15/FA"),
                        AwardAmount: ko.observable(200),
                        AwardStatus: {
                            Category: ko.observable("Estimated")
                        },
                        IsIgnoredOnAwardLetter: ko.observable()
                    }]),
                    isAwardOnHoldOrInReview: ko.observable()
                }
            ]),
            StatusDescription: ko.observable(),
            hasPendingLoans: ko.observable(),
            passesLimitationCriteria: ko.observable(),
            IsStatusModifiable: ko.observable(),
            IsAnyAmountModifiable: ko.observable(),
            totalAmount: ko.observable(3678)
        };

        for (var i = 0; i < anotherLoanCollection.Loans().length; i++) {
            var obj = anotherLoanCollection.Loans()[i];
            anotherLoanCollection.Loans()[i].totalAwardPeriodAmount = function (awardPeriod) { return totalAwardPeriodAmount(awardPeriod, obj); }
        }

        anotherLoanCollection.totalAwardPeriodAmount = function (awardPeriod) { return totalLoanCollectionPeriodAmount(awardPeriod, anotherLoanCollection); };

        awardYearConfigurations = [
            {
                AwardYearCode: ko.observable("2015"),
                IsDeclinedStatusChangeReviewRequired: ko.observable(false),
                IsLoanAmountChangeReviewRequired: ko.observable(false),
                AreAwardingChangesAllowed: ko.observable(),
                AllowAnnualAwardUpdatesOnly: ko.observable(),
                ShoppingSheetConfiguration: ko.observable()
            }
        ];

        finAidConfiguration = {
            AwardYearConfigurations: ko.observable(awardYearConfigurations)
        };

        percentageChartCollection1 = {
            AwardYearCode: ko.observable("2015"),
            PercentageChartModels: ko.observable(
                [
                    {
                        AwardCategoryType: ko.observable("Award"),
                        PercentageForAwardType: ko.observable(30)
                    },
                    {
                        AwardCategoryType: ko.observable("Loan"),
                        PercentageForAwardType: ko.observable(40)
                    },
                    {
                        AwardCategoryType: ko.observable("Work"),
                        PercentageForAwardType: ko.observable(30)
                    }
                ]
            )
        };

        percentageChartCollection2 = {
            AwardYearCode: ko.observable("2016"),
            PercentageChartModels: ko.observable(
                [
                    {
                        AwardCategoryType: ko.observable("Award"),
                        PercentageForAwardType: ko.observable(70)
                    },
                    {
                        AwardCategoryType: ko.observable("Loan"),
                        PercentageForAwardType: ko.observable(20)
                    },
                    {
                        AwardCategoryType: ko.observable("Work"),
                        PercentageForAwardType: ko.observable(10)
                    }
                ]
            )
        };

        awardLetterInfoForYear = testData().awardLetterInfoData;
    });

    afterEach(function () {
        viewModel = null;
        awardPrerequisites = null;
    });

    //#region awardsViewModel observables tests after init
    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":isLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":SpinnerMessage to equal submittingChangeMessage after initialization", function () {
        expect(viewModel.SpinnerMessage()).toEqual(submittingChangeMessage);
    });

    it(":acceptRejectInProgress is false after initialization", function () {
        expect(viewModel.acceptRejectInProgress()).toBeFalsy();
    });

    it(":yearSelector is not null after initialization", function () {        
        expect(viewModel.yearSelector).not.toEqual(null);
    });

    it(":BaseViewModel is not null after initialization", function () {
        expect(viewModel.BaseViewModel).not.toEqual(null);
    });

    it(":sharedDataViewModel is not null after initialization", function () {
        expect(viewModel.sharedDataViewModel).not.toEqual(null);
    });

    it(":StudentAwards to be an array after initialization", function () {
        expect(viewModel.StudentAwards()).toEqual([]);
    });

    it(":StudentSubLoans to be an array after initialization", function () {
        expect(viewModel.StudentSubLoans()).toEqual([]);
    });

    it(":StudentUnsubLoans to be an array after initialization", function () {
        expect(viewModel.StudentUnsubLoans()).toEqual([]);
    });

    it(":OtherLoans to be an array after initialization", function () {
        expect(viewModel.OtherLoans()).toEqual([]);
    });

    it(":StudentGradPlusLoans to be an array after initialization", function () {
        expect(viewModel.StudentGradPlusLoans()).toEqual([]);
    });

    it(":AwardPrerequisites to be an array after initialization", function () {
        expect(viewModel.AwardPrerequisites()).toEqual([]);
    });

    it(":AwardPercentageCharts to be an array after initialization", function () {
        expect(viewModel.AwardPercentageCharts()).toEqual([]);
    });

    it(":LoanRequirements to be undefined after initialization", function () {
        expect(viewModel.LoanRequirements()).toBeUndefined();
    });

    it(":Person to be undefined after initialization", function () {
        expect(viewModel.Person()).toBeUndefined();
    });

    it(":Configuration to be undefined after initialization", function () {
        expect(viewModel.Configuration()).toBeUndefined();
    });

    it(":totalAccordionIsOpen is false after initialization", function () {
        expect(viewModel.totalAccordionIsOpen()).toBeFalsy();
    });

    it(":hasErrors is false after initialization", function () {
        expect(viewModel.hasErrors()).toBeFalsy();
    });

    it(":AwardLetterData is an empty array after initialization", function () {
        expect(viewModel.AwardLetterData()).toEqual([]);
    });

    it(":loanAmountChangeOccurred is false after initialization", function () {
        expect(viewModel.loanAmountChangeOccurred()).toBeFalsy();
    });

    it(":awardChangeOccurred is false after initialization", function () {
        expect(viewModel.awardChangeOccurred()).toBeFalsy();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(awardsAdminActionUrl);
    });
    //#endregion

    //#region arePrerequisitesSatisfiedForSelectedYear tests
    it(":arePrerequisitesSatisfiedForSelectedYear to be true when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.arePrerequisitesSatisfiedForSelectedYear()).toBeTruthy();
    });
    
    it(":arePrerequisitesSatisfiedForSelectedYear to be true when selectedYear is undefined", function () {        
        expect(viewModel.arePrerequisitesSatisfiedForSelectedYear()).toBeTruthy();
    });

    it(":arePrerequisitesSatisfiedForSelectedYear to be true when selectedYear's prereqs are satisfied", function () {
        viewModel.selectedYear(awardYear);
        viewModel.AwardPrerequisites(awardPrerequisites);

        expect(viewModel.arePrerequisitesSatisfiedForSelectedYear()).toBeTruthy();
    });

    it(":arePrerequisitesSatisfiedForSelectedYear to be false when selectedYear's prereqs are not satisfied", function () {
        viewModel.selectedYear(awardYear);
        awardPrerequisites[0].AreAllPrerequisitesSatisfied(false);
        viewModel.AwardPrerequisites(awardPrerequisites);

        expect(viewModel.arePrerequisitesSatisfiedForSelectedYear()).toBeFalsy();
    });

    it(":arePrerequisitesSatisfiedForSelectedYear to be true when there is no matching prereq data", function () {
        viewModel.selectedYear(awardYear);
        awardPrerequisites[0].AwardYearCode("foo");
        viewModel.AwardPrerequisites(awardPrerequisites);

        expect(viewModel.arePrerequisitesSatisfiedForSelectedYear()).toBeTruthy();
    });
    //#endregion

    //#region areChecklistItemsAssigned tests
    it(":areChecklistItemsAssigned to be false when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });

    it(":areChecklistItemsAssigned to be false when selectedYear is undefined", function () {
        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });

    it(":areChecklistItemsAssigned to be true when selectedYear's AreChecklistItemsAssigned flag is set to true", function () {
        viewModel.selectedYear(awardYear);
        expect(viewModel.areChecklistItemsAssigned()).toBeTruthy();
    });

    it(":areChecklistItemsAssigned to be false when selectedYear's AreChecklistItemsAssigned flag is set to false", function () {
        awardYear.AreChecklistItemsAssigned(false);
        viewModel.selectedYear(awardYear);
        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });
    //#endregion

    //#region selectedAwardsForYear tests
    it(":selectedAwardsForYear to be an empty array when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedAwardsForYear()).toEqual([]);
    });

    it(":selectedAwardsForYear to be an empty array when selectedYear is undefined", function () {        
        expect(viewModel.selectedAwardsForYear()).toEqual([]);
    });

    it(":selectedAwardsForYear to have the expected number of awards", function () {        
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedAwardsForYear().length).toEqual(studentAwardsForYear.length);        
    });

    it(":selectedAwardsForYear to be an empty array when there are no matching awards for the year", function () {
        studentAwardCollection.AwardYearCode("na");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedAwardsForYear()).toEqual([]);
    });

    it(":selectedAwardsForYear are the expected ones - StudentAwards contains award collections for more than one year", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedAwardsForYear().length).toEqual(studentAwardCollection.StudentAwardsForYear().length);

        //reset the year
        awardYear.Code("2016");
        expect(viewModel.selectedAwardsForYear().length).toEqual(anotherStudentAwardCollection.StudentAwardsForYear().length);
    });
    //#endregion

    //#region hasAwardsForYearTests
    it(":hasAwardsForYear to be false when there is no matching award collection for the selected year", function () {
        studentAwardCollection.AwardYearCode("na");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAwardsForYear()).toBeFalsy();
    });

    it(":hasAwardsForYear to be false when there are no awards for the selected year", function () {
        studentAwardCollection.StudentAwardsForYear([]);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAwardsForYear()).toBeFalsy();
    });

    it(":hasAwardsForYear to be true when there are awards for the selected year", function () {        
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAwardsForYear()).toBeTruthy();
    });
    //#endregion

    //#region selectedWorkAwardsForYear tests
    it(":selectedWorkAwardsForYear to be an empty array when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedWorkAwardsForYear()).toEqual([]);
    });

    it(":selectedWorkAwardsForYear to be an empty array when selectedYear is undefined", function () {
        expect(viewModel.selectedWorkAwardsForYear()).toEqual([]);
    });

    it(":selectedWorkAwardsForYear to have the expected number of awards", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedWorkAwardsForYear().length).toEqual(studentWorkAwardsForYear.length);
    });

    it(":selectedWorkAwardsForYear to be an empty array when there are no matching awards for the year", function () {
        studentAwardCollection.AwardYearCode("na");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedWorkAwardsForYear()).toEqual([]);
    });
    //#endregion

    //#region hasWorkAwardsForYear tests
    it(":hasWorkAwardsForYear to be false when there is no matching award collection for the selected year", function () {
        studentAwardCollection.AwardYearCode("na");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasWorkAwardsForYear()).toBeFalsy();
    });

    it(":hasWorkAwardsForYear to be false when there are no work awards for the selected year", function () {
        studentAwardCollection.StudentWorkAwardsForYear([]);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasWorkAwardsForYear()).toBeFalsy();
    });

    it(":hasWorkAwardsForYear to be true when there are awards for the selected year", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasWorkAwardsForYear()).toBeTruthy();
    });
    //#endregion

    //#region selectedOtherLoansForYear tests
    it(":selectedOtherLoansForYear to be an empty array when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedOtherLoansForYear()).toEqual([]);
    });

    it(":selectedOtherLoansForYear to be an empty array when selectedYear is undefined", function () {
        expect(viewModel.selectedOtherLoansForYear()).toEqual([]);
    });

    it(":selectedOtherLoansForYear to have the expected number of loans", function () {
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedOtherLoansForYear().length).toEqual(studentAwardsForYear.length);
    });

    it(":selectedOtherLoansForYear to be an empty array when there are no matching loans for the year", function () {
        studentAwardCollection.AwardYearCode("na");
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedOtherLoansForYear()).toEqual([]);
    });

    it(":selectedOtherLoansForYear are the expected ones - OtherLoans contains award collections for more than one year", function () {
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedOtherLoansForYear().length).toEqual(studentAwardCollection.StudentAwardsForYear().length);

        //reset the year
        awardYear.Code("2016");
        expect(viewModel.selectedOtherLoansForYear().length).toEqual(anotherStudentAwardCollection.StudentAwardsForYear().length);
    });
    //#endregion

    //#region hasOtherLoansForYear tests
    it(":hasOtherLoansForYear to be false when there is no matching award collection for the selected year", function () {
        studentAwardCollection.AwardYearCode("na");
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasOtherLoansForYear()).toBeFalsy();
    });

    it(":hasOtherLoansForYear to be false when there are no other loans for the selected year", function () {
        studentAwardCollection.StudentAwardsForYear([]);
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasOtherLoansForYear()).toBeFalsy();
    });

    it(":hasOtherLoansForYear to be true when there are other loans for the selected year", function () {
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasOtherLoansForYear()).toBeTruthy();
    });
    //#endregion

    //#region selectedSubLoansModelForYear tests
    it(":selectedSubLoansModelForYear to be truthy when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedSubLoansModelForYear()).toBeTruthy();
    });

    it(":selectedSubLoansModelForYear to be truthy when selectedYear is undefined", function () {
        expect(viewModel.selectedSubLoansModelForYear()).toBeTruthy();
    });

    it(":selectedSubLoansModelForYear to have the expected number of loans", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedSubLoansModelForYear().Loans().length).toEqual(loanCollection.Loans().length);
    });

    it(":selectedSubLoansModelForYear to be truthy when there is no matching loan collection for the year", function () {
        loanCollection.AwardYearCode("na");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedSubLoansModelForYear()).toBeTruthy();
    });

    it(":selectedSubLoansModelForYear is the expected one - StudentSubLoans contains loan collections for more than one year", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentSubLoans().push(anotherLoanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedSubLoansModelForYear().Loans().length).toEqual(loanCollection.Loans().length);

        //reset the year
        awardYear.Code("2014");
        expect(viewModel.selectedSubLoansModelForYear().Loans().length).toEqual(anotherLoanCollection.Loans().length);
    });
    //#endregion

    //#region hasSubLoanForYear tests
    it(":hasSubLoanForYear to be false when there is no matching sub loan collection for the selected year", function () {
        loanCollection.AwardYearCode("na");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasSubLoanForYear()).toBeFalsy();
    });
    
    it(":hasSubLoanForYear to be true when there is a matcjing sub loan collection for the selected year", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasSubLoanForYear()).toBeTruthy();
    });
    //#endregion

    //#region selectedUnsubLoansModelForYear tests
    it(":selectedUnsubLoansModelForYear to be truthy when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedUnsubLoansModelForYear()).toBeTruthy();
    });

    it(":selectedUnsubLoansModelForYear to be truthy when selectedYear is undefined", function () {
        expect(viewModel.selectedUnsubLoansModelForYear()).toBeTruthy();
    });

    it(":selectedUnsubLoansModelForYear to have the expected number of loans", function () {
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedUnsubLoansModelForYear().Loans().length).toEqual(loanCollection.Loans().length);
    });

    it(":selectedUnsubLoansModelForYear to be truthy when there is no matching loan collection for the year", function () {
        loanCollection.AwardYearCode("na");
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedUnsubLoansModelForYear()).toBeTruthy();
    });

    it(":selectedUnsubLoansModelForYear is the expected one - StudentUnsubLoans contains loan collections for more than one year", function () {
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(anotherLoanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedUnsubLoansModelForYear().Loans().length).toEqual(loanCollection.Loans().length);

        //reset the year
        awardYear.Code("2014");
        expect(viewModel.selectedUnsubLoansModelForYear().Loans().length).toEqual(anotherLoanCollection.Loans().length);
    });
    //#endregion

    //#region hasUnsubLoanForYear tests
    it(":hasUnsubLoanForYear to be false when there is no matching unsub loan collection for the selected year", function () {
        loanCollection.AwardYearCode("na");
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasUnsubLoanForYear()).toBeFalsy();
    });

    it(":hasUnsubLoanForYear to be true when there is a matching unsub loan collection for the selected year", function () {
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasUnsubLoanForYear()).toBeTruthy();
    });
    //#endregion

    //#region selectedGradPlusLoansModelForYear tests
    it(":selectedGradPlusLoansModelForYear to be truthy when selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedGradPlusLoansModelForYear()).toBeTruthy();
    });

    it(":selectedGradPlusLoansModelForYear to be truthy when selectedYear is undefined", function () {
        expect(viewModel.selectedGradPlusLoansModelForYear()).toBeTruthy();
    });

    it(":selectedGradPlusLoansModelForYear to have the expected number of loans", function () {
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedGradPlusLoansModelForYear().Loans().length).toEqual(loanCollection.Loans().length);
    });

    it(":selectedGradPlusLoansModelForYear to be truthy when there is no matching loan collection for the year", function () {
        loanCollection.AwardYearCode("na");
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedGradPlusLoansModelForYear()).toBeTruthy();
    });

    it(":selectedGradPlusLoansModelForYear is the expected one - StudentGradPlusLoans contains loan collections for more than one year", function () {
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(anotherLoanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.selectedGradPlusLoansModelForYear().Loans().length).toEqual(loanCollection.Loans().length);

        //reset the year
        awardYear.Code("2014");
        expect(viewModel.selectedGradPlusLoansModelForYear().Loans().length).toEqual(anotherLoanCollection.Loans().length);
    });
    //#endregion

    //#region hasGradPlusLoansForYear tests
    it(":hasGradPlusLoansForYear to be false when there is no matching grad plus loan collection for the selected year", function () {
        loanCollection.AwardYearCode("na");
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasGradPlusLoansForYear()).toBeFalsy();
    });

    it(":hasGradPlusLoansForYear to be true when there is a matching grad plus loan collection for the selected year", function () {
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasGradPlusLoansForYear()).toBeTruthy();
    });
    //#endregion

    //#region hasAnyAwardTypesForYear tests
    it(":hasAnyAwardTypesForYear is true if hasAwardsForYear is true", function () {
        viewModel.StudentAwards().push(anotherStudentAwardCollection);
        viewModel.selectedYear(awardYear.Code("2016"));
        expect(viewModel.hasAnyAwardTypesForYear()).toBeTruthy();
    });

    it(":hasAnyAwardTypesForYear is true if hasWorkAwardsForYear is true", function () {
        studentAwardCollection.StudentAwardsForYear([]);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyAwardTypesForYear()).toBeTruthy();
    });

    it(":hasAnyAwardTypesForYear is true if hasOtherLoansForYear is true", function () {
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyAwardTypesForYear()).toBeTruthy();
    });

    it(":hasAnyAwardTypesForYear is true if hasSubLoanForYear is true", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyAwardTypesForYear()).toBeTruthy();
    });

    it(":hasAnyAwardTypesForYear is true if hasUnsubLoanForYear is true", function () {
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyAwardTypesForYear()).toBeTruthy();
    });

    it(":hasAnyAwardTypesForYear is true if hasGradPlusLoanForYear is true", function () {
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyAwardTypesForYear()).toBeTruthy();
    });

    it(":hasAnyAwardTypesForYear is false if there are no awards for selected year", function () {
        viewModel.StudentAwards([]);
        viewModel.OtherLoans([]);
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyAwardTypesForYear()).toBeFalsy();
    });
    //#endregion

    //#region hasAnyLoanTypeForYear
    it(":hasAnyLoanTypeForYear is true if there are other loans for the selected year", function () {
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyLoanTypeForYear()).toBeTruthy();
    });

    it(":hasAnyLoanTypeForYear is true if there are sub loans for the selected year", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyLoanTypeForYear()).toBeTruthy();
    });

    it(":hasAnyLoanTypeForYear is true if there are unsub loans for the selected year", function () {
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyLoanTypeForYear()).toBeTruthy();
    });

    it(":hasAnyLoanTypeForYear is true if there are grad plus loans for the selected year", function () {
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyLoanTypeForYear()).toBeTruthy();
    });

    it(":hasAnyLoanTypeForYear is false if there are no loans for the selected year", function () {
        viewModel.OtherLoans([]);
        viewModel.StudentSubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAnyLoanTypeForYear()).toBeFalsy();
    });
    //#endregion

    //#region hasDirectLoanRequirements tests
    it(":hasDirectLoanRequirements is true when no status descriptions for direct loans are equal to Rejected/Denied", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasDirectLoanRequirements()).toBeTruthy();
    });

    it(":hasDirectLoanRequirements is true when not all status descriptions for direct loans are equal to Rejected/Denied", function () {
        loanCollection.StatusDescription("Rejected");
        viewModel.StudentSubLoans().push(loanCollection);

        loanCollection.StatusDescription("Accepted");
        viewModel.StudentUnsubLoans().push(loanCollection);

        viewModel.selectedYear(awardYear);
        expect(viewModel.hasDirectLoanRequirements()).toBeTruthy();
    });

    it(":hasDirectLoanRequirements is false when all status descriptions for direct loans are equal to Rejected/Denied", function () {
        loanCollection.StatusDescription("Rejected");
        viewModel.StudentSubLoans().push(loanCollection);        
        viewModel.StudentUnsubLoans().push(loanCollection);

        viewModel.selectedYear(awardYear);
        expect(viewModel.hasDirectLoanRequirements()).toBeFalsy();
    });
    //#endregion

    //#region hasGradPlusLoanRequirements tests
    it(": hasGradPlusLoanRequirements is true when no status descriptions for grad plus loans are equal to Rejected/Denied", function () {
        viewModel.StudentGradPlusLoans().push(loanCollection);        
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasGradPlusLoanRequirements()).toBeTruthy();
    });
    
    it(": hasGradPlusLoanRequirements is false when all status descriptions for grad plus loans are equal to Rejected/Denied", function () {
        loanCollection.StatusDescription("Denied");
        viewModel.StudentGradPlusLoans().push(loanCollection);        
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasDirectLoanRequirements()).toBeFalsy();
    });
    //#endregion

    //#region allowYearLevelUpdatesOnly tests
    it(":allowYearLevelUpdatesOnly is false when the yearConfiguration is null", function () {
        //viewModel.Configuration().AwardYearConfigurations().push(awardYearConfiguration);
        expect(viewModel.allowYearLevelUpdatesOnly()).toBeFalsy();
    });

    it(":allowYearLevelUpdatesOnly is false when the configuration property is false", function () {
        awardYearConfigurations[0].AllowAnnualAwardUpdatesOnly(false);
        viewModel.Configuration(finAidConfiguration);
        expect(viewModel.allowYearLevelUpdatesOnly()).toBeFalsy();
    });

    it(":allowYearLevelUpdatesOnly is true when the configuration property is true", function () {
        awardYearConfigurations[0].AllowAnnualAwardUpdatesOnly(true);
        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);
        expect(viewModel.allowYearLevelUpdatesOnly()).toBeTruthy();
    });
    //#endregion

    //#region awardsPercentageChartForYear tests
    it(":awardsPercentageChartForYear is null when selectedYear is null", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(null);
        expect(viewModel.awardsPercentageChartForYear()).toBeNull();
    });

    it(":awardsPercentageChartForYear is null when selectedYear is undefined", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);        
        expect(viewModel.awardsPercentageChartForYear()).toBeNull();
    });

    it(":awardsPercentageChartForYear is null when there is no matching chart collection for the selected year", function () {
        percentageChartCollection1.AwardYearCode("na");
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.awardsPercentageChartForYear()).toBeNull();
    });

    it(":awardsPercentageChartForYear is null when there is no award chart in the collection for the selected year", function () {
        percentageChartCollection1.PercentageChartModels([]);
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.awardsPercentageChartForYear()).toBeNull();
    });

    it(":awardsPercentageChartForYear is not null/undefined when there is an award chart for selected year", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.awardsPercentageChartForYear()).not.toBeNull();
        expect(viewModel.awardsPercentageChartForYear()).not.toBeUndefined();
    });

    it(":awardsPercentageChartForYear equals the expected one when there are multiple percentage chart collections", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.AwardPercentageCharts().push(percentageChartCollection2);

        viewModel.selectedYear(awardYear);
        expect(viewModel.awardsPercentageChartForYear().PercentageForAwardType()).toEqual(percentageChartCollection1.PercentageChartModels()[0].PercentageForAwardType());

        //reset the year
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        expect(viewModel.awardsPercentageChartForYear().PercentageForAwardType()).toEqual(percentageChartCollection2.PercentageChartModels()[0].PercentageForAwardType());
    });
    //#endregion

    //#region hasAwardsPercentageChartForYear tests
    it(":hasAwardsPercentageChartForYear is false when there is no award percentage chart for the selected year", function () {
        percentageChartCollection1.PercentageChartModels([
            {
                AwardCategoryType: ko.observable("Loan"),
                PercentageForAwardType: ko.observable(40)
            }
        ]);
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAwardsPercentageChartForYear()).toBeFalsy();
    });

    it(":hasAwardsPercentageChartForYear is true when there is an award percentage chart for the selected year", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasAwardsPercentageChartForYear()).toBeTruthy();
    });
    //#endregion

    //#region workAwardsPercentageChartForYear tests
    it(":workAwardsPercentageChartForYear is null when selectedYear is null", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(null);
        expect(viewModel.workAwardsPercentageChartForYear()).toBeNull();
    });

    it(":workAwardsPercentageChartForYear is null when selectedYear is undefined", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        expect(viewModel.workAwardsPercentageChartForYear()).toBeNull();
    });

    it(":workAwardsPercentageChartForYear is null when there is no matching chart collection for the selected year", function () {
        percentageChartCollection1.AwardYearCode("na");
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.workAwardsPercentageChartForYear()).toBeNull();
    });

    it(":workAwardsPercentageChartForYear is null when there is no work award chart in the collection for the selected year", function () {
        percentageChartCollection1.PercentageChartModels([]);
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.workAwardsPercentageChartForYear()).toBeNull();
    });

    it(":workAwardsPercentageChartForYear is not null/undefined when there is an work award chart for selected year", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.workAwardsPercentageChartForYear()).not.toBeNull();
        expect(viewModel.workAwardsPercentageChartForYear()).not.toBeUndefined();
    });

    it(":workAwardsPercentageChartForYear equals the expected one when there are multiple percentage chart collections", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.AwardPercentageCharts().push(percentageChartCollection2);

        viewModel.selectedYear(awardYear);
        expect(viewModel.workAwardsPercentageChartForYear().PercentageForAwardType()).toEqual(percentageChartCollection1.PercentageChartModels()[2].PercentageForAwardType());

        //reset the year
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        expect(viewModel.workAwardsPercentageChartForYear().PercentageForAwardType()).toEqual(percentageChartCollection2.PercentageChartModels()[2].PercentageForAwardType());
    });
    //#endregion

    //#region hasWorkAwardsPercentageChartForYear tests
    it(":hasWorkAwardsPercentageChartForYear is false when there is no work award percentage chart for the selected year", function () {
        percentageChartCollection1.PercentageChartModels([
            {
                AwardCategoryType: ko.observable("Award"),
                PercentageForAwardType: ko.observable(50)
            }
        ]);
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasWorkAwardsPercentageChartForYear()).toBeFalsy();
    });

    it(":hasWorkAwardsPercentageChartForYear is true when there is a work award percentage chart for the selected year", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasWorkAwardsPercentageChartForYear()).toBeTruthy();
    });
    //#endregion

    //#region loansPercentageChartForYear tests
    it(":loansPercentageChartForYear is null when selectedYear is null", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(null);
        expect(viewModel.loansPercentageChartForYear()).toBeNull();
    });

    it(":loansPercentageChartForYear is null when selectedYear is undefined", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        expect(viewModel.loansPercentageChartForYear()).toBeNull();
    });

    it(":loansPercentageChartForYear is null when there is no matching chart collection for the selected year", function () {
        percentageChartCollection1.AwardYearCode("na");
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.loansPercentageChartForYear()).toBeNull();
    });

    it(":loansPercentageChartForYear is null when there is no loan chart in the collection for the selected year", function () {
        percentageChartCollection1.PercentageChartModels([]);
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.loansPercentageChartForYear()).toBeNull();
    });

    it(":loansPercentageChartForYear is not null/undefined when there is an loan chart for selected year", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.loansPercentageChartForYear()).not.toBeNull();
        expect(viewModel.loansPercentageChartForYear()).not.toBeUndefined();
    });

    it(":loansPercentageChartForYear equals the expected one when there are multiple percentage chart collections", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.AwardPercentageCharts().push(percentageChartCollection2);

        viewModel.selectedYear(awardYear);
        expect(viewModel.loansPercentageChartForYear().PercentageForAwardType()).toEqual(percentageChartCollection1.PercentageChartModels()[1].PercentageForAwardType());

        //reset the year
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        expect(viewModel.loansPercentageChartForYear().PercentageForAwardType()).toEqual(percentageChartCollection2.PercentageChartModels()[1].PercentageForAwardType());
    });
    //#endregion

    //#region hasLoansPercentageChartForYear tests
    it(":hasLoansPercentageChartForYear is false when there is no loan percentage chart for the selected year", function () {
        percentageChartCollection1.PercentageChartModels([
            {
                AwardCategoryType: ko.observable("Award"),
                PercentageForAwardType: ko.observable(50)
            }
        ]);
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasLoansPercentageChartForYear()).toBeFalsy();
    });

    it(":hasLoansPercentageChartForYear is true when there is a loan percentage chart for the selected year", function () {
        viewModel.AwardPercentageCharts().push(percentageChartCollection1);
        viewModel.selectedYear(awardYear);
        expect(viewModel.hasLoansPercentageChartForYear()).toBeTruthy();
    });
    //#endregion

    //#region awardsTotalAmountForYear tests
    it(":awardsTotalAmountForYear is 0 when there are no awards for the selected year", function () {
        viewModel.StudentAwards([]);
        viewModel.selectedYear(awardYear);
        expect(viewModel.awardsTotalAmountForYear()).toEqual(0);
    });

    it(":awardsTotalAmountForYear equals expected awards amount total for the selected year", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        var total = 0;
        for (var i = 0; i < studentAwardsForYear.length; i++) {
            total += studentAwardsForYear[i].totalAmount();
        }
        expect(viewModel.awardsTotalAmountForYear()).toEqual(total);
    });
    //#endregion

    //#region workAwardsTotalAmountForYear tests
    it(":workAwardsTotalAmountForYear is 0 when there are no work awards for the selected year", function () {
        viewModel.StudentAwards().push(anotherStudentAwardCollection);
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        expect(viewModel.workAwardsTotalAmountForYear()).toEqual(0);
    });

    it(":workAwardsTotalAmountForYear equals expected work awards amount total for the selected year", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        var total = 0;
        for (var i = 0; i < studentWorkAwardsForYear.length; i++) {
            total += studentWorkAwardsForYear[i].totalAmount();
        }
        expect(viewModel.workAwardsTotalAmountForYear()).toEqual(total);
    });
    //#endregion

    //#region loansTotalAmountForYear tests
    it(":loansTotalAmountForYear is 0 when there are no loans for the selected year", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);
        viewModel.selectedYear(awardYear);
        expect(viewModel.loansTotalAmountForYear()).toEqual(0);
    });

    it(":loansTotalAmountForYear equals expected loan amount total for the selected year", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        anotherLoanCollection.AwardYearCode("2015");
        viewModel.StudentUnsubLoans().push(anotherLoanCollection);
        loanCollection.totalAmount(9000);
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        var total = 0;
        for (var i = 0; i < studentAwardsForYear.length; i++) {
            total += studentAwardsForYear[i].totalAmount();
        }
        total += loanCollection.totalAmount() + anotherLoanCollection.totalAmount() + 9000;
        
        expect(viewModel.loansTotalAmountForYear()).toEqual(total);
    });
    //#endregion

    //#region distinctAwardPeriodsForYear tests
    it(":distinctAwardPeriodsForYear returns true if the selected year is null", function () {
        viewModel.AwardYears().push(awardYear);
        viewModel.selectedYear(null);
        expect(viewModel.distinctAwardPeriodsForYear()).toBeTruthy();
    });

    it(":distinctAwardPeriodsForYear returns true if the selected year is undefined", function () {
        viewModel.AwardYears().push(awardYear);        
        expect(viewModel.distinctAwardPeriodsForYear()).toBeTruthy();
    });

    it(":distinctAwardPeriodsForYear returns expected number of periods for the selected year", function () {
        viewModel.AwardYears().push(awardYear);
        viewModel.AwardYears().push(anotherAwardYear);

        viewModel.selectedYear(awardYear);
        expect(ko.utils.unwrapObservable(viewModel.distinctAwardPeriodsForYear()).length).toEqual(awardYear.DistinctAwardPeriods().length);

        //reset selected award year
        viewModel.selectedYear(anotherAwardYear);
        expect(ko.utils.unwrapObservable(viewModel.distinctAwardPeriodsForYear()).length).toEqual(anotherAwardYear.DistinctAwardPeriods().length);
    });
    //#endregion

    //#region totalAwardPeriodAmount tests
    it(":totalAwardPeriodAmount equals 0 when there are no awards or loans for the selected year", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);
        viewModel.StudentAwards([]);
        viewModel.selectedYear(awardYear);

        var awardPeriod = {
            Code: ko.observable("15/FA")
        }

        expect(viewModel.totalAwardPeriodAmount(awardPeriod)).toEqual(0);
    });

    it(":totalAwardPeriodAmount equals expected award period amount for 15/FA period", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        var total = 0;
        for (var i = 0; i < studentAwardsForYear.length; i++) {
            total += (studentAwardsForYear[i].totalAwardPeriodAmount(awardPeriod)) * 2;  //other loans + awards, hence *2
        }
        for (var i = 0; i < studentWorkAwardsForYear.length; i++) {
            total += (studentWorkAwardsForYear[i].totalAwardPeriodAmount(awardPeriod));
        }
        total += (loanCollection.totalAwardPeriodAmount(awardPeriod) * 3);  //sub, unsub, gradPlus loans
        expect(viewModel.totalAwardPeriodAmount(awardPeriod)).toEqual(total);
    });    
    //#endregion

    //#region hasUnacceptedAwards tests
    it(":hasUnacceptedAwards is false if all awards for the year are accepted/rejected", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        studentAwardCollection.StudentWorkAwardsForYear([]);
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasUnacceptedAwards()).toBeFalsy();
    });

    it(":hasUnacceptedAwards is true if at least one loan for the selected year is pending", function () {
        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasUnacceptedAwards()).toBeTruthy();
    });

    it(":hasUnacceptedAwards is true if at least one award for the selected year is pending", function () {        
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        studentAwardCollection.StudentAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasUnacceptedAwards()).toBeTruthy();
    });

    it(":hasUnacceptedAwards is true if at least one work award for the selected year is pending", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        studentAwardCollection.StudentWorkAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasUnacceptedAwards()).toBeTruthy();
    });

    it(":hasUnacceptedAwards is true if at least one other loan for the selected year is pending", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        studentAwardCollection.StudentAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.OtherLoans().push(studentAwardCollection);        
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasUnacceptedAwards()).toBeTruthy();
    });
    //#endregion

    //#region hasAwardsWithErrors tests
    it(":hasAwardsWithErrors is false when none of the awards for the selected year has errors", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasAwardsWithErrors()).toBeFalsy();
    });

    it(":hasAwardsWithErrors is true when loan's total exceeds the max amount", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);

        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        loanCollection.MaximumAmount(0);

        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasAwardsWithErrors()).toBeTruthy();
    });

    it(":hasAwardsWithErrors is true when one of the awards' totals is 0 and that award IsStatusModifiable", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);        
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        studentAwardCollection.StudentAwardsForYear()[0].totalAmount(0);
        studentAwardCollection.StudentAwardsForYear()[0].IsStatusModifiable(true);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasAwardsWithErrors()).toBeTruthy();
    });

    it(":hasAwardsWithErrors is true when one of the loanCollection totals is 0 and that loanCollection IsAnyAmountModifiable", function () {        
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);        
        loanCollection.totalAmount(0);
        viewModel.StudentSubLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.hasAwardsWithErrors()).toBeTruthy();
    });
    //#endregion

    //#region isAnyStatusModifiable tests
    it(":isAnyStatusModifiable is false when no award for the year IsStatusModifiable", function () {
        //Set all IsStatusModifiable flags to false
        loanCollection.IsStatusModifiable(false);
        for (var i = 0; i < studentAwardsForYear.length; i++) {
            studentAwardsForYear[i].IsStatusModifiable(false);
        }
        for (var i = 0; i < studentWorkAwardsForYear.length; i++) {
            studentWorkAwardsForYear[i].IsStatusModifiable(false);
        }

        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.isAnyStatusModifiable()).toBeFalsy();
    });

    it(":isAnyStatusModifiable is true if at least one loanCollection IsStatusModifiable", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        loanCollection.IsStatusModifiable(true);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.isAnyStatusModifiable()).toBeTruthy();
    });

    it(":isAnyStatusModifiable is true if at least one award IsStatusModifiable", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);        
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(studentAwardCollection);
        studentAwardCollection.StudentWorkAwardsForYear()[0].IsStatusModifiable(true);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        expect(viewModel.isAnyStatusModifiable()).toBeTruthy();
    });
    //#endregion

    //#region tableColumnCount tests
    it("tableColumnCount equals expected number of columns", function () {
        viewModel.AwardYears().push(awardYear);
        viewModel.selectedYear(awardYear);
        var count = awardYear.DistinctAwardPeriods().length + 3;
        expect(viewModel.tableColumnCount()).toEqual(count);
    });

    it("tableColumnCount is truthy when the distinctAwardPeriodsForYear is truthy(undefined)", function () {
        viewModel.AwardYears().push(awardYear);
        viewModel.selectedYear(null);        
        expect(viewModel.tableColumnCount()).toBeTruthy();
    });
    //#endregion

    //#region totalLoanAmount tests
    it(":totalLoanAmount equals expected amount for the selected year", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);

        var total = loanCollection.totalAmount() * 3; //using the same loanCollection for all three loan cats

        expect(viewModel.totalLoanAmount()).toEqual(total);
    });

    it(":totalLoanAmount equals 0 if there are no loans for the year", function () {
        viewModel.selectedYear(awardYear);
        expect(viewModel.totalLoanAmount()).toEqual(0);
    });
    //#endregion

    //#region totalAmount tests
    it(":totalAmount is 0 if there are no awards/loans for the year", function () {
        viewModel.selectedYear(awardYear);
        expect(viewModel.totalAmount()).toEqual(0);
    });

    it(":totalAmount equals expected amount for the year", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        //Get the total
        var total = 0;
        total += (loanCollection.totalAmount() * 3);
        for (var i = 0; i < studentAwardsForYear.length; i++) {
            //studentAwardsForYear are used for awards and other loans, hence *2
            total += (studentAwardsForYear[i].totalAmount() * 2);   
        }
        for (var i = 0; i < studentWorkAwardsForYear.length; i++) {
            total += studentWorkAwardsForYear[i].totalAmount();
        }

        expect(viewModel.totalAmount()).toEqual(total);
    });
    //#endregion

    //#region flipTotalAccordion tests
    it(":flipTotalAccordion sets the value of totalAccordionIsOpen to true if the flag is initially false", function () {        
        viewModel.flipTotalAccordion();

        expect(viewModel.totalAccordionIsOpen()).toBeTruthy();
    });

    it(":flipTotalAccordion sets the value of totalAccordionIsOpen to false if the flag is initially true", function () {
        viewModel.totalAccordionIsOpen(true);
        viewModel.flipTotalAccordion();

        expect(viewModel.totalAccordionIsOpen()).toBeFalsy();
    });
    //#endregion

    //#region totalAccordionRows tests
    it(":totalAccordionRows equals 2 if totalAccordionIsOpen is false", function () {        
        expect(viewModel.totalAccordionRows()).toEqual(2);
    });

    it(":totalAccordionRows equals 4 if totalAccordionIsOpen is true", function () {
        viewModel.totalAccordionIsOpen(true);
        expect(viewModel.totalAccordionRows()).toEqual(4);
    });
    //#endregion

    //#region awardPeriodColumnWidth tests
    it(":awardPeriodColumnWidth equals 0 when tableColumnCount is 3", function () {
        awardYear.DistinctAwardPeriods([]);
        viewModel.AwardYears().push(awardYear);
        viewModel.selectedYear(awardYear);

        var expectedWidth = "0%";
        expect(viewModel.awardPeriodColumnWidth()).toEqual(expectedWidth);
    });

    it(":awardPeriodColumnWidth equals expected when tableColumnCount is greater than 3", function () {        
        viewModel.AwardYears().push(awardYear);
        viewModel.selectedYear(awardYear);

        var expectedWidth = (55 / (viewModel.tableColumnCount() - 3)).toString() + "%";
        expect(viewModel.awardPeriodColumnWidth()).toEqual(expectedWidth);
    });
    //#endregion

    //#region currencySymbol tests
    it(":currencySymbol returns expected symbol", function () {
        var expectedSymbol = Globalize.culture().numberFormat.currency.symbol;
        expect(viewModel.currencySymbol()).toEqual(expectedSymbol);
    });
    //#endregion

    //#region cancelHandler tests
    it(":cancelHandler resets award's IsActive flags to original values", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        
        var award = viewModel.StudentAwards()[0].StudentAwardsForYear()[0];
        award.StudentAwardPeriods()[0].IsActive(false);
        //Make sure the value is set to false
        expect(award.StudentAwardPeriods()[0].IsActive()).toBeFalsy();       

        //trigger the cancelHandler
        viewModel.cancelHandler(award, null);
        expect(award.StudentAwardPeriods()[0].IsActive()).toBeTruthy();
    });

    it(":cancelHandler resets loan's IsActive flags to original values and AwardAmount to 0 if loan period is Rejected/Denied", function () {
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);

        var collection = viewModel.StudentSubLoans()[0];
        collection.Loans()[0].StudentAwardPeriods()[0].IsActive(false);
        collection.Loans()[0].StudentAwardPeriods()[0].AwardStatus.Category("Rejected");
        //Make sure the value is set to false
        expect(collection.Loans()[0].StudentAwardPeriods()[0].IsActive()).toBeFalsy();

        //trigger the cancelHandler
        viewModel.cancelHandler(collection, null);
        expect(collection.Loans()[0].StudentAwardPeriods()[0].IsActive()).toBeTruthy();
        expect(collection.Loans()[0].StudentAwardPeriods()[0].AwardAmount()).toEqual(0);
    });
    //#endregion

    //#region acceptRejectLoanCollectionHandler tests
    it(":acceptRejectLoanCollectionHandler returns false if loanCollection is null", function () {
        spyOn(viewModel, "acceptRejectInProgress");
        expect(viewModel.acceptRejectLoanCollectionHandler(null, null)).toBeFalsy();
        expect(viewModel.acceptRejectInProgress).not.toHaveBeenCalled();
    });

    it(":acceptRejectLoanCollectionHandler sets correct values to observables on Accept", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        event = { currentTarget: { name: "Accept" } };

        viewModel.acceptRejectLoanCollectionHandler(loanCollection, event);
        expect(viewModel.SpinnerMessage()).toEqual(faAwardsAcceptSpinnerMessage);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectLoanCollectionHandler sets correct values to observables on Reject", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        event = { currentTarget: { name: "Reject" } };

        viewModel.acceptRejectLoanCollectionHandler(loanCollection, event);
        expect(viewModel.SpinnerMessage()).toEqual(faAwardsRejectSpinnerMessage);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectLoanCollectionHandler sets correct values to observables on Change", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        event = { currentTarget: { name: "Change" } };

        viewModel.acceptRejectLoanCollectionHandler(loanCollection, event);
        expect(viewModel.SpinnerMessage()).toEqual(faAwardsSubmittingChangesMessage);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectLoanCollectionHandler calls updateAwardsFromViewModel and sets loanCollectionUpdated flag to true", function () {
        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(viewModel);
        });
        spyOn(viewModel, "updateAwardsFromViewModel");
        spyOn(loanCollection, "loanCollectionUpdated");
        event = { currentTarget: { name: "Change" } };

        viewModel.acceptRejectLoanCollectionHandler(loanCollection, event);
        expect(viewModel.updateAwardsFromViewModel).toHaveBeenCalled();
        expect(loanCollection.loanCollectionUpdated).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectLoanCollectionHandler prevents loanCollection accordion to be toggled and stops loading if any error ocurred", function () {
        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);
        spyOn($, "ajax").and.callFake(function (e) {            
            e.error({ status: 0 }, "", "");
            e.complete({}, "");
        });
        spyOn(viewModel, "acceptRejectInProgress");
        spyOn(viewModel, "isLoading");
        spyOn(global, "rearrangeMultiAccordionElements");
        spyOn(loanCollection, "flipAccordion");

        viewModel.acceptRejectLoanCollectionHandler(loanCollection, event);
        expect(global.rearrangeMultiAccordionElements).toHaveBeenCalled();
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(false);
        expect(viewModel.isLoading).toHaveBeenCalledWith(false);
        expect(loanCollection.flipAccordion).not.toHaveBeenCalled();
    });

    it(":acceptRejectLoanCollectionHandler flips the loan accordion and stops loading on successful completion", function () {
        viewModel.Configuration(finAidConfiguration);        
        viewModel.selectedYear(awardYear);

        spyOn($, "ajax").and.callFake(function (e) {
            e.success(viewModel);
            e.complete({}, "");
        });
        spyOn(viewModel, "updateAwardsFromViewModel");
        spyOn(viewModel, "acceptRejectInProgress");
        spyOn(viewModel, "isLoading");
        spyOn(global, "rearrangeMultiAccordionElements");
        spyOn(loanCollection, "flipAccordion");

        event = { currentTarget: { name: "Accept" } };

        viewModel.acceptRejectLoanCollectionHandler(loanCollection, event);

        expect(global.rearrangeMultiAccordionElements).toHaveBeenCalled();
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(false);
        expect(viewModel.isLoading).toHaveBeenCalledWith(false);
        expect(loanCollection.flipAccordion).toHaveBeenCalled();
    });
    //#endregion

    //#region acceptRejectHandler tests
    it(":acceptRejectHandler returns true if award is null", function () {
        spyOn(viewModel, "acceptRejectInProgress");
        expect(viewModel.acceptRejectHandler(null, null)).toBeTruthy();
        expect(viewModel.acceptRejectInProgress).not.toHaveBeenCalled();
    });

    it(":acceptRejectHandler sets correct values to observables on Accept", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        event = { currentTarget: { name: "Accept" } };

        viewModel.acceptRejectHandler(studentAwardsForYear[0], event);
        expect(viewModel.SpinnerMessage()).toEqual(faAwardsAcceptSpinnerMessage);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectHandler sets correct values to observables on Reject", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        event = { currentTarget: { name: "Reject" } };

        viewModel.acceptRejectHandler(studentAwardsForYear[1], event);
        expect(viewModel.SpinnerMessage()).toEqual(faAwardsRejectSpinnerMessage);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectHandler calls updateAwardsFromViewModel and sets awardUpdated flag to true", function () {
        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(viewModel);
        });
        spyOn(viewModel, "updateAwardsFromViewModel");
        spyOn(studentAwardsForYear[0], "awardUpdated");
        event = { currentTarget: { name: "Accept" } };

        viewModel.acceptRejectHandler(studentAwardsForYear[0], event);
        expect(viewModel.updateAwardsFromViewModel).toHaveBeenCalled();
        expect(studentAwardsForYear[0].awardUpdated).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectHandler prevents award's accordion from being toggled and stops loading if any error ocurred", function () {
        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);
        spyOn($, "ajax").and.callFake(function (e) {
            e.error({ status: 0 }, "", "");
            e.complete({}, "");
        });
        spyOn(viewModel, "acceptRejectInProgress");
        spyOn(viewModel, "isLoading");
        spyOn(global, "rearrangeMultiAccordionElements");
        spyOn(studentAwardsForYear[0], "flipAccordion");

        viewModel.acceptRejectHandler(studentAwardsForYear[0], event);

        expect(global.rearrangeMultiAccordionElements).toHaveBeenCalled();
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(false);
        expect(viewModel.isLoading).toHaveBeenCalledWith(false);
        expect(studentAwardsForYear[0].flipAccordion).not.toHaveBeenCalled();
    });

    it(":acceptRejectHandler flips the award's accordion and stops loading on successful completion", function () {
        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);

        spyOn($, "ajax").and.callFake(function (e) {
            e.success(viewModel);
            e.complete({}, "");
        });
        spyOn(viewModel, "updateAwardsFromViewModel");
        spyOn(viewModel, "acceptRejectInProgress");
        spyOn(viewModel, "isLoading");
        spyOn(global, "rearrangeMultiAccordionElements");
        spyOn(studentAwardsForYear[0], "flipAccordion");

        event = { currentTarget: { name: "Accept" } };

        viewModel.acceptRejectHandler(studentAwardsForYear[0], event);

        expect(global.rearrangeMultiAccordionElements).toHaveBeenCalled();
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(false);
        expect(viewModel.isLoading).toHaveBeenCalledWith(false);
        expect(studentAwardsForYear[0].flipAccordion).toHaveBeenCalled();
    });
    //#endregion

    //#region acceptRejectAllHandler tests
    it(":acceptRejectAllHandler returns true if the action is Accept and there are awards with errors", function () {
        studentAwardCollection.StudentAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        loanCollection.MaximumAmount(0);
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);

        event = { currentTarget: { name: "Accept" } };
        spyOn(viewModel, "acceptRejectInProgress");        

        expect(viewModel.acceptRejectAllHandler(null, event)).toBeTruthy();
        expect(viewModel.acceptRejectInProgress).not.toHaveBeenCalled();
    });

    it(":acceptRejectAllHandler sets correct values to observables on Accept", function () {
        studentAwardCollection.StudentAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        event = { currentTarget: { name: "Accept" } };

        viewModel.acceptRejectAllHandler(null, event);
        expect(viewModel.SpinnerMessage()).toEqual(faAwardsAcceptAllSpinnerMessage);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectAllHandler sets correct values to observables on Reject", function () {
        studentAwardCollection.StudentAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        event = { currentTarget: { name: "Reject" } };

        viewModel.acceptRejectAllHandler(null, event);
        expect(viewModel.SpinnerMessage()).toEqual(faAwardsRejectAllSpinnerMessage);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectAllHandler calls updateAwardsFromViewModel and sets loanCollection/awardUpdated flags to true", function () {
        studentAwardCollection.StudentAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.OtherLoans().push(studentAwardCollection);

        loanCollection.Loans()[0].AwardStatus.Category("Estimated");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);
        
        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);

        spyOn($, "ajax").and.callFake(function (e) {
            e.success(viewModel);
        });
        spyOn(viewModel, "updateAwardsFromViewModel");
        spyOn(studentAwardsForYear[0], "awardUpdated");
        spyOn(loanCollection, "loanCollectionUpdated");
        event = { currentTarget: { name: "Accept" } };

        viewModel.acceptRejectAllHandler(null, event);
        expect(viewModel.updateAwardsFromViewModel).toHaveBeenCalled();
        expect(studentAwardsForYear[0].awardUpdated).toHaveBeenCalledWith(true);
        expect(loanCollection.loanCollectionUpdated).toHaveBeenCalledWith(true);
    });

    it(":acceptRejectAllHandler stops loading and does not toggle total accordion on error", function () {
        studentAwardCollection.StudentWorkAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.OtherLoans().push(studentAwardCollection);

        loanCollection.Loans()[0].AwardStatus.Category("Estimated");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);
        event = { currentTarget: { name: "Reject" } };

        spyOn($, "ajax").and.callFake(function (e) {
            e.error({ status: 0 }, "", "");
            e.complete(null, null);
        });
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        spyOn(viewModel, "flipTotalAccordion");

        viewModel.acceptRejectAllHandler(null, event);
        expect(viewModel.isLoading).toHaveBeenCalledWith(false);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(false);
        expect(viewModel.flipTotalAccordion).not.toHaveBeenCalled();

    });

    it(":acceptRejectAllHandler stops loading and toggles totalAccordion on successful completion", function () {
        studentAwardCollection.StudentWorkAwardsForYear()[0].AwardStatus.Category("Pending");
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.OtherLoans().push(studentAwardCollection);

        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.Configuration(finAidConfiguration);
        viewModel.selectedYear(awardYear);
        event = { currentTarget: { name: "Reject" } };

        spyOn($, "ajax").and.callFake(function (e) {
            e.success(viewModel);
            e.complete(null, null);
        });

        spyOn(viewModel, "updateAwardsFromViewModel");
        spyOn(viewModel, "isLoading");
        spyOn(viewModel, "acceptRejectInProgress");
        spyOn(viewModel, "flipTotalAccordion");

        viewModel.acceptRejectAllHandler(null, event);
        expect(viewModel.isLoading).toHaveBeenCalledWith(false);
        expect(viewModel.acceptRejectInProgress).toHaveBeenCalledWith(false);
        expect(viewModel.flipTotalAccordion).toHaveBeenCalled();
    });
    //#endregion

    //#region processAmountChange tests
    it(":processAmountChange sets error flags to true when there is an error", function () {
        var loan = loanCollection.Loans()[0];
        var parents = [
            {},
            loan,
            loanCollection
        ];
        var message = "input error";
        var awardPeriod = loan.StudentAwardPeriods()[0];

        loanChangeAmount = ko.observable("55");
        loanChangeAmount.hasError = ko.observable(true);
        loanChangeAmount.validationMessage = message;
        spyOn(loan, "hasInputErrors");
        spyOn(loan, "inputErrorMessage")
        spyOn(loanCollection, "hasInputErrors");

        viewModel.processAmountChange(parents, loanChangeAmount, awardPeriod);
        expect(loan.hasInputErrors).toHaveBeenCalledWith(true);
        expect(loan.inputErrorMessage).toHaveBeenCalledWith(message);
        expect(loanCollection.hasInputErrors).toHaveBeenCalledWith(true);
        expect(viewModel.hasErrors()).toBeTruthy();
    });

    it(":processAmountChange sets error flags to false when there is no error", function () {
        var loan = loanCollection.Loans()[0];
        var parents = [
            {},
            loan,
            loanCollection
        ];
        var awardPeriod = loan.StudentAwardPeriods()[0];
        loanChangeAmount = ko.observable("55");
        loanChangeAmount.hasError = ko.observable(false);
        
        spyOn(loan, "hasInputErrors");        
        spyOn(loanCollection, "hasInputErrors");

        viewModel.processAmountChange(parents, loanChangeAmount, awardPeriod);
        expect(loan.hasInputErrors).toHaveBeenCalledWith(false);        
        expect(loanCollection.hasInputErrors).toHaveBeenCalledWith(false);
        expect(viewModel.hasErrors()).toBeFalsy();
    });


    it(":processAmountChange updates AwardAmount when there is no error", function () {
        var loan = loanCollection.Loans()[0];
        var parents = [
            {},
            loan,
            loanCollection
        ];
        var awardPeriod = loan.StudentAwardPeriods()[0];
        loanChangeAmount = ko.observable("88.88");
        loanChangeAmount.hasError = ko.observable(false);
              
        viewModel.processAmountChange(parents, loanChangeAmount, awardPeriod);
        expect(awardPeriod.AwardAmount()).toEqual(88.88);
    });

    it(":processAmountChange doesn't update AwardAmount when there is an error", function () {
        var loan = loanCollection.Loans()[0];
        var parents = [
            {},
            loan,
            loanCollection
        ];
        var awardPeriod = loan.StudentAwardPeriods()[0];
        var initialValue = ko.utils.unwrapObservable(awardPeriod.AwardAmount());
        loanChangeAmount = ko.observable("88");
        loanChangeAmount.hasError = ko.observable(true);
        awardPeriod.AwardAmount(initialValue);

        viewModel.processAmountChange(parents, loanChangeAmount, awardPeriod);
        expect(awardPeriod.AwardAmount()).toEqual(initialValue);
    });
    //#endregion

    //#region processTotalAmountChange tests
    it(":processTotalAmountChange calls setError method if totalLoanChangeAmount has errors", function () {
        totalLoanChangeAmount = ko.observable("100");
        totalLoanChangeAmount.hasError = ko.observable(true);
        var message = "error";
        totalLoanChangeAmount.validationMessage = message;        
        var loan = loanCollection.Loans()[0];

        spyOn(viewModel, "setError");

        viewModel.processTotalAmountChange(loanCollection, loan, totalLoanChangeAmount);
        expect(viewModel.setError).toHaveBeenCalledWith(loan, loanCollection, message);
    });

    it(":processTotalAmountChange sets error flags to false if the totalLoanChangeAmount does not have errors", function () {
        totalLoanChangeAmount = ko.observable("100");
        totalLoanChangeAmount.hasError = ko.observable(false);
        
        var loan = loanCollection.Loans()[0];

        spyOn(loan, "hasInputErrors");
        spyOn(loanCollection, "hasInputErrors");

        viewModel.processTotalAmountChange(loanCollection, loan, totalLoanChangeAmount);
        expect(loan.hasInputErrors).toHaveBeenCalledWith(false);
        expect(loanCollection.hasInputErrors).toHaveBeenCalledWith(false);
        expect(viewModel.hasErrors()).toBeFalsy();

    });

    it(":processTotalAmountChange calls setError if the new amount is less than amount that cannot be modified", function () {
        var loan = loanCollection.Loans()[0];        
        loan.StudentAwardPeriods()[0].IsAmountModifiable(false);
        totalLoanChangeAmount = ko.observable("5");
        totalLoanChangeAmount.hasError = ko.observable(false);

        spyOn(totalLoanChangeAmount, "hasError");
        spyOn(viewModel, "setError");
        
        viewModel.processTotalAmountChange(loanCollection, loan, totalLoanChangeAmount);
        var message = "Insufficient amount";
        expect(viewModel.setError).toHaveBeenCalledWith(loan, loanCollection, message);
    });
    //#endregion

    //#region setError tests
    it(":setError sets error flags to true and updates the error message", function () {
        var award = loanCollection.Loans()[0];
        var message = "Error message";

        spyOn(award, "hasInputErrors");
        spyOn(loanCollection, "hasInputErrors");
        spyOn(award, "inputErrorMessage");

        viewModel.setError(award, loanCollection, message);
        expect(award.hasInputErrors).toHaveBeenCalledWith(true);
        expect(loanCollection.hasInputErrors).toHaveBeenCalledWith(true);
        expect(award.inputErrorMessage).toHaveBeenCalledWith(message);
        expect(viewModel.hasErrors()).toBeTruthy();
    });
    //#endregion

    //#region disableBulkAction
    it(":disableBulkAction returns true if all awards are accepted/rejected", function () {
        //all awards are accepted/rejected:
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        studentAwardCollection.StudentWorkAwardsForYear([]);
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);

        //all the rest of conditions are false:
        viewModel.acceptRejectInProgress(false);
        viewModel.hasErrors(false);
        awardYearConfigurations[0].AreAwardingChangesAllowed(true);
        viewModel.Configuration(finAidConfiguration);
        viewModel.isAdmin(false);
        viewModel.IsProxyView(false);

        expect(viewModel.disableBulkAction()).toBe(true);
    });

    it(":disableBulkAction returns true if acceptRejectInProgress flag is true", function () {
        viewModel.acceptRejectInProgress(true);

        //all the rest of conditions are false:
        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        viewModel.hasErrors(false);
        awardYearConfigurations[0].AreAwardingChangesAllowed(true);
        viewModel.Configuration(finAidConfiguration);
        viewModel.isAdmin(false);
        viewModel.IsProxyView(false);

        expect(viewModel.disableBulkAction()).toBe(true);
    });

    it(":disableBulkAction returns true if hasErrors flag is true", function () {
        viewModel.hasErrors(true);

        //all the rest of conditions are false:
        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        viewModel.acceptRejectInProgress(false);
        awardYearConfigurations[0].AreAwardingChangesAllowed(true);
        viewModel.Configuration(finAidConfiguration);
        viewModel.isAdmin(false);
        viewModel.IsProxyView(false);

        expect(viewModel.disableBulkAction()).toBe(true);
    });

    it(":disableBulkAction returns true if awarding changes are not allowed for the year", function () {
        awardYearConfigurations[0].AreAwardingChangesAllowed(false);
        viewModel.Configuration(finAidConfiguration);

        //all the rest of conditions are false:
        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        viewModel.acceptRejectInProgress(false);
        viewModel.hasErrors(false);
        viewModel.isAdmin(false);
        viewModel.IsProxyView(false);

        expect(viewModel.disableBulkAction()).toBe(true);
    });

    it(":disableBulkAction returns true if isAdmin flag is true", function () {
        viewModel.isAdmin(true);

        //all the rest of conditions are false:
        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        viewModel.acceptRejectInProgress(false);
        awardYearConfigurations[0].AreAwardingChangesAllowed(true);
        viewModel.Configuration(finAidConfiguration);
        viewModel.hasErrors(false);
        viewModel.IsProxyView(false);

        expect(viewModel.disableBulkAction()).toBe(true);
    });

    it(":disableBulkAction returns true if IsProxyView flag is true", function () {
        viewModel.IsProxyView(true);

        //all the rest of conditions are false:
        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        viewModel.acceptRejectInProgress(false);
        awardYearConfigurations[0].AreAwardingChangesAllowed(true);
        viewModel.Configuration(finAidConfiguration);
        viewModel.isAdmin(false);
        viewModel.hasErrors(false);

        expect(viewModel.disableBulkAction()).toBe(true);
    });

    it(":disableBulkAction returns false if none of the conditions are true", function () {
        loanCollection.Loans()[0].AwardStatus.Category("Pending");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        viewModel.acceptRejectInProgress(false);
        viewModel.hasErrors(false);
        awardYearConfigurations[0].AreAwardingChangesAllowed(true);
        viewModel.Configuration(finAidConfiguration);
        viewModel.isAdmin(false);
        viewModel.IsProxyView(false);
        expect(viewModel.disableBulkAction()).toBe(false);
    });
    //#endregion

    //#region isAwardLetterForYearAccepted
    it(":isAwardLetterForYearAccepted is false if there is no selectedAwardLetterInfoForYear", function () {
        viewModel.selectedYear(awardYear); //2015 at this point
        viewModel.AwardLetterData().push(awardLetterInfoForYear);
        expect(viewModel.isAwardLetterForYearAccepted()).toEqual(false);
    });

    it(":isAwardLetterForYearAccepted is false if the selectedAwardLetterInfoForYear is null", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);        

        expect(viewModel.isAwardLetterForYearAccepted()).toEqual(false);
    });

    it(":isAwardLetterForYearAccepted is true if the award letter for the year is accepted", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        awardLetterInfoForYear.IsAwardLetterAccepted(true);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);

        expect(viewModel.isAwardLetterForYearAccepted()).toEqual(true);
    });

    it(":isAwardLetterForYearAccepted is false if the award letter for the year is not accepted", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        awardLetterInfoForYear.IsAwardLetterAccepted(false);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);

        expect(viewModel.isAwardLetterForYearAccepted()).toEqual(false);
    });
    //#endregion

    //#region isAwardLetterForYearAvailable
    it(":isAwardLetterForYearAvailable if false if there is no selectedAwardLetterInfoForYear", function () {
        viewModel.selectedYear(awardYear); //2015 at this point
        viewModel.AwardLetterData().push(awardLetterInfoForYear);

        expect(viewModel.isAwardLetterForYearAvailable()).toEqual(false);
    });

    it(":isAwardLetterForYearAvailable if false if the selectedAwardLetterInfoForYear is null", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        expect(viewModel.isAwardLetterForYearAvailable()).toEqual(false);
    });

    it(":isAwardLetterForYearAvailable if true if the award letter for year is available", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);

        expect(viewModel.isAwardLetterForYearAvailable()).toEqual(true);
    });

    it(":isAwardLetterForYearAvailable if false if the award letter for year is not available", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        awardLetterInfoForYear.IsAwardLetterAvailable(false);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);

        expect(viewModel.isAwardLetterForYearAvailable()).toEqual(false);
    });
    //#endregion

    //#region allPendingAwardPeriodsForYear
    it(":allPendingAwardPeriodsForYear is empty if there are no pending or estimated award periods", function () {
        studentAwardCollection.StudentWorkAwardsForYear([]);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.allPendingAwardPeriodsForYear()).toEqual([]);
    });

    it(":allPendingAwardPeriodsForYear is not empty if there are pending or estimated award periods", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.allPendingAwardPeriodsForYear().length).not.toEqual(0);
    });

    it(":allPendingAwardPeriodsForYear contains expected number of pending/estimated award periods", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        anotherLoanCollection.AwardYearCode("2015");
        viewModel.StudentGradPlusLoans().push(anotherLoanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.allPendingAwardPeriodsForYear().length).toEqual(6);
    });
    //#endregion

    //#region areAllPendingAwardsIgnoredFromEval
    it(":areAllPendingAwardsIgnoredFromEval returns true if all award periods have IsIgnoredOnAwardLetter flag set to true", function () {
        viewModel.StudentAwards().push(anotherStudentAwardCollection);
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        expect(viewModel.areAllPendingAwardsIgnoredFromEval()).toBe(true);
    });

    it(":areAllPendingAwardsIgnoredFromEval returns false if all award periods are Accepted/Rejected", function () {
        studentAwardCollection.StudentWorkAwardsForYear([]);
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.areAllPendingAwardsIgnoredFromEval()).toBe(false);
    });

    it(":areAllPendingAwardsIgnoredFromEval returns false if at least one pending award period is not ignored", function () {
        anotherStudentAwardCollection.StudentAwardsForYear()[0].StudentAwardPeriods()[0].IsIgnoredOnAwardLetter(false);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        expect(viewModel.areAllPendingAwardsIgnoredFromEval()).toBe(false);
    });
    //#endregion 

    //#region areAnyAwardsViewableOnAwardLetter
    it(":areAnyAwardsViewableOnAwardLetter is true if there are no excluded awards", function () {
        viewModel.StudentAwards().push(studentAwardCollection);
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);
       
        viewModel.selectedYear(awardYear);

        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is false if there are no awards for the year", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);
        viewModel.StudentAwards([]);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);        

        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(false);
    });

    it(":areAnyAwardsViewableOnAwardLetter is true if there is at least one award to display", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);
        
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is true if there is at least one work award to display", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);

        anotherStudentAwardCollection.StudentAwardsForYear([]);
        anotherStudentAwardCollection.StudentWorkAwardsForYear([{
            AwardYearId: ko.observable("2016"),
            Code: ko.observable("Award A"),
            totalAmount: ko.observable(500),
            AwardStatus: {
                Category: ko.observable("Accepted"),
                Code: ko.observable()
            },
            IsStatusModifiable: ko.observable(),
            IsAmountModifiable: ko.observable(),
            StudentAwardPeriods: ko.observable([
            {
                Code: ko.observable("15/FA"),
                AwardAmount: ko.observable(500),
                IsViewableOnAwardLetter: ko.observable(true)
            }]),
            isAwardOnHoldOrInReview: ko.observable()
        }]);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is true if there is at least one 'other' loan to display", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans().push(anotherStudentAwardCollection);        
        viewModel.StudentAwards([]);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is true if there is at least one sub loan to display", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);        
        viewModel.StudentAwards([]);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is true if there is at least one unsub loan to display", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentSubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);
        viewModel.StudentAwards([]);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is true if there is at least one grad plus loan to display", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.OtherLoans([]);
        viewModel.StudentAwards([]);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is true if there are awards to display", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        anotherStudentAwardCollection.StudentAwardsForYear()[0].StudentAwardPeriods()[0].IsViewableOnAwardLetter(false);
        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(true);
    });

    it(":areAnyAwardsViewableOnAwardLetter is false if all awards are excluded", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);

        anotherStudentAwardCollection.StudentAwardsForYear()[0].StudentAwardPeriods()[0].IsViewableOnAwardLetter(false);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        
        expect(viewModel.areAnyAwardsViewableOnAwardLetter()).toEqual(false);
    });
    //#endregion

    //#region isAwardLetterReadyToBeSigned
    it(":isAwardLetterReadyToBeSigned is false if award letter is not available", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(false);

        expect(viewModel.isAwardLetterReadyToBeSigned()).toEqual(false);
    });

    it(":isAwardLetterReadyToBeSigned is false if there are no awards viewable on award letter", function () {
        viewModel.StudentSubLoans([]);
        viewModel.StudentUnsubLoans([]);
        viewModel.StudentGradPlusLoans([]);
        viewModel.OtherLoans([]);
        viewModel.StudentAwards([]);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        expect(viewModel.isAwardLetterReadyToBeSigned()).toEqual(false);
    });

    it(":isAwardLetterReadyToBeSigned is false if the user is not self", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);
        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        viewModel.IsUserSelf(false);

        expect(viewModel.isAwardLetterReadyToBeSigned()).toEqual(false);
    });

    it(":isAwardLetterReadyToBeSigned is false if there are unaccepted awards and they are not ignored", function () {
        anotherStudentAwardCollection.StudentAwardsForYear()[0].StudentAwardPeriods()[0].IsIgnoredOnAwardLetter(false);
        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);
        viewModel.IsUserSelf(true);

        expect(viewModel.isAwardLetterReadyToBeSigned()).toEqual(false);
    });

    it(":isAwardLetterReadyToBeSigned is true if there are no unaccepted awards", function () {
        studentAwardCollection.AwardYearCode("2016");
        studentAwardCollection.StudentWorkAwardsForYear([]);
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);        
        viewModel.IsUserSelf(true);        

        expect(viewModel.isAwardLetterReadyToBeSigned()).toEqual(true);
    });

    it(":isAwardLetterReadyToBeSigned is true if there are unaccepted awards but they are ignored", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        awardLetterInfoForYear.CategoriesExcludedFromAwardLetter([]);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        viewModel.IsUserSelf(true);

        expect(viewModel.isAwardLetterReadyToBeSigned()).toEqual(true);
    });
    //#endregion

    //#region displayAwardLetterPopUpDialog
    it(":displayAwardLetterReadyPopUpDialog is false if the award letter is not ready to be signed", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(false);        
        viewModel.AwardLetterData.push(awardLetterInfoForYear);
        viewModel.displayPopUpDialog();

        expect(viewModel.displayAwardLetterReadyPopUpDialog()).toEqual(false);
    });

    it(":displayAwardLetterReadyPopUpDialog is false if the award letter is ready to be signed but no changes occurred", function () {
        studentAwardCollection.AwardYearCode("2016");
        studentAwardCollection.StudentWorkAwardsForYear([]);
        viewModel.OtherLoans().push(studentAwardCollection);
        viewModel.StudentAwards().push(studentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);
        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        viewModel.AwardLetterData().push(awardLetterInfoForYear);
        viewModel.IsUserSelf(true);
        viewModel.firstPopUpDialogDisplay(true);
        viewModel.awardChangeOccurred(false);
        viewModel.loanAmountChangeOccurred(false);
        viewModel.displayPopUpDialog();

        expect(viewModel.displayAwardLetterReadyPopUpDialog()).toEqual(false);
    });

    it(":displayAwardLetterReadyPopUpDialog is false if the award letter is ready to be signed and loan amount change needs review", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        awardLetterInfoForYear.CategoriesExcludedFromAwardLetter([]);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        viewModel.loanAmountChangeOccurred(true);
        awardYearConfigurations[0].IsLoanAmountChangeReviewRequired(true);
        awardYearConfigurations[0].AwardYearCode("2016");
        viewModel.Configuration(finAidConfiguration);

        viewModel.IsUserSelf(true);
        viewModel.displayPopUpDialog();

        expect(viewModel.displayAwardLetterReadyPopUpDialog()).toEqual(false);
    });

    it(":displayAwardLetterReadyPopUpDialog is true if the award letter is ready to be signed and loan amount change (first time) does not need review", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        awardLetterInfoForYear.IsAwardLetterAccepted(true);
        awardLetterInfoForYear.CategoriesExcludedFromAwardLetter([]);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        viewModel.loanAmountChangeOccurred(true);
        awardYearConfigurations[0].IsLoanAmountChangeReviewRequired(false);
        awardYearConfigurations[0].AwardYearCode("2016");
        viewModel.Configuration(finAidConfiguration);

        viewModel.IsUserSelf(true);
        viewModel.firstPopUpDialogDisplay(true);
        viewModel.displayPopUpDialog();

        expect(viewModel.displayAwardLetterReadyPopUpDialog()).toEqual(true);
    });

    it(":displayAwardLetterReadyPopUpDialog is true if the award letter is ready to be signed, doesAnyAwardRequireAction is true, and award change occurred for the first time", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        awardLetterInfoForYear.DoesAnyAwardRequireAction(true);
        awardLetterInfoForYear.CategoriesExcludedFromAwardLetter([]);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        viewModel.awardChangeOccurred(true);
        viewModel.firstPopUpDialogDisplay(true);
        viewModel.IsUserSelf(true);
        viewModel.displayPopUpDialog();

        expect(viewModel.displayAwardLetterReadyPopUpDialog()).toEqual(true);
    });
    //#endregion

    //#region displayAwardLetterReadyMessage 
    it(":displayAwardLetterReadyMessage is false if the award letter is not ready to be signed", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(false);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        expect(viewModel.displayAwardLetterReadyMessage()).toEqual(false);
    });

    it(":displayAwardLetterReadyMessage is false if the award letter is accepted", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        awardLetterInfoForYear.IsAwardLetterAccepted(true);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        viewModel.IsUserSelf(true);

        expect(viewModel.displayAwardLetterReadyMessage()).toEqual(false);
    });

    it(":displayAwardLetterReadyMessage is false if displayAwardLetterReadyPopUpDialog is true", function () {
        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        awardLetterInfoForYear.IsAwardLetterAccepted(false);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        viewModel.IsUserSelf(true);
        viewModel.displayAwardLetterReadyPopUpDialog(true);

        expect(viewModel.displayAwardLetterReadyMessage()).toEqual(false);
    });
    
    it(":displayAwardLetterReadyMessage is true if award letter is ready, unaccepted, and no changes occurred", function () {
        loanCollection.AwardYearCode("2016");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.StudentUnsubLoans().push(loanCollection);
        viewModel.StudentGradPlusLoans().push(loanCollection);

        viewModel.OtherLoans().push(anotherStudentAwardCollection);
        viewModel.StudentAwards().push(anotherStudentAwardCollection);

        awardYear.Code("2016");
        viewModel.selectedYear(awardYear);

        awardLetterInfoForYear.IsAwardLetterAvailable(true);
        awardLetterInfoForYear.IsAwardLetterAccepted(false);
        viewModel.AwardLetterData.push(awardLetterInfoForYear);

        viewModel.IsUserSelf(true);
        
        expect(viewModel.displayAwardLetterReadyMessage()).toEqual(true);
    });
    //#endregion

    //#region closeAwardLetterReadyPopUpDialog 
    it(":closeAwardLetterReadyPopUpDialog sets awardChangeOccurred to false", function () {
        viewModel.awardChangeOccurred(true);
        viewModel.closeAwardLetterReadyPopUpDialog();

        expect(viewModel.awardChangeOccurred()).toEqual(false);
    });

    it(":closeAwardLetterReadyPopUpDialog sets loanAmountChangeOccurred to false", function () {
        viewModel.loanAmountChangeOccurred(true);
        viewModel.closeAwardLetterReadyPopUpDialog();

        expect(viewModel.loanAmountChangeOccurred()).toEqual(false);
    });
    //#endregion

    //#region isSubLoanPending
    it(":isSubLoanPending returns false if there are no sub loans", function () {
        loanCollection.AwardYearCode(awardYear.Code);
        viewModel.StudentSubLoans([]);
        viewModel.selectedYear(awardYear);
        expect(viewModel.isSubLoanPending()).toBe(false);
    });

    it(":isSubLoanPending returns false if there are no pending sub loans", function () {
        for (var i = 0; i < loanCollection.Loans().length; i++) {
            loanCollection.Loans()[i].AwardStatus.Category("Accepted");
        }
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.isSubLoanPending()).toBe(false);
    });

    it(":isSubLoanPending returns true if there is at least one pending subloan", function () {
        loanCollection.Loans()[0].AwardStatus.Category("Pending");        
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.isSubLoanPending()).toBe(true);
    });

    it(":isSubLoanPending returns true if there is at least one estimated subloan", function () {
        loanCollection.Loans()[0].AwardStatus.Category("Estimated");
        viewModel.StudentSubLoans().push(loanCollection);
        viewModel.selectedYear(awardYear);
        expect(viewModel.isSubLoanPending()).toBe(true);
    });
    //#endregion
});