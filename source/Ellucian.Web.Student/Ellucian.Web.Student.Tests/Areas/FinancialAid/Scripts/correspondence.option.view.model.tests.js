﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
describe("correspondence.option.view.model.js", function () {

    // Define the global variables needed for the tests
    beforeAll(function () {
        updateCorrespondenceOptionActionUrl = "updateUrl/";
        jsonContentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        faCorrespondenceOptionProcessingMessage = "faCorrespondenceOptionProcessingMessage";
        faCorrespondenceOptionUpdateSuccessMessage = "faCorrespondenceOptionUpdateSuccessMessage";
        faCorrespondenceOptionUnableToSubmitChangeMessage = "faCorrespondenceOptionUnableToSubmitChangeMessage";
        correspondenceOptionAdminActionUrl = "adminUrl";
    });

    // viewModel is the local instance of the view model we are testing
    var viewModel,
        event,
        correspondenceOptionDataModel,
        data;

    beforeEach(function () {
        //ViewModel we are testing
        viewModel = new correspondenceOptionViewModel();

        //event that gets triggered whenever checkbox selection changes
        event = {
            target: {
                checked: true
            }
        };

        correspondenceOptionDataModel = {
            AwardYearCode: ko.observable("2015"),
            IsCorrespondenceOptionChecked: ko.observable(false),
            OptionMessage: "",
            isOriginalOptionChecked: ko.observable(false)
        };

        //Data we receive on ajax success
        data = {
            IsCorrespondenceOptionChecked: ko.observable(true),
            AwardYearCode: ko.observable("2015"),
            OptionMessage: "Message"
        };
    });

    afterEach(function () {
        viewModel = null;
        event = null;
        correspondenceOptionDataModel = null;
        data = null;
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":IsProxyView is false after initialization", function () {
        expect(viewModel.IsProxyView()).toBeFalsy();
    });

    it(":isAdmin is false after initialization", function () {
        expect(viewModel.isAdmin()).toBeFalsy();
    });

    it(":isLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":Person is undefined after initialization", function () {
        expect(viewModel.Person()).toBeUndefined();
    });

    it(":CorrespondenceOptionData is undefined after initialization", function () {
        expect(viewModel.CorrespondenceOptionData()).toBeUndefined();
    });

    it(":isCheckboxSelectionChange is false after initialization", function () {
        expect(viewModel.isCheckboxSelectionChanged()).toBeFalsy();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(correspondenceOptionAdminActionUrl);
    });

    it(":changeSelection changes the value of isCheckboxSelectionChanged to true when called", function () {
        viewModel.CorrespondenceOptionData(correspondenceOptionDataModel);
        viewModel.changeSelection(correspondenceOptionDataModel, event);

        expect(viewModel.isCheckboxSelectionChanged()).toBeTruthy();
        expect(viewModel.CorrespondenceOptionData().IsCorrespondenceOptionChecked()).toBeTruthy();
    });

    it(":changeSelection changes the value of isCheckboxSelectionChanged to false when called", function () {
        correspondenceOptionDataModel = {
            AwardYearCode: ko.observable("2015"),
            IsCorrespondenceOptionChecked: ko.observable(true),
            OptionMessage: "",
            isOriginalOptionChecked: ko.observable(true)
        };
        event = {
            target: {
                checked: false
            }
        };
        viewModel.CorrespondenceOptionData(correspondenceOptionDataModel);
        viewModel.changeSelection(correspondenceOptionDataModel, event);

        expect(viewModel.isCheckboxSelectionChanged()).toBeTruthy();
        expect(viewModel.CorrespondenceOptionData().IsCorrespondenceOptionChecked()).toBeFalsy();
    });

    it(":changeSelection does not change the value of isCheckboxSelectionChanged if there was no change", function () {
        correspondenceOptionDataModel = {
            AwardYearCode: ko.observable("2015"),
            IsCorrespondenceOptionChecked: ko.observable(true),
            OptionMessage: "",
            isOriginalOptionChecked: ko.observable(true)
        };
       
        viewModel.CorrespondenceOptionData(correspondenceOptionDataModel);
        viewModel.changeSelection(correspondenceOptionDataModel, event);

        expect(viewModel.isCheckboxSelectionChanged()).toBeFalsy();
        expect(viewModel.CorrespondenceOptionData().IsCorrespondenceOptionChecked()).toBeTruthy();
    });

    it(":submitSelection beforeSend isLoading assigned correctly when called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        viewModel.submitSelection(correspondenceOptionDataModel);
        expect(viewModel.isLoading()).toBeTruthy();        
    });

    it(":submitSelection success updates the corresponding values when called", function () {        
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(data);            
        });

        viewModel.submitSelection(correspondenceOptionDataModel);
        expect(correspondenceOptionDataModel.IsCorrespondenceOptionChecked()).toEqual(data.IsCorrespondenceOptionChecked);
        expect(correspondenceOptionDataModel.isOriginalOptionChecked()).toEqual(data.IsCorrespondenceOptionChecked);
        expect(viewModel.isCheckboxSelectionChanged()).toBeFalsy();
    });

    it(":submitSelection stops loading on complete", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(data);
            e.complete({}, {});
        });
        spyOn(viewModel, "isLoading");

        viewModel.submitSelection(correspondenceOptionDataModel);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":submitSelection stops loading on error", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.error({ status: 0 }, "", "");
        });
        spyOn(viewModel, "isLoading");

        viewModel.submitSelection(correspondenceOptionDataModel);
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.isLoading()).toBeFalsy();
    });

});