﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
describe("outside.awards.view.model.js", function () {

    var viewModel,
        awardYearsData,
        outsideAwardsData,
        studentData,
        candidateAward, 
            inputField;

    beforeAll(function () {
        createOutsideAwardActionUrl = "create url";
        faOutsideAwardsUnableToCreateMessage = "Unable to create";
        faOutsideAwardsRemoveAwardConfirmationText = "Confirmation text {0}";
        deleteOutsideAwardActionUrl = "delete url";
        faOutsideAwardsUnableToDeleteMessage = "Unable to delete";
        updateOutsideAwardActionUrl = "update url";
        faOutsideAwardsUnableToUpdateMessage = "Unable to update";
        faOutsideAwardsUpdateButtonLabel = "update award";
        faOutsideAwardsAddButtonLabel = "add award";
        outsideAwardsAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        viewModel = new outsideAwardsViewModel();
        awardYearsData = testData().awardYearsData;
        outsideAwardsData = testData().outsideAwardsData;
        studentData = testData().personData;
        candidateAward = {
            Id: ko.observable(""),
            AwardName: ko.observable(""),
            AwardType: ko.observable(),
            AwardAmount: ko.observable("").extend({                
                numeric: {
                    precision: 2,
                    min: 1,
                    max: 99999.99,
                    valCondition: function () { return true },
                    updateOnError: true
                }
            }),
            AwardFundingSource: ko.observable(""),
            AwardYearCode: ko.observable(),
            StudentId: ko.observable()
        };
        inputField = document.createElement('input');
        inputField.id = 'award-amount-field';
        document.body.appendChild(inputField);
    });

    afterEach(function () {
        viewModel = null;
        awardYearsData = null;
        outsideAwardsData = null;
        studentData = null;
    });

    it(":OutsideAwards is an empty array after initialization", function () {
        expect(viewModel.OutsideAwards()).toEqual([]);
    });

    it(":OutsideAwardTypes is an empty array after initialization", function () {
        expect(viewModel.OutsideAwardTypes()).toEqual([]);
    });

    it(":displayOutsideAwardsView is false after initialization", function () {
        expect(viewModel.displayOutsideAwardsView()).toEqual(false);
    });

    it(":displayOutsideAwardInputForm is false after initialization", function () {
        expect(viewModel.displayOutsideAwardInputForm()).toEqual(false);
    });

    it(":displayAwardRemoveDialog is false after initialization", function () {
        expect(viewModel.displayAwardRemoveDialog()).toEqual(false);
    });

    it(":editOutsideAward is false after initialization", function () {
        expect(viewModel.editOutsideAward()).toEqual(false);
    });

    it(":isModified is false after initialization", function () {
        expect(viewModel.isModified()).toEqual(false);
    });

    it(":hasErrors is false after initialization", function () {
        expect(viewModel.hasErrors()).toEqual(false);
    });

    it(":showUI is undefined after initialization", function () {
        expect(viewModel.showUI()).toEqual(undefined);
    });

    it(":Student is undefined after initialization", function () {
        expect(viewModel.Student()).toEqual(undefined);
    });

    it(":IsUserSelf is undefined after initialization", function () {
        expect(viewModel.IsUserSelf()).toEqual(undefined);
    });

    it(":outsideAward is defined after initialization", function () {
        expect(viewModel.outsideAward()).toBeDefined();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(outsideAwardsAdminActionUrl);
    });

    it(":createOutsideAward adds a new award to the OutsideAwards array on success", function () {
        viewModel.selectedYear(awardYearsData[1]);
        viewModel.Student(studentData);

        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        viewModel.createOutsideAward();
        expect(viewModel.OutsideAwards().length).toEqual(1);
    });

    it(":createOutsideAward send expected message to the notification center on error", function () {
        viewModel.selectedYear(awardYearsData[1]);
        viewModel.Student(studentData);
        spyOn($, "ajax").and.callFake(function (e) {
            e.error({ status: 500 });
        });
        spyOn($.fn, "notificationCenter");
        var expected = {
            message: "Unable to create",
            type: "error",
            flash: true
        };
        viewModel.createOutsideAward();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', expected);
    });

    it(":createOutsideAward sets displayOutsideAwardInputForm to false on completion", function () {
        viewModel.selectedYear(awardYearsData[1]);
        viewModel.Student(studentData);
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete();
        });
        spyOn(viewModel, "displayOutsideAwardInputForm");
        viewModel.createOutsideAward();
        expect(viewModel.displayOutsideAwardInputForm).toHaveBeenCalledWith(false);
    });

    it(":createOutsideAward triggers clearInputValues function on completion", function () {
        viewModel.selectedYear(awardYearsData[1]);
        viewModel.Student(studentData);
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete();
        });
        spyOn(viewModel, "clearInputValues");
        viewModel.createOutsideAward();
        expect(viewModel.clearInputValues).toHaveBeenCalled();
    });

    it(":addOutsideAward triggers clearInputValues function", function () {
        spyOn(viewModel, "clearInputValues");
        viewModel.addOutsideAward();
        expect(viewModel.clearInputValues).toHaveBeenCalled();
    });

    it(":addOutsideAward sets displayOutsideAwardInputForm to true", function () {
        expect(viewModel.displayOutsideAwardInputForm()).toEqual(false);
        viewModel.addOutsideAward();
        expect(viewModel.displayOutsideAwardInputForm()).toEqual(true);
    });

    it(":cancelOutsideAwardAction sets displayOutsideAwardInputForm to false", function () {
        viewModel.displayOutsideAwardInputForm(true);
        viewModel.cancelOutsideAwardAction();
        expect(viewModel.displayOutsideAwardInputForm()).toEqual(false);
    });

    it(":cancelOutsideAwardAction sets editOutsideAward to false if it is true", function () {
        viewModel.editOutsideAward(true);
        viewModel.cancelOutsideAwardAction();
        expect(viewModel.editOutsideAward()).toEqual(false);
    });

    it(":cancelOutsideAwardAction sets originalOutsideAward to null if editOutsideAward is true", function () {
        viewModel.editOutsideAward(true);
        viewModel.cancelOutsideAwardAction();
        expect(viewModel.originalOutsideAward()).toEqual("");
    });

    it(":cancelOutsideAwardAction sets isModified to false if editOutsideAward is true", function () {
        viewModel.editOutsideAward(true);
        viewModel.cancelOutsideAwardAction();
        expect(viewModel.isModified()).toEqual(false);
    });

    it(":clearInputValues resets outsideAward award amount to 0", function () {
        viewModel.outsideAward().AwardAmount(2300);
        viewModel.clearInputValues();
        expect(viewModel.outsideAward().AwardAmount()).toEqual('0.00');
    });

    it(":clearInputValues resets outsideAward award funding source to null", function () {
        viewModel.outsideAward().AwardFundingSource("source");
        viewModel.clearInputValues();
        expect(viewModel.outsideAward().AwardFundingSource()).toEqual("");
    });

    it(":clearInputValues resets outsideAward award name to null", function () {
        viewModel.outsideAward().AwardName("name");
        viewModel.clearInputValues();
        expect(viewModel.outsideAward().AwardName()).toEqual("");
    });

    it(":clearInputValues resets outsideAward award type to 'Scholarship'", function () {
        viewModel.outsideAward().AwardType("Grant");
        viewModel.clearInputValues();
        expect(viewModel.outsideAward().AwardType()).toEqual("Scholarship");
    });

    it(":processAmountChange sets hasErrors to true if award amount has error", function () {
        var awardAmount = ko.observable('2345a').extend({ numeric: { precision: 2, valCondition: function () { return true } } });
        
        viewModel.processAmountChange(awardAmount, {}, {});
        expect(viewModel.hasErrors()).toEqual(true);
    });

    it(":processAmountChange sets hasErrors to false if award amount does not have errors", function () {
        var awardAmount = ko.observable('2345').extend({ numeric: { precision: 2, valCondition: function () { return true } } });

        viewModel.processAmountChange(awardAmount, {}, {});
        expect(viewModel.hasErrors()).toEqual(false);
    });

    it(":processAmountChange sets isModified to true if award amount was changed", function () {
        var awardAmount = ko.observable('2345').extend({ numeric: { precision: 2, valCondition: function () { return true } } });        
        viewModel.originalOutsideAward({AwardAmount: ko.observable(2500)});
        candidateAward.AwardAmount(2345);
        viewModel.outsideAward(candidateAward);
        viewModel.editOutsideAward(true);
        viewModel.isModified(false);

        viewModel.processAmountChange(awardAmount, {}, {});
        expect(viewModel.isModified()).toEqual(true);
    });

    it(":clearInputField clears target value", function () {
        var event = {
            target: {
                value: "smth"
            }
        };
        viewModel.clearInputField({}, event);
        expect(event.target.value).toEqual(' ');
    });

    it(":removeOutsideAwardMessage is undefined after initialization", function () {
        expect(viewModel.removeOutsideAwardMessage()).toBeUndefined();
    });

    it(":outsideAwardToRemoveId is undefined after initialization", function () {
        expect(viewModel.outsideAwardToRemoveId()).toBeUndefined();
    });

    it(":showAwardRemoveDialog sets displayAwardRemoveDialog to true", function () {
        candidateAward.AwardName("awardName");
        viewModel.showAwardRemoveDialog(candidateAward, {});
        expect(viewModel.displayAwardRemoveDialog()).toEqual(true);
    });

    it(":showAwardRemoveDialog sets removeOutsideAwardMessage with expected value", function () {
        candidateAward.AwardName("awardName");
        var expectedMessage = "Confirmation text awardName";
        viewModel.showAwardRemoveDialog(candidateAward, {});
        expect(viewModel.removeOutsideAwardMessage()).toEqual(expectedMessage);
    });

    it(":showAwardRemoveDialog sets outsideAwardToRemoveId with expected value", function () {
        candidateAward.Id("foo");
        viewModel.showAwardRemoveDialog(candidateAward, {});
        expect(viewModel.outsideAwardToRemoveId()).toEqual("foo");
    });

    it(":cancelRemoveOutsideAward sets displayAwardRemoveDialog to false", function () {
        viewModel.displayAwardRemoveDialog(true);
        viewModel.cancelRemoveOutsideAward();
        expect(viewModel.displayAwardRemoveDialog()).toEqual(false);
    });

    it(":cancelRemoveOutsideAward sets removeOutsideAwardMessage to null", function () {
        viewModel.removeOutsideAwardMessage("message");
        viewModel.cancelRemoveOutsideAward();
        expect(viewModel.removeOutsideAwardMessage()).toEqual("");
    });

    it(":cancelRemoveOutsideAward sets outsideAwardToRemoveId to null", function () {
        viewModel.outsideAwardToRemoveId("id");
        viewModel.cancelRemoveOutsideAward();
        expect(viewModel.outsideAwardToRemoveId()).toEqual("");
    });

    it(":deleteOutsideAward removes an award from OutsideAwards list", function () {
        viewModel.Student(studentData);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        viewModel.OutsideAwards().push(outsideAwardsData[0]);
        expect(viewModel.OutsideAwards().length).toEqual(1);

        viewModel.outsideAwardToRemoveId(outsideAwardsData[0].Id());
        viewModel.deleteOutsideAward({}, {});
        expect(viewModel.OutsideAwards().length).toEqual(0);
    });

    it(":deleteOutsideAward sends expected message to the notification center on error", function () {
        viewModel.Student(studentData);
        spyOn($, "ajax").and.callFake(function (e) {
            e.error({ status: 403 });
        });

        spyOn($.fn, "notificationCenter");
        var expected = {
            message: "Unable to delete",
            type: "error",
            flash: true
        };

        viewModel.deleteOutsideAward({}, {});
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', expected);
    });

    it(":deleteOutsideAward sets displayAwardRemoveDialog to false on completion", function () {
        viewModel.Student(studentData);
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete();
        });
        viewModel.displayAwardRemoveDialog(true);
        viewModel.deleteOutsideAward({}, {});
        expect(viewModel.displayAwardRemoveDialog()).toEqual(false);
    });

    it(":handleOutsideAwardEvent triggers createOutsideAward function if clicked button is 'add'", function () {
        var event = {
            currentTarget: {
                name: "add"
            }
        };
        spyOn(viewModel, "createOutsideAward");
        viewModel.handleOutsideAwardEvent({}, event);
        expect(viewModel.createOutsideAward).toHaveBeenCalled();
    });

    it(":handleOutsideAwardEvent triggers updateOutsideAward function if clicked button is 'update'", function () {
        var event = {
            currentTarget: {
                name: "update"
            }
        };
        spyOn(viewModel, "updateOutsideAward");
        viewModel.handleOutsideAwardEvent({}, event);
        expect(viewModel.updateOutsideAward).toHaveBeenCalled();
    });

    it(":disableOutsideAwardAction returns true if there are no updates on outside award update", function () {
        viewModel.editOutsideAward(true);
        viewModel.isModified(false);
        expect(viewModel.disableOutsideAwardAction()).toEqual(true);
    });

    it(":disableOutsideAwardAction returns true if there are input errors", function () {
        viewModel.editOutsideAward(false);
        viewModel.hasErrors(true);
        expect(viewModel.disableOutsideAwardAction()).toEqual(true);
    });

    it(":disableOutsideAwardAction returns true if outside award name is blank", function () {
        viewModel.hasErrors(false);
        candidateAward.AwardName("");
        candidateAward.AwardType("scholarship");
        candidateAward.AwardFundingSource("self");
        candidateAward.AwardAmount(456);
        viewModel.outsideAward(candidateAward);
        expect(viewModel.disableOutsideAwardAction()).toEqual(true);
    });

    it(":disableOutsideAwardAction returns true if outside award funding source is blank", function () {
        viewModel.hasErrors(false);
        candidateAward.AwardName("name");
        candidateAward.AwardType("scholarship");
        candidateAward.AwardFundingSource("");
        candidateAward.AwardAmount(456);
        viewModel.outsideAward(candidateAward);
        expect(viewModel.disableOutsideAwardAction()).toEqual(true);
    });

    it(":disableOutsideAwardAction returns true if outside award amount is zero", function () {
        viewModel.hasErrors(false);
        candidateAward.AwardName("name");
        candidateAward.AwardType("scholarship");
        candidateAward.AwardFundingSource("source");
        candidateAward.AwardAmount(0);
        viewModel.outsideAward(candidateAward);
        expect(viewModel.disableOutsideAwardAction()).toEqual(true);
    });

    it(":disableOutsideAwardAction returns false if all outside award fields are present on add", function () {
        viewModel.editOutsideAward(false);
        candidateAward.AwardName("name");
        candidateAward.AwardType("scholarship");
        candidateAward.AwardFundingSource("self");
        candidateAward.AwardAmount(456);
        viewModel.outsideAward(candidateAward);
        expect(viewModel.disableOutsideAwardAction()).toEqual(false);
    });

    it(":disableOutsideAwardAction returns false if there were edit changes and all fields are present on award update", function () {
        viewModel.editOutsideAward(true);
        viewModel.isModified(true);
        candidateAward.AwardName("name");
        candidateAward.AwardType("scholarship");
        candidateAward.AwardFundingSource("self");
        candidateAward.AwardAmount(456);
        viewModel.outsideAward(candidateAward);
        expect(viewModel.disableOutsideAwardAction()).toEqual(false);
    });

    it(":outsideAwardActionButtonLabel returns update button label if editOutsideAward is true", function () {
        viewModel.editOutsideAward(true);
        expect(viewModel.outsideAwardActionButtonLabel()).toEqual("update award");
    });

    it(":outsideAwardActionButtonLabel returns add button label if editOutsideAward is false", function () {
        viewModel.editOutsideAward(false);
        expect(viewModel.outsideAwardActionButtonLabel()).toEqual("add award");
    });

    it(":outsideAwardActionButtonName return update button name if editOutsideAward is true", function () {
        viewModel.editOutsideAward(true);
        expect(viewModel.outsideAwardActionButtonName()).toEqual("update");
    });

    it(":outsideAwardActionButtonName return add button name if editOutsideAward is false", function () {
        viewModel.editOutsideAward(false);
        expect(viewModel.outsideAwardActionButtonName()).toEqual("add");
    });

    it(":originalOutsideAward is undefined after initialization", function () {
        expect(viewModel.originalOutsideAward()).toBeUndefined();
    });

    it(":showAwardEditDialog sets displayOutsideAwardInputForm to true", function () {
        viewModel.displayOutsideAwardInputForm(false);
        viewModel.showAwardEditDialog(candidateAward, {});
        expect(viewModel.displayOutsideAwardInputForm()).toEqual(true);
    });

    it(":showAwardEditDialog sets editOutsideAward to true", function () {
        viewModel.editOutsideAward(false);
        viewModel.showAwardEditDialog(candidateAward, {});
        expect(viewModel.editOutsideAward()).toEqual(true);
    });

    it(":showAwardEditDialog sets all outside award properties with expected values", function () {
        candidateAward.Id("234");
        candidateAward.StudentId("0004791");
        candidateAward.AwardName("awardName");
        candidateAward.AwardType("type");
        candidateAward.AwardAmount(2345);
        candidateAward.AwardFundingSource("sources");
        candidateAward.AwardYearCode("2016");

        viewModel.showAwardEditDialog(candidateAward, {});
        expect(viewModel.outsideAward().Id()).toEqual(candidateAward.Id());
        expect(viewModel.outsideAward().StudentId()).toEqual(candidateAward.StudentId());
        expect(viewModel.outsideAward().AwardName()).toEqual(candidateAward.AwardName());
        expect(viewModel.outsideAward().AwardType()).toEqual(candidateAward.AwardType());
        expect(viewModel.outsideAward().AwardAmount()).toEqual(candidateAward.AwardAmount());
        expect(viewModel.outsideAward().AwardFundingSource()).toEqual(candidateAward.AwardFundingSource());
        expect(viewModel.outsideAward().AwardYearCode()).toEqual(candidateAward.AwardYearCode());
    });

    it(":showAwardEditDialog sets originalOutsideAward with award values", function () {
        viewModel.showAwardEditDialog(candidateAward, {});
        expect(viewModel.originalOutsideAward()).toEqual(candidateAward);
    });

    it(":updateOutsideAward sets appropriate outside award with updated values on success", function () {
        viewModel.OutsideAwards().push(outsideAwardsData[0]);        
        viewModel.outsideAward(outsideAwardsData[0].AwardAmount(456.87));

        spyOn($, "ajax").and.callFake(function (e) {
            e.success({
                Id: outsideAwardsData[0].Id(),
                AwardName: outsideAwardsData[0].AwardName(),
                AwardType: outsideAwardsData[0].AwardType(),
                AwardFundingSource: outsideAwardsData[0].AwardFundingSource(),
                AwardAmount: outsideAwardsData[0].AwardAmount()
            });
        });
        viewModel.updateOutsideAward();
        expect(viewModel.OutsideAwards()[0].AwardAmount()).toEqual(456.87);
    });

    it(":updateOutsideAward sends expect message to the notificationCenter on error", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.error({ status: 400 });
        });
        spyOn($.fn, "notificationCenter");
        var expected = {
            message: "Unable to update",
            type: "error",
            flash: true
        };
        viewModel.updateOutsideAward();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', expected);
    });

    it(":updateOutsideAward sets displayOutsideAwardInputForm to false on completion", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete();
        });
        viewModel.displayOutsideAwardInputForm(true);
        viewModel.updateOutsideAward();
        expect(viewModel.displayOutsideAwardInputForm()).toEqual(false);
    });

    it(":updateOutsideAward sets editOutsideAward to false on completion", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete();
        });
        viewModel.editOutsideAward(true);
        viewModel.updateOutsideAward();
        expect(viewModel.editOutsideAward()).toEqual(false);
    });

    it(":updateOutsideAward sets isModified to false on completion", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete();
        });
        viewModel.isModified(true);
        viewModel.updateOutsideAward();
        expect(viewModel.isModified()).toEqual(false);
    });

    it(":processAwardUpdate sets isModified to true if AwardName has changed", function () {
        var event = {
            originalEvent: { }
        };
        viewModel.originalOutsideAward(candidateAward);
        viewModel.outsideAward().AwardName("updated award name");
        viewModel.editOutsideAward(true);

        viewModel.processAwardUpdate({}, event);
        expect(viewModel.isModified()).toEqual(true);
    });

    it(":processAwardUpdate sets isModified to true if AwardType has changed", function () {
        var event = {
            originalEvent: {}
        };
        viewModel.originalOutsideAward(candidateAward);
        viewModel.outsideAward().AwardType("loan");
        viewModel.editOutsideAward(true);

        viewModel.processAwardUpdate({}, event);
        expect(viewModel.isModified()).toEqual(true);
    });

    it(":processAwardUpdate sets isModified to true if AwardFundingSource has changed", function () {
        var event = {
            originalEvent: {}
        };
        viewModel.originalOutsideAward(candidateAward);
        viewModel.outsideAward().AwardFundingSource("source");
        viewModel.editOutsideAward(true);

        viewModel.processAwardUpdate({}, event);
        expect(viewModel.isModified()).toEqual(true);
    });

    it(":processAwardUpdate does not set isModified to true if there were changes but editOutsideAward is false", function () {
        var event = {
            originalEvent: {}
        };
        
        viewModel.outsideAward().AwardFundingSource("source");
        viewModel.editOutsideAward(false);

        viewModel.processAwardUpdate({}, event);
        expect(viewModel.isModified()).toEqual(false);
    });

});