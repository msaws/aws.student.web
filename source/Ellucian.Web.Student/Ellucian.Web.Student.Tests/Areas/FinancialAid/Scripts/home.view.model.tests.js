﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("home.view.model.js", function () {

    var viewModel,
        awardYearData,
        awardYearsData,
        studentChecklistData,
        averageAwardPackageData,
        loanSummaryData,
        linksData,
        configurationData;

    beforeAll(function(){
        $.fn.tabs = function () { return $.fn; }
        faHomeNoFinAidDataMessage = "No fin aid data available";
        faHomeNoChecklistAssignedStudentMessage = "You have no checklist items";
        faHomeNoChecklistAssignedMessage = "Student has no checklist items";
        homeAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        viewModel = new homeViewModel();
        awardYearsData = testData().awardYearsData;
        awardYearData = awardYearsData[0];
        studentChecklistData = testData().studentChecklistsData;
        averageAwardPackageData = testData().averageAwardPackageData;
        loanSummaryData = testData().studentLoansSummaryData;
        linksData = testData().linksData;
        configurationData = testData().configurationData;
    });

    afterEach(function () {
        viewModel = null;
        awardYearData = null;
        awardYearsData = null;
        studentChecklistData = null;
        averageAwardPackageData = null;
    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it("StudentChecklists is an empty array after initialization", function () {
        expect(viewModel.StudentChecklists()).toEqual([]);
    });

    it(":StudentLoansSummary is undefined after initialization", function () {
        expect(viewModel.StudentLoansSummary()).toBeUndefined();
    });

    it(":AverageAwardPackages is an empty array after initialization", function () {
        expect(viewModel.AverageAwardPackages()).toEqual([]);
    });

    it(":FormLinks is an empty array after initialization", function () {
        expect(viewModel.FormLinks()).toEqual([]);
    });

    it(":HelpfulLinks is an empty array after initialization", function () {
        expect(viewModel.HelpfulLinks()).toEqual([]);
    });

    it(":Person is undefined after initialization", function () {
        expect(viewModel.Person()).toBeUndefined();
    });

    it(":Configuration is undefined after initialization", function () {
        expect(viewModel.Configuration()).toBeUndefined();
    });

    it(":PellLifetimeEligibilityUsedPercentage is undefined after initialization", function () {
        expect(viewModel.PellLifetimeEligibilityUsedPercentage()).toBeUndefined();
    });

    it(":StudentAccountSummary is undefined after initialization", function () {
        expect(viewModel.StudentAccountSummary()).toBeUndefined();
    });

    it(":IsAdminView is undefined after initialization", function () {
        expect(viewModel.IsAdminView()).toBeUndefined();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(homeAdminActionUrl);
    });

    it(":changeToMobile triggers show method", function () {
        spyOn($.fn, "show");
        viewModel.changeToMobile();
        expect($.fn.show).toHaveBeenCalled();
    });

    it(":changeToMobile triggers tabs method", function () {
        spyOn($.fn, "tabs").and.callThrough();
        viewModel.changeToMobile();
        var arg = { selected: 0 };
        expect($.fn.tabs).toHaveBeenCalledWith(arg);
    });

    it(":changeToMobile triggers addClass method", function () {
        spyOn($.fn, "addClass");
        viewModel.changeToMobile();       
        expect($.fn.addClass).toHaveBeenCalled();        
    });

    
    it(":changeToDesktop triggers hide method", function () {
        spyOn($.fn, "hide");
        viewModel.changeToDesktop();
        expect($.fn.hide).toHaveBeenCalled();
    });

    it(":changeToDesktop triggers removeClass method", function () {
        spyOn($.fn, "removeClass");
        viewModel.changeToDesktop();
        expect($.fn.removeClass).toHaveBeenCalled();
    });

    it(":areChecklistItemsAssigned is false if selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });

    it(":areChecklistItemsAssigned is true if selectedYear is not null and corresponding flag is set to true", function () {
        viewModel.selectedYear(awardYearData);
        expect(viewModel.areChecklistItemsAssigned()).toBeTruthy();
    });

    it(":areChecklistItemsAssigned is false if selectedYear is not null and corresponding flag is set to false", function () {
        awardYearData.AreChecklistItemsAssigned(false);
        viewModel.selectedYear(awardYearData);
        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });

    it(":selectedChecklistForYear returns true if selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedChecklistForYear()).toBeTruthy();
    });

    it(":selectedChecklistForYear returns true if StudentChecklists is null", function () {
        viewModel.StudentChecklists(null);
        expect(viewModel.selectedChecklistForYear()).toBeTruthy();
    });

    it(":selectedChecklistForYear returns true if there is no matching checklist for the year", function () {
        awardYearData.Code("foo");
        viewModel.selectedYear(awardYearData);
        viewModel.StudentChecklists(studentChecklistData);
        expect(viewModel.selectedChecklistForYear()).toBeTruthy();
    });

    it(":selectedChecklistForYear returns expected checklist for the year", function () {
        viewModel.selectedYear(awardYearData);
        viewModel.StudentChecklists(studentChecklistData);
        var expectedChecklist = studentChecklistData[0];
        expect(viewModel.selectedChecklistForYear()).toEqual(expectedChecklist);
    });

    it(":selectedAverageAwardPackageForYear returns true if selected award year is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.selectedAverageAwardPackageForYear()).toBeTruthy();
    });

    it(":selectedAverageAwardPackageForYear returns true if AverageAwardPackages list is null", function () {
        viewModel.AverageAwardPackages(null);
        expect(viewModel.selectedAverageAwardPackageForYear()).toBeTruthy();
    });

    it(":selectedAverageAwardPackageForYear returns null if no matching award package is found", function () {
        awardYearData.Code("foo");
        viewModel.selectedYear(awardYearData);
        viewModel.AverageAwardPackages(averageAwardPackageData);
        expect(viewModel.selectedAverageAwardPackageForYear()).toBe(null);
    });

    it(":selectedAverageAwardPackageForYear returns expected package", function () {        
        viewModel.selectedYear(awardYearData);
        viewModel.AverageAwardPackages(averageAwardPackageData);
        var expectedPackage = averageAwardPackageData[0];
        expect(viewModel.selectedAverageAwardPackageForYear()).toEqual(expectedPackage);
    });

    it(":studentHasLoanHistory returns false if StudentLoansSummary is null", function () {
        viewModel.StudentLoansSummary(null);
        expect(viewModel.studentHasLoanHistory()).toBeFalsy();
    });

    it(":studentHasLoanHistory returns false if AggregateLoanAmount is 0", function () {
        loanSummaryData.AggregateTotalLoanAmount(0);
        viewModel.StudentLoansSummary(loanSummaryData);
        expect(viewModel.studentHasLoanHistory()).toBeFalsy();
    });

    it(":studentHasLoanHistory returns true if student loans summary data is not empty and aggregate amount is gt 0", function () {        
        viewModel.StudentLoansSummary(loanSummaryData);
        expect(viewModel.studentHasLoanHistory()).toBeTruthy();
    });

    it(":areHelpfulLinksReceived returns false if HelpfulLinks list is null", function () {
        viewModel.HelpfulLinks(null);
        expect(viewModel.areHelpfulLinksReceived()).toBeFalsy();
    });

    it(":areHelpfulLinksReceived returns false if HelpfulLinks list is empty", function () {
        viewModel.HelpfulLinks([]);
        expect(viewModel.areHelpfulLinksReceived()).toBeFalsy();
    });

    it(":areHelpfulLinksReceived returns true if HelpfulLinks list is not null or empty", function () {
        viewModel.HelpfulLinks(linksData);
        expect(viewModel.areHelpfulLinksReceived()).toBeTruthy();
    });

    it(":areFormLinksReceived returns false if FormLinks list is null", function () {
        viewModel.FormLinks(null);
        expect(viewModel.areFormLinksReceived()).toBeFalsy();
    });

    it(":areFormLinksReceived returns false if FormLinks list is empty", function () {
        viewModel.FormLinks([]);
        expect(viewModel.areFormLinksReceived()).toBeFalsy();
    });

    it(":areFormLinksReceived returns true if FormLinks list is not null or empty", function () {
        viewModel.FormLinks(linksData);
        expect(viewModel.areFormLinksReceived()).toBeTruthy();
    });

    it(":areLinksReceived returns false if there are no form or helpful links", function () {
        viewModel.HelpfulLinks([]);
        viewModel.FormLinks([]);
        expect(viewModel.areLinksReceived()).toBeFalsy();
    });

    it(":areLinksReceived returns true if there are helpful links", function () {
        viewModel.HelpfulLinks(linksData);
        viewModel.FormLinks([]);
        expect(viewModel.areLinksReceived()).toBeTruthy();
    });

    it(":areLinksReceived returns true if there are form links", function () {
        viewModel.HelpfulLinks(null);
        viewModel.FormLinks(linksData);
        expect(viewModel.areLinksReceived()).toBeTruthy();
    });

    it(":areLinksReceived returns true if there are both form and helpful links", function () {
        viewModel.HelpfulLinks(linksData);
        viewModel.FormLinks(linksData);
        expect(viewModel.areLinksReceived()).toBeTruthy();
    });

    it(":showNoChecklistItemsMessage set to true when no checklist items are assigned and in admin view", function () {
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.isAdmin(true);
        viewModel.selectedYear().AreChecklistItemsAssigned(false);
        expect(viewModel.showNoChecklistItemsMessage()).toBeTruthy();
    });

    it(":showNoChecklistItemsMessage set to true when no checklist items are assigned and in proxy view", function () {        
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.IsProxyView(true);
        viewModel.selectedYear().AreChecklistItemsAssigned(false);
        expect(viewModel.showNoChecklistItemsMessage()).toBeTruthy();
    });

    it(":showNoChecklistItemsMessage set to true when no checklist items are assigned and in student view", function () {        
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.isAdmin(false);
        viewModel.IsProxyView(false);
        viewModel.selectedYear().AreChecklistItemsAssigned(false);
        expect(viewModel.showNoChecklistItemsMessage()).toBeTruthy();
    });

    it(":showNoChecklistItemsMessage set to false when checklist items are assigned", function () {        
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.selectedYear().AreChecklistItemsAssigned(true);
        expect(viewModel.showNoChecklistItemsMessage()).toBeFalsy();
    });

    it(":showNotification is set to false if showUI is false", function () {
        viewModel.showUI(false);
        expect(viewModel.showNotification()).toBeFalsy();
    });

    it(":showNotification is set to true if showNoChecklistItemsMessage is true", function () {
        viewModel.Configuration(configurationData);
        viewModel.showUI(true);        
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.IsProxyView(true);
        viewModel.selectedYear().AreChecklistItemsAssigned(false);
        expect(viewModel.showNotification()).toBeTruthy();
    });

    it(":showNotification is set to true if no award years", function () {
        viewModel.Configuration(configurationData);
        viewModel.showUI(true);
        expect(viewModel.showNotification()).toBeTruthy();
    });

    it(":showNotification is set to false if showNoChecklistItemsMessage is false and award years are present", function () {
        viewModel.Configuration(configurationData);
        viewModel.showUI(true);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.selectedYear().AreChecklistItemsAssigned(true);
        expect(viewModel.showNotification()).toBeFalsy();
    });

    it(":notificationMessage contains the expected value when no checklist items assigned in admin view", function () {
        var expectedMessage = "Student has no checklist items";

        viewModel.Configuration(configurationData);
        viewModel.showUI(true);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.isAdmin(true);
        viewModel.selectedYear().AreChecklistItemsAssigned(false);

        expect(viewModel.notificationMessage()).toEqual(expectedMessage);
    });

    it(":notificationMessage contains the expected value when no checklist items assigned in proxy view", function () {
        var expectedMessage = "Student has no checklist items";

        viewModel.Configuration(configurationData);
        viewModel.showUI(true);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.IsProxyView(true);
        viewModel.selectedYear().AreChecklistItemsAssigned(false);

        expect(viewModel.notificationMessage()).toEqual(expectedMessage);
    });

    it(":notificationMessage contains the expected value when no checklist items assigned in student view", function () {
        var expectedMessage = "You have no checklist items";

        viewModel.Configuration(configurationData);
        viewModel.showUI(true);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);        
        viewModel.selectedYear().AreChecklistItemsAssigned(false);

        expect(viewModel.notificationMessage()).toEqual(expectedMessage);
    });

    it(":notificationMessage contains the expected value when no award years", function () {
        var expectedMessage = "No fin aid data available";

        viewModel.Configuration(configurationData);
        viewModel.showUI(true);
        

        expect(viewModel.notificationMessage()).toEqual(expectedMessage);
    });

    it(":notificationMessage is undefined if showUI is false", function () {
        
        viewModel.Configuration(configurationData);
        viewModel.showUI(false);


        expect(viewModel.notificationMessage()).toBeUndefined();
    });

    it(":notificationMessage is undefined if none of the conditions for a message met", function () {

        viewModel.Configuration(configurationData);
        viewModel.showUI(false);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.selectedYear().AreChecklistItemsAssigned(true);


        expect(viewModel.notificationMessage()).toBeUndefined();
    });

    it(":displayPellLEUPercentage returns false if yearConfiguration is null", function () {
        viewModel.Configuration(null);
        viewModel.PellLifetimeEligibilityUsedPercentage(23);
        expect(viewModel.displayPellLEUPercentage()).toBe(false);
    });

    it(":displayPellLEUPercentage returns false if yearConfiguration is undefined", function () {
        viewModel.Configuration();
        viewModel.PellLifetimeEligibilityUsedPercentage(23);
        expect(viewModel.displayPellLEUPercentage()).toBe(false);
    });

    it(":displayPellLEUPercentage returns false if PellLifetimeEligibilityUsedPercentage is null", function () {
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.PellLifetimeEligibilityUsedPercentage(null);
        expect(viewModel.displayPellLEUPercentage()).toBe(false);
    });

    it(":displayPellLEUPercentage returns false if PellLifetimeEligibilityUsedPercentage is undefined", function () {
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.PellLifetimeEligibilityUsedPercentage();
        expect(viewModel.displayPellLEUPercentage()).toBe(false);
    });

    it(":displayPellLEUPercentage returns false if DisplayPellLEU flag is set to false", function () {
        configurationData.AwardYearConfigurations()[0].DisplayPellLifetimeEligibilityUsedPercentage(false);
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.PellLifetimeEligibilityUsedPercentage(35.78);
        expect(viewModel.displayPellLEUPercentage()).toBe(false);
    });

    it(":displayPellLEUPercentage returns true if DisplayPellLEU flag is set to true and percentage is present", function () {
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.PellLifetimeEligibilityUsedPercentage(35.78);
        expect(viewModel.displayPellLEUPercentage()).toBe(true);
    });

    it(":displayStudentAccountSummary returns false if awardYearConfiguration is null", function () {
        expect(viewModel.displayStudentAccountSummary()).toBe(false);
    });

    it(":displayStudentAccountSummary returns false if SuppressAccountSummaryDisplay flag is true", function () {
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        expect(viewModel.displayStudentAccountSummary()).toBe(false);
    });

    it(":displayStudentAccountSummary returns false if StudentAccountSummary is null", function () {
        configurationData.AwardYearConfigurations()[0].SuppressStudentAccountSummaryDisplay(false);
        viewModel.Configuration(configurationData);       
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.StudentAccountSummary(null);
        expect(viewModel.displayStudentAccountSummary()).toBe(false);
    });

    it(":displayStudentAccountSummary returns false if StudentAccountSummary is 'undefined'", function () {
        configurationData.AwardYearConfigurations()[0].SuppressStudentAccountSummaryDisplay(false);
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.StudentAccountSummary();
        expect(viewModel.displayStudentAccountSummary()).toBe(false);
    });

    it(":displayStudentAccountSummary returns true if StudentAccountSummary is defined and not suppressed", function () {
        configurationData.AwardYearConfigurations()[0].SuppressStudentAccountSummaryDisplay(false);
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.StudentAccountSummary(testData().studentAccountSummaryData);
        expect(viewModel.displayStudentAccountSummary()).toBe(true);
    });

    it(":homeStepStyle returns expected style when displayStudentAccountSummary and displayAverageAwardPackage are false", function () {
        var expected = "one-home-steps-width";
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        expect(viewModel.homeStepStyle()).toEqual(expected);
    });

    it(":homeStepStyle returns expected style when displayStudentAccountSummary is true, but displayAverageAwardPackage is false", function () {
        var expected = "two-home-steps-width";
        configurationData.AwardYearConfigurations()[0].SuppressStudentAccountSummaryDisplay(false);
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.StudentAccountSummary(testData().studentAccountSummaryData);
        expect(viewModel.homeStepStyle()).toEqual(expected);
    });

    it(":homeStepStyle returns expected style when displayStudentAccountSummary is false, and displayAverageAwardPackage is true", function () {
        var expected = "two-home-steps-width";
        configurationData.AwardYearConfigurations()[0].SuppressAverageAwardPackageDisplay(false);
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        expect(viewModel.homeStepStyle()).toEqual(expected);
    });

    it(":homeStepStyle returns expected style when displayAverageAwardPackage and displayStudentAccountSummary are true", function () {
        var expected = "three-home-steps-width";
        configurationData.AwardYearConfigurations()[0].SuppressAverageAwardPackageDisplay(false);
        configurationData.AwardYearConfigurations()[0].SuppressStudentAccountSummaryDisplay(false);
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        viewModel.StudentAccountSummary(testData().studentAccountSummaryData);
        expect(viewModel.homeStepStyle()).toEqual(expected);
    });
    it(":displayAverageAwardPackage returns false if awardYearConfiguration is null", function () {
        expect(viewModel.displayAverageAwardPackage()).toBe(false);
    });

    it(":displayAverageAwardPackage returns false if SuppressAverageAwardPackageDisplay flag is true", function () {
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        expect(viewModel.displayAverageAwardPackage()).toBe(false);
    });

    
    it(":displayAverageAwardPackage returns true if SuppressAverageAwardPackageDisplay is false", function () {
        configurationData.AwardYearConfigurations()[0].SuppressAverageAwardPackageDisplay(false);
        viewModel.Configuration(configurationData);
        viewModel.AwardYears(awardYearsData);
        viewModel.selectedYear(awardYearData);
        expect(viewModel.displayAverageAwardPackage()).toBe(true);
    });

    
});