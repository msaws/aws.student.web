﻿//Copyright 2015 Ellucian Company L.P. and its affiliates
var personProxyLoadingThrobberMessage = "Loading...";
var personProxyLoadingThrobberAltText = "Alt Loading";
var personProxyChangingThrobberMessage = "Changing...";
var personProxyChangingThrobberAltText = "Alt Changing";

describe("award.letter.js", function () {
    var contentType;

    beforeAll(function () {
        faNotAuthorizedMessage = "Not authorized!";
        faMissingStudentIdMessage = "Missing id!";
        faAwardLetterUnableToLoadMessage = "Unable to load!";
        getGeneralLetterDataActionUrl = "getGeneralDataUrl";
        getAwardLetterDataActionUrl = "getAwardLetterUrl";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        awardLetterAdminActionUrl = "adminUrl";
        awardLetterViewModelInstance = new awardLetterViewModel();
        awardLetterViewModelInstance.selectedYear = ko.observable({ Code: ko.observable(), ArePrerequisitesSatisfied: ko.observable() });
        faInvalidStudentIdMessage = "Invalid id";        
    });

    beforeEach(function () {
        contentType = "json";
    });

    afterEach(function () {
        contentType = null;
    });

    it(":ajax is invoked with expected url", function () {
        spyOn($, "ajax");
        getAwardLetter();
        expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual("getGeneralDataUrl");
    });

    it(":ajax is invoked with expected dataType", function () {
        spyOn($, "ajax");
        getAwardLetter();
        expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
    });

    it(":ajax is invoked with expected action type", function () {
        spyOn($, "ajax");
        getAwardLetter();
        expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
    });

    it(":ajax success callback invokes ko.mapping.fromJS", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(ko.mapping, "fromJS");
        getAwardLetter();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it(":ajax success callback sets selectedYear to the expected value", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        awardLetterViewModelInstance.AwardYears([{ Code: ko.observable("2015"), ArePrerequisitesSatisfied: ko.observable(true) }]);
        memory.setItem("yearCode", "2015");
        getAwardLetter();
        expect(awardLetterViewModelInstance.selectedYear().Code()).toEqual("2015");
    });

    it(":ajax success callback invokes getAwardLetter function if ArePrerequisitesSatisfied flag is true", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        
        spyOn(utility, "getAwardLetterData");
        awardLetterViewModelInstance.AwardYears([{ Code: ko.observable("2015"), ArePrerequisitesSatisfied: ko.observable(true) }]);
        memory.setItem("yearCode", "2015");
        getAwardLetter();
        expect(utility.getAwardLetterData).toHaveBeenCalled();
    });

    it(":ajax error callback places debug info to console", function () {
        var jqXhr = {
            status: 'status',
            responseText: 'response'
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn(console, "debug");
        getAwardLetter();
        expect(console.debug).toHaveBeenCalledWith("Award letter retrieval failed: status response");
    });

    it(":ajax error callback sets expected message in notificationCenter when 403 status", function () {
        var jqXhr = {
            status: 403
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getAwardLetter();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Not authorized!", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when 400 status", function () {
        var jqXhr = {
            status: 400
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getAwardLetter();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Missing id!", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when 404 status", function () {
        var jqXhr = {
            status: 404
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getAwardLetter();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Invalid id", type: "error" });
    });

    it(":ajax error callback sets expected message in notificationCenter when 500 status", function () {
        var jqXhr = {
            status: 500
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getAwardLetter();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Unable to load!", type: "error" });
    });

    it(":ajax complete callback triggers checkForMobile function", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn(awardLetterViewModelInstance, "checkForMobile");
        getAwardLetter();
        expect(awardLetterViewModelInstance.checkForMobile).toHaveBeenCalled();
    });
});