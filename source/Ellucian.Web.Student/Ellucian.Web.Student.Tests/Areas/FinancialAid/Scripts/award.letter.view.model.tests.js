﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("awardLetterViewModel", function () {

    beforeAll(function () {
        getLetterActionUrl = "getUrl/";
        getAwardLetterReportActionUrl = "reportUrl/";
        updateAwardLetterActionUrl = "updateUrl/"
        jsonContentType = "json";
        faAwardLetterSigningMessage = "Signing Award Letter";
        faAwardLetterSignedMessage = "Award Letter Signed!!";
        faAwardLetterUnableToRetrieveReport = "Unable to retrieve the report";
        faAwardLetterErrorUpdatingMessage = "Unable to update award letter";
        awardLetterAdminActionUrl = "adminUrl";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    var viewModel,
        awardYear,
        awardLetter,
        student,
        acceptButtonEvent,
        acceptAwardLetterResponseData,
        awardLetterHistoryItems,
        awardLetterId,
        awardPackageChecklistItemControlStatus,
        e;

    beforeEach(function () {
        viewModel = new awardLetterViewModel();

        awardLetterId = "55";

        e = {
            preventDefault: function () { }
        };

        //some tests are changing the value of this url. reset it to original value here
        getAwardLetterReportActionUrl = "reportUrl/";

        awardYear = {
            Code: ko.observable("2014"),
            AreChecklistItemsAssigned: ko.observable(false)
        },
        awardLetter = {
            AwardYearCode: ko.observable("2014"),
            AreAnyAwardsPending: ko.observable(false),
            IsLetterAccepted: ko.observable(false),
            AcceptedDate: ko.observable(),
            AcceptedDateDisplay: ko.observable(),
            CurrentDate: ko.observable(),
            AwardLetterRecordId: ko.observable(awardLetterId)
        },
        student = {
            Id: ko.observable("0003914")
        },
        acceptButtonEvent = {
            currentTarget: {
                name: "AcceptButton"
            }
        },
        acceptAwardLetterResponseData = {
            IsLetterAccepted: true,
            AcceptedDate: "04/06/2015",
            AcceptedDateDisplay: "April 06, 2015"
        },
        awardLetterHistoryItems =
        [
            {
                Id: ko.observable("12"),
                CreatedDate: ko.observable("09/08/2015"),
                isItemVisible: ko.observable(true)
            },
            {
                Id: ko.observable("16"),
                CreatedDate: ko.observable("10/08/2015"),
                isItemVisible: ko.observable(true)
            },
            {
                Id: ko.observable("25"),
                CreatedDate: ko.observable("10/08/2016"),
                isItemVisible: ko.observable(true)
            },
            {
                Id: ko.observable("169"),
                CreatedDate: ko.observable("10/08/2014"),
                isItemVisible: ko.observable(true)
            }
        ],
        awardPackageChecklistItemControlStatus = "RemovedFromChecklist";

    });

    afterEach(function () {

    });

    it(":showUI is false after initialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":Student is undefined after initialization", function () {
        expect(viewModel.Student()).toBeUndefined();
    });

    it(":StudentAwardLetter is undefined after initialization", function () {
        expect(viewModel.StudentAwardLetter()).toBeUndefined();
    });

    it(":IsLoading is false after initialization", function () {
        expect(viewModel.isLoading()).toBeFalsy();
    });
    
    it(":acceptAwardLetterInProgress is false after initialization", function () {
        expect(viewModel.acceptAwardLetterInProgress()).toBeFalsy();
    });

    it(":showAllAwardLetterHistoryItems is false after initialization", function () {
        expect(viewModel.showAllAwardLetterHistoryItems()).toBeFalsy();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(awardLetterAdminActionUrl);
    });

    it(":areChecklistItemsAssigned is false if selectedYear is null", function () {
        viewModel.selectedYear(null);
        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });

    it(":areChecklistItemsAssigned is false if selectedYear is undefined", function () {
        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });
    
    it(":areChecklistItemsAssigned returns true value from property of selectedYear", function () {
        viewModel.StudentAwardLetter(awardLetter);
        awardYear.AreChecklistItemsAssigned(true);
        viewModel.selectedYear(awardYear);

        expect(viewModel.areChecklistItemsAssigned()).toBeTruthy();
    });

    it(":areChecklistItemsAssigned returns false value from property of selectedYear", function () {
        viewModel.StudentAwardLetter(awardLetter);
        awardYear.AreChecklistItemsAssigned(false);
        viewModel.selectedYear(awardYear);

        expect(viewModel.areChecklistItemsAssigned()).toBeFalsy();
    });
    

    it(":StudentAwardLetter has AwardYearCode that matches Code of selectedYear", function () {
        awardLetter.AwardYearCode("2010");
        awardYear.Code("2010");
        viewModel.StudentAwardLetter(awardLetter);
        viewModel.selectedYear(awardYear);

        expect(viewModel.StudentAwardLetter().AwardYearCode()).toEqual(viewModel.selectedYear().Code());
    });
    

    /*******      Not Currently using awardLetterReportLink property   **************/

    //it(":awardLetterReportLink is empty if Student is undefined", function () {
    //    expect(viewModel.Student()).toBeUndefined();
    //    expect(viewModel.awardLetterReportLink()).toEqual("");
    //});

    //it(":awardLetterReportLink is empty if Student is null", function () {
    //    viewModel.Student(null);
    //    expect(viewModel.awardLetterReportLink()).toEqual("");
    //});

    //it(":awardLetterReportLink is empty if selectedYear is null", function () {
    //    viewModel.Student(student);
    //    viewModel.selectedYear(null);

    //    expect(viewModel.awardLetterReportLink()).toEqual("");
    //});

    //it(":awardLetterReportLink is empty if actionUrl is null", function () {
    //    //setting actionUrl first because its not an observable and therefore doesn't trigger a dependency change on awardLetterReportLink
    //    getAwardLetterReportActionUrl = null;
    //    viewModel.Student(student);
    //    viewModel.selectedYear(awardYear);
        
    //    expect(viewModel.awardLetterReportLink()).toEqual("");
    //});

    //it(":awardLetterReportLink is formatted correctly", function () {
    //    viewModel.Student(student);
    //    viewModel.selectedYear(awardYear);
    //    viewModel.StudentAwardLetters.push(awardLetter);
    //    var expectedString = getAwardLetterReportActionUrl + "?studentId=" + student.Id() + "&awardLetterId=" + awardLetterId;

    //    expect(viewModel.awardLetterReportLink()).toEqual(expectedString);
    //});

    it(":awardLetterButtonHandler doesn't execute if awardLetter argument is null", function () {
        spyOn(viewModel, "acceptAwardLetterInProgress");
        expect(viewModel.awardLetterButtonHandler(null, null)).toBeTruthy();
        expect(viewModel.acceptAwardLetterInProgress).not.toHaveBeenCalled();
    });

    it(":awardLetterButtonHandler sets AcceptedDate if AcceptButton is clicked", function () {        
        spyOn(viewModel, "acceptAwardLetterInProgress");
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(acceptAwardLetterResponseData);
        });

        viewModel.awardLetterButtonHandler(awardLetter, acceptButtonEvent);

        expect(awardLetter.AcceptedDate()).toEqual(acceptAwardLetterResponseData.AcceptedDate);
        expect(viewModel.acceptAwardLetterInProgress).toHaveBeenCalledWith(true);
    });

    it(":awardLetterButtonHandler opens data loading dialog box before sending request", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });

        viewModel.awardLetterButtonHandler(awardLetter, acceptButtonEvent);
        
        expect(viewModel.isLoading()).toBeTruthy();
    });

    it(":awardLetterButtonHandler accepts award letter on success", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(acceptAwardLetterResponseData);
        });

        viewModel.awardLetterButtonHandler(awardLetter, acceptButtonEvent);

        expect(awardLetter.IsLetterAccepted()).toEqual(acceptAwardLetterResponseData.IsLetterAccepted);
        expect(awardLetter.AcceptedDate()).toEqual(acceptAwardLetterResponseData.AcceptedDate);
        expect(awardLetter.AcceptedDateDisplay()).toEqual(acceptAwardLetterResponseData.AcceptedDateDisplay);
    });

    it(":awardLetterButtonHandler stops loading on error", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.error({ status: 0 }, "", "");
        });
        spyOn(viewModel, "isLoading");

        viewModel.awardLetterButtonHandler(awardLetter, acceptButtonEvent);

        expect(awardLetter.IsLetterAccepted()).toBeFalsy();
        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.isLoading()).toBeFalsy();
    });

    it(":awardLetterButtonHandler stops loading on complete", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
            e.success(acceptAwardLetterResponseData);
            e.complete({}, {});
        });
        spyOn(viewModel, "acceptAwardLetterInProgress");
        spyOn(viewModel, "isLoading");

        viewModel.awardLetterButtonHandler(awardLetter, acceptButtonEvent);

        expect(viewModel.isLoading).toHaveBeenCalledWith(true);
        expect(viewModel.isLoading()).toBeFalsy();
        expect(viewModel.acceptAwardLetterInProgress).toHaveBeenCalledWith(true);
        expect(viewModel.acceptAwardLetterInProgress()).toBeFalsy();
    });

    it(":awardLetterButtonHandler displays the expected error message on error", function () {
        var jqXHR = { status: 403 };
        var secondArg = {
            message: "Unable to update award letter",
            type: "error"
        };

        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        viewModel.awardLetterButtonHandler(awardLetter, acceptButtonEvent);
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":awardLetterAcceptButtonEnabler is false if no StudentAwardLetter for the year", function () {
        expect(viewModel.StudentAwardLetter()).toBeUndefined();
        expect(viewModel.awardLetterAcceptButtonEnabler()).toBeFalsy();
    });

    it(":awardLetterAcceptButtonEnabler is false if AreAnyAwardsPending is true", function () {
        viewModel.selectedYear(awardYear);
        awardLetter.AreAnyAwardsPending(true);
        viewModel.StudentAwardLetter(awardLetter);

        expect(viewModel.awardLetterAcceptButtonEnabler()).toBeFalsy();
    });

    it(":awardLetterAcceptButtonEnabler is false if verification checkbox is false", function () {
        viewModel.selectedYear(awardYear);
        awardLetter.AreAnyAwardsPending(false);
        awardLetter.IsLetterAccepted(false);
        viewModel.StudentAwardLetter(awardLetter);

        expect(viewModel.awardLetterAcceptButtonEnabler()).toBeFalsy();
    });

    it(":awardLetterAcceptButtonEnabler is false if there's already an AcceptedDate", function () {
        viewModel.selectedYear(awardYear);
        awardLetter.AreAnyAwardsPending(false);
        awardLetter.IsLetterAccepted(true);
        awardLetter.AcceptedDate(new Date());
        viewModel.StudentAwardLetter(awardLetter);

        expect(viewModel.awardLetterAcceptButtonEnabler()).toBeFalsy();
    });

    it(":awardLetterAcceptButtonEnabler is true if verification checkbox is true and theres no AcceptedDate", function () {
        viewModel.selectedYear(awardYear);
        awardLetter.AreAnyAwardsPending(false);
        awardLetter.IsLetterAccepted(true);
        awardLetter.AcceptedDate(null);
        viewModel.StudentAwardLetter(awardLetter);

        expect(viewModel.awardLetterAcceptButtonEnabler()).toBeTruthy();
    });    

    it(":revertChanges sets verification checkbox to false", function () {
        viewModel.selectedYear(awardYear);
        awardLetter.IsLetterAccepted(true);
        viewModel.StudentAwardLetter(awardLetter);

        viewModel.revertChanges();

        expect(awardLetter.IsLetterAccepted()).toBeFalsy();
    });

    it(":revertChanges returns true", function () {
        viewModel.selectedYear(awardYear);
        viewModel.StudentAwardLetter(awardLetter);

        expect(viewModel.revertChanges()).toBeTruthy();
    });

    it(":AwardLetterHistoryItems return the expected number of history items", function () {
        viewModel.AwardLetterHistoryItems(awardLetterHistoryItems);
        expect(viewModel.AwardLetterHistoryItems().length).toEqual(awardLetterHistoryItems.length);
    });    

    it(":areAnyAwardLetterHistoryRecords returns false if AwardLetterHistoryItems is null", function () {
        expect(viewModel.AwardLetterHistoryItems()).toEqual([]);
        expect(viewModel.areAnyAwardLetterHistoryRecords()).toBeFalsy();
    });

    it(":areAnyAwardLetterHistoryRecords returns true if there is a set of award letter history items", function () {        
        viewModel.AwardLetterHistoryItems(awardLetterHistoryItems);

        expect(viewModel.AwardLetterHistoryItems()).not.toBeNull();
        expect(viewModel.areAnyAwardLetterHistoryRecords()).toBeTruthy();
    });

    it(":areAnyAwardLetterHistoryRecords returns false if there are no history records for the year", function () {
        awardLetterHistoryItems= [];
        viewModel.AwardLetterHistoryItems(awardLetterHistoryItems);

        expect(viewModel.AwardLetterHistoryItems()).not.toBeNull();
        expect(viewModel.areAnyAwardLetterHistoryRecords()).toBeFalsy();
    });

    it(":amountReportSectionStyle returns expected css classes when areAnyAwardLetterHistoryRecords is false", function () {       
        viewModel.AwardLetterHistoryItems([]);
        expect(viewModel.areAnyAwardLetterHistoryRecords()).toBeFalsy();
        var style = "section-wrapper clear-group";
        expect(viewModel.amountReportSectionStyle()).toEqual(style);
    });

    it(":amountReportSectionStyle returns expected css classes when areAnyAwardLetterHistoryRecords is true", function () {        
        viewModel.AwardLetterHistoryItems(awardLetterHistoryItems);

        expect(viewModel.areAnyAwardLetterHistoryRecords()).toBeTruthy();

        var style = "section-wrapper clear-group column-six";
        expect(viewModel.amountReportSectionStyle()).toEqual(style);
    });

    it(":isAwardPackageChecklistItemRemoved returns true when the control status is RemovedFromChecklist", function () {
        viewModel.AwardPackageChecklistItemControlStatus(awardPackageChecklistItemControlStatus);
        expect(viewModel.isAwardPackageChecklistItemRemoved()).toBeTruthy();
    });

    it(":isAwardPackageChecklistItemRemoved returns false when the control status is not RemovedFromChecklist", function () {
        viewModel.AwardPackageChecklistItemControlStatus("foo");
        expect(viewModel.isAwardPackageChecklistItemRemoved()).toBeFalsy();
    });

    it(":isAwardPackageChecklistItemRemoved returns false when AwardPackageChecklistItemControlStatus is undefined", function () {        
        expect(viewModel.isAwardPackageChecklistItemRemoved()).toBeFalsy();
    });

    it(":isAwardPackageChecklistItemRemoved returns false when AwardPackageChecklistItemControlStatus is null", function () {
        viewModel.AwardPackageChecklistItemControlStatus(null);
        expect(viewModel.isAwardPackageChecklistItemRemoved()).toBeFalsy();
    });

    it(":awardLetterHistoryItemsToDisplay returns an empty array if there are no AwardLetterHistoryItems", function () {
        expect(viewModel.awardLetterHistoryItemsToDisplay()).toEqual([]);
    });

    it(":awardLetterHistoryItemsToDisplay returns the expected number of award letter history items", function () {
        viewModel.AwardLetterHistoryItems(awardLetterHistoryItems);
        expect(viewModel.awardLetterHistoryItemsToDisplay().length).toEqual(awardLetterHistoryItems.length);
    });

    it(":awardLetterHistoryItemsToDisplay - only first 3 items are visible if showAllAwardLetterHistoryItems flag is false", function () {
        viewModel.AwardLetterHistoryItems(awardLetterHistoryItems);
        viewModel.showAllAwardLetterHistoryItems(false);
        for (var i = 0; i < 3; i++) {
            expect(viewModel.awardLetterHistoryItemsToDisplay()[i].isItemVisible()).toBeTruthy();
        }
        for (var i = 3; i < awardLetterHistoryItems.length; i++) {
            expect(viewModel.awardLetterHistoryItemsToDisplay()[i].isItemVisible()).toBeFalsy();
        }
    });

    it(":awardLetterHistoryItemsToDisplay - all items are visible if showAllAwardLetterHistoryItems flag is true", function () {
        viewModel.AwardLetterHistoryItems(awardLetterHistoryItems);
        viewModel.showAllAwardLetterHistoryItems(true);
        
        for (var i = 0; i < awardLetterHistoryItems.length; i++) {
            expect(viewModel.awardLetterHistoryItemsToDisplay()[i].isItemVisible()).toBeTruthy();
        }
    });

    it(":toggleAwardLetterHistoryView sets showAllAwardLetterHistoryItems to true", function () {
        viewModel.showAllAwardLetterHistoryItems(false);
        viewModel.toggleAwardLetterHistoryView();
        expect(viewModel.showAllAwardLetterHistoryItems()).toBeTruthy();
    });

    it(":redirectToUrl returns null if Student is undefined", function () {        
        expect(viewModel.redirectToUrl(awardLetter, e)).toEqual("");
    });

    it(":redirectToUrl returns null if Student is null", function () {
        viewModel.Student(null);
        expect(viewModel.redirectToUrl(awardLetter, e)).toEqual("");
    });

    it(":redirectToUrl returns null if getAwardLetterReportActionUrl is undefined", function () {
        getAwardLetterReportActionUrl = 'undefined';
        expect(viewModel.redirectToUrl(awardLetter, e)).toEqual("");
    });

    it(":redirectToUrl returns null if getAwardLetterReportActionUrl is null", function () {
        getAwardLetterReportActionUrl = null;
        expect(viewModel.redirectToUrl(awardLetter, e)).toEqual("");
    });

    it(":redirectToUrl uses correct content type", function () {
        var contentType = 'application/pdf';
        viewModel.Student(student);
        spyOn($, "ajax");
        viewModel.redirectToUrl(awardLetter, e);
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);       
    });

    it(":redirectToUrl calls window.open on success", function () {
        viewModel.Student(student);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(acceptAwardLetterResponseData);
        });
        spyOn(window, "open");
        var url = getAwardLetterReportActionUrl + "?studentId=" + student.Id() + "&awardLetterId=" + awardLetter.AwardLetterRecordId();
        viewModel.redirectToUrl(awardLetter, e);
        expect(window.open).toHaveBeenCalledWith(url);        
    });

    it("redirectToUrl displays expected error message on error", function () {
        var jqXHR = {
            status: 403
        };
        var secondArg = {
            message: "Unable to retrieve the report",
            type: "error",
            flash: true
        };
        viewModel.Student(student);
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        viewModel.redirectToUrl(awardLetter, e);
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });
});