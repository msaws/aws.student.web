﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
describe("loan.request.mapping.js", function () {

    var loanRequestData,
        loanRequestViewModelInstance,
        inputField;

    var global = jasmine.getGlobal();

    beforeAll(function () {
        loanRequestData = testData().loanRequestData;
        faLoanRequestExtensionText = "Ext. {0}";
        faLoanRequestCounselorMessage = "This is {0} a loan request {1} counselor message {2}";
        faLoanRequestUnmetCostRequestAmountMessage = "Unmet cost message {0}";
        currentStep = "current";
        completeStep = "complete";
        incompleteStep = "incomplete";
        loanRequestAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        loanRequestViewModelInstance = new loanRequestViewModel();
        ko.mapping.fromJS(loanRequestData, loanRequestViewModelMapping, loanRequestViewModelInstance);

        inputField = document.createElement('input');
        inputField.id = 'customAmountInput';
        document.body.appendChild(inputField);
    });

    describe("loanCounselorModelTests", function () {
        var counselorData,
            counselorModel;
        
        beforeEach(function () {
            counselorData = loanRequestData.AssignedToCounselor();            
        });

        it(":contactMessage equals expected value", function () {
            var phone = "<a href=\'tel:7775674567\'>777-567-4567</a> ";
            var ext = "Ext. 123";
            var email = " (<a href=\'mailto:counselor@gmail.com\'>counselor@gmail.com</a>) ";
            var expectedMessage = faLoanRequestCounselorMessage.format(phone, ext, email);

            counselorModel = new loanCounselorModel(counselorData);

            expect(counselorModel.contactMessage()).toEqual(expectedMessage);
        });

        it(":contactMessage equals expected value when there is no phone number", function () {
            counselorData.PhoneNumber('');
            counselorModel = new loanCounselorModel(counselorData);

            var phone = " ";
            var ext = "Ext. 123";
            var email = " (<a href=\'mailto:counselor@gmail.com\'>counselor@gmail.com</a>) ";
            var expectedMessage = faLoanRequestCounselorMessage.format(phone, ext, email);

            expect(counselorModel.contactMessage()).toEqual(expectedMessage);
        });

        it(":contactMessage equals expected value when there is no extension", function () {
            counselorData.Extension('');
            counselorData.PhoneNumber('777-567-4567');
            counselorModel = new loanCounselorModel(counselorData);

            var phone = "<a href=\'tel:7775674567\'>777-567-4567</a> ";
            var ext = "";
            var email = " (<a href=\'mailto:counselor@gmail.com\'>counselor@gmail.com</a>) ";
            var expectedMessage = faLoanRequestCounselorMessage.format(phone, ext, email);

            expect(counselorModel.contactMessage()).toEqual(expectedMessage);
        });

        it(":contactMessage equals expected value when there is no email address", function () {
            counselorData.EmailAddress('');
            counselorData.Extension('123');
            counselorModel = new loanCounselorModel(counselorData);

            var phone = "<a href=\'tel:7775674567\'>777-567-4567</a> ";
            var ext = "Ext. 123";
            var email = " ";
            var expectedMessage = faLoanRequestCounselorMessage.format(phone, ext, email);

            expect(counselorModel.contactMessage()).toEqual(expectedMessage);
        });

        it(":contactMessage equals expected value when there is no phone, extension, and email address", function () {
            counselorData.EmailAddress('');
            counselorData.Extension('');
            counselorData.PhoneNumber('');
            counselorModel = new loanCounselorModel(counselorData);

            var phone = " ";
            var ext = "";
            var email = " ";
            var expectedMessage = faLoanRequestCounselorMessage.format(phone, ext, email);

            expect(counselorModel.contactMessage()).toEqual(expectedMessage);
        });
    });

    describe("loanRequestAwardPeriodModelTests", function () {

        var awardPeriodData,
            awardPeriodModel;

        beforeEach(function () {
            awardPeriodData = loanRequestData.LoanRequestAwardPeriods[0];
            awardPeriodModel = new loanRequestAwardPeriodModel(awardPeriodData);
        });

        it(":loanRequestAwardPeriodModel is not null", function () {
            expect(awardPeriodModel).not.toBeUndefined();
        });

        it("loanRequestAwardPeriodModel properties match expected", function () {
            var expectedCode = awardPeriodData.Code();
            var expectedDescr = awardPeriodData.Description();

            expect(awardPeriodModel.Code()).toEqual(expectedCode);
            expect(awardPeriodModel.Description()).toEqual(expectedDescr);
        });
    });

    describe("loanRequestModelTests", function () {

        var requestModel;

        beforeEach(function () {
            requestModel = new loanRequestModel(loanRequestData);
        });

        it(":MaximumRequestAmount is undefined on initialization", function () {
            expect(requestModel.MaximumRequestAmount()).toBeUndefined();
        });

        it(":customRequestAmount is undefined on initialization", function () {
            expect(requestModel.customRequestAmount()).toBeFalsy();
        });

        it(":unmetCostRequestAmountMessage returns expected value", function () {
            requestModel.UnmetCost(55);

            var expected = "Unmet cost message <span class=\"bold\">$55.00</span>";
            expect(requestModel.unmetCostRequestAmountMessage()).toEqual(expected);
        });

        it(":selectedRequestOption is 'unmetCost' if the UnmetCost is greater than 0", function () {
            requestModel.UnmetCost(1234);
            expect(requestModel.selectedRequestOption()).toEqual('unmetCost');
        });

        it(":selectedRequestOption is 'customAmount' if the UnmetCost is 0", function () {
            loanRequestData.UnmetCost(0);
            requestModel = new loanRequestModel(loanRequestData);
            expect(requestModel.selectedRequestOption()).toEqual('customAmount');
        });

        it(":selectedRequestAmount equals expected amount if selected option is 'unmetCost'", function () {
            loanRequestData.UnmetCost(12344);
            requestModel = new loanRequestModel(loanRequestData);
            expect(requestModel.selectedRequestAmount()).toEqual(12344);
        });

        it(":selectedRequestAmount equals expected amount if selected option is 'customAmount'", function () {
            loanRequestData.UnmetCost(0);
            requestModel = new loanRequestModel(loanRequestData);
            var expected = 567;
            requestModel.customRequestAmount(expected);
            expect(requestModel.selectedRequestAmount()).toEqual(expected);
        });

        it(":showCostEquation is initialized to false", function () {
            expect(requestModel.showCostEquation()).toBe(false);
        });

        it(":showHideCostEquation sets showCostEquation to false if it is initially true", function () {
            requestModel.showCostEquation(true);
            requestModel.showHideCostEquation();
            expect(requestModel.showCostEquation()).toBe(false);
        });

        it(":showHideCostEquation sets showCostEquation to true if it is initially false", function () {
            requestModel.showCostEquation(false);
            requestModel.showHideCostEquation();
            expect(requestModel.showCostEquation()).toBe(true);
        });

        it(":selectedAwardPeriods has expected number of periods after initialization", function () {
            var expected = loanRequestData.LoanRequestAwardPeriods.length;
            expect(requestModel.selectedAwardPeriods().length).toEqual(expected);
        });

        it(":currentWorkFlowStep is initialized to 'EnterLoanAmount' if there is no pending request", function () {
            loanRequestData.IsRequestPending(false);
            requestModel = new loanRequestModel(loanRequestData);

            expect(requestModel.currentWorkFlowStep()).toEqual('EnterLoanAmount');
        });

        it(":currentWorkFlowStep is initialized to 'Confirmation' if there is a pending request", function () {
            loanRequestData.IsRequestPending(true);
            requestModel = new loanRequestModel(loanRequestData);

            expect(requestModel.currentWorkFlowStep()).toEqual('Confirmation');
        });

        it(":currentWorkFlowStep is set to 'SelectPeriods' after executing next step if current workflow step is 'EnterLoanAmount'", function () {
            loanRequestData.IsRequestPending(false);
            requestModel = new loanRequestModel(loanRequestData);
            requestModel.nextStep({});
            expect(requestModel.currentWorkFlowStep()).toEqual('SelectPeriods');
        });

        it(":currentWorkFlowStep is set to 'ReviewAndSubmit' after executing next step if current workflow step is 'SelectPeriods'", function () {
            requestModel.currentWorkFlowStep('SelectPeriods');
            requestModel.nextStep({});
            expect(requestModel.currentWorkFlowStep()).toEqual('ReviewAndSubmit');
        });

        it(":currentWorkFlowStep is set to 'EnterLoanAmount' after executing prev step if current workflow step is 'SelectPeriods'", function () {
            requestModel.currentWorkFlowStep('SelectPeriods');
            requestModel.prevStep({});
            expect(requestModel.currentWorkFlowStep()).toEqual('EnterLoanAmount');
        });

        it(":currentWorkFlowStep is set to 'SelectPeriods' after executing prev step if current workflow step is 'ReviewAndSubmit'", function () {
            requestModel.currentWorkFlowStep('ReviewAndSubmit');
            requestModel.prevStep({});
            expect(requestModel.currentWorkFlowStep()).toEqual('SelectPeriods');
        });

        it(":cancelRequest clears out the comments", function () {
            requestModel.Comments("This is a comment");
            requestModel.cancelRequest();
            expect(requestModel.Comments()).toBe(null);
        });

        it(":cancelRequest sets selectedRequestOption to 'unmetCost' when unmet cost is gt 0", function () {
            requestModel.selectedRequestOption(null);
            requestModel.UnmetCost(500);
            requestModel.cancelRequest();
            expect(requestModel.selectedRequestOption()).toEqual('unmetCost');
        });

        it(":cancelRequest sets selectedRequestOption to 'customAmount' when unmet cost equals 0", function () {
            requestModel.selectedRequestOption(null);
            requestModel.UnmetCost(0);
            requestModel.cancelRequest();
            expect(requestModel.selectedRequestOption()).toEqual('customAmount');
        });

        it(":cancelRequest resets LoanRequestAwardPeriods to original periods", function () {
            requestModel.LoanRequestAwardPeriods([]);
            requestModel.cancelRequest();
            expect(requestModel.LoanRequestAwardPeriods().length).toEqual(2);
    });

        it(":cancelRequest resets custom amount to 0", function () {
            requestModel.customRequestAmount(100);
            requestModel.cancelRequest();
            expect(requestModel.customRequestAmount()).toEqual('');
        });

        it(":cancelRequest sets currentWorkFlowStep to 'Confirmation' if there is a pending request", function () {
            requestModel.IsRequestPending(true);
            requestModel.cancelRequest();
            expect(requestModel.currentWorkFlowStep()).toEqual("Confirmation");
        });

        it(":cancelRequest sets currentWorkFlowStep to 'EnterLoanAmount' if there are no pending requests", function () {
            requestModel.IsRequestPending(false);
            requestModel.cancelRequest();
            expect(requestModel.currentWorkFlowStep()).toEqual("EnterLoanAmount");
        });

        it(":requestSubmitted is initialized to be false", function () {
            expect(requestModel.requestSubmitted()).toBe(false);
        });

        it(":distributeAwardPeriodAmounts returns true if there is a pending request", function () {
            requestModel.IsRequestPending(true);
            expect(requestModel.distributeAwardPeriodAmounts()).toBeTruthy();
        });

        it(":distributeAwardPeriodAmounts sets TotalRequestAmount to selectedRequestAmount", function () {
            requestModel.selectedRequestOption("unmetCost");
            requestModel.UnmetCost(2500);
            requestModel.IsRequestPending(false);
            expect(requestModel.TotalRequestAmount()).toEqual(requestModel.selectedRequestAmount());
        });
    });
});