﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
describe("year.selector.view.model.js", function () {

    personProxyLoadingThrobberMessage = "Loading...";
    personProxyLoadingThrobberAltText = "Alt Loading";
    personProxyChangingThrobberMessage = "Changing...";
    personProxyChangingThrobberAltText = "Alt Changing";

    var viewModel,
        awardYearsData,
        configurationData,
        memory;

    var global = jasmine.getGlobal();

    beforeAll(function () {
        memory = sessionStorage ||
             (window.UserDataStorage && new UserDataStorage()) ||
             new CookieStorage();
        utility = new financialAidUtility();
        getAwardLetterDataActionUrl = "awardLetterUrl";
        faAwardsSubmittingChangesMessage = "submitting changes";
        outsideAwardsAdminActionUrl = "adminUrl";
        awardsAdminActionUrl = "adminUrl";
        awardLetterAdminActionUrl = "adminUrl";
        homeAdminActionUrl = "adminUrl";
        documentsAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        viewModel = new yearSelector();
        viewModel.Configuration = ko.observable();
        awardYearsData = testData().awardYearsData;
        configurationData = testData().configurationData;
        viewModel.HasPrivacyRestriction = ko.observable(false);
        
    });

    afterEach(function () {
        viewModel = null;
        documentsViewModelInstance = null;
        awardsViewModelInstance = null;
        awardLetterViewModelInstance = null;
        outsideAwardsViewModelInstance = null;
        homeViewModelInstance = null;
    });

    it(":isAdmin is undefined after initialization", function () {
        expect(viewModel.isAdmin()).toBeUndefined();
    });

    it(":AwardYears list is empty after initialization", function () {
        expect(viewModel.AwardYears()).toEqual([]);
    });

    it(":hasAwardYears returns false if AwardYears list is empty", function () {
        expect(viewModel.hasAwardYears()).toBeFalsy();
    });

    it(":hasAwardYears returns true if AwardYears list is not empty", function () {
        viewModel.AwardYears(awardYearsData);
        expect(viewModel.hasAwardYears()).toBeTruthy();
    });

    it(":showYearSelector returns false if there are no years", function () {
        expect(viewModel.showYearSelector()).toBe(false);
    });

    it(":showYearSelector returns false if there is privacy restriction", function () {
        viewModel.HasPrivacyRestriction(true);
        viewModel.AwardYears(awardYearsData);
        expect(viewModel.showYearSelector()).toBe(false);
    });

    it(":showYearSelector returns true if there is no privacy restriction and years exist", function () {
        viewModel.AwardYears(awardYearsData);
        expect(viewModel.showYearSelector()).toBe(true);
    });

    it(":selectedYear is defined after initialization", function () {
        expect(viewModel.selectedYear()).toBeDefined();
    });

    it(":yearConfiguration is null if Configuration object is undefined", function () {
        expect(viewModel.yearConfiguration()).toEqual(null);
    });

    it(":yearConfiguration is null if Configuration object is null", function () {
        viewModel.Configuration(null);
        expect(viewModel.yearConfiguration()).toEqual(null);
    });

    it(":yearConfiguration is null if no matching configuration for the selected year is found", function () {
        var awardYear = awardYearsData[0];
        awardYear.Code("foo");
        viewModel.selectedYear(awardYear);
        viewModel.Configuration(configurationData);
        expect(viewModel.yearConfiguration()).toEqual(null);
    });

    it(":yearConfiguration returns expected configuration for the selected year", function () {
        viewModel.selectedYear(awardYearsData[0]);
        viewModel.Configuration(configurationData);
        var expectedConfiguration = configurationData.AwardYearConfigurations()[0];
        expect(viewModel.yearConfiguration()).toEqual(expectedConfiguration);
    });

    it(":hasConfiguration returns null if yearConfiguration is null", function () {
        expect(viewModel.hasConfiguration()).toBeFalsy();
    });

    it(":hasConfiguration returns true if yearConfiguration exists", function () {
        viewModel.selectedYear(awardYearsData[0]);
        viewModel.Configuration(configurationData);
        expect(viewModel.hasConfiguration()).toBeTruthy();
    });

    it(":selectionChanged invokes getItem method on the memory object", function () {
        spyOn(memory, "getItem");
        viewModel.selectionChanged(new awardLetterViewModel());
        expect(memory.getItem).toHaveBeenCalled();
    });

    it(":selectionChanged invokes getItem method but does not setItem if current page is not new", function () {
        memory.setItem("newPage", 1);
        spyOn(memory, "getItem").and.callThrough();
        spyOn(memory, "setItem");
        viewModel.selectionChanged(new awardLetterViewModel());
        expect(memory.setItem).not.toHaveBeenCalled();
    });

    it(":selectionChanged invokes getItem method but does not setItem if selectedYear is undefined", function () {
        memory.setItem("newPage", 0);        
        spyOn(memory, "getItem").and.returnValue(0);
        spyOn(memory, "setItem");        
        viewModel.selectionChanged({ selectedYear: ko.observable(), selectedLoanRequestForYear: null});
        expect(memory.setItem).not.toHaveBeenCalled();
    });

    it(":selectionChanged invokes getItem method and calls setItem with expected values", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);
        spyOn(memory, "setItem");
        viewModel.selectionChanged({ selectedYear: ko.observable({Code: ko.observable("2015"), Description: ko.observable("2015 desc")}), selectedLoanRequestForYear: null });
        expect(memory.setItem).toHaveBeenCalledWith('yearCode', '2015');
        expect(memory.setItem).toHaveBeenCalledWith('yearDesc', '2015 desc');
    });

    it(":selectionChanged invokes cancelRequest if the calling page is 'Request a new loan'", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);
        
        var data = { selectedYear: ko.observable({ Code: ko.observable("2015"), Description: ko.observable("2015 desc") }), selectedLoanRequestForYear: ko.observable(testData().loanRequestData) };
        spyOn(data.selectedLoanRequestForYear(), "cancelRequest");

        viewModel.selectionChanged(data);        
        expect(data.selectedLoanRequestForYear().cancelRequest).toHaveBeenCalled();
    });

    it(":selectionChanged invokes changeToMobile function on documentsViewModelInstance if the calling page is 'Required Documents'", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);

        documentsViewModelInstance = new documentsViewModel();
        documentsViewModelInstance.isMobile(true);
        spyOn(documentsViewModelInstance, "changeToMobile");

        viewModel.selectionChanged({ selectedYear: ko.observable({ Code: ko.observable("2015"), Description: ko.observable("2015 desc") }) });
        expect(documentsViewModelInstance.changeToMobile).toHaveBeenCalled();
    });

    it(":selectionChanged invokes rearrangeMultiAccordionElements function if the calling page is 'My Awards'", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);
        spyOn(window, "rearrangeMultiAccordionElements");
        awardsViewModelInstance = new awardsViewModel();        
        
        viewModel.selectionChanged({ selectedYear: ko.observable({ Code: ko.observable("2015"), Description: ko.observable("2015 desc") }) });
        expect(window.rearrangeMultiAccordionElements).toHaveBeenCalled();
    });

    it(":selectionChanged invokes utility getAwardLetterData method if the calling page is 'Award Letter'", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);
        spyOn(utility, "getAwardLetterData");

        awardLetterViewModelInstance = new awardLetterViewModel();
        var yearCode = "2015";
        var arePrereqsSat = true;
        var data = { selectedYear: ko.observable({ Code: ko.observable(yearCode), Description: ko.observable("2015 desc"), ArePrerequisitesSatisfied: ko.observable(arePrereqsSat) }) };
        viewModel.selectionChanged(data);

        expect(utility.getAwardLetterData).toHaveBeenCalledWith(data, yearCode, arePrereqsSat);
    });

    it(":selectionChanged invokes utility getOutsideAwardsData method if the calling page is 'Outside Awards'", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);
        spyOn(utility, "getOutsideAwardsData");

        outsideAwardsViewModelInstance = new outsideAwardsViewModel();
        var yearCode = "2015";        
        var data = { selectedYear: ko.observable({ Code: ko.observable(yearCode), Description: ko.observable("2015 desc"), ArePrerequisitesSatisfied: ko.observable() }) };
        viewModel.selectionChanged(data);

        expect(utility.getOutsideAwardsData).toHaveBeenCalledWith(data, yearCode);
    });

    it(":selectionChanged invokes tabs function if the calling page is 'Home' and the view is mobile", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);
        homeViewModelInstance = new homeViewModel();
        spyOn($.fn, "tabs");
        var data = { selectedYear: ko.observable({ Code: ko.observable("2015"), Description: ko.observable("2015 desc"), ArePrerequisitesSatisfied: ko.observable() }), isMobile: ko.observable(true) };
        viewModel.selectionChanged(data);
        expect($.fn.tabs).toHaveBeenCalled();
    });

    it(":selectionChanged does not invoke tabs function if the calling page is 'Home' and the view is not mobile", function () {
        memory.setItem("newPage", 0);
        spyOn(memory, "getItem").and.returnValue(0);
        homeViewModelInstance = new homeViewModel();
        spyOn($.fn, "tabs");
        var data = { selectedYear: ko.observable({ Code: ko.observable("2015"), Description: ko.observable("2015 desc"), ArePrerequisitesSatisfied: ko.observable() }), isMobile: ko.observable(false) };
        viewModel.selectionChanged(data);
        expect($.fn.tabs).not.toHaveBeenCalled();
    });
});