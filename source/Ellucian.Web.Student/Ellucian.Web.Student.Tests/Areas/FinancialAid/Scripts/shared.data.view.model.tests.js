﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates
describe("shared.data.view.model.js", function () {

    var viewModel,                        //local instance of the viewModel we are testing
        sapStatusDataModel,               //data for sap status
        academicProgressStatusDataModel   //data for academicProgressStatus

    beforeEach(function () {
        viewModel = new sharedDataViewModel();

        academicProgressStatusDataModel = {
            Code: ko.observable("P"),
            Description: ko.observable("Probation"),
            Category: ko.observable("Unsatisfactory"),
            Explanation: ("Explanation")
        },

        sapStatusDataModel = {
            PeriodStartTerm: ko.observable("2015/SP"),
            PeriodEndTerm: ko.observable("2015/FA"),
            EvaluationDate: ko.observable("2015/05/20"),
            AcademicProgressStatus: academicProgressStatusDataModel,
        }
    });

    afterEach(function () {
        viewModel = null;
        sapStatisDataModel = null;
    });

    it(":SAPStatus is undefined after intialize", function () {
        expect(viewModel.SAPStatus()).toBeUndefined();
    });

    it(":IsProxyView is false after initialize", function () {
        expect(viewModel.IsProxyView()).toBeFalsy();
    });

    it(":SAPStatus properties equal expected", function () {
        viewModel.SAPStatus(sapStatusDataModel);
        expect(viewModel.SAPStatus().AcademicProgressStatus).toEqual(sapStatusDataModel.AcademicProgressStatus);
        expect(viewModel.SAPStatus().PeriodStartTerm).toEqual(sapStatusDataModel.PeriodStartTerm);
        expect(viewModel.SAPStatus().PeriodEndTerm).toEqual(sapStatusDataModel.PeriodEndTerm);
        expect(viewModel.SAPStatus().EvaluationDate).toEqual(sapStatusDataModel.EvaluationDate);
    });

    it(":isAcademicProgressSatisfactory is true after initialize", function () {
        expect(viewModel.isAcademicProgressSatisfactory()).toBeTruthy();
    });

    it(":isAcademicProgressSatisfactory is false when passed value is false", function () {
        viewModel.SAPStatus(sapStatusDataModel);
        expect(viewModel.isAcademicProgressSatisfactory()).toBeFalsy();
    });

    it(":isAcademicProgressSatisfactory is true when passed value is true", function () {
        academicProgressStatusDataModel.Category("Satisfactory");
        sapStatusDataModel.AcademicProgressStatus = academicProgressStatusDataModel;
        viewModel.SAPStatus(sapStatusDataModel);
        expect(viewModel.isAcademicProgressSatisfactory()).toBeTruthy();
    });
});