﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates

describe("academic.progress.view.model.js", function () {
    
    var viewModel,                   //local instance of the view model we are testing
    sapStatusDataModel,              //data for sap status    
    personDataModel,                 //data for person
    faCounselor,                     //data for counselor
    academicProgressStatusDataModel, //data for academicProgressStatus
    sapDetailItemDataModel,          //data for sap detail item
    latestProgressEvaluationModel    //data for the lastest academic progress evaluation

    beforeAll(function () {
        academicProgressAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        viewModel = new academicProgressViewModel();

        academicProgressStatusDataModel = {
            Code: ko.observable("P"),
            Description: ko.observable("Probation"),
            Category: ko.observable("Unsatisfactory"),
            Explanation: ko.observable("Explanation")
        },

        sapStatusDataModel = {
            PeriodStartTerm: ko.observable("2015/SP"),
            PeriodEndTerm: ko.observable("2015/FA"),
            EvaluationDate: ko.observable("2015/05/20"),
            AcademicProgressStatus: academicProgressStatusDataModel,
        },
        
        sapDetailItemDataModel = {
            Code: ko.observable("attempted"),
            Description: ko.observable("attempted credits"),
            Explanation: ko.observable("attempted credit explanation"),
            Value: ko.observable("45")
            
        },

        latestProgressEvaluationModel = {
            AcademicProgressEvaluationId: ko.observable("657483930000"),
            SAPStatus: sapStatusDataModel,
            SAPDetailItems: ko.observableArray(),
            isItemVisible: ko.observable(true)
        },

        personDataModel = {
            Id: ko.observable("0004791")
        },

        faCounselor = {
            Id: ko.observable("0003914")
        }

    });

    afterEach(function () {
        viewModel = null;
        academicProgressStatusDataModel = null;
        sapLinksDataModel = null;
        sapStatusDataModel = null;
        faCounselor = null;
        personDataModel = null;
        latestProgressEvaluationModel = null;
    });

    it(":showUI is false after intialization", function () {
        expect(viewModel.showUI()).toBeFalsy();
    });

    it(":SAPLinks is array after intialization", function () {
        var array = [];
        expect(viewModel.SAPLinks()).toEqual(array);
    });

    it(":SAPHistoryItems is array after intialization", function () {
        var array = [];
        expect(viewModel.SAPHistoryItems()).toEqual(array);
    });

    it(":LatestAcademicProgressEvaluation is undefined after intialization", function () {
        expect(viewModel.LatestAcademicProgressEvaluation()).toBeUndefined();
    });

    it(":Person is undefined after intialization", function () {
        expect(viewModel.Person()).toBeUndefined();
    });

    it(":FinancialAidCounselor is undefined after intialization", function () {
        expect(viewModel.FinancialAidCounselor()).toBeUndefined();
    });

    it(":isAcademicProgressDataAvailable is false after initialization", function () {
        expect(viewModel.isAcademicProgressDataAvailable()).toBeFalsy();
    });

    it(":IsAcademicProgressActive is false after initialization", function () {
        expect(viewModel.IsAcademicProgressActive()).toBeFalsy();
    });

    it(":isSAPHistoryItemView is false after initialization", function () {
        expect(viewModel.isSAPHistoryItemView()).toBeFalsy();
    });

    it(":historyItemData is null after intialization", function () {
        expect(viewModel.historyItemData()).toBeNull();
    });

    it(":showAllSAPHistoryItems is false after initialization", function () {
        expect(viewModel.showAllSAPHistoryItems()).toBeFalsy();
    });

    it(":PersonId is undefined on initialization", function () {
        expect(viewModel.PersonId()).toBeUndefined();
    });

    it(":PersonName is undefined on initialization", function () {
        expect(viewModel.PersonName()).toBeUndefined();
    });

    it(":PrivacyMessage is undefined on initialization", function () {
        expect(viewModel.PrivacyMessage()).toBeUndefined();
    });

    it(":HasPrivacyRestriction is false on initialization", function () {
        expect(viewModel.HasPrivacyRestriction()).toBe(false);
    });

    it(":changeUserUrl equals expected value", function () {
        expect(viewModel.changeUserUrl()).toEqual(academicProgressAdminActionUrl);
    });

    it(":isAcademicProgressDataAvailable is false if LatestAcademicProgressEvaluation is undefined", function () {        
        viewModel.IsAcademicProgressActive(true);
        expect(viewModel.isAcademicProgressDataAvailable()).toBeFalsy();
    });

    it(":isAcademicProgressDataAvailable is false if SAPStatus is null", function () {
        latestProgressEvaluationModel.SAPStatus = null;
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        viewModel.IsAcademicProgressActive(true);
        expect(viewModel.isAcademicProgressDataAvailable()).toBeFalsy();
    });

    it(":isAcademicProgressDataAvailable is false if IsAcademicProgressActive is false", function () {        
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        viewModel.IsAcademicProgressActive(false);
        expect(viewModel.isAcademicProgressDataAvailable()).toBeFalsy();
    });

    it(":isAcademicProgressDataAvailable is false if IsAcademicProgressActive is falsy", function () {
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        viewModel.IsAcademicProgressActive(null);
        expect(viewModel.isAcademicProgressDataAvailable()).toBeFalsy();
    });

    it(":isAcademicProgressDataAvailable is true if SAPStatus exists and IsAcademicProgressActive is truthy", function () {
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        viewModel.IsAcademicProgressActive(true);
        expect(viewModel.isAcademicProgressDataAvailable()).toBeTruthy();
    });    

    it(":academicProgressStatusCategory is equal to expected category", function () {
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        viewModel.IsAcademicProgressActive(true);
        expect(viewModel.academicProgressStatusCategory()).toEqual(academicProgressStatusDataModel.Category());
    });

    it(":academicProgressStatusCategory is null if LatestAcademicProgressEvaluation is null", function () {
        viewModel.LatestAcademicProgressEvaluation(null);
        expect(viewModel.academicProgressStatusCategory()).toBeNull();
    });

    it(":sapSectionColumnStyle return sap-section-full when there are no SAP detail items", function () {
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        expect(viewModel.sapSectionColumnStyle()).toEqual("sap-section-full");
    });

    it(":sapSectionColumnStyle return sap-section when LatestAcademicProgressEvaluation is null", function () {
        viewModel.LatestAcademicProgressEvaluation(null);
        expect(viewModel.sapSectionColumnStyle()).toEqual("sap-section");
    });

    it(":sapSectionColumnStyle return sap-section-full when SAP detail items list is null", function () {
        latestProgressEvaluationModel.SAPDetailItems(null);
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        expect(viewModel.sapSectionColumnStyle()).toEqual("sap-section-full");
    });

    it(":sapSectionColumnStyle return sap-section when there is at least one SAP detail items", function () {
        latestProgressEvaluationModel.SAPDetailItems.push(sapDetailItemDataModel);
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        expect(viewModel.sapSectionColumnStyle()).toEqual("sap-section");
    });

    it(":isAcademicProgressSatisfactory is true after initialization", function () {
        expect(viewModel.isAcademicProgressSatisfactory()).toBeTruthy();
    });

    it(":isAcademicProgressSatisfactory is true if academicProgressStatus is Satisfactory", function () {
        academicProgressStatusDataModel.Category("Satisfactory");
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        expect(viewModel.isAcademicProgressSatisfactory()).toBeTruthy();
    });

    it(":isAcademicProgressSatisfactory is false if academicProgressStatus is Unsatisfactory", function () {        
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        viewModel.IsAcademicProgressActive(true);
        expect(viewModel.isAcademicProgressSatisfactory()).toBeFalsy();
    });

    it(":isAcademicProgressSatisfactory is false if academicProgressStatus is of Warning category", function () {
        academicProgressStatusDataModel.Category("Warning");
        viewModel.LatestAcademicProgressEvaluation(latestProgressEvaluationModel);
        viewModel.IsAcademicProgressActive(true);
        expect(viewModel.isAcademicProgressSatisfactory()).toBeFalsy();
    });

    it(":isSapHistoryItemView flag is set to true when toggleView function is called and initital flag value is false", function () {
        viewModel.toggleView(latestProgressEvaluationModel, null);
        expect(viewModel.isSAPHistoryItemView()).toBeTruthy();
    });

    it(":isSapHistoryItemView flag is set to false when toggleView function is called and initital flag value is true", function () {
        viewModel.isSAPHistoryItemView(true);
        viewModel.toggleView(latestProgressEvaluationModel, null);
        expect(viewModel.isSAPHistoryItemView()).toBeFalsy();
    });

    it(":historyItemData is equal to the data passed to the toggleView function when the isSAPHistoryItemView is inittially false", function () {
        viewModel.toggleView(latestProgressEvaluationModel, null);
        expect(viewModel.historyItemData()).toEqual(latestProgressEvaluationModel);
        expect(viewModel.historyItemData().AcademicProgressEvaluationId).toEqual(latestProgressEvaluationModel.AcademicProgressEvaluationId);
    });

    it(":showAllSAPHistoryItems is set to true if toggleSAPHistoryView function is triggered and initial flag value is false", function () {
        viewModel.toggleSAPHistoryView();
        expect(viewModel.showAllSAPHistoryItems()).toBeTruthy();
    });    

    it(":sapHistoryItemsToDisplay returns the same number of elements as SAPHistoryItems()", function () {
        viewModel.SAPHistoryItems().push(latestProgressEvaluationModel);
        viewModel.SAPHistoryItems().push(latestProgressEvaluationModel);
        expect(viewModel.sapHistoryItemsToDisplay().length).toEqual(viewModel.SAPHistoryItems().length);
    });

    it(":sapHistoryItemsToDisplay returns all items as visible if SAPHistoryItems count is less than 4 and showAllHistoryItems flag is false", function () {
        viewModel.showAllSAPHistoryItems(false);
        viewModel.SAPHistoryItems().push(latestProgressEvaluationModel);
        viewModel.SAPHistoryItems().push(latestProgressEvaluationModel);
        for (var i = 0; i < viewModel.sapHistoryItemsToDisplay().length; i++) {
            expect(viewModel.sapHistoryItemsToDisplay()[i].isItemVisible()).toBeTruthy();
        }
    });

    it(":toggleSAPHistoryView call sets showAllSAPHistoryItems flag to true", function () {
        viewModel.showAllSAPHistoryItems(false);
        viewModel.toggleSAPHistoryView();
        expect(viewModel.showAllSAPHistoryItems()).toBeTruthy();
    });
    
});