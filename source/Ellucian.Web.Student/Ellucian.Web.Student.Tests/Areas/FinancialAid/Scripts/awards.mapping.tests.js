﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.
describe("awards.mapping.js", function () {

    var awardsViewModelData,
        awardsViewModelInstance;

    beforeAll(function () {
        awardsViewModelData = testData().awardsViewModelData;
        faAwardsSubmittingChangesMessage = "Submitting change";
        faAwardsAwardActionMessage = "Award action message";
        faAwardsLoanActionMessage = "Loan action message";
        faAwardsSubsidizedLoansInputMessage = "Subsidized loan input message";
        faAwardsUnubsidizedLoansInputMessage = "Unsubsidized loan input message";
        faAwardsGradPlusLoansInputMessage = "Graduate Plus loan input message";
        faAwardsSingleAwardPlacedOnHoldMessage = "Single award on hold message";
        faAwardsTotalAwardAmountEqualsZeroAcceptMessage = "Total amount is zero message";
        faAwardsSingleAwardInReviewMessage = "Single award in review message";
        faAwardsLoanActionNoMaxLimitsMessage = "No max limits message";
        faAwardsSubsidizedLoansTitle = "Subsidized loan title";
        faAwardsUnsubsidizedLoansTitle = "Unsubsidized loan title";
        faAwardsGradPlusLoansTitle = "Grad Plus loans title";
        MaximumAmountWarningForScreenReaders = "Max amount warning";
        faAwardsNonPendingLoanActionMessage = "non pending loan message";
        faAwardsNonPendingLoanActionNoMaxLimitsMessage = "non pending loan no max message";
        faAwardsNonPendingLoanActionZeroAllowedMessage = "non pending loan zero allowed message";
        faAwardsNonPendingLoanActionNoMaxLimitsZeroAllowedMessage = "non pending no max zero allowed loan message";

    });

    describe("studentAwardModel", function () {
        var awardOptionsData,
            awardModel;

        beforeEach(function () {
            awardOptionsData = {
                data: awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0],
                parent: awardsViewModelData.StudentAwards[0]
            };
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
        });

        it(":isLoan is set to false on initialization", function () {
            expect(awardModel.isLoan).toEqual(false);
        });

        it(":hasInputErrors is set to false on initialization", function () {
            expect(awardModel.hasInputErrors()).toBe(false);
        });

        it(":inputErrorMessage is undefined on initialization", function () {
            expect(awardModel.inputErrorMessage()).toBeUndefined();
        });

        it(":awardUpdated is set to false on initialization", function () {
            expect(awardModel.awardUpdated()).toBe(false);
        });

        it(":accordionIsOpen is set to false on initialization", function () {
            expect(awardModel.accordionIsOpen()).toBe(false);
        });

        it(":flipAccordion sets accordionIsOpen to false if it is initially true", function () {
            awardModel.accordionIsOpen(true);
            awardModel.flipAccordion();
            expect(awardModel.accordionIsOpen()).toBe(false);
        });

        it(":flipAccordion sets accordionIsOpen to true if it is initially false", function () {
            awardModel.accordionIsOpen(false);
            awardModel.flipAccordion();
            expect(awardModel.accordionIsOpen()).toBe(true);
        });

        it(":totalAmount equals expected value", function () {
            var expected = 1000;
            expect(awardModel.totalAmount()).toEqual(expected);
        });

        it(":totalLoanChangeAmount equals 0 after initialization", function () {
            expect(awardModel.totalLoanChangeAmount()).toEqual('0');
        });

        it(":loanChangeAmountWithPrecision equals formatted totalAmount if there are no input errors", function () {
            awardModel.hasInputErrors(false);
            var expected = '1,000.00';
            expect(awardModel.loanChangeAmountWithPrecision()).toEqual(expected);
        });

        it(":loanChangeAmountWithPrecision equals 0 if there are input errors", function () {
            awardModel.hasInputErrors(true);
            var expected = '0.00';
            expect(awardModel.loanChangeAmountWithPrecision()).toEqual(expected);
        });

        it(":isLoanAmountInFocus is initialized to false", function () {
            expect(awardModel.isLoanAmountInFocus()).toBe(false);
        });

        it(":isDirectOrGradPlusLoan returns true if loan type is SubsidizedLoan", function () {
            awardModel.LoanType('SubsidizedLoan');
            expect(awardModel.isDirectOrGradPlusLoan()).toBe(true);
        });

        it(":isDirectOrGradPlusLoan returns true if loan type is UnsubsidizedLoan", function () {
            awardModel.LoanType('UnsubsidizedLoan');
            expect(awardModel.isDirectOrGradPlusLoan()).toBe(true);
        });

        it(":isDirectOrGradPlusLoan returns true if loan type is GraduatePlusLoan", function () {
            awardModel.LoanType('GraduatePlusLoan');
            expect(awardModel.isDirectOrGradPlusLoan()).toBe(true);
        });

        it(":isDirectOrGradPlusLoan returns false if loan type is blank", function () {
            awardModel.LoanType('');
            expect(awardModel.isDirectOrGradPlusLoan()).toBe(false);
        });

        it(":isDirectOrGradPlusLoan returns false if loan type is not sub, unsub, or grad plus", function () {
            awardModel.LoanType('foo');
            expect(awardModel.isDirectOrGradPlusLoan()).toBe(false);
        });

        it(":totalAwardPeriodAmount returns expected value", function () {
            awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].StudentAwardPeriods()
                .push(
                {
                    AwardAmount: ko.observable(250),
                    Code: ko.observable("16/FA"),
                    IsActive: ko.observable(true)
                });
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            expect(awardModel.totalAwardPeriodAmount({Code: ko.observable("16/FA")})).toEqual(750);
        });

        it(":awardRows returns 5 if accordionIsOpen is true", function () {
            awardModel.accordionIsOpen(true);
            expect(awardModel.awardRows()).toEqual(5);
        });

        it(":awardRows returns 2 if accordionIsOpen is false", function () {
            awardModel.accordionIsOpen(false);
            expect(awardModel.awardRows()).toEqual(2);
        });

        it(":isAwardOnHoldOrInReview returns true if award status category is pending and status is not modifiable", function () {
            awardModel.AwardStatus.Category("Pending");
            awardModel.IsStatusModifiable(false);
            expect(awardModel.isAwardOnHoldOrInReview()).toBe(true);
        });

        it(":isAwardOnHoldOrInReview returns true if award status category is estimated and status is not modifiable", function () {
            awardModel.AwardStatus.Category("Estimated");
            awardModel.IsStatusModifiable(false);
            expect(awardModel.isAwardOnHoldOrInReview()).toBe(true);
        });

        it(":isAwardOnHoldOrInReview returns true if total amount is 0 and the accordion is open", function () {
            for (var i = 0; i < awardModel.StudentAwardPeriods().length; i++) {
                awardModel.StudentAwardPeriods()[i].AwardAmount(0);
            }
            awardModel.IsStatusModifiable(true);
            awardModel.accordionIsOpen(true);
            expect(awardModel.isAwardOnHoldOrInReview()).toBe(true);
        });

        it(":isAwardOnHoldOrInReview returns false if award status category is estimated and status is modifiable", function () {
            awardModel.AwardStatus.Category("Estimated");
            awardModel.IsStatusModifiable(true);
            expect(awardModel.isAwardOnHoldOrInReview()).toBe(false);
        });

        it(":isAwardOnHoldOrInReview returns false if award status category is accepted", function () {
            awardModel.AwardStatus.Category("Accepted");
            expect(awardModel.isAwardOnHoldOrInReview()).toBe(false);
        });

        it(":infoNotification equals expected value if the award is in review", function () {
            awardModel.AwardStatus.Category("Pending");
            awardModel.IsAwardInReview(true);
            awardModel.IsStatusModifiable(false);
            awardModel.IsAmountModifiable(false);
            var expected = "Single award in review message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification equals expected value if the award is in review and is an accepted direct loan", function () {
            awardModel.LoanType('UnsubsidizedLoan');
            awardModel.AwardStatus.Category("Accepted");
            awardModel.IsAwardInReview(true);
            awardModel.IsStatusModifiable(false);
            var expected = "Single award in review message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification equals expected value if award is on hold", function () {
            awardModel.AwardStatus.Category("Estimated");
            awardModel.IsAwardInReview(false);
            awardModel.IsStatusModifiable(false);
            awardModel.IsAmountModifiable(false);
            var expected = "Single award on hold message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is not a loan and total amount is 0", function () {
            for (var i = 0; i < awardModel.StudentAwardPeriods().length; i++) {
                awardModel.StudentAwardPeriods()[i].AwardAmount(0);
            }
            awardModel.IsStatusModifiable(true);
            awardModel.LoanType('');
            var expected = "Total amount is zero message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan and total loan collection amount is 0", function () {
            awardsViewModelData.StudentAwards[0].totalAmount(0);            
            awardModel.LoanType('SubsidizedLoan');
            var expected = "Total amount is zero message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan, total loan collection amount is 0 and 0 is allowed", function () {
            awardsViewModelData.StudentAwards[0].totalAmount(0);
            awardModel.LoanType('SubsidizedLoan');
            awardsViewModelData.StudentAwards[0].allowDeclineZeroAcceptedLoans(true);
            var expected = "Total amount is zero message";
            expect(awardModel.infoNotification()).not.toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan and the max limit is suppressed", function () {
            awardsViewModelData.StudentAwards[0].suppressMaxLoanLimit(true);
            awardsViewModelData.StudentAwards[0].totalAmount(5000);
            awardModel.LoanType('SubsidizedLoan');
            var expected = "No max limits message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan, the max limit is suppressed and allow zero flag is on", function () {
            awardsViewModelData.StudentAwards[0].suppressMaxLoanLimit(true);
            awardsViewModelData.StudentAwards[0].totalAmount(5000);
            awardsViewModelData.StudentAwards[0].allowDeclineZeroAcceptedLoans(true);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            awardModel.LoanType('SubsidizedLoan');
            awardModel.AwardStatus.Category("Accepted");
            var expected = "non pending no max zero allowed loan message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan, the max limit is suppressed and the loan is accepted", function () {
            awardsViewModelData.StudentAwards[0].suppressMaxLoanLimit(true);
            awardsViewModelData.StudentAwards[0].totalAmount(5000);
            awardsViewModelData.StudentAwards[0].allowDeclineZeroAcceptedLoans(false);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            awardModel.LoanType('SubsidizedLoan');
            awardModel.AwardStatus.Category("Accepted");
            var expected = "non pending loan no max message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan and the max limit is not suppressed", function () {
            awardsViewModelData.StudentAwards[0].suppressMaxLoanLimit(false);
            awardsViewModelData.StudentAwards[0].totalAmount(5000);
            awardModel.LoanType('SubsidizedLoan');
            awardModel.AwardStatus.Category("Pending");
            var expected = "Loan action message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan, the max limit is not suppressed and allow zero flag is on", function () {
            awardsViewModelData.StudentAwards[0].suppressMaxLoanLimit(false);
            awardsViewModelData.StudentAwards[0].totalAmount(5000);
            awardsViewModelData.StudentAwards[0].allowDeclineZeroAcceptedLoans(true);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            awardModel.LoanType('SubsidizedLoan');
            awardModel.AwardStatus.Category("Accepted");
            var expected = "non pending loan zero allowed message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is a loan, the max limit is not suppressed and the loan is accepted", function () {
            awardsViewModelData.StudentAwards[0].suppressMaxLoanLimit(false);
            awardsViewModelData.StudentAwards[0].totalAmount(5000);
            awardsViewModelData.StudentAwards[0].allowDeclineZeroAcceptedLoans(false);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            awardModel.LoanType('SubsidizedLoan');
            awardModel.AwardStatus.Category("Accepted");
            var expected = "non pending loan message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification returns expected value if the award is not a loan and other conditions are not met", function () {
            awardModel.LoanType('');
            awardModel.AwardStatus.Category("Accepted");
            awardModel.IsStatusModifiable(false);
            var expected = "Award action message";
            expect(awardModel.infoNotification()).toEqual(expected);
        });

        it(":infoNotification is undefined if award is unsub and there are pending sub", function () {
            awardsViewModelData.StudentAwards[0].isAnySubLoanPending(true);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            awardModel.LoanType('UnsubsidizedLoan');
            expect(awardModel.infoNotification()).toBeUndefined();
        });

        it(":offScreenDescription equals expected value", function () {
            var expected = awardModel.Description();
            expect(awardModel.offScreenDescription()).toEqual(expected);
        });

        it(":awardLoanInputMessage equals expected if loan type is 'SubsidizedLoan'", function () {
            awardModel.LoanType('SubsidizedLoan');
            var expected = "Subsidized loan input message";
            expect(awardModel.awardLoanInputMessage()).toEqual(expected);
        });

        it(":awardLoanInputMessage equals expected if loan type is 'UnsubsidizedLoan'", function () {
            awardModel.LoanType('UnsubsidizedLoan');
            var expected = "Unsubsidized loan input message";
            expect(awardModel.awardLoanInputMessage()).toEqual(expected);
        });

        it(":awardLoanInputMessage equals expected if loan type is 'GraduatePlusLoan'", function () {
            awardModel.LoanType('GraduatePlusLoan');
            var expected = "Graduate Plus loan input message";
            expect(awardModel.awardLoanInputMessage()).toEqual(expected);
        });

        it(":isInfoNotificationVisible returns false if the current award is unsub and there are pending sub loans", function () {
            awardModel.LoanType('UnsubsidizedLoan');
            awardsViewModelData.StudentAwards[0].isAnySubLoanPending(true);
            awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].AwardStatus.Category("Pending");
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            expect(awardModel.isInfoNotificationVisible()).toBe(false);
        });

        it(":isInfoNotificationVisible returns true if the current award is unsub and there are no pending sub loans", function () {
            awardModel.LoanType('UnsubsidizedLoan');
            awardsViewModelData.StudentAwards[0].isAnySubLoanPending(false);
            awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].AwardStatus.Category("Pending");
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            expect(awardModel.isInfoNotificationVisible()).toBe(true);
        });

        it(":isInfoNotificationVisible returns true if the current award is other loan and it is pending", function () {
            awardModel.LoanType('OtherLoan');
            awardsViewModelData.StudentAwards[0].isAnySubLoanPending(true);
            awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].AwardStatus.Category("Estimated");
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            expect(awardModel.isInfoNotificationVisible()).toBe(true);
        });

        it(":isInfoNotificationVisible returns true if the current award is gradPlus loan and it is amountModifiable", function () {
            awardModel.LoanType('GraduatePlusLoan');
            awardsViewModelData.StudentGradPlusLoans[0].isAnySubLoanPending(true);
            awardsViewModelData.StudentGradPlusLoans[0].Loans[0].AwardStatus.Category("Estimated");
            awardsViewModelData.StudentGradPlusLoans[0].Loans[0].IsAmountModifiable(true);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel({ data: awardsViewModelData.StudentGradPlusLoans[0].Loans[0], parent: awardsViewModelData.StudentGradPlusLoans[0] });
            expect(awardModel.isInfoNotificationVisible()).toBe(true);
        });

        it(":disableRejectButton returns true if the status is not modifiable", function () {
            awardModel.IsStatusModifiable(false);
            expect(awardModel.disableRejectButton()).toBe(true);
        });

        it(":disableRejectButton returns false if the status is modifiable", function () {
            awardModel.IsStatusModifiable(true);
            expect(awardModel.disableRejectButton()).toBe(false);
        });

        it(":disableAcceptButton returns true if the status is not modifiable", function () {
            awardModel.IsStatusModifiable(false);
            expect(awardModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns true if the total amount is zero", function () {            
            for (var i = 0; i < awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].StudentAwardPeriods()[i].AwardAmount(0);
            }
            awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].IsStatusModifiable(true);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            expect(awardModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns false if the status is modifiable and amount is greater than 0", function () {
            for (var i = 0; i < awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].StudentAwardPeriods()[i].AwardAmount(10);
            }
            awardsViewModelData.StudentAwards[0].StudentAwardsForYear[0].IsStatusModifiable(true);
            awardModel = new Ellucian.AwardsViewModelMapping.studentAwardModel(awardOptionsData);
            expect(awardModel.disableAcceptButton()).toBe(false);
        });
    });

    describe("studentLoanCollectionModel", function () {
        var loanCollectionOptionsData,
            loanCollectionModel;

        beforeEach(function () {
            loanCollectionOptionsData = {
                data: awardsViewModelData.StudentGradPlusLoans[0],
                parent: awardsViewModelData
            };
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel(loanCollectionOptionsData);
        });

        it(":Loans is initialized", function () {
            expect(loanCollectionModel.Loans()).not.toBeUndefined();
        });

        it(":hasInputErrors is initialized to false", function () {
            expect(loanCollectionModel.hasInputErrors()).toBe(false);
        });

        it("isLoan is set to true", function () {
            expect(loanCollectionModel.isLoan).toEqual(true);
        });

        it(":loanCollectionUpdated is initialized to false", function () {
            expect(loanCollectionModel.loanCollectionUpdated()).toBe(false);
        });

        it(":accordionIsOpen is initialized to false", function () {
            expect(loanCollectionModel.accordionIsOpen()).toBe(false);
        });

        it(":flipAccordion sets accordionIspen to false if it is initally true", function () {
            loanCollectionModel.accordionIsOpen(true);
            loanCollectionModel.flipAccordion();
            expect(loanCollectionModel.accordionIsOpen()).toBe(false);
        });

        it(":flipAccordion sets accordionIspen to true if it is initally false", function () {
            loanCollectionModel.accordionIsOpen(false);
            loanCollectionModel.flipAccordion();
            expect(loanCollectionModel.accordionIsOpen()).toBe(true);
        });

        it(":collectionRows equals expected number when accordion is open", function () {
            var expected = loanCollectionModel.Loans().length + 4;
            loanCollectionModel.accordionIsOpen(true);
            expect(loanCollectionModel.collectionRows()).toEqual(expected);
        });

        it(":collectionRows equals expected number when accordion is closed", function () {
            var expected = 1;
            loanCollectionModel.accordionIsOpen(false);
            expect(loanCollectionModel.collectionRows()).toEqual(expected);
        });

        it(":totalAmount equals expected", function () {
            var expected = 0;
            for(var i = 0; i < awardsViewModelData.StudentGradPlusLoans[0].Loans.length; i++){
                expected += awardsViewModelData.StudentGradPlusLoans[0].Loans[i].totalAmount();
            }
            expect(loanCollectionModel.totalAmount()).toEqual(expected);
        });

        it(":totalAwardPeriodAmount returns expected amount", function () {
            var expected = 2100;
            expect(loanCollectionModel.totalAwardPeriodAmount({ Code: ko.observable("16/FA") })).toEqual(expected);
        });

        it(":totalAwardPeriodAmount returns expected amount with two matching award periods", function () {
            var expected = 3100;
            loanCollectionModel.Loans()[0].StudentAwardPeriods()
                .push({
                    AwardAmount: ko.observable(1000),
                    Code: ko.observable("16/FA"),
                    IsActive: ko.observable(true)
                });
            expect(loanCollectionModel.totalAwardPeriodAmount({ Code: ko.observable("16/FA") })).toEqual(expected);
        });

        it(":hasPendingLoans returns true if there are pending loans", function () {
            loanCollectionModel.Loans()[0].AwardStatus.Category("Pending");
            expect(loanCollectionModel.hasPendingLoans()).toBe(true);
        });

        it(":hasPendingLoans returns false if there are no pending loans", function () {
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Accepted");
            }
            expect(loanCollectionModel.hasPendingLoans()).toBe(false);
        });

        it(":suppressMaxLoanLimit returns true if the SuppressMaximumLoanLimits flag is true", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            expect(loanCollectionModel.suppressMaxLoanLimit()).toBe(true);
        });

        it(":suppressMaxLoanLimit returns false if the SuppressMaximumLoanLimits flag is false", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(false);
            expect(loanCollectionModel.suppressMaxLoanLimit()).toBe(false);
        });

        it(":passesLimitationCriteria returns true if there are no pending loans and neither the amount nor the status is modifiable", function () {
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Accepted");
            }
            loanCollectionModel.IsStatusModifiable(false);
            loanCollectionModel.IsAnyAmountModifiable(false);
            expect(loanCollectionModel.passesLimitationCriteria()).toBe(true);
        });

        it(":passesLimitationCriteria returns false if max loan limit is not suppressed and the totalAmount is greater than Max", function () {
            loanCollectionModel.MaximumAmount(25);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(false);
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Pending");
            }
            loanCollectionModel.IsStatusModifiable(true);
            loanCollectionModel.IsAnyAmountModifiable(true);
            expect(loanCollectionModel.passesLimitationCriteria()).toBe(false);
        });

        it(":passesLimitationCriteria returns true if SuppressMaximumLoanLimits is true", function () {
            loanCollectionModel.MaximumAmount(25);
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Pending");
            }
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            loanCollectionModel.IsStatusModifiable(true);
            loanCollectionModel.IsAnyAmountModifiable(true);
            expect(loanCollectionModel.passesLimitationCriteria()).toBe(true);
        });

        it(":checkboxActivator triggers ko.utils.arrayForEach function", function () {
            spyOn(ko.utils, "arrayForEach");
            loanCollectionModel.checkboxActivator({}, {});
            expect(ko.utils.arrayForEach).toHaveBeenCalled();
        });

        it(":multipleUntouchableAwards returns false if there are no pending or in review loans", function () {
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Accepted");
            }
            loanCollectionModel.IsLoanCollectionInReview(false);
            expect(loanCollectionModel.multipleUntouchableAwards()).toBe(false);
        });

        it(":multipleUntouchableAwards returns false if there is one pending loan", function () {
            loanCollectionModel.Loans()[0].AwardStatus.Category("Estimated");
            for (var i = 1; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Accepted");
            }

            loanCollectionModel.IsLoanCollectionInReview(false);
            expect(loanCollectionModel.multipleUntouchableAwards()).toBe(false);
        });

        it(":multipleUntouchableAwards returns true if there is more than one pending loan", function () {            
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Estimated");
                loanCollectionModel.Loans()[i].IsStatusModifiable(false);
            }

            loanCollectionModel.IsLoanCollectionInReview(false);
            expect(loanCollectionModel.multipleUntouchableAwards()).toBe(true);
        });

        it(":multipleUntouchableAwards returns false if there is one loan in review", function () {
            loanCollectionModel.IsLoanCollectionInReview(true);
            loanCollectionModel.Loans()[0].IsAwardInReview(true);
            for (var i = 1; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Accepted");
            }
            expect(loanCollectionModel.multipleUntouchableAwards()).toBe(false);
        });

        it(":multipleUntouchableAwards returns true if there is more than one loan in review", function () {
            loanCollectionModel.IsLoanCollectionInReview(true);
            
            for (var i = 1; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Accepted");
                loanCollectionModel.Loans()[i].IsAwardInReview(true);
            }
            expect(loanCollectionModel.multipleUntouchableAwards()).toBe(true);
        });

        it(":isAnyLoanOnHold returns true if there is at least one loan that is pending and not status modifiable", function () {
            loanCollectionModel.Loans()[0].StatusDescription("Pending");
            loanCollectionModel.Loans()[0].IsStatusModifiable(false);
            expect(loanCollectionModel.isAnyLoanOnHold()).toBe(true);
        });

        it(":isAnyLoanOnHold returns false if there are no pending loans", function () {
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].StatusDescription("Accepted");
            }
            expect(loanCollectionModel.isAnyLoanOnHold()).toBe(false);
        });

        it(":isLoanCollectionOnHoldOrInReview returns true if there are some pending loans and the status is not modifiable", function () {
            loanCollectionModel.StatusDescription("Estimated");
            loanCollectionModel.IsStatusModifiable(false);
            expect(loanCollectionModel.isLoanCollectionOnHoldOrInReview()).toBe(true);
        });

        it(":isLoanCollectionOnHoldOrInReview returns true if isAnyLoanOnHold is true", function () {            
            loanCollectionModel.Loans()[0].StatusDescription("Pending");
            loanCollectionModel.Loans()[0].IsStatusModifiable(false);
            loanCollectionModel.IsStatusModifiable(true);
            loanCollectionModel.IsLoanCollectionInReview(false);
            expect(loanCollectionModel.isLoanCollectionOnHoldOrInReview()).toBe(true);
        });

        it(":isLoanCollectionOnHoldOrInReview returns true if IsLoanCollectionInReview is true", function () {
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].StatusDescription("Accepted");
            }
            loanCollectionModel.IsStatusModifiable(true);
            loanCollectionModel.IsLoanCollectionInReview(true);
            expect(loanCollectionModel.isLoanCollectionOnHoldOrInReview()).toBe(true);
        });

        it(":isLoanCollectionOnHoldOrInReview returns true if totalAmount is zero, accordion is open and overall status is modifiable", function () {            
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].StatusDescription("Accepted");
                for (var k = 0; k < loanCollectionModel.Loans()[i].StudentAwardPeriods().length; k++) {
                    loanCollectionModel.Loans()[i].StudentAwardPeriods()[k].AwardAmount(0);
                }
            }            
            loanCollectionModel.accordionIsOpen(true);
            loanCollectionModel.IsLoanCollectionInReview(false);
            loanCollectionModel.IsStatusModifiable(true);
            
            expect(loanCollectionModel.isLoanCollectionOnHoldOrInReview()).toBe(true);
        });

        it(":isLoanCollectionOnHoldOrInReview returns true if totalAmount is zero, accordion is open and any amount is modifiable", function () {
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].StatusDescription("Accepted");
                for (var k = 0; k < loanCollectionModel.Loans()[i].StudentAwardPeriods().length; k++) {
                    loanCollectionModel.Loans()[i].StudentAwardPeriods()[k].AwardAmount(0);
                }
            }
            loanCollectionModel.accordionIsOpen(true);
            loanCollectionModel.IsLoanCollectionInReview(false);
            loanCollectionModel.IsStatusModifiable(false);
            loanCollectionModel.IsAnyAmountModifiable(true);

            expect(loanCollectionModel.isLoanCollectionOnHoldOrInReview()).toBe(true);
        });

        it(":isLoanCollectionOnHoldOrInReview returns false if the collection is not on hold or in review", function () {
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].StatusDescription("Accepted");
            }
            loanCollectionModel.IsLoanCollectionInReview(false);
            loanCollectionModel.accordionIsOpen(false);
            loanCollectionModel.IsStatusModifiable(true);
            loanCollectionModel.IsAnyAmountModifiable(false);

            expect(loanCollectionModel.isLoanCollectionOnHoldOrInReview()).toBe(false);
        });

        it(":offScreenDescription returns expected result for SubsidizedLoan", function () {
            loanCollectionModel.LoanCollectionType("SubsidizedLoan");
            var expected = "Subsidized loan title";
            expect(loanCollectionModel.offScreenDescription()).toEqual(expected);
        });

        it(":offScreenDescription returns expected result for UnsubsidizedLoan", function () {
            loanCollectionModel.LoanCollectionType("UnsubsidizedLoan");
            var expected = "Unsubsidized loan title";
            expect(loanCollectionModel.offScreenDescription()).toEqual(expected);
        });

        it(":offScreenDescription returns expected result for GraduatePlusLoan", function () {
            loanCollectionModel.LoanCollectionType("GraduatePlusLoan");
            var expected = "Grad Plus loans title";
            expect(loanCollectionModel.offScreenDescription()).toEqual(expected);
        });

        it(":isLoanCollectionActionable returns false if collection is unsub and sub loan is pending", function () {
            awardsViewModelData.isSubLoanPending(true);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isLoanCollectionActionable()).toBe(false);
        });

        it(":isLoanCollectionActionable returns true if collection is unsub and sub loan is not pending", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isLoanCollectionActionable()).toBe(true);
        });

        it(":isLoanCollectionActionable returns true if collection is grad plus and sub loan is pending", function () {
            awardsViewModelData.isSubLoanPending(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentGradPlusLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isLoanCollectionActionable()).toBe(true);
        });

        it(":areAnyPeriodsActive returns false if there are no active periods", function () {
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsStatusModifiable(true);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsActive(false);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.areAnyPeriodsActive()).toBe(false);
        });

        it(":areAnyPeriodsActive returns true if there is at least one active period", function () {
            awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[0].IsActive(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.areAnyPeriodsActive()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns true if loan collection is not actionable", function () {
            awardsViewModelData.isSubLoanPending(true);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");

            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns true if loan collection does not pass limitation criteria", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(false);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(5000);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsStatusModifiable(true);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsActive(true);
            }
            awardsViewModelData.StudentUnsubLoans[0].MaximumAmount(5000);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns true if no amount is modifiable", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsAnyAmountModifiable(false);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns true if there are input errors", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsAnyAmountModifiable(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(true);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns true if total loan amount is zero and allow zero flag is false", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsAnyAmountModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(0);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns true if isAdmin is true", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.isAdmin(true);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsAnyAmountModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns true if IsProxyView is true", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.isAdmin(false);
            awardsViewModelData.IsProxyView(true);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsAnyAmountModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableSubmitLoanAmountChangeButton returns false if all conditions for it to be true are unsatisfied", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.isAdmin(false);
            awardsViewModelData.IsProxyView(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsAnyAmountModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsActive(true);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(false);
        });

        it(":disableSubmitLoanAmountChangeButton returns false if totalAmount is zero and allow zero flag is true", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.isAdmin(false);
            awardsViewModelData.IsProxyView(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsAnyAmountModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsActive(true);
            }
            awardsViewModelData.yearConfiguration().AllowDeclineZeroOfAcceptedLoans(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(false);
        });

        it(":disableRejectButton returns true if loan collection is not actionable", function () {
            awardsViewModelData.isSubLoanPending(true);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.disableRejectButton()).toBe(true);
        });

        it(":disableRejectButton returns true if loanCollection is not status modifiable", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(false);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.disableRejectButton()).toBe(true);
        });

        it(":disableRejectButton returns true if there are input errors", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(true);

            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(true);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableRejectButton returns true if there are no actionable periods", function () {
            awardsViewModelData.isSubLoanPending(false);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsActive(false);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(true);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(true);
        });

        it(":disableRejectButton returns false if none of the conditions to be true are met", function () {
            awardsViewModelData.isSubLoanPending(false);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsActive(true);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableSubmitLoanAmountChangeButton()).toBe(false);
        });

        it(":disableAcceptButton returns true if loan collection is not actionable", function () {
            awardsViewModelData.isSubLoanPending(true);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns true if there are input errors", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(true);
            expect(loanCollectionModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns true if loan collection does not pass limitation criteria", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(false);
            awardsViewModelData.StudentUnsubLoans[0].LoanCollectionType("UnsubsidizedLoan");
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(5000);
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsStatusModifiable(true);
            }
            awardsViewModelData.StudentUnsubLoans[0].MaximumAmount(5000);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);

            expect(loanCollectionModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns true if loanCollection is not status modifiable", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(false);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns true if total loan amount is zero", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(0);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns true if status is modifiable and the loan collection is accepted", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            loanCollectionModel.IsAccepted(true);
            expect(loanCollectionModel.disableAcceptButton()).toBe(true);
        });

        it(":disableAcceptButton returns false if all conditions for it to be true are unsatisfied", function () {
            awardsViewModelData.isSubLoanPending(false);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.hasInputErrors(false);
            loanCollectionModel.IsAccepted(false);
            expect(loanCollectionModel.disableAcceptButton()).toBe(false);
        });

        it(":isLoanCollectionWarningVisible is true if SuppressMaximumLoanLimits is false", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(false);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isLoanCollectionWarningVisible()).toBe(true);
        });

        it(":isLoanCollectionWarningVisible is true if isLoanColectionActionable is false", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.isSubLoanPending(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isLoanCollectionWarningVisible()).toBe(true);
        });

        it(":isLoanCollectionWarningVisible is false if none of the conditions are met to be true", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.isSubLoanPending(false);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isLoanCollectionWarningVisible()).toBe(false);
        });

        it(":isAnySubLoanPending return true if isSubLoan observable is true", function () {
            awardsViewModelData.isSubLoanPending(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isAnySubLoanPending()).toBe(true);
        });

        it(":isAnySubLoanPending return false if isSubLoan observable is false", function () {
            awardsViewModelData.isSubLoanPending(false);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isAnySubLoanPending()).toBe(false);
        });

        it(":isSubmitLoanAmountChangeButtonVisible returns true if status is not modifiable but amount is", function () {
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(false);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsAmountModifiable(true);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.isSubmitLoanAmountChangeButtonVisible()).toBe(true);
        });

        it(":isSubmitLoanAmountChangeButtonVisible returns true if status is modifiable and loan collection is accepted", function () {
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].IsAmountModifiable(false);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.IsAccepted(true);
            expect(loanCollectionModel.isSubmitLoanAmountChangeButtonVisible()).toBe(true);
        });

        it(":isSubmitLoanAmountChangeButtonVisible returns false if status is modifiable and loan collection is not accepted", function () {
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.IsAccepted(false);
            expect(loanCollectionModel.isSubmitLoanAmountChangeButtonVisible()).toBe(false);
        });

        it(":allowDeclineZeroAcceptedLoans returns true if the flag is true", function () {
            awardsViewModelData.yearConfiguration().AllowDeclineZeroOfAcceptedLoans(true);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.allowDeclineZeroAcceptedLoans()).toBe(true);
        });

        it(":allowDeclineZeroAcceptedLoans returns false if the flag is false", function () {
            awardsViewModelData.yearConfiguration().AllowDeclineZeroOfAcceptedLoans(false);
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            expect(loanCollectionModel.allowDeclineZeroAcceptedLoans()).toBe(false);
        });

        it(":showErrorIcon returns true if accordion is closed and loan collection does not pass the limitation criteria", function () {
            loanCollectionModel.MaximumAmount(25);
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(false);
            for (var i = 0; i < loanCollectionModel.Loans().length; i++) {
                loanCollectionModel.Loans()[i].AwardStatus.Category("Pending");
            }
            loanCollectionModel.IsStatusModifiable(true);
            loanCollectionModel.IsAnyAmountModifiable(true);
            loanCollectionModel.accordionIsOpen(false);
            expect(loanCollectionModel.showErrorIcon()).toBe(true);
        });

        it(":showErrorIcon returns true if accordion is closed and there are input errors", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            loanCollectionModel.accordionIsOpen(false);
            loanCollectionModel.hasInputErrors(true);
            expect(loanCollectionModel.showErrorIcon()).toBe(true);
        });

        it(":showErrorIcon returns true if accordion is closed, total amount is zero and zeroes are not allowed", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(true);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(0);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.accordionIsOpen(false);
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.showErrorIcon()).toBe(true);
        });

        it(":showErrorIcon is false if none of the above conditions are met", function () {
            awardsViewModelData.yearConfiguration().SuppressMaximumLoanLimits(true);
            awardsViewModelData.StudentUnsubLoans[0].IsStatusModifiable(false);
            for (var i = 0; i < awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods().length; i++) {
                awardsViewModelData.StudentUnsubLoans[0].Loans[0].StudentAwardPeriods()[i].AwardAmount(10);
            }
            loanCollectionModel = new Ellucian.AwardsViewModelMapping.studentLoanCollectionModel({ data: awardsViewModelData.StudentUnsubLoans[0], parent: awardsViewModelData });
            loanCollectionModel.accordionIsOpen(false);
            loanCollectionModel.hasInputErrors(false);
            expect(loanCollectionModel.showErrorIcon()).toBe(false);
        });
    });
});