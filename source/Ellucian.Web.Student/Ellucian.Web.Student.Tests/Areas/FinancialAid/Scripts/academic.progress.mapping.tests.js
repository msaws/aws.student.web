﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
describe("academic.progress.mapping.js", function () {

    var academicProgressData,
        academicProgressViewModelInstance;

    beforeAll(function () {
        academicProgressData = testData().academicProgressData;
        faAcademicProgressEvaluationMissingStartTermDateLabel = "Ending {0}";
        faAcademicProgressStatusExplanationHeader = "Status explanation header {0}";
        faAcademicProgressEvaluationDateLabel = "evaluationDateLabel {0}";
        faAcademicProgressEvaluationSAPHistoryItemHeader = "sap history item header";
        faAcademicProgressEvaluationViewHistoryItemLabel = "view history item label";
        faSharedAcademicProgressMessageHighlightedText = "highlightedText";
        faSharedSatisfactoryAcademicProgressMessage = "academicProgressMessage";
        academicProgressAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        academicProgressViewModelInstance = new academicProgressViewModel();
        ko.mapping.fromJS(academicProgressData, academicProgressViewModelMapping, academicProgressViewModelInstance);
    });

    describe("sapStatusModelTests", function () {
        var sapModel,
            options;

        beforeEach(function () {
            options = {
                data: academicProgressData.LatestAcademicProgressEvaluation.SAPStatus
            };
            sapModel = new sapStatusModel(options.data);
        });

        it(":evaluationPeriod returns expected value when period start and end terms are present", function () {
            var expected = "2016/FA - 2017/FA";
            expect(sapModel.evaluationPeriod()).toEqual(expected);
        });

        it(":evaluationPeriod returns expected value when period start term is missing and end term is present", function () {
            var expected = "Ending 2017/FA";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodStartTerm(null);
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.evaluationPeriod()).toEqual(expected);
        });

        it(":evaluationPeriod returns expected value when period start and end dates are present", function () {
            var expected = "08/04/16 - 08/04/17";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodStartTerm(null);
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodEndTerm(null);
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodStartDate("08/04/16");
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodEndDate("08/04/17");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.evaluationPeriod()).toEqual(expected);
        });

        it(":evaluationPeriod returns expected value when period start date is missing and end date is present", function () {
            var expected = "Ending 08/04/17";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodStartTerm(null);
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodEndTerm(null);
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodStartDate(null);
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.PeriodEndDate("08/04/17");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.evaluationPeriod()).toEqual(expected);
        });

        it(":academicProgressStyle returns expected value when status category is 'Warning'", function () {
            var expected = "warning";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Category("Warning");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.academicProgressStatusStyle()).toEqual(expected);
        });

        it(":academicProgressStyle returns expected value when status category is 'Unsatisfactory'", function () {
            var expected = "error";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Category("Unsatisfactory");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.academicProgressStatusStyle()).toEqual(expected);
        });

        it(":academicProgressStyle returns expected value when status category is 'Satisfactory'", function () {
            var expected = "check";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Category("Satisfactory");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.academicProgressStatusStyle()).toEqual(expected);
        });

        it(":academicProgressStyle is undefined when sap status category is of unknown type", function () {
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Category("foo");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.academicProgressStatusStyle()).toBeUndefined();
        });

        it(":statusExplanationHeader returns expected value when status is present", function () {
            var expected = "Status explanation header Probation desc";
            expect(sapModel.statusExplanationHeader()).toEqual(expected);
        });

        it(":statusExplanationHeader returns expected value when status description is not present", function () {
            var expected = "Status explanation header ";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Description("");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.statusExplanationHeader()).toEqual(expected);
        });

        it(":academicProgressEvaluationDateLabel returns expected result when EvaluationDate is null", function () {
            var expected = "evaluationDateLabel ";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.EvaluationDate(null);
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.academicProgressEvaluationDateLabel()).toEqual(expected);
        });

        it(":academicProgressEvaluationDateLabel returns expected result when EvaluationDate is presentl", function () {
            var expected = "evaluationDateLabel 08/18/2016";
            academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.EvaluationDate("08/18/2016");
            sapModel = new sapStatusModel(options.data);
            expect(sapModel.academicProgressEvaluationDateLabel()).toEqual(expected);
        });
        
    });

    describe("sapDetailItemModelTests", function () {
        var detailItemModel,
            options;

        beforeEach(function () {
            options = {
                data: academicProgressData.LatestAcademicProgressEvaluation.SAPDetailItems[0]
            };
            detailItemModel = new sapDetailItemModel(options.data);
        });

        it(":detailItemModel and its properties are defined and equal expected ones", function () {
            expect(detailItemModel).toBeDefined();
            expect(detailItemModel.Code()).toBeDefined();
            expect(detailItemModel.Code()).toEqual(academicProgressData.LatestAcademicProgressEvaluation.SAPDetailItems[0].Code());
            expect(detailItemModel.Description()).toBeDefined();
            expect(detailItemModel.Description()).toEqual(academicProgressData.LatestAcademicProgressEvaluation.SAPDetailItems[0].Description());
            expect(detailItemModel.Explanation()).toBeDefined();
            expect(detailItemModel.Explanation()).toEqual(academicProgressData.LatestAcademicProgressEvaluation.SAPDetailItems[0].Explanation());
        });
    });

    describe("academicProgressStatusModelTests", function () {
        var statusModel,
            options;

        beforeEach(function () {
            options = {
                data: academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus
            };
            statusModel = new academicProgressStatusModel(options.data);
        });

        it(":academicProgressStatusModel was mapped correctly", function () {
            expect(statusModel).toBeDefined();
            expect(statusModel.Category()).toEqual(academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Category());
            expect(statusModel.Code()).toEqual(academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Code());
            expect(statusModel.Description()).toEqual(academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Description());
            expect(statusModel.Explanation()).toEqual(academicProgressData.LatestAcademicProgressEvaluation.SAPStatus.AcademicProgressStatus.Explanation());
        });
    });

    describe("academicProgressEvaluationModelTests", function () {
        var evaluationModel,
            options;

        beforeEach(function () {
            options = {
                data: academicProgressData.LatestAcademicProgressEvaluation,
                parent: academicProgressData
            };
            evaluationModel = new academicProgressEvaluationModel(options);
        });

        it(":academicProgressEvaluationModel is defined", function () {
            expect(evaluationModel).toBeDefined();
            expect(evaluationModel.SAPStatus).toBeDefined();
            expect(evaluationModel.SAPDetailItems).toBeDefined();
            expect(evaluationModel.SAPAppeal).toBeDefined();
        });

        it(":evaluationPeriod is defined", function () {
            expect(evaluationModel.evaluationPeriod()).toBeDefined();
        });        
    });
});