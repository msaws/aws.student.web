﻿//Copyright 2017 Ellucian Company L.P. and its affiliates
describe("shopping.sheet.mapping.js", function () {
    var shoppingSheetViewModelData;
        
    beforeAll(function () {
        shoppingSheetViewModelData = testData().shoppingSheetViewModelData;
        faShoppingSheetCostsForYearLabel = "Costs for year: {0}";
        faShoppingSheetCostOfAttendanceGlossaryText = "Cost of attendance {0}";
        faShoppingSheetNetCostsGlossaryText = "Net costs {0}";
    });

    describe("shoppingSheetModelTests", function () {
        var shoppingSheetModelInstance;

        it(":shortYearLabel is blank when there is no AwardYearLabel and AwardYearCode", function () {
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearLabel(null);
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearCode(null);
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.shortYearLabel()).toEqual(" ");
        });

        it(":shortYearLabel equals AwardYearLabel if it exists", function () {
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearLabel("2017");
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.shortYearLabel()).toEqual(shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearLabel());
        });

        it(":shortYearLabel equals AwardYearCode if AwardYearLabel is null but AwardYearCode exists", function () {
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearLabel(null);
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearCode("2017-2018");
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.shortYearLabel()).toEqual(shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearCode());
        });

        it(":costsForYearLabel equals expected value", function () {
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearLabel("2017");
            var expected = "Costs for year: 2017";
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.costsForYearLabel()).toEqual(expected);
        });

        it(":costOfAttendanceGlossaryText equals expected value", function () {
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearLabel("2017");
            var expected = "Cost of attendance 2017";
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.costOfAttendanceGlossaryText()).toEqual(expected);
        });

        it(":netCostGlossaryText equals expected value", function () {
            shoppingSheetViewModelData.StudentShoppingSheets[0].AwardYearLabel("2017");
            var expected = "Net costs 2017";
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.netCostGlossaryText()).toEqual(expected);
        });

        it(":configuration is null if yearConfiguration is null", function () {
            shoppingSheetViewModelData.yearConfiguration(null);
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.configuration()).toBe(null);
        });

        it(":configuration to not be null if yearConfiguration is not null", function () {
            shoppingSheetViewModelData = testData().shoppingSheetViewModelData;
            shoppingSheetModelInstance = new shoppingSheetModel(shoppingSheetViewModelData.StudentShoppingSheets[0], shoppingSheetViewModelData);
            expect(shoppingSheetModelInstance.configuration()).not.toBe(null);
        });
    });

    describe("shoppingSheetConfigurationModelTests", function () {
        var shoppingSheetConfigurationModelInstance;

        beforeAll(function () {
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
        });

        it(":graduationRateLevel returns expected value when the level is low", function () {
            expect(shoppingSheetConfigurationModelInstance.graduationRateLevel()).toEqual("hbar-pointer-low");
        });

        it(":graduationRateLevel returns expected value when the level is medium", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.GraduationRateLevel("Medium");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.graduationRateLevel()).toEqual("hbar-pointer-medium");
        });

        it(":graduationRateLevel returns expected value when the level is high", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.GraduationRateLevel("High");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.graduationRateLevel()).toEqual("hbar-pointer-high");
        });

        it(":graduationRateLevel is undefined when the level is unknown value", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.GraduationRateLevel("foo");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.graduationRateLevel()).toBeUndefined();
        });

        it(":loanDefaultRateLevel returns expected value when the level is low", function () {
            expect(shoppingSheetConfigurationModelInstance.loanDefaultRateLevel()).toEqual("ldr-left-bar-lower");
        });

        it(":loanDefaultRateLevel returns expected value when the level is medium", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.LoanDefaultRateLevel("Medium");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.loanDefaultRateLevel()).toEqual("ldr-left-bar-equal");
        });

        it(":loanDefaultRateLevel returns expected value when the level is high", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.LoanDefaultRateLevel("High");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.loanDefaultRateLevel()).toEqual("ldr-left-bar-higher");
        });

        it(":loanDefaultRateLevel is undefined when the level is an unknown value", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.LoanDefaultRateLevel("bar");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.loanDefaultRateLevel()).toBeUndefined();
        });

        it(":nationalLoanDefaultRateLevel returns expected value when the level is low", function () {
            expect(shoppingSheetConfigurationModelInstance.nationalLoanDefaultRateLevel()).toEqual("ldr-right-bar-lower");
        });

        it(":nationalLoanDefaultRateLevel returns expected value when the level is medium", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.NationalLoanDefaultRateLevel("Medium");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.nationalLoanDefaultRateLevel()).toEqual("ldr-right-bar-equal");
        });

        it(":nationalLoanDefaultRateLevel returns expected value when the level is high", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.NationalLoanDefaultRateLevel("High");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.nationalLoanDefaultRateLevel()).toEqual("ldr-right-bar-higher");
        });

        it(":nationalLoanDefaultRateLevel is undefined when the level is an unknown value", function () {
            shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration.NationalLoanDefaultRateLevel("bar");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.nationalLoanDefaultRateLevel()).toBeUndefined();
        });

        it(":vbarPointerTop returns expected value when the NationalRepaymentRateLevel is low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            var expected = (50 - (config.NationalRepaymentRateAverage() / config.InstitutionRepaymentRate() * 80)) + "px";
            expect(shoppingSheetConfigurationModelInstance.vbarPointerTop()).toEqual(expected);
        });

        it(":vbarPointerTop returns expected value when the NationalRepaymentRateLevel is not low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            config.NationalRepaymentRateLevel("High");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            var expected = "-30px";
            expect(shoppingSheetConfigurationModelInstance.vbarPointerTop()).toEqual(expected);
        });

        it(":barAverageBottom returns expected value when the NationalRepaymentRateLevel is low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            config.NationalRepaymentRateLevel("Low");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            var expected = (config.NationalRepaymentRateAverage() / config.InstitutionRepaymentRate() * 80) + "px";
            expect(shoppingSheetConfigurationModelInstance.barAverageBottom()).toEqual(expected);
        });

        it(":barAverageBottom returns expected value when the NationalRepaymentRateLevel is not low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            config.NationalRepaymentRateLevel("High");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            var expected = "80px";
            expect(shoppingSheetConfigurationModelInstance.barAverageBottom()).toEqual(expected);
        });

        it(":barBodyHeight returns expected value when the InstitutionRepaymentRateLevel is low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            config.InstitutionRepaymentRateLevel("Low");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            var expected = (config.InstitutionRepaymentRate() / config.NationalRepaymentRateAverage() * 80) + "px";
            expect(shoppingSheetConfigurationModelInstance.barBodyHeight()).toEqual(expected);
        });

        it(":barBodyHeight returns expected value when the InstitutionRepaymentRateLevel is not low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            config.InstitutionRepaymentRateLevel("High");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            var expected = "80px";
            expect(shoppingSheetConfigurationModelInstance.barBodyHeight()).toEqual(expected);
        });

        it(":barBodyMarginTop returns expected value when the InstitutionRepaymentRateLevel is low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            config.InstitutionRepaymentRateLevel("Low");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            var expected = (80 - (config.InstitutionRepaymentRate() / config.NationalRepaymentRateAverage() * 80)) + "px";
            expect(shoppingSheetConfigurationModelInstance.barBodyMarginTop()).toEqual(expected);
        });

        it(":barBodyMarginTop returns expected value when the InstitutionRepaymentRateLevel is not low", function () {
            var config = shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration;
            config.InstitutionRepaymentRateLevel("High");
            shoppingSheetConfigurationModelInstance = new shoppingSheetConfigurationModel(shoppingSheetViewModelData.yearConfiguration().ShoppingSheetConfiguration);
            expect(shoppingSheetConfigurationModelInstance.barBodyMarginTop()).toBeUndefined();
        });
    });
});