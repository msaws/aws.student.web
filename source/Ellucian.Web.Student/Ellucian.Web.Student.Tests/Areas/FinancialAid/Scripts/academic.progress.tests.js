﻿//Copyright 2015-2016 Ellucian Company L.P. and its affiliates

describe("academic.progress.js", function () {
    var academicProgressUrl,
        contentType;

    beforeAll(function () {
        getAcademicProgressUrl = "url";
        jsonContentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        academicProgressAdminActionUrl = "adminUrl";
        academicProgressViewModelInstance = new academicProgressViewModel();
        faNotAuthorizedMessage = "Not authorized";
        faAcademicProgressUnableToLoadMessage = "Unable to load";
        faMissingStudentIdMessage = "StudentId is required";
        faInvalidStudentIdMessage = "Invalid id";        
    });

    beforeEach(function () {        
        academicProgressUrl = "url";
        contentType = "json";        
    });       
   
    it(":ajax call uses correct url", function () {
        spyOn($, "ajax");
        getAcademicProgress();
        expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(academicProgressUrl);
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getAcademicProgress();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":showUI has been called with true on success", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(academicProgressViewModelInstance, "showUI");
        getAcademicProgress();
        expect(academicProgressViewModelInstance.showUI).toHaveBeenCalledWith(true);
    });

    it(":on error with status of 403 correct message is being sent", function () {
        var jqXHR = {
            status: 403
        };
        var secondArg = {
            message: "Not authorized",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });
       
        spyOn($.fn, "notificationCenter");
        getAcademicProgress();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":correct message is being sent when non-403 error occurs", function () {
        var jqXHR = {
            status: 400
        };
        var secondArg = {
            message: "StudentId is required",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getAcademicProgress();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":correct message is being sent when 404 error occurs", function () {
        var jqXHR = {
            status: 404
        };
        var secondArg = {
            message: "Invalid id",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getAcademicProgress();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":notification center does not get called when error status is 0", function () {
        var jqXHR = {
            status: 0
        };
       
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getAcademicProgress();
        expect($.fn.notificationCenter).not.toHaveBeenCalled();
    });

    it(":correct message is set when an error other than 400/403 occurs", function () {
        var jqXHR = {
            status: 500
        };
        var secondArg = {
            message: "Unable to load",
            type: "error"
        }

        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXHR, undefined, undefined);
        });

        spyOn($.fn, "notificationCenter");
        getAcademicProgress();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":checkForMobile has been called on complete", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({});
        });
        spyOn(academicProgressViewModelInstance, "checkForMobile");
        getAcademicProgress();
        expect(academicProgressViewModelInstance.checkForMobile).toHaveBeenCalled();
    });

    it(":rearrangeElements has been called on complete", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({});
        });
        spyOn(window, "rearrangeElements").and.callThrough();
        getAcademicProgress();
        expect(rearrangeElements).toHaveBeenCalled();
    });

    it(":rearrangeElements invokes .each function", function () {
        var array = [$('div'), $('div')];
        spyOn($.fn, "each");
        rearrangeElements($(array));
        expect($.fn.each).toHaveBeenCalled();
    });
        
});