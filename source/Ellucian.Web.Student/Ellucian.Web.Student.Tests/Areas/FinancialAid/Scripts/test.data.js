﻿//Copyright 2015-2017 Ellucian Company L.P. and its affiliates
var testData = function() {

    return {
        //Array of award years
        awardYearsData: [
            {
                Code: ko.observable("2015"),
                AreChecklistItemsAssigned: ko.observable(true)
            },
            {
                Code: ko.observable("2016"),
                AreChecklistItemsAssigned: ko.observable(true)
            }
        ],

        //Array of student checklists
        studentChecklistsData: [
            {
                AwardYearCode: ko.observable("2015"),
                ChecklistItems: ko.observable([
                    {
                        Type: ko.observable("AwardPackage"),
                        Link: ko.observable("www.luci.com"),
                        Description: ko.observable("Award Package checklist item"),
                        Target: ko.observable("_blank"),
                        Status: ko.observable("Completed"),
                        StatusDate: ko.observable("10/12/2015"),
                        DueDate: ko.observable("12/12/2015"),
                        ExpirationDate: ko.observable("12/12/2015"),
                        Title: ko.observable("Award Package"),
                        Summary: ko.observable("Award Package Summary"),
                        IsActive: ko.observable(true),
                        IsReadOnly: ko.observable(false),
                        ControlStatus: ko.observable("Required")
                    },
                    {
                        Type: ko.observable("AwardLetter"),
                        Link: ko.observable("www.awardletter.com"),
                        Description: ko.observable("Award letter checklist item"),
                        Target: ko.observable("_blank"),
                        Status: ko.observable("ActionNeeded"),
                        StatusDate: ko.observable("08/08/2015"),
                        DueDate: ko.observable("10/10/2015"),
                        ExpirationDate: ko.observable(),
                        Title: ko.observable("Award Letter"),
                        Summary: ko.observable("AL summary"),
                        IsActive: ko.observable(false),
                        IsReadOnly: ko.observable(true),
                        ControlStatus: ko.observable("Skipped")
                    },
                    {
                        Type: ko.observable("FAFSA"),
                        Link: ko.observable("www.luci.com"),
                        Description: ko.observable("FAFSA checklist item"),
                        Target: ko.observable("_blank"),
                        Status: ko.observable("Completed"),
                        StatusDate: ko.observable("01/12/2015"),
                        DueDate: ko.observable("12/12/2015"),
                        ExpirationDate: ko.observable("12/12/2015"),
                        Title: ko.observable("FAFSA"),
                        Summary: ko.observable("FAFSA Summary"),
                        IsActive: ko.observable(false),
                        IsReadOnly: ko.observable(false),
                        ControlStatus: ko.observable("Required")
                    }
                ]),
                ActiveChecklistItem: ko.observable({
                    Type: ko.observable("AwardPackage"),
                    Link: ko.observable("www.luci.com"),
                    Description: ko.observable("Award Package checklist item"),
                    Target: ko.observable("_blank"),
                    Status: ko.observable("Completed"),
                    StatusDate: ko.observable("10/12/2015"),
                    DueDate: ko.observable("12/12/2015"),
                    ExpirationDate: ko.observable("12/12/2015"),
                    Title: ko.observable("Award Package"),
                    Summary: ko.observable("Award Package Summary"),
                    IsActive: ko.observable(true),
                    IsReadOnly: ko.observable(false),
                    ControlStatus: ko.observable("Required")
                })
            },

            {
                AwardYearCode: ko.observable("2016"),
                ChecklistItems: ko.observable([
                    {
                        Type: ko.observable("AwardPackage"),
                        Link: ko.observable("www.luci.com"),
                        Description: ko.observable("Award Package checklist item 2016"),
                        Target: ko.observable("_blank"),
                        Status: ko.observable("Completed"),
                        StatusDate: ko.observable("10/12/2016"),
                        DueDate: ko.observable("12/12/2016"),
                        ExpirationDate: ko.observable("12/12/2016"),
                        Title: ko.observable("Award Package"),
                        Summary: ko.observable("Award Package Summary"),
                        IsActive: ko.observable(false),
                        IsReadOnly: ko.observable(false),
                        ControlStatus: ko.observable("Required")
                    },
                    {
                        Type: ko.observable("AwardLetter"),
                        Link: ko.observable("www.awardletter.com"),
                        Description: ko.observable("Award letter checklist item 2016"),
                        Target: ko.observable("_blank"),
                        Status: ko.observable("ActionNeeded"),
                        StatusDate: ko.observable("08/08/2016"),
                        DueDate: ko.observable("10/10/2016"),
                        ExpirationDate: ko.observable(),
                        Title: ko.observable("Award Letter"),
                        Summary: ko.observable("AL summary"),
                        IsActive: ko.observable(false),
                        IsReadOnly: ko.observable(true),
                        ControlStatus: ko.observable("Skipped")
                    },
                    {
                        Type: ko.observable("FAFSA"),
                        Link: ko.observable("www.luci.com"),
                        Description: ko.observable("FAFSA checklist item"),
                        Target: ko.observable("_blank"),
                        Status: ko.observable("Completed"),
                        StatusDate: ko.observable("01/12/2016"),
                        DueDate: ko.observable("12/12/2016"),
                        ExpirationDate: ko.observable("12/12/2016"),
                        Title: ko.observable("FAFSA"),
                        Summary: ko.observable("FAFSA Summary"),
                        IsActive: ko.observable(true),
                        IsReadOnly: ko.observable(false),
                        ControlStatus: ko.observable("Required")
                    }
                ]),
                ActiveChecklistItem: ko.observable({
                    Type: ko.observable("FAFSA"),
                    Link: ko.observable("www.luci.com"),
                    Description: ko.observable("FAFSA checklist item"),
                    Target: ko.observable("_blank"),
                    Status: ko.observable("Completed"),
                    StatusDate: ko.observable("01/12/2016"),
                    DueDate: ko.observable("12/12/2016"),
                    ExpirationDate: ko.observable("12/12/2016"),
                    Title: ko.observable("FAFSA"),
                    Summary: ko.observable("FAFSA Summary"),
                    IsActive: ko.observable(true),
                    IsReadOnly: ko.observable(false),
                    ControlStatus: ko.observable("Required")
                })
            }
        ],

        //Array of averageAwardPackages
        averageAwardPackageData: [
            {
                AverageGrantAmount: ko.observable(15000),
                AverageLoanAmount: ko.observable(35000),
                AverageScholarshipAmount: ko.observable(2000),
                AwardYearCode: ko.observable("2015"),
                IsAverageAwardPackageInfoLinkPresent: ko.observable(true)
            },
            {
                AverageGrantAmount: ko.observable(3456),
                AverageLoanAmount: ko.observable(8765),
                AverageScholarshipAmount: ko.observable(9876),
                AwardYearCode: ko.observable("2016"),
                IsAverageAwardPackageInfoLinkPresent: ko.observable(false)
            }
        ],

        //Student loan summary
        studentLoansSummaryData: {
            TotalLoanAmountBorrowed: ko.observable(2000),
            AggregateTotalLoanAmount: ko.observable(5000),
            OtherLoansAmount: ko.observable(3000),
            LoanHistory: ko.observable([
                {
                    SchoolName: ko.observable("Ellucian university"),
                    TotalAmount: ko.observable(1300)
                },
                {
                    SchoolName: ko.observable("Ellucian institute of business"),
                    TotalAmount: ko.observable(700)
                }
            ]),

        },

        //Array of links 
        linksData: [
            {
                LinkType: ko.observable("form 1"),
                LinkUrl: ko.observable("form1.com"),
                Title: ko.observable("form1")
            },
            {
                LinkType: ko.observable("form 2"),
                LinkUrl: ko.observable("form2.com"),
                Title: ko.observable("form2")
            },
            {
                LinkType: ko.observable("user"),
                LinkUrl: ko.observable("user.edu"),
                Title: ko.observable("user link")
            },
            {
                LinkType: ko.observable("sap"),
                LinkUrl: ko.observable("sap page url"),
                Title: ko.observable("sap link")
            },
            {
                LinkType: ko.observable("other"),
                LinkUrl: ko.observable("luci.com"),
                Title: ko.observable("other link")
            }
        ],

        personData: {
            Id: ko.observable("0004791")
        },

        //FinancialAid configuration
        configurationData: {
            IsConfigurationComplete: ko.observable(true),
            MissingConfigurationMessages: ko.observable(["message 1", "message 2"]),
            AwardYearConfigurations: ko.observable([
                {
                    AwardYearCode: ko.observable("2015"),
                    IsDeclinedStatusChangeReviewRequired: ko.observable(true),
                    IsLoanAmountChangeReviewRequired: ko.observable(false),
                    IsLoanRequestEnabled: ko.observable(true),
                    AreAwardingChangesAllowed: ko.observable(false),
                    AllowAnnualAwardUpdatesOnly: ko.observable(false),
                    ShoppingSheetConfiguration: ko.observable({}),
                    DisplayPellLifetimeEligibilityUsedPercentage: ko.observable(true),
                    SuppressStudentAccountSummaryDisplay: ko.observable(true),
                    SuppressAverageAwardPackageDisplay: ko.observable(true)
                },
                {
                    AwardYearCode: ko.observable("2016"),
                    IsDeclinedStatusChangeReviewRequired: ko.observable(false),
                    IsLoanAmountChangeReviewRequired: ko.observable(false),
                    IsLoanRequestEnabled: ko.observable(true),
                    AreAwardingChangesAllowed: ko.observable(true),
                    AllowAnnualAwardUpdatesOnly: ko.observable(true),
                    ShoppingSheetConfiguration: ko.observable({}),
                    DisplayPellLifetimeEligibilityUsedPercentage: ko.observable(true),
                    SuppressStudentAccountSummaryDisplay: ko.observable(true),
                    SuppressAverageAwardPackageDisplay: ko.observable(true)
                }
            ])
        },

        sapStatusData: {
            PeriodStartTerm: ko.observable(),
            PeriodEndTerm: ko.observable(),
            PeriodStartDate: ko.observable(),
            PeriodEndDate: ko.observable(),
            EvaluationDate: ko.observable(),
            AcademicProgressStatus: ko.observable(
                {

                }),
            StudentAcademicProgram: ko.observable()
        },

        shoppingSheetsData: [
            {
                AwardYearCode: ko.observable("2015"),
                BooksAndSupplies: ko.observable(2500),
                HousingAndMeals: ko.observable(15000),
                OtherCosts: ko.observable(1000),
                TuitionAndFees: ko.observable(20000),
                StateGrants: ko.observable(10000),
                UnsubsidizedLoans: ko.observable(10000)
            },
            {
                AwardYearCode: ko.observable("2016"),
                BooksAndSupplies: ko.observable(5000),
                HousingAndMeals: ko.observable(10000),
                OtherCosts: ko.observable(4000),
                TuitionAndFees: ko.observable(30000),
                StateGrants: ko.observable(5000),
                UnsubsidizedLoans: ko.observable(6000)
            }
        ],

        shoppingSheetViewModelData: {
            yearConfiguration: ko.observable({
                ShoppingSheetConfiguration: {
                    GraduationRateLevel: ko.observable("Low"),
                    LoanDefaultRateLevel: ko.observable("Low"),
                    NationalLoanDefaultRateLevel: ko.observable("Low"),
                    NationalRepaymentRateLevel: ko.observable("Low"),
                    InstitutionRepaymentRateLevel: ko.observable("High"),
                    InstitutionRepaymentRate: ko.observable(50),
                    NationalRepaymentRateAverage: ko.observable(40)
                }
            }),
            StudentShoppingSheets: [
                {
                    AwardYearCode: ko.observable("2017"),
                    AwardYearLabel: ko.observable("2017-2018")
                },
                {
                    AwardYearCode: ko.observable("2015"),
                    AwardYearLabel: ko.observable("2015-2016")
                }
            ]
        },

        //Loan Request data
        loanRequestData: {
            AwardYearCode: ko.observable("2015"),
            TotalRequestAmount: ko.observable(2000),
            AssignedToCounselor: ko.observable({
                Id: ko.observable("0003914"),
                PhoneNumber: ko.observable("777-567-4567"),
                Extension: ko.observable("123"),
                EmailAddress: ko.observable("counselor@gmail.com")

            }),
            IsRequestPending: ko.observable(false),
            StudentId: ko.observable("0004791"),
            LoanRequestAwardPeriods:[
                {
                    Code: ko.observable("15/FA"),
                    Description: ko.observable("Fall 2015"),
                    StartDate: ko.observable("05/10/2016"),
                    AwardAmount: ko.observable(2000)
                },
                {
                    Code: ko.observable("16/SP"),
                    Description: ko.observable("Spring 2016"),
                    StartDate: ko.observable("05/11/2016"),
                    AwardAmount: ko.observable(5000)
                }
            ],
            requestSubmitted: ko.observable(false),
            currentWorkFlowStep: ko.observable(),
            cancelRequest: function () { },
            UnmetCost: ko.observable(),
            Comments: ko.observable(),
            clearCustomInput: function () { }
        },

        documentsData: {
            StudentDocumentCollections: [
                {
                    AwardYearCode: ko.observable("2016"),
                    IncompleteDocuments: [
                        {
                            Code: ko.observable("Incomplete doc 1"),
                            Description: ko.observable("Incomplete doc 1 description"),
                            Status: ko.observable("Incomplete"),
                            StatusDisplay: ko.observable("Do not have it yet")
                        },
                        {
                            Code: ko.observable("Incomplete doc 2"),
                            Description: ko.observable("Incomplete doc 2 description"),
                            Status: ko.observable("Overdue"),
                            StatusDisplay: ko.observable("Way overdue")
                        }
                    ],
                    CompleteDocuments: [
                        {
                            Code: ko.observable("Complete doc 1"),
                            Description: ko.observable("Complete doc 1 description"),
                            Status: ko.observable("Received"),
                            StatusDisplay: ko.observable("GOT IT!")
                        },
                        {
                            Code: ko.observable("Complete doc 2"),
                            Description: ko.observable("Complete doc 2 description"),
                            Status: ko.observable("Waived"),
                            StatusDisplay: ko.observable("You did a great job so we waive it")
                        }
                    ]
                }
            ]
        },

        awardLetterData: {
            StudentAwardLetter: {
                AwardYearCode: ko.observable("2016"),
                StudentId: ko.observable("0005667"),
                StudentName: ko.observable("Alexia Morales"),
                AwardLetterRecordId: ko.observable("45"),
                AcceptedDate: ko.observable("2016/07/08"),
                AcceptedDateDisplay: ko.observable("2016/07/08"),
                IsAnyStatusModifiable: ko.observable(),
                AreAnyAwardsPending: ko.observable(false),
                AreAnyPendingIgnoredAwardsOnHold: ko.observable(false),
                IsLetterAccepted: ko.observable()
            },
            AwardLetterHistoryItems: [
                {
                    AwardLetterRecordId: ko.observable("43"),
                    CreatedDate: ko.observable("2015/08/09")
                },
                {
                    AwardLetterRecordId: ko.observable("40"),
                    CreatedDate: ko.observable("2015/01/25")
                }
            ],
            AwardYears: [
                {
                    Code: ko.observable("2015"),
                    AreChecklistItemsAssigned: ko.observable(true)
                },
                {
                    Code: ko.observable("2016"),
                    AreChecklistItemsAssigned: ko.observable(true)
                }
            ]            
        },

        awardLetterInfoData: {
            AwardYearCode: ko.observable("2016"),
            IsAwardLetterAvailable: ko.observable(true),
            IsAwardLetterAccepted: ko.observable(false),
            CategoriesExcludedFromAwardLetter: ko.observable(["Rejected", "Denied"]),
            DoesAnyAwardRequireAction: ko.observable(false)
        },

        outsideAwardsData: [
            {
                Id: ko.observable("1"),
                StudentId: ko.observable("0004791"),
                AwardName: ko.observable("award 1"),
                AwardType: ko.observable("Scholarship"),
                AwardAmount: ko.observable(1000),
                AwardFundingSource: ko.observable("One company"),
                AwardYearCode: ko.observable("2016")
            },
            {
                Id: ko.observable("2"),
                StudentId: ko.observable("0004791"),
                AwardName: ko.observable("award 2"),
                AwardType: ko.observable("Grant"),
                AwardAmount: ko.observable(5000),
                AwardFundingSource: ko.observable("Yahoo"),
                AwardYearCode: ko.observable("2016")
            },
            {
                Id: ko.observable("16"),
                StudentId: ko.observable("0004791"),
                AwardName: ko.observable("Student loan"),
                AwardType: ko.observable("Loan"),
                AwardAmount: ko.observable(14500),
                AwardFundingSource: ko.observable("Bank"),
                AwardYearCode: ko.observable("2016")
            }
        ],

        outsideAwardsViewModelData: {
            OutsideAwards: [
                {
                    Id: ko.observable("1"),
                    StudentId: ko.observable("0004791"),
                    AwardName: ko.observable("award 1"),
                    AwardType: ko.observable("Scholarship"),
                    AwardAmount: 1000,
                    AwardFundingSource: ko.observable("One company"),
                    AwardYearCode: ko.observable("2016")
                },
                {
                    Id: ko.observable("2"),
                    StudentId: ko.observable("0004791"),
                    AwardName: ko.observable("award 2"),
                    AwardType: ko.observable("Grant"),
                    AwardAmount: 5000,
                    AwardFundingSource: ko.observable("Yahoo"),
                    AwardYearCode: ko.observable("2016")
                },
                {
                    Id: ko.observable("16"),
                    StudentId: ko.observable("0004791"),
                    AwardName: ko.observable("Student loan"),
                    AwardType: ko.observable("Loan"),
                    AwardAmount: 14500,
                    AwardFundingSource: ko.observable("Bank"),
                    AwardYearCode: ko.observable("2016")
                }
            ],
            AwardYears: [
                {
                    Code: ko.observable("2015"),
                    AreChecklistItemsAssigned: ko.observable(true)
                },
                {
                    Code: ko.observable("2016"),
                    AreChecklistItemsAssigned: ko.observable(true)
                }
            ]
        },

        academicProgressData: {
            LatestAcademicProgressEvaluation: {
                SAPStatus: {
                    PeriodStartTerm: ko.observable("2016/FA"),
                    PeriodEndTerm: ko.observable("2017/FA"),
                    PeriodStartDate: ko.observable(),
                    PeriodEndDate: ko.observable(),
                    EvaluationDate: ko.observable(),
                    AcademicProgressStatus: 
                        {
                            Category: ko.observable(),
                            Code: ko.observable("Probation"),
                            Description: ko.observable("Probation desc"),
                            Explanation: ko.observable("Probation explanation")
                        },
                    StudentAcademicProgram: ko.observable()
                },
                SAPDetailItems: [
                    {
                        Code: ko.observable("Code1"),
                        Description: ko.observable("Description1"),
                        Explanation: ko.observable("Explanation1")
                    }
                ],
                SAPAppeal: {
                    Status: ko.observable("REQ"),
                    AppealDate: ko.observable(),
                    AppealAdvisor: ko.observable()
                },
                AcademicProgressEvaluationId: ko.observable("11"),
                AcademicProgressEvaluationType: ko.observable("GEN")
            },
            SAPHistoryItems: [],
            SAPLinks: [],
            Person: {
                Id: ko.observable("0004791")
            },
            FinancialAidCounselor: {},
            IsAcademicProgressActive: ko.observable(true),
            IsProxyView: ko.observable(false),
            isSAPHistoryItemView: ko.observable(),
            showAllSAPHistoryItems: ko.observable()
        },

        studentAccountSummaryData: {
            AmountDue: ko.observable(4567),
            AmountOverdue: ko.observable(8765),
            TotalAmountDue: ko.observable(13332)
        },

        correspondenceOptionData: {
            IsCorrespondenceOptionChecked: ko.observable()
        },

        awardsViewModelData: {
            AwardLetterData: [
                {
                    AwardYearCode: ko.observable("2016"),
                    DoesAnyAwardRequireAction: ko.observable(),
                    IsAwardLetterAccepted: ko.observable(),
                    IsAwardLetterAvailable: ko.observable()
                }
            ],
            AwardPercentageCharts: [
                {
                    AwardYearCode: ko.observable("2016"),
                    PercentageChartModels: [
                        {
                            AwardCategoryType: ko.observable("Award"),
                            ChartData: [],
                            ChartOptions: [],
                            PercentageForAllOtherAwards: ko.observable(55),
                            PercentageForAwardType: ko.observable(45)
                        }
                    ]
                }
            ],
            AwardPrerequisites: [
                {
                    AreAllPrerequisitesSatisfied: ko.observable(),
                    AwardYearCode: ko.observable("2016")
                }
            ],
            AwardYears: [
                {
                    AreChecklistItemsAssigned: ko.observable(),
                    ArePrerequisitesSatisfied: ko.observable(),
                    Code: ko.observable("2016"),
                    Counselor: ko.observable(),
                    Description: ko.observable("2016-2017 Award Year"),
                    DistinctAwardPeriods: [
                        {
                            Code: ko.observable("16/FA"),
                            Description: ko.observable("16 Fall"),
                            StartDate: ko.observable()
                        },
                        {
                            Code: ko.observable("17/SP"),
                            Description: ko.observable("17 Spring"),
                            StartDate: ko.observable()
                        }
                    ]
                }
            ],
            Configuration: {
                AwardYearConfigurations: [
                    {
                        AllowAnnualAwardUpdatesOnly: ko.observable(false),
                        AreAwardingChangesAllowed: ko.observable(true),
                        AwardYearCode: ko.observable("2016"),
                        DisplayPellLifetimeEligibilityUsedPercentage: ko.observable(true),
                        IsDeclinedStatusChangeReviewRequired: ko.observable(false),
                        IsLoanAmountChangeReviewRequired: ko.observable(false),
                        IsLoanRequestEnabled: ko.observable(true),
                        ShoppingSheetConfiguration: {},
                        SuppressAverageAwardPackageDisplay: ko.observable(true),
                        SuppressMaximumLoanLimits: ko.observable(false),
                        SuppressStudentAccountSummaryDisplay: ko.observable(false)
                    }
                ],
                IsConfigurationComplete: ko.observable(true),
                MissingConfigurationMessages: []                
            },
            IsProxyView: ko.observable(false),
            isAdmin: ko.observable(false),
            IsUserSelf: ko.observable(true),
            LoanRequirements: {
                ActiveDirectLoanMpnStatus: ko.observable("Incomplete"),
                ActiveGraduatePlusMpnStatus: ko.observable("Incomplete"),
                DirectLoanEntranceInterviewStatus: ko.observable("Incomplete"),
                GraduatePlusEntranceInterviewStatus: ko.observable("Incomplete"),
                StudentId: ko.observable("0004791")
            },
            OtherLoans: [
                {
                    AwardYearCode: ko.observable("2016"),
                    StudentAwardsForYear: [
                        {
                            AwardCategoryType: ko.observable("Loan"),
                            AwardStatus: {
                                Category: ko.observable("Estimated"),
                                Code: ko.observable("E"),
                                Description: ko.observable("Estimated")
                            },
                            AwardYearId: ko.observable("2016"),
                            Code: ko.observable("Other loan"),
                            Description: ko.observable("Other loan award"),
                            Explanation: ko.observable("Pretty obvious"),
                            IsAmountModifiable: ko.observable(false),
                            IsAwardInReview: ko.observable(false),
                            IsEligible: ko.observable(false),
                            IsStatusModifiable: ko.observable(true),
                            LoanType: ko.observable(),
                            StatusDescription: ko.observable("Estimated"),
                            StudentAwardPeriods: [
                                {
                                    AwardAmount: ko.observable(2500),
                                    AwardId: ko.observable("Other loan"),
                                    AwardStatus: {
                                        Category: ko.observable("Estimated"),
                                        Code: ko.observable("E"),
                                        Description: ko.observable("Estimated")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("16/FA"),
                                    Description: ko.observable("16 Fall"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(false),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(true),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(2500)
                                }
                            ]
                        }
                    ],
                    StudentAwardsForYearTotalAmount: ko.observable(2500)
                }
            ],
            StudentAwards: [
                {
                    AwardYearCode: ko.observable("2016"),
                    StudentAwardsForYear: [
                        {
                            AwardCategoryType: ko.observable("Award"),
                            AwardStatus: {
                                Category: ko.observable("Estimated"),
                                Code: ko.observable("E"),
                                Description: ko.observable("Estimated")
                            },
                            AwardYearId: ko.observable("2016"),
                            Code: ko.observable("Woofy"),
                            Description: ko.observable("Woofy award"),
                            Explanation: ko.observable("Pretty obvious"),
                            IsAmountModifiable: ko.observable(false),
                            IsAwardInReview: ko.observable(false),
                            IsEligible: ko.observable(false),
                            IsStatusModifiable: ko.observable(true),
                            LoanType: ko.observable(),
                            StatusDescription: ko.observable("Estimated"),
                            StudentAwardPeriods: ko.observable([
                                {
                                    AwardAmount: ko.observable(500),
                                    AwardId: ko.observable("Woofy"),
                                    AwardStatus: {
                                        Category: ko.observable("Estimated"),
                                        Code: ko.observable("E"),
                                        Description: ko.observable("Estimated")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("16/FA"),
                                    Description: ko.observable("16 Fall"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(false),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(true),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(500)
                                },
                                {
                                    AwardAmount: ko.observable(500),
                                    AwardId: ko.observable("Woofy"),
                                    AwardStatus: {
                                        Category: ko.observable("Estimated"),
                                        Code: ko.observable("E"),
                                        Description: ko.observable("Estimated")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("17/SP"),
                                    Description: ko.observable("17 Spring"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(false),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(true),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(500)
                                }
                            ])
                        }
                    ],
                    StudentAwardsForYearTotalAmount: ko.observable(1000),
                    StudentWorkAwardsForYear: [
                        {
                            AwardCategoryType: ko.observable("Award"),
                            AwardStatus: {
                                Category: ko.observable("Pending"),
                                Code: ko.observable("P"),
                                Description: ko.observable("Pending")
                            },
                            AwardYearId: ko.observable("2016"),
                            Code: ko.observable("FWS"),
                            Description: ko.observable("Federal Work Study"),
                            Explanation: ko.observable("Federal Work Study explanation"),
                            IsAmountModifiable: ko.observable(false),
                            IsAwardInReview: ko.observable(false),
                            IsEligible: ko.observable(false),
                            IsStatusModifiable: ko.observable(true),
                            LoanType: ko.observable(),
                            StatusDescription: ko.observable("Pending"),
                            StudentAwardPeriods: ko.observable([
                                {
                                    AwardAmount: ko.observable(3000),
                                    AwardId: ko.observable("FWS"),
                                    AwardStatus: {
                                        Category: ko.observable("Pending"),
                                        Code: ko.observable("P"),
                                        Description: ko.observable("Pending")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("16/FA"),
                                    Description: ko.observable("16 Fall"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(false),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(true),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(3000)
                                }
                            ])
                        }
                    ],
                    StudentWorkAwardsForYearTotalAmount: ko.observable(3000),
                    totalAmount: ko.observable(),
                    suppressMaxLoanLimit: ko.observable(),
                    isAnySubLoanPending: ko.observable(false),
                    allowDeclineZeroAcceptedLoans: ko.observable(),
                    IsAccepted: ko.observable()
                }
            ],
            StudentGradPlusLoans: [
                {
                    AwardYearCode: ko.observable("2016"),
                    IsAnyAmountModifiable: ko.observable(true),
                    IsLoanCollectionInReview: ko.observable(),
                    IsStatusModifiable: ko.observable(true),
                    LoanCollectionType: ko.observable("GraduatePlusLoan"),
                    LoanPeriods: [
                        {
                            Code: ko.observable("16/FA"),
                            Description: ko.observable("16 Fall"),
                            IsActive: ko.observable(true),
                            IsStatusModifiable: ko.observable(true),
                            StartDate: ko.observable()
                        },
                        {
                            Code: ko.observable("17/SP"),
                            Description: ko.observable("17 Spring"),
                            IsActive: ko.observable(true),
                            IsStatusModifiable: ko.observable(true),
                            StartDate: ko.observable()
                        }
                    ],
                    Loans: [
                        {
                            AwardCategoryType: ko.observable("Loan"),
                            AwardStatus: {
                                Category: ko.observable("Accepted"),
                                Code: ko.observable("A"),
                                Description: ko.observable("Accepted")
                            },
                            AwardYearId: ko.observable("2016"),
                            Code: ko.observable("GPLUS"),
                            Description: ko.observable("Grad Plus Award"),
                            Explanation: ko.observable(),
                            IsAmountModifiable: ko.observable(true),
                            IsAwardInReview: ko.observable(),
                            IsStatusModifiable: ko.observable(false),
                            LoanType: ko.observable("GraduatePlusLoan"),
                            StatusDescription: ko.observable("Accepted"),
                            StudentAwardPeriods: ko.observable([
                                {
                                    AwardAmount: ko.observable(1500),
                                    AwardId: ko.observable("GPLUS"),
                                    AwardStatus: {
                                        Category: ko.observable("Accepted"),
                                        Code: ko.observable("A"),
                                        Description: ko.observable("Accepted")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("16/FA"),
                                    Description: ko.observable("16 Fall"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(true),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(false),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(1500)
                                },
                                {
                                    AwardAmount: ko.observable(1500),
                                    AwardId: ko.observable("GPLUS"),
                                    AwardStatus: {
                                        Category: ko.observable("Accepted"),
                                        Code: ko.observable("A"),
                                        Description: ko.observable("Accepted")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("17/SP"),
                                    Description: ko.observable("17 Spring"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(true),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(false),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(1500)
                                }
                            ]),
                            totalAmount: ko.observable(3000)
                        },
                        {
                            AwardCategoryType: ko.observable("Loan"),
                            AwardStatus: {
                                Category: ko.observable("Estimated")
                            },
                            AwardYearId: ko.observable("2016"),
                            Code: ko.observable("GPLUS1"),
                            Description: ko.observable("Grad Plus Award"),
                            IsAmountModifiable: ko.observable(true),
                            IsAwardInReview: ko.observable(),
                            IsStatusModifiable: ko.observable(false),
                            LoanType: ko.observable("GraduatePlusLoan"),
                            StatusDescription: ko.observable("Estimated"),
                            StudentAwardPeriods: ko.observable([
                                {
                                    AwardAmount: ko.observable(600),
                                    AwardId: ko.observable("GPLUS1"),
                                    AwardStatus: {
                                        Category: ko.observable("Estimated"),
                                        Code: ko.observable("E"),
                                        Description: ko.observable("Estimated")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("16/FA"),
                                    Description: ko.observable("16 Fall"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(true),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(false),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(600)
                                }
                            ]),
                            totalAmount: ko.observable(600)
                        }
                    ],
                    LoansTotalAmount: ko.observable(3000),
                    MaximumAmount: ko.observable(6000),
                    StatusDescription: ko.observable("Accepted"),
                    isAnySubLoanPending: ko.observable(false),
                    totalAmount: ko.observable(),
                    suppressMaxLoanLimit: ko.observable(),
                    allowDeclineZeroAcceptedLoans: ko.observable(),
                    IsAccepted: ko.observable()
                }
            ],
            StudentSubLoans: [
                {
                    AwardYearCode: ko.observable("2016"),
                    IsAnyAmountModifiable: ko.observable(true),
                    IsLoanCollectionInReview: ko.observable(),
                    IsStatusModifiable: ko.observable(true),
                    LoanCollectionType: ko.observable("SubsidizedLoan"),
                    LoanPeriods: [
                        {
                            Code: ko.observable("16/FA"),
                            Description: ko.observable("16 Fall"),
                            IsActive: ko.observable(true),
                            IsStatusModifiable: ko.observable(true),
                            StartDate: ko.observable()
                        },
                        {
                            Code: ko.observable("17/SP"),
                            Description: ko.observable("17 Spring"),
                            IsActive: ko.observable(true),
                            IsStatusModifiable: ko.observable(true),
                            StartDate: ko.observable()
                        }
                    ],
                    Loans: [
                        {
                            AwardCategoryType: ko.observable("Loan"),
                            AwardStatus: {
                                Category: ko.observable("Estimated"),
                                Code: ko.observable("E"),
                                Description: ko.observable("Estimated")
                            },
                            AwardYearId: ko.observable("2016"),
                            Code: ko.observable("SUB"),
                            Description: ko.observable("Subsidized loan"),
                            Explanation: ko.observable(),
                            IsAmountModifiable: ko.observable(true),
                            IsAwardInReview: ko.observable(),
                            IsStatusModifiable: ko.observable(false),
                            LoanType: ko.observable("SubsidizedLoan"),
                            StatusDescription: ko.observable("Estimated"),
                            StudentAwardPeriods: ko.observable([
                                {
                                    AwardAmount: ko.observable(2565),
                                    AwardId: ko.observable("SUB"),
                                    AwardStatus: {
                                        Category: ko.observable("Accepted"),
                                        Code: ko.observable("A"),
                                        Description: ko.observable("Accepted")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("16/FA"),
                                    Description: ko.observable("16 Fall"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(true),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(false),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(2565)
                                },
                                {
                                    AwardAmount: ko.observable(2565),
                                    AwardId: ko.observable("SUB"),
                                    AwardStatus: {
                                        Category: ko.observable("Estimated"),
                                        Code: ko.observable("E"),
                                        Description: ko.observable("Estimated")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("17/SP"),
                                    Description: ko.observable("17 Spring"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(true),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(false),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(2565)
                                }
                            ])
                        }
                    ],
                    LoansTotalAmount: ko.observable(5130),
                    MaximumAmount: ko.observable(10000),
                    StatusDescription: ko.observable("Estimated"),
                    isAnySubLoanPending: ko.observable(false),
                    allowDeclineZeroAcceptedLoans: ko.observable(),
                    IsAccepted: ko.observable()
                }
            ],
            StudentUnsubLoans: [
                {
                    AwardYearCode: ko.observable("2016"),
                    IsAnyAmountModifiable: ko.observable(true),
                    IsLoanCollectionInReview: ko.observable(),
                    IsStatusModifiable: ko.observable(true),
                    LoanCollectionType: ko.observable("UnsubsidizedLoan"),
                    LoanPeriods: [
                        {
                            Code: ko.observable("16/FA"),
                            Description: ko.observable("16 Fall"),
                            IsActive: ko.observable(true),
                            IsStatusModifiable: ko.observable(true),
                            StartDate: ko.observable()
                        },
                        {
                            Code: ko.observable("17/SP"),
                            Description: ko.observable("17 Spring"),
                            IsActive: ko.observable(true),
                            IsStatusModifiable: ko.observable(true),
                            StartDate: ko.observable()
                        }
                    ],
                    Loans: [
                        {
                            AwardCategoryType: ko.observable("Loan"),
                            AwardStatus: {
                                Category: ko.observable("Pending"),
                                Code: ko.observable("P"),
                                Description: ko.observable("Pending")
                            },
                            AwardYearId: ko.observable("2016"),
                            Code: ko.observable("UNSUB"),
                            Description: ko.observable("Unsubsidized loan"),
                            Explanation: ko.observable(),
                            IsAmountModifiable: ko.observable(true),
                            IsAwardInReview: ko.observable(),
                            IsStatusModifiable: ko.observable(false),
                            LoanType: ko.observable("UnsubsidizedLoan"),
                            StatusDescription: ko.observable("Pending"),
                            StudentAwardPeriods: ko.observable([
                                {
                                    AwardAmount: ko.observable(4000),
                                    AwardId: ko.observable("UNSUB"),
                                    AwardStatus: {
                                        Category: ko.observable("Accepted"),
                                        Code: ko.observable("A"),
                                        Description: ko.observable("Accepted")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("16/FA"),
                                    Description: ko.observable("16 Fall"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(true),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(false),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(4000)
                                },
                                {
                                    AwardAmount: ko.observable(1000),
                                    AwardId: ko.observable("UNSUB"),
                                    AwardStatus: {
                                        Category: ko.observable("Pending"),
                                        Code: ko.observable("P"),
                                        Description: ko.observable("Pending")
                                    },
                                    AwardYearId: ko.observable("2016"),
                                    Code: ko.observable("17/SP"),
                                    Description: ko.observable("17 Spring"),
                                    IsActive: ko.observable(true),
                                    IsAmountModifiable: ko.observable(true),
                                    IsIgnoredOnAwardLetter: ko.observable(),
                                    IsStatusModifiable: ko.observable(false),
                                    IsViewableOnAwardLetter: ko.observable(true),
                                    OriginalAwardAmount: ko.observable(1000)
                                }
                            ]),
                            totalAmount: ko.observable()
                        }
                    ],
                    LoansTotalAmount: ko.observable(5000),
                    MaximumAmount: ko.observable(10000),
                    StatusDescription: ko.observable("Pending"),
                    isAnySubLoanPending: ko.observable(false),
                    allowDeclineZeroAcceptedLoans: ko.observable(),
                    IsAccepted: ko.observable()
                }
            ],
            yearConfiguration: ko.observable({
                AllowAnnualAwardUpdatesOnly: ko.observable(false),
                AreAwardingChangesAllowed: ko.observable(true),
                AwardYearCode: ko.observable("2016"),
                DisplayPellLifetimeEligibilityUsedPercentage: ko.observable(true),
                IsDeclinedStatusChangeReviewRequired: ko.observable(false),
                IsLoanAmountChangeReviewRequired: ko.observable(false),
                IsLoanRequestEnabled: ko.observable(true),
                ShoppingSheetConfiguration: {},
                SuppressAverageAwardPackageDisplay: ko.observable(true),
                SuppressMaximumLoanLimits: ko.observable(false),
                SuppressStudentAccountSummaryDisplay: ko.observable(false),
                AllowDeclineZeroOfAcceptedLoans: ko.observable()
            }),
            isSubLoanPending: ko.observable(false)
            
        }
    }
    
}