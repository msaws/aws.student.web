﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates
describe("award.letter.mapping.js", function () {

    var awardLetterData,
        awardLetterViewModelInstance;

    beforeAll(function () {
        awardLetterData = testData().awardLetterData;
        faAwardLetterActionRequiredMobileMessage = "Action Required Mobile!";
        faAwardLetterAwardsActionRequired = "Action Required!";
        faAwardLetterAwardsOnHoldMessage = "Awards on hold message";
        faAwardLetterPendingAwardsMessage = "Pending awards message";
        faAwardLetterGreeting = "Greeting {0}!";
        faAwardLetterAcceptedWithDateMessage = "Letter Accepted on {0}!";
        getAwardLetterReportActionUrl = "GetReportUrl";
        awardLetterAdminActionUrl = "adminUrl";
    });

    beforeEach(function () {
        awardLetterViewModelInstance = new awardLetterViewModel();
        ko.mapping.fromJS(awardLetterData, awardLetterViewModelMapping, awardLetterViewModelInstance);
    });

    describe("studentAwardLetterModelTests", function () {
        var awardLetterModel,
            options;
        
        beforeEach(function () {
            awardLetterViewModelInstance.isMobile = ko.observable(false);
            awardLetterViewModelInstance.isAwardPackageChecklistItemRemoved = ko.observable(false);
            options = {
                data: awardLetterData.StudentAwardLetter,
                parent: awardLetterViewModelInstance
            };
            awardLetterModel = new studentAwardLetterModel(options);
            parent = awardLetterViewModelInstance;
        });

        it(":letterWithPendingAwardsMessage is undefined if none of the conditions met", function () {            
            expect(awardLetterModel.letterWithPendingAwardsMessage()).toBeUndefined();
        });

        it(":letterWithPendingAwardsMessage returns expected message for action required message scenario", function () {
            awardLetterModel.AreAnyAwardsPending(true);
            awardLetterModel.AcceptedDate("");
            parent.isAwardPackageChecklistItemRemoved(false);
            expect(awardLetterModel.letterWithPendingAwardsMessage()).toEqual("Action Required!");
        });

        it(":letterWithPendingAwardsMessage returns expected message for action required mobile message scenario", function () {
            awardLetterModel.AreAnyAwardsPending(true);
            awardLetterModel.AcceptedDate("");
            parent.isMobile(true);
            expect(awardLetterModel.letterWithPendingAwardsMessage()).toEqual("Action Required Mobile!");
        });

        it(":letterWithPendingAwardsMessage returns expected message for pending awards with accepted date scenario when there are pending awards", function () {
            awardLetterModel.AreAnyAwardsPending(true);            
            awardLetterModel.AcceptedDate("foo");
            expect(awardLetterModel.letterWithPendingAwardsMessage()).toEqual("Pending awards message");
        });

        it(":showLetterWithPendingAwardsMessage is falsy if there are no pending awards", function () {
            awardLetterModel.AreAnyAwardsPending(false);
            expect(awardLetterModel.showLetterWithPendingAwardsMessage()).toBeFalsy();
        });

        it(":showLetterWithPendingAwardsMessage is true if there are pending awards", function () {
            awardLetterModel.AreAnyAwardsPending(true);            
            expect(awardLetterModel.showLetterWithPendingAwardsMessage()).toBeTruthy();
        });

        it(":showAwardLetterNotice is false if there are pending awards", function () {
            awardLetterModel.AreAnyAwardsPending(true);
            expect(awardLetterModel.showAwardLetterNotice()).toBeFalsy();
        });

        it(":showAwardLetterNotice is false if there is an accepted date", function () {
            awardLetterModel.AcceptedDate("foo");
            expect(awardLetterModel.showAwardLetterNotice()).toBeFalsy();
        });

        it(":showAwardLetterNotice is true when all conditions are met", function () {
            awardLetterModel.AreAnyAwardsPending(false);
            awardLetterModel.AcceptedDate("");
            expect(awardLetterModel.showAwardLetterNotice()).toBeTruthy();
        });

        it(":letterNoticeStyle equals expected when award letter is pending with pending awards and award package item is not removed", function () {
            awardLetterModel.AreAnyAwardsPending(true);
            parent.isAwardPackageChecklistItemRemoved(false);
            parent.isMobile(false);            
            expect(awardLetterModel.letterNoticeStyle()).toEqual('letter-pending');
        });

        it(":letterNoticeStyle equals expected when award letter is pending with pending awards and mobile view", function () {
            awardLetterModel.AreAnyAwardsPending(true);
            parent.isAwardPackageChecklistItemRemoved(true);
            parent.isMobile(true);
            expect(awardLetterModel.letterNoticeStyle()).toEqual('letter-pending');
        });

        it(":letterNoticeStyle returns letter-accepted when there is an accepted date and no pending awards", function () {
            awardLetterModel.AreAnyAwardsPending(false);
            awardLetterModel.AcceptedDate("foo");
            expect(awardLetterModel.letterNoticeStyle()).toEqual('letter-accepted');
        });

        it(":letterNoticeStyle returns letter-accepted when award package checklist item removed", function () {
            parent.isAwardPackageChecklistItemRemoved(true);
            awardLetterModel.AcceptedDate("foo");
            expect(awardLetterModel.letterNoticeStyle()).toEqual('letter-accepted');
        });

        it(":letterNoticeStyle returns letter-no-message if award package checklist item is removed, there is no accepted date, there are pending awards and it is not mobile view", function () {
            parent.isAwardPackageChecklistItemRemoved(true);
            awardLetterModel.AcceptedDate("");
            awardLetterModel.AreAnyAwardsPending(true);
            parent.isMobile(false);
            expect(awardLetterModel.letterNoticeStyle()).toEqual('letter-no-message');
        });

        it(":awardLetterGreeting returns expected value", function () {
            var expected = "Greeting Alexia Morales!";
            expect(awardLetterModel.awardLetterGreeting()).toEqual(expected);
        });

        it(":letterIsAcceptedMessage returns expected value", function () {
            var expected = "Letter Accepted on 2016/07/08!";
            expect(awardLetterModel.letterIsAcceptedMessage()).toEqual(expected);
        });
    });

    describe("awardLetterHistoryItemModelTests", function () {

        var historyItemModel,
            historyItemTestData;

        beforeEach(function () {
            historyItemTestData = awardLetterData.AwardLetterHistoryItems[0];
            historyItemModel = new awardLetterHistoryItemModel(historyItemTestData);
        });

        it(":isItemVisible initializes to true", function () {
            expect(historyItemModel.isItemVisible()).toBeTruthy();
        });        
    });
});