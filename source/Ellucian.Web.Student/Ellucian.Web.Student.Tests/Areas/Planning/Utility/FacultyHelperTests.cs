﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Utility
{
    [TestClass]
    public class FacultyHelperTests
    {
        /// <summary>
        /// Unit tests for the AddCourse method of the DegreePlanHelper
        /// </summary>
        [TestClass]
        public class FacultyHelperBuildStudentWaiversTests
        {
            IEnumerable<StudentWaiver> waiverDtos;
            IEnumerable<StudentWaiverReason> waiverReasons;
            IEnumerable<Ellucian.Colleague.Dtos.Planning.PlanningStudent> studentDtos;
            IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> facultyDtos;
            DateTimeOffset pastTime = DateTime.Now.AddDays(-5);
            
            /// <summary>
            /// Initialize any common objects used by the individual test methods
            /// </summary>
            [TestInitialize]
            public void Initialize()
            {
                waiverDtos = new List<StudentWaiver>() { 
                    new StudentWaiver() { 
                        Id = "1", 
                        AuthorizedBy = "22222", 
                        ChangedBy = "11111", 
                        DateTimeChanged = pastTime, 
                        ReasonCode = "RC", 
                        SectionId = "SectionId", 
                        StudentId = "StudentId", 
                        RequisiteWaivers = new List<RequisiteWaiver>(), 
                        Comment = "Comment",
                        IsRevoked = false
                    },
                    new StudentWaiver() { 
                        Id = "2",
                        StudentId = "Unknown"
                    },
                    new StudentWaiver() { 
                        Id = "3",
                        StudentId = "StudentId2",
                        AuthorizedBy = "Alpha",
                        IsRevoked = true
                    }
                };

                waiverReasons = new List<StudentWaiverReason>() {
                    new StudentWaiverReason() {Code = "RC", Description = "Waiver Reason"}
                };

                studentDtos = new List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>() {
                    new Ellucian.Colleague.Dtos.Planning.PlanningStudent() {
                       Id = "StudentId",
                       LastName = "StudentLastName",
                       FirstName = "StudentFirstName",
                       PreferredEmailAddress = "StudentEmailAddress"
                    }
                };

                facultyDtos = new List<Ellucian.Colleague.Dtos.Student.Faculty>() {
                    new Ellucian.Colleague.Dtos.Student.Faculty() {
                       Id = "11111",
                       LastName = "ChangedLastName",
                       FirstName = "ChangedFirstName"
                    },
                    new Ellucian.Colleague.Dtos.Student.Faculty() {
                       Id = "22222",
                       LastName = "AuthorizedLastName",
                       FirstName = "AuthorizedFirstName"
                    }
                };
            }

            /// <summary>
            /// Unit test creating a waiver model from DTO data
            /// </summary>
            [TestMethod]
            public void BuildStudentWaivers_TestPropertiesWithData()
            {
                List<StudentWaiverModel> models = FacultyHelper.BuildStudentWaivers(waiverDtos, waiverReasons, studentDtos, facultyDtos);

                StudentWaiverModel model = models.Where(m => m.StudentId == "StudentId").FirstOrDefault();
                Assert.AreEqual("StudentLastName, StudentFirstName", model.DisplayNameLfm);
                Assert.AreEqual("Waiver Reason", model.Reason);
                Assert.AreEqual("StudentId", model.StudentId);
                Assert.AreEqual("AuthorizedLastName, A", model.AuthorizedBy);
                Assert.AreEqual(pastTime.LocalDateTime.ToShortDateString() + " " + pastTime.LocalDateTime.ToLongTimeString(), model.UpdatedOn);
                Assert.AreEqual("Comment", model.FreeformExplanation);
                Assert.IsFalse(model.IsRevoked);
            }

            /// <summary>
            /// Unit test creating a waiver model from DTO data
            /// </summary>
            [TestMethod]
            public void BuildStudentWaivers_TestPropertiesWithNoData()
            {
                List<StudentWaiverModel> models = FacultyHelper.BuildStudentWaivers(waiverDtos, waiverReasons, studentDtos, facultyDtos);

                StudentWaiverModel model = models.Where(m => m.StudentId == "Unknown").FirstOrDefault();
                // Can't test name because unknown value is in resx file.
                Assert.AreEqual(string.Empty, model.Reason);
                Assert.AreEqual("Unknown", model.StudentId);
                Assert.AreEqual(string.Empty, model.AuthorizedBy);
                Assert.AreEqual(string.Empty, model.FreeformExplanation);
                Assert.IsFalse(model.IsRevoked);
            }

            /// <summary>
            /// Unit test creating a waiver model from DTO data
            /// </summary>
            [TestMethod]
            public void BuildStudentWaivers_WhenAuthorizedIsAlpha()
            {
                List<StudentWaiverModel> models = FacultyHelper.BuildStudentWaivers(waiverDtos, waiverReasons, studentDtos, facultyDtos);

                StudentWaiverModel model = models.Where(m => m.StudentId == "StudentId2").FirstOrDefault();
                Assert.AreEqual("StudentId2", model.StudentId);
                Assert.AreEqual("Alpha", model.AuthorizedBy);
                Assert.IsTrue(model.IsRevoked);
            }
        }
    }
}
