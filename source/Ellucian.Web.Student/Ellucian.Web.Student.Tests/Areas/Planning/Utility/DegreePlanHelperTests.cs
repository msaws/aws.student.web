﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Web.Student.Areas.Planning.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Utility
{
    /// <summary>
    /// This class contains unit tests for the DegreePlanHelper class.
    /// </summary>
    [TestClass]
    public class DegreePlanHelperTests
    {
        /// <summary>
        /// Unit tests for the DegreePlanHelper constructor
        /// </summary>
        [TestClass]
        public class DegreePlanHelperConstructorTests
        {
            [TestMethod]
            public void DegreePlanHelperConstructorSuccess()
            {
                // Nothing to test
            }
        }

        /// <summary>
        /// Unit tests for the AddCourse method of the DegreePlanHelper
        /// </summary>
        [TestClass]
        public class DegreePlanHelperAddCourseTests
        {
            DegreePlanHelper degreePlanHelper;

            DegreePlan4 degreePlan;

            /// <summary>
            /// Initialize any common objects used by the individual test methods
            /// </summary>
            [TestInitialize]
            public void Initialize()
            {
                degreePlanHelper = new DegreePlanHelper();
                degreePlanHelper.UserId = "User";

                degreePlan = new DegreePlan4() { Id = 1, PersonId = "person", Version = 1 };
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanHelper = null;
                degreePlan = null;
            }

            /// <summary>
            /// Unit test the addition of a course to an existing planned term
            /// </summary>
            [TestMethod]
            public void DegreePlanHelperAddCourseToExistingTerm()
            {
                // Add the term to the degree plan
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = "term" });

                var termId = "term";
                var courseId = "course";
                var gradingType = GradingType.PassFail;
                var sectionId = "section";
                var credits = 3;

                var success = degreePlanHelper.AddCourse(degreePlan, termId, courseId, gradingType, sectionId, credits);

                // Did AddCourse think is was successful?
                Assert.IsTrue(success);

                // Is the new course in the new term in the plan?
                Assert.IsTrue(degreePlan.Terms.First(t => t.TermId == termId).PlannedCourses.Any(pc => pc.CourseId == courseId && pc.SectionId == sectionId && pc.Credits == credits && pc.AddedBy == degreePlanHelper.UserId));
            }

            /// <summary>
            /// Unit test the addition of the course to a new planned term (term not already on the plan)
            /// </summary>
            [TestMethod]
            public void DegreePlanHelperAddCourseToNewTerm()
            {
                var termId = "term";
                var courseId = "course";
                var gradingType = GradingType.PassFail;
                var sectionId = "section";
                var credits = 3;

                var success = degreePlanHelper.AddCourse(degreePlan, termId, courseId, gradingType, sectionId, credits);

                // Did AddCourse think is was successful?
                Assert.IsTrue(success);

                // Is the new term in the degree plan?
                Assert.IsTrue(degreePlan.Terms.Any(t => t.TermId == termId));

                // Is the new course in the new term in the plan?  
                // AND is it unprotected.
                Assert.IsTrue(degreePlan.Terms.First(t => t.TermId == termId).PlannedCourses.Any(pc => pc.CourseId == courseId && pc.SectionId == sectionId && pc.Credits == credits && pc.AddedBy == degreePlanHelper.UserId && pc.IsProtected == false));
            }

            /// <summary>
            /// Person other than student add a course to a protected plan - plan with a protected nonterm planned course
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_AdvisorAddsCourseToProtectedPlan()
            {
                // Person other than student adds a course to a protected plan.
                // From initialization userId = "user", degreePlan.PersonId = "person".

                // Add a protected course to the degree plan
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.NonTermPlannedCourses.Add(new PlannedCourse4() { CourseId = "course1", SectionId = "nt", IsProtected = true });

                var termId = "term";
                var courseId = "course";
                var gradingType = GradingType.PassFail;
                var sectionId = "section";
                var credits = 3;

                var success = degreePlanHelper.AddCourse(degreePlan, termId, courseId, gradingType, sectionId, credits);
                Assert.IsTrue(success);

                // Is the newly added course is also protected?
                var degreePlanTerm = degreePlan.Terms.Where(t => t.TermId == termId).FirstOrDefault();
                var newPc = degreePlanTerm.PlannedCourses.FirstOrDefault();
                Assert.IsTrue(!newPc.IsProtected);
            }

            [TestMethod]
            public void DegreePlanHelper_StudentAddCourseToProtectedPlan()
            {
                // Student adds a course to a protected plan.
                // From initialization userId = "user"

                // Set up a protected plan for the student
                var degreePlan2 = new DegreePlan4() { Id = 2, PersonId = "User", Version = 1 };
                degreePlan2.Terms = new List<DegreePlanTerm4>();
                var pc = new PlannedCourse4() { CourseId = "course1", TermId = "term", IsProtected = true, Credits = 3, GradingType = GradingType.Graded };
                degreePlan2.Terms.Add(new DegreePlanTerm4() { TermId = "term", PlannedCourses = new List<PlannedCourse4>() { pc } });

                var termId = "term";
                var courseId = "course2";
                var gradingType = GradingType.PassFail;
                var sectionId = "";
                var credits = 3;

                var success = degreePlanHelper.AddCourse(degreePlan2, termId, courseId, gradingType, sectionId, credits);
                Assert.IsTrue(success);

                // Newly added course should not be protected.
                var degreePlanTerm = degreePlan2.Terms.Where(t => t.TermId == termId).FirstOrDefault();
                var newPc = degreePlanTerm.PlannedCourses.Where(p => p.CourseId == "course2").FirstOrDefault();
                Assert.IsFalse(newPc.IsProtected);
            }
        }

        /// <summary>
        /// Unit tests for the RemoveCourse method of the DegreePlanHelper
        /// </summary>
        [TestClass]
        public class DegreePlanHelperRemoveCourseTests
        {
            DegreePlanHelper degreePlanHelper;
            DegreePlan4 degreePlan;
            private string termId = "term";
            private string courseId1 = "course1";
            private string courseId2 = "course2";
            private GradingType gradingType = GradingType.Graded;
            private string sectionId1 = "section1";
            private string sectionId2 = "section2";
            private decimal credits = 3;

            /// <summary>
            /// Initialize any common objects used by the individual test methods
            /// </summary>
            [TestInitialize]
            public void Initialize()
            {
                degreePlanHelper = new DegreePlanHelper();
                degreePlanHelper.UserId = "User";

                degreePlan = new DegreePlan4() { Id = 1, PersonId = "person", Version = 1 };
                // Add a term  to the degree plan. 
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = termId, PlannedCourses = new List<PlannedCourse4>() });

                // Add course2 with no section
                bool added = degreePlanHelper.AddCourse(degreePlan, termId, courseId2, gradingType, null, credits);
                // Add course1 with a section
                added = degreePlanHelper.AddCourse(degreePlan, termId, courseId1, gradingType, sectionId1, credits);
                // Add course1 again with another section
                added = degreePlanHelper.AddCourse(degreePlan, termId, courseId1, gradingType, sectionId2, credits);
                // Add course1 with no section
                added = degreePlanHelper.AddCourse(degreePlan, termId, courseId1, gradingType, null, credits);
                // Add a nonterm course and section
                added = degreePlanHelper.AddCourse(degreePlan, null, courseId1, gradingType, sectionId1, credits);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanHelper = null;
                degreePlan = null;
            }

            /// <summary>
            /// Unit test for removing a planned course with a courseID but no sectionId
            /// </summary>
            [TestMethod]
            public void DegreePlanHelperRemoveCourse_EmptySectionId()
            {
                var success = degreePlanHelper.RemoveCourse(degreePlan, termId, courseId1, "");

                // Did RemoveCourse think is was successful?
                Assert.IsTrue(success);

                // Is the removed item still on the plan? Did it remove the correct one?
                Assert.IsFalse(degreePlan.Terms.First(t => t.TermId == termId).PlannedCourses.Any(pc => pc.CourseId == courseId1 && pc.SectionId == null && pc.Credits == credits && pc.AddedBy == degreePlanHelper.UserId));
            }


            /// <summary>
            /// Unit test for removing a planned course with a courseID but null sectionId
            /// </summary>
            [TestMethod]
            public void DegreePlanHelperRemoveCourse_NullSectionId()
            {
                var success = degreePlanHelper.RemoveCourse(degreePlan, termId, courseId1, null);

                // Did RemoveCourse think is was successful?
                Assert.IsTrue(success);

                // Is the removed item still on the plan? Did it remove the correct one?
                Assert.IsFalse(degreePlan.Terms.First(t => t.TermId == termId).PlannedCourses.Any(pc => pc.CourseId == courseId1 && pc.SectionId == null && pc.Credits == credits && pc.AddedBy == degreePlanHelper.UserId));
            }

            /// <summary>
            /// Unit test the removal of a planned course with a course and a section and make sure it removes the right one.
            /// </summary>
            [TestMethod]
            public void DegreePlanHelperRemoveCourseWithSectionId()
            {
                var success = degreePlanHelper.RemoveCourse(degreePlan, termId, courseId1, sectionId2);

                // Did RemoveCourse think is was successful?
                Assert.IsTrue(success);

                // Is the removed course still on the plan? Make sure the one with section1 is still there and the one with section2 is gone.
                Assert.IsTrue(degreePlan.Terms.First(t => t.TermId == termId).PlannedCourses.Any(pc => pc.CourseId == courseId1 && pc.SectionId == sectionId1 && pc.Credits == credits && pc.AddedBy == degreePlanHelper.UserId));
                Assert.IsFalse(degreePlan.Terms.First(t => t.TermId == termId).PlannedCourses.Any(pc => pc.CourseId == courseId1 && pc.SectionId == sectionId2 && pc.Credits == credits && pc.AddedBy == degreePlanHelper.UserId));
            }

            /// <summary>
            /// Unit test the removal of a nonterm planned course 
            /// </summary>
            [TestMethod]
            public void DegreePlanHelperRemoveNonTermCourseWithSectionId()
            {
                var success = degreePlanHelper.RemoveCourse(degreePlan, null, courseId1, sectionId1);

                // Did RemoveCourse think is was successful?
                Assert.IsTrue(success);

                // Is the removed course still on the plan? 
                Assert.IsFalse(degreePlan.NonTermPlannedCourses.Any(pc => pc.CourseId == courseId1 && pc.SectionId == sectionId1));
            }
        }

        /// <summary>
        /// Unit tests for the DegreePlanHelper ClearPlannedCourses
        /// </summary>
        [TestClass]
        public class DegreePlanHelperClearPlannedCoursesTests
        {
            DegreePlanHelper degreePlanHelper;

            DegreePlan4 degreePlan;

            PlannedCourse4 term1plannedCourse1;
            PlannedCourse4 term1plannedCourse2;
            PlannedCourse4 term2plannedCourse1;
            string term1;
            string term2;

            /// <summary>
            /// Initialize any common objects used by the individual test methods
            /// </summary>
            [TestInitialize]
            public void Initialize()
            {
                degreePlanHelper = new DegreePlanHelper();
                degreePlanHelper.UserId = "User";

                degreePlan = new DegreePlan4() { Id = 1, PersonId = "person", Version = 1 };

                term1plannedCourse1 = new PlannedCourse4() { CourseId = "course1", Credits = 3, IsProtected = false, TermId = "term1" };
                term1plannedCourse2 = new PlannedCourse4() { CourseId = "course2", Credits = 3, IsProtected = true, TermId = "term1" };
                term2plannedCourse1 = new PlannedCourse4() { CourseId = "course3", Credits = 3, IsProtected = false, TermId = "term2" };

                term1 = "term1";
                term2 = "term2";
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanHelper = null;
                degreePlan = null;
            }

            /// <summary>
            /// Unit test the clearing an empty plan
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ClearPlannedCourses_NullPlan()
            {
                var success = degreePlanHelper.ClearPlannedCourses(null, new List<string>() { "termId" });
                Assert.IsFalse(success);
            }

            /// <summary>
            /// Unit test the clearing an empty plan
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ClearPlannedCourses_NullTerms()
            {
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = "term" });
                var success = degreePlanHelper.ClearPlannedCourses(degreePlan, null);
                Assert.IsFalse(success);
            }

            /// <summary>
            /// Unit test the clearing a plan where no terms are specified
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ClearPlannedCourses_NoTermsSpecified()
            {
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1 });
                var success = degreePlanHelper.ClearPlannedCourses(degreePlan, new List<string>());
                Assert.IsFalse(success);
            }

            /// <summary>
            /// Unit test the clearing a plan where a term not on plan is specified
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ClearPlannedCourses_TermNotOnPlan()
            {
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1 });
                var success = degreePlanHelper.ClearPlannedCourses(degreePlan, new List<string>() { "junk" });
                Assert.IsFalse(success);
            }

            /// <summary>
            /// Unit test the clearing all terms on a plan 
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ClearPlannedCourses_AllTermsOnPlan()
            {
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { term1plannedCourse1, term1plannedCourse2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { term2plannedCourse1 } });
                var success = degreePlanHelper.ClearPlannedCourses(degreePlan, new List<string>() { "term1", "term2" });
                Assert.IsTrue(success);

                // And are the terms cleared?
                var degreePlanTerm1 = degreePlan.Terms.Where(t => t.TermId == term1).FirstOrDefault();
                var degreePlanTerm2 = degreePlan.Terms.Where(t => t.TermId == term2).FirstOrDefault();
                Assert.IsTrue(degreePlanTerm1.PlannedCourses.Count == 0);
                Assert.IsTrue(degreePlanTerm2.PlannedCourses.Count == 0);
            }

            /// <summary>
            /// Unit test the clearing all terms on a plan 
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ClearPlannedCourses_ClearOneTermOnPlan()
            {
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { term1plannedCourse1, term1plannedCourse2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { term2plannedCourse1 } });
                var success = degreePlanHelper.ClearPlannedCourses(degreePlan, new List<string>() { "term1" });
                Assert.IsTrue(success);

                // And is the right term cleared?
                var degreePlanTerm1 = degreePlan.Terms.Where(t => t.TermId == term1).FirstOrDefault();
                var degreePlanTerm2 = degreePlan.Terms.Where(t => t.TermId == term2).FirstOrDefault();
                Assert.IsTrue(degreePlanTerm1.PlannedCourses.Count == 0);
                Assert.IsTrue(degreePlanTerm2.PlannedCourses.Count == 1);
            }
        }

        /// <summary>
        /// Unit tests for the DegreePlanHelper UpdatePlanProtection
        /// </summary>
        [TestClass]
        public class DegreePlanHelperUpatePlanProtectionTests
        {
            DegreePlanHelper degreePlanHelper;

            DegreePlan4 degreePlan;

            PlannedCourse4 term1plannedCourse1;
            PlannedCourse4 term1plannedCourse2;
            PlannedCourse4 term2plannedCourse1;
            PlannedCourse4 nontermPlannedCourse;
            string term1;
            string term2;

            /// <summary>
            /// Initialize any common objects used by the individual test methods
            /// </summary>
            [TestInitialize]
            public void Initialize()
            {
                degreePlanHelper = new DegreePlanHelper();
                degreePlanHelper.UserId = "User";

                degreePlan = new DegreePlan4() { Id = 1, PersonId = "person", Version = 1 };

                term1plannedCourse1 = new PlannedCourse4() { CourseId = "course1", Credits = 3, IsProtected = false, TermId = "term1" };
                term1plannedCourse2 = new PlannedCourse4() { CourseId = "course2", Credits = 3, IsProtected = true, TermId = "term1" };
                term2plannedCourse1 = new PlannedCourse4() { CourseId = "course3", Credits = 3, IsProtected = false, TermId = "term2" };
                nontermPlannedCourse = new PlannedCourse4() { CourseId = "course3", SectionId = "section1", Credits = 3, IsProtected = false };
                term1 = "term1";
                term2 = "term2";
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanHelper = null;
                degreePlan = null;
            }

            /// <summary>
            /// Unit test the protecting a null plan
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ProtectPlan_NullPlan()
            {
                var success = degreePlanHelper.UpdatePlanProtection(null, true);
                Assert.IsFalse(success);
            }

            /// <summary>
            /// Unit test the protecting a plan with no nonterm planned courses and no terms.
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ProtectPlan_EmptyPlan()
            {
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Terms = new List<DegreePlanTerm4>();
                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, true);
                Assert.IsFalse(success);
            }

            /// <summary>
            /// Unit test the protecting a plan 
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_ProtectPlan_Success()
            {
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { term1plannedCourse1, term1plannedCourse2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { term2plannedCourse1 } });
                degreePlan.NonTermPlannedCourses.Add(nontermPlannedCourse);

                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, true);
                Assert.IsTrue(success);

                // Make sure the planned courses are protected.
                foreach (var term in degreePlan.Terms)
                {
                    foreach (var pc in term.PlannedCourses)
                    {
                        Assert.IsTrue(pc.IsProtected);
                    }
                }

                // Make sure the nonterm planned course was also marked protected
                foreach (var ntc in degreePlan.NonTermPlannedCourses)
                {
                    Assert.IsTrue(ntc.IsProtected);
                }
            }

            /// <summary>
            /// Unit test the marking a plan unprotected
            /// </summary>
            [TestMethod]
            public void DegreePlanHelper_UnprotectPlan_Success()
            {
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { term1plannedCourse1, term1plannedCourse2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { term2plannedCourse1 } });

                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, false);
                Assert.IsTrue(success);

                // Make sure the planned courses are protected.
                foreach (var term in degreePlan.Terms)
                {
                    foreach (var pc in term.PlannedCourses)
                    {
                        Assert.IsFalse(pc.IsProtected);
                    }
                }
            }

            [TestMethod]
            public void DegreePlanHelper_ProtectSpecificCourse_Success()
            {
                var course1 = new PlannedCourse4() { CourseId = "course1", Credits = 3, IsProtected = false, TermId = "term1" };
                var course2 = new PlannedCourse4() { CourseId = "course2", Credits = 3, IsProtected = false, TermId = "term1" };
                var course3 = new PlannedCourse4() { CourseId = "course3", Credits = 3, IsProtected = false, TermId = "term2" };
                var nontermPlannedCourse1 = new PlannedCourse4() { CourseId = "course4", SectionId = "section1", Credits = 3, IsProtected = false };

                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { course1, course2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { course3 } });
                degreePlan.NonTermPlannedCourses.Add(nontermPlannedCourse1);

                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, true, new List<string>() { "course3" });
                Assert.IsTrue(success);

                // Make sure only the specific planned course is protected.
                foreach (var term in degreePlan.Terms)
                {
                    foreach (var pc in term.PlannedCourses)
                    {
                        if (pc.CourseId == "course3")
                        {
                            Assert.IsTrue(pc.IsProtected);
                        }
                        else
                        {
                            Assert.IsFalse(pc.IsProtected);
                        }
                    }
                }

                // Make sure the nonterm planned course was not also marked protected
                foreach (var ntc in degreePlan.NonTermPlannedCourses)
                {
                    Assert.IsFalse(ntc.IsProtected);
                }
            }

            [TestMethod]
            public void DegreePlanHelper_ProtectSpecificNonTermCourse_Success()
            {
                var course1 = new PlannedCourse4() { CourseId = "course1", Credits = 3, IsProtected = false, TermId = "term1" };
                var course2 = new PlannedCourse4() { CourseId = "course2", Credits = 3, IsProtected = false, TermId = "term1" };
                var course3 = new PlannedCourse4() { CourseId = "course3", Credits = 3, IsProtected = false, TermId = "term2" };
                var nontermPlannedCourse1 = new PlannedCourse4() { CourseId = "course4", SectionId = "section1", Credits = 3, IsProtected = false };

                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { course1, course2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { course3 } });
                degreePlan.NonTermPlannedCourses.Add(nontermPlannedCourse1);

                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, true, new List<string>() { "course4" });
                Assert.IsTrue(success);

                // Make sure only the specific planned course is protected.
                foreach (var term in degreePlan.Terms)
                {
                    foreach (var pc in term.PlannedCourses)
                    {
                        Assert.IsFalse(pc.IsProtected);
                    }
                }

                // Make sure the nonterm planned course was  marked protected
                foreach (var ntc in degreePlan.NonTermPlannedCourses)
                {
                    Assert.IsTrue(ntc.IsProtected);
                }
            }

            [TestMethod]
            public void DegreePlanHelper_ProtectSpecificCourses_Success()
            {
                var course1 = new PlannedCourse4() { CourseId = "course1", Credits = 3, IsProtected = false, TermId = "term1" };
                var course2 = new PlannedCourse4() { CourseId = "course2", Credits = 3, IsProtected = false, TermId = "term1" };
                var course3 = new PlannedCourse4() { CourseId = "course3", Credits = 3, IsProtected = false, TermId = "term2" };
                var nontermPlannedCourse1 = new PlannedCourse4() { CourseId = "course4", SectionId = "section1", Credits = 3, IsProtected = false };

                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { course1, course2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { course3 } });
                degreePlan.NonTermPlannedCourses.Add(nontermPlannedCourse1);

                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, true, new List<string>() { "course2", "course4" });
                Assert.IsTrue(success);

                // Make sure only the specific planned course is protected.
                foreach (var term in degreePlan.Terms)
                {
                    foreach (var pc in term.PlannedCourses)
                    {
                        if (pc.CourseId == "course2")
                        {
                            Assert.IsTrue(pc.IsProtected);
                        }
                        else
                        {
                            Assert.IsFalse(pc.IsProtected);
                        }
                        
                    }
                }

                // Make sure the nonterm planned course was  marked protected
                foreach (var ntc in degreePlan.NonTermPlannedCourses)
                {
                    Assert.IsTrue(ntc.IsProtected);
                }
            }

            [TestMethod]
            public void DegreePlanHelper_ProtectSpecificCourses_CoursesNotFound()
            {
                var course1 = new PlannedCourse4() { CourseId = "course1", Credits = 3, IsProtected = false, TermId = "term1" };
                var course2 = new PlannedCourse4() { CourseId = "course2", Credits = 3, IsProtected = false, TermId = "term1" };
                var course3 = new PlannedCourse4() { CourseId = "course3", Credits = 3, IsProtected = false, TermId = "term2" };
                var nontermPlannedCourse1 = new PlannedCourse4() { CourseId = "course4", SectionId = "section1", Credits = 3, IsProtected = false };

                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { course1, course2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { course3 } });
                degreePlan.NonTermPlannedCourses.Add(nontermPlannedCourse1);

                // None of the planned courses have these Ids so there is nothing to protect. We expect a success false.
                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, true, new List<string>() { "yyy", "xxx" });
                Assert.IsFalse(success);
            }

            [TestMethod]
            public void DegreePlanHelper_UnprotectSpecificCourses_NoChange_AlreadyUnprotected()
            {
                var course1 = new PlannedCourse4() { CourseId = "course1", Credits = 3, IsProtected = false, TermId = "term1" };
                var course2 = new PlannedCourse4() { CourseId = "course2", Credits = 3, IsProtected = false, TermId = "term1" };
                var course3 = new PlannedCourse4() { CourseId = "course3", Credits = 3, IsProtected = false, TermId = "term2" };
                var nontermPlannedCourse1 = new PlannedCourse4() { CourseId = "course4", SectionId = "section1", Credits = 3, IsProtected = false };

                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term1, PlannedCourses = new List<PlannedCourse4>() { course1, course2 } });
                degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = term2, PlannedCourses = new List<PlannedCourse4>() { course3 } });
                degreePlan.NonTermPlannedCourses.Add(nontermPlannedCourse1);

                // Since the courses are already not protected, we expect a false success flag.
                var success = degreePlanHelper.UpdatePlanProtection(degreePlan, false, new List<string>() { "course2", "course4" });
                Assert.IsFalse(success);
            }
        }


    }
}
