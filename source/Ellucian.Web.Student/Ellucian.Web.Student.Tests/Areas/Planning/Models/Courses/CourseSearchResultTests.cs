﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.Planning.Models.Requirements;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Areas.Planning.Models.Advising;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using System;
using Ellucian.Web.Student.Models.Courses;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Courses
{
    [TestClass]
    public class CourseSearchResultConstructorTests
    {
        CoursePage2 coursePage = null;
        IEnumerable<Subject> subjects = null;
        IEnumerable<AcademicLevel> academicLevels = null;
        IEnumerable<CourseLevel> courseLevels = null;
        IEnumerable<Location> locations = null;
        IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty = null;
        IEnumerable<CourseType> courseTypes = null;
        IEnumerable<TopicCode> topicCodes = null;
        IEnumerable<Term> terms = null;

        [TestInitialize]
        public void Initialize()
        {
            coursePage = new CoursePage2()
            {
                AcademicLevels = new List<Filter> { 
                    new Filter() { Count = 2, Selected = false, Value = "AcademicLevel1"  },
                    new Filter() { Count = 2, Selected = false, Value = "AcademicLevel3"  }
                },
                CourseLevels = new List<Filter> { 
                    new Filter() { Count = 2, Selected = false, Value = "CourseLevel1"  },
                    new Filter() { Count = 2, Selected = false, Value = "CourseLevel3"  }
                },
                CourseTypes = new List<Filter> { 
                    new Filter() { Count = 2, Selected = false, Value = "CourseType1"  },
                    new Filter() { Count = 2, Selected = false, Value = "CourseType3"  }
                },
                CurrentPageIndex = 1,
                CurrentPageItems = new List<CourseSearch2>()
                {
                    new CourseSearch2() { Id = "11111"},
                    new CourseSearch2() { Id = "22222"}
                },
                Faculty = new List<Filter>()
                {
                    new Filter() { Count = 2, Selected = false, Value = "11111"},
                    new Filter() { Count = 2, Selected = false, Value = "33333"}
                },
                Locations = new List<Filter>()
                {
                    new Filter() { Count = 2, Selected = false, Value = "Location1"},
                    new Filter() { Count = 2, Selected = false, Value = "Location3"}
                },
                Subjects = new List<Filter>()
                {
                    new Filter() { Count = 2, Selected = false, Value = "Subject1"},
                    new Filter() { Count = 2, Selected = false, Value = "Subject3"}
                },
                PageSize = 10,
                TotalItems = 2,
                TotalPages = 1
            };
            subjects = new List<Subject>() {
                new Subject(){ Code = "Subject1", Description = "Description1", ShowInCourseSearch = true},
                new Subject(){ Code = "Subject2", Description = "Description2", ShowInCourseSearch = true},
                new Subject(){ Code = "Subject3", Description = "Description3", ShowInCourseSearch = true},
                new Subject(){ Code = "Subject4", Description = "Description4", ShowInCourseSearch = true}
            };
            academicLevels = new List<AcademicLevel>() {
                new AcademicLevel(){ Code = "AcademicLevel1", Description = "Description1"},
                new AcademicLevel(){ Code = "AcademicLevel2", Description = "Description2"},
                new AcademicLevel(){ Code = "AcademicLevel3", Description = "Description3"},
                new AcademicLevel(){ Code = "AcademicLevel4", Description = "Description4"}
            };
            courseLevels = new List<CourseLevel>() {
                new CourseLevel(){ Code = "CourseLevel1", Description = "Description1"},
                new CourseLevel(){ Code = "CourseLevel2", Description = "Description2"},
                new CourseLevel(){ Code = "CourseLevel3", Description = "Description3"},
                new CourseLevel(){ Code = "CourseLevel4", Description = "Description4"}
            };
            locations = new List<Location>() {
                new Location(){ Code = "Location1", Description = "Description1"},
                new Location(){ Code = "Location2", Description = "Description2"},
                new Location(){ Code = "Location3", Description = "Description3"},
                new Location(){ Code = "Location4", Description = "Description4"}
            };
            faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>() {
                new Ellucian.Colleague.Dtos.Student.Faculty(){ Id = "11111"},
                new Ellucian.Colleague.Dtos.Student.Faculty(){ Id = "22222"},
                new Ellucian.Colleague.Dtos.Student.Faculty(){ Id = "33333"},
                new Ellucian.Colleague.Dtos.Student.Faculty(){ Id = "44444"}
            };
            courseTypes = new List<CourseType>() {
                new CourseType(){ Code = "CourseType1", Description = "Description1"},
                new CourseType(){ Code = "CourseType2", Description = "Description2"},
                new CourseType(){ Code = "CourseType3", Description = "Description3"},
                new CourseType(){ Code = "CourseType4", Description = "Description4"}
            };
            topicCodes = new List<TopicCode>() {
                new TopicCode(){ Code = "TopicCode1", Description = "Description1"},
                new TopicCode(){ Code = "TopicCode2", Description = "Description2"},
                new TopicCode(){ Code = "TopicCode3", Description = "Description3"},
                new TopicCode(){ Code = "TopicCode4", Description = "Description4"}
            };
            terms = new List<Term>() {
                new Term(){ Code = "Term1", Description = "Description1"},
                new Term(){ Code = "Term2", Description = "Description2"},
                new Term(){ Code = "Term3", Description = "Description3"},
                new Term(){ Code = "Term4", Description = "Description4"}
            };
        }

        [TestMethod]
        public void CourseSearchResultConstructor()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, academicLevels, courseLevels, locations, faculty, courseTypes, topicCodes, terms);
            Assert.AreEqual(coursePage.CurrentPageIndex, CourseSearchResult.CurrentPageIndex);
            Assert.AreEqual(coursePage.PageSize, CourseSearchResult.PageSize);
            Assert.AreEqual(coursePage.TotalItems, CourseSearchResult.TotalItems);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CourseSearchResultConstructor_coursePageNull()
        {
            var CourseSearchResult = new CourseSearchResult(null, subjects, academicLevels, courseLevels, locations, faculty, courseTypes, topicCodes, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_subjectsNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, null, academicLevels, courseLevels, locations, faculty, courseTypes, topicCodes, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_academicLevelssNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, null, courseLevels, locations, faculty, courseTypes, topicCodes, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_courseLevelsNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, academicLevels, null, locations, faculty, courseTypes, topicCodes, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_locationsNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, academicLevels, courseLevels, null, faculty, courseTypes, topicCodes, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_facultyNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, academicLevels, courseLevels, locations, null, courseTypes, topicCodes, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_courseTypesNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, academicLevels, courseLevels, locations, faculty, null, topicCodes, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_topicCodesNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, academicLevels, courseLevels, locations, faculty, courseTypes, null, terms);
        }

        [TestMethod]
        public void CourseSearchResultConstructor_termsNull()
        {
            var CourseSearchResult = new CourseSearchResult(coursePage, subjects, academicLevels, courseLevels, locations, faculty, courseTypes, topicCodes, null);
        }
    }
}