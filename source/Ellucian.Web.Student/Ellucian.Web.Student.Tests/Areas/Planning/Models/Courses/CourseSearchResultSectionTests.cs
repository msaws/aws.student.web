﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.Planning.Models.Requirements;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Areas.Planning.Models.Advising;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using System;
using Ellucian.Web.Student.Models.Courses;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Courses
{
    [TestClass]
    public class CourseSearchResultSectionConstructorTests
    {
        Section3 section = null;

        [TestInitialize]
        public void Initialize()
        {
            section = new Section3() { Id = "11111", CourseId = "11111", Title = "Section Test Title" };
        }

        [TestMethod]
        public void CourseSearchResultSectionConstructor()
        {
            var CourseSearchResultSection = new CourseSearchResultSection(section);
            Assert.AreEqual(section.Id, CourseSearchResultSection.Id);
            Assert.AreEqual(section.CourseId, CourseSearchResultSection.CourseId);
            Assert.AreEqual(section.Title, CourseSearchResultSection.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CourseSearchResultConstructor_coursePageNull()
        {
            var CourseSearchResultSection = new CourseSearchResultSection(null);
        }
    }
}