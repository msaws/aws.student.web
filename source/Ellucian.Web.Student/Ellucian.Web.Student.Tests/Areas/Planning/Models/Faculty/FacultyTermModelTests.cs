﻿using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Faculty
{
    [TestClass]
    public class FacultyTermModelTest
    {
        FacultyTermModel model;
        string aString;


        [TestInitialize]
        public void Initialize()
        {
            aString = "test";
            model = new FacultyTermModel(aString, aString);
        }

        [TestCleanup]
        public void Cleanup()
        {
            model = null;
        }
        
        [TestMethod]
        public void FacultyTermModelConstructor_AllProperties()
        {
            Assert.AreEqual(aString, model.Code);
            Assert.AreEqual(aString, model.Description);
            Assert.AreEqual(0, model.Sections.Count);
        }

        [TestMethod]
        public void FacultyTermModelAddSections_Success()
        {
            var section = new Section3() { Id = "SectionId", TermId = "TermId", EndDate = DateTime.Today.AddDays(30), StartDate = DateTime.Today, Title = "Section Title", Meetings = new List<SectionMeeting2>(), Location = "Loc", CourseName = "ENGL-100", Number = "01" };
            var allLocations = new Dictionary<string, Location>();
            allLocations.Add("Loc", new Location() { Code = "Loc", Description = "Location Description" });
            var allBuildings = new Dictionary<string, Building>();
            allBuildings.Add("Bldg",  new Building() { Code = "Bldg", Description = "Building Name" });
            var allTerms = new Dictionary<string, Term>();
            allTerms.Add("Term", new Term() { Code = "Term", Description = "Term Name" });
            var allRooms = new Dictionary<string, Room>();
            allRooms.Add("Bldg*100", new Room() { BuildingCode = "Bldg", Number = "100", Id = "Bldg*100", Code = "Bldg*100" } );
            var allInstrMethods = new Dictionary<string, InstructionalMethod>();
            allInstrMethods.Add("Inst", new InstructionalMethod() { Code = "Inst", Description = "Instructional Method" } );
            
            var sections = new List<FacultySectionModel>();
            sections.Add(new FacultySectionModel(section, allLocations, allBuildings, allRooms, allInstrMethods, allTerms));
            model.AddSections(sections);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FacultyTermModelAddSections_NullArgument()
        {
            model.AddSections(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void FacultyTermModelAddSections_EmptyArgument()
        {
            model.AddSections(new List<FacultySectionModel>());
        }
    }
}
