﻿using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Ellucian.Web.Student.Areas.Planning.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Faculty
{
    [TestClass]
    public class StudentPetitionModelTest
    {
        List<StudentPetition> studentPetitionDtos = new List<StudentPetition>();
        List<PetitionStatus> petitionStatusDtos = new List<PetitionStatus>();
        List<StudentPetitionReason> petitionReasonDtos = new List<StudentPetitionReason>();
        List<Course2> courseDtos = new List<Course2>();
        List<Section3> sectionDtos = new List<Section3>();
        List<Term> termsDtos = new List<Term>();

        [TestInitialize]
        public void Initialize()
        {
            CreateListOfPetitions();
            CreateLookupData();
        }


        [TestCleanup]
        public void Cleanup()
        {
            studentPetitionDtos = null;
            petitionStatusDtos = null;
            petitionReasonDtos = null;
            courseDtos = null;
            sectionDtos = null;
            termsDtos = null;
        }



        private void CreateListOfPetitions()
        {
            studentPetitionDtos.Add(new StudentPetition()
            {
                Id = "1",
                Comment = "trial 1",
                CourseId = "C1",
                DateTimeChanged = new DateTimeOffset(DateTime.Today),
                EndDate = null,
                ReasonCode = "R1",
                SectionId = null,
                StartDate = null,
                StatusCode = "A",
                StudentId = "001123",
                TermCode = "2013/FA",
                Type = StudentPetitionType.FacultyConsent
            });
            studentPetitionDtos.Add(new StudentPetition()
            {
                Id = "2",
                Comment = "trial 2",
                CourseId = "C1",
                DateTimeChanged = new DateTimeOffset(DateTime.Today),
                EndDate = null,
                ReasonCode = "R2",
                SectionId = null,
                StartDate = null,
                StatusCode = "A",
                StudentId = "001123",
                TermCode = "2013/FA",
                Type = StudentPetitionType.StudentPetition
            });
            studentPetitionDtos.Add(new StudentPetition()
            {
                Id = "3",
                Comment = "trial 3",
                CourseId = "C3",
                DateTimeChanged = new DateTimeOffset(DateTime.Today),
                EndDate = null,
                ReasonCode = "R3",
                SectionId = "S3",
                StartDate = null,
                StatusCode = "D",
                StudentId = "001123",
                TermCode = "2016/FA",
                Type = StudentPetitionType.FacultyConsent
            });
            studentPetitionDtos.Add(new StudentPetition()
            {
                Id = "4",
                Comment = "trial 1",
                CourseId = "C4",
                DateTimeChanged = new DateTimeOffset(DateTime.Today),
                EndDate = null,
                ReasonCode = "R1",
                SectionId = "S1",
                StartDate = null,
                StatusCode = "P",
                StudentId = "001123",
                TermCode = "2016/FA",
                Type = StudentPetitionType.StudentPetition
            });
            studentPetitionDtos.Add(new StudentPetition()
            {
                Id = "5",
                Comment = "trial 1",
                CourseId = "C3",
                DateTimeChanged = new DateTimeOffset(DateTime.Today),
                EndDate = null,
                ReasonCode = "R1",
                SectionId = "S1",
                StartDate = null,
                StatusCode = "D",
                StudentId = "001123",
                TermCode = "2015/FA",
                Type = StudentPetitionType.StudentPetition
            });
            studentPetitionDtos.Add(new StudentPetition()
            {
                Id = "6",
                Comment = "trial 1",
                CourseId = "C5",
                DateTimeChanged = new DateTimeOffset(DateTime.Today),
                EndDate = null,
                ReasonCode = "R3",
                SectionId = "S1",
                StartDate = null,
                StatusCode = "A",
                StudentId = "001123",
                TermCode = "2017/FA",
                Type = StudentPetitionType.FacultyConsent
            });

        }

        private void CreateLookupData()
        {
            //petition statuses
            petitionStatusDtos.Add(new PetitionStatus()
                {
                    Code = "A",
                    Description = "Approved",
                    IsGranted = true
                });

            petitionStatusDtos.Add(new PetitionStatus()
            {
                Code = "D",
                Description = "Denied",
                IsGranted = true
            });
            petitionStatusDtos.Add(new PetitionStatus()
            {
                Code = "P",
                Description = "Pending",
                IsGranted = true
            });

            //reasons
            petitionReasonDtos.Add(new StudentPetitionReason() { Code = "R1", Description = "Reason 1" });
            petitionReasonDtos.Add(new StudentPetitionReason() { Code = "R2", Description = "Reason 2" });
            petitionReasonDtos.Add(new StudentPetitionReason() { Code = "R3", Description = "Reason 3" });

            //courses
            courseDtos.Add(new Course2()
            {
                Id = "C1",
                Number = "1",
                SubjectCode = "Course"
            });
            courseDtos.Add(new Course2()
            {
                Id = "C2",
                Number = "2",
                SubjectCode = "Course"
            });
            courseDtos.Add(new Course2()
            {
                Id = "C3",
                Number = "3",
                SubjectCode = "Course"
            });
            courseDtos.Add(new Course2()
            {
                Id = "C4",
                Number = "4",
                SubjectCode = "Course"
            });
            courseDtos.Add(new Course2()
            {
                Id = "C5",
                Number = "5",
                SubjectCode = "Course"
            });

            //terms
            termsDtos.Add(new Term() { Code = "2013/FA", Description = "2013 fall term" });
            termsDtos.Add(new Term() { Code = "2014/FA", Description = "2014 fall term" });
            termsDtos.Add(new Term() { Code = "2015/FA", Description = "2015 fall term" });
            termsDtos.Add(new Term() { Code = "2016/FA", Description = "2016 fall term" });
            termsDtos.Add(new Term() { Code = "2017/FA", Description = "2017 fall term" });

            //sections
            sectionDtos.Add(new Section3() { Id = "S1", CourseId = null, CourseName = null, TermId = null, Number = "1" });
            sectionDtos.Add(new Section3() { Id = "S2", CourseId = null, CourseName = null, TermId = null, Number = "2" });
            sectionDtos.Add(new Section3() { Id = "S3", CourseId = null, CourseName = null, TermId = null, Number = "3" });
            sectionDtos.Add(new Section3() { Id = "S4", CourseId = null, CourseName = null, TermId = null, Number = "4" });
            sectionDtos.Add(new Section3() { Id = "S5", CourseId = null, CourseName = null, TermId = null, Number = "5" });


        }


        [TestClass]
        public class PetitionsTest : StudentPetitionModelTest
        {
            [TestMethod]
            public void BuildPetitionsModelForStudent_ValidDataForPetitionsOnly()
            {
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                //create an ordered sequence of course names
                IEnumerable<string> orderedCourseNames = from course in courseDtos
                                                         join petition in studentPetitionDtos
                                                         on course.Id equals petition.CourseId
                                                         where petition.Type==StudentPetitionType.StudentPetition
                                                         orderby course.SubjectCode + "-" + course.Number
                                                         select  course.SubjectCode + "-" + course.Number;

                //not null
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                //course cannot be empty or null
                Assert.IsTrue(petitions.Count(p => string.IsNullOrEmpty(p.Course)) == 0);
                //is ordered on course
                Assert.IsTrue(petitions.Select(p => p.Course).SequenceEqual(orderedCourseNames.Distinct()));
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_CourseIdIsEmpty()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = null,
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = null,
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Course, string.Empty);
                //after sorting empty should be first one
                Assert.IsTrue(petitions[0].PetitionId == "7");

            }
            [TestMethod]
            public void BuildPetitionsModelForStudent_CourseIdDoesNotExists()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C99",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = null,
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Course, string.Empty);
                //after sorting empty should be first one
                Assert.IsTrue(petitions[0].PetitionId == "7");
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_SectionIdIsEmpty()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = null,
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Section, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_SectionIdDoesNotExists()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = "S99",
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Section, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_TermCodeIsEmpty()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = null,
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Term, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_StartDateIsProvided()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = "S1",
                    StartDate = DateTime.Today,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = null,
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").StartDate, DateTime.Today.ToShortDateString());
            }
            [TestMethod]
            public void BuildPetitionsModelForStudent_EnDateIsProvided()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = DateTime.Today,
                    ReasonCode = "R1",
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = null,
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").EndDate, DateTime.Today.ToShortDateString());
            }
            [TestMethod]
            public void BuildPetitionsModelForStudent_EndDateIsEmpty()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = null,
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").EndDate, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_TermCodeDoesNotExists()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = "2021/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Term, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_ReasonCodeIsEmpty()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode =null,
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Reason, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_ReasonCodeDoesNotExists()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R99",
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = "A",
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Reason, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_StatusCodeIsEmpty()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R1",
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = null,
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Status, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_StatusCodeDoesNotExists()
            {
                studentPetitionDtos.Add(new StudentPetition()
                {
                    Id = "7",
                    Comment = "trial 1",
                    CourseId = "C1",
                    DateTimeChanged = new DateTimeOffset(DateTime.Today),
                    EndDate = null,
                    ReasonCode = "R99",
                    SectionId = "S1",
                    StartDate = null,
                    StatusCode = "Z",
                    StudentId = "001123",
                    TermCode = "2013/FA",
                    Type = StudentPetitionType.StudentPetition
                });
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.First(p => p.PetitionId == "7").Status, string.Empty);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_PetitionDtosIsNull()
            {

                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(null, petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(),0);
            }
            [TestMethod]
            public void BuildPetitionsModelForStudent_PetitionDtosIsEmpty()
            {

                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(new List<StudentPetition>(), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), 0);
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_CourseDtosIsNull()
            {

                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, null, sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.Count(), petitions.Count(p=>p.Course==string.Empty));

            }
            [TestMethod]
            public void BuildPetitionsModelForStudent_CourseDtosIsEmpty()
            {

                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, new List<Course2>(), sectionDtos, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.Count(), petitions.Count(p => p.Course == string.Empty));
            }

            [TestMethod]
            public void BuildPetitionsModelForStudent_SectionDtosIsNull()
            {

                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, null, termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.Count(), petitions.Count(p => p.Section == string.Empty));

            }
            [TestMethod]
            public void BuildPetitionsModelForStudent_SectionDtosIsEmpty()
            {

                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.StudentPetition), petitionStatusDtos, petitionReasonDtos, courseDtos, new List<Section3>(), termsDtos);
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.StudentPetition));
                Assert.AreEqual(petitions.Count(), petitions.Count(p => p.Section == string.Empty));
            }
        }

        [TestClass]
        public class FacultyConsentsTest : StudentPetitionModelTest
        {
            [TestMethod]
            public void BuildFacultyConsentsModelForStudent_ValidDataForPetitionsOnly()
            {
                List<StudentPetitionModel> petitions = FacultyHelper.BuildStudentPetitions(studentPetitionDtos.Where(s => s.Type == StudentPetitionType.FacultyConsent), petitionStatusDtos, petitionReasonDtos, courseDtos, sectionDtos, termsDtos);
                //create an ordered sequence of course names
                IEnumerable<string> orderedCourseNames = from course in courseDtos
                                                         join petition in studentPetitionDtos
                                                         on course.Id equals petition.CourseId
                                                         where petition.Type == StudentPetitionType.FacultyConsent
                                                         orderby course.SubjectCode + "-" + course.Number
                                                         select course.SubjectCode + "-" + course.Number;

                //not null
                Assert.IsNotNull(petitions);
                Assert.AreEqual(petitions.Count(), studentPetitionDtos.Count(s => s.Type == StudentPetitionType.FacultyConsent));
                //course cannot be empty or null
                Assert.IsTrue(petitions.Count(p => string.IsNullOrEmpty(p.Course)) == 0);
                //is ordered on course
                Assert.IsTrue(petitions.Select(p => p.Course).SequenceEqual(orderedCourseNames.Distinct()));
            }
        }
    }
}
