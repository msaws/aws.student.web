﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Faculty
{
    [TestClass]
    public class FacultyRosterStudentModelTests
    {
        FacultyRosterStudentModel model;
        Colleague.Dtos.Student.StudentBatch3 student;
        IEnumerable<Colleague.Dtos.Student.ClassLevel> classLevels;
        
        [TestInitialize]
        public void Initialize()
        {
            student = new Colleague.Dtos.Student.StudentBatch3()
            {
                Id = "1234567",
                LastName = "James",
                FirstName = "Earl",
                MiddleName = "Boolean",
                PreferredEmailAddress = "ebjamesxyz@ellucian.com",
                ClassLevelCodes = new List<string>() { "2", "XYZ" }

            };
            classLevels = new List<Colleague.Dtos.Student.ClassLevel>() {
                new Colleague.Dtos.Student.ClassLevel() {Code = "1",Description = "Freshman"},
                new Colleague.Dtos.Student.ClassLevel() {Code="2",Description = "Second Year"}
            };
        }

        [TestMethod]
        public void FacultyRosterStudentModel_AllPropertiesBuilt()
        {
            model = new FacultyRosterStudentModel(student, classLevels);
            Assert.AreEqual(student.Id, model.StudentId);
            Assert.AreEqual("James, Earl B.", model.DisplayNameLfm);
            Assert.AreEqual("James", model.NameSort1);
            Assert.AreEqual("Earl", model.NameSort2);
            Assert.AreEqual("Boolean", model.NameSort3);
            Assert.AreEqual(student.PreferredEmailAddress, model.PreferredEmail);
            Assert.AreEqual("Second Year, XYZ", model.ClassLevel);
        }

        [TestMethod]
        public void FacultyRosterStudentModel_WithPersonDisplayName()
        {
            var otherstudent = new Colleague.Dtos.Student.StudentBatch3()
            {
                Id = "1234567",
                LastName = "James",
                FirstName = "Earl",
                MiddleName = "Boolean",
                PreferredEmailAddress = "ebjamesxyz@ellucian.com",
                ClassLevelCodes = new List<string>() { "2", "XYZ" },
                PersonDisplayName = new Colleague.Dtos.Base.PersonHierarchyName() {  HierarchyCode = "CHL", FirstName = "OtherFirst", LastName = "OtherLast", MiddleName = "OtherMiddle", FullName = "OtherLast, OtherFirst O."}

            };
            model = new FacultyRosterStudentModel(otherstudent, classLevels);
            Assert.AreEqual(otherstudent.Id, model.StudentId);
            Assert.AreEqual("OtherLast, OtherFirst O.", model.DisplayNameLfm);
            Assert.AreEqual("OtherLast", model.NameSort1);
            Assert.AreEqual("OtherFirst", model.NameSort2);
            Assert.AreEqual("OtherMiddle", model.NameSort3);
            Assert.AreEqual(otherstudent.PreferredEmailAddress, model.PreferredEmail);
            Assert.AreEqual("Second Year, XYZ", model.ClassLevel);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FacultyRosterStudentModel_ThrowsExceptionIfStudentNull()
        {
            student = null;
            model = new FacultyRosterStudentModel(student, classLevels);
        }

        [TestMethod]
        public void FacultyRoster_StudentModel_SucceedsIfCourseLevelsNull()
        {
            classLevels = null;
            model = new FacultyRosterStudentModel(student, classLevels);
            Assert.AreEqual(student.Id, model.StudentId);
            Assert.AreEqual("James, Earl B.", model.DisplayNameLfm);
            Assert.AreEqual(student.PreferredEmailAddress, model.PreferredEmail);
            Assert.AreEqual("2, XYZ", model.ClassLevel);
        }
    }
}
