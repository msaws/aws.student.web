﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.Planning;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Faculty
{
    [TestClass]
    public class FacultyStudentGradeModelTest
    {
        StudentBatch3 student = new StudentBatch3();
        private FacultyStudentGradeModel model;
        List<ClassLevel> classLevels = new List<ClassLevel>();
        string sectionId = "121212";
        AcademicCredit2 acadCredit=new AcademicCredit2();
        Section3 section = new Section3();
        

        [TestInitialize]
        public void Initialize()
        {
            student.ClassLevelCodes = new List<string>() { "C1" };
            classLevels.Add(new ClassLevel() { Code="C1",Description="Senior",SortOrder=1 });
            classLevels.Add(new ClassLevel() { Code = "C2", Description = "Junior", SortOrder = 2 });

            classLevels.Add(new ClassLevel() { Code = "C3", Description = "Sophomore", SortOrder = 3 });

            classLevels.Add(new ClassLevel() { Code = "C4", Description = "Freshmen", SortOrder = 4 });

            classLevels.Add(new ClassLevel() { Code = "C5", Description = "Senior Law", SortOrder = null });
            classLevels.Add(new ClassLevel() { Code = "C6", Description = "Junior Law" });
             classLevels.Add(new ClassLevel() { Code = "C7", Description = "Freshman Law", SortOrder = null });

            section.Id = "121212";
            acadCredit.NeverAttended = null;
        }

        [TestCleanup]
        public void Cleanup()
        {
            model = null;
        }
        

        [TestMethod]
        public void Validate_ClassLevelsForStudentWithClassCode()
        {
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, classLevels[0].Description);
            Assert.AreEqual(model.ClassLevelOrder, classLevels[0].SortOrder);
        }

        [TestMethod]
        public void Validate_ClassLevelsWithNoSortOrderProvided()
        {
            student.ClassLevelCodes = new List<string>() { "C6" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, classLevels[5].Description);
            Assert.AreEqual(model.ClassLevelOrder, null);
        }

        [TestMethod]
        public void Validate_ClassLevelsWithNULLSortOrder()
        {
            student.ClassLevelCodes = new List<string>() { "C5" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, classLevels[4].Description);
            Assert.AreEqual(model.ClassLevelOrder, null);
        }

        [TestMethod]
        public void Validate_StudentClassLevelNotInList()
        {
            student.ClassLevelCodes = new List<string>() { "C8" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, string.Empty);
            Assert.AreEqual(model.ClassLevelOrder, null);
        }

        [TestMethod]
        public void Validate_StudentHasMultipleClassLevels()
        {
            student.ClassLevelCodes = new List<string>() { "C1","C3" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, string.Join(",",classLevels[0].Description,classLevels[2].Description));
            Assert.AreEqual(model.ClassLevelOrder, classLevels[2].SortOrder);
        }

        [TestMethod]
        public void Validate_StudentHasMultipleClassLevels_OneNotInList()
        {
            student.ClassLevelCodes = new List<string>() { "C8","C4" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, classLevels[3].Description);
            Assert.AreEqual(model.ClassLevelOrder, classLevels[3].SortOrder);
        }

        [TestMethod]
        public void Validate_StudentHasMultipleClassLevels_AllNotInList()
        {
            student.ClassLevelCodes = new List<string>() { "C8","C9" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, string.Empty);
            Assert.AreEqual(model.ClassLevelOrder, null);
        }
        [TestMethod]
        public void Validate_StudentHasMultipleClassLevels_OnHasSortOrderNULLInList()
        {
            student.ClassLevelCodes = new List<string>() { "C4","C5" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, string.Join(",", classLevels[3].Description, classLevels[4].Description));
            Assert.AreEqual(model.ClassLevelOrder, classLevels[3].SortOrder);
        }
        [TestMethod]
        public void Validate_StudentHasMultipleClassLevels_AllHaveSortOrderNULLInList()
        {
            student.ClassLevelCodes = new List<string>() { "C5","C7" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, string.Join(",", classLevels[4].Description, classLevels[6].Description));
            Assert.AreEqual(model.ClassLevelOrder, null);
        }

        [TestMethod]
        public void Validate_StudentHasMultipleClassLevels_OneHasNoSortOrderInList()
        {
            student.ClassLevelCodes = new List<string>() { "C6","C4" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, string.Join(",", classLevels[5].Description, classLevels[3].Description));
            Assert.AreEqual(model.ClassLevelOrder, classLevels[3].SortOrder);
        }
        [TestMethod]
        public void Validate_StudentHasMultipleClassLevels_AllHaveSortOrderInList()
        {
            student.ClassLevelCodes = new List<string>() { "C1","C2","C3" };
            model = new FacultyStudentGradeModel(sectionId, acadCredit, student, null, section, false, student.ClassLevelCodes, classLevels);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(model.ClassLevelDescription, string.Join(",", classLevels[0].Description, classLevels[1].Description, classLevels[2].Description));
            Assert.AreEqual(model.ClassLevelOrder, classLevels[2].SortOrder);
        }
    }
}
