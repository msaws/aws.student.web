﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.Planning;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Faculty
{
    [TestClass]
    public class FacultyStudentGradesModelTests
    {
        StudentBatch student = new StudentBatch();
        List<ClassLevel> classLevels = new List<ClassLevel>();
        Section3 section = new Section3();
        private FacultyStudentGradesModel gradesModel;

        [TestInitialize]
        public void Initialize()
        {
            student.ClassLevelCodes = new List<string>() { "C1" };
            classLevels.Add(new ClassLevel() { Code = "C1", Description = "Senior", SortOrder = 1 });
            classLevels.Add(new ClassLevel() { Code = "C2", Description = "Junior", SortOrder = 2 });
            // basic section
            section.GradeSchemeCode = "UG";
            section.Id = "121212";
            section.Number = "SectionNumber";
            gradesModel = new FacultyStudentGradesModel();
        }

        [TestCleanup]
        public void Cleanup()
        {
            gradesModel = null;
        }


        [TestMethod]
        public void TestingNullGrades()
        {
            List<Grade> grades = null;

            gradesModel.GradeTypes = grades != null ? grades.Where(g => g.GradeSchemeCode == section.GradeSchemeCode && !g.ExcludeFromFacultyGrading).OrderBy(gr => gr.LetterGrade).Select(g => Tuple.Create<string, string,bool>(g.LetterGrade, g.IncompleteGrade,false)).ToList() : new List<Tuple<string, string,bool>>();
            Assert.AreEqual(0, gradesModel.GradeTypes.Count());
            Assert.IsTrue(gradesModel.GradeTypes is List<Tuple<string, string,bool>>);
        }

        [TestMethod]
        public void TestingEmptyGrades()
        {
            List<Grade> grades = new List<Grade>();

            gradesModel.GradeTypes = grades != null ? grades.Where(g => g.GradeSchemeCode == section.GradeSchemeCode && !g.ExcludeFromFacultyGrading).OrderBy(gr => gr.LetterGrade).Select(g => Tuple.Create<string, string,bool>(g.LetterGrade, g.IncompleteGrade,false)).ToList() : new List<Tuple<string, string,bool>>();
            Assert.AreEqual(0, gradesModel.GradeTypes.Count());
            Assert.IsTrue(gradesModel.GradeTypes is List<Tuple<string, string,bool>>);
        }

        [TestMethod]
        public void TestingWithNoSameSchemeGrades()
        {
            List<Grade> grades = new List<Grade>() {new Grade() {GradeSchemeCode = "GR", LetterGrade = "A", ExcludeFromFacultyGrading = false}};

            gradesModel.GradeTypes = grades != null ? grades.Where(g => g.GradeSchemeCode == section.GradeSchemeCode && !g.ExcludeFromFacultyGrading).OrderBy(gr => gr.LetterGrade).Select(g => Tuple.Create<string, string,bool>(g.LetterGrade, g.IncompleteGrade,false)).ToList() : new List<Tuple<string, string,bool>>();
            Assert.AreEqual(0, gradesModel.GradeTypes.Count());
            Assert.IsTrue(gradesModel.GradeTypes is List<Tuple<string, string,bool>>);
        }

        [TestMethod]
        public void TestingWithMatchingSchemeGrades()
        {
            List<Grade> grades = new List<Grade>() { new Grade() { GradeSchemeCode = "UG", LetterGrade = "A" }, new Grade() { GradeSchemeCode = "UG", LetterGrade = "B" }, new Grade() { GradeSchemeCode = "GR", LetterGrade = "C" } };

            gradesModel.GradeTypes = grades != null ? grades.Where(g => g.GradeSchemeCode == section.GradeSchemeCode && !g.ExcludeFromFacultyGrading).OrderBy(gr => gr.LetterGrade).Select(g => Tuple.Create<string, string,bool>(g.LetterGrade, g.IncompleteGrade,false)).ToList() : new List<Tuple<string, string,bool>>();
            Assert.AreEqual(2, gradesModel.GradeTypes.Count());
            Assert.IsTrue(gradesModel.GradeTypes is List<Tuple<string, string,bool>>);
        }

        [TestMethod]
        public void TestingWithMatchingSchemeGradesOneExcluded()
        {
            List<Grade> grades = new List<Grade>() { new Grade() { GradeSchemeCode = "UG", LetterGrade = "A", ExcludeFromFacultyGrading = true }, new Grade() { GradeSchemeCode = "UG", LetterGrade = "B" }, new Grade() { GradeSchemeCode = "GR", LetterGrade = "C" } };

            gradesModel.GradeTypes = grades != null ? grades.Where(g => g.GradeSchemeCode == section.GradeSchemeCode && !g.ExcludeFromFacultyGrading).OrderBy(gr => gr.LetterGrade).Select(g => Tuple.Create<string, string,bool>(g.LetterGrade, g.IncompleteGrade,false)).ToList() : new List<Tuple<string, string,bool>>();
            Assert.AreEqual(1, gradesModel.GradeTypes.Count());
            Assert.IsTrue(gradesModel.GradeTypes is List<Tuple<string, string,bool>>);
            Assert.AreEqual("B", gradesModel.GradeTypes[0].Item1);
        }

        [TestMethod]
        public void TestingWithMatchingSchemeGradesIncompleteGrade()
        {
            List<Grade> grades = new List<Grade>() { new Grade() { GradeSchemeCode = "UG", LetterGrade = "B", IncompleteGrade = "X"}, new Grade() { GradeSchemeCode = "UG", LetterGrade = "A" }, new Grade() { GradeSchemeCode = "GR", LetterGrade = "C" } };

            gradesModel.GradeTypes = grades != null ? grades.Where(g => g.GradeSchemeCode == section.GradeSchemeCode && !g.ExcludeFromFacultyGrading).OrderBy(gr => gr.LetterGrade).Select(g => Tuple.Create<string, string,bool>(g.LetterGrade, g.IncompleteGrade,false)).ToList() : new List<Tuple<string, string,bool>>();
            Assert.AreEqual(2, gradesModel.GradeTypes.Count());
            Assert.IsTrue(gradesModel.GradeTypes is List<Tuple<string, string,bool>>);
            Assert.AreEqual("A", gradesModel.GradeTypes[0].Item1);
            Assert.IsNull(gradesModel.GradeTypes[0].Item2);
            Assert.AreEqual("B", gradesModel.GradeTypes[1].Item1);
            Assert.AreEqual("X", gradesModel.GradeTypes[1].Item2);

        }
    }
}
