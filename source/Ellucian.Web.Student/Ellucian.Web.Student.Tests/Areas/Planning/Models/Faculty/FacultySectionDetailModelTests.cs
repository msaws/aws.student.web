﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.Planning;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Faculty
{
    [TestClass]
    public class FacultySectionDetailModelTest
    {
        private Dictionary<string, Location> allLocations;
        private Dictionary<string, Building> allBuildings;
        private Dictionary<string, Room> allRooms;
        private Dictionary<string, InstructionalMethod> allInstrMethods;
        private List<StudentWaiverReason> allReasons;
        private Section3 section;
        private FacultySectionDetailModel model;

        private string courseTitle;
        private string courseName;
        private string courseNumber;

        [TestInitialize]
        public void Initialize()
        {
            allLocations = new Dictionary<string, Location>();
            allLocations.Add("Loc", new Location() { Code = "Loc", Description = "Location Description" });
            allBuildings = new Dictionary<string, Building>();
            allBuildings.Add("Bldg",  new Building() { Code = "Bldg", Description = "Building Name" });
            allRooms = new Dictionary<string, Room>();
            allRooms.Add("Bldg*100", new Room() { BuildingCode = "Bldg", Number = "100", Id = "Bldg*100", Code = "Bldg*100" } );
            allInstrMethods = new Dictionary<string, InstructionalMethod>();
            allInstrMethods.Add("Inst", new InstructionalMethod() { Code = "Inst", Description = "Instructional Method" } );
            allReasons = new List<StudentWaiverReason>() { new StudentWaiverReason() { Code = "waiver1", Description = "waiver1" }, new StudentWaiverReason() { Code = "waiver2", Description = "wavier2" } };
            var sectionMeetings = new List<SectionMeeting2>() { new SectionMeeting2() { Days = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday }, InstructionalMethodCode = "Inst", Room = "Bldg*100", StartTime = DateTime.Now, EndTime = DateTime.Now.AddHours(1), IsOnline = false } };

            courseTitle = "Section Title";
            courseName = "Course Name";
            courseNumber = "01";
            section = new Section3() { Id = "SectionId", TermId = "TermId", EndDate = DateTime.Today.AddDays(30), StartDate = DateTime.Today, Title = courseTitle, Meetings = sectionMeetings, Location = "Loc", CourseName = courseName, Number = courseNumber };
        }

        [TestCleanup]
        public void Cleanup()
        {
            model = null;
        }
        
        [TestMethod]
        public void FacultySectionDetailModelConstructor_AllPropertiesSet()
        {
            model = new FacultySectionDetailModel(section.Id, allReasons);
            Assert.AreEqual(section.Id, model.SectionId);
            var reasonsExpectedNotActual = allReasons.Except(model.WaiverReasons);
            var reasonsActualNotExpected = model.WaiverReasons.Except(allReasons);
            Assert.AreEqual(0, reasonsExpectedNotActual.Count());
            Assert.AreEqual(0, reasonsActualNotExpected.Count());
        }

        [TestMethod]
        public void FacultySectionDetailModelConstructor_WaiversNotSet()
        {
            model = new FacultySectionDetailModel(section.Id,  null);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(0, model.WaiverReasons.Count);
        }
    }
}
