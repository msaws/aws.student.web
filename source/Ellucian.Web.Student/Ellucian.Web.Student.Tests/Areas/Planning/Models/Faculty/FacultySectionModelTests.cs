﻿using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Faculty;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.Planning;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Faculty
{
    [TestClass]
    public class FacultySectionModelTest
    {
        private Dictionary<string, Location> allLocations;
        private Dictionary<string, Building> allBuildings;
        private Dictionary<string, Term> allTerms;
        private Dictionary<string, Room> allRooms;
        private Dictionary<string, InstructionalMethod> allInstrMethods;
        private Section3 section;
        private FacultySectionModel model;

        private string courseTitle;
        private string courseName;
        private string courseNumber;

        [TestInitialize]
        public void Initialize()
        {
            allLocations = new Dictionary<string, Location>();
            allLocations.Add("Loc", new Location() { Code = "Loc", Description = "Location Description" });
            allBuildings = new Dictionary<string, Building>();
            allBuildings.Add("Bldg",  new Building() { Code = "Bldg", Description = "Building Name" });
            allTerms = new Dictionary<string, Term>();
            allTerms.Add("Term", new Term() { Code = "Term", Description = "Ter, Name" });
            allRooms = new Dictionary<string, Room>();
            allRooms.Add("Bldg*100", new Room() { BuildingCode = "Bldg", Number = "100", Id = "Bldg*100", Code = "Bldg*100" } );
            allInstrMethods = new Dictionary<string, InstructionalMethod>();
            allInstrMethods.Add("Inst", new InstructionalMethod() { Code = "Inst", Description = "Instructional Method" } );
            var sectionMeetings = new List<SectionMeeting2>() { new SectionMeeting2() { Days = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday }, InstructionalMethodCode = "Inst", Room = "Bldg*100", StartTime = DateTime.Now, EndTime = DateTime.Now.AddHours(1), IsOnline = false } };

            courseTitle = "Section Title";
            courseName = "Course Name";
            courseNumber = "01";
            section = new Section3() { Id = "SectionId", TermId = "TermId", EndDate = DateTime.Today.AddDays(30), StartDate = DateTime.Today, Title = courseTitle, Meetings = sectionMeetings, Location = "Loc", CourseName = courseName, Number = courseNumber };
        }

        [TestCleanup]
        public void Cleanup()
        {
            model = null;
        }
        
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FacultySectionModelConstructor_NoSection()
        {
            model = new FacultySectionModel(null, allLocations, allBuildings, allRooms, allInstrMethods, allTerms);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_NoLocations()
        {
            model = new FacultySectionModel(section, new Dictionary<string, Location>(), allBuildings, allRooms, allInstrMethods, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(null, model.Location);
        }

        [TestMethod]
        public void FacultySecitonModelConstructor_NullLocations()
        {
            model = new FacultySectionModel(section, null, allBuildings, allRooms, allInstrMethods, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(null, model.Location);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_NoBuildings()
        {
            model = new FacultySectionModel(section, allLocations, new Dictionary<string, Building>(), allRooms, allInstrMethods, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(1, model.Meetings.Count);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_NullBuildings()
        {
            model = new FacultySectionModel(section, allLocations, null, allRooms, allInstrMethods, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(1, model.Meetings.Count);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_NoRooms()
        {
            model = new FacultySectionModel(section, allLocations, allBuildings, new Dictionary<string, Room>(), allInstrMethods, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(1, model.Meetings.Count);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_NullRooms()
        {
            model = new FacultySectionModel(section, allLocations, allBuildings, null, allInstrMethods, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(1, model.Meetings.Count);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_NoInstructionalMethods()
        {
            model = new FacultySectionModel(section, allLocations, allBuildings, allRooms, new Dictionary<string, InstructionalMethod>(), allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(1, model.Meetings.Count);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_NullInstructionalMethods()
        {
            model = new FacultySectionModel(section, allLocations, allBuildings, allRooms, null, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(1, model.Meetings.Count);
        }

        [TestMethod]
        public void FacultySectionModelConstructor_AllProperties()
        {
            model = new FacultySectionModel(section, allLocations, allBuildings, allRooms, allInstrMethods, allTerms);
            Assert.AreEqual(section.Id, model.SectionId);
            Assert.AreEqual(section.StartDate, model.StartDate);
            Assert.AreEqual(section.EndDate, model.EndDate);
            Assert.AreEqual("Location Description", model.Location);
            Assert.AreEqual(courseName + GlobalResources.GetString(PlanningResourceFiles.DegreePlanResources, "UiCourseDelimiter") + courseNumber + ": " + courseTitle, model.FormattedNameDisplay);
            foreach (var item in model.Meetings)
            {
                Assert.AreEqual("Building Name", item.BuildingDisplay);
                Assert.AreEqual("Bldg*100", item.RoomDisplay);
                Assert.AreEqual("Instructional Method", item.InstructionalMethodDisplay);
            }
        }
    }
}
