﻿// Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.DegreePlan
{
    [TestClass]
    public abstract class DegreePlanModelTestsBase
    {
        public Section3 baseSection;
        public Section3 baseSection2;
        public SectionRegistrationDate futureSectionRegistrationDates;
        public List<InstructionalMethod> baseInstructionalMethods;
        public List<Grade> baseGrades;

        [TestInitialize]
        public void BuildBaseObjects()
        {
            baseSection = new Section3()
            {
                ActiveStudentIds = new List<string>() { "0000001", "0000002" },
                AllowAudit = true,
                AllowPassNoPass = true,
                Available = 3,
                Books = new List<SectionBook>() { 
                        new SectionBook() {BookId = "book1", IsRequired = true},
                        new SectionBook() {BookId = "book2", IsRequired = false}
                    },
                Capacity = 5,
                Ceus = 2m,
                CourseId = "course1",
                EndDate = new DateTime(2015, 05, 07),
                FacultyIds = new List<string>() { "faculty11" },
                Id = "section1",
                IsActive = true,
                LearningProvider = "LearningProvider1",
                LearningProviderSiteId = "www.learningprovider.com",
                Location = "loc1",
                MaximumCredits = 4m,
                Meetings = new List<SectionMeeting2>()
                    {
                        new SectionMeeting2() { 
                            InstructionalMethodCode = "LEC",
                            StartDate = new DateTime(2015,01,11),
                            EndDate = new DateTime(2013,05,06)                    
                        }
                    },
                MinimumCredits = 2m,
                Number = "01",
                OnlyPassNoPass = true,
                OverridesCourseRequisites = true,
                PrimarySectionId = "section2",
                Requisites = new List<Requisite>() {
                        new Requisite() {
                            CompletionOrder = RequisiteCompletionOrder.Concurrent,
                            CorequisiteCourseId = "course2",
                            IsRequired = true,
                            RequirementCode = "req1"
                        }
                    },
                SectionRequisites = new List<SectionRequisite>() {
                        new SectionRequisite() {
                            CorequisiteSectionIds = new List<string>() { "section3"},
                            IsRequired = true,
                            NumberNeeded = 1
                        }
                    },
                StartDate = new DateTime(2015, 01, 05),
                TermId = "term1",
                Title = "Section1 Title",
                VariableCreditIncrement = 1,
                WaitlistAvailable = true,
                Waitlisted = 2
            };

            baseSection2 = new Section3()
            {
                ActiveStudentIds = new List<string>() { "0000001", "0000002" },
                AllowAudit = true,
                AllowPassNoPass = true,
                Available = 3,
                Books = new List<SectionBook>() { 
                        new SectionBook() {BookId = "book1", IsRequired = true},
                        new SectionBook() {BookId = "book2", IsRequired = false}
                    },
                Capacity = 5,
                Ceus = 2m,
                CourseId = "course1",
                EndDate = new DateTime(2015, 05, 07),
                FacultyIds = new List<string>() { "faculty11" },
                Id = "section2",
                IsActive = true,
                LearningProvider = "LearningProvider1",
                LearningProviderSiteId = "www.learningprovider.com",
                Location = "loc1",
                MaximumCredits = 4m,
                Meetings = new List<SectionMeeting2>()
                    {
                        new SectionMeeting2() { 
                            InstructionalMethodCode = "LEC",
                            StartDate = new DateTime(2015,01,11),
                            EndDate = new DateTime(2013,05,06)                    
                        }
                    },
                MinimumCredits = 2m,
                Number = "02",
                OnlyPassNoPass = true,
                OverridesCourseRequisites = true,
                PrimarySectionId = "section2",
                Requisites = new List<Requisite>() {
                        new Requisite() {
                            CompletionOrder = RequisiteCompletionOrder.Concurrent,
                            CorequisiteCourseId = "course2",
                            IsRequired = true,
                            RequirementCode = "req1"
                        }
                    },
                SectionRequisites = new List<SectionRequisite>() {
                        new SectionRequisite() {
                            CorequisiteSectionIds = new List<string>() { "section3"},
                            IsRequired = true,
                            NumberNeeded = 1
                        }
                    },
                StartDate = new DateTime(2015, 01, 05),
                TermId = "CURRENT",
                Title = "Section2 Title",
                VariableCreditIncrement = 1,
                WaitlistAvailable = true,
                Waitlisted = 2
            };

            baseInstructionalMethods = new List<InstructionalMethod>() {
                new InstructionalMethod() {Code = "LEC", Description = "Lecture"}
            };

            baseGrades = new List<Grade>() {
                new Grade() {Id="A", GradeSchemeCode = "UG", IsWithdraw = false},
                new Grade() {Id="B", GradeSchemeCode = "UG", IsWithdraw = false},
                new Grade() {Id="C", GradeSchemeCode = "UG", IsWithdraw = false},
                new Grade() {Id="D", GradeSchemeCode = "UG", IsWithdraw = false},
                new Grade() {Id="F", GradeSchemeCode = "UG", IsWithdraw = false},
                new Grade() {Id="W", GradeSchemeCode = "UG", IsWithdraw = true},
            };

            futureSectionRegistrationDates = new SectionRegistrationDate()
            {
                SectionId = "section1",
                PreRegistrationStartDate = DateTime.Today.AddDays(30),
                PreRegistrationEndDate = DateTime.Today.AddDays(45),
                RegistrationStartDate = DateTime.Today.AddDays(60),
                RegistrationEndDate = DateTime.Today.AddDays(75),
                AddStartDate = DateTime.Today.AddDays(90),
                AddEndDate = DateTime.Today.AddDays(105),
                DropStartDate = DateTime.Today.AddDays(120),
                DropEndDate = DateTime.Today.AddDays(135)
            };


        }
    }
}
