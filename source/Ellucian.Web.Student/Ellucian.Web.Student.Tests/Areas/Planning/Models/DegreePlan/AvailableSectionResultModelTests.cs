﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models
{
    [TestClass]
    public class AvailableSectionResultModelTests
    {
        IEnumerable<Location> locations;
        IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty;
        IEnumerable<Building> buildings;
        IEnumerable<Room> rooms;
        IEnumerable<InstructionalMethod> instMethods;
        IEnumerable<Section3> sections;
        IEnumerable<Course2> courses;
        IEnumerable<PlannedCourseSearch> plannedCourses;
        IEnumerable<TopicCode> topics;
        CoursePage2 coursePage;

        [TestInitialize]
        public void Initialize()
        {
            locations = new List<Location>() {new Location() {Code = "MC", Description = "Main Campus"}};
            faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>() { new Ellucian.Colleague.Dtos.Student.Faculty() { Id = "0001111", FirstName = "Bob", LastName = "Smith" } };
            buildings = new List<Building>() {new Building() {Code = "HALL", Description = "Whatever Hall"}};
            rooms = new List<Room>() {new Room() {Code = "111", BuildingCode = "HALL"}};
            instMethods = new List<InstructionalMethod>() {new InstructionalMethod() {Code = "LEC", Description = "Lecture"}};
            topics = new List<TopicCode>() { new TopicCode { Code = "VL", Description = "Victorian Literature" }, new TopicCode { Code = "RE", Description = "Real Estate" } };
            var section1 = new Section3() { Id = "1", CourseId = "11111", Title = "Section Test Title 1", FacultyIds = new List<string>(), Available = 0};
            section1.Meetings = new List<SectionMeeting2>() {new SectionMeeting2(){StartTime = new DateTimeOffset(new DateTime(2014,1,1,10,0,0)), EndTime = new DateTimeOffset(new DateTime(2014,1,1,11,0,0)), Days = new List<DayOfWeek>() {System.DayOfWeek.Monday, System.DayOfWeek.Wednesday}, InstructionalMethodCode = "LEC" }};
            var section2 = new Section3() { Id = "2", CourseId = "11111", Title = "Section Test Title 2", FacultyIds = new List<string>(), Available = 3};
            section2.Meetings = new List<SectionMeeting2>() { new SectionMeeting2() { StartTime = new DateTimeOffset(new DateTime(2014,1,1,8,0,0)), EndTime = new DateTimeOffset(new DateTime(2014,1,1,9,0,0)), Days = new List<DayOfWeek>() { System.DayOfWeek.Monday, System.DayOfWeek.Wednesday }, InstructionalMethodCode = "LEC" } };
            var section3 = new Section3() { Id = "3", CourseId = "11111", Title = "Section Test Title 3", FacultyIds = new List<string>(), Available = 5};
            section3.Meetings = new List<SectionMeeting2>() { new SectionMeeting2() { StartTime = new DateTimeOffset(new DateTime(2014,1,1,10, 0, 0)), EndTime = new DateTimeOffset(new DateTime(2014,1,1,11, 0, 0)), Days = new List<DayOfWeek>() { System.DayOfWeek.Tuesday, System.DayOfWeek.Thursday }, InstructionalMethodCode = "LEC" } };
            var section4 = new Section3() { Id = "4", CourseId = "22222", Title = "Section Test Title 4", FacultyIds = new List<string>(), Available = 25};
            section3.Meetings = new List<SectionMeeting2>() { new SectionMeeting2() { StartTime = new DateTimeOffset(new DateTime(2014,1,1,10, 0, 0)), EndTime = new DateTimeOffset(new DateTime(2014,1,1,11, 0, 0)), Days = new List<DayOfWeek>() { System.DayOfWeek.Tuesday, System.DayOfWeek.Thursday }, InstructionalMethodCode = "LEC" } };
            sections = new List<Section3>() {section1, section2, section3};
            var course1 = new Course2() { Id = "11111", SubjectCode = "ENGL", Number = "100", Title = "Freshman Engligh" };
            var course2 = new Course2() { Id = "22222", SubjectCode = "ART", Number = "100", Title = "Beginning Art" };
            courses = new List<Course2> { course1, course2 };
            
            // Set up planned course search objects for which available sections are requested
            var plannedCourse1 = new PlannedCourseSearch() { CourseId = "11111", SectionId = "2"};
            var plannedCourse2 = new PlannedCourseSearch() { CourseId = "22222"};
            plannedCourses = new List<PlannedCourseSearch>() { plannedCourse1, plannedCourse2 };

            // Build a search result that contains the 4 sections above for the two courses and empty filters
            CourseSearch2 courseSearch1 = new CourseSearch2() {Id = "11111", MatchingSectionIds = new List<string>() {"1", "2", "3"} };
            CourseSearch2 courseSearch2 = new CourseSearch2() { Id = "22222", MatchingSectionIds = new List<string>() {"4" } };
            coursePage = new CoursePage2() {  CurrentPageIndex = 1, PageSize = Int16.MaxValue, TotalPages = 1, TotalItems = 2,  CurrentPageItems = new List<CourseSearch2>() { courseSearch1, courseSearch2 }};
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AvailableSectionResultModel_NullCoursePage()
        {
            var availableSectionResultModel = new AvailableSectionResultModel(null, plannedCourses, false, courses, sections, locations, faculty, buildings, rooms, instMethods, topics);
        }

        [TestMethod]
        public void AvailableSectionResultModel_TwoResults()
        {
            var availableSectionResultModel = new AvailableSectionResultModel(coursePage, plannedCourses, false, courses, sections, locations, faculty, buildings, rooms, instMethods, topics);
            Assert.AreEqual(2, availableSectionResultModel.AvailableSectionResults.Count);
        }

        [TestMethod]
        public void AvailableSectionResultModel_DoesNotIncludePlannedSection()
        {
            var availableSectionResultModel = new AvailableSectionResultModel(coursePage, plannedCourses, false, courses, sections, locations, faculty, buildings, rooms, instMethods, topics);
            var course1Result = availableSectionResultModel.AvailableSectionResults.Where(x => x.CourseId == "11111").FirstOrDefault();
            Assert.AreEqual(2, course1Result.AvailableSections.Count);
        }

        [TestMethod]
        public void AvailableSectionResultModel_AvailableOnly()
        {
            var availableSectionResultModel = new AvailableSectionResultModel(coursePage, plannedCourses, true, courses, sections, locations, faculty, buildings, rooms, instMethods, topics);
            var course1Result = availableSectionResultModel.AvailableSectionResults.Where(x => x.CourseId == "11111").FirstOrDefault();
            Assert.AreEqual(1, course1Result.AvailableSections.Count);
        }
    }
}
