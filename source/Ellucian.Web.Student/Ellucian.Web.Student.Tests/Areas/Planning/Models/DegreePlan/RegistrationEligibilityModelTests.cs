﻿// Copyright 2014 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.DegreePlan
{
    [TestClass]
    public class RegistrationEligibilityModelTests
    {
        [TestClass]
        public class RegistrationEligibilityModelConstructor
        {
            [TestInitialize]
            public void Initialize()
            {

            }

            [TestMethod]
            public void RegistrationEligibilityModelConstructor_Success()
            {
                var eligibility = new RegistrationEligibilityModel();

                Assert.IsNotNull(eligibility.Notifications);
                Assert.AreEqual(0, eligibility.Notifications.Count);
                Assert.IsNotNull(eligibility.Terms);
                Assert.AreEqual(0, eligibility.Terms.Count);
            }
        }
    }
}
