﻿// Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.DegreePlan
{
    [TestClass]
    public class PlanSectionMeetingTimeModelTests
    {
        [TestClass]
        public class PlannedSectionMeetingTimeModelConstructor
        {
            private PlannedSectionMeetingTimeModel meetingTimeModel;
            private SectionMeeting2 sectionMeetingDto;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private List<Room> rooms;
            private List<Building> buildings;
            private List<InstructionalMethod> instructionalMethods;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                logger = new Mock<ILogger>().Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                var plannedCourseAdapter = new AutoMapperAdapter<Course2, PlannedCourseModel>(adapterRegistry, logger);
                adapterRegistryMock.Setup(a => a.GetAdapter<Course2, PlannedCourseModel>()).Returns(plannedCourseAdapter);

                rooms = new List<Room>() {
                   new Room() {Id = "1", Code = "ROOM1", BuildingCode = "BLDG1"},
                   new Room() {Id = "2", Code = "ROOM2", BuildingCode = "BLDG1"},
                   new Room() {Id = "3", Code = "ROOM3", BuildingCode = "BLDG2"}
                };
                buildings = new List<Building>()
                {
                    new Building() {Code = "BLDG1", Description = "Building 1"},
                    new Building() {Code = "BLDG2", Description = "Building 2"}
                };
                instructionalMethods = new List<InstructionalMethod>() {
                    new InstructionalMethod() {Code = "LEC", Description = "Lecture"},
                    new InstructionalMethod() {Code = "LAB", Description = "Laboratory"}
                };

                var startDateTime = new DateTime(2014, 01, 15, 10, 15, 00);
                DateTimeOffset? startTime = new DateTimeOffset(startDateTime, TimeZoneInfo.Local.GetUtcOffset(startDateTime));
                var endDateTime = new DateTime(2014, 05, 15, 11, 55, 00);
                DateTimeOffset? endTime = new DateTimeOffset(endDateTime, TimeZoneInfo.Local.GetUtcOffset(startDateTime));

                sectionMeetingDto = new SectionMeeting2()
                {
                    Days = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday },
                    EndDate = endDateTime,
                    EndTime = endTime,
                    InstructionalMethodCode = "LEC",
                    IsOnline = true,
                    Room = "1",
                    StartDate = startDateTime,
                    StartTime = startTime
                };

                meetingTimeModel = new PlannedSectionMeetingTimeModel(sectionMeetingDto, instructionalMethods, rooms, buildings);
                
            }

            [TestMethod]
            public void InstructionalMethod()
            {
                Assert.AreEqual(instructionalMethods.Where(i=>i.Code == sectionMeetingDto.InstructionalMethodCode).First().Description, meetingTimeModel.InstructionalMethod);
            }

            [TestMethod]
            public void StartTime()
            {
                Assert.AreEqual(sectionMeetingDto.StartTime.Value.LocalDateTime.ToShortTimeString(), meetingTimeModel.StartTime);
            }

            [TestMethod]
            public void EndTime()
            {
                Assert.AreEqual(sectionMeetingDto.EndTime.Value.LocalDateTime.ToShortTimeString(), meetingTimeModel.EndTime);                
            }

            [TestMethod]
            public void Days()
            {
                CollectionAssert.AreEqual(sectionMeetingDto.Days.ToList(), meetingTimeModel.Days.ToList());    
            }

            [TestMethod]
            public void Dates()
            {
                Assert.AreEqual(sectionMeetingDto.StartDate, meetingTimeModel.StartDate);
                Assert.AreEqual(sectionMeetingDto.EndDate, meetingTimeModel.EndDate);
            }

            [TestMethod]
            public void IsOnline()
            {
                Assert.IsTrue(sectionMeetingDto.IsOnline);
            }

            [TestMethod]
            public void RoomAndBuilding()
            {
                var room = rooms.Where(r => r.Id == sectionMeetingDto.Room).First();
                Assert.AreEqual(room.Code, meetingTimeModel.Room);
                Assert.AreEqual(buildings.Where(b => b.Code == room.BuildingCode).First().Description, meetingTimeModel.Building);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ThrowsExceptionForNullMeeting()
            {
                sectionMeetingDto = null;
                meetingTimeModel = new PlannedSectionMeetingTimeModel(sectionMeetingDto, instructionalMethods, rooms, buildings);
            }
        }
    }
}