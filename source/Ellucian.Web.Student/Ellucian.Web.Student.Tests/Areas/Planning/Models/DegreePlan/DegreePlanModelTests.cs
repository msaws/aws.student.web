﻿// Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.DegreePlan
{
    [TestClass]
    public class DegreePlanModelTests
    {
        [TestClass]
        public class DegreePlanConstructor : DegreePlanModelTestsBase
        {
            private DegreePlanModel degreePlanModel;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private DegreePlan4 degreePlan;
            private AcademicHistory3 academicHistory;
            private IEnumerable<Term> terms;
            private IEnumerable<Term> registrationTerms;
            private IEnumerable<Course2> courses;
            private IEnumerable<Section3> sections;
            private IEnumerable<Ellucian.Colleague.Dtos.Student.Faculty> faculty;
            private IEnumerable<Room> rooms;
            private IEnumerable<Building> buildings;
            private IEnumerable<InstructionalMethod> instructionalMethods;
            private IEnumerable<Grade> grades = new List<Grade>();
            private List<Student.Models.Notification> notifications;
            private Dictionary<string, string> requirementsMapping;
            private IEnumerable<Location> locations;
            private IEnumerable<Student.Areas.Planning.Models.Advising.AdvisingNote> advisingNotes;
            private Advisor lastReviewedBy;
            private List<Advisor> studentAdvisors;
            private Advisor advisor;
            private List<Advisement> studentAdvisements;
            private List<AdvisorType> advisorTypes;
            private List<SectionRegistrationDate> sectionRegistrationDates;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                logger = new Mock<ILogger>().Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                var plannedCourseAdapter = new AutoMapperAdapter<Course2, PlannedCourseModel>(adapterRegistry, logger);
                adapterRegistryMock.Setup(a => a.GetAdapter<Course2, PlannedCourseModel>()).Returns(plannedCourseAdapter);

                degreePlan = new DegreePlan4();
                academicHistory = new AcademicHistory3() { NonTermAcademicCredits = new List<AcademicCredit2>() };
                terms = new List<Term>()
                    {                     
                        new Term() {
                            Code = "2014/WI",
                            Description = "Spring 2014",
                            ReportingYear = 2014,
                            Sequence = 3,
                            StartDate = new DateTime(2014,01,06),
                            EndDate = new DateTime(2014,01,24),
                            DefaultOnPlan = true
                        },
                        new Term() {
                            Code = "2013/WI",
                            Description = "Winter 2013",
                            ReportingYear = 2013,
                            Sequence = 1,
                            StartDate = new DateTime(2013,01,07),
                            EndDate = new DateTime(2013,01,23),
                            DefaultOnPlan = true
                        },
                        new Term() {
                            Code = "2013/FA",
                            Description = "Fall 2013",
                            ReportingYear = 2013,
                            Sequence = 2,
                            StartDate = new DateTime(2013,08,15),
                            EndDate = new DateTime(2013,12,07),
                            DefaultOnPlan = true
                        },
                        new Term() {
                            Code = "FUTURE",
                            Description = "Future term",
                            ReportingYear = DateTime.Today.Year + 2,
                            Sequence = 2,
                            StartDate = new DateTime(DateTime.Today.Year + 2, 01, 15),
                            EndDate = new DateTime(DateTime.Today.Year + 2, 05, 15),
                            DefaultOnPlan = true
                        },  
                        new Term() {
                            Code = "FUTURE_NOTDEFAULT",
                            Description = "Future term",
                            ReportingYear = DateTime.Today.Year + 2,
                            Sequence = 2,
                            StartDate = new DateTime(DateTime.Today.Year + 2, 01, 15),
                            EndDate = new DateTime(DateTime.Today.Year + 2, 05, 15),
                            DefaultOnPlan = false
                        },  
                        new Term() {
                            Code = "FUTURE_NOTONPLAN",
                            Description = "Future term",
                            ReportingYear = DateTime.Today.Year + 2,
                            Sequence = 2,
                            StartDate = new DateTime(DateTime.Today.Year + 2, 01, 15),
                            EndDate = new DateTime(DateTime.Today.Year + 2, 05, 15),
                            DefaultOnPlan = false
                        },  
                        new Term() {
                            Code = "CURRENT",
                            Description = "Current Term",
                            ReportingYear = DateTime.Today.Year,
                            Sequence = 5,
                            StartDate = DateTime.Today.Subtract(new TimeSpan(78,0,0)),
                            EndDate = DateTime.Today.AddDays(30),
                            DefaultOnPlan = true
                        },
                    };
                registrationTerms = new List<Term>() {
                    new Ellucian.Colleague.Dtos.Student.Term() { Code = "CURRENT" }
                };
                courses = new List<Course2>();
                sections = new List<Section3>();
                faculty = new List<Ellucian.Colleague.Dtos.Student.Faculty>();
                rooms = new List<Room>();
                buildings = new List<Building>();
                instructionalMethods = new List<InstructionalMethod>();
                notifications = new List<Student.Models.Notification>();
                requirementsMapping = new Dictionary<string, string>();
                locations = new List<Location>();
                advisingNotes = new List<Student.Areas.Planning.Models.Advising.AdvisingNote>();
                lastReviewedBy = new Advisor();
                advisor = new Advisor() { Id = "222", LastName = "SecondAdvisor", FirstName = "William", EmailAddresses = new List<string>() { "will@advising.com" } };
                studentAdvisors = new List<Advisor>() { 
                    new Advisor() { Id = "111", LastName = "FirstAdvisor", FirstName = "Rachel"}, 
                    advisor
                };
                studentAdvisements = new List<Advisement>() {
                    new Advisement() { AdvisorId = "111", StartDate = DateTime.Now},
                    new Advisement() { AdvisorId = "222", StartDate = DateTime.Now, AdvisorType = "MAJ"},
                    new Advisement() { AdvisorId = "Missing", StartDate = DateTime.Now}
                };
                advisorTypes = new List<AdvisorType>()
                {
                    new AdvisorType() {Code = "ABC", Description = "Some Type"},
                    new AdvisorType() {Code = "MAJ", Description = "Major Advisor"},
                    new AdvisorType() {Code = "MIN", Description = "Minor Advisor"}
                };

                degreePlan = new DegreePlan4()
                {
                    PersonId = "0000012",
                    Terms = new List<DegreePlanTerm4>()
                    {
                        new DegreePlanTerm4() { TermId = "2013/FA", PlannedCourses = new List<PlannedCourse4>()},
                        new DegreePlanTerm4() {TermId = "2014/SP", PlannedCourses = new List<PlannedCourse4>()},
                        new DegreePlanTerm4() {TermId = "FUTURE_NOTDEFAULT", PlannedCourses = new List<PlannedCourse4>()}
                    },
                    NonTermPlannedCourses = new List<PlannedCourse4>()
                };

                sectionRegistrationDates = new List<SectionRegistrationDate>()
                {
                    new SectionRegistrationDate() { SectionId = "section1",
                                                    PreRegistrationStartDate = DateTime.Today.AddDays(30),
                                                    PreRegistrationEndDate = DateTime.Today.AddDays(45),
                                                    RegistrationStartDate = DateTime.Today.AddDays(60),
                                                    RegistrationEndDate = DateTime.Today.AddDays(75),
                                                    AddStartDate = DateTime.Today.AddDays(90),
                                                    AddEndDate = DateTime.Today.AddDays(105),
                                                    DropStartDate = DateTime.Today.AddDays(120),
                                                    DropEndDate = DateTime.Today.AddDays(135)
                                                  }
                };
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanModel = null;
            }

            // Should this be checking for this explicitly? throwing exception through linq
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ThrowsExceptionForNullDegreePlanTerms()
            {
                degreePlan = new DegreePlan4();
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
            }

            // should the model be checking for these explicitly? throwing exception through linq
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void throwsExceptionForNullNonTermPlannedCourses()
            {
                degreePlan = new DegreePlan4()
                {
                    Terms = new List<DegreePlanTerm4>()
                    {
                        new DegreePlanTerm4() { TermId = "2013/FA", PlannedCourses = new List<PlannedCourse4>()},
                    },
                };
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
            }

            // Should the model be throwing an error if there is no person id?
            [TestMethod]
            public void CreatesMinimalDegreePlan()
            {
                degreePlan = new DegreePlan4()
                {
                    Terms = new List<DegreePlanTerm4>()
                    {
                        new DegreePlanTerm4() { TermId = "2013/FA", PlannedCourses = new List<PlannedCourse4>()},
                    },
                    NonTermPlannedCourses = new List<PlannedCourse4>()
                };
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                Assert.AreEqual(degreePlan.Terms.Count, degreePlanModel.Terms.Count);
                Assert.AreEqual(degreePlan.PersonId, degreePlanModel.PersonId);
            }

            [TestMethod]
            public void ConvertsLastReviewedInformation()
            {
                degreePlan = new DegreePlan4()
                {
                    Terms = new List<DegreePlanTerm4>()
                    {
                        new DegreePlanTerm4() { TermId = "2013/FA", PlannedCourses = new List<PlannedCourse4>()},
                    },
                    NonTermPlannedCourses = new List<PlannedCourse4>(),
                    LastReviewedDate = new DateTime(2013, 09, 10)
                };
                var lastReviewedBy = new Advisor()
                {
                    LastName = "Swanson"
                };
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                Assert.IsTrue(degreePlanModel.LastReviewedBy.Contains(lastReviewedBy.LastName));
                Assert.AreEqual(degreePlan.LastReviewedDate.GetValueOrDefault().ToShortDateString(), degreePlanModel.LastReviewedDate);
            }

            [TestMethod]
            public void Notifications()
            {
                degreePlan = new DegreePlan4()
                {
                    Terms = new List<DegreePlanTerm4>()
                    {
                        new DegreePlanTerm4() { TermId = "2013/FA", PlannedCourses = new List<PlannedCourse4>()},
                    },
                    NonTermPlannedCourses = new List<PlannedCourse4>(),
                };
                notifications = new List<Student.Models.Notification>() {
                    new Student.Models.Notification(){
                        Message = "warning notification",
                        Type = "Warning",
                        Flash = true
                    },
                    new Student.Models.Notification(){
                        Message = "error notification",
                        Type = "Error",
                        Flash = false
                    }
                };
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                Assert.AreEqual(2, degreePlanModel.Notifications.Count);
            }

            [TestMethod]
            public void StudentAdvising()
            {
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                Assert.AreEqual(2, degreePlanModel.AdvisingContacts.Count);
                // Test each contact's info.
                var studentAdvisingModel1 = degreePlanModel.AdvisingContacts.ElementAt(0);
                Assert.IsNull(studentAdvisingModel1.EmailAddress);
                Assert.AreEqual("Rachel FirstAdvisor", studentAdvisingModel1.Name);
                Assert.AreEqual(string.Empty, studentAdvisingModel1.AdvisorType);
                var studentAdvisingModel2 = degreePlanModel.AdvisingContacts.ElementAt(1);
                Assert.AreEqual("will@advising.com", studentAdvisingModel2.EmailAddress);
                Assert.AreEqual("William SecondAdvisor", studentAdvisingModel2.Name);
                Assert.AreEqual("(Major Advisor)", studentAdvisingModel2.AdvisorType);
            }

            // This test must be ignored because global resources are inaccessible and this specific scenario... with a planned
            // course in a prior term... tries to format a string from global resources with arguments.
            [TestMethod]
            [Ignore]
            public void CoursePlanningStatus_PlannedCoursesInEndedTerm_AssignedUndeterminedStatus()
            {
                // Arrange
                var termCode = "2013/FA";
                degreePlan = BuildDegreePlanWithTerms(new List<string>() { termCode });
                var courseId = "11";
                // Course is on plan for prior term
                var degreePlanTerm = degreePlan.Terms.Where(t => t.TermId == termCode).First();
                degreePlanTerm.PlannedCourses.Add(BuildPlannedCourse4Dto(courseId, termCode, string.Empty));
                // Initialize list of courses with the needed courses
                courses = new List<Course2>() { BuildCourse2Dto(courseId) };

                // Act - Build degree plan model
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);

                // Assert - Course Planning Status for the course should be "Undetermined"
                var planningStatus = degreePlanModel.PlanningStatuses.Where(ps => ps.CourseId == courseId).FirstOrDefault();
                Assert.AreEqual(PlannedCoursePlanningStatus.Undetermined.ToString(), planningStatus.ToString());
            }

            [TestMethod]
            public void CoursePlanningStatus_InProgressAndPlannedInCurrentTerm_IsInProgress()
            {
                // Arrange
                degreePlan = BuildDegreePlanWithTerms(new List<string>() { "CURRENT" });
                var courseId = "course1";
                // Add course twice to force an extraneous planned course in degree plan model
                degreePlan.Terms.ElementAt(0).PlannedCourses.Add(BuildPlannedCourse4Dto(courseId, "CURRENT", string.Empty));
                degreePlan.Terms.ElementAt(0).PlannedCourses.Add(BuildPlannedCourse4Dto(courseId, "CURRENT", string.Empty));
                // Initialize list of courses with the needed courses
                courses = new List<Course2>() { BuildCourse2Dto(courseId) };
                // Initialize list of sections with the needed sections
                sections = new List<Section3>() { baseSection }; // Use section2 built by base model
                // Add in progress academic credit to the academic history
                academicHistory.AcademicTerms.Add(new AcademicTerm3() { TermId = "CURRENT" });
                var sectionId = "section1";
                academicHistory.AcademicTerms.ElementAt(0).AcademicCredits.Add(new AcademicCredit2() { CourseId = courseId, IsCompletedCredit = false, SectionId = sectionId });

                // Act - Build degree plan model
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, baseInstructionalMethods, baseGrades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);

                // Assert - Course Planning Status for the course should be "InProgress"
                var planningStatus = degreePlanModel.PlanningStatuses.Where(ps => ps.CourseId == courseId).FirstOrDefault();
                Assert.AreEqual("search-course-in-progress-banner", planningStatus.Class);
            }

            [TestMethod]
            public void CoursePlanningStatus_CompletedFollowedByWithdrawn_IsWithdrawn()
            {
                // Arrange
                degreePlan = BuildDegreePlanWithTerms(terms.Select(t => t.Code).ToList());
                var courseId = "course1";
                var sectionId = "section1";
                // Initialize list of courses with the needed courses
                courses = new List<Course2>() { BuildCourse2Dto(courseId) };
                // Initialize list of sections with the needed sections
                sections = new List<Section3>() { baseSection }; // Use section2 built by base model
                // Add completed academic credit to the academic history
                academicHistory.AcademicTerms.Add(new AcademicTerm3() { TermId = "2013/WI" });
                academicHistory.AcademicTerms.ElementAt(0).AcademicCredits.Add(new AcademicCredit2() { CourseId = courseId, IsCompletedCredit = true, SectionId = sectionId });
                academicHistory.AcademicTerms.Add(new AcademicTerm3() { TermId = "2013/FA" });
                // Adding the same section for a different term... the model doesn't know the difference
                academicHistory.AcademicTerms.ElementAt(1).AcademicCredits.Add(new AcademicCredit2() { CourseId = courseId, IsCompletedCredit = true, SectionId = sectionId, VerifiedGradeId = "W" });
                // Act - Build degree plan model. Use Base Grades because we actually need grades to determine the withdraw grade.
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, baseInstructionalMethods, baseGrades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);

                // Assert - Course Planning Status for the course should be "IsWithdrawn"
                var planningStatus = degreePlanModel.PlanningStatuses.Where(ps => ps.CourseId == courseId).FirstOrDefault();
                Assert.AreEqual("search-course-withdrawn-banner", planningStatus.Class);
            }

            [TestMethod]
            public void DegreePlan_IncludesNonCourseAcademicCredit()
            {
                // Arrange
                degreePlan = BuildDegreePlanWithTerms(terms.Select(t => t.Code).ToList());
                // Add completed academic credit to the academic history
                academicHistory.AcademicTerms.Add(new AcademicTerm3() { TermId = "2013/FA" });
                // Add the credit with no course id and no section id
                academicHistory.AcademicTerms.ElementAt(0).AcademicCredits.Add(new AcademicCredit2() { Title = "Special Internship", Credit = 2m });
                // Act - Build degree plan model. Use Base Grades because we actually need grades to determine the withdraw grade.
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, baseInstructionalMethods, baseGrades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);

                // Assert - a planned course now exists for this academic credit on the plan
                var fallTerm = degreePlanModel.Terms.Where(t => t.Code == "2013/FA").FirstOrDefault();
                Assert.AreEqual(1, fallTerm.PlannedCourses.Count());
                var noncourse = fallTerm.PlannedCourses.Where(pc => pc.FullTitleDisplay == ": Special Internship");
                Assert.AreEqual(1, noncourse.Count());
            }

            [TestMethod]
            public void DegreePlan_AvailablePlanningTerms_IncludesNonDefaultTermOnDegreePlan()
            {
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                Assert.AreEqual(3, degreePlanModel.AvailableSamplePlanTerms.Count());
                // Future planning term that has DefaultOnPlan set to true
                Assert.IsTrue(degreePlanModel.AvailableSamplePlanTerms.Contains(terms.Where(t => t.Code == "FUTURE").First()));
                // future planning term that has DefaultOnPlan set to false and is on student's degree plan
                Assert.IsTrue(degreePlanModel.AvailableSamplePlanTerms.Contains(terms.Where(t => t.Code == "FUTURE_NOTDEFAULT").First()));
                // current planning term that ends in the future that has DefaultOnPlan set to true and is on student's plan
                Assert.IsTrue(degreePlanModel.AvailableSamplePlanTerms.Contains(terms.Where(t => t.Code == "CURRENT").First()));
            }

            [TestMethod]
            public void DegreePlan_NoClearableTerms()
            {
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                Assert.AreEqual(0, degreePlanModel.ClearableTerms.Count());
            }

            [TestMethod]
            public void DegreePlan_ClearableTerms()
            {
                //Terms are only clearable if they are not in the past and have planned courses on them.
                var degreePlanWithPlannedCourses = new DegreePlan4();
                degreePlanWithPlannedCourses.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlanWithPlannedCourses.Approvals = new List<DegreePlanApproval2>();
                degreePlanWithPlannedCourses.Terms = new List<DegreePlanTerm4>();

                degreePlanWithPlannedCourses.Terms.Add(new DegreePlanTerm4() { TermId = "CURRENT", PlannedCourses = new List<PlannedCourse4>() { new PlannedCourse4() { CourseId = "Course1", Credits = 3.0m, TermId = "CURRENT", GradingType = GradingType.Graded } } });
                degreePlanWithPlannedCourses.Terms.Add(new DegreePlanTerm4() { TermId = "FUTURE", PlannedCourses = new List<PlannedCourse4>() });
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlanWithPlannedCourses, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                Assert.AreEqual(1, degreePlanModel.ClearableTerms.Count());
            }

            [TestMethod]
            public void DegreePlan_TermAllowRegistration_NoPlannedCoursesOnTerm()
            {
                var degreePlanWithPlannedCourses = new DegreePlan4();
                degreePlanWithPlannedCourses.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlanWithPlannedCourses.Approvals = new List<DegreePlanApproval2>();
                degreePlanWithPlannedCourses.Terms = new List<DegreePlanTerm4>();

                degreePlanWithPlannedCourses.Terms.Add(new DegreePlanTerm4() { TermId = "FUTURE", PlannedCourses = new List<PlannedCourse4>() });
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlanWithPlannedCourses, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                var term1 = degreePlanModel.Terms.First();
                Assert.IsFalse(term1.AllowRegistration);
            }

            [TestMethod]
            public void DegreePlan_TermAllowRegistration_PlannedCoursesWithNoSections()
            {
                var degreePlanWithPlannedCourses = new DegreePlan4();
                degreePlanWithPlannedCourses.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlanWithPlannedCourses.Approvals = new List<DegreePlanApproval2>();
                degreePlanWithPlannedCourses.Terms = new List<DegreePlanTerm4>();

                degreePlanWithPlannedCourses.Terms.Add(new DegreePlanTerm4() { TermId = "CURRENT", PlannedCourses = new List<PlannedCourse4>() { new PlannedCourse4() { CourseId = "Course1", Credits = 3.0m, TermId = "CURRENT", GradingType = GradingType.Graded } } });
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlanWithPlannedCourses, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                var term1 = degreePlanModel.Terms.First();
                Assert.IsFalse(term1.AllowRegistration);
            }

            [TestMethod]
            public void DegreePlan_TermAllowRegistration_PlannedCoursesWithSectionAndFutureOverride()
            {
                var degreePlanWithPlannedCourses = new DegreePlan4();
                degreePlanWithPlannedCourses.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlanWithPlannedCourses.Approvals = new List<DegreePlanApproval2>();
                degreePlanWithPlannedCourses.Terms = new List<DegreePlanTerm4>();
                // Initialize list of sections with the needed sections
                sections = new List<Section3>() { baseSection }; // Use section2 built by base model
                degreePlanWithPlannedCourses.Terms.Add(new DegreePlanTerm4() { TermId = "CURRENT", PlannedCourses = new List<PlannedCourse4>() { new PlannedCourse4() { CourseId = "Course1", Credits = 3.0m, TermId = "CURRENT", GradingType = GradingType.Graded, SectionId = "section1", Warnings = new List<PlannedCourseWarning2>() } } });
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlanWithPlannedCourses, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                var term1 = degreePlanModel.Terms.First();
                Assert.IsFalse(term1.AllowRegistration);
            }

            [TestMethod]
            public void DegreePlan_TermAllowRegistration_PlannedCoursesWithSectionAndNoOverride()
            {
                var degreePlanWithPlannedCourses = new DegreePlan4();
                degreePlanWithPlannedCourses.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlanWithPlannedCourses.Approvals = new List<DegreePlanApproval2>();
                degreePlanWithPlannedCourses.Terms = new List<DegreePlanTerm4>();
                // Initialize list of sections with the needed sections
                sections = new List<Section3>() { baseSection2 }; // Use section2 built by base model
                degreePlanWithPlannedCourses.Terms.Add(new DegreePlanTerm4() { TermId = "CURRENT", PlannedCourses = new List<PlannedCourse4>() { new PlannedCourse4() { CourseId = "Course1", Credits = 3.0m, TermId = "CURRENT", GradingType = GradingType.Graded, SectionId = "section2", Warnings = new List<PlannedCourseWarning2>() } } });
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlanWithPlannedCourses, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);
                var term1 = degreePlanModel.Terms.First();
                Assert.IsTrue(term1.AllowRegistration);
            }

            [TestMethod]
            public void DegreePlan_NoTerms_ModelBuilds()
            {
                var degreePlan = new DegreePlan4();
                degreePlan.Id = 1;
                degreePlan.Version = 4;
                degreePlan.PersonId = "pId";
                degreePlan.Terms = new List<DegreePlanTerm4>();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlanModel = new DegreePlanModel(adapterRegistry, degreePlan, academicHistory, terms, registrationTerms, courses, sections, faculty, rooms, buildings, instructionalMethods, grades, notifications, requirementsMapping, locations, advisingNotes, lastReviewedBy, studentAdvisors, studentAdvisements, advisorTypes, sectionRegistrationDates);

                Assert.AreEqual(degreePlan.Id, degreePlanModel.Id);
                Assert.AreEqual(degreePlan.PersonId, degreePlanModel.PersonId);
                Assert.AreEqual(degreePlan.Version, degreePlanModel.Version);
                Assert.AreEqual(0, degreePlanModel.Terms.Count());
                Assert.AreEqual(false, degreePlanModel.HasPlannedCourses);
                Assert.IsNull(degreePlanModel.TermToShow);
            }

            private DegreePlan4 BuildDegreePlanWithTerms(List<string> termCodes)
            {
                var degreePlan = new DegreePlan4();
                degreePlan.NonTermPlannedCourses = new List<PlannedCourse4>();
                degreePlan.Approvals = new List<DegreePlanApproval2>();
                degreePlan.Terms = new List<DegreePlanTerm4>();
                foreach (var termCode in termCodes)
                {
                    degreePlan.Terms.Add(new DegreePlanTerm4() { TermId = termCode, PlannedCourses = new List<PlannedCourse4>() });
                };
                return degreePlan;
            }

            private Course2 BuildCourse2Dto(string courseId)
            {
                return new Course2()
                {
                    Id = courseId,
                    EquatedCourseIds = new List<string>(),
                    LocationCodes = new List<string>(),
                    Requisites = new List<Requisite>()
                };
            }

            private Section2 BuildSection2Dto(string courseId, string sectionId)
            {
                return new Section2()
                {
                    Id = sectionId,
                    CourseId = courseId,
                    FacultyIds = new List<string>(),
                    Meetings = new List<SectionMeeting>(),
                    Requisites = new List<Requisite>(),
                    SectionRequisites = new List<SectionRequisite>()
                };
            }

            private PlannedCourse4 BuildPlannedCourse4Dto(string courseId, string termCode, string sectionId)
            {
                return new PlannedCourse4()
                {
                    CourseId = courseId,
                    TermId = termCode,
                    SectionId = sectionId,
                    IsProtected = false
                };
            }
        }
    }
}