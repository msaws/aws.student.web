﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.Planning;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.DegreePlan
{
    [TestClass]
    public class PlannedCourseModelTests : DegreePlanModelTestsBase
    {
        private PlannedCourseModel plannedCourse;

        [TestInitialize]
        public void Initialize()
        {
            plannedCourse = new PlannedCourseModel();
        }

        [TestMethod]
        public void PlannedCourseModel_SectionIsNull_HasSectionFalse()
        {
            Assert.IsFalse(plannedCourse.HasSection);
        }

        [TestMethod]
        public void PlannedCourseModel_HasAcademicHistoryWithSection_HasRegisteredSectionTrue()
        {
            plannedCourse.AcademicHistory = new AcademicHistoryModel();
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(baseSection);
            Assert.IsTrue(plannedCourse.HasRegisteredSection);
        }

        [TestMethod]
        public void PlannedCourseModel_HasCompleteRegisteredSectionWithoutWithdrawn_IsCompletedTrue()
        {
            plannedCourse.AcademicHistory = new AcademicHistoryModel() { IsCompletedCredit = true, IsWithdrawn = false };
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(baseSection);

            // check PlanningStatus
            Assert.AreEqual(PlannedCoursePlanningStatus.IsCompleted, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_HasCompleteRegisteredSectionWithWithdrawn_IsWithdrawnTrue()
        {
            plannedCourse.AcademicHistory = new AcademicHistoryModel() { IsCompletedCredit = true, IsWithdrawn = true };
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(baseSection);

            // check PlanningStatus
            Assert.AreEqual(PlannedCoursePlanningStatus.IsWithdrawn, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_HasCompleteRegisteredSectionWithoutCompleteCredit_IsInProgressTrue()
        {
            plannedCourse.AcademicHistory = new AcademicHistoryModel();
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(baseSection);

            // check PlanningStatus
            Assert.AreEqual(PlannedCoursePlanningStatus.IsInProgress, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_HasNoAcademicHistory_IsPlannedTrue()
        {
            // check PlanningStatus
            Assert.AreEqual(PlannedCoursePlanningStatus.IsPlanned, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_HasAcademicHistoryWithoutSection_Planned()
        {
            plannedCourse.AcademicHistory = new AcademicHistoryModel();

            Assert.AreEqual(PlannedCoursePlanningStatus.IsPlanned, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_CreditsCeusDisplay_Credits()
        {
            var expected = "3";
            plannedCourse.Credits = 3;
            var result = plannedCourse.CreditsCeusDisplay;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PlannedCourseModel_CreditsCeusDisplay_MinimumCredits()
        {
            var expected = "3";
            plannedCourse.MinimumCredits = 3;
            var result = plannedCourse.CreditsCeusDisplay;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PlannedCourseModel_CreditsCeusDisplay_Credits_SectionHasMinCredits()
        {
            var expected = "3";
            plannedCourse.Credits = null;
            plannedCourse.SetSection(new PlannedSectionModel(new Section3()));
            plannedCourse.Section.MinimumCredits = 3;
            var result = plannedCourse.CreditsCeusDisplay;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [Ignore]
        public void PlannedCourseModel_CreditsCeusDisplay_CEUs()
        {
            var expected = "3";
            plannedCourse.Ceus = 3;
            var result = plannedCourse.CreditsCeusDisplay;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PlannedCourseModel_CreditsCeusDisplay_CEUs_Zero()
        {
            var expected = "0";
            plannedCourse.Ceus = 0;
            var result = plannedCourse.CreditsCeusDisplay;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [Ignore]//Can't execute this test because string formatting on null Globel resources fails
        public void PlannedCourseModel_CreditsCeusDisplay_CEUs_SectionHasMinCEUs()
        {
            var expected = "3";
            plannedCourse.Ceus = null;
            plannedCourse.SetSection(new PlannedSectionModel(new Section3()));
            plannedCourse.Section.Ceus = 3;
            var result = plannedCourse.CreditsCeusDisplay;

            Assert.AreEqual(expected, result);
        }


        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_CourseName()
        {
            var expected = "Test Title";
            plannedCourse.AcademicHistory.CourseName = expected;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_NoCourseNameSubjectAndNumber()
        {
            var subject = "Subject";
            var number = "Number";
            plannedCourse.AcademicHistory.CourseName = null;
            plannedCourse.SubjectCode = subject;
            plannedCourse.Number = number;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(subject + number, result);
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_NoCourseNameSubjectOnly()
        {
            var subject = "Subject";
            plannedCourse.AcademicHistory.CourseName = null;
            plannedCourse.SubjectCode = subject;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(subject, result);
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_NoCourseNameNumberOnly()
        {
            var number = "Number";
            plannedCourse.AcademicHistory.CourseName = null;
            plannedCourse.Number = number;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(number, result);
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_CourseName_SectionOnly()
        {
            var expected = "Test";
            plannedCourse.SetSection(new PlannedSectionModel(new Section3()));
            plannedCourse.Section.Number = expected;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_CourseName_CourseAndSection()
        {
            var expected = "Test";
            plannedCourse.AcademicHistory.CourseName = expected;
            plannedCourse.SetSection(new PlannedSectionModel(new Section3()));
            plannedCourse.Section.Number = expected;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(expected + expected, result);
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_TitleOverride()
        {
            var expected = "Test";
            plannedCourse.AcademicHistory.TitleOverride = expected;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(": " + expected, result); // The model has a hard-coded ": " to separate name/number from title
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_TitleSection()
        {
            var expected = "Test";
            plannedCourse.SetSection(new PlannedSectionModel(new Section3()));
            plannedCourse.Section.Title = expected;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(": " + expected, result); // The model has a hard-coded ": " to separate name/number from title
        }

        [TestMethod]
        public void PlannedCourseModel_FullTitleDisplay_TitleCourse()
        {
            var expected = "Test";
            plannedCourse.Title = expected;
            var result = plannedCourse.FullTitleDisplay;

            Assert.AreEqual(": " + expected, result); // The model has a hard-coded ": " to separate name/number from title
        }

        [TestMethod]
        public void PlannedCourseModel_CourseTitleDisplay()
        {
            var expected1 = "Test1";
            var expected2 = "Test2";
            var expected3 = "Test3";
            plannedCourse.SubjectCode = expected1;
            plannedCourse.Number = expected2;
            plannedCourse.Title = expected3;
            var result = plannedCourse.CourseTitleDisplay;

            Assert.AreEqual(expected1 + expected2 + ": " + expected3, result); // The model has a hard-coded ": " to separate name/number from title
        }

        [TestMethod]
        public void PlannedCourseModel_GetGradingType_NoAcademicHistory()
        {
            var expected = GradingType.Audit;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.AcademicHistory.GradingType = GradingType.Graded;
            plannedCourse.GradingType = expected;

            Assert.AreEqual(expected, plannedCourse.GetGradingType);
        }

        [TestMethod]
        public void PlannedCourseModel_GetGradingType_FromAcadmicHistory()
        {
            var expected = GradingType.PassFail;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.AcademicHistory.GradingType = expected;
            plannedCourse.GradingType = GradingType.Audit;

            Assert.AreEqual(expected, plannedCourse.GetGradingType);
        }

        [TestMethod]
        public void PlannedCourseModel_PlanningStatus_SectionNull_NoWaitlist()
        {
            var expected = PlannedCoursePlanningStatus.IsPlanned;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.SectionWaitlistStatus = WaitlistStatus.NotWaitlisted.ToString();
            
            Assert.AreEqual(expected, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_IsPlanned_True()
        {
            var expected = true;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.SectionWaitlistStatus = WaitlistStatus.NotWaitlisted.ToString();

            Assert.AreEqual(expected, plannedCourse.IsPlanned);
        }

        [TestMethod]
        public void PlannedCourseModel_IsPlanned_False()
        {
            var expected = false;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.SectionWaitlistStatus = WaitlistStatus.Active.ToString();

            Assert.AreEqual(expected, plannedCourse.IsPlanned);
        }

        [TestMethod]
        public void PlannedCourseModel_PlanningStatus_SectionNull_WaitlistActive()
        {
            var expected = PlannedCoursePlanningStatus.IsWaitlisted;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.SectionWaitlistStatus = WaitlistStatus.Active.ToString();

            Assert.AreEqual(expected, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_IsWaitlisted_True()
        {
            var expected = true;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.SectionWaitlistStatus = WaitlistStatus.Active.ToString();

            Assert.AreEqual(expected, plannedCourse.IsWaitlisted);
        }

        [TestMethod]
        public void PlannedCourseModel_IsWaitlisted_False()
        {
            var expected = false;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.SectionWaitlistStatus = WaitlistStatus.NotWaitlisted.ToString();

            Assert.AreEqual(expected, plannedCourse.IsWaitlisted);
        }

        [TestMethod]
        public void PlannedCourseModel_PlanningStatus_SectionNull_WaitlistPermission()
        {
            var expected = PlannedCoursePlanningStatus.IsWaitlisted;
            plannedCourse.AcademicHistory.Section = null;
            plannedCourse.SectionWaitlistStatus = WaitlistStatus.PermissionToRegister.ToString();

            Assert.AreEqual(expected, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_PlanningStatus_HasSection_NotCompleted_InProgress()
        {
            var expected = PlannedCoursePlanningStatus.IsInProgress;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.Section.StartDate = DateTime.Now.AddDays(-1);
            
            Assert.AreEqual(expected, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_IsInProgress_True()
        {
            var expected = true;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.Section.StartDate = DateTime.Now.AddDays(-1);

            Assert.AreEqual(expected, plannedCourse.IsInProgress);
        }

        [TestMethod]
        public void PlannedCourseModel_IsInProgress_False()
        {
            var expected = false;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.Section.StartDate = DateTime.Now.AddDays(1);

            Assert.AreEqual(expected, plannedCourse.IsInProgress);
        }

        [TestMethod]
        public void PlannedCourseModel_PlanningStatus_HasSection_NotCompleted_Preregistered()
        {
            var expected = PlannedCoursePlanningStatus.IsPreregistered;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.Section.StartDate = DateTime.Now.AddDays(1);

            Assert.AreEqual(expected, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_IsPreregistered_True()
        {
            var expected = true;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.Section.StartDate = DateTime.Now.AddDays(1);

            Assert.AreEqual(expected, plannedCourse.IsPreregistered);
        }

        [TestMethod]
        public void PlannedCourseModel_IsPreregistered_False()
        {
            var expected = false;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.Section.StartDate = DateTime.Now.AddDays(-1);

            Assert.AreEqual(expected, plannedCourse.IsPreregistered);
        }

        [TestMethod]
        public void PlannedCourseModel_PlanningStatus_HasSection_Completed()
        {
            var expected = PlannedCoursePlanningStatus.IsCompleted;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.AcademicHistory.IsCompletedCredit = true;
            plannedCourse.AcademicHistory.IsWithdrawn = false;

            Assert.AreEqual(expected, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_IsCompleted_True()
        {
            var expected = true;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.AcademicHistory.IsCompletedCredit = true;
            plannedCourse.AcademicHistory.IsWithdrawn = false;

            Assert.AreEqual(expected, plannedCourse.IsCompleted);
        }

        [TestMethod]
        public void PlannedCourseModel_IsCompleted_False()
        {
            var expected = false;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.AcademicHistory.IsCompletedCredit = true;
            plannedCourse.AcademicHistory.IsWithdrawn = true;
            Assert.AreEqual(expected, plannedCourse.IsCompleted);
        }

        [TestMethod]
        public void PlannedCourseModel_PlanningStatus_HasSection_Withdrawn()
        {
            var expected = PlannedCoursePlanningStatus.IsWithdrawn;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.AcademicHistory.IsCompletedCredit = true;
            plannedCourse.AcademicHistory.IsWithdrawn = true;

            Assert.AreEqual(expected, plannedCourse.PlanningStatus);
        }

        [TestMethod]
        public void PlannedCourseModel_IsWithdrawn_True()
        {
            var expected = true;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.AcademicHistory.IsCompletedCredit = true;
            plannedCourse.AcademicHistory.IsWithdrawn = true;
            Assert.AreEqual(expected, plannedCourse.IsWithdrawn);
        }

        [TestMethod]
        public void PlannedCourseModel_IsWithdrawn_False()
        {
            var expected = false;
            plannedCourse.AcademicHistory.Section = new PlannedSectionModel(new Section3());
            plannedCourse.AcademicHistory.IsCompletedCredit = true;
            plannedCourse.AcademicHistory.IsWithdrawn = false;
            Assert.AreEqual(expected, plannedCourse.IsWithdrawn);
        }
    }
}
