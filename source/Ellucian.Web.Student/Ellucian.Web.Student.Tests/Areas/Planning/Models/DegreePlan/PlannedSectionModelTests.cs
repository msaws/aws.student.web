﻿// Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.DegreePlan
{
    [TestClass]
    public class PlannedSectionModelTests
    {
        [TestClass]
        public class PlannedSectionConstructor : DegreePlanModelTestsBase
        {
            private PlannedSectionModel plannedSectionModel;

            [TestInitialize]
            public void Initialize()
            {

            }

            [TestMethod]
            public void PlannedSectionModelConstructor()
            {
                plannedSectionModel = new PlannedSectionModel(baseSection);
                Assert.AreEqual(baseSection.AllowAudit, plannedSectionModel.AllowAudit);
                Assert.AreEqual(baseSection.AllowPassNoPass, plannedSectionModel.AllowPassNoPass);
                Assert.AreEqual(baseSection.Available, plannedSectionModel.Available);
                Assert.AreEqual(baseSection.Books, plannedSectionModel.Books);
                Assert.AreEqual(baseSection.Capacity, plannedSectionModel.Capacity);
                Assert.AreEqual(baseSection.Ceus, plannedSectionModel.Ceus);
                Assert.AreEqual(baseSection.CourseId, plannedSectionModel.CourseId);

            }
        }
    }
}