﻿// Copyright 2014 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Student.Areas.Planning.Models.DegreePlan;
using Microsoft.VisualStudio.TestTools.UnitTesting;
        
namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.DegreePlan
{
    [TestClass]
    public class TermEligibilityModelTests
    {

        [TestClass]
        public class TermEligibilityModelConstructor
        {
            [TestInitialize]
            public void Initialize()
            {

            }

            [TestMethod]
            public void TermEligibilityModelConstructor_Success()
            {
                //public TermEligibilityModel(string term, string message, bool allowRegistration)

                var term = "TestTerm";
                var message = "Test Message";
                var allow = true;
                var allowSkipWaitlist = false;
                var eligibility1 = new TermEligibilityModel(term, message, allow, allowSkipWaitlist);

                Assert.AreEqual(term, eligibility1.Term);
                Assert.AreEqual(message, eligibility1.Message);
                Assert.AreEqual(allow, eligibility1.AllowRegistration);
                Assert.AreEqual(allowSkipWaitlist, eligibility1.AllowSkippingWaitlist);

            }
        }
    }
}