﻿using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.Requirements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Requirements
{
    [TestClass]
    public class StudentProgramModelTests
    {
        private Mock<Ellucian.Web.Adapters.IAdapterRegistry> adapterRegistryMock;
        private IAdapterRegistry adapterRegistry;
        private Program program;
        private StudentProgram studentProgram;
        private ProgramEvaluation3 result;
        private List<AcademicCreditModel> acadCredits;
        private List<AcademicCreditModel> plannedCourses;
        private List<Course2> courses;
        private List<Subject> subjects;
        private List<Department> departments;

        [TestInitialize]
        public void Initialize()
        {
            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;

            program = new Program();
            studentProgram = new StudentProgram();
            studentProgram.AdditionalRequirements = new List<AdditionalRequirement>();
            result = new ProgramEvaluation3();
            result.ProgramRequirements = new ProgramRequirements();
            result.ProgramRequirements.Requirements = new List<Requirement>();
            result.OtherPlannedCredits = new List<PlannedCredit>();
            result.OtherAcademicCredits = new List<string>();
            acadCredits = new List<AcademicCreditModel>();
            plannedCourses = new List<AcademicCreditModel>();
            courses = new List<Course2>();
            subjects = new List<Subject>();
            departments = new List<Department>();

            // Abandoned attempt to test beyond this area. Problem comes when getting deeper into the requirement/subrequirement constructors, and 
            // trying to retrieve text from Global.Resource. It requires HttpContext to be set, and we have not figured out a way to mock that.
            //result = BuildProgramEvaluationResult();
            //acadCredits = BuildAcadCreditModelList(10);
            //plannedCourses = BuildAcadCreditModelList(20);
        }

        [TestMethod][Ignore]
        public void GpaRoundedUpToThreeDecimalPlaces()
        {
            result.ProgramRequirements.MinOverallGpa = 3.0155m;
            result.ProgramRequirements.MinInstGpa = 3.0256m;
            result.CumGpa = 3.4557m;
            result.InstGpa = 3.5559m;
            var studentProgramModel = new StudentProgramModel(adapterRegistry, program, studentProgram, result, acadCredits, plannedCourses, courses, subjects, departments);
            Assert.AreEqual("3.016", studentProgramModel.MinimumOverallGpa);
            Assert.AreEqual("3.026", studentProgramModel.MinimumInstitutionGpa);
            Assert.AreEqual("3.456", studentProgramModel.CumulativeGpa);
            Assert.AreEqual("3.556", studentProgramModel.InstitutionGpa);
        }

        [TestMethod][Ignore]
        public void GpaRoundedDownToThreeDecimalPlaces()
        {
            result.ProgramRequirements.MinOverallGpa = 3.0151m;
            result.ProgramRequirements.MinInstGpa = 3.0252m;
            result.CumGpa = 3.4553m;
            result.InstGpa = 3.5554m;
            var studentProgramModel = new StudentProgramModel(adapterRegistry, program, studentProgram, result, acadCredits, plannedCourses, courses, subjects, departments);
            Assert.AreEqual("3.015", studentProgramModel.MinimumOverallGpa);
            Assert.AreEqual("3.025", studentProgramModel.MinimumInstitutionGpa);
            Assert.AreEqual("3.455", studentProgramModel.CumulativeGpa);
            Assert.AreEqual("3.555", studentProgramModel.InstitutionGpa);            
        }

        [TestMethod][Ignore]
        public void GpaRetainsThreeDecimalPlaces()
        {
            result.ProgramRequirements.MinOverallGpa = 0m;
            result.ProgramRequirements.MinInstGpa = 3.02m;
            result.CumGpa = 3.4m;
            result.InstGpa = 3m;
            var studentProgramModel = new StudentProgramModel(adapterRegistry, program, studentProgram, result, acadCredits, plannedCourses, courses, subjects, departments);
            Assert.AreEqual("0.000", studentProgramModel.MinimumOverallGpa);
            Assert.AreEqual("3.020", studentProgramModel.MinimumInstitutionGpa);
            Assert.AreEqual("3.400", studentProgramModel.CumulativeGpa);
            Assert.AreEqual("3.000", studentProgramModel.InstitutionGpa);
        }

        //Build a program evaluation result to use in tests
        //public ProgramEvaluation BuildProgramEvaluationResult()
        //{
        //    ProgramEvaluation result = new ProgramEvaluation();

        //    Group g1 = new Group();
        //    g1.Id = "g1";
        //    GroupResult gr1 = new GroupResult();
        //    gr1.GroupId = g1.Id;
        //    gr1.Gpa = 0.000m;

        //    Subrequirement s1 = new Subrequirement();
        //    s1.Id = "s1";
        //    s1.Groups = new List<Group>();
        //    s1.Groups.Add(g1);

        //    SubrequirementResult sr1 = new SubrequirementResult();
        //    sr1.SubrequirementId = s1.Id;
        //    sr1.Gpa = 0.000m;
        //    sr1.GroupResults = new List<GroupResult>();
        //    sr1.GroupResults.Add(gr1);

        //    Requirement r1 = new Requirement();
        //    r1.Id = "r1";
        //    r1.Subrequirements = new List<Subrequirement>();
        //    r1.Subrequirements.Add(s1);

        //    RequirementResult rr1 = new RequirementResult();
        //    rr1.RequirementId = r1.Id;
        //    rr1.Gpa = 0.000m;
        //    rr1.SubrequirementResults = new List<SubrequirementResult>();
        //    rr1.SubrequirementResults.Add(sr1);

        //    result.ProgramRequirements = new ProgramRequirements();
        //    result.ProgramRequirements.Requirements = new List<Requirement>();
        //    result.ProgramRequirements.Requirements.Add(r1);

        //    result.RequirementResults = new List<RequirementResult>();
        //    result.RequirementResults.Add(rr1);

        //    return result;
        //}

        //Build academicCredit model items (academic credits, planned courses used as input to StudentProgramModel constructor)
        //public List<AcademicCreditModel> BuildAcadCreditModelList(int startId)
        //{
        //    var acadCreditModelList = new List<AcademicCreditModel>();

        //    for (int i = startId; i < startId + 10; i++)
        //    {
        //        var acadCreditModel = new AcademicCreditModel();
        //        acadCreditModel.Id = i.ToString();
        //        var courseId = "1" + acadCreditModel.Id;
        //        acadCreditModel.CourseId = courseId;

        //        acadCreditModelList.Add(acadCreditModel);                
        //    }

        //    return acadCreditModelList;
        //}

    }
}
