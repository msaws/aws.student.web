﻿using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Areas.Planning.Models.Requirements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Requirements
{
    [TestClass]
    public class RequirementResultModelTests
    {
        private Mock<Ellucian.Web.Adapters.IAdapterRegistry> adapterRegistryMock;
        private IAdapterRegistry adapterRegistry;
        private Program program;
        private StudentProgram studentProgram;
        private ProgramEvaluation3 result;
        private List<AcademicCreditModel> acadCredits;
        private List<AcademicCreditModel> plannedCourses;
        private List<Course2> courses;
        private List<Subject> subjects;
        private List<Department> departments;
        private RequirementResultModel reqRsltModel;
        private Requirement req;

        [TestInitialize]
        public void Initialize()
        {
            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;

            program = new Program();
            studentProgram = new StudentProgram();
            studentProgram.AdditionalRequirements = new List<AdditionalRequirement>();
            result = new ProgramEvaluation3();
            result.ProgramRequirements = new ProgramRequirements();
            result.ProgramRequirements.Requirements = new List<Requirement>();
            result.OtherPlannedCredits = new List<PlannedCredit>();
            result.OtherAcademicCredits = new List<string>();
            result.RequirementResults = new List<RequirementResult3>()
            {
                new RequirementResult3() 
                {
                    RequirementId = "12",
                    SubrequirementResults = new List<SubrequirementResult3>()
                    {
                        new SubrequirementResult3() 
                        {
                            SubrequirementId = "1234", // Subreq intentionally not created for this result because cannot mock resources
                            CompletionStatus = CompletionStatus.NotStarted,
                            PlanningStatus = PlanningStatus.CompletelyPlanned,
                            Gpa = 3.23m,
                            ModificationMessages = new List<string>() {"subreq modification1, subreq modification2"}
                        }
                    },
                    CompletionStatus = CompletionStatus.PartiallyCompleted,
                    PlanningStatus = PlanningStatus.NotPlanned,
                    ModificationMessages = new List<string>() {"req modification1","req modification2"}
                }
            };
            acadCredits = new List<AcademicCreditModel>();
            plannedCourses = new List<AcademicCreditModel>();
            courses = new List<Course2>();
            subjects = new List<Subject>();
            departments = new List<Department>();
            req = new Requirement()
                {
                    Id = "12",
                    Code = "REQ1",
                    Description = "requirement description",
                    MinSubRequirements = 2,
                    MinGpa = null,
                    Subrequirements = new List<Subrequirement>() 
                    {
                        new Subrequirement()
                        {
                            Id = "123",
                            Code = "SUBREQ1",
                            DisplayText = "#GROUP specification",
                            MinGroups = 2,
                            MinGpa = null,
                            Groups = new List<Group>()
                            {
                                new Group() 
                                {
                                    Id = "1234",
                                    Code = "GROUP1"
                                }
                            }
                        },
                        new Subrequirement()
                        {
                            Id = "124",
                            Code = "SUBREQ2",
                            DisplayText = "subreq2 display text",
                            MinGroups = 1,
                            MinGpa = null,
                            Groups = new List<Group>()
                        }
                    }
                };

            reqRsltModel = new RequirementResultModel(adapterRegistry, result, req, acadCredits, plannedCourses, courses, subjects, departments);
        }

        // These tests do not work and therefore have been ignored due to our inability to use Moq to mock GlobalResources. You can remove the [Ignore]
        // on one of these tests (and on the class) to see what is happening.

        [TestMethod][Ignore]
        public void RequirementResultModel_Id()
        {
            Assert.AreEqual(req.Id, reqRsltModel.Id);
        }

        [TestMethod][Ignore]
        public void RequirementResultModel_Code()
        {
            Assert.AreEqual(req.Code, reqRsltModel.Code);
        }

        [TestMethod][Ignore]
        public void RequirementResultModel_Description()
        {
            Assert.AreEqual(req.Description, reqRsltModel.Description);
        }

    }
}
