﻿using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Planning.Models.Courses;
using Ellucian.Web.Student.Areas.Planning.Models.Requirements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Requirements
{
    [TestClass]
    public class GroupDetailModelTests
    {
        GroupDetailModel gm1;
        GroupDetailModel gm2;

        [TestInitialize]
        public void Initialize()
        {
            gm1 = new GroupDetailModel();
            gm1.FromCourses = new List<RequirementCourseModel>();
            gm1.Courses = new List<RequirementCourseModel>();
            gm1.ButNotCourseLevels = new List<string>();
            gm1.ButNotCourses = new List<RequirementCourseModel>();
            gm1.ButNotDepartments = new List<Colleague.Dtos.Base.Department>();
            gm1.ButNotSubjects = new List<Subject>();
            gm1.FromDepartments = new List<Colleague.Dtos.Base.Department>();
            gm1.FromLevels = new List<string>();
            gm1.FromSubjects = new List<Subject>();
            gm1.AcademicCreditRules = new List<string>();

            //Use for testing decimals and rounding.
            gm2 = new GroupDetailModel();
            gm2.FromCourses = new List<RequirementCourseModel>();
            gm2.Courses = new List<RequirementCourseModel>();
            gm2.ButNotCourseLevels = new List<string>();
            gm2.ButNotCourses = new List<RequirementCourseModel>();
            gm2.ButNotDepartments = new List<Colleague.Dtos.Base.Department>();
            gm2.ButNotSubjects = new List<Subject>();
            gm2.FromDepartments = new List<Colleague.Dtos.Base.Department>();
            gm2.FromLevels = new List<string>();
            gm2.FromSubjects = new List<Subject>();
            gm2.AcademicCreditRules = new List<string>();
            gm2.MinCredits = 3.01m;
            gm2.MinCreditsPerCourse = 1.00000m;
            gm2.MinCreditsPerSubject = 2.00002m;
            gm2.MinCreditsPerDepartment = 555.555556m;
            gm2.MaxCredits = 44.123456m;
            gm2.MaxCreditsPerCourse = 7777777.7777777m;
            gm2.MaxCreditsPerSubject = 10.0m;
            gm2.MaxCreditsPerRule = 5.002m;
            gm2.MaxCreditsRule = "XXX";
            gm2.MaxCreditsPerDepartment = 15;
        }

        [TestMethod]
        public void DifferentCoursesWithSameSubjAndNumberNotDupedInCourses()
        {
            Course crs1 = new Course();
            Course crs2 = new Course();

            crs1.Id = "crs1";
            crs2.Id = "crs2";

            crs1.Number = "100";
            crs2.Number = "100";
            crs1.SubjectCode = "MATH";
            crs2.SubjectCode = "MATH";

            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            RequirementCourseModel cm2 = new RequirementCourseModel() {Id = crs2.Id, Number = crs2.Number, SubjectCode = crs2.SubjectCode };

            gm1.Courses.Add(cm1);
            gm1.Courses.Add(cm2);

            Console.WriteLine("Directive = '" + gm1.Directive + "'");

            Assert.IsFalse(gm1.Directive.Contains("courses MATH100, MATH100"));

        }

        [TestMethod]
        public void DifferentCoursesWithSameSubjAndNumberNotDupedInFromCourses()
        {
            Course crs1 = new Course();
            Course crs2 = new Course();
            gm1.MinCourses = 1;

            crs1.Id = "crs1";
            crs2.Id = "crs2";

            crs1.Number = "100";
            crs2.Number = "100";
            crs1.SubjectCode = "MATH";
            crs2.SubjectCode = "MATH";

            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            RequirementCourseModel cm2 = new RequirementCourseModel() { Id = crs2.Id, Number = crs2.Number, SubjectCode = crs2.SubjectCode };

            gm1.FromCourses.Add(cm1);
            gm1.FromCourses.Add(cm2);

            Console.WriteLine("Directive = '" + gm1.Directive + "'");

            Assert.IsFalse(gm1.Directive.Contains("courses MATH100, MATH100"));

        }

        [TestMethod]
        public void DifferentCoursesWithSameSubjAndNumberCountedCorrectly()
        {
            Course crs1 = new Course();
            Course crs2 = new Course();
            gm1.MinCourses = 1;

            crs1.Id = "crs1";
            crs2.Id = "crs2";

            crs1.Number = "100";
            crs2.Number = "100";
            crs1.SubjectCode = "MATH";
            crs2.SubjectCode = "MATH";

            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            RequirementCourseModel cm2 = new RequirementCourseModel() { Id = crs2.Id, Number = crs2.Number, SubjectCode = crs2.SubjectCode };

            gm1.FromCourses.Add(cm1);
            gm1.FromCourses.Add(cm2);

            Console.WriteLine("Directive = '" + gm1.Directive + "'");

            Assert.IsFalse(gm1.Directive.Contains("course MATH100,"));
            Assert.IsTrue(gm1.Directive.Contains("course MATH100."));

        }

        [TestMethod]
        public void DifferentCoursesWithDiffSubjStillSeparated()
        {
            Course crs1 = new Course();
            Course crs2 = new Course();
            gm1.MinCourses = 1;

            crs1.Id = "crs1";
            crs2.Id = "crs2";

            crs1.Number = "100";
            crs2.Number = "200";
            crs1.SubjectCode = "MATH";
            crs2.SubjectCode = "MATH";

            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            RequirementCourseModel cm2 = new RequirementCourseModel() { Id = crs2.Id, Number = crs2.Number, SubjectCode = crs2.SubjectCode };

            gm1.FromCourses.Add(cm1);
            gm1.FromCourses.Add(cm2);

            Console.WriteLine("Directive = '" + gm1.Directive + "'");

            Assert.IsTrue(gm1.Directive.Contains("MATH100, MATH200."));

        }

        [TestMethod]
        public void DirectiveTextCreditRounding()
        {
            string directive = gm2.Directive;
            Assert.IsTrue(directive.Contains("Complete 3.01 credits."));
            Assert.IsTrue(directive.Contains("Minimum of 1 credits per course."));
            Assert.IsTrue(directive.Contains("Minimum of 2.00002 credits per subject."));
            Assert.IsTrue(directive.Contains("Minimum of 555.55556 credits per department."));
            Assert.IsTrue(directive.Contains("Maximum of 44.12346 credits."));
            Assert.IsTrue(directive.Contains("Maximum of 7777777.77778 credits per course."));
            Assert.IsTrue(directive.Contains("Maximum of 10 credits per subject."));
            Assert.IsTrue(directive.Contains("Maximum of 15 credits per department."));
            Assert.IsTrue(directive.Contains("Maximum of 5.002 credits from rule: XXX."));
        }

        [TestMethod]
        public void DirectiveReturnsDisplayText()
        {
            Course crs1 = new Course();
            crs1.Id = "crs1";
            crs1.Number = "100";
            crs1.SubjectCode = "MATH";
            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };

            gm1.Courses.Add(cm1);

            var text = "This is the PRINT text";
            gm1.DisplayText = text;
            Assert.AreEqual(text, gm1.Directive);
        }

        [TestMethod]
        public void DirectiveReturnsGroupTextIfNoDisplayText()
        {
            Course crs1 = new Course();
            crs1.Id = "crs1";
            crs1.Number = "100";
            crs1.SubjectCode = "MATH";
            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            gm1.Courses.Add(cm1);

            var text = string.Empty;
            Assert.AreNotEqual(text, gm1.Directive);
        }

        [TestMethod]
        public void MinCoursesText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"}
            };
            gm1.FromDepartments = depts;

            Assert.IsTrue(gm1.Directive.Contains("Complete 3 courses."));
        }

        [TestMethod]
        public void FromDepartmentsText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"}
            };
            gm1.FromDepartments = depts;

            Assert.IsTrue(gm1.Directive.Contains("Choose from the department of History."));
        }

        [TestMethod]
        public void MinCreditsText()
        {
            gm1.MinCredits = 4m;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"}
            };
            gm1.FromSubjects = subjects;

            Assert.IsTrue(gm1.Directive.Contains("Complete 4 credits."));
        }

        [TestMethod]
        public void FromSubjectsText()
        {
            gm1.MinCredits = 4m;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"}
            };
            gm1.FromSubjects = subjects;

            Assert.IsTrue(gm1.Directive.Contains("Choose from the subject of Mathematics."));
        }

        [TestMethod]
        public void TakeCoursesText()
        {
            var crs1 = new Course();
            crs1.Id = "crs1";
            crs1.Number = "100";
            crs1.SubjectCode = "MATH";
            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            gm1.Courses.Add(cm1);

            var crs2 = new Course();
            crs2.Id = "crs2";
            crs2.Number = "101";
            crs2.SubjectCode = "MATH";
            RequirementCourseModel cm2 = new RequirementCourseModel() { Id = crs2.Id, Number = crs2.Number, SubjectCode = crs2.SubjectCode };
            gm1.Courses.Add(cm2);

            var crs3 = new Course();
            crs3.Id = "crs2";
            crs3.Number = "102";
            crs3.SubjectCode = "PHYS";
            RequirementCourseModel cm3 = new RequirementCourseModel() { Id = crs3.Id, Number = crs3.Number, SubjectCode = crs3.SubjectCode };
            gm1.Courses.Add(cm3);

            Assert.IsTrue(gm1.Directive.Contains("Take courses MATH100, MATH101, PHYS102."));
        }

        [TestMethod]
        public void FromCoursesText()
        {
            var crs1 = new Course();
            crs1.Id = "crs1";
            crs1.Number = "100";
            crs1.SubjectCode = "MATH";
            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            gm1.FromCourses.Add(cm1);

            var crs2 = new Course();
            crs2.Id = "crs2";
            crs2.Number = "101";
            crs2.SubjectCode = "MATH";
            RequirementCourseModel cm2 = new RequirementCourseModel() { Id = crs2.Id, Number = crs2.Number, SubjectCode = crs2.SubjectCode };
            gm1.FromCourses.Add(cm2);

            var crs3 = new Course();
            crs3.Id = "crs2";
            crs3.Number = "102";
            crs3.SubjectCode = "PHYS";
            RequirementCourseModel cm3 = new RequirementCourseModel() { Id = crs3.Id, Number = crs3.Number, SubjectCode = crs3.SubjectCode };
            gm1.FromCourses.Add(cm3);

            gm1.MinCourses = 2;
            Assert.IsTrue(gm1.Directive.Contains("Choose from the courses MATH100, MATH101, PHYS102."));
        }

        [TestMethod]
        public void FromLevelsText()
        {
            gm1.MinCredits = 4m;
            var levels = new List<string>() { "100", "200" };
            gm1.FromLevels = levels;

            Assert.IsTrue(gm1.Directive.Contains("Choose from the levels of 100, 200."));
        }

        [TestMethod]
        public void HasRulesSetIfAnyRulesPresent()
        {
            gm1.MinCourses = 2;
            gm1.AcademicCreditRules = new List<string>() { "RULE1", "RULE2" };

            Assert.IsTrue(gm1.HasRules);
        }

        [TestMethod]
        public void HasAcademicCreditBasedRules_True()
        {
            gm1.MinCourses = 2;
            gm1.AcademicCreditRules = new List<string>() { "RULE1", "RULE2" };
            gm1.HasAcademicCreditBasedRules = true;

            Assert.IsTrue(gm1.HasRules);
            Assert.IsTrue(gm1.HasAcademicCreditBasedRules);
        }

        [TestMethod]
        public void HasAcademicCreditBasedRules_False()
        {
            gm1.MinCourses = 2;
            gm1.AcademicCreditRules = new List<string>() { "RULE1", "RULE2" };
            gm1.HasAcademicCreditBasedRules = false;

            Assert.IsTrue(gm1.HasRules);
            Assert.IsFalse(gm1.HasAcademicCreditBasedRules);
        }

        [TestMethod]
        public void FromRulesText()
        {
            gm1.MinCourses = 2;
            gm1.AcademicCreditRules = new List<string>() { "RULE1", "RULE2" };

            Assert.IsTrue(gm1.Directive.Contains("Courses must be from rule(s): RULE1, RULE2."));
        }

        [TestMethod]
        public void ButNotCoursesText()
        {
            var crs1 = new Course();
            crs1.Id = "crs1";
            crs1.Number = "100";
            crs1.SubjectCode = "MATH";
            RequirementCourseModel cm1 = new RequirementCourseModel() { Id = crs1.Id, Number = crs1.Number, SubjectCode = crs1.SubjectCode };
            gm1.ButNotCourses.Add(cm1);

            var crs2 = new Course();
            crs2.Id = "crs2";
            crs2.Number = "101";
            crs2.SubjectCode = "MATH";
            RequirementCourseModel cm2 = new RequirementCourseModel() { Id = crs2.Id, Number = crs2.Number, SubjectCode = crs2.SubjectCode };
            gm1.ButNotCourses.Add(cm2);

            gm1.MinCredits = 4m;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"}
            };
            gm1.FromSubjects = subjects;

            Assert.IsTrue(gm1.Directive.Contains("Excluding the courses MATH100, MATH101."));
        }

        [TestMethod]
        public void ButNotSubjectsTest()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"}
            };
            gm1.FromDepartments = depts;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"}
            };
            gm1.ButNotSubjects = subjects;

            Assert.IsTrue(gm1.Directive.Contains("Excluding the subject Mathematics."));
        }

        [TestMethod]
        public void ButNotDepartmentsTest()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"}
            };
            gm1.ButNotDepartments = depts;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"}
            };
            gm1.FromSubjects = subjects;

            Assert.IsTrue(gm1.Directive.Contains("Excluding the department History."));
        }

        [TestMethod]
        public void ButNotlevelsText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"}
            };
            gm1.FromDepartments = depts;
            var levels = new List<string>() { "100", "200" };
            gm1.ButNotCourseLevels = levels;

            Assert.IsTrue(gm1.Directive.Contains("Excluding the levels 100, 200."));
            Assert.IsFalse(gm1.HasRules);
        }

        [TestMethod]
        public void MinDepartmentsText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MinDepartments = 2;

            Assert.IsTrue(gm1.Directive.Contains("Minimum of 2 departments."));
        }

        [TestMethod]
        public void MinSubjectsText()
        {
            gm1.MinCredits = 4m;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromSubjects = subjects;
            gm1.MinSubjects = 3;

            Assert.IsTrue(gm1.Directive.Contains("Minimum of 3 subjects."));
        }

        [TestMethod]
        public void MinCoursesPerDepartmentText()
        {
            gm1.MinCourses = 8;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MinCoursesPerDepartment = 2;

            Assert.IsTrue(gm1.Directive.Contains("Minimum of 2 courses per department."));
        }

        [TestMethod]
        public void MinCoursesPerSubjectText()
        {
            gm1.MinCourses = 4;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MinCoursesPerSubject = 1;

            Assert.IsTrue(gm1.Directive.Contains("Minimum of 1 courses per subject."));
            Assert.IsFalse(gm1.HasRules);
        }

        [TestMethod]
        public void MinCreditsPerCourseText()
        {
            gm1.MinCourses = 4;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MinCreditsPerCourse = 3.12m;

            Assert.IsTrue(gm1.Directive.Contains("Minimum of 3.12 credits per course."));
        }

        [TestMethod]
        public void MinCreditsPerDepartmentText()
        {
            gm1.MinCourses = 4;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MinCreditsPerDepartment = 4.153m;

            Assert.IsTrue(gm1.Directive.Contains("Minimum of 4.153 credits per department."));
        }

        [TestMethod]
        public void MinCreditsPerSubjectText()
        {
            gm1.MinCourses = 4;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MinCreditsPerSubject = 13.5432m;

            Assert.IsTrue(gm1.Directive.Contains("Minimum of 13.5432 credits per subject."));
            Assert.IsFalse(gm1.HasRules);
        }

        [TestMethod]
        public void MaxCoursesText()
        {
            gm1.MinCourses = 2;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MaxCourses = 4;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 4 courses."));
        }

        [TestMethod]
        public void MaxCoursesPerDepartmentText()
        {
            gm1.MinCourses = 6;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MaxCoursesPerDepartment = 5;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 5 courses per department."));
        }

        [TestMethod]
        public void MaxCoursesPerSubjectText()
        {
            gm1.MinCourses = 6;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MaxCoursesPerSubject = 4;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 4 courses per subject."));
        }

        [TestMethod]
        public void MaxCreditsText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxCredits = 15.56789m;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 15.56789 credits."));
        }

        [TestMethod]
        public void MaxCreditsPerCourseText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxCreditsPerCourse = 4.1m;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 4.1 credits per course."));
            Assert.IsFalse(gm1.HasRules);
        }

        [TestMethod]
        public void MaxCreditsPerDepartmentText()
        {
            gm1.MinCourses = 6;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"},
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "HIST", Description = "History"},
            };
            gm1.FromSubjects = subjects;
            gm1.MaxCreditsPerDepartment = 4.25m;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 4.25 credits per department."));
        }

        [TestMethod]
        public void MaxCreditsPerSubjectText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxCreditsPerSubject = 4.1m;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 4.1 credits per subject."));
        }

        [TestMethod]
        public void MaxDepartmentsText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxDepartments = 2;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 2 departments."));
        }

        [TestMethod]
        public void MaxSubjectsText()
        {
            gm1.MinCourses = 3;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxSubjects = 3;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 3 subjects."));
            Assert.IsFalse(gm1.HasRules);
        }

        [TestMethod]
        public void MaxCoursesAtLevelsText()
        {
            gm1.MinCourses = 4;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxCoursesAtLevels = new Ellucian.Colleague.Dtos.Student.Requirements.MaxCoursesAtLevels()
            {
                Levels = new List<string>() { "100", "200" },
                MaxCourses = 2
            };

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 2 courses at the 100, 200 levels."));
        }

        [TestMethod]
        public void MaxCreditsAtLevelsText()
        {
            gm1.MinCourses = 4;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "ENGL", Description = "English"},
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "PHYS", Description = "Physics"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxCreditsAtLevels = new Ellucian.Colleague.Dtos.Student.Requirements.MaxCreditAtLevels()
            {
                Levels = new List<string>() { "100" },
                MaxCredits = 3
            };

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 3 credits at the 100 level."));
            Assert.IsFalse(gm1.HasRules);
        }

        [TestMethod]
        public void MaxCoursesPerRuleText()
        {
            gm1.MinCourses = 2;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxCoursesRule = "RULE3";
            gm1.MaxCoursesPerRule = 5;

            Assert.IsTrue(gm1.Directive.Contains("Maximum of 5 courses from rule: RULE3."));
        }

        [TestMethod]
        public void HasRulesTrueIfMaxCreditsRulePresent()
        {
            gm1.MinCourses = 2;
            var depts = new List<Ellucian.Colleague.Dtos.Base.Department>() {
                new Ellucian.Colleague.Dtos.Base.Department() {Code = "HIST", Description = "History"}
            };
            gm1.FromDepartments = depts;
            gm1.MaxCoursesRule = "RULE3";
            gm1.MaxCoursesPerRule = 5;

            Assert.IsTrue(gm1.HasRules);
        }

        [TestMethod]
        public void MaxCreditsPerRuleText()
        {
            gm1.MinCourses = 2;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"}
                        };
            gm1.FromSubjects = subjects;
            gm1.MaxCreditsRule = "RULE3";
            gm1.MaxCreditsPerRule = 6;
            Assert.IsTrue(gm1.Directive.Contains("Maximum of 6 credits from rule: RULE3."));
        }



        [TestMethod]
        public void HasRulesTrueIfMaxCoursesRulePresent()
        {
            gm1.MinCourses = 2;
            var subjects = new List<Ellucian.Colleague.Dtos.Student.Subject>() {
                new Ellucian.Colleague.Dtos.Student.Subject() {Code = "MATH", Description = "Mathematics"}
                        };
            gm1.FromSubjects = subjects;
            gm1.MaxCreditsRule = "RULE3";
            gm1.MaxCreditsPerRule = 6;

            Assert.IsTrue(gm1.HasRules);
        }

    }
}
