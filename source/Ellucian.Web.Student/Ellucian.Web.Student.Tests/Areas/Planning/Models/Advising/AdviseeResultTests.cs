﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Student.Areas.Planning.Models.Advising;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.Planning.Models.Advising
{
    [TestClass]
    public class AdviseeResultTests
    {
        Advisee AdviseeDto = new Advisee();

        [TestInitialize]
        public void Initialize()
        {
            AdviseeDto.Id = "11111";
            AdviseeDto.ApprovalRequested = true;
            AdviseeDto.DegreePlanId = 11111;
            AdviseeDto.EducationalGoal = "Masters degree";
            AdviseeDto.PreferredEmailAddress = "test@test.com";
            AdviseeDto.ProgramIds = new List<string>() {"BS.MATH"};
            AdviseeDto.IsAdvisee = false;
            AdviseeDto.PersonDisplayName = new Colleague.Dtos.Base.PersonHierarchyName() { FullName = "Student Display Name" };
        }

        [TestCleanup]
        public void Cleanup()
        {
            AdviseeDto = null;
        }

        [TestMethod]
        public void AdviseeResultConstructor()
        {
            var privacyMessage = "This is a privacy message.";

            AdviseeResult adviseeResult = new AdviseeResult(AdviseeDto, privacyMessage, false);

            Assert.AreEqual(AdviseeDto.Id, adviseeResult.Id);
            Assert.AreEqual(AdviseeDto.DegreePlanId, adviseeResult.DegreePlanId);
            Assert.AreEqual(AdviseeDto.ApprovalRequested, adviseeResult.HasApprovalPending);
            Assert.AreEqual(AdviseeDto.EducationalGoal, adviseeResult.EducationalGoal);
            Assert.AreEqual(AdviseeDto.PreferredEmailAddress, adviseeResult.PreferredEmailAddress);
            Assert.AreEqual(AdviseeDto.IsAdvisee, adviseeResult.IsAdvisee);
            Assert.AreEqual(privacyMessage, adviseeResult.PrivacyMessage);
            Assert.AreEqual("Student Display Name", adviseeResult.DisplayName);
        }

        [TestMethod]
        public void AdviseeResultEmptyConstructor()
        {
            AdviseeResult adviseeResult = new AdviseeResult();

            Assert.AreEqual(string.Empty, adviseeResult.Id);
            Assert.AreEqual(null, adviseeResult.DegreePlanId);
            Assert.AreEqual(false, adviseeResult.HasApprovalPending);
            Assert.IsNotNull(adviseeResult.ProgramDisplay);
            Assert.AreEqual(0, adviseeResult.ProgramDisplay.Count);
            Assert.AreEqual(string.Empty, adviseeResult.DisplayName);
            Assert.AreEqual(string.Empty, adviseeResult.PreferredEmailAddress);
            Assert.IsNotNull(adviseeResult.AdvisorNames);
            Assert.AreEqual(0, adviseeResult.AdvisorNames.Count);
        }


    }
}
