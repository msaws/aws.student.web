﻿var Ellucian = Ellucian || {};
Ellucian.Planning = Ellucian.Planning || {};


describe("planRepository", function () {

    // A student ID is required to get/update plans
    var studentId = "12345";

    // We know how the storage keys are generated, 
    // create them here, so we can directly test storage
    var degreePlanKey = "degreeplan" + studentId;
    var studentProgramsKey = "studentprograms" + studentId;

    // This is a test plan, it can be simple because we're just testing the repository, not the plan itself
    var testPlan = { "id": studentId };
    var testPlan2 = { "id": studentId, "foo": "bar" }; // A second test plan, for testing updates
    var testPrograms = [{ "program1": "This is a test program." }, { "program2": "This is another test program" }];

    var planRepository = Ellucian.Planning.planRepository;
    var storage=Ellucian.Storage.session;

    beforeEach(function () {
        account = {
            handleInvalidSessionResponse: function (data) {
                return false;
            }
        };
    });

    beforeEach(function () {
        storage.clear();
    });

    describe("get", function () {

        it(":gets cached plan", function (done) {
            // Cheating: manually enter data into the cache, bypassing the repository
            storage.setItem(degreePlanKey, testPlan);
            storage.setItem(studentProgramsKey, testPrograms);

            planRepository.get(studentId).then(function (result) {
                expect(result[0]).toEqual(testPlan);
                expect(result[1]).toEqual(testPrograms);
                done();
            });
        });

        it(":gets uncached plan and caches values", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve({ DegreePlan: testPlan, StudentPrograms: testPrograms });
            });

            planRepository.get(studentId).then(function (result) {
                expect(result[0]).toEqual(testPlan);
                expect(result[1]).toEqual(testPrograms);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan);
                expect(storage.getItem(studentProgramsKey)).toEqual(testPrograms);

                done();
            });
        });

        it(":returns error when unable to retreive plan", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            planRepository.get(studentId).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.getFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.get().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {
            planRepository.get("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("register", function () {

        var registrationMessages = ["You registered!"];

        // Need to define this here, because it's normally included in a Razor partial view (can't be included in chutzpah.json)
        // We don't need to test its functionality, just need it defined so the test can run.
        window.allTrackUserTiming = function () { };

        it(":calls allTrackUserTiming on success", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(registrationMessages);
            });

            spyOn(window, "allTrackUserTiming");

            planRepository.register(studentId, []).then(function (result) {
                expect(result).toEqual(registrationMessages);

                expect(allTrackUserTiming).toHaveBeenCalled();

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.register(studentId, []).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.register(studentId, []).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.register(studentId, []).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.register().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.register("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.register(studentId, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("completeRegistration", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.completeRegistration(studentId).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on failure", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            planRepository.completeRegistration(studentId).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.completeRegistration(studentId).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.completeRegistration().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.completeRegistration("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("addTerm", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan);
            });

            planRepository.addTerm(studentId, "Fall/1977", testPlan).then(function (result) {
                expect(result).toEqual(testPlan);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.addTerm(studentId, "Fall/1977", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.addTerm(studentId, "Fall/1977", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.addTerm(studentId, "1977.Fall", testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.addTerm().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.addTerm("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.addTerm(studentId, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("removeTerm", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan);
            });

            planRepository.removeTerm(studentId, "Fall/1977", testPlan).then(function (result) {
                expect(result).toEqual(testPlan);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.removeTerm(studentId, "Fall/1977", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.removeTerm(studentId, "Fall/1977", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.removeTerm(studentId, "1977.Fall", testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.removeTerm().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.removeTerm("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.removeTerm(studentId, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("addCourse", function () {

        var credits = 3;

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.addCourse(studentId, "1234", "1977.Fall", credits, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.addCourse(studentId, "1234", "1977.Fall", credits, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.addCourse(studentId, "1234", "1977.Fall", credits, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.addCourse(studentId, "1234", "1977.Fall", credits, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.addCourse().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.addCourse("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.addCourse(studentId, null, null, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("updateCourse", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.updateCourse(studentId, "1234", "1977.Fall", "1234", testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.updateCourse(studentId, "1234", "1977.Fall", "1234", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.updateCourse(studentId, "1234", "1977.Fall", "1234", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.updateCourse(studentId, "1234", "1977.Fall", "1234", testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.updateCourse().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.updateCourse("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.updateCourse(studentId, null, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("removeCourse", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan);
            });

            planRepository.removeCourse(studentId, "1234", "1977.Fall", "1234", {}).then(function (result) {
                expect(result).toEqual(testPlan);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.removeCourse(studentId, "1234", "1977.Fall", "1234", {}).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.removeCourse(studentId, "1234", "1977.Fall", "1234", {}).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.removeCourse(studentId, "1234", "1977.Fall", "1234", {}).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.removeCourse().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.removeCourse("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.removeCourse(studentId, null, null, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("addSection", function () {

        var gradingType = "GRADED";
        var credits = 3;

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.addSection(studentId, "1234", "1977.Fall", "1234", gradingType, credits, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.addSection(studentId, "1234", "1977.Fall", "1234", gradingType, credits, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.addSection(studentId, "1234", "1977.Fall", "1234", gradingType, credits, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.addSection(studentId, "1234", "1977.Fall", "1234", gradingType, credits, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.addSection().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.addSection("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.addSection(studentId, null, null, null, null, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("removeSection", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.removeSection(studentId, "1234", testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.removeSection(studentId, "1234", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.removeSection(studentId, "1234", testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.removeSection(studentId, "1234", testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.removeSection().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.removeSection("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.removeSection(studentId, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("requestReview", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.requestReview(studentId, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.requestReview(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.requestReview(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.requestReview(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.requestReview().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.requestReview("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.requestReview(studentId, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("submitNote", function () {

        var note = "This is a note.";
        var type = "STUDENT";

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.submitNote(studentId, note, type, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.submitNote(studentId, note, type, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.submitNote(studentId, note, type, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.submitNote(studentId, note, type, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.submitNote().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.submitNote("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.submitNote(studentId, null, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("completeReview", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.completeReview(studentId, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.completeReview(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.completeReview(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.completeReview(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.completeReview().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.completeReview("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.completeReview(studentId, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("approveCourses", function () {

        var term = "2017/FA";
        var courses = [];
        var status = "test";

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.approveCourses(studentId, term, courses, status, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.approveCourses(studentId, term, courses, status, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.approveCourses(studentId, term, courses, status, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.approveCourses(studentId, term, courses, status, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.approveCourses().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.approveCourses("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.approveCourses(studentId, null, null, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("archivePlan", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.archivePlan(studentId, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.archivePlan(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.archivePlan(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.archivePlan(studentId, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.archivePlan().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.archivePlan("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.archivePlan(studentId, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("updateProtection", function () {

        var courses = [];
        var type = true;

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.updateProtection(studentId, courses, type, testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.updateProtection(studentId, courses, type, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.updateProtection(studentId, courses, type, testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.updateProtection(studentId, courses, type, testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.updateProtection().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.updateProtection("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.updateProtection(studentId, null, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });

    describe("clearPlan", function () {

        it(":gets updated plan and caches value", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });

            planRepository.clearPlan(studentId, [], testPlan).then(function (result) {
                expect(result).toEqual(testPlan2);

                expect(storage.getItem(degreePlanKey)).toEqual(testPlan2);

                done();
            });
        });

        it(":removes old plan from storage on unknown failure", function (done) {

            var error = { isStale: false, message: Ellucian.Planning.planRepositoryMessages.updateFailed };
            var jqXHR = { status: 999 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.clearPlan(studentId, [], testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on stale data failure", function (done) {

            var error = { isStale: true, message: Ellucian.Planning.planRepositoryMessages.staleData };
            var jqXHR = { status: 409 };

            spyOn($, "ajax").and.callFake(function () {
                return $.Deferred().reject(jqXHR);
            });

            planRepository.clearPlan(studentId, [], testPlan).catch(function (result) {
                expect(result).toEqual(error);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":removes old plan from storage on invalid session", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve(testPlan2);
            });
            account = {
                handleInvalidSessionResponse: function (data) {
                    return true;
                }
            };

            planRepository.clearPlan(studentId, [], testPlan).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.updateFailed);

                expect(storage.getItem(degreePlanKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            planRepository.clearPlan().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {

            planRepository.clearPlan("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.studentIdRequired);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when arguments are invalid", function (done) {

            planRepository.clearPlan(studentId, null, null).catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.planRepositoryMessages.argumentError);

                expect(storage.getItem(degreePlanKey)).toEqual(null);
                expect(storage.getItem(studentProgramsKey)).toEqual(null);

                done();
            });
        });
    });


});