﻿describe("requirementsRepository", function () {

    var studentId = "12345";

    var evaluationsKey = "evaluations" + studentId;
    var activeProgramsKey = "activeprograms" + studentId;

    var testEvaluations = [{ "Evaluation": "Test evaluation" }, { "Evaluation": "Test evaluation" }];
    var testPrograms = [{ "program1": "This is a test program." }, { "program2": "This is another test program" }];
    var requirementsRepository = Ellucian.Planning.requirementsRepository;
    var errorMessage = "Unable to retrieve programs.";
    var storage = Ellucian.Storage.session;

    beforeAll(function () {
        account = {
            handleInvalidSessionResponse: function (data) {
                return false;
            }
        };
    });

    beforeEach(function () {
        storage.clear();
    });

    describe("get", function () {

        it(":requirementsRepository gets cached plan", function (done) {
            // Cheating: manually enter data into the cache, bypassing the repository
            storage.setItem(evaluationsKey, testEvaluations);
            storage.setItem(activeProgramsKey, testPrograms);

            requirementsRepository.get(studentId).then(function (result) {
                expect(result[0]).toEqual(testEvaluations);
                expect(result[1]).toEqual(testPrograms);
                done();
            });            
        });

        it(":requirementsRepository gets uncached plan and caches values", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().resolve({ Evaluations: testEvaluations, ActivePrograms: testPrograms });
            });

            requirementsRepository.get(studentId).then(function (result) {
                expect(result[0]).toEqual(testEvaluations);
                expect(result[1]).toEqual(testPrograms);

                expect(storage.getItem(evaluationsKey)).toEqual(testEvaluations);
                expect(storage.getItem(activeProgramsKey)).toEqual(testPrograms);

                done();
            });
        });

        it(":requirementsRepository returns error when unable to retreive requirements", function (done) {

            spyOn($, "ajax").and.callFake(function (e) {
                return $.Deferred().reject();
            });

            requirementsRepository.get(studentId).catch(function (result) {
                expect(result).toEqual(errorMessage);

                expect(storage.getItem(evaluationsKey)).toEqual(null);
                expect(storage.getItem(activeProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is missing", function (done) {

            requirementsRepository.get().catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.requirementsRepositoryMessages.studentIdRequired);

                expect(storage.getItem(evaluationsKey)).toEqual(null);
                expect(storage.getItem(activeProgramsKey)).toEqual(null);

                done();
            });
        });

        it(":returns error when student ID is empty", function (done) {
            requirementsRepository.get("").catch(function (result) {
                expect(result).toEqual(Ellucian.Planning.requirementsRepositoryMessages.studentIdRequired);

                expect(storage.getItem(evaluationsKey)).toEqual(null);
                expect(storage.getItem(activeProgramsKey)).toEqual(null);

                done();
            });
        });
    });
});