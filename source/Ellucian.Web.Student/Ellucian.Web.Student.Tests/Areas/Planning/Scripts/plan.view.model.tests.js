﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
forceEnableRegistrationButtons = false;
currentUserId = "1234";

describe("PlanViewModel", function () {

    var viewModel,
        isProxyUser,
        baseUrl;

    var programRequirementsSubscriberModelInstance = new Ellucian.Planning.programRequirementsSubscriberModel();

    var generatePlan = function () {
        return ko.observable({
            PersonId: ko.observable("12345"),
            DegreePlanDto: {}
        });
    };

    beforeEach(function () {
        isProxyUser = false;
        baseUrl = "fakeUrl.com";
        viewModel = new Ellucian.Planning.PlanViewModel(isProxyUser, baseUrl, Ellucian.Planning.planRepository);
    });

    describe("Constructor", function () {

        it(":planningTerms is observableArray", function () {
            expect(ko.isObservable(viewModel.planningTerms)).toBeTruthy();
            expect(Array.isArray(viewModel.planningTerms())).toBeTruthy();
        });

        it(":isShowingSchedule is true", function () {
            expect(viewModel.isShowingSchedule).toBeTruthy();
        });
    });

    describe("updatePlanningTerms", function () {

        it(":clears planningTerms and adds new data", function () {
            original = [{ id: "fake1" }];
            updated = [{ id: "fake2" }, { id: "fake3" }]

            viewModel.planningTerms(original);

            viewModel.updatePlanningTerms(updated);

            expect(viewModel.planningTerms().length).toBe(updated.length);
        });

    });

    describe("ShowAvailableSections", function () {
        it("should call exposeAvailableSections if sections have been retreived", function () {
            spyOn(viewModel, 'exposeAvailableSections');

            var term = new Term(null);
            term.AllowRegistration = ko.observable(false);

            viewModel.currentTerm(term);
            viewModel.currentTerm().availableSectionsRetrieved(true);
            viewModel.ShowAvailableSections({}, {});

            expect(viewModel.exposeAvailableSections).toHaveBeenCalled();
        });

        it("should get available sections if they haven't already been retreived", function () {
            spyOn(viewModel, 'getAvailableSections');

            var term = new Term(null);
            term.AllowRegistration = ko.observable(false);

            viewModel.currentTerm(term);
            viewModel.currentTerm().availableSectionsRetrieved(false);
            viewModel.ShowAvailableSections({}, {});

            expect(viewModel.getAvailableSections).toHaveBeenCalled();
        });
    });

    describe("commitRemoveCourse", function () {

        beforeEach(function () {
            viewModel.removingCourseId("1234");
            viewModel.removingCourseTerm("1977/Spring");
            viewModel.DegreePlan = generatePlan();
        });

        it("sets LoadingMessage, closes dialog, and invokes planRepository.removeCourse", function () {

            spyOn(Ellucian.Planning.planRepository, "removeCourse").and.callFake(function (e) {
                return Promise.resolve();
            });

            viewModel.commitRemoveCourse();

            expect(viewModel.LoadingMessage()).toEqual(Ellucian.Planning.planViewModelMessages.removingCourseProcessing);
            expect(viewModel.removeCourseDialogIsOpen()).toEqual(false);
            expect(Ellucian.Planning.planRepository.removeCourse).toHaveBeenCalled();
        });
    });

    it("addTermButton set correctly on initialization", function () {
        expect(viewModel.addTermButton).toEqual({
            id: 'schedplan-addtermformaddbutton',
            title: Ellucian.DegreePlans.Resources.UiAddTermPopupButtonText,
            isPrimary: true,
            callback: viewModel.CommitAddTerm
        });
    });

    it("addTermCancelButton set correctly on initialization", function () {
        expect(viewModel.addTermCancelButton).toEqual({
            id: 'schedplan-addtermformcancelbutton',
            title: Ellucian.DegreePlans.Resources.UiAddTermCancelButtonText,
            isPrimary: false,
            callback: viewModel.CancelAddTerm
        });
    });

    it("clearTermButton set correctly on initialization", function () {
        expect(viewModel.clearTermButton).toEqual({
            id: 'dp-clearterm-remove',
            title: Ellucian.DegreePlans.Resources.RemoveItemButtonText,
            isPrimary: true,
            callback: viewModel.CommitClearPlannedCourses
        });
    });

    it("clearTermCancelButton set correctly on initialization", function () {
        expect(viewModel.clearTermCancelButton).toEqual({
            id: 'dp-clearterm-cancel',
            title: Ellucian.DegreePlans.Resources.RemoveItemCancelButtonText,
            enabled: viewModel.clearTermSelected,
            isPrimary: false,
            callback: viewModel.CancelClearPlannedTerms
        });
    });

    it("dropSectionButton set correctly on initialization", function () {
        expect(viewModel.dropSectionButton).toEqual({
            id: 'dp-dropsection-commit',
            title: Ellucian.DegreePlans.Resources.DropDialogDrop,
            isPrimary: true,
            callback: viewModel.CommitDrop
        });
    });

    it("dropSectionCancelButton set correctly on initialization", function () {
        expect(viewModel.dropSectionCancelButton).toEqual({
            id: 'dp-dropsection-cancel',
            title: Ellucian.DegreePlans.Resources.DropDialogCancel,
            isPrimary: false,
            callback: viewModel.CancelDropSection
        });
    });

    it("removeCourseRemoveButton set correctly on initialization", function () {
        expect(viewModel.removeCourseRemoveButton).toEqual({
            id: 'schedplan-removecourse-remove',
            title: Ellucian.DegreePlans.Resources.RemoveItemButtonText,
            isPrimary: true,
            callback: viewModel.commitRemoveCourse
        });
    });

    it("removeCourseCancelButton set correctly on initialization", function () {
        expect(viewModel.removeCourseCancelButton).toEqual({
            id: 'schedplan-removecourse-cancel',
            title: Ellucian.DegreePlans.Resources.RemoveItemCancelButtonText,
            isPrimary: false,
            callback: viewModel.CancelRemoveCourse
        });
    });

    it("removeTermButton set correctly on initialization", function () {
        expect(viewModel.removeTermButton).toEqual({
            id: 'dp-removeterm-remove',
            title: Ellucian.DegreePlans.Resources.RemoveItemButtonText,
            isPrimary: true,
            callback: viewModel.CommitRemoveTerm
        });
    });

    it("removeTermCancelButton set correctly on initialization", function () {
        expect(viewModel.removeTermCancelButton).toEqual({
            id: 'dp-removeterm-cancel',
            title: Ellucian.DegreePlans.Resources.RemoveItemCancelButtonText,
            isPrimary: false,
            callback: viewModel.CancelRemoveTerm
        });
    });

    describe("program requirements subscriber is called", function () {
        //check degreePlanChanged is published
        it("degreePlanChanged observable is published on topic", function () {
            expect(viewModel.degreePlanChanged()).toBe(false);
        });
       
        it("program reequirments subscriber update session is called when degreePlanChanged topic is set to true", function () {
            currentUserId = "432156";
            viewModel.removingCourseId("1234");
            viewModel.removingCourseTerm("1977/Spring");
            viewModel.DegreePlan = generatePlan();
            spyOn(programRequirementsSubscriberModelInstance, "updateSession");
            spyOn(Ellucian.Planning.planRepository, "removeCourse").and.callFake(function (e) {
                return Promise.resolve();
            });

            viewModel.commitRemoveCourse();
            setTimeout(function () {
                expect(programRequirementsSubscriberModelInstance.updateSession).toHaveBeenCalled();

            }, 2000);
        });
    });


  
});
