﻿describe("tableFilters", function () {
    var arrayToSortModel=function()
    {
        var self = this;
        self.gradesModel = ko.observableArray([]);
        self.gradesModel.sortField = ko.observable();
        self.gradesModel.sortDirection = ko.observable();
    }
    
    var studentModel = function (initValues) {
        var self = this;
        self.studentId = initValues.studentId;
        self.lastName = initValues.lastName || ""; //string type
        self.firstName = initValues.firstName || ""; //string type
        self.middleName = initValues.middleName || ""; //string type
        self.credits = initValues.credits||0; //number type
        self.attendanceDate = initValues.attendanceDate||"01/02/2016"; //date as string
        self.age = initValues.age||"20"; //number as string
        self.expireDate =initValues.expireDate|| new Date();//date type
        self.grade=initValues.grade||"A";
       
    };
    var arrayToSort = new arrayToSortModel();
   
    
    beforeEach(function () {
        arrayToSort.gradesModel.removeAll();
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smith',firstName:'andrea',middleName:'', studentId:'00001',attendanceDate:'1/1/2018', credits: 39 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'smith', firstName: 'Andrea', middleName: '', studentId: '00002', attendanceDate: '3/1/2016', credits: 3 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smith', firstName: 'andrea', middleName: '', studentId: '00003', attendanceDate: '2/1/2017', credits: 222 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smith', firstName: 'Andrea', middleName: 'aaa', studentId: '00004', attendanceDate: '12/22/2017', credits: 4 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smithson', firstName: 'andrea', middleName: '', studentId: '00005', attendanceDate: '4/22/2017', credits: 30 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'smith', firstName: 'bhandrea', middleName: '', studentId: '00006', attendanceDate: '12/31/2016', credits: 1 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smith', firstName: 'bhakrea', middleName: '', studentId: '00007', attendanceDate: '12/31/2016', credits: 2.5 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smith', firstName: 'bhakrea', middleName: 'again', studentId: '00008', attendanceDate: '2/22/2018', credits: 100 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'zach', firstName: 'andrea', middleName: '', studentId: '00009', attendanceDate: '2/22/2017', credits: 2 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'AUSTIN', firstName: 'andrea', middleName: '', studentId: '000010', attendanceDate: '2/22/2016', credits: 33.7 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smith', firstName: 'bhakrea', middleName: '', studentId: '000011', attendanceDate: '12/31/2016', credits: 2.5 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'Smith', firstName: 'bhakrea', middleName: 'again', studentId: '000012', attendanceDate: '2/22/2018', credits: 100 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: 'zach', firstName: 'andrea', middleName: '', studentId: '000013', attendanceDate: '2/22/2017', credits: 2 }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: null, firstName: null, middleName: null, studentId: '000014', attendanceDate: null, credits: null }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: null, firstName: null, middleName: null, studentId: '000015', attendanceDate: null, credits: null }));
        arrayToSort.gradesModel.push(new studentModel({  firstName: null, middleName: null, studentId: '000016', credits: null }));
        arrayToSort.gradesModel.push(new studentModel({ lastName: null, firstName: null, middleName: null, studentId: '000017', attendanceDate: null}));

    });
    
    //sort on ascending orders
    
    it("array after initialization", function () {
        expect(arrayToSort.gradesModel().length).toBe(17);
        expect(arrayToSort.gradesModel()[0].studentId).toBe('00001');
        expect(arrayToSort.gradesModel()[1].studentId).toBe('00002');
        expect(arrayToSort.gradesModel()[2].studentId).toBe('00003');
        expect(arrayToSort.gradesModel()[3].studentId).toBe('00004');
        expect(arrayToSort.gradesModel()[4].studentId).toBe('00005');
        expect(arrayToSort.gradesModel()[5].studentId).toBe('00006');
        expect(arrayToSort.gradesModel()[6].studentId).toBe('00007');
        expect(arrayToSort.gradesModel()[7].studentId).toBe('00008');
        expect(arrayToSort.gradesModel()[8].studentId).toBe('00009');
        expect(arrayToSort.gradesModel()[9].studentId).toBe('000010');
        expect(arrayToSort.gradesModel()[10].studentId).toBe('000011');
        expect(arrayToSort.gradesModel()[11].studentId).toBe('000012');
        expect(arrayToSort.gradesModel()[12].studentId).toBe('000013');
        expect(arrayToSort.gradesModel()[13].studentId).toBe('000014');
        expect(arrayToSort.gradesModel()[14].studentId).toBe('000015');
        expect(arrayToSort.gradesModel()[15].studentId).toBe('000016');
        expect(arrayToSort.gradesModel()[16].studentId).toBe('000017');
    });
    
    //sort names
    it(":sort asc on lastName;firstName;middleName", function () {
        arrayToSort.gradesModel.sortField('lastName;firstName;middleName');
        arrayToSort.gradesModel.sortDirection('asc');
        tableFilters.sorting.sort(arrayToSort.gradesModel);
        
        expect(arrayToSort.gradesModel()[0].studentId).toBe('000014');
        expect(arrayToSort.gradesModel()[1].studentId).toBe('000015');
        expect(arrayToSort.gradesModel()[2].studentId).toBe('000016');
        expect(arrayToSort.gradesModel()[3].studentId).toBe('000017');
        expect(arrayToSort.gradesModel()[4].studentId).toBe('000010');
        expect(arrayToSort.gradesModel()[5].studentId).toBe('00001');
        expect(arrayToSort.gradesModel()[6].studentId).toEqual('00002', "actual data is: " + arrayToSort.gradesModel()[0].studentId);
        expect(arrayToSort.gradesModel()[7].studentId).toBe('00003');
        expect(arrayToSort.gradesModel()[8].studentId).toBe('00004');
        expect(arrayToSort.gradesModel()[9].studentId).toBe('00007');
        expect(arrayToSort.gradesModel()[10].studentId).toBe('000011');
        expect(arrayToSort.gradesModel()[11].studentId).toBe('00008');
        expect(arrayToSort.gradesModel()[12].studentId).toBe('000012');
        expect(arrayToSort.gradesModel()[13].studentId).toBe('00006');
        expect(arrayToSort.gradesModel()[14].studentId).toBe('00005');
        expect(arrayToSort.gradesModel()[15].studentId).toBe('00009');
        expect(arrayToSort.gradesModel()[16].studentId).toBe('000013');
    });

    it(":sort asc on lastName", function () {
        arrayToSort.gradesModel.sortField('lastName');
        arrayToSort.gradesModel.sortDirection('asc');
        tableFilters.sorting.sort(arrayToSort.gradesModel);
  
        expect(arrayToSort.gradesModel()[0].studentId).toBe('000014');
        expect(arrayToSort.gradesModel()[1].studentId).toBe('000015');
        expect(arrayToSort.gradesModel()[2].studentId).toBe('000016');
        expect(arrayToSort.gradesModel()[3].studentId).toBe('000017');
        expect(arrayToSort.gradesModel()[4].studentId).toBe('000010');
        expect(arrayToSort.gradesModel()[5].studentId).toBe('00001');
        expect(arrayToSort.gradesModel()[6].studentId).toEqual('00002', "actual data is: " + arrayToSort.gradesModel()[0].studentId);
        expect(arrayToSort.gradesModel()[7].studentId).toBe('00003');
        expect(arrayToSort.gradesModel()[8].studentId).toBe('00004');
        expect(arrayToSort.gradesModel()[9].studentId).toBe('00006');
        expect(arrayToSort.gradesModel()[10].studentId).toBe('00007');
        expect(arrayToSort.gradesModel()[11].studentId).toBe('00008');
        expect(arrayToSort.gradesModel()[12].studentId).toBe('000011');
        expect(arrayToSort.gradesModel()[13].studentId).toBe('000012');
        expect(arrayToSort.gradesModel()[14].studentId).toBe('00005');
        expect(arrayToSort.gradesModel()[15].studentId).toBe('00009');
        expect(arrayToSort.gradesModel()[16].studentId).toBe('000013');
       
    });

    ////sort dates
    it(":sort asc on attendanceDate", function () {
        arrayToSort.gradesModel.sortField('attendanceDate');
        arrayToSort.gradesModel.sortDirection('asc');
        tableFilters.sorting.sort(arrayToSort.gradesModel);
        expect(arrayToSort.gradesModel()[0].studentId).toBe('000014');
        expect(arrayToSort.gradesModel()[1].studentId).toBe('000015');
        expect(arrayToSort.gradesModel()[2].studentId).toBe('000016');
        expect(arrayToSort.gradesModel()[3].studentId).toBe('000017');
        expect(arrayToSort.gradesModel()[4].studentId).toBe('000010');
        expect(arrayToSort.gradesModel()[5].studentId).toBe('00002');
        expect(arrayToSort.gradesModel()[6].studentId).toBe('00006');
        expect(arrayToSort.gradesModel()[7].studentId).toBe('00007');
        expect(arrayToSort.gradesModel()[8].studentId).toBe('000011');
          expect(arrayToSort.gradesModel()[9].studentId).toBe('00003');
          expect(arrayToSort.gradesModel()[10].studentId).toBe('00009');
          expect(arrayToSort.gradesModel()[11].studentId).toBe('000013');
         expect(arrayToSort.gradesModel()[12].studentId).toBe('00005');
        expect(arrayToSort.gradesModel()[13].studentId).toBe('00004');
        expect(arrayToSort.gradesModel()[14].studentId).toBe('00001');
        expect(arrayToSort.gradesModel()[15].studentId).toBe('00008');
        expect(arrayToSort.gradesModel()[16].studentId).toBe('000012');
      
    });

    //sort numbers
    it(":sort asc on credits", function () {
        arrayToSort.gradesModel.sortField('credits');
        arrayToSort.gradesModel.sortDirection('asc');
        tableFilters.sorting.sort(arrayToSort.gradesModel);
        expect(arrayToSort.gradesModel()[0].studentId).toBe('000014');
        expect(arrayToSort.gradesModel()[1].studentId).toBe('000015');
        expect(arrayToSort.gradesModel()[2].studentId).toBe('000016');
        expect(arrayToSort.gradesModel()[3].studentId).toBe('000017');
        expect(arrayToSort.gradesModel()[4].studentId).toBe('00006');
        expect(arrayToSort.gradesModel()[5].studentId).toBe('00009');
        expect(arrayToSort.gradesModel()[6].studentId).toBe('000013');
        expect(arrayToSort.gradesModel()[7].studentId).toBe('00007');
        expect(arrayToSort.gradesModel()[8].studentId).toBe('000011');
        expect(arrayToSort.gradesModel()[9].studentId).toBe('00002');
        expect(arrayToSort.gradesModel()[10].studentId).toBe('00004');
        expect(arrayToSort.gradesModel()[11].studentId).toBe('00005');
        expect(arrayToSort.gradesModel()[12].studentId).toBe('000010');
        expect(arrayToSort.gradesModel()[13].studentId).toBe('00001');
        expect(arrayToSort.gradesModel()[14].studentId).toBe('00008');
        expect(arrayToSort.gradesModel()[15].studentId).toBe('000012');
        expect(arrayToSort.gradesModel()[16].studentId).toBe('00003');


    });
    ////sort boolean
    ////sort objects

    ////sort on descending orders
    //sort dates
    it(":sort desc on attendanceDate", function () {
        arrayToSort.gradesModel.sortField('attendanceDate');
        arrayToSort.gradesModel.sortDirection('desc');
        tableFilters.sorting.sort(arrayToSort.gradesModel);
        expect(arrayToSort.gradesModel()[16].studentId).toBe('000017');
        expect(arrayToSort.gradesModel()[15].studentId).toBe('000016');
        expect(arrayToSort.gradesModel()[14].studentId).toBe('000015');
        expect(arrayToSort.gradesModel()[13].studentId).toBe('000014');
        expect(arrayToSort.gradesModel()[12].studentId).toBe('000010');
        expect(arrayToSort.gradesModel()[11].studentId).toBe('00002');
        expect(arrayToSort.gradesModel()[10].studentId).toBe('000011');
        expect(arrayToSort.gradesModel()[9].studentId).toBe('00007');
        expect(arrayToSort.gradesModel()[8].studentId).toBe('00006');
        expect(arrayToSort.gradesModel()[7].studentId).toBe('00003');
        expect(arrayToSort.gradesModel()[6].studentId).toBe('000013');
        expect(arrayToSort.gradesModel()[5].studentId).toBe('00009');
        expect(arrayToSort.gradesModel()[4].studentId).toBe('00005');
        expect(arrayToSort.gradesModel()[3].studentId).toBe('00004');
        expect(arrayToSort.gradesModel()[2].studentId).toBe('00001');
        expect(arrayToSort.gradesModel()[1].studentId).toBe('000012');
        expect(arrayToSort.gradesModel()[0].studentId).toBe('00008');


      

    });

    //sort numbers
    it(":sort desc on credits", function () {
        arrayToSort.gradesModel.sortField('credits');
        arrayToSort.gradesModel.sortDirection('desc');
        tableFilters.sorting.sort(arrayToSort.gradesModel);
        
        expect(arrayToSort.gradesModel()[16].studentId).toBe('000017');
        expect(arrayToSort.gradesModel()[15].studentId).toBe('000016');
        expect(arrayToSort.gradesModel()[14].studentId).toBe('000015');
        expect(arrayToSort.gradesModel()[13].studentId).toBe('000014');
        expect(arrayToSort.gradesModel()[12].studentId).toBe('00006');
        expect(arrayToSort.gradesModel()[11].studentId).toBe('000013');
        expect(arrayToSort.gradesModel()[10].studentId).toBe('00009');
        expect(arrayToSort.gradesModel()[9].studentId).toBe('000011');
        expect(arrayToSort.gradesModel()[8].studentId).toBe('00007');
        expect(arrayToSort.gradesModel()[7].studentId).toBe('00002');
        expect(arrayToSort.gradesModel()[6].studentId).toBe('00004');
        expect(arrayToSort.gradesModel()[5].studentId).toBe('00005');
        expect(arrayToSort.gradesModel()[4].studentId).toBe('000010');
        expect(arrayToSort.gradesModel()[3].studentId).toBe('00001');
        expect(arrayToSort.gradesModel()[2].studentId).toBe('000012');
        expect(arrayToSort.gradesModel()[1].studentId).toBe('00008');
        expect(arrayToSort.gradesModel()[0].studentId).toBe('00003');
    });
});