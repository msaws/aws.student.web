﻿var adviseeListActionUrl = '@Url.Action("AdviseesAsync", "Advisors")';

describe("adviseeViewModel", function () {
    var viewModel;
    beforeEach(function () {
        viewModel = new Ellucian.Planning.AdviseesViewModel();
    });


    describe("initialization", function () {

        it(":attributes set correctly after initialization", function () {
            expect(viewModel.noAssignedFound()).toBe(false);
            expect(viewModel.noAdviseesFound()).toBe(false);
            expect(viewModel.moreAdvisees()).toBe(false);
            
        });
    });
});