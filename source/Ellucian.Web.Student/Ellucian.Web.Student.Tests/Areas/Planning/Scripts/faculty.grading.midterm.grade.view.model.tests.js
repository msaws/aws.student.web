﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var grade = {
    IsGradeVerified: ko.observable(true),
    FinalGrade: ko.observable("A"),
    hasErrors: ko.observable(false),
    isValid: ko.observable(true),
    isExpireDateRequired: ko.observable(true),
    isLDARequired: ko.observable(true),
    StudentId: ko.observable("0001234"),
    finalGradeSelected: ko.observable(),
    gradeEntryErrors: ko.observableArray(null),
    GradeExpireDate: ko.observable(),
    NeverAttended: ko.observable(),
    LastDateAttended: ko.observable(),
    Midterm1Grade: ko.observable(),
    Midterm2Grade: ko.observable(),
    Midterm3Grade: ko.observable(),
    Midterm4Grade: ko.observable(),
    Midterm5Grade: ko.observable(),
    Midterm6Grade: ko.observable(),
};

describe("faculty.grading.midterm.grade.view.model.js Tests: ", function () {

    describe("midtermGradingModel: ", function () {

        beforeEach(function () {
            viewModel = new midtermGradingModel();
        })

        it("calls ko.validation.init with options on initialization", function () {
            spyOn(ko.validation, 'init').and.callThrough();

            viewModel = new midtermGradingModel();

            expect(ko.validation.init).toHaveBeenCalledWith({
                messagesOnModified: true,
                insertMessages: false,
                decorateInputElement: true,
                errorsAsTitle: true
            });
        });

        it("StudentGrades is an empty array on initialization", function () {
            expect(viewModel.StudentGrades()).toEqual([]);
        });

        describe("areThereAnyErrors: ", function () {

            beforeEach(function () {
                viewModel.StudentGrades.push(grade);
            });

            afterEach(function () {
                viewModel.StudentGrades.removeAll();
            });

            it("returns 0 when hasErrors if false for all items", function () {
                expect(viewModel.areThereAnyErrors()).toEqual(0);
            });

            it("returns count of items with hasErrors = true", function () {
                viewModel.StudentGrades()[0].isValid(false);
                viewModel.StudentGrades()[0].hasErrors(true);

                expect(viewModel.areThereAnyErrors()).toEqual(1);
            });

        });

        it("GradeTypes is an empty array on initialization", function () {
            expect(viewModel.GradeTypes()).toEqual([]);
        });

        it("SectionId is undefined on initialization", function () {
            expect(viewModel.SectionId()).toEqual(undefined);
        });

        it("addStudentGrade adds item to StudentGrades", function () {
            viewModel.addStudentGrade(grade);

            expect(viewModel.StudentGrades().length).toEqual(1);
        });

        describe("hasStudentGrades: ", function () {

            it("returns false when StudentGrades is null", function () {
                viewModel.StudentGrades(null);

                expect(viewModel.hasStudentGrades()).toBeFalsy();
            });

            it("returns false when StudentGrades is undefined", function () {
                viewModel.StudentGrades(undefined);

                expect(viewModel.hasStudentGrades()).toBeFalsy();
            });

            it("returns false when StudentGrades is empty", function () {
                viewModel.StudentGrades([]);

                expect(viewModel.hasStudentGrades()).toBeFalsy();
            });

            it("returns true when StudentGrades is not null or empty", function () {
                viewModel.StudentGrades.push(grade);

                expect(viewModel.hasStudentGrades()).toBeTruthy();
            });
        });

    });

    describe("studentMidTermGradeModel: ", function () {

        beforeEach(function () {
            viewModel = new studentMidTermGradeModel(grade);
        });

        it("calls ko.mapping.fromJS on initialization with data, mapping, and self", function () {
            spyOn(ko.mapping, 'fromJS').and.callThrough();

            viewModel = new studentMidTermGradeModel(grade);

            expect(ko.mapping.fromJS).toHaveBeenCalledWith(grade, {
                'ignore': ['FinalGrade', 'IsGradeVerified', 'GradeExpireDate', 'LastDateAttended']
            }, viewModel);
        });

        it("calls baseStudentGradeModel on initialization with self", function () {
            spyOn(baseStudentGradeModel, 'call').and.callThrough();

            viewModel = new studentMidTermGradeModel(grade);

            expect(baseStudentGradeModel.call).toHaveBeenCalledWith(viewModel);
        });

        describe("hasErrors: ", function () {

            beforeEach(function () {
                viewModel.gradeEntryErrors.removeAll();
            })

            it("returns true if gradeEntryErrors contains any items", function () {
                viewModel.gradeEntryErrors.push('error');

                expect(viewModel.hasErrors()).toBeTruthy();
            });

            it("returns false if gradeEntryErrors is empty", function () {
                expect(viewModel.hasErrors()).toBeFalsy();
            })

        });

        describe("Midterm1Grade subscription: ", function () {

            it("sets isFieldDirty to true, isSuccessfullyUpdated to false, and calls checkIfNeedToSave when Midterm1Grade is not null or empty", function () {
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm1Grade('A');

                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeFalsy();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("does not set isFieldDirty or isSuccessfullyUpdated or call checkIfNeedToSave when Midterm1Grade is null", function () {
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm1Grade(null);

                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.checkIfNeedToSave).not.toHaveBeenCalled();
            });
        });

        describe("Midterm2Grade subscription: ", function () {

            it("sets isFieldDirty to true, isSuccessfullyUpdated to false, and calls checkIfNeedToSave when Midterm2Grade is not null or empty", function () {
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm2Grade('A');

                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeFalsy();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("does not set isFieldDirty or isSuccessfullyUpdated or call checkIfNeedToSave when Midterm2Grade is null", function () {
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm2Grade(null);

                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.checkIfNeedToSave).not.toHaveBeenCalled();
            });
        });

        describe("Midterm3Grade subscription: ", function () {

            it("sets isFieldDirty to true, isSuccessfullyUpdated to false, and calls checkIfNeedToSave when Midterm3Grade is not null or empty", function () {
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm3Grade('A');

                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeFalsy();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("does not set isFieldDirty or isSuccessfullyUpdated or call checkIfNeedToSave when Midterm3Grade is null", function () {
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm3Grade(null);

                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.checkIfNeedToSave).not.toHaveBeenCalled();
            });
        });

        describe("Midterm4Grade subscription: ", function () {

            it("sets isFieldDirty to true, isSuccessfullyUpdated to false, and calls checkIfNeedToSave when Midterm4Grade is not null or empty", function () {
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm4Grade('A');

                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeFalsy();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("does not set isFieldDirty or isSuccessfullyUpdated or call checkIfNeedToSave when Midterm4Grade is null", function () {
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm4Grade(null);

                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.checkIfNeedToSave).not.toHaveBeenCalled();
            });
        });

        describe("Midterm5Grade subscription: ", function () {

            it("sets isFieldDirty to true, isSuccessfullyUpdated to false, and calls checkIfNeedToSave when Midterm5Grade is not null or empty", function () {
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm5Grade('A');

                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeFalsy();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("does not set isFieldDirty or isSuccessfullyUpdated or call checkIfNeedToSave when Midterm5Grade is null", function () {
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm5Grade(null);

                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.checkIfNeedToSave).not.toHaveBeenCalled();
            });
        });

        describe("Midterm6Grade subscription: ", function () {

            it("sets isFieldDirty to true, isSuccessfullyUpdated to false, and calls checkIfNeedToSave when Midterm6Grade is not null or empty", function () {
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm6Grade('A');

                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeFalsy();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("does not set isFieldDirty or isSuccessfullyUpdated or call checkIfNeedToSave when Midterm6Grade is null", function () {
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.Midterm6Grade(null);

                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.checkIfNeedToSave).not.toHaveBeenCalled();
            });
        });

        describe("checkIfNeedToSave: ", function () {

            it("calls postStudentGrade if isFieldDirty is true", function () {
                spyOn(viewModel, 'postStudentGrade');
                viewModel.isFieldDirty(true);

                viewModel.checkIfNeedToSave();

                expect(viewModel.postStudentGrade).toHaveBeenCalled();
            });

            it("does not call postStudentGrade if isFieldDirty is not true", function () {
                spyOn(viewModel, 'postStudentGrade');
                viewModel.isFieldDirty(false);

                viewModel.checkIfNeedToSave();

                expect(viewModel.postStudentGrade).not.toHaveBeenCalled();
            });

        })

    });

});