﻿describe("StudentPetitionsWaiversViewModel", function () {

    // viewModel is the local instance of the view model we are testing
    var viewModel;
    beforeEach(function () {
        viewModel = new StudentPetitionsWaiversViewModel();
        data = {

            "StudentPetitions": [{
                "PetitionId": "1",
                "Course": "C1",
                "UpdatedOn": "01/02/2015",
                "EndDate": "",
                "Reason": "R1",
                "SectionId": "",
                "Section": "2",
                "Status": "A",
                "StartDate": "",
                "StudentId": "001123",
                "FreeformExplanation": "",
                "Term": "2013/FA"

            }, {
                "PetitionId": "2",
                "Course": "C2",
                "UpdatedOn": "02/02/2015",
                "EndDate": "",
                "Reason": "R1",
                "SectionId": "",
                "Section": "",
                "Status": "",
                "StartDate": "",
                "StudentId": "001123",
                "FreeformExplanation": "",
                "Term": "2014/FA"
            }],
            "StudentConsents": [{
                "PetitionId": "3",
                "Course": "C1",
                "UpdatedOn": "01/02/2015",
                "EndDate": "",
                "Reason": "R1",
                "SectionId": "",
                "Section": "",
                "Status": "",
                "StartDate": "",
                "StudentId": "001123",
                "FreeformExplanation": "",
                "Term": "2013/FA"
            }, {
                "PetitionId": "4",
                "Course": "C1",
                "UpdatedOn": "01/02/2015",
                "EndDate": "",
                "Reason": "R1",
                "SectionId": "",
                "Section": "",
                "Status": "",
                "StartDate": "",
                "StudentId": "001123",
                "FreeformExplanation": "",
                "Term": "2013/FA"
            }],
            "StudentWaivers": [{
                "StudentId": "001123",
                "UpdatedOn": "02/02/2015",
                "Course": "C1",
                "Section": "S1",
                "Term": "2015/FA",
                "StartDate": "",
                "EndDate": "",
                "Prerequisite": "A B C",
                "Status": "Approved"
            }, {
                "StudentId": "001123",
                "UpdatedOn": "02/02/2015",
                "Course": "C2",
                "Section": "S1",
                "Term": "",
                "StartDate": "01/05/2015",
                "EndDate": "",
                "Prerequisite": "A B C",
                "Status": "Approved"
            },
            {
                "StudentId": "001123",
                "UpdatedOn": "02/02/2015",
                "Course": "C2",
                "Section": "S1",
                "Term": "",
                "StartDate": "01/05/2015",
                "EndDate": "05/01/2015",
                "Prerequisite": "A B C",
                "Status": "Approved"
            }]
        };
        
    });

    it(":isLoaded is false after initialization", function () {
        expect(viewModel.isLoaded()).toBeFalsy();
    });
    it(":StudentWaivers is empty array after initilization", function () {
        expect(viewModel.StudentWaivers().constructor).toBe(Array);
        expect(viewModel.StudentWaivers().length).toBe(0);
    });
    it(":StudentPetitions is empty array after initialization", function () {
        expect(viewModel.StudentPetitions().constructor).toBe(Array);
        expect(viewModel.StudentPetitions().length).toBe(0);
    });
    it(":StudentConsents is empty array after initialization", function () {
        expect(viewModel.StudentConsents().constructor).toBe(Array);
        expect(viewModel.StudentConsents().length).toBe(0);
    });
    it(":hasPetitions is false after initialization", function () {
        expect(viewModel.hasPetitions()).toBeFalsy();
    });
    it(":hasConsents is false after initialization", function () {
        expect(viewModel.hasConsents()).toBeFalsy();
    });
    it(":hasWaivers is false after initialization", function () {
        expect(viewModel.hasWaivers()).toBeFalsy();
    });

    it(":hasWaivers is true and waivers array is populated after mapping", function () {
        ko.mapping.fromJS(data, planMapping, viewModel);
        expect(viewModel.hasWaivers()).toBe(true);
        expect(viewModel.StudentWaivers().length).toBe(3);
        expect(viewModel.StudentWaivers()[0].Course()).toBe("C1");
        expect(viewModel.StudentWaivers()[1].Course()).toBe("C2");
    });
    it(":hasPetitions is true and petitions array is populated after mapping", function () {
        ko.mapping.fromJS(data, planMapping, viewModel);
        expect(viewModel.hasPetitions()).toBe(true);
        expect(viewModel.StudentPetitions().length).toBe(2);
        expect(viewModel.StudentPetitions()[0].PetitionId()).toBe("1");
        expect(viewModel.StudentPetitions()[1].PetitionId()).toBe("2");
    });
    it(":hasConsents is true and consents array is populated after mapping", function () {
        ko.mapping.fromJS(data, planMapping, viewModel);
        expect(viewModel.hasConsents()).toBe(true);
        expect(viewModel.StudentConsents().length).toBe(2);
        expect(viewModel.StudentConsents()[0].PetitionId()).toBe("3");
        expect(viewModel.StudentConsents()[1].PetitionId()).toBe("4");
    });
    
    it(":Waivers array has computed Period after mapping", function () {
        ko.mapping.fromJS(data, planMapping, viewModel);
        expect(viewModel.StudentWaivers()[0].Period()).toBe("2015/FA");
        expect(viewModel.StudentWaivers()[1].Period()).toBe("01/05/2015 - Unlimited");
        expect(viewModel.StudentWaivers()[2].Period()).toBe("01/05/2015 - 05/01/2015");
    });
});
