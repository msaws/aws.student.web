﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.


//This is to test pub/sub relationship between degree-plan and program-evaluation for student view
describe("planning-evaluation-session-keys-tests", function () {
    var viewModel,
       isProxyUser,
       baseUrl;
    var programRequirementsSubscriberModelInstance = new Ellucian.Planning.programRequirementsSubscriberModel();
    var loadSamplePlanInstance = new loadSamplePlanViewModel();
    currentUserId = "12345";
    var degreePlanKey = "degreeplan" + currentUserId;
    var studentProgramsKey = "studentprograms" + currentUserId;
    var evaluationsKey = "evaluations" + currentUserId;
    var activeProgramsKey = "activeprograms" + currentUserId;
    var storage = Ellucian.Storage.session;
  
  

    var generatePlan = function () {
        return ko.observable({
            PersonId: ko.observable("12345"),
            DegreePlanDto: {}
        });
    };

    beforeEach(function () {
        isProxyUser = false;
        baseUrl = "fakeUrl.com";
        viewModel = new Ellucian.Planning.PlanViewModel(isProxyUser, baseUrl, Ellucian.Planning.planRepository);
        viewModel.DegreePlan = generatePlan();
        storage.setItem(degreePlanKey, { testPlan: "testPlan" });
        storage.setItem(studentProgramsKey, { testPrograms: "testPrograms" });
        storage.setItem(evaluationsKey, { testEvaluations: "testEvaluations" });
        storage.setItem(activeProgramsKey, { testPrograms: "testPrograms" });

    });
    afterEach(function () {
        storage.clear();
    });

    describe("evaluation keys in co-ordination with degree-plan", function () {


        it("when course is removed from  degree plan , evaluation storage keys are removed", function () {
            viewModel.removingCourseId("1234");
            viewModel.removingCourseTerm("1977/Spring");
            var eval=storage.getItem(evaluationsKey);
            var activePrograms = storage.getItem(activeProgramsKey);
            expect(eval).not.toBeUndefined();
            expect(eval).not.toBeNull();
            expect(activePrograms).not.toBeUndefined();
            expect(activePrograms).not.toBeNull();
            spyOn(programRequirementsSubscriberModelInstance,"updateSession");
            spyOn(Ellucian.Planning.planRepository, "removeCourse").and.callFake(function (e) {
                return Promise.resolve();
            });

            viewModel.commitRemoveCourse();
            setTimeout(function () {
                expect(programRequirementsSubscriberModelInstance.updateSession).toHaveBeenCalled();
                eval = storage.getItem(evaluationsKey);
                activePrograms = storage.getItem(activeProgramsKey);
                expect(eval).toBeNull();
                expect(activePrograms).toBeNull();
                storage.clear();
            }, 2000);

        });

    });

    describe("load sample plan on my progress page", function () {

        it("when sample plan is loaded, evaluation and degree plan keys are removed", function () {
           
            loadSamplePlanInstance.DegreePlan = generatePlan();
            var eval = storage.getItem(evaluationsKey);
            var activePrograms = storage.getItem(activeProgramsKey);
            var degreePlan = storage.getItem(degreePlanKey);
            var studentPrograms = storage.getItem(studentProgramsKey);
            expect(eval).not.toBeUndefined();
            expect(eval).not.toBeNull();
            expect(activePrograms).not.toBeUndefined();
            expect(activePrograms).not.toBeNull();
            expect(degreePlan).not.toBeUndefined();
            expect(degreePlan).not.toBeNull();
            expect(studentPrograms).not.toBeUndefined();
            expect(studentPrograms).not.toBeNull();
            spyOn(ko.utils, "postJson").and.callFake(function () { return true; });;
            spyOn(Ellucian.Planning.planRepository, "removeSessionKeys")
            loadSamplePlanInstance.LoadPlan();
            setTimeout(function () {
                expect(ko.utils.postJson).toHaveBeenCalled();
                expect(Ellucian.Planning.planRepository.removeSessionKeys).toHaveBeenCalled();
                eval = storage.getItem(evaluationsKey);
                activePrograms = storage.getItem(studentProgramsKey);
                 degreePlan = storage.getItem(degreePlanKey);
                 studentPrograms = storage.getItem(studentProgramsKey);
                expect(eval).toBeNull();
                expect(activePrograms).toBeNull();
                expect(degreePlan).toBeNull();
                expect(studentPrograms).toBeNull();
            }, 2000);

        });

    });
});
