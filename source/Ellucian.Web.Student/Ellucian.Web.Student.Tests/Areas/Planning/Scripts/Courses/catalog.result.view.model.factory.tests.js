﻿

describe("catalog.result.view.model.factory", function () {
    var contentType;
    var catalogResultViewModelInstance;
    var data;
    var isWithDegreePlan;
    beforeAll(function () {
        jsonContentType = "json";
        //create json string for data
        data = {
            "Subjects": [{ "Value": "ACCT", "Description": "Accounting", "Count": 4, "Selected": true },
                { "Value": "AOJU", "Description": "Administration of Justice", "Count": 2, "Selected": false },
                { "Value": "AGBU", "Description": "Agriculture Business", "Count": 3, "Selected": false },
                { "Value": "AGME", "Description": "Agriculture Mechanics", "Count": 5, "Selected": false }],
            "TermFilters": [{ "Value": "2017/SP", "Description": "2017 Spring Term", "Selected": false },
                { "Value": "2017/S1", "Description": "2017 Summer 1 Term", "Count": 2, "Selected": false },
                { "Value": "2017/S2", "Description": "2017 Summer 2 Term", "Count": 3, "Selected": false },
                { "Value": "2017/WI", "Description": "2017 Winterim Term", "Count": 4, "Selected": false },
                 { "Value": "2017/FA", "Description": "2017 Fall Term", "Count": 3, "Selected": false }],
            "Locations": [
                { "Value": "SBSD", "Description": "Sb Sheriff Dept Train Center", "Count": 2, "Selected": false },
                { "Value": "SBVC", "Description": "San Bernardino Valley College", "Count": 2, "Selected": false },
                { "Value": "SC", "Description": "SC Institute - Main Campus", "Count": 2, "Selected": false },
                { "Value": "SW", "Description": "SC Institute - Western Campus", "Count": 2, "Selected": false },
                { "Value": "TC", "Description": "Toronto College of Botany", "Count": 2, "Selected": false },
                { "Value": "TC", "Description": "Toronto College of Botany", "Count": 2, "Selected": false },
                { "Value": "TC", "Description": "Toronto College of Botany", "Count": 2, "Selected": false },
                { "Value": "TC", "Description": "Toronto College of Botany", "Count": 2, "Selected": false },
                { "Value": "TC", "Description": "Toronto College of Botany", "Count": 2, "Selected": false }],
            "AcademicLevels": [
                { "Value": "AKW", "Description": "Akw Acad Level Test", "Count": 2, "Selected": false },
                { "Value": "BMA", "Description": "Bma Test", "Count": 2, "Selected": false },
                { "Value": "CE", "Description": "Continuing Education Level", "Count": 2, "Selected": false },
                { "Value": "GR", "Description": "Graduate", "Count": 2, "Selected": false },
                { "Value": "JD", "Description": "Law", "Count": 2, "Selected": false },
                { "Value": "MIS", "Description": "Marissa\u0027s Academic Level", "Count": 2, "Selected": false },
                { "Value": "SALAL", "Description": "Salil Descriptions", "Count": 2, "Selected": false },
                { "Value": "UG", "Description": "Undergraduate", "Count": 2, "Selected": false }],

            "DaysOfWeek": [
                { "Value": "Sunday", "Description": "0", "Count": 2, "Selected": false },
                { "Value": "Monday", "Description": "1", "Count": 2, "Selected": false },
                { "Value": "Tuesday", "Description": "2", "Count": 2, "Selected": false },
                { "Value": "Wednesday", "Description": "3", "Count": 2, "Selected": false },
                { "Value": "Thursday", "Description": "4", "Count": 2, "Selected": false },
                { "Value": "Friday", "Description": "5", "Count": 2, "Selected": false },
                { "Value": "Saturday", "Description": "6", "Count": 2, "Selected": false }]
        }

        //convert json to javascript object
    });

    beforeEach(function () {
        contentType = "json";
    });



   

    describe("factory creates result view model for advisor role", function () {

        beforeAll(function () {
            isWithDegreePlan = true;
            catalogResultViewModelInstance = new catalogResultViewModel(Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, isWithDegreePlan, true, Ellucian.Planning.planRepository);
            ko.mapping.fromJS(data, courseMapping, catalogResultViewModelInstance);
        });

        it("validate object is of type catalogResultViewModelWithDegreePlanForAdvisors", function () {
            expect(catalogResultViewModelInstance instanceof catalogResultViewModelWithDegreePlanForAdvisors).toBe(true);
            expect(catalogResultViewModelInstance instanceof catalogResultViewModelWithoutDegreePlan).toBe(false);
            expect(catalogResultViewModelInstance instanceof catalogResultViewModelWithDegreePlan).toBe(false);
            expect(catalogResultViewModelInstance.hasOwnProperty("advisingKeywordSearchHandler")).toBe(true);
            expect(catalogResultViewModelInstance.hasOwnProperty("addSectionButtonLabel")).toBe(true);


        });
    });

    describe("factory creates result view model for student with planning", function () {

        beforeAll(function () {
            isWithDegreePlan = true;
            catalogResultViewModelInstance = new catalogResultViewModel(Ellucian.Course.ActionUrls.searchCourseCatalogAsyncActionUrl, isWithDegreePlan, false, Ellucian.Planning.planRepository);
            ko.mapping.fromJS(data, courseMapping, catalogResultViewModelInstance);
        });

        it("validate object is of type catalogResultViewModelWithDegreePlan", function () {
            expect(catalogResultViewModelInstance instanceof catalogResultViewModelWithDegreePlan).toBe(true);
            expect(catalogResultViewModelInstance instanceof catalogResultViewModelWithoutDegreePlan).toBe(false);
            expect(catalogResultViewModelInstance instanceof catalogResultViewModelWithDegreePlanForAdvisors).toBe(false);
            expect(catalogResultViewModelInstance.hasOwnProperty("advisingKeywordSearchHandler")).toBe(false);
            expect(catalogResultViewModelInstance.hasOwnProperty("addSectionButtonLabel")).toBe(true);


        });
    });
});

