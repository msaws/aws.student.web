﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var sectionId = "123";
var facultyGradingNavigationInstance = new facultyGradingNavigationModel();
var data = {
    "CanEditGrades": true,
    "CanVerifyGrades": true,
    "IsTermOpen": true,
    "SectionId": "19534",
    "SectionStartDate": "02/03/2015",
    "SectionEndDate": null,
    "GradeTypes": [
         { Item1: ko.observable("A"), Item2: ko.observable("") },
         { Item1: ko.observable("B"), Item2: ko.observable("") },
         { Item1: ko.observable("D"), Item2: ko.observable("39") },
    ],
    "MobileGradeViews": [
        { "Item1": "O", "Item2": "Grade View" },
        { "Item1": "F", "Item2": "Final Grade" },
        { "Item1": "M1", "Item2": "Midterm Grade 1" }
    ],
    "StudentGrades": [
        { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Able, George", "FinalGrade": "", "GradeExpireDate": "", "IsGradeVerified": false, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012623", "VerifyGradeResponseErrors": "", "Midterm1Grade": "A", "Midterm2Grade": null, "Midterm3Grade": "", "Midterm4Grade": "B", "Midterm5Grade": "C", "Midterm6Grade": "WF" },
        { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Cruz, ted", "FinalGrade": "A", "GradeExpireDate": "", "IsGradeVerified": false, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012882", "VerifyGradeResponseErrors": "", "Midterm1Grade": null, "Midterm2Grade": "A", "Midterm3Grade": "B", "Midterm4Grade": "", "Midterm5Grade": "WP", "Midterm6Grade": "C" },
        { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Sunflower, Sunny", "FinalGrade": "A", "GradeExpireDate": "", "IsGradeVerified": true, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012559", "VerifyGradeResponseErrors": "", "Midterm1Grade": "F", "Midterm2Grade": "A", "Midterm3Grade": "E", "Midterm4Grade": "P", "Midterm5Grade": "", "Midterm6Grade": "D" }
    ]
};

describe("faculty.grading.view.model.js Tests: ", function () {

    describe("facultyGradingNavigationModel: ", function () {

        beforeEach(function () {
            jasmine.clock().install();
            viewModel = new facultyGradingNavigationModel();
        });

        afterEach(function () {
            jasmine.clock().uninstall();
        })

        it("calls BaseViewModel on initialization with a mobile breakpoint of 769", function () {
            spyOn(BaseViewModel, "call").and.callThrough();

            viewModel = new facultyGradingNavigationModel();

            expect(BaseViewModel.call).toHaveBeenCalledWith(viewModel, 769);
        });

        it("changeToDesktop does nothing when called", function () {
            spyOn(viewModel, 'retrieveStudentGrades');

            viewModel.changeToDesktop();

            expect(viewModel.retrieveStudentGrades).not.toHaveBeenCalled();
        });

        it("changeToMobile does nothing when called", function () {
            spyOn(viewModel, 'retrieveStudentGrades');

            viewModel.changeToMobile();

            expect(viewModel.retrieveStudentGrades).not.toHaveBeenCalled();
        });

        it("areGradesRetrieved is false on initialization", function () {
            expect(viewModel.areGradesRetrieved()).toBeFalsy();
        });

        it("SectionStartDate is undefined on initialization", function () {
            expect(viewModel.SectionStartDate()).toEqual(undefined);
        });

        it("SectionEndDate is undefined on initialization", function () {
            expect(viewModel.SectionEndDate()).toEqual(undefined);
        });

        it("displayGradeOverview is true on initialization", function () {
            expect(viewModel.displayGradeOverview()).toBeTruthy();
        });

        it("displayFinalGrades is false on initialization", function () {
            expect(viewModel.displayFinalGrades()).toBeFalsy();
        });

        it("displayMidterm1Grades is false on initialization", function () {
            expect(viewModel.displayMidterm1Grades()).toBeFalsy();
        });

        it("displayMidterm2Grades is false on initialization", function () {
            expect(viewModel.displayMidterm2Grades()).toBeFalsy();
        });

        it("displayMidterm3Grades is false on initialization", function () {
            expect(viewModel.displayMidterm3Grades()).toBeFalsy();
        });

        it("displayMidterm4Grades is false on initialization", function () {
            expect(viewModel.displayMidterm4Grades()).toBeFalsy();
        });

        it("displayMidterm5Grades is false on initialization", function () {
            expect(viewModel.displayMidterm5Grades()).toBeFalsy();
        });

        it("displayMidterm6Grades is false on initialization", function () {
            expect(viewModel.displayMidterm6Grades()).toBeFalsy();
        });

        describe("showMidtermGrades: ", function () {

            it("false when displayMidterm1Grades - displayMidterm6Grades are all false", function () {
                expect(viewModel.showMidtermGrades()).toBeFalsy();
            });

            it("true when displayMidterm1Grades is true", function () {
                viewModel.displayMidterm1Grades(true);

                expect(viewModel.showMidtermGrades()).toBeTruthy();
            });

            it("true when displayMidterm2Grades is true", function () {
                viewModel.displayMidterm2Grades(true);

                expect(viewModel.showMidtermGrades()).toBeTruthy();
            });

            it("true when displayMidterm3Grades is true", function () {
                viewModel.displayMidterm3Grades(true);

                expect(viewModel.showMidtermGrades()).toBeTruthy();
            });

            it("true when displayMidterm4Grades is true", function () {
                viewModel.displayMidterm4Grades(true);

                expect(viewModel.showMidtermGrades()).toBeTruthy();
            });

            it("true when displayMidterm5Grades is true", function () {
                viewModel.displayMidterm5Grades(true);

                expect(viewModel.showMidtermGrades()).toBeTruthy();
            });

            it("true when displayMidterm6Grades is true", function () {
                viewModel.displayMidterm6Grades(true);

                expect(viewModel.showMidtermGrades()).toBeTruthy();
            });

        });

        it("IsTermOpen is false on initialization", function () {
            expect(viewModel.IsTermOpen()).toBeFalsy();
        });

        it("NumberOfMidtermGradesToShow is 0 on initialization", function () {
            expect(viewModel.NumberOfMidtermGradesToShow()).toEqual(0);
        });

        it("isGradeEntryPerformed is false on initialization", function () {
            expect(viewModel.isGradeEntryPerformed()).toBeFalsy();
        });

        it("isWaitingForOverview is false on initialization", function () {
            expect(viewModel.isWaitingForOverview()).toBeFalsy();
        });

        it("MobileGradeViews is an empty array on initialization", function () {
            expect(viewModel.MobileGradeViews()).toEqual([]);
        });

        it("mobileGradeViewSelected is 'O' on initialization", function () {
            expect(viewModel.mobileGradeViewSelected()).toEqual("O");
        });

        describe("mobileGradeViewSelected subscription: ", function () {

            it("gradeOverviewTabSelected is called when new value is 'O'", function () {
                spyOn(viewModel, 'gradeOverviewTabSelected');

                // Because mobileGradeViewSelected initializes to "O",
                // we need to change its value and then change it back to "O"
                // to trigger the correct subscription condition
                viewModel.mobileGradeViewSelected("F");
                viewModel.mobileGradeViewSelected("O");

                expect(viewModel.gradeOverviewTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called when new value is 'F'", function () {
                spyOn(viewModel, 'finalGradesTabSelected');

                viewModel.mobileGradeViewSelected("F");

                expect(viewModel.finalGradesTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called when new value is 'M1'", function () {
                spyOn(viewModel, 'midterm1GradesTabSelected');

                viewModel.mobileGradeViewSelected("M1");

                expect(viewModel.midterm1GradesTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called when new value is 'M2'", function () {
                spyOn(viewModel, 'midterm2GradesTabSelected');

                viewModel.mobileGradeViewSelected("M2");

                expect(viewModel.midterm2GradesTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called when new value is 'M3'", function () {
                spyOn(viewModel, 'midterm3GradesTabSelected');

                viewModel.mobileGradeViewSelected("M3");

                expect(viewModel.midterm3GradesTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called when new value is 'M4'", function () {
                spyOn(viewModel, 'midterm4GradesTabSelected');

                viewModel.mobileGradeViewSelected("M4");

                expect(viewModel.midterm4GradesTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called when new value is 'M5'", function () {
                spyOn(viewModel, 'midterm5GradesTabSelected');

                viewModel.mobileGradeViewSelected("M5");

                expect(viewModel.midterm5GradesTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called when new value is 'M6'", function () {
                spyOn(viewModel, 'midterm6GradesTabSelected');

                viewModel.mobileGradeViewSelected("M6");

                expect(viewModel.midterm6GradesTabSelected).toHaveBeenCalled();
            });

            it("gradeOverviewTabSelected is called new value is unhandled", function () {
                spyOn(viewModel, 'gradeOverviewTabSelected');

                viewModel.mobileGradeViewSelected("X");

                expect(viewModel.gradeOverviewTabSelected).toHaveBeenCalled();
            });

        });

        describe("gradeOverviewTabSelected: ", function () {

            // Validates view model properties that are set regardless of
            // isGradeEntryPerformed value
            function validateViewModelProperties() {
                expect(viewModel.displayGradeOverview()).toBeTruthy();
                expect(viewModel.isGradeEntryPerformed()).toBeFalsy();
                expect(viewModel.displayFinalGrades()).toBeFalsy();
                expect(viewModel.displayMidterm1Grades()).toBeFalsy();
                expect(viewModel.displayMidterm2Grades()).toBeFalsy();
                expect(viewModel.displayMidterm3Grades()).toBeFalsy();
                expect(viewModel.displayMidterm4Grades()).toBeFalsy();
                expect(viewModel.displayMidterm5Grades()).toBeFalsy();
                expect(viewModel.displayMidterm6Grades()).toBeFalsy();
            }

            it("updates view model properties and calls retrieveStudentGrades when isGradeEntryPerformed is true", function () {
                spyOn(viewModel, 'retrieveStudentGrades');

                viewModel.isGradeEntryPerformed(true);
                viewModel.gradeOverviewTabSelected();

                expect(viewModel.retrieveStudentGrades).toHaveBeenCalled();
                validateViewModelProperties();
            });

            it("updates view model properties and does not call retrieveStudentGrades when isGradeEntryPerformed is false", function () {
                spyOn(viewModel, 'retrieveStudentGrades');

                viewModel.isGradeEntryPerformed(false);
                viewModel.gradeOverviewTabSelected();

                expect(viewModel.retrieveStudentGrades).not.toHaveBeenCalled();
                validateViewModelProperties();
            });
        })

        it("finalGradesTabSelected updates view model properties when called", function () {
            viewModel.finalGradesTabSelected();

            expect(viewModel.displayGradeOverview()).toBeFalsy();
            expect(viewModel.displayFinalGrades()).toBeTruthy();
            expect(viewModel.displayMidterm1Grades()).toBeFalsy();
            expect(viewModel.displayMidterm2Grades()).toBeFalsy();
            expect(viewModel.displayMidterm3Grades()).toBeFalsy();
            expect(viewModel.displayMidterm4Grades()).toBeFalsy();
            expect(viewModel.displayMidterm5Grades()).toBeFalsy();
            expect(viewModel.displayMidterm6Grades()).toBeFalsy();
        });

        it("midterm1GradesTabSelected updates view model properties when called", function () {
            viewModel.midterm1GradesTabSelected();

            expect(viewModel.displayGradeOverview()).toBeFalsy();
            expect(viewModel.displayFinalGrades()).toBeFalsy();
            expect(viewModel.displayMidterm1Grades()).toBeTruthy();
            expect(viewModel.displayMidterm2Grades()).toBeFalsy();
            expect(viewModel.displayMidterm3Grades()).toBeFalsy();
            expect(viewModel.displayMidterm4Grades()).toBeFalsy();
            expect(viewModel.displayMidterm5Grades()).toBeFalsy();
            expect(viewModel.displayMidterm6Grades()).toBeFalsy();
        });

        it("midterm2GradesTabSelected updates view model properties when called", function () {
            viewModel.midterm2GradesTabSelected();

            expect(viewModel.displayGradeOverview()).toBeFalsy();
            expect(viewModel.displayFinalGrades()).toBeFalsy();
            expect(viewModel.displayMidterm1Grades()).toBeFalsy();
            expect(viewModel.displayMidterm2Grades()).toBeTruthy();
            expect(viewModel.displayMidterm3Grades()).toBeFalsy();
            expect(viewModel.displayMidterm4Grades()).toBeFalsy();
            expect(viewModel.displayMidterm5Grades()).toBeFalsy();
            expect(viewModel.displayMidterm6Grades()).toBeFalsy();
        });

        it("midterm3GradesTabSelected updates view model properties when called", function () {
            viewModel.midterm3GradesTabSelected();

            expect(viewModel.displayGradeOverview()).toBeFalsy();
            expect(viewModel.displayFinalGrades()).toBeFalsy();
            expect(viewModel.displayMidterm1Grades()).toBeFalsy();
            expect(viewModel.displayMidterm2Grades()).toBeFalsy();
            expect(viewModel.displayMidterm3Grades()).toBeTruthy();
            expect(viewModel.displayMidterm4Grades()).toBeFalsy();
            expect(viewModel.displayMidterm5Grades()).toBeFalsy();
            expect(viewModel.displayMidterm6Grades()).toBeFalsy();
        });

        it("midterm4GradesTabSelected updates view model properties when called", function () {
            viewModel.midterm4GradesTabSelected();

            expect(viewModel.displayGradeOverview()).toBeFalsy();
            expect(viewModel.displayFinalGrades()).toBeFalsy();
            expect(viewModel.displayMidterm1Grades()).toBeFalsy();
            expect(viewModel.displayMidterm2Grades()).toBeFalsy();
            expect(viewModel.displayMidterm3Grades()).toBeFalsy();
            expect(viewModel.displayMidterm4Grades()).toBeTruthy();
            expect(viewModel.displayMidterm5Grades()).toBeFalsy();
            expect(viewModel.displayMidterm6Grades()).toBeFalsy();
        });

        it("midterm5GradesTabSelected updates view model properties when called", function () {
            viewModel.midterm5GradesTabSelected();

            expect(viewModel.displayGradeOverview()).toBeFalsy();
            expect(viewModel.displayFinalGrades()).toBeFalsy();
            expect(viewModel.displayMidterm1Grades()).toBeFalsy();
            expect(viewModel.displayMidterm2Grades()).toBeFalsy();
            expect(viewModel.displayMidterm3Grades()).toBeFalsy();
            expect(viewModel.displayMidterm4Grades()).toBeFalsy();
            expect(viewModel.displayMidterm5Grades()).toBeTruthy();
            expect(viewModel.displayMidterm6Grades()).toBeFalsy();
        });

        it("midterm6GradesTabSelected updates view model properties when called", function () {
            viewModel.midterm6GradesTabSelected();

            expect(viewModel.displayGradeOverview()).toBeFalsy();
            expect(viewModel.displayFinalGrades()).toBeFalsy();
            expect(viewModel.displayMidterm1Grades()).toBeFalsy();
            expect(viewModel.displayMidterm2Grades()).toBeFalsy();
            expect(viewModel.displayMidterm3Grades()).toBeFalsy();
            expect(viewModel.displayMidterm4Grades()).toBeFalsy();
            expect(viewModel.displayMidterm5Grades()).toBeFalsy();
            expect(viewModel.displayMidterm6Grades()).toBeTruthy();
        });

        it("finalGradeMapping 'create' function for StudentGrades", function () {
            viewModel.SectionStartDate(data.SectionStartDate);
            viewModel.SectionEndDate(data.SectionEndDate);
            viewModel.finalGradingModelInstance.GradeTypes().push(data.GradeTypes[0]);
            viewModel.finalGradingModelInstance.GradeTypes().push(data.GradeTypes[1]);
            viewModel.finalGradingModelInstance.GradeTypes().push(data.GradeTypes[2]);

            ko.mapping.fromJS(data, viewModel.finalGradeMapping);
            
            expect(viewModel.finalGradingModelInstance.studentGradesHolder.length).toEqual(data.StudentGrades.length);
        });

        it("midtermGradeMapping 'create' function for StudentGrades adds grades to midtermGradingModelInstance", function () {
            ko.mapping.fromJS(data, viewModel.midtermGradeMapping);

            expect(viewModel.midtermGradingModelInstance.StudentGrades().length).toEqual(data.StudentGrades.length);
        });

        it("overviewGradingModelInstance is declared as overviewGradingModel on initialization", function () {
            expect(viewModel.overviewGradingModelInstance instanceof overviewGradingModel).toBeTruthy();
        });

        it("finalGradingModelInstance is declared as finalGradingModel on initialization", function () {
            expect(viewModel.finalGradingModelInstance instanceof finalGradingModel).toBeTruthy();
        });

        it("midtermGradingModelInstance is declared as midtermGradingModel on initialization", function () {
            expect(viewModel.midtermGradingModelInstance instanceof midtermGradingModel).toBeTruthy();
        });

        it("verifyGradingModelInstance is declared as verifyGradeModel and calls verifyGradeModel with itself and its finalGradingModelInstance on initialization", function () {
            spyOn(window, 'verifyGradeModel').and.callThrough();

            viewModel = new facultyGradingNavigationModel();

            expect(viewModel.verifyGradingModelInstance instanceof verifyGradeModel).toBeTruthy();
            expect(window.verifyGradeModel).toHaveBeenCalledWith(viewModel, viewModel.finalGradingModelInstance);
        });

        it("notificationInstance calls notificationModel on initialization", function () {
            spyOn(window, 'notificationModel');

            viewModel = new facultyGradingNavigationModel();

            expect(window.notificationModel).toHaveBeenCalled();
        });

        describe("retrieveStudentGrades: ", function () {

            function validateViewModelProperties() {
                expect(viewModel.checkForMobile).toHaveBeenCalledWith(window, document);
                expect($.fn.makeTableResponsive).toHaveBeenCalled();
                expect(viewModel.isWaitingForOverview()).toBeFalsy();
            }

            it("success", function () {
                var data = { StudentGrades: [] };

                spyOn(viewModel, "checkForMobile").and.callThrough();
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend({});
                    e.success(data);
                    e.error({});
                    e.complete({});
                });
                spyOn(ko.mapping, "fromJS").and.callThrough();
                spyOn($.fn, "makeTableResponsive");

                // Surround function call with jasmine.clock calls to handle the 
                // setTimeout call in the AJAX call 'complete' function
                viewModel.retrieveStudentGrades();
                jasmine.clock().tick(200);

                expect(ko.mapping.fromJS).toHaveBeenCalledWith(data, {}, viewModel.overviewGradingModelInstance);
                validateViewModelProperties();
            });

            it("error", function () {
                spyOn(viewModel, "checkForMobile").and.callThrough();
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend({});
                    e.success({});
                    e.error({ status: 400 }, "", "");
                    e.complete({});
                });
                spyOn($.fn, "notificationCenter");
                spyOn($.fn, "makeTableResponsive");

                // Surround function call with jasmine.clock calls to handle the 
                // setTimeout call in the AJAX call 'complete' function
                viewModel.retrieveStudentGrades();
                jasmine.clock().tick(200);

                expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: stringUnableToRetrieveSection, type: "error" });
                validateViewModelProperties();
            });

        });

    });

    describe("facultyGradingNavigationModel.prototype.fromJS: ", function () {

        it("calls ko.mapping.fromJS and sets areGradesRetrieved to true", function () {
            var data = {
                "CanEditGrades": true,
                "CanVerifyGrades": true,
                "IsTermOpen": true,
                "SectionId": "19534",
                "SectionStartDate": "02/03/2015",
                "SectionEndDate": null,
                "GradeTypes": [
                    { "Item1": "A", "Item2": "" },
                     { "Item1": "B", "Item2": "" },
                     { "Item1": "D", "Item2": "39" },
                ],
                "MobileGradeViews": [
                    { "Item1": "O", "Item2": "Grade View" },
                    { "Item1": "F", "Item2": "Final Grade" },
                    { "Item1": "M1", "Item2": "Midterm Grade 1" }
                ],
                "StudentGrades": [
                    { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Able, George", "FinalGrade": "", "GradeExpireDate": "", "IsGradeVerified": false, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012623", "VerifyGradeResponseErrors": "", "Midterm1Grade": "A", "Midterm2Grade": null, "Midterm3Grade": "", "Midterm4Grade": "B", "Midterm5Grade": "C", "Midterm6Grade": "WF" },
                    { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Cruz, ted", "FinalGrade": "A", "GradeExpireDate": "", "IsGradeVerified": false, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012882", "VerifyGradeResponseErrors": "", "Midterm1Grade": null, "Midterm2Grade": "A", "Midterm3Grade": "B", "Midterm4Grade": "", "Midterm5Grade": "WP", "Midterm6Grade": "C" },
                    { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Sunflower, Sunny", "FinalGrade": "A", "GradeExpireDate": "", "IsGradeVerified": true, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012559", "VerifyGradeResponseErrors": "", "Midterm1Grade": "F", "Midterm2Grade": "A", "Midterm3Grade": "E", "Midterm4Grade": "P", "Midterm5Grade": "", "Midterm6Grade": "D" }
                ]
            };
            spyOn(ko.mapping, 'fromJS');

            viewModel = new facultyGradingNavigationModel();
            viewModel.fromJS(data);

            expect(ko.mapping.fromJS.calls.count()).toEqual(6);
            expect(viewModel.areGradesRetrieved()).toBeTruthy();
        })

    });

    describe("facultyGradingNavigationModel.prototype.registerEventHandlers: ", function () {

        it("prototype.registerEventHandlers calls jQuery on 'blur' and sets KO context", function () {
            spyOn($.fn, 'on').and.callThrough();

            viewModel = new facultyGradingNavigationModel();
            viewModel.registerEventHandlers();

            expect($.fn.on).toHaveBeenCalledWith('blur', jasmine.any(Function));
        });

    });

    describe("overviewGradingModel: ", function () {

        beforeEach(function () {
            viewModel = new overviewGradingModel();
        });

        it("StudentGrades is an empty array on initialization", function () {
            expect(viewModel.StudentGrades()).toEqual([]);
        });

        describe("hasStudentGrades: ", function () {

            it("true when StudentGrades has at least one array member", function () {
                viewModel.StudentGrades.push({ "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Able, George", "FinalGrade": "", "GradeExpireDate": "", "IsGradeVerified": false, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012623", "VerifyGradeResponseErrors": "", "Midterm1Grade": "A", "Midterm2Grade": null, "Midterm3Grade": "", "Midterm4Grade": "B", "Midterm5Grade": "C", "Midterm6Grade": "WF" });

                expect(viewModel.hasStudentGrades()).toBeTruthy();
            });

            it("false when StudentGrades has no array members", function () {
                viewModel.StudentGrades.removeAll();

                expect(viewModel.hasStudentGrades()).toBeFalsy();
            });

        });

        it("CanVerifyGrades is false on initialization", function () {
            expect(viewModel.CanVerifyGrades()).toBeFalsy();
        });

        it("IsTermOpen is false on initialization", function () {
            expect(viewModel.IsTermOpen()).toBeFalsy();
        });

        it("NumberOfMidtermGradesToShow is 0 on initialization", function () {
            expect(viewModel.NumberOfMidtermGradesToShow()).toEqual(0);
        });

        it("InfoMessageToDisplay is '' on initialization", function () {
            expect(viewModel.InfoMessageToDisplay()).toEqual("");
        });

        describe("remainingGradesToBeProcessed: ", function () {

            describe("IsTermOpen is false: ", function () {

                beforeEach(function () {
                    viewModel.IsTermOpen(false);
                });

                it("equals 0 and sets InfoMessageToDisplay to '' when IsTermOpen is false", function () {
                    expect(viewModel.remainingGradesToBeProcessed()).toBe(0);
                    expect(viewModel.InfoMessageToDisplay()).toEqual("");
                });

            });

            describe("IsTermOpen is true: ", function () {

                beforeEach(function () {
                    viewModel.IsTermOpen(true);
                });

                describe("CanVerifyGrades is true: ", function () {

                    beforeEach(function () {
                        viewModel.StudentGrades.removeAll();
                        viewModel.CanVerifyGrades(true);
                    });

                    it("is 0 and sets InfoMessageToDisplay to '' when there are no StudentGrades", function () {
                        expect(viewModel.remainingGradesToBeProcessed()).toBe(0);
                        expect(viewModel.InfoMessageToDisplay()).toEqual("");
                    });

                    it("is 0 and sets InfoMessageToDisplay to '' when all StudentGrades with IsGradeVerified = true", function () {

                        viewModel.StudentGrades.push({
                            IsGradeVerified: ko.observable(true)
                        });

                        expect(viewModel.remainingGradesToBeProcessed()).toBe(0);
                        expect(viewModel.InfoMessageToDisplay()).toEqual("");

                    });

                    it("equals number of non-verified grades and sets InfoMessageToDisplay to VerifyGradeInformationMessage when all StudentGrades with IsGradeVerified = false", function () {

                        viewModel.StudentGrades.push({
                            IsGradeVerified: ko.observable(false)
                        });

                        expect(viewModel.remainingGradesToBeProcessed()).toBe(1);
                        expect(viewModel.InfoMessageToDisplay()).toEqual(VerifyGradeInformationMessage);

                    });

                    it("equals number of non-verified grades and sets InfoMessageToDisplay to VerifyGradeInformationMessage when some, but not all StudentGrades with IsGradeVerified = true", function () {

                        viewModel.StudentGrades.push({
                            IsGradeVerified: ko.observable(true)
                        });

                        viewModel.StudentGrades.push({
                            IsGradeVerified: ko.observable(false)
                        });

                        expect(viewModel.remainingGradesToBeProcessed()).toBe(1);
                        expect(viewModel.InfoMessageToDisplay()).toEqual(VerifyGradeInformationMessage);

                    });

                });

                describe("CanVerifyGrades is false: ", function () {

                    beforeEach(function () {
                        viewModel.StudentGrades.removeAll();
                        viewModel.CanVerifyGrades(false);
                    });

                    it("equals 0 when there are no StudentGrades", function () {
                        expect(viewModel.remainingGradesToBeProcessed()).toBe(0);
                        expect(viewModel.InfoMessageToDisplay()).toEqual("");
                    });

                    it("equals 0 when all StudentGrades with FinalGrade set", function () {

                        viewModel.StudentGrades.push({
                            FinalGrade: ko.observable("A")
                        });

                        expect(viewModel.remainingGradesToBeProcessed()).toBe(0);
                        expect(viewModel.InfoMessageToDisplay()).toEqual("");

                    });

                    it("equals 1 when all StudentGrades with FinalGrade null/empty", function () {

                        viewModel.StudentGrades.push({
                            FinalGrade: ko.observable(null)
                        });

                        expect(viewModel.remainingGradesToBeProcessed()).toBe(1);
                        expect(viewModel.InfoMessageToDisplay()).toEqual(FinalGradeInformationMessage);

                    });


                    it("equals number of non-verified grades and sets InfoMessageToDisplay to FinalGradeInformationMessage when some, but not all StudentGrades with IsGradeVerified = true", function () {

                        viewModel.StudentGrades.push({
                            FinalGrade: ko.observable(null)
                        });

                        viewModel.StudentGrades.push({
                            FinalGrade: ko.observable("A")
                        });

                        expect(viewModel.remainingGradesToBeProcessed()).toBe(1);
                        expect(viewModel.InfoMessageToDisplay()).toEqual(FinalGradeInformationMessage);

                    });

                });

            });

        });

    });

    describe("baseStudentGradeModel: ", function () {

        beforeEach(function () {
            jasmine.clock().install();
            viewModel = new baseStudentGradeModel();
            viewModel.StudentId = ko.observable("0001234");
            viewModel.gradeEntryErrors = ko.observableArray([]);

        });

        afterEach(function () {
            jasmine.clock().uninstall();
        });

        it("isFieldDirty is false on initialization", function () {
            expect(viewModel.isFieldDirty()).toBeFalsy();
        });

        it("isSuccessfullyUpdated is null on initialization", function () {
            expect(viewModel.isSuccessfullyUpdated()).toEqual(null);
        });

        it("isWaiting is false on initialization", function () {
            expect(viewModel.isWaiting()).toBeFalsy();
        });

        it("gradeEntryErrors is an empty array on initialization", function () {
            expect(viewModel.gradeEntryErrors()).toEqual([]);
        });

        describe("isSuccessfullyUpdated subscription: ", function () {

            it("sets isGradeEntryPerformed to true and updates notification center when new value is true", function () {
                spyOn(facultyGradingNavigationInstance.notificationInstance, 'updateNotificationCenter').and.callThrough();

                viewModel.isSuccessfullyUpdated(true);

                expect(facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter).toHaveBeenCalledWith(viewModel.StudentId(), null);
                expect(facultyGradingNavigationInstance.isGradeEntryPerformed()).toBeTruthy();
            });

            it("updates notification center when new value is false", function () {
                spyOn(facultyGradingNavigationInstance.notificationInstance, 'updateNotificationCenter').and.callThrough();

                viewModel.isSuccessfullyUpdated(false);

                expect(facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter).toHaveBeenCalledWith(viewModel.StudentId(), viewModel.gradeEntryErrors());
            });

        });

        describe("postStudentGrade: ", function () {

            function validatePropertiesOnAjaxComplete() {
                expect(model.isWaiting()).toBeFalsy();
                expect(model.isFieldDirty()).toBeFalsy();
            }

            describe("success: ", function () {

                describe("data.gradeEntryErrors is null or empty: ", function () {

                    it("gradeEntryErrors is null", function () {
                        spyOn($, "ajax").and.callFake(function (e) {
                            e.beforeSend({});
                            e.success({
                                gradeEntryErrors: null
                            }, "", "");
                            e.complete({});
                        });

                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.gradeEntryErrors().length).toEqual(0);
                        expect(model.isSuccessfullyUpdated()).toBeTruthy();
                        validatePropertiesOnAjaxComplete();
                    });

                    it("gradeEntryErrors is empty", function () {
                        spyOn($, "ajax").and.callFake(function (e) {
                            e.beforeSend({});
                            e.success({
                                gradeEntryErrors: []
                            }, "", "");
                            e.complete({});
                        });

                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.gradeEntryErrors().length).toEqual(0);
                        expect(model.isSuccessfullyUpdated()).toBeTruthy();
                        validatePropertiesOnAjaxComplete();
                    });

                });

                describe("data.gradeEntryErrors is not null or empty: ", function () {

                    it("model does not have FinalGrade or IsGradeVerified properties", function () {
                        spyOn($, "ajax").and.callFake(function (e) {
                            e.beforeSend({});
                            e.success({
                                gradeEntryErrors: [FinalGradeSaveFailMessage]
                            }, "", "");
                            e.complete({});
                        });

                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.gradeEntryErrors().length).toEqual(1);
                        expect(model.isSuccessfullyUpdated()).toBeFalsy();
                        validatePropertiesOnAjaxComplete();
                    });

                    it("model has FinalGrade property but not IsGradeVerified property", function () {
                        spyOn($, "ajax").and.callFake(function (e) {
                            e.beforeSend({});
                            e.success({
                                gradeEntryErrors: [FinalGradeSaveFailMessage]
                            }, "", "");
                            e.complete({});
                        });

                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.FinalGrade = ko.observable("A");
                        model.finalGradeSelected = ko.observable();
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.gradeEntryErrors().length).toEqual(1);
                        expect(model.isSuccessfullyUpdated()).toBeFalsy();
                        expect(model.finalGradeSelected()).toEqual(undefined);
                        validatePropertiesOnAjaxComplete();
                    });

                    it("model has IsGradeVerified property set to true", function () {
                        spyOn($, "ajax").and.callFake(function (e) {
                            e.beforeSend({});
                            e.success({
                                gradeEntryErrors: [FinalGradeSaveFailMessage]
                            }, "", "");
                            e.complete({});
                        });

                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.FinalGrade = ko.observable("A");
                        model.IsGradeVerified = ko.observable(true);
                        model.finalGradeSelected = ko.observable(true);
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.gradeEntryErrors().length).toEqual(1);
                        expect(model.isSuccessfullyUpdated()).toBeFalsy();
                        expect(model.finalGradeSelected()).toBeTruthy();
                        validatePropertiesOnAjaxComplete();
                    });

                    it("model has IsGradeVerified property set to null", function () {
                        spyOn($, "ajax").and.callFake(function (e) {
                            e.beforeSend({});
                            e.success({
                                gradeEntryErrors: [FinalGradeSaveFailMessage]
                            }, "", "");
                            e.complete({});
                        });

                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.FinalGrade = ko.observable("A");
                        model.IsGradeVerified = ko.observable(null);
                        model.finalGradeSelected = ko.observable(true);
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.gradeEntryErrors().length).toEqual(1);
                        expect(model.isSuccessfullyUpdated()).toBeFalsy();
                        expect(model.finalGradeSelected()).toEqual(null);
                        validatePropertiesOnAjaxComplete();
                    });

                    it("model has IsGradeVerified property set to false", function () {
                        spyOn($, "ajax").and.callFake(function (e) {
                            e.beforeSend({});
                            e.success({
                                gradeEntryErrors: [FinalGradeSaveFailMessage]
                            }, "", "");
                            e.complete({});
                        });

                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.FinalGrade = ko.observable("A");
                        model.IsGradeVerified = ko.observable(false);
                        model.finalGradeSelected = ko.observable(true);
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.gradeEntryErrors().length).toEqual(1);
                        expect(model.isSuccessfullyUpdated()).toBeFalsy();
                        expect(model.finalGradeSelected()).toEqual(null);
                        validatePropertiesOnAjaxComplete();
                    });

                });

            });

            describe("error: ", function () {

                function validatePropertiesOnAjaxError() {
                    expect(model.gradeEntryErrors().length).toEqual(1);
                    expect(model.gradeEntryErrors()[0]).toEqual(FinalGradeSaveFailMessage);
                    expect(model.isSuccessfullyUpdated()).toBeFalsy();
                }

                beforeEach(function () {
                    spyOn($, "ajax").and.callFake(function (e) {
                        e.beforeSend({});
                        e.error({ status: 400 }, "", "");
                        e.complete({});
                    });
                });

                describe("viewModel has 'FinalGrade' and 'IsGradeVerified' properties", function () {

                    it("IsGradeVerified is null", function () {
                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.FinalGrade = ko.observable();
                        model.IsGradeVerified = ko.observable(null);
                        model.finalGradeSelected = ko.observable();
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.finalGradeSelected()).toEqual(null);
                        validatePropertiesOnAjaxError();
                        validatePropertiesOnAjaxComplete();
                    });

                    it("IsGradeVerified is false", function () {
                        model = new baseStudentGradeModel();
                        model.StudentId = ko.observable("0001234");
                        model.FinalGrade = ko.observable();
                        model.IsGradeVerified = ko.observable(false);
                        model.finalGradeSelected = ko.observable();
                        model.postStudentGrade();
                        jasmine.clock().tick(200);

                        expect(model.finalGradeSelected()).toEqual(null);
                        validatePropertiesOnAjaxError();
                        validatePropertiesOnAjaxComplete();
                    });


                });

                it("IsGradeVerified is undefined", function () {
                    model = new baseStudentGradeModel();
                    model.StudentId = ko.observable("0001234");
                    model.FinalGrade = ko.observable();
                    model.finalGradeSelected = ko.observable();
                    model.postStudentGrade();
                    jasmine.clock().tick(200);

                    validatePropertiesOnAjaxError();
                    validatePropertiesOnAjaxComplete();
                });

            });

        });

    });

});