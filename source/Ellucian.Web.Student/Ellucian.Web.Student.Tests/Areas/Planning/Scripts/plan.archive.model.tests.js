﻿describe("planArchiveViewModel", function () {
   
    var data, viewModel;
    beforeEach(function () {
        data = {
            Id: "1234"
        };
        viewModel = new planArchiveViewModel(data);
    });
    it(":isLoaded is false after initialization", function () {
        expect(viewModel.isLoaded()).toBeFalsy();
    });

    it(":archives.length is 0 after initilization", function () {
        expect(viewModel.archives().length).toBe(0);
    });

    it(":hasArchives is false after initilization", function () {
        expect(viewModel.hasArchives()).toBeFalsy();
    });
});
