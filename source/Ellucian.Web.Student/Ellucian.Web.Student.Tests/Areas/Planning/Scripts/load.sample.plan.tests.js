﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
forceEnableRegistrationButtons = false;
currentUserId = "1234";

describe("loadSamplePlanModel", function () {
    var viewModel;
    var degreePlanId="257";
     var studentPrograms={
   "StudentPrograms":[
    
    {
    
        "Code":"AGBU.AA",
        "Title":"Associate of Arts Agriculture Business",
        "Catalog":"2001",
        "Description":"",
        "Departments":
            [
                "Agriculture Business"
            ],
        "Majors":
            [
                "Agriculture Business 2 Yr Only"
            ],
        "Minors":[],
        "Ccds":[],
        "Specializations":[],
        "Degree":"Associate of Arts",
        "MinimumCredits":60,
        "MinimumInstitutionalCredits":30,
        "CompletedCredits":24,
        "CompletedInstitutionalCredits":24,
        "InProgressCredits":19,
        "InProgressInstitutionalCredits":19,
        "PlannedCredits":0,
        "PlannedInstitutionalCredits":0,
        "TotalCredits":43,
        "TotalInstitutionalCredits":43,
        "Requirements":
            [
            {
                "Id": "72919",
                "Code": "BB.AEVP.TEST",
                "Description": "Extra Course Handling - Aevp",
                "Status": null,
                "Gpa": "Current GPA: 0.875",
                "CompletionStatus": "Completed",
                "PlanningStatus": "CompletelyPlanned",
                "MinGpa": null,
                "MinSubrequirements": null,
                "Subrequirements":
                    [
                        {
                            "Id": "72920",
                            "Code": "AEVP Test",
                            "DisplayText": "Take 6 credits; from Subject ART ENGL;",
                            "MinGpa": null,
                            "MinGroups": null,
                            "Groups":
                                [
                                    {
                                        "CompletionStatus": "Completed",
                                        "PlanningStatus": "CompletelyPlanned",
                                        "AppliedAcademicCredits":
                                            [
                                                {
                                                    "Id": "42435",
                                                    "CourseId": "246",
                                                    "CourseName": "ART-103",
                                                    "Title": "Aqueous Media",
                                                    "Credit": 4,
                                                    "VerifiedGrade": "B",
                                                    "HasVerifiedGrade": true,
                                                    "Term": "2015/FA",
                                                    "IsNonCourse": false,
                                                    "IsWithdrawGrade": false,
                                                    "IsCompletedCredit": true,
                                                    "AdjustedGpaCredit": 0,
                                                    "OtherAttemptedButNotCompleted": false,
                                                    "IsGpaOnlyCredit": false,
                                                    "AllowedByOverride": false,
                                                    "IsExtraCourse": false,
                                                    "DisplayStatus": "Completed",
                                                    "ReplacedStatus": "NotReplaced",
                                                    "ReplacementStatus": "NotReplacement",
                                                    "ReplacementNotation": "",
                                                    "OverrideNotation": ""
                                                }
                                            ]
                                    }
                                ]
                        }
                    ]
            }
            ]
    },

       {

           "Code": "MATH.BA",
           "Title": "Bachelors of Arts in Mathematics",
           "Catalog": "2012",
           "Description": "BA degree in maths",
           "Departments":
               [
                   "Mathematics"
               ],
           "Majors":
               [
                   "Math"
               ],
           "Minors": [],
           "Ccds": [],
           "Specializations": [],
           "Degree": "Associate of Arts",
           "MinimumCredits": 60,
           "MinimumInstitutionalCredits": 30,
           "CompletedCredits": 24,
           "CompletedInstitutionalCredits": 24,
           "InProgressCredits": 19,
           "InProgressInstitutionalCredits": 19,
           "PlannedCredits": 0,
           "PlannedInstitutionalCredits": 0,
           "TotalCredits": 43,
           "TotalInstitutionalCredits": 43,
           "Requirements":
               [
               {
                   "Id": "72919",
                   "Code": "BB.AEVP.TEST",
                   "Description": "Extra Course Handling - Aevp",
                   "Status": null,
                   "Gpa": "Current GPA: 0.875",
                   "CompletionStatus": "Completed",
                   "PlanningStatus": "CompletelyPlanned",
                   "MinGpa": null,
                   "MinSubrequirements": null,
                   "Subrequirements":
                       [
                           {
                               "Id": "72920",
                               "Code": "AEVP Test",
                               "DisplayText": "Take 6 credits; from Subject ART ENGL;",
                               "MinGpa": null,
                               "MinGroups": null,
                               "Groups":
                                   [
                                       {
                                           "CompletionStatus": "Completed",
                                           "PlanningStatus": "CompletelyPlanned",
                                           "AppliedAcademicCredits":
                                               [
                                                   {
                                                       "Id": "42435",
                                                       "CourseId": "246",
                                                       "CourseName": "ART-103",
                                                       "Title": "Aqueous Media",
                                                       "Credit": 4,
                                                       "VerifiedGrade": "B",
                                                       "HasVerifiedGrade": true,
                                                       "Term": "2015/FA",
                                                       "IsNonCourse": false,
                                                       "IsWithdrawGrade": false,
                                                       "IsCompletedCredit": true,
                                                       "AdjustedGpaCredit": 0,
                                                       "OtherAttemptedButNotCompleted": false,
                                                       "IsGpaOnlyCredit": false,
                                                       "AllowedByOverride": false,
                                                       "IsExtraCourse": false,
                                                       "DisplayStatus": "Completed",
                                                       "ReplacedStatus": "NotReplaced",
                                                       "ReplacementStatus": "NotReplacement",
                                                       "ReplacementNotation": "",
                                                       "OverrideNotation": ""
                                                   }
                                               ]
                                       }
                                   ]
                           }
                       ]
               }
               ]
       },
       {

           "Code": "TRIAL.AA",
           "Title": "Associate of Arts Agriculture Business",
           "Catalog": "2001",
           "Description": "",
           "Departments":
               [
                   "Agriculture Business"
               ],
           "Majors":
               [
                   "Agriculture Business 2 Yr Only"
               ],
           "Minors": [],
           "Ccds": [],
           "Specializations": [],
           "Degree": "Associate of Arts",
           "MinimumCredits": 60,
           "MinimumInstitutionalCredits": 30,
           "CompletedCredits": 24,
           "CompletedInstitutionalCredits": 24,
           "InProgressCredits": 19,
           "InProgressInstitutionalCredits": 19,
           "PlannedCredits": 0,
           "PlannedInstitutionalCredits": 0,
           "TotalCredits": 43,
           "TotalInstitutionalCredits": 43,
           "Requirements":
               [
               {
                   "Id": "72919",
                   "Code": "BB.AEVP.TEST",
                   "Description": "Extra Course Handling - Aevp",
                   "Status": null,
                   "Gpa": "Current GPA: 0.875",
                   "CompletionStatus": "Completed",
                   "PlanningStatus": "CompletelyPlanned",
                   "MinGpa": null,
                   "MinSubrequirements": null,
                   "Subrequirements":
                       [
                           {
                               "Id": "72920",
                               "Code": "AEVP Test",
                               "DisplayText": "Take 6 credits; from Subject ART ENGL;",
                               "MinGpa": null,
                               "MinGroups": null,
                               "Groups":
                                   [
                                       {
                                           "CompletionStatus": "Completed",
                                           "PlanningStatus": "CompletelyPlanned",
                                           "AppliedAcademicCredits":
                                               [
                                                   {
                                                       "Id": "42435",
                                                       "CourseId": "246",
                                                       "CourseName": "ART-103",
                                                       "Title": "Aqueous Media",
                                                       "Credit": 4,
                                                       "VerifiedGrade": "B",
                                                       "HasVerifiedGrade": true,
                                                       "Term": "2015/FA",
                                                       "IsNonCourse": false,
                                                       "IsWithdrawGrade": false,
                                                       "IsCompletedCredit": true,
                                                       "AdjustedGpaCredit": 0,
                                                       "OtherAttemptedButNotCompleted": false,
                                                       "IsGpaOnlyCredit": false,
                                                       "AllowedByOverride": false,
                                                       "IsExtraCourse": false,
                                                       "DisplayStatus": "Completed",
                                                       "ReplacedStatus": "NotReplaced",
                                                       "ReplacementStatus": "NotReplacement",
                                                       "ReplacementNotation": "",
                                                       "OverrideNotation": ""
                                                   }
                                               ]
                                       }
                                   ]
                           }
                       ]
               }
               ]
       },
    ]
    
    };

     var planningTerms = {
    
        "planningTerms":[
        
            {
                "Code":"2017/SP",
                "Description":"2017 Spring Term",
                "StartDate":"2017-01-19T05:00:00.000Z",
                "EndDate":"2017-05-10T04:00:00.000Z",
                "ReportingYear":2016,
                "Sequence":10,
                "ReportingTerm":"2017RSP",
                "FinancialPeriod":1,
                "DefaultOnPlan":true,
                "IsActive":true,
                "RegistrationDates":
                    [
                        {
                            "RegistrationStartDate":null,
                            "RegistrationEndDate":null,
                            "PreRegistrationStartDate":null,
                            "PreRegistrationEndDate":null,
                            "AddStartDate":null,
                            "AddEndDate":null,
                            "DropStartDate":null,
                            "DropEndDate":null,
                            "DropGradeRequiredDate":null,
                            "Location":""
                        }
                    ],
                "FinancialAidYears":
                [],
                "SessionCycles":
                    ["\"LG,\"","S"],
                "YearlyCycles":
                    ["OY"]
            }
        ]
    };

     var allPrograms={
     
         "AllPrograms":[
         {
             "Code":"SSS.SHARE",
             "Title":"Additional Requirement Sharing Program",
             "Description":"",
             "Departments":["ENGL"],
             "Catalogs":["2012"],
             "Majors":["English"],
             "Minors":[],
             "Ccds":[],
             "Specializations":[],
             "Degree":"Bachelor of Arts",
             "AcademicLevelCode":"UG",
             "RelatedPrograms":[],
             "TranscriptGrouping":"UG",
             "UnofficialTranscriptGrouping":"",
             "OfficialTranscriptGrouping":"",
             "IsGraduationAllowed":true,
             "Locations": []
         
         },
        
     {
             "Code":"MATH.BA",
         "Title":"Bachelors in MATH",
         "Description":"",
         "Departments":["MATH"],
         "Catalogs":["2012"],
         "Majors":["MAthematics"],
         "Minors":[],
         "Ccds":[],
         "Specializations":[],
         "Degree":"Bachelor of Math",
         "AcademicLevelCode":"UG",
         "RelatedPrograms":[],
         "TranscriptGrouping":"UG",
         "UnofficialTranscriptGrouping":"",
         "OfficialTranscriptGrouping":"",
         "IsGraduationAllowed":true,
         "Locations": []
         
     }
         
         ]     
     };

   


    

    //verify initiailization
     describe("initialization", function () {
         beforeEach(function () {
             viewModel = new loadSamplePlanViewModel();
         });

        it(":studentPrograms and AllPrograms are observableArray", function () {
            expect(ko.isObservable(viewModel.StudentPrograms)).toBeTruthy();
            expect(Array.isArray(viewModel.StudentPrograms())).toBeTruthy();
            expect(ko.isObservable(viewModel.AllPrograms)).toBeTruthy();
            expect(Array.isArray(viewModel.AllPrograms())).toBeTruthy();
        });

        it("selectedProgram and selectedprogramCode are null", function () {
            expect(viewModel.SelectedProgram()).toBe(null);
            expect(viewModel.SelectedProgramCode()).toBeUndefined();
           

        });
        it("selectedprogramTitle is empty", function () {
            expect(viewModel.SelectedProgramTitle()).toBe("");

        });
        it("validate other observable values", function () {
            expect(viewModel.dialogIsOpen()).toBeFalsy();
            expect(viewModel.previewIsVisible()).toBeFalsy();
            expect(viewModel.previewIsLoading()).toBeFalsy();
            expect(viewModel.Terms()).toBeUndefined();
            expect(viewModel.selectedTerm()).toBeUndefined();
            expect(viewModel.DegreePlan()).toBeUndefined();
            expect(viewModel.DegreePlanId()).toBeUndefined();
            expect(ko.isObservable(viewModel.planningTerms)).toBeTruthy();
            expect(Array.isArray(viewModel.planningTerms())).toBeTruthy();

        });

        it("showPreviewButton is true", function () {
            expect(viewModel.showPreviewButton()).toBeTruthy();
        });
        it("enablePreviewButton is false", function () {
            expect(viewModel.enablePreviewButton()).toBeFalsy();
        });

   

    });
    //verify model after binding
    describe("after loading data", function () {

        beforeAll(function () {
            viewModel = new loadSamplePlanViewModel();
            viewModel.DegreePlanId(degreePlanId);
            ko.mapping.fromJS(studentPrograms,{}, viewModel);
            ko.mapping.fromJS(planningTerms, {}, viewModel);
            ko.mapping.fromJS(allPrograms, {}, viewModel);


        });
    

    it("verify mapping correctly happened", function () {
        expect(ko.isObservable(viewModel.StudentPrograms)).toBeTruthy();
        expect(Array.isArray(viewModel.StudentPrograms())).toBeTruthy();
        expect(viewModel.StudentPrograms().length).toBe(3);
        expect(viewModel.StudentPrograms()[0].Code()).toBe("AGBU.AA");
        expect(viewModel.planningTerms().length).toBe(1);
        expect(viewModel.planningTerms()[0].Code()).toBe("2017/SP");

    });

    it("verify selected program code modified as subscribed to Selected program", function () {
        expect(viewModel.SelectedProgramCode()).toBeUndefined();
        viewModel.SelectedProgram(viewModel.StudentPrograms()[0]);
        expect(viewModel.SelectedProgramCode()).toBe("AGBU.AA");
    });

    });

    it("verify selected program title when selectedprogramCode exist", function () {
        viewModel.SelectedProgram(viewModel.StudentPrograms()[1]);
        expect(viewModel.SelectedProgramCode()).toBe("MATH.BA");
        expect(viewModel.SelectedProgramTitle()).toBe("Bachelors in MATH");

    });



    it("verify selected program title when selectedprogramCode do not exist", function () {
        viewModel.SelectedProgram(viewModel.StudentPrograms()[2]);
        expect(viewModel.SelectedProgramCode()).toBe("TRIAL.AA");
        expect(viewModel.SelectedProgramTitle()).toBe("");

    });
    it("enablePrviewButton when selectedprogramCode is null", function () {
        viewModel.SelectedProgram(null);
        expect(viewModel.SelectedProgramCode()).toBe("");
        expect(viewModel.enablePreviewButton()).toBe(false);
    });
    it("enablePrviewButton when selectedprogramCode is undefined", function () {
        viewModel.SelectedProgram(undefined);
        expect(viewModel.SelectedProgramCode()).toBe("");
        expect(viewModel.enablePreviewButton()).toBe(false);
    });
 
    it("enablePrviewButton when selectedprogramCode has value", function () {
        viewModel.SelectedProgram(viewModel.StudentPrograms()[1]);
        expect(viewModel.SelectedProgramCode()).toBe("MATH.BA");
        expect(viewModel.enablePreviewButton()).toBe(true);
    });

    it("showPreviewButton when preiviewIsLoading is false and previewIsVisible is false", function () {
        viewModel.previewIsLoading(false);
        viewModel.previewIsVisible(false);
        expect(viewModel.showPreviewButton()).toBeTruthy();
    });

    it("showPreviewButton when preiviewIsLoading is true and previewIsVisible is true", function () {
        viewModel.previewIsLoading(true);
        viewModel.previewIsVisible(true);
        expect(viewModel.showPreviewButton()).toBeFalsy();
    });

    it("showPreviewButton when preiviewIsLoading is false and previewIsVisible is true", function () {
        viewModel.previewIsLoading(false);
        viewModel.previewIsVisible(true);
        expect(viewModel.showPreviewButton()).toBeFalsy();

    });

    it("showPreviewButton when preiviewIsLoading is true and previewIsVisible is false", function () {
        viewModel.previewIsLoading(true);
        viewModel.previewIsVisible(false);
        expect(viewModel.showPreviewButton()).toBeFalsy();

    });
 
});
