﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var facultyGradingNavigationInstance = new facultyGradingNavigationModel();

describe("faculty.grading.final.grade.view.model.js Tests: ", function () {

    describe("finalGradingModel: ", function () {

        beforeEach(function () {
            viewModel = new finalGradingModel();
        });

        it("studentGradesHolder is an empty array on initialization", function () {
            expect(viewModel.studentGradesHolder).toEqual([]);
        });

        it("StudentGrades is an empty array on initialization", function () {
            expect(viewModel.StudentGrades()).toEqual([]);
        });

        describe("countOfGradesNotVerified: ", function () {

            beforeEach(function () {
                viewModel.StudentGrades.push({
                    IsGradeVerified: ko.observable(true),
                    FinalGrade: ko.observable("A"),
                    hasErrors: ko.observable(false),
                    isValid: ko.observable(true),
                    isExpireDateRequired: ko.observable(true),
                    isLDARequired: ko.observable(true),
                });
            })

            it("all StudentGrades members' IsGradeVerified set to true", function () {
                expect(viewModel.countOfGradesNotVerified()).toBe(0);
            });

            it("all StudentGrades members' FinalGrade set to null", function () {
                viewModel.StudentGrades()[0].IsGradeVerified(false);
                viewModel.StudentGrades()[0].FinalGrade(null);
                viewModel.StudentGrades()[0].hasErrors(false);
                viewModel.StudentGrades()[0].isValid(true);
                viewModel.StudentGrades()[0].isExpireDateRequired(true);
                viewModel.StudentGrades()[0].isLDARequired(true);

                expect(viewModel.countOfGradesNotVerified()).toBe(0);
            });

            it("all StudentGrades members' isValid set to false", function () {
                viewModel.StudentGrades()[0].IsGradeVerified(false);
                viewModel.StudentGrades()[0].FinalGrade("A");
                viewModel.StudentGrades()[0].hasErrors(true);
                viewModel.StudentGrades()[0].isValid(false);
                viewModel.StudentGrades()[0].isExpireDateRequired(true);
                viewModel.StudentGrades()[0].isLDARequired(true);

                expect(viewModel.countOfGradesNotVerified()).toBe(0);
            });

            it("all StudentGrades members' isExpireDateRequired set to false", function () {
                viewModel.StudentGrades()[0].IsGradeVerified(false);
                viewModel.StudentGrades()[0].FinalGrade("A");
                viewModel.StudentGrades()[0].hasErrors(false);
                viewModel.StudentGrades()[0].isValid(true);
                viewModel.StudentGrades()[0].isExpireDateRequired(false);
                viewModel.StudentGrades()[0].isLDARequired(true);

                expect(viewModel.countOfGradesNotVerified()).toBe(0);
            });

            it("all StudentGrades members' isLDARequired set to false", function () {
                viewModel.StudentGrades()[0].IsGradeVerified(false);
                viewModel.StudentGrades()[0].FinalGrade("A");
                viewModel.StudentGrades()[0].hasErrors(false);
                viewModel.StudentGrades()[0].isValid(true);
                viewModel.StudentGrades()[0].isExpireDateRequired(true);
                viewModel.StudentGrades()[0].isLDARequired(false);

                expect(viewModel.countOfGradesNotVerified()).toBe(0);
            });

            it("all StudentGrades members' counted", function () {
                viewModel.StudentGrades()[0].IsGradeVerified(false);
                viewModel.StudentGrades()[0].FinalGrade("A");
                viewModel.StudentGrades()[0].hasErrors(false);
                viewModel.StudentGrades()[0].isValid(true);
                viewModel.StudentGrades()[0].isExpireDateRequired(true);
                viewModel.StudentGrades()[0].isLDARequired(true);

                expect(viewModel.countOfGradesNotVerified()).toBe(1);
            });

        });

        describe("countOfGradeEnterInProgress: ", function () {

            beforeEach(function () {
                viewModel.StudentGrades.push({
                    hasErrors: ko.observable(false),
                    isValid: ko.observable(true),
                    isWaiting: ko.observable(false)
                });
            })

            it("all StudentGrades members' isWaiting set to false", function () {
                expect(viewModel.countOfGradeEnterInProgress()).toBe(0);
            });

            it("all StudentGrades members' isWaiting set to true", function () {
                viewModel.StudentGrades()[0].isWaiting(true);

                expect(viewModel.countOfGradeEnterInProgress()).toBe(1);
            });
        });

        describe("areThereAnyErrors: ", function () {

            beforeEach(function () {
                viewModel.StudentGrades.push({
                    hasErrors: ko.observable(false),
                    isValid: ko.observable(true),
                });
            })

            it("all StudentGrades members' hasErrors set to false", function () {
                expect(viewModel.areThereAnyErrors()).toBe(0);
            });

            it("all StudentGrades members' hasErrors set to true", function () {
                viewModel.StudentGrades()[0].hasErrors(true);
                viewModel.StudentGrades()[0].isValid(false);

                expect(viewModel.areThereAnyErrors()).toBe(1);
            });
        });

        it("GradeTypes is an empty array on initialization", function () {
            expect(viewModel.GradeTypes()).toEqual([]);
        });

        it("SectionId is undefined on initialization", function () {
            expect(viewModel.SectionId()).toEqual(undefined);
        });

        describe("hasStudentGrades: ", function () {

            it("false when StudentGrades array is null", function () {
                viewModel.StudentGrades(null);

                expect(viewModel.hasStudentGrades()).toBeFalsy();
            });

            it("false when StudentGrades array is undefined", function () {
                viewModel.StudentGrades();

                expect(viewModel.hasStudentGrades()).toBeFalsy();
            });

            it("false when StudentGrades array is empty", function () {
                viewModel.StudentGrades([]);

                expect(viewModel.hasStudentGrades()).toBeFalsy();
            });

            it("false when StudentGrades array is empty", function () {
                viewModel.StudentGrades.push({
                    IsGradeVerified: ko.observable(true),
                    FinalGrade: ko.observable("A"),
                    hasErrors: ko.observable(false),
                    isValid: ko.observable(true),
                    isExpireDateRequired: ko.observable(true),
                    isLDARequired: ko.observable(true),
                });

                expect(viewModel.hasStudentGrades()).toBeTruthy();
            });
        });

        it("addStudentGrade adds array member to studentGradesHolder", function () {
            viewModel.addStudentGrade({});

            expect(viewModel.studentGradesHolder.length).toEqual(1);
        });

    });

    describe("studentGradeModel: ", function () {

        var data;
        var sectionStartDate;
        var sectionEndDate;
        var initialGradeData;

        beforeEach(function () {
            data = {
                "AcademicCreditStatus": "New",
                "Credits": "3",
                "StudentName": "Able, George",
                "FinalGrade": "A",
                "GradeExpireDate": "",
                "IsGradeVerified": false,
                "LastDateAttended": "",
                "NeverAttended": "",
                "SectionId": "19534",
                "StudentId": "0012623",
                "VerifyGradeResponseErrors": "",
                "Midterm1Grade": "A",
                "Midterm2Grade": "B",
                "Midterm3Grade": "C",
                "Midterm4Grade": "D",
                "Midterm5Grade": "F",
                "Midterm6Grade": "",
                "isExpireDateRequired": true,
                "isValid": true,
                "isLDARequired": true
            };
            sectionStartDate = "02/04/2017";
            sectionEndDate = "05/31/2017";
            initialGradeData = ko.observable({
                Item1: ko.observable("A"),
                Item2: ko.observable("39"),
                Item3: ko.observable("X")
            });
            viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
        })

        it("calls baseStudentGradeModel on initialization with self", function () {
            spyOn(baseStudentGradeModel, 'call').and.callThrough();

            viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);

            expect(baseStudentGradeModel.call).toHaveBeenCalledWith(viewModel);
        });

        it("sectionStartDate is SectionStartDate as moment on initialization", function () {
            expect(viewModel.sectionStartDate).toEqual(moment(sectionStartDate));
        });

        it("sectionEndDate is SectionEndDate as moment on initialization", function () {
            expect(viewModel.sectionEndDate).toEqual(moment(sectionEndDate));
        });

        describe("hasErrors: ", function () {

        });

        it("lastDateValidationMessage is '' on initialization", function () {
            expect(viewModel.lastDateValidationMessage()).toEqual('');
        });

        it("isLDARequired is true on initialization", function () {
            expect(viewModel.isLDARequired()).toBeTruthy();
        });

        it("LastDateAttended.isValid is true on initialization", function () {
            expect(viewModel.LastDateAttended.isValid()).toBeTruthy();
        });

        it("gradeExpireDateMessage is '' on initialization", function () {
            expect(viewModel.gradeExpireDateMessage()).toEqual('');
        });

        it("isExpireDateRequired is true on initialization", function () {
            expect(viewModel.isExpireDateRequired()).toBeTruthy();
        });

        it("GradeExpireDate.isValid is true on initialization", function () {
            expect(viewModel.GradeExpireDate.isValid()).toBeTruthy();
        });

        describe("isValid: ", function () {

        });

        it("finalGradeSelected is initialGradeData on initialization", function () {
            expect(viewModel.finalGradeSelected()).toEqual(initialGradeData);
        });

        describe("finalGradeSelected subscription: ", function () {

            var newValue = {
                Item1: ko.observable("A"),
                Item2: ko.observable("39"),
                Item3: ko.observable("X")
            };

            it("finalGradeSelected.Item2 is not null or empty but GradeExpireDate is null", function () {

                viewModel.finalGradeSelected(newValue);

                expect(viewModel.isExpireDateRequired()).toBeFalsy();
                expect(viewModel.FinalGrade()).toEqual(newValue.Item1());
                expect(viewModel.isLDARequired()).toBeTruthy();
            });

            it("finalGradeSelected.Item2 is null and GradeExpireDate is not null or empty", function () {
                data.GradeExpireDate = "12/31/2017";
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                newValue.Item2('');

                viewModel.finalGradeSelected(newValue);

                expect(viewModel.isExpireDateRequired()).toBeTruthy();
                expect(viewModel.GradeExpireDate()).toEqual('');
                expect(viewModel.FinalGrade()).toEqual(newValue.Item1());
                expect(viewModel.isLDARequired()).toBeTruthy();
            });

            it("finalGradeSelected.Item3 is true ", function () {
                data.GradeExpireDate = "12/31/2017";
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                newValue.Item3(true);

                viewModel.finalGradeSelected(newValue);

                expect(viewModel.isExpireDateRequired()).toBeTruthy();
                expect(viewModel.GradeExpireDate()).toEqual('');
                expect(viewModel.FinalGrade()).toEqual(newValue.Item1());
                expect(viewModel.isLDARequired()).toBeFalsy();
            });

            it("FinalGrade set to null if newValue is null", function () {
                viewModel.finalGradeSelected(null);
                expect(viewModel.FinalGrade()).toBeNull();
            });

        });

        describe("validateLastDate: ", function () {

            it("returns true when value is null", function () {
                expect(viewModel.validateLastDate(null)).toBeTruthy();
            });

            it("returns true when value has a length of 0", function () {
                expect(viewModel.validateLastDate('')).toBeTruthy();
            });

            it("sets lastDateValidationMessage to ValidDateErrorMessage and returns false when value is not a date", function () {
                expect(viewModel.validateLastDate('ABCD')).toBeFalsy();
                expect(viewModel.lastDateValidationMessage()).toEqual(ValidDateErrorMessage);
            });

            it("sets lastDateValidationMessage to AttendanceDateErrorMesssage and returns false when value is earlier than sectionStartDate", function () {
                expect(viewModel.validateLastDate('01/01/0001')).toBeFalsy();
                expect(viewModel.lastDateValidationMessage()).toEqual(AttendanceDateErrorMesssage);
            });

            it("sets lastDateValidationMessage to AttendanceDateErrorMesssage and returns false when value is greater than sectionEndDate", function () {
                expect(viewModel.validateLastDate('12/31/9999')).toBeFalsy();
                expect(viewModel.lastDateValidationMessage()).toEqual(AttendanceDateErrorMesssage);
            });

            it("returns true when value is between sectionStartDate and sectionEndDate", function () {
                expect(viewModel.validateLastDate('04/01/2017')).toBeTruthy();
            });

        });

        describe("LastDateAttended subscription: ", function () {

            function validateSubscriptionOutcomes() {
                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeNull();
            }

            beforeEach(function () {
                var finalGrade = {
                    Item1: ko.observable("A"),
                    Item2: ko.observable("39"),
                    Item3: ko.observable("X")
                };
                data.GradeExpireDate = "12/31/2017";
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                viewModel.finalGradeSelected(finalGrade);
            })


            it("sets isLDARequired to true when newValue is null and finalGradeSelected.Item3 is not true", function () {
                spyOn(viewModel, 'validateLastDate').and.callThrough();

                viewModel.LastDateAttended(null);

                expect(viewModel.isLDARequired()).toBeTruthy();
                expect(viewModel.validateLastDate).toHaveBeenCalledWith(null);
                expect(viewModel.LastDateAttended.isValid()).toBeTruthy();
                validateSubscriptionOutcomes();
            });

            it("sets isLDARequired to false when newValue is null and finalGradeSelected.Item3 is true", function () {
                var finalGrade = {
                    Item1: ko.observable("A"),
                    Item2: ko.observable("39"),
                    Item3: ko.observable(true)
                };
                data.GradeExpireDate = "12/31/2017";
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                viewModel.finalGradeSelected(finalGrade);
                spyOn(viewModel, 'validateLastDate').and.callThrough();

                viewModel.LastDateAttended(null);

                expect(viewModel.isLDARequired()).toBeFalsy();
                expect(viewModel.validateLastDate).toHaveBeenCalled();
                expect(viewModel.LastDateAttended.isValid()).toBeTruthy();
                validateSubscriptionOutcomes();
            });

        });

        describe("validateGradeExpireDate: ", function () {

            it("returns true when value is null", function () {
                expect(viewModel.validateGradeExpireDate(null)).toBeTruthy();
            });

            it("returns true when value has a length of 0", function () {
                expect(viewModel.validateGradeExpireDate('')).toBeTruthy();
            });

            it("sets gradeExpireDateMessage to ValidDateErrorMessage and returns false when value is not a date", function () {
                expect(viewModel.validateGradeExpireDate('ABCD')).toBeFalsy();
                expect(viewModel.gradeExpireDateMessage()).toEqual(ValidDateErrorMessage);
            });

            it("sets gradeExpireDateMessage to ExpirationDateErrorMessage and returns false when value is less than sectionEndDate", function () {
                expect(viewModel.validateGradeExpireDate('01/01/0001')).toBeFalsy();
                expect(viewModel.gradeExpireDateMessage()).toEqual(ExpirationDateErrorMessage);
            });

            it("returns true when value is greater than sectionEndDate", function () {
                expect(viewModel.validateGradeExpireDate('12/31/9999')).toBeTruthy();
            });

        });

        describe("GradeExpireDate subscription: ", function () {

            function validateSubscriptionOutcomes() {
                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeNull();
            }

            beforeEach(function () {
                var finalGrade = {
                    Item1: ko.observable("A"),
                    Item2: ko.observable("39"),
                    Item3: ko.observable("X")
                };
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                viewModel.finalGradeSelected(finalGrade);
            })


            it("sets isExpireDateRequired to false when newValue is null and finalGradeSelected.Item2 is not null", function () {
                spyOn(viewModel, 'validateGradeExpireDate').and.callThrough();

                viewModel.GradeExpireDate(null);

                expect(viewModel.isExpireDateRequired()).toBeFalsy();
                expect(viewModel.validateGradeExpireDate).toHaveBeenCalledWith(null);
                expect(viewModel.GradeExpireDate.isValid()).toBeTruthy();
                validateSubscriptionOutcomes();
            });

            it("sets isExpireDateRequired to true when newValue is null and finalGradeSelected.Item2 is null", function () {
                var finalGrade = {
                    Item1: ko.observable("A"),
                    Item2: ko.observable(null),
                    Item3: ko.observable("X")
                };
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                viewModel.finalGradeSelected(finalGrade);
                spyOn(viewModel, 'validateGradeExpireDate').and.callThrough();

                viewModel.GradeExpireDate(null);

                expect(viewModel.isExpireDateRequired()).toBeTruthy();
                expect(viewModel.validateGradeExpireDate).toHaveBeenCalled();
                expect(viewModel.GradeExpireDate.isValid()).toBeTruthy();
                validateSubscriptionOutcomes();
            });

        });

        describe("FinalGrade subscription: ", function () {

            it("isFieldDirty set to true and isSuccessfullyUpdated set to null when FinalGrade is not null", function () {
                viewModel.FinalGrade("B");

                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeNull();
            });

            it("isFieldDirty and isSuccessfullyUpdated not set when FinalGrade is null", function () {
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                
                viewModel.FinalGrade(null);

                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
            });


        });

        describe("NeverAttended subscription: ", function () {

            it("returns when newValue is null", function () {
                var expectedIsFieldDirty = viewModel.isFieldDirty();
                var expectedIsSuccessfullyUpdated = viewModel.isSuccessfullyUpdated();
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.NeverAttended(null);

                expect(viewModel.isFieldDirty()).toEqual(expectedIsFieldDirty);
                expect(viewModel.isSuccessfullyUpdated()).toEqual(expectedIsSuccessfullyUpdated);
                expect(viewModel.checkIfNeedToSave).not.toHaveBeenCalled();

            });

            it("sets LastDateAttended to '' when newValue is true", function () {
                var finalGrade = {
                    Item1: ko.observable("A"),
                    Item2: ko.observable("39"),
                    Item3: ko.observable(true)
                };
                data.GradeExpireDate = "12/31/2017";
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                viewModel.finalGradeSelected(finalGrade);
                spyOn(viewModel, 'checkIfNeedToSave');
                
                viewModel.NeverAttended(true);

                expect(viewModel.LastDateAttended()).toEqual('');
                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeNull();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("sets isLDARequired to true and does not set LastDateAttended when newValue is not true, null, or empty", function () {
                var expectedLastAttendedDate = viewModel.LastDateAttended();
                var finalGrade = {
                    Item1: ko.observable("A"),
                    Item2: ko.observable("39"),
                    Item3: ko.observable(true)
                };
                data.GradeExpireDate = "12/31/2017";
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                viewModel.finalGradeSelected(finalGrade);
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.NeverAttended(true);

                expect(viewModel.LastDateAttended()).toEqual(expectedLastAttendedDate);
                expect(viewModel.isLDARequired()).toBeTruthy();
                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeNull();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

            it("sets isLDARequired to false when newValue is true", function () {
                var finalGrade = {
                    Item1: ko.observable("A"),
                    Item2: ko.observable("39"),
                    Item3: ko.observable(true)
                };
                data.GradeExpireDate = "12/31/2017";
                viewModel = new studentGradeModel(data, sectionStartDate, sectionEndDate, initialGradeData);
                viewModel.finalGradeSelected(finalGrade);
                viewModel.LastDateAttended(null);
                spyOn(viewModel, 'checkIfNeedToSave');

                viewModel.NeverAttended(false);

                expect(viewModel.isLDARequired()).toBeFalsy();
                expect(viewModel.isFieldDirty()).toBeTruthy();
                expect(viewModel.isSuccessfullyUpdated()).toBeNull();
                expect(viewModel.checkIfNeedToSave).toHaveBeenCalled();
            });

        });

        describe("updateNotificationOnGradeVerify: ", function () {

            it("returns when IsGradeVerified is null", function () {
                spyOn(facultyGradingNavigationInstance.notificationInstance, 'updateNotificationCenter');
                viewModel.IsGradeVerified(null);
                expect(facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter).not.toHaveBeenCalled();
            });

            it("calls facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter when IsGradeVerified is true", function () {
                spyOn(facultyGradingNavigationInstance.notificationInstance, 'updateNotificationCenter').and.callThrough();
                viewModel.IsGradeVerified(true);
                expect(facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter).toHaveBeenCalledWith(viewModel.StudentId(), null);
            });

            it("does not call facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter when IsGradeVerified is false and gradeEntryErrors is empty", function () {
                spyOn(facultyGradingNavigationInstance.notificationInstance, 'updateNotificationCenter').and.callThrough();
                viewModel.IsGradeVerified(false);
                expect(facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter).not.toHaveBeenCalled();
            });

            it("calls facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter when IsGradeVerified is false and gradeEntryErrors has at least one value", function () {
                viewModel.IsGradeVerified(true);
                viewModel.gradeEntryErrors.push('error');
                spyOn(facultyGradingNavigationInstance.notificationInstance, 'updateNotificationCenter').and.callThrough();
                viewModel.IsGradeVerified(false);
                expect(facultyGradingNavigationInstance.notificationInstance.updateNotificationCenter).toHaveBeenCalledWith(viewModel.StudentId(), viewModel.gradeEntryErrors());
            });

        });

    });

    describe("verifyGradeModel: ", function () {

        var parent;
        var sibling;

        beforeEach(function () {
            parent = {
                overviewGradingModelInstance: new overviewGradingModel(),
                isGradeEntryPerformed: ko.observable(true)
            };
            sibling = new finalGradingModel();
            sibling.StudentGrades.push({
                IsGradeVerified: ko.observable(true),
                FinalGrade: ko.observable("A"),
                hasErrors: ko.observable(false),
                isValid: ko.observable(true),
                isExpireDateRequired: ko.observable(true),
                isLDARequired: ko.observable(true),
                StudentId: ko.observable("0001234"),
                finalGradeSelected: ko.observable(),
                gradeEntryErrors: ko.observable(null),
                GradeExpireDate: ko.observable(),
                NeverAttended: ko.observable(),
                LastDateAttended: ko.observable()
            });
            sibling.GradeTypes([{
                        Item1: ko.observable("A"),
                        Item2: ko.observable("")
                    },
                    {
                        Item1: ko.observable("B"),
                        Item2: ko.observable("")
                    },
                    {
                        Item1: ko.observable("D"),
                        Item2: ko.observable("39")
                    }
            ]);
            sibling.SectionId("123");
            viewModel = new verifyGradeModel(parent, sibling);
        });

        it("isVerifying is false on initialization", function () {
            expect(viewModel.isVerifying()).toBeFalsy();
        });

        it("verifyGradesDialogisOpen is false on initialization", function () {
            expect(viewModel.verifyGradesDialogisOpen()).toBeFalsy();
        });

        it("verifyGradeIsClicked sets verifyGradesDialogisOpen to true", function () {
            viewModel.verifyGradeIsClicked();

            expect(viewModel.verifyGradesDialogisOpen()).toBeTruthy();
        });

        describe("transform: ", function () {

            var verifiedGrades = [{
                    StudentId: "0001234",
                    VerifyGradeResponseErrors: null,
                    GradeExpireDate: "02/04/2017",
                    NeverAttended: true,
                    LastDateAttended: "03/01/2017",
                    IsGradeVerified: true,
                    FinalGrade: "A"
                }];

            it("gradeType found in sibling.GradeTypes", function () {
                viewModel.transform(verifiedGrades);

                expect(sibling.StudentGrades()[0].finalGradeSelected()).toEqual(sibling.GradeTypes()[0]);
            });

            it("gradeType not found in sibling.GradeTypes", function () {
                var expected = sibling.StudentGrades()[0].finalGradeSelected()
                sibling.GradeTypes([
                    {
                        Item1: ko.observable("X"),
                        Item2: ko.observable("")
                    }
                ]);
                viewModel = new verifyGradeModel(parent, sibling);
                viewModel.transform(verifiedGrades);

                expect(sibling.StudentGrades()[0].finalGradeSelected()).toEqual(expected);
            });

        });

        describe("verifyGrades: ", function () {

            function validateCompleteOutcomes() {
                expect(viewModel.isVerifying()).toBeFalsy();
            };

            it("success", function () {
                var data = {
                    "CanEditGrades": true,
                    "CanVerifyGrades": true,
                    "IsTermOpen": true,
                    "SectionId": "19534",
                    "SectionStartDate": "02/03/2015",
                    "SectionEndDate": null,
                    "GradeTypes": [
                        { "Item1": "A", "Item2": "" },
                         { "Item1": "B", "Item2": "" },
                         { "Item1": "D", "Item2": "39" },
                    ],
                    "StudentGrades": [
                        { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Able, George", "FinalGrade": "A", "GradeExpireDate": "", "IsGradeVerified": false, "LastDateAttended": "", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012623", "VerifyGradeResponseErrors": "", "Midterm1Grade": "A", "Midterm2Grade": "B", "Midterm3Grade": "C", "Midterm4Grade": "D", "Midterm5Grade": "F", "Midterm6Grade": "" },
                         { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Cruz, ted", "FinalGrade": "", "GradeExpireDate": "", "IsGradeVerified": false, "LastDateAttended": "02/03/2016", "NeverAttended": "", "SectionId": "19534", "StudentId": "0012882", "VerifyGradeResponseErrors": "", "Midterm1Grade": "A", "Midterm2Grade": "B", "Midterm3Grade": "C", "Midterm4Grade": "D", "Midterm5Grade": "", "Midterm6Grade": "" },
                          { "AcademicCreditStatus": "New", "Credits": "3", "StudentName": "Sunflower, Sunny", "FinalGrade": "D", "GradeExpireDate": "02/04/2017", "IsGradeVerified": true, "LastDateAttended": "", "NeverAttended": true, "SectionId": "19534", "StudentId": "0012559", "VerifyGradeResponseErrors": "", "Midterm1Grade": "A", "Midterm2Grade": "B", "Midterm3Grade": "C", "Midterm4Grade": "D", "Midterm5Grade": "", "Midterm6Grade": "" }
                    ]
                };
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend({});
                    e.success(data);
                    e.complete({});
                });
                spyOn(ko.mapping, 'fromJS').and.callThrough();
                spyOn(viewModel, 'transform').and.callThrough();

                viewModel.verifyGrades();
                expect(ko.mapping.fromJS).toHaveBeenCalledWith(data, {}, parent.overviewGradingModelInstance);
                expect(viewModel.transform).toHaveBeenCalledWith(data.StudentGrades);
                expect(parent.isGradeEntryPerformed()).toBeFalsy();

                validateCompleteOutcomes();
            });

            it("error", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend({});
                    e.success({});
                    e.error({ status: 400 }, "", "");
                    e.complete({});
                });
                spyOn($.fn, "notificationCenter");

                viewModel.verifyGrades();

                expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", { message: VerificationFailedErrorMessage, type: "error" });
                validateCompleteOutcomes();
            });

        });

        it("countOfGradesToVerify is sibling countOfGradesNotVerified", function () {
            expect(viewModel.countOfGradesToVerify()).toEqual(sibling.countOfGradesNotVerified());
        });

        it("verifyDialogOptions is set correctly on initialization", function () {
            expect(viewModel.verifyDialogOptions()).toEqual({
                autoOpen: false,
                modal: true,
                resizable: false,
                minWidth: 500,
                scrolltop: 0,
                scrollleft: 0,
                show: { effect: "fade", duration: dialogFadeTimeMs },
                hide: { effect: "fade", duration: dialogFadeTimeMs }
            });
        });

        it("closeDialog sets verifyGradesDialogisOpen to false", function () {
            viewModel.verifyGradesDialogisOpen(true);
            viewModel.closeDialog();

            expect(viewModel.verifyGradesDialogisOpen()).toBeFalsy();
        });

        it("okayToVerifyGrades calls verifyGrades and closeDialog", function () {
            spyOn(viewModel, 'verifyGrades');
            spyOn(viewModel, 'closeDialog');

            viewModel.okayToVerifyGrades();
            expect(viewModel.verifyGrades).toHaveBeenCalled();
            expect(viewModel.closeDialog).toHaveBeenCalled();
        });

        describe("displayActionButtons: ", function () {

            it("returns false when countOfGradesToVerify <= 0", function () {
                expect(viewModel.displayActionButtons()).toBeFalsy();
            })

            it("returns true when countOfGradesToVerify > 0", function () {
                sibling.StudentGrades()[0].IsGradeVerified(false);
                sibling.StudentGrades()[0].FinalGrade("A");
                sibling.StudentGrades()[0].hasErrors(false);
                sibling.StudentGrades()[0].isValid(true);
                sibling.StudentGrades()[0].isExpireDateRequired(true);
                sibling.StudentGrades()[0].isLDARequired(true);

                viewModel = new verifyGradeModel(parent, sibling);

                expect(viewModel.displayActionButtons()).toBeTruthy();
            });

        });

        describe("displayOkButton: ", function () {

            it("returns true when countOfGradesToVerify <= 0", function () {
                expect(viewModel.displayOkButton()).toBeTruthy();
            })

            it("returns false when countOfGradesToVerify > 0", function () {
                sibling.StudentGrades()[0].IsGradeVerified(false);
                sibling.StudentGrades()[0].FinalGrade("A");
                sibling.StudentGrades()[0].hasErrors(false);
                sibling.StudentGrades()[0].isValid(true);
                sibling.StudentGrades()[0].isExpireDateRequired(true);
                sibling.StudentGrades()[0].isLDARequired(true);

                viewModel = new verifyGradeModel(parent, sibling);

                expect(viewModel.displayOkButton()).toBeFalsy();
            });

        });

        it("postGradesButton set correctly on initialization", function () {
            expect(viewModel.postGradesButton).toEqual({
                id: 'postGradesButton',
                title: Ellucian.Faculty.Resources.PostGradesButtonText,
                isPrimary: true,
                visible: viewModel.displayActionButtons,
                callback: viewModel.okayToVerifyGrades
            });
        });

        it("okButton set correctly on initialization", function () {
            expect(viewModel.okButton).toEqual({
                id: 'okButton',
                title: Ellucian.Faculty.Resources.OKButtonText,
                isPrimary: true,
                visible: viewModel.displayOkButton,
                callback: viewModel.closeDialog
            });
        });

        it("cancelButton set correctly on initialization", function () {
            expect(viewModel.cancelButton).toEqual({
                id: 'cancelButton',
                title: Ellucian.Faculty.Resources.CancelButtonText,
                isPrimary: false,
                visible: viewModel.displayActionButtons,
                callback: viewModel.closeDialog
            });
        });

    });

    describe("notificationModel: ", function () {

        beforeEach(function () {
            viewModel = new notificationModel();
        });

        it("currentNotificationIndex is 0 on initialization", function () {
            expect(viewModel.currentNotificationIndex).toEqual(0);
        });

        it("notificationMessages is an empty array on initialization", function () {
            expect(viewModel.notificationMessages).toEqual([]);
        });

    });
});