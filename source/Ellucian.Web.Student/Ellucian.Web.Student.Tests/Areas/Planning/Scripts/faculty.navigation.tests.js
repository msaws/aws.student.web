﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

describe("faculty.navigation.js tests: ", function () {

    describe("facultyPermissionModel: ", function () {

        beforeEach(function () {
            viewModel = new facultyPermissionModel();
        })

        describe("addConsentSaveButtonEnabled: ", function () {

            it("returns true when consentStudent is not undefined or null", function () {
                viewModel.consentStudent({ Name: 'Joseph' });

                expect(viewModel.addConsentSaveButtonEnabled()).toBeTruthy();
            });

            it("returns false when consentStudent is undefined", function () {
                viewModel.consentStudent = undefined;

                expect(viewModel.addConsentSaveButtonEnabled()).toBeFalsy();
            });

            it("returns false when consentStudent is null", function () {
                expect(viewModel.addConsentSaveButtonEnabled()).toBeFalsy();
            });

        });

        describe("addPetitionSaveButtonEnabled: ", function () {

            it("returns true when petitionStudent is not undefined or null", function () {
                viewModel.petitionStudent({ Name: 'Joseph' });

                expect(viewModel.addPetitionSaveButtonEnabled()).toBeTruthy();
            });

            it("returns false when petitionStudent is undefined", function () {
                viewModel.petitionStudent = undefined;

                expect(viewModel.addPetitionSaveButtonEnabled()).toBeFalsy();
            });

            it("returns false when petitionStudent is null", function () {
                expect(viewModel.addPetitionSaveButtonEnabled()).toBeFalsy();
            });

        });

        describe("addWaiverDenyButtonEnabled: ", function () {

            it("returns true when waivedStudent is not undefined or null", function () {
                viewModel.waivedStudent({ Name: 'Joseph' });

                expect(viewModel.addWaiverDenyButtonEnabled()).toBeTruthy();
            });

            it("returns false when waivedStudent is undefined", function () {
                viewModel.waivedStudent = undefined;

                expect(viewModel.addWaiverDenyButtonEnabled()).toBeFalsy();
            });

            it("returns false when waivedStudent is null", function () {
                expect(viewModel.addWaiverDenyButtonEnabled()).toBeFalsy();
            });

        });

        it("addConsentSaveButton set correctly on initialization", function () {
            expect(viewModel.addConsentSaveButton).toEqual({
                id: 'addconsentsavebutton',
                title: Ellucian.Faculty.Resources.AddConsentSaveButtonText,
                isPrimary: true,
                enabled: viewModel.addConsentSaveButtonEnabled,
                aria: Ellucian.Faculty.Resources.AddConsentSaveButtonAriaLabel,
                callback: viewModel.saveConsent
            });
        });

        it("addConsentCancelButton set correctly on initialization", function () {
            expect(viewModel.addConsentCancelButton).toEqual({
                id: 'addconsentcancelbutton',
                title: Ellucian.Faculty.Resources.AddConsentCancelButtonText,
                isPrimary: false,
                callback: viewModel.cancelAddConsentDialog
            });
        });

        it("addPetitionSaveButton set correctly on initialization", function () {
            expect(viewModel.addPetitionSaveButton).toEqual({
                id: 'addpetitionsavebutton',
                title: Ellucian.Faculty.Resources.AddPetitionSaveButtonText,
                isPrimary: true,
                enabled: viewModel.addPetitionSaveButtonEnabled,
                aria: Ellucian.Faculty.Resources.AddPetitionSaveButtonAriaLabel,
                callback: viewModel.savePetition
            });
        });

        it("addPetitionCancelButton set correctly on initialization", function () {
            expect(viewModel.addPetitionCancelButton).toEqual({
                id: 'addpetitioncancelbutton',
                title: Ellucian.Faculty.Resources.AddPetitionCancelButtonText,
                isPrimary: false,
                callback: viewModel.cancelAddPetitionDialog
            });
        });

        it("addWaiverDenybutton set correctly on initialization", function () {
            expect(viewModel.addWaiverDenybutton).toEqual({
                id: 'addwaiverdenybutton',
                title: Ellucian.Faculty.Resources.AddWaiverSaveButtonText,
                isPrimary: true,
                enabled: viewModel.addWaiverDenyButtonEnabled,
                aria: Ellucian.Faculty.Resources.AddWaiverSaveButtonAriaLabel,
                callback: viewModel.approveOrDeny
            });
        });

        it("addWaiverCancelButton set correctly on initialization", function () {
            expect(viewModel.addWaiverCancelButton).toEqual({
                id: 'addwaivercancelbutton',
                title: Ellucian.Faculty.Resources.AddWaiverCancelButtonText,
                isPrimary: false,
                callback: viewModel.cancelAddWaiverDialog
            });
        });

    });

});