﻿// Copyright 2016 Ellucian Company L.P. and its affiliates

describe("cost centers home script", function () {
    
    var testRepository;
    var barGraphDom = '<div id="bar-graph-view"></div>';
    var listViewDom = '<div id="main-list-view"></div>';
    var barGraphToggleDom = '<div id="bar-graph-toggle"></div>';
    var listToggleDom = '<div id="list-toggle"></div>';
    var viewModel;

    beforeAll(function () {
        viewModel = new costCenterSummaryViewModel();
        // costCenterSummaryViewModelInstance.SelectedFiscalYear("2015");
    });

    beforeEach(function () {
        $(document.body).append(barGraphDom);
        $(document.body).append(listViewDom);
        $(document.body).append(barGraphToggleDom);
        $(document.body).append(listToggleDom);
    });

    //describe("toggleGrid", function () {
    //    it("should show the graph elements and hide the list elements", function () {
    //        toggleGrid();

    //        // Show the bar graph view and hide the list view
    //        expect($("#bar-graph-view").is(":visible")).toBeTruthy();
    //        expect($("#main-list-view").is(":visible")).toBeFalsy();

    //        // Mark the bar graph toggle button as ARIA pressed: true and the list toggle button as false
    //        expect($("#bar-graph-toggle").attr("aria-pressed")).toBeTruthy();
    //        expect($("#list-toggle").attr("aria-pressed")).toBe("false");

    //        // The bar graph toggle should have the 'active-view' class and the list toggle should not.
    //        expect($("#bar-graph-toggle").hasClass("active-view")).toBeTruthy();
    //        expect($("#list-toggle").hasClass("active-view")).toBeFalsy();
    //    });
    //});

    //describe("toggleGrid", function () {
    //    it("should show the list elements and hide the graph elements", function () {
    //        toggleList();

    //        // Show the bar graph view and hide the list view
    //        expect($("#main-list-view").is(":visible")).toBeTruthy();
    //        expect($("#bar-graph-view").is(":visible")).toBeFalsy();

    //        // Mark the bar graph toggle button as ARIA pressed: true and the list toggle button as false
    //        expect($("#list-toggle").attr("aria-pressed")).toBeTruthy();
    //        expect($("#bar-graph-toggle").attr("aria-pressed")).toBe("false");

    //        // The bar graph toggle should have the 'active-view' class and the list toggle should not.
    //        expect($("#list-toggle").hasClass("active-view")).toBeTruthy();
    //        expect($("#bar-graph-toggle").hasClass("active-view")).toBeFalsy();
    //    });
    //});
});