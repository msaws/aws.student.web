﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

describe("costCenterModel", function () {
    var costCenterRepository,
        testCostCenterData,
        testCostCenterModel;

    beforeEach(function () {
        costCenterRepository = new testCostCenterRepository();
    });

    afterEach(function () {
        costCenterRepository = null;
        testCostCenterData = null;
        testCostCenterModel = null;
    });

    describe("costCenterBarGraphData", function () {
        it("should return an array with a value for the budget spent percent, a CSS class for green color, and a title", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.costCenterBarGraphData()[0].value).toBe(testCostCenterData.BudgetSpentPercent);
            expect(testCostCenterModel.costCenterBarGraphData()[0].barClass).toBe("bar-graph bar-graph--success");
            expect(testCostCenterModel.costCenterBarGraphData()[0].title).toBe(costCenterBarGraphTitle);
        });

        it("should return an array with a value for the budget spent percent, a CSS class for yellow color, and a title", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetSpentPercent = 90;
            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.costCenterBarGraphData()[0].value).toBe(testCostCenterData.BudgetSpentPercent);
            expect(testCostCenterModel.costCenterBarGraphData()[0].barClass).toBe("bar-graph bar-graph--warning");
            expect(testCostCenterModel.costCenterBarGraphData()[0].title).toBe(costCenterBarGraphTitle);
        });

        it("should return an array with a value for the budget spent percent, a CSS class for red color, and a title", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = -500;
            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.costCenterBarGraphData()[0].value).toBe(testCostCenterData.BudgetSpentPercent);
            expect(testCostCenterModel.costCenterBarGraphData()[0].barClass).toBe("bar-graph bar-graph--error");
            expect(testCostCenterModel.costCenterBarGraphData()[0].title).toBe(costCenterBarGraphTitle);
        });
    });

    describe("costCenterRevenueBarGraphData ", function () {
        it("should return an array with a value for the budget spent percent, a CSS class for teal color, and a title", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.costCenterRevenueBarGraphData()[0].value).toBe(testCostCenterData.BudgetSpentPercentRevenue);
            expect(testCostCenterModel.costCenterRevenueBarGraphData()[0].barClass).toBe("bar-graph bar-graph--hint");
            expect(testCostCenterModel.costCenterRevenueBarGraphData()[0].title).toBe(costCenterBarGraphTitle);
        });
    });

    describe("remainingBudgetColor", function () {
        it("should return a CSS class for red text if the remaining budget is less than zero", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = -100.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.remainingBudgetColor()).toBe("remaining-red");
        });

        it("should return a CSS class for black text if the remaining budget is greater than zero", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.remainingBudgetColor()).toBe("remaining-black");
        });
    });

    describe("remainingBudgetRevenueColor", function () {
        it("should return a CSS class for green text if the remaining budget is less than zero", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemainingRevenue = -100.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.remainingBudgetRevenueColor()).toBe("remaining-green");
        });

        it("should return a CSS class for black text if the remaining budget is greater than zero", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.remainingBudgetRevenueColor()).toBe("remaining-black");
        });
    });

    describe("overageColor", function () {
        it("should return CSS class 'zero-budget' if the remaining budget is zero", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.TotalBudget = 0.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.overageColor()).toBe("zero-budget");
        });

        it("should return CSS class 'zero-budget' if the cost center is 100% over budget", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.Overage = 100.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.overageColor()).toBe("zero-budget overage");
        });

        it("should return CSS class 'zero-budget' if the cost center is more than 100% over budget", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.Overage = 150.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.overageColor()).toBe("zero-budget overage");
        });
    });

    describe("overageRevenueColor", function () {
        it("should return CSS class 'zero-budget' if the remaining budget is zero", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.TotalBudgetRevenue = 0.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.overageRevenueColor()).toBe("zero-budget");
        });

        it("should return CSS class 'zero-budget' if the cost center is 100% over budget", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.OverageRevenue = 100.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.overageRevenueColor()).toBe("zero-budget overage-revenue");
        });

        it("should return CSS class 'zero-budget' if the cost center is more than 100% over budget", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.OverageRevenue = 150.00;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.overageRevenueColor()).toBe("zero-budget overage-revenue");
        });
    });

    describe("financialHealthIndicator", function () {
        it("should return CSS class 'cost-center-icon-green' if the cost center has more than 0% budget spent", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = 1000.00;
            testCostCenterData.BudgetSpentPercent = 0;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.financialHealthIndicator()).toBe("cost-center-icon-green");
        });

        it("should return CSS class 'cost-center-icon-green' if the cost center has 0-84% budget spent", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = 100.00;
            testCostCenterData.BudgetSpentPercent = 50;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.financialHealthIndicator()).toBe("cost-center-icon-green");
        });

        it("should return CSS class 'cost-center-icon-green' if the cost center has 84% budget spent", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = 16.00;
            testCostCenterData.BudgetSpentPercent = 84;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.financialHealthIndicator()).toBe("cost-center-icon-green");
        });

        it("should return CSS class 'cost-center-icon-yellow' if the cost center has 85% budget spent", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = 15.00;
            testCostCenterData.BudgetSpentPercent = 85;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.financialHealthIndicator()).toBe("cost-center-icon-yellow");
        });

        it("should return CSS class 'cost-center-icon-yellow' if the cost center has 85-100% budget spent", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = 8.00;
            testCostCenterData.BudgetSpentPercent = 92;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.financialHealthIndicator()).toBe("cost-center-icon-yellow");
        });

        it("should return CSS class 'cost-center-icon-yellow' if the cost center has 100% budget spent", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = 0;
            testCostCenterData.BudgetSpentPercent = 100;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.financialHealthIndicator()).toBe("cost-center-icon-yellow");
        });

        it("should return CSS class 'cost-center-icon-red' if the cost center has spent more than 100% budget", function () {
            testCostCenterData = costCenterRepository.getCostCenter().CostCenter;
            testCostCenterData.BudgetRemaining = -1;
            testCostCenterData.BudgetSpentPercent = 101;

            testCostCenterModel = new costCenterModel(testCostCenterData);
            expect(testCostCenterModel.financialHealthIndicator()).toBe("cost-center-icon-red");
        });
    });
});
