﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

function testCostCenterRepository() {
    var singleCostCenter = {
        "CostCenterId": "10000133333",
        "TotalBudget": "$1000.00",
        "TotalExpenses": "200.00",
        "TotalExpensesFormatted": "$200.00",
        "BudgetRemaining": 800.00,
        "BudgetRemainingRevenue": 10.00,
        "BudgetSpentPercent": 20,
        "BudgetSpentPercentRevenue": 30,
        "Overage": 0,
        "TodaysFiscalYear": "2016",
        "HasExpense": true,
        "HasRevenue": true,
        "CostCenterRevenueSubtotals": [
            {
                "TotalBudget": "$1000.00",
                "TotalExpenses": "200.00",
                "TotalExpensesFormatted": "$200.00",
                "BudgetRemaining": 800.00,
                "BudgetSpentPercent": 20,
                "Overage": 0,
                "SortedGlAccounts": [
                    {
                        "IsPooledAccount": true,
                        "Id": "1",
                        "PooledAccount": {
                            "IsUmbrellaVisible": true,
                            "Umbrella": {
                                "TotalBudget": "$1000.00",
                                "TotalExpenses": "200.00",
                                "TotalExpensesFormatted": "$200.00",
                                "BudgetRemaining": 800.00,
                                "BudgetRemainingFormatted": "$800.00",
                                "BudgetSpentPercent": 20,
                                "ListViewBudgetSpentPercent": "20 %",
                                "Overage": 0,
                            },
                            "Poolees": [
                                {
                                    "TotalBudget": "$1000.00",
                                    "TotalExpenses": "200.00",
                                    "TotalExpensesFormatted": "$200.00",
                                    "BudgetRemaining": 800.00,
                                    "BudgetRemainingFormatted": "$800.00",
                                    "BudgetSpentPercent": 20,
                                    "ListViewBudgetSpentPercent": "20 %",
                                    "Overage": 0,
                                }, {
                                    "TotalBudget": "$1000.00",
                                    "TotalExpenses": "200.00",
                                    "TotalExpensesFormatted": "$200.00",
                                    "BudgetRemaining": 800.00,
                                    "BudgetRemainingFormatted": "$800.00",
                                    "BudgetSpentPercent": 20,
                                    "ListViewBudgetSpentPercent": "20 %",
                                    "Overage": 0,
                                }, {
                                    "TotalBudget": "$1000.00",
                                    "TotalExpenses": "200.00",
                                    "TotalExpensesFormatted": "$200.00",
                                    "BudgetRemaining": 800.00,
                                    "BudgetRemainingFormatted": "$800.00",
                                    "BudgetSpentPercent": 20,
                                    "ListViewBudgetSpentPercent": "20 %",
                                    "Overage": 0,
                                },
                            ]
                        }
                    },
                    {
                        "IsPooledAccount": false,
                        "Id": "1",
                        "NonPooledAccount": {
                            "TotalBudget": "$1000.00",
                            "TotalExpenses": "200.00",
                            "TotalExpensesFormatted": "$200.00",
                            "BudgetRemaining": 800.00,
                            "BudgetSpentPercent": 20,
                            "Overage": 0,
                        },
                    }
                ],
            },
        ],
        "CostCenterExpenseSubtotals": [
            {
                "TotalBudget": "$1000.00",
                "TotalExpenses": "200.00",
                "TotalExpensesFormatted": "$200.00",
                "BudgetRemaining": 800.00,
                "BudgetSpentPercent": 20,
                "Overage": 0,
                "SortedGlAccounts": [
                    {
                        "IsPooledAccount": true,
                        "Id": "1",
                        "PooledAccount": {
                            "IsUmbrellaVisible": true,
                            "Umbrella": {
                                "TotalBudget": "$1000.00",
                                "TotalExpenses": "200.00",
                                "TotalExpensesFormatted": "$200.00",
                                "BudgetRemaining": 800.00,
                                "BudgetRemainingFormatted": "$800.00",
                                "BudgetSpentPercent": 20,
                                "ListViewBudgetSpentPercent": "20 %",
                                "Overage": 0,
                            },
                            "Poolees": [
                                {
                                    "TotalBudget": "$1000.00",
                                    "TotalExpenses": "200.00",
                                    "TotalExpensesFormatted": "$200.00",
                                    "BudgetRemaining": 800.00,
                                    "BudgetRemainingFormatted": "$800.00",
                                    "BudgetSpentPercent": 20,
                                    "ListViewBudgetSpentPercent": "20 %",
                                    "Overage": 0,
                                }, {
                                    "TotalBudget": "$1000.00",
                                    "TotalExpenses": "200.00",
                                    "TotalExpensesFormatted": "$200.00",
                                    "BudgetRemaining": 800.00,
                                    "BudgetRemainingFormatted": "$800.00",
                                    "BudgetSpentPercent": 20,
                                    "ListViewBudgetSpentPercent": "20 %",
                                    "Overage": 0,
                                }, {
                                    "TotalBudget": "$1000.00",
                                    "TotalExpenses": "200.00",
                                    "TotalExpensesFormatted": "$200.00",
                                    "BudgetRemaining": 800.00,
                                    "BudgetRemainingFormatted": "$800.00",
                                    "BudgetSpentPercent": 20,
                                    "ListViewBudgetSpentPercent": "20 %",
                                    "Overage": 0,
                                },
                            ]
                        }
                    },
                    {
                        "IsPooledAccount": false,
                        "Id": "1",
                        "NonPooledAccount": {
                            "TotalBudget": "$1000.00",
                            "TotalExpenses": "200.00",
                            "TotalExpensesFormatted": "$200.00",
                            "BudgetRemaining": 800.00,
                            "BudgetSpentPercent": 20,
                            "Overage": 0,
                        },
                    }
                ],
            },
        ],
    };

    var fiscalYearsData = [
        "2015",
        "2016",
        "2017"
    ];

    var todaysFiscalYear = "2016";

    var costCenterData = {
        "CostCenter": singleCostCenter,
        "FiscalYears": fiscalYearsData,
        "TodaysFiscalYear": todaysFiscalYear,
    };

    var costCentersData = {
        "CostCenters": [
            singleCostCenter,
            {
                "TotalBudget": "$1000.00",
                "TotalExpenses": "$200.00",
                "BudgetRemaining": 800.00,
                "BudgetSpentPercent": 20,
                "Overage": 0,
                "GlAccounts": [
                    {
                    }
                ],
            },
        ],
        "FiscalYears": fiscalYearsData,
        "TodaysFiscalYear": todaysFiscalYear,
        "HasAnyExpense": true,
        "HasAnyRevenue": true,
        "SavedFilterNames": [

        ]
    };

    this.getCostCenter = function () {
        return costCenterData;
    }

    this.getCostCenters = function () {
        return costCentersData;
    }
}