﻿// Copyright 2016 Ellucian Company L.P. and its affiliates

var filterRangeOrderErrorMessage = "range order error",
    filterInvalidRangeMessage = "invalid range",
    majorComponentLengthShortValidationErrorMessage = "too short",
    majorComponentLengthValidationErrorMessage = "too long"

describe("generalLedgerComponentModel:", function () {
    var viewModel,
        contentType;

    beforeEach(function () {
        viewModel = new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0});
        contentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    it("initial (or default) values are correct", function () {
        expect(viewModel.resetFilterCriteria().length).toEqual(0);
        expect(viewModel.tokenizedFilterCriteria().length).toEqual(0);
    });

    it("validateFilterCriteria calls the validation sub-functions", function () {
        spyOn(viewModel, "validateLength").and.callFake(function (a) {
            return true;
        });
        spyOn(viewModel, "validateRangeOrder").and.callFake(function (a) {
            return true;
        });

        viewModel.validateFilterCriteria()
        expect(viewModel.validateLength).toHaveBeenCalled();
        expect(viewModel.validateRangeOrder).toHaveBeenCalled();
    });

    it("criteria is too long", function () {
        var val = viewModel.validateLength("123", ko.observableArray([]));
        expect(val).toBeFalsy();
    });

    it("criteria is too short", function () {
        var val = viewModel.validateLength("1", ko.observableArray([]));
        expect(val).toBeFalsy();
    });

    it("criteria is out of order", function () {
        var val = viewModel.validateRangeOrder("99-1", ko.observableArray([]));
        expect(val).toBeFalsy();
    });

    it("criteria has too many parts", function () {
        var val = viewModel.validateRangeOrder("01-02-03", ko.observableArray([]));
        expect(val).toBeFalsy();
    });
});