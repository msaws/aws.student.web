﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

describe("documentLineItemModel", function () {
    var documentRepository,
        journalEntryData,
        lineItemData,
        lineItemModel;

    beforeEach(function () {
        documentRepository = new testDocumentRepository();
    });

    afterEach(function () {
        documentRepository = null;
        journalEntryData = null;
        lineItemData = null;
        lineItemModel;
    })

    it(": itemAmountColor should return a red class if there is a credit amount", function () {
        journalEntryData = documentRepository.buildJournalEntry();
        lineItemData = journalEntryData.LineItems[0];
        lineItemModel = new documentLineItemModel(lineItemData);
        expect(lineItemModel.itemAmountColor()).toBe("itemAmount-red");
    });

    describe("itemAmount", function () {
        it("should return the credit display if there is a credit amount", function () {
            journalEntryData = documentRepository.buildJournalEntry();
            lineItemData = journalEntryData.LineItems[0];
            lineItemModel = new documentLineItemModel(lineItemData);
            expect(lineItemModel.itemAmount()).toBe(lineItemData.CreditDisplay);
        });

        it("should return the debit display if there is a debit amount", function () {
            journalEntryData = documentRepository.buildJournalEntry();
            lineItemData = journalEntryData.LineItems[1];
            lineItemModel = new documentLineItemModel(lineItemData);
            expect(lineItemModel.itemAmount()).toBe(lineItemData.DebitDisplay);
        });

        it("should return zero if there is neither a credit nor debit amount", function () {
            journalEntryData = documentRepository.buildJournalEntry();
            lineItemData = journalEntryData.LineItems[2];
            lineItemModel = new documentLineItemModel(lineItemData);
            expect(lineItemModel.itemAmount()).toBe("0");
        });
    });
});