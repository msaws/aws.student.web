﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
var Ellucian.ColleagueFinance.getPurchaseOrderUrl = "http://www.ellucian.com/purchase-order",
    Ellucian.ColleagueFinance.getBlanketPurchaseOrderUrl = "http://www.ellucian.com/blanket-purchase-order",
    Ellucian.ColleagueFinance.getRecurringVoucherUrl = "http://www.ellucian.com/recurring-voucher",
    Ellucian.ColleagueFinance.getVoucherUrl = "http://www.ellucian.com/voucher",
    Ellucian.ColleagueFinance.getRequisitionUrl = "http://www.ellucian.com/requisition",
    glDescription = "gl description",
    projectId = "29",
    glNumber = "11-00-01-02-20601-51000",
    lineItemCode = "AJK2",
    lineItemDescription = "Andy's second line item",
    backToText = "Back to ",
    lineItemsLabelText = "Line Items",
    jeItemsLabelText = "JE Items";

describe("documentModel", function () {
    var documentRepository,
        repoVoucher, voucherModel,
        repoPurchaseOrder, purchaseOrderModel,
        repoRequisition, requisitionModel,
        repoJournalEntry, journalEntryModel,
        repoBpo, bpoModel,
        repoRcv, rcvModel,
            docModel;

    beforeEach(function () {
        documentRepository = new testDocumentRepository();
        repoVoucher = documentRepository.buildVoucher();
        repoPurchaseOrder = documentRepository.buildPurchaseOrder();
        repoRequisition = documentRepository.buildRequisition();
        repoJournalEntry = documentRepository.buildJournalEntry();
        repoBpo = documentRepository.buildBlanketPurchaseOrder();
        repoRcv = documentRepository.buildRecurringVoucher();
    });

    afterEach(function () {
        documentRepository = null;
        repoVoucher = null;
        repoPurchaseOrder = null;
        repoRequisition = null;
        repoJournalEntry = null;
        repoBpo = null;
        repoRcv = null;
        docModel = null;
    })
    
    describe("DocumentItems", function () {
        it("for a voucher should be a list of line items", function () {
            voucherModel = new documentModel(repoVoucher);
            expect(voucherModel.DocumentItems().length).toBe(repoVoucher.LineItems.length);
        });

        it("for a purchase order, should be a list of line items", function () {
            purchaseOrderModel = new documentModel(repoPurchaseOrder);
            expect(purchaseOrderModel.DocumentItems().length).toBe(repoPurchaseOrder.LineItems.length);
        });

        it("for a requisition, should be a list of line items", function () {
            requisitionModel = new documentModel(repoRequisition);
            expect(requisitionModel.DocumentItems().length).toBe(repoRequisition.LineItems.length);
        });

        it("for a journal entry, should be a list of line items", function () {
            journalEntryModel = new documentModel(repoJournalEntry);
            expect(journalEntryModel.DocumentItems().length).toBe(repoJournalEntry.LineItems.length);
        });

        it("for a blanket purchase order, should be a list of GL distributions", function () {
            bpoModel = new documentModel(repoBpo);
            expect(bpoModel.DocumentItems().length).toBe(repoBpo.GlDistributions.length);
        });

        it("for a recurring voucher, should be a list of schedules", function () {
            rcvModel = new documentModel(repoRcv);
            expect(rcvModel.DocumentItems().length).toBe(repoRcv.Schedules.length);
        });
    });

    describe("AssociatedDocumentLink", function () {
        it("should (for a voucher) generate a URL for an associated PO", function () {
            // Set the appropriate data for the associated document
            repoVoucher.IsAssociatedDocumentPurchaseOrder = true;
            repoVoucher.IsAssociatedDocumentBlanketPurchaseOrder = false;
            repoVoucher.IsAssociatedDocumentRecurringVoucher = false;
            repoVoucher.AssociatedDocument = "00000124";

            var expectedUrl = Ellucian.ColleagueFinance.getPurchaseOrderUrl + '/' + repoVoucher.AssociatedDocument + '?' +
                encodeURIComponent(glDescription) + '&' + projectId + '&' + encodeURIComponent(glNumber) + '&' +
                encodeURIComponent(lineItemCode) + '&' + encodeURIComponent(lineItemDescription);

            voucherModel = new documentModel(repoVoucher);
            expect(voucherModel.AssociatedDocumentLink()).toBe(expectedUrl);
        });

        it("should (for a voucher) generate a URL for an associated BPO", function () {
            // Set the appropriate data for the associated document
            repoVoucher.IsAssociatedDocumentPurchaseOrder = false;
            repoVoucher.IsAssociatedDocumentBlanketPurchaseOrder = true;
            repoVoucher.IsAssociatedDocumentRecurringVoucher = false;
            repoVoucher.AssociatedDocument = "00000345";

            var expectedUrl = Ellucian.ColleagueFinance.getBlanketPurchaseOrderUrl + '/' + repoVoucher.AssociatedDocument + '?' +
                encodeURIComponent(glDescription) + '&' + projectId + '&' + encodeURIComponent(glNumber) + '&' +
                encodeURIComponent(lineItemCode) + '&' + encodeURIComponent(lineItemDescription);

            voucherModel = new documentModel(repoVoucher);
            expect(voucherModel.AssociatedDocumentLink()).toBe(expectedUrl);
        });

        it("should (for a voucher) generate a URL for an associated RCV", function () {
            // Set the appropriate data for the associated document
            repoVoucher.IsAssociatedDocumentPurchaseOrder = false;
            repoVoucher.IsAssociatedDocumentBlanketPurchaseOrder = false;
            repoVoucher.IsAssociatedDocumentRecurringVoucher = true;
            repoVoucher.AssociatedDocument = "00000678";

            var expectedUrl = Ellucian.ColleagueFinance.getRecurringVoucherUrl + '/' + repoVoucher.AssociatedDocument + '?' +
                encodeURIComponent(glDescription) + '&' + projectId + '&' + encodeURIComponent(glNumber) + '&' +
                encodeURIComponent(lineItemCode) + '&' + encodeURIComponent(lineItemDescription);

            voucherModel = new documentModel(repoVoucher);
            expect(voucherModel.AssociatedDocumentLink()).toBe(expectedUrl);
        });
    });

    describe("documentItemType", function () {
        it("returns 'Line Items' when document is a Voucher", function () {
            docModel = new documentModel(repoVoucher);
            expect(docModel.documentItemType()).toBe(backToText + lineItemsLabelText);
        });

        it("returns 'Line Items' when document is a Requisition", function () {
            docModel = new documentModel(repoRequisition);
            expect(docModel.documentItemType()).toBe(backToText + lineItemsLabelText);
        });

        it("returns 'Line Items' when document is a Purchase Order", function () {
            docModel = new documentModel(repoPurchaseOrder);
            expect(docModel.documentItemType()).toBe(backToText + lineItemsLabelText);
        });

        it("returns 'JE Items' when document is a Journal Entry", function () {
            docModel = new documentModel(repoJournalEntry);
            expect(docModel.documentItemType()).toBe(backToText + jeItemsLabelText);
        });
    });

    it("should generate a URL for an associated voucher", function () {
        var recordId = "0000001";
        var expectedUrl = Ellucian.ColleagueFinance.getVoucherUrl + '/' + recordId + '/' + glNumber;

        voucherModel = new documentModel(repoVoucher);
        expect(voucherModel.AssociatedVoucherLink(recordId)).toBe(expectedUrl);
    });

    it("should generate a URL for an associated recurring voucher", function () {
        var recordId = "0000001";
        var expectedUrl = Ellucian.ColleagueFinance.getRecurringVoucherUrl + '/' + recordId + '/' + glNumber;

        voucherModel = new documentModel(repoVoucher);
        expect(voucherModel.AssociatedRecurringVoucherLink(recordId)).toBe(expectedUrl);
    });

    it("should generate a URL for an associated purchase order", function () {
        var recordId = "0000001";
        var expectedUrl = Ellucian.ColleagueFinance.getPurchaseOrderUrl + '/' + recordId + '/' + glNumber;

        voucherModel = new documentModel(repoVoucher);
        expect(voucherModel.AssociatedPurchaseOrderLink(recordId)).toBe(expectedUrl);
    });

    it("should generate a URL for an associated requisition", function () {
        var recordId = "0000001";
        var expectedUrl = Ellucian.ColleagueFinance.getRequisitionUrl + '/' + recordId + '/' + glNumber;

        voucherModel = new documentModel(repoVoucher);
        expect(voucherModel.AssociatedRequisitionLink(recordId)).toBe(expectedUrl);
    });

    it("should generate a URL for an associated blanket purchase order", function () {
        var recordId = "0000001";
        var expectedUrl = Ellucian.ColleagueFinance.getBlanketPurchaseOrderUrl + '/' + recordId + '/' + glNumber;

        voucherModel = new documentModel(repoVoucher);
        expect(voucherModel.AssociatedBlanketPurchaseOrderLink(recordId)).toBe(expectedUrl);
    });
});