﻿describe("documentApproverModel", function () {
    var viewModel,
        approver;
    beforeEach(function () {
        approver = {
            "Name": "Andy Kleehammer",
            "Date": "12/31/2014",
            "DateOrWaiting": "12/31/2014",
        };
    });

    it("should return the 'waiting' CSS class", function () {
        approver.Date = "";
        approverModel = new documentApproverModel(approver);
        expect(approverModel.approvalStatusIcon()).toBe("document-waiting-icon");
    });

    it("should return the 'approved' CSS class", function () {
        approverModel = new documentApproverModel(approver);
        expect(approverModel.approvalStatusIcon()).toBe("document-approved-icon");
    });

    it("should return the 'awaiting-approval' CSS class if no date", function () {
        approver.Date = "";
        approverModel = new documentApproverModel(approver);
        expect(approverModel.awaitingApproval()).toBe("awaiting-approval");
    });

    it("should return the blank if there is an approval date", function () {
        approverModel = new documentApproverModel(approver);
        expect(approverModel.awaitingApproval()).toBe("");
    });
});