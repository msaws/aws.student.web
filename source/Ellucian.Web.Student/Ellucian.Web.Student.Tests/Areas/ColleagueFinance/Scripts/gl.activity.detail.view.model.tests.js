﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates

var getGlTransactionsAsync = "http://localhost/CostCenters/GlDetails",
    currentFiscalYearDescription = "Current Year",
    noGlTransactionsToDisplayText = "No GL activity to display.",
    unableToLoadGlActivityText = "Unable to load GL activity.",
    resourceNotFoundText = "Resource not found.",
    glAccount = "11_22_33_44444_55555";

describe("glActivityDetailViewModel", function () {
    var viewModel,
        contentType;

    beforeEach(function () {
        viewModel = new glActivityDetailViewModel();
        contentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    describe("getGlTransactions", function () {
        it("ajax is invoked with expected url", function () {

            var expectedUrl = getGlTransactionsAsync + "?glAccount=" + glAccount + "&fiscalYear=" + viewModel.SelectedFiscalYear();
            spyOn($, "ajax");
            viewModel.getGlTransactions(glAccount);
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            viewModel.getGlTransactions(glAccount);
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            spyOn($, "ajax");
            viewModel.getGlTransactions(glAccount);
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("should display the filter before sending the request", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            viewModel.IsPageLoaded(true);
            viewModel.getGlTransactions(glAccount);
            expect(viewModel.IsGlActivityLoading()).toBeTruthy();
        });

        it("should issue the default notification if an error is returned", function () {
            var jqXhr = {
                status: 400
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.error(jqXhr, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getGlTransactions(glAccount);
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadGlActivityText, type: "error" });
        });

        it("should issue a 'resource not found' notification if a 404 error is returned", function () {
            var jqXhr = {
                status: 404
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.error(jqXhr, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getGlTransactions(glAccount);
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("success should set Fiscal Years, Selected Fiscal Year, and GlAccountActivity", function () {
            var returnedData = {
                "GlAccountActivityDetail": {
                    "EncumbranceTransactions": [
                    {
                        "ReferenceNumber": "1",
                        "Date": "01/01/2010",
                        "Description": "Some description would go here",
                        "Amount": "$1,000,000.00"
                    }
                    ],
                    "ActualTransactions": [
                        {
                            "ReferenceNumber": "2",
                            "Date": "01/01/2010",
                            "Description": "Some other description would go here",
                            "Amount": "$100,000,000.00"
                        }
                    ],
                    "BudgetTransactions": [
                        {
                            "ReferenceNumber": "3",
                            "Date": "01/01/2010",
                            "Description": "Some other, OTHER description would go here",
                            "Amount": "$1,000,000,000.00"
                        }
                    ],
                    "TotalActualsMemos": 0,
                    "TotalBudgetMemos": 0
                },
                "FiscalYears": [{
                    "FiscalYear": "2017",
                    "FiscalYearDescription": "FY2017"
                }],
                "TodaysFiscalYear": "2017"
            };

            viewModel.SelectedFiscalYear("");

            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedData);
            });

            viewModel.getGlTransactions(glAccount);

            expect(viewModel.SelectedFiscalYear()).toBe(returnedData.TodaysFiscalYear);

            expect(viewModel.FiscalYears()[0].FiscalYear()).toBe(returnedData.FiscalYears[0].FiscalYear);
            expect(viewModel.FiscalYears()[0].FiscalYearDescription()).toBe(returnedData.FiscalYears[0].FiscalYearDescription);

            var actual = viewModel.GlAccountActivity();

            expect(actual.EncumbranceTransactions()[0].ReferenceNumber()).toBe(returnedData.GlAccountActivityDetail.EncumbranceTransactions[0].ReferenceNumber);
            expect(actual.EncumbranceTransactions()[0].Date()).toBe(returnedData.GlAccountActivityDetail.EncumbranceTransactions[0].Date);
            expect(actual.EncumbranceTransactions()[0].Description()).toBe(returnedData.GlAccountActivityDetail.EncumbranceTransactions[0].Description);
            expect(actual.EncumbranceTransactions()[0].Amount()).toBe(returnedData.GlAccountActivityDetail.EncumbranceTransactions[0].Amount);

            expect(actual.ActualTransactions()[0].ReferenceNumber()).toBe(returnedData.GlAccountActivityDetail.ActualTransactions[0].ReferenceNumber);
            expect(actual.ActualTransactions()[0].Date()).toBe(returnedData.GlAccountActivityDetail.ActualTransactions[0].Date);
            expect(actual.ActualTransactions()[0].Description()).toBe(returnedData.GlAccountActivityDetail.ActualTransactions[0].Description);
            expect(actual.ActualTransactions()[0].Amount()).toBe(returnedData.GlAccountActivityDetail.ActualTransactions[0].Amount);

            expect(actual.BudgetTransactions()[0].ReferenceNumber()).toBe(returnedData.GlAccountActivityDetail.BudgetTransactions[0].ReferenceNumber);
            expect(actual.BudgetTransactions()[0].Date()).toBe(returnedData.GlAccountActivityDetail.BudgetTransactions[0].Date);
            expect(actual.BudgetTransactions()[0].Description()).toBe(returnedData.GlAccountActivityDetail.BudgetTransactions[0].Description);
            expect(actual.BudgetTransactions()[0].Amount()).toBe(returnedData.GlAccountActivityDetail.BudgetTransactions[0].Amount);
        });

        it("should update the view model upon completion", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getGlTransactions(glAccount);

            // Check that the observables were correctly set
            expect(viewModel.IsPageLoaded()).toBeTruthy();
            expect(viewModel.IsGlActivityLoading()).toBeFalsy();
            expect(viewModel.PopulateFiscalYearDropDown()).toBeFalsy();
        });
    });

    describe("pickFiscalYear", function () {
        it("should execute the getGlTransactions function if the page has been loaded and the filter is not updating", function () {
            viewModel.IsPageLoaded(true);
            viewModel.IsGlActivityLoading(false);
            spyOn(viewModel, "getGlTransactions");

            viewModel.pickFiscalYear();
            expect(viewModel.getGlTransactions).toHaveBeenCalled();
        });

        it("should not execute the getGlTransactions function if the page is still being loaded", function () {
            viewModel.IsPageLoaded(false);
            spyOn(viewModel, "getGlTransactions");

            viewModel.pickFiscalYear();
            expect(viewModel.getGlTransactions).not.toHaveBeenCalled();
        });

        it("should not execute the getGlTransactions function if the filter is updating", function () {
            viewModel.IsGlActivityLoading(true);
            spyOn(viewModel, "getGlTransactions");

            viewModel.pickFiscalYear();
            expect(viewModel.getGlTransactions).not.toHaveBeenCalled();
        });
    });

    describe("mobileTabs", function () {
        it("should set GlDetailTabSelected to E when clickEncumbrancesTab is called", function () {
            viewModel.clickEncumbrancesTab();

            expect(viewModel.GlDetailTabSelected()).toEqual("E");
        });

        it("should set GlDetailTabSelected to A when clickActualsTab is called", function () {
            viewModel.clickActualsTab();

            expect(viewModel.GlDetailTabSelected()).toEqual("A");
        });

        it("should set GlDetailTabSelected to B when clickBudgetTab is called", function () {
            viewModel.clickBudgetTab();

            expect(viewModel.GlDetailTabSelected()).toEqual("B");
        });

        it("should evaluate the tabs based on a memory value of E", function () {
            memory.setItem("gl-detail-tab", "E");
            viewModel.evaluateTabs();

            expect(viewModel.GlDetailTabSelected()).toEqual("E");
        });

        it("should evaluate the tabs based on a memory value of A", function () {
            memory.setItem("gl-detail-tab", "A");
            viewModel.evaluateTabs();

            expect(viewModel.GlDetailTabSelected()).toEqual("A");
        });

        it("should evaluate the tabs based on a memory value of B", function () {
            memory.setItem("gl-detail-tab", "B");
            viewModel.evaluateTabs();

            expect(viewModel.GlDetailTabSelected()).toEqual("B");
        });
    });

    describe("glActivityExportData", function () {
        it("should contain any transactions that GlAccountActivity has", function () {
            var returnedData = ko.mapping.fromJS({
                "EncumbranceTransactions": [
                {
                    "ReferenceNumber": "1",
                    "Date": "01/01/2010",
                    "Description": "Some description would go here",
                    "Amount": "$1,000,000.00"
                }
                ],
                "ActualTransactions": [
                    {
                        "ReferenceNumber": "2",
                        "Date": "01/01/2010",
                        "Description": "Some other description would go here",
                        "Amount": "$100,000,000.00"
                    }
                ],
                "BudgetTransactions": [
                    {
                        "ReferenceNumber": "3",
                        "Date": "01/01/2010",
                        "Description": "Some other, OTHER description would go here",
                        "Amount": "$1,000,000,000.00"
                    }
                ],
                "TotalActualsMemos": 0,
                "TotalBudgetMemos": 0,
                "EstimatedOpeningBalance": 0,
                "ClosingYearAmount": 0

            }, glActivityDetailMapping);

            viewModel.GlAccountActivity(returnedData);

            var actual = viewModel.glActivityExportData();

            expect(actual.length).toBe(3);

            expect(actual[0].Type).toBe(Ellucian.ColleagueFinance.encumbrancesHeaderLabel);
            expect(actual[0].Document).toBe("'" + returnedData.EncumbranceTransactions()[0].ReferenceNumber() + "'");
            expect(actual[0].Date).toBe(returnedData.EncumbranceTransactions()[0].Date());
            expect(actual[0].Description).toBe(returnedData.EncumbranceTransactions()[0].Description());
            expect(actual[0].Amount).toBe(returnedData.EncumbranceTransactions()[0].Amount());

            expect(actual[1].Type).toBe(Ellucian.ColleagueFinance.actualsHeaderLabel);
            expect(actual[1].Document).toBe("'" + returnedData.ActualTransactions()[0].ReferenceNumber() + "'");
            expect(actual[1].Date).toBe(returnedData.ActualTransactions()[0].Date());
            expect(actual[1].Description).toBe(returnedData.ActualTransactions()[0].Description());
            expect(actual[1].Amount).toBe(returnedData.ActualTransactions()[0].Amount());

            expect(actual[2].Type).toBe(Ellucian.ColleagueFinance.budgetHeaderLabel);
            expect(actual[2].Document).toBe("'" + returnedData.BudgetTransactions()[0].ReferenceNumber() + "'");
            expect(actual[2].Date).toBe(returnedData.BudgetTransactions()[0].Date());
            expect(actual[2].Description).toBe(returnedData.BudgetTransactions()[0].Description());
            expect(actual[2].Amount).toBe(returnedData.BudgetTransactions()[0].Amount());
        });

        it("should contain (positive) amounts pending posting and estimated opening/closing balances if GlAccountActivity has any", function () {
            var returnedData = ko.mapping.fromJS({
                "EncumbranceTransactions": [],
                "ActualTransactions": [],
                "BudgetTransactions": [],
                "TotalActualsMemos": 100,
                "TotalActualsMemosDisplay": "$100.00",
                "TotalBudgetMemos": 200,
                "TotalBudgetMemosDisplay": "$200.00",
                "EstimatedOpeningBalance": 100,
                "EstimatedOpeningBalanceDisplay": "$100.00",
                "ClosingYearAmount": 100,
                "ClosingYearAmountDisplay": "$100.00"

            }, glActivityDetailMapping);

            viewModel.GlAccountActivity(returnedData);

            var actual = viewModel.glActivityExportData();

            expect(actual.length).toBe(4);

            expect(actual[0].Type).toBe(Ellucian.ColleagueFinance.actualsHeaderLabel);
            expect(actual[0].Document).toBe("");
            expect(actual[0].Date).toBe("");
            expect(actual[0].Description).toBe(Ellucian.ColleagueFinance.estimatedOpeningBalanceText);
            expect(actual[0].Amount).toBe(returnedData.EstimatedOpeningBalanceDisplay());

            expect(actual[1].Type).toBe(Ellucian.ColleagueFinance.actualsHeaderLabel);
            expect(actual[1].Document).toBe("");
            expect(actual[1].Date).toBe("");
            expect(actual[1].Description).toBe(Ellucian.ColleagueFinance.closingYearAmountText);
            expect(actual[1].Amount).toBe(returnedData.ClosingYearAmountDisplay());

            expect(actual[2].Type).toBe(Ellucian.ColleagueFinance.actualsHeaderLabel);
            expect(actual[2].Document).toBe("");
            expect(actual[2].Date).toBe("");
            expect(actual[2].Description).toBe(Ellucian.ColleagueFinance.amountPendingPostingText);
            expect(actual[2].Amount).toBe(returnedData.TotalActualsMemosDisplay());

            expect(actual[3].Type).toBe(Ellucian.ColleagueFinance.budgetHeaderLabel);
            expect(actual[3].Document).toBe("");
            expect(actual[3].Date).toBe("");
            expect(actual[3].Description).toBe(Ellucian.ColleagueFinance.amountPendingPostingText);
            expect(actual[3].Amount).toBe(returnedData.TotalBudgetMemosDisplay());
        });

        it("should contain (negative) amounts pending posting and estimated opening/closing balances if GlAccountActivity has any", function () {
            var returnedData = ko.mapping.fromJS({
                "EncumbranceTransactions": [],
                "ActualTransactions": [],
                "BudgetTransactions": [],
                "TotalActualsMemos": -0.01,
                "TotalActualsMemosDisplay": "-$0.01",
                "TotalBudgetMemos": -200,
                "TotalBudgetMemosDisplay": "-$200.00",
                "EstimatedOpeningBalance": -100,
                "EstimatedOpeningBalanceDisplay": "-$100.00",
                "ClosingYearAmount": -100,
                "ClosingYearAmountDisplay": "-$100.00"

            }, glActivityDetailMapping);

            viewModel.GlAccountActivity(returnedData);

            var actual = viewModel.glActivityExportData();

            expect(actual.length).toBe(4);

            expect(actual[0].Type).toBe(Ellucian.ColleagueFinance.actualsHeaderLabel);
            expect(actual[0].Document).toBe("");
            expect(actual[0].Date).toBe("");
            expect(actual[0].Description).toBe(Ellucian.ColleagueFinance.estimatedOpeningBalanceText);
            expect(actual[0].Amount).toBe(returnedData.EstimatedOpeningBalanceDisplay());

            expect(actual[1].Type).toBe(Ellucian.ColleagueFinance.actualsHeaderLabel);
            expect(actual[1].Document).toBe("");
            expect(actual[1].Date).toBe("");
            expect(actual[1].Description).toBe(Ellucian.ColleagueFinance.closingYearAmountText);
            expect(actual[1].Amount).toBe(returnedData.ClosingYearAmountDisplay());

            expect(actual[2].Type).toBe(Ellucian.ColleagueFinance.actualsHeaderLabel);
            expect(actual[2].Document).toBe("");
            expect(actual[2].Date).toBe("");
            expect(actual[2].Description).toBe(Ellucian.ColleagueFinance.amountPendingPostingText);
            expect(actual[2].Amount).toBe(returnedData.TotalActualsMemosDisplay());

            expect(actual[3].Type).toBe(Ellucian.ColleagueFinance.budgetHeaderLabel);
            expect(actual[3].Document).toBe("");
            expect(actual[3].Date).toBe("");
            expect(actual[3].Description).toBe(Ellucian.ColleagueFinance.amountPendingPostingText);
            expect(actual[3].Amount).toBe(returnedData.TotalBudgetMemosDisplay());
        });
    });

    describe("toggleExportDropdown", function () {
        it("should change isExportDropdownExpanded from false to true", function () {
            viewModel.isExportDropdownExpanded(false);

            viewModel.toggleExportDropdown();

            expect(viewModel.isExportDropdownExpanded()).toBeTruthy();
            expect(viewModel.exportDropdownCss()).toBe("esg-is-open css-is-open");
        });

        it("should change isExportDropdownExpanded from true to false", function () {
            viewModel.isExportDropdownExpanded(true);

            viewModel.toggleExportDropdown();

            expect(viewModel.isExportDropdownExpanded()).toBeFalsy();
            expect(viewModel.exportDropdownCss()).toBe("");
        });
    });
});