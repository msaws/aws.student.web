﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

var budgetBarText = " of ",
    noCostCentersToDisplayText = "No cost centers to display.";

describe("costCenterGlAccountModel", function () {
    var model;
    var data;
    
    
    beforeEach(function () {
        data = {
            "TotalBudget": "$10,000",
            "TotalExpensesFormatted": "$7,500",
            "BudgetRemaining": 2500,
            "Overage": 0,
            "BudgetSpentPercent": 75,
            "ListViewBudgetSpentPercent": "75%",
            "ListViewBudgetSpentPercentRevenue": "75%"
        };
        model = new costCenterGlAccountModel(data);
    });

    describe("nonZeroTotalBudget", function () {
        it("should return blank if the total budget is zero", function () {
            expect(model.nonZeroTotalBudget()).toBe(data.TotalBudget);
        });

        it("should return blank if the total budget is zero", function () {
            data.TotalBudget = "$0.00";
            model = new costCenterGlAccountModel(data);
            expect(model.nonZeroTotalBudget()).toBe("");
        });
    });

    describe("baseCostCenterModel", function () {
        describe("overageColor", function () {
            it("it should return 'zero-budget' if the budget is zero", function () {
                data.TotalBudget = "0";
                model = new costCenterGlAccountModel(data);
                expect(model.overageColor()).toBe("zero-budget");
            });

            it("it should return 'zero-budget overage' if the overage amount is equal to 100", function () {
                data.Overage = 100;
                model = new costCenterGlAccountModel(data);
                expect(model.overageColor()).toBe("zero-budget overage");
            });
        });

        describe("financialHealthIndicator", function () {
            it("it should return 'cost-center-icon-green' if the budget remaining is greater than zero and the spent percent is less than 85%", function () {
                expect(model.financialHealthIndicator()).toBe("cost-center-icon-green");
            });

            it("it should return 'cost-center-icon-red' if the budget remaining is less than 0", function () {
                data.BudgetRemaining = -100;
                model = new costCenterGlAccountModel(data);
                expect(model.financialHealthIndicator()).toBe("cost-center-icon-red");
            });

            it("it should return 'cost-center-icon-yellow' if the budget spent percent is 85", function () {
                data.BudgetSpentPercent = 85;
                model = new costCenterGlAccountModel(data);
                expect(model.financialHealthIndicator()).toBe("cost-center-icon-yellow");
            });

            it("it should return 'cost-center-icon-yellow' if the budget spent percent is in between 85 and 100", function () {
                data.BudgetSpentPercent = 97;
                model = new costCenterGlAccountModel(data);
                expect(model.financialHealthIndicator()).toBe("cost-center-icon-yellow");
            });

            it("it should return 'cost-center-icon-yellow' if the budget spent percent is 100", function () {
                data.BudgetSpentPercent = 100;
                model = new costCenterGlAccountModel(data);
                expect(model.financialHealthIndicator()).toBe("cost-center-icon-yellow");
            });
        });
    });
});