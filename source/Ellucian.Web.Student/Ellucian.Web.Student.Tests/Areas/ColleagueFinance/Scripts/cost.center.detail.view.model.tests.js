﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates

// create a variable that contains the test markup that the unit test
// function will be using.
var tableDom = '<table>';
tableDom += '<tr id="51" class="collapsible"></tr>'
tableDom += '<tr class="collapsible-items-row"></tr>'
tableDom += '</table>';

var memoryStorage = {};

var memory = {
	getItem: function (id) {
		return memoryStorage[id];
	},
	setItem: function (id, val) {
		memoryStorage[id] = val;
	}
}

describe("costCenterDetailViewModel", function () {
	var viewModel;

	beforeAll(function () {
		account = {
			handleInvalidSessionResponse: function (data) { return false; }
		};
	});

	beforeEach(function () {
		viewModel = new costCenterDetailViewModel();
		contentType = "json";
		//set the test markup and set the GL number collapsible item row to collapsed.
		$(document.body).append(tableDom);
		$(".collapsible-items-row").hide();
		memory.setItem("fiscalYear", "2016");
	});

	describe("isCostCenterDefined", function () {
		it("should return false if the cost center observable is 'undefined'", function () {
			expect(viewModel.isCostCenterDefined()).toBeFalsy();
		});

		it("should return false if the cost center observable is null", function () {
			viewModel.CostCenter(null);
			expect(viewModel.isCostCenterDefined()).toBeFalsy();
		});

		it("should return true if the cost center observable is has been defined", function () {
		    viewModel.CostCenter({ CostCenterExpenseSubtotals: ko.observableArray([]), CostCenterRevenueSubtotals: ko.observableArray([]) });
			expect(viewModel.isCostCenterDefined()).toBeTruthy();
		});
	});

	describe("getMajorComponentsThenCostCenter", function () {
		it("ajax is invoked with expected url", function () {
			spyOn($, "ajax");
			viewModel.getMajorComponentsThenCostCenter();
			expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(getMajorComponentsActionUrl);
		});

		it("ajax is invoked with expected dataType", function () {
			spyOn($, "ajax");
			viewModel.getMajorComponentsThenCostCenter();
			expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
		});

		it("ajax is invoked with expected action type", function () {
			spyOn($, "ajax");
			viewModel.getMajorComponentsThenCostCenter();
			expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
		});

		it("should set the filter updating observable to true", function () {
			viewModel.IsPageLoaded(true);
			spyOn($, "ajax").and.callFake(function (e) {
				e.beforeSend({});
			});
			spyOn($.fn, "notificationCenter");
			viewModel.getMajorComponentsThenCostCenter();
			expect($.fn.notificationCenter).toHaveBeenCalledWith('removeNotification', { message: noCostCenterToDisplayText, type: "info" });
		});

		it("success() should populate the MajorComponents observable", function () {
			var returnedMajorComponentData = [new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }), new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })];
			viewModel.IsPageLoaded(true);
			spyOn($, "ajax").and.callFake(function (e) {
				e.success(returnedMajorComponentData);
			});

			expect(viewModel.MajorComponents().length).toBe(0);
			viewModel.getMajorComponentsThenCostCenter();
			expect(viewModel.MajorComponents().length).toBe(returnedMajorComponentData.length);
		});

		it("success() should apply values from browser memory", function () {
			var fundValues = ["11", "12", "13"];
			memory.setItem("Fund", fundValues.join(","));
			var returnedMajorComponentData = [new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }), new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })];
			viewModel.IsPageLoaded(true);
			spyOn($, "ajax").and.callFake(function (e) {
				e.success(returnedMajorComponentData);
			});
			viewModel.getMajorComponentsThenCostCenter();

			expect(viewModel.MajorComponents().length).toBe(returnedMajorComponentData.length);
			expect(viewModel.MajorComponents()[0].tokenizedFilterCriteria().length).toBe(fundValues.length);
			for (var i = 0; i < viewModel.MajorComponents()[0].tokenizedFilterCriteria().length; i++) {
				expect(viewModel.MajorComponents()[0].tokenizedFilterCriteria()[i]).toBe(fundValues[i]);
			}
		});

		it("error() should add a 404 error to the notification center", function () {
			viewModel.IsPageLoaded(true);
			spyOn($, "ajax").and.callFake(function (e) {
				e.error({ status: 404 }, {}, {});
			});
			spyOn($.fn, "notificationCenter");
			viewModel.getMajorComponentsThenCostCenter();

			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
		});

		it("error() should add a cost center load error to the notification center", function () {
			viewModel.IsPageLoaded(true);
			spyOn($, "ajax").and.callFake(function (e) {
				e.error({ status: 500 }, {}, {});
			});
			spyOn($.fn, "notificationCenter");
			viewModel.getMajorComponentsThenCostCenter();

			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadCostCenterText, type: "error" });
		});

		it("complete() should trigger 'getFilteredCostCenter'", function () {
			viewModel.IsPageLoaded(true);
			spyOn($, "ajax").and.callFake(function (e) {
				e.complete();
			});
			spyOn(viewModel, "getFilteredCostCenter");
			viewModel.getMajorComponentsThenCostCenter();

			expect(viewModel.getFilteredCostCenter).toHaveBeenCalled();
		});
	});

	describe("getFilteredCostCenter", function () {
		it("should get the fiscal year from memory if the page has been loaded", function () {
			var testRepo = new testCostCenterRepository();
			viewModel.IsPageLoaded(true);
			var expectedFiscalYear = "2011";
			memory.setItem("fiscalYear", expectedFiscalYear);
			var expectedCostCenterData = testRepo.getCostCenter();
			spyOn($, "ajax").and.callFake(function (e) {
				e.success(expectedCostCenterData);
			});

			viewModel.getFilteredCostCenter();
			expect(viewModel.SelectedFiscalYear()).toBe((expectedCostCenterData.TodaysFiscalYear));
		});

		it("ajax is invoked with expected url", function () {
			spyOn($, "ajax");
			viewModel.getFilteredCostCenter();
			expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(getFilteredCostCenterActionUrl);
		});

		it("ajax is invoked with expected dataType", function () {
			spyOn($, "ajax");
			viewModel.getFilteredCostCenter();
			expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
		});

		it("ajax is invoked with expected action type", function () {
			spyOn($, "ajax");
			viewModel.getFilteredCostCenter();
			expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("POST");
		});

		it("beforeSend() should display the filter before sending the request if the page has already been loaded", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.beforeSend({});
			});
			viewModel.IsPageLoaded(true);
			viewModel.getFilteredCostCenter();
			expect(viewModel.IsFilterUpdating()).toBeTruthy();
		});

		it("beforeSend() should remove any 'no cost center to display' notifications.", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.beforeSend({});
			});
			spyOn($.fn, "notificationCenter");

			viewModel.getFilteredCostCenter();
			expect($.fn.notificationCenter).toHaveBeenCalledWith('removeNotification', { message: noCostCenterToDisplayText, type: "info" });
		});

		it("success() should update cost center data", function () {
			var testRepo = new testCostCenterRepository();
			viewModel.MajorComponents([
				new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }),
				new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })
			]);
			viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-15", "20-30"]);
			var expectedCostCenterData = testRepo.getCostCenter();
			spyOn($, "ajax").and.callFake(function (e) {
				e.success(expectedCostCenterData);
			});

			viewModel.getFilteredCostCenter();
			expect(viewModel.PopulateFiscalYearDropDown()).toBeFalsy();
			expect(viewModel.SelectedFiscalYear()).toBe(expectedCostCenterData.TodaysFiscalYear);
			expect(viewModel.CostCenter().CostCenterId()).toBe(expectedCostCenterData.CostCenter.CostCenterId);
		});

		it("error() should issue the default notification if an error is returned", function () {
			var jqXhr = {
				status: 400
			};
			spyOn($, "ajax").and.callFake(function (e) {
				e.error(jqXhr, {}, {});
			});
			spyOn($.fn, "notificationCenter");
			viewModel.getFilteredCostCenter();
			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadCostCenterText, type: "error" });
		});

		it("error() should issue a 'resource not found' notification if a 404 error is returned", function () {
			var jqXhr = {
				status: 404
			};
			spyOn($, "ajax").and.callFake(function (e) {
				e.error(jqXhr, {}, {});
			});
			spyOn($.fn, "notificationCenter");
			viewModel.getFilteredCostCenter();
			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
		});

		it("should call 'setCostCenterSubtotals' upon completion", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.complete();
			});
			spyOn(viewModel, "setCostCenterSubtotals");

			viewModel.getFilteredCostCenter();
			expect(viewModel.setCostCenterSubtotals).toHaveBeenCalled();
		});

		it("should update the view model upon completion", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.complete();
			});
			spyOn($.fn, "notificationCenter");
			spyOn(viewModel, "setCostCenterSubtotals");
			viewModel.getFilteredCostCenter();

			// Check that the observables were correctly set
			expect(viewModel.IsPageLoaded()).toBeTruthy();
			expect(viewModel.IsFilterUpdating()).toBeFalsy();
			expect(viewModel.PopulateFiscalYearDropDown()).toBeFalsy();
		});

		it("should display a notification is the cost center is not defined", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.complete({});
			});
			spyOn($.fn, "notificationCenter");
			spyOn(viewModel, "setCostCenterSubtotals");
			viewModel.getFilteredCostCenter();

			// A notification should be created if there are no cost centers.
			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: noCostCenterToDisplayText, type: "info" });
		});

		it("should display a notification is the cost center ID is null", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.complete({ "CostCenter": { "CostCenterId": null } });
			});
			spyOn($.fn, "notificationCenter");
			spyOn(viewModel, "setCostCenterSubtotals");
			viewModel.getFilteredCostCenter();

			// A notification should be created if there are no cost centers.
			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: noCostCenterToDisplayText, type: "info" });
		});

		it("should display a notification is the cost center ID is blank", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.complete({ "CostCenter": { "CostCenterId": "" } });
			});
			spyOn($.fn, "notificationCenter");
			spyOn(viewModel, "setCostCenterSubtotals");
			viewModel.getFilteredCostCenter();

			// A notification should be created if there are no cost centers.
			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: noCostCenterToDisplayText, type: "info" });
		});

		it("should display a notification is the cost center ID is 'undefined'", function () {
			spyOn($, "ajax").and.callFake(function (e) {
				e.complete({ "CostCenter": { "CostCenterId": 'undefined' } });
			});
			spyOn($.fn, "notificationCenter");
			spyOn(viewModel, "setCostCenterSubtotals");
			viewModel.getFilteredCostCenter();

			// A notification should be created if there are no cost centers.
			expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: noCostCenterToDisplayText, type: "info" });
		});
	});

	describe("setCostCenterSubtotals", function () {
	    it("should call 'initializeActiveRows' for each subtotal ID", function () {
			var subtotalValues = ["11", "12", "13"];
			memory.setItem("activeCostCenterSubtotal", subtotalValues.join(","));
			spyOn(Ellucian.ColleagueFinance, "initializeActiveRows");

			viewModel.setCostCenterSubtotals();
			expect(Ellucian.ColleagueFinance.initializeActiveRows).toHaveBeenCalled();
		});
	});
});