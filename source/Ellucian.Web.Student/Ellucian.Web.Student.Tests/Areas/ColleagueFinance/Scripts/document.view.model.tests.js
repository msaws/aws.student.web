﻿// Copyright 2015 Ellucian Company L.P. and its affiliates

describe("documentViewModel", function () {
    var viewModel,
        documentRepository,
        url = "http://localhost/voucher/V000001";

    beforeAll(function () {
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    beforeEach(function () {
        documentRepository = new testDocumentRepository();
    });

    describe("isDocumentDefined", function () {
        it("should return true if the view model has a document", function () {
            viewModel = new documentViewModel();
            viewModel.Document(documentRepository.buildVoucher());
            expect(viewModel.isDocumentDefined()).toBe(true);
        });

        it("should return false if the view model does not have a document", function () {
            viewModel = new documentViewModel();
            expect(viewModel.isDocumentDefined()).toBe(false);
        });
    });

    describe("clickTab", function () {
        it("should update observables accordingly when the Overview tab is selected", function () {
            viewModel = new documentViewModel();
            viewModel.clickTab('A');
            viewModel.clickTab('O');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('O');
        });

        it("should do nothing when selecting the Overview tab if it's already selected", function () {
            viewModel = new documentViewModel();
            viewModel.clickTab('O');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('O');

            viewModel.clickTab('O');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('O');
        });

        it("should update observables accordingly when the Approvers tab is selected", function () {
            viewModel = new documentViewModel();
            viewModel.clickTab('A');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('A');
        });

        it("should do nothing when selecting the Approvers tab if it's already selected", function () {
            viewModel = new documentViewModel();
            viewModel.clickTab('A');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('A');

            viewModel.clickTab('A');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('A');
        });

        it("should update observables accordingly when the Line Items tab is selected", function () {
            viewModel = new documentViewModel();
            viewModel.clickTab('A');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('A');
        });

        it("should do nothing when selecting the Line Items tab if it's already selected", function () {
            viewModel = new documentViewModel();
            viewModel.clickTab('L');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('L');

            viewModel.clickTab('L');
            expect(viewModel.LineItemDetailDisplayed()).toBe(false);
            expect(viewModel.TabSelected()).toBe('L');
        });
    });

    describe("changeToMobile", function () {
        it("should call reloadPage if the document is defined and isMobile is true", function () {
            viewModel.isMobile(true);
            viewModel.Document({});

            spyOn(viewModel, 'reloadPage');

            viewModel.changeToMobile();
            expect(viewModel.reloadPage).toHaveBeenCalled();
        })
    });

    describe("changeToDesktop", function () {
        it("should call reloadPage if the document is defined and isMobile is false", function () {
            viewModel.isMobile(false);
            viewModel.Document({});

            spyOn(viewModel, 'reloadPage');

            viewModel.changeToDesktop();
            expect(viewModel.reloadPage).toHaveBeenCalled();
        })
    });

    describe("loadDocument", function () {
        it("should use the document URL passed in", function () {
            spyOn($, "ajax");
            viewModel.loadDocument(url, "");
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(url);
        });

        it("should map the data into the view model on success", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(documentRepository.buildVoucher());
            });

            viewModel.loadDocument(url, "");
            expect(viewModel.Document().IsVoucher()).toBeTruthy();
        });

        //it("should update the isLoaded observable on complete", function () {
        //    spyOn($, "ajax").and.callFake(function (e) {
        //        e.complete();
        //    });

        //    viewModel.loadDocument(url, "");
        //    expect(viewModel.isLoaded()).toBeTruthy();
        //});

        //it("should call selectOverviewTab on complete, if on mobile device", function () {
        //    viewModel.isMobile(true);
        //    spyOn($, "ajax").and.callFake(function (e) {
        //        e.complete();
        //    });
        //    spyOn(viewModel, "selectOverviewTab");

        //    viewModel.loadDocument(url, "");
        //    expect(viewModel.selectOverviewTab).toHaveBeenCalled();
        //});

        //it("should add an error message to the notification center using the supplied message", function () {
        //    spyOn($, "ajax").and.callFake(function (e) {
        //        e.error();
        //    });
        //    spyOn($.fn, "notificationCenter");
        //    var errorMessage = "Error loading document.";
        //    viewModel.loadDocument(url, errorMessage);
        //    expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: errorMessage, type: "error" });
        //});
    });
});