﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates

var memoryStorage = {};

var memory = {
    getItem: function (id) {
        return memoryStorage[id];
    },
    setItem: function (id, val) {
        memoryStorage[id] = val;
    }
};

// Initialize the ColleagueFinance namespace object for use in ColleagueFinance module.
var Ellucian = Ellucian || {};
Ellucian.ColleagueFinance = Ellucian.ColleagueFinance || {};
Ellucian.ColleagueFinance.dialogDom = '<div class="esg-modal-dialog__body"></div>'

describe("costCenterSummaryViewModel", function () {
    var viewModel,
		contentType;

    beforeEach(function () {
        viewModel = new costCenterSummaryViewModel();
        contentType = "json";
        $(document.body).remove(".esg-modal-dialog__body");
        $(document.body).append(Ellucian.ColleagueFinance.dialogDom);
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        memoryStorage = {};
    });

    describe("pickFiscalYear", function () {
        it("should execute the getFilteredCostCenters function if the page has been loaded and the filter is not updating", function () {
            viewModel.IsPageLoaded(true);
            viewModel.IsFilterUpdating(false);
            spyOn(viewModel, "getFilteredCostCenters");

            viewModel.pickFiscalYear();
            expect(viewModel.getFilteredCostCenters).toHaveBeenCalled();
        });

        it("should not execute the getFilteredCostCenters function if the page is still being loaded", function () {
            viewModel.IsPageLoaded(false);
            spyOn(viewModel, "getFilteredCostCenters");

            viewModel.pickFiscalYear();
            expect(viewModel.getFilteredCostCenters).not.toHaveBeenCalled();
        });

        it("should not execute the getFilteredCostCenters function if the filter is updating", function () {
            viewModel.IsFilterUpdating(true);
            spyOn(viewModel, "getFilteredCostCenters");

            viewModel.pickFiscalYear();
            expect(viewModel.getFilteredCostCenters).not.toHaveBeenCalled();
        });
    });

    describe("getFilteredCostCenters", function () {
        it("ajax is invoked with expected url", function () {
            var expectedUrl = getFilteredCostCentersActionUrl;
            spyOn($, "ajax");
            viewModel.getFilteredCostCenters();
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            viewModel.getFilteredCostCenters();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            spyOn($, "ajax");
            viewModel.getFilteredCostCenters();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("POST");
        });

        it("beforeSend() should display the filter before sending the request", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            viewModel.getFilteredCostCenters();
            expect(viewModel.IsFilterUpdating()).toBeTruthy();
        });

        it("should get the fiscal year from browser memory and assign it to the observable", function () {
            viewModel.IsPageLoaded(false);
            viewModel.SelectedFiscalYear("");
            memory.setItem("fiscalYear", "2016");
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            viewModel.getFilteredCostCenters();
            expect(viewModel.SelectedFiscalYear()).toBe(memory.getItem("fiscalYear"));
        });

        it("success() should update cost centers data", function () {
            var testRepo = new testCostCenterRepository();

            memory.setItem("selectedSavedFilter", "true");
            memory.setItem("show-no-activity", "true");

            viewModel.MajorComponents([
                new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }),
                new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })
            ]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-15", "20-30"]);
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(testRepo.getCostCenters());
            });
            viewModel.getFilteredCostCenters();

            expect(viewModel.isFilterCriteriaEntered()).toBeTruthy();
            expect(viewModel.PopulateFiscalYearDropDown()).toBeFalsy();
            expect(viewModel.SelectedFiscalYear()).toBe(testRepo.getCostCenters().TodaysFiscalYear);
            expect(viewModel.CostCenters().length).toBe(testRepo.getCostCenters().CostCenters.length);
            expect(viewModel.SavedFilterNames().length).toBe(testRepo.getCostCenters().SavedFilterNames.length);
        });

        it("error() should issue the default notification if an error is returned", function () {
            var jqXhr = {
                status: 400
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.error(jqXhr, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getFilteredCostCenters();
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadCostCentersText, type: "error" });
        });

        it("error() should issue a 'resource not found' notification if a 404 error is returned", function () {
            var jqXhr = {
                status: 404
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.error(jqXhr, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getFilteredCostCenters();
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("complete() should update the view model upon completion", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn($.fn, "notificationCenter");
            spyOn($.fn, "on");
            viewModel.getFilteredCostCenters();

            // Check that the observables were correctly set
            expect(viewModel.IsPageLoaded()).toBeTruthy();
            expect(viewModel.IsFilterUpdating()).toBeFalsy();
            expect(viewModel.CostCentersCount()).toBe(viewModel.CostCenters().length);

            // A notification should be created if there are no cost centers.
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: noCostCentersToDisplayText, type: "info" });
            expect($.fn.on).toHaveBeenCalled();
        });
    });

    describe("openSaveFilterDialog", function () {
        it("should trigger enter key emulation", function () {
            spyOn(viewModel, "triggerAllEnterKeys");
            viewModel.openSaveFilterDialog();
            expect(viewModel.triggerAllEnterKeys).toHaveBeenCalled();
        });

        it("should set 'isSaveDialogVisible' to True", function () {
            viewModel.openSaveFilterDialog();
            expect(viewModel.isSaveDialogVisible()).toBeTruthy();
        });
    });

    describe("closeSaveFilterDialog", function () {
        it("should set boolean values and string values correctly", function () {
            viewModel.closeSaveFilterDialog();
            expect(viewModel.isSaveFilterValid()).toBeTruthy();
            expect(viewModel.isSaveDialogVisible()).toBeFalsy();
            expect(viewModel.createOrUpdate()).toBe("create");
            expect(viewModel.saveFilterName()).toBe("");
        });
    });

    describe("validateSaveFilter", function () {
        it("'isSaveFilterNameVisible' should be true if 'createOrUpdate' is set to 'create'", function () {
            viewModel.createOrUpdate("create");
            expect(viewModel.isSaveFilterNameVisible()).toBeTruthy();
        });

        it("'isSaveFilterNameVisible' should be false if 'createOrUpdate' is not set to 'create'", function () {
            viewModel.createOrUpdate("update");
            expect(viewModel.isSaveFilterNameVisible()).toBeFalsy();
        });

        it("should return true if 'isSaveFilterNameVisible' is false", function () {
            viewModel.createOrUpdate("update");
            var actual = viewModel.validateSaveFilter();
            expect(actual).toBeTruthy();
        });

        it("should return true if 'saveFilterName' contains non-whitespace characters", function () {
            viewModel.createOrUpdate("create");
            viewModel.saveFilterName("Valid Name");
            var actual = viewModel.validateSaveFilter();
            expect(actual).toBeTruthy();
        });

        it("should return false if 'saveFilterName' contains only whitespace characters", function () {
            viewModel.createOrUpdate("create");
            viewModel.saveFilterName("  ");     // Invalid name
            var actual = viewModel.validateSaveFilter();
            expect(actual).toBeFalsy();
        });

        it("should return false if 'saveFilterName' is an empty string", function () {
            viewModel.createOrUpdate("create");
            viewModel.saveFilterName("");
            var actual = viewModel.validateSaveFilter();
            expect(actual).toBeFalsy();
        });


    });

    describe("toggleSavedFilters", function () {
        it("should toggle the boolean value of 'isSavedFiltersExpanded'", function () {
            viewModel.isSavedFiltersExpanded(false);
            viewModel.toggleSavedFilters();
            expect(viewModel.isSavedFiltersExpanded()).toBeTruthy();
        });
    });

    describe("selectSavedFilter", function () {
        it("ajax is invoked with expected url", function () {
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            var expectedUrl = getSavedFilterCriteriaUrl + "?filterId=" + encodeURI(filter.Key());
            spyOn($, "ajax");
            viewModel.selectSavedFilter(filter);
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            var filterName = "test";
            spyOn($, "ajax");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("beforeSend() should set the 'isLoadingSavedFilterCriteria' observable to true", function () {
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend();
            });
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect(viewModel.isLoadingSavedFilterCriteria()).toBeTruthy();
        });

        it("success() should set major components and flags", function () {
            var returnedSavedFilter = {
                Name: "Test",
                Id: "1",
                IncludeActiveAccountsWithNoActivity: true,
                ComponentCriteria: [
					{
					    ComponentName: "Fund",
					    IndividualComponentValues: [
							11,
							12
					    ],
					    RangeComponentValues: [
							{
							    StartValue: 20,
							    EndValue: 25
							},
							{
							    StartValue: 30,
							    EndValue: 35
							}
					    ]
					}
                ]
            };
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }), new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedSavedFilter);
            });
            spyOn(viewModel, "toggleSavedFilters");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect(viewModel.isSavedFilterSelected()).toBeTruthy();
            expect(viewModel.selectedSavedFilterLabel()).toBe(returnedSavedFilter.Name);
            expect(viewModel.selectedSavedFilterId()).toBe(returnedSavedFilter.Id);
        });

        it("error() should add a 404 error to the notification center", function () {
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("error() should add a filter error to the notification center", function () {
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 500 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadFilterMessage, type: "error" });
        });

        it("complete() should set 'isLoadingSavedFilterCriteria' to false", function () {
            viewModel.autoloadCostCenters(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn(viewModel, "getFilteredCostCenters");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect(viewModel.isLoadingSavedFilterCriteria()).toBeFalsy();
            expect(viewModel.getFilteredCostCenters).toHaveBeenCalled();
            expect(viewModel.autoloadCostCenters()).toBeFalsy();
        });

        it("complete() should NOT call 'getFilteredCostCenters' if we're not supposed to auto-load the cost centers", function () {
            viewModel.autoloadCostCenters(false);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn(viewModel, "getFilteredCostCenters");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectSavedFilter(filter);

            expect(viewModel.isLoadingSavedFilterCriteria()).toBeFalsy();
            expect(viewModel.getFilteredCostCenters).not.toHaveBeenCalled();
            expect(viewModel.autoloadCostCenters()).toBeFalsy();
        });
    });

    describe("deleteFilter", function () {
        it("ajax is not invoked when filterName is empty", function () {
            spyOn($, "ajax");
            var filter = {};
            filter.Value = ko.observable("");
            filter.Key = ko.observable("");

            viewModel.deleteFilter(filter);

            expect($.ajax).not.toHaveBeenCalled();
        });

        it("ajax is invoked with the expected URL", function () {
            spyOn($, "ajax");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");
            var expectedUrl = deleteSavedFilterUrl + "?filterId=" + encodeURI(filter.Key());
            viewModel.deleteFilter(filter);

            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");
            viewModel.deleteFilter(filter);

            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            spyOn($, "ajax");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");
            viewModel.deleteFilter(filter);

            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("beforeSend sets isDeletingSavedFilter to true", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");
            viewModel.deleteFilter(filter);

            expect(viewModel.isDeletingSavedFilter()).toBeTruthy();
        });

        it("success sets selectedSavedFilterLabel, Id, and SavedFilterNames observables", function () {
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");

            viewModel.selectedSavedFilterLabel(filter.Value());
            viewModel.selectedSavedFilterId(filter.Key());

            var returnedFilters = [];
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedFilters);
            });
            spyOn($.fn, "notificationCenter");
            viewModel.deleteFilter(filter);

            expect(viewModel.SavedFilterNames().length).toBe(returnedFilters.length);
            expect(viewModel.selectedSavedFilterId()).toBe("");
            expect(viewModel.selectedSavedFilterLabel()).toBe(Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel);
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: filterDeletedSuccessMessage, type: "success", flash: true });
        });

        it("error displays notification center error message", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 500 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");
            viewModel.deleteFilter(filter);

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.ColleagueFinance.filterDeleteFailedMessage, type: "error" });
        });

        it("error displays 404 error notification", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            var filter = {};
            filter.Value = ko.observable("filter-name");
            filter.Key = ko.observable("1");
            viewModel.deleteFilter(filter);

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("complete sets isDeletingSavedFilter to false", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete({});
            });
            var filter = {};
            filter.Key = ko.observable("1");
            filter.Value = ko.observable("filter-name");
            viewModel.deleteFilter(filter);

            expect(viewModel.isDeletingSavedFilter()).toBeFalsy();
        });

        it("complete clears the 'current' and 'working' observables if the filter being deleted is the current filter", function () {
            viewModel.currentDefaultFilterId("1");
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete({});
            });
            var filter = {};
            filter.Key = ko.observable("1");
            filter.Value = ko.observable("filter-name");
            viewModel.deleteFilter(filter);

            expect(viewModel.isDeletingSavedFilter()).toBeFalsy();
            expect(viewModel.currentDefaultFilterId()).toBe("");
            expect(viewModel.currentDefaultFilterName()).toBe("");
            expect(viewModel.workingDefaultFilterId()).toBe("");
            expect(viewModel.workingDefaultFilterName()).toBe("");
        });
    });

    describe("saveFilter", function () {
        it("ajax is invoked with expected url", function () {
            var expectedUrl = postSaveFilterUrl;
            viewModel.createOrUpdate("update");
            spyOn($, "ajax");
            viewModel.saveFilter();
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            viewModel.createOrUpdate("update");
            spyOn($, "ajax");
            viewModel.saveFilter();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            viewModel.createOrUpdate("update");
            spyOn($, "ajax");
            viewModel.saveFilter();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("POST");
        });

        it("beforeSend() should show the spinner", function () {
            viewModel.createOrUpdate("update");
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            viewModel.saveFilter();
            expect(viewModel.isLoadingSavedFilterCriteria()).toBeFalsy();
            expect(viewModel.isFilterSaving()).toBeTruthy();
        });

        it("success() sets a flash notification and updates the list of SavedFilterNames", function () {
            var returnedFilters = ["Saved Filter 1", "Saved Filter 2", "Saved Filter 3"];
            viewModel.createOrUpdate("create");
            viewModel.saveFilterName("Saved Filter 4");
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }), new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20"]);
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedFilters);
            });
            spyOn($.fn, "notificationCenter");
            viewModel.saveFilter();

            expect(viewModel.SavedFilterNames().length).toBe(returnedFilters.length);
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: filterSavedMessage, type: "success", flash: true });
        });

        it("error() should add a 404 error to the notification center", function () {
            viewModel.createOrUpdate("update");
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.saveFilter();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("error() should add a filter error to the notification center", function () {
            viewModel.createOrUpdate("update");
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 500 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.saveFilter();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToSaveFilterMessage, type: "error" });
        });

        it("complete() should hide the loading spinner", function () {
            viewModel.createOrUpdate("update");
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn(viewModel, "closeSaveFilterDialog");
            viewModel.saveFilter();

            expect(viewModel.isFilterSaving()).toBeFalsy();
            expect(viewModel.closeSaveFilterDialog).toHaveBeenCalled();
        });

    });

    describe("getMajorComponentsThenCostCenters", function () {
        it("ajax is invoked with expected url", function () {
            var expectedUrl = getMajorComponentsActionUrl;
            spyOn($, "ajax");
            viewModel.getMajorComponentsThenCostCenters();
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            viewModel.getMajorComponentsThenCostCenters();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            spyOn($, "ajax");
            viewModel.getMajorComponentsThenCostCenters();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("beforeSend() should set the 'IsFilterUpdating' observable to true", function () {
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            viewModel.getMajorComponentsThenCostCenters();
            expect(viewModel.IsFilterUpdating()).toBeTruthy();
        });

        it("success() should set the populateFilterComponents flag and set the MajorComponents observable", function () {
            var returnedMajorComponentData = [new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }), new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })];

            memory.setItem("show-no-activity", "true");

            viewModel.populateFilterComponents(true);
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedMajorComponentData);
            });
            viewModel.getMajorComponentsThenCostCenters();

            expect(viewModel.populateFilterComponents()).toBeFalsy();
            expect(viewModel.MajorComponents().length).toBe(returnedMajorComponentData.length);
        });

        it("success() should apply values from browser memory", function () {
            var fundValues = ["11", "12", "13"];
            memory.setItem("Fund", fundValues.join(","));
            var returnedMajorComponentData = [new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }), new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })];
            viewModel.populateFilterComponents(true);
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedMajorComponentData);
            });
            viewModel.getMajorComponentsThenCostCenters();

            expect(viewModel.populateFilterComponents()).toBeFalsy();
            expect(viewModel.MajorComponents().length).toBe(returnedMajorComponentData.length);
            expect(viewModel.MajorComponents()[0].tokenizedFilterCriteria().length).toBe(fundValues.length);
            for (var i = 0; i < viewModel.MajorComponents()[0].tokenizedFilterCriteria().length; i++) {
                expect(viewModel.MajorComponents()[0].tokenizedFilterCriteria()[i]).toBe(fundValues[i]);
            }
        });

        it("error() should add a 404 error to the notification center", function () {
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getMajorComponentsThenCostCenters();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("error() should add a cost centers load error to the notification center", function () {
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 500 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getMajorComponentsThenCostCenters();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadCostCentersText, type: "error" });
        });

        it("complete() should trigger 'getFilteredCostCenters' if 'useDefaultFilter' is false", function () {
            viewModel.IsPageLoaded(true);
            viewModel.useDefaultFilter(false);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn(viewModel, "getUIPreference");
            spyOn(viewModel, "getFilteredCostCenters");
            viewModel.getMajorComponentsThenCostCenters();

            expect(viewModel.getUIPreference).toHaveBeenCalled();
            expect(viewModel.getFilteredCostCenters).toHaveBeenCalled();
        });

        it("complete() should trigger 'getDefaultFilter' if 'useDefaultFilter' is true", function () {
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn(viewModel, "getUIPreference");
            spyOn(viewModel, "getDefaultFilter");
            viewModel.getMajorComponentsThenCostCenters();

            expect(viewModel.getUIPreference).toHaveBeenCalled();
            expect(viewModel.getDefaultFilter).toHaveBeenCalled();
        });
    });

    describe("resetFilters", function () {
        it("should clear the tokenizedFilterCriteria", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }), new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20"]);
            viewModel.resetFilters();

            for (var i = 0; i < viewModel.MajorComponents().length; i++) {
                expect(viewModel.MajorComponents()[i].tokenizedFilterCriteria.length).toEqual(0);
            }
        });
        it("should set tokenizedFilterCriteria content to resetFilterCriteria content", function () {
            viewModel.MajorComponents([
                new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }),
                new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 2 })]
            );
            for (var i = 0; i < viewModel.MajorComponents().length; i++) {
                viewModel.MajorComponents()[i].resetFilterCriteria.push("10");
            }

            viewModel.resetFilters();

            expect(viewModel.showActiveAccountsWithNoActivity()).toBeTruthy();
            expect(viewModel.selectedSavedFilterLabel()).toBe(Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel);
            expect(viewModel.selectedSavedFilterId()).toBe("");
            expect(viewModel.isSavedFilterSelected()).toBeFalsy();

            for (var i = 0; i < viewModel.MajorComponents().length; i++) {
                expect(viewModel.MajorComponents()[i].tokenizedFilterCriteria().length).toEqual(viewModel.MajorComponents()[i].resetFilterCriteria().length);
                expect(viewModel.MajorComponents()[i].tokenizedFilterCriteria()).toEqual(viewModel.MajorComponents()[i].resetFilterCriteria());
            }
        });
    });

    describe("isFilterChanged", function () {
        it("should return false when there are no changes", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].appliedFilterCriteria(["11", "12", "13-20"]);

            var actual = viewModel.isFilterChanged();

            expect(actual).toBeFalsy();
        });

        it("should return true when there are changes", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20", "21"]);
            viewModel.MajorComponents()[0].appliedFilterCriteria(["11", "12", "13-20"]);

            var actual = viewModel.isFilterChanged();

            expect(actual).toBeTruthy();
        });

        it("should return true when the Active Accounts with No Activity option has changed", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].appliedFilterCriteria(["11", "12", "13-20"]);
            viewModel.showActiveAccountsWithNoActivity(false);

            var actual = viewModel.isFilterChanged();

            expect(actual).toBeTruthy();
        });

        it("should return true when there is a valid, new working criteria", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].appliedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].workingCriteria = ko.observable("21");

            var actual = viewModel.isFilterChanged();

            expect(actual).toBeTruthy();
        });

        it("should return false when there is a valid, duplicate working criteria", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].appliedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].workingCriteria = ko.observable("12");

            var actual = viewModel.isFilterChanged();

            expect(actual).toBeFalsy();
        });

        it("should return false when there are no changes and an invalid working criteria", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].appliedFilterCriteria(["11", "12", "13-20"]);
            viewModel.MajorComponents()[0].workingCriteria = ko.observable("1");

            var actual = viewModel.isFilterChanged();

            expect(actual).toBeFalsy();
        });
    });

    describe("isSaveCriteriaButtonEnabled", function () {
        it("should return false when there are no changes", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria([]);
            viewModel.showActiveAccountsWithNoActivity(true);

            var actual = viewModel.isSaveCriteriaButtonEnabled();

            expect(actual).toBeFalsy();
        });

        it("should return true when there are changes to components", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11"]);
            viewModel.showActiveAccountsWithNoActivity(true);

            var actual = viewModel.isSaveCriteriaButtonEnabled();

            expect(actual).toBeTruthy();
        });

        it("should return true when the Active Accounts with No Activity option has changed", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria([]);
            viewModel.showActiveAccountsWithNoActivity(false);

            var actual = viewModel.isSaveCriteriaButtonEnabled();

            expect(actual).toBeTruthy();
        });

        it("should return true when there is a valid, new working criteria", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria([]);
            viewModel.MajorComponents()[0].workingCriteria = ko.observable("21");
            viewModel.showActiveAccountsWithNoActivity(true);

            var actual = viewModel.isSaveCriteriaButtonEnabled();

            expect(actual).toBeTruthy();
        });

        it("should return false when there are no changes and an invalid working criteria", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria([]);
            viewModel.MajorComponents()[0].workingCriteria = ko.observable("1");

            var actual = viewModel.isSaveCriteriaButtonEnabled();

            expect(actual).toBeFalsy();
        });
    });

    describe("filterSummary", function () {
        it("should sort the unique, active filter criteria", function () {
            viewModel.MajorComponents([new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 })]);
            viewModel.MajorComponents()[0].appliedFilterCriteria(["11", "12", "10"]);

            var actual = viewModel.filterSummary();

            expect(actual.length).toEqual(1);
            expect(actual[0]).toEqual("Fund: 10, 11, 12");
        });
    });

    describe("getUIPreference", function () {
        it("ajax is invoked with expected url", function () {
            var expectedUrl = Ellucian.ColleagueFinance.getUIPreferenceUrl;
            spyOn($, "ajax");
            viewModel.getUIPreference();
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            viewModel.getUIPreference();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            spyOn($, "ajax");
            viewModel.getUIPreference();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("success sets isBarGraphViewActive to the returned boolean value", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(false);
            });
            viewModel.getUIPreference();

            expect(viewModel.isBarGraphViewActive()).toBeFalsy();
        });

        it("error sets isBarGraphViewActive to the default (true) value", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            viewModel.getUIPreference();

            expect(viewModel.isBarGraphViewActive()).toBeTruthy();
        });

        it("complete sets isUIViewKnown to true", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            viewModel.getUIPreference();

            expect(viewModel.isUIViewKnown()).toBeTruthy();
        });
    });

    describe("setUIPreference ", function () {
        it("ajax is invoked with expected url", function () {
            var expectedUrl = Ellucian.ColleagueFinance.setUIPreferenceUrl;
            spyOn($, "ajax");
            viewModel.setUIPreference(true);
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            viewModel.setUIPreference();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            spyOn($, "ajax");
            viewModel.setUIPreference();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("POST");
        });

        it("beforeSend clears notification", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend();
            });
            spyOn($.fn, "notificationCenter");

            viewModel.setUIPreference();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('removeNotification', { message: Ellucian.ColleagueFinance.setPreferenceFailedMessage, type: "error" });
        });

        it("error adds error notification", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");

            viewModel.setUIPreference();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.ColleagueFinance.setPreferenceFailedMessage, type: "error" });
        });
    });

    describe("toggleGrid", function () {
        it("should set isBarGraphViewActive to true", function () {
            spyOn(viewModel, "setUIPreference");
            viewModel.toggleGrid();

            expect(viewModel.isBarGraphViewActive()).toBeTruthy();
            expect(viewModel.setUIPreference).toHaveBeenCalledWith(true);
        });
    });

    describe("toggleList", function () {
        it("should set isBarGraphViewActive to false", function () {
            spyOn(viewModel, "setUIPreference");
            viewModel.toggleList();

            expect(viewModel.isBarGraphViewActive()).toBeFalsy();
            expect(viewModel.setUIPreference).toHaveBeenCalledWith(false);
        });
    });

    describe("toggleAA", function () {
        it("should toggle the aAExpanded state", function () {
            viewModel.aAExpanded(false);

            viewModel.toggleAA();

            expect(viewModel.aAExpanded()).toBeTruthy();
        });
    });

    describe("toggleFilter", function () {
        it("should toggle the active-filter class", function () {
            spyOn($.fn, "toggleClass");

            viewModel.toggleFilter();

            expect($.fn.toggleClass).toHaveBeenCalledWith("active-filter");
        });
    });

    describe("default filters", function () {
        describe("currentDefaultFilterLabel", function () {
            it("should return the name of the current default filter, if specified", function () {
                viewModel.currentDefaultFilterName("Object 53013");
                expect(viewModel.currentDefaultFilterLabel()).toBe(viewModel.currentDefaultFilterName());
            });
            it("should return 'None' if the default filter name is blank", function () {
                viewModel.currentDefaultFilterName("");
                expect(viewModel.currentDefaultFilterLabel()).toBe(Ellucian.ColleagueFinance.noneLabel);
            });
            it("should return 'None' if the default filter name is null", function () {
                viewModel.currentDefaultFilterName(null);
                expect(viewModel.currentDefaultFilterLabel()).toBe(Ellucian.ColleagueFinance.noneLabel);
            });
            it("should return 'None' if the default filter name is the default label", function () {
                viewModel.currentDefaultFilterName(Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel);
                expect(viewModel.currentDefaultFilterLabel()).toBe(Ellucian.ColleagueFinance.noneLabel);
            });
        });

        describe("workingDefaultFilterLabel", function () {
            it("should return the name of the working default filter, if specified", function () {
                viewModel.workingDefaultFilterName("Object 53013");
                expect(viewModel.workingDefaultFilterLabel()).toBe(viewModel.workingDefaultFilterName());
            });
            it("should return 'None' if the working filter name is blank", function () {
                viewModel.workingDefaultFilterName("");
                expect(viewModel.workingDefaultFilterLabel()).toBe(Ellucian.ColleagueFinance.noneLabel);
            });
            it("should return 'None' if the working filter name is null", function () {
                viewModel.workingDefaultFilterName(null);
                expect(viewModel.workingDefaultFilterLabel()).toBe(Ellucian.ColleagueFinance.noneLabel);
            });
            it("should return 'None' if the default filter name is the default label", function () {
                viewModel.workingDefaultFilterName(Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel);
                expect(viewModel.workingDefaultFilterLabel()).toBe(Ellucian.ColleagueFinance.noneLabel);
            });
        });

        describe("openDefaultFilterDialog", function () {
            it("should add the 'overflow-visible' class to the dialog, if not present", function () {
                viewModel.selectedSavedFilterId("1");
                viewModel.selectedSavedFilterLabel("Object 53013");

                viewModel.openDefaultFilterDialog();
                expect($(".esg-modal-dialog__body").hasClass("overflow-visible")).toBeTruthy();
            });
            it("should display the dialog via an observable", function () {
                viewModel.selectedSavedFilterId("1");
                viewModel.selectedSavedFilterLabel("Object 53013");
                viewModel.openDefaultFilterDialog();

                expect(viewModel.isDefaultDialogVisible()).toBeTruthy();
            });
        });

        describe("closeDefaultFilterDialog", function () {
            it("should hide the dialog and set the working default filter observables", function () {
                viewModel.currentDefaultFilterId("5");
                viewModel.currentDefaultFilterName("Department 20601")
                viewModel.closeDefaultFilterDialog();
                expect(viewModel.isDefaultDialogVisible()).toBeFalsy();
                expect(viewModel.workingDefaultFilterId()).toBe(viewModel.currentDefaultFilterId());
                expect(viewModel.workingDefaultFilterName()).toBe(viewModel.currentDefaultFilterName());
            });
        });

        describe("toggleDefaultFilters", function () {
            it("should set the backing observable to false if currently true", function () {
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.toggleDefaultFilters();
                expect(viewModel.isDefaultFiltersExpanded()).toBeFalsy();
            });
            it("should set the backing observable to true if currently false", function () {
                viewModel.isDefaultFiltersExpanded(false);
                viewModel.toggleDefaultFilters();
                expect(viewModel.isDefaultFiltersExpanded()).toBeTruthy();
            });
        });

        describe("selectDefaultFilter", function () {
            it("should not update the working default observables if no filter is specified", function () {
                viewModel.workingDefaultFilterId("1");
                viewModel.workingDefaultFilterName("Object 53010");
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.selectDefaultFilter();

                expect(viewModel.workingDefaultFilterId()).toBe("1");
                expect(viewModel.workingDefaultFilterName()).toBe("Object 53010");
                expect(viewModel.selectDefaultFilter()).toBeFalsy();
            });
            it("should not update the working default observables if the filter is null", function () {
                viewModel.workingDefaultFilterId("1");
                viewModel.workingDefaultFilterName("Object 53010");
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.selectDefaultFilter(null);

                expect(viewModel.workingDefaultFilterId()).toBe("1");
                expect(viewModel.workingDefaultFilterName()).toBe("Object 53010");
                expect(viewModel.selectDefaultFilter()).toBeFalsy();
            });
            it("should not update the working default observables if the filter key is null", function () {
                var filter = { "Key": null, "Value": "Department 20601" };
                viewModel.workingDefaultFilterId("1");
                viewModel.workingDefaultFilterName("Object 53010");
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.selectDefaultFilter(filter);

                expect(viewModel.workingDefaultFilterId()).toBe("1");
                expect(viewModel.workingDefaultFilterName()).toBe("Object 53010");
                expect(viewModel.selectDefaultFilter()).toBeFalsy();
            });
            it("should not update the working default observables if the filter key is blank", function () {
                var filter = { "Key": "", "Value": "Department 20601" };
                viewModel.workingDefaultFilterId("1");
                viewModel.workingDefaultFilterName("Object 53010");
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.selectDefaultFilter(filter);

                expect(viewModel.workingDefaultFilterId()).toBe("1");
                expect(viewModel.workingDefaultFilterName()).toBe("Object 53010");
                expect(viewModel.selectDefaultFilter()).toBeFalsy();
            });
            it("should not update the working default observables if the filter value is null", function () {
                var filter = { "Key": "2", "Value": null };
                viewModel.workingDefaultFilterId("1");
                viewModel.workingDefaultFilterName("Object 53010");
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.selectDefaultFilter(filter);

                expect(viewModel.workingDefaultFilterId()).toBe("1");
                expect(viewModel.workingDefaultFilterName()).toBe("Object 53010");
                expect(viewModel.selectDefaultFilter()).toBeFalsy();
            });
            it("should not update the working default observables if the filter value is empty", function () {
                var filter = { "Key": "2", "Value": "" };
                viewModel.workingDefaultFilterId("1");
                viewModel.workingDefaultFilterName("Object 53010");
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.selectDefaultFilter(filter);

                expect(viewModel.workingDefaultFilterId()).toBe("1");
                expect(viewModel.workingDefaultFilterName()).toBe("Object 53010");
                expect(viewModel.selectDefaultFilter()).toBeFalsy();
            });
            it("should update the working default observables if the filter fully populated", function () {
                var mappedFilter = ko.mapping.fromJS({ "Key": "2", "Value": "Department 20601" }, costCenterMapping);
                viewModel.workingDefaultFilterId("1");
                viewModel.workingDefaultFilterName("Object 53010");
                viewModel.isDefaultFiltersExpanded(true);
                viewModel.selectDefaultFilter(mappedFilter);

                expect(viewModel.workingDefaultFilterId()).toBe(mappedFilter.Key());
                expect(viewModel.workingDefaultFilterName()).toBe(mappedFilter.Value());
                expect(viewModel.selectDefaultFilter()).toBeFalsy();
            });
        });

        describe("saveDefaultFilter", function () {
            it("ajax is invoked with expected url", function () {
                viewModel.workingDefaultFilterId("2");
                viewModel.workingDefaultFilterName("Object 53013");
                var expectedUrl = Ellucian.ColleagueFinance.setDefaultFilterAsyncUrl
	                + "?filterKey=" + viewModel.workingDefaultFilterId()
                    + "&filterValue=" + viewModel.workingDefaultFilterName();
                spyOn($, "ajax");
                viewModel.saveDefaultFilter();
                expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
            });

            it("ajax is invoked with expected dataType", function () {
                spyOn($, "ajax");
                viewModel.saveDefaultFilter();
                expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
            });

            it("ajax is invoked with expected action type", function () {
                spyOn($, "ajax");
                viewModel.saveDefaultFilter();
                expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
            });

            it("beforeSend() should display the loading spinner", function () {
                viewModel.IsPageLoaded(true);
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend({});
                });
                viewModel.saveDefaultFilter();
                expect(viewModel.isSavingDefaultFilter()).toBeTruthy();
            });

            it("success() should populate the current default filter and issue a notification", function () {
                var savedFiltersData = { "Key": "2", "Value": "Object 53013" };
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success(savedFiltersData);
                });
                spyOn($.fn, "notificationCenter");
                viewModel.saveDefaultFilter();

                expect(viewModel.currentDefaultFilterId()).toBe("2");
                expect(viewModel.currentDefaultFilterName()).toBe("Object 53013");
                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.ColleagueFinance.defaultFilterSavedMessage, type: "success", flash: true });
            });

            it("error() should add a 404 error to the notification center", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.error({ status: 404 }, {}, {});
                });
                spyOn($.fn, "notificationCenter");
                viewModel.saveDefaultFilter();

                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
            });

            it("error() should display a 'filter not saved' message", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.error({ status: 500 }, {}, {});
                });
                spyOn($.fn, "notificationCenter");
                viewModel.saveDefaultFilter();

                expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.ColleagueFinance.defaultFilterSaveFailedMessage, type: "error" });
            });

            it("complete() should hide the dialog, spinner, and set the working default filter", function () {
                viewModel.currentDefaultFilterId("1");
                viewModel.currentDefaultFilterName("Object 53013");
                spyOn($, "ajax").and.callFake(function (e) {
                    e.complete();
                });
                viewModel.saveDefaultFilter();

                expect(viewModel.isDefaultDialogVisible()).toBeFalsy();
                expect(viewModel.isSavingDefaultFilter()).toBeFalsy();
                expect(viewModel.workingDefaultFilterId()).toBe(viewModel.currentDefaultFilterId());
                expect(viewModel.currentDefaultFilterName()).toBe(viewModel.currentDefaultFilterName());
            });
        });

        describe("getDefaultFilter", function () {
            it("ajax is invoked with expected url", function () {
                var expectedUrl = Ellucian.ColleagueFinance.getDefaultFilterAsyncUrl;
                spyOn($, "ajax");
                viewModel.getDefaultFilter();
                expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
            });

            it("ajax is invoked with expected dataType", function () {
                spyOn($, "ajax");
                viewModel.getDefaultFilter();
                expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
            });

            it("ajax is invoked with expected action type", function () {
                spyOn($, "ajax");
                viewModel.getDefaultFilter();
                expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
            });

            it("beforeSend() should display the loading spinner", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.beforeSend({});
                });
                viewModel.getDefaultFilter();
                expect(viewModel.isLoadingSavedFilterCriteria()).toBeTruthy();
            });

            it("success() should initialize the default filter observables when there a default filter exists", function () {
                var defaultFilterData = { "Key": "2", "Value": "Object 53013" };
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success(defaultFilterData);
                });
                spyOn(viewModel, "selectSavedFilter");
                viewModel.getDefaultFilter();

                expect(viewModel.currentDefaultFilterId()).toBe(defaultFilterData.Key);
                expect(viewModel.currentDefaultFilterName()).toBe(defaultFilterData.Value);
                expect(viewModel.workingDefaultFilterId()).toBe(defaultFilterData.Key);
                expect(viewModel.workingDefaultFilterName()).toBe(defaultFilterData.Value);
                expect(viewModel.autoloadCostCenters()).toBeFalsy();
                expect(viewModel.selectSavedFilter).not.toHaveBeenCalled();
            });

            it("success() should call selectSavedFilter if the optional argument is true", function () {
                var defaultFilterData = { "Key": "2", "Value": "Object 53013" };
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success(defaultFilterData);
                });
                spyOn(viewModel, "selectSavedFilter");
                viewModel.getDefaultFilter(true);

                expect(viewModel.currentDefaultFilterId()).toBe(defaultFilterData.Key);
                expect(viewModel.currentDefaultFilterName()).toBe(defaultFilterData.Value);
                expect(viewModel.workingDefaultFilterId()).toBe(defaultFilterData.Key);
                expect(viewModel.workingDefaultFilterName()).toBe(defaultFilterData.Value);
                expect(viewModel.autoloadCostCenters()).toBeTruthy();
                expect(viewModel.selectSavedFilter).toHaveBeenCalled();
            });

            it("success() should clear the default observables when there is no saved default filter", function () {
                var defaultFilterData = { "Key": "", "Value": "" };
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success(defaultFilterData);
                });
                spyOn(viewModel, "getFilteredCostCenters");
                viewModel.getDefaultFilter();

                expect(viewModel.currentDefaultFilterId()).toBe("");
                expect(viewModel.currentDefaultFilterName()).toBe("");
                expect(viewModel.workingDefaultFilterId()).toBe("");
                expect(viewModel.workingDefaultFilterName()).toBe("");
                expect(viewModel.getFilteredCostCenters).not.toHaveBeenCalled();
            });

            it("success() should call getFilteredCostCenters if the optional argument is true", function () {
                var defaultFilterData = { "Key": "", "Value": "" };
                spyOn($, "ajax").and.callFake(function (e) {
                    e.success(defaultFilterData);
                });
                spyOn(viewModel, "getFilteredCostCenters");
                viewModel.getDefaultFilter(true);

                expect(viewModel.currentDefaultFilterId()).toBe("");
                expect(viewModel.currentDefaultFilterName()).toBe("");
                expect(viewModel.workingDefaultFilterId()).toBe("");
                expect(viewModel.workingDefaultFilterName()).toBe("");
                expect(viewModel.getFilteredCostCenters).toHaveBeenCalled();
            });

            it("error() should call getFilteredCostCenters if the optional argument is true", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.error({ status: 500 }, {}, {});
                });
                spyOn(viewModel, "getFilteredCostCenters");
                viewModel.getDefaultFilter(true);

                expect(viewModel.getFilteredCostCenters).toHaveBeenCalled();
            });

            it("error() should NOT call getFilteredCostCenters if the optional argument is false", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.error({ status: 500 }, {}, {});
                });
                spyOn(viewModel, "getFilteredCostCenters");
                viewModel.getDefaultFilter();

                expect(viewModel.getFilteredCostCenters).not.toHaveBeenCalled();
            });

            it("complete() should hide the loading spinner", function () {
                spyOn($, "ajax").and.callFake(function (e) {
                    e.complete();
                });
                viewModel.getDefaultFilter();

                expect(viewModel.isLoadingSavedFilterCriteria()).toBeFalsy();
            });
        });
    });


    describe("costCentersTabClicked", function () {
        it("should set isCostCentersTabActive to true", function () {
            viewModel.costCentersTabClicked();

            expect(viewModel.isCostCentersTabActive()).toBeTruthy();
        });
    });

    describe("objectViewTabClicked", function () {
        it("should ensure object data exists and set isCostCentersTabActive to false", function () {
            spyOn(viewModel, "getFilteredObjectData");

            viewModel.objectViewTabClicked();

            expect(viewModel.isCostCentersTabActive()).toBeFalsy();
            expect(viewModel.getFilteredObjectData).toHaveBeenCalled();
        });
    });

    describe("getFilteredObjectData", function () {
        it("ajax is invoked with expected url", function () {
            var expectedUrl = Ellucian.ColleagueFinance.getFilteredObjectDataAsyncUrl;
            spyOn($, "ajax");
            viewModel.getFilteredObjectData();
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            spyOn($, "ajax");
            viewModel.getFilteredObjectData();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            spyOn($, "ajax");
            viewModel.getFilteredObjectData();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("POST");
        });

        it("beforeSend() should display the filter before sending the request", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend();
            });
            viewModel.getFilteredObjectData();
            expect(viewModel.IsFilterUpdating()).toBeTruthy();
        });

        it("beforeSend() should get the fiscal year from browser memory and assign it to the observable", function () {
            viewModel.IsPageLoaded(false);
            viewModel.SelectedFiscalYear("");
            memory.setItem("fiscalYear", "2016");
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend();
            });
            viewModel.getFilteredObjectData();
            expect(viewModel.SelectedFiscalYear()).toBe(memory.getItem("fiscalYear"));
        });

        it("success() should update object data", function () {

            memory.setItem("selectedSavedFilter", "true");
            memory.setItem("show-no-activity", "true");

            var resultData = {
                "ObjectData": {
                    "ExpenseObjectCodes": [
                    {
                        "GlClass": 0,
                        "Id": "",
                        "Name": "",
                        "TotalBudget": "",
                        "TotalActuals": "",
                        "TotalEncumbrances": "",
                        "TotalBudgetRemaining": "",
                        "TotalPercentProcessed": "",
                        "FinancialHealthIndicator": "",
                        "FinancialHealthIndicatorText": "",
                        "ColorCodeStyle": "",
                        "GlAccounts": [
                            {
                                "IsPooledAccount": true,
                                "IsUmbrellaVisible": true,
                                "FormattedGlAccount": "1",
                                "PrimaryGlAccount": {
                                    "Budget": "$1000.00",
                                    "Actuals": "$200.00",
                                    "Encumbrances": "$200.00",
                                    "BudgetRemaining": "$600.00",
                                    "PercentProcessed": "$800.00",
                                    "FinancialHealthIndicator": "group-icon-green",
                                    "FinancialHealthIndicatorText": "Under Budget",
                                    "PoolTypeCssClass": "umbrella"
                                },
                                "Poolees": [
                                    {
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    }, {
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    }, {
                                        "_Budget": 1000.00,
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    },
                                ]
                            },
                            {
                                "IsPooledAccount": false,
                                "IsUmbrellaVisible": true,
                                "FormattedGlAccount": "1",
                                "PrimaryGlAccount": {
                                    "Budget": "$1000.00",
                                    "Actuals": "$200.00",
                                    "Encumbrances": "$200.00",
                                    "BudgetRemaining": "$600.00",
                                    "PercentProcessed": "$800.00",
                                    "FinancialHealthIndicator": "group-icon-green",
                                    "FinancialHealthIndicatorText": "Under Budget",
                                    "PoolTypeCssClass": "non-pooled-account"
                                },
                                "Poolees": []
                            }
                        ]
                    }],
                    "RevenueObjectCodes": [],
                    "AssetObjectCodes": [],
                    "LiabilityObjectCodes": [],
                    "FundBalanceObjectCodes": [],
                    "TotalBudgetExpense": "$2,000.00",
                    "TotalActualsExpense": "$2,000.00",
                    "TotalEncumbrancesExpense": "$2,000.00",
                    "TotalBudgetRemainingExpense": "$0.00",
                    "TotalPercentProcessedExpense": "100 %",
                    "TotalFinancialHealthIndicatorExpense": "group-icon-yellow",
                    "TotalFinancialHealthIndicatorTextExpense": "Nearing Budget",
                    "ColorCodeStyleExpense": "remaining-black",
                    "TotalBudgetRevenue": "$2,000.00",
                    "TotalActualsRevenue": "$2,000.00",
                    "TotalEncumbrancesRevenue": "$2,000.00",
                    "TotalBudgetRemainingRevenue": "$0.00",
                    "TotalPercentProcessedRevenue": "100 %",
                    "ColorCodeStyleRevenue": "remaining-black",
                    "HasAnyExpense": true,
                    "HasAnyRevenue": false,
                    "HasAnyAsset": false,
                    "HasAnyLiability": false,
                    "HasAnyFundBalance": false
                },
                "TodaysFiscalYear": "2016",
                "FiscalYears": [
                    {
                        "FiscalYear": "2016",
                        "FiscalYearDescription": "FY2016"
                    }
                ],
                "SavedFilterNames": [],  // { "Key": "1", "Value": "Filter 1" }, { "Key": "2", "Value": "Filter 2" }
                "MajorComponents": [{}]
            };
            viewModel.MajorComponents([
                new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }),
                new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })
            ]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-15", "20-30"]);
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(resultData);
            });
            viewModel.getFilteredObjectData();

            expect(viewModel.isFilterCriteriaEntered()).toBeTruthy();
            expect(viewModel.PopulateFiscalYearDropDown()).toBeFalsy();
            expect(viewModel.SelectedFiscalYear()).toBe(resultData.TodaysFiscalYear);
            expect(viewModel.SavedFilterNames().length).toBe(resultData.SavedFilterNames.length);
            expect(viewModel.FiscalYears.length).toBe(resultData.FiscalYears.length);
            expect(viewModel.ObjectData().TotalBudgetExpense).toBe(resultData.ObjectData.TotalBudgetExpense);
            expect(viewModel.ObjectData().TotalActualsExpense).toBe(resultData.ObjectData.TotalActualsExpense);
            expect(viewModel.ObjectData().TotalEncumbrancesExpense).toBe(resultData.ObjectData.TotalEncumbrancesExpense);
            expect(viewModel.ObjectData().TotalBudgetRemainingExpense).toBe(resultData.ObjectData.TotalBudgetRemainingExpense);
            expect(viewModel.ObjectData().TotalPercentProcessedExpense).toBe(resultData.ObjectData.TotalPercentProcessedExpense);
            expect(viewModel.ObjectData().TotalFinancialHealthIndicatorExpense).toBe(resultData.ObjectData.TotalFinancialHealthIndicatorExpense);
            expect(viewModel.ObjectData().TotalFinancialHealthIndicatorTextExpense).toBe(resultData.ObjectData.TotalFinancialHealthIndicatorTextExpense);
            expect(viewModel.ObjectData().ColorCodeStyleExpense).toBe(resultData.ObjectData.ColorCodeStyleExpense);
            expect(viewModel.HasAnyExpense()).toBe(resultData.ObjectData.HasAnyExpense);
            expect(viewModel.HasAnyRevenue()).toBe(resultData.ObjectData.HasAnyRevenue);
            expect(viewModel.HasAnyAsset()).toBe(resultData.ObjectData.HasAnyAsset);
            expect(viewModel.HasAnyLiability()).toBe(resultData.ObjectData.HasAnyLiability);
            expect(viewModel.HasAnyFundBalance()).toBe(resultData.ObjectData.HasAnyFundBalance);
        });

        it("error() should issue the default notification if an error is returned", function () {
            var jqXhr = {
                status: 400
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.error(jqXhr, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getFilteredObjectData();
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.ColleagueFinance.unableToLoadObjectDataText, type: "error" });
        });

        it("error() should issue a 'resource not found' notification if a 404 error is returned", function () {
            var jqXhr = {
                status: 404
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.error(jqXhr, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getFilteredObjectData();
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("complete() should update the view model upon completion", function () {
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn($.fn, "notificationCenter");
            spyOn($.fn, "on");
            spyOn(viewModel, "initializeObjectRows");
            viewModel.getFilteredObjectData();

            // Check that the observables were correctly set
            expect(viewModel.IsPageLoaded()).toBeTruthy();
            expect(viewModel.IsFilterUpdating()).toBeFalsy();

            // A notification should be created if there are no cost centers.
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.ColleagueFinance.noObjectCodesToDisplayText, type: "info" });
            expect($.fn.on).toHaveBeenCalled();
        });
    });

    describe("initializeObjectRows", function () {
        it("should initialize active rows from memory", function () {
            memory.setItem("activeObjectRows", "obj-test");

            spyOn(Ellucian.ColleagueFinance, "initializeActiveRows");

            viewModel.initializeObjectRows();

            expect(Ellucian.ColleagueFinance.initializeActiveRows).toHaveBeenCalled();
        });
    });

    describe("applyFilterToCurrentTab", function () {
        it("should call getFilteredObjectData", function () {
            viewModel.isCostCentersTabActive(false);
            spyOn(viewModel, "getFilteredObjectData");
            viewModel.applyFilterToCurrentTab();
            expect(viewModel.getFilteredObjectData).toHaveBeenCalled();
        });
    });

    describe("getCostCentersViewPreference", function () {
        it("should call getDefaultFilterThenCostCenters", function () {
            viewModel.isTabKnown(true);
            viewModel.useDefaultFilter(true);
            spyOn(viewModel, "getUIPreference");
            spyOn(viewModel, "getDefaultFilterThenCostCenters");
            viewModel.getCostCentersViewPreference();
            expect(viewModel.getUIPreference).toHaveBeenCalled();
            expect(viewModel.getDefaultFilterThenCostCenters).toHaveBeenCalled();
        });

        it("should call applyFilterToCurrentTab", function () {
            viewModel.isTabKnown(true);
            viewModel.useDefaultFilter(false);
            spyOn(viewModel, "getUIPreference");
            spyOn(viewModel, "getDefaultFilter");
            spyOn(viewModel, "applyFilterToCurrentTab");
            viewModel.getCostCentersViewPreference();
            expect(viewModel.getUIPreference).toHaveBeenCalled();
            expect(viewModel.getDefaultFilter).toHaveBeenCalled();
            expect(viewModel.applyFilterToCurrentTab).toHaveBeenCalled();
        });

        it("ajax is invoked with expected url", function () {
            viewModel.isTabKnown(false);
            var expectedUrl = Ellucian.ColleagueFinance.getCostCentersViewPreferenceUrl;
            spyOn($, "ajax");
            viewModel.getCostCentersViewPreference();
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            viewModel.isTabKnown(false);
            spyOn($, "ajax");
            viewModel.getCostCentersViewPreference();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            viewModel.isTabKnown(false);
            spyOn($, "ajax");
            viewModel.getCostCentersViewPreference();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("success sets isCostCentersTabActive to the returned boolean value", function () {
            viewModel.isTabKnown(false);
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(false);
            });
            viewModel.getCostCentersViewPreference();
            expect(viewModel.isCostCentersTabActive()).toBeFalsy();
        });

        it("error sets isCostCentersTabActive to the default (true) value", function () {
            viewModel.isTabKnown(false);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            viewModel.getCostCentersViewPreference();
            expect(viewModel.isCostCentersTabActive()).toBeTruthy();
        });
    });

    describe("flatExportData", function () {
        it("flatExportData should flatten and export object data", function () {
            var resultData = {
                "ObjectData": {
                    "ExpenseObjectCodes": [
                    {
                        "GlClass": "5",
                        "Id": "55556",
                        "Name": "Operating Fund",
                        "TotalBudget": "",
                        "TotalActuals": "",
                        "TotalEncumbrances": "",
                        "TotalBudgetRemaining": "",
                        "TotalPercentProcessed": "",
                        "FinancialHealthIndicator": "",
                        "FinancialHealthIndicatorText": "",
                        "ColorCodeStyle": "",
                        "GlAccounts": [
                            {
                                "IsPooledAccount": true,
                                "IsUmbrellaVisible": true,
                                "PrimaryGlAccount": {
                                    "FormattedGlAccount": "11-01-00-10-33333-55556",
                                    "FormattedGlClass": "Expense",
                                    "Description": "Object 55556",
                                    "Budget": "$1000.00",
                                    "Actuals": "$200.00",
                                    "Encumbrances": "$200.00",
                                    "BudgetRemaining": "$600.00",
                                    "PercentProcessed": "$800.00",
                                    "FinancialHealthIndicator": "group-icon-green",
                                    "FinancialHealthIndicatorText": "Under Budget",
                                    "PoolTypeCssClass": "umbrella"
                                },
                                "Poolees": [
                                    {
                                        "FormattedGlAccount": "11-01-00-10-33333-51111",
                                        "FormattedGlClass": "Expense",
                                        "Description": "Object 51111",
                                        "_Budget": 1000.00,
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    }, {
                                        "FormattedGlAccount": "11-01-00-10-33333-52222",
                                        "FormattedGlClass": "Expense",
                                        "Description": "Object 52222",
                                        "_Budget": 1000.00,
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    }, {
                                        "FormattedGlAccount": "11-01-00-10-33333-53333",
                                        "FormattedGlClass": "Expense",
                                        "Description": "Object 53333",
                                        "_Budget": 1000.00,
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    },
                                ]
                            },
                            {
                                "IsPooledAccount": false,
                                "IsUmbrellaVisible": true,
                                "PrimaryGlAccount": {
                                    "FormattedGlAccount": "11-01-00-10-33333-55555",
                                    "FormattedGlClass": "Expense",
                                    "Description": "Object 55555",
                                    "Budget": "$1000.00",
                                    "Actuals": "$200.00",
                                    "Encumbrances": "$200.00",
                                    "BudgetRemaining": "$600.00",
                                    "PercentProcessed": "$800.00",
                                    "FinancialHealthIndicator": "group-icon-green",
                                    "FinancialHealthIndicatorText": "Under Budget",
                                    "PoolTypeCssClass": "non-pooled-account"
                                },
                                "Poolees": []
                            }
                        ]
                    }],
                    "RevenueObjectCodes": [],
                    "AssetObjectCodes": [],
                    "LiabilityObjectCodes": [],
                    "FundBalanceObjectCodes": [],
                    "TotalBudgetExpense": "$2,000.00",
                    "TotalActualsExpense": "$2,000.00",
                    "TotalEncumbrancesExpense": "$2,000.00",
                    "TotalBudgetRemainingExpense": "$0.00",
                    "TotalPercentProcessedExpense": "100 %",
                    "TotalFinancialHealthIndicatorExpense": "group-icon-yellow",
                    "TotalFinancialHealthIndicatorTextExpense": "Nearing Budget",
                    "ColorCodeStyleExpense": "remaining-black",
                    "TotalBudgetRevenue": "$2,000.00",
                    "TotalActualsRevenue": "$2,000.00",
                    "TotalEncumbrancesRevenue": "$2,000.00",
                    "TotalBudgetRemainingRevenue": "$0.00",
                    "TotalPercentProcessedRevenue": "100 %",
                    "ColorCodeStyleRevenue": "remaining-black",
                    "HasAnyExpense": true,
                    "HasAnyRevenue": false,
                    "HasAnyAsset": false,
                    "HasAnyLiability": false,
                    "HasAnyFundBalance": false
                },
                "TodaysFiscalYear": "2016",
                "FiscalYears": [
                    {
                        "FiscalYear": "2016",
                        "FiscalYearDescription": "FY2016"
                    }
                ],
                "SavedFilterNames": [{}],
                "MajorComponents": [{}]
            };
            viewModel = new costCenterSummaryViewModel("#objectview");
            viewModel.MajorComponents([
                new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }),
                new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })
            ]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-15", "20-30"]);

            viewModel.ObjectData(resultData.ObjectData);

            var actualResultData = viewModel.flatExportData();

            for (var i = 0; i < actualResultData.length; i++) {
                var actualResultObject = actualResultData[i];

                // Get the corresponding "expected" result data object
                for (var j = 0; j < resultData.ObjectData.ExpenseObjectCodes.length; j++) {
                    var expenseObjectCode = resultData.ObjectData.ExpenseObjectCodes[j];

                    // Check the primary accounts
                    for (var k = 0; k < expenseObjectCode.GlAccounts.length; k++) {
                        var expectedGlAccount = expenseObjectCode.GlAccounts[k];
                        var primaryAccount = expectedGlAccount.PrimaryGlAccount;

                        // Check the primary account
                        if (primaryAccount.FormattedGlAccount === actualResultObject.GlAccount) {
                            expect(primaryAccount.FormattedGlClass).toBe(actualResultObject.GlClass);
                            expect(primaryAccount.Description).toBe(actualResultObject.Description);
                            expect(primaryAccount.Budget).toBe(actualResultObject.Budget);
                            expect(primaryAccount.Actuals).toBe(actualResultObject.Actuals);
                            expect(primaryAccount.Encumbrances).toBe(actualResultObject.Encumbrances);
                            expect(primaryAccount.BudgetRemaining).toBe(actualResultObject.Remaining);
                            expect(primaryAccount.PercentProcessed).toBe(actualResultObject.Processed);
                        }

                        // Check the poolee accounts
                        if (expectedGlAccount.IsPooledAccount) {
                            for (var x = 0; x < expectedGlAccount.Poolees.length; x++) {
                                var pooleeAccount = expectedGlAccount.Poolees[x];
                                if (pooleeAccount.FormattedGlAccount === actualResultObject.GlAccount) {
                                    expect(pooleeAccount.FormattedGlClass).toBe(actualResultObject.GlClass);
                                    expect(pooleeAccount.Description).toBe(actualResultObject.Description);
                                    expect(pooleeAccount.Budget).toBe(actualResultObject.Budget);
                                    expect(pooleeAccount.Actuals).toBe(actualResultObject.Actuals);
                                    expect(pooleeAccount.Encumbrances).toBe(actualResultObject.Encumbrances);
                                    expect(actualResultObject.Remaining).toBe('');
                                    expect(actualResultObject.Processed).toBe('');
                                }
                            }
                        }
                    }
                }
            }
        });

        it("flatExportData should flatten and export masked object data", function () {
            var resultData = {
                "ObjectData": {
                    "ExpenseObjectCodes": [
                    {
                        "GlClass": "5",
                        "Id": "55556",
                        "Name": "Operating Fund",
                        "TotalBudget": "",
                        "TotalActuals": "",
                        "TotalEncumbrances": "",
                        "TotalBudgetRemaining": "",
                        "TotalPercentProcessed": "",
                        "FinancialHealthIndicator": "",
                        "FinancialHealthIndicatorText": "",
                        "ColorCodeStyle": "",
                        "GlAccounts": [
                            {
                                "IsPooledAccount": true,
                                "IsUmbrellaVisible": false,
                                "PrimaryGlAccount": {
                                    "FormattedGlAccount": "11-01-00-10-33333-55556",
                                    "FormattedGlClass": "Expense",
                                    "Description": "Object 55556",
                                    "Budget": "$1000.00",
                                    "Actuals": "$200.00",
                                    "Encumbrances": "$200.00",
                                    "BudgetRemaining": "$600.00",
                                    "PercentProcessed": "$800.00",
                                    "FinancialHealthIndicator": "group-icon-green",
                                    "FinancialHealthIndicatorText": "Under Budget",
                                    "PoolTypeCssClass": "umbrella"
                                },
                                "Poolees": [
                                    {
                                        "FormattedGlAccount": "11-01-00-10-33333-51111",
                                        "FormattedGlClass": "Expense",
                                        "Description": "Object 51111",
                                        "_Budget": 1000.00,
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    }, {
                                        "FormattedGlAccount": "11-01-00-10-33333-52222",
                                        "FormattedGlClass": "Expense",
                                        "Description": "Object 52222",
                                        "_Budget": 1000.00,
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    }, {
                                        "FormattedGlAccount": "11-01-00-10-33333-53333",
                                        "FormattedGlClass": "Expense",
                                        "Description": "Object 53333",
                                        "_Budget": 1000.00,
                                        "Budget": "$1000.00",
                                        "Actuals": "$200.00",
                                        "Encumbrances": "$200.00",
                                        "BudgetRemaining": "$600.00",
                                        "PercentProcessed": "$800.00",
                                        "FinancialHealthIndicator": "group-icon-green",
                                        "FinancialHealthIndicatorText": "Under Budget",
                                        "PoolTypeCssClass": "poolee"
                                    },
                                ]
                            },
                            {
                                "IsPooledAccount": false,
                                "IsUmbrellaVisible": false,
                                "PrimaryGlAccount": {
                                    "FormattedGlAccount": "11-01-00-10-33333-55555",
                                    "FormattedGlClass": "Expense",
                                    "Description": "Object 55555",
                                    "Budget": "$1000.00",
                                    "Actuals": "$200.00",
                                    "Encumbrances": "$200.00",
                                    "BudgetRemaining": "$600.00",
                                    "PercentProcessed": "$800.00",
                                    "FinancialHealthIndicator": "group-icon-green",
                                    "FinancialHealthIndicatorText": "Under Budget",
                                    "PoolTypeCssClass": "non-pooled-account"
                                },
                                "Poolees": []
                            }
                        ]
                    }],
                    "RevenueObjectCodes": [],
                    "AssetObjectCodes": [],
                    "LiabilityObjectCodes": [],
                    "FundBalanceObjectCodes": [],
                    "TotalBudgetExpense": "$2,000.00",
                    "TotalActualsExpense": "$2,000.00",
                    "TotalEncumbrancesExpense": "$2,000.00",
                    "TotalBudgetRemainingExpense": "$0.00",
                    "TotalPercentProcessedExpense": "100 %",
                    "TotalFinancialHealthIndicatorExpense": "group-icon-yellow",
                    "TotalFinancialHealthIndicatorTextExpense": "Nearing Budget",
                    "ColorCodeStyleExpense": "remaining-black",
                    "TotalBudgetRevenue": "$2,000.00",
                    "TotalActualsRevenue": "$2,000.00",
                    "TotalEncumbrancesRevenue": "$2,000.00",
                    "TotalBudgetRemainingRevenue": "$0.00",
                    "TotalPercentProcessedRevenue": "100 %",
                    "ColorCodeStyleRevenue": "remaining-black",
                    "HasAnyExpense": true,
                    "HasAnyRevenue": false,
                    "HasAnyAsset": false,
                    "HasAnyLiability": false,
                    "HasAnyFundBalance": false
                },
                "TodaysFiscalYear": "2016",
                "FiscalYears": [
                    {
                        "FiscalYear": "2016",
                        "FiscalYearDescription": "FY2016"
                    }
                ],
                "SavedFilterNames": [{}],
                "MajorComponents": [{}]
            };
            viewModel = new costCenterSummaryViewModel("#objectview");
            viewModel.MajorComponents([
                new generalLedgerComponentModel({ ComponentName: "Fund", ComponentLength: "2", StartPosition: 0 }),
                new generalLedgerComponentModel({ ComponentName: "Program", ComponentLength: "2", StartPosition: 0 })
            ]);
            viewModel.MajorComponents()[0].tokenizedFilterCriteria(["11", "12", "13-15", "20-30"]);

            viewModel.ObjectData(resultData.ObjectData);

            var actualResultData = viewModel.flatExportData();

            for (var i = 0; i < actualResultData.length; i++) {
                var actualResultObject = actualResultData[i];

                // Get the corresponding "expected" result data object
                for (var j = 0; j < resultData.ObjectData.ExpenseObjectCodes.length; j++) {
                    var expenseObjectCode = resultData.ObjectData.ExpenseObjectCodes[j];

                    // Check the primary accounts
                    for (var k = 0; k < expenseObjectCode.GlAccounts.length; k++) {
                        var expectedGlAccount = expenseObjectCode.GlAccounts[k];
                        var primaryAccount = expectedGlAccount.PrimaryGlAccount;

                        // Check the primary account
                        if (!expectedGlAccount.IsUmbrellaVisible && primaryAccount.FormattedGlAccount === actualResultObject.GlAccount) {
                            expect(primaryAccount.FormattedGlClass).toBe(actualResultObject.GlClass);
                            expect(primaryAccount.Description).toBe(actualResultObject.Description);
                            expect(Ellucian.ColleagueFinance.maskString).toBe(actualResultObject.Budget);
                            expect(Ellucian.ColleagueFinance.maskString).toBe(actualResultObject.Actuals);
                            expect(Ellucian.ColleagueFinance.maskString).toBe(actualResultObject.Encumbrances);
                            expect(Ellucian.ColleagueFinance.maskString).toBe(actualResultObject.Remaining);
                            expect(Ellucian.ColleagueFinance.maskString).toBe(actualResultObject.Processed);
                        }
                    }
                }
            }
        });
    });

    describe("toggleObjectAccordianState", function () {
        it("adds new Id", function () {
            viewModel = new costCenterSummaryViewModel("#objectview");

            viewModel.toggleObjectAccordianState({ Id: "1" });
            expect(viewModel.expanding()).toBeTruthy();

            setTimeout(function () {
                expect(viewModel.expandedObjects().length).toBe(1);
                expect(viewModel.expanding()).toBeFalsy();
            }, 30);

        });

        it("removes duplicate Id", function () {
            viewModel = new costCenterSummaryViewModel("#objectview");
            viewModel.expandedObjects.push("1");

            viewModel.toggleObjectAccordianState({ Id: "1" });
            expect(viewModel.collapsing()).toBeTruthy();

            setTimeout(function () {
                expect(viewModel.expandedObjects().length).toBe(0);
                expect(viewModel.collapsing()).toBeFalsy();
            }, 60);
        });
    });

    describe("notifyUserAndExpandCollapse", function () {
        it("should collapse sub-accordians when collapsing top level accordian", function () {
            var currentState = ko.observable(true);
            viewModel = new costCenterSummaryViewModel("#objectview");

            spyOn(viewModel, "invertBooleanObservable");

            viewModel.notifyUserAndExpandCollapse(currentState);
            expect(viewModel.collapsing()).toBeTruthy();

            setTimeout(function () {
                expect(viewModel.expandedObjects().length).toBe(0);
                expect(viewModel.invertBooleanObservable).toHaveBeenCalledWith(currentState);
                expect(viewModel.collapsing()).toBeFalsy();
            }, 30);
        });

        it("should expand top level accordian", function () {
            var currentState = ko.observable(false);
            viewModel = new costCenterSummaryViewModel("#objectview");

            spyOn(viewModel, "invertBooleanObservable");

            viewModel.notifyUserAndExpandCollapse(currentState);
            expect(viewModel.expanding()).toBeTruthy();

            setTimeout(function () {
                expect(viewModel.expandedObjects().length).toBe(0);
                expect(viewModel.invertBooleanObservable).toHaveBeenCalledWith(currentState);
                expect(viewModel.expanding()).toBeFalsy();
            }, 30);
        });
    });

    describe("invertBooleanObservable", function () {
        it("inverts the state of the observable argument", function () {
            var observable = ko.observable(false);

            viewModel = new costCenterSummaryViewModel("#objectview");

            viewModel.invertBooleanObservable(observable);

            expect(observable()).toBeTruthy
        });
    });

    describe("toggleCodes", function () {
        it("toggleAssetCodes calls notifyUserAndExpandCollapse", function () {
            viewModel = new costCenterSummaryViewModel("#objectview");

            spyOn(viewModel, "notifyUserAndExpandCollapse");

            viewModel.toggleAssetCodes();

            expect(viewModel.notifyUserAndExpandCollapse).toHaveBeenCalledWith(viewModel.DisplayAssetCodes);
        });

        it("toggleLiabilityCodes calls notifyUserAndExpandCollapse", function () {
            viewModel = new costCenterSummaryViewModel("#objectview");

            spyOn(viewModel, "notifyUserAndExpandCollapse");

            viewModel.toggleLiabilityCodes();

            expect(viewModel.notifyUserAndExpandCollapse).toHaveBeenCalledWith(viewModel.DisplayLiabilityCodes);
        });

        it("toggleFundBalanceCodes calls notifyUserAndExpandCollapse", function () {
            viewModel = new costCenterSummaryViewModel("#objectview");

            spyOn(viewModel, "notifyUserAndExpandCollapse");

            viewModel.toggleFundBalanceCodes();

            expect(viewModel.notifyUserAndExpandCollapse).toHaveBeenCalledWith(viewModel.DisplayFundBalanceCodes);
        });

        it("toggleRevenueCodes calls notifyUserAndExpandCollapse", function () {
            viewModel = new costCenterSummaryViewModel("#objectview");

            spyOn(viewModel, "notifyUserAndExpandCollapse");

            viewModel.toggleRevenueCodes();

            expect(viewModel.notifyUserAndExpandCollapse).toHaveBeenCalledWith(viewModel.DisplayRevenueCodes);
        });

        it("toggleExpenseCodes calls notifyUserAndExpandCollapse", function () {
            viewModel = new costCenterSummaryViewModel("#objectview");

            spyOn(viewModel, "notifyUserAndExpandCollapse");

            viewModel.toggleExpenseCodes();

            expect(viewModel.notifyUserAndExpandCollapse).toHaveBeenCalledWith(viewModel.DisplayExpenseCodes);
        });
    });
});
