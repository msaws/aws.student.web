﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
describe("glTransactionViewModel", function () {
    var viewModel;
    var data = {
        "Source": "EP",
        "ReferenceNumber": "P000001",
        "DocumentId": "0001234"
    };

    beforeEach(function () {
        
    });

    afterEach(function () {
        
    })

    it("should call ko.mapping.fromJS if projectMapping is undefined and glActivityDetailMapping is defined", function () {
        projectMapping = undefined;
        spyOn(ko.mapping, "fromJS");
        viewModel = new glTransactionViewModel(data);

        expect(ko.mapping.fromJS).toHaveBeenCalledWith(data, glActivityDetailMapping, viewModel);
    });

    describe("isLinkable", function () {
        it("should return true if the source is a voucher", function () {
            data.Source = "PJ";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.isLinkable()).toBeTruthy();
        });

        it("should return true if the source is a journal entry", function () {
            data.Source = "JE";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.isLinkable()).toBeTruthy();
        });

        it("should return true if the source is a requisition", function () {
            data.Source = "ER";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.isLinkable()).toBeTruthy();
        });

        it("should return true if the source is an encumbrance and document is a recurring voucher", function () {
            data.Source = "EP";
            data.ReferenceNumber = "RV0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.isLinkable()).toBeTruthy();
        });

        it("should return true if the source is an encumbrance and document is a purchase order", function () {
            data.Source = "EP";
            data.ReferenceNumber = "P0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.isLinkable()).toBeTruthy();
        });

        it("should return true if the source is an encumbrance and document is a blanket purchase order", function () {
            data.Source = "EP";
            data.ReferenceNumber = "B0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.isLinkable()).toBeTruthy();
        });
    });

    describe("documentLink", function () {
        it("should return a link to a voucher if the source is a voucher", function () {
            data.Source = "PJ";
            data.ReferenceNumber = "V0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.documentLink()).toBe(Ellucian.ColleagueFinance.getVoucherUrl + "/" + data.ReferenceNumber);
        });

        it("should return a link to a journal entry if the source is a journal entry", function () {
            data.Source = "JE";
            data.ReferenceNumber = "J0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.documentLink()).toBe(Ellucian.ColleagueFinance.getJournalEntryUrl + "/" + data.ReferenceNumber);
        });

        it("should return a link to a requisition if the source is a requisition", function () {
            data.Source = "ER";
            data.DocumentId = "0000002";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.documentLink()).toBe(Ellucian.ColleagueFinance.getRequisitionUrl + "/" + data.DocumentId);
        });

        it("should return a link to a recurring voucher if the source is a reference number begins with 'RV'", function () {
            data.Source = "EP";
            data.ReferenceNumber = "RV0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.documentLink()).toBe(Ellucian.ColleagueFinance.getRecurringVoucherUrl + "/" + data.ReferenceNumber);
        });

        it("should return a link to a purchase order if the source is a reference number begins with 'P'", function () {
            data.Source = "EP";
            data.ReferenceNumber = "P0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.documentLink()).toBe(Ellucian.ColleagueFinance.getPurchaseOrderUrl + "/" + data.DocumentId);
        });

        it("should return a link to a blanket purchase order if the source is a reference number begins with 'B'", function () {
            data.Source = "EP";
            data.ReferenceNumber = "B0000001";
            viewModel = new glTransactionViewModel(data);
            expect(viewModel.documentLink()).toBe(Ellucian.ColleagueFinance.getBlanketPurchaseOrderUrl + "/" + data.DocumentId);
        });
    });
});