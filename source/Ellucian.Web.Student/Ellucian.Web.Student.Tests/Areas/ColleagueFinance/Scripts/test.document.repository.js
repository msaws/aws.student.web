﻿function testDocumentRepository() {
    var baseFinanceDocumentData = {
        "IsVoucher": true,
        "IsPurchaseOrder": false,
        "IsBlanketPurchaseOrder": false,
        "IsRequisition": false,
        "IsJournalEntry": false,
        "IsRecurringVoucher":false,

        // What's the deal with this????? I needed to define this even though it's a function in document.model.js.
        "CompleteDocumentUrl": '?' + encodeURIComponent(glDescription) + '&' + projectId + '&' + encodeURIComponent(glNumber) + '&' + encodeURIComponent(lineItemCode) + '&' + encodeURIComponent(lineItemDescription),
    };

    var accountsPayablePurchasingDocumentData = {
        "LineItems": [
            {
                "Price": 150.00,
                "GlDistributions": [
                    {
                        "FormattedGlAccount": "11-00-01-02-20601-51000",
                        "Amount": 500.00,
                    },
                    {
                        "FormattedGlAccount": "11-00-01-02-20601-51001",
                        "Amount": 1000.00,
                    },
                    {
                        "FormattedGlAccount": "##-##-##-##-#####-#####",
                        "Amount": null,
                    },
                ],
                "TaxDistributions": [
                    {
                        "TaxCode": "VA",
                        "Amount": 150.00,
                    },
                    {
                        "TaxCode": "MD",
                        "Amount": 200.00,
                    },
                ],
            },
        ],
    };

    var ledgerEntryDocumentData = {
        "LineItems": [
            {
                "FormattedGlAccount": "11-00-01-02-20601-51000",
                "Debit": 0,
                "DebitDisplay": "$0.00",
                "Credit": 250.11,
                "CreditDisplay": "$250.11",
            },
            {
                "FormattedGlAccount": "11-00-01-02-20601-51001",
                "Debit": 150.22,
                "DebitDisplay": "$150.22",
                "Credit": 0,
                "CreditDisplay": "$0.00",
            },
            {
                "FormattedGlAccount": "11-00-01-02-20601-51001",
                "Debit": 0,
                "DebitDisplay": "$0.00",
                "Credit": 0,
                "CreditDisplay": "$0.00",
            },
        ],
    };

    var voucherData = {
        "AssociatedDocument": "000325",
        "IsAssociatedDocumentPurchaseOrder": true,
        "IsAssociatedDocumentBlanketPurchaseOrder": false,
        "IsAssociatedDocumentRecurringVoucher": false,
    };

    var blanketPurchaseOrderData = {
        "GlDistributions": [
            {
                "FormattedGlAccount": "11-00-01-02-20601-51000",
            },
            {
                "FormattedGlAccount": "11-00-01-02-20601-51001",
            },
        ],
    };

    var recurringVoucherData = {
        "Schedules": [
            {
                "Amount": 100.00,
            },
            {
                "Amount": 200.00,
            },
        ],
    };

    this.buildVoucher = function () {
        var voucher = {};
        for (var attributeName in baseFinanceDocumentData) { voucher[attributeName] = baseFinanceDocumentData[attributeName]; }
        for (var attributeName in accountsPayablePurchasingDocumentData) { voucher[attributeName] = accountsPayablePurchasingDocumentData[attributeName]; }
        for (var attributeName in voucherData) { voucher[attributeName] = voucherData[attributeName]; }

        return voucher;
    }

    this.buildRecurringVoucher = function () {
        var rcVoucher = {};
        for (var attributeName in baseFinanceDocumentData) { rcVoucher[attributeName] = baseFinanceDocumentData[attributeName]; }
        for (var attributeName in accountsPayablePurchasingDocumentData) { rcVoucher[attributeName] = accountsPayablePurchasingDocumentData[attributeName]; }
        for (var attributeName in recurringVoucherData) { rcVoucher[attributeName] = recurringVoucherData[attributeName]; }

        // Change a few things before we finish
        rcVoucher.IsVoucher = false;
        rcVoucher.IsRecurringVoucher = true;
        rcVoucher.IsProcurementDocument = true;
        rcVoucher.IsLedgerEntryDocument = false;

        return rcVoucher;
    }

    this.buildPurchaseOrder = function () {
        var purchaseOrder = {};
        for (var attributeName in baseFinanceDocumentData) { purchaseOrder[attributeName] = baseFinanceDocumentData[attributeName]; }
        for (var attributeName in accountsPayablePurchasingDocumentData) { purchaseOrder[attributeName] = accountsPayablePurchasingDocumentData[attributeName]; }

        // Change a few things before we finish
        purchaseOrder.IsVoucher = false;
        purchaseOrder.IsPurchaseOrder = true;
        purchaseOrder.IsProcurementDocument = true;
        purchaseOrder.IsLedgerEntryDocument = false;

        return purchaseOrder;
    }

    this.buildRequisition = function () {
        var requisition = {};
        for (var attributeName in baseFinanceDocumentData) { requisition[attributeName] = baseFinanceDocumentData[attributeName]; }
        for (var attributeName in accountsPayablePurchasingDocumentData) { requisition[attributeName] = accountsPayablePurchasingDocumentData[attributeName]; }

        // Change a few things before we finish
        requisition.IsVoucher = false;
        requisition.IsRequisition = true;
        requisition.IsProcurementDocument = true;
        requisition.IsLedgerEntryDocument = false;

        return requisition;
    }

    this.buildBlanketPurchaseOrder = function () {
        var bpo = {};
        for (var attributeName in baseFinanceDocumentData) { bpo[attributeName] = baseFinanceDocumentData[attributeName]; }
        for (var attributeName in accountsPayablePurchasingDocumentData) { bpo[attributeName] = accountsPayablePurchasingDocumentData[attributeName]; }
        for (var attributeName in blanketPurchaseOrderData) { bpo[attributeName] = blanketPurchaseOrderData[attributeName]; }

        // Change a few things before we finish
        bpo.IsVoucher = false;
        bpo.IsBlanketPurchaseOrder = true;
        bpo.IsProcurementDocument = true;
        bpo.IsLedgerEntryDocument = false;

        return bpo;
    }

    this.buildJournalEntry = function () {
        var journalEntry = {};
        for (var attributeName in baseFinanceDocumentData) { journalEntry[attributeName] = baseFinanceDocumentData[attributeName]; }
        for (var attributeName in ledgerEntryDocumentData) { journalEntry[attributeName] = ledgerEntryDocumentData[attributeName]; }

        // Change a few things before we finish
        journalEntry.IsVoucher = false;
        journalEntry.IsJournalEntry = true;
        journalEntry.IsProcurementDocument = false;
        journalEntry.IsLedgerEntryDocument = true;

        return journalEntry;
    }
}