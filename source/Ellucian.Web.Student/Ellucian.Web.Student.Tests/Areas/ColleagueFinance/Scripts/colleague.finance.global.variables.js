﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates

var getGlTransactionsAsync = "http://localhost/CostCenter/GetGlTransactionsAsync",
    getFilteredCostCentersActionUrl = "http://localhost/CostCenters/GetFilteredCostCentersAsync",
    getFilteredCostCenterActionUrl = "http://localhost/CostCenters/GetFilteredCostCenterAsync",
    getSavedFilterCriteriaUrl = "http://localhost/CostCenters/GetSavedFilterCriteriaAsync",
    resourceNotFoundText = "Resource not found.",
    deleteSavedFilterUrl = "http://localhost/CostCenters/DeleteSavedFilterAsync",
    postSaveFilterUrl = "http://localhost/CostCenters/AddOrUpdateSavedFilterAsync",
    getMajorComponentsActionUrl = "http://localhost/CostCenters/GetMajorComponentsAsync",
    costCenterId = "10110020601",
    fakeGlNumber = "1001010101001",
    contentType,
    unableToLoadCostCenterText = "Unable to load cost center.",
    unableToLoadGlActivityText = "Unable to load GL Activity.",
    noCostCenterToDisplayText = "No cost center to display.",
    noCostCenterNotification = "No Cost centers to display.",
    noCostCentersToDisplayText = "No cost centers to display.",
    unableToLoadCostCentersText = "Unable to load cost centers.",
    unableToLoadFilterMessage = "Unable to load saved filter.",
    filterSavedMessage = "Your filter has been saved.",
    unableToSaveFilterMessage = "Unable to save your filter",
    filterDeletedSuccessMessage = "Filter deleted.",
    majorComponentLengthShortValidationErrorMessage = "value is too short",
    majorComponentLengthValidationErrorMessage = "value is too long",
    costCenterBarGraphTitle = "Expenses vs Budget",
    budgetBarText = " of ",
    getVoucherUrl = "http://localhost/Voucher",
    getJournalEntryUrl = "http://localhost/JournalEntry",
    getRequisitionUrl = "http://localhost/Requisition",
    getRecurringVoucherUrl = "http://localhost/RecurringVoucher",
    getPurchaseOrderUrl = "http://localhost/PurchaseOrder",
    getBlanketPurchaseOrderUrl = "http://localhost/BlanketPurchaseOrder",
    getPurchaseOrderAsyncUrl = "http://localhost/GetPurchaseOrderAsync";

// Initialize the ColleagueFinance namespace object for use in ColleagueFinance module.
var Ellucian = Ellucian || {};
Ellucian.ColleagueFinance = Ellucian.ColleagueFinance || {};

Ellucian.ColleagueFinance.filterDeleteFailedMessage = "Unable to delete filter";
Ellucian.ColleagueFinance.setPreferenceFailedMessage = "failed to save UI pref";
Ellucian.ColleagueFinance.defaultSelectedSavedFilterLabel = "Selected Filter";
Ellucian.ColleagueFinance.noneLabel = "None";
Ellucian.ColleagueFinance.setDefaultFilterAsyncUrl = "http://localhost/CostCenters/SetDefaultFilterAsync";
Ellucian.ColleagueFinance.getDefaultFilterAsyncUrl = "http://localhost/CostCenters/GetDefaultFilterAsync";
Ellucian.ColleagueFinance.defaultFilterSavedMessage = "Your default filter was saved.";
Ellucian.ColleagueFinance.defaultFilterSaveFailedMessage = "Your default filter was not saved.";
Ellucian.ColleagueFinance.getUIPreferenceUrl = "http://localhost/CostCenters/GetUIPreference";
Ellucian.ColleagueFinance.setUIPreferenceUrl = "http://localhost/CostCenters/SetUIPreference";
Ellucian.ColleagueFinance.maskString = "**********";