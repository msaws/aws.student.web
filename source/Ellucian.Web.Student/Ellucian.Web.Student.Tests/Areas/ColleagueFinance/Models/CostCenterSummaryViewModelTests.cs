﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.ColleagueFinance;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// Test the CostCenterSummaryViewModel class.
    /// </summary>
    [TestClass]
    public class CostCenterSummaryViewModelTests
    {
        #region Initialize and Cleanup
        private TestCostCenterDtoRepository testRepository;
        private List<CostCenter> costCenterDtos;
        private List<string> fiscalYears;
        private List<GeneralLedgerComponent> glComponentDtos;

        [TestInitialize]
        public void Initialize()
        {
            testRepository = new TestCostCenterDtoRepository();
            costCenterDtos = testRepository.GetCostCenters();
            fiscalYears = new List<string>()
            {
                "2016",
                "2015",
                "2014",
                "2013",
                "2012",
                "2011",
            };
            glComponentDtos = new List<GeneralLedgerComponent>()
            {
                new GeneralLedgerComponent()
                {
                    ComponentName = "FUND",
                    StartPosition = 1,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "PROGRAM",
                    StartPosition = 4,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "ACTIVITY",
                    StartPosition = 7,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "LOCATION",
                    StartPosition = 10,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "DEPARTMENT",
                    StartPosition = 13,
                    ComponentLength = 5
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "OBJECT",
                    StartPosition = 19,
                    ComponentLength = 5
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            testRepository = null;
            costCenterDtos = null;
            fiscalYears = null;
            glComponentDtos = null;
        }
        #endregion

        [TestMethod]
        public void Constructor()
        {
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(costCenterSummaryVM.CostCenters is List<BaseCostCenterViewModel>);
            Assert.AreEqual("", costCenterSummaryVM.TodaysFiscalYear);
            Assert.AreEqual(costCenterDtos.Count, costCenterSummaryVM.CostCenters.Count);
            foreach (var costCenterDto in costCenterDtos)
            {
                var costCenterVM = costCenterSummaryVM.CostCenters.Where(x =>
                    x.CostCenterId == costCenterDto.Id).First();
                Assert.AreEqual(costCenterDto.Name, costCenterVM.Name);
                Assert.AreEqual(costCenterDto.UnitId, costCenterVM.UnitId);
                Assert.AreEqual(costCenterDto.TotalBudget.ToString("C"), costCenterVM.TotalBudget);
                Assert.AreEqual(costCenterDto.TotalActuals.ToString("C"), costCenterVM.TotalActuals);
                Assert.AreEqual(costCenterDto.TotalEncumbrances.ToString("C"), costCenterVM.TotalEncumbrances);
            }

            Assert.IsTrue(costCenterSummaryVM.FiscalYears is List<FiscalYearFilterViewModel>);
            Assert.AreEqual(fiscalYears.Count, costCenterSummaryVM.FiscalYears.Count);

            var firstFiscalYear = costCenterSummaryVM.FiscalYears.First();
            Assert.AreEqual(fiscalYears.First(), firstFiscalYear.FiscalYear);

            for (int i = 0; i < fiscalYears.Count; i++)
            {
                var filterVM = costCenterSummaryVM.FiscalYears.Where(x => x.FiscalYear == fiscalYears.ElementAt(i)).First();
                Assert.AreEqual(fiscalYears.ElementAt(i), filterVM.FiscalYear);
                Assert.AreEqual("FY" + fiscalYears.ElementAt(i), filterVM.FiscalYearDescription);
            }

            // The list of GL components should be a list of GeneralLedgerComponentViewModel objects and should be empty.
            Assert.IsTrue(costCenterSummaryVM.MajorComponents is List<GeneralLedgerComponentViewModel>);
            Assert.IsFalse(costCenterSummaryVM.MajorComponents.Any());
            Assert.AreEqual(costCenterDtos.Any(d => d.CostCenterSubtotals.Any(s => s.GlClass == GlClass.Revenue)), costCenterSummaryVM.HasAnyRevenue);
            Assert.AreEqual(costCenterDtos.Any(d => d.CostCenterSubtotals.Any(s => s.GlClass == GlClass.Expense)), costCenterSummaryVM.HasAnyExpense);

        }

        [TestMethod]
        public void Constructor_CostCenterDtosIsNull()
        {
            var costCenterSummaryVM = new CostCenterSummaryViewModel(null, fiscalYears, null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(costCenterSummaryVM.CostCenters is List<BaseCostCenterViewModel>);
            Assert.AreEqual(0, costCenterSummaryVM.CostCenters.Count);
        }

        [TestMethod]
        public void Constructor_CostCenterDtoIsNull()
        {
            costCenterDtos.Add(null);
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(costCenterSummaryVM.CostCenters is List<BaseCostCenterViewModel>);
            Assert.AreEqual(costCenterDtos.Count - 1, costCenterSummaryVM.CostCenters.Count);
        }

        [TestMethod]
        public void Constructor_FiscalYearsIsNull()
        {
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, null, null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(costCenterSummaryVM.CostCenters is List<BaseCostCenterViewModel>);
            Assert.AreEqual(0, costCenterSummaryVM.FiscalYears.Count);
        }

        [TestMethod]
        public void Constructor_FiscalYearIsNull()
        {
            fiscalYears.Add(null);
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);

            Assert.IsTrue(costCenterSummaryVM.FiscalYears is List<FiscalYearFilterViewModel>);
            Assert.AreEqual(fiscalYears.Count - 1, costCenterSummaryVM.FiscalYears.Count);
        }

        [TestMethod]
        public void Constructor_FiscalYearIsEmpty()
        {
            fiscalYears.Add(string.Empty);
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);

            Assert.IsTrue(costCenterSummaryVM.FiscalYears is List<FiscalYearFilterViewModel>);
            Assert.AreEqual(fiscalYears.Count - 1, costCenterSummaryVM.FiscalYears.Count);
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_Success()
        {
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.AreEqual(glComponentDtos.Count(), costCenterSummaryVM.MajorComponents.Count(), "The number of major components should be the same.");
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_CallTwice()
        {
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.AreEqual(glComponentDtos.Count(), costCenterSummaryVM.MajorComponents.Count(), "The number of major components should be the same.");
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_NullComponentInList()
        {
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);
            glComponentDtos.Add(null);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.AreEqual(glComponentDtos.Count() - 1, costCenterSummaryVM.MajorComponents.Count(), "The number of major components should be the same.");
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_ComponentListIsNull()
        {
            var costCenterSummaryVM = new CostCenterSummaryViewModel(costCenterDtos, fiscalYears, null);
            glComponentDtos = null;
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.IsTrue(costCenterSummaryVM.MajorComponents.Count() == 0, "The number of major components should be the same.");
        }
    }
}
