﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class GlObjectCodeGlAccountSequenceViewModelTests
    {
        [TestMethod]
        public void NonPooledConstructor()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = 55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var accountVM = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense, true);
            var vm = new GlObjectCodeGlAccountSequenceViewModel(accountVM);

            Assert.AreEqual(accountVM, vm.PrimaryGlAccount);
            Assert.AreEqual(false, vm.IsPooledAccount);
            Assert.AreEqual(true, vm.IsUmbrellaVisible);
            Assert.AreEqual(dto.FormattedGlAccount, vm.FormattedGlAccount);
            Assert.AreEqual(0, vm.Poolees.Count);
        }

        [TestMethod]
        public void PooledConstructor()
        {
            var poolDto = new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                {
                    new GlObjectCodeGlAccount() 
                    {
                        PoolType = GlBudgetPoolType.Poolee,
                        FormattedGlAccount = "4",
                        GlAccountNumber = "4",
                        Description = "Fourth Description",
                        Budget = 0,
                        Actuals = 50,
                        Encumbrances = 25
                    }
                }
            };

            var accountVM = new GlObjectCodeBudgetPoolViewModel(poolDto, GlClass.Expense);
            var vm = new GlObjectCodeGlAccountSequenceViewModel(accountVM);

            Assert.AreEqual(accountVM.Umbrella, vm.PrimaryGlAccount);
            Assert.AreEqual(true, vm.IsPooledAccount);
            Assert.AreEqual(poolDto.IsUmbrellaVisible, vm.IsUmbrellaVisible);
            Assert.AreEqual(poolDto.Umbrella.FormattedGlAccount, vm.FormattedGlAccount);
            Assert.AreEqual(poolDto.Poolees.Count, vm.Poolees.Count);
        }
    }
}
