﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class CostCenterSubtotalViewModelTests
    {
        #region Initialize and Cleanup
        private TestCostCenterDtoRepository testRepository;
        private CostCenterSubtotal costCenterSubtotalDto;

        [TestInitialize]
        public void Initialize()
        {
            testRepository = new TestCostCenterDtoRepository();
            costCenterSubtotalDto = testRepository.GetCostCenterSubtotal();
        }

        [TestCleanup]
        public void Cleanup()
        {
            testRepository = null;
            costCenterSubtotalDto = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void Constructor_Success()
        {
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.AreEqual(costCenterSubtotalDto.Id, costCenterSubtotalVM.Id);
            Assert.AreEqual(costCenterSubtotalDto.Name, costCenterSubtotalVM.Name);
            Assert.AreEqual(costCenterSubtotalDto.GlClass, costCenterSubtotalVM.GlClass);

            Assert.AreEqual(costCenterSubtotalDto.TotalBudget.ToString("C"), costCenterSubtotalVM.TotalBudget);
            Assert.AreEqual(costCenterSubtotalDto.TotalActuals.ToString("C"), costCenterSubtotalVM.TotalActuals);
            Assert.AreEqual(costCenterSubtotalDto.TotalEncumbrances.ToString("C"), costCenterSubtotalVM.TotalEncumbrances);
            Assert.AreEqual((costCenterSubtotalDto.TotalActuals + costCenterSubtotalDto.TotalEncumbrances).ToString("C"), costCenterSubtotalVM.TotalExpensesFormatted);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.GlAccounts.Count + costCenterSubtotalDto.Pools.Count, costCenterSubtotalVM.SortedGlAccounts.Count);

        }

        [TestMethod]
        public void ConstructorRevenue_Success()
        {
            costCenterSubtotalDto.GlClass = GlClass.Revenue;
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.AreEqual(costCenterSubtotalDto.Id, costCenterSubtotalVM.Id);
            Assert.AreEqual(costCenterSubtotalDto.Name, costCenterSubtotalVM.Name);
            Assert.AreEqual(costCenterSubtotalDto.GlClass, costCenterSubtotalVM.GlClass);

            Assert.AreEqual((-1 * costCenterSubtotalDto.TotalBudget).ToString("C"), costCenterSubtotalVM.TotalBudgetRevenue);
            Assert.AreEqual((-1 * costCenterSubtotalDto.TotalActuals).ToString("C"), costCenterSubtotalVM.TotalActualsRevenue);
            Assert.AreEqual((-1 * costCenterSubtotalDto.TotalEncumbrances).ToString("C"), costCenterSubtotalVM.TotalEncumbrancesRevenue);
            Assert.AreEqual((-1 * (costCenterSubtotalDto.TotalActuals + costCenterSubtotalDto.TotalEncumbrances)).ToString("C"), costCenterSubtotalVM.TotalExpensesRevenueFormatted);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.GlAccounts.Count + costCenterSubtotalDto.Pools.Count, costCenterSubtotalVM.SortedGlAccounts.Count);

        }

        [TestMethod]
        public void Constructor_SubtotalIsNull()
        {
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(null);
            decimal zero = 0m;

            Assert.AreEqual(null, costCenterSubtotalVM.Id);
            Assert.AreEqual(null, costCenterSubtotalVM.Name);
            Assert.AreEqual(zero.ToString("C"), costCenterSubtotalVM.TotalBudget);
            Assert.AreEqual(zero.ToString("C"), costCenterSubtotalVM.TotalExpensesFormatted);
            Assert.AreEqual(zero.ToString("C"), costCenterSubtotalVM.TotalActuals);
            Assert.AreEqual(zero.ToString("C"), costCenterSubtotalVM.TotalEncumbrances);
            Assert.AreEqual(null, costCenterSubtotalVM.SortedGlAccounts);
        }

        [TestMethod]
        public void Constructor_GlAccountsIsNull()
        {
            costCenterSubtotalDto.GlAccounts = null;
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.Pools.Count, costCenterSubtotalVM.SortedGlAccounts.Count);
        }

        [TestMethod]
        public void Constructor_GlAccountsContainsNull()
        {
            costCenterSubtotalDto.GlAccounts[1] = null;
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.GlAccounts.Count + costCenterSubtotalDto.Pools.Count - 1, costCenterSubtotalVM.SortedGlAccounts.Count);
        }

        [TestMethod]
        public void Constructor_PoolsIsNull()
        {
            costCenterSubtotalDto.Pools = null;
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.GlAccounts.Count, costCenterSubtotalVM.SortedGlAccounts.Count);
        }

        [TestMethod]
        public void Constructor_PoolsContainsNullPool()
        {
            costCenterSubtotalDto.Pools[1] = null;
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.Pools.Count + costCenterSubtotalDto.GlAccounts.Count - 1, costCenterSubtotalVM.SortedGlAccounts.Count());
        }

        [TestMethod]
        public void Constructor_PoolsContainsNullUmbrella()
        {
            costCenterSubtotalDto.Pools[1].Umbrella = null;
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.Pools.Select(x => x.Umbrella).Count() - 1, costCenterSubtotalVM.SortedGlAccounts.Where(a => a.IsPooledAccount).Select(x => x.PooledAccount.Umbrella).Count());
        }

        [TestMethod]
        public void Constructor_PoolsContainsNullPoolee()
        {
            costCenterSubtotalDto.Pools[1].Poolees[1] = null;
            var costCenterSubtotalVM = new CostCenterSubtotalViewModel(costCenterSubtotalDto);

            Assert.IsTrue(costCenterSubtotalVM.SortedGlAccounts is List<CostCenterAccountSequenceViewModel>);
            Assert.AreEqual(costCenterSubtotalDto.Pools[1].Poolees.Count - 1, costCenterSubtotalVM.SortedGlAccounts.LastOrDefault(a => a.IsPooledAccount).PooledAccount.Poolees.Count);
        }
        #endregion

        #region GlBudgetPoolViewModel tests

        [TestMethod]
        public void GlBudgetPoolViewModel_UmbrellaNotVisible()
        {
            var poolDto = costCenterSubtotalDto.Pools[0];
            poolDto.IsUmbrellaVisible = false;
            var vm = new GlBudgetPoolViewModel(poolDto);

            var zero = 0m;
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.TotalActuals);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.TotalEncumbrances);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.TotalBudget);
        }

        #endregion

        #region CostCenterAccountSequenceViewModel tests

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CostCenterAccountSequenceViewModel_NullGlAccount()
        {
            var costCenterAccountSequenceVM = new CostCenterAccountSequenceViewModel(null as CostCenterGlAccountViewModel);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CostCenterAccountSequenceViewModel_NullPool()
        {
            var costCenterAccountSequenceVM = new CostCenterAccountSequenceViewModel(null as GlBudgetPoolViewModel);
        }

        #endregion
    }
}