﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the LineItemGlDistributionViewModel class.
    /// </summary>
    [TestClass]
    public class LineItemTaxViewModelTests
    {
        #region Initialize and Cleanup
        public LineItemTax lineItemTaxDto;
        public LineItemTaxViewModel lineItemTaxVM;

        [TestInitialize]
        public void Initialize()
        {
            lineItemTaxDto = new LineItemTax()
            {
                TaxCode = "VA",
                TaxAmount = 150.99m
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            lineItemTaxDto = null;
            lineItemTaxVM = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void Constructor_FullyPopulated()
        {
            lineItemTaxVM = new LineItemTaxViewModel(lineItemTaxDto);
            Assert.AreEqual(lineItemTaxDto.TaxCode, lineItemTaxVM.TaxCode);
            Assert.AreEqual("", lineItemTaxVM.TaxCodeDescription);
            Assert.AreEqual(lineItemTaxDto.TaxAmount, lineItemTaxVM.Amount);
        }
        #endregion

        #region AmountDisplay tests
        [TestMethod]
        public void AmountDisplay_Base()
        {
            lineItemTaxVM = new LineItemTaxViewModel(lineItemTaxDto);
            Assert.AreEqual(lineItemTaxDto.TaxAmount.ToString("C"), lineItemTaxVM.AmountDisplay);
        }
        #endregion
    }
}
