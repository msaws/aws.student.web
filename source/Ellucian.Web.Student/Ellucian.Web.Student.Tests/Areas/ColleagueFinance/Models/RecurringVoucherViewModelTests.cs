﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the RecurringVoucherViewModelTests
    /// </summary>
    [TestClass]
    public class RecurringVoucherViewModelTests
    {
        #region Initialize and Cleanup

        public RecurringVoucher recurringVoucherDto;
        public RecurringVoucherViewModel recurringVoucherVM;
        public TestDocumentDtoRepository documentRepository;
             
        
        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            recurringVoucherDto = null;
            recurringVoucherVM = null;
            documentRepository = null;
        }
        #endregion

        #region Constructor tests

        [TestMethod]
        public void RecurringVoucherViewModel_VerifyBaseFinanceDocument()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(recurringVoucherDto.RecurringVoucherId, recurringVoucherVM.DocumentId, "The document ID should be initialized.");
            Assert.AreEqual(recurringVoucherDto.Comments, recurringVoucherVM.Comments, "The document comments list should be initialized.");
            Assert.IsTrue(recurringVoucherVM.Approvers is List<DocumentApproverViewModel>, "The document approvers should be the correct type.");
            Assert.IsTrue(recurringVoucherVM.Schedules is List<RecurringVoucherScheduleViewModel>, "The document schedules should be the correct type.");
            Assert.IsTrue(recurringVoucherVM.IsRecurringVoucher, "The document is a recurring voucher.");
            Assert.IsTrue(recurringVoucherVM.IsProcurementDocument, "The document is a procurement document.");
            Assert.IsFalse(recurringVoucherVM.IsRequisition, "The document is not a requisition.");
            Assert.IsFalse(recurringVoucherVM.IsPurchaseOrder, "The document is not a purchase order.");
            Assert.IsFalse(recurringVoucherVM.IsBlanketPurchaseOrder, "The document is not a blanket purchase order.");
            Assert.IsFalse(recurringVoucherVM.IsVoucher, "The document is not a voucher.");
            Assert.IsFalse(recurringVoucherVM.IsLedgerEntryDocument, "The document is not a ledger entry document.");
            Assert.IsFalse(recurringVoucherVM.IsJournalEntry, "The document is not a journal entry.");
        }

        [TestMethod]
        public void VoucherViewModel_VerifyAccountsPayablePurchasingDocument()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Verify that the AccountsPayablePurchasingDocumentViewModel initialized the appropriate variables
            Assert.AreEqual(recurringVoucherDto.Date.ToShortDateString(), recurringVoucherVM.Date, "The document date should be initialized.");
        }

        [TestMethod]
        public void RecurringVoucherViewModel_RemainingProperties()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Confirm that the remaining internal properties have been initialized properly
            Assert.AreEqual(recurringVoucherDto.Comments, recurringVoucherVM.Comments);               
            Assert.AreEqual(recurringVoucherDto.InvoiceNumber, recurringVoucherVM.InvoiceNumber);
            Assert.AreEqual(recurringVoucherDto.InvoiceDate.ToShortDateString(), recurringVoucherVM.InvoiceDate);
            Assert.AreEqual(recurringVoucherDto.Status, recurringVoucherVM.Status);
            Assert.AreEqual(recurringVoucherDto.StatusDate.ToShortDateString(), recurringVoucherVM.StatusDate);
            Assert.AreEqual(recurringVoucherDto.VendorId, recurringVoucherVM.VendorId);
            Assert.AreEqual(recurringVoucherDto.VendorName, recurringVoucherVM.VendorName);
            Assert.AreEqual(recurringVoucherDto.Amount.ToString("C"), recurringVoucherVM.Amount);
            Assert.AreEqual(recurringVoucherDto.MaintenanceDate.Value.ToShortDateString(), recurringVoucherVM.MaintenanceDate);
            Assert.AreEqual(recurringVoucherDto.Date.ToShortDateString(), recurringVoucherVM.Date);
            Assert.AreEqual(recurringVoucherDto.ApType, recurringVoucherVM.ApType);
            Assert.AreEqual(recurringVoucherDto.CurrencyCode, recurringVoucherVM.CurrencyCode, "Currency code should match.");
            Assert.AreEqual(recurringVoucherDto.TotalScheduleAmountInLocalCurrency.Value.ToString("C"), recurringVoucherVM.TotalScheduleAmountInLocalCurrency, "Local totals should be the same.");
            Assert.AreEqual(recurringVoucherDto.TotalScheduleTaxAmountInLocalCurrency.Value.ToString("C"), recurringVoucherVM.TotalScheduleTaxAmountInLocalCurrency, "Local tax totals should be the same.");
            Assert.AreEqual(recurringVoucherDto.Approvers.Count(), recurringVoucherVM.Approvers.Count(), "The document approvers list sizes should be the same.");

            Assert.IsTrue(recurringVoucherVM.Schedules is List<RecurringVoucherScheduleViewModel>);
            Assert.AreEqual(recurringVoucherDto.Schedules.Count(), recurringVoucherVM.Schedules.Count());

            // Each line item should have sequential IDs starting at 1
            int id = 1;
            foreach (var schedule in recurringVoucherVM.Schedules)
            {
                Assert.AreEqual("document-line-item" + id.ToString(), schedule.DocumentItemId, "The document item ID should match.");
                id++;
            }
        }

        [TestMethod]
        public void RecurringVoucherViewModel_NullableObjectsAreNull()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.MaintenanceDate = null;
            recurringVoucherDto.Approvers = null;
            recurringVoucherDto.VendorName = null;
            recurringVoucherDto.CurrencyCode = null;
            recurringVoucherDto.TotalScheduleAmountInLocalCurrency = null;
            recurringVoucherDto.TotalScheduleTaxAmountInLocalCurrency = null;
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(recurringVoucherDto.InvoiceNumber, recurringVoucherVM.InvoiceNumber);
            Assert.AreEqual(recurringVoucherDto.InvoiceDate.ToShortDateString(), recurringVoucherVM.InvoiceDate);
            Assert.AreEqual(recurringVoucherDto.Status, recurringVoucherVM.Status);
            Assert.AreEqual(recurringVoucherDto.Date.ToShortDateString(), recurringVoucherVM.Date);
            Assert.AreEqual("", recurringVoucherVM.MaintenanceDate);
            Assert.AreEqual("", recurringVoucherVM.VendorName);
            Assert.AreEqual(null, recurringVoucherVM.CurrencyCode, "Currency code should be null.");
            Assert.AreEqual(null, recurringVoucherVM.TotalScheduleAmountInLocalCurrency, "Local total should be null.");
            Assert.AreEqual(null, recurringVoucherVM.TotalScheduleTaxAmountInLocalCurrency, "Local tax total should be null.");

            Assert.IsTrue(recurringVoucherVM.Approvers is List<DocumentApproverViewModel>);
            Assert.IsTrue(recurringVoucherVM.Approvers.Count() == 0);
        }

        [TestMethod]
        public void Constructor_ShortVendorName()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.VendorName = "Ellucian Consulting";
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            Assert.AreEqual(recurringVoucherDto.VendorName, recurringVoucherVM.VendorName);
        }

        [TestMethod]
        public void Constructor_LongVendorName()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.VendorName = "Ellucian Consultants International LLC";
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            Assert.AreEqual(recurringVoucherDto.VendorName, recurringVoucherVM.VendorName);
        }
        #endregion

        #region Status Description Tests

        [TestMethod]
        public void StatusDescription_Cancelled()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.Status = RecurringVoucherStatus.Cancelled;
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

           // Confirm that the recurring voucher status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "RecurringVoucherStatusCancelled"), recurringVoucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Closed()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.Status = RecurringVoucherStatus.Closed;
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Confirm that the recurring voucher status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "RecurringVoucherStatusClosed"), recurringVoucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_NotApproved()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.Status = RecurringVoucherStatus.NotApproved;
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Confirm that the recurring voucher status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "RecurringVoucherStatusNotApproved"), recurringVoucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Outstanding()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.Status = RecurringVoucherStatus.Outstanding;
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Confirm that the recurring voucher status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "RecurringVoucherStatusOutstanding"), recurringVoucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Voided()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.Status = RecurringVoucherStatus.Voided;
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            // Confirm that the recurring voucher status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "RecurringVoucherStatusVoided"), recurringVoucherVM.StatusDescription);
        }
        #endregion

        #region Set Approvers tests

        [TestMethod]
        public void SetApprovers_Success()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);
            recurringVoucherVM.SetApprovers(recurringVoucherDto.Approvers);

            Assert.AreEqual(recurringVoucherDto.Approvers.Count(), recurringVoucherVM.Approvers.Count(), "The approvers lists should be the same size.");
        }

        [TestMethod]
        public void SetApprovers_CallTwice()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);
            recurringVoucherVM.SetApprovers(recurringVoucherDto.Approvers);
            recurringVoucherVM.SetApprovers(recurringVoucherDto.Approvers);

            Assert.AreEqual(recurringVoucherDto.Approvers.Count(), recurringVoucherVM.Approvers.Count(), "The approvers lists should be the same size.");
        }

        [TestMethod]
        public void SetApprovers_NullApproverInList()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.Approvers = new List<Approver>();
            recurringVoucherDto.Approvers.Add(null);

            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);
            recurringVoucherVM.SetApprovers(recurringVoucherDto.Approvers);

            Assert.IsTrue(recurringVoucherVM.Approvers.Count() == 0, "The approvers list should have 0 items.");
        }

        [TestMethod]
        public void SetApprovers_ApproversListIsNull()
        {
            recurringVoucherDto = documentRepository.GetRecurringVoucher();
            recurringVoucherDto.Approvers = null;

            recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);
            recurringVoucherVM.SetApprovers(recurringVoucherDto.Approvers);

            Assert.IsTrue(recurringVoucherVM.Approvers.Count() == 0, "The approvers list should be 0.");
        }


        #endregion

        #region TotalScheduleAmount tests
        [TestMethod]
        public void TotalScheduleAmount()
        {
            var recurringVoucherDto = documentRepository.GetRecurringVoucher();
            var recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            Assert.AreEqual(recurringVoucherDto.Schedules.Sum(x => x.Amount).ToString("C"), recurringVoucherVM.TotalScheduleAmount, "TotalScheduleAmount should equal the sum of the DTO schedule amounts.");
        }

        [TestMethod]
        public void TotalTaxAmount()
        {
            var recurringVoucherDto = documentRepository.GetRecurringVoucher();
            var recurringVoucherVM = new RecurringVoucherViewModel(recurringVoucherDto);

            Assert.AreEqual(recurringVoucherDto.Schedules.Sum(x => x.TaxAmount).ToString("C"), recurringVoucherVM.TotalScheduleTaxAmount, "TotalTaxAmount should equal the sum of the DTO schedule amounts.");
        }
        #endregion
    }
}
