﻿// Copyright 2014 -2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the PurchaseOrderViewModel
    /// </summary>
    [TestClass]
    public class PurchaseOrderViewModelTests
    {
        #region Initialize and Cleanup
        public PurchaseOrder purchaseOrderDto;
        public PurchaseOrderViewModel purchaseOrderVM;
        public TestDocumentDtoRepository documentRepository;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            purchaseOrderDto = null;
            purchaseOrderVM = null;
            documentRepository = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void PurchaseOrderViewModel_VerifyBaseFinanceDocument()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(purchaseOrderDto.Id, purchaseOrderVM.DocumentId, "The document ID should be initialized.");
            Assert.AreEqual(purchaseOrderDto.Number, purchaseOrderVM.Number, "The document number should be initialized.");
            Assert.AreEqual(purchaseOrderDto.Comments, purchaseOrderVM.Comments, "The document comments list should be initialized.");
            Assert.IsTrue(purchaseOrderVM.Approvers is List<DocumentApproverViewModel>, "The document approvers should be the correct type.");
            Assert.IsTrue(purchaseOrderVM.LineItems is List<LineItemViewModel>, "The document line items list should be the correct type.");
            Assert.IsTrue(purchaseOrderVM.IsPurchaseOrder, "The document is a purchase order.");
            Assert.IsTrue(purchaseOrderVM.IsProcurementDocument, "The document is a procurement document.");
            Assert.IsFalse(purchaseOrderVM.IsRequisition, "The document is not a requisition.");
            Assert.IsFalse(purchaseOrderVM.IsVoucher, "The document is not a voucher.");
            Assert.IsFalse(purchaseOrderVM.IsLedgerEntryDocument, "The document is not a ledger entry document.");
            Assert.IsFalse(purchaseOrderVM.IsJournalEntry, "The document is not a journal entry.");
        }

        [TestMethod]
        public void PurchaseOrderViewModel_VerifyAccountsPayablePurchasingDocument()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Verify that the AccountsPayablePurchasingDocumentViewModel initialized the appropriate variables
            Assert.AreEqual(purchaseOrderDto.Date.ToShortDateString(), purchaseOrderVM.Date, "The document date should be initialized.");
        }

        [TestMethod]
        public void PurchaseOrderViewModel_RemainingProperties()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(purchaseOrderDto.Status, purchaseOrderVM.Status);
            Assert.AreEqual(purchaseOrderDto.StatusDate.ToShortDateString(), purchaseOrderVM.StatusDate);
            Assert.AreEqual(purchaseOrderDto.DeliveryDate.Value.ToShortDateString(), purchaseOrderVM.DeliveryDate);
            Assert.AreEqual(purchaseOrderDto.InitiatorName, purchaseOrderVM.InitiatorName);
            Assert.AreEqual(purchaseOrderDto.RequestorName, purchaseOrderVM.RequestorName);
            Assert.AreEqual(purchaseOrderDto.ShipToCodeName, purchaseOrderVM.ShipToCode);
            Assert.AreEqual(purchaseOrderDto.InternalComments, purchaseOrderVM.InternalComments);
            Assert.AreEqual(purchaseOrderDto.VendorId, purchaseOrderVM.VendorId);
            Assert.AreEqual(purchaseOrderDto.VendorName, purchaseOrderVM.VendorName);
            Assert.AreEqual(purchaseOrderDto.Amount.ToString("C"), purchaseOrderVM.Amount);
            Assert.AreEqual(purchaseOrderDto.MaintenanceDate.Value.ToShortDateString(), purchaseOrderVM.MaintenanceDate);
            Assert.AreEqual(purchaseOrderDto.CurrencyCode, purchaseOrderVM.CurrencyCode);
            Assert.AreEqual(purchaseOrderDto.Date.ToShortDateString(), purchaseOrderVM.Date);
            Assert.AreEqual(purchaseOrderDto.ApType, purchaseOrderVM.ApType);

            Assert.IsTrue(purchaseOrderVM.Requisitions is List<RequisitionNumberViewModel>);
            Assert.AreEqual(purchaseOrderDto.Requisitions.Count(), purchaseOrderVM.Requisitions.Count());
            foreach (var reqId in purchaseOrderDto.Requisitions)
            {
                Assert.IsTrue(purchaseOrderVM.Requisitions.Any(x => x.Id == reqId));
            }

            Assert.IsTrue(purchaseOrderVM.Vouchers is List<string>);
            Assert.AreEqual(purchaseOrderDto.Vouchers.Count(), purchaseOrderVM.Vouchers.Count());

            Assert.IsTrue(purchaseOrderVM.LineItems is List<LineItemViewModel>);
            Assert.AreEqual(purchaseOrderDto.LineItems.Count(), purchaseOrderVM.LineItems.Count());

            Assert.AreEqual(purchaseOrderDto.Approvers.Count(), purchaseOrderVM.Approvers.Count(), "The document approvers list sizes should be the same.");

            // Each line item should have sequential IDs starting at 1
            int id = 1;
            foreach (var lineItem in purchaseOrderVM.LineItems)
            {
                Assert.AreEqual("document-line-item" + id.ToString(), lineItem.DocumentItemId, "The document item ID should match.");
                id++;
            }
        }

        [TestMethod]
        public void Constructor_NullableObjectsAreNull()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.DeliveryDate = null;
            purchaseOrderDto.Requisitions = null;
            purchaseOrderDto.Vouchers = null;
            purchaseOrderDto.MaintenanceDate = null;
            purchaseOrderDto.LineItems = null;
            purchaseOrderDto.VendorName = null;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            Assert.AreEqual(purchaseOrderDto.Status, purchaseOrderVM.Status);
            Assert.AreEqual(purchaseOrderDto.StatusDate.ToShortDateString(), purchaseOrderVM.StatusDate);
            Assert.AreEqual("", purchaseOrderVM.DeliveryDate);
            Assert.AreEqual(purchaseOrderDto.InitiatorName, purchaseOrderVM.InitiatorName);
            Assert.AreEqual(purchaseOrderDto.RequestorName, purchaseOrderVM.RequestorName);
            Assert.AreEqual(purchaseOrderDto.ShipToCodeName, purchaseOrderVM.ShipToCode);
            Assert.AreEqual(purchaseOrderDto.InternalComments, purchaseOrderVM.InternalComments);

            Assert.AreEqual(purchaseOrderDto.VendorId, purchaseOrderVM.VendorId);
            Assert.AreEqual("", purchaseOrderVM.VendorName);
            Assert.AreEqual(purchaseOrderDto.Amount.ToString("C"), purchaseOrderVM.Amount);
            Assert.AreEqual("", purchaseOrderVM.MaintenanceDate);
            Assert.AreEqual(purchaseOrderDto.CurrencyCode, purchaseOrderVM.CurrencyCode);
            Assert.AreEqual(purchaseOrderDto.Date.ToShortDateString(), purchaseOrderVM.Date);
            Assert.AreEqual(purchaseOrderDto.ApType, purchaseOrderVM.ApType);

            Assert.IsTrue(purchaseOrderVM.Requisitions is List<RequisitionNumberViewModel>);
            Assert.IsTrue(purchaseOrderVM.Requisitions.Count() == 0);

            Assert.IsTrue(purchaseOrderVM.Vouchers is List<string>);
            Assert.IsTrue(purchaseOrderVM.Vouchers.Count() == 0);

            Assert.IsTrue(purchaseOrderVM.LineItems is List<LineItemViewModel>);
            Assert.IsTrue(purchaseOrderVM.LineItems.Count() == 0);
        }

        [TestMethod]
        public void Constructor_ShortVendorName()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.VendorName = "Susty Corporation";
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            Assert.AreEqual(purchaseOrderDto.VendorName, purchaseOrderVM.VendorName);
        }

        [TestMethod]
        public void Constructor_LongVendorName()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.VendorName = "Susty Corporation International LLC";
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            Assert.AreEqual(purchaseOrderDto.VendorName, purchaseOrderVM.VendorName);
        }

        [TestMethod]
        public void Constructor_NullInitiatorName()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.VendorName = "Susty Corporation International LLC";
            purchaseOrderDto.InitiatorName = null;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            Assert.AreEqual(String.Empty, purchaseOrderVM.InitiatorName);
        }

        [TestMethod]
        public void Constructor_LongInitiatorName()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.InitiatorName = "Susty Corporation International LLC";
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            Assert.AreEqual(purchaseOrderDto.InitiatorName, purchaseOrderVM.InitiatorName);
        }

        [TestMethod]
        public void Constructor_NullRequestorName()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.VendorName = "Susty Corporation International LLC";
            purchaseOrderDto.RequestorName = null;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            Assert.AreEqual(string.Empty, purchaseOrderVM.RequestorName);
        }

        [TestMethod]
        public void Constructor_LongRequestorName()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.VendorName = "Susty Corporation International LLC";
            purchaseOrderDto.RequestorName = "Susty Corporation International LLC";
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            Assert.AreEqual(purchaseOrderDto.RequestorName, purchaseOrderVM.RequestorName);
        }
        #endregion

        #region StatusDescription tests
        [TestMethod]
        public void StatusDescription_Accepted()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Accepted;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusAccepted"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Backordered()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Backordered;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusBackordered"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Closed()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Closed;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusClosed"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_InProgress()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.InProgress;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusInProgress"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Invoiced()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Invoiced;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusInvoiced"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_NotApproved()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.NotApproved;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusNotApproved"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Outstanding()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Outstanding;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusOutstanding"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Paid()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Paid;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusPaid"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Reconciled()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Reconciled;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusReconciled"), purchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Voided()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderDto.Status = PurchaseOrderStatus.Voided;
            purchaseOrderVM = new PurchaseOrderViewModel(purchaseOrderDto);

            // Confirm that the purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusVoided"), purchaseOrderVM.StatusDescription);
        }
        #endregion
    }
}
