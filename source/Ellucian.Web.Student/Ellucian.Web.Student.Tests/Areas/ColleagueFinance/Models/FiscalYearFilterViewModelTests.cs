﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class FiscalYearFilterViewModelTests
    {
        #region Tests
        [TestMethod]
        public void Constructor1()
        {
            var fiscalYear = "2016";
            var filterVM = new FiscalYearFilterViewModel(fiscalYear);

            Assert.AreEqual(fiscalYear, filterVM.FiscalYear);
            Assert.AreEqual("FY" + fiscalYear, filterVM.FiscalYearDescription);
        }

        [TestMethod]
        public void Constructor2()
        {
            var fiscalYear = "2016";
            var description = "Current Year";
            var filterVM = new FiscalYearFilterViewModel(fiscalYear, description);

            Assert.AreEqual(fiscalYear, filterVM.FiscalYear);
            Assert.AreEqual(description, filterVM.FiscalYearDescription);
        }
        #endregion
    }
}