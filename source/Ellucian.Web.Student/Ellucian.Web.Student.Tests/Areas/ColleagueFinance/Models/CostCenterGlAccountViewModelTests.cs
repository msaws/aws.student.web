﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class CostCenterGlAccountViewModelTests
    {
        #region Initialize and Cleanup
        private TestCostCenterDtoRepository testRepository;
        private CostCenterGlAccount costCenterGlAccountDto;

        [TestInitialize]
        public void Initialize()
        {
            testRepository = new TestCostCenterDtoRepository();
            costCenterGlAccountDto = testRepository.GetCostCenterGlAccount();
        }

        [TestCleanup]
        public void Cleanup()
        {
            testRepository = null;
            costCenterGlAccountDto = null;
        }
        #endregion

        #region Tests
        [TestMethod]
        public void Constructor()
        {
            var glAccountVM = new CostCenterGlAccountViewModel(costCenterGlAccountDto, GlClass.Expense);

            Assert.AreEqual(costCenterGlAccountDto.GlAccountNumber, glAccountVM.GlAccount);
            Assert.AreEqual(costCenterGlAccountDto.FormattedGlAccount, glAccountVM.FormattedGlAccount);
            Assert.AreEqual(costCenterGlAccountDto.Description, glAccountVM.Description);
            Assert.AreEqual(costCenterGlAccountDto.Budget.ToString("C"), glAccountVM.TotalBudget);
            Assert.AreEqual(costCenterGlAccountDto.Encumbrances.ToString("C"), glAccountVM.TotalEncumbrances);
            Assert.AreEqual(costCenterGlAccountDto.Actuals.ToString("C"), glAccountVM.TotalActuals);
            Assert.AreEqual((costCenterGlAccountDto.Actuals + costCenterGlAccountDto.Encumbrances).ToString("C"), glAccountVM.TotalExpensesFormatted);
            var expectedAmount = (costCenterGlAccountDto.Budget - (costCenterGlAccountDto.Actuals + costCenterGlAccountDto.Encumbrances)).ToString("C");
            Assert.AreEqual(expectedAmount, glAccountVM.BudgetRemainingFormatted);
            var expectedPercent = (Math.Round(((costCenterGlAccountDto.Actuals + costCenterGlAccountDto.Encumbrances) / costCenterGlAccountDto.Budget) * 100, MidpointRounding.AwayFromZero)).ToString() + " %";
            Assert.AreEqual(expectedPercent, glAccountVM.ListViewBudgetSpentPercent);
            Assert.AreEqual(GlClass.Expense, glAccountVM.GlClass);
            
        }

        [TestMethod]
        public void ConstructorWithRevenueGlClass()
        {
            var glAccountVM = new CostCenterGlAccountViewModel(costCenterGlAccountDto, GlClass.Revenue);

            Assert.AreEqual(costCenterGlAccountDto.GlAccountNumber, glAccountVM.GlAccount);
            Assert.AreEqual(costCenterGlAccountDto.FormattedGlAccount, glAccountVM.FormattedGlAccount);
            Assert.AreEqual(costCenterGlAccountDto.Description, glAccountVM.Description);
            Assert.AreEqual((-1 * costCenterGlAccountDto.Budget).ToString("C"), glAccountVM.TotalBudgetRevenue);
            Assert.AreEqual((-1 * costCenterGlAccountDto.Encumbrances).ToString("C"), glAccountVM.TotalEncumbrancesRevenue);
            Assert.AreEqual((-1 * costCenterGlAccountDto.Actuals).ToString("C"), glAccountVM.TotalActualsRevenue);
            Assert.AreEqual((-1 * (costCenterGlAccountDto.Actuals + costCenterGlAccountDto.Encumbrances)).ToString("C"), glAccountVM.TotalExpensesRevenueFormatted);
            Assert.AreEqual((-1 * costCenterGlAccountDto.Budget).ToString("C"), glAccountVM.TotalBudget);
            Assert.AreEqual((-1 * costCenterGlAccountDto.Encumbrances).ToString("C"), glAccountVM.TotalEncumbrances);
            Assert.AreEqual((-1 * costCenterGlAccountDto.Actuals).ToString("C"), glAccountVM.TotalActuals);
            Assert.AreEqual((-1 * (costCenterGlAccountDto.Actuals + costCenterGlAccountDto.Encumbrances)).ToString("C"), glAccountVM.TotalExpensesFormatted);
            var expectedAmount = (-1 * (costCenterGlAccountDto.Budget - (costCenterGlAccountDto.Actuals + costCenterGlAccountDto.Encumbrances))).ToString("C");
            Assert.AreEqual(expectedAmount, glAccountVM.BudgetRemainingFormatted);
            var expectedPercent = (Math.Round(((costCenterGlAccountDto.Actuals + costCenterGlAccountDto.Encumbrances) / costCenterGlAccountDto.Budget) * 100, MidpointRounding.AwayFromZero)).ToString() + " %";
            Assert.AreEqual(expectedPercent, glAccountVM.ListViewBudgetSpentPercent);
            Assert.AreEqual(GlClass.Revenue, glAccountVM.GlClass);
        }

        [TestMethod]
        public void ConstructorWithInvalidGlClass()
        {
            var glAccountVM = new CostCenterGlAccountViewModel(costCenterGlAccountDto, GlClass.Asset);

            Assert.AreEqual(costCenterGlAccountDto.GlAccountNumber, glAccountVM.GlAccount);
            Assert.AreEqual(costCenterGlAccountDto.FormattedGlAccount, glAccountVM.FormattedGlAccount);
            Assert.AreEqual(costCenterGlAccountDto.Description, glAccountVM.Description);
            Assert.AreEqual(string.Empty, glAccountVM.TotalBudget);
            Assert.AreEqual(string.Empty, glAccountVM.TotalEncumbrances);
            Assert.AreEqual(string.Empty, glAccountVM.TotalActuals);
            Assert.AreEqual(string.Empty, glAccountVM.TotalExpensesFormatted);
            Assert.AreEqual(string.Empty, glAccountVM.BudgetRemainingFormatted);
            Assert.AreEqual(string.Empty, glAccountVM.ListViewBudgetSpentPercent);
            Assert.AreEqual(GlClass.Asset, glAccountVM.GlClass);
        }

        [TestMethod]
        public void Constructor_GlAccountIsNull()
        {
            var glAccountVM = new CostCenterGlAccountViewModel(null as CostCenterGlAccount, GlClass.Expense);
            decimal zero = 0m;

            Assert.AreEqual(null, glAccountVM.GlAccount);
            Assert.AreEqual(null, glAccountVM.FormattedGlAccount);
            Assert.AreEqual(null, glAccountVM.Description);
            Assert.AreEqual(zero.ToString("C"), glAccountVM.TotalBudget);
            Assert.AreEqual(zero.ToString("C"), glAccountVM.TotalExpensesFormatted);
            Assert.AreEqual(zero.ToString("C"), glAccountVM.TotalActuals);
            Assert.AreEqual(zero.ToString("C"), glAccountVM.TotalEncumbrances);
        }
        #endregion
    }
}