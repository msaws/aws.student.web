﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class GlObjectCodeBudgetPoolViewModelTests
    {
        [TestMethod]
        public void ConstructorWithoutFinancialHealthIndicator()
        {
            var poolDto = new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                {
                    new GlObjectCodeGlAccount()
                    {
                        PoolType = GlBudgetPoolType.Poolee,
                        FormattedGlAccount = "4",
                        GlAccountNumber = "4",
                        Description = "Fourth Description",
                        Budget = 0,
                        Actuals = 50,
                        Encumbrances = 25
                    }
                }
            };

            var vm = new GlObjectCodeBudgetPoolViewModel(poolDto, GlClass.Expense, false);

            Assert.AreEqual(poolDto.IsUmbrellaVisible, vm.IsUmbrellaVisible);

            Assert.AreEqual(poolDto.Umbrella.FormattedGlAccount, vm.Umbrella.FormattedGlAccount);
            Assert.AreEqual(poolDto.Umbrella.GlAccountNumber, vm.Umbrella.GlAccountNumber);
            var expectedPoolTypeCssClass = "non-pooled-account";
            if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Umbrella)
            {
                expectedPoolTypeCssClass = "umbrella";
            }
            else if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Poolee)
            {
                expectedPoolTypeCssClass = "poolee";
            }
            Assert.AreEqual(expectedPoolTypeCssClass, vm.Umbrella.PoolTypeCssClass);
            Assert.AreEqual(poolDto.Umbrella.Description, vm.Umbrella.Description);
            Assert.AreEqual(poolDto.Umbrella.Encumbrances.ToString("C"), vm.Umbrella.Encumbrances);
            Assert.AreEqual(poolDto.Umbrella.Actuals.ToString("C"), vm.Umbrella.Actuals);
            Assert.AreEqual(poolDto.Umbrella.Budget.ToString("C"), vm.Umbrella.Budget);
            Assert.AreEqual(string.Empty, vm.Umbrella.FinancialHealthIndicator);
            Assert.AreEqual(string.Empty, vm.Umbrella.FinancialHealthIndicatorText);

            Assert.AreEqual(poolDto.Poolees.Count, vm.Poolees.Count);
            for (int i = 0; i < poolDto.Poolees.Count; i++)
            {
                Assert.AreEqual(poolDto.Poolees[i].FormattedGlAccount, vm.Poolees[i].FormattedGlAccount);
                Assert.AreEqual(poolDto.Poolees[i].GlAccountNumber, vm.Poolees[i].GlAccountNumber);
                expectedPoolTypeCssClass = "non-pooled-account";
                if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Umbrella)
                {
                    expectedPoolTypeCssClass = "umbrella";
                }
                else if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Poolee)
                {
                    expectedPoolTypeCssClass = "poolee";
                }
                Assert.AreEqual(expectedPoolTypeCssClass, vm.Poolees[i].PoolTypeCssClass);
                Assert.AreEqual(poolDto.Poolees[i].Description, vm.Poolees[i].Description);
                Assert.AreEqual(poolDto.Poolees[i].Encumbrances.ToString("C"), vm.Poolees[i].Encumbrances);
                Assert.AreEqual(poolDto.Poolees[i].Actuals.ToString("C"), vm.Poolees[i].Actuals);

                if (expectedPoolTypeCssClass == "poolee")
                {
                    Assert.AreEqual("", vm.Poolees[i].Budget);
                }
                else
                {
                    Assert.AreEqual(poolDto.Poolees[i].Budget.ToString("C"), vm.Poolees[i].Budget);
                }
            }

            if (!poolDto.IsUmbrellaVisible)
            {
                decimal zero = 0m;
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Budget);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Actuals);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Encumbrances);
            }
        }

        [TestMethod]
        public void ConstructorWithFinancialHealthIndicator()
        {
            var poolDto = new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                {
                    new GlObjectCodeGlAccount()
                    {
                        PoolType = GlBudgetPoolType.Poolee,
                        FormattedGlAccount = "4",
                        GlAccountNumber = "4",
                        Description = "Fourth Description",
                        Budget = 0,
                        Actuals = 50,
                        Encumbrances = 25
                    }
                }
            };

            var vm = new GlObjectCodeBudgetPoolViewModel(poolDto, GlClass.Expense, true);

            Assert.AreEqual(poolDto.IsUmbrellaVisible, vm.IsUmbrellaVisible);

            Assert.AreEqual(poolDto.Umbrella.FormattedGlAccount, vm.Umbrella.FormattedGlAccount);
            Assert.AreEqual(poolDto.Umbrella.GlAccountNumber, vm.Umbrella.GlAccountNumber);
            var expectedPoolTypeCssClass = "non-pooled-account";
            if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Umbrella)
            {
                expectedPoolTypeCssClass = "umbrella";
            }
            else if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Poolee)
            {
                expectedPoolTypeCssClass = "poolee";
            }
            Assert.AreEqual(expectedPoolTypeCssClass, vm.Umbrella.PoolTypeCssClass);
            Assert.AreEqual(poolDto.Umbrella.Description, vm.Umbrella.Description);
            Assert.AreEqual(poolDto.Umbrella.Encumbrances.ToString("C"), vm.Umbrella.Encumbrances);
            Assert.AreEqual(poolDto.Umbrella.Actuals.ToString("C"), vm.Umbrella.Actuals);
            Assert.AreEqual(poolDto.Umbrella.Budget.ToString("C"), vm.Umbrella.Budget);

            if ((poolDto.Umbrella.Actuals + poolDto.Umbrella.Encumbrances) > poolDto.Umbrella.Budget)
            {
                Assert.AreEqual("group-icon-red", vm.Umbrella.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.Umbrella.FinancialHealthIndicatorText);
            }
            else if ((poolDto.Umbrella.Actuals + poolDto.Umbrella.Encumbrances) > poolDto.Umbrella.Budget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.Umbrella.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.Umbrella.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.Umbrella.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.Umbrella.FinancialHealthIndicatorText);
            }

            Assert.AreEqual(poolDto.Poolees.Count, vm.Poolees.Count);
            for (int i = 0; i < poolDto.Poolees.Count; i++)
            {
                Assert.AreEqual(poolDto.Poolees[i].FormattedGlAccount, vm.Poolees[i].FormattedGlAccount);
                Assert.AreEqual(poolDto.Poolees[i].GlAccountNumber, vm.Poolees[i].GlAccountNumber);
                expectedPoolTypeCssClass = "non-pooled-account";
                if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Umbrella)
                {
                    expectedPoolTypeCssClass = "umbrella";
                }
                else if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Poolee)
                {
                    expectedPoolTypeCssClass = "poolee";
                }
                Assert.AreEqual(expectedPoolTypeCssClass, vm.Poolees[i].PoolTypeCssClass);
                Assert.AreEqual(poolDto.Poolees[i].Description, vm.Poolees[i].Description);
                Assert.AreEqual(poolDto.Poolees[i].Encumbrances.ToString("C"), vm.Poolees[i].Encumbrances);
                Assert.AreEqual(poolDto.Poolees[i].Actuals.ToString("C"), vm.Poolees[i].Actuals);

                if (expectedPoolTypeCssClass == "poolee")
                {
                    Assert.AreEqual("", vm.Poolees[i].Budget);
                }
                else
                {
                    Assert.AreEqual(poolDto.Poolees[i].Budget.ToString("C"), vm.Poolees[i].Budget);
                }
            }

            if (!poolDto.IsUmbrellaVisible)
            {
                decimal zero = 0m;
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Budget);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Actuals);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Encumbrances);
            }
        }

        [TestMethod]
        public void ConstructorWithMaskedUmbrella()
        {
            decimal zero = 0m;
            var poolDto = new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = false,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                {
                    new GlObjectCodeGlAccount()
                    {
                        PoolType = GlBudgetPoolType.Poolee,
                        FormattedGlAccount = "4",
                        GlAccountNumber = "4",
                        Description = "Fourth Description",
                        Budget = 0,
                        Actuals = 50,
                        Encumbrances = 25
                    }
                }
            };

            var vm = new GlObjectCodeBudgetPoolViewModel(poolDto, GlClass.Expense);

            Assert.AreEqual(poolDto.IsUmbrellaVisible, vm.IsUmbrellaVisible);

            Assert.AreEqual(poolDto.Umbrella.FormattedGlAccount, vm.Umbrella.FormattedGlAccount);
            Assert.AreEqual(poolDto.Umbrella.GlAccountNumber, vm.Umbrella.GlAccountNumber);
            var expectedPoolTypeCssClass = "non-pooled-account";
            if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Umbrella)
            {
                expectedPoolTypeCssClass = "umbrella";
            }
            else if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Poolee)
            {
                expectedPoolTypeCssClass = "poolee";
            }
            Assert.AreEqual(expectedPoolTypeCssClass, vm.Umbrella.PoolTypeCssClass);
            Assert.AreEqual(poolDto.Umbrella.Description, vm.Umbrella.Description);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Encumbrances);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Actuals);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Budget);

            Assert.AreEqual(string.Empty, vm.Umbrella.FinancialHealthIndicator);
            Assert.AreEqual(string.Empty, vm.Umbrella.FinancialHealthIndicatorText);


            Assert.AreEqual(poolDto.Poolees.Count, vm.Poolees.Count);
            for (int i = 0; i < poolDto.Poolees.Count; i++)
            {
                Assert.AreEqual(poolDto.Poolees[i].FormattedGlAccount, vm.Poolees[i].FormattedGlAccount);
                Assert.AreEqual(poolDto.Poolees[i].GlAccountNumber, vm.Poolees[i].GlAccountNumber);
                expectedPoolTypeCssClass = "non-pooled-account";
                if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Umbrella)
                {
                    expectedPoolTypeCssClass = "umbrella";
                }
                else if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Poolee)
                {
                    expectedPoolTypeCssClass = "poolee";
                }
                Assert.AreEqual(expectedPoolTypeCssClass, vm.Poolees[i].PoolTypeCssClass);
                Assert.AreEqual(poolDto.Poolees[i].Description, vm.Poolees[i].Description);
                Assert.AreEqual(poolDto.Poolees[i].Encumbrances.ToString("C"), vm.Poolees[i].Encumbrances);
                Assert.AreEqual(poolDto.Poolees[i].Actuals.ToString("C"), vm.Poolees[i].Actuals);

                if (expectedPoolTypeCssClass == "poolee")
                {
                    Assert.AreEqual("", vm.Poolees[i].Budget);
                }
                else
                {
                    Assert.AreEqual(poolDto.Poolees[i].Budget.ToString("C"), vm.Poolees[i].Budget);
                }
            }

            if (!poolDto.IsUmbrellaVisible)
            {
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Budget);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Actuals);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Encumbrances);
            }
        }

        [TestMethod]
        public void ConstructorWithFinancialHealthButMaskedUmbrella()
        {
            decimal zero = 0m;
            var poolDto = new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = false,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                {
                    new GlObjectCodeGlAccount()
                    {
                        PoolType = GlBudgetPoolType.Poolee,
                        FormattedGlAccount = "4",
                        GlAccountNumber = "4",
                        Description = "Fourth Description",
                        Budget = 0,
                        Actuals = 50,
                        Encumbrances = 25
                    }
                }
            };

            var vm = new GlObjectCodeBudgetPoolViewModel(poolDto, GlClass.Expense, true);

            Assert.AreEqual(poolDto.IsUmbrellaVisible, vm.IsUmbrellaVisible);

            Assert.AreEqual(poolDto.Umbrella.FormattedGlAccount, vm.Umbrella.FormattedGlAccount);
            Assert.AreEqual(poolDto.Umbrella.GlAccountNumber, vm.Umbrella.GlAccountNumber);
            var expectedPoolTypeCssClass = "non-pooled-account";
            if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Umbrella)
            {
                expectedPoolTypeCssClass = "umbrella";
            }
            else if (poolDto.Umbrella.PoolType == GlBudgetPoolType.Poolee)
            {
                expectedPoolTypeCssClass = "poolee";
            }
            Assert.AreEqual(expectedPoolTypeCssClass, vm.Umbrella.PoolTypeCssClass);
            Assert.AreEqual(poolDto.Umbrella.Description, vm.Umbrella.Description);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Encumbrances);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Actuals);
            Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Budget);

            Assert.AreEqual(string.Empty, vm.Umbrella.FinancialHealthIndicator);
            Assert.AreEqual(string.Empty, vm.Umbrella.FinancialHealthIndicatorText);


            Assert.AreEqual(poolDto.Poolees.Count, vm.Poolees.Count);
            for (int i = 0; i < poolDto.Poolees.Count; i++)
            {
                Assert.AreEqual(poolDto.Poolees[i].FormattedGlAccount, vm.Poolees[i].FormattedGlAccount);
                Assert.AreEqual(poolDto.Poolees[i].GlAccountNumber, vm.Poolees[i].GlAccountNumber);
                expectedPoolTypeCssClass = "non-pooled-account";
                if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Umbrella)
                {
                    expectedPoolTypeCssClass = "umbrella";
                }
                else if (poolDto.Poolees[i].PoolType == GlBudgetPoolType.Poolee)
                {
                    expectedPoolTypeCssClass = "poolee";
                }
                Assert.AreEqual(expectedPoolTypeCssClass, vm.Poolees[i].PoolTypeCssClass);
                Assert.AreEqual(poolDto.Poolees[i].Description, vm.Poolees[i].Description);
                Assert.AreEqual(poolDto.Poolees[i].Encumbrances.ToString("C"), vm.Poolees[i].Encumbrances);
                Assert.AreEqual(poolDto.Poolees[i].Actuals.ToString("C"), vm.Poolees[i].Actuals);

                if (expectedPoolTypeCssClass == "poolee")
                {
                    Assert.AreEqual("", vm.Poolees[i].Budget);
                }
                else
                {
                    Assert.AreEqual(poolDto.Poolees[i].Budget.ToString("C"), vm.Poolees[i].Budget);
                }
            }

            if (!poolDto.IsUmbrellaVisible)
            {
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Budget);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Actuals);
                Assert.AreEqual(zero.ToString("C"), vm.Umbrella.Encumbrances);
            }
        }
    }
}
