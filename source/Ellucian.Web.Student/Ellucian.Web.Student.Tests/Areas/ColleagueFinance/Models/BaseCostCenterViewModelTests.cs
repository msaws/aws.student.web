﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.ColleagueFinance;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class BaseCostCenterViewModelTests
    {
        #region Initialize and Cleanup
        private TestCostCenterDtoRepository testRepository;
        private CostCenter costCenterDto;

        [TestInitialize]
        public void Initialize()
        {
            testRepository = new TestCostCenterDtoRepository();
            costCenterDto = testRepository.GetCostCenter();
        }

        [TestCleanup]
        public void Cleanup()
        {
            testRepository = null;
            costCenterDto = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void Constructor()
        {
            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);

            Assert.AreEqual(costCenterDto.Id, costCenterVM.CostCenterId);
            Assert.AreEqual(costCenterDto.Name, costCenterVM.Name);
            Assert.AreEqual(costCenterDto.UnitId, costCenterVM.UnitId);
            Assert.AreEqual(costCenterDto.TotalBudget.ToString("C"), costCenterVM.TotalBudget);
            Assert.AreEqual((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances).ToString("C"), costCenterVM.TotalExpensesFormatted);
            Assert.AreEqual(costCenterDto.TotalActuals.ToString("C"), costCenterVM.TotalActuals);
            Assert.AreEqual(costCenterDto.TotalEncumbrances.ToString("C"), costCenterVM.TotalEncumbrances);

            Assert.AreEqual((-1 * costCenterDto.TotalBudgetRevenue).ToString("C"), costCenterVM.TotalBudgetRevenue);
            Assert.AreEqual((-1 * (costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue)).ToString("C"), costCenterVM.TotalExpensesRevenueFormatted);
            Assert.AreEqual((-1 * costCenterDto.TotalActualsRevenue).ToString("C"), costCenterVM.TotalActualsRevenue);
            Assert.AreEqual((-1 * costCenterDto.TotalEncumbrancesRevenue).ToString("C"), costCenterVM.TotalEncumbrancesRevenue);

            var expectedPercent = (Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100, MidpointRounding.AwayFromZero)).ToString() + " %";

            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercent);


        }

        [TestMethod]
        public void Constructor_CostCenterDtoIsNull()
        {
            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(null);
            decimal zero = 0m;

            Assert.AreEqual(null, costCenterVM.CostCenterId);
            Assert.AreEqual(null, costCenterVM.Name);
            Assert.AreEqual(null, costCenterVM.UnitId);
            Assert.AreEqual("", costCenterVM.TotalBudget);
            Assert.AreEqual("", costCenterVM.TotalExpensesFormatted);
            Assert.AreEqual("", costCenterVM.TotalActuals);
            Assert.AreEqual("", costCenterVM.TotalEncumbrances);

            Assert.AreEqual("", costCenterVM.TotalBudgetRevenue);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalExpensesRevenueFormatted);
            Assert.AreEqual("", costCenterVM.TotalActualsRevenue);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalEncumbrancesRevenue);
        }

        [TestMethod]
        public void Constructor_CostCenterDtoHasNoExpenseAccounts()
        {
            foreach(var subtotal in costCenterDto.CostCenterSubtotals)
            {
                subtotal.GlClass = GlClass.Revenue;
            }

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);

            Assert.AreEqual(costCenterDto.Id, costCenterVM.CostCenterId);
            Assert.AreEqual(costCenterDto.Name, costCenterVM.Name);
            Assert.AreEqual(costCenterDto.UnitId, costCenterVM.UnitId);
            Assert.AreEqual((-1 * costCenterDto.TotalBudgetRevenue).ToString("C"), costCenterVM.TotalBudgetRevenue);
            Assert.AreEqual((-1 * (costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue)).ToString("C"), costCenterVM.TotalExpensesRevenueFormatted);
            Assert.AreEqual((-1 * costCenterDto.TotalActualsRevenue).ToString("C"), costCenterVM.TotalActualsRevenue);
            Assert.AreEqual((-1 * costCenterDto.TotalEncumbrancesRevenue).ToString("C"), costCenterVM.TotalEncumbrancesRevenue);
        }


        #endregion

        #region BudgetRemaining tests

        [TestMethod]
        public void BudgetRemainingFormatted_PositiveAmount()
        {
            costCenterDto.TotalBudget = 1000.00m;
            costCenterDto.TotalActuals = 150.00m;
            costCenterDto.TotalEncumbrances = 100.00m;

            var expectedAmount = (costCenterDto.TotalBudget - (costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances)).ToString("C");

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_NegativeAmount()
        {
            costCenterDto.TotalBudget = 500.25m;
            costCenterDto.TotalActuals = 100.00m;
            costCenterDto.TotalEncumbrances = 900.25m;

            var expectedAmount = (costCenterDto.TotalBudget - (costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances)).ToString("C");

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_NoExpense()
        {
            costCenterDto.CostCenterSubtotals = null;

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(string.Empty, costCenterVM.BudgetRemainingFormatted);
        }

        #endregion

        #region ListViewBudgetSpentPercent tests

        [TestMethod]
        public void ListViewBudgetSpentPercent_Underbudget()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 35.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expectedPercent = (Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100, MidpointRounding.AwayFromZero)).ToString() + " %";

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_RoundEven()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 35.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expectedPercent = Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100, MidpointRounding.AwayFromZero).ToString() + " %";

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_RoundOdd()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 36.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expectedPercent = Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100, MidpointRounding.AwayFromZero).ToString() + " %";

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_Overbudget()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 145.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expected = (Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100)).ToString() + " %";

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(expected, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_ZeroExpenses()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = 0.00m;
            costCenterDto.TotalEncumbrances = 0.00m;

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual("0 %", costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_NonCreditExpenses()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = 50.00m;
            costCenterDto.TotalEncumbrances = 0.00m;

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual("101 %", costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_CreditExpenses()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = -50.00m;
            costCenterDto.TotalEncumbrances = 0m;

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual("0 %", costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_NoExpense()
        {
            costCenterDto.CostCenterSubtotals = null;

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(string.Empty, costCenterVM.ListViewBudgetSpentPercent);
        }

        #endregion

        #region FinancialHealthTextforScreenReader tests

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Green()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 0.00m;
            costCenterDto.TotalEncumbrances = 0.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(resourceFileDescription, costCenterVM.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Yellow()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 80.00m;
            costCenterDto.TotalEncumbrances = 6.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(resourceFileDescription, costCenterVM.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Red()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 100.00m;
            costCenterDto.TotalEncumbrances = 1.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");

            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(resourceFileDescription, costCenterVM.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_NoExpense()
        {
            costCenterDto.CostCenterSubtotals = null;
            
            BaseCostCenterViewModel costCenterVM = new BaseCostCenterViewModel(costCenterDto);
            Assert.AreEqual(string.Empty, costCenterVM.FinancialHealthTextForScreenReader);
        }

        #endregion
    }
}