﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    public class TestLedgerEntryDocumentDtoRepository
    {
        private JournalEntry JournalEntryDto;

        public TestLedgerEntryDocumentDtoRepository()
        {
            BuildJournalEntryDto();
        }

        public JournalEntry GetJournalEntry()
        {
            return JournalEntryDto;
        }

        private void BuildJournalEntryDto()
        {
            JournalEntryDto = new JournalEntry();
            JournalEntryDto.Id = "J123456";
            JournalEntryDto.Author = "Teresa Longerbeam Author DTO";
            JournalEntryDto.AutomaticReversal = false;
            JournalEntryDto.Comments = "Comments for the JE";
            JournalEntryDto.Date = new DateTime(2015, 3, 21);
            JournalEntryDto.EnteredByName = "Teresa Longerbeam";
            JournalEntryDto.EnteredDate = new DateTime(2015, 3, 11);
            JournalEntryDto.Status = JournalEntryStatus.Complete;
            JournalEntryDto.TotalCredits = 9876.54m;
            JournalEntryDto.TotalDebits = 9876.54m;
            JournalEntryDto.Type = JournalEntryType.General;

            JournalEntryDto.Approvers = new List<Approver>();
            JournalEntryDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Gary Thorne",
                ApprovalDate = new DateTime(2015, 1, 1)
            });
            JournalEntryDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Teresa Longerbeam",
                ApprovalDate = new DateTime(2015, 1, 2)
            });
            JournalEntryDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Andy Kleehammer",
                ApprovalDate = null
            });

            JournalEntryDto.Items = new List<JournalEntryItem>();
            JournalEntryDto.Items.Add(new JournalEntryItem()
            {
                Credit  = 0m,
                Debit = 2135.12m,
                Description = "First item for the JE",
                FormattedGlAccount = "11-00-01-00-33333-54005",
                GlAccount = "11_00_01_00_33333_54005",
                GlAccountDescription ="Department 33333 : Office Supplies",
                ProjectLineItemCode = "MA",
                ProjectNumber = "TGL-SS-CONS"
            });
            JournalEntryDto.Items.Add(new JournalEntryItem()
            {
                Credit = 0m,
                Debit = 3456.78m,
                Description = "Second item for the JE",
                FormattedGlAccount = "11-00-01-00-33333-54011",
                GlAccount = "11_00_01_00_33333_54011",
                GlAccountDescription = "Department 33333 : Laboratory Supplies",
                ProjectLineItemCode = "MA",
                ProjectNumber = "TGL-SS-CONS"
            });
            JournalEntryDto.Items.Add(new JournalEntryItem()
            {
                Credit = 0m,
                Debit = 4125.30m,
                Description = "Third item for the JE",
                FormattedGlAccount = "11-00-01-00-33333-54030",
                GlAccount = "11_00_01_00_33333_54030",
                GlAccountDescription = "Department 33333 : Janitorial Supplies",
                ProjectLineItemCode = "MA",
                ProjectNumber = "TGL-SS-CONS"
            });
            JournalEntryDto.Items.Add(new JournalEntryItem()
            {
                Credit = 0m,
                Debit = 159.34m,
                Description = "Fourth item for the JE",
                FormattedGlAccount = "11-00-01-00-33333-54400",
                GlAccount = "11_00_01_00_33333_54400",
                GlAccountDescription = "Department 33333 : Software under $100",
                ProjectLineItemCode = "MA",
                ProjectNumber = "TGL-SS-CONS"
            });
            JournalEntryDto.Items.Add(new JournalEntryItem()
            {
                Credit = 9876.54m,
                Debit = 0m,
                Description = "Fifth item for the JE",
                FormattedGlAccount = "11-00-01-00-33333-33333",
                GlAccount = "11_00_01_00_33333_33333",
                GlAccountDescription = "Department 33333 : Object 33333",
                ProjectLineItemCode = "EQ",
                ProjectNumber = "TGL-SS-CONS"
            });
        }
    }
}
