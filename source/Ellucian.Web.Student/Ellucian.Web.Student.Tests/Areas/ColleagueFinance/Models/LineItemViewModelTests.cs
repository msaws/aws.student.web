﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the LineItemGlDistributionViewModel class.
    /// </summary>
    [TestClass]
    public class LineItemViewModelTests
    {
        #region Initialize and Cleanup
        public LineItem lineItemDto;
        public LineItemViewModel lineItemVM;

        [TestInitialize]
        public void Initialize()
        {
            lineItemDto = new LineItem()
            {
                Description = "line item description",
                Quantity = 54m,
                Price = 159.55m,
                ExtendedPrice = 169.89m,
                UnitOfIssue = "UN",
                ExpectedDeliveryDate = new DateTime(2015, 1, 1),
                InvoiceNumber = "IN1234",
                TaxForm = "TF",
                TaxFormCode = "TFC",
                TaxFormLocation = "TFL",
                Comments = "Comments.",
                GlDistributions = new List<LineItemGlDistribution>()
                {
                    new LineItemGlDistribution()
                    {
                        GlAccount = "11_00_01_02_20601_53013",
                        FormattedGlAccount = "11-00-01-02-20601-53013",
                        ProjectNumber = "AJK-200",
                        ProjectLineItemCode = "AJK1",
                        Quantity = 10m,
                        Amount = 100m
                    }
                },
                LineItemTaxes = new List<LineItemTax>()
                {
                    new LineItemTax()
                    {
                        TaxCode = "VA",
                        TaxAmount = 150.55m
                    }
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            lineItemDto = null;
            lineItemVM = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void Constructor_FullyPopulated()
        {
            lineItemVM = new LineItemViewModel(lineItemDto);

            Assert.AreEqual(lineItemDto.Description, lineItemVM.Description);
            Assert.AreEqual(lineItemDto.Quantity, lineItemVM.Quantity);
            Assert.AreEqual(lineItemDto.Price, lineItemVM.Price);
            Assert.AreEqual(lineItemDto.ExtendedPrice, lineItemVM.ExtendedPrice);
            Assert.AreEqual(lineItemDto.VendorPart, lineItemVM.VendorItem);
            Assert.AreEqual(lineItemDto.UnitOfIssue, lineItemVM.UnitOfIssue);
            Assert.AreEqual(lineItemDto.ExpectedDeliveryDate.Value.ToShortDateString(), lineItemVM.ExpectedDeliveryDate);
            Assert.AreEqual(lineItemDto.InvoiceNumber, lineItemVM.InvoiceNumber);
            Assert.AreEqual(lineItemDto.TaxForm, lineItemVM.TaxForm);
            Assert.AreEqual(lineItemDto.TaxFormCode, lineItemVM.TaxFormCode);
            Assert.AreEqual(lineItemDto.TaxFormLocation, lineItemVM.TaxFormLocation);
            Assert.AreEqual(lineItemDto.Comments, lineItemVM.Comments);

            Assert.IsTrue(lineItemVM.GlDistributions is List<LineItemGlDistributionViewModel>);
            Assert.AreEqual(lineItemDto.GlDistributions.Count(), lineItemVM.GlDistributions.Count());

            Assert.IsTrue(lineItemVM.TaxDistributions is List<LineItemTaxViewModel>);
            Assert.AreEqual(lineItemDto.LineItemTaxes.Count(), lineItemVM.TaxDistributions.Count());
        }

        [TestMethod]
        public void Constructor_NullObjectsAreNull()
        {
            lineItemDto.ExpectedDeliveryDate = null;
            lineItemDto.GlDistributions = null;
            lineItemDto.LineItemTaxes = null;
            lineItemVM = new LineItemViewModel(lineItemDto);

            Assert.AreEqual(lineItemDto.Description, lineItemVM.Description);
            Assert.AreEqual(lineItemDto.Quantity, lineItemVM.Quantity);
            Assert.AreEqual(lineItemDto.Price, lineItemVM.Price);
            Assert.AreEqual(lineItemDto.ExtendedPrice, lineItemVM.ExtendedPrice);
            Assert.AreEqual(lineItemDto.VendorPart, lineItemVM.VendorItem);
            Assert.AreEqual(lineItemDto.UnitOfIssue, lineItemVM.UnitOfIssue);
            Assert.AreEqual("", lineItemVM.ExpectedDeliveryDate);
            Assert.AreEqual(lineItemDto.InvoiceNumber, lineItemVM.InvoiceNumber);
            Assert.AreEqual(lineItemDto.TaxForm, lineItemVM.TaxForm);
            Assert.AreEqual(lineItemDto.TaxFormCode, lineItemVM.TaxFormCode);
            Assert.AreEqual(lineItemDto.TaxFormLocation, lineItemVM.TaxFormLocation);
            Assert.AreEqual(lineItemDto.Comments, lineItemVM.Comments);

            Assert.IsTrue(lineItemVM.GlDistributions is List<LineItemGlDistributionViewModel>);
            Assert.IsTrue(lineItemVM.GlDistributions.Count() == 0);

            Assert.IsTrue(lineItemVM.TaxDistributions is List<LineItemTaxViewModel>);
            Assert.IsTrue(lineItemVM.TaxDistributions.Count() == 0);
        }
        #endregion

        #region PriceDisplay tests
        [TestMethod]
        public void PriceDisplay()
        {
            lineItemVM = new LineItemViewModel(lineItemDto);
            Assert.AreEqual(lineItemDto.Price.ToString("C"), lineItemVM.PriceDisplay);
        }
        #endregion

        #region ExtendedPriceDisplay tests
        [TestMethod]
        public void ExtendedPriceDisplay()
        {
            lineItemVM = new LineItemViewModel(lineItemDto);
            Assert.AreEqual(lineItemDto.ExtendedPrice.ToString("C"), lineItemVM.ExtendedPriceDisplay);
        }
        #endregion
    }
}
