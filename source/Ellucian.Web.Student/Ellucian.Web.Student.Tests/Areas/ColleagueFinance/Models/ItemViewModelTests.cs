﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// Test the ItemViewModel
    /// </summary>
    [TestClass]
    public class ItemViewModelTests
    {
        #region Initialize and Cleanup
        public JournalEntryItem journalEntryItemDto;
        public ItemViewModel itemVM;

        [TestInitialize]
        public void Initialize()
        {
            journalEntryItemDto = new JournalEntryItem()
            {
                Credit = 0m,
                Debit = 2135.12m,
                Description = "One item for the JE",
                FormattedGlAccount = "11-00-01-00-33333-54005",
                GlAccount = "11_00_01_00_33333_54005",
                GlAccountDescription = "Department 33333 : Office Supplies",
                ProjectLineItemCode = "MA",
                ProjectNumber = "TGL-SS-CONS"
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            journalEntryItemDto = null;
            itemVM = null;
        }
        #endregion

        #region Constructor tests

        [TestMethod]
        public void Constructor_FullyPopulated()
        {
            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(journalEntryItemDto.Credit, itemVM.Credit);
            Assert.AreEqual(journalEntryItemDto.Debit, itemVM.Debit);
            Assert.AreEqual(journalEntryItemDto.Description, itemVM.Description);
            Assert.AreEqual(journalEntryItemDto.FormattedGlAccount, itemVM.FormattedGlAccount);
            Assert.AreEqual(journalEntryItemDto.GlAccount, itemVM.GlAccount);
            Assert.AreEqual(journalEntryItemDto.GlAccountDescription, itemVM.GlAccountDescription);
            Assert.AreEqual(journalEntryItemDto.ProjectLineItemCode, itemVM.ProjectLineItemCode);
            Assert.AreEqual(journalEntryItemDto.ProjectNumber, itemVM.ProjectNumber);
        }

        [TestMethod]
        public void Constructor_NullObjectsAreNull()
        {
            journalEntryItemDto.ProjectLineItemCode = null;
            journalEntryItemDto.ProjectNumber = null;

            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(journalEntryItemDto.Credit, itemVM.Credit);
            Assert.AreEqual(journalEntryItemDto.Debit, itemVM.Debit);
            Assert.AreEqual(journalEntryItemDto.Description, itemVM.Description);
            Assert.AreEqual(journalEntryItemDto.FormattedGlAccount, itemVM.FormattedGlAccount);
            Assert.AreEqual(journalEntryItemDto.GlAccount, itemVM.GlAccount);
            Assert.AreEqual(journalEntryItemDto.GlAccountDescription, itemVM.GlAccountDescription);
            Assert.AreEqual(null, itemVM.ProjectLineItemCode);
            Assert.AreEqual(null, itemVM.ProjectNumber);
        }

        [TestMethod]
        public void Constructor_NoCreditAmount()
        {
            journalEntryItemDto.Credit = 0m;
            journalEntryItemDto.Debit = 2135.12m;

            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(journalEntryItemDto.Credit, itemVM.Credit);
            Assert.AreEqual(journalEntryItemDto.Debit, itemVM.Debit);
        }

        [TestMethod]
        public void Constructor_NoDebitAmount()
        {
            journalEntryItemDto.Credit = 2135.12m;
            journalEntryItemDto.Debit = 0m;

            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(journalEntryItemDto.Credit, itemVM.Credit);
            Assert.AreEqual(journalEntryItemDto.Debit, itemVM.Debit);
        }

        [TestMethod]
        public void Constructor_GlDescriptionLong()
        {
            var longGlDesc = "1234567890123456789012345678901234567890123456789012345678901234567890";
            journalEntryItemDto.GlAccountDescription = longGlDesc;

            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(longGlDesc.Substring(0, 60), itemVM.GlAccountDescription);
        }


        [TestMethod]
        public void Constructor_NoGlDescription()
        {
            journalEntryItemDto.GlAccountDescription = null;

            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(string.Empty, itemVM.GlAccountDescription);
        }
        #endregion

        #region DebitDisplay tests
        [TestMethod]
        public void DebitDisplayNoValue()
        {
            journalEntryItemDto.Debit = null;
            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(string.Empty, itemVM.DebitDisplay);
        }
        [TestMethod]
        public void DebitDisplayHasValue()
        {
            journalEntryItemDto.Debit = 100.00m;
            var debitAmount = 100.00m;
            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(debitAmount.ToString("C"), itemVM.DebitDisplay);
        }
        #endregion

        #region CreditDisplay tests
        [TestMethod]
        public void CreditDisplayNoValue()
        {
            journalEntryItemDto.Credit = null;
            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(string.Empty, itemVM.CreditDisplay);
        }
        [TestMethod]
        public void CreditDisplayHasValue()
        {
            journalEntryItemDto.Credit = 100.00m;
            var creditAmount = 100.00m;
            itemVM = new ItemViewModel(journalEntryItemDto);

            Assert.AreEqual(creditAmount.ToString("C"), itemVM.CreditDisplay);
        }
        #endregion
    }
}
