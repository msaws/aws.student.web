﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the PurchaseOrderNumberViewModel
    /// </summary>
    [TestClass]
    public class PurchaseOrderNumberViewModelTests
    {
        #region Initialize and Cleanup
        public PurchaseOrder purchaseOrderDto;
        public PurchaseOrderNumberViewModel purchaseOrderNumberVM;
        public TestDocumentDtoRepository documentRepository;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            purchaseOrderDto = null;
            purchaseOrderNumberVM = null;
            documentRepository = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void PurchaseOrderNumberViewModel_VerifyBaseFinanceDocument()
        {
            purchaseOrderDto = documentRepository.GetPurchaseOrder();
            purchaseOrderNumberVM = new PurchaseOrderNumberViewModel(purchaseOrderDto.Id);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(purchaseOrderDto.Id, purchaseOrderNumberVM.Id, "The document ID should be initialized.");
        }
        #endregion
    }
}
