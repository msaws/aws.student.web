﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the ProjectLineItemGlAccountViewModel class.
    /// </summary>
    [TestClass]
    public class GlTransactionViewModelTests
    {
        #region Initialize and Cleanup
        public GlTransaction glTransactionDto;
        public GlTransactionViewModel glTransactionVM;

        [TestInitialize]
        public void Initialize()
        {
            // Set up the DTO to create the VM.
            glTransactionDto = new GlTransaction();
            glTransactionDto.Amount = 125.00m;
            glTransactionDto.Description = "Transaction description.";
            glTransactionDto.ReferenceNumber = "V0000001";
            glTransactionDto.TransactionDate = new DateTime(2014, 11, 25);

            // Set up the VM
            glTransactionVM = new GlTransactionViewModel(glTransactionDto);
        }

        [TestCleanup]
        public void Cleanup()
        {
            glTransactionDto = null;
            glTransactionVM = null;
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void GlTransactionViewModelConstructor()
        {
            Assert.AreEqual(glTransactionDto.Amount.ToString("C"), glTransactionVM.Amount);
            Assert.AreEqual(glTransactionDto.Description, glTransactionVM.Description);
            Assert.AreEqual(glTransactionDto.DocumentId, glTransactionVM.DocumentId);
            Assert.AreEqual(glTransactionDto.ReferenceNumber, glTransactionVM.ReferenceNumber);
            Assert.AreEqual(glTransactionDto.TransactionDate.ToShortDateString(), glTransactionVM.Date);
        }
        #endregion
    }
}
