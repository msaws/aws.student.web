﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class CostCenterViewModelTests
    {
        #region Initialize and Cleanup
        private TestCostCenterDtoRepository testRepository;
        private CostCenter costCenterDto;

        [TestInitialize]
        public void Initialize()
        {
            testRepository = new TestCostCenterDtoRepository();
            costCenterDto = testRepository.GetCostCenter();
        }

        [TestCleanup]
        public void Cleanup()
        {
            testRepository = null;
            costCenterDto = null;
        }
        #endregion
        
        #region Constructor tests
        [TestMethod]
        public void Constructor()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);

            Assert.AreEqual(costCenterDto.Id, costCenterVM.CostCenterId);
            Assert.AreEqual(costCenterDto.Name, costCenterVM.Name);
            Assert.AreEqual(costCenterDto.UnitId, costCenterVM.UnitId);
            Assert.AreEqual(costCenterDto.TotalBudget.ToString("C"), costCenterVM.TotalBudget);
            Assert.AreEqual(costCenterDto.TotalActuals.ToString("C"), costCenterVM.TotalActuals);
            Assert.AreEqual(costCenterDto.TotalEncumbrances.ToString("C"), costCenterVM.TotalEncumbrances);
            Assert.AreEqual((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances).ToString("C"), costCenterVM.TotalExpensesFormatted);

            Assert.AreEqual(costCenterDto.CostCenterSubtotals.Where(s => s.GlClass == GlClass.Expense).ToList().Count, costCenterVM.CostCenterExpenseSubtotals.Count);
            Assert.AreEqual(costCenterDto.CostCenterSubtotals.Where(s => s.GlClass == GlClass.Revenue).ToList().Count, costCenterVM.CostCenterRevenueSubtotals.Count);

            Assert.AreEqual((-1 * costCenterDto.TotalBudgetRevenue).ToString("C"), costCenterVM.TotalBudgetRevenue);
            Assert.AreEqual((-1 * costCenterDto.TotalActualsRevenue).ToString("C"), costCenterVM.TotalActualsRevenue);
            Assert.AreEqual((-1 * costCenterDto.TotalEncumbrancesRevenue).ToString("C"), costCenterVM.TotalEncumbrancesRevenue);
            Assert.AreEqual((-1 * (costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue)).ToString("C"), costCenterVM.TotalExpensesRevenueFormatted);
            
            Assert.AreEqual(((-1 * costCenterDto.TotalBudgetRevenue) - costCenterDto.TotalBudget).ToString("C"), costCenterVM.TotalBudgetNet);
            Assert.AreEqual(((-1 * costCenterDto.TotalActualsRevenue) - costCenterDto.TotalActuals).ToString("C"), costCenterVM.TotalActualsNet);

        }

        [TestMethod]
        public void Constructor_CostCenterDtoIsNull()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(null);
            decimal zero = 0m;

            Assert.AreEqual(null, costCenterVM.CostCenterId);
            Assert.AreEqual(null, costCenterVM.Name);
            Assert.AreEqual(null, costCenterVM.UnitId);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalBudget);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalExpensesFormatted);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalActuals);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalEncumbrances);
            Assert.AreEqual(null, costCenterVM.CostCenterExpenseSubtotals);

            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalBudgetRevenue);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalExpensesRevenueFormatted);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalActualsRevenue);
            Assert.AreEqual(zero.ToString("C"), costCenterVM.TotalEncumbrancesRevenue);
        }

        [TestMethod]
        public void Constructor_CostCenterSubtotalsContainsNull()
        {
            costCenterDto.CostCenterSubtotals[1] = null;
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);

            Assert.AreEqual(costCenterDto.CostCenterSubtotals.Count - 1, costCenterVM.CostCenterExpenseSubtotals.Count);
        }
        #endregion

        #region BudgetRemaining tests

        // Expense properties
        [TestMethod]
        public void BudgetRemainingTest()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            var expectedAmount = costCenterDto.TotalBudget - (costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemaining);
            Assert.AreEqual(costCenterVM.BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), costCenterVM.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingStringTest()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(costCenterVM.BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), costCenterVM.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingStringNegativeTest()
        {
            costCenterDto.TotalBudget = 500.25m;
            costCenterDto.TotalActuals = 900.25m;
            costCenterDto.TotalEncumbrances = 900.25m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(costCenterVM.BudgetRemaining.ToString("C"), costCenterVM.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_PositiveAmount()
        {
            costCenterDto.TotalBudget = 1000.00m;
            costCenterDto.TotalActuals = 150.00m;
            costCenterDto.TotalEncumbrances = 100.00m;

            var expectedAmount = (costCenterDto.TotalBudget - (costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances)).ToString("C");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_NegativeAmount()
        {
            costCenterDto.TotalBudget = 500.25m;
            costCenterDto.TotalActuals = 100.00m;
            costCenterDto.TotalEncumbrances = 900.25m;

            var expectedAmount = (costCenterDto.TotalBudget - (costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances)).ToString("C");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingScreenReaderText_PositiveAmount()
        {
            costCenterDto.TotalBudget = 500.25m;
            costCenterDto.TotalActuals = 100.00m;
            costCenterDto.TotalEncumbrances = 0.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            
            var expected = costCenterVM.BudgetSpentAmount;
            Assert.AreEqual(expected, costCenterVM.BudgetRemainingScreenReaderText);
        }

        [TestMethod]
        public void BudgetRemainingScreenReaderText_NegativeAmount()
        {
            costCenterDto.TotalBudget = 500.25m;
            costCenterDto.TotalActuals = 100.00m;
            costCenterDto.TotalEncumbrances = 900.25m;

            var expectedAmount = Math.Abs((costCenterDto.TotalBudget - (costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances))).ToString("C") +
                 GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterOverbudgetText");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingScreenReaderText);
        }

        // Revenue properties
        [TestMethod]
        public void BudgetRemainingRevenueTest()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            var expectedAmount = (costCenterDto.TotalBudgetRevenue - (costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue)) * -1;
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingRevenue);
            Assert.AreEqual(costCenterVM.BudgetRemainingRevenue.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), costCenterVM.BudgetRemainingRevenueString);
        }

        [TestMethod]
        public void BudgetRemainingRevenueStringTest()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(costCenterVM.BudgetRemainingRevenue.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), costCenterVM.BudgetRemainingRevenueString);
        }

        [TestMethod]
        public void BudgetRemainingRevenueString_NegativeTest()
        {
            costCenterDto.TotalBudgetRevenue = -500.25m;
            costCenterDto.TotalActualsRevenue = -900.25m;
            costCenterDto.TotalEncumbrancesRevenue = -900.25m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(costCenterVM.BudgetRemainingRevenue.ToString("C"), costCenterVM.BudgetRemainingRevenueString);
        }

        [TestMethod]
        public void BudgetRemainingRevenueString_PositiveTest()
        {
            costCenterDto.TotalBudgetRevenue = -500.25m;
            costCenterDto.TotalActualsRevenue = -100.25m;
            costCenterDto.TotalEncumbrancesRevenue = 0.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);

            var expected = costCenterVM.BudgetRemainingRevenue.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText");
            
            Assert.AreEqual(expected, costCenterVM.BudgetRemainingRevenueString);
        }

        [TestMethod]
        public void BudgetRemainingRevenueFormatted_PositiveAmount()
        {
            costCenterDto.TotalBudgetRevenue = -1000.00m;
            costCenterDto.TotalActualsRevenue = -150.00m;
            costCenterDto.TotalEncumbrancesRevenue = -100.00m;

            var expectedAmount = ((costCenterDto.TotalBudgetRevenue - (costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue)) * -1).ToString("C");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingRevenueFormatted);
        }
        
        [TestMethod]
        public void BudgetRemainingRevenueFormatted_NegativeAmount()
        {
            costCenterDto.TotalBudgetRevenue = -500.25m;
            costCenterDto.TotalActualsRevenue = -100.00m;
            costCenterDto.TotalEncumbrancesRevenue = -900.25m;

            var expectedAmount = ((costCenterDto.TotalBudgetRevenue - (costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue)) * -1).ToString("C");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedAmount, costCenterVM.BudgetRemainingRevenueFormatted);
        }

        [TestMethod]
        public void BudgetRemainingScreenReaderTextRevenue_NegativeAmount()
        {
            costCenterDto.TotalBudgetRevenue = -500.25m;
            costCenterDto.TotalActualsRevenue = -100.00m;
            costCenterDto.TotalEncumbrancesRevenue = -900.25m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);

            var expected = Math.Abs(costCenterVM.BudgetRemainingRevenue).ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterOverbudgetText");

            Assert.AreEqual(expected, costCenterVM.BudgetRemainingScreenReaderTextRevenue);
        }

        [TestMethod]
        public void BudgetRemainingScreenReaderTextRevenue_PositiveAmount()
        {
            costCenterDto.TotalBudgetRevenue = -500.25m;
            costCenterDto.TotalActualsRevenue = -100.00m;
            costCenterDto.TotalEncumbrancesRevenue = 0.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);

            var expected = costCenterVM.BudgetReceivedAmount;

            Assert.AreEqual(expected, costCenterVM.BudgetRemainingScreenReaderTextRevenue);
        }

        #endregion

        #region Overage Tests

        //Expense properties
        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetZero()
        {
            costCenterDto.TotalBudget = 0m;
            costCenterDto.TotalActuals = 500m;
            costCenterDto.TotalEncumbrances = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(100.00m, costCenterVM.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageGreaterThan100()
        {
            costCenterDto.TotalBudget = 364.89m;
            costCenterDto.TotalActuals = 1000.25m;
            costCenterDto.TotalEncumbrances = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(100.00m, costCenterVM.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageLessThan100()
        {
            costCenterDto.TotalBudget = 900.00m;
            costCenterDto.TotalActuals = 900.25m;
            costCenterDto.TotalEncumbrances = 100.00m;

            var expectedOverage = Math.Round(((((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) - costCenterDto.TotalBudget) / costCenterDto.TotalBudget) * 100), 2);

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedOverage, costCenterVM.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetEqualsExpenses()
        {
            costCenterDto.TotalBudget = 900.25m;
            costCenterDto.TotalActuals = 800.25m;
            costCenterDto.TotalEncumbrances = 100.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0.00m, costCenterVM.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetZero()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = -144.94m;
            costCenterDto.TotalEncumbrances = -10.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0.00m, costCenterVM.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetGreaterThanExpenses_BudgetGreaterThanZero()
        {
            costCenterDto.TotalBudget = 900.25m;
            costCenterDto.TotalActuals = 400.25m;
            costCenterDto.TotalEncumbrances = 100.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0.00m, costCenterVM.Overage);
        }

        // Revenue properties
        [TestMethod]
        public void OverageRevenueTest_BudgetLessThanExpenses_BudgetZero()
        {
            costCenterDto.TotalBudgetRevenue = 0m;
            costCenterDto.TotalActualsRevenue = -500m;
            costCenterDto.TotalEncumbrancesRevenue = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(100.00m, costCenterVM.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageGreaterThan100()
        {
            costCenterDto.TotalBudgetRevenue = -364.89m;
            costCenterDto.TotalActualsRevenue = -1000.25m;
            costCenterDto.TotalEncumbrancesRevenue = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(100.00m, costCenterVM.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageLessThan100()
        {
            costCenterDto.TotalBudgetRevenue = -900.00m;
            costCenterDto.TotalActualsRevenue = -900.25m;
            costCenterDto.TotalEncumbrancesRevenue = -100.00m;

            var expectedOverage = Math.Round(((((costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue) - costCenterDto.TotalBudgetRevenue) / costCenterDto.TotalBudgetRevenue) * 100), 2);

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedOverage, costCenterVM.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetEqualsExpenses()
        {
            costCenterDto.TotalBudgetRevenue = 900.25m;
            costCenterDto.TotalActualsRevenue = 800.25m;
            costCenterDto.TotalEncumbrancesRevenue = 100.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0.00m, costCenterVM.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetZero()
        {
            costCenterDto.TotalBudgetRevenue = 0.00m;
            costCenterDto.TotalActualsRevenue = 144.94m;
            costCenterDto.TotalEncumbrancesRevenue = 10.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0.00m, costCenterVM.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetGreaterThanExpenses_BudgetGreaterThanZero()
        {
            costCenterDto.TotalBudgetRevenue = -900.25m;
            costCenterDto.TotalActualsRevenue = -400.25m;
            costCenterDto.TotalEncumbrancesRevenue = -100.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0.00m, costCenterVM.OverageRevenue);
        }

        #endregion

        #region Budget Spent Tests

        // Expense properties
        [TestMethod]
        public void BudgetSpentPercent_ZeroTest()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = 0.00m;
            costCenterDto.TotalEncumbrances = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0m, costCenterVM.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_NegativeExpensesTest()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = -43.25m;
            costCenterDto.TotalEncumbrances = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0m, costCenterVM.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_BudgetEqualsExpensesTest()
        {
            costCenterDto.TotalBudget = 900.25m;
            costCenterDto.TotalActuals = 400.00m;
            costCenterDto.TotalEncumbrances = 500.25m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(100m, costCenterVM.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_CalculatedTest()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 20.00m;
            costCenterDto.TotalEncumbrances = 28.44m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(48m, costCenterVM.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_ExpensesGreaterThanBudget_BudgetZero()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = 600.00m;
            costCenterDto.TotalEncumbrances = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0m, costCenterVM.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_ExpensesGreaterThanBudget_BudgetNotZero()
        {
            costCenterDto.TotalBudget = 1000.00m;
            costCenterDto.TotalActuals = 1610.23m;
            costCenterDto.TotalEncumbrances = 100.00m;

            var expectedPercent = Math.Round(100 - (Math.Round(((((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) - costCenterDto.TotalBudget) / costCenterDto.TotalBudget) * 100), 2)));

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.BudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_Underbudget()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 35.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expectedPercent = (Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100, MidpointRounding.AwayFromZero)).ToString() + " %";
            
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_RoundEven()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 35.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expectedPercent = Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100, MidpointRounding.AwayFromZero).ToString() + " %";

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_RoundOdd()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 36.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expectedPercent = Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100, MidpointRounding.AwayFromZero).ToString() + " %";

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_Overbudget()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 145.50m;
            costCenterDto.TotalEncumbrances = 10.00m;

            var expected = (Math.Round(((costCenterDto.TotalActuals + costCenterDto.TotalEncumbrances) / costCenterDto.TotalBudget) * 100)).ToString() + " %";

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expected, costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_ZeroExpenses()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = 0.00m;
            costCenterDto.TotalEncumbrances = 0.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual("0 %", costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_NonCreditExpenses()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = 50.00m;
            costCenterDto.TotalEncumbrances = 0.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual("101 %", costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_CreditExpenses()
        {
            costCenterDto.TotalBudget = 0.00m;
            costCenterDto.TotalActuals = -50.00m;
            costCenterDto.TotalEncumbrances = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual("0 %", costCenterVM.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentAmount()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            
            var expectedBudgetSpentAmount = costCenterVM.TotalExpensesFormatted + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarText") + costCenterVM.TotalBudget;
            Assert.AreEqual(expectedBudgetSpentAmount, costCenterVM.BudgetSpentAmount);
        }

        // Revenue properties
        [TestMethod]
        public void BudgetSpentPercentRevenue_ZeroTest()
        {
            costCenterDto.TotalBudgetRevenue = 0.00m;
            costCenterDto.TotalActualsRevenue = 0.00m;
            costCenterDto.TotalEncumbrancesRevenue = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0m, costCenterVM.BudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void BudgetSpentPercentRevenue_NegativeExpensesTest()
        {
            costCenterDto.TotalBudgetRevenue = 0.00m;
            costCenterDto.TotalActualsRevenue = -43.25m;
            costCenterDto.TotalEncumbrancesRevenue = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0m, costCenterVM.BudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void BudgetSpentPercentRevenue_BudgetEqualsExpensesTest()
        {
            costCenterDto.TotalBudgetRevenue = -900.25m;
            costCenterDto.TotalActualsRevenue = -400.00m;
            costCenterDto.TotalEncumbrancesRevenue = -500.25m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(100m, costCenterVM.BudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void BudgetSpentPercentRevenue_CalculatedTest()
        {
            costCenterDto.TotalBudgetRevenue = -100.00m;
            costCenterDto.TotalActualsRevenue = -20.00m;
            costCenterDto.TotalEncumbrancesRevenue = -28.44m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(48m, costCenterVM.BudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void BudgetSpentPercentRevenue_ExpensesGreaterThanBudget_BudgetZero()
        {
            costCenterDto.TotalBudgetRevenue = 0.00m;
            costCenterDto.TotalActualsRevenue = 600.00m;
            costCenterDto.TotalEncumbrancesRevenue = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(0m, costCenterVM.BudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void BudgetSpentPercentRevenue_ExpensesGreaterThanBudget_BudgetNotZero()
        {
            costCenterDto.TotalBudgetRevenue = -1000.00m;
            costCenterDto.TotalActualsRevenue = -1610.23m;
            costCenterDto.TotalEncumbrancesRevenue = -100.00m;

            var expectedPercent = Math.Round(100 - (Math.Round(((((costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue) - costCenterDto.TotalBudgetRevenue) / costCenterDto.TotalBudgetRevenue) * 100), 2)));

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.BudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercentRevenue_Underbudget()
        {
            costCenterDto.TotalBudgetRevenue = 100.00m;
            costCenterDto.TotalActualsRevenue = 35.50m;
            costCenterDto.TotalEncumbrancesRevenue = 10.00m;

            var expectedPercent = (Math.Round(((costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue) / costCenterDto.TotalBudgetRevenue) * 100, MidpointRounding.AwayFromZero)).ToString() + " %";

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercentRevenue_RoundEven()
        {
            costCenterDto.TotalBudgetRevenue = 100.00m;
            costCenterDto.TotalActualsRevenue = 35.50m;
            costCenterDto.TotalEncumbrancesRevenue = 10.00m;

            var expectedPercent = Math.Round(((costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue) / costCenterDto.TotalBudgetRevenue) * 100, MidpointRounding.AwayFromZero).ToString() + " %";

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercentRevenue_RoundOdd()
        {
            costCenterDto.TotalBudgetRevenue = 100.00m;
            costCenterDto.TotalActualsRevenue = 36.50m;
            costCenterDto.TotalEncumbrancesRevenue = 10.00m;

            var expectedPercent = Math.Round(((costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue) / costCenterDto.TotalBudgetRevenue) * 100, MidpointRounding.AwayFromZero).ToString() + " %";

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expectedPercent, costCenterVM.ListViewBudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercentRevenue_Overbudget()
        {
            costCenterDto.TotalBudgetRevenue = 100.00m;
            costCenterDto.TotalActualsRevenue = 145.50m;
            costCenterDto.TotalEncumbrancesRevenue = 10.00m;

            var expected = (Math.Round(((costCenterDto.TotalActualsRevenue + costCenterDto.TotalEncumbrancesRevenue) / costCenterDto.TotalBudgetRevenue) * 100)).ToString() + " %";

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(expected, costCenterVM.ListViewBudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercentRevenue_ZeroBudget_ZeroExpenses()
        {
            costCenterDto.TotalBudgetRevenue = 0.00m;
            costCenterDto.TotalActualsRevenue = 0.00m;
            costCenterDto.TotalEncumbrancesRevenue = 0.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual("0 %", costCenterVM.ListViewBudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercentRevenue_ZeroBudget_NonCreditExpenses()
        {
            costCenterDto.TotalBudgetRevenue = 0.00m;
            costCenterDto.TotalActualsRevenue = -50.00m;
            costCenterDto.TotalEncumbrancesRevenue = 0.00m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual("101 %", costCenterVM.ListViewBudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercentRevenue_ZeroBudget_CreditExpenses()
        {
            costCenterDto.TotalBudgetRevenue = 0.00m;
            costCenterDto.TotalActualsRevenue = 50.00m;
            costCenterDto.TotalEncumbrancesRevenue = 0m;

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual("0 %", costCenterVM.ListViewBudgetSpentPercentRevenue);
        }

        [TestMethod]
        public void BudgetReceivedAmount()
        {
            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            
            var expected = costCenterVM.TotalExpensesRevenueFormatted +
                GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarText") +
                costCenterVM.TotalBudgetRevenue + " " +
                GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetedText");
            Assert.AreEqual(expected, costCenterVM.BudgetReceivedAmount);

            
        }

        #endregion

        #region FinancialHealthTextforScreenReader texts

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Green()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 0.00m;
            costCenterDto.TotalEncumbrances = 0.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(resourceFileDescription, costCenterVM.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Yellow()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 80.00m;
            costCenterDto.TotalEncumbrances = 6.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(resourceFileDescription, costCenterVM.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Red()
        {
            costCenterDto.TotalBudget = 100.00m;
            costCenterDto.TotalActuals = 100.00m;
            costCenterDto.TotalEncumbrances = 1.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");

            CostCenterViewModel costCenterVM = new CostCenterViewModel(costCenterDto);
            Assert.AreEqual(resourceFileDescription, costCenterVM.FinancialHealthTextForScreenReader);
        }

        #endregion
    }
}
