﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Areas.ColleagueFinance;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// Test the GeneralLegerComponentViewModel class.
    /// </summary>
    [TestClass]
    public class GeneralLedgerComponentViewModelTests
    {
        #region Initialize and Cleanup

        private GeneralLedgerComponent glComponentDto;
        public GeneralLedgerComponentViewModel glComponentVM;

        [TestInitialize]
        public void Initialize()
        {
            glComponentDto = new GeneralLedgerComponent()
            {
                ComponentName = "FUND",
                ComponentLength = 2,
                StartPosition = 1
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            glComponentDto = null;
            glComponentVM = null;
        }

        #endregion

        [TestMethod]
        public void Constructor()
        {
            glComponentVM = new GeneralLedgerComponentViewModel(glComponentDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(glComponentVM is GeneralLedgerComponentViewModel);
            Assert.AreEqual(glComponentDto.ComponentLength.ToString(), glComponentVM.ComponentLength);
            Assert.AreEqual("Fund", glComponentVM.ComponentName);
            Assert.AreEqual(glComponentDto.StartPosition, glComponentVM.StartPosition);
        }

        [TestMethod]
        public void Constructor_Pascal_Case()
        {
            glComponentDto.ComponentName = "Fund";
            glComponentVM = new GeneralLedgerComponentViewModel(glComponentDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(glComponentVM is GeneralLedgerComponentViewModel);
            Assert.AreEqual(glComponentDto.ComponentLength.ToString(), glComponentVM.ComponentLength);
            Assert.AreEqual(glComponentDto.ComponentName, glComponentVM.ComponentName);
            Assert.AreEqual("Fund", glComponentVM.ComponentName);
            Assert.AreEqual(glComponentDto.StartPosition, glComponentVM.StartPosition);
        }

        [TestMethod]
        public void Constructor_GlComponentDtoIsNull()
        {
            glComponentVM = new GeneralLedgerComponentViewModel(null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(glComponentVM is GeneralLedgerComponentViewModel);
            Assert.IsNull(glComponentVM.ComponentLength);
            Assert.IsNull(glComponentVM.ComponentName);
            Assert.AreEqual(0, glComponentVM.StartPosition);
        }
    }
}
