﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the BlanketPurchaseOrderViewModel
    /// </summary>
    [TestClass]
    public class BlanketPurchaseOrderViewModelTests
    {
        #region Initialize and Cleanup
        public BlanketPurchaseOrder blanketPurchaseOrderDto;
        public BlanketPurchaseOrderViewModel blanketPurchaseOrderVM;
        public TestDocumentDtoRepository documentRepository;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            blanketPurchaseOrderDto = null;
            blanketPurchaseOrderVM = null;
            documentRepository = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void BlanketPurchaseOrderViewModel_VerifyBaseFinanceDocument()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(blanketPurchaseOrderDto.Id, blanketPurchaseOrderVM.DocumentId, "The document ID should be initialized.");
            Assert.AreEqual(blanketPurchaseOrderDto.Number, blanketPurchaseOrderVM.Number, "The document number should be initialized.");
            Assert.AreEqual(blanketPurchaseOrderDto.Comments, blanketPurchaseOrderVM.Comments, "The document comments list should be initialized.");
            Assert.IsTrue(blanketPurchaseOrderVM.Approvers is List<DocumentApproverViewModel>, "The document approvers should be the correct type.");
            Assert.IsTrue(blanketPurchaseOrderVM.LineItems is List<LineItemViewModel>, "The document line items list should be the correct type.");
            Assert.IsTrue(blanketPurchaseOrderVM.IsBlanketPurchaseOrder, "The document is a purchase order.");
            Assert.IsTrue(blanketPurchaseOrderVM.IsProcurementDocument, "The document is a procurement document.");
            Assert.IsFalse(blanketPurchaseOrderVM.IsRequisition, "The document is not a requisition.");
            Assert.IsFalse(blanketPurchaseOrderVM.IsPurchaseOrder, "The document is not a purchase order.");
            Assert.IsFalse(blanketPurchaseOrderVM.IsVoucher, "The document is not a voucher.");
            Assert.IsFalse(blanketPurchaseOrderVM.IsLedgerEntryDocument, "The document is not a ledger entry document.");
            Assert.IsFalse(blanketPurchaseOrderVM.IsJournalEntry, "The document is not a journal entry.");
        }

        [TestMethod]
        public void BlanketPurchaseOrderViewModel_VerifyAccountsPayablePurchasingDocument()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Verify that the AccountsPayablePurchasingDocumentViewModel initialized the appropriate variables
            Assert.AreEqual(blanketPurchaseOrderDto.Date.ToShortDateString(), blanketPurchaseOrderVM.Date, "The document date should be initialized.");
        }

        [TestMethod]
        public void PurchaseOrderViewModel_RemainingProperties()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(blanketPurchaseOrderDto.Status, blanketPurchaseOrderVM.Status);
            Assert.AreEqual(blanketPurchaseOrderDto.StatusDate.ToShortDateString(), blanketPurchaseOrderVM.StatusDate);
            Assert.AreEqual(blanketPurchaseOrderDto.Description, blanketPurchaseOrderVM.Description);
            Assert.AreEqual(blanketPurchaseOrderDto.ExpirationDate.Value.ToShortDateString(), blanketPurchaseOrderVM.ExpirationDate);
            Assert.AreEqual(blanketPurchaseOrderDto.InitiatorName, blanketPurchaseOrderVM.InitiatorName);
            Assert.AreEqual(blanketPurchaseOrderDto.InternalComments, blanketPurchaseOrderVM.InternalComments);
            Assert.AreEqual(blanketPurchaseOrderDto.VendorId, blanketPurchaseOrderVM.VendorId);
            Assert.AreEqual(blanketPurchaseOrderDto.VendorName, blanketPurchaseOrderVM.VendorName);
            Assert.AreEqual(blanketPurchaseOrderDto.Amount.ToString("C"), blanketPurchaseOrderVM.Amount);
            Assert.AreEqual(blanketPurchaseOrderDto.MaintenanceDate.Value.ToShortDateString(), blanketPurchaseOrderVM.MaintenanceDate);
            Assert.AreEqual(blanketPurchaseOrderDto.CurrencyCode, blanketPurchaseOrderVM.CurrencyCode);
            Assert.AreEqual(blanketPurchaseOrderDto.ApType, blanketPurchaseOrderVM.ApType);

            Assert.IsTrue(blanketPurchaseOrderVM.Requisitions is List<RequisitionNumberViewModel>);
            Assert.AreEqual(blanketPurchaseOrderDto.Requisitions.Count(), blanketPurchaseOrderVM.Requisitions.Count());
            foreach (var reqId in blanketPurchaseOrderDto.Requisitions)
            {
                Assert.IsTrue(blanketPurchaseOrderVM.Requisitions.Any(x => x.Id == reqId));
            }

            Assert.IsTrue(blanketPurchaseOrderVM.Vouchers is List<string>);
            Assert.AreEqual(blanketPurchaseOrderDto.Vouchers.Count(), blanketPurchaseOrderVM.Vouchers.Count());

            Assert.IsTrue(blanketPurchaseOrderVM.LineItems is List<LineItemViewModel>);
            Assert.AreEqual(0, blanketPurchaseOrderVM.LineItems.Count(), "BPO's don't have 'Line Items' per se.");

            Assert.AreEqual(blanketPurchaseOrderDto.Approvers.Count(), blanketPurchaseOrderVM.Approvers.Count(), "The document approvers list sizes should be the same.");

            Assert.IsTrue(blanketPurchaseOrderVM.GlDistributions is List<BlanketPurchaseOrderGlDistributionViewModel>, "List should be initialized to the correct type.");
            Assert.AreEqual(blanketPurchaseOrderDto.GlDistributions.Count(), blanketPurchaseOrderVM.GlDistributions.Count(), "Lists should have the same number of distributions.");

            // Each line item should have sequential IDs starting at 1
            int id = 1;
            foreach (var glDistribuiton in blanketPurchaseOrderVM.GlDistributions)
            {
                Assert.AreEqual("document-line-item" + id.ToString(), glDistribuiton.DocumentItemId, "The document item ID should match.");
                id++;
            }
        }

        [TestMethod]
        public void Constructor_NullableObjectsAreNull()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.Description = null;
            blanketPurchaseOrderDto.CurrencyCode = null;
            blanketPurchaseOrderDto.ExpirationDate = null;
            blanketPurchaseOrderDto.Requisitions = null;
            blanketPurchaseOrderDto.Vouchers = null;
            blanketPurchaseOrderDto.MaintenanceDate = null;
            //blanketPurchaseOrderDto.LineItems = null;
            blanketPurchaseOrderDto.VendorName = null;
            blanketPurchaseOrderDto.GlDistributions = null;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            Assert.AreEqual(blanketPurchaseOrderDto.Status, blanketPurchaseOrderVM.Status);
            Assert.AreEqual(blanketPurchaseOrderDto.StatusDate.ToShortDateString(), blanketPurchaseOrderVM.StatusDate);
            Assert.AreEqual(null, blanketPurchaseOrderVM.Description);
            Assert.AreEqual("", blanketPurchaseOrderVM.ExpirationDate);
            Assert.AreEqual(blanketPurchaseOrderDto.InitiatorName, blanketPurchaseOrderVM.InitiatorName);
            Assert.AreEqual(blanketPurchaseOrderDto.InternalComments, blanketPurchaseOrderVM.InternalComments);

            Assert.AreEqual(blanketPurchaseOrderDto.VendorId, blanketPurchaseOrderVM.VendorId);
            Assert.AreEqual("", blanketPurchaseOrderVM.VendorName);
            Assert.AreEqual(blanketPurchaseOrderDto.Amount.ToString("C"), blanketPurchaseOrderVM.Amount);
            Assert.AreEqual("", blanketPurchaseOrderVM.MaintenanceDate);
            Assert.AreEqual(null, blanketPurchaseOrderVM.CurrencyCode);
            Assert.AreEqual(blanketPurchaseOrderDto.Date.ToShortDateString(), blanketPurchaseOrderVM.Date);
            Assert.AreEqual(blanketPurchaseOrderDto.ApType, blanketPurchaseOrderVM.ApType);

            Assert.IsTrue(blanketPurchaseOrderVM.Requisitions is List<RequisitionNumberViewModel>);
            Assert.IsTrue(blanketPurchaseOrderVM.Requisitions.Count() == 0);

            Assert.IsTrue(blanketPurchaseOrderVM.Vouchers is List<string>);
            Assert.IsTrue(blanketPurchaseOrderVM.Vouchers.Count() == 0);

            Assert.IsTrue(blanketPurchaseOrderVM.LineItems is List<LineItemViewModel>);
            Assert.IsTrue(blanketPurchaseOrderVM.LineItems.Count() == 0);

            Assert.IsTrue(blanketPurchaseOrderVM.GlDistributions is List<BlanketPurchaseOrderGlDistributionViewModel>, "List should be initialized to the correct type.");
            Assert.AreEqual(0, blanketPurchaseOrderVM.GlDistributions.Count(), "GL Distributions list should be empty.");
        }

        [TestMethod]
        public void Constructor_NullGlDistributionDtoObjectInList()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.Description = null;
            blanketPurchaseOrderDto.CurrencyCode = null;
            blanketPurchaseOrderDto.ExpirationDate = null;
            blanketPurchaseOrderDto.Requisitions = null;
            blanketPurchaseOrderDto.Vouchers = null;
            blanketPurchaseOrderDto.MaintenanceDate = null;
            //blanketPurchaseOrderDto.LineItems = null;
            blanketPurchaseOrderDto.VendorName = null;
            blanketPurchaseOrderDto.GlDistributions[1] = null;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            Assert.IsTrue(blanketPurchaseOrderVM.GlDistributions is List<BlanketPurchaseOrderGlDistributionViewModel>, "List should be initialized to the correct type.");
            Assert.AreEqual(blanketPurchaseOrderDto.GlDistributions.Count()-1, blanketPurchaseOrderVM.GlDistributions.Count(), "GL Distributions list should be empty.");
        }

        [TestMethod]
        public void Constructor_ShortVendorName()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.VendorName = "Susty Corporation";
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            Assert.AreEqual(blanketPurchaseOrderDto.VendorName, blanketPurchaseOrderVM.VendorName);
        }

        [TestMethod]
        public void Constructor_LongVendorName()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.VendorName = "Susty Corporation International LLC";
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            Assert.AreEqual(blanketPurchaseOrderDto.VendorName, blanketPurchaseOrderVM.VendorName);
        }

        [TestMethod]
        public void Constructor_NullInitiatorName()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.VendorName = "Susty Corporation International LLC";
            blanketPurchaseOrderDto.InitiatorName = null;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            Assert.AreEqual(String.Empty, blanketPurchaseOrderVM.InitiatorName);
        }

        [TestMethod]
        public void Constructor_LongInitiatorName()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.VendorName = "Susty Corporation International LLC";
            blanketPurchaseOrderDto.InitiatorName = "Susty Corporation International LLC";
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            Assert.AreEqual(blanketPurchaseOrderDto.InitiatorName, blanketPurchaseOrderVM.InitiatorName);
        }
        #endregion

        #region StatusDescription tests
        [TestMethod]
        public void StatusDescription_Closed()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.Status = BlanketPurchaseOrderStatus.Closed;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Confirm that the blanket purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusClosed"), blanketPurchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_InProgress()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.Status = BlanketPurchaseOrderStatus.InProgress;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Confirm that the blanket purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusInProgress"), blanketPurchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_NotApproved()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.Status = BlanketPurchaseOrderStatus.NotApproved;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Confirm that the blanket purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusNotApproved"), blanketPurchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Outstanding()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.Status = BlanketPurchaseOrderStatus.Outstanding;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Confirm that the blanket purchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusOutstanding"), blanketPurchaseOrderVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Voided()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderDto.Status = BlanketPurchaseOrderStatus.Voided;
            blanketPurchaseOrderVM = new BlanketPurchaseOrderViewModel(blanketPurchaseOrderDto);

            // Confirm that the blanketpurchase order status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "BlanketPurchaseOrderStatusVoided"), blanketPurchaseOrderVM.StatusDescription);
        }
        #endregion
    }
}
