﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the BlanketPurchaseOrderNumberViewModel
    /// </summary>
    [TestClass]
    public class BlanketPurchaseOrderNumberViewModelTests
    {
        #region Initialize and Cleanup
        public BlanketPurchaseOrder blanketPurchaseOrderDto;
        public BlanketPurchaseOrderNumberViewModel blanketPurchaseOrderNumberVM;
        public TestDocumentDtoRepository documentRepository;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            blanketPurchaseOrderDto = null;
            blanketPurchaseOrderNumberVM = null;
            documentRepository = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void RequisitionnumberViewModel_VerifyBaseFinanceDocument()
        {
            blanketPurchaseOrderDto = documentRepository.GetBlanketPurchaseOrder();
            blanketPurchaseOrderNumberVM = new BlanketPurchaseOrderNumberViewModel(blanketPurchaseOrderDto.Id);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(blanketPurchaseOrderDto.Id, blanketPurchaseOrderNumberVM.Id, "The document ID should be initialized.");

        }
        #endregion

    }
}
