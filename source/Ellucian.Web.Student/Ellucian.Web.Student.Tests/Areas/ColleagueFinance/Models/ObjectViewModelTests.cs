﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class ObjectViewModelTests
    {
        [TestMethod]
        public void ConstructorOverBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = 100.00m,
                TotalBudget = 2.00m,
                TotalEncumbrances = 50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = 50,
                            Encumbrances = 25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            fiscalYears.Add("2016");

            var todaysFiscalYear = "2016";

            var majorComponentDtos = new List<GeneralLedgerComponent>();
            majorComponentDtos.Add(new GeneralLedgerComponent() 
            {
                ComponentName = "Fund",
                ComponentLength = 2,
                StartPosition = 2
            });

            var savedFilterNames = new List<KeyValuePair<string, string>>();
            savedFilterNames.Add(new KeyValuePair<string, string>("key", "value"));

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalBudget);
            Assert.AreEqual(_TotalBudgetExpense.ToString("C"), vm.ObjectData.TotalBudgetExpense);

            var _TotalActualsExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalActuals);
            Assert.AreEqual(_TotalActualsExpense.ToString("C"), vm.ObjectData.TotalActualsExpense);

            var _TotalEncumbrancesExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalEncumbrances);
            Assert.AreEqual(_TotalEncumbrancesExpense.ToString("C"), vm.ObjectData.TotalEncumbrancesExpense);

            var expectedBudgetRemaining = (_TotalBudgetExpense -
                (_TotalActualsExpense + _TotalEncumbrancesExpense)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingExpense);

            var _TotalBudgetRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalBudget) * -1;
            var _TotalActualsRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalActuals) * -1;
            var totalNetBudget = _TotalBudgetRevenue - _TotalBudgetExpense;
            var totalNetActuals = _TotalActualsRevenue - _TotalActualsExpense;

            Assert.AreEqual(totalNetBudget.ToString("C"), vm.ObjectData.TotalBudgetNet);
            Assert.AreEqual(totalNetActuals.ToString("C"), vm.ObjectData.TotalActualsNet);

            var percent = 0m;

            if (_TotalBudgetExpense == 0)
            {
                if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsExpense + _TotalEncumbrancesExpense) /
                    _TotalBudgetExpense) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedExpense);

            var indicatorStyle = "";
            var indicatorText = "";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                indicatorStyle = "group-icon-red";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
            }
            else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > _TotalBudgetExpense * 0.85m)
            {
                indicatorStyle = "group-icon-yellow";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
            }
            else
            {
                indicatorStyle = "group-icon-green";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
            }
            Assert.AreEqual(indicatorStyle, vm.ObjectData.TotalFinancialHealthIndicatorExpense);
            Assert.AreEqual(indicatorText, vm.ObjectData.TotalFinancialHealthIndicatorTextExpense);

            var colorStyle = "remaining-black";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                colorStyle = "remaining-red";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleExpense);
        }

        [TestMethod]
        public void ConstructorOverBudgetRevenue()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Revenue,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = -100.00m,
                TotalBudget = -2.00m,
                TotalEncumbrances = -50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = -100,
                    Actuals = -50,
                    Encumbrances = -25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = -50,
                            Encumbrances = -25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            fiscalYears.Add("2016");

            var todaysFiscalYear = "2016";

            var majorComponentDtos = new List<GeneralLedgerComponent>();
            majorComponentDtos.Add(new GeneralLedgerComponent()
            {
                ComponentName = "Fund",
                ComponentLength = 2,
                StartPosition = 2
            });

            var savedFilterNames = new List<KeyValuePair<string, string>>();
            savedFilterNames.Add(new KeyValuePair<string, string>("key", "value"));

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalBudget) * -1;
            Assert.AreEqual(_TotalBudgetRevenue.ToString("C"), vm.ObjectData.TotalBudgetRevenue);

            var _TotalActualsRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalActuals) * -1;
            Assert.AreEqual(_TotalActualsRevenue.ToString("C"), vm.ObjectData.TotalActualsRevenue);

            var _TotalEncumbrancesRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalEncumbrances) * -1;
            Assert.AreEqual(_TotalEncumbrancesRevenue.ToString("C"), vm.ObjectData.TotalEncumbrancesRevenue);

            var expectedBudgetRemaining = (_TotalBudgetRevenue -
                (_TotalActualsRevenue + _TotalEncumbrancesRevenue)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingRevenue);

            var _TotalBudgetExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalBudget);
            var _TotalActualsExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalActuals);
            var totalNetBudget = _TotalBudgetRevenue - _TotalBudgetExpense;
            var totalNetActuals = _TotalActualsRevenue - _TotalActualsExpense;

            Assert.AreEqual(totalNetBudget.ToString("C"), vm.ObjectData.TotalBudgetNet);
            Assert.AreEqual(totalNetActuals.ToString("C"), vm.ObjectData.TotalActualsNet);

            var percent = 0m;

            if (_TotalBudgetRevenue == 0)
            {
                if ((_TotalActualsRevenue + _TotalEncumbrancesRevenue) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsRevenue + _TotalEncumbrancesRevenue) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsRevenue + _TotalEncumbrancesRevenue) /
                    _TotalBudgetRevenue) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedRevenue);

            var colorStyle = "remaining-black";
            if (_TotalActualsRevenue + _TotalEncumbrancesRevenue > _TotalBudgetRevenue)
            {
                colorStyle = "remaining-green";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleRevenue);
        }

        [TestMethod]
        public void ConstructorOverBudgetAsset()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Asset,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = 100.00m,
                TotalBudget = 2.00m,
                TotalEncumbrances = 50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = 50,
                            Encumbrances = 25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            fiscalYears.Add("2016");

            var todaysFiscalYear = "2016";

            var majorComponentDtos = new List<GeneralLedgerComponent>();
            majorComponentDtos.Add(new GeneralLedgerComponent()
            {
                ComponentName = "Fund",
                ComponentLength = 2,
                StartPosition = 2
            });

            var savedFilterNames = new List<KeyValuePair<string, string>>();
            savedFilterNames.Add(new KeyValuePair<string, string>("key", "value"));

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalActualsAsset = objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).Sum(f => f.TotalActuals);
            Assert.AreEqual(_TotalActualsAsset.ToString("C"), vm.ObjectData.TotalActualsAsset);

            var _TotalEncumbrancesAsset = objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).Sum(f => f.TotalEncumbrances);
            Assert.AreEqual(_TotalEncumbrancesAsset.ToString("C"), vm.ObjectData.TotalEncumbrancesAsset);
        }

        [TestMethod]
        public void ConstructorOverBudgetLiability()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Liability,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = -100.00m,
                TotalBudget = -2.00m,
                TotalEncumbrances = -50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = -100,
                    Actuals = -50,
                    Encumbrances = -25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = -50,
                            Encumbrances = -25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            fiscalYears.Add("2016");

            var todaysFiscalYear = "2016";

            var majorComponentDtos = new List<GeneralLedgerComponent>();
            majorComponentDtos.Add(new GeneralLedgerComponent()
            {
                ComponentName = "Fund",
                ComponentLength = 2,
                StartPosition = 2
            });

            var savedFilterNames = new List<KeyValuePair<string, string>>();
            savedFilterNames.Add(new KeyValuePair<string, string>("key", "value"));

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalActualsLiability = objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).Sum(f => f.TotalActuals) * -1;
            Assert.AreEqual(_TotalActualsLiability.ToString("C"), vm.ObjectData.TotalActualsLiability);

            var _TotalEncumbrancesLiability = objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).Sum(f => f.TotalEncumbrances) * -1;
            Assert.AreEqual(_TotalEncumbrancesLiability.ToString("C"), vm.ObjectData.TotalEncumbrancesLiability);
        }

        [TestMethod]
        public void ConstructorOverBudgetFundBalance()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.FundBalance,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = -100.00m,
                TotalBudget = -2.00m,
                TotalEncumbrances = -50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = -100,
                    Actuals = -50,
                    Encumbrances = -25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = -50,
                            Encumbrances = -25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            fiscalYears.Add("2016");

            var todaysFiscalYear = "2016";

            var majorComponentDtos = new List<GeneralLedgerComponent>();
            majorComponentDtos.Add(new GeneralLedgerComponent()
            {
                ComponentName = "Fund",
                ComponentLength = 2,
                StartPosition = 2
            });

            var savedFilterNames = new List<KeyValuePair<string, string>>();
            savedFilterNames.Add(new KeyValuePair<string, string>("key", "value"));

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalActualsFundBalance = objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).Sum(f => f.TotalActuals) * -1;
            Assert.AreEqual(_TotalActualsFundBalance.ToString("C"), vm.ObjectData.TotalActualsFundBalance);

            var _TotalEncumbrancesFundBalance = objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).Sum(f => f.TotalEncumbrances) * -1;
            Assert.AreEqual(_TotalEncumbrancesFundBalance.ToString("C"), vm.ObjectData.TotalEncumbrancesFundBalance);
        }

        [TestMethod]
        public void ConstructorNearingBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = 100.00m,
                TotalBudget = 200.00m,
                TotalEncumbrances = 75.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = 50,
                            Encumbrances = 25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            var todaysFiscalYear = "";
            var majorComponentDtos = new List<GeneralLedgerComponent>();
            var savedFilterNames = new List<KeyValuePair<string, string>>();

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalBudget);
            Assert.AreEqual(_TotalBudgetExpense.ToString("C"), vm.ObjectData.TotalBudgetExpense);

            var _TotalActualsExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalActuals);
            Assert.AreEqual(_TotalActualsExpense.ToString("C"), vm.ObjectData.TotalActualsExpense);

            var _TotalEncumbrancesExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalEncumbrances);
            Assert.AreEqual(_TotalEncumbrancesExpense.ToString("C"), vm.ObjectData.TotalEncumbrancesExpense);

            var expectedBudgetRemaining = (_TotalBudgetExpense -
                (_TotalActualsExpense + _TotalEncumbrancesExpense)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingExpense);

            var percent = 0m;

            if (_TotalBudgetExpense == 0)
            {
                if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsExpense + _TotalEncumbrancesExpense) /
                    _TotalBudgetExpense) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedExpense);

            var indicatorStyle = "";
            var indicatorText = "";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                indicatorStyle = "group-icon-red";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
            }
            else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > _TotalBudgetExpense * 0.85m)
            {
                indicatorStyle = "group-icon-yellow";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
            }
            else
            {
                indicatorStyle = "group-icon-green";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
            }
            Assert.AreEqual(indicatorStyle, vm.ObjectData.TotalFinancialHealthIndicatorExpense);
            Assert.AreEqual(indicatorText, vm.ObjectData.TotalFinancialHealthIndicatorTextExpense);

            var colorStyle = "remaining-black";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                colorStyle = "remaining-red";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleExpense);
        }

        [TestMethod]
        public void ConstructorUnderBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = 100.00m,
                TotalBudget = 200.00m,
                TotalEncumbrances = 50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = 50,
                            Encumbrances = 25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            var todaysFiscalYear = "";
            var majorComponentDtos = new List<GeneralLedgerComponent>();
            var savedFilterNames = new List<KeyValuePair<string, string>>();

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalBudget);
            Assert.AreEqual(_TotalBudgetExpense.ToString("C"), vm.ObjectData.TotalBudgetExpense);

            var _TotalActualsExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalActuals);
            Assert.AreEqual(_TotalActualsExpense.ToString("C"), vm.ObjectData.TotalActualsExpense);

            var _TotalEncumbrancesExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalEncumbrances);
            Assert.AreEqual(_TotalEncumbrancesExpense.ToString("C"), vm.ObjectData.TotalEncumbrancesExpense);

            var expectedBudgetRemaining = (_TotalBudgetExpense -
                (_TotalActualsExpense + _TotalEncumbrancesExpense)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingExpense);

            var percent = 0m;

            if (_TotalBudgetExpense == 0)
            {
                if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsExpense + _TotalEncumbrancesExpense) /
                    _TotalBudgetExpense) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedExpense);

            var indicatorStyle = "";
            var indicatorText = "";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                indicatorStyle = "group-icon-red";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
            }
            else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > _TotalBudgetExpense * 0.85m)
            {
                indicatorStyle = "group-icon-yellow";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
            }
            else
            {
                indicatorStyle = "group-icon-green";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
            }
            Assert.AreEqual(indicatorStyle, vm.ObjectData.TotalFinancialHealthIndicatorExpense);
            Assert.AreEqual(indicatorText, vm.ObjectData.TotalFinancialHealthIndicatorTextExpense);

            var colorStyle = "remaining-black";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                colorStyle = "remaining-red";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleExpense);
        }

        [TestMethod]
        public void ConstructorPositiveExpensesWithNoBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = 100.00m,
                TotalBudget = 0.00m,
                TotalEncumbrances = 50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = 50,
                            Encumbrances = 25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            var todaysFiscalYear = "";
            var majorComponentDtos = new List<GeneralLedgerComponent>();
            var savedFilterNames = new List<KeyValuePair<string, string>>();

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalBudget);
            Assert.AreEqual(_TotalBudgetExpense.ToString("C"), vm.ObjectData.TotalBudgetExpense);

            var _TotalActualsExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalActuals);
            Assert.AreEqual(_TotalActualsExpense.ToString("C"), vm.ObjectData.TotalActualsExpense);

            var _TotalEncumbrancesExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalEncumbrances);
            Assert.AreEqual(_TotalEncumbrancesExpense.ToString("C"), vm.ObjectData.TotalEncumbrancesExpense);

            var expectedBudgetRemaining = (_TotalBudgetExpense -
                (_TotalActualsExpense + _TotalEncumbrancesExpense)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingExpense);

            var percent = 0m;

            if (_TotalBudgetExpense == 0)
            {
                if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsExpense + _TotalEncumbrancesExpense) /
                    _TotalBudgetExpense) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedExpense);

            var indicatorStyle = "";
            var indicatorText = "";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                indicatorStyle = "group-icon-red";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
            }
            else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > _TotalBudgetExpense * 0.85m)
            {
                indicatorStyle = "group-icon-yellow";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
            }
            else
            {
                indicatorStyle = "group-icon-green";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
            }
            Assert.AreEqual(indicatorStyle, vm.ObjectData.TotalFinancialHealthIndicatorExpense);
            Assert.AreEqual(indicatorText, vm.ObjectData.TotalFinancialHealthIndicatorTextExpense);

            var colorStyle = "remaining-black";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                colorStyle = "remaining-red";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleExpense);
        }

        [TestMethod]
        public void ConstructorPositiveExpensesWithNoBudgetRevenue()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Revenue,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = -100.00m,
                TotalBudget = 0.00m,
                TotalEncumbrances = -50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = -100,
                    Actuals = -50,
                    Encumbrances = -25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = -50,
                            Encumbrances = -25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            fiscalYears.Add("2016");

            var todaysFiscalYear = "2016";

            var majorComponentDtos = new List<GeneralLedgerComponent>();
            majorComponentDtos.Add(new GeneralLedgerComponent()
            {
                ComponentName = "Fund",
                ComponentLength = 2,
                StartPosition = 2
            });

            var savedFilterNames = new List<KeyValuePair<string, string>>();
            savedFilterNames.Add(new KeyValuePair<string, string>("key", "value"));

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalBudget) * -1;
            Assert.AreEqual(_TotalBudgetRevenue.ToString("C"), vm.ObjectData.TotalBudgetRevenue);

            var _TotalActualsRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalActuals) * -1;
            Assert.AreEqual(_TotalActualsRevenue.ToString("C"), vm.ObjectData.TotalActualsRevenue);

            var _TotalEncumbrancesRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalEncumbrances) * -1;
            Assert.AreEqual(_TotalEncumbrancesRevenue.ToString("C"), vm.ObjectData.TotalEncumbrancesRevenue);

            var expectedBudgetRemaining = (_TotalBudgetRevenue -
                (_TotalActualsRevenue + _TotalEncumbrancesRevenue)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingRevenue);

            var percent = 0m;

            if (_TotalBudgetRevenue == 0)
            {
                if ((_TotalActualsRevenue + _TotalEncumbrancesRevenue) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsRevenue + _TotalEncumbrancesRevenue) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsRevenue + _TotalEncumbrancesRevenue) /
                    _TotalBudgetRevenue) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedRevenue);

            var colorStyle = "remaining-black";
            if (_TotalActualsRevenue + _TotalEncumbrancesRevenue > _TotalBudgetRevenue)
            {
                colorStyle = "remaining-green";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleRevenue);
        }

        [TestMethod]
        public void ConstructorNegativeExpensesWithNoBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = -100.00m,
                TotalBudget = 0.00m,
                TotalEncumbrances = -50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = 50,
                            Encumbrances = 25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            var todaysFiscalYear = "";
            var majorComponentDtos = new List<GeneralLedgerComponent>();
            var savedFilterNames = new List<KeyValuePair<string, string>>();

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalBudget);
            Assert.AreEqual(_TotalBudgetExpense.ToString("C"), vm.ObjectData.TotalBudgetExpense);

            var _TotalActualsExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalActuals);
            Assert.AreEqual(_TotalActualsExpense.ToString("C"), vm.ObjectData.TotalActualsExpense);

            var _TotalEncumbrancesExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalEncumbrances);
            Assert.AreEqual(_TotalEncumbrancesExpense.ToString("C"), vm.ObjectData.TotalEncumbrancesExpense);

            var expectedBudgetRemaining = (_TotalBudgetExpense -
                (_TotalActualsExpense + _TotalEncumbrancesExpense)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingExpense);

            var percent = 0m;

            if (_TotalBudgetExpense == 0)
            {
                if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsExpense + _TotalEncumbrancesExpense) /
                    _TotalBudgetExpense) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedExpense);

            var indicatorStyle = "";
            var indicatorText = "";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                indicatorStyle = "group-icon-red";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
            }
            else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > _TotalBudgetExpense * 0.85m)
            {
                indicatorStyle = "group-icon-yellow";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
            }
            else
            {
                indicatorStyle = "group-icon-green";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
            }
            Assert.AreEqual(indicatorStyle, vm.ObjectData.TotalFinancialHealthIndicatorExpense);
            Assert.AreEqual(indicatorText, vm.ObjectData.TotalFinancialHealthIndicatorTextExpense);

            var colorStyle = "remaining-black";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                colorStyle = "remaining-red";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleExpense);
        }

        [TestMethod]
        public void ConstructorNegativeExpensesWithNoBudgetRevenue()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Revenue,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = 100.00m,
                TotalBudget = 0.00m,
                TotalEncumbrances = 50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = -100,
                Actuals = -50,
                Encumbrances = -25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = -100,
                    Actuals = -50,
                    Encumbrances = -25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = -50,
                            Encumbrances = -25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            fiscalYears.Add("2016");

            var todaysFiscalYear = "2016";

            var majorComponentDtos = new List<GeneralLedgerComponent>();
            majorComponentDtos.Add(new GeneralLedgerComponent()
            {
                ComponentName = "Fund",
                ComponentLength = 2,
                StartPosition = 2
            });

            var savedFilterNames = new List<KeyValuePair<string, string>>();
            savedFilterNames.Add(new KeyValuePair<string, string>("key", "value"));

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, todaysFiscalYear, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(todaysFiscalYear, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalBudget) * -1;
            Assert.AreEqual(_TotalBudgetRevenue.ToString("C"), vm.ObjectData.TotalBudgetRevenue);

            var _TotalActualsRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalActuals) * -1;
            Assert.AreEqual(_TotalActualsRevenue.ToString("C"), vm.ObjectData.TotalActualsRevenue);

            var _TotalEncumbrancesRevenue = objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).Sum(f => f.TotalEncumbrances) * -1;
            Assert.AreEqual(_TotalEncumbrancesRevenue.ToString("C"), vm.ObjectData.TotalEncumbrancesRevenue);

            var expectedBudgetRemaining = (_TotalBudgetRevenue -
                (_TotalActualsRevenue + _TotalEncumbrancesRevenue)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingRevenue);

            var percent = 0m;

            if (_TotalBudgetRevenue == 0)
            {
                if ((_TotalActualsRevenue + _TotalEncumbrancesRevenue) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsRevenue + _TotalEncumbrancesRevenue) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsRevenue + _TotalEncumbrancesRevenue) /
                    _TotalBudgetRevenue) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedRevenue);

            var colorStyle = "remaining-black";
            if (_TotalActualsRevenue + _TotalEncumbrancesRevenue > _TotalBudgetRevenue)
            {
                colorStyle = "remaining-green";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleRevenue);
        }

        [TestMethod]
        public void ConstructorNullTodaysFiscalYear()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "obj",
                Name = "Test Object",
                Pools = new List<GlObjectCodeBudgetPool>(),
                TotalActuals = 100.00m,
                TotalBudget = 200.00m,
                TotalEncumbrances = 50.00m
            };
            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "1",
                GlAccountNumber = "1",
                Description = "Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });

            dto.GlAccounts.Add(new GlObjectCodeGlAccount()
            {
                PoolType = GlBudgetPoolType.None,
                FormattedGlAccount = "2",
                GlAccountNumber = "2",
                Description = "Second Description",
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25
            });
            dto.Pools.Add(new GlObjectCodeBudgetPool()
            {
                IsUmbrellaVisible = true,
                Umbrella = new GlObjectCodeGlAccount()
                {
                    PoolType = GlBudgetPoolType.Umbrella,
                    FormattedGlAccount = "3",
                    GlAccountNumber = "3",
                    Description = "Third Description",
                    Budget = 100,
                    Actuals = 50,
                    Encumbrances = 25
                },
                Poolees = new List<GlObjectCodeGlAccount>()
                    {
                        new GlObjectCodeGlAccount() 
                        {
                            PoolType = GlBudgetPoolType.Poolee,
                            FormattedGlAccount = "4",
                            GlAccountNumber = "4",
                            Description = "Fourth Description",
                            Budget = 0,
                            Actuals = 50,
                            Encumbrances = 25
                        }
                    }
            });
            var objectCodeDtos = new List<GlObjectCode>();
            objectCodeDtos.Add(dto);
            var fiscalYears = new List<string>();
            
            var majorComponentDtos = new List<GeneralLedgerComponent>();
            var savedFilterNames = new List<KeyValuePair<string, string>>();

            var vm = new ObjectViewModel(objectCodeDtos, fiscalYears, null, majorComponentDtos, savedFilterNames);

            Assert.AreEqual(string.Empty, vm.TodaysFiscalYear);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).ToList().Count, vm.ObjectData.ExpenseObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Revenue).ToList().Count, vm.ObjectData.RevenueObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Asset).ToList().Count, vm.ObjectData.AssetObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.Liability).ToList().Count, vm.ObjectData.LiabilityObjectCodes.Count);
            Assert.AreEqual(objectCodeDtos.Where(d => d.GlClass == GlClass.FundBalance).ToList().Count, vm.ObjectData.FundBalanceObjectCodes.Count);

            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Expense), vm.ObjectData.HasAnyExpense);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Revenue), vm.ObjectData.HasAnyRevenue);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Asset), vm.ObjectData.HasAnyAsset);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.Liability), vm.ObjectData.HasAnyLiability);
            Assert.AreEqual(objectCodeDtos.Any(d => d.GlClass == GlClass.FundBalance), vm.ObjectData.HasAnyFundBalance);

            var _TotalBudgetExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalBudget);
            Assert.AreEqual(_TotalBudgetExpense.ToString("C"), vm.ObjectData.TotalBudgetExpense);

            var _TotalActualsExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalActuals);
            Assert.AreEqual(_TotalActualsExpense.ToString("C"), vm.ObjectData.TotalActualsExpense);

            var _TotalEncumbrancesExpense = objectCodeDtos.Where(d => d.GlClass == GlClass.Expense).Sum(f => f.TotalEncumbrances);
            Assert.AreEqual(_TotalEncumbrancesExpense.ToString("C"), vm.ObjectData.TotalEncumbrancesExpense);

            var expectedBudgetRemaining = (_TotalBudgetExpense -
                (_TotalActualsExpense + _TotalEncumbrancesExpense)).ToString("C");
            Assert.AreEqual(expectedBudgetRemaining, vm.ObjectData.TotalBudgetRemainingExpense);

            var percent = 0m;

            if (_TotalBudgetExpense == 0)
            {
                if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > 0)
                {
                    percent = 101;
                }
                else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) < 0)
                {
                    percent = -101;
                }
            }
            else
            {
                percent = Math.Round((((_TotalActualsExpense + _TotalEncumbrancesExpense) /
                    _TotalBudgetExpense) * 100m));
            }

            var expectedPercentProcessed = string.Format("{0:n0}", percent) + " %";
            Assert.AreEqual(expectedPercentProcessed, vm.ObjectData.TotalPercentProcessedExpense);

            var indicatorStyle = "";
            var indicatorText = "";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                indicatorStyle = "group-icon-red";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");
            }
            else if ((_TotalActualsExpense + _TotalEncumbrancesExpense) > _TotalBudgetExpense * 0.85m)
            {
                indicatorStyle = "group-icon-yellow";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");
            }
            else
            {
                indicatorStyle = "group-icon-green";
                indicatorText = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");
            }
            Assert.AreEqual(indicatorStyle, vm.ObjectData.TotalFinancialHealthIndicatorExpense);
            Assert.AreEqual(indicatorText, vm.ObjectData.TotalFinancialHealthIndicatorTextExpense);

            var colorStyle = "remaining-black";
            if (_TotalActualsExpense + _TotalEncumbrancesExpense > _TotalBudgetExpense)
            {
                colorStyle = "remaining-red";
            }
            Assert.AreEqual(colorStyle, vm.ObjectData.ColorCodeStyleExpense);
        }
    }
}
