﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    public class TestCostCenterDtoRepository
    {
        private List<CostCenter> CostCenterDtos;

        public TestCostCenterDtoRepository()
        {
            BuildCostCenterDto();
        }

        public List<CostCenter> GetCostCenters()
        {
            return CostCenterDtos;
        }

        public CostCenter GetCostCenter()
        {
            return CostCenterDtos.First();
        }

        public CostCenterSubtotal GetCostCenterSubtotal()
        {
            return CostCenterDtos.First().CostCenterSubtotals.First();
        }

        public CostCenterGlAccount GetCostCenterGlAccount()
        {
            return CostCenterDtos.First().CostCenterSubtotals.First().GlAccounts.First();
        }

        public List<GlBudgetPool> GetBudgetPools()
        {
            return CostCenterDtos.First().CostCenterSubtotals.First().Pools;
        }

        public GlBudgetPool GetBudgetPool()
        {
            return CostCenterDtos.First().CostCenterSubtotals.First().Pools.First();
        }


        private void BuildCostCenterDto()
        {
            CostCenterDtos = new List<CostCenter>()
            {
                new CostCenter()
                {
                Id = "10110033333",
                UnitId = "01",
                Name = "Cost Center Name",
                TotalBudget = 10000000m,
                TotalActuals = 1000m,
                TotalEncumbrances = 500m,
                TotalBudgetRevenue = 100000m,
                TotalActualsRevenue = 10000m,
                TotalEncumbrancesRevenue = 500,
                    CostCenterSubtotals = new List<CostCenterSubtotal>()
                    {
                        new CostCenterSubtotal()
                        {
                            Id = "23984",
                            Name = "Subtotal 1",
                            TotalBudget = 10000m,
                            TotalActuals = 100m,
                            TotalEncumbrances = 50m,
                            GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                            GlAccounts = new List<CostCenterGlAccount>()
                            {
                                new CostCenterGlAccount()
                                {
                                    GlAccountNumber = "10_11",
                                    FormattedGlAccount = "10-11",
                                    Description = "GL Description 1",
                                    Budget = 100m,
                                    Encumbrances = 50m,
                                    Actuals = 25m,
                                    PoolType = GlBudgetPoolType.None
                                },
                                new CostCenterGlAccount()
                                {
                                    GlAccountNumber = "10_12",
                                    FormattedGlAccount = "10-12",
                                    Description = "GL Description 1",
                                    Budget = 100m,
                                    Encumbrances = 50m,
                                    Actuals = 25m,
                                    PoolType = GlBudgetPoolType.None
                                },
                                new CostCenterGlAccount()
                                {
                                    GlAccountNumber = "10_13",
                                    FormattedGlAccount = "10-13",
                                    Description = "GL Description 1",
                                    Budget = 100m,
                                    Encumbrances = 50m,
                                    Actuals = 25m,
                                    PoolType = GlBudgetPoolType.None
                                },
                            },
                            Pools = new List<GlBudgetPool>()
                            {
                                new GlBudgetPool()
                                {
                                    Umbrella = new CostCenterGlAccount()
                                    {
                                        GlAccountNumber = "10_14",
                                        FormattedGlAccount = "10-14",
                                        Description = "GL Description 1",
                                        Budget = 100m,
                                        Encumbrances = 50m,
                                        Actuals = 25m
                                    },
                                    IsUmbrellaVisible = true,
                                    Poolees = new List<CostCenterGlAccount>()
                                    {
                                        new CostCenterGlAccount()
                                        {
                                            GlAccountNumber = "10_14",
                                            FormattedGlAccount = "10-14",
                                            Description = "GL Description 1",
                                            Budget = 100m,
                                            Encumbrances = 50m,
                                            Actuals = 25m
                                        },
                                        new CostCenterGlAccount()
                                        {
                                            GlAccountNumber = "10_15",
                                            FormattedGlAccount = "10-15",
                                            Description = "GL Description 1",
                                            Budget = 100m,
                                            Encumbrances = 50m,
                                            Actuals = 25m
                                        },
                                        new CostCenterGlAccount()
                                        {
                                            GlAccountNumber = "10_16",
                                            FormattedGlAccount = "10-16",
                                            Description = "GL Description 1",
                                            Budget = 100m,
                                            Encumbrances = 50m,
                                            Actuals = 25m
                                        },
                                    },
                                },
                                new GlBudgetPool()
                                {
                                    Umbrella = new CostCenterGlAccount()
                                    {
                                        GlAccountNumber = "10_17",
                                        FormattedGlAccount = "10-17",
                                        Description = "GL Description 1",
                                        Budget = 100m,
                                        Encumbrances = 50m,
                                        Actuals = 25m
                                    },
                                    IsUmbrellaVisible = true,
                                    Poolees = new List<CostCenterGlAccount>()
                                    {
                                        new CostCenterGlAccount()
                                        {
                                            GlAccountNumber = "10_18",
                                            FormattedGlAccount = "10-18",
                                            Description = "GL Description 1",
                                            Budget = 100m,
                                            Encumbrances = 50m,
                                            Actuals = 25m
                                        },
                                        new CostCenterGlAccount()
                                        {
                                            GlAccountNumber = "10_19",
                                            FormattedGlAccount = "10-19",
                                            Description = "GL Description 1",
                                            Budget = 100m,
                                            Encumbrances = 50m,
                                            Actuals = 25m
                                        },
                                        new CostCenterGlAccount()
                                        {
                                            GlAccountNumber = "10_20",
                                            FormattedGlAccount = "10-20",
                                            Description = "GL Description 1",
                                            Budget = 100m,
                                            Encumbrances = 50m,
                                            Actuals = 25m
                                        },
                                    },
                                },
                            },
                        },
                        new CostCenterSubtotal()
                        {
                            Id = "3254353",
                            Name = "Subtotal 2",
                            TotalBudget = 10000m,
                            TotalActuals = 100m,
                            TotalEncumbrances = 50m,
                            GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                            GlAccounts = new List<CostCenterGlAccount>()
                            {
                                new CostCenterGlAccount()
                                {
                                    GlAccountNumber = "11_11",
                                    FormattedGlAccount = "11-11",
                                    Description = "GL Description 1",
                                    Budget = 100m,
                                    Encumbrances = 50m,
                                    Actuals = 25m,
                                    PoolType = GlBudgetPoolType.None
                                },
                                new CostCenterGlAccount()
                                {
                                    GlAccountNumber = "11_12",
                                    FormattedGlAccount = "11-12",
                                    Description = "GL Description 1",
                                    Budget = 100m,
                                    Encumbrances = 50m,
                                    Actuals = 25m,
                                    PoolType = GlBudgetPoolType.None
                                },
                                new CostCenterGlAccount()
                                {
                                    GlAccountNumber = "11_13",
                                    FormattedGlAccount = "11-13",
                                    Description = "GL Description 1",
                                    Budget = 100m,
                                    Encumbrances = 50m,
                                    Actuals = 25m,
                                    PoolType = GlBudgetPoolType.None
                                },
                            },
                        },
                    }
                },
                new CostCenter()
                {
                    Id = "10110044444",
                    UnitId = "02",
                    Name = "Cost Center Name",
                    TotalBudget = 10000000m,
                    TotalActuals = 1000m,
                    TotalEncumbrances = 500m,
                CostCenterSubtotals = new List<CostCenterSubtotal>()
                {
                    new CostCenterSubtotal()
                    {
                        Id = "23984",
                        Name = "Subtotal 1",
                        TotalBudget = 10000m,
                        TotalActuals = 100m,
                        TotalEncumbrances = 50m,
                        GlAccounts = new List<CostCenterGlAccount>()
                        {
                            new CostCenterGlAccount()
                            {
                                GlAccountNumber = "10_11",
                                FormattedGlAccount = "10-11",
                                Description = "GL Description 1",
                                Budget = 100m,
                                Encumbrances = 50m,
                                Actuals = 25m,
                                PoolType = GlBudgetPoolType.None
                            },
                            new CostCenterGlAccount()
                            {
                                GlAccountNumber = "10_12",
                                FormattedGlAccount = "10-12",
                                Description = "GL Description 1",
                                Budget = 100m,
                                Encumbrances = 50m,
                                Actuals = 25m,
                                PoolType = GlBudgetPoolType.None
                            },
                            new CostCenterGlAccount()
                            {
                                GlAccountNumber = "10_13",
                                FormattedGlAccount = "10-13",
                                Description = "GL Description 1",
                                Budget = 100m,
                                Encumbrances = 50m,
                                Actuals = 25m,
                                PoolType = GlBudgetPoolType.None
                            },
                        },
                    },
                    new CostCenterSubtotal()
                    {
                        Id = "3254353",
                        Name = "Subtotal 2",
                        TotalBudget = 10000m,
                        TotalActuals = 100m,
                        TotalEncumbrances = 50m,
                        GlAccounts = new List<CostCenterGlAccount>()
                        {
                            new CostCenterGlAccount()
                            {
                                GlAccountNumber = "11_11",
                                FormattedGlAccount = "11-11",
                                Description = "GL Description 1",
                                Budget = 100m,
                                Encumbrances = 50m,
                                Actuals = 25m,
                                PoolType = GlBudgetPoolType.None
                            },
                            new CostCenterGlAccount()
                            {
                                GlAccountNumber = "11_12",
                                FormattedGlAccount = "11-12",
                                Description = "GL Description 1",
                                Budget = 100m,
                                Encumbrances = 50m,
                                Actuals = 25m,
                                PoolType = GlBudgetPoolType.None
                            },
                            new CostCenterGlAccount()
                            {
                                GlAccountNumber = "11_13",
                                FormattedGlAccount = "11-13",
                                Description = "GL Description 1",
                                Budget = 100m,
                                Encumbrances = 50m,
                                Actuals = 25m,
                                PoolType = GlBudgetPoolType.None
                            },
                        },
                    },
                }
                }
            };
        }
    }
}