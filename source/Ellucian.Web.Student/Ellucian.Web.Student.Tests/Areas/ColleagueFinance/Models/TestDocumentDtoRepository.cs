﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    public class TestDocumentDtoRepository
    {
        private Voucher2 VoucherDto;
        private RecurringVoucher RecurringVoucherDto;
        private PurchaseOrder PurchaseOrderDto;
        private Requisition RequisitionDto;
        private BlanketPurchaseOrder BlanketPurchaseOrderDto;

        public TestDocumentDtoRepository()
        {
            BuildVoucherDto();
            BuildRecurringVoucherDto();
            BuildPurchaseOrderDto();
            BuildRequisitionDto();
            BuildBlanketPurchaseOrderDto();
        }

        #region Voucher methods
        public Voucher2 GetVoucher()
        {
            return VoucherDto;
        }

        private void BuildVoucherDto()
        {
            VoucherDto = new Voucher2();
            VoucherDto.VoucherId = "V0000001";
            VoucherDto.VendorId = "00012345";
            VoucherDto.VendorName = "Susty Corporation";
            VoucherDto.Amount = 545.11m;
            VoucherDto.Date = new DateTime(2015, 1, 7);
            VoucherDto.DueDate = new DateTime(2015, 1, 1);
            VoucherDto.MaintenanceDate = new DateTime(2015, 1, 6);
            VoucherDto.InvoiceNumber = "IN123123";
            VoucherDto.InvoiceDate = new DateTime(2015, 1, 5);
            VoucherDto.CheckNumber = "1234";
            VoucherDto.CheckDate = new DateTime(2015, 1, 4);
            VoucherDto.Comments = "Comments";
            VoucherDto.PurchaseOrderId = "P0000001";
            VoucherDto.BlanketPurchaseOrderId = "";
            VoucherDto.RecurringVoucherId = "";
            VoucherDto.CurrencyCode = "CC1";
            VoucherDto.Status = VoucherStatus.Outstanding;
            VoucherDto.ApType = "APT";

            #region Populate Approvers
            VoucherDto.Approvers = new List<Approver>();
            VoucherDto.Approvers.Add(new Approver()
                {
                    ApprovalName = "Gary Thorne",
                    ApprovalDate = new DateTime(2015, 1, 1)
                });
            VoucherDto.Approvers.Add(new Approver()
                {
                    ApprovalName = "Teresa Longerbeam",
                    ApprovalDate = new DateTime(2015, 1, 2)
                });
            VoucherDto.Approvers.Add(new Approver()
                {
                    ApprovalName = "Andy Kleehammer",
                    ApprovalDate = null
                });
            #endregion

            #region Populate Line Items
            var glDistributions = new List<LineItemGlDistribution>();
            glDistributions.Add(new LineItemGlDistribution()
                {
                    Amount = 123.45m,
                    FormattedGlAccount = "10-00-01-02-20601-53013",
                    GlAccount = "10_00_01_02_20601_53013",
                    ProjectLineItemCode = "PLIC",
                    ProjectNumber = "AJK-100",
                    Quantity = 53m
                });
            glDistributions.Add(new LineItemGlDistribution()
                {
                    Amount = 200.11m,
                    FormattedGlAccount = "10-00-01-02-20601-51000",
                    GlAccount = "10_00_01_02_20601_51000",
                    ProjectLineItemCode = "GTT1",
                    ProjectNumber = "GTT-897",
                    Quantity = 19m
                });

            var lineItemTaxes = new List<LineItemTax>();
            lineItemTaxes.Add(new LineItemTax()
                {
                    TaxCode = "VA",
                    TaxAmount = 105.33m
                });
            lineItemTaxes.Add(new LineItemTax()
                {
                    TaxCode = "MD",
                    TaxAmount = 57.62m
                });

            VoucherDto.LineItems = new List<LineItem>();
            VoucherDto.LineItems.Add(new LineItem()
                {
                    Comments = "Volcano insurance.",
                    Description = "Line item #1",
                    ExpectedDeliveryDate = new DateTime(2015, 1, 1),
                    ExtendedPrice = 512.45m,
                    InvoiceNumber = "IN12345",
                    Price = 100.99m,
                    Quantity = 12m,
                    TaxForm = "Tax Form",
                    TaxFormCode = "TFC1",
                    TaxFormLocation = "TFL1",
                    UnitOfIssue = "DOLLA",
                    VendorPart = "Vendor Part",
                    GlDistributions = glDistributions,
                    LineItemTaxes = lineItemTaxes
                });

            VoucherDto.LineItems.Add(new LineItem()
                {
                    Comments = "Volcano insurance.",
                    Description = "Line item #1",
                    ExpectedDeliveryDate = new DateTime(2015, 1, 1),
                    ExtendedPrice = 512.45m,
                    InvoiceNumber = "IN12345",
                    Price = 100.99m,
                    Quantity = 12m,
                    TaxForm = "Tax Form",
                    TaxFormCode = "TFC1",
                    TaxFormLocation = "TFL1",
                    UnitOfIssue = "DOLLA",
                    VendorPart = "Vendor Part",
                    GlDistributions = glDistributions,
                    LineItemTaxes = lineItemTaxes
                });
            #endregion
        }
        #endregion

        #region Recurring Voucher methods

        public RecurringVoucher GetRecurringVoucher()
        {
            return RecurringVoucherDto;
        }

        private void BuildRecurringVoucherDto()
        {
            RecurringVoucherDto = new RecurringVoucher();
            RecurringVoucherDto.RecurringVoucherId = "RV0000001";
            RecurringVoucherDto.VendorId = "00012345";
            RecurringVoucherDto.VendorName = "Susty Corporation";
            RecurringVoucherDto.Amount = 545.11m;
            RecurringVoucherDto.Date = new DateTime(2015, 1, 7);
            RecurringVoucherDto.MaintenanceDate = new DateTime(2015, 1, 6);
            RecurringVoucherDto.InvoiceNumber = "IN123123";
            RecurringVoucherDto.InvoiceDate = new DateTime(2015, 2, 7);
            RecurringVoucherDto.Comments = "Comments";
            RecurringVoucherDto.Status = RecurringVoucherStatus.Outstanding;
            RecurringVoucherDto.StatusDate = new DateTime(2015, 1, 7);
            RecurringVoucherDto.CurrencyCode = "CAD";
            RecurringVoucherDto.TotalScheduleAmountInLocalCurrency = 489.07m;
            RecurringVoucherDto.TotalScheduleTaxAmountInLocalCurrency = 203.19m;
            RecurringVoucherDto.ApType = "AP";

            #region Populate Approvers
            RecurringVoucherDto.Approvers = new List<Approver>();
            RecurringVoucherDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Gary Thorne",
                ApprovalDate = new DateTime(2015, 1, 7)
            });
            RecurringVoucherDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Teresa Longerbeam",
                ApprovalDate = new DateTime(2015, 1, 7)
            });
            RecurringVoucherDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Andy Kleehammer",
                ApprovalDate = null
            });
            #endregion

            #region Populate Schedules

            RecurringVoucherDto.Schedules = new List<RecurringVoucherSchedule>()
            {
                new RecurringVoucherSchedule()
                {
                    Date = new DateTime(2015, 2, 15),
                    Amount = 534.21m,
                    TaxAmount = 21.00m,
                    VoucherId = "V0000001",
                },
                new RecurringVoucherSchedule()
                {
                    Date = new DateTime(2015, 3, 15),
                    Amount = 534.21m,
                    TaxAmount = 21.00m,
                    VoucherId = "V0000002"
                },
                new RecurringVoucherSchedule()
                {
                    Date = new DateTime(2015, 3, 15),
                    Amount = 534.21m,
                    TaxAmount = 21.00m,
                    VoucherId = "V0000003"
                }
            };


            #endregion
        }
        #endregion

        #region Purchase Order methods
        public PurchaseOrder GetPurchaseOrder()
        {
            return PurchaseOrderDto;
        }

        public void BuildPurchaseOrderDto()
        {
            PurchaseOrderDto = new PurchaseOrder();
            PurchaseOrderDto.Id = "489";
            PurchaseOrderDto.Number = "P0000411";
            PurchaseOrderDto.Status = PurchaseOrderStatus.Outstanding;
            PurchaseOrderDto.StatusDate = new DateTime(2015, 1, 6);
            PurchaseOrderDto.Amount = 545.11m;
            PurchaseOrderDto.CurrencyCode = "CC1";
            PurchaseOrderDto.Date = new DateTime(2015, 1, 7);
            PurchaseOrderDto.DeliveryDate = new DateTime(2015, 1, 1);
            PurchaseOrderDto.MaintenanceDate = new DateTime(2015, 2, 6);
            PurchaseOrderDto.VendorId = "00012345";
            PurchaseOrderDto.VendorName = "Susty Corporation";
            PurchaseOrderDto.InitiatorName = "Andy Kleehammer";
            PurchaseOrderDto.RequestorName = "Gary Thorne";
            PurchaseOrderDto.ApType = "AP";
            PurchaseOrderDto.ShipToCodeName = "ST";
            PurchaseOrderDto.Comments = "Comments";
            PurchaseOrderDto.InternalComments = "Internal comments";

            #region Populate Approvers
            PurchaseOrderDto.Approvers = new List<Approver>();
            PurchaseOrderDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Gary Thorne",
                ApprovalDate = new DateTime(2015, 1, 1)
            });
            PurchaseOrderDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Teresa Longerbeam",
                ApprovalDate = new DateTime(2015, 1, 2)
            });
            PurchaseOrderDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Andy Kleehammer",
                ApprovalDate = null
            });
            #endregion

            #region Populate Line Items
            var glDistributions = new List<LineItemGlDistribution>();
            glDistributions.Add(new LineItemGlDistribution()
            {
                Amount = 123.45m,
                FormattedGlAccount = "10-00-01-02-20601-53013",
                GlAccount = "10_00_01_02_20601_53013",
                ProjectLineItemCode = "PLIC",
                ProjectNumber = "AJK-100",
                Quantity = 53m
            });
            glDistributions.Add(new LineItemGlDistribution()
            {
                Amount = 200.11m,
                FormattedGlAccount = "10-00-01-02-20601-51000",
                GlAccount = "10_00_01_02_20601_51000",
                ProjectLineItemCode = "GTT1",
                ProjectNumber = "GTT-897",
                Quantity = 19m
            });

            var lineItemTaxes = new List<LineItemTax>();
            lineItemTaxes.Add(new LineItemTax()
            {
                TaxCode = "VA",
                TaxAmount = 105.33m
            });
            lineItemTaxes.Add(new LineItemTax()
            {
                TaxCode = "MD",
                TaxAmount = 57.62m
            });

            PurchaseOrderDto.LineItems = new List<LineItem>();
            PurchaseOrderDto.LineItems.Add(new LineItem()
            {
                Comments = "Volcano insurance.",
                Description = "Line item #1",
                ExpectedDeliveryDate = new DateTime(2015, 1, 1),
                ExtendedPrice = 512.45m,
                InvoiceNumber = "IN12345",
                Price = 100.99m,
                Quantity = 12m,
                TaxForm = "Tax Form",
                TaxFormCode = "TFC1",
                TaxFormLocation = "TFL1",
                UnitOfIssue = "DOLLA",
                VendorPart = "Vendor Part",
                GlDistributions = glDistributions,
                LineItemTaxes = lineItemTaxes
            });

            PurchaseOrderDto.LineItems.Add(new LineItem()
            {
                Comments = "Volcano insurance.",
                Description = "Line item #1",
                ExpectedDeliveryDate = new DateTime(2015, 1, 1),
                ExtendedPrice = 512.45m,
                InvoiceNumber = "IN12345",
                Price = 100.99m,
                Quantity = 12m,
                TaxForm = "Tax Form",
                TaxFormCode = "TFC1",
                TaxFormLocation = "TFL1",
                UnitOfIssue = "DOLLA",
                VendorPart = "Vendor Part",
                GlDistributions = glDistributions,
                LineItemTaxes = lineItemTaxes
            });
            #endregion

            #region Populate Requisitions and Vouchers
            PurchaseOrderDto.Requisitions = new List<string>()
            {
                "601",
                "745",
                "211",
                "109"
            };

            PurchaseOrderDto.Vouchers = new List<string>()
            {
                "V0000601",
                "V0000745",
                "V0000211",
                "V0000109"
            };
            #endregion
        }
        #endregion

        #region Requisition methods
        public Requisition GetRequisition()
        {
            return RequisitionDto;
        }

        public void BuildRequisitionDto()
        {
            RequisitionDto = new Requisition();
            RequisitionDto.Id = "489";
            RequisitionDto.Number = "0000411";
            RequisitionDto.Status = RequisitionStatus.Outstanding;
            RequisitionDto.StatusDate = new DateTime(2015, 3, 24);
            RequisitionDto.Amount = 545.11m;
            RequisitionDto.CurrencyCode = "CC1";
            RequisitionDto.Date = new DateTime(2015, 3, 24);
            RequisitionDto.DesiredDate = new DateTime(2015, 4, 1);
            RequisitionDto.MaintenanceDate = new DateTime(2015, 3, 24);
            RequisitionDto.VendorId = "0012345";
            RequisitionDto.VendorName = "Susty Corporation";
            RequisitionDto.InitiatorName = "Andy Kleehammer";
            RequisitionDto.RequestorName = "Gary Thorne";
            RequisitionDto.ApType = "AP";
            RequisitionDto.Comments = "Comments";
            RequisitionDto.InternalComments = "Internal comments";

            #region Populate Approvers
            RequisitionDto.Approvers = new List<Approver>();
            RequisitionDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Gary Thorne",
                ApprovalDate = new DateTime(2015, 3, 24)
            });
            RequisitionDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Teresa Longerbeam",
                ApprovalDate = new DateTime(2015, 3, 24)
            });
            RequisitionDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Andy Kleehammer",
                ApprovalDate = null
            });
            #endregion

            #region Populate Line Items
            var glDistributions = new List<LineItemGlDistribution>();
            glDistributions.Add(new LineItemGlDistribution()
            {
                Amount = 123.45m,
                FormattedGlAccount = "10-00-01-02-20601-53013",
                GlAccount = "10_00_01_02_20601_53013",
                ProjectLineItemCode = "PLIC",
                ProjectNumber = "AJK-100",
                Quantity = 53m
            });
            glDistributions.Add(new LineItemGlDistribution()
            {
                Amount = 200.11m,
                FormattedGlAccount = "10-00-01-02-20601-51000",
                GlAccount = "10_00_01_02_20601_51000",
                ProjectLineItemCode = "GTT1",
                ProjectNumber = "GTT-897",
                Quantity = 19m
            });

            var lineItemTaxes = new List<LineItemTax>();
            lineItemTaxes.Add(new LineItemTax()
            {
                TaxCode = "VA",
                TaxAmount = 105.33m
            });
            lineItemTaxes.Add(new LineItemTax()
            {
                TaxCode = "MD",
                TaxAmount = 57.62m
            });

            RequisitionDto.LineItems = new List<LineItem>();
            RequisitionDto.LineItems.Add(new LineItem()
            {
                Comments = "Volcano insurance.",
                Description = "Line item #1",
                DesiredDate = new DateTime(2015, 4, 2),
                ExtendedPrice = 512.45m,
                Price = 100.99m,
                Quantity = 12m,
                TaxForm = "Tax Form",
                TaxFormCode = "TFC1",
                TaxFormLocation = "TFL1",
                UnitOfIssue = "DOLLA",
                VendorPart = "Vendor Part",
                GlDistributions = glDistributions,
                LineItemTaxes = lineItemTaxes
            });

            RequisitionDto.LineItems.Add(new LineItem()
            {
                Comments = "Volcano insurance.",
                Description = "Line item #2",
                DesiredDate = new DateTime(2015, 4, 2),
                ExtendedPrice = 512.45m,
                Price = 100.99m,
                Quantity = 12m,
                TaxForm = "Tax Form",
                TaxFormCode = "TFC1",
                TaxFormLocation = "TFL1",
                UnitOfIssue = "DOLLA",
                VendorPart = "Vendor Part",
                GlDistributions = glDistributions,
                LineItemTaxes = lineItemTaxes
            });
            #endregion

            #region Populate Purchase Orders
            RequisitionDto.PurchaseOrders = new List<string>()
            {
                "123",
                "124"
            };

            #endregion
        }
        #endregion

        #region Blanket Purchase Order methods

        public BlanketPurchaseOrder GetBlanketPurchaseOrder()
        {
            return BlanketPurchaseOrderDto;
        }

        public void BuildBlanketPurchaseOrderDto()
        {
            BlanketPurchaseOrderDto = new BlanketPurchaseOrder();
            BlanketPurchaseOrderDto.Id = "489";
            BlanketPurchaseOrderDto.Number = "B0000411";
            BlanketPurchaseOrderDto.Status = BlanketPurchaseOrderStatus.Outstanding;
            BlanketPurchaseOrderDto.StatusDate = new DateTime(2015, 1, 6);
            BlanketPurchaseOrderDto.Amount = 545.11m;
            BlanketPurchaseOrderDto.CurrencyCode = "CC1";
            BlanketPurchaseOrderDto.Date = new DateTime(2015, 3, 26);
            BlanketPurchaseOrderDto.ExpirationDate = new DateTime(2015, 5, 1);
            BlanketPurchaseOrderDto.MaintenanceDate = new DateTime(2015, 3, 26);
            BlanketPurchaseOrderDto.VendorId = "00012345";
            BlanketPurchaseOrderDto.VendorName = "Susty Corporation";
            BlanketPurchaseOrderDto.InitiatorName = "Andy Kleehammer";
            BlanketPurchaseOrderDto.Description = "Rocks";
            BlanketPurchaseOrderDto.ApType = "AP";
            BlanketPurchaseOrderDto.Comments = "Comments";
            BlanketPurchaseOrderDto.InternalComments = "Internal comments";

            #region Populate Approvers
            BlanketPurchaseOrderDto.Approvers = new List<Approver>();
            BlanketPurchaseOrderDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Gary Thorne",
                ApprovalDate = new DateTime(2015, 3, 26)
            });
            BlanketPurchaseOrderDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Teresa Longerbeam",
                ApprovalDate = new DateTime(2015, 3, 26)
            });
            BlanketPurchaseOrderDto.Approvers.Add(new Approver()
            {
                ApprovalName = "Andy Kleehammer",
                ApprovalDate = null
            });
            #endregion

            #region Populate Requisitions and Vouchers
            BlanketPurchaseOrderDto.Requisitions = new List<string>()
            {
                "601",
                "745",
                "211",
                "109"
            };

            BlanketPurchaseOrderDto.Vouchers = new List<string>()
            {
                "V0000601",
                "V0000745",
                "V0000211",
                "V0000109"
            };
            #endregion

            #region Populate GlDistributions
            BlanketPurchaseOrderDto.GlDistributions = new List<BlanketPurchaseOrderGlDistribution>()
            {
                new BlanketPurchaseOrderGlDistribution()
                {
                    GlAccount = "10_11",
                    Description = "Supplies : office",
                    FormattedGlAccount = "10-11",
                    ProjectNumber = "AJK-100",
                    ProjectLineItemCode = "AJK1",
                    EncumberedAmount = 10000.00m,
                    ExpensedAmount = 1000.00m
                },
                new BlanketPurchaseOrderGlDistribution()
                {
                    GlAccount = "10_22",
                    Description = "Supplies : laboratory",
                    FormattedGlAccount = "10-22",
                    ProjectNumber = "AJK-200",
                    ProjectLineItemCode = "AJK2",
                    EncumberedAmount = 100.00m,
                    ExpensedAmount = 14.00m
                },
                new BlanketPurchaseOrderGlDistribution()
                {
                    GlAccount = "10_33",
                    Description = "Supplies : athelics",
                    FormattedGlAccount = "10-33",
                    ProjectNumber = "AJK-300",
                    ProjectLineItemCode = "AJK3",
                    EncumberedAmount = 1000000.00m,
                    ExpensedAmount = 9561.00m
                }
            };
            #endregion
        }
        #endregion
    }
}