﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class GlActivityDetailViewModelTests
    {
        #region Initialize and Cleanup

        private GlAccountActivityDetail glAccountActivityDetailDto;
        private List<string> fiscalYears;
        private string currentFiscalYear;

        [TestInitialize]
        public void Initialize()
        {
            fiscalYears = new List<string>();
            fiscalYears.Add("2017");
            fiscalYears.Add("2016");
            fiscalYears.Add("2015");
            fiscalYears.Add("2014");
            fiscalYears.Add("2013");
            fiscalYears.Add("2012");
            fiscalYears.Add("2011");

            currentFiscalYear = "2016";

            glAccountActivityDetailDto = new GlAccountActivityDetail()
            {
                Actuals = 100.0m,
                Budget = 200.0m,
                Encumbrances = 50.0m,
                Description = "GL Account Activity Detail Description",
                GlAccountNumber = "11_22_33_44444_55555",
                FormattedGlAccount = "11-22-33-44444-55555",
                MemoActuals = 0.0m,
                MemoBudget = 0.0m,
                ActualsTransactions = new List<GlTransaction>(),
                BudgetTransactions = new List<GlTransaction>(),
                EncumbranceTransactions = new List<GlTransaction>(),
                EstimatedOpeningBalance = 12.34m,
                ClosingYearAmount = 23.45m
            };

            glAccountActivityDetailDto.ActualsTransactions.Add(new GlTransaction() { Amount = 100.0m, Description = "Actual Transaction description", DocumentId = "1", ReferenceNumber = "1", Source = "", TransactionDate = new DateTime(2016, 1, 1) });
            glAccountActivityDetailDto.BudgetTransactions.Add(new GlTransaction() { Amount = 200.0m, Description = "Budget Transaction description", DocumentId = "2", ReferenceNumber = "2", Source = "", TransactionDate = new DateTime(2016, 1, 1) });
            glAccountActivityDetailDto.EncumbranceTransactions.Add(new GlTransaction() { Amount = 100.0m, Description = "Encumbrance Transaction description", DocumentId = "3", ReferenceNumber = "3", Source = "", TransactionDate = new DateTime(2016, 1, 1) });
        }

        [TestCleanup]
        public void Cleanup()
        {
            glAccountActivityDetailDto = null;
        }
        #endregion

        #region Tests

        [TestMethod]
        public void Constructor()
        {
            var glActivityDetailVM = new GlActivityDetailViewModel(glAccountActivityDetailDto, fiscalYears, currentFiscalYear);

            Assert.AreEqual(glAccountActivityDetailDto.GlAccountNumber, glActivityDetailVM.GlAccountActivityDetail.GlAccount);
            Assert.AreEqual(glAccountActivityDetailDto.FormattedGlAccount, glActivityDetailVM.GlAccountActivityDetail.FormattedGlAccount);
            Assert.AreEqual(glAccountActivityDetailDto.Description, glActivityDetailVM.GlAccountActivityDetail.Description);
            Assert.AreEqual(glAccountActivityDetailDto.Budget.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalBudget);
            Assert.AreEqual(glAccountActivityDetailDto.Encumbrances.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalEncumbrances);
            var totalActuals = glAccountActivityDetailDto.Actuals + glAccountActivityDetailDto.EstimatedOpeningBalance + glAccountActivityDetailDto.ClosingYearAmount;
            Assert.AreEqual(totalActuals.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalActuals);
            Assert.AreEqual(glAccountActivityDetailDto.MemoActuals.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalActualsMemosDisplay);
            Assert.AreEqual(glAccountActivityDetailDto.MemoBudget.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalBudgetMemosDisplay);
            Assert.AreEqual(glAccountActivityDetailDto.EstimatedOpeningBalance.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.EstimatedOpeningBalanceDisplay);
            Assert.AreEqual(glAccountActivityDetailDto.ClosingYearAmount.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.ClosingYearAmountDisplay);
            Assert.AreEqual(glAccountActivityDetailDto.ActualsTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.ActualTransactions.Count);
            Assert.AreEqual(glAccountActivityDetailDto.BudgetTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.BudgetTransactions.Count);
            Assert.AreEqual(glAccountActivityDetailDto.EncumbranceTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.EncumbranceTransactions.Count);

            Assert.AreEqual(fiscalYears.Count, glActivityDetailVM.FiscalYears.Count);

            Assert.AreEqual(currentFiscalYear, glActivityDetailVM.TodaysFiscalYear);
        }

        [TestMethod]
        public void Constructor_NullGlAccountActivityDto()
        {
            var glActivityDetailVM = new GlActivityDetailViewModel(null as GlAccountActivityDetail, fiscalYears, currentFiscalYear);

            Assert.AreEqual(null, glActivityDetailVM.GlAccountActivityDetail.GlAccount);
            Assert.AreEqual(null, glActivityDetailVM.GlAccountActivityDetail.FormattedGlAccount);
            Assert.AreEqual(null, glActivityDetailVM.GlAccountActivityDetail.Description);
            Assert.AreEqual("$0.00", glActivityDetailVM.GlAccountActivityDetail.TotalBudget);
            Assert.AreEqual("$0.00", glActivityDetailVM.GlAccountActivityDetail.TotalEncumbrances);
            Assert.AreEqual("$0.00", glActivityDetailVM.GlAccountActivityDetail.TotalActuals);
            Assert.AreEqual(null, glActivityDetailVM.GlAccountActivityDetail.ActualTransactions);
            Assert.AreEqual(null, glActivityDetailVM.GlAccountActivityDetail.BudgetTransactions);
            Assert.AreEqual(null, glActivityDetailVM.GlAccountActivityDetail.EncumbranceTransactions);

            Assert.AreEqual(fiscalYears.Count, glActivityDetailVM.FiscalYears.Count);

            Assert.AreEqual(currentFiscalYear, glActivityDetailVM.TodaysFiscalYear);
        }

        [TestMethod]
        public void Constructor_NullFiscalYears()
        {
            var glActivityDetailVM = new GlActivityDetailViewModel(glAccountActivityDetailDto, null, currentFiscalYear);

            Assert.AreEqual(glAccountActivityDetailDto.GlAccountNumber, glActivityDetailVM.GlAccountActivityDetail.GlAccount);
            Assert.AreEqual(glAccountActivityDetailDto.FormattedGlAccount, glActivityDetailVM.GlAccountActivityDetail.FormattedGlAccount);
            Assert.AreEqual(glAccountActivityDetailDto.Description, glActivityDetailVM.GlAccountActivityDetail.Description);
            Assert.AreEqual(glAccountActivityDetailDto.Budget.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalBudget);
            Assert.AreEqual(glAccountActivityDetailDto.Encumbrances.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalEncumbrances);
            var totalActuals = glAccountActivityDetailDto.Actuals + glAccountActivityDetailDto.EstimatedOpeningBalance + glAccountActivityDetailDto.ClosingYearAmount;
            Assert.AreEqual(totalActuals.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalActuals);
            Assert.AreEqual(glAccountActivityDetailDto.ActualsTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.ActualTransactions.Count);
            Assert.AreEqual(glAccountActivityDetailDto.BudgetTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.BudgetTransactions.Count);
            Assert.AreEqual(glAccountActivityDetailDto.EncumbranceTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.EncumbranceTransactions.Count);

            Assert.AreEqual(0, glActivityDetailVM.FiscalYears.Count);

            Assert.AreEqual(currentFiscalYear, glActivityDetailVM.TodaysFiscalYear);
        }

        [TestMethod]
        public void Constructor_NullCurrentFiscalYear()
        {
            var glActivityDetailVM = new GlActivityDetailViewModel(glAccountActivityDetailDto, fiscalYears, null);

            Assert.AreEqual(glAccountActivityDetailDto.GlAccountNumber, glActivityDetailVM.GlAccountActivityDetail.GlAccount);
            Assert.AreEqual(glAccountActivityDetailDto.FormattedGlAccount, glActivityDetailVM.GlAccountActivityDetail.FormattedGlAccount);
            Assert.AreEqual(glAccountActivityDetailDto.Description, glActivityDetailVM.GlAccountActivityDetail.Description);
            Assert.AreEqual(glAccountActivityDetailDto.Budget.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalBudget);
            Assert.AreEqual(glAccountActivityDetailDto.Encumbrances.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalEncumbrances);
            var totalActuals = glAccountActivityDetailDto.Actuals + glAccountActivityDetailDto.EstimatedOpeningBalance + glAccountActivityDetailDto.ClosingYearAmount;
            Assert.AreEqual(totalActuals.ToString("C"), glActivityDetailVM.GlAccountActivityDetail.TotalActuals);
            Assert.AreEqual(glAccountActivityDetailDto.ActualsTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.ActualTransactions.Count);
            Assert.AreEqual(glAccountActivityDetailDto.BudgetTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.BudgetTransactions.Count);
            Assert.AreEqual(glAccountActivityDetailDto.EncumbranceTransactions.Count, glActivityDetailVM.GlAccountActivityDetail.EncumbranceTransactions.Count);

            Assert.AreEqual(fiscalYears.Count, glActivityDetailVM.FiscalYears.Count);

            Assert.AreEqual("", glActivityDetailVM.TodaysFiscalYear);
        }

        #endregion
    }
}
