﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class CostCenterDetailViewModelTests
    {
        #region Initialize and Cleanup
        private TestCostCenterDtoRepository testRepository;
        private CostCenter costCenterDto;
        private List<string> fiscalYears;
        private List<GeneralLedgerComponent> glComponentDtos;

        [TestInitialize]
        public void Initialize()
        {
            testRepository = new TestCostCenterDtoRepository();
            costCenterDto = testRepository.GetCostCenter();
            fiscalYears = new List<string>()
            {
                "2016",
                "2015",
                "2014",
                "2013",
                "2012",
                "2011",
            };

            glComponentDtos = new List<GeneralLedgerComponent>()
            {
                new GeneralLedgerComponent()
                {
                    ComponentName = "FUND",
                    StartPosition = 1,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "PROGRAM",
                    StartPosition = 4,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "ACTIVITY",
                    StartPosition = 7,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "LOCATION",
                    StartPosition = 10,
                    ComponentLength = 2
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "DEPARTMENT",
                    StartPosition = 13,
                    ComponentLength = 5
                },
                new GeneralLedgerComponent()
                {
                    ComponentName = "OBJECT",
                    StartPosition = 19,
                    ComponentLength = 5
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            testRepository = null;
            costCenterDto = null;
            fiscalYears = null;
        }
        #endregion

        [TestMethod]
        public void Constructor()
        {
            var costCenterDetailVM = new CostCenterDetailViewModel(costCenterDto, fiscalYears, null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(costCenterDetailVM.CostCenter is CostCenterViewModel);
            Assert.AreEqual("", costCenterDetailVM.TodaysFiscalYear);
            Assert.AreEqual(costCenterDto.Name, costCenterDetailVM.CostCenter.Name);
            Assert.AreEqual(costCenterDto.UnitId, costCenterDetailVM.CostCenter.UnitId);
            Assert.AreEqual(costCenterDto.TotalBudget.ToString("C"), costCenterDetailVM.CostCenter.TotalBudget);
            Assert.AreEqual(costCenterDto.TotalActuals.ToString("C"), costCenterDetailVM.CostCenter.TotalActuals);
            Assert.AreEqual(costCenterDto.TotalEncumbrances.ToString("C"), costCenterDetailVM.CostCenter.TotalEncumbrances);

            Assert.IsTrue(costCenterDetailVM.FiscalYears is List<FiscalYearFilterViewModel>);
            Assert.AreEqual(fiscalYears.Count, costCenterDetailVM.FiscalYears.Count);

            Assert.AreEqual(0, costCenterDetailVM.MajorComponents.Count);

            var firstFiscalYear = costCenterDetailVM.FiscalYears.First();
            Assert.AreEqual(fiscalYears.First(), firstFiscalYear.FiscalYear);
            
            for (int i = 0; i < fiscalYears.Count; i++)
            {
                var filterVM = costCenterDetailVM.FiscalYears.Where(x => x.FiscalYear == fiscalYears.ElementAt(i)).First();
                Assert.AreEqual(fiscalYears.ElementAt(i), filterVM.FiscalYear);
                Assert.AreEqual("FY" + fiscalYears.ElementAt(i), filterVM.FiscalYearDescription);
            }
        }

        [TestMethod]
        public void Constructor_CostCenterDtoIsNull()
        {
            var costCenterDetailVM = new CostCenterDetailViewModel(null, fiscalYears, null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(costCenterDetailVM.CostCenter is CostCenterViewModel);
        }

        [TestMethod]
        public void Constructor_FiscalYearsIsNull()
        {
            var costCenterDetailVM = new CostCenterDetailViewModel(costCenterDto, null, null);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(costCenterDetailVM.CostCenter is CostCenterViewModel);
            Assert.AreEqual(0, costCenterDetailVM.FiscalYears.Count);
        }

        [TestMethod]
        public void Constructor_FiscalYearIsNull()
        {
            fiscalYears.Add(null);
            var costCenterDetailVM = new CostCenterDetailViewModel(costCenterDto, fiscalYears, null);

            Assert.IsTrue(costCenterDetailVM.FiscalYears is List<FiscalYearFilterViewModel>);
            Assert.AreEqual(fiscalYears.Count - 1, costCenterDetailVM.FiscalYears.Count);
        }

        [TestMethod]
        public void Constructor_FiscalYearIsEmpty()
        {
            fiscalYears.Add(string.Empty);
            var costCenterSummaryVM = new CostCenterDetailViewModel(costCenterDto, fiscalYears, null);

            Assert.IsTrue(costCenterSummaryVM.FiscalYears is List<FiscalYearFilterViewModel>);
            Assert.AreEqual(fiscalYears.Count - 1, costCenterSummaryVM.FiscalYears.Count);
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_Success()
        {
            var costCenterSummaryVM = new CostCenterDetailViewModel(costCenterDto, fiscalYears, null);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.AreEqual(glComponentDtos.Count(), costCenterSummaryVM.MajorComponents.Count(), "The number of major components should be the same.");
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_CallTwice()
        {
            var costCenterSummaryVM = new CostCenterDetailViewModel(costCenterDto, fiscalYears, null);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.AreEqual(glComponentDtos.Count(), costCenterSummaryVM.MajorComponents.Count(), "The number of major components should be the same.");
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_NullComponentInList()
        {
            var costCenterSummaryVM = new CostCenterDetailViewModel(costCenterDto, fiscalYears, null);
            glComponentDtos.Add(null);
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.AreEqual(glComponentDtos.Count() - 1, costCenterSummaryVM.MajorComponents.Count(), "The number of major components should be the same.");
        }

        [TestMethod]
        public void SetGeneralLedgerMajorComponents_ComponentListIsNull()
        {
            var costCenterSummaryVM = new CostCenterDetailViewModel(costCenterDto, fiscalYears, null);
            glComponentDtos = null;
            costCenterSummaryVM.SetGeneralLedgerMajorComponents(glComponentDtos);

            Assert.IsTrue(costCenterSummaryVM.MajorComponents.Count() == 0, "The number of major components should be the same.");
        }
    }
}