﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the LineItemGlDistributionViewModel class.
    /// </summary>
    [TestClass]
    public class DocumentApproverViewModelTests
    {
        #region Initialize and Cleanup
        public Voucher2 voucherDto;
        public Approver documentApproverDto;
        public TestDocumentDtoRepository documentRepository;
        public DocumentApproverViewModel documentApproverVM;

        [TestInitialize]
        public void Initialize()
        {
            voucherDto = new Voucher2();
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            voucherDto = null;
            documentApproverDto = null;
            documentRepository = null;
            documentApproverVM = null;
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void DocumentApproverViewModel_FullyPopulated()
        {
            voucherDto = documentRepository.GetVoucher();
            documentApproverDto = voucherDto.Approvers.Where(x => x.ApprovalDate.HasValue).First();
            documentApproverVM = new DocumentApproverViewModel(documentApproverDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(documentApproverDto.ApprovalName, documentApproverVM.Name);
            Assert.AreEqual(documentApproverDto.ApprovalDate.Value.ToShortDateString(), documentApproverVM.Date);
            Assert.AreEqual(documentApproverDto.ApprovalDate.Value.ToShortDateString(), documentApproverVM.DateOrWaiting);
        }

        [TestMethod]
        public void DocumentApproverViewModel_DateIsNull()
        {
            voucherDto = documentRepository.GetVoucher();
            documentApproverDto = voucherDto.Approvers.Where(x => !x.ApprovalDate.HasValue).First();
            documentApproverVM = new DocumentApproverViewModel(documentApproverDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(documentApproverDto.ApprovalName, documentApproverVM.Name);
            Assert.AreEqual("", documentApproverVM.Date);
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "AwaitingApprovalText"), documentApproverVM.DateOrWaiting);
        }
        #endregion
    }
}
