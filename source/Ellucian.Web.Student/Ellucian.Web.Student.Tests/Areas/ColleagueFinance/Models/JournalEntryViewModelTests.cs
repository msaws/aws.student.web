﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// Test the JournalEntryViewModel
    /// </summary>
    [TestClass]
    public class JournalEntryViewModelTests
    {
        #region Initialize and Cleanup

        public JournalEntry journalEntryDto;
        public JournalEntryViewModel journalEntryVM;
        public TestLedgerEntryDocumentDtoRepository ledgerEntryDocumentRepository;

        [TestInitialize]
        public void Initialize()
        {
            ledgerEntryDocumentRepository = new TestLedgerEntryDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            journalEntryDto = null;
            journalEntryVM = null;
            ledgerEntryDocumentRepository = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void JournalEntryViewModel_VerifyBaseFinanceDocument()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(journalEntryDto.Id, journalEntryVM.DocumentId, " The journal entry ID should be initialized.");
            Assert.AreEqual(journalEntryDto.Id, journalEntryVM.Number, "The journal entry should be initialized.");
            Assert.AreEqual(journalEntryDto.Comments, journalEntryVM.Comments, "The journal entry comments should be initialized.");
            Assert.IsTrue(journalEntryVM.Approvers is List<DocumentApproverViewModel>, "The journal entry approvers should be the correct type.");
            Assert.IsTrue(journalEntryVM.IsJournalEntry, "The document is a journal entry.");
            Assert.IsTrue(journalEntryVM.IsLedgerEntryDocument, "The document is a ledger entry document.");
            Assert.IsFalse(journalEntryVM.IsRequisition, "The document is not a requisition.");
            Assert.IsFalse(journalEntryVM.IsPurchaseOrder, "The document is not a purchase order.");
            Assert.IsFalse(journalEntryVM.IsVoucher, "The document is not a voucher.");
            Assert.IsFalse(journalEntryVM.IsProcurementDocument, "The document is not a procurement document.");
        }

        [TestMethod]
        public void JournalEntryViewModel_VerifyLedgerEntryDocument()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Verify that the AccountsPayablePurchasingDocumentViewModel initialized the appropriate variables
            Assert.AreEqual(journalEntryDto.Date.ToShortDateString(), journalEntryVM.Date, "The journal entry date should be initialized.");
            Assert.AreEqual(journalEntryDto.EnteredDate.ToShortDateString(), journalEntryVM.EnteredDate, "The journal entry entered date should be initialized.");
            Assert.AreEqual(journalEntryDto.EnteredByName, journalEntryVM.EnteredBy, "The journal entry entered by should be initialized.");
            Assert.IsTrue(journalEntryVM.LineItems is List<ItemViewModel>, "The journal entry items list should be the correct type.");
        }
        [TestMethod]
        public void JournalEntryViewModel_VerifyRemainingProperties()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(journalEntryDto.Author, journalEntryVM.Author);
            Assert.AreEqual(journalEntryDto.AutomaticReversal, journalEntryVM.AutomaticReversal);
            Assert.AreEqual(journalEntryDto.Status, journalEntryVM.Status);
            Assert.AreEqual(journalEntryDto.TotalCredits.ToString("C"), journalEntryVM.TotalCredits);
            Assert.AreEqual(journalEntryDto.TotalDebits.ToString("C"), journalEntryVM.TotalDebits);
            Assert.AreEqual(journalEntryDto.Type, journalEntryVM.Type);
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryTypeGeneral"), journalEntryVM.TypeDescription);

            Assert.AreEqual(journalEntryDto.Items.Count(), journalEntryVM.LineItems.Count(), "The number of items in the journal entry should be the same.");

            Assert.AreEqual(journalEntryDto.Approvers.Count(), journalEntryVM.Approvers.Count(), "The number of approvers in the journal entry should be the same.");
        }

        [TestMethod]
        public void Constructor_NullableObjectsAreNull()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();

            journalEntryDto.Author = null;
            journalEntryDto.Comments = null;
            journalEntryDto.Items = null;
            journalEntryDto.Approvers = null;

            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            Assert.AreEqual(null, journalEntryVM.Comments);
            Assert.AreEqual(null, journalEntryVM.Author);
            Assert.AreEqual(journalEntryDto.AutomaticReversal, journalEntryVM.AutomaticReversal);
            Assert.AreEqual("No", journalEntryVM.AutomaticReversalDescription);
            Assert.AreEqual(journalEntryDto.Status, journalEntryVM.Status);
            Assert.AreEqual(journalEntryDto.TotalCredits.ToString("C"), journalEntryVM.TotalCredits);
            Assert.AreEqual(journalEntryDto.TotalDebits.ToString("C"), journalEntryVM.TotalDebits);
            Assert.AreEqual(journalEntryDto.Type, journalEntryVM.Type);
            Assert.IsTrue(journalEntryVM.Approvers is List<DocumentApproverViewModel>);
            Assert.IsTrue(journalEntryVM.Approvers.Count() == 0);
            Assert.IsTrue(journalEntryVM.LineItems is List<ItemViewModel>);
            Assert.IsTrue(journalEntryVM.LineItems.Count() == 0);
        }

        [TestMethod]
        public void Constructor_AutomaticReversalTrue()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.AutomaticReversal = true;

            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            Assert.AreEqual(journalEntryDto.AutomaticReversal, journalEntryVM.AutomaticReversal);
            Assert.AreEqual("Yes", journalEntryVM.AutomaticReversalDescription);

        }

        [TestMethod]
        public void Constructor_TypeDescriptionOpening()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Type = JournalEntryType.OpeningBalance;

            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryTypeOpeningBalance"), journalEntryVM.TypeDescription);
        }

        #endregion

        #region Status Description Tests

        [TestMethod]
        public void StatusDescription_Complete()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Status = JournalEntryStatus.Complete;
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Confirm the journal entry status is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryStatusComplete"), journalEntryVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_NotApproved()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Status = JournalEntryStatus.NotApproved;
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Confirm the journal entry status is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryStatusNotApproved"), journalEntryVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Unfinished()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Status = JournalEntryStatus.Unfinished;
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Confirm the journal entry status is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryStatusUnfinished"), journalEntryVM.StatusDescription);
        }
        #endregion

        #region Type Description Tests

        [TestMethod]
        public void TypeDescription_General()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Type = JournalEntryType.General;
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Confirm the journal entry type is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryTypeGeneral"), journalEntryVM.StatusDescription);
        }

        [TestMethod]
        public void TypeDescription_OpeningBalance()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Type = JournalEntryType.OpeningBalance;

            journalEntryVM = new JournalEntryViewModel(journalEntryDto);

            // Confirm the journal entry status is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.GeneralLedgerDocumentsResources, "JournalEntryTypeOpeningBalance"), journalEntryVM.StatusDescription);
        }
        #endregion

        #region SetItems tests
        [TestMethod]
        public void SetItems_Success()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);
            journalEntryVM.SetItems(journalEntryDto.Items);

            Assert.AreEqual(journalEntryDto.Items.Count(), journalEntryVM.LineItems.Count(), "The line items lists should be the same size.");

            // Each line item should have sequential IDs starting at 1
            int id = 1;
            foreach (var lineItem in journalEntryVM.LineItems)
            {
                Assert.AreEqual("document-line-item" + id.ToString(), lineItem.DocumentItemId, "The document item ID should match.");
                id++;
            }
        }

        [TestMethod]
        public void SetItems_CallTwice()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryVM = new JournalEntryViewModel(journalEntryDto);
            journalEntryVM.SetItems(journalEntryDto.Items);
            journalEntryVM.SetItems(journalEntryDto.Items);

            Assert.AreEqual(journalEntryDto.Items.Count(), journalEntryVM.LineItems.Count(), "The line items lists should be the same size.");
        }

        [TestMethod]
        public void SetItems_NullLineItemInList()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Items = new List<JournalEntryItem>();
            journalEntryDto.Items.Add(null);

            journalEntryVM = new JournalEntryViewModel(journalEntryDto);
            journalEntryVM.SetItems(journalEntryDto.Items);

            Assert.IsTrue(journalEntryVM.LineItems.Count() == 0, "The line items list should have 0 items.");
        }

        [TestMethod]
        public void SetItems_LineItemsListIsNull()
        {
            journalEntryDto = ledgerEntryDocumentRepository.GetJournalEntry();
            journalEntryDto.Items = null;

            journalEntryVM = new JournalEntryViewModel(journalEntryDto);
            journalEntryVM.SetItems(journalEntryDto.Items);

            Assert.IsTrue(journalEntryVM.LineItems.Count() == 0, "The line items list should have 0 items.");
        }
        #endregion
    }
}
