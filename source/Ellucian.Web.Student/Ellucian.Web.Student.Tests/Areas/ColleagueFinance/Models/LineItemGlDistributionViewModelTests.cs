﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the LineItemGlDistributionViewModel class.
    /// </summary>
    [TestClass]
    public class LineItemGlDistributionViewModelTests
    {
        #region Initialize and Cleanup
        public LineItemGlDistribution glDistributionDto;
        public LineItemGlDistributionViewModel lineItemGlDistributionVM;

        [TestInitialize]
        public void Initialize()
        {
            glDistributionDto = new LineItemGlDistribution()
            {
                GlAccount = "11_00_01_02_20601_53013",
                FormattedGlAccount = "11-00-01-02-20601-53013",
                ProjectNumber = "AJK-200",
                ProjectLineItemCode = "AJK1",
                Quantity = 10m,
                Amount = 100m
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            glDistributionDto = null;
            lineItemGlDistributionVM = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void Constructor_FullyPopulated()
        {
            lineItemGlDistributionVM = new LineItemGlDistributionViewModel(glDistributionDto);
            Assert.AreEqual(glDistributionDto.GlAccount, lineItemGlDistributionVM.GlAccount);
            Assert.AreEqual(glDistributionDto.FormattedGlAccount, lineItemGlDistributionVM.FormattedGlAccount);
            Assert.AreEqual(glDistributionDto.ProjectNumber, lineItemGlDistributionVM.ProjectNumber);
            Assert.AreEqual(glDistributionDto.ProjectLineItemCode, lineItemGlDistributionVM.ProjectLineItemCode);
            Assert.AreEqual(glDistributionDto.Quantity, lineItemGlDistributionVM.Quantity);
            Assert.AreEqual(glDistributionDto.Amount, lineItemGlDistributionVM.Amount);
        }
        #endregion

        #region AmountDisplay tests
        [TestMethod]
        public void AmountDisplay_Base()
        {
            lineItemGlDistributionVM = new LineItemGlDistributionViewModel(glDistributionDto);
            Assert.AreEqual(glDistributionDto.Amount.ToString("C"), lineItemGlDistributionVM.AmountDisplay);
        }
        #endregion

        #region IsAccountMasked tests
        [TestMethod]
        public void IsAccountMasked_True()
        {
            glDistributionDto.GlAccount = null;
            lineItemGlDistributionVM = new LineItemGlDistributionViewModel(glDistributionDto);
            Assert.IsTrue(lineItemGlDistributionVM.IsAccountMasked);
        }

        [TestMethod]
        public void IsAccountMasked_False()
        {
            lineItemGlDistributionVM = new LineItemGlDistributionViewModel(glDistributionDto);
            Assert.IsFalse(lineItemGlDistributionVM.IsAccountMasked);
        }
        #endregion
    }
}
