﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the RequisitionNumberViewModel
    /// </summary>
    [TestClass]
    public class RequisitionNumberViewModelTests
    {
        #region Initialize and Cleanup
        public Requisition requisitionDto;
        public RequisitionNumberViewModel requisitionNumberVM;
        public TestDocumentDtoRepository documentRepository;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            requisitionDto = null;
            requisitionNumberVM = null;
            documentRepository = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void RequisitionnumberViewModel_VerifyBaseFinanceDocument()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionNumberVM = new RequisitionNumberViewModel(requisitionDto.Id);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(requisitionDto.Id, requisitionNumberVM.Id, "The document ID should be initialized.");
        }
        #endregion

    }
}
