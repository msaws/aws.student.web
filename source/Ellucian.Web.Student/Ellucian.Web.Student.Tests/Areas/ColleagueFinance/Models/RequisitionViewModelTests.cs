﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the RequisitionViewModel
    /// </summary>
    [TestClass]
    public class RequisitionViewModelTests
    {
        #region Initialize and Cleanup
        public Requisition requisitionDto;
        public RequisitionViewModel requisitionVM;
        public TestDocumentDtoRepository documentRepository;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            requisitionDto = null;
            requisitionVM = null;
            documentRepository = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void RequisitionViewModel_VerifyBaseFinanceDocument()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionVM = new RequisitionViewModel(requisitionDto);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(requisitionDto.Id, requisitionVM.DocumentId, "The document ID should be initialized.");
            Assert.AreEqual(requisitionDto.Number, requisitionVM.Number, "The document number should be initialized.");
            Assert.AreEqual(requisitionDto.Comments, requisitionVM.Comments, "The document comments list should be initialized.");
            Assert.IsTrue(requisitionVM.Approvers is List<DocumentApproverViewModel>, "The document approvers should be the correct type.");
            Assert.IsTrue(requisitionVM.LineItems is List<LineItemViewModel>, "The document line items list should be the correct type.");
            Assert.IsTrue(requisitionVM.IsRequisition, "The document is a requisition.");
            Assert.IsTrue(requisitionVM.IsProcurementDocument, "The document is a procurement document.");
            Assert.IsFalse(requisitionVM.IsPurchaseOrder, "The document is not a purchase order.");
            Assert.IsFalse(requisitionVM.IsVoucher, "The document is not a voucher.");
            Assert.IsFalse(requisitionVM.IsLedgerEntryDocument, "The document is not a ledger entry document.");
            Assert.IsFalse(requisitionVM.IsJournalEntry, "The document is not a journal entry.");
        }

        [TestMethod]
        public void RequisitionViewModel_VerifyAccountsPayablePurchasingDocument()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionVM = new RequisitionViewModel(requisitionDto);

            // Verify that the AccountsPayablePurchasingDocumentViewModel initialized the appropriate variables
            Assert.AreEqual(requisitionDto.Date.ToShortDateString(), requisitionVM.Date, "The document date should be initialized.");
        }

        [TestMethod]
        public void RequisitionViewModel_RemainingProperties()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionVM = new RequisitionViewModel(requisitionDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(requisitionDto.Status, requisitionVM.Status);
            Assert.AreEqual(requisitionDto.StatusDate.ToShortDateString(), requisitionVM.StatusDate);
            Assert.AreEqual(requisitionDto.DesiredDate.Value.ToShortDateString(), requisitionVM.DesiredDate);
            Assert.AreEqual(requisitionDto.InitiatorName, requisitionVM.InitiatorName);
            Assert.AreEqual(requisitionDto.RequestorName, requisitionVM.RequestorName);
            Assert.AreEqual(requisitionDto.InternalComments, requisitionVM.InternalComments);
            Assert.AreEqual(requisitionDto.VendorId, requisitionVM.VendorId);
            Assert.AreEqual(requisitionDto.VendorName, requisitionVM.VendorName);
            Assert.AreEqual(requisitionDto.Amount.ToString("C"), requisitionVM.Amount);
            Assert.AreEqual(requisitionDto.MaintenanceDate.Value.ToShortDateString(), requisitionVM.MaintenanceDate);
            Assert.AreEqual(requisitionDto.CurrencyCode, requisitionVM.CurrencyCode);
            Assert.AreEqual(requisitionDto.ApType, requisitionVM.ApType);

            Assert.IsTrue(requisitionVM.PurchaseOrders is List<PurchaseOrderNumberViewModel>);
            Assert.AreEqual(requisitionDto.PurchaseOrders.Count(), requisitionVM.PurchaseOrders.Count());
            foreach (var poId in requisitionDto.PurchaseOrders)
            {
                Assert.IsTrue(requisitionVM.PurchaseOrders.Any(x => x.Id == poId));
            }

            Assert.IsTrue(requisitionVM.LineItems is List<LineItemViewModel>);
            Assert.AreEqual(requisitionDto.LineItems.Count(), requisitionVM.LineItems.Count());

            Assert.AreEqual(requisitionDto.Approvers.Count(), requisitionVM.Approvers.Count(), "The document approvers list sizes should be the same.");

            // Each line item should have sequential IDs starting at 1
            int id = 1;
            foreach (var lineItem in requisitionVM.LineItems)
            {
                Assert.AreEqual("document-line-item" + id.ToString(), lineItem.DocumentItemId, "The document item ID should match.");
                id++;
            }
        }

        [TestMethod]
        public void Constructor_NullableObjectsAreNull()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.DesiredDate = null;
            requisitionDto.PurchaseOrders = null;
            requisitionDto.MaintenanceDate = null;
            requisitionDto.CurrencyCode = null;
            requisitionDto.ApType = null;
            requisitionDto.LineItems = null;
            requisitionDto.VendorName = null;
            requisitionVM = new RequisitionViewModel(requisitionDto);

            Assert.AreEqual("", requisitionVM.DesiredDate);
            Assert.AreEqual("", requisitionVM.VendorName);
            Assert.AreEqual("", requisitionVM.MaintenanceDate);
            Assert.AreEqual(null, requisitionVM.CurrencyCode);
            Assert.AreEqual(null, requisitionVM.ApType);

            Assert.IsTrue(requisitionVM.PurchaseOrders is List<PurchaseOrderNumberViewModel>);
            Assert.IsTrue(requisitionVM.PurchaseOrders.Count() == 0);

            Assert.IsTrue(requisitionVM.LineItems is List<LineItemViewModel>);
            Assert.IsTrue(requisitionVM.LineItems.Count() == 0);
        }

        [TestMethod]
        public void Constructor_ShortVendorName()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.VendorName = "Susty Corporation";
            requisitionVM = new RequisitionViewModel(requisitionDto);

            Assert.AreEqual(requisitionDto.VendorName, requisitionVM.VendorName);
        }

        [TestMethod]
        public void Constructor_LongVendorName()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.VendorName = "Susty Corporation International LLC";
            requisitionVM = new RequisitionViewModel(requisitionDto);

            Assert.AreEqual(requisitionDto.VendorName, requisitionVM.VendorName);
        }

        [TestMethod]
        public void Constructor_NullInitiatorName()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.VendorName = "Susty Corporation International LLC";
            requisitionDto.InitiatorName = null;
            requisitionVM = new RequisitionViewModel(requisitionDto);

            Assert.AreEqual(String.Empty, requisitionVM.InitiatorName);
        }

        [TestMethod]
        public void Constructor_LongInitiatorName()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.VendorName = "Susty Corporation International LLC";
            requisitionDto.InitiatorName = "Susty Corporation International LLC";
            requisitionVM = new RequisitionViewModel(requisitionDto);

            Assert.AreEqual(requisitionDto.InitiatorName, requisitionVM.InitiatorName);
        }

        [TestMethod]
        public void Constructor_NulllRequestorName()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.VendorName = "Susty Corporation International LLC";
            requisitionDto.RequestorName = null;
            requisitionVM = new RequisitionViewModel(requisitionDto);

            Assert.AreEqual(string.Empty, requisitionVM.RequestorName);
        }

        [TestMethod]
        public void Constructor_LongRequestorName()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.VendorName = "Susty Corporation International LLC";
            requisitionDto.RequestorName = "Susty Corporation International LLC";
            requisitionVM = new RequisitionViewModel(requisitionDto);

            Assert.AreEqual(requisitionDto.RequestorName, requisitionVM.RequestorName);
        }

        #endregion

        #region StatusDescription tests
        [TestMethod]
        public void StatusDescription_InProgress()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.Status = RequisitionStatus.InProgress;
            requisitionVM = new RequisitionViewModel(requisitionDto);

            // Confirm that the requisition status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RequisitionStatusInProgress"), requisitionVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_NotApproved()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.Status = RequisitionStatus.NotApproved;
            requisitionVM = new RequisitionViewModel(requisitionDto);

            // Confirm that the requisition status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RequisitionStatusNotApproved"), requisitionVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Outstanding()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.Status = RequisitionStatus.Outstanding;
            requisitionVM = new RequisitionViewModel(requisitionDto);

            // Confirm that the requisition status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "RequisitionStatusOutstanding"), requisitionVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_PoCreated()
        {
            requisitionDto = documentRepository.GetRequisition();
            requisitionDto.Status = RequisitionStatus.PoCreated;
            requisitionVM = new RequisitionViewModel(requisitionDto);

            // Confirm that the requisition status description is correct
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ProcurementDocumentsResources, "PurchaseOrderStatusPoCreated"), requisitionVM.StatusDescription);
        }
        #endregion
    }
}
