﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class RecurringVoucherScheduleViewModelTests
    {
        #region Initialize and Cleanup
        public TestDocumentDtoRepository documentRepository;
        public RecurringVoucherSchedule scheduleDto;
        public RecurringVoucherScheduleViewModel scheduleVM;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
            scheduleDto = new RecurringVoucherSchedule();
        }

        [TestCleanup]
        public void Cleanup()
        {
            documentRepository = null;
            scheduleDto = null;
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void RecurringVoucherScheduleViewModel_Success()
        {
            scheduleDto = documentRepository.GetRecurringVoucher().Schedules[1];
            scheduleVM = new RecurringVoucherScheduleViewModel(scheduleDto);

            Assert.AreEqual(scheduleDto.Amount, scheduleVM.Amount, "Amount should match.");
            Assert.AreEqual(scheduleDto.Amount.ToString("C"), scheduleVM.AmountDisplay, "Amount should be formatted.");
            Assert.AreEqual(scheduleDto.Date.ToShortDateString(), scheduleVM.Date, "Schedule date should match.");
            Assert.AreEqual(scheduleDto.TaxAmount, scheduleVM.TaxAmount, "Tax amount should match.");
            Assert.AreEqual(scheduleDto.TaxAmount.ToString("C"), scheduleVM.TaxAmountDisplay, "Amount should be formatted.");
            Assert.AreEqual(string.Empty, scheduleVM.VoucherDueDate, "Voucher due date should be initialized to empty.");
            Assert.AreEqual(scheduleDto.VoucherId, scheduleVM.VoucherId, "Voucher ID should match.");
            Assert.AreEqual(scheduleDto.PurgedVoucherId, scheduleVM.PurgedVoucherId, "Purged Voucher ID should match.");
            Assert.AreEqual(string.Empty, scheduleVM.VoucherStatus, "Voucher status should be initialized to empty.");
        }
        #endregion
    }
}
