﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    /// <summary>
    /// This class tests the LineItemGlDistributionViewModel class.
    /// </summary>
    [TestClass]
    public class VoucherViewModelTests
    {
        #region Initialize and Cleanup
        public Voucher2 voucherDto;
        public TestDocumentDtoRepository documentRepository;
        public VoucherViewModel voucherVM;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            voucherDto = null;
            documentRepository = null;
            voucherVM = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void VoucherViewModel_VerifyBaseFinanceDocument()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherVM = new VoucherViewModel(voucherDto);

            // Verify that BaseFinanceDocument initialized the appropriate variables
            Assert.AreEqual(voucherDto.VoucherId, voucherVM.DocumentId, "The document ID should be initialized.");
            Assert.AreEqual(voucherDto.VoucherId, voucherVM.Number, "The document number should be initialized.");
            Assert.IsTrue(voucherVM.Approvers is List<DocumentApproverViewModel>, "The document approvers should be the correct type.");
            Assert.IsTrue(voucherVM.LineItems is List<LineItemViewModel>, "The document line items list should be the correct type.");
            Assert.IsTrue(voucherVM.IsVoucher, "The document is a voucher.");
            Assert.IsTrue(voucherVM.IsProcurementDocument, "The document is a procurement document.");
            Assert.IsFalse(voucherVM.IsRequisition, "The document is not a requisition.");
            Assert.IsFalse(voucherVM.IsPurchaseOrder, "The document is not a purchase order.");
            Assert.IsFalse(voucherVM.IsLedgerEntryDocument, "The document is not a ledger entry document.");
            Assert.IsFalse(voucherVM.IsJournalEntry, "The document is not a journal entry.");
        }

        [TestMethod]
        public void VoucherViewModel_VerifyAccountsPayablePurchasingDocument()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherVM = new VoucherViewModel(voucherDto);

            // Verify that the AccountsPayablePurchasingDocumentViewModel initialized the appropriate variables
            Assert.AreEqual(voucherDto.Date.ToShortDateString(), voucherVM.Date, "The document date should be initialized.");
        }

        [TestMethod]
        public void VoucherViewModel_RemainingProperties()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the remaining internal properties have been initialized properly
            Assert.AreEqual(voucherDto.Comments, voucherVM.Comments, "The document comments list should be initialized.");
            Assert.AreEqual(voucherDto.DueDate.Value.ToShortDateString(), voucherVM.DueDate);
            Assert.AreEqual(voucherDto.InvoiceNumber, voucherVM.InvoiceNumber);
            if (voucherDto.InvoiceDate.HasValue)
            {
                Assert.AreEqual(voucherDto.InvoiceDate.Value.ToShortDateString(), voucherVM.InvoiceDate);
            }
            else
            {
                Assert.AreEqual(voucherDto.InvoiceDate, string.Empty);
            }
            Assert.AreEqual(voucherDto.CheckNumber, voucherVM.CheckNumber);
            Assert.AreEqual(voucherDto.CheckDate.Value.ToShortDateString(), voucherVM.CheckDate);
            Assert.AreEqual(voucherDto.Status, voucherVM.Status);
            Assert.AreEqual(voucherDto.VendorId, voucherVM.VendorId);
            Assert.AreEqual(voucherDto.VendorName, voucherVM.VendorName);
            Assert.AreEqual(voucherDto.Amount.ToString("C"), voucherVM.Amount);
            Assert.AreEqual(voucherDto.MaintenanceDate.Value.ToShortDateString(), voucherVM.MaintenanceDate);
            Assert.AreEqual(voucherDto.CurrencyCode, voucherVM.CurrencyCode);
            Assert.AreEqual(voucherDto.Date.ToShortDateString(), voucherVM.Date);
            Assert.AreEqual(voucherDto.ApType, voucherVM.ApType);
            Assert.AreEqual(voucherDto.Approvers.Count(), voucherVM.Approvers.Count(), "The document approvers list sizes should be the same.");

            Assert.IsTrue(voucherVM.LineItems is List<LineItemViewModel>);
            Assert.AreEqual(voucherDto.LineItems.Count(), voucherVM.LineItems.Count());
        }

        [TestMethod]
        public void VoucherViewModel_NullableObjectsAreNull()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.DueDate = null;
            voucherDto.CheckDate = null;
            voucherDto.MaintenanceDate = null;
            voucherDto.LineItems = null;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual("", voucherVM.DueDate);
            Assert.AreEqual(voucherDto.InvoiceNumber, voucherVM.InvoiceNumber);
            if (voucherDto.InvoiceDate.HasValue)
            {
                Assert.AreEqual(voucherDto.InvoiceDate.Value.ToShortDateString(), voucherVM.InvoiceDate);
            }
            else
            {
                Assert.AreEqual(voucherDto.InvoiceDate, string.Empty);
            }
            Assert.AreEqual(voucherDto.CheckNumber, voucherVM.CheckNumber);
            Assert.AreEqual("", voucherVM.CheckDate);
            Assert.AreEqual(voucherDto.Status, voucherVM.Status);
            Assert.AreEqual("", voucherVM.MaintenanceDate);

            Assert.IsTrue(voucherVM.LineItems is List<LineItemViewModel>);
            Assert.IsTrue(voucherVM.LineItems.Count() == 0);
        }

        [TestMethod]
        public void Constructor_NoVendorName()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.VendorName = null;
            voucherVM = new VoucherViewModel(voucherDto);

            Assert.AreEqual(string.Empty, voucherVM.VendorName);
        }

        [TestMethod]
        public void Constructor_ShortVendorName()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.VendorName = "Susty Corporation";
            voucherVM = new VoucherViewModel(voucherDto);

            Assert.AreEqual(voucherDto.VendorName, voucherVM.VendorName);
        }

        [TestMethod]
        public void Constructor_LongVendorName()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.VendorName = "Susty Corporation International LLC";
            voucherVM = new VoucherViewModel(voucherDto);

            Assert.AreEqual(voucherDto.VendorName, voucherVM.VendorName);
        }
        #endregion

        #region AssociatedDocument tests
        [TestMethod]
        public void AssociatedDocument_HasPurchaseOrderId()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "P000123";
            voucherDto.BlanketPurchaseOrderId = "";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(voucherDto.PurchaseOrderId, voucherVM.AssociatedDocument);
            Assert.AreEqual(FinanceDocumentType.PurchaseOrder, voucherVM.AssociatedDocumentType);
        }

        [TestMethod]
        public void AssociatedDocument_HasBlanketPurchaseOrderId()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "B000234";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(voucherDto.BlanketPurchaseOrderId, voucherVM.AssociatedDocument);
            Assert.AreEqual(FinanceDocumentType.BlanketPurchaseOrder, voucherVM.AssociatedDocumentType);
        }

        [TestMethod]
        public void AssociatedDocument_HasRecurringVoucherId()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "";
            voucherDto.RecurringVoucherId = "RC000345";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(voucherDto.RecurringVoucherId, voucherVM.AssociatedDocument);
            Assert.AreEqual(FinanceDocumentType.RecurringVoucher, voucherVM.AssociatedDocumentType);
        }

        [TestMethod]
        public void AssociatedDocument_HasNoAssociatedDocument()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(voucherDto.PurchaseOrderId, voucherVM.AssociatedDocument);
            Assert.AreEqual(voucherDto.BlanketPurchaseOrderId, voucherVM.AssociatedDocument);
            Assert.AreEqual(voucherDto.RecurringVoucherId, voucherVM.AssociatedDocument);
        }
        #endregion

        #region IsAssociatedDocumentPurchaseOrder tests
        [TestMethod]
        public void IsAssociatedDocumentPurchaseOrder_True()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "P000123";
            voucherDto.BlanketPurchaseOrderId = "";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(voucherVM.IsAssociatedDocumentPurchaseOrder);
        }

        [TestMethod]
        public void IsAssociatedDocumentPurchaseOrder_False()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "B000012";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsFalse(voucherVM.IsAssociatedDocumentPurchaseOrder);
        }
        #endregion

        #region IsAssociatedDocumentRecurringVoucher tests
        [TestMethod]
        public void IsAssociatedDocumentRecurringVoucher_True()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "";
            voucherDto.RecurringVoucherId = "RV00000012";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(voucherVM.IsAssociatedDocumentRecurringVoucher);
        }

        [TestMethod]
        public void IsAssociatedDocumentRecurringVoucher_False()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "B000012";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsFalse(voucherVM.IsAssociatedDocumentRecurringVoucher);
        }
        #endregion

        #region IsAssociatedDocumentLinkable tests
        [TestMethod]
        public void IsAssociatedDocumentLinkable_HasPurchaseOrderId()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "P000123";
            voucherDto.BlanketPurchaseOrderId = "";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(voucherVM.IsAssociatedDocumentLinkable);
        }

        [TestMethod]
        public void IsAssociatedDocumentLinkable_HasBlanketPurchaseOrderId()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "B000234";
            voucherDto.RecurringVoucherId = "";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(voucherVM.IsAssociatedDocumentLinkable);
            Assert.IsTrue(voucherVM.IsAssociatedDocumentBlanketPurchaseOrder);
        }

        [TestMethod]
        public void IsAssociatedDocumentLinkable_HasRecurringVoucherId()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.PurchaseOrderId = "";
            voucherDto.BlanketPurchaseOrderId = "";
            voucherDto.RecurringVoucherId = "RC000345";
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.IsTrue(voucherVM.IsAssociatedDocumentLinkable);
        }
        #endregion

        #region StatusDescription tests
        [TestMethod]
        public void StatusDescription_Cancelled()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Status = VoucherStatus.Cancelled;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "VoucherStatusCancelled"), voucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_InProgress()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Status = VoucherStatus.InProgress;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "VoucherStatusInProgress"), voucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_NotApproved()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Status = VoucherStatus.NotApproved;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "VoucherStatusNotApproved"), voucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Outstanding()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Status = VoucherStatus.Outstanding;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "VoucherStatusOutstanding"), voucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Paid()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Status = VoucherStatus.Paid;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "VoucherStatusPaid"), voucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Reconciled()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Status = VoucherStatus.Reconciled;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "VoucherStatusReconciled"), voucherVM.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Voided()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Status = VoucherStatus.Voided;
            voucherVM = new VoucherViewModel(voucherDto);

            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "VoucherStatusVoided"), voucherVM.StatusDescription);
        }

        #endregion

        #region SetApprovers tests
        [TestMethod]
        public void SetApprovers_Success()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetApprovers(voucherDto.Approvers);

            Assert.AreEqual(voucherDto.Approvers.Count(), voucherVM.Approvers.Count(), "The approvers lists should be the same size.");
        }

        [TestMethod]
        public void SetApprovers_CallTwice()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetApprovers(voucherDto.Approvers);
            voucherVM.SetApprovers(voucherDto.Approvers);

            Assert.AreEqual(voucherDto.Approvers.Count(), voucherVM.Approvers.Count(), "The approvers lists should be the same size.");
        }

        [TestMethod]
        public void SetApprovers_NullApproverInList()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Approvers = new List<Approver>();
            voucherDto.Approvers.Add(null);

            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetApprovers(voucherDto.Approvers);

            Assert.IsTrue(voucherVM.Approvers.Count() == 0, "The approvers list should have 0 items.");
        }

        [TestMethod]
        public void SetApprovers_ApproversListIsNull()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.Approvers = null;

            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetApprovers(voucherDto.Approvers);

            Assert.IsTrue(voucherVM.Approvers.Count() == 0, "The approvers list should be 0.");
        }
        #endregion

        #region SetLineItems tests
        [TestMethod]
        public void SetLineItems_Success()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetLineItems(voucherDto.LineItems);

            Assert.AreEqual(voucherDto.LineItems.Count(), voucherVM.LineItems.Count(), "The line items lists should be the same size.");

            // Each line item should have sequential IDs starting at 1
            int id = 1;
            foreach (var lineItem in voucherVM.LineItems)
            {
                Assert.AreEqual("document-line-item" + id.ToString(), lineItem.DocumentItemId, "The document item ID should match.");
                id++;
            }
        }

        [TestMethod]
        public void SetLineItems_CallTwice()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetLineItems(voucherDto.LineItems);
            voucherVM.SetLineItems(voucherDto.LineItems);

            Assert.AreEqual(voucherDto.LineItems.Count(), voucherVM.LineItems.Count(), "The line items lists should be the same size.");
        }

        [TestMethod]
        public void SetLineItems_NullLineItemInList()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.LineItems = new List<LineItem>();
            voucherDto.LineItems.Add(null);

            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetLineItems(voucherDto.LineItems);

            Assert.IsTrue(voucherVM.LineItems.Count() == 0, "The line items list should have 0 items.");
        }

        [TestMethod]
        public void SetLineItems_LineItemsListIsNull()
        {
            voucherDto = documentRepository.GetVoucher();
            voucherDto.LineItems = null;

            voucherVM = new VoucherViewModel(voucherDto);
            voucherVM.SetLineItems(voucherDto.LineItems);

            Assert.IsTrue(voucherVM.LineItems.Count() == 0, "The line items list should be 0.");
        }
        #endregion
    }
}
