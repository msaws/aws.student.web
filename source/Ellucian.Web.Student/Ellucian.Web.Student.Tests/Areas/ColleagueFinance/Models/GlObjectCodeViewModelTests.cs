﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class GlObjectCodeViewModelTests
    {
        [TestMethod]
        public void ConstructorUnderBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "",
                Name = "",
                TotalActuals = 100m,
                TotalBudget = 200m,
                TotalEncumbrances = 50m,
                Pools = new List<GlObjectCodeBudgetPool>()
            };

            var vm = new GlObjectCodeViewModel(dto);

            Assert.AreEqual(dto.GlClass, vm.GlClass);
            Assert.AreEqual(dto.Id, vm.Id);
            Assert.AreEqual(dto.TotalBudget.ToString("C"), vm.TotalBudget);
            Assert.AreEqual(dto.TotalActuals.ToString("C"), vm.TotalActuals);
            Assert.AreEqual(dto.TotalEncumbrances.ToString("C"), vm.TotalEncumbrances);
            Assert.AreEqual(dto.Name, vm.Name);

            Assert.AreEqual((dto.TotalBudget - (dto.TotalActuals + dto.TotalEncumbrances)).ToString("C"), vm.TotalBudgetRemaining);

            if (dto.TotalBudget == 0)
            {
                if (dto.TotalActuals + dto.TotalEncumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.TotalPercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.TotalActuals + dto.TotalEncumbrances) / dto.TotalBudget) * 100)) + " %", vm.TotalPercentProcessed);
            }


            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorNearingBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "",
                Name = "",
                TotalActuals = 100m,
                TotalBudget = 200m,
                TotalEncumbrances = 75m,
                Pools = new List<GlObjectCodeBudgetPool>()
            };

            var vm = new GlObjectCodeViewModel(dto);

            Assert.AreEqual(dto.GlClass, vm.GlClass);
            Assert.AreEqual(dto.Id, vm.Id);
            Assert.AreEqual(dto.TotalBudget.ToString("C"), vm.TotalBudget);
            Assert.AreEqual(dto.TotalActuals.ToString("C"), vm.TotalActuals);
            Assert.AreEqual(dto.TotalEncumbrances.ToString("C"), vm.TotalEncumbrances);
            Assert.AreEqual(dto.Name, vm.Name);

            Assert.AreEqual((dto.TotalBudget - (dto.TotalActuals + dto.TotalEncumbrances)).ToString("C"), vm.TotalBudgetRemaining);

            if (dto.TotalBudget == 0)
            {
                if (dto.TotalActuals + dto.TotalEncumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.TotalPercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.TotalActuals + dto.TotalEncumbrances) / dto.TotalBudget) * 100)) + " %", vm.TotalPercentProcessed);
            }


            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorOverBudget()
        {
            var dto = new GlObjectCode()
                {
                    GlAccounts = new List<GlObjectCodeGlAccount>(),
                    GlClass = GlClass.Expense,
                    Id = "",
                    Name = "",
                    TotalActuals = 100m,
                    TotalBudget = 200m,
                    TotalEncumbrances = 250m,
                    Pools = new List<GlObjectCodeBudgetPool>()
                };

            var vm = new GlObjectCodeViewModel(dto);

            Assert.AreEqual(dto.GlClass, vm.GlClass);
            Assert.AreEqual(dto.Id, vm.Id);
            Assert.AreEqual(dto.TotalBudget.ToString("C"), vm.TotalBudget);
            Assert.AreEqual(dto.TotalActuals.ToString("C"), vm.TotalActuals);
            Assert.AreEqual(dto.TotalEncumbrances.ToString("C"), vm.TotalEncumbrances);
            Assert.AreEqual(dto.Name, vm.Name);

            Assert.AreEqual((dto.TotalBudget - (dto.TotalActuals + dto.TotalEncumbrances)).ToString("C"), vm.TotalBudgetRemaining);

            if (dto.TotalBudget == 0)
            {
                if (dto.TotalActuals + dto.TotalEncumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.TotalPercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.TotalActuals + dto.TotalEncumbrances) / dto.TotalBudget) * 100)) + " %", vm.TotalPercentProcessed);
            }


            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorOverBudgetRevenue()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Revenue,
                Id = "",
                Name = "",
                TotalActuals = -100m,
                TotalBudget = -200m,
                TotalEncumbrances = -250m,
                Pools = new List<GlObjectCodeBudgetPool>()
            };

            var vm = new GlObjectCodeViewModel(dto);

            var bud = dto.TotalBudget * -1;
            var act = dto.TotalActuals * -1;
            var enc = dto.TotalEncumbrances * -1;

            Assert.AreEqual(dto.GlClass, vm.GlClass);
            Assert.AreEqual(dto.Id, vm.Id);
            Assert.AreEqual(bud.ToString("C"), vm.TotalBudget);
            Assert.AreEqual(act.ToString("C"), vm.TotalActuals);
            Assert.AreEqual(enc.ToString("C"), vm.TotalEncumbrances);
            Assert.AreEqual(dto.Name, vm.Name);

            Assert.AreEqual((bud - (act + enc)).ToString("C"), vm.TotalBudgetRemaining);

            if (bud == 0)
            {
                if (act + enc == 0)
                {
                    Assert.AreEqual("0 %", vm.TotalPercentProcessed);
                }
                else if ((act + enc) > 0)
                {
                    Assert.AreEqual("101 %", vm.TotalPercentProcessed);
                }
                else if ((act + enc) < 0)
                {
                    Assert.AreEqual("-101 %", vm.TotalPercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((act + enc) / bud) * 100)) + " %", vm.TotalPercentProcessed);
            }


            if ((act + enc) > bud)
            {
                Assert.AreEqual("remaining-green", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }
        }

        [TestMethod]
        public void ConstructorPositiveExpenseWithNoBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "",
                Name = "",
                TotalActuals = 100m,
                TotalBudget = 0m,
                TotalEncumbrances = 250m,
                Pools = new List<GlObjectCodeBudgetPool>()
            };

            var vm = new GlObjectCodeViewModel(dto);

            Assert.AreEqual(dto.GlClass, vm.GlClass);
            Assert.AreEqual(dto.Id, vm.Id);
            Assert.AreEqual(dto.TotalBudget.ToString("C"), vm.TotalBudget);
            Assert.AreEqual(dto.TotalActuals.ToString("C"), vm.TotalActuals);
            Assert.AreEqual(dto.TotalEncumbrances.ToString("C"), vm.TotalEncumbrances);
            Assert.AreEqual(dto.Name, vm.Name);

            Assert.AreEqual((dto.TotalBudget - (dto.TotalActuals + dto.TotalEncumbrances)).ToString("C"), vm.TotalBudgetRemaining);

            if (dto.TotalBudget == 0)
            {
                if (dto.TotalActuals + dto.TotalEncumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.TotalPercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.TotalActuals + dto.TotalEncumbrances) / dto.TotalBudget) * 100)) + " %", vm.TotalPercentProcessed);
            }


            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorNegativeExpenseWithNoBudget()
        {
            var dto = new GlObjectCode()
            {
                GlAccounts = new List<GlObjectCodeGlAccount>(),
                GlClass = GlClass.Expense,
                Id = "",
                Name = "",
                TotalActuals = -100m,
                TotalBudget = 0m,
                TotalEncumbrances = 0m,
                Pools = new List<GlObjectCodeBudgetPool>()
            };

            var vm = new GlObjectCodeViewModel(dto);

            Assert.AreEqual(dto.GlClass, vm.GlClass);
            Assert.AreEqual(dto.Id, vm.Id);
            Assert.AreEqual(dto.TotalBudget.ToString("C"), vm.TotalBudget);
            Assert.AreEqual(dto.TotalActuals.ToString("C"), vm.TotalActuals);
            Assert.AreEqual(dto.TotalEncumbrances.ToString("C"), vm.TotalEncumbrances);
            Assert.AreEqual(dto.Name, vm.Name);

            Assert.AreEqual((dto.TotalBudget - (dto.TotalActuals + dto.TotalEncumbrances)).ToString("C"), vm.TotalBudgetRemaining);

            if (dto.TotalBudget == 0)
            {
                if (dto.TotalActuals + dto.TotalEncumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.TotalPercentProcessed);
                }
                else if ((dto.TotalActuals + dto.TotalEncumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.TotalPercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.TotalActuals + dto.TotalEncumbrances) / dto.TotalBudget) * 100)) + " %", vm.TotalPercentProcessed);
            }


            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.TotalActuals + dto.TotalEncumbrances) > dto.TotalBudget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }
    }
}
