﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class GlObjectCodeGlAccountViewModelTests
    {
        [TestMethod]
        public void ConstructorWithoutFinancialHealthIndicator()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense);

            Assert.AreEqual(dto.Budget.ToString("C"), vm.Budget);
            Assert.AreEqual(dto.Actuals.ToString("C"), vm.Actuals);
            Assert.AreEqual(dto.Encumbrances.ToString("C"), vm.Encumbrances);
            Assert.AreEqual((dto.Budget - (dto.Actuals + dto.Encumbrances)).ToString("C"), vm.BudgetRemaining);

            if (dto.Budget == 0)
            {
                if (dto.Actuals + dto.Encumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.PercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.Actuals + dto.Encumbrances) / dto.Budget) * 100)) + " %", vm.PercentProcessed);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            Assert.AreEqual(string.Empty, vm.FinancialHealthIndicator);
            Assert.AreEqual(string.Empty, vm.FinancialHealthIndicatorText);


            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ExpenseLabel"), vm.FormattedGlClass);
        }

        [TestMethod]
        public void ConstructorWithUnderBudget()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense, true);

            Assert.AreEqual(dto.Budget.ToString("C"), vm.Budget);
            Assert.AreEqual(dto.Actuals.ToString("C"), vm.Actuals);
            Assert.AreEqual(dto.Encumbrances.ToString("C"), vm.Encumbrances);
            Assert.AreEqual((dto.Budget - (dto.Actuals + dto.Encumbrances)).ToString("C"), vm.BudgetRemaining);
            Assert.AreEqual("non-pooled-account", vm.PoolTypeCssClass);

            if (dto.Budget == 0)
            {
                if (dto.Actuals + dto.Encumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.PercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.Actuals + dto.Encumbrances) / dto.Budget) * 100)) + " %", vm.PercentProcessed);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.Actuals + dto.Encumbrances) / dto.Budget > 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorWithNearBudget()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 100,
                Actuals = 50,
                Encumbrances = 45,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense, true);

            Assert.AreEqual(dto.Budget.ToString("C"), vm.Budget);
            Assert.AreEqual(dto.Actuals.ToString("C"), vm.Actuals);
            Assert.AreEqual(dto.Encumbrances.ToString("C"), vm.Encumbrances);
            Assert.AreEqual((dto.Budget - (dto.Actuals + dto.Encumbrances)).ToString("C"), vm.BudgetRemaining);

            if (dto.Budget == 0)
            {
                if (dto.Actuals + dto.Encumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.PercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.Actuals + dto.Encumbrances) / dto.Budget) * 100)) + " %", vm.PercentProcessed);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.Actuals + dto.Encumbrances) > dto.Budget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorWithOverBudget()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 100,
                Actuals = 50,
                Encumbrances = 55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense, true);

            Assert.AreEqual(dto.Budget.ToString("C"), vm.Budget);
            Assert.AreEqual(dto.Actuals.ToString("C"), vm.Actuals);
            Assert.AreEqual(dto.Encumbrances.ToString("C"), vm.Encumbrances);
            Assert.AreEqual((dto.Budget - (dto.Actuals + dto.Encumbrances)).ToString("C"), vm.BudgetRemaining);

            if (dto.Budget == 0)
            {
                if (dto.Actuals + dto.Encumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.PercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.Actuals + dto.Encumbrances) / dto.Budget) * 100)) + " %", vm.PercentProcessed);
            }


            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.Actuals + dto.Encumbrances) > dto.Budget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorWithOverBudgetRevenue()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = -100,
                Actuals = -50,
                Encumbrances = -55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Revenue, false);

            var bud = dto.Budget * -1;
            var act = dto.Actuals * -1;
            var enc = dto.Encumbrances * -1;

            Assert.AreEqual(bud.ToString("C"), vm.Budget);
            Assert.AreEqual(act.ToString("C"), vm.Actuals);
            Assert.AreEqual(enc.ToString("C"), vm.Encumbrances);
            Assert.AreEqual((bud - (act + enc)).ToString("C"), vm.BudgetRemaining);

            if (bud == 0)
            {
                if (act + enc == 0)
                {
                    Assert.AreEqual("0 %", vm.PercentProcessed);
                }
                else if ((act + enc) > 0)
                {
                    Assert.AreEqual("101 %", vm.PercentProcessed);
                }
                else if ((act + enc) < 0)
                {
                    Assert.AreEqual("-101 %", vm.PercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((act + enc) / bud) * 100)) + " %", vm.PercentProcessed);
            }


            if ((act + enc) > bud)
            {
                Assert.AreEqual("remaining-green", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "RevenueLabel"), vm.FormattedGlClass);
        }

        [TestMethod]
        public void ConstructorWithNegativeExpensesAndNoBudget()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = -55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense, true);

            Assert.AreEqual(dto.Budget.ToString("C"), vm.Budget);
            Assert.AreEqual(dto.Actuals.ToString("C"), vm.Actuals);
            Assert.AreEqual(dto.Encumbrances.ToString("C"), vm.Encumbrances);
            Assert.AreEqual((dto.Budget - (dto.Actuals + dto.Encumbrances)).ToString("C"), vm.BudgetRemaining);

            if (dto.Budget == 0)
            {
                if (dto.Actuals + dto.Encumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.PercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.Actuals + dto.Encumbrances) / dto.Budget) * 100)) + " %", vm.PercentProcessed);
            }


            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.Actuals + dto.Encumbrances) > dto.Budget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ConstructorWithPositiveExpensesAndNoBudget()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = 55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense, true);

            Assert.AreEqual(dto.Budget.ToString("C"), vm.Budget);
            Assert.AreEqual(dto.Actuals.ToString("C"), vm.Actuals);
            Assert.AreEqual(dto.Encumbrances.ToString("C"), vm.Encumbrances);
            Assert.AreEqual((dto.Budget - (dto.Actuals + dto.Encumbrances)).ToString("C"), vm.BudgetRemaining);

            if (dto.Budget == 0)
            {
                if (dto.Actuals + dto.Encumbrances == 0)
                {
                    Assert.AreEqual("0 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) > 0)
                {
                    Assert.AreEqual("101 %", vm.PercentProcessed);
                }
                else if ((dto.Actuals + dto.Encumbrances) < 0)
                {
                    Assert.AreEqual("-101 %", vm.PercentProcessed);
                }
            }
            else
            {
                Assert.AreEqual(string.Format("{0:n0}", Math.Round(((dto.Actuals + dto.Encumbrances) / dto.Budget) * 100)) + " %", vm.PercentProcessed);
            }


            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("remaining-red", vm.ColorCodeStyle);
            }
            else
            {
                Assert.AreEqual("remaining-black", vm.ColorCodeStyle);
            }

            if ((dto.Actuals + dto.Encumbrances) > dto.Budget)
            {
                Assert.AreEqual("group-icon-red", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed"), vm.FinancialHealthIndicatorText);
            }
            else if ((dto.Actuals + dto.Encumbrances) > dto.Budget * 0.85m)
            {
                Assert.AreEqual("group-icon-yellow", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow"), vm.FinancialHealthIndicatorText);
            }
            else
            {
                Assert.AreEqual("group-icon-green", vm.FinancialHealthIndicator);
                Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen"), vm.FinancialHealthIndicatorText);
            }
        }

        [TestMethod]
        public void ZeroOutBudgetActualsEncumbrances()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = 55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense, true);
            vm.ZeroOutBudgetActualsEncumbrances();

            decimal zero = 0m;
            Assert.AreEqual(zero.ToString("C"), vm.Budget);
            Assert.AreEqual(zero.ToString("C"), vm.Actuals);
            Assert.AreEqual(zero.ToString("C"), vm.Encumbrances);

        }

        [TestMethod]
        public void GlClassAsset()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = 55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Asset, true);

            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "AssetLabel"), vm.FormattedGlClass);

        }

        [TestMethod]
        public void GlClassLiability()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = 55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_21111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.Liability, true);

            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "LiabilityLabel"), vm.FormattedGlClass);

        }

        [TestMethod]
        public void GlClassFundBalance()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = 55,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_31111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var vm = new GlObjectCodeGlAccountViewModel(dto, GlClass.FundBalance, true);

            Assert.AreEqual(GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "FundBalanceLabel"), vm.FormattedGlClass);

        }

        [TestMethod]
        public void Budget_NonpooledGlAccount()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25,
                PoolType = GlBudgetPoolType.None,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var viewModel = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense);
            Assert.AreEqual(dto.Budget.ToString("C"), viewModel.Budget);
        }

        [TestMethod]
        public void Budget_UmbrellaGlAccount()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25,
                PoolType = GlBudgetPoolType.Umbrella,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var viewModel = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense);
            Assert.AreEqual(dto.Budget.ToString("C"), viewModel.Budget);
        }

        [TestMethod]
        public void Budget_PooleeGlAccount_NonZeroBudget()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 100,
                Actuals = 50,
                Encumbrances = 25,
                PoolType = GlBudgetPoolType.Poolee,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var viewModel = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense);
            Assert.AreEqual(dto.Budget.ToString("C"), viewModel.Budget);
        }

        [TestMethod]
        public void Budget_PooleeGlAccount_ZeroBudget()
        {
            GlObjectCodeGlAccount dto = new GlObjectCodeGlAccount()
            {
                Budget = 0,
                Actuals = 50,
                Encumbrances = 25,
                PoolType = GlBudgetPoolType.Poolee,
                GlAccountNumber = "11_11_11_11111_11111",
                FormattedGlAccount = "11-11-11-11111-11111",
                Description = "Test non-pooled gl account"
            };

            var viewModel = new GlObjectCodeGlAccountViewModel(dto, GlClass.Expense);
            Assert.AreEqual("", viewModel.Budget);
        }
    }
}
