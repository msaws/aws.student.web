﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.ColleagueFinance.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ColleagueFinance.Models
{
    [TestClass]
    public class BlanketPurchaseOrderGlDistributionViewModelTests
    {
        #region Initialize and Cleanup
        public TestDocumentDtoRepository documentRepository;
        public BlanketPurchaseOrderGlDistributionViewModel BpoGlDistributionVM;

        [TestInitialize]
        public void Initialize()
        {
            documentRepository = new TestDocumentDtoRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            documentRepository = null;
            BpoGlDistributionVM = null;
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void Constructor()
        {
            var glDistributionDto = documentRepository.GetBlanketPurchaseOrder().GlDistributions.FirstOrDefault();
            BpoGlDistributionVM = new BlanketPurchaseOrderGlDistributionViewModel(glDistributionDto);

            Assert.AreEqual(glDistributionDto.GlAccount, BpoGlDistributionVM.GlAccount, "Gl Accounts should match.");
            Assert.AreEqual(glDistributionDto.Description, BpoGlDistributionVM.GlAccountDescription, "Gl Account descriptions should match.");
            Assert.AreEqual(glDistributionDto.FormattedGlAccount, BpoGlDistributionVM.FormattedGlAccount, "Formatted GL Accounts should match.");
            Assert.AreEqual(glDistributionDto.ProjectNumber, BpoGlDistributionVM.ProjectNumber, "Project number should match.");
            Assert.AreEqual(glDistributionDto.ProjectLineItemCode, BpoGlDistributionVM.ProjectLineItemCode, "Line Item code should match.");
            Assert.AreEqual(glDistributionDto.EncumberedAmount.ToString("C"), BpoGlDistributionVM.EncumberedAmount, "Encumbered amount should match and be formatted.");
            Assert.AreEqual(glDistributionDto.ExpensedAmount.ToString("C"), BpoGlDistributionVM.ExpensedAmount, "Expensed amount should match and be formatted.");
        }

        [TestMethod]
        public void ReminaingAmount()
        {
            var glDistributionDto = documentRepository.GetBlanketPurchaseOrder().GlDistributions.FirstOrDefault();
            BpoGlDistributionVM = new BlanketPurchaseOrderGlDistributionViewModel(glDistributionDto);

            Assert.AreEqual((glDistributionDto.EncumberedAmount - glDistributionDto.ExpensedAmount).ToString("C"), BpoGlDistributionVM.RemainingAmount, "Remaining amount should be the difference between encumbered and expensed amounts.");
        }
        #endregion
    }
}
