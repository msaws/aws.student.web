﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("transcript.requests.js", function () {
    var contentType;

    beforeAll(function () {
        transcriptRequestsViewModelInstance = new transcriptRequestsViewModel();
        jsonContentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        getTranscriptRequestsInfoUrl = "getTranscriptInfoUrl";
        Ellucian = {};
        Ellucian.Student = {};
        Ellucian.Student.TranscriptRequests = {
            transcriptInfoGetFailure: "Failure",
            newTranscriptRequestSuccessMessage: "Success!",
            unableToSubmitNewTranscriptRequestMessage: "Unable to submit"
            };
        notify = "Success";
    });

    beforeEach(function () {
        contentType = "json";
    });

    it(":ajax call uses correct url", function () {
        spyOn($, "ajax");
        getTranscriptRequestsInfo();
        expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual("getTranscriptInfoUrl");
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getTranscriptRequestsInfo();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":success callback sets isTrancriptRequest flag to true", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        spyOn(transcriptRequestsViewModelInstance, "isTranscriptRequest");
        getTranscriptRequestsInfo();
        expect(transcriptRequestsViewModelInstance.isTranscriptRequest).toHaveBeenCalledWith(true);
    });

    it(":on exception in success callback correct message is sent to the notification center", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        transcriptRequestsViewModelInstance.AddressInfo(null);
        spyOn($.fn, "notificationCenter");
        var secondArg = {
            message: "Failure",
            type: "error"
        };
        getTranscriptRequestsInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":error callback sends correct message to the notification center", function () {
        var obj = { status: 500 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(obj, {}, {});
        });        
        spyOn($.fn, "notificationCenter");
        var secondArg = {
            message: "Failure",
            type: "error"
        };
        getTranscriptRequestsInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":complete callback triggers checkForMobile function", function () {        
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });        
        spyOn(transcriptRequestsViewModelInstance, "checkForMobile");
        getTranscriptRequestsInfo();
        expect(transcriptRequestsViewModelInstance.checkForMobile).toHaveBeenCalled();
    });

    it(":complete callback send expected message to the notification center when notify = Failure", function () {
        notify = "Failure";
        spyOn($, "ajax").and.callFake(function (e) {
            e.error({}, {});
        });
        spyOn($.fn, "notificationCenter");
        var secondArg = {
            message: "Failure",
            type: "error"
        };
        getTranscriptRequestsInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });
});