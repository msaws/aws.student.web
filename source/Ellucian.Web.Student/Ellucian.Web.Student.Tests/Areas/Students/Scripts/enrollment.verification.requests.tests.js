﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("enrollment.verification.requests.js", function () {
    var contentType;

    beforeAll(function () {
        enrollmentVerificationRequestsViewModelInstance = new enrollmentVerificationRequestsViewModel();
        jsonContentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        getEnrollmentVerificationRequestsInfoUrl = "getEnrollmentRequestInfoUrl";

        Ellucian = Ellucian|| {};
        Ellucian.Student =Ellucian|| {};
        Ellucian.Student.Requests =  Ellucian.Student.Requests || {};
        Ellucian.Student.Requests.enrollmentVerificationInfoGetFailure = "Failure";
        Ellucian.Student.Requests.enrollmentVerificationRequestSubmissionSuccess = "Success!";
        Ellucian.Student.Requests.enrollmentVerificationRequestSubmissionFailure = "Unable to submit";
        notify = "Success";
    });

    beforeEach(function () {
        contentType = "json";
    });

    it(":ajax call uses correct url", function () {
        spyOn($, "ajax");
        getEnrollmentVerificationRequestsInfo();
        expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual("getEnrollmentRequestInfoUrl");
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getEnrollmentVerificationRequestsInfo();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":on exception in success callback correct message is sent to the notification center", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });
        enrollmentVerificationRequestsViewModelInstance.AddressInfo(null);
        spyOn($.fn, "notificationCenter");
        var secondArg = {
            message: "Failure",
            type: "error"
        };
        getEnrollmentVerificationRequestsInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":error callback sends correct message to the notification center", function () {
        var obj = { status: 500 };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(obj, {}, {});
        });        
        spyOn($.fn, "notificationCenter");
        var secondArg = {
            message: "Failure",
            type: "error"
        };
        getEnrollmentVerificationRequestsInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });

    it(":complete callback send expected message to the notification center when notify = Failure", function () {
        notify = "Failure";
        spyOn($, "ajax").and.callFake(function (e) {
            e.error({}, {});
        });
        spyOn($.fn, "notificationCenter");
        var secondArg = {
            message: "Failure",
            type: "error"
        };
        getEnrollmentVerificationRequestsInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', secondArg);
    });
});