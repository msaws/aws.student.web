﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

var testData = function () {

    return {

        transcriptRequestsData: {
            TranscriptType: ko.observable({
                Id: ko.observable("Undegraduate")
            }),
            TranscriptHoldType: ko.observable({
                Code: ko.observable("Process immediately")
            })
        }
    }
}