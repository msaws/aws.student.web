﻿describe("graduationViewModel", function () {
    var viewModel;
    var global = jasmine.getGlobal();

    beforeAll(function () {
        Ellucian =Ellucian|| {};
        Ellucian.Student =Ellucian.Student|| {};
        Ellucian.Student.Graduation =Ellucian.Student.Graduation|| {};
        Ellucian.Student.Graduation.ActionUrls = {
            getPreviousGraduationInfoUrl: "prevInfoUrl",
        graduationInfoUrl : "#",
        submitGraduationApplicationUrl : "#",
        updateGraduationApplicationUrl : "updateApplicationUrl/",
        addStudentGraduationInfoUrl : "addStudentGraduationUrl/"
    };
        mapPaymentReviewActionUrl = "#";
        gradView = {
            Programs: ko.observable([
                {Code: ko.observable("program1")}
            ])
        };
    });

    beforeEach(function () {
        viewModel = new GraduationViewModel();
        viewModel.GraduationTermSelected = "2015/FA";
        viewModel.isFormDirty(false);        
        
        ko.validation.init({
            messagesOnModified: true,
            insertMessages: false,
            decorateInputElement: false
        });
        ko.validation.configure({
            messagesOnModified: true,
            insertMessages: true
        });
        viewModel.errors = ko.validation.group(viewModel);
    });
    afterEach(function () {
        viewModel = null;
    })
    
    // Check defaults?
    it(":isLoaded initializes to false", function () {
        expect(viewModel.isLoaded()).toBe(false);
    });

    it(":isNewPreferredAddress initializes to false", function () {
        expect(viewModel.isNewPreferredAddress()).toBe(false);
    });

    it(":hasAddressChanged initializes to false", function () {
        expect(viewModel.hasAddressChanged()).toBe(false);
    });

    it(":OverrideCapAndGownDisplay initializes to false", function () {
        expect(viewModel.OverrideCapAndGownDisplay()).toBe(false);
    });

    it(":OverrideCapAndGownDisplay sets ShowCapAndGown to true", function () {
        viewModel.OverrideCapAndGownDisplay(true);
        viewModel.AttendingCommencement(false);
        viewModel.ShowAttendCommencement(true);

        viewModel.CapAndGownLink(true);
        viewModel.ShowCapSize(false);
        viewModel.ShowGownSize(false);

        expect(viewModel.ShowCapAndGown()).toBe(true);
    });

    it(":OverrideCapAndGownDisplay sets ShowCapAndGown to false", function () {
        viewModel.OverrideCapAndGownDisplay(false);
        viewModel.AttendingCommencement(false);
        viewModel.ShowAttendCommencement(true);

        viewModel.CapAndGownLink(true);
        viewModel.ShowCapSize(false);
        viewModel.ShowGownSize(false);

        expect(viewModel.ShowCapAndGown()).toBe(false);
    });

    //Check Validation
    //ValidDiplomaName
    it(":ValidDiplomaName returns false when string is empty when diploma name is required and is not hiddden", function () {
        viewModel.RequireDiplomaName(true);
        viewModel.ShowDiplomaName(true);
        viewModel.DiplomaName("");
        expect(viewModel.DiplomaName.isValid()).toBe(false)
    });
    it(":ValidDiplomaName returns false when string is only whitespace when diploma name is required and is not hiddden", function () {
        viewModel.RequireDiplomaName(true);
        viewModel.ShowDiplomaName(true);
        viewModel.DiplomaName("         ");
        expect(viewModel.DiplomaName.isValid()).toBe(false)
    });
    it(":ValidDiplomaName returns true when name entered", function () {
        viewModel.RequireDiplomaName(true);
        viewModel.ShowDiplomaName(true);
        viewModel.DiplomaName("Jane");
        expect(viewModel.DiplomaName.isValid()).toBe(true)
    });
    it(":ValidDiplomaName returns true when two names entered", function () {
        viewModel.RequireDiplomaName(true);
        viewModel.ShowDiplomaName(true);
        viewModel.DiplomaName("Jane Smith");
        expect(viewModel.DiplomaName.isValid()).toBe(true)
    });
    it(":ValidDiplomaName returns true with a leading space", function () {
        viewModel.RequireDiplomaName(true);
        viewModel.ShowDiplomaName(true);
        viewModel.DiplomaName(" The Batman");
        expect(viewModel.DiplomaName.isValid()).toBe(true)
    });

    //Valid Phonetic Spelling
    it(":ValidPhoneticSpelling returns false when empty and phonetice spelling is required and also visible", function () {
        viewModel.RequirePhoneticSpelling(true);
        viewModel.ShowPhoneticSpelling(true);
        viewModel.PhoneticSpellingOfName("");
        expect(viewModel.PhoneticSpellingOfName.isValid()).toBe(false)
    });
    it(":ValidPhoneticSpelling returns false when string is only whitespace", function () {
        viewModel.RequirePhoneticSpelling(true);
        viewModel.ShowPhoneticSpelling(true);
        viewModel.PhoneticSpellingOfName("         ");
        expect(viewModel.PhoneticSpellingOfName.isValid()).toBe(false)
    });
    it(":ValidPhoneticSpelling returns true when name entered", function () {
        viewModel.RequirePhoneticSpelling(true);
        viewModel.ShowPhoneticSpelling(true);
        viewModel.PhoneticSpellingOfName("Jane");
        expect(viewModel.PhoneticSpellingOfName.isValid()).toBe(true)
    });
    it(":ValidPhoneticSpelling returns true when two name entered", function () {
        viewModel.RequirePhoneticSpelling(true);
        viewModel.ShowPhoneticSpelling(true);
        viewModel.PhoneticSpellingOfName("Jane Smith");
        expect(viewModel.PhoneticSpellingOfName.isValid()).toBe(true)
    });
    it(":ValidPhoneticSpelling returns true with a leading space", function () {
        viewModel.RequirePhoneticSpelling(true);
        viewModel.ShowPhoneticSpelling(true);
        viewModel.PhoneticSpellingOfName(" The Batman");
        expect(viewModel.PhoneticSpellingOfName.isValid()).toBe(true)
    });

    //Valid Number Guests
    it(":ValidNumberGuests returns false when empty and required and visble", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.RequireNumberGuests(true);
        viewModel.ShowNumberGuests(true);
        viewModel.NumberOfGuests("");
        expect(viewModel.NumberOfGuests.isValid()).toBe(false)
    });
    it(":ValidNumberGuests returns false when only whitespace and required and visible", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.RequireNumberGuests(true);
        viewModel.ShowNumberGuests(true);
        viewModel.NumberOfGuests(" ");
        expect(viewModel.NumberOfGuests.isValid()).toBe(false)
    });
    it(":ValidNumberGuests returns false when not a number", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.NumberOfGuests("0a");
        expect(viewModel.NumberOfGuests.isValid()).toBe(false)
    });
    it(":ValidNumberGuests returns false when negative", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.NumberOfGuests("-1");
        expect(viewModel.NumberOfGuests.isValid()).toBe(false)
    });
    it(":ValidNumberGuests returns false when larger than max", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.ShowNumberGuests(true);
        viewModel.NumberOfGuests("55");
        expect(viewModel.NumberOfGuests.isValid()).toBe(false)
    });

    it(":ValidNumberGuests returns true when 0", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.NumberOfGuests(0);
        expect(viewModel.NumberOfGuests.isValid()).toBe(true)
    });
    it(":ValidNumberGuests returns true when max", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.NumberOfGuests("10");
        expect(viewModel.NumberOfGuests.isValid()).toBe(true)
    });
    it(":ValidNumberGuests returns true when in range", function () {
        viewModel.MaximumCommencementGuests(10);
        viewModel.NumberOfGuests("5");
        expect(viewModel.NumberOfGuests.isValid()).toBe(true)
    });
    it(":ValidNumberGuests returns false when not whole", function () {
        viewModel.ShowNumberGuests(true);
        viewModel.MaximumCommencementGuests(10);
        viewModel.NumberOfGuests("1.5");
        expect(viewModel.NumberOfGuests.isValid()).toBe(false)
    });

    //Valid HomeTown
    it(":ValidHometown returns false when empty only when hometown is required and visible", function () {
        viewModel.RequireHometown(true);
        viewModel.ShowHometown(true);
        viewModel.Hometown("");
        expect(viewModel.Hometown.isValid()).toBe(false)
    });
    it(":ValidHometown returns false when whitespace only when hometown is required and visible", function () {
        viewModel.RequireHometown(true);
        viewModel.ShowHometown(true);
        viewModel.Hometown("         ");
        expect(viewModel.Hometown.isValid()).toBe(false)
    });
    it(":ValidHometown returns true if one name", function () {
        viewModel.Hometown("Jane");
        expect(viewModel.Hometown.isValid()).toBe(true)
    });
    it(":ValidHometown returns true if two names", function () {
        viewModel.Hometown("Jane Smith");
        expect(viewModel.Hometown.isValid()).toBe(true)
    });
    it(":ValidHometown returns true with leading space", function () {
        viewModel.Hometown(" The Batman");
        expect(viewModel.Hometown.isValid()).toBe(true)
    });

    //Valid Street1
    it(":ValidStreet1 returns false when empty", function () {
        viewModel.AddressInfo().Street1("");
        viewModel.existingAddress(false);
        viewModel.WillPickupDiploma(false);
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(false)
    });
    it(":ValidStreet1 returns false when only whitespace", function () {
        viewModel.AddressInfo().Street1("         ");
        viewModel.existingAddress(false);
        viewModel.WillPickupDiploma(false);
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(false)
    });
    it(":ValidStreet1 returns true one word", function () {
        viewModel.AddressInfo().Street1("Jane");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });
    it(":ValidStreet1 returns true leading numbers", function () {
        viewModel.AddressInfo().Street1("123 Jane");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });
    it(":ValidStreet1 returns true multiple words", function () {
        viewModel.AddressInfo().Street1("Jane Smith");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });
    it(":ValidStreet1 returns true leading space", function () {
        viewModel.AddressInfo().Street1(" The Batman");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });


    //Require Address
    it(":Require address is false when choosing an existing Address", function () {
        viewModel.existingAddress(true);
        expect(viewModel.RequireAddress()).toBe(false)
    });
    it(":ValidHometown returns false on whitespace when hometown is required and visible", function () {
        viewModel.ShowHometown(true);
        viewModel.RequireHometown(true);
        viewModel.Hometown("         ");
        expect(viewModel.Hometown.isValid()).toBe(false)
    });

    //If Address Changes hasAddressChanged should be true
    it(":hasAddressChanged should be true if an Address Street1 field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().Street1("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address Street2 field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().Street2("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address Street3 field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().Street3("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address Street4 field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().Street4("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address City field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().City("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address State field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().State("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address PostalCode field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().PostalCode("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address Country field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().Country("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });

    it(":hasAddressChanged should be true if an Address internationalSelected field changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AddressInfo().internationalSelected("123 Main");
        expect(viewModel.hasAddressChanged()).toBe(true);
    });    

    it(":newAddressClicked sets hasAddressChange to true when called", function () {
        spyOn(viewModel, "hasAddressChanged");

        viewModel.newAddressClicked();
        expect(viewModel.hasAddressChanged).toHaveBeenCalledWith(true);
    });

    /****     submitApp tests    ****/
    it(":submitApp calls showPayment function if status equals 200 and ShowPaymentDetailsFlag is true", function () {
        viewModel.GraduationPaymentModel = ko.observable({
            ShowPaymentDetails: ko.observable(true)
        });
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({
                StatusCode: 200
            });
        });
        spyOn(viewModel, "showPayment");
        viewModel.submitApp();
        expect(viewModel.showPayment).toHaveBeenCalled();
    });

    it(":submitApp calls showPayment function if status equals 406 and ShowPaymentDetailsFlag is true", function () {
        viewModel.GraduationPaymentModel = ko.observable({
            ShowPaymentDetails: ko.observable(true)
        });
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({
                StatusCode: 406
            });
        });
        spyOn(viewModel, "showPayment");
        viewModel.submitApp();
        expect(viewModel.showPayment).toHaveBeenCalled();
    });

    it(":submitApp sets window.location to expected value if status equals 406 and ShowPaymentDetailsFlag is false", function () {
        viewModel.GraduationPaymentModel = ko.observable({
            ShowPaymentDetails: ko.observable(false)
        });
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({
                StatusCode: 406                
            });
        });        
        viewModel.submitApp();
        expect(window.location.hash).toEqual("#" + '?Notify=editAddressError');
    });


    /***   isFormDirty gets set to true when contents change    ***/
    it(":isFormDirty is set to true if PhoneticSpellingOfName changes", function () {
        viewModel.bindingComplete(true);
        viewModel.PhoneticSpellingOfName("newSpelling");
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if DiplomaName changes", function () {
        viewModel.bindingComplete(true);
        viewModel.DiplomaName("newDiplomaName");
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if Hometown changes", function () {
        viewModel.bindingComplete(true);
        viewModel.Hometown("Fairfax");
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if CommencementSireSelected changes", function () {
        viewModel.bindingComplete(true);
        viewModel.CommencementSiteSelected(true);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if AttendingCommencement changes", function () {
        viewModel.bindingComplete(true);
        viewModel.AttendingCommencement(false);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if IncludeNameInProgram changes", function () {
        viewModel.bindingComplete(true);
        viewModel.IncludeNameInProgram(false);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if NumberOfGuests changes", function () {
        viewModel.bindingComplete(true);
        viewModel.NumberOfGuests(10);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if CapSizeSelected changes", function () {
        viewModel.bindingComplete(true);
        viewModel.CapSizeSelected(true);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if GownSizeSelected changes", function () {
        viewModel.bindingComplete(true);
        viewModel.GownSizeSelected(true);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if WillPickUpDiploma changes", function () {
        viewModel.bindingComplete(true);
        viewModel.WillPickupDiploma(false);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if existingAddress changes", function () {
        viewModel.bindingComplete(true);
        viewModel.existingAddress(true);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if MilitaryStatusSelected changes", function () {
        viewModel.bindingComplete(true);
        viewModel.MilitaryStatusSelected(true);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if PrimaryLocationSelected changes", function () {
        viewModel.bindingComplete(true);
        viewModel.PrimaryLocationSelected(true);
        expect(viewModel.isFormDirty()).toBe(true);
    });

    it(":isFormDirty is set to true if SpecialAccomodations value changes", function () {
        viewModel.bindingComplete(true);
        viewModel.SpecialAccommodations("foo");
        expect(viewModel.isFormDirty()).toBe(true);
    });

    /****  Multiple locations tests    ****/
    it(":multipleLocations retuns false if there is only one PrimaryLocation", function () {
        viewModel.PrimaryLocations([{IsSelected: ko.observable(true)}]);
        expect(viewModel.multipleLocations()).toBe(false);
    });

    it(":multipleLocations retuns false if there are no PrimaryLocations", function () {
        viewModel.PrimaryLocations([]);
        expect(viewModel.multipleLocations()).toBe(false);
    });

    it(":multipleLocations retuns true if there are more than one PrimaryLocations", function () {
        viewModel.PrimaryLocations([
            { IsSelected: ko.observable(true) },
            { IsSelected: ko.observable(false) }
        ]);
        expect(viewModel.multipleLocations()).toBe(true);
    });

    /**** Active location tests   ****/
    it(":activeLocation returns true if there is a selected primary location", function () {
        viewModel.PrimaryLocations([
            { IsSelected: ko.observable(true) }]);
        expect(viewModel.activeLocation()).toBe(true);
    });

    it(":activeLocation returns false if there is no selected primary location", function () {
        viewModel.PrimaryLocations([
            { IsSelected: ko.observable(false) },
            { IsSelected: ko.observable(false) },
            { IsSelected: ko.observable(false) }
        ]);
        expect(viewModel.activeLocation()).toBe(false);
    });

    it(":activeLocation returns false if there are no primary locations", function () {
        viewModel.PrimaryLocations([]);
        expect(viewModel.activeLocation()).toBe(false);
    });

    it(":activeLocation sets PrimaryLocationSelected if there is a selected primary location", function () {
        viewModel.PrimaryLocations([
            { IsSelected: ko.observable(false) },
            { IsSelected: ko.observable(true) },
            { IsSelected: ko.observable(false) }
        ]);
        expect(viewModel.PrimaryLocationSelected()).toBeTruthy();
    });

    /****    DisplaySpecialAccomodationsField tests    ****/
    it(":DisplaySpecialAccommodationsField returns true if showSpecialAccommodations and attendingCommencement flags are true", function () {
        viewModel.ShowSpecialAccommodations(true);
        viewModel.AttendingCommencement(true);
        expect(viewModel.DisplaySpecialAccommodationsField()).toBe(true);
    });

    it(":DisplaySpecialAccommodationsField returns true if showSpecialAccommodations flag is true and ShowAttendCommencement flag is false", function () {
        viewModel.ShowSpecialAccommodations(true);
        viewModel.ShowAttendCommencement(false);
        expect(viewModel.DisplaySpecialAccommodationsField()).toBe(true);
    });

    it(":DisplaySpecialAccommodationsField returns false if showSpecialAccommodations flag is false", function () {
        viewModel.ShowSpecialAccommodations(false);
        viewModel.ShowAttendCommencement(false);
        expect(viewModel.DisplaySpecialAccommodationsField()).toBe(false);
    });
    
    /***     updateApp tests    ****/
    it(":updateApp ajax call completes with a status 406 and location hash equals expected", function () {
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({ status: 406 });
        });

        viewModel.updateApp();
        expect(window.location.hash).toEqual("#" + '?Notify=editAddressError');
    });

    it(":window.location is set with expected value on updateApp ajax complete call with status 200", function () {
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({ status: 200 });
        });
        viewModel.updateApp();
        expect(window.location.hash).toEqual("#?Notify=editSuccess");
    });

    it(":window.location is set with expected value on updateApp ajax complete call with any status that's not 200/406", function () {
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({ status: 500 });
        });
        viewModel.updateApp();
        expect(window.location.hash).toEqual("#?Notify=editFailure");
    });

    it(":isUpdating set to true in beforeSend call when updateApp triggered", function () {
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.beforeSend({});
        });
        spyOn(viewModel, "isUpdating");
        viewModel.updateApp();
        expect(viewModel.isUpdating).toHaveBeenCalledWith(true);
    });

    it(":isUpdating set to false on error when updateApp triggered", function () {
        spyOn(viewModel, "readyToSubmit").and.returnValue(true);
        spyOn($, "ajax").and.callFake(function (e) {
            e.error();
        });
        spyOn(viewModel, "isUpdating");
        viewModel.updateApp();
        expect(viewModel.isUpdating).toHaveBeenCalledWith(false);
    });


    /***To Test ReadyToSubmit function that is called before form is submitted or updated***/

    /*** Update tests - to retain the original values of hidden fields when form is in editMode ***/

    it("Diploma name does not nullify and is same as is retrieved when form is in edit mode and diploma name is hidden and not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("my name");
        //want to keep default as existing address to avoid address validations
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    it("Diploma name is empty and is retrieved as empty when form is in edit mode and diploma name is hidden and not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName()).toEqual("");
    });

    it("Diploma name is null and is retrieved as null when form is in edit mode and diploma name is hidden and not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName(null);
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName()).toEqual(null);
    });

    it("Diploma name does not nullify and is same as is retrieved when form is in edit mode and diploma name is not hidden and is not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("my name");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName.isValid()).toBeTruthy();
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    it("Diploma name is empty and is retrieved as empty when form is in edit mode and diploma name is not hidden and is not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName.isValid()).toBeTruthy();
        expect(viewModel.DiplomaName()).toEqual("");
    });

    it("Diploma name is null and is retrieved as null when form is in edit mode and diploma name is not hidden and is not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName(null);
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName()).toEqual(null);
    });


    it("Diploma name is empty and invalid when form is in edit mode and diploma name is not hidden and is  required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(true);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(false);
        expect(viewModel.hasInputErrors()).toBe(true);
        expect(viewModel.DiplomaName()).toEqual("");
    });

    it("Diploma name is null and is invalid when form is in edit mode and diploma name is not hidden and is  required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(true);
        viewModel.DiplomaName(null);
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(false);
        expect(viewModel.hasInputErrors()).toBe(true);
        expect(viewModel.DiplomaName()).toEqual(null);
    });

    it("Diploma name has contents and is valid when form is in edit mode and diploma name is not hidden and is  required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(true);
        viewModel.DiplomaName("my name");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    it("Diploma name has contents and is valid when form is in edit mode and diploma name is not hidden and is not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("my name");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    it("Diploma name has null and is valid when form is in edit mode and diploma name is not hidden and is not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName(null);
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual(null);
    });

    it("Diploma name has space and is valid when form is in edit mode and diploma name is not hidden and is not required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual("");
    });

    /***Submit tests - to check if the values are properly re-initailized before submission happens***/


    it("Diploma name is empty when form is submitted and diploma name is hidden and not required", function () {
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName()).toEqual("");
    });

    it("Diploma name has same value as initalized with when form is submitted and diploma name is hidden and not required", function () {
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("my name");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    
    it("Diploma name does not nullify and is same as is initialized or entered when form is submitted and diploma name is not hidden and is not required", function () {
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("my name");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName.isValid()).toBeTruthy();
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    it("Diploma name is empty and is retrieved as empty when form is submitted and diploma name is not hidden and is not required", function () {
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName.isValid()).toBeTruthy();
        expect(viewModel.DiplomaName()).toEqual("");
    });

    it("Diploma name is null and is retrieved as null when form is submitted and diploma name is not hidden and is not required", function () {
        viewModel.ShowDiplomaName(false);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName(null);
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(result).toBe(true);
        expect(viewModel.DiplomaName()).toEqual(null);
    });


    it("Diploma name is empty and invalid when form is submitted and diploma name is not hidden and is  required", function () {
        viewModel.editOnlyMode(true);
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(true);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(false);
        expect(viewModel.hasInputErrors()).toBe(true);
        expect(viewModel.DiplomaName()).toEqual("");
    });

    it("Diploma name is null and is invalid when form is submitted and diploma name is not hidden and is  required", function () {
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(true);
        viewModel.DiplomaName(null);
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(false);
        expect(viewModel.hasInputErrors()).toBe(true);
        expect(viewModel.DiplomaName()).toEqual(null);
    });

    it("Diploma name has contents and is valid when form is submitted and diploma name is not hidden and is  required", function () {
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(true);
        viewModel.DiplomaName("my name");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    it("Diploma name has contents and is valid when form is submitted and diploma name is not hidden and is not required", function () {
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("my name");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual("my name");
    });

    it("Diploma name has null and is valid when form is submitted and diploma name is not hidden and is not required", function () {
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName(null);
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual(null);
    });

    it("Diploma name has space and is valid when form is submitted and diploma name is not hidden and is not required", function () {
        viewModel.ShowDiplomaName(true);
        viewModel.RequireDiplomaName(false);
        viewModel.DiplomaName("");
        viewModel.existingAddress(true);
        var result = viewModel.readyToSubmit();
        expect(viewModel.DiplomaName.isValid()).toBe(true);
        expect(viewModel.hasInputErrors()).toBe(false);
        expect(viewModel.DiplomaName()).toEqual("");
    });

    it("IsAcademicCredentialsUpdated false; should be editable", function () {
        viewModel.IsAcademicCredentialsUpdated(false);
        expect(viewModel.editOnlyMode, true);

    });

    it("IsAcademicCredentialsUpdated true; should not be editable", function () {
        viewModel.IsAcademicCredentialsUpdated(true);
        expect(viewModel.editOnlyMode, false);
    });
    
});