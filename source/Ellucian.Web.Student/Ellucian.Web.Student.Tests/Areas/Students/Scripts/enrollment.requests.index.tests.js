﻿describe("enrollment.requests.index.js", function () {
    var contentType;
    beforeAll(function () {
        jsonContentType = "json";
        getExistingEnrollmentRequestsUrl = "prevInfoUrl";
        Ellucian =Ellucian || {};
        Ellucian.Student = Ellucian.Student|| {};
        Ellucian.Student.Requests = Ellucian.Student.Requests || {};
        Ellucian.Student.Requests.enrollmentVerificationInfoGetFailure= "Failure!";
        Ellucian.Student.Requests.enrollmentVerificationRequestSubmissionSuccess="Success!";
        Ellucian.Student.Requests.unableToSubmitEnrollmentRequestPayment= "Payment Error!";
        Ellucian.Student.Requests.enrollmentVerificationRequestSubmissionFailure= "Update Failure!";
        Ellucian.Student.Requests.requestEmailConfirmation = "Email sent to";
        emailAddress = "email@address.com";
        notify = "";
        requestInfo = new StudentRequestsViewModel();
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    beforeEach(function () {
        contentType = "json";
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getEnrollmentRequestInfo();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":on ajax request error an expected message is being sent to notification center", function () {
        var jqXhr = {
            status: 500
        };
        var messageArg = {
            message: "Failure!",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getEnrollmentRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', messageArg);
    });

    it(":on ajax complete makeTableResponsive is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });

        spyOn($.fn, "makeTableResponsive");
        getEnrollmentRequestInfo();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":on ajax complete isLoaded is called with value 'true'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });

        spyOn(requestInfo, "isLoaded");
        getEnrollmentRequestInfo();
        expect(requestInfo.isLoaded).toHaveBeenCalledWith(true);
    });

    it(":correct message is produced when notify equals 'EnrollmentRequestSubmitSuccess'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "EnrollmentRequestSubmitSuccess";
        getEnrollmentRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Success! Email sent to email@address.com", type: "success" });
    });

    it(":correct message is produced when notify equals 'EnrollmentRequestSubmitFailure'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "EnrollmentRequestSubmitFailure";
        getEnrollmentRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Update Failure!", type: "error" });
    });

    it(":correct message is produced when notify equals 'EnrollmentRequestPaymentFailure'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "EnrollmentRequestPaymentFailure";
        getEnrollmentRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Payment Error!", type: "error" });
    });

    it(":on ajax success ko.mapping is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });

        spyOn(ko.mapping, "fromJS");
        getEnrollmentRequestInfo();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

});