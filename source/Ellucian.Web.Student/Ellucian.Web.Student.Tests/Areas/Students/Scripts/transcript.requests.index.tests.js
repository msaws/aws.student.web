﻿describe("transcript.requests.index.js", function () {
    var contentType;
    beforeAll(function () {
        jsonContentType = "json";
        getExistingTranscriptRequestsUrl = "prevInfoUrl";
        Ellucian = {};
        Ellucian.Student = {};
        Ellucian.Student.TranscriptRequests = {
            transcriptInfoGetFailure: "Failure!",
            newTranscriptRequestSuccessMessage: "Success!",
            unableToSubmitTranscriptRequestPayment: "Payment Error!",
            unableToSubmitNewTranscriptRequestMessage: "Update Failure!",
        };
        Ellucian.Student.Requests = {
            requestEmailConfirmation: "Email sent to"
        };
        emailAddress = "email@address.com";
        notify = "";
        requestInfo = new TranscriptRequestsViewModel();
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    beforeEach(function () {
        contentType = "json";
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getTranscriptRequestInfo();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":on ajax request error an expected message is being sent to notification center", function () {
        var jqXhr = {
            status: 500
        };
        var messageArg = {
            message: "Failure!",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getTranscriptRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', messageArg);
    });

    it(":on ajax complete makeTableResponsive is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });

        spyOn($.fn, "makeTableResponsive");
        getTranscriptRequestInfo();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":on ajax complete isLoaded is called with value 'true'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });

        spyOn(requestInfo, "isLoaded");
        getTranscriptRequestInfo();
        expect(requestInfo.isLoaded).toHaveBeenCalledWith(true);
    });

    it(":correct message is produced when notify equals 'TranscriptRequestSubmitSuccess'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "TranscriptRequestSubmitSuccess";
        getTranscriptRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Success! Email sent to email@address.com", type: "success" });
    });

    it(":correct message is produced when notify equals 'TranscriptRequestSubmitFailure'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "TranscriptRequestSubmitFailure";
        getTranscriptRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Update Failure!", type: "error" });
    });

    it(":correct message is produced when notify equals 'TranscriptRequestPaymentFailure'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "TranscriptRequestPaymentFailure";
        getTranscriptRequestInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: "Payment Error!", type: "error" });
    });

    it(":on ajax success ko.mapping is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });

        spyOn(ko.mapping, "fromJS");
        getTranscriptRequestInfo();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

});