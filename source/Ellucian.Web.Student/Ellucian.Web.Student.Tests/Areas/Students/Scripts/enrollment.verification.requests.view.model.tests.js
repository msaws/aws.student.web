﻿describe("enrollmentVerificationRequestsViewModel", function () {
    var viewModel;
    var global = jasmine.getGlobal();

    beforeEach(function () {
        viewModel = new enrollmentVerificationRequestsViewModel();
        
        ko.validation.init({
            messagesOnModified: true,
            insertMessages: false,
            decorateInputElement: false
        });
        ko.validation.configure({
            messagesOnModified: true,
            insertMessages: true
        });
        viewModel.errors = ko.validation.group(viewModel);
    });
    afterEach(function () {
         viewModel = null;
    })


    it(":Recipient initializes to empty and therefore invalid", function () {
         expect(viewModel.Recipient.isValid()).toBe(false);
    });

    it(":Recipient set to valid name", function () {
         viewModel.Recipient("Nathaniel");
         expect(viewModel.Recipient.isValid()).toBe(true);
    });


    //Valid Street1
    it(":ValidStreet1 returns false when just initialized", function () {
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(false)
    });
    it(":ValidStreet1 returns false when empty", function () {
        viewModel.AddressInfo().Street1("");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(false)
    });
    it(":ValidStreet1 returns false when only whitespace", function () {
        viewModel.AddressInfo().Street1("         ");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(false)
    });
    it(":ValidStreet1 returns true one word", function () {
        viewModel.AddressInfo().Street1("Jane");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });
    it(":ValidStreet1 returns true leading numbers", function () {
        viewModel.AddressInfo().Street1("123 Jane");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });
    it(":ValidStreet1 returns true multiple words", function () {
        viewModel.AddressInfo().Street1("Jane Smith");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });
    it(":ValidStreet1 returns true leading space", function () {
        viewModel.AddressInfo().Street1(" The Batman");
        expect(viewModel.AddressInfo().Street1.isValid()).toBe(true)
    });

     //Valid City

    it(":ValidCity returns false when empty", function () {
        viewModel.AddressInfo().City("");
        expect(viewModel.AddressInfo().City.isValid()).toBe(false)
    });
    it(":ValidCity returns false when only whitespace", function () {
        viewModel.AddressInfo().City("         ");
        expect(viewModel.AddressInfo().City.isValid()).toBe(false)
    });
    it(":ValidCity returns true one word", function () {
        viewModel.AddressInfo().City("Jane");
        expect(viewModel.AddressInfo().City.isValid()).toBe(true)
    });
    it(":ValidCity returns true leading numbers", function () {
        viewModel.AddressInfo().City("123 Jane");
        expect(viewModel.AddressInfo().City.isValid()).toBe(true)
    });
    it(":ValidCity returns true multiple words", function () {
        viewModel.AddressInfo().City("Jane Smith");
        expect(viewModel.AddressInfo().City.isValid()).toBe(true)
    });
    it(":ValidCity returns true leading space", function () {
        viewModel.AddressInfo().City(" The Batman");
        expect(viewModel.AddressInfo().City.isValid()).toBe(true)
    });
    

     //Valid PostalCode

    it(":ValidPostalCode returns false when empty", function () {
        viewModel.AddressInfo().PostalCode("");
        expect(viewModel.AddressInfo().PostalCode.isValid()).toBe(false)
    });
    it(":ValidPostalCode returns false when only whitespace", function () {
        viewModel.AddressInfo().PostalCode("         ");
        expect(viewModel.AddressInfo().PostalCode.isValid()).toBe(false)
    });
    it(":ValidPostalCode returns true one word", function () {
        viewModel.AddressInfo().PostalCode("12345");
        expect(viewModel.AddressInfo().PostalCode.isValid()).toBe(true)
    });
    it(":ValidPostalCode returns true leading numbers", function () {
        viewModel.AddressInfo().PostalCode("123 PO");
        expect(viewModel.AddressInfo().PostalCode.isValid()).toBe(true)
    });
    it(":ValidPostalCode returns true multiple words", function () {
        viewModel.AddressInfo().PostalCode("123 123");
        expect(viewModel.AddressInfo().PostalCode.isValid()).toBe(true)
    });
    it(":ValidPostalCode returns true leading space", function () {
        viewModel.AddressInfo().PostalCode(" 123 123");
        expect(viewModel.AddressInfo().PostalCode.isValid()).toBe(true)
    });

    it(":ValidNumberOfCopies returns true with valid input", function () {
        viewModel.NumberOfCopiesSelected("2");
        expect(viewModel.NumberOfCopiesSelected.isValid()).toBe(true)
    });
    it(":ValidNumberOfCopies returns false with too low number", function () {
        viewModel.NumberOfCopiesSelected("0");
        expect(viewModel.NumberOfCopiesSelected.isValid()).toBe(false)
    });
    it(":ValidNumberOfCopies returns false with too high number", function () {
        viewModel.NumberOfCopiesSelected("10");
        expect(viewModel.NumberOfCopiesSelected.isValid()).toBe(false)
    });

});