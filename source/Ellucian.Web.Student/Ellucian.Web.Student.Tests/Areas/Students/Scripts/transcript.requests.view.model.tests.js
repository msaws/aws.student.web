﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("transcript.requests.view.model.js", function(){
    
    var viewModel;

    beforeAll(function () {
        viewModel = new transcriptRequestsViewModel();
        viewModel.isValid = ko.observable(true);
        createNewTranscriptRequestActionUrl = "CreateRequestUrl";
        newTranscriptRequestFormUrl = "#";
    });

    beforeEach(function () { });

    it(":TranscriptTypes equals an empty array after initialization", function () {
        expect(viewModel.TranscriptTypes()).toEqual([]);
    });

    it(":TranscriptType is undefined after initialization", function () {
        expect(viewModel.TranscriptType()).toBeUndefined();
    });

    it(":TranscriptHoldTypes equals an empty array after initialization", function () {
        expect(viewModel.TranscriptHoldTypes()).toEqual([]);
    });
    
    it(":TranscriptHoldType is undefined after initialization", function () {
        expect(viewModel.TranscriptHoldType()).toBeUndefined();
    });

    it(":isSubmissionInProgress is false after initialization", function () {
        expect(viewModel.isSubmissionInProgress()).toBe(false);
    });

    it(":submitTranscriptRequest triggers ko.validation.group", function () {
        spyOn(ko.validation, "group");
        viewModel.submitTranscriptRequest();
        expect(ko.validation.group).toHaveBeenCalledWith(viewModel, { deep: true });
    });

    it(":cleanupAddressInfoBeforeSubmit is triggered if viewModel is valid", function () {
        spyOn(ko.validation, "group");
        spyOn(viewModel, "cleanupAddressInfoBeforeSubmit");
        viewModel.submitTranscriptRequest();
        expect(viewModel.cleanupAddressInfoBeforeSubmit).toHaveBeenCalled();
    });

   
    it(":hasInputErrors is set to true if viewModel is not valid", function () {
        viewModel.isValid(false);
        viewModel.submitTranscriptRequest();
        expect(viewModel.hasInputErrors()).toBe(true);
    });
    
});