﻿describe("student.grades.index.js", function () {
    var contentType;
    var data;
    beforeAll(function () {
        jsonContentType = "json";
        getStudentGradeInfoUrl = "prevInfoUrl";
        noGradingTermsToShowMessage = "No grading information";
        notify = "";
       // studentGradingInstance = new studentGradesViewModel();
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
        data = "{\"StudentTermGrades\":[{\"TermName\":\"2016 Fall Term\",\"StudentGrades\":[{\"FormattedCourseNameDisplay\":[\"ENGL-101-01\",\"8/25/2016 - 12/14/2016\"],\"CourseNameSort\":\"ENGL-101-01\",\"Title\":\"College Writing I\",\"StartDate\":\"2016-08-25T04:00:00.000Z\",\"EndDate\":\"2016-12-14T05:00:00.000Z\",\"FinalGrade\":\"\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"3\",\"Midterm1Grade\":null,\"Midterm2Grade\":null,\"Midterm3Grade\":null,\"Midterm4Grade\":null,\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"8/25/2016 - 12/14/2016\",\"FinalGradeDisplay\":[\"\"]},{\"FormattedCourseNameDisplay\":[\"ENGL-101-02\",\"8/25/2016 - 12/14/2016\"],\"CourseNameSort\":\"ENGL-101-02\",\"Title\":\"College Writing I\",\"StartDate\":\"2016-08-25T04:00:00.000Z\",\"EndDate\":\"2016-12-14T05:00:00.000Z\",\"FinalGrade\":\"\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"3\",\"Midterm1Grade\":null,\"Midterm2Grade\":null,\"Midterm3Grade\":null,\"Midterm4Grade\":null,\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"8/25/2016 - 12/14/2016\",\"FinalGradeDisplay\":[\"\"]},{\"FormattedCourseNameDisplay\":[\"ENGL-101-03\",\"8/25/2016 - 12/14/2016\"],\"CourseNameSort\":\"ENGL-101-03\",\"Title\":\"College Writing I\",\"StartDate\":\"2016-08-25T04:00:00.000Z\",\"EndDate\":\"2016-12-14T05:00:00.000Z\",\"FinalGrade\":\"\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"3\",\"Midterm1Grade\":null,\"Midterm2Grade\":null,\"Midterm3Grade\":null,\"Midterm4Grade\":null,\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"8/25/2016 - 12/14/2016\",\"FinalGradeDisplay\":[\"\"]},{\"FormattedCourseNameDisplay\":[\"ENGL-101-S2\",\"8/25/2016 - 12/14/2016\"],\"CourseNameSort\":\"ENGL-101-S2\",\"Title\":\"College Writing I\",\"StartDate\":\"2016-08-25T04:00:00.000Z\",\"EndDate\":\"2016-12-14T05:00:00.000Z\",\"FinalGrade\":\"\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"3\",\"Midterm1Grade\":null,\"Midterm2Grade\":null,\"Midterm3Grade\":null,\"Midterm4Grade\":null,\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"8/25/2016 - 12/14/2016\",\"FinalGradeDisplay\":[\"\"]}],\"TermYear\":2016,\"TermOrder\":5,\"CompletedGpa\":null,\"CompletedGpaDisplay\":\"\"},{\"TermName\":\"2016 Spring Term\",\"StudentGrades\":[{\"FormattedCourseNameDisplay\":[\"CRIM-222-VC\",\"1/1/2016 - 5/11/2016\"],\"CourseNameSort\":\"CRIM-222-VC\",\"Title\":\"Fingerprint Analysis\",\"StartDate\":\"2016-01-01T05:00:00.000Z\",\"EndDate\":\"2016-05-11T04:00:00.000Z\",\"FinalGrade\":\"\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"4\",\"Midterm1Grade\":null,\"Midterm2Grade\":null,\"Midterm3Grade\":null,\"Midterm4Grade\":null,\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"1/1/2016 - 5/11/2016\",\"FinalGradeDisplay\":[\"\"]},{\"FormattedCourseNameDisplay\":[\"HIST-200-01\",\"1/21/2016 - 5/11/2016\"],\"CourseNameSort\":\"HIST-200-01\",\"Title\":\"Ancient Civilization\",\"StartDate\":\"2016-01-21T05:00:00.000Z\",\"EndDate\":\"2016-05-11T04:00:00.000Z\",\"FinalGrade\":\"B\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"3\",\"Midterm1Grade\":null,\"Midterm2Grade\":null,\"Midterm3Grade\":null,\"Midterm4Grade\":null,\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"1/21/2016 - 5/11/2016\",\"FinalGradeDisplay\":[\"B\"]},{\"FormattedCourseNameDisplay\":[\"MATH-200-01\",\"1/21/2016 - 1/1/2017\"],\"CourseNameSort\":\"MATH-200-01\",\"Title\":\"Calculus III\",\"StartDate\":\"2016-01-21T05:00:00.000Z\",\"EndDate\":\"2017-01-01T05:00:00.000Z\",\"FinalGrade\":\"AU\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"3\",\"Midterm1Grade\":\"I\",\"Midterm2Grade\":\"C\",\"Midterm3Grade\":\"A\",\"Midterm4Grade\":\"B\",\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"1/21/2016 - 1/1/2017\",\"FinalGradeDisplay\":[\"AU\"]}],\"TermYear\":2015,\"TermOrder\":8,\"CompletedGpa\":1,\"CompletedGpaDisplay\":\"Term GPA:  1.000\"},{\"TermName\":\"2015 Fall Term\",\"StudentGrades\":[{\"FormattedCourseNameDisplay\":[\"ART-103\",\"8/20/2015 - 12/9/2015\"],\"CourseNameSort\":\"ART-103\",\"Title\":\"Aqueous Media\",\"StartDate\":\"2015-08-20T04:00:00.000Z\",\"EndDate\":\"2015-12-09T05:00:00.000Z\",\"FinalGrade\":\"B\",\"FinalGradeExpirationDate\":\"\",\"CreditsCeus\":\"4\",\"Midterm1Grade\":null,\"Midterm2Grade\":null,\"Midterm3Grade\":null,\"Midterm4Grade\":null,\"Midterm5Grade\":null,\"Midterm6Grade\":null,\"DatesDisplay\":\"8/20/2015 - 12/9/2015\",\"FinalGradeDisplay\":[\"B\"]}],\"TermYear\":2015,\"TermOrder\":3,\"CompletedGpa\":1.1818181818181819,\"CompletedGpaDisplay\":\"Term GPA:  1.182\"}],\"NumberOfMidtermGradesToShow\":6,\"Notifications\":[]}";


    });

    beforeEach(function () {
        contentType = "json";
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getStudentGradeInfo();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":on ajax request error an expected message is being sent to notification center", function () {
        var jqXhr = {
            status: 500
        };
        var messageArg = {
            message: "Unable to retrieve student grades",
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getStudentGradeInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', messageArg);
    });

    it(":on ajax success makeTableResponsive is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({}, {});
        });

        spyOn($.fn, "makeTableResponsive");
        getStudentGradeInfo();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":on ajax success retrieved is called with value 'true'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({}, {});
        });

        spyOn(studentGradingInstance, "retrieved");
        getStudentGradeInfo();
        expect(studentGradingInstance.retrieved).toHaveBeenCalledWith(true);
    });

    it(":on ajax success ko is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });

        spyOn(ko.mapping, "fromJS");
        getStudentGradeInfo();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });

    it("on ajax success proper mapping happens to student grade view model from the received json data", function () {
        var dataObject = JSON.parse(data);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(dataObject);
        });
        spyOn(ko, "toJS");
        spyOn($.fn, "notificationCenter");
        getStudentGradeInfo();
        expect(studentGradingInstance.StudentTermGrades().length).toEqual(3);
        expect(studentGradingInstance.StudentTermGrades()[0].StudentGrades().length).toEqual(4);
        expect(studentGradingInstance.StudentTermGrades()[0].showTermDetail()).toBe(true);
        expect(studentGradingInstance.StudentTermGrades()[1].showTermDetail()).toBe(false);
        expect(studentGradingInstance.StudentTermGrades()[1].showTermDetail()).toBe(false);
        expect(studentGradingInstance.Notifications().length).toBe(0);
        
        expect(ko.toJS.calls.any()).toEqual(false);
        
        expect($.fn.notificationCenter.calls.any()).toEqual(false);
    });

    it("received json data that does not have any student term grades", function ()
    {
        tempdata = "{\"StudentTermGrades\":[],\"NumberOfMidtermGradesToShow\":6,\"Notifications\":[]}";
        var dataObject = JSON.parse(tempdata);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(dataObject);
        });
        getStudentGradeInfo();
        expect(studentGradingInstance.StudentTermGrades().length).toEqual(0);
        expect(studentGradingInstance.Notifications().length).toBe(0);
        expect(studentGradingInstance.NumberOfMidtermGradesToShow()).toBe(6);
    });

    it("received json data have notifications and that many times notifications were added on notification bar", function () {
        var dataObject = JSON.parse(data);
        dataObject.Notifications.push({ Message: "this is error", Type: "Error", Flash: true });
        dataObject.Notifications.push({ Message: "this is information", Type: "Information", Flash: false });
        dataObject.Notifications.push({ Message: "this is success", Type: "Success", Flash: false });
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(dataObject);
        });
        spyOn(ko, "toJS");
        spyOn($.fn, "notificationCenter");
        getStudentGradeInfo();
        expect(studentGradingInstance.StudentTermGrades().length).toEqual(3);
        expect(studentGradingInstance.StudentTermGrades()[0].StudentGrades().length).toEqual(4);
        expect(studentGradingInstance.Notifications().length).toBe(3);
        expect(ko.toJS.calls.count()).toEqual(3);
        expect($.fn.notificationCenter.calls.count()).toEqual(3);
    });

    it("verify that notification observable array in grading model is properly converted to javascript object", function () {
        var dataObject = JSON.parse(data);
        dataObject.Notifications.push({ Message: "this is error", Type: "Error", Flash: true });
        dataObject.Notifications.push({ Message: "this is information", Type: "Information", Flash: false });
        dataObject.Notifications.push({ Message: "this is success", Type: "Success", Flash: false });
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(dataObject);
        });
        spyOn(ko, "toJS").and.callThrough();
        getStudentGradeInfo();
        expect(ko.toJS.calls.all()[0].returnValue.Message).toEqual("this is error");
        expect(ko.toJS.calls.all()[1].returnValue.Message).toEqual("this is information");
        expect(ko.toJS.calls.all()[2].returnValue.Message).toEqual("this is success");
        expect(ko.toJS.calls.all()[0].returnValue.Type).toEqual("Error");
        expect(ko.toJS.calls.all()[1].returnValue.Type).toEqual("Information");
        expect(ko.toJS.calls.all()[2].returnValue.Type).toEqual("Success");

    });

});