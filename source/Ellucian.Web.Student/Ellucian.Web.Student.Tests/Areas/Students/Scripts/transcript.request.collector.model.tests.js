﻿//Copyright 2016 Ellucian Company L.P. and its affiliates

describe("transcript.request.collector.model.js", function () {
    var model,
        transcriptRequestsData,
        data;

    beforeAll(function () {
        studentRequestModel = new baseStudentRequestCollectorModel({});
        studentRequestModel.call = function () { };
        data = new testData();
        transcriptRequestsData = data.transcriptRequestsData;
        model = new transcriptRequestCollectorModel(transcriptRequestsData);
    });

    it(":TranscriptGrouping is set to expected value if TranscriptType data is present", function () {
        expect(model.TranscriptGrouping).toEqual(transcriptRequestsData.TranscriptType().Id());
    });

    it(":TranscriptGrouping is not set if TranscriptType data is not present", function () {
        transcriptRequestsData.TranscriptType(null);
        model = new transcriptRequestCollectorModel(transcriptRequestsData);
        expect(model.TranscriptGrouping).toBeUndefined();
    });

    it(":HoldRequestCode is set to expected value if TranscriptHoldType data is present", function () {
        expect(model.HoldRequestCode).toEqual(transcriptRequestsData.TranscriptHoldType().Code());
    });

    it(":HoldRequestCode is not set if TranscriptHoldType data is not present", function () {
        transcriptRequestsData.TranscriptHoldType(null);
        model = new transcriptRequestCollectorModel(transcriptRequestsData);
        expect(model.HoldRequestCode).toBeUndefined();
    });
});