﻿/// <reference path="shared.references.js" />
describe("graduationAddViewModel", function () {
    var addViewModel;
    programCode = "program1";
    var collector;
    var global = jasmine.getGlobal();

    beforeAll(function () {
        mapPaymentReviewActionUrl = "#";
        gradView = {
            Programs: ko.observable([
                {Code: ko.observable("program1")}
            ])
        };

        programCode = "program1";
        collector = new GraduationViewModel();
        collector.GraduationTermSelected({Code:ko.observable("2015/FA")});

        
    });

    afterEach(function () {
        addViewModel = null;
    })

  
    it(":programCode in addViewModel is assigned the programCode passed as parameter", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.ProgramCode).toBe("program1");
    });

    it(":graduation Term in addViewModel is assigned the term from  passed as parameter", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.GraduationTerm).toBe("2015/FA");
    });

    //diploma name
    it(":DiplomaName in addViewModel is undefined when collector model DiplomaName is undefined", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.DiplomaName).toBe(undefined);
    });
    
    it(":DiplomaName in addViewModel is undefined when collector model DiplomaName is null", function () {
        collector.DiplomaName(null);
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.DiplomaName).toBe(undefined);
    });

    it(":DiplomaName in addViewModel is undefined when collector model DiplomaName is Empty", function () {
        collector.DiplomaName("");
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.DiplomaName).toBe(undefined);
    });

    it(":DiplomaName in addViewModel is trimmed value when collector model DiplomaName has spaces", function () {
        collector.DiplomaName("  abcd  ");
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.DiplomaName).toBe("abcd");
    });

    //phonetic speliing name
    it(":PhoneticSpellingOfName in addViewModel is undefined when collector model DiplomaName is PhoneticSpellingOfName", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.PhoneticSpellingOfName).toBe(undefined);
    });

    it(":PhoneticSpellingOfName in addViewModel is undefined when collector model PhoneticSpellingOfName is null", function () {
        collector.PhoneticSpellingOfName(null);
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.PhoneticSpellingOfName).toBe(undefined);
    });

    it(":PhoneticSpellingOfName in addViewModel is undefined when collector model PhoneticSpellingOfName is Empty", function () {
        collector.PhoneticSpellingOfName("");
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.PhoneticSpellingOfName).toBe(undefined);
    });

    it(":PhoneticSpellingOfName in addViewModel is trimmed value when collector model PhoneticSpellingOfName has spaces", function () {
        collector.PhoneticSpellingOfName("  abcd  ");
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.PhoneticSpellingOfName).toBe("abcd");
    });
    
    // hometown
    it(":Hometown in addViewModel is undefined when collector model Hometown is PhoneticSpellingOfName", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.Hometown).toBe(undefined);
    });

    it(":Hometown in addViewModel is undefined when collector model Hometown is null", function () {
        collector.Hometown(null);
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.Hometown).toBe(undefined);
    });

    it(":Hometown in addViewModel is undefined when collector model Hometown is Empty", function () {
        collector.Hometown("");
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.Hometown).toBe(undefined);
    });

    it(":Hometown in addViewModel is trimmed value when collector model Hometown has spaces", function () {
        collector.Hometown("  abcd  ");
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.Hometown).toBe("abcd");
    });
    
    //commencement location

    it(":Commencement Location in addViewModel is undefined when collector model CommencementSiteSelected is undefined", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.CommencementLocation).toBe(undefined);
    });

    it(":Commencement Location in addViewModel is undefined when collector model CommencementSiteSelected is null", function () {
        collector.CommencementSiteSelected(null);
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.CommencementLocation).toBe(undefined);
    });

    it(":Commencement Location in addViewModel is undefined when collector model CommencementSiteSelected is emplty", function () {
        collector.CommencementSiteSelected("");
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.CommencementLocation).toBe(undefined);
    });

    it(":Commencement Location in addViewModel is same Code as defined in collector model CommencementSiteSelected observable", function () {
        collector.CommencementSiteSelected({Code:ko.observable("abcd")});
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.CommencementLocation).toBe("abcd");
    });

    // attending commencement
    it(":Attending Commencement in addViewModel is same as collector model Attending Commencement and initial value is true", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.AttendingCommencement).toBe(collector.AttendingCommencement());
        expect(addViewModel.AttendingCommencement).toBe(true);
    });

    it(":Attending Commencement in addViewModel is null when collector model Attending Commencement is null", function () {
        collector.AttendingCommencement(null);
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.AttendingCommencement).toBe(collector.AttendingCommencement());
        expect(addViewModel.AttendingCommencement).toBe(null);
    });

    //Will pickup diploma
    it(":WillPickupDiploma in addViewModel is same as collector model WillPickupDiploma and initial value is true", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.WillPickupDiploma).toBe(collector.WillPickupDiploma());
        expect(addViewModel.WillPickupDiploma).toBe(true);
    });

    it(":WillPickupDiploma in addViewModel is null when collector model WillPickupDiploma is null", function () {
        collector.WillPickupDiploma(null);
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.WillPickupDiploma).toBe(collector.WillPickupDiploma());
        expect(addViewModel.WillPickupDiploma).toBe(null);
    });

    //include name in program
    it(":IncludeNameInProgram in addViewModel is same as collector model IncludeNameInProgram and initial value is true", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.IncludeNameInProgram).toBe(collector.IncludeNameInProgram());
        expect(addViewModel.IncludeNameInProgram).toBe(true);
    });


    it(":IncludeNameInProgram in addViewModel is null when collector model IncludeNameInProgram is null", function () {
        collector.IncludeNameInProgram(null);
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.IncludeNameInProgram).toBe(collector.IncludeNameInProgram());
        expect(addViewModel.IncludeNameInProgram).toBe(null);
    });

   //number of guests
    it(":NumberOfGuests in addViewModel is same as collector model NumberOfGuests and initial value is 0", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.NumberOfGuests).toBe(collector.NumberOfGuests());
        expect(addViewModel.NumberOfGuests).toBe("0");
    });

    //default address values
    it(":Verify default values for diploma address when collector model has no address provided", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.MailDiplomaToAddressLines.length).toBe(0);
        expect(addViewModel.MailDiplomaToCity).toBe("");
        expect(addViewModel.MailDiplomaToPostalCode).toBe("");
        expect(addViewModel.MailDiplomaToState).toBe(undefined);
        expect(addViewModel.MailDiplomaToCountry).toBe(undefined);

    });

    //collector has proper address
    it(":Verify values for diploma address when collector model has proper address provided", function () {
        collector.AddressInfo().Street1("line 1");
        collector.AddressInfo().Street2("line 2");
        collector.AddressInfo().Street2("line 3");
        collector.AddressInfo().Street4("line 4");
        collector.AddressInfo().City("my city");
        collector.AddressInfo().PostalCode("12345");
        collector.AddressInfo().State({ Code: "VA" });
        collector.AddressInfo().Country({ Code: "USA" });

        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.MailDiplomaToAddressLines.length).toBe(3);
        expect(addViewModel.MailDiplomaToCity).toBe("my city");
        expect(addViewModel.MailDiplomaToPostalCode).toBe("12345");
        expect(addViewModel.MailDiplomaToState).toBe("VA");
        expect(addViewModel.MailDiplomaToCountry).toBe("USA");

    });

    //cap size

    it(":Verify default values for CapSize when collector model has no CapSizeSelected provided", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.CapSize).toBe(undefined);
    });

    it(":Verify values for CapSize when collector model has specific CapSizeSelected provided", function () {
        collector.CapSizeSelected({Code:ko.observable("SM")});
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.CapSize).toBe("SM");
    });

    //gown size
    it(":Verify default values for GownSize when collector model has no GownSizeSelected provided", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.GownSize).toBe(undefined);
    });

    it(":Verify values for GownSize when collector model has proper GownSizeSelected provided", function () {
        collector.GownSizeSelected({ Code: ko.observable("SM") });
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.GownSize).toBe("SM");
    });

    //military status and special accomodations 
    it(":Verify default values for MilitaryStatus when collector model has no MilitaryStatusSelected provided", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.MilitaryStatus).toBe(undefined);
        expect(addViewModel.SpecialAccommodations).toBe("");
    });

    it(":Verify values for MilitaryStatus when collector model has proper MilitaryStatusSelected provided", function () {
        collector.MilitaryStatusSelected({ Code: ko.observable("ML") });
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.MilitaryStatus).toBe("ML");
        expect(addViewModel.SpecialAccommodations).toBe("");
    });
    
    //primary location 
    it(":Verify default values for PrimaryLocation when collector model has no PrimaryLocationSelected provided", function () {
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.PrimaryLocation).toBe(undefined);
    });

    it(":Verify values for PrimaryLocation when collector model has proper PrimaryLocationSelected provided", function () {
        collector.PrimaryLocationSelected({ Code: ko.observable("YY") });
        addViewModel = new GraduationAddViewModel(programCode, collector);
        expect(addViewModel.PrimaryLocation).toBe("YY");
    });

});