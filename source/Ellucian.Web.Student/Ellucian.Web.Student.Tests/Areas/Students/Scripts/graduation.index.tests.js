﻿describe("graduation.index.js", function () {
    var contentType;    
    beforeAll(function () {
        jsonContentType = "json";
       
       Ellucian = Ellucian || {};
       Ellucian.Student = Ellucian.Student || {};
       Ellucian.Student.Graduation = Ellucian.Student.Graduation || {};

       Ellucian.Student.Graduation.gradAppGetFailure ="Failure!";
       Ellucian.Student.Graduation.gradAppUpdateSuccess = "Success!";
       Ellucian.Student.Graduation.gradAppProfileUpdateError = "Error!";
       Ellucian.Student.Graduation.gradAppUpdateFailure = "Update Failure!";
        
        notify = "";
        account = {
            handleInvalidSessionResponse: function (data) { return false; }
        };
    });

    beforeEach(function () {
        contentType = "json";        
    });

    it(":ajax call uses correct content type", function () {
        spyOn($, "ajax");
        getGraduationInfo();
        expect($.ajax.calls.mostRecent().args[0]["contentType"]).toEqual(contentType);
    });

    it(":on ajax request error an expected message is being sent to notification center", function () {
        var jqXhr = {
            status: 500
        };
        var messageArg = {
            message: Ellucian.Student.Graduation.gradAppGetFailure,
            type: "error"
        };
        spyOn($, "ajax").and.callFake(function (e) {
            e.error(jqXhr, {}, {});
        });
        spyOn($.fn, "notificationCenter");
        getGraduationInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', messageArg);
    });

    it(":on ajax complete makeTableResponsive is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });

        spyOn($.fn, "makeTableResponsive");
        getGraduationInfo();
        expect($.fn.makeTableResponsive).toHaveBeenCalled();
    });

    it(":on ajax complete isLoaded is called with value 'true'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });

        spyOn(gradInfo, "isLoaded");
        getGraduationInfo();
        expect(gradInfo.isLoaded).toHaveBeenCalledWith(true);
    });

    it(":correct message is produced when notify equals 'editSuccess'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "editSuccess";
        getGraduationInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.Student.Graduation.gradAppUpdateSuccess, type: "success" });
    });

    it(":correct message is produced when notify equals 'editFailure'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "editFailure";
        getGraduationInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.Student.Graduation.gradAppUpdateFailure, type: "error" });
    });

    it(":correct message is produced when notify equals 'editAddressError'", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.complete({}, {});
        });
        spyOn($.fn, "notificationCenter");
        notify = "editAddressError";
        getGraduationInfo();
        expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.Student.Graduation.gradAppProfileUpdateError, type: "error" });
    });

    it(":on ajax success ko.mapping is called", function () {
        spyOn($, "ajax").and.callFake(function (e) {
            e.success({});
        });

        spyOn(ko.mapping, "fromJS");
        getGraduationInfo();
        expect(ko.mapping.fromJS).toHaveBeenCalled();
    });
        
    //tests to confirm proper mapping happened

    it("on ajax success graduation info view model is properly mapped from received json object", function(){
        //var gradInfo = new GraduationInfoViewModel();
        var stringifiedData="{\"Programs\":[{\"Code\":\"AGBU.AA\",\"Title\":\"Associate of Arts Agriculture Business\",\"Term\":\"2016 Fall Term\",\"Selected\":false,\"Degree\":\"Associate of Arts\",\"Majors\":[\"Agriculture Business 2 Yr Only\"],\"SubmittedString\":\"Application submitted on 2/14/2017 \",\"IsGraduationApplicationSubmitted\":true},{\"Code\":\"MATH.BA\",\"Title\":\"Bachelors of Arts in Math\",\"Term\":\"2016 Fall Term\",\"Selected\":false,\"Degree\":\"Bachelor of Arts\",\"Majors\":[\"Mathematics\"],\"SubmittedString\":\"Application submitted on 2/21/2017 \",\"IsGraduationApplicationSubmitted\":true}],\"AcceptingNewApplications\":true,\"CommencementInformationLink\":\"http://commencement.cornell.edu\",\"ApplyForDifferentProgramLink\":\"http://commencement.cornell.edu\",\"ShowMyProgressLink\":true}";
        var dataObject = JSON.parse(stringifiedData);
        spyOn($, "ajax").and.callFake(function (e) {
            e.success(dataObject);
        });
            spyOn(ko.mapping, "fromJS").and.callThrough();
            getGraduationInfo();
            expect(ko.mapping.fromJS).toHaveBeenCalled();
            expect(gradInfo).not.toBe(null);
            expect(gradInfo).not.toBe(undefined);
            expect(gradInfo.Programs().length).toBe(2);
            expect(gradInfo.ShowMyProgressLink()).toBe(true);
            expect(gradInfo.CommencementInformationLink()).toEqual("http://commencement.cornell.edu");
            expect(gradInfo.ApplyForDifferentProgramLink()).toEqual("http://commencement.cornell.edu");
    });


});