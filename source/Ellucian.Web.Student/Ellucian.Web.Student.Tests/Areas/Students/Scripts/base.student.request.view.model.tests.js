﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
describe("base.student.request.view.model.js", function () {
    var viewModel;
    var global = jasmine.getGlobal();

    beforeAll(function () {
        viewModel = new baseStudentRequestViewModel();
    });

    it(":isTranscriptRequest is false after initialization", function () {
        expect(viewModel.isTranscriptRequest()).toBeFalsy();
    });

    it(":hasInputErrors is false after initialization", function () {
        expect(viewModel.hasInputErrors()).toBeFalsy();
    });

    it(":AddressInfo is defined after initialization", function () {
        expect(viewModel.AddressInfo()).toBeDefined();
    });

    it(":Recipient is defined after initialization", function () {
        expect(viewModel.Recipient()).toBeDefined();
    });

    it(":MaximumNumberOfCopies equal 9 after initialization", function () {
        expect(viewModel.MaximumNumberOfCopies()).toBe(9);
    });

    it(":NumberOfCopiesSelected equal 1 after initialization", function () {
        expect(viewModel.NumberOfCopiesSelected()).toBe(1);
    });

    it(":Comments property is undefined after initialization", function () {
        expect(viewModel.Comments()).toBeUndefined();
    });

    it(":cleanupAddressBeforeSubmit sets City to null if it is an international address", function () {
        viewModel.AddressInfo().internationalSelected(true);
        viewModel.cleanupAddressInfoBeforeSubmit();
        expect(viewModel.AddressInfo().City()).toEqual(null);
    });

    it(":cleanupAddressBeforeSubmit sets State to null if it is an international address", function () {
        viewModel.AddressInfo().internationalSelected(true);
        viewModel.cleanupAddressInfoBeforeSubmit();
        expect(viewModel.AddressInfo().State()).toEqual(null);
    });

    it(":cleanupAddressBeforeSubmit sets PostalCode to null if it is an international address", function () {
        viewModel.AddressInfo().internationalSelected(true);
        viewModel.cleanupAddressInfoBeforeSubmit();
        expect(viewModel.AddressInfo().PostalCode()).toEqual(null);
    });

    it(":cleanupAddressBeforeSubmit sets Country with expected value if address is not international", function () {
        viewModel.AddressInfo().internationalSelected(false);
        viewModel.AddressInfo().State({Code: "VA", CountryCode: "US"});
        viewModel.AddressInfo().Countries([{ Code: "US" }, { Code: "CA" }]);
        viewModel.cleanupAddressInfoBeforeSubmit();
        expect(viewModel.AddressInfo().Country()).toEqual({ Code: "US" });
    });

    it(":cleanupAddressBeforeSubmit sets Street3 property to null if address is not international", function () {
        viewModel.AddressInfo().internationalSelected(false);
        viewModel.AddressInfo().State({ Code: "VA", CountryCode: "US" });
        viewModel.cleanupAddressInfoBeforeSubmit();
        expect(viewModel.AddressInfo().Street3()).toEqual(null);
    });

    it(":cleanupAddressBeforeSubmit sets Street4 property to null if address is not international", function () {
        viewModel.AddressInfo().internationalSelected(false);
        viewModel.AddressInfo().State({ Code: "VA", CountryCode: "US" });
        viewModel.cleanupAddressInfoBeforeSubmit();
        expect(viewModel.AddressInfo().Street4()).toEqual(null);
    });
});