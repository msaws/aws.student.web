﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Student.Areas.Student.Models.StudentRequests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.Students.ModelTests
{
    [TestClass]
    public class TranscriptRequestModelTests
    {
        private TranscriptRequestModel transcriptRequestModel;

        [TestInitialize]
        public void Initialize()
        {
            transcriptRequestModel = new TranscriptRequestModel();
        }

        [TestCleanup]
        public void Cleanup()
        {
            transcriptRequestModel = null;
        }

        [TestMethod]
        public void TranscriptGroupingGetSetTest()
        {
            string expectedGroping = "Undegraduate";
            transcriptRequestModel.TranscriptGrouping = expectedGroping;
            Assert.AreEqual(expectedGroping, transcriptRequestModel.TranscriptGrouping);
        }
    }
}
