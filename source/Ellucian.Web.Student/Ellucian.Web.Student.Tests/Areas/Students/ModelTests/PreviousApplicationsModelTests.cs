﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.Student.Models.Graduation;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Web.Student.Tests.Areas.Students.ModelTests
{
    [TestClass]
    public class PreviousApplicationsModelTests : GraduationTestData
    {
        private PreviousApplicationsModel previousApps;

        [TestInitialize]
        public void Initialize()
        {
            // Set up the VM
            previousApps = new PreviousApplicationsModel(base.MeAsStudent, base.Programs, base.Majors, base.AllTerms, base.Configuration, base.StudentPrograms, base.previousApplications, base.currentUser, base.financeConfiguration);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PreviousApplicationsModel_StudentNull_ThrowsException()
        {
            var newmodel = new PreviousApplicationsModel(null, base.Programs, base.Majors, base.AllTerms, base.Configuration, base.StudentPrograms, base.previousApplications, base.currentUser, base.financeConfiguration);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PreviousApplicationsModel_StudentPrograms_ThrowsException()
        {
            var newmodel = new PreviousApplicationsModel(base.MeAsStudent, base.Programs, base.Majors, base.AllTerms, base.Configuration, null, base.previousApplications, base.currentUser, base.financeConfiguration);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PreviousApplicationsModel_Programs_ThrowsException()
        {
            var newmodel = new PreviousApplicationsModel(base.MeAsStudent, null, base.Majors, base.AllTerms, base.Configuration, base.StudentPrograms, base.previousApplications, base.currentUser, base.financeConfiguration);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PreviousApplicationsModel_Terms_ThrowsException()
        {
            var newmodel = new PreviousApplicationsModel(base.MeAsStudent, base.Programs, base.Majors, null, base.Configuration, base.StudentPrograms, base.previousApplications, base.currentUser, base.financeConfiguration);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PreviousApplicationsModel_Majors_ThrowsException()
        {
            var newmodel = new PreviousApplicationsModel(base.MeAsStudent, base.Programs, null, base.AllTerms, base.Configuration, base.StudentPrograms, base.previousApplications, base.currentUser, base.financeConfiguration);
        }
        
        [TestMethod]
        public void PreviousApplicationsModel_Returns_Programs()
        {
            Assert.AreEqual(2, previousApps.Programs.Count);
            // Sorted correctly with unsubmitted one first
            var firstProgram = previousApps.Programs.ElementAt(0);
            Assert.AreEqual("MATH.BA", firstProgram.Code);
        }

        [TestMethod]
        public void PreviousApplicationsModel_AcceptingNewApplications()
        {
            Assert.IsTrue(previousApps.AcceptingNewApplications);

        }

        [TestMethod]
        public void PreviousApplicationsModel_ProgramInfo()
        {
            var secondProgram = previousApps.Programs.ElementAt(1);
            Assert.AreEqual("ENGL.BA", secondProgram.Code);
            Assert.AreEqual("BA", secondProgram.Degree);
            Assert.IsTrue(secondProgram.IsGraduationApplicationSubmitted);

        }

        [TestMethod]
        public void PreviousApplicationsModel_Links()
        {
            string differentProgLink = "differentproglink.com";
            string commencementInfoLink = "commencementInfolink.com";
            var newConfiguration = new GraduationConfiguration();
            newConfiguration.GraduationTerms = base.Configuration.GraduationTerms;
            newConfiguration.RequireImmediatePayment = base.Configuration.RequireImmediatePayment;
            newConfiguration.ApplyForDifferentProgramLink = differentProgLink;
            newConfiguration.CommencementInformationLink = commencementInfoLink;
            var newmodel = new PreviousApplicationsModel(base.MeAsStudent, base.Programs, base.Majors, base.AllTerms, newConfiguration, base.StudentPrograms, base.previousApplications, base.currentUser, base.financeConfiguration);
            Assert.AreEqual(differentProgLink, newmodel.ApplyForDifferentProgramLink);
            Assert.AreEqual(commencementInfoLink, newmodel.CommencementInformationLink);
        }
    }
}
