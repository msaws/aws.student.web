﻿using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Areas.Student.Models.Graduation;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.Students.ModelTests
{
    public class GraduationTestData
    {
        protected Ellucian.Colleague.Dtos.Student.Student MeAsStudent { get; set; }
        protected Program GraduatingProgram { get; set; }
        protected List<Program> Programs { get; set; }
        protected List<Major> Majors { get; set; }
        protected List<Minor> Minors { get; set; }
        protected List<Term> AllTerms { get; set; }
        protected List<Specialization> Specialization { get; set; }
        protected GraduationConfiguration Configuration { get; set; }
        protected List<StudentProgram2> StudentPrograms { get; set; }
        protected List<CapSize> CapSizes { get; set; }
        protected List<GownSize> GownSizes { get; set; }
        protected List<CommencementSite> CommencementSites { get; set; }
        protected List<Country> Countries { get; set; }
        protected List<State> States { get; set; }
        protected FinanceConfiguration financeConfiguration { get; set; }
        protected UserProfileConfiguration userProfileConfiguration { get; set; }
        protected GraduationApplicationFee graduationFees { get; set; }
        protected List<PrimaryLocationModel> primaryLocations { get; set; }
        protected ICurrentUser currentUser;
        protected IEnumerable<Role> roles;
        protected List<GraduationApplication> previousApplications { get; set; }

        public GraduationTestData()
        {
            MeAsStudent = new Colleague.Dtos.Student.Student() { LastName = "Smith", FirstName = "John", MiddleName = "Paul", ProgramIds = new List<string>() { "MATH.BA", "MECH.CERT", "ENGL.BA" } };

            Programs = new List<Program>();
            Majors = new List<Major>();
            Minors = new List<Minor>();
            AllTerms = new List<Term>();
            Specialization = new List<Specialization>();
            Configuration = new GraduationConfiguration();
            StudentPrograms = new List<StudentProgram2>();
            CapSizes = new List<CapSize>();
            GownSizes = new List<GownSize>();
            CommencementSites = new List<CommencementSite>();
            Countries = new List<Country>();
            States = new List<State>();
            financeConfiguration = new FinanceConfiguration();
            graduationFees = new GraduationApplicationFee();
            primaryLocations = new List<PrimaryLocationModel>();
            Countries = new List<Country>();
            States = new List<State>();
            previousApplications = new List<GraduationApplication>();

            // Graduation Configuration
            Configuration.RequireImmediatePayment = false;
            Configuration.GraduationTerms = new List<string>() { "2017/FA", "2017/SP" };
            Configuration.MaximumCommencementGuests = 5;

            // Programs
            GraduatingProgram = new Program() { Code = "ENGL.BA", IsGraduationAllowed = true, Title ="English BA", Degree = "BA", Majors = new List<string>() { "ACCT"}, Minors = new List<string>() {"ENGL"}, Specializations = new List<string>(), Ccds = new List<string>()};

            Programs.Add(GraduatingProgram);
            Programs.Add(new Program() { Code = "MECH.CERT", IsGraduationAllowed = false });
            Programs.Add(new Program() { Code = "MATH.BA", IsGraduationAllowed = true });

            // Student Programs - match with program Ids in MeAsStudent
            StudentPrograms.Add(new StudentProgram2() { ProgramCode = "MATH.BA", AdditionalRequirements = new List<AdditionalRequirement2>() });
            StudentPrograms.Add(new StudentProgram2() { ProgramCode = "MECH.CERT", AdditionalRequirements = new List<AdditionalRequirement2>() });
            StudentPrograms.Add(new StudentProgram2() { ProgramCode = "ENGL.BA", AdditionalRequirements = new List<AdditionalRequirement2>() });
            // Countries & States
            Countries.Add(new Country() { Code = "US", Description = "USA", IsNotInUse = false, IsoCode = "US" });
            States.Add(new State() { Code = "AL", CountryCode = "", Description = "ALASKA" });
            // Supporting info for the programs
            Majors.Add(new Major() { Code = "ASAA", Description = "Astrophysics" });
            Majors.Add(new Major() { Code = "ACCT", Description = "Accounting" });
            Majors.Add(new Major() { Code = "ENGL", Description = "English" });

            Minors.Add(new Minor() { Code = "ACCT", Description = "Accounting" });
            Minors.Add(new Minor() { Code = "ACCT", Description = "Accounting" });
            Minors.Add(new Minor() { Code = "ENGL", Description = "English" });
            // Terms - need to match the graduation terms and the term in any previous applications
            AllTerms.Add(new Term() { Code = "2017/SP", Description = "2017/SP" });
            AllTerms.Add(new Term() { Code = "2017/FA", Description = "2017/FA" });
            // Finance Configuration - needed if immediate payment is true - and needs at least 1 payment method
            financeConfiguration.PaymentMethods = new List<AvailablePaymentMethod>() { new AvailablePaymentMethod() { InternalCode = "11" } };
            // User profile configuration
            userProfileConfiguration = new UserProfileConfiguration() { CanUpdateAddressWithoutPermission = false };
            
            // Pre-existingn applications 
            previousApplications.Add(new GraduationApplication() { GraduationTerm = "2017/SP", ProgramCode = "ENGL.BA", DiplomaName = "Diploma Name", Hometown = "Hometown", IncludeNameInProgram = false, NumberOfGuests = 5 });

            var currentUserMock = new Mock<ICurrentUser>();
            currentUserMock.Setup(c => c.Roles).Returns(new List<string>() { "STUDENT", "ADVISOR" });

            currentUser = currentUserMock.Object;
            roles = new List<Role>() {
                new Role() {Permissions = new List<Permission>() {new Permission() {Code="UPDATE.OWN.EMAIL"}}, Title = "STUDENT"},
                new Role() {Permissions = new List<Permission>() {}, Title = "ADVISOR"}
            };

        }


    }

}
