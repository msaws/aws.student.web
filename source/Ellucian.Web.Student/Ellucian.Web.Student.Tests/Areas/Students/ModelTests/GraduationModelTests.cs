﻿using System;
using System.Text;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.Student.Models.Graduation;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Finance.Configuration;
using Ellucian.Web.Security;
using Moq;

namespace Ellucian.Web.Student.Tests.Areas.Students.ModelTests
{
    /// <summary>
    /// Summary description for GraduationModelTests
    /// to test configuration settings
    /// to test student and terms and programCodes
    /// </summary>
    [TestClass]
    public class GraduationModelTest : GraduationTestData
    {
        GraduationModel model = null;
        public GraduationModelTest()
            : base()
        {

        }

        #region Additional test attributes
        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void Initialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void Cleanup()
        {
            model = null;
        }

        #endregion

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_StudentNull_ThrowsException()
        {
            model = new GraduationModel(null, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_ProgramNull_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, null, Majors, Minors, Specialization, AllTerms,
                          Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_MajorsNull_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, null, Minors, Specialization, AllTerms,
                          Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_MajorsEmpty_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, new List<Major>(), Minors, Specialization, AllTerms,
                          Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_StudentProgramsNull_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          Configuration, null, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_GraduationConfigurationNull_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          null, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_StatesNull_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, null, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_StatesEmpty_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, new List<State>(), financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_CountriesNull_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, null, States, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModelTest_CountriesEmpty_ThrowsException()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, new List<Country>(), States, financeConfiguration,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);

        }

        [TestMethod]
        public void GraduationModel_FullName()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            Assert.AreEqual("John Paul Smith", model.FullName);
        }

        [TestMethod]
        public void GraduationModel_GraduationTerms()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            Assert.AreEqual(2, model.GraduationTerms.Count);
            Assert.IsNotNull(model.GraduationTerms.Where(t => t.Code == "2017/FA").FirstOrDefault());
        }

        [TestMethod]
        public void GraduationModel_MilitaryStatuses()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            Assert.AreEqual(3, model.MilitaryStatuses.Count);
            Assert.IsNotNull(model.MilitaryStatuses.Where(t => t.Description == "Veteran").FirstOrDefault());
        }

        [TestMethod]
        public void GraduationModel_AdditionalRequirements()
        {
            
            var gradProgram = base.StudentPrograms.Where( sp=>sp.ProgramCode == GraduatingProgram.Code).FirstOrDefault();
            gradProgram.AdditionalRequirements = new List<AdditionalRequirement2>() {(new AdditionalRequirement2() { Type = AwardType.Major, AwardName = "ART" })};
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            
            Assert.AreEqual(1, model.Programs.Count());
            var prog = model.Programs.ElementAt(0);
            Assert.AreEqual(2, prog.Majors.Count());
            Assert.IsTrue(prog.Majors.Where(p => p == "ART").FirstOrDefault() != null);
        }

        [TestMethod]
        public void GraduationModel_NothingRequired()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            Assert.IsFalse(model.RequireAttendCommencement);
            Assert.IsFalse(model.RequireCapSize);
            Assert.IsFalse(model.RequireCommencementLocation);
            Assert.IsFalse(model.RequireDiplomaName);
            Assert.IsFalse(model.RequireGownSize);
            Assert.IsFalse(model.RequireHometown);
            Assert.IsFalse(model.RequireMilitaryStatus);
            Assert.IsFalse(model.RequireNameInProgram);
            Assert.IsFalse(model.RequireNumberGuests);
            Assert.IsFalse(model.RequirePhoneticSpelling);
            Assert.IsFalse(model.RequirePickUpDiploma);
            Assert.IsFalse(model.RequirePrimaryLocation);
            Assert.IsFalse(model.RequireSpecialAccommodations);

        }

        [TestMethod]
        public void GraduationModel_ShowNothing()
        {
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            Assert.IsFalse(model.ShowAttendCommencement);
            Assert.IsFalse(model.ShowCapSize);
            Assert.IsFalse(model.ShowCommencementLocation);
            Assert.IsFalse(model.ShowDiplomaName);
            Assert.IsFalse(model.ShowGownSize);
            Assert.IsFalse(model.ShowHometown);
            Assert.IsFalse(model.ShowMilitaryStatus);
            Assert.IsFalse(model.ShowNameInProgram);
            Assert.IsFalse(model.ShowNewPreferredAddressQuestion);
            Assert.IsFalse(model.ShowNumberGuests);
            Assert.IsFalse(model.ShowPhoneticSpelling);
            Assert.IsFalse(model.ShowPickUpDiploma);
            Assert.IsFalse(model.ShowPrimaryLocation);
            Assert.IsFalse(model.ShowSpecialAccommodations);
        }

        [TestMethod]
        public void GraduationModel_ShowAll()
        {
            var applicationQuestions = new List<GraduationQuestion>() {
                new GraduationQuestion() { Type = GraduationQuestionType.AttendCommencement, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.CapSize, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.CommencementLocation, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.DiplomaName, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.GownSize, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.Hometown, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.MilitaryStatus, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.NameInProgram, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.NumberGuests, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.PhoneticSpelling, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.PickUpDiploma, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.PrimaryLocation, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.RequestAddressChange, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.SpecialAccommodations, IsRequired = true}
            };
            var graduationConfigurationUpdated = new GraduationConfiguration() { ApplicationQuestions = applicationQuestions, GraduationTerms = Configuration.GraduationTerms, RequireImmediatePayment = false };
            var userProfileConfigurationUpdated = new UserProfileConfiguration() { AddressesAreUpdatable = true, CanUpdateAddressWithoutPermission = true};
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           graduationConfigurationUpdated, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, userProfileConfigurationUpdated, base.currentUser, base.roles);
            Assert.IsTrue(model.ShowAttendCommencement);
            Assert.IsTrue(model.ShowCapSize);
            Assert.IsTrue(model.ShowCommencementLocation);
            Assert.IsTrue(model.ShowDiplomaName);
            Assert.IsTrue(model.ShowGownSize);
            Assert.IsTrue(model.ShowHometown);
            Assert.IsTrue(model.ShowMilitaryStatus);
            Assert.IsTrue(model.ShowNameInProgram);
            Assert.IsTrue(model.ShowNumberGuests);
            Assert.IsTrue(model.ShowPhoneticSpelling);
            Assert.IsTrue(model.ShowPickUpDiploma);
            Assert.IsTrue(model.ShowPrimaryLocation);
            Assert.IsTrue(model.ShowSpecialAccommodations);
            Assert.IsTrue(model.ShowNewPreferredAddressQuestion);
        }

        public void GraduationModel_RequireAll()
        {
            var applicationQuestions = new List<GraduationQuestion>() {
                new GraduationQuestion() { Type = GraduationQuestionType.AttendCommencement, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.CapSize, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.CommencementLocation, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.DiplomaName, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.GownSize, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.Hometown, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.MilitaryStatus, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.NameInProgram, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.NumberGuests, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.PhoneticSpelling, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.PickUpDiploma, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.PrimaryLocation, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.RequestAddressChange, IsRequired = true},
                new GraduationQuestion() { Type = GraduationQuestionType.SpecialAccommodations, IsRequired = true}
            };
            var graduationConfigurationUpdated = new GraduationConfiguration() { ApplicationQuestions = applicationQuestions, GraduationTerms = Configuration.GraduationTerms, RequireImmediatePayment = false };
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           graduationConfigurationUpdated, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);
            Assert.IsTrue(model.RequireAttendCommencement);
            Assert.IsTrue(model.RequireCapSize);
            Assert.IsTrue(model.RequireCommencementLocation);
            Assert.IsTrue(model.RequireDiplomaName);
            Assert.IsTrue(model.RequireGownSize);
            Assert.IsTrue(model.RequireHometown);
            Assert.IsTrue(model.RequireMilitaryStatus);
            Assert.IsTrue(model.RequireNameInProgram);
            Assert.IsTrue(model.RequireNumberGuests);
            Assert.IsTrue(model.RequirePhoneticSpelling);
            Assert.IsTrue(model.RequirePickUpDiploma);
            Assert.IsTrue(model.RequirePrimaryLocation);
            Assert.IsTrue(model.RequireSpecialAccommodations);

        }
        [TestMethod]
        public void GraduationModel_ExistingApplication()
        {
            var existingApp = base.previousApplications.ElementAt(0);
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles, existingApp);
            Assert.AreEqual(existingApp, model.GraduationApplication);
            Assert.AreEqual("2017/SP", model.GraduationTermDisplay);
            Assert.IsFalse(model.IsDiplomaAddressSameAsPreferred);
            Assert.IsFalse(model.IsAcademicCredentialsUpdated);
            Assert.IsTrue(model.GraduationApplication is GraduationApplication);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModel_ImmediatePaymentNoFees_ThrowsException()
        {
            var applicationQuestions = new List<GraduationQuestion>() {
                new GraduationQuestion() { Type = GraduationQuestionType.RequestAddressChange, IsRequired = true}
            };           
            var graduationConfigurationUpdated = new GraduationConfiguration() { ApplicationQuestions = applicationQuestions, GraduationTerms = Configuration.GraduationTerms, RequireImmediatePayment = true };
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          graduationConfigurationUpdated, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, new List<Country>(), States, financeConfiguration,
                          null, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GraduationModel_ImmediatePaymentNoFinanceConfig_ThrowsException()
        {
            var applicationQuestions = new List<GraduationQuestion>() {
                new GraduationQuestion() { Type = GraduationQuestionType.RequestAddressChange, IsRequired = true}
            }; 
            var graduationConfigurationUpdated = new GraduationConfiguration() { ApplicationQuestions = applicationQuestions, GraduationTerms = Configuration.GraduationTerms, RequireImmediatePayment = true };
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                          graduationConfigurationUpdated, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, new List<Country>(), States, null,
                          graduationFees, primaryLocations, base.userProfileConfiguration, base.currentUser, base.roles);

        }



    }

    [TestClass]
    public class GraduationModelTest_For_Roles_Permissions : GraduationTestData
    {
        GraduationModel model = null;

        public GraduationModelTest_For_Roles_Permissions()
            : base()
        {

        }

        #region Additional test attributes
        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void Initialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void Cleanup()
        {
            model = null;
        }

        #endregion

        [TestMethod]
        public void GraduationModel_UserProfileConfiguration_Is_Null()
        {
            
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           Configuration, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, null, base.currentUser, base.roles);
            Assert.IsFalse(model.ShowNewPreferredAddressQuestion);
        }

        [TestMethod]
        public void GraduationModel_IncorrectPermission()
        {
            var applicationQuestions = new List<GraduationQuestion>() {
                
                new GraduationQuestion() { Type = GraduationQuestionType.RequestAddressChange, IsRequired = false}

            };
            var graduationConfigurationUpdated = new GraduationConfiguration() { ApplicationQuestions = applicationQuestions, GraduationTerms = Configuration.GraduationTerms, RequireImmediatePayment = false };
            var userProfileConfigNew = new UserProfileConfiguration() { AddressesAreUpdatable = true, CanUpdateAddressWithoutPermission = false };
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           graduationConfigurationUpdated, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, userProfileConfigNew, base.currentUser, base.roles);
            Assert.IsFalse(model.ShowNewPreferredAddressQuestion);
        }

        [TestMethod]
        public void GraduationModel_NeedsPermission_CorrectPermission()
        {
            var applicationQuestions = new List<GraduationQuestion>() {
                
                new GraduationQuestion() { Type = GraduationQuestionType.RequestAddressChange, IsRequired = false}

            };
            var graduationConfigurationUpdated = new GraduationConfiguration() { ApplicationQuestions = applicationQuestions, GraduationTerms = Configuration.GraduationTerms, RequireImmediatePayment = false };
            var newroles = new List<Role>() {
                new Role() {Permissions = new List<Permission>() {new Permission() {Code="UPDATE.OWN.ADDRESS"}}, Title = "STUDENT"},
                new Role() {Permissions = new List<Permission>() {}, Title = "ADVISOR"}
            };
            var userProfileConfigNew = new UserProfileConfiguration() { AddressesAreUpdatable = true, CanUpdateAddressWithoutPermission = false };
            model = new GraduationModel(MeAsStudent, GraduatingProgram, Majors, Minors, Specialization, AllTerms,
                           graduationConfigurationUpdated, base.StudentPrograms, CapSizes, GownSizes, CommencementSites, Countries, States, financeConfiguration,
                           graduationFees, primaryLocations, userProfileConfigNew, base.currentUser, newroles);
            Assert.IsTrue(model.ShowNewPreferredAddressQuestion);
        }
    }


}
