﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Areas.Student.Models.StudentRequests;
using System.Collections.ObjectModel;

namespace Ellucian.Web.Student.Tests.Areas.Students.ModelTests
{
    [TestClass]
    public class TranscriptRequestsViewModelTests
    {
        private TestModelData testModelData;
        private List<Country> countryDtos;
        private List<State> stateDtos;
        private List<TranscriptGrouping> transcriptGroupingDtos;
        private List<HoldRequestType> holdRequestTypeDtos;

        private TranscriptRequestsViewModel transcriptRequestsViewModel;

        [TestInitialize]
        public void Initialize()
        {
            testModelData = new TestModelData();
            countryDtos = testModelData.countriesData;
            stateDtos = testModelData.statesData;
            transcriptGroupingDtos = testModelData.transcriptGroupingsData;
            holdRequestTypeDtos = testModelData.holdRequestTypeData;

            BuildTranscriptRequestsViewModel();
        }

        [TestCleanup]
        public void Cleanup()
        {
            testModelData = null;
            countryDtos = null;
            stateDtos = null;
            transcriptGroupingDtos = null;
            holdRequestTypeDtos = null;
            transcriptRequestsViewModel = null;
        }

        [TestMethod]
        public void ViewModelInitializedTest()
        {            
            Assert.IsNotNull(transcriptRequestsViewModel);
        }

        [TestMethod]
        public void CountriesEqualAndSortedExpectedTest()
        {
            
            CollectionAssert.AreEqual(countryDtos.OrderBy(c => c.Description).ToList(), transcriptRequestsViewModel.Countries.ToList());
            var firstCountry = transcriptRequestsViewModel.Countries.FirstOrDefault();
            Assert.AreEqual("Brazil", firstCountry.Description);
        }

        [TestMethod]
        public void StatesEqualAndSortedExpectedTest()
        {
            CollectionAssert.AreEqual(stateDtos.OrderBy(s => s.Description).ToList(), transcriptRequestsViewModel.States.ToList());
            var firstState = transcriptRequestsViewModel.States.FirstOrDefault();
            Assert.AreEqual("Alabama", firstState.Description);
        }

        [TestMethod]
        [Ignore]
        public void TranscriptTypesEqualExpectedTest()
        {
            CollectionAssert.AreEqual(transcriptGroupingDtos.ToList(), transcriptRequestsViewModel.TranscriptTypes.ToList());
        }

        [TestMethod]
        public void HoldTypesEqualExpectedTest()
        {
            CollectionAssert.AreEqual(holdRequestTypeDtos.ToList(), transcriptRequestsViewModel.TranscriptHoldTypes.ToList());
        }

        [TestMethod]
        public void CountriesListIsEmpty_IfNoCountriesWerePassedTest()
        {
            countryDtos = null;
            BuildTranscriptRequestsViewModel();
            Assert.IsFalse(transcriptRequestsViewModel.Countries.Any());
        }

        [TestMethod]
        public void StatesListIsEmpty_IfNoStatesWerePassedTest()
        {
            stateDtos = null;
            BuildTranscriptRequestsViewModel();
            Assert.IsFalse(transcriptRequestsViewModel.States.Any());
        }

        [TestMethod]
        public void TranscriptTypesListIsNull_IfNoTranscriptGroupingsPassedTest()
        {
            transcriptGroupingDtos = null;
            BuildTranscriptRequestsViewModel();
            Assert.IsNull(transcriptRequestsViewModel.TranscriptTypes);
        }

        [TestMethod]
        public void HoldTypesListIsNull_IfNoHoldTypesPassedTest()
        {
            holdRequestTypeDtos = null;
            BuildTranscriptRequestsViewModel();
            Assert.IsNull(transcriptRequestsViewModel.TranscriptHoldTypes);
        }



        private void BuildTranscriptRequestsViewModel()
        {
            transcriptRequestsViewModel = new TranscriptRequestsViewModel(countryDtos, stateDtos, holdRequestTypeDtos);
        }
    }
}
