﻿//Copyright 2016 Ellucian Company L.P. and its affiliates
using Ellucian.Web.Student.Areas.Student.Models.StudentRequests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.Students.ModelTests
{
    [TestClass]
    public class EnrollmentVerificationRequestModelTests
    {
        private EnrollmentVerificationRequestModel requestModel;

        [TestInitialize]
        public void Initialize()
        {
            requestModel = new EnrollmentVerificationRequestModel();
        }

        [TestCleanup]
        public void Cleanup()
        {
            requestModel = null;
        }

        [TestMethod]
        public void RequestModelIsNotNullTest()
        {
            Assert.IsNotNull(requestModel);
        }

        [TestMethod]
        public void RecipientGetSetTest()
        {
            string expectedRecipient = "Recipient";
            requestModel.Recipient = expectedRecipient;
            Assert.AreEqual(expectedRecipient, requestModel.Recipient);
        }

        [TestMethod]
        public void MailToAddressLinesGetSetTest()
        {
            List<string> expectedLines = new List<string>() { "123 Main Street", "apt.4B" };
            requestModel.MailToAddressLines = expectedLines;
            Assert.AreEqual(expectedLines, requestModel.MailToAddressLines);
        }

        [TestMethod]
        public void MailToCityGetSetTest()
        {
            string expectedCity = "Central city";
            requestModel.MailToCity = expectedCity;
            Assert.AreEqual(expectedCity, requestModel.MailToCity);
        }

        [TestMethod]
        public void MailToStateGetSetTest()
        {
            string expectedState = "VA";
            requestModel.MailToState = expectedState;
            Assert.AreEqual(expectedState, requestModel.MailToState);
        }

        [TestMethod]
        public void MailToCountryGetSetTest()
        {
            string expectedCountry = "Bulgaria";
            requestModel.MailToCountry = expectedCountry;
            Assert.AreEqual(expectedCountry, requestModel.MailToCountry);
        }

        [TestMethod]
        public void MailToPostalCodeTest()
        {
            string expectedPostalCode = "22033";
            requestModel.MailToPostalCode = expectedPostalCode;
            Assert.AreEqual(expectedPostalCode, requestModel.MailToPostalCode);
        }

        [TestMethod]
        public void NumberOfCopiesGetSetTest() 
        {
            int expectedNumber = 5;
            requestModel.NumberOfCopies = expectedNumber;
            Assert.AreEqual(expectedNumber, requestModel.NumberOfCopies);
        }

        [TestMethod]
        public void CommentsGetSetTest()
        {
            string expectedComments = "Comments";
            requestModel.Comments = expectedComments;
            Assert.AreEqual(expectedComments, requestModel.Comments);
        }

        [TestMethod]
        public void HoldRequestCodeGetSetTest()
        {
            string expectedCode = "Hold for grades";
            requestModel.HoldRequestCode = expectedCode;
            Assert.AreEqual(expectedCode, requestModel.HoldRequestCode);
        }

    }
}
