﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.Students.ModelTests
{
    public class TestModelData
    {
        public List<Country> countriesData;
        public List<State> statesData;
        public List<TranscriptGrouping> transcriptGroupingsData;
        public List<HoldRequestType> holdRequestTypeData;

        public TestModelData()
        {
            #region Countries
            countriesData = new List<Country>()
            {
                new Country(){
                    Code = "US",
                    Description = "United States"
                },
                new Country(){
                    Code = "CA",
                    Description = "Canada"
                },
                new Country(){
                    Code = "IR",
                    Description = "Ireland"
                },
                new Country(){
                    Code = "RU",
                    Description = "Russia"
                },
                new Country(){
                    Code = "BR",
                    Description = "Brazil"
                },
                new Country(){
                    Code = "GB",
                    Description = "Great Britain"
                }
            };
            #endregion

            #region States
            statesData = new List<State>()
            {
                new State(){
                    Code = "VA",
                    CountryCode = "US",
                    Description = "Virginia"
                },
                new State(){
                    Code = "CA",
                    CountryCode = "US",
                    Description = "California"
                },
                new State(){
                    Code = "AK",
                    CountryCode = "US",
                    Description = "Alaska"
                },
                new State(){
                    Code = "AL",
                    CountryCode = "US",
                    Description = "Alabama"
                }
            };
            #endregion

            #region TranscriptGroupings
            transcriptGroupingsData = new List<TranscriptGrouping>(){
                new TranscriptGrouping(){
                    Id = "GR",
                    Description = "Graduate"
                },
                new TranscriptGrouping(){
                    Id = "UG",
                    Description = "Undergraduate"
                },
                new TranscriptGrouping(){
                    Id = "PG",
                    Description = "Postgraduate"
                },
                new TranscriptGrouping(){
                    Id = "DA",
                    Description = "Degree Audit"
                },
                new TranscriptGrouping(){
                    Id = "HS",
                    Description = "High School"
                }
            };
            #endregion

            #region HoldRequestTypes
            holdRequestTypeData = new List<HoldRequestType>(){
                new HoldRequestType(){
                    Code = "FG",
                    Description = "Final Grades"
                },
                new HoldRequestType(){
                    Code = "PI",
                    Description = "Process Immediately"
                },
                new HoldRequestType(){
                    Code = "WTW",
                    Description = "Wait three weeks"
                }
            };
            #endregion
        }

    }
}
