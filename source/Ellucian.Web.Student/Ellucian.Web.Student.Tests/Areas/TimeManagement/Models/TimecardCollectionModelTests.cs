﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Colleague.Dtos.TimeManagement;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class TimecardCollectionModelTests : TimeManagementTestData
    {
        [TestInitialize]
        public void Initialize()
        {
            base.SetupTimecardCollectionModelTests();
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            timecardCollectionModel = new TimecardCollectionModel();
            Assert.IsNotNull(timecardCollectionModel.Timecards);
        }

        [TestMethod]
        public void ConstructorTests()
        {
            timecardCollectionModel = new TimecardCollectionModel(employeeId, payCycleId, startDate, endDate, employeeDictionary);
            Assert.IsNotNull(timecardCollectionModel.Timecards);
            Assert.AreEqual(startDate, timecardCollectionModel.StartDate);
            Assert.AreEqual(endDate, timecardCollectionModel.EndDate);
        }

        [TestMethod]
        public void TimecardIsAdded()
        {
            var len = timecardCollectionModel.Timecards.Count;
            timecardCollectionModel.AddTimecard(timecardModel);
            Assert.AreEqual(len + 1, timecardCollectionModel.Timecards.Count);
        }

        [TestMethod]
        public void TimecardIsHad()
        {
            timecardCollectionModel.AddTimecard(timecardModel);
            Assert.IsTrue(timecardCollectionModel.Has(timecardModel.TimecardId));
        }

        [TestMethod]
        public void EffectiveStartDateIsFound()
        {
            var payCycle = positionQuery.GetPayCycle(payCycleId);
            var effStartDate = timecardModel.WorkEntryList.SelectMany(w => w.DailyTimeEntryGroupList.Select(d => d.DateWorked)).OrderBy(d => d).FirstOrDefault()
                 .NormalizeToStartDayOfWeek(payCycle.WorkWeekStartDay);
            Assert.AreEqual(effStartDate, timecardCollectionModel.EffectiveStartDate);
        }

        [TestMethod]
        public void PayCycleIdIsFound()
        {
            var pid = timecardModel.PayCycleId;
            timecardCollectionModel.AddTimecard(timecardModel);
            Assert.AreEqual(pid, timecardCollectionModel.PayCycleId);
        }

        [TestMethod]
        public void EmployeeIdIsFound()
        {
            var eid = timecardModel.EmployeeId;
            timecardCollectionModel.AddTimecard(timecardModel);
            Assert.AreEqual(eid, timecardCollectionModel.EmployeeId);
        }

        [TestMethod]
        public void EmployeeNameIsFound()
        {
            timecardCollectionModel.AddTimecard(timecardModel);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(timecardCollectionModel.EmployeeName));
        }
    }
}
