﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class TimeManagementUtilitiesTests
    {
        List<Timecard2> Timecards2;
        Position pos;
        PersonPosition perpos;
        PositionQueryResultItem pqr;
        List<TimePeriodModel> timePeriodModels;

        public void SetupData()
        {
            Timecards2 = new List<Timecard2>()
            {
                new Timecard2()
                {
                    Id = "what",
                    PositionId = "iss",
                    StartDate = new DateTime(2011, 02, 01),
                    EndDate = new DateTime(2011, 02, 28)
                },
                new Timecard2()
                {
                    Id = "one",
                    PositionId = "smc",
                    StartDate = new DateTime(2011, 02, 01),
                    EndDate = new DateTime(2011, 02, 28)
                },
                new Timecard2()
                {
                    Id = "becomes",
                    PositionId = "smc",
                    StartDate = new DateTime(2012, 02, 01),
                    EndDate = new DateTime(2012, 02, 28)
                }
            };

            pos = new Position()
            {
                Id = "smc"
            };

            perpos = new PersonPosition()
            {
                PositionId = pos.Id,
                StartDate = new DateTime(2011,01,01)
            };

            pqr = new PositionQueryResultItem(perpos, pos) { PayCycle = new PayCycle() { Id = "foobaroo"} };

            timePeriodModels = new List<TimePeriodModel>()
            {
                new TimePeriodModel()
                {                    
                    StartDate = new DateTime(2011, 01, 01),
                    EndDate = new DateTime(2011, 01, 31)
                },
                new TimePeriodModel()
                {                    
                    StartDate =  new DateTime(2011, 02, 01),
                    EndDate =  new DateTime(2011, 02, 28)
                },
                new TimePeriodModel()
                {                    
                    StartDate = new DateTime(2011, 03, 01),
                    EndDate = new DateTime(2011, 03, 31)
                }
            };
        }

        [TestInitialize]
        public void Initialize()
        {
            SetupData();
        }

        [TestMethod]
        public void ExpectedWeeksAndDatesAreReturnedGivenRange()
        {
            var startDate = new DateTime(2011, 02, 01);
            var endDate = new DateTime(2011, 02, 28);
            var weeks = TimeManagementUtilities.Weeks(startDate, endDate);

            Assert.AreEqual(4, weeks.Count());

            foreach(var w in weeks)
            {
                Assert.AreEqual(7, w.Count());
            }
        }

        [TestMethod]
        public void ExpectedWeeksAndDatesAreReturnedGivenPayPeriodDTO()
        {
            var payPeriod = new PayPeriod() 
            {
                StartDate = new DateTime(2011, 02, 01),
                EndDate = new DateTime(2011, 02, 28)
            };
            var weeks = payPeriod.Weeks();

            Assert.AreEqual(4, weeks.Count());

            foreach (var w in weeks)
            {
                Assert.AreEqual(7, w.Count());
            }
        }

        [TestMethod]
        public void TimecardDTOForPositionForDateIsFound()
        {
            var date = new DateTime(2011, 02, 14);

            var timecard = Timecards2.FindTimecardDtoForPositionForDate(pqr, date);

            Assert.AreEqual("one", timecard.Id);
        }

        [TestMethod]
        public void TimePeriodModelForDateIsFound()
        {
            var chosenStartDate = new DateTime(2011, 02, 01);
            var chosenEndDate = new DateTime(2011, 02, 28);
            Assert.IsNotNull(timePeriodModels.FindTimePeriodForDate(chosenStartDate, chosenEndDate));
        }

        //[TestMethod]
        //public void TimecardIsCreatedForPosition()
        //{
        //    var personId = "24601";
        //    var startDate = new DateTime(2011,02,01);
        //    var endDate = new DateTime(2011,02,07);
        //    var timecard = TimeManagementUtilities.CreateNewTimecardForPosition(pqr, personId, startDate, endDate);
        //    Assert.IsNotNull(timecard);
        //}
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CannotCreateTimecardForPositionWithoutPQR()
        {
            pqr = null;
            var personId = "24601";
            var startDate = new DateTime(2011, 02, 01);
            var endDate = new DateTime(2011, 02, 07);
            TimeManagementUtilities.CreateNewTimecardForPosition(pqr, personId, startDate, endDate);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CannotCreateTimecardForPositionWithoutPersonId()
        {
            var personId = string.Empty;
            var startDate = new DateTime(2011, 02, 01);
            var endDate = new DateTime(2011, 02, 07);
            TimeManagementUtilities.CreateNewTimecardForPosition(pqr, personId, startDate, endDate);
        }

        [TestMethod]
        public void MinimumDateIsGotten()
        {

        }

        [TestMethod]
        public void MaximumDateIsGotten()
        {

        }

        [TestMethod]
        public void EmployeeNameIsFormatted()
        {

        }

        [TestMethod]
        public void EmployeeNameWIthMiddleNameIsFormatted()
        {

        }

        [TestMethod]
        public void NoEmployeeNameReturnsId()
        {

        }

        [TestMethod]
        public void IntegerIsModulized()
        {

        }

        [TestMethod]
        public void DateIsNormalizedToStartDayOfWeek()
        {

        }
        [TestMethod]
        public void DateIsNormalizedToEndDayOfWeek()
        {

        }
        [TestMethod]
        public void TimecardHistoryIsConvertedToTimecard()
        {

        }
        [TestMethod]
        public void OnlyADistinctItemIsAddedToASortedList()
        {

        }
    }
}
