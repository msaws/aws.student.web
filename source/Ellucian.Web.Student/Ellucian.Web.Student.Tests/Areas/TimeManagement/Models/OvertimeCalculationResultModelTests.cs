﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Colleague.Dtos.TimeManagement;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class OvertimeCalculationResultModelTests : TimeManagementTestData
    {

        [TestInitialize]
        public void Initialize()
        {
            base.SetupOvertimeCalculationResultModelTests();
        }

        [TestMethod]
        public void PayCycleIdIsSetTest()
        {
            overtimeCalculationResultmodel = new OvertimeCalculationResultModel(payCycleId, overtimeCalculationResultdto.Thresholds);
            Assert.AreEqual(payCycleId, overtimeCalculationResultmodel.PayCycleId);
        }

        [TestMethod]
        public void TotalOvertimeIsCalculatedFromThresholdsTest()
        {
            var multiplier = 8.5;
            overtimeCalculationResultdto.Thresholds.ForEach(th => th.TotalOvertime = TimeSpan.FromHours(multiplier));
            overtimeCalculationResultmodel = new OvertimeCalculationResultModel(payCycleId, overtimeCalculationResultdto.Thresholds);
            Assert.AreEqual((decimal)(overtimeCalculationResultdto.Thresholds.Count() * multiplier), overtimeCalculationResultmodel.TotalOvertime);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullOrEmptyPaycycleIdExceptionTest()
        {
            payCycleId = string.Empty;
            new OvertimeCalculationResultModel(payCycleId, overtimeCalculationResultdto.Thresholds);
        }
        [TestMethod]        
        public void NullThresholdsIsZeroOvertimeTest()
        {
            overtimeCalculationResultdto.Thresholds = null;
            var model = new OvertimeCalculationResultModel(payCycleId, overtimeCalculationResultdto.Thresholds);
            Assert.AreEqual(0, model.TotalOvertime);
        }
    }
}
