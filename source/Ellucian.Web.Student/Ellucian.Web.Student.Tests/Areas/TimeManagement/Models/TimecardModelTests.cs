﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
     [TestClass]
     public class TimecardModelTests : TimeManagementTestData
     {
          TimecardModel timecard;

          [TestInitialize]
          public void Intitialize()
          {
               base.SetupTimecardModelTests();
               base.SetupTimecardHistories();
               timecardQuery = new TimecardQueryDataCoordinator(personId, new List<Timecard2>() { timecard2Dto }, timecardHistories, statuses, commentsList);
               var timecardQueryResultContext = timecardQuery.FindTimecardQueryResultContext(positionQueryResult, timecard2Dto.StartDate, timecard2Dto.EndDate);
               timecard = new TimecardModel(timecardQueryResultContext, positionQueryResult, earningsTypes, perstats, departments, locations, employeeDictionary, timeFrameStartDate);
          }

          [TestMethod]
          public void DefaultConstructorTest()
          {
               var model = new TimecardModel();
               Assert.IsNotNull(model.WorkEntryList);
               Assert.IsNotNull(model.EarningsTypes);
          }

          [TestMethod]
          public void ModelIdIsCreatedCorrectlyTest()
          {
               var expectedModelId = (timecard.PositionId.GetHashCode() ^
                                       timecard.PayCycleId.GetHashCode() ^
                                       timecard.StartDate.GetHashCode() ^
                                       timecard.EndDate.GetHashCode()).ToString();
               Assert.AreEqual(expectedModelId, timecard.ModelId);
          }

          [TestMethod]
          public void TimecardModelIsValidTest()
          {
               var originalTimecard = timecard;
               Assert.IsTrue(timecard.IsValid());
               timecard.EmployeeId = string.Empty;
               Assert.IsFalse(timecard.IsValid());
               timecard.EmployeeId = originalTimecard.EmployeeId;
               timecard.PositionId = string.Empty;
               Assert.IsFalse(timecard.IsValid());
               timecard.PositionId = originalTimecard.PositionId;
               timecard.PayCycleId = string.Empty;
               Assert.IsFalse(timecard.IsValid());
               timecard.PayCycleId = originalTimecard.PayCycleId;
               timecard.EmployeeSubmitDeadline = null;
               Assert.IsFalse(timecard.IsValid());
               timecard.EmployeeSubmitDeadline = originalTimecard.EmployeeSubmitDeadline;
               timecard.SupervisorApproveDeadline = null;
               Assert.IsFalse(timecard.IsValid());
          }

          [TestMethod]
          public void PropertiesAreSetIfThereArePerPosWages()
          {
               //var type = timecard.GetType();
               //foreach (var prop in type.GetProperties())
               //{
               //    if (!type.IsValueType && prop.Name != "Timestamp") // timestamp is not set in constructor
               //        Assert.IsNotNull(prop.GetValue(timecard));
               //}

               //timecardDto.StartDate = new DateTime(0999, 03, 01);
               //timecardDto.EndDate = new DateTime(1000, 03, 01);
               //timecardQueryResult = new TimecardQueryResultItem(timecardDto, timecardHistories.First(), statuses.First(), commentsList);
               //timecardQueryResultContext = new TimecardQueryResultContext(null, timecardQueryResult, null);
               //timecard = new TimecardModel(timecardQueryResultContext, positionQueryResult, earningsTypes, perstats, timeFrameStartDate);

               //foreach (var prop in type.GetProperties())
               //{
               //    if (!type.IsValueType)
               //        Assert.IsNull(prop.GetValue(timecard));
               //}
          }

          [TestMethod]
          public void ModelIsConvertedToDTO()
          {
               var dto = timecard.ConvertModelToDto();

               foreach (var prop1 in dto.GetType().GetProperties())
               {
                    foreach (var prop2 in timecard.GetType().GetProperties())
                    {
                         if (prop1.Name == prop2.Name)
                         {
                              Assert.AreEqual(prop1.GetValue(dto), prop2.GetValue(timecard));
                         }
                    }
               }
          }
     }
}
