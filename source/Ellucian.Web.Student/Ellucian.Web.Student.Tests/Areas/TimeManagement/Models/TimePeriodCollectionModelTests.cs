﻿using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class TimePeriodCollectionModelTests : TimeManagementTestData
    {
        [TestInitialize]
        public void Initialize()
        {
            base.SetupTimePeriodCollectionModelTestData();
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            var model = new TimePeriodCollectionModel();
            Assert.IsNotNull(model.TimePeriodModels);
        }

        [TestMethod]
        public void ConstructorTest()
        {
            timePeriodCollectionModel = new TimePeriodCollectionModel(payCycleId, timecardQuery, positionQuery, perstats);
            Assert.IsNotNull(timePeriodCollectionModel.TimePeriodModels);
            Assert.AreEqual(payCycleId, timePeriodCollectionModel.PayCycleId);
        }

        [TestMethod]
        public void TimePeriodModelsAreBuilt()
        {
            timePeriodCollectionModel = new TimePeriodCollectionModel(payCycleId, timecardQuery, positionQuery, perstats);
            Assert.IsTrue(timePeriodCollectionModel.TimePeriodModels.Count > 0);
        }

        [TestMethod]
        public void GetTimePeriodModelGetsTimePeriodModelForCriteria()
        {
            var gottenModel = timePeriodCollectionModel.GetTimePeriodModel(startDate, endDate);
            Assert.IsTrue(timePeriodCollectionModel.TimePeriodModels.Any(m => m.Equals(gottenModel)));
        }

        [TestMethod]
        public void GetTimePeriodModelsGetsTimePeriodModelsForCriteria()
        {
            var gottenModels = timePeriodCollectionModel.GetTimePeriodModels(payCycleId, positionId, periodStartDate, periodEndDate);
            foreach(var model in gottenModels)
            {
                Assert.IsTrue(timePeriodCollectionModel.TimePeriodModels.Any(m => m.Equals(model)));
            }
        }
    }
}
