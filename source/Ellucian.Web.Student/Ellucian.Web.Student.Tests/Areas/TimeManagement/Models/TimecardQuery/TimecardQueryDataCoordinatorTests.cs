﻿using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models.TimecardQuery
{
     [TestClass]
     public class TimecardQueryDataCoordinatorTests
     {
          public TimeManagementTestData testData;

          public List<Timecard2> inputTimecards;
          public List<TimecardHistory2> inputHistories;
          public List<TimecardStatus> inputStatuses;
          public List<TimeEntryComments> inputComments;
          public string personId;

          public void TimecardQueryDataCoordinatorTestsInitialize()
          {
               testData = new TimeManagementTestData();
               inputTimecards = testData.BuildTimecardDtos();
               inputHistories = testData.BuildTimecardHistoryDtos();
               inputStatuses = testData.BuildTimecardStatusDtos();
               inputComments = testData.BuildTimeEntryCommentDtos();

               personId = inputTimecards[0].EmployeeId;
          }

          public TimecardQueryDataCoordinator BuildQuery()
          {
               return new TimecardQueryDataCoordinator(personId, inputTimecards, inputHistories, inputStatuses, inputComments);
          }

          [TestClass]
          public class ConstructorTests : TimecardQueryDataCoordinatorTests
          {

               [TestInitialize]
               public void Initialize()
               {
                    TimecardQueryDataCoordinatorTestsInitialize();

               }

               [TestMethod]
               public void PutsQueryResultsInDictionary_PaycycleIdKeyTest()
               {
                    var query = BuildQuery();
                    var payCycleId = testData.payCycleIds[0];
                    var expectedPayCycleTimecards = inputTimecards.Where(t => t.PayCycleId == payCycleId && t.EmployeeId == personId);
                    var results = query[payCycleId];

                    Assert.AreEqual(expectedPayCycleTimecards.Count(), results.Count());
               }

               [TestMethod]
               public void ResultsAreOrderedTest()
               {
                    var query = BuildQuery();
                    var payCycleId = testData.payCycleIds[0];


                    var results = query[payCycleId];
                    var orderedResults = query[payCycleId].OrderBy(qt => qt);

                    CollectionAssert.AreEqual(orderedResults.ToList(), results.ToList());
               }

               [TestMethod]
               public void QueryResultItemsMatchInputTimecardsForPersonTest()
               {
                    var query = BuildQuery();

                    foreach (var timecardDto in inputTimecards.Where(t => t.EmployeeId == personId))
                    {
                         var results = query[timecardDto.PayCycleId];
                         var expectedQueryResult = new TimecardQueryResultItem(timecardDto, null, null, null);
                         var matchingQueryResult = results.FirstOrDefault(r => r.TimecardJoiner == expectedQueryResult.TimecardJoiner);

                         Assert.IsNotNull(matchingQueryResult);
                    }
               }

               [TestMethod]
               public void QueryResultItemCount_Equals_InputTimecardsForPersonCountTest()
               {
                    var query = BuildQuery();

                    var payCycleId = testData.payCycleIds[0];
                    var expectedCount = inputTimecards.Where(t => t.EmployeeId == personId && t.PayCycleId == payCycleId).Count();
                    var actualCount = query[payCycleId].Count();

                    Assert.AreEqual(expectedCount, actualCount);

               }

               [TestMethod]
               public void NoQueryResultsIfFoobarPersonIdTest()
               {
                    personId = "foobar";
                    var query = BuildQuery();

                    foreach (var payCycleId in inputTimecards.Select(t => t.PayCycleId))
                    {
                         var result = query[payCycleId];
                         Assert.IsFalse(result.Any());
                    }
               }

               [TestMethod]
               public void AllQueryResultsIfNullPersonIdTest()
               {
                    personId = null;
                    var query = BuildQuery();

                    foreach (var payCycleId in inputTimecards.Select(t => t.PayCycleId))
                    {
                         var results = query[payCycleId];
                         var distinctEmployeeIds = results.Select(qr => qr.TimecardJoiner.EmployeeId).Distinct();

                         Assert.IsTrue(distinctEmployeeIds.Count() > 1);
                    }

                    foreach (var timecardDto in inputTimecards) //not filtering out any specific employee timecards, all timecards should be included in query
                    {
                         var results = query[timecardDto.PayCycleId];
                         var expectedQueryResult = new TimecardQueryResultItem(timecardDto, null, null, null);
                         var matchingQueryResult = results.FirstOrDefault(r => r.TimecardJoiner == expectedQueryResult.TimecardJoiner);

                         Assert.IsNotNull(matchingQueryResult);
                    }
               }

               [TestMethod]
               public void NoInputHistoryTest()
               {
                    inputHistories = null;
                    var query = BuildQuery();

                    var payCycleId = testData.payCycleIds[0];
                    var expectedCount = inputTimecards.Where(t => t.EmployeeId == personId && t.PayCycleId == payCycleId).Count();
                    var actualCount = query[payCycleId].Count();

                    Assert.AreEqual(expectedCount, actualCount);
               }

               [TestMethod]
               public void NoInputTimecardsTest()
               {
                    inputTimecards = null;
                    var query = BuildQuery();

                    var payCycleId = testData.payCycleIds[0];
                    var expectedCount = inputHistories.Where(t => t.EmployeeId == personId && t.PayCycleId == payCycleId && t.StatusAction == StatusAction.Paid).Count();
                    var actualCount = query[payCycleId].Count();

                    Assert.AreEqual(expectedCount, actualCount);

                    Assert.IsTrue(query[payCycleId].All(r => r.IsHistorical));
               }

               [TestMethod]
               public void ExtraHistoryTest()
               {
                    inputHistories.Add(new TimecardHistory2()
                    {
                         Id = "foobar",
                         EmployeeId = personId,
                         EndDate = new DateTime(1986, 6, 5),
                         StartDate = new DateTime(1986, 6, 12),
                         PayCycleId = testData.payCycleIds[0],
                         PositionId = testData.positionIds[0],
                         PeriodEndDate = new DateTime(1986, 6, 1),
                         PeriodStartDate = new DateTime(1986, 6, 30),
                         StatusAction = StatusAction.Paid,
                         TimeEntryHistories = new List<TimeEntryHistory2>(),
                         Timestamp = new Colleague.Dtos.Base.Timestamp() { AddDateTime = new DateTime(2016, 10, 13) }
                    });

                    var query = BuildQuery();

                    var payCycleId = testData.payCycleIds[0];
                    var expectedCount = inputTimecards.Where(t => t.EmployeeId == personId && t.PayCycleId == payCycleId).Count() + 1;

                    var actualCount = query[payCycleId].Count();

                    Assert.AreEqual(expectedCount, actualCount);
               }

               [TestMethod]
               public void NoStatuses_HasNoEffectTest()
               {
                    inputStatuses = null;
                    var query = BuildQuery();
                    var payCycleId = testData.payCycleIds[0];
                    var expectedCount = inputTimecards.Where(t => t.EmployeeId == personId && t.PayCycleId == payCycleId).Count();
                    var actualCount = query[payCycleId].Count();

                    Assert.AreEqual(expectedCount, actualCount);

               }

               [TestMethod]
               public void NoComments_HasNoEffectTest()
               {
                    inputComments = null;
                    var query = BuildQuery();
                    var payCycleId = testData.payCycleIds[0];
                    var expectedCount = inputTimecards.Where(t => t.EmployeeId == personId && t.PayCycleId == payCycleId).Count();
                    var actualCount = query[payCycleId].Count();

                    Assert.AreEqual(expectedCount, actualCount);
               }

               [TestMethod]
               public void NoQueryResultsIfNoTimecardsAndNoHistoryTest()
               {
                    var potentialPayCycles = inputTimecards.Select(t => t.PayCycleId);
                    inputTimecards = null;
                    inputHistories = null;
                    var query = BuildQuery();

                    foreach (var payCycleId in potentialPayCycles)
                    {
                         var result = query[payCycleId];
                         Assert.IsFalse(result.Any());
                    }
               }
          }


          [TestClass]
          public class GetOrderedResultsforPayPeriodTests : TimecardQueryDataCoordinatorTests
          {

               [TestInitialize]
               public void Initialize()
               {
                    TimecardQueryDataCoordinatorTestsInitialize();
               }

               [TestMethod]
               [ExpectedException(typeof(ArgumentNullException))]
               public void PayCycleIdRequiredTest()
               {
                    var query = BuildQuery();
                    query.GetOrderedResultsForPayPeriod(null, DateTime.Today);
               }

               [TestMethod]
               public void FoobarPayCycleIdTest()
               {
                    var query = BuildQuery();
                    var result = query.GetOrderedResultsForPayPeriod("foobar", DateTime.Today);
                    Assert.IsFalse(result.Any());
               }

               [TestMethod]
               public void GetsOrderedResultsTest()
               {
                    personId = null;
                    var payCycleId = testData.payCycleIds[0];
                    var periodStartDate = inputTimecards[0].PeriodStartDate;
                    var periodEndDate = inputTimecards[0].PeriodEndDate;

                    inputTimecards.ForEach(t => { t.PeriodStartDate = periodStartDate; t.PeriodEndDate = periodEndDate; });
                    var query = BuildQuery();
                    var orderedResults = query.GetOrderedResultsForPayPeriod(payCycleId, periodEndDate);

                    Assert.AreEqual(inputTimecards.Count(), orderedResults.Count());

                    CollectionAssert.AreEqual(orderedResults.OrderBy(result => result).ToList(), orderedResults.ToList());
               }
          }
     }
}
