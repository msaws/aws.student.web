﻿//using Ellucian.Colleague.Dtos.Base;
//using Ellucian.Colleague.Dtos.HumanResources;
//using Ellucian.Colleague.Dtos.TimeManagement;
//using Ellucian.Web.Student.Areas.TimeManagement.Models;
//using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
//{
//    [TestClass]
//    public class TimecardQueryTests
//    {

//        string personId;
//        List<Timecard> timecards;
//        List<TimecardStatus> timecardStatuses;
//        List<TimeEntryComments> comments;
//        List<TimecardHistory> timecardHistories;

//        public void SetupTimecardQueryTestData()
//        {
//            personId = "00123";
//            timecards = new List<Timecard>(){
//                    new Timecard(){
//                         EmployeeId = "00123",
//                         EndDate = new DateTime(16,9,7),
//                         Id = "001",
//                         PayCycleId = "M1",
//                         PeriodEndDate = new DateTime(16, 9, 30),
//                         PeriodStartDate = new DateTime(16, 9, 1),
//                         PositionId = "JANITOR",
//                         StartDate = new DateTime(16,9,1),
//                         TimeEntries = new List<TimeEntry>(){
//                              new TimeEntry(){
//                                   EarningsTypeId = "TEST",
//                                   Id = "1",
//                                   TimecardId = "001",
//                                   WorkedDate = new DateTime(16, 9, 1),
//                                   WorkedTime = new TimeSpan(8, 0, 0)
//                              }
//                         }
//                    },
//                    new Timecard(){
//                         EmployeeId = "00123",
//                         EndDate = new DateTime(16,9,14),
//                         Id = "002",
//                         PayCycleId = "M1",
//                         PeriodEndDate = new DateTime(16, 9, 30),
//                         PeriodStartDate = new DateTime(16, 9, 1),
//                         PositionId = "JANITOR",
//                         StartDate = new DateTime(16,9,8),
//                         TimeEntries = new List<TimeEntry>(){
//                              new TimeEntry(){
//                                   EarningsTypeId = "TEST",
//                                   Id = "1",
//                                   TimecardId = "002",
//                                   WorkedDate = new DateTime(16, 9, 1),
//                                   WorkedTime = new TimeSpan(8, 0, 0)
//                              }
//                         }
//                    }
//               };
//            timecardStatuses = new List<TimecardStatus>(){
//                    new TimecardStatus(){
//                         ActionerId = "00123",
//                         ActionType = 0,
//                         HistoryId = "history1",
//                         Id = "status1",
//                         TimecardId = "001",
//                         Timestamp = new Timestamp()
//                    },

//                    new TimecardStatus(){
//                         ActionerId = "00123",
//                         ActionType = 0,
//                         HistoryId = "history2",
//                         Id = "status2",
//                         TimecardId = "002",
//                         Timestamp = new Timestamp()
//                    }
//               };
//            comments = new List<TimeEntryComments>(){
//                    new TimeEntryComments(){

//                         CommentAuthorName = "John Goodman",
//                         Comments = "Test Comment",
//                         CommentsTimestamp = new Timestamp(),
//                         EmployeeId = "00123",
//                         Id = "comment1",
//                         PayCycleId = "M1",
//                         PayPeriodEndDate = new DateTime(16, 9, 30),
//                         PositionId = "JANITOR",
//                         TimecardId = "001",
//                         TimecardStatusId = "status1"
//                    }
//               };
//            timecardHistories = new List<TimecardHistory>(){
//                    // history for previous pay period
//                    new TimecardHistory(){
//                         EmployeeId = "00123",
//                         EndDate = new DateTime(16,8,31),
//                         Id = "history0",
//                         PayCycleId = "M1",
//                         PeriodEndDate = new DateTime(16, 8, 31),
//                         PeriodStartDate = new DateTime(16, 8, 1),
//                         PositionId = "JANITOR",
//                         StartDate = new DateTime(16,9,25),
//                         StatusAction = StatusAction.Paid,
//                         TimeEntryHistories = new List<TimeEntryHistory>(){
//                              new TimeEntryHistory(){
//                                   EarningsTypeId = "TEST",
//                                   Id = "timeentryhistory0",
//                                   TimecardHistoryId = "history0",
//                                   WorkedDate = new DateTime(16, 9, 1),
//                                   WorkedTime = new TimeSpan(8, 0, 0)
//                              }
//                         },
//                         Timestamp = new Timestamp(){
//                              AddDateTime = new DateTimeOffset(16, 8, 31, 0, 0, 0, new TimeSpan(0))
//                         }
//                    },
//                    //histories for submitted timecards
//                    new TimecardHistory(){
//                         EmployeeId = "00123",
//                         EndDate = new DateTime(16,9,7),
//                         Id = "history1",
//                         PayCycleId = "M1",
//                         PeriodEndDate = new DateTime(16, 9, 30),
//                         PeriodStartDate = new DateTime(16, 9, 1),
//                         PositionId = "JANITOR",
//                         StartDate = new DateTime(16,9,1),
//                         StatusAction = StatusAction.Approved,
//                         TimeEntryHistories = new List<TimeEntryHistory>(){
//                              new TimeEntryHistory(){
//                                   EarningsTypeId = "TEST",
//                                   Id = "timeentryhistory1",
//                                   TimecardHistoryId = "history1",
//                                   WorkedDate = new DateTime(16, 9, 1),
//                                   WorkedTime = new TimeSpan(8, 0, 0)
//                              }
//                         },
//                         Timestamp = new Timestamp(){
//                              AddDateTime = new DateTimeOffset(16, 9, 7, 0, 0, 0, new TimeSpan(0))
//                         }
//                    },
//                    new TimecardHistory(){
//                         EmployeeId = "00123",
//                         EndDate = new DateTime(16,9,14),
//                         Id = "history2",
//                         PayCycleId = "M1",
//                         PeriodEndDate = new DateTime(16, 9, 30),
//                         PeriodStartDate = new DateTime(16, 9, 1),
//                         PositionId = "JANITOR",
//                         StartDate = new DateTime(16,9,8),
//                         StatusAction = StatusAction.Approved,
//                         TimeEntryHistories = new List<TimeEntryHistory>(){
//                              new TimeEntryHistory(){
//                                   EarningsTypeId = "TEST",
//                                   Id = "timeentryhistory2",
//                                   TimecardHistoryId = "history2",
//                                   WorkedDate = new DateTime(16, 9, 1),
//                                   WorkedTime = new TimeSpan(8, 0, 0)
//                              }
//                         },
//                         Timestamp = new Timestamp(){
//                              AddDateTime = new DateTimeOffset(16, 9, 14, 0, 0, 0, new TimeSpan(0))
//                         }
                         
//                     }
//               };
//        }
//        [TestMethod]
//        public void buildTimecardQuery()
//        {
//            SetupTimecardQueryTestData();
//            var timecardQuery = new TimecardQueryDataCoordinator(personId, timecards, timecardHistories, timecardStatuses, comments);
//            Assert.IsNotNull(timecardQuery);
//        }

//        [TestMethod]
//        public void TestFindTimecardQueryResult()
//        {
//            // set up data for the test
//            SetupTimecardQueryTestData();

//            var personPosition = new PersonPosition();
//            var position = new Position()
//            {
//                Id = "JANITOR"
//            };
//            var personPositionWageList = new List<PersonPositionWage>();
//            var payCycle = new PayCycle()
//            {
//                Id = "M1"
//            };
//            var positionQueryResultItem = new PositionQueryResultItem(personPosition, position, personPositionWageList, payCycle);
//            var timecardQuery = new TimecardQueryDataCoordinator(personId, timecards, timecardHistories, timecardStatuses, comments);

//            var startDate = new DateTime(16, 9, 1);
//            var endDate = new DateTime(16, 9, 7);

//            // test
//            var result = timecardQuery.FindTimecardQueryResult(positionQueryResultItem, startDate, endDate);

//            // assert
//            Assert.IsNotNull(result);
//            Assert.AreEqual("001", result.Timecard.Id);
//            Assert.AreEqual(startDate, result.Timecard.StartDate);
//            Assert.AreEqual(endDate, result.Timecard.EndDate);
//        }

//        public List<TimecardQueryResultItem> FindTimecardQueryResultsForPositionAndPayPeriod(PositionQueryResultItem positionQueryResultItem, DateTime periodEndDate)
//        [TestMethod]
//        public void TestFindTimecardQueryResultsForPositionAndPayPeriod()
//        {
//            // set up data for the test
//            SetupTimecardQueryTestData();

//            var personPosition = new PersonPosition();
//            var position = new Position()
//            {
//                Id = "JANITOR"
//            };
//            var personPositionWageList = new List<PersonPositionWage>();
//            var payCycle = new PayCycle()
//            {
//                Id = "M1"
//            };
//            var positionQueryResultItem = new PositionQueryResultItem(personPosition, position, personPositionWageList, payCycle);
//            var timecardQuery = new TimecardQueryResultItem(personId, timecards, timecardHistories, timecardStatuses, comments);


//            var startDate = new DateTime(16, 9, 23);
//            var periodEndDate = new DateTime(16, 9, 30);

//            //test
//            var result = timecardQuery.FindTimecardQueryResult(positionQueryResultItem, startDate, periodEndDate);

//            //assert
//            Assert.IsNotNull(result);
//            Assert.IsTrue(result.Any());
//            Assert.AreEqual(2, result.Count());
//        }

//         public IEnumerable<TimecardQueryResultItem> FindTimecardQueryResultsForPayPeriod(string payCycleId, DateTime periodEndDate)
//        [TestMethod]
//        public void TestFindTimecardQueryResultsForPayPeriod()
//        {
//            // set up data for the test
//            SetupTimecardQueryTestData();
//            var timecardQuery = new TimecardQuery(personId, timecards, timecardStatuses, comments, timecardHistories);
//            var periodEndDate = new DateTime(16, 9, 30);
//            var payCycleId = "M1";

//            // test
//            var result = timecardQuery.FindTimecardQueryResultsForPayPeriod(payCycleId, periodEndDate);

//            // assert
//            Assert.IsNotNull(result);
//            Assert.IsTrue(result.Any());
//            Assert.AreEqual(2, result.Count());

//        }

//        [TestMethod]
//        public void TestFilterTimecardHistories()
//        {
//            // create a TimecardHistory that we can filter out 
//            SetupTimecardQueryTestData();
//            timecardHistories.Add(
//                 new TimecardHistory()
//                 {
//                     EmployeeId = "00124",
//                     EndDate = new DateTime(16, 9, 7),
//                     Id = "history1",
//                     PayCycleId = "M1",
//                     PeriodEndDate = new DateTime(16, 9, 30),
//                     PeriodStartDate = new DateTime(16, 9, 1),
//                     PositionId = "JANITOR",
//                     StartDate = new DateTime(16, 9, 1),
//                     TimeEntryHistories = new List<TimeEntryHistory>(),
//                     Timestamp = new Timestamp()
//                     {
//                         AddDateTime = new DateTimeOffset(16, 9, 6, 0, 0, 0, new TimeSpan(0))
//                     }
//                 });
//            var timecardQuery = new TimecardQuery(personId, timecards, timecardStatuses, comments, timecardHistories);

//            // test
//            var result = timecardQuery.FilterTimecardHistories(timecardHistories);

//            // assert
//            Assert.AreEqual(3, result.Count());
//            Assert.IsNull(result.FirstOrDefault(x => x.EmployeeId == "00124"));
//        }

//    }







//}
