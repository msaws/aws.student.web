﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using System.Reflection;


namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    public class TimeManagementTestData
    {

        #region EMPLOYEE TIME ENTRY
        // timecard model
        public TimecardQueryResultItem timecardQueryResult;
        public PositionQueryResultItem positionQueryResult;
        public IEnumerable<EarningsType> earningsTypes;
        public IEnumerable<PersonEmploymentStatus> perstats;
        public IEnumerable<TimecardStatus> statuses;
        public IEnumerable<TimeEntryComments> commentsList;
        public PersonPosition personPosition;
        public IEnumerable<PersonPositionWage> perPosWages;
        public PayCycle payCycle;
        public Position position;
        public DateTime timeFrameStartDate;
        public TimecardModel timecardModel;
        // time entry
        public TimeEntry2 timeEntry2Dto;

        // timecard
        public Timecard2 timecard2Dto;
        public string timecardId;
        public string employeeId;
        public string positionTitle;
        public string positionId;
        public bool isAssociatedPositionPrimary;
        public string supervisor;
        public string payCycleId;
        public DateTime startDate;
        public DateTime endDate;
        public DateTime periodStartDate;
        public DateTime periodEndDate;
        //public string OvertimeCalcMethod;
        public string comments;
        public Timestamp timestamp;
        public int? timecardStatus;
        public WorkEntry workEntry;
        public EarningsType earningsType;

        // overtime calculation result model
        public OvertimeCalculationResult overtimeCalculationResultdto;
        public OvertimeCalculationResultModel overtimeCalculationResultmodel;
        public OvertimeThresholdResult threshold1;
        public OvertimeThresholdResult threshold2;
        public DailyOvertimeAllocation allocation1;
        public DailyOvertimeAllocation allocation2;

        // time period model
        public TimePeriodModel timePeriodModel;
        // using 'StartDate' from timecard
        // using 'EndDate' from timecard
        //public TimecardPositionStatusModel timecardPositionStatusModel;

        // time period collection model
        public TimePeriodCollectionModel timePeriodCollectionModel;
        public TimecardQueryDataCoordinator timecardQuery;
        public PositionQuery positionQuery;

        // timecard position status model
        public TimecardPositionStatusModel timecardPositionStatusModel;
        public bool isPositionPrimary;
        public int? currentStatus;

        //timecard collection model
        public TimecardCollectionModel timecardCollectionModel;
        public Dictionary<string, HumanResourceDemographics> employeeDictionary;
        public bool isSupervisor;
        public string personId;

        public IEnumerable<TimecardHistory2> timecardHistories;
        public IEnumerable<TimeEntryHistory2> timeEntryHistories;

        public IEnumerable<Department> departments = new List<Department>()
          {
              new Department()
              {
                  Code = "FOO",
                  Description = "Foo Department",
              },
              new Department()
              {
                  Code = "BAR",
                  Description = "Bar Department"
              }
          };
        public IEnumerable<Location> locations = new List<Location>()
          {
              new Location()
              {
                  Code = "MAIN",
                  Description = "Main Campus",
              },
              new Location()
              {
                  Code = "WEST",
                  Description = "West Campus"
              }
          };

        public TimeManagementTestData()
        {

        }

        public string[] personIds = { "24601", "0003914", "0000010" };
        // private string[] timecardIds = { "001" };
        public string[] positionIds = { "QWERTY", "DVORAK" };
        public string[] payCycleIds = { "TMA-1" };

        public List<Timecard2> BuildTimecardDtos()
        {
            var timecardDtos = new List<Timecard2>();
            foreach (var personId in personIds)
            {
                var timecardId = personId.GetHashCode() ^ personIds.ToList().IndexOf(personId).GetHashCode();
                var timecard = new Timecard2()
                {
                    Id = timecardId.ToString(),
                    EmployeeId = personId,
                    EndDate = new DateTime(2016, 03, 07),
                    PayCycleId = payCycleIds[0],
                    PeriodEndDate = new DateTime(2016, 03, 14),
                    PeriodStartDate = new DateTime(2016, 03, 01),
                    PositionId = positionIds[0],
                    StartDate = new DateTime(2016, 03, 01),
                    Timestamp = new Timestamp()
                    {
                        AddDateTime = new DateTime(2016, 03, 05),
                        ChangeDateTime = new DateTime(2016, 03, 05),
                        AddOperator = personId,
                        ChangeOperator = personId
                    },
                    TimeEntries = new List<TimeEntry2>()
                    {
                        new TimeEntry2()
                        {
                            Id = (timecardId ^ 1).GetHashCode().ToString(),
                            EarningsTypeId = "0101",
                            InDateTime = new DateTime(2016, 03, 05, 8, 0, 0),
                            OutDateTime = new DateTime(2016, 03, 05, 16, 0, 0),
                            WorkedDate = new DateTime(2016, 03, 05),
                            WorkedTime = new TimeSpan(8, 0, 0),
                            PersonLeaveId = "21",
                            ProjectId = "12",
                            TimecardId = timecardId.ToString(),
                            Timestamp = new Timestamp()
                            {
                                AddDateTime = new DateTime(2016, 03, 05),
                                ChangeDateTime = new DateTime(2016, 03, 05),
                                AddOperator = personId,
                                ChangeOperator = personId
                            }
                        }
                    }
                };

                Timecard2 timecard2 = new Timecard2();
                CopyObjectUtility<Timecard2>(timecard, timecard2);
                timecard2.PositionId = positionIds[1];

                timecardDtos.Add(timecard);
                timecardDtos.Add(timecard2);
            }


            return timecardDtos;
        }

        public List<TimecardHistory2> BuildTimecardHistoryDtos()
        {
            var timecards = BuildTimecardDtos().ToList();
            var histories = new List<TimecardHistory2>();
            foreach (var timecardDto in timecards)
            {
                var historyId = timecardDto.GetHashCode().ToString();
                var history = new TimecardHistory2()
                {
                    Id = historyId,
                    EmployeeId = timecardDto.EmployeeId,
                    EndDate = timecardDto.EndDate,
                    PayCycleId = timecardDto.PayCycleId,
                    PositionId = timecardDto.PositionId,
                    PeriodStartDate = timecardDto.PeriodStartDate,
                    PeriodEndDate = timecardDto.PeriodEndDate,
                    StartDate = timecardDto.StartDate,
                    StatusAction = (StatusAction)(timecards.IndexOf(timecardDto) % 5),
                    Timestamp = timecardDto.Timestamp,
                    TimeEntryHistories = timecardDto.TimeEntries.Select(te => new TimeEntryHistory2()
                    {
                        Id = te.Id.GetHashCode().ToString(),
                        EarningsTypeId = te.EarningsTypeId,
                        InDateTime = te.InDateTime,
                        OutDateTime = te.OutDateTime,
                        PersonLeaveId = te.PersonLeaveId,
                        ProjectId = te.ProjectId,
                        TimecardHistoryId = historyId,
                        Timestamp = te.Timestamp,
                        WorkedTime = te.WorkedTime,
                        WorkedDate = te.WorkedDate
                    }).ToList()
                };

                histories.Add(history);
            }
            return histories;
        }

        public List<TimecardStatus> BuildTimecardStatusDtos()
        {
            var timecards = BuildTimecardDtos().ToList();
            var statuses = new List<TimecardStatus>();
            foreach (var timecard in timecards)
            {
                var status1 = new TimecardStatus()
                {
                    Id = (timecard.Id.GetHashCode() ^ timecard.GetHashCode()).ToString(),
                    ActionerId = timecard.EmployeeId,
                    ActionType = StatusAction.Submitted,
                    TimecardId = timecard.Id,
                    Timestamp = timecard.Timestamp,
                };
                var status2 = new TimecardStatus()
                {
                    Id = (timecard.Id.GetHashCode() ^ timecard.GetHashCode()).ToString(),
                    ActionerId = timecard.EmployeeId,
                    ActionType = StatusAction.Unsubmitted,
                    TimecardId = timecard.Id,
                    Timestamp = timecard.Timestamp,
                };
                status2.Timestamp.AddDateTime.AddDays(1);
                statuses.Add(status1);
                statuses.Add(status2);
            }
            return statuses;
        }

        public List<TimeEntryComments> BuildTimeEntryCommentDtos()
        {
            var timecards = BuildTimecardDtos().ToList();
            var comments = new List<TimeEntryComments>();
            foreach (var timecard in timecards)
            {
                //for a specific timecard
                var comment1 = new TimeEntryComments()
                {
                    Id = (timecard.Id.GetHashCode() ^ timecard.Timestamp.GetHashCode()).ToString(),
                    CommentAuthorName = "foobar",
                    Comments = "This is a comment",
                    CommentsTimestamp = timecard.Timestamp,
                    EmployeeId = timecard.EmployeeId,
                    TimecardId = timecard.Id,
                    PayCycleId = timecard.PayCycleId,
                    PositionId = timecard.PositionId,
                    PayPeriodEndDate = timecard.PeriodEndDate
                };

                //for a group of timecards (as if a supervisor entered a comment)
                var comment2 = new TimeEntryComments()
                {
                    Id = (timecard.Id.GetHashCode() ^ timecard.Timestamp.GetHashCode()).ToString(),
                    CommentAuthorName = "raboof",
                    Comments = "This is a supervisor comment",
                    CommentsTimestamp = timecard.Timestamp,
                    EmployeeId = timecard.EmployeeId,
                    PayCycleId = timecard.PayCycleId,
                    PositionId = timecard.PositionId,
                    PayPeriodEndDate = timecard.PeriodEndDate
                };
                comment2.CommentsTimestamp.AddDateTime.AddHours(1);
                comments.Add(comment1);
                comments.Add(comment2);
            }

            return comments;
        }


        public void CopyObjectUtility<T>(T copyFrom, T copyTo)
        {

            foreach (var sourcePropertyInfo in copyFrom.GetType().GetProperties())
            {
                var destPropertyInfo = copyTo.GetType().GetProperty(sourcePropertyInfo.Name);

                destPropertyInfo.SetValue(
                    copyTo,
                    sourcePropertyInfo.GetValue(copyFrom, null),
                    null);
            }
        }

        public void SetupPersonData()
        {
            personId = "24601";
            isSupervisor = false;
        }

        public void SetupTimecardModelTests()
        {
            timeEntry2Dto = new TimeEntry2()
            {
                Id = "10",
                EarningsTypeId = "0101",
                InDateTime = new DateTime(2016, 03, 05, 8, 0, 0),
                OutDateTime = new DateTime(2016, 03, 05, 16, 0, 0),
                WorkedDate = new DateTime(2016, 03, 05),
                WorkedTime = new TimeSpan(8, 0, 0),
                PersonLeaveId = "21",
                ProjectId = "12",
                TimecardId = "001",
                Timestamp = new Timestamp()
                {
                    AddDateTime = new DateTime(2016, 03, 05),
                    ChangeDateTime = new DateTime(2016, 03, 05),
                    AddOperator = "24601",
                    ChangeOperator = "24601"
                }
            };
            timecard2Dto = new Timecard2()
            {
                Id = "001",
                EmployeeId = "24601",
                EndDate = new DateTime(2016, 03, 07),
                PayCycleId = "TMA-1",
                PeriodEndDate = new DateTime(2016, 03, 14),
                PeriodStartDate = new DateTime(2016, 03, 01),
                PositionId = "SAMURAI",
                StartDate = new DateTime(2016, 03, 01),
                Timestamp = new Timestamp(),
                TimeEntries = new List<TimeEntry2>()
                {
                    timeEntry2Dto
                }
            };
            statuses = new List<TimecardStatus>()
            {
                new TimecardStatus()
                {
                    ActionerId = "24601",
                    ActionType = StatusAction.Submitted,
                    HistoryId = "001",
                    Id = "AWS",
                    TimecardId = "001",
                    Timestamp = new Timestamp()
                    {
                        AddDateTime = new DateTime(2016,03,05),
                        ChangeDateTime = new DateTime(2016,03,05),
                        AddOperator = "24601",
                        ChangeOperator = "24601"
                    }
                }
            };
            commentsList = new List<TimeEntryComments>()
            {
                new TimeEntryComments()
                {
                    Id = "q",
                    CommentAuthorName = "Barnabus J. Behooviuth",
                    EmployeeId = "24601",
                    PayCycleId = "TMA-1",
                    TimecardId = "001",
                    PayPeriodEndDate = new DateTime(2016, 03, 14),
                    PositionId = "SAMURAI",
                    TimecardStatusId = "AWS",
                    Comments = "a bb ccc dddd eeee fffff gggggg hhhhhhhh iiiiiiii",
                    CommentsTimestamp = new Timestamp()
                    {
                        AddDateTime = new DateTime(2016,03,05),
                        ChangeDateTime = new DateTime(2016,03,05),
                        AddOperator = "24601",
                        ChangeOperator = "24601"
                    }
                }
            };
            personPosition = new PersonPosition()
            {
                AlternateSupervisorId = "",
                EndDate = null,
                Id = "PPID",
                LastWebTimeEntryPayPeriodEndDate = null,
                MigrationDate = null,
                NonEmployeePosition = false,
                PersonId = "24601",
                PositionId = "SAMURAI",
                StartDate = new DateTime(2010, 10, 10),
                SupervisorId = "1776",

            };
            position = new Position()
            {
                Id = "SAMURAI",
                AlternateSupervisorPositionId = "",
                EndDate = null,
                IsExempt = false,
                IsSalary = false,
                PositionPayScheduleIds = new List<string>() { "zz", "xx", "cc" },
                ShortTitle = "Samurai",
                Title = "loyal servant of the tokugawa shogunate",
                StartDate = new DateTime(2009, 07, 07),
                SupervisorPositionId = "SHOGUN",
                TimecardType = TimecardType.Summary,
                PositionDept = "FOO",
                PositionLocation = "MAIN"
                
            };
            perPosWages = new List<PersonPositionWage>()
            {
                new PersonPositionWage()
                {
                    Id = "w",
                    EndDate = null,
                    StartDate = new DateTime(2010,10,10),
                    FundingSources = new List<PositionFundingSource>()
                    {
                        new PositionFundingSource()
                        {
                            FundingOrder = 1,
                            FundingSourceId = "fun!",
                            ProjectId = "12",
                        }
                    },
                    IsPaySuspended = false,
                    PayClassId = "BWH",
                    PayCycleId = "TMA-1",
                    PersonId = "24601",
                    PersonPositionId = "PPID",
                    PositionId = "SAMURAI",
                    PositionPayDefaultId = "def",
                    RegularWorkEarningsTypeId = "e"
                }
            };
            payCycle = new PayCycle()
            {
                AnnualPayFrequency = 26,
                Description = "Bi-weekly",
                Id = "TMA-1",
                PayClassIds = new List<string>() { "BWH" },
                PayPeriods = new List<PayPeriod>()
                {
                    new PayPeriod()
                    {
                        EmployeeTimecardCutoffDateTime = new DateTime(2020,10,10),
                        SupervisorTimecardCutoffDateTime = new DateTime(2021,10,10),
                        EndDate = new DateTime(2016,03,14),
                        StartDate = new DateTime(2016, 03,01),
                        Status = PayPeriodStatus.Prepared
                    }
                },
                WorkWeekStartDay = DayOfWeek.Monday
            };

            positionQueryResult = new PositionQueryResultItem(personPosition, position, perPosWages, payCycle);
            earningsTypes = new List<EarningsType>() {
                new EarningsType()
                {
                    Category = EarningsCategory.Regular,
                    IsActive = true,
                    Description = "$$",
                    Id = "e",
                    Method = EarningsMethod.None
                }
            };
            perstats = new List<PersonEmploymentStatus>() {
                new PersonEmploymentStatus()
                {
                    EndDate = null,
                    Id = "i",
                    PersonId = "24601",
                    PersonPositionId = "PPID",
                    PrimaryPositionId = "SAMURAI",
                    StartDate = new DateTime(2010,10,10)
                }
            };
            timeFrameStartDate = new DateTime(2016, 03, 01);

            employeeDictionary = new Dictionary<string, HumanResourceDemographics>();
            employeeDictionary.Add("24601", new HumanResourceDemographics() { Id = "24601", LastName = "Monk" });
            employeeDictionary.Add("0003914", new HumanResourceDemographics() { Id = "0003914", LastName = "Waller" });
            employeeDictionary.Add("0000010", new HumanResourceDemographics() { Id = "0000010", LastName = "Marcellus" });

            }

        public void SetupOvertimeCalculationResultModelTests()
        {
            payCycleId = "TMA-1";
            allocation1 = new DailyOvertimeAllocation()
            {
                Date = new DateTime(2000, 10, 12),
                Overtime = new TimeSpan(1, 2, 3)
            };
            allocation2 = new DailyOvertimeAllocation()
            {
                Date = new DateTime(2000, 10, 14),
                Overtime = new TimeSpan(1, 2, 3)
            };

            threshold1 = new OvertimeThresholdResult()
            {
                EarningsTypeId = "Regular",
                Factor = 1.63m,
                TotalOvertime = new TimeSpan(4, 5, 6),
                DailyAllocations = new List<DailyOvertimeAllocation>()
                {
                    allocation1, allocation2
                }

            };
            threshold2 = new OvertimeThresholdResult()
            {
                EarningsTypeId = "Regular",
                Factor = 1.21m,
                TotalOvertime = new TimeSpan(7, 8, 9),
                DailyAllocations = new List<DailyOvertimeAllocation>()
                {
                    allocation2, allocation1
                }

            };

            overtimeCalculationResultdto = new OvertimeCalculationResult()
            {
                PersonId = "24601",
                StartDate = new DateTime(2000, 10, 11),
                EndDate = new DateTime(2000, 10, 16),
                ResultDateTime = DateTime.UtcNow,
                Thresholds = new List<OvertimeThresholdResult>()
                {
                    threshold1, threshold2
                }
            };
        }

        public void SetupTimePeriodModelTestData()
        {
            this.SetupTimecardModelTests();
            timePeriodModel = new TimePeriodModel(startDate, endDate);
            timecardPositionStatusModel = new TimecardPositionStatusModel(positionQueryResult);
        }

        public void SetupTimecardHistories()
        {
            if (timecard2Dto == null || timeEntry2Dto == null)
                this.SetupTimecardModelTests();

            timeEntryHistories = new List<TimeEntryHistory2>()
            {
                new TimeEntryHistory2()
                {
                    Id = "te1",
                    EarningsTypeId = timeEntry2Dto.EarningsTypeId,
                    InDateTime = timeEntry2Dto.InDateTime,
                    OutDateTime =timeEntry2Dto.OutDateTime,
                    PersonLeaveId = timeEntry2Dto.PersonLeaveId,
                    ProjectId = timeEntry2Dto.ProjectId,
                    TimecardHistoryId = "th1",
                    Timestamp = new Timestamp(),
                    WorkedDate = timeEntry2Dto.WorkedDate,
                    WorkedTime = timeEntry2Dto.WorkedTime
                }
            };

            timecardHistories = new List<TimecardHistory2>()
            {
                new TimecardHistory2()
                {
                    Id = "th1",
                    StatusAction = StatusAction.Submitted,
                    EmployeeId = timecard2Dto.EmployeeId,
                    EndDate = timecard2Dto.EndDate,
                    PayCycleId = timecard2Dto.PayCycleId,
                    PeriodEndDate = timecard2Dto.PeriodEndDate,
                    PeriodStartDate = timecard2Dto.PeriodStartDate,
                    PositionId = timecard2Dto.PositionId,
                    StartDate = timecard2Dto.StartDate,
                    Timestamp = new Timestamp(),
                    TimeEntryHistories = this.timeEntryHistories.ToList()
                }
            };
        }

        public void SetupTimePeriodCollectionModelTestData()
        {
            this.SetupPersonData();
            this.SetupTimecardModelTests();
            this.SetupTimecardHistories();
            var dtos = new List<Timecard2>() { timecard2Dto };
            var perposes = new List<PersonPosition>() { personPosition };
            var poses = new List<Position>() { position };
            var cycles = new List<PayCycle>() { payCycle };

            payCycleId = "TMA-1";
            periodEndDate = new DateTime(2016, 03, 14);
            periodStartDate = new DateTime(2016, 03, 01);
            startDate = periodStartDate;
            endDate = new DateTime(2016, 03, 06);
            positionId = "SAMURAI";

            timecardQuery = new TimecardQueryDataCoordinator(personId, dtos, timecardHistories, statuses, commentsList);
            positionQuery = new PositionQuery(personId, perposes, poses, perPosWages, cycles, isSupervisor);
            timePeriodCollectionModel = new TimePeriodCollectionModel(payCycleId, timecardQuery, positionQuery, perstats);
        }

        public void SetupTimecardPositionStatusModelTests()
        {
            this.SetupTimecardModelTests();

            positionId = position.Id;
            positionTitle = position.Title;
            payCycleId = payCycle.Id;
            isPositionPrimary = true;
            currentStatus = 0;

        }

        public void SetupTimecardCollectionModelTests()
        {
            employeeId = "24601";
            startDate = new DateTime(2016, 03, 01);
            endDate = new DateTime(2016, 03, 7);
            employeeDictionary = new Dictionary<string, HumanResourceDemographics>();
            employeeDictionary.Add("24601", new HumanResourceDemographics() { FirstName = "Jean", LastName = "Valjean", Id = "24601", MiddleName = "Q", PreferredName = "Vic" });

            this.SetupPersonData();
            this.SetupTimecardModelTests();
            this.SetupTimecardHistories();
            var dtos = new List<Timecard2>() { timecard2Dto };
            var perposes = new List<PersonPosition>() { personPosition };
            var poses = new List<Position>() { position };
            var cycles = new List<PayCycle>() { payCycle };

            payCycleId = "TMA-1";
            periodEndDate = new DateTime(2016, 03, 14);
            periodStartDate = new DateTime(2016, 03, 01);
            positionId = "SAMURAI";

            timecardQuery = new TimecardQueryDataCoordinator(personId, dtos, timecardHistories, statuses, commentsList);
            positionQuery = new PositionQuery(personId, perposes, poses, perPosWages, cycles, isSupervisor);


            var timecardQueryResultContext = timecardQuery.FindTimecardQueryResultContext(positionQueryResult, startDate, endDate);
            timecardModel = new TimecardModel(timecardQueryResultContext, positionQueryResult, earningsTypes, perstats, departments, locations, employeeDictionary, timeFrameStartDate);
            timecardCollectionModel = new TimecardCollectionModel(employeeId, payCycleId, startDate, endDate, employeeDictionary, positionQuery, timecardQuery, perstats, earningsTypes, departments, locations);
        }
        #endregion

        #region SUPERVISOR TIME APPROVAL
        // supervisor info
        public string superId;
        public PersonPosition superPerPos;
        public IEnumerable<PersonPosition> superPerPoses;
        public Position superPos;
        public IEnumerable<Position> superPoses;
        public PersonPositionWage superPerWag;
        public IEnumerable<PersonPositionWage> superPerWags;
        public PayCycle superCycle;
        public IEnumerable<PayCycle> superCycles;
        public bool isSuperSupervisor;
        // approval detail model
        public ApprovalDetailModel approvalDetailModel;
        public PositionQuery supervisorPositionQuery;
        //public string payCycleId; 
        //DateTime payPeriodEndDate;
        //public PositionQuery positionQuery;
        //public TimecardQuery timecardQuery;
        //public IEnumerable<EarningsType> earningsTypes;
        //public IEnumerable<PersonEmploymentStatus> employmentStatuses;
        //public Dictionary<string, HumanResourceDemographics> employeeDictionary;


        public void SetupSupervisorInfo()
        {
            superId = "0000191";
            isSuperSupervisor = true;
            superPos = new Position()
            {
                Id = "SHOGUN",
                AlternateSupervisorPositionId = "",
                EndDate = null,
                IsExempt = false,
                IsSalary = true,
                PositionPayScheduleIds = new List<string>(),
                ShortTitle = "Shogun",
                Title = "S h o g u n",
                StartDate = new DateTime(1600, 02, 01),
                SupervisorPositionId = "",
                TimecardType = TimecardType.None
            };
            superPerPos = new PersonPosition()
            {
                Id = "SHOGUN191",
                AlternateSupervisorId = "",
                EndDate = null,
                LastWebTimeEntryPayPeriodEndDate = null,
                MigrationDate = null,
                NonEmployeePosition = true,
                PersonId = superId,
                PositionId = superPos.Id,
                StartDate = superPos.StartDate,
                SupervisorId = ""
            };
            superPerWag = new PersonPositionWage()
            {
                EndDate = null,
                FundingSources = new List<PositionFundingSource>(),
                Id = "SPG",
                IsPaySuspended = false,
                PayClassId = "Classy",
                PayCycleId = "TMA-3",
                PersonId = superId,
                PersonPositionId = superPerPos.Id,
                PositionId = superPos.Id,
                PositionPayDefaultId = "",
                RegularWorkEarningsTypeId = "",
                StartDate = superPos.StartDate
            };
            superCycle = new PayCycle()
            {
                AnnualPayFrequency = 1,
                Description = "TaxesYo",
                Id = "$$",
                PayClassIds = new List<string>() { superPerWag.PayClassId },
                PayPeriods = payCycle.PayPeriods,
                WorkWeekStartDay = DayOfWeek.Monday

            };
            superCycles = new List<PayCycle>() { superCycle };
            superPerWags = new List<PersonPositionWage>() { superPerWag };
            superPerPoses = new List<PersonPosition>() { superPerPos };
            superPoses = new List<Position>() { superPos };
        }

        public void SetupApproverDetailModelTests()
        {
            this.SetupTimecardModelTests();
            this.SetupTimePeriodModelTestData();
            this.SetupTimecardHistories();
            this.SetupTimePeriodCollectionModelTestData();
            this.SetupTimecardPositionStatusModelTests();
            this.SetupTimecardCollectionModelTests();

            this.SetupSupervisorInfo();

            payCycleId = "TMA-1";
            supervisorPositionQuery = new PositionQuery(superId, superPerPoses, superPoses, superPerWags, superCycles, isSuperSupervisor);
            approvalDetailModel = new ApprovalDetailModel(supervisorPositionQuery, payCycleId, periodEndDate, positionQuery, timecardQuery, earningsTypes, perstats, employeeDictionary, departments, locations);
        }

        #endregion
    }
}
