﻿using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{

    [TestClass]
    public class WorkEntryTests
    {
        TimecardQueryDataCoordinator timecardQuery;
        // timecardQuery data
        string personId;
        List<Timecard2> timecards;
        List<TimecardStatus> timecardStatuses;
        List<TimeEntryComments> comments;
        List<TimecardHistory2> timecardHistories;

        PositionQueryResultItem positionQueryResultItem;
        // positionQuery data
        PersonPosition personPosition;
        Position position;
        List<PersonPositionWage> personPositionWageList;
        PayCycle payCycle;

        // additional lists
        List<EarningsType> earnTypes;
        List<PersonEmploymentStatus> employmentStatuses;
        DateTime timeFrameStartDate;

        public void SetupAdditionalData()
        {
            earnTypes = new List<EarningsType>(){
                    new EarningsType(){
                         Id = "TEST",
                    }
               };

            employmentStatuses = new List<PersonEmploymentStatus>(){
                    new PersonEmploymentStatus(){
                         StartDate = new DateTime(16, 1, 1),
                         PrimaryPositionId = "JANITOR"
                    }
               };
            timeFrameStartDate = new DateTime(16, 9, 1);
        }

        public void SetupPositionQueryTestData()
        {

            personPosition = new PersonPosition();
            position = new Position()
            {
                Id = "JANITOR"
            };
            personPositionWageList = new List<PersonPositionWage>()
               {
                    new PersonPositionWage(){
                         RegularWorkEarningsTypeId = "TEST",
                         StartDate = new DateTime(16, 1, 1)
                    }
               };
            payCycle = new PayCycle()
            {
                Id = "M1",
                PayPeriods = new List<PayPeriod>()
                    {
                         new PayPeriod(){
                              StartDate = new DateTime(16, 9, 1),
                              EndDate = new DateTime(16, 9, 30),
                              EmployeeTimecardCutoffDateTime = new DateTime(16, 9, 30),
                              SupervisorTimecardCutoffDateTime = new DateTime(16, 9, 30),
                              Status = PayPeriodStatus.Prepared
                         }
                    }
            };
        }


        public void SetupTimecardQueryTestData()
        {
            personId = "00123";
            timecards = new List<Timecard2>(){
                    new Timecard2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,7),
                         Id = "001",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,1),
                         TimeEntries = new List<TimeEntry2>(){
                              new TimeEntry2(){
                                   EarningsTypeId = "TEST",
                                   Id = "1",
                                   TimecardId = "001",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         }
                    },
                    new Timecard2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,14),
                         Id = "002",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,8),
                         TimeEntries = new List<TimeEntry2>(){
                              new TimeEntry2(){
                                   EarningsTypeId = "TEST",
                                   Id = "1",
                                   TimecardId = "002",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         }
                    }
               };
            timecardStatuses = new List<TimecardStatus>(){
                    new TimecardStatus(){
                         ActionerId = "00123",
                         ActionType = 0,
                         HistoryId = "history1",
                         Id = "status1",
                         TimecardId = "001",
                         Timestamp = new Timestamp()
                    },

                    new TimecardStatus(){
                         ActionerId = "00123",
                         ActionType = 0,
                         HistoryId = "history2",
                         Id = "status2",
                         TimecardId = "002",
                         Timestamp = new Timestamp()
                    }
               };
            comments = new List<TimeEntryComments>(){
                    new TimeEntryComments(){

                         CommentAuthorName = "John Goodman",
                         Comments = "Test Comment",
                         CommentsTimestamp = new Timestamp(),
                         EmployeeId = "00123",
                         Id = "comment1",
                         PayCycleId = "M1",
                         PayPeriodEndDate = new DateTime(16, 9, 30),
                         PositionId = "JANITOR",
                         TimecardId = "001",
                         TimecardStatusId = "status1"
                    }
               };
            timecardHistories = new List<TimecardHistory2>(){
                    // history for previous pay period
                    new TimecardHistory2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,8,31),
                         Id = "history0",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 8, 31),
                         PeriodStartDate = new DateTime(16, 8, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,8,25),
                         TimeEntryHistories = new List<TimeEntryHistory2>(){
                              new TimeEntryHistory2(){
                                   EarningsTypeId = "TEST",
                                   Id = "timeentryhistory0",
                                   TimecardHistoryId = "history0",
                                   WorkedDate = new DateTime(16, 8, 31),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         },
                         Timestamp = new Timestamp(){
                              AddDateTime = new DateTimeOffset(16, 8, 31, 0, 0, 0, new TimeSpan(0))
                         },
                         StatusAction = StatusAction.Paid
                    },
                    //histories for submitted timecards
                    new TimecardHistory2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,7),
                         Id = "history1",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,1),
                         TimeEntryHistories = new List<TimeEntryHistory2>(){
                              new TimeEntryHistory2(){
                                   EarningsTypeId = "TEST",
                                   Id = "timeentryhistory1",
                                   TimecardHistoryId = "history1",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         },
                         Timestamp = new Timestamp(){
                              AddDateTime = new DateTimeOffset(16, 9, 7, 0, 0, 0, new TimeSpan(0))
                         }
                    },
                    new TimecardHistory2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,14),
                         Id = "history2",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,8),
                         TimeEntryHistories = new List<TimeEntryHistory2>(){
                              new TimeEntryHistory2(){
                                   EarningsTypeId = "TEST",
                                   Id = "timeentryhistory2",
                                   TimecardHistoryId = "history2",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         },
                         Timestamp = new Timestamp(){
                              AddDateTime = new DateTimeOffset(16, 9, 14, 0, 0, 0, new TimeSpan(0))
                         }
                         
                     }
               };
        }


        [TestMethod]
        public void CreateWorkEntry()
        {
            SetupTimecardQueryTestData();
            SetupPositionQueryTestData();
            SetupAdditionalData();

            var positionQueryResultItem = new PositionQueryResultItem(personPosition, position, personPositionWageList, payCycle);
            var timecardQuery = new TimecardQueryDataCoordinator(personId, timecards, timecardHistories, timecardStatuses, comments);
            var earningsTypeId = "TEST";
            var timeFrameStartDate = new DateTime(16, 9, 1);
            var startDate = new DateTime(16, 9, 1);
            var endDate = new DateTime(16, 9, 7);
            var tqri = timecardQuery.FindTimecardQueryResult(positionQueryResultItem, startDate, endDate);
            var timeEntryDtos = tqri.Timecard2.TimeEntries;
            var timeHistoryDtos = new List<TimeEntry2>();

            var workEntry = new WorkEntry(positionQueryResultItem, "00123", earningsTypeId, timeFrameStartDate, startDate, endDate, timeEntryDtos, timeHistoryDtos);

            Assert.IsNotNull(workEntry);


        }

        [TestMethod]
        public void CreateWorkEntryWithTimecardDataStartingMidWeek()
        {
            SetupTimecardQueryTestData();
            SetupPositionQueryTestData();
            SetupAdditionalData();

            var positionQueryResultItem = new PositionQueryResultItem(personPosition, position, personPositionWageList, payCycle);
            var timecardQuery = new TimecardQueryDataCoordinator(personId, timecards, timecardHistories, timecardStatuses, comments);
            var earningsTypeId = "TEST";
            var timeFrameStartDate = new DateTime(16, 8, 30);
            var startDate = new DateTime(16, 9, 1);
            var endDate = new DateTime(16, 9, 7);
            var tqri = timecardQuery.FindTimecardQueryResult(positionQueryResultItem, startDate, endDate);
            var timeEntryDtos = tqri.Timecard2.TimeEntries;
            var timeHistoryDtos = new List<TimeEntry2>();

            var workEntry = new WorkEntry(positionQueryResultItem, "00123", earningsTypeId, timeFrameStartDate, startDate, endDate, timeEntryDtos, timeHistoryDtos);

            Assert.IsNotNull(workEntry);
            Assert.IsNull(workEntry.DailyTimeEntryGroupList[0].DailyTimeEntryList[0].HoursWorked);
            Assert.IsNull(workEntry.DailyTimeEntryGroupList[1].DailyTimeEntryList[0].HoursWorked);
            Assert.IsNotNull(workEntry.DailyTimeEntryGroupList[2].DailyTimeEntryList[0].HoursWorked);
            Assert.AreEqual(8, workEntry.DailyTimeEntryGroupList[2].DailyTimeEntryList[0].HoursWorked);
        }

        [TestMethod]
        public void CreateWorkEntryWithHistoricalTimecardData()
        {

            SetupTimecardQueryTestData();
            SetupPositionQueryTestData();
            SetupAdditionalData();

            var positionQueryResultItem = new PositionQueryResultItem(personPosition, position, personPositionWageList, payCycle);
            var timecardQuery = new TimecardQueryDataCoordinator(personId, timecards, timecardHistories, timecardStatuses, comments);
            var earningsTypeId = "TEST";
            var timeFrameStartDate = new DateTime(16, 8, 30);
            var startDate = new DateTime(16, 9, 1);
            var endDate = new DateTime(16, 9, 7);
            var tqrc = timecardQuery.FindTimecardQueryResultContext(positionQueryResultItem, startDate, endDate);
            var timeEntryDtos = tqrc.Current.Timecard2.TimeEntries;
            var timeHistoryDtos = tqrc.Previous.Timecard2.TimeEntries;

            var workEntry = new WorkEntry(positionQueryResultItem, "00123", earningsTypeId, timeFrameStartDate, startDate, endDate, timeEntryDtos, timeHistoryDtos);

            //assert
            Assert.IsNotNull(workEntry);
            Assert.IsNotNull(workEntry.DailyTimeEntryGroupList[1].DailyTimeEntryList[0].HoursWorked);
            Assert.AreEqual(8, workEntry.DailyTimeEntryGroupList[1].DailyTimeEntryList[0].HoursWorked);
            Assert.IsNotNull(workEntry.DailyTimeEntryGroupList[2].DailyTimeEntryList[0].HoursWorked);
            Assert.AreEqual(8, workEntry.DailyTimeEntryGroupList[2].DailyTimeEntryList[0].HoursWorked);
        }

    }
}
