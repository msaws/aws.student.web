﻿using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class TimecardPositionStatusModelTests : TimeManagementTestData
    {
        [TestInitialize]
        public void Initialize()
        {
            base.SetupTimecardPositionStatusModelTests();
        }

        [TestMethod]
        public void FirstContructorPropertiesAreSet()
        {
            timecardPositionStatusModel = new TimecardPositionStatusModel(positionId, positionTitle, payCycleId, isPositionPrimary, currentStatus);
            Assert.AreEqual(positionId, timecardPositionStatusModel.PositionId);
            Assert.AreEqual(positionTitle, timecardPositionStatusModel.PositionTitle);
            Assert.AreEqual(payCycleId, timecardPositionStatusModel.PayCycleId);
            Assert.AreEqual(isPositionPrimary, timecardPositionStatusModel.IsPositionPrimary);
            Assert.AreEqual(currentStatus, timecardPositionStatusModel.CurrentStatus);

        }

        [TestMethod]
        public void SecondConstructorPropertiesAreSet()
        {
            timecardPositionStatusModel = new TimecardPositionStatusModel(positionQueryResult);
            Assert.AreEqual(positionQueryResult.PositionId, timecardPositionStatusModel.PositionId);
            Assert.AreEqual(positionQueryResult.PositionTitle, timecardPositionStatusModel.PositionTitle);
        }
    }
}
