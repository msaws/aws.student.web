﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class PositionModelTests : TimeManagementTestData
    {
        string id;
        string title;
        bool isSupervised; // maybe
        bool isPrimary;
        List<TimecardQueryResultItem> timecardQueryResultItemList;
        TimecardQueryResultItem timecardQueryResultItem;
        public void PositionModelTestsInitialize() 
        {
            id = "1";
            title = "POSTMA1";
            isSupervised = true; // maybe
            isPrimary = true;
            //timecardQueryResultItem = new TimecardQueryResultItem(base.timecard2Dto, base.timecardHistories.First(), new TimecardStatus(), new List<TimeEntryComments>() );
            timecardQueryResultItemList = new List<TimecardQueryResultItem>() {  };
        }

        [TestClass]
        public class ContsuctorTests : PositionModelTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.PositionModelTestsInitialize();
            }

            // public PositionModel(string id, string title, bool isSupervised, bool isPrimary, List<TimecardQueryResultItem> timecardsForPositionForPeriod)

            //[TestMethod]
            //public void ValuePropertiesAreSet()
            //{

            //    var model = new PositionModel(id, title, isSupervised, isPrimary, timecardQueryResultItemList);

            //    Assert.AreEqual(id, model.Id);
            //    Assert.AreEqual(title, model.Title);
            //    Assert.AreEqual(isSupervised, model.IsSupervised);
            //    Assert.AreEqual(isPrimary, model.IsPrimary);
            //}

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void EmptyTimecardQueryResultItemListArgException()
            {
                var model = new PositionModel(id, title, isSupervised, isPrimary, new List<TimecardQueryResultItem>());
            }

        }
    }
}
