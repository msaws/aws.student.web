﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.TimeManagement.Models.TimecardQuery;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class DailyTimeEntryTests
    {
        TimecardQueryDataCoordinator timecardQuery;
        // timecardQuery data
        string personId;
        List<Timecard2> Timecards2;
        List<TimecardStatus> timecardStatuses;
        List<TimeEntryComments> comments;
        List<TimecardHistory2> timecardHistories2;



        public void SetupTimecardQueryTestData()
        {
            personId = "00123";
            Timecards2 = new List<Timecard2>(){
                    new Timecard2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,7),
                         Id = "001",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,1),
                         TimeEntries = new List<TimeEntry2>(){
                              new TimeEntry2(){
                                   EarningsTypeId = "TEST",
                                   Id = "1",
                                   TimecardId = "001",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         }
                    },
                    new Timecard2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,14),
                         Id = "002",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,8),
                         TimeEntries = new List<TimeEntry2>(){
                              new TimeEntry2(){
                                   EarningsTypeId = "TEST",
                                   Id = "1",
                                   TimecardId = "002",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         }
                    },
                    new Timecard2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,14),
                         Id = "003",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,8),
                         TimeEntries = new List<TimeEntry2>(){
                              new TimeEntry2(){
                                   EarningsTypeId = "TEST",
                                   Id = "1",
                                   TimecardId = "003",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0),
                                   InDateTime = new DateTime(16, 9, 1, 13, 0, 0),
                                   OutDateTime = new DateTime(16, 9, 1, 13, 30, 0)
                              }
                         }
                    }

               };
            timecardStatuses = new List<TimecardStatus>(){
                    new TimecardStatus(){
                         ActionerId = "00123",
                         ActionType = 0,
                         HistoryId = "history1",
                         Id = "status1",
                         TimecardId = "001",
                         Timestamp = new Timestamp()
                    },

                    new TimecardStatus(){
                         ActionerId = "00123",
                         ActionType = 0,
                         HistoryId = "history2",
                         Id = "status2",
                         TimecardId = "002",
                         Timestamp = new Timestamp()
                    }
               };
            comments = new List<TimeEntryComments>(){
                    new TimeEntryComments(){

                         CommentAuthorName = "John Goodman",
                         Comments = "Test Comment",
                         CommentsTimestamp = new Timestamp(),
                         EmployeeId = "00123",
                         Id = "comment1",
                         PayCycleId = "M1",
                         PayPeriodEndDate = new DateTime(16, 9, 30),
                         PositionId = "JANITOR",
                         TimecardId = "001",
                         TimecardStatusId = "status1"
                    }
               };
            timecardHistories2 = new List<TimecardHistory2>(){
                    // history for previous pay period
                    new TimecardHistory2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,8,31),
                         Id = "history0",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 8, 31),
                         PeriodStartDate = new DateTime(16, 8, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,8,25),
                         TimeEntryHistories = new List<TimeEntryHistory2>(){
                              new TimeEntryHistory2(){
                                   EarningsTypeId = "TEST",
                                   Id = "timeentryhistory0",
                                   TimecardHistoryId = "history0",
                                   WorkedDate = new DateTime(16, 8, 31),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         },
                         Timestamp = new Timestamp(){
                              AddDateTime = new DateTimeOffset(16, 8, 31, 0, 0, 0, new TimeSpan(0))
                         }
                    },
                    //histories for submitted timecards
                    new TimecardHistory2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,7),
                         Id = "history1",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,1),
                         TimeEntryHistories = new List<TimeEntryHistory2>(){
                              new TimeEntryHistory2(){
                                   EarningsTypeId = "TEST",
                                   Id = "timeentryhistory1",
                                   TimecardHistoryId = "history1",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         },
                         Timestamp = new Timestamp(){
                              AddDateTime = new DateTimeOffset(16, 9, 7, 0, 0, 0, new TimeSpan(0))
                         }
                    },
                    new TimecardHistory2(){
                         EmployeeId = "00123",
                         EndDate = new DateTime(16,9,14),
                         Id = "history2",
                         PayCycleId = "M1",
                         PeriodEndDate = new DateTime(16, 9, 30),
                         PeriodStartDate = new DateTime(16, 9, 1),
                         PositionId = "JANITOR",
                         StartDate = new DateTime(16,9,8),
                         TimeEntryHistories = new List<TimeEntryHistory2>(){
                              new TimeEntryHistory2(){
                                   EarningsTypeId = "TEST",
                                   Id = "timeentryhistory2",
                                   TimecardHistoryId = "history2",
                                   WorkedDate = new DateTime(16, 9, 1),
                                   WorkedTime = new TimeSpan(8, 0, 0)
                              }
                         },
                         Timestamp = new Timestamp(){
                              AddDateTime = new DateTimeOffset(16, 9, 14, 0, 0, 0, new TimeSpan(0))
                         }
                         
                     }
               };
        }

        [TestClass]
        public class ConstructorTests : DailyTimeEntryTests
        {

            [TestMethod]
            public void CreateDefaultDailyTimeEntry()
            {
                var dailyTimeEntry = new DailyTimeEntry();

                // assert
                Assert.IsNotNull(dailyTimeEntry);
                Assert.IsNull(dailyTimeEntry.Id);
                Assert.IsFalse(dailyTimeEntry.IsHistorical);
                Assert.IsFalse(dailyTimeEntry.IsReadOnly);
                Assert.AreEqual(new DateTime(), dailyTimeEntry.DateWorked);
                Assert.IsNull(dailyTimeEntry.TimeStart);
                Assert.IsNull(dailyTimeEntry.TimeEnd);
            }

            [TestMethod]
            public void CreateEmptyDailyTimeEntry()
            {
                var date = new DateTime(16, 10, 19);
                var dailyTimeEntry = new DailyTimeEntry(date, true);

                // assert
                Assert.IsNotNull(dailyTimeEntry);
                Assert.AreEqual(String.Empty, dailyTimeEntry.Id);
                Assert.IsFalse(dailyTimeEntry.IsHistorical);
                Assert.IsTrue(dailyTimeEntry.IsReadOnly);
                Assert.AreEqual(date, dailyTimeEntry.DateWorked);
                Assert.IsNull(dailyTimeEntry.TimeStart);
                Assert.IsNull(dailyTimeEntry.TimeEnd);
            }

            [TestMethod]
            public void CreateFullDailyTimeEntry()
            {
                SetupTimecardQueryTestData();
                var dailyTimeEntry = new DailyTimeEntry(Timecards2[0].TimeEntries[0], false, false);

                // assert
                Assert.IsNotNull(dailyTimeEntry);
                Assert.AreEqual(Timecards2[0].TimeEntries[0].Id, dailyTimeEntry.Id);
                Assert.IsFalse(dailyTimeEntry.IsHistorical);
                Assert.IsFalse(dailyTimeEntry.IsReadOnly);
                Assert.AreEqual(Timecards2[0].TimeEntries[0].WorkedDate, dailyTimeEntry.DateWorked);
            }

            [TestMethod]
            public void CreateFullDailyTimeEntryWithTimeInOut()
            {
                SetupTimecardQueryTestData();
                var dailyTimeEntry = new DailyTimeEntry(Timecards2[2].TimeEntries[0], false, false);

                // assert
                Assert.IsNotNull(dailyTimeEntry);
                Assert.AreEqual(Timecards2[2].TimeEntries[0].Id, dailyTimeEntry.Id);
                Assert.IsFalse(dailyTimeEntry.IsHistorical);
                Assert.IsFalse(dailyTimeEntry.IsReadOnly);
                Assert.AreEqual(Timecards2[2].TimeEntries[0].WorkedDate, dailyTimeEntry.DateWorked);
                Assert.AreEqual(Timecards2[2].TimeEntries[0].InDateTime, dailyTimeEntry.TimeStart);
                Assert.AreEqual(Timecards2[2].TimeEntries[0].OutDateTime, dailyTimeEntry.TimeEnd);
            }
        }

    }
}
