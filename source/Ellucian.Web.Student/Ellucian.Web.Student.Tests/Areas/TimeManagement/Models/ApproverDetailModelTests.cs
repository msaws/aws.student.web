﻿using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class ApproverDetailModelTests : TimeManagementTestData
    {
        [TestInitialize]
        public void Initialize()
        {
            base.SetupApproverDetailModelTests();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoPayCycleConstructorError()
        {
            payCycleId = string.Empty;
            new ApprovalDetailModel(supervisorPositionQuery, payCycleId, periodEndDate, positionQuery, timecardQuery, earningsTypes, perstats, employeeDictionary, departments, locations);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoPositionQueryConstructorError()
        {
            positionQuery = null;
            new ApprovalDetailModel(supervisorPositionQuery, payCycleId, periodEndDate, positionQuery, timecardQuery, earningsTypes, perstats, employeeDictionary, departments, locations);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoTimecardQueryConstructorError()
        {
            timecardQuery = null;
            new ApprovalDetailModel(supervisorPositionQuery, payCycleId, periodEndDate, positionQuery, timecardQuery, earningsTypes, perstats, employeeDictionary, departments, locations);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoEarningTypesConstructorError()
        {
            earningsTypes = null;
            new ApprovalDetailModel(supervisorPositionQuery, payCycleId, periodEndDate, positionQuery, timecardQuery, earningsTypes, perstats, employeeDictionary, departments, locations);
        }

        [TestMethod]
        public void PropertiesAreSetInContsructor()
        {
            Assert.AreEqual(supervisorPositionQuery.PersonId, approvalDetailModel.SupervisorId);
            CollectionAssert.AreEqual(supervisorPositionQuery.Select(q => q.PersonPosition.PositionId).ToList(), approvalDetailModel.SupervisorPositionIds);
            Assert.AreEqual(perstats.First().PersonId, approvalDetailModel.SuperviseeId);
            Assert.IsNotNull(approvalDetailModel.TimecardCollections);
            Assert.IsNotNull(approvalDetailModel.SupervisorName);
            Assert.IsNotNull(approvalDetailModel.SuperviseeName);
        }
    }
}
