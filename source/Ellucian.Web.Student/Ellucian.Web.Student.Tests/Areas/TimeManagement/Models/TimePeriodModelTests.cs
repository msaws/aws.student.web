﻿using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Student.Areas.TimeManagement.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.TimeManagement.Models
{
    [TestClass]
    public class TimePeriodModelTests : TimeManagementTestData
    {
        [TestInitialize]
        public void Initialize()
        {
            base.SetupTimePeriodModelTestData();
        }

        [TestMethod]
        public void DefaultConstructorTests()
        {
            var model = new TimePeriodModel();
            Assert.IsNotNull(model);
        }

        [TestMethod]
        public void ModelIsConstructedWithStartAndEndDatesTest()
        {
            timePeriodModel = new TimePeriodModel(startDate, endDate);
            Assert.AreEqual(startDate, timePeriodModel.StartDate);
            Assert.AreEqual(endDate, timePeriodModel.EndDate);
            Assert.AreEqual(0, timePeriodModel.TotalHours);
            Assert.IsNotNull(timePeriodModel.TimecardPositionStatuses);
            Assert.AreEqual(0,timePeriodModel.TimecardPositionStatuses.Count);
        }

        [TestMethod]
        public void HoursAreAddedFromDTO()
        {
            Assert.AreEqual(0, timePeriodModel.TotalHours);            
            timePeriodModel.AddTimecardHours(timecard2Dto);
            var dtoHours = timecard2Dto.TimeEntries.Sum(te => te.WorkedTime.HasValue ? te.WorkedTime.Value.TotalHours : 0);
            Assert.AreEqual(dtoHours, timePeriodModel.TotalHours);
            timePeriodModel.AddTimecardHours(timecard2Dto);
            Assert.AreEqual(2*dtoHours, timePeriodModel.TotalHours); // ensure it is incremented
        }

        [TestMethod]
        public void HoursAreAddedFromDTOs()
        {
            Assert.AreEqual(0, timePeriodModel.TotalHours);
            var dtos = new List<Timecard2>() { timecard2Dto, timecard2Dto, timecard2Dto };
            timePeriodModel.AddTimecardHours(dtos);
            var dtoHours = timecard2Dto.TimeEntries.Sum(te => te.WorkedTime.HasValue ? te.WorkedTime.Value.TotalHours : 0);
            Assert.AreEqual(dtoHours * dtos.Count, timePeriodModel.TotalHours);
        }

        [TestMethod]
        public void TimecardPositionStatusIsAdded()
        {
            var len = timePeriodModel.TimecardPositionStatuses.Count;
            timePeriodModel.AddTimecardPositionStatus(timecardPositionStatusModel);
            Assert.AreEqual(len + 1, timePeriodModel.TimecardPositionStatuses.Count);
        }
    }
}
