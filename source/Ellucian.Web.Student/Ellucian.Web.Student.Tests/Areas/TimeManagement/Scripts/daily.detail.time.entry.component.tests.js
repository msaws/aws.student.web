﻿
var component = require('TimeManagement/TimeSheet/DailyDetailTimeEntry/_DailyDetailTimeEntry'),
    errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");

describe('Test Suite for DailyDetailTimeEntry component', function () {

    let params = {};
    // dailyDetailViewModel refers to the dailyDetailTimeEntryViewModel
    let dailyDetailViewModel = component.viewModel;

    beforeEach(function () {

        params = {
            dailyTimeEntryModel: {
                Id: ko.observable('001'),
                DateWorked: ko.observable( new Date(2016, 5, 27) ),
                TimeStart: ko.observable(new Date(2016, 5, 27, 4, 0, 0, 0)),
                TimeEnd: ko.observable(new Date(2016, 5, 27, 9, 30, 0, 0)),
                HoursWorked: ko.observable(5.5),
                IsReadOnly: ko.observable(false),
                MessageChannel: ko.observableArray([])
            },
            isReadOnly: ko.observable(false),
            isMobile: ko.observable(false),
            onTimeWorkedChange: function(vm) {},
        }

    });

    describe('argument verification', function() {
        it("should succeed", function () {
            let errCnt = 0;
            try { new dailyDetailViewModel(params); }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(0);
        });
        it("should throw error when params is null or undefined", function() {
            let errCnt = 0;
            try { new dailyDetailViewModel(undefined); }
            catch(err) { errCnt++; }
            try { new dailyDetailViewModel(null); }
            catch(err) { errCnt++; }
            expect(errCnt).toEqual(2);
        });
        it("should throw error when isReadOnly is null, undefined, or not an observable", () => {
            let errCnt = 0;
            try { 
                params.isReadOnly = undefined;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.isReadOnly = null;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.isReadOnly = true;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            expect(errCnt).toEqual(3);
        });
        it("should throw error when dailyTimeEntryModel is null or undefined", () => {
            let errCnt = 0;
            try { 
                params.dailyTimeEntryModel = undefined;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.dailyTimeEntryModel = null;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            expect(errCnt).toEqual(2);
        });
        it("should throw error when isMobile is null, undefined, or not an observable", () => {
            let errCnt = 0;
            try { 
                params.isMobile = undefined;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.isMobile = null;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.isMobile = true;
                new dailyDetailViewModel(params); 
            }
            catch(err) { errCnt++; }
            expect(errCnt).toEqual(3);
        });
    })

    describe('isReadOnly', function () {
        it('should be false when isReadOnly from params is false and IsReadOnly from the server is false', function () {
            var expected = false;
            params.isReadOnly = ko.observable(false);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(false);
            var actual = new dailyDetailViewModel(params).isReadOnly();
            expect(actual).toEqual(expected);
        });
        it('should be true when isReadOnly from params is false and IsReadOnly from the server is true', function () {
            var expected = true;
            params.isReadOnly = ko.observable(false);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(true);
            var actual = new dailyDetailViewModel(params).isReadOnly();
            expect(actual).toEqual(expected);
        });
        it('should be true when isReadOnly from params is true and IsReadOnly from the server is false', function () {
            var expected = true;
            params.isReadOnly = ko.observable(true);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(false);
            var actual = new dailyDetailViewModel(params).isReadOnly();
            expect(actual).toEqual(expected);
        });
        it('should be true when isReadOnly from params is true and IsReadOnly from the server is true', function () {
            var expected = true;
            params.isReadOnly = ko.observable(true);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(true);
            var actual = new dailyDetailViewModel(params).isReadOnly();
            expect(actual).toEqual(expected);
        });

    });

    describe('user input for time in and time out', function () {

        let viewModel;
        beforeEach(function() {
            viewModel = new dailyDetailViewModel(params);
        });
        afterEach(function() {
            viewModel.dispose();
        })

        it('should initilialize formattedTimeStart to server provided TimeStart', function () {
            expect(viewModel.formattedTimeStart()).toEqual(params.dailyTimeEntryModel.TimeStart());
        });
        it('should initilialize formattedTimeEnd to server provided TimeEnd', function () {
            
            expect(viewModel.formattedTimeEnd()).toEqual(params.dailyTimeEntryModel.TimeEnd());
        });
        it('should initilialize formattedTimeStart to null', function () {
            params.dailyTimeEntryModel.TimeStart(null);
            viewModel = new dailyDetailViewModel(params);
            expect(viewModel.formattedTimeStart()).toBeNull();
        });
        it('should initilialize formattedTimeEnd to null', function () {
            
            params.dailyTimeEntryModel.TimeEnd(null);
            viewModel = new dailyDetailViewModel(params);
            expect(viewModel.formattedTimeEnd()).toBeNull();
        });

        it("should calculate HoursWorked to zero when formattedTimeStart is set to null", function() {           
            viewModel.formattedTimeStart(null);
            expect(viewModel.HoursWorked()).toBe(0);
        });
        it("should calculate HoursWorked to zero when formattedTimeEnd is set to null", function() {
            viewModel.formattedTimeEnd(null);
            expect(viewModel.HoursWorked()).toBe(0);
        });
        it("should calculate HoursWorked to zero when formattedTimeEnd is not a date", function() {
            viewModel.formattedTimeEnd("foobar");
            expect(viewModel.HoursWorked()).toBe(0);
        });
        it('should calculate HoursWorked from TimeStart and TimeEnd if both times are provided', function () {
            viewModel.formattedTimeStart(new Date(2017, 0, 19, 9));
            viewModel.formattedTimeEnd(new Date(2017, 0, 19, 17));
            expect(viewModel.HoursWorked()).toEqual(8);
        });
        it("should set TimeStart to formatted moment", function() {
            let inDateTime = new Date(2017, 0, 19, 9)
            viewModel.formattedTimeStart(inDateTime);
            expect(viewModel.TimeStart()).toEqual(moment(viewModel.formattedTimeStart()).format());            
        });
        it("should set TimeEnd to formatted moment", function() {

            let inDateTime = new Date(2017, 0, 19, 9)
            viewModel.formattedTimeEnd(inDateTime);
            expect(viewModel.TimeEnd()).toEqual(moment(viewModel.formattedTimeEnd()).format());            
        });
        it('should set date portion of TimeStart to DateWorked', function() {
            let inDateTime = new Date(2017, 0, 19, 9)
            viewModel.formattedTimeStart(inDateTime);
            expect(moment(viewModel.TimeStart()).toDate().getFullYear()).toEqual(viewModel.DateWorked().getFullYear());
            expect(moment(viewModel.TimeStart()).toDate().getMonth()).toEqual(viewModel.DateWorked().getMonth());
            expect(moment(viewModel.TimeStart()).toDate().getDate()).toEqual(viewModel.DateWorked().getDate());
        });
        it("should push time out of order error message and remove it when error is fixed", function() {
            let outOfOrderTime = new Date(2017, 5, 17, 10);
            viewModel.formattedTimeStart(outOfOrderTime);
            expect(viewModel.MessageChannel()).toContain(errorDefinitions.keys.timeOutOfOrderError);

            viewModel.formattedTimeStart(params.dailyTimeEntryModel.TimeStart());
            expect(viewModel.MessageChannel()).not.toContain(errorDefinitions.keys.timeOutOfOrderError);
        });
        it('should invoke onTimeWorkedChange callback', function() {
            spyOn(params, "onTimeWorkedChange");
            viewModel.formattedTimeStart(new Date(2016, 5, 27, 5, 0, 0, 0));
            expect(params.onTimeWorkedChange).toHaveBeenCalledWith(viewModel);
        });
        it('should not invoke onTimeWorkedChange callback when inputHasError', function() {
            spyOn(params, "onTimeWorkedChange");
            viewModel.formattedTimeStart(new Date(2017, 5, 27, 12, 0, 0, 0));
            expect(params.onTimeWorkedChange).not.toHaveBeenCalled();
        });
        it("should set Id and HoursWorked to null when start and end dates are both set to null", function() {
            viewModel.formattedTimeStart(null);
            viewModel.formattedTimeEnd(null);
            expect(viewModel.Id()).toBeNull();
            expect(viewModel.HoursWorked()).toBeNull();
        });

    });

    describe('timepicker options', function() {
        it("should merge default values into startTimepickerOptions", function() {
            let viewModel = new dailyDetailViewModel(params);
            expect(viewModel.startTimepickerOptions().step).toEqual(15);
            expect(viewModel.startTimepickerOptions().stopScrollPropagation).toBeTruthy();
            expect(viewModel.startTimepickerOptions().timeFormat(viewModel.TimeStart())).toEqual(Globalize.format(viewModel.TimeStart(), 't'));
            expect(viewModel.startTimepickerOptions().maxTime).toEqual("11:55 PM");
            expect(viewModel.startTimepickerOptions().showOn).toEqual(['focus', 'click']);
        });
       
        it("should merge default values into endTimepickerOptions", function() {
            let viewModel = new dailyDetailViewModel(params);
            expect(viewModel.endTimepickerOptions().step).toEqual(15);
            expect(viewModel.endTimepickerOptions().stopScrollPropagation).toBeTruthy();
            expect(viewModel.endTimepickerOptions().timeFormat(viewModel.TimeStart())).toEqual(Globalize.format(viewModel.TimeStart(), 't'));
            expect(viewModel.endTimepickerOptions().maxTime).toEqual("11:55 PM");
            expect(viewModel.endTimepickerOptions().showOn).toEqual(['focus', 'click']);
        });
        it("should use specific default values when isMobile is true", function() {
            let viewModel = new dailyDetailViewModel(params);
            params.isMobile(true);
            expect(viewModel.startTimepickerOptions().showOn).toEqual([]);
            expect(viewModel.endTimepickerOptions().showOn).toEqual([]);
        });
        it("should use specific endTimepickerOptions when TimeStart has value", function() {
            let viewModel = new dailyDetailViewModel(params);
            expect(viewModel.endTimepickerOptions().showDuration).toBeTruthy();
            expect(viewModel.endTimepickerOptions().durationTime()).toEqual(viewModel.TimeStart());
            expect(viewModel.endTimepickerOptions().scrollDefault).toEqual(viewModel.TimeStart());
        });

        it("should use specific endTimepickerOptions when TimeStart is null", function() {
            let viewModel = new dailyDetailViewModel(params);
            viewModel.formattedTimeStart(null);
            expect(viewModel.endTimepickerOptions().showDuration).toBeFalsy();
            expect(viewModel.endTimepickerOptions().durationTime()).toEqual(viewModel.TimeStart());
            expect(viewModel.endTimepickerOptions().scrollDefault).toEqual(viewModel.TimeStart());
        });
        
    });

    describe("hasInputError", function() {
        let viewModel;
        beforeEach(function() {
            viewModel = new dailyDetailViewModel(params);
        });

        afterEach(function() {
            viewModel.dispose();
        });

        it("should return false when there are no specific errors and is not read-only", function() {
            expect(viewModel.isReadOnly()).toBeFalsy();
            expect(viewModel.MessageChannel()).toEqual([]);
            expect(viewModel.hasOverlapError()).toBeFalsy();
            expect(viewModel.hasTotalTimeError()).toBeFalsy();
            expect(viewModel.hasTimeOutOfOrderError()).toBeFalsy();
            expect(viewModel.hasInputError()).toBeFalsy();
        });

        it("should return false if read-only, even if there are error MessageChannel", function() {
            params.isReadOnly(true);
            viewModel.MessageChannel.push(errorDefinitions.keys.overlapError);
            expect(viewModel.hasInputError()).toBeFalsy();
        });

        it("should return true if hasOverlapError is true", function() {
            viewModel.MessageChannel.push(errorDefinitions.keys.overlapError);
            expect(viewModel.hasOverlapError()).toBeTruthy();
            expect(viewModel.hasInputError()).toBeTruthy();
        });

        it("should return true if hasTotalTimeError is true", function() {
            viewModel.MessageChannel.push(errorDefinitions.keys.totalDayTimeError);
            expect(viewModel.hasTotalTimeError()).toBeTruthy();
            expect(viewModel.hasInputError()).toBeTruthy();
        });

        it("should return true if hasTimeOutOfOrderError is true", function() {
            viewModel.MessageChannel.push(errorDefinitions.keys.timeOutOfOrderError);
            expect(viewModel.hasTimeOutOfOrderError()).toBeTruthy();
            expect(viewModel.hasInputError()).toBeTruthy();
        });
    });


    describe("missingTimeEntryIndicator computed", function() {
    
        let viewModel;
        beforeEach(function() {
            viewModel = new dailyDetailViewModel(params);     
        });
        afterEach(function() {
            viewModel.dispose();
        });

        it("should return empty string when TimeStart and TimeEnd have value", function() {
            expect(viewModel.TimeStart()).not.toBeNull();
            expect(viewModel.TimeEnd()).not.toBeNull();

            expect(viewModel.missingTimeEntryIndicator()).toEqual('');
        });

        it("should return empty string when TimeStart and TimeEnd have no value", function() {
            viewModel.formattedTimeStart(null).formattedTimeEnd(null);
            
            expect(viewModel.missingTimeEntryIndicator()).toEqual('');
        });

        it("should return timeStart when TimeStart is missing", function() {
            viewModel.formattedTimeStart(null);
            expect(viewModel.missingTimeEntryIndicator()).toEqual('timeStart');
        })

        it("should return timeEnd when TimeEnd is missing", function() {
            viewModel.formattedTimeEnd(null);
            expect(viewModel.missingTimeEntryIndicator()).toEqual('timeEnd');
        });

        it("should return empty string if hasInputError is true even if time is missing", function() {
            viewModel.formattedTimeEnd(null);
            viewModel.MessageChannel.push(errorDefinitions.keys.overlapError);
            expect(viewModel.missingTimeEntryIndicator()).toEqual('');
        })
    });


});