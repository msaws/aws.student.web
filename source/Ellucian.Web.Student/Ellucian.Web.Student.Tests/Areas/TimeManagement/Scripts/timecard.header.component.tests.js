﻿var component = require("TimeManagement/Components/TimecardHeader/_TimecardHeader"),
    resources = require('Site/resource.manager');

describe("Test Suite for TimecardHeader component", function () {

    let params,
        viewModel;

    beforeEach(function () {
        params = {
            timecardModel: {
                PositionTitle: ko.observable("Pos Title 1"),
                PositionId: ko.observable("001"),
                TimecardStatus: ko.observable(1),
                WorkEntryList: ko.observableArray([
                    {
                        EarningsTypeId: "ET1",
                        DailyTimeEntryGroupList: ko.observableArray([
                            {
                                DateWorked: ko.observable(new Date(2016, 10, 30)),
                                DailyTimeEntryList: ko.observableArray([
                                    { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(1) },
                                    { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(2) }
                                ])
                            },
                            {
                                DateWorked: ko.observable(new Date(2016, 11, 1)),
                                DailyTimeEntryList: ko.observableArray([
                                    { DateWorked: ko.observable(new Date(2016, 11, 30)), HoursWorked: ko.observable(3) },
                                    { DateWorked: ko.observable(new Date(2016, 11, 30)), HoursWorked: ko.observable(4) }
                                ])
                            }
                        ])
                    }
                ])
            },
        };

        viewModel = new component.viewModel(params);

    });


    describe('timecard header model', function () {
        it('should merge the data model to self', function () {
            expect(viewModel.hasTimecardStatus).toBeDefined();
            expect(viewModel.timecardStatusText).toBeDefined();
            expect(viewModel.timecardStatusClass).toBeDefined();
            expect(viewModel.timecardHours).toBeDefined();
        });
    });


})