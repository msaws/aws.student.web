﻿var component = require("TimeManagement/TimeApproval/ApproveTimecardDetailTable/_ApproveTimecardDetailTable"),
    dataMapping = require('TimeManagement/TimeApproval/time.approval.data.mapping'),
    resources = require('Site/resource.manager'),
    status = require('TimeManagement/Modules/submission.status'),
    actions = require("TimeManagement/TimeApproval/time.approval.actions");


describe("Test Suite for Approve Timecard Detail Table component", function () {
    var params = {};
    var ApproveTimecardViewModel = component.viewModel;
    var viewModel;
    var resourceManagerSpyReturnPlaceholderValue = ko.observable("00:00 AM");

    beforeEach(function () {
        params = {
            dataModel: {
                EmployeeId: ko.observable("123"),
                EmployeeName: ko.observable('Doe, John'),
                EffectiveStartDate: ko.observable(new Date(2016, 10, 30)),
                StartDate: ko.observable(new Date(2016, 11, 1)),
                EndDate: ko.observable(new Date(2016, 11, 6)),
                PayCycleId: ko.observable("M1"),
                Timecards: ko.observableArray([
                    {
                        EmployeeId: ko.observable("123"),
                        StartDate: ko.observable(new Date(2016, 11, 1)),
                        EndDate: ko.observable(new Date(2016, 11, 6))
                    }
                ])
            }
        }

        spyOn(actions, "CalculateOvertimeAsync").and.returnValue(Promise.resolve({
            TotalOvertime: 5.00
        }));
        spyOn(resources, "getObservable").and.returnValue(resourceManagerSpyReturnPlaceholderValue);

        viewModel = new ApproveTimecardViewModel(params);
    });

    describe("general functions", function () {
        it("should construct daysInWeek from timecard collection", function () {
            var actual = viewModel.daysInWeek(),
            expected = [new Date(2016, 10, 30), new Date(2016, 11, 1), new Date(2016, 11, 2), new Date(2016, 11, 3),
                        new Date(2016, 11, 4), new Date(2016, 11, 5), new Date(2016, 11, 6)]
            expect(actual).toEqual(expected);
        });
        it("should retrieve the detail start and end time placeholders", function () {
            var actualStartTimePlaceholder = viewModel.dailyDetailTimeEntryStartTimePlaceholder,
                actualEndTimePlaceholder = viewModel.dailyDetailTimeEntryEndTimePlaceholder,
                expectedStartTimePlaceholder = resourceManagerSpyReturnPlaceholderValue,
                expectedEndTimePlaceholder = resourceManagerSpyReturnPlaceholderValue;
            expect(actualStartTimePlaceholder).toEqual(expectedStartTimePlaceholder);
            expect(actualEndTimePlaceholder).toEqual(expectedEndTimePlaceholder);
        });
        it("should return the work entry earnings type description", function () {
            var actualWorkEntryDescription = viewModel.workEntryDescription({
                EarningsTypes: ko.observableArray([{ Id: ko.observable("FOO"), Description: ko.observable("foo") },
                                                   { Id: ko.observable("BAR"), Description: ko.observable("Bar") }]) },  // this is for the timecard argument
                { EarningsTypeId: ko.observable("BAR") }),  // this is for the workEntry argument
                expectedWorkEntryDescription = "Bar";
            expect(actualWorkEntryDescription).toEqual(actualWorkEntryDescription);
        });
        it("should return the timecard's status object", function () {
            var actualTimecardStatusObject = viewModel.timecardStatusObject({ TimecardStatus: ko.observable(0) });
            expectedTimecardStatusObject = status.get(0);
            expect(actualTimecardStatusObject).toEqual(expectedTimecardStatusObject);
        });
        it("should recognize that a timecard has a status", function () {
            var actualHasTimecardStatus = viewModel.hasTimecardStatus({ TimecardStatus: ko.observable(0) });
            expect(actualHasTimecardStatus).toBeTruthy;
        });
        it("should recognize that a timecard has no status", function () {
            var actualHasTimecardStatus = viewModel.hasTimecardStatus({ TimecardStatus: ko.observable(null) });
            expect(actualHasTimecardStatus).toBeFalsy;
            var actualHasTimecardStatus = viewModel.hasTimecardStatus({ TimecardStatus: ko.observable(undefined) });
            expect(actualHasTimecardStatus).toBeFalsy;
        });

    });

    describe("isTimecardSupervised function", function () {
        it("should recognize a timecard for a supervised position when timecard has a supervisor Id", function () {
            params.supervisorId = ko.observable('456');  // this is the id of the current employee (supervisor) 
            viewModel = new ApproveTimecardViewModel(params);
            var actualIsTimecardSupervised = viewModel.isTimecardSupervised({
                SupervisorId: ko.observable('456')  // this is the id of the supervisor for the time on a timecard
            });
            expect(actualIsTimecardSupervised).toBeTruthy;
        });
        it("should recognize a timecard for a non-supervised position when timecard has a supervisor Id", function () {
            params.supervisorId = ko.observable('456');
            viewModel = new ApproveTimecardViewModel(params);
            var actualIsTimecardSupervised = viewModel.isTimecardSupervised({
                SupervisorId: ko.observable('789')
            });
            expect(actualIsTimecardSupervised).toBeFalsy;
        });
        it("should recognize a timecard for a supervised position when timecard has supervisor position Id", function () {
            params.supervisorId = ko.observable('456');
            params.supervisorPositionIds = ko.observableArray(["Pos1", "Pos2", "Pos3"]);
            viewModel = new ApproveTimecardViewModel(params);
            var actualIsTimecardSupervised = viewModel.isTimecardSupervised({
                SupervisorId: ko.observable(null),
                SupervisorPositionId: ko.observable('Pos2')
            });
            expect(actualIsTimecardSupervised).toBeTruthy;
        });
        it("should recognize a timecard for a non-supervised position when timecard has supervisor position Id", function () {
            params.supervisorId = ko.observable('456');
            params.supervisorPositionIds = ko.observableArray(["Pos1", "Pos2", "Pos3"]);
            viewModel = new ApproveTimecardViewModel(params);
            var actualIsTimecardSupervised = viewModel.isTimecardSupervised({
                SupervisorId: ko.observable(null),
                SupervisorPositionId: ko.observable("Pos4")
            });
            expect(actualIsTimecardSupervised).toBeFalsy;
        });
    });

    describe("dailyTimeEntryType function", function () {
        it("should recognize a summary timecard", function () {
            var actualDailyTimeEntryType = viewModel.dailyTimeEntryType({
                DailyTimeEntryType: ko.observable(1),
            });
            expect(actualDailyTimeEntryType.isSummary).toBeTruthy;
            expect(actualDailyTimeEntryType.isDetail).toBeFalsy;
        });
        it("should recognize a detail timecard", function () {
            var actualDailyTimeEntryType = viewModel.dailyTimeEntryType({
                DailyTimeEntryType: ko.observable(2),
            });
            expect(actualDailyTimeEntryType.isSummary).toBeFalsy;
            expect(actualDailyTimeEntryType.isDetail).toBeTruthy;
        });
        it("should throw an error if timecard time entry type is neither summary nor detail", function () {
            var errCnt = 0;
            try {
                var actualDailyTimeEntryType = viewModel.dailyTimeEntryType({
                    DailyTimeEntryType: ko.observable(null),
                });
            }
            catch (err) { errCnt++; }
            try {
                var actualDailyTimeEntryType = viewModel.dailyTimeEntryType({
                    DailyTimeEntryType: ko.observable(undefined),
                });
            }
            catch (err) { errCnt++; }
            try {
                var actualDailyTimeEntryType = viewModel.dailyTimeEntryType({
                    DailyTimeEntryType: ko.observable(3),
                });
            }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(3);
        });
    });

    describe("formattedTimeStart and formattedTimeEnd functions", function () {
        it("should return formatted time start string and isMissing equal false when time in is populated", function () {
            var actualFormattedTimeStart = viewModel.formattedTimeStart({
                DateWorked: ko.observable(new Date(2016, 11, 1)),
                TimeStart: ko.observable(new Date(2016, 11, 1, 8, 0, 0, 0))
            }),
                expectedFormattedTime = "8:00 AM";
            expect(actualFormattedTimeStart.formattedTime).toEqual(expectedFormattedTime);
            expect(actualFormattedTimeStart.isMissing).toBeFalsy();
        });
        it("should return time start placeholder string and isMissing equal true when time in is missing", function () {
            var actualFormattedTimeStart = viewModel.formattedTimeStart({
                DateWorked: ko.observable(new Date(2016, 11, 1)),
                TimeStart: ko.observable(null)
            }),
                expectedFormattedTime = resourceManagerSpyReturnPlaceholderValue();
            expect(actualFormattedTimeStart.formattedTime).toEqual(expectedFormattedTime);
            expect(actualFormattedTimeStart.isMissing).toBeTruthy();
        });
        it("should return formatted time end string and isMissing equal false when time out is populated", function () {
            var actualFormattedTimeEnd = viewModel.formattedTimeEnd({
                DateWorked: ko.observable(new Date(2016, 11, 1)),
                TimeEnd: ko.observable(new Date(2016, 11, 1, 17, 0, 0, 0))
            }),
                expectedFormattedTime = "5:00 PM";
            expect(actualFormattedTimeEnd.formattedTime).toEqual(expectedFormattedTime);
            expect(actualFormattedTimeEnd.isMissing).toBeFalsy();
        });
        it("should return time end placeholder string and isMissing equal true when time out is missing", function () {
            var actualFormattedTimeEnd = viewModel.formattedTimeEnd({
                DateWorked: ko.observable(new Date(2016, 11, 1)),
                TimeEnd: ko.observable(null)
            }),
                expectedFormattedTime = resourceManagerSpyReturnPlaceholderValue();
            expect(actualFormattedTimeEnd.formattedTime).toEqual(expectedFormattedTime);
            expect(actualFormattedTimeEnd.isMissing).toBeTruthy();
        });
    });

    describe("isOutsideTimecardDates function", function () {
        it("should flag date before timecard start date as outside timecard dates", function () {
            params.dataModel.EffectiveStartDate = ko.observable(new Date(2016, 10, 27));
            params.dataModel.StartDate = ko.observable(new Date(2016, 11, 1))
            viewModel = new ApproveTimecardViewModel(params);
            var actual = viewModel.isOutsideTimecardDates(ko.observable(2));
            expect(actual).toBeTruthy();
        });
        it("should flag date after timecard end date as outside timecard dates", function () {
            params.dataModel.EffectiveStartDate = ko.observable(new Date(2016, 10, 27));
            params.dataModel.EndDate = ko.observable(new Date(2016, 10, 30))
            viewModel = new ApproveTimecardViewModel(params);
            var actual = viewModel.isOutsideTimecardDates(ko.observable(4));
            expect(actual).toBeTruthy();
        });
        it("should not flag date between timecard start and end dates as outside timecard dates", function () {
            params.dataModel.EffectiveStartDate = ko.observable(new Date(2016, 10, 27));
            params.dataModel.StartDate = ko.observable(new Date(2016, 10, 27))
            params.dataModel.EndDate = ko.observable(new Date(2016, 10, 30))
            viewModel = new ApproveTimecardViewModel(params);
            var actual = viewModel.isOutsideTimecardDates(ko.observable(3));
            expect(actual).toBeFalsy();
        });
    });

    describe("hours accumulators", function () {

        var timecardPos1 = {},
            timecardPos2 = {},
            workEntryPos1 = {},
            workEntryPos2 = {},
            timeEntryGroupPos1Day1 = {},
            timeEntryGroupPos1Day2 = {},
            timeEntryGroupPos1Day3 = {},
            timeEntryGroupPos2Day1 = {},
            timeEntryGroupPos2Day2 = {},
            timeEntryGroupPos2Day3 = {},
            timeEntryPos1Day1Entry1 = {},
            timeEntryPos1Day1Entry2 = {},
            timeEntryPos1Day2Entry1 = {},
            timeEntryPos1Day2Entry2 = {},
            timeEntryPos1Day3Entry1 = {},
            timeEntryPos1Day3Entry2 = {},
            timeEntryPos2Day1Entry1 = {},
            timeEntryPos2Day1Entry2 = {},
            timeEntryPos2Day2Entry1 = {},
            timeEntryPos2Day2Entry2 = {},
            timeEntryPos2Day3Entry1 = {},
            timeEntryPos2Day3Entry2 = {}

        beforeEach(function () {
            timeEntryPos1Day1Entry1 = { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(1) },
            timeEntryPos1Day1Entry2 = { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(0.1) },
            timeEntryPos1Day2Entry1 = { DateWorked: ko.observable(new Date(2016, 11, 1)), HoursWorked: ko.observable(2) },
            timeEntryPos1Day2Entry2 = { DateWorked: ko.observable(new Date(2016, 11, 1)), HoursWorked: ko.observable(0.2) },
            timeEntryPos1Day3Entry1 = { DateWorked: ko.observable(new Date(2016, 11, 2)), HoursWorked: ko.observable(3) },
            timeEntryPos1Day3Entry2 = { DateWorked: ko.observable(new Date(2016, 11, 2)), HoursWorked: ko.observable(0.3) },
            timeEntryPos2Day1Entry1 = { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(1) },
            timeEntryPos2Day1Entry2 = { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(3) },
            timeEntryPos2Day2Entry1 = { DateWorked: ko.observable(new Date(2016, 11, 1)), HoursWorked: ko.observable(2) },
            timeEntryPos2Day2Entry2 = { DateWorked: ko.observable(new Date(2016, 11, 1)), HoursWorked: ko.observable(4) },
            timeEntryPos2Day3Entry1 = { DateWorked: ko.observable(new Date(2016, 11, 2)), HoursWorked: ko.observable(3) },
            timeEntryPos2Day3Entry2 = { DateWorked: ko.observable(new Date(2016, 11, 2)), HoursWorked: ko.observable(5) },

            timeEntryGroupPos1Day1 = {
                DateWorked: ko.observable(new Date(2016, 10, 30)),
                DailyTimeEntryList: ko.observableArray([timeEntryPos1Day1Entry1, timeEntryPos1Day1Entry2])
            },
            timeEntryGroupPos1Day2 = {
                DateWorked: ko.observable(new Date(2016, 11, 1)),
                DailyTimeEntryList: ko.observableArray([timeEntryPos1Day2Entry1, timeEntryPos1Day2Entry2])
            },
            timeEntryGroupPos1Day3 = {
                DateWorked: ko.observable(new Date(2016, 11, 2)),
                DailyTimeEntryList: ko.observableArray([timeEntryPos1Day3Entry1, timeEntryPos1Day3Entry2])
            },
            timeEntryGroupPos2Day1 = {
                DateWorked: ko.observable(new Date(2016, 10, 30)),
                DailyTimeEntryList: ko.observableArray([timeEntryPos2Day1Entry1, timeEntryPos2Day1Entry2])
            },

            timeEntryGroupPos2Day2 = {
                DateWorked: ko.observable(new Date(2016, 11, 1)),
                DailyTimeEntryList: ko.observableArray([timeEntryPos2Day2Entry1, timeEntryPos2Day2Entry2])
            },
            timeEntryGroupPos2Day3 = {
                DateWorked: ko.observable(new Date(2016, 11, 2)),
                DailyTimeEntryList: ko.observableArray([timeEntryPos2Day3Entry1, timeEntryPos2Day3Entry2])
            }

            workEntryPos1 = {
                EarningsTypeId: "ET1",
                DailyTimeEntryGroupList: ko.observableArray([timeEntryGroupPos1Day1, timeEntryGroupPos1Day2, timeEntryGroupPos1Day3])
            },
            workEntryPos2 = {
                EarningsTypeId: "ET1",
                DailyTimeEntryGroupList: ko.observableArray([timeEntryGroupPos2Day1, timeEntryGroupPos2Day2, timeEntryGroupPos2Day3])
            }

            timecardPos1 = {
                EmployeeId: ko.observable("123"),
                StartDate: ko.observable(new Date(2016, 11, 1)),
                EndDate: ko.observable(new Date(2016, 11, 6)),
                PositionId: ko.observable("001"),
                PositionTitle: ko.observable("Pos Title 1"),
                WorkEntryList: ko.observableArray([workEntryPos1])
            },

            timecardPos2 = {
                EmployeeId: ko.observable("123"),
                StartDate: ko.observable(new Date(2016, 11, 1)),
                EndDate: ko.observable(new Date(2016, 11, 6)),
                PositionId: ko.observable('002'),
                PositionTitle: ko.observable("Pos Title 2"),
                WorkEntryList: ko.observableArray([workEntryPos2])
            },

            params = {
                dataModel: {
                    EmployeeId: ko.observable("123"),
                    EmployeeName: ko.observable('Doe, John'),
                    EffectiveStartDate: ko.observable(new Date(2016, 10, 30)),
                    StartDate: ko.observable(new Date(2016, 11, 1)),
                    EndDate: ko.observable(new Date(2016, 11, 6)),
                    PayCycleId: ko.observable("M1"),
                    Timecards: ko.observableArray([timecardPos1, timecardPos2])
                }
            },

            viewModel = new ApproveTimecardViewModel(params);

        });

        it("should calculate totalHours for a single timecard", function () {
            var totalhours = viewModel.totalHours(timecardPos1);
            expect(totalhours).toEqual(6.6);
        });
        it("should calculate totalHours for a timecard collection", function () {
            var totalhours = viewModel.totalHours();
            expect(totalhours).toEqual(24.6);
        });
        it("should calculate totalWorkEntryHours for a work entry", function () {
            var totalWorkEntryHours = viewModel.totalWorkEntryHours(workEntryPos1);
            expect(totalWorkEntryHours).toEqual(6.6);
        });
        it("should calculate totalDayHours for a day in a single timecard", function () {
            var totalDayHours = viewModel.totalDayHours(ko.observable(0), timecardPos1);
            expect(totalDayHours).toEqual(1.1);
        });
        it("should calculate totalDayHours for a day in a timecard collection", function () {
            var totalDayHours = viewModel.totalDayHours(ko.observable(0));
            expect(totalDayHours).toEqual(5.1);
        });
        it("should calculate dailyTimeHoursWorked for a time entry group", function () {
            var dailyTimeHoursWorked = viewModel.dailyTimeHoursWorked(timeEntryGroupPos1Day2);
            expect(dailyTimeHoursWorked).toEqual(2.2);
        });
        // need to debug the following test
        xit("should calculate totalRegularWorkHours for a timecard collection", function () {
            const viewModel = component.viewModel;
            const innerHtml = document.body.innerHTML; // this binds the pureComputed totalRegularWorkHours to the view so this test will work
            document.body.innerHTML = innerHtml.concat(component.template);
            let model;
            model = new viewModel(params);
            ko.applyBindings(model);

            var totalRegularWorkHours = model.totalRegularWorkHours();
            expect(totalRegularWorkHours).toEqual(19.6);

            ko.cleanNode(document.body);
        });
    });

});