﻿var component = require("TimeManagement/TimeSheet/DailyTimeEntryGroup/_DailyTimeEntryGroup"),
    dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping'),
    resources = require('Site/resource.manager');

describe("Test Suite for DailyTimeEntryGroup component", function () {
    
    let params,
        viewModel;

    beforeEach(function () {
        params = {
            dataModel: {
                Id: ko.observable('001'),
                DateWorked: ko.observable(new Date(2016,10,28)),
                IsReadOnly: ko.observable(false),
                DailyTimeEntryList: ko.observableArray([])
            },
            dailyTimeEntryType: ko.observable(1),
            isReadOnly: ko.observable(false),
            onDailyTimeEntryChange: (dailyTimeEntryGroupModel) => { }
        };

        viewModel = new component.viewModel(params);
    });

    describe('daily time entry group model', function () {
        it('should merge the data model to self',function(){
            expect(viewModel.Id).toBeDefined();
            expect(viewModel.DateWorked).toBeDefined();
            expect(viewModel.IsReadOnly).toBeDefined();
            expect(viewModel.DailyTimeEntryList).toBeDefined()
        });
    });

})