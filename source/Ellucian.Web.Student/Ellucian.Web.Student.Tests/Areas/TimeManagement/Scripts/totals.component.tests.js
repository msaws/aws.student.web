﻿var component = require("TimeManagement/TimeSheet/Totals/_Totals"),
    timeSheetEvents = require('TimeManagement/TimeSheet/time.sheet.events'),
    actions = require("TimeManagement/TimeSheet/time.sheet.actions"),
    timeSheetDay = require("TimeManagement/TimeSheet/time.sheet.day");
    
require("Site/jquery.ui.plugin_responsive-table");

describe("Test Suite for Totals component", function () {

    var params = {};
    var daysInWeek = [];
    var TotalsViewModel = component.viewModel;

    beforeEach(function () {


        /* intercept any save event */
        spyOn(actions, "CalculateOvertimeAsync").and.returnValue(Promise.resolve({
            TotalOvertime: 5.00
        }));

        daysInWeek = [new Date(2016, 10, 23), new Date(2016, 10, 24), new Date(2016, 10, 25), new Date(2016, 10, 26),
                      new Date(2016, 10, 27), new Date(2016, 10, 28), new Date(2016, 10, 29)];
        params = {
            timeSheetDays: ko.observable(daysInWeek.map(function(day, index) { 
                return new timeSheetDay([{
                    Id: ko.observable(index),
                    DateWorked: ko.observable(day),
                    HoursWorked: ko.observable(index+10),
                    TimeStart: ko.observable(null),
                    TimeEnd: ko.observable(null),
                    MessageChannel: ko.observableArray([])
                }])
            })),
            startDate: ko.observable(daysInWeek[0]),
            endDate: ko.observable(daysInWeek[6]),
            payCycleId: ko.observable('BW'),
            days: ko.observableArray(daysInWeek),
            selectedDay: ko.observable(daysInWeek[5])
        }

    });

    describe('argument verification', function () {
        let viewModel;
        //beforeEach(function () {
        //    viewModel = new TotalsViewModel(params);
        //});
        afterEach(function () {
            viewModel.dispose();
        });
        it("should succeed", function () {
            let errCnt = 0;
            try { viewModel = new TotalsViewModel(params); }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(0);
        });

        it("should throw error when params is null or undefined", function () {
            let errCnt = 0;
            try { viewModel = new TotalsViewModel(undefined); }
            catch (err) { errCnt++; viewModel.dispose();}
            try { viewModel = new TotalsViewModel(null); }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(2);
        });
        it("should throw error when timeSheetDays is null or undefined or not an observable, or not an array", function () {
            let errCnt = 0;
            try {
                params.timeSheetDays = undefined;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose();}
            try {
                params.timeSheetDays = null;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose(); }
            try {
                params.timeSheetDays = [];
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose(); }
            try {
                params.timeSheetDays = ko.observable("foobar");                
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++;}
            expect(errCnt).toEqual(4);
        });
        it("should throw error when startDate or EndDate is null or undefined", function () {
            let errCnt = 0;
            try {
                params.startDate = undefined;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose();}
            try {
                params.startDate = null;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose(); }
            try {
                params.EndDate = undefined;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose();}
            try {
                params.EndDate = null;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(4);
        });
        it("should throw error when payCycleId is null or undefined", function () {
            let errCnt = 0;
            try {
                params.payCycleId = undefined;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose();}
            try {
                params.payCycleId = null;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(2);
        });
        it("should throw error when days is null or undefined", function () {
            let errCnt = 0;
            try {
                params.days = undefined;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose();}
            try {
                params.days = null;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(2);
        });
        it("should throw error when selectedDay is null, undefined, or not an observable", function () {
            let errCnt = 0;
            try {
                params.selectedDay = undefined;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose();}
            try {
                params.selectedDay = null;
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; viewModel.dispose();}
            try {
                params.selectedDay = 'Sat';
                viewModel = new TotalsViewModel(params);
            }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(3);
        });
    });

    describe('total hours calculations', function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new TotalsViewModel(params);
        });
        afterEach(function () {
            viewModel.dispose();
        });
        it('should build dailyTotalHours array from dailyTotalHoursRegistry', function () {
                actualDailyTotalHours = viewModel.dailyTotalHours(),
                expectedDailyTotalHours = [
                    { dayString: 'Wednesday 11/23', dayHours: 10, isOutsideTimecardDates: false},
                    { dayString: 'Thursday 11/24', dayHours: 11, isOutsideTimecardDates: false },
                    { dayString: 'Friday 11/25', dayHours: 12, isOutsideTimecardDates: false },
                    { dayString: 'Saturday 11/26', dayHours: 13, isOutsideTimecardDates: false },
                    { dayString: 'Sunday 11/27', dayHours: 14, isOutsideTimecardDates: false },
                    { dayString: 'Monday 11/28', dayHours: 15, isOutsideTimecardDates: false },
                    { dayString: 'Tuesday 11/29', dayHours: 16, isOutsideTimecardDates: false }];
            expect(actualDailyTotalHours).toEqual(expectedDailyTotalHours);
        });
        it('should set weeklyTotalHours from dailyTotalHours array', function () {
            expect(viewModel.weeklyTotalHours()).toEqual(91);
        });
        it("should publish weeklyTotalHours on postbox topic", function () {
            let subscriber = ko.observable(null).subscribeTo(timeSheetEvents.TIMESHEET_TOTALHOURSUPDATE, true);
            expect(subscriber()).toEqual(viewModel.weeklyTotalHours());
            subscriber.unsubscribeFrom(timeSheetEvents.TIMESHEET_TOTALHOURSUPDATE);
        });
   
        it('should set weeklyRegularHours to weeklyTotalHours - totalOvertimeHours', function (done) {

            viewModel.totalOvertimeHours.subscribe(function(newVal) {
                expect(viewModel.weeklyRegularHours()).toEqual(86);
                expect(newVal).toEqual(5);
                done();
            });

        });

        it('should set selectedDayTotalHours for selected date', function () {
            let actualDayTotalHours = viewModel.selectedDayTotalHours();
            expect(actualDayTotalHours).toEqual(15);
        });
    });

    describe('day/date functions', function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new TotalsViewModel(params);
        });
        afterEach(function () {
            viewModel.dispose();
        });

        it('should return long name of the day of the week for getDayName', function () {
            let actualDayName = viewModel.getDayName(new Date(2016, 10, 23));
            expect(actualDayName).toEqual('Wednesday');
        });
        it('should return index in the days array if selectedDay is present in array', function () {
            params.selectedDay(new Date(2016, 10, 28));
            expect(viewModel.selectedDayIndex()).toEqual(5);
        });
        it('should return -1 if selectedDay is not present in the days array', function () {
            viewModel.dispose();
            params.selectedDay = ko.observable(new Date(2016, 11, 28));
            viewModel = new TotalsViewModel(params);
            expect(viewModel.selectedDayIndex()).toEqual(-1);
        });
    });

    describe("saving the timesheet", function () {
        let viewModel;
        beforeEach(function () {
            viewModel = new TotalsViewModel(params);
        });
        afterEach(function () {
            viewModel.dispose();
        })

        //skipping until SS Core updates ko.postbox (first sprint of 2.16) to use ko.postbox.reset
        xit("should recalculate overtime if the published object matches the timecard identifiers", function () {

            viewModel.dispose();
            ko.postbox.reset();

            viewModel = new TotalsViewModel(params);
            actions.CalculateOvertimeAsync.calls.reset();

            ko.postbox.publish(timeSheetEvents.TIMESHEET_SAVECOMPLETE, {
                startDate: params.days()[0],
                endDate: params.days()[6],
                payCycleId: params.payCycleId()
            })

            expect(actions.CalculateOvertimeAsync.calls.count()).toEqual(1);
        });

        xit("should not recalculate overtime if the published object does not match the timecard identifiers", function () {
            viewModel.dispose();
            ko.postbox.reset();

            viewModel = new TotalsViewModel(params);
            actions.CalculateOvertimeAsync.calls.reset();

            ko.postbox.publish(timeSheetEvents.TIMESHEET_SAVECOMPLETE, {
                startDate: new Date(),
                endDate: new Date(),
                payCycleId: "foobar"
            })

            expect(actions.CalculateOvertimeAsync).not.toHaveBeenCalled();
        });
    });

});