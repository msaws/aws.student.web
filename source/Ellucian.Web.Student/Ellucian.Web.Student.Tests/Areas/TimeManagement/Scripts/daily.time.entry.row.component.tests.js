﻿var component = require("TimeManagement/TimeSheet/DailyTimeEntryRow/_DailyTimeEntryRow"),
    dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping'),
    resources = require('Site/resource.manager');

describe("Test Suite for DailyTimeEntryRow component", function () {
    
    let params,
        viewModel;

    beforeEach(function () {
        params = {
            workEntryModel: {
                DailyTimeEntryGroupList: ko.observableArray([]),
                EarningsTypeId: ko.observable('ADJ'),
                Id: ko.observable('TMA-1'),
            },
            earningsTypeModels: [{ Id: ko.observable('ADJ'), Description: 'Regular Adjunct Pay', Category: 0, IsActive: true, Method: 0}],
            isReadOnly: ko.observable(false),
            isMobileObservable: ko.observable(false),
            daySelectedObservable: ko.observable(null),
            onWorkTypeChange: (newEarningsTypeId) => {},
            onDailyTimeEntryChange: (workTypeId, dailyTimeEntryModel) => { },
            dailyTimeEntryType: 2
        };

        viewModel = new component.viewModel(params);
    });

    describe('daily time entry row model', function () {
        it('should merge the data model to self',function(){
            expect(viewModel.DailyTimeEntryGroupList).toBeDefined();
            expect(viewModel.EarningsTypeId).toBeDefined();
            expect(viewModel.Id).toBeDefined();
        });
    });
})
