﻿var component = require("TimeManagement/TimeSheet/Timecard/_Timecard"),
    dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping'),
    resources = require('Site/resource.manager'),
    actions = require("TimeManagement/TimeSheet/time.sheet.actions"),
    autoSaveEvents = require('Site/Components/AutoSave/_AutoSaveEvents'),
    timeSheetEvents = require('TimeManagement/TimeSheet/time.sheet.events');

describe("Test Suite for Timecard component", function () {

    let params,
        viewModel;

    beforeEach(function () {
        params = {
            timecardModel: {
                TimecardId: ko.observable("12345"),
                EmployeeId: ko.observable("123"),
                PositionTitle: ko.observable("Pos Title 1"),
                PositionId: ko.observable("001"),
                PayCycleId: ko.observable("M1"),
                DailyTimeEntryType: ko.observable(1),
                StartDate: ko.observable(new Date(2016, 11, 1)),
                EndDate: ko.observable(new Date(2016, 11, 6)),
                PeriodStartDate: ko.observable(new Date(2016, 11, 1)),
                PeriodEndDate: ko.observable(new Date(2016, 11, 31)),
                EmployeeSubmitDeadline: ko.observable(new Date(2016, 12, 31)),
                SupervisorApproveDeadline: ko.observable(new Date(2017, 1, 15)),
                EarningsTypes: ko.observableArray(["ET1"]),
                WorkEntryList: ko.observableArray([
                    {
                        EarningsTypeId: "ET1",
                        DailyTimeEntryGroupList: ko.observableArray([
                            {
                                DateWorked: ko.observable(new Date(2016, 10, 30)),
                                DailyTimeEntryList: ko.observableArray([
                                    { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(1) },
                                    { DateWorked: ko.observable(new Date(2016, 10, 30)), HoursWorked: ko.observable(2) }
                                ])
                            },
                            {
                                DateWorked: ko.observable(new Date(2016, 11, 1)),
                                DailyTimeEntryList: ko.observableArray([
                                    { DateWorked: ko.observable(new Date(2016, 11, 30)), HoursWorked: ko.observable(3) },
                                    { DateWorked: ko.observable(new Date(2016, 11, 30)), HoursWorked: ko.observable(4) }
                                ])
                            }
                        ])
                    }
                ]),
                Comments: ko.observableArray([]),
                TimecardStatus: ko.observable(-1)
            },
            days: ko.observableArray([new Date(2016, 10, 30), new Date(2016, 11, 1), new Date(2016, 11, 2), new Date(2016, 11, 3),
                                      new Date(2016, 11, 4), new Date(2016, 11, 5), new Date(2016, 11, 6)]),
            selectedDayObservable: ko.observable(new Date(2016, 11, 1)),
            employeeName: ko.observable("John Doe")
        };

        //viewModel = new component.viewModel(params);
    });

    function setViewModel() {
        if (viewModel) {
            viewModel.dispose();
        }
        viewModel = new component.viewModel(params);
    }

    afterEach(function () {
        viewModel.dispose();
    });

    describe('timecard model', function () {

        beforeEach(function () {
            setViewModel();
        })

        it('should merge the data model to self', function () {
            expect(viewModel.EmployeeId).toBeDefined();
            expect(viewModel.StartDate).toBeDefined();
            expect(viewModel.EndDate).toBeDefined();
            expect(viewModel.PositionId).toBeDefined();
            expect(viewModel.PositionTitle).toBeDefined();
            expect(viewModel.WorkEntryList).toBeDefined();
        });
    });

    let shouldDescribe = describe
    if (__DEBUGTEST__) {
        //allows us to dynamically execute these work change handler tests that involve at least 3 seconds of idle time
        //for each test based on a webpack variable (that we define in webpack.conf.js). These tests are not executed during a 
        //debug test session, but will be executed during a single or coverage run.
        shouldDescribe = xdescribe
       
    }

    /*
     * PLEASE NOTE!!!!!!!!!
     * These tests are enabled/disabled dynamically based on the gulp task being run. If you're debugging tests, the following
     * suite is disabled so that the tests run quickly. Enable them by making a local code modification and undoing the change when you're done.
     * 
     */
    shouldDescribe("work change handler", function () {

        beforeEach(function () {
            spyOn(ko.postbox, "publish");
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 4000;
        });
        afterEach(function () {
            ko.postbox.publish.calls.reset();
        })

        function timeoutPromiseFactory() {
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    resolve();
                }, 3500);
            });
        }

        describe("when TimecardId exists", function () {

            beforeEach(function () {
                
                params.timecardModel.TimecardId("12345");
                setViewModel();
            });



            it("should save the timecard after 3 seconds", function (done) {
                spyOn(actions, "SaveTimecardAsync").and.returnValue(Promise.resolve());
                expect(viewModel.hasErrors()).toBeFalsy();
                viewModel.dailyTimeEntryChangeHandler("foo", "bar");
                timeoutPromiseFactory().then(function () {
                    expect(actions.SaveTimecardAsync).toHaveBeenCalled();
                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVESTART);
                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVECOMPLETE);
                    expect(ko.postbox.publish).toHaveBeenCalledWith(timeSheetEvents.TIMESHEET_SAVECOMPLETE, jasmine.any(Object));
                    done();
                });
            });

            it("should report errors if saving timecard failed", function (done) {
                spyOn(actions, "SaveTimecardAsync").and.returnValue(Promise.reject("foobar"));
                expect(viewModel.hasErrors()).toBeFalsy();
                spyOn(console, "log");

                viewModel.dailyTimeEntryChangeHandler("foo", "bar");

                timeoutPromiseFactory().then(function () {
                    expect(actions.SaveTimecardAsync).toHaveBeenCalled();
                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVESTART);
                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVEERROR);
                    expect(console.log).toHaveBeenCalledWith("foobar");
                    done();
                });
            });

            it("should not attempt to save timecard if errors exist", function (done) {
                spyOn(actions, "SaveTimecardAsync");
                viewModel.hasErrors(true);

                viewModel.dailyTimeEntryChangeHandler("foo", "bar");
                timeoutPromiseFactory().then(function () {
                    expect(actions.SaveTimecardAsync).not.toHaveBeenCalled();
                    expect(ko.postbox.publish).not.toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVESTART);
                    done();
                });
            });

        });

        describe("when TimecardId does not exist", function () {

            beforeEach(function () {
                params.timecardModel.TimecardId("");
                setViewModel();
            });

            it("should create the timecard after 3 seconds", function (done) {
                spyOn(actions, "CreateTimecardAsync").and.returnValue(Promise.resolve("1234"));
                expect(viewModel.hasErrors()).toBeFalsy();
                viewModel.dailyTimeEntryChangeHandler("foo", "bar");
                timeoutPromiseFactory().then(function () {
                    expect(actions.CreateTimecardAsync).toHaveBeenCalled();
                    expect(viewModel.TimecardId()).toEqual("1234");

                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVESTART);
                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVECOMPLETE);
                    expect(ko.postbox.publish).toHaveBeenCalledWith(timeSheetEvents.TIMESHEET_SAVECOMPLETE, jasmine.any(Object));
                    done();
                });
            });

            it("should report errors if creating the timecard failed", function (done) {
                spyOn(actions, "CreateTimecardAsync").and.returnValue(Promise.reject("foobar"));
                expect(viewModel.hasErrors()).toBeFalsy();
                spyOn(console, "log");

                viewModel.dailyTimeEntryChangeHandler("foo", "bar");
                timeoutPromiseFactory().then(function () {
                    expect(actions.CreateTimecardAsync).toHaveBeenCalled();
                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVESTART);
                    expect(ko.postbox.publish).toHaveBeenCalledWith(autoSaveEvents.AUTOSAVE_SAVEERROR);
                    done();
                });
            });

            it("should not attempt to create timecard if errors exist", function (done) {
                spyOn(actions, "CreateTimecardAsync");;
                viewModel.hasErrors(true);
                viewModel.dailyTimeEntryChangeHandler("foo", "bar");
                timeoutPromiseFactory().then(function () {
                    expect(actions.CreateTimecardAsync).not.toHaveBeenCalled();
                    done();
                });
            });
        });

        describe("when a second save is attempted while creating timecards", function () {

            beforeEach(function () {
                params.timecardModel.TimecardId("");
                setViewModel();
                jasmine.DEFAULT_TIMEOUT_INTERVAL = 7100;
            });

            it("should wait until the create is complete", function (done) {
                spyOn(actions, "CreateTimecardAsync").and.returnValue(Promise.resolve("1234"));
                spyOn(actions, "SaveTimecardAsync").and.returnValue(Promise.resolve());

                viewModel.dailyTimeEntryChangeHandler("foo", "bar");
                viewModel.dailyTimeEntryChangeHandler("foo", "bar");

                timeoutPromiseFactory().then(function () {
                    expect(actions.CreateTimecardAsync).toHaveBeenCalled();
                    expect(viewModel.TimecardId()).toEqual("1234");
                    expect(actions.SaveTimecardAsync).not.toHaveBeenCalled();
                    return timeoutPromiseFactory();
                }).then(function () {
                    expect(actions.SaveTimecardAsync).toHaveBeenCalled();
                    done();
                });
            });
        });
    })
})
