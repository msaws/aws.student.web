﻿   var timeSheetEvents = require("TimeManagement/TimeSheet/time.sheet.events"),
        resourceManager = require("Site/resource.manager"),
       errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");
        ActionBarViewModel = require("TimeManagement/TimeSheet/TimecardCollectionActionBar/_ActionBar").viewModel;;

    describe("Test Suite for Timecard Collection ActionBar component", function () {
        var params = {},
            days = [],
            resourceManagerSpyReturnValue = ko.observable("foobar"),
            viewModel;



        beforeEach(function () {
            window.location.hash = "s=4/28/2016&e=4/31/2016&p=BW&pps=4/1/2016&ppe=4/31/2016";

            spyOn(resourceManager, "getObservable").and.returnValue(resourceManagerSpyReturnValue);
            spyOn($.fn, "on"); //captures the scroll event to prevent errors. not 100% sure how to test the sticky action bar

            days = [new Date(2016, 4, 28), new Date(2016, 4, 29), new Date(2016, 4, 30), new Date(2016, 4, 31)];
            params = {
                isMobile: ko.observable(false),
                mobileDaySelectorDates: ko.observableArray(days),
                selectedDate: ko.observable(days[0]),
                previousCollectionDateRange: ko.observable({ startDate: days[0], endDate: days[3], periodStartDate: days[0], periodEndDate: days[3] }),
                nextCollectionDateRange: ko.observable({ startDate: days[0], endDate: days[3], periodStartDate: days[0], periodEndDate: days[3] })
            }           

            viewModel = new ActionBarViewModel(params);
        });



        afterEach(function () {
            viewModel.dispose();
        })

        describe("Input Params", function () {
            it("should set isMobile", function () {
                expect(viewModel.isMobile()).toEqual(params.isMobile());
                params.isMobile(true);
                expect(viewModel.isMobile()).toEqual(params.isMobile());
            });

            it("should set mobileDaySelectorDates", function () {
                expect(viewModel.mobileDaySelectorDates).toEqual(params.mobileDaySelectorDates);
            });

            it("should set weekDisplay from mobileDaySelectorDates", function () {
                var expected = "Week {0} - {1}".format(Globalize.format(params.mobileDaySelectorDates()[0], "d"), Globalize.format(params.mobileDaySelectorDates()[3], "d"));
                expect(viewModel.weekDisplay()).toEqual(expected);
            })
        });

        describe("get day picker string function", function () {
            it("should return name of day and month/day", function () {
                var saturday = new Date(2016, 5, 4);
                expect(viewModel.getDayPickerString(saturday)).toEqual("Saturday 6/4");
            });

            it("should use globalization formatting", function () {
                spyOn(Globalize, "format");
                var saturday = new Date(2016, 5, 4);
                viewModel.getDayPickerString(saturday)
                expect(Globalize.format).toHaveBeenCalledWith(saturday, "dddd");
                expect(Globalize.format).toHaveBeenCalledWith(saturday, "M/d");
            });
        });

        describe("Total Hours", function () {
            beforeEach(function () {
                resourceManagerSpyReturnValue("{0} foobar");
                viewModel.dispose();
                viewModel = new ActionBarViewModel(params);
            });

            it("should initialize to 0", function () {

                expect(viewModel.totalHoursDisplay()).toEqual("0.00 foobar");
            });

            it("should update via postbox event", function () {

                var expected = "54.30 foobar";
                ko.postbox.publish(timeSheetEvents.TIMESHEET_TOTALHOURSUPDATE, 54.3);
                expect(viewModel.totalHoursDisplay()).toEqual(expected);
            });

        });

        describe("Collection DateRange parameters", function () {
            it("should set previous href by replacing elements of the url hash with previousCollectionDateRangeValues", function () {
                expect(viewModel.previousHref().match(/s=([^\n&]*)/)[1]).toEqual(Globalize.format(params.previousCollectionDateRange().startDate, "d"));
                expect(viewModel.previousHref().match(/e=([^\n&]*)/)[1]).toEqual(Globalize.format(params.previousCollectionDateRange().endDate, "d"));
                expect(viewModel.previousHref().match(/pps=([^\n&]*)/)[1]).toEqual(Globalize.format(params.previousCollectionDateRange().periodStartDate, "d"));
                expect(viewModel.previousHref().match(/ppe=([^\n&]*)/)[1]).toEqual(Globalize.format(params.previousCollectionDateRange().periodEndDate, "d"));

                expect(viewModel.isPreviousTimecardEnabled()).toBeTruthy();
            });

            it("should disable previous action if no previous date range exists", function () {
                params.previousCollectionDateRange(null);
                expect(viewModel.previousHref()).toBeNull();
                expect(viewModel.isPreviousTimecardEnabled()).toBeFalsy();
            })

            it("should set next href by replacing elements of the url hash with nextCollectionDateRangeValues", function () {
                expect(viewModel.nextHref().match(/s=([^\n&]*)/)[1]).toEqual(Globalize.format(params.nextCollectionDateRange().startDate, "d"));
                expect(viewModel.nextHref().match(/e=([^\n&]*)/)[1]).toEqual(Globalize.format(params.nextCollectionDateRange().endDate, "d"));
                expect(viewModel.nextHref().match(/pps=([^\n&]*)/)[1]).toEqual(Globalize.format(params.nextCollectionDateRange().periodStartDate, "d"));
                expect(viewModel.nextHref().match(/ppe=([^\n&]*)/)[1]).toEqual(Globalize.format(params.nextCollectionDateRange().periodEndDate, "d"));

                expect(viewModel.isNextTimecardEnabled()).toBeTruthy();
            });

            it("should disable next action if no next date range exists", function () {
                params.nextCollectionDateRange(null);
                expect(viewModel.nextHref()).toBeNull();
                expect(viewModel.isNextTimecardEnabled()).toBeFalsy();

            });
        });

        describe("Time Sheet Errors", function () {
            var errorArray = [];

            beforeEach(function () {
                errorArray = [{
                    id: errorDefinitions.keys.overlapError,
                    type: errorDefinitions.types.error,
                    message: "foobarraboof",
                    date: days[0],
                },
                {
                    id: errorDefinitions.keys.missingTimeWarning,
                    type: errorDefinitions.types.warning,
                    message: "rabooffoobar",
                    date: days[1],
                }];
            });

            it("should initialize to no errors", function () {
                expect(viewModel.timeSheetErrorMessages()).toEqual([]);
                expect(viewModel.timeSheetWarningMessages()).toEqual([]);
                expect(viewModel.mobileNotificationMessage()).toBeNull();
            });

            it("should update timeSheetErrorMessages on TIMESHEET_MESSAGES postbox topic", function () {
                ko.postbox.publish(timeSheetEvents.TIMESHEET_MESSAGES, errorArray);
                expect(viewModel.timeSheetErrorMessages().length).toEqual(1);
                expect(viewModel.timeSheetErrorMessages()[0]).toEqual(Globalize.format(errorArray[0].date, "dddd") + " - " + errorArray[0].message);
            });

            it("should update timeSheetWarningMessages on TIMESHEET_MESSAGES postbox topic", function () {
                ko.postbox.publish(timeSheetEvents.TIMESHEET_MESSAGES, errorArray);
                expect(viewModel.timeSheetWarningMessages().length).toEqual(1);
                expect(viewModel.timeSheetWarningMessages()[0]).toEqual(Globalize.format(errorArray[1].date, "dddd") + " - " + errorArray[1].message);
            });

            it("should update mobileNotificationMessage on TIMESHEET_MESSAGES postbox topic", function () {
                ko.postbox.publish(timeSheetEvents.TIMESHEET_MESSAGES, errorArray);
                expect(viewModel.mobileNotificationMessage()).not.toBeNull();//.toEqual({ type: errorDefinitions.types.error, message: viewModel.timeSheetErrorMessages()[0] });
            });

            it("should prefer last error message over warnings in mobileNotificationMessage", function () {
                errorArray.push({
                    id: errorDefinitions.keys.overlapError,
                    type: errorDefinitions.types.error,
                    message: "preferredMessage",
                    date: days[1],
                });
                ko.postbox.publish(timeSheetEvents.TIMESHEET_MESSAGES, errorArray);
                expect(viewModel.mobileNotificationMessage()).toEqual({ type: errorDefinitions.types.error, message: viewModel.timeSheetErrorMessages()[viewModel.timeSheetErrorMessages().length - 1] });
            });

            it("should prefer last warning message if no errors in mobileNotificationMessage", function () {
                errorArray[0].type = errorDefinitions.types.warning;
                ko.postbox.publish(timeSheetEvents.TIMESHEET_MESSAGES, errorArray);
                expect(viewModel.mobileNotificationMessage()).toEqual({ type: errorDefinitions.types.warning, message: viewModel.timeSheetWarningMessages()[viewModel.timeSheetWarningMessages().length - 1] });
            });
        });

        describe("After Dropdown component is rendered, the postProcessDropdown function", function () {
            it("should add an esg fluid class to the dropdown via jquery if isMobile is true", function () {
                spyOn($.fn, "addClass");
                viewModel.isMobile(true);
                viewModel.postProcessDropdown();
                expect($.fn.addClass).toHaveBeenCalledWith("esg-button-group--fluid");
            });

            it("should not add fluid class to the drop via jquery if isMobile is false", function () {
                spyOn($.fn, "addClass");
                viewModel.isMobile(false);
                viewModel.postProcessDropdown();
                expect($.fn.addClass).not.toHaveBeenCalled();
            });
        });

    });

    
    



