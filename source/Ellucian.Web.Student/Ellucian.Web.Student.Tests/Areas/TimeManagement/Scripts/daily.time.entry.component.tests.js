﻿const component = require("TimeManagement/TimeSheet/DailyTimeEntry/_DailyTimeEntry"),
    errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");

describe("Test Suite for DailyTimeEntry component", function () {

    let params = null;
    let DailyTimeEntryViewModel = component.viewModel;

    beforeEach(function () {
        params = {
            dailyTimeEntryModel: {
                Id: ko.observable("12345"),
                DateWorked: ko.observable(new Date(2016, 5, 27)),
                HoursWorked: ko.observable(7.75),
                IsReadOnly: ko.observable(false),
                MessageChannel: ko.observableArray([])
            },
            onHoursWorkedChange: function (model) { return "success" },
            isReadOnly: ko.observable(false)
        }
    });

    describe('argument verification', function() {
        it("should succeed", function () {
            let errCnt = 0;
            try { new DailyTimeEntryViewModel(params); }
            catch (err) { errCnt++; }
            expect(errCnt).toEqual(0);
        });
        it("should throw error when params is null or undefined", function() {
            let errCnt = 0;
            try { new DailyTimeEntryViewModel(undefined); }
            catch(err) { errCnt++; }
            try { new DailyTimeEntryViewModel(null); }
            catch(err) { errCnt++; }
            expect(errCnt).toEqual(2);
        });
        it("should throw error when isReadOnly is defined but not an observable", () => {
            let errCnt = 0;
            try { 
                params.isReadOnly = ko.observable(undefined);
                new DailyTimeEntryViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.isReadOnly = ko.observable(null);
                new DailyTimeEntryViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.isReadOnly = true;
                new DailyTimeEntryViewModel(params); 
            }
            catch(err) { errCnt++; }
            expect(errCnt).toEqual(1);
        });
        it("should throw error when onHoursWorkedChange is defined but not a function", () => {
            let errCnt = 0;
            try { 
                params.onHoursWorkedChange = undefined;
                new DailyTimeEntryViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.onHoursWorkedChange = null;
                new DailyTimeEntryViewModel(params); 
            }
            catch(err) { errCnt++; }
            try { 
                params.onHoursWorkedChange = {id: 1, val: 'a'};
                new DailyTimeEntryViewModel(params); 
            }
            catch(err) { errCnt++; }
            expect(errCnt).toEqual(1);
        });
    });

    describe('isReadOnly', function () {
        it('should be false when isReadOnly from params is false and IsReadOnly from the server is false', function () {
            params.isReadOnly = ko.observable(false);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(false);
            var actual = new DailyTimeEntryViewModel(params).isReadOnly();
            expect(actual).toBeFalsy;
        });
        it('should be true when isReadOnly from params is false and IsReadOnly from the server is true', function () {
            params.isReadOnly = ko.observable(false);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(true);
            var actual = new DailyTimeEntryViewModel(params).isReadOnly();
            expect(actual).toBeTruthy;
        });
        it('should be true when isReadOnly from params is true and IsReadOnly from the server is false', function () {
            params.isReadOnly = ko.observable(true);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(false);
            var actual = new DailyTimeEntryViewModel(params).isReadOnly();
            expect(actual).toBeTruthy;
        });
        it('should be true when isReadOnly from params is true and IsReadOnly from the server is true', function () {
            params.isReadOnly = ko.observable(true);
            params.dailyTimeEntryModel.IsReadOnly = ko.observable(true);
            var actual = new DailyTimeEntryViewModel(params).isReadOnly();
            expect(actual).toBeTruthy;
        });
    });

    describe('hasInputError', function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new DailyTimeEntryViewModel(params);
        });
        afterEach(function() {
            viewModel.dispose();
        });
        it("should return false if entry has no errors", function() {
            expect(viewModel.isReadOnly()).toBeFalsy();
            expect(viewModel.MessageChannel()).toEqual([]);
            expect(viewModel.hasTotalTimeError()).toBeFalsy();
            expect(viewModel.hasInputError()).toBeFalsy();
        });
        it("should return true if hasTotalTimeError is true", function() {
            viewModel.MessageChannel.push(errorDefinitions.keys.totalDayTimeError)
            expect(viewModel.hasTotalTimeError()).toBeTruthy();
            expect(viewModel.hasInputError()).toBeTruthy();
        })

        it('should return false if entry is read-only even if it has an error', function () {
            params.isReadOnly = ko.observable(true);
            viewModel = new DailyTimeEntryViewModel(params);
            viewModel.MessageChannel.push(errorDefinitions.keys.totalDayTimeError)
            expect(viewModel.hasInputError()).toBeFalsy();
        });        
    });

    describe("User input for hours worked", function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new DailyTimeEntryViewModel(params);
        });
        afterEach(function() {
            viewModel.dispose();
        });

        it("should default to HoursWorked in data model", function() {
            expect(viewModel.formattedHours()).toEqual(viewModel.HoursWorked().toString());
        })

        it("should accept null value and set HoursWorked in datamodel to null", function () {
            viewModel.formattedHours(null);
            expect(viewModel.HoursWorked()).toBeNull();
        });

        it("should normalize empty string to null", function () {
            viewModel.formattedHours("");
            expect(viewModel.HoursWorked()).toBeNull();
        });

        it("should parse string value to 2 decimal float", function () {           
            viewModel.formattedHours("8");
            expect(viewModel.HoursWorked()).toEqual(8.00);
        });

        it("should strip out non-digits and non-periods", function () {           
            viewModel.formattedHours("8d.0f0");
            expect(viewModel.HoursWorked()).toEqual(8.00);
        });

        it("should set non-numbers to 0", function () {
            viewModel.formattedHours("foobar");
            expect(viewModel.HoursWorked()).toEqual(0);
        });
    });

    describe("Subscribing to changes in user input for hours worked", function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new DailyTimeEntryViewModel(params);
        });
        afterEach(function() {
            viewModel.dispose();
        });

        it("should arrange for deleting the time entry", function () {
            
            viewModel.formattedHours(null);
            expect(viewModel.HoursWorked()).toBeNull();
            expect(viewModel.Id()).toBeNull();
        });

        it("should arrange for adding a new time entry", function () {
            viewModel.Id(null);
            viewModel.HoursWorked(null);
            viewModel.formattedHours("8.00");
            expect(viewModel.HoursWorked()).toEqual(8.00);
        });

        it("should arrange for changing a time entry", function () {
           
            viewModel.formattedHours("8.00");
            expect(viewModel.HoursWorked()).toEqual(8.00);
        });

        it("should invoke onHoursWorkedChange callback in params", function () {
            spyOn(params, "onHoursWorkedChange");            
            viewModel.formattedHours("8.00");
            expect(params.onHoursWorkedChange).toHaveBeenCalledWith(viewModel);
        });

        it("should not invoke onHoursWorkedChange callback if hasInputErrors is true", function() {
            spyOn(params, "onHoursWorkedChange");
            viewModel.MessageChannel.push(errorDefinitions.keys.totalDayTimeError);
            viewModel.formattedHours("8.00");
            expect(params.onHoursWorkedChange).not.toHaveBeenCalled();
        })
    });


})
