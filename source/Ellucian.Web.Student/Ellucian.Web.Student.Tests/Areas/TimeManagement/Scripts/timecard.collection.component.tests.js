﻿var TimecardCollectionComponent = require("TimeManagement/TimeSheet/TimecardCollection/_TimecardCollection"),
    resources = require("Site/resource.manager"),
    timeSheetEvents = require("TimeManagement/TimeSheet/time.sheet.events");

describe("Test Suite for TimecardCollection component", function () {

    var params,
        viewModel,
        storedDate,
        storedSessionValue,
        resourcesGetObservableSpyValue,
        //realLocalStorage,
        realEllucianGlobal,
        newViewModel = function () { //helper for when params need to change
            if (viewModel) { viewModel.dispose(); };
            viewModel = new TimecardCollectionComponent.viewModel(params);
            return viewModel;
        };

    beforeAll(function () {
        realEllucianGlobal = window.Ellucian;

        spyOn(Ellucian.Storage.local, "getItem").and.callFake(function (key) {
            return storedDate;
        });
        spyOn(Ellucian.Storage.local, "setItem").and.callFake(function (key, value) {
            storedDate = value;
        });
        spyOn(Ellucian.Storage.session, "getItem").and.callFake(function (key) {
            return storedSessionValue;
        });
        spyOn(Ellucian.Storage.session, "setItem").and.callFake(function (key, value) {
            storedSessionValue = value;
        });


        resourcesGetObservableSpyValue = ko.observable("foobar");
        spyOn(resources, "getObservable").and.returnValue(resourcesGetObservableSpyValue);
    });

    beforeEach(function () {
        storedDate = new Date(2017, 1, 17);
        storedSessionValue = true;

        params = {
            dataModel: ko.mapping.fromJS({
                EffectiveStartDate: new Date(2017, 1, 12),
                StartDate: new Date(2017, 1, 16),
                EndDate: new Date(2017, 1, 18),
                PayCycleId: "SM",
                NextStartDate: new Date(2017, 1, 19),
                NextEndDate: new Date(2017, 1, 25),
                NextPayPeriodStartDate: new Date(2017, 2, 1),
                NextPayPeriodEndDate: new Date(2017, 2, 15),
                PreviousStartDate: new Date(2017, 1, 12),
                PreviousEndDate: new Date(2017, 1, 15),
                PreviousPayPeriodStartDate: new Date(2017, 1, 1),
                PreviousPayPeriodEndDate: new Date(2017, 1, 15),
                EmployeeId: "0003914",
                EmployeeName: "DeDiana, Matt",
                Timecards: [
                    {
                        PositionId: "POSITIONX",
                        WorkEntryList: [
                            {
                                DailyTimeEntryGroupList: [
                                    {
                                        //Sunday
                                        DailyTimeEntryList: [
                                            {
                                                HoursWorked: 8,
                                                MessageChannel: [],
                                                TimeStart: null,
                                                TimeEnd: null,
                                                DateWorked: new Date(2017, 1, 12)
                                            }
                                        ]
                                    },
                                    {
                                        //Monday
                                        DailyTimeEntryList: [
                                            {
                                                HoursWorked: 8,
                                                MessageChannel: [],
                                                TimeStart: null,
                                                TimeEnd: null,
                                                DateWorked: new Date(2017, 1, 13)
                                            }
                                        ]
                                    },
                                    {
                                        //Tuesday
                                        DailyTimeEntryList: [
                                            {
                                                HoursWorked: 8,
                                                MessageChannel: [],
                                                TimeStart: null,
                                                TimeEnd: null,
                                                DateWorked: new Date(2017, 1, 14)
                                            }
                                        ]
                                    },
                                    {
                                        //Wednesday
                                        DailyTimeEntryList: [
                                            {
                                                HoursWorked: 8,
                                                MessageChannel: [],
                                                TimeStart: null,
                                                TimeEnd: null,
                                                DateWorked: new Date(2017, 1, 15)
                                            }
                                        ]
                                    },
                                    {
                                        //Thursday
                                        DailyTimeEntryList: [
                                            {
                                                HoursWorked: 8,
                                                MessageChannel: [],
                                                TimeStart: null,
                                                TimeEnd: null,
                                                DateWorked: new Date(2017, 1, 16)
                                            }
                                        ]
                                    },
                                    {
                                        //Friday
                                        DailyTimeEntryList: [
                                            {
                                                HoursWorked: 8,
                                                MessageChannel: [],
                                                TimeStart: null,
                                                TimeEnd: null,
                                                DateWorked: new Date(2017, 1, 17)
                                            }
                                        ]
                                    },
                                    {
                                        //Saturday
                                        DailyTimeEntryList: [
                                            {
                                                HoursWorked: 8,
                                                MessageChannel: [],
                                                TimeStart: null,
                                                TimeEnd: null,
                                                DateWorked: new Date(2017, 1, 18)
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    }
                ]
            })
        }
        viewModel = new TimecardCollectionComponent.viewModel(params);
    })

    afterEach(function () {
        viewModel.dispose();
    });

    afterAll(function () {
        window.Ellucian = realEllucianGlobal;
    });



    describe("Employee Name Display", function () {
        it("should display just employee name when no comma exists", function () {
            params.dataModel.EmployeeName("Matt DeDiana");
            newViewModel();
            expect(viewModel.employeeNameDisplay()).toEqual(viewModel.EmployeeName());
        });

        it("should display employee name by parsing data model if comma exists", function () {
            params.dataModel.EmployeeName("DeDiana, Matt");
            newViewModel();
            expect(viewModel.employeeNameDisplay()).toEqual("Matt DeDiana");
        });
    });

    describe("Available Days", function () {
        it("should be days between Start and End Dates, inclusive", function () {
            expect(viewModel.availableDays().length).toEqual(3);
            expect(viewModel.availableDays()[0]).toEqual(params.dataModel.StartDate());
            expect(viewModel.availableDays()[2]).toEqual(params.dataModel.EndDate());
        });
    });

    describe("Days in Week", function () {
        it("should start with the effective Start Date", function () {
            expect(viewModel.daysInWeek()[0]).toEqual(params.dataModel.EffectiveStartDate());
        });

        it("should contain 7 days", function () {
            expect(viewModel.daysInWeek().length).toEqual(7);
        });
    });

    describe("Selected Day", function () {
        it("should be initialized to date from storage if date is between Start and End Dates", function () {
            expect(viewModel.selectedDay()).toEqual(storedDate);
        });

        it("should be initialized to string date from storage if date is between Start and End Dates", function () {
            storedDate = moment(storedDate).format();
            newViewModel();
            expect(viewModel.selectedDay()).toEqual(moment(storedDate).toDate());
        });

        it("should be initialized to null if value in storage is null", function () {
            storedDate = null;
            newViewModel();
            expect(viewModel.selectedDay()).toBeNull();
        });

        it("should be initialized to null if value in storage is not a date", function () {
            storedDate = "foobar";
            newViewModel();
            expect(viewModel.selectedDay()).toBeNull();
        });

        it("should be initialized to null if value in storage is before StartDate", function () {
            storedDate = new Date(1969, 1, 1);
            newViewModel();
            expect(viewModel.selectedDay()).toBeNull();
        });

        it("should be initialize to null if value in storage is after EndDate", function () {
            storedDate = new Date(2050, 2, 1);
            newViewModel();
            expect(viewModel.selectedDay()).toBeNull();
        });

        it("should set date in storage when modified", function () {
            var expected = new Date(1986, 5, 4);
            viewModel.selectedDay(expected);
            expect(Ellucian.Storage.local.setItem).toHaveBeenCalled();
            expect(storedDate).toEqual(expected);
        });
    });

    describe("Collapsible Group State", function () {
        it("should be initialized to value from session storage if storage key found", function () {
            expect(viewModel.collapsibleGroupStateInfo[0].collapsibleIsOpenObs()).toEqual(storedSessionValue);
        });
        it("should be initialized to false if storage key not found", function () {
            storedSessionValue = null;
            newViewModel();
            expect(viewModel.collapsibleGroupStateInfo[0].collapsibleIsOpenObs()).toBeFalsy;
        });
        it("should set value in storage when modified", function () {
            viewModel.collapsibleGroupStateInfo[0].collapsibleIsOpenObs(ko.observable(false));
            expect(Ellucian.Storage.session.setItem).toHaveBeenCalled();
            expect(storedSessionValue).toBeFalsy;
        });
    });

    describe("Paginator Date Ranges", function () {
        it("should create Previous Date Range Object", function () {
            expect(viewModel.previousDateRange().startDate).toEqual(params.dataModel.PreviousStartDate());
            expect(viewModel.previousDateRange().endDate).toEqual(params.dataModel.PreviousEndDate());
            expect(viewModel.previousDateRange().periodStartDate).toEqual(params.dataModel.PreviousPayPeriodStartDate());
            expect(viewModel.previousDateRange().periodEndDate).toEqual(params.dataModel.PreviousPayPeriodEndDate());
        });
        it("should create Next Date Range object", function () {
            expect(viewModel.nextDateRange().startDate).toEqual(params.dataModel.NextStartDate());
            expect(viewModel.nextDateRange().endDate).toEqual(params.dataModel.NextEndDate());
            expect(viewModel.nextDateRange().periodStartDate).toEqual(params.dataModel.NextPayPeriodStartDate());
            expect(viewModel.nextDateRange().periodEndDate).toEqual(params.dataModel.NextPayPeriodEndDate());
        });
    });

    describe("No Active Positions Message", function () {
        it("should get value from resource string", function () {
            expect(viewModel.noActivePositionsMessage()).toEqual(resourcesGetObservableSpyValue());
            expect(resources.getObservable).toHaveBeenCalledWith('TimeSheet.TimeEntryNoActivePositions');
            resourcesGetObservableSpyValue("double check");
            expect(viewModel.noActivePositionsMessage()).toEqual(resourcesGetObservableSpyValue());
        });

    });

    describe("All Messages", function () {
        var messages = ko.observable(null).subscribeTo(timeSheetEvents.TIMESHEET_MESSAGES, true);

        beforeEach(function () {
            messages(null);
        });

        it("should be intialized to empty array", function () {
            newViewModel();
            expect(messages()).toEqual([]);
        });

        //it("")
    });
});