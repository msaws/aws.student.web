﻿const dataMapping = require('TimeManagement/TimeSheet/time.sheet.data.mapping'),
        actions = require('TimeManagement/TimeSheet/time.sheet.actions');
window.Ellucian = window.Ellucian || {};
window.Ellucian.TimeSheet = window.Ellucian.TimeSheet || {};
window.Ellucian.TimeSheet.ActionUrls = {
    getHomeViewDataUrl: '@Url.Action("GetTimePeriodDataAsync", "TimeSheet")',
    getTimeSheetDataUrl: '@Url.Action("GetTimeSheetDataAsync", "TimeSheet")',
    getOvertimeCalculationUrl: '@Url.Action("GetOvertimeCalculationAsync", "TimeSheet")',
    updateTimecardUrl: '@Url.Action("UpdateTimecardAsync", "TimeSheet")',
    createTimecardUrl: '@Url.Action("CreateTimecardAsync", "TimeSheet")',
    createPayPeriodTimecardsUrl: '@Url.Action("CreateTimecardsInPayPeriod", "TimeSheet")',
    submitTimecardStatusUrl: '@Url.Action("SubmitTimecardStatusAsync", "TimeSheet")',
    createTimeEntryCommentsUrl: '@Url.Action("CreateTimeEntryCommentsAsync", "TimeSheet")',
    getPayCyclesUrl: '@Url.Action("GetTimePeriodPayCyclesAsync", "TimeSheet")'
};

describe('tests for time.sheet.action', function () {
    let wasCalled = false
        wasWarned = false,
        lastRequest = '';
    beforeEach(function () {       
        spyOn($, 'ajax').and.callFake(function (e) {
            lastRequest = e.url;
            return $.Deferred().resolve(wasCalled = true);
        });
        spyOn(console, 'warn').and.callFake(function () {
            return wasWarned = true;
        });
    });

    afterEach(function () {
        wasWarned = wasCalled = false;
        lastRequest = '';
    });


    describe('GetTimePeriodsAsync', function () {
        it('should return its analogue http request promise', function () {
            actions.GetTimePeriodsAsync('payCycle#1').then(function () {
                expect(lastRequest.indexOf('GetTimePeriodsAsync') > -1).toBe(true);
                expect(wasCalled).toBe(true);
            });
        });
    });

    describe('GetPayCyclesAsync', function () {
        it('should return its analogue http request promise', function () {
            actions.GetPayCyclesAsync().then(function () {
                expect(lastRequest.indexOf('GetPayCyclesAsync') > -1).toBe(true);
                expect(wasCalled).toBe(true);
            });;
        });
    });

    describe('SaveTimecardAsync', function () {

        it('should return an update timecard http request promise if there is a timecard id supplied', function () {
            actions.SaveTimecardAsync({ TimecardId: ko.observable("foo") }).then(function () {
                expect(wasCalled).toBe(true);
                expect(lastRequest.indexOf('SaveTimecardAsync') > -1).toBe(true);
                wasCalled = false;
            });;
        });
        it('should warn the console if there is no timecard data model supplied', function () {
            let errorWasCaught = false;
            try{
                actions.SaveTimecardAsync(null);
            } catch (err) {
                errorWasCaught = true;
            }
            expect(errorWasCaught).toBeTruthy();
        });
    });

    describe('GetTimesheetDataAsync', function () {
        it('should warn the console if any argument is missing', function () {
            actions.GetTimesheetDataAsync(false, true, true).then(function () {
                expect(wasWarned).toBe(true);
                wasWarned = false;
            });;
            actions.GetTimesheetDataAsync(true, false, true).then(function () {
                expect(wasWarned).toBe(true);
                wasWarned = false;
            });;
            actions.GetTimesheetDataAsync(true, true, false).then(function () {
                expect(wasWarned).toBe(true);
            });;
        });
        it('should return its analogue http request promise', function () {
            actions.GetTimesheetDataAsync(true, true, true).then(function () {
                expect(lastRequest.indexOf('GetTimesheetDataAsync') > -1).toBe(true);
                expect(wasCalled).toBe(true);
            });;
        });
    });

    describe('SubmitTimecardStatusAsync', function () {
        it('should warn the console if no id is supplied', function () {
            actions.SubmitTimecardStatusAsync('TMA-01', null).then(function () {
                expect(wasWarned).toBe(true);
            });;
        });
        it('should return its analogue http request promise', function () {
            actions.SubmitTimecardStatusAsync('TMA-01', 'TMA-03').then(function () {
                expect(lastRequest.indexOf('SubmitTimecardStatusAsync') > -1).toBe(true);
                expect(wasCalled).toBe(true);
            });;
        });
    });

    describe('CalculateOvertimeAsync', function () {
        it('should warn the console if any argument is missing', function () {
            actions.CalculateOvertimeAsync(false, true, true).then(function () {
                expect(wasWarned).toBe(true);
                wasWarned = false;
            });;
            actions.CalculateOvertimeAsync(true, false, true).then(function () {
                expect(wasWarned).toBe(true);
                wasWarned = false;
            });;
            actions.CalculateOvertimeAsync(false, true, false).then(function () {
                expect(wasWarned).toBe(true);
            });;
        });
        it('should return its analogue http request promise', function () {
            actions.CalculateOvertimeAsync(true, true, true).then(function () {
                expect(lastRequest.indexOf('CalculateOvertimeAsync') > -1).toBe(true);
                expect(wasCalled).toBe(true);
            });
        });
    });

    describe('CreateTimeEntryCommentsAsync', function () {
        it('should warn the console if no comment is supplied', function () {
            actions.CreateTimeEntryCommentsAsync(null).then(function () {
                expect(wasWarned).toBe(true);
            });;
        });
        it('should return its analogue http request promise', function () {
            actions.CreateTimeEntryCommentsAsync(
                Math.sin(Math.random(Math.cos(Math.random(Math.sin(Math.random(Math.cos(Math.random())))))))
            ).then(function () {
                expect(lastRequest.indexOf('CreateTimeEntryCommentsAsync') > -1).toBe(true);
                expect(wasCalled).toBe(true);
            });;
        });
    });
});