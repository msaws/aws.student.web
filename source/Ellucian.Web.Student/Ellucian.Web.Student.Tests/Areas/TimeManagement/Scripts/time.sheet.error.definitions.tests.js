﻿const resources = require("Site/resource.manager"),
    errorDefinitions = require("TimeManagement/TimeSheet/time.sheet.error.definitions");;

describe("Test Suite for Error Definitions Module", function () {

	//let resourceObservableShim = ko.observable("foobar");
	//let errorDefinitions = {};
	beforeEach(function () {
		//spyOn(resources, "getObservable").and.returnValue(resourceObservableShim);
		
	});

	describe("Error Definition Keys", function () {

		it("should contain overlapError id", function () {
			expect(errorDefinitions.keys.overlapError).toBeDefined();
		});

		it("should contain totalDayTimeError id", function () {
			expect(errorDefinitions.keys.totalDayTimeError).toBeDefined();
		});

		it("should contain timeOutOfOrderError id", function () {
			expect(errorDefinitions.keys.timeOutOfOrderError).toBeDefined();
		});

		it("should contain missingTimeWarning id", function () {
			expect(errorDefinitions.keys.missingTimeWarning).toBeDefined();
		});
	});

	describe("Error Definition Types", function () {

		it("should be esg error type", function () {
			expect(errorDefinitions.types.error).toEqual("error");
		});

		it("should be esg warning type", function () {
			expect(errorDefinitions.types.warning).toEqual("warning");
		});
	});

	describe("Get Error", function () {
		it("should return error definition for overlapError", function () {
			let definition = errorDefinitions.get(errorDefinitions.keys.overlapError);
			expect(definition.type).toEqual(errorDefinitions.types.error);
		});

		it("should return error definition for totalDayTimeError", function () {
			let definition = errorDefinitions.get(errorDefinitions.keys.totalDayTimeError);
			expect(definition.type).toEqual(errorDefinitions.types.error);
		});

		it("should return error definition for timeOutOfOrderError", function () {
			let definition = errorDefinitions.get(errorDefinitions.keys.timeOutOfOrderError);
			expect(definition.type).toEqual(errorDefinitions.types.error);
		});

		it("should return error definition for missingTimeWarning", function () {
			let definition = errorDefinitions.get(errorDefinitions.keys.missingTimeWarning);
			expect(definition.type).toEqual(errorDefinitions.types.warning);
		});

		it("should return undefined for foobar", function () {
			let definition = errorDefinitions.get("foobar");
			expect(definition).not.toBeDefined();
		});

	});
});