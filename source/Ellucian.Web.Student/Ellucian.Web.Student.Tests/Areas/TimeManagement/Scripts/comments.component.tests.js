﻿
//define(['TimeManagement/Components/Comments/_Comments'], function (Comments) {
//    describe("Test Suite for Comments Component", function () {

//        var defaultParams = null;
//        beforeEach(function () {
//            defaultParams = {
//                Comments: ko.observable(null),
//            }
//        });
        
        
//        describe("::Merging Custom Labels to Default Labels", function () {
//            it("should use default labels", function () {
//                var labels = new Comments.viewModel(defaultParams).Labels;
//                expect(labels.title()).toEqual("Edit Comments");
//                expect(labels.openButtonLabel()).toEqual("Edit Comments");
//                expect(labels.closeButtonLabel()).toEqual("Close");

//            });

//            it("should use custom labels", function () {
//                var customLabels = {
//                    title: "foo",
//                    openButtonLabel: "bar",
//                    closeButtonLabel: "foobar"
//                }
//                defaultParams.CustomLabels = customLabels;
//                var labels = new Comments.viewModel(defaultParams).Labels;
//                expect(labels.title()).toEqual(customLabels.title);
//                expect(labels.openButtonLabel()).toEqual(customLabels.openButtonLabel);
//                expect(labels.closeButtonLabel()).toEqual(customLabels.closeButtonLabel);
//            });

//            it("should merge labels", function () {
//                var customLabels = {
//                    title: "foo"
//                }
//                defaultParams.CustomLabels = customLabels;
//                var labels = new Comments.viewModel(defaultParams).Labels;
//                expect(labels.title()).toEqual("foo");
//                expect(labels.openButtonLabel()).toEqual("Edit Comments");
//                expect(labels.closeButtonLabel()).toEqual("Close");
//            });
//        });

//        describe("::Using Mobile Settings", function () {
//            it("should set mobile flag", function () {
//                defaultParams.MobileOptions = { isMobile: true };
//                expect(new Comments.viewModel(defaultParams).isMobile).toBeTruthy();
//            });

//            it("should not set mobile flag", function () {
//                expect(new Comments.viewModel(defaultParams).isMobile).toBeFalsy();
//            });

//            it("should set mobile commentsDialogOptions", function () {
//                defaultParams.MobileOptions = {
//                    isMobile: true,
//                    mobileDialogOptions: { minWidth: 0 }
//                };
//                expect(new Comments.viewModel(defaultParams).commentsDialogOptions.minWidth)
//                    .toEqual(0);
//            });            
//        });

//        describe("::Setting autoOpen option", function () {
//            it("should set commentsDialogIsOpen to true", function () {
//                defaultParams.DialogOptions = { autoOpen: true };
//                expect(new Comments.viewModel(defaultParams).commentsDialogIsOpen()).toBeTruthy();
//            });

//            it("should default to false", function () {
//                expect(new Comments.viewModel(defaultParams).commentsDialogIsOpen()).toBeFalsy();
//            });
//        });

//        describe("::toggleDialog function", function () {
//            it("should open the dialog", function () {
//                var viewModel = new Comments.viewModel(defaultParams);
//                viewModel.toggleDialog();
//                expect(viewModel.commentsDialogIsOpen()).toBeTruthy();
//            });

//            it("should close the dialog", function () {
//                var viewModel = new Comments.viewModel(defaultParams);
//                viewModel.toggleDialog();
//                viewModel.toggleDialog();
//                expect(viewModel.commentsDialogIsOpen()).toBeFalsy();
//            });
//        });

//        describe("::Comments observable", function () {
//            it("should be initialized to Comments in params", function () {
//                defaultParams.Comments = ko.observable("This is a Comment");
//                expect(new Comments.viewModel(defaultParams).comments()).toEqual(defaultParams.Comments());
//            });

//            it("should subscribe to changes of Comments in params", function () {
//                defaultParams.Comments = ko.observable(null);
//                var viewmodel = new Comments.viewModel(defaultParams);
//                expect(viewmodel.comments()).toBeNull();
//                defaultParams.Comments("this is a change");
//                expect(viewmodel.comments()).toEqual(defaultParams.Comments());
//            })
//        })

//    });
//});