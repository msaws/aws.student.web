﻿
var status = require('TimeManagement/Modules/submission.status');

describe('timecard submission status module tests', function () {

    beforeEach(function () {

    });

    describe('status module exports', function () {

        it('should deliver the status constants', function () {
            for (var thing in status) {
                if(typeof status[thing] !== 'function'){
                    if(status[thing].code == -1)
                        expect(status[thing].name).toEqual('none');
                    else if(status[thing].code == 0)
                        expect(status[thing].name).toEqual('Submitted');
                    else if(status[thing].code == 1)
                        expect(status[thing].name).toEqual('Unsubmitted');
                    else if(status[thing].code == 2)
                        expect(status[thing].name).toEqual('Rejected');
                    else if(status[thing].code == 3)
                        expect(status[thing].name).toEqual('Approved');
                    else if(status[thing].code == 4)
                        expect(status[thing].name).toEqual('Paid');
                }
            }
        });

        it('should get a status object with a valid code', function () {
            var sts = status.get(0);
            expect(typeof sts).toBe('object');
            expect(typeof sts.code).toBe('number');
            expect(typeof sts.name).toBe('string');
            expect(ko.isObservable(sts.statusLabel)).toBeTruthy();
        });
        it('should get a status object with a valid name', function () {
            var sts = status.get('unsubmitted');
            expect(typeof sts).toBe('object');
            expect(typeof sts.code).toBe('number');
            expect(typeof sts.name).toBe('string');
            expect(ko.isObservable(sts.statusLabel)).toBeTruthy();
        });
        it('should return the base status object when no argument provided', function () {
            var sts = status.get();
            expect(typeof sts).toBe('object');
            expect(typeof sts.code).toBe('number');
            expect(typeof sts.name).toBe('string');
            expect(ko.isObservable(sts.statusLabel)).toBeTruthy();
        });
        it('should get an error with an invalid code', function () {
            try {
                var sts = status.get(19);
            } catch (e) {
                expect(e.message).toBe('Invalid status code provided');
            }
        });
        it('should get an error with an invalid name', function () {
            try {
                var sts = status.get('stootus');
            } catch (e) {
                expect(e.message).toBe('Invalid status name provided');
            }
        });
        it('should get an error with some other contrivation', function () {
            try {
                var sts = status.get([]);
            } catch (e) {
                expect(e.message).toBe('Unable to determine status from argument provided');
            }
        });
    });

});