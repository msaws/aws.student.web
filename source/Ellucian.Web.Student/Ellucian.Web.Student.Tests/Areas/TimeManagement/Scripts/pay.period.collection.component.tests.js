﻿var component = require("TimeManagement/TimeApproval/PayPeriodCollection/_PayPeriodCollection");

describe("Test Suite for TimeApprvoal Pay Period Collection", function () {

    var inputParams = {};
    var viewModel = component.viewModel;

    beforeEach(function () {
        inputParams = {
            dataModel: {
                SupervisorCutoffDateTime: new Date(2016, 9, 31),
                PeriodEndDate: new Date(2016, 9, 31),
            },
            supervisorName: ko.observable("Matt DeDiana")
        }
    });

    describe("Input Parameters", function () {

        it("should contain the proper attributes", function () {
            spyOn(console, "warn");
            new viewModel(inputParams);
            expect(console.warn).not.toHaveBeenCalled();


        });

        it("should warn when data model doesn't exist", function () {
            spyOn(console, "warn");
            inputParams.dataModel = null;
            try {
                new viewModel(inputParams);
            } catch (e) { }

            expect(console.warn).toHaveBeenCalled();
        })

        it("should warn when supervisor name doesn't exist or is not observable", function () {
            spyOn(console, "warn");
            inputParams.supervisorName = null;
            try {
                new viewModel(inputParams);
            }
            catch (e) { }
            expect(console.warn.calls.count()).toEqual(1);

            inputParams.supervisorName = "Matt DeDiana";
            try {
                new viewModel(inputParams);
            }
            catch (e) { }
            expect(console.warn.calls.count()).toEqual(2);

        });
    });

    describe("Data Model", function () {

        it("should be mapped to viewModel", function () {
            spyOn(ko.mapping, "fromJS");
            var model = new viewModel(inputParams);
            expect(ko.mapping.fromJS).toHaveBeenCalled();                
        });
    });

    describe("Properties of View Model", function () {
       
        it("should compute the due date display string", function () {
            
            spyOn(Globalize, "format");
            var model = new viewModel(inputParams);
            expect(ko.isPureComputed(model.timecardDueDateTimeDisplay));
            model.timecardDueDateTimeDisplay();

            expect(Globalize.format.calls.argsFor(0)).toEqual([model.SupervisorCutoffDateTime(), "d"]);
            expect(Globalize.format.calls.argsFor(1)).toEqual([model.SupervisorCutoffDateTime(), "t"]);
        });

        it("should computed the pay period end date display string", function () {
            spyOn(Globalize, "format");
            var model = new viewModel(inputParams);
            expect(ko.isPureComputed(model.periodEndDateDisplay));
            model.periodEndDateDisplay();

            expect(Globalize.format).toHaveBeenCalledWith(model.PeriodEndDate(), "d");
        });
    })
})