﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    [TestClass]
    public class ProjectLineItemViewModelTests
    {
        #region Initialize and Cleanup
        ProjectLineItem projectLineItemDto;

        [TestInitialize]
        public void Initialize()
        {
            projectLineItemDto = new ProjectLineItem();
            projectLineItemDto.ItemCode = "RV";
            projectLineItemDto.TotalBudget = 502.68m;
            projectLineItemDto.TotalActuals = 100.54m;
            projectLineItemDto.TotalMemoActuals = 275.98m;
            projectLineItemDto.TotalEncumbrances = 39.44m;
            projectLineItemDto.TotalMemoEncumbrances = 342.43m;
            projectLineItemDto.GlAccounts = new List<ProjectLineItemGlAccount>();
            projectLineItemDto.GlClass = GlClass.Expense;
        }

        [TestCleanup]
        public void Cleanup()
        {
            projectLineItemDto = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void ViewModelConstructor()
        {
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(projectLineItemDto.ItemCode, projectLineItemResult.ItemCode);
            Assert.AreEqual(projectLineItemDto.TotalBudget.ToString("C"), projectLineItemResult.TotalBudget);
            Assert.AreEqual(projectLineItemDto.TotalActuals.ToString("C"), projectLineItemResult.TotalActuals);
            Assert.AreEqual(projectLineItemDto.TotalMemoActuals.ToString("C"), projectLineItemResult.TotalMemoActuals);
            Assert.AreEqual(projectLineItemDto.TotalEncumbrances.ToString("C"), projectLineItemResult.TotalEncumbrances);
            Assert.AreEqual(projectLineItemDto.TotalMemoEncumbrances.ToString("C"), projectLineItemResult.TotalMemoEncumbrances);
            Assert.IsTrue(projectLineItemResult.GlAccounts.Count() == 0);
        }

        [TestMethod]
        public void ViewModelConstructor_GlAccountsPopulated()
        {
            var lineItemGlAccount = new ProjectLineItemGlAccount()
            {
                GlAccount = "20-20601-51000",
                Description = "Aviation : Paper",
                ActualsGlTransactions = new List<GlTransaction>(),
                EncumbranceGlTransactions = new List<GlTransaction>()
            };
            projectLineItemDto.GlAccounts.Add(lineItemGlAccount);

            var lineItemGlAccount2 = new ProjectLineItemGlAccount()
            {
                GlAccount = "10-20601-51000",
                Description = "Aviation : Pens",
                ActualsGlTransactions = new List<GlTransaction>(),
                EncumbranceGlTransactions = new List<GlTransaction>()
            };
            projectLineItemDto.GlAccounts.Add(lineItemGlAccount2);

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.IsTrue(projectLineItemVM.GlAccounts.Count() == projectLineItemDto.GlAccounts.Count);

            // The GL accounts should be sorted by ascending GL number.
            var sortedGLAccountDtos = projectLineItemDto.GlAccounts.OrderBy(x => x.GlAccount).ToList();
            for (int i = 0; i < sortedGLAccountDtos.Count; i++)
            {
                var glAccountDto = sortedGLAccountDtos[i];
                var glAccountVM = projectLineItemVM.GlAccounts[i];
                Assert.AreEqual(glAccountDto.GlAccount, glAccountVM.GlAccount);
            }
        }
        #endregion

        #region Asset Totals properties

        [TestMethod]
        public void TotalBudgetAsset()
        {
            projectLineItemDto.TotalBudget = 100.54m;
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(projectLineItemDto.TotalBudget.ToString("C"), projectLineItemResult.TotalBudget);
        }

        [TestMethod]
        public void TotalBudgetAsset_ZeroBudget()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(string.Empty, projectLineItemResult.TotalBudget);
        }

        [TestMethod]
        public void TotalActualsAsset()
        {
            projectLineItemDto.TotalActuals = 100.54m;
            projectLineItemDto.GlClass = GlClass.Asset;
            var expectedTotalActuals = (projectLineItemDto.TotalActuals).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalActuals, projectLineItemResult.TotalActuals);
        }

        [TestMethod]
        public void TotalEncumbrancesAsset()
        {
            projectLineItemDto.TotalEncumbrances = 100.54m;
            projectLineItemDto.GlClass = GlClass.Asset;
            var expectedTotalEncumbrances = (projectLineItemDto.TotalEncumbrances).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalEncumbrances, projectLineItemResult.TotalEncumbrances);
        }

        #endregion

        #region Liability properties

        [TestMethod]
        public void TotalBudgetLiability()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.Liability;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual((projectLineItemDto.TotalBudget * -1).ToString("C"), projectLineItemVM.TotalBudget);
        }

        [TestMethod]
        public void TotalBudgetLiability_ZeroBudget()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.Liability;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemVM.TotalBudget);
        }

        [TestMethod]
        public void TotalActualsLiability()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 100.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.Liability;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudget = (projectLineItemDto.TotalActuals * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudget, projectLineItemVM.TotalActuals);
        }

        [TestMethod]
        public void TotalEncumbrancesLiability()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 100.00m;
            projectLineItemDto.GlClass = GlClass.Liability;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudget = (projectLineItemDto.TotalEncumbrances * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudget, projectLineItemVM.TotalEncumbrances);
        }

        #endregion

        #region Fund Balance properties

        [TestMethod]
        public void TotalBudgetFundBalance()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.FundBalance;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual((projectLineItemDto.TotalBudget * -1).ToString("C"), projectLineItemVM.TotalBudget);
        }

        [TestMethod]
        public void TotalBudgetFundBalance_ZeroBudget()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.FundBalance;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemVM.TotalBudget);
        }

        [TestMethod]
        public void TotalActualsFundBalance()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 100.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.FundBalance;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudget = (projectLineItemDto.TotalActuals * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudget, projectLineItemVM.TotalActuals);
        }

        [TestMethod]
        public void TotalEncumbrancesFundBalance()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 100.00m;
            projectLineItemDto.GlClass = GlClass.FundBalance;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudget = (projectLineItemDto.TotalEncumbrances * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudget, projectLineItemVM.TotalEncumbrances);
        }

        #endregion

        #region Totals properties
        [TestMethod]
        public void TotalBudget()
        {
            projectLineItemDto.TotalBudget = 100.54m;
            var expectedTotalBudget = (projectLineItemDto.TotalBudget).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalBudget, projectLineItemResult.TotalBudget);
        }

        [TestMethod]
        public void TotalActuals()
        {
            projectLineItemDto.TotalActuals = 100.54m;
            var expectedTotalActuals = (projectLineItemDto.TotalActuals).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalActuals, projectLineItemResult.TotalActuals);
        }

        [TestMethod]
        public void TotalMemoActuals()
        {
            projectLineItemDto.TotalMemoActuals = 100.54m;
            var expectedTotalMemoActuals = (projectLineItemDto.TotalMemoActuals).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalMemoActuals, projectLineItemResult.TotalMemoActuals);
        }

        [TestMethod]
        public void TotalEncumbrances()
        {
            projectLineItemDto.TotalEncumbrances = 100.54m;
            var expectedTotalEncumbrances = (projectLineItemDto.TotalEncumbrances).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalEncumbrances, projectLineItemResult.TotalEncumbrances);
        }


        [TestMethod]
        public void TotalMemoEncumbrances()
        {
            projectLineItemDto.TotalMemoEncumbrances = 100.54m;
            var expectedTotalMemoEncumbrances = (projectLineItemDto.TotalMemoEncumbrances).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalMemoEncumbrances, projectLineItemResult.TotalMemoEncumbrances);
        }

        [TestMethod]
        public void TotalMemosDisplay()
        {
            projectLineItemDto.TotalMemoActuals = 285.96m;
            projectLineItemDto.TotalMemoEncumbrances = 89.69m;
            var expectedTotalMemosDisplay = (projectLineItemDto.TotalMemoActuals + projectLineItemDto.TotalMemoEncumbrances).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalMemosDisplay, projectLineItemResult.TotalMemosDisplay);
        }

        [TestMethod]
        public void TotalExpenses()
        {
            projectLineItemDto.TotalActuals = 100.54m;
            projectLineItemDto.TotalEncumbrances = 54.39m;
            var expectedTotalExpenses = (projectLineItemDto.TotalActuals + projectLineItemDto.TotalEncumbrances).ToString("C");
            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expectedTotalExpenses, projectLineItemResult.TotalExpenses);
        }
        #endregion

        #region BudgetRemaining tests
        [TestMethod]
        public void BudgetRemainingTest()
        {
            projectLineItemDto.TotalBudget = 900.25m;
            projectLineItemDto.TotalActuals = 250.00m;
            projectLineItemDto.TotalEncumbrances = 250.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(400.00m, projectLineItemResult.BudgetRemaining);
        }
        #endregion

        #region BudgetRemainingString tests

        [TestMethod]
        public void BudgetRemainingStringTest()
        {
            projectLineItemDto.TotalBudget = 900.25m;
            projectLineItemDto.TotalActuals = 250.00m;
            projectLineItemDto.TotalEncumbrances = 250.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(projectLineItemResult.BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), projectLineItemResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingStringNegativeTest()
        {
            projectLineItemDto.TotalBudget = 500.25m;
            projectLineItemDto.TotalActuals = 550.00m;
            projectLineItemDto.TotalEncumbrances = 350.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(projectLineItemResult.BudgetRemaining.ToString("C"), projectLineItemResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingString_Revenue()
        {
            projectLineItemDto.TotalBudget = -900.25m;
            projectLineItemDto.TotalActuals = -250.00m;
            projectLineItemDto.TotalEncumbrances = -250.25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(projectLineItemResult.BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), projectLineItemResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingString_RevenueNegative()
        {
            projectLineItemDto.TotalBudget = -500.25m;
            projectLineItemDto.TotalActuals = -550.00m;
            projectLineItemDto.TotalEncumbrances = -350.25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(projectLineItemResult.BudgetRemaining.ToString("C"), projectLineItemResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingString_Asset()
        {
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingString_Liability()
        {
            projectLineItemDto.GlClass = GlClass.Liability;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingString_FundBalance()
        {
            projectLineItemDto.GlClass = GlClass.FundBalance;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingString);
        }

        #endregion

        #region BudgetRemainingForScreenReader tests

        [TestMethod]
        public void BudgetRemainingForScreenReader_PositiveBudget()
        {
            projectLineItemDto.TotalBudget = 900.25m;
            projectLineItemDto.TotalActuals = 250.00m;
            projectLineItemDto.TotalEncumbrances = 250.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual("", projectLineItemResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_NegativeBudget()
        {
            projectLineItemDto.TotalBudget = 200.25m;
            projectLineItemDto.TotalActuals = 250.00m;
            projectLineItemDto.TotalEncumbrances = 250.25m;

            var expected = Math.Abs(projectLineItemDto.TotalBudget - projectLineItemDto.TotalActuals - projectLineItemDto.TotalEncumbrances).ToString("C");

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expected, projectLineItemResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_RevenuePositiveBudget()
        {
            projectLineItemDto.TotalBudget = -900.25m;
            projectLineItemDto.TotalActuals = -250.00m;
            projectLineItemDto.TotalEncumbrances = -250.25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual("", projectLineItemResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_RevenueNegativeBudget()
        {
            projectLineItemDto.TotalBudget = -200.25m;
            projectLineItemDto.TotalActuals = -250.00m;
            projectLineItemDto.TotalEncumbrances = -250.25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            var expected = Math.Abs(projectLineItemDto.TotalBudget - projectLineItemDto.TotalActuals - projectLineItemDto.TotalEncumbrances).ToString("C");

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expected, projectLineItemResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_Asset()
        {
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_Liability()
        {
            projectLineItemDto.GlClass = GlClass.Liability;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_FundBalance()
        {
            projectLineItemDto.GlClass = GlClass.FundBalance;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingForScreenReader);
        }

        #endregion

        #region BudgetRemainingFormatted tests

        [TestMethod]
        public void BudgetRemainingFormatted_PositiveAmount()
        {
            projectLineItemDto.TotalBudget = 1000.00m;
            projectLineItemDto.TotalActuals = 250.00m;
            projectLineItemDto.TotalEncumbrances = 250.00m;

            var expected = (projectLineItemDto.TotalBudget - projectLineItemDto.TotalActuals - projectLineItemDto.TotalEncumbrances).ToString("C");

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expected, projectLineItemResult.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_NegativeAmount()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 200.00m;
            projectLineItemDto.TotalEncumbrances = 300.00m;

            var expected = (projectLineItemDto.TotalBudget - projectLineItemDto.TotalActuals - projectLineItemDto.TotalEncumbrances).ToString("C");

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expected, projectLineItemResult.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_Asset()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 200.00m;
            projectLineItemDto.TotalEncumbrances = 300.00m;
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_Liability()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 200.00m;
            projectLineItemDto.TotalEncumbrances = 300.00m;
            projectLineItemDto.GlClass = GlClass.Liability;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_FundBalance()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 200.00m;
            projectLineItemDto.TotalEncumbrances = 300.00m;
            projectLineItemDto.GlClass = GlClass.FundBalance;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(string.Empty, projectLineItemResult.BudgetRemainingFormatted);
        }

        #endregion

        #region Overage Tests
        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetZero()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = 200.00m;
            projectLineItemDto.TotalEncumbrances = 300.00m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(100.00m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageGreaterThan100()
        {
            projectLineItemDto.TotalBudget = 364.89m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 600.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(100.00m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageLessThan100()
        {
            projectLineItemDto.TotalBudget = 900.00m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 600.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(11.14m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetEqualsExpenses()
        {
            projectLineItemDto.TotalBudget = 900.25m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 500.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0.00m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetGreaterThanExpenses_BudgetZero()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = -154.94m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0.00m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetGreaterThanExpenses_BudgetGreaterThanZero()
        {
            projectLineItemDto.TotalBudget = 900.25m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 100.25m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0.00m, projectLineItemResult.Overage);
        }
        #endregion

        #region BudgetSpentPercent tests
        [TestMethod]
        public void BudgetSpentPercent_ZeroTest()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0m, projectLineItemResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_NegativeExpensesTest()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = -43.25m;
            projectLineItemDto.TotalEncumbrances = 0.00m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0m, projectLineItemResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_BudgetEqualsExpensesTest()
        {
            projectLineItemDto.TotalBudget = 900.25m;
            projectLineItemDto.TotalActuals = 400.25m;
            projectLineItemDto.TotalEncumbrances = 500.00m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(100m, projectLineItemResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_CalculatedTest()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 20.00m;
            projectLineItemDto.TotalEncumbrances = 28.44m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(48m, projectLineItemResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_ExpensesGreaterThanBudget_BudgetZero()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 200.00m;
            projectLineItemDto.TotalEncumbrances = 400.00m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0m, projectLineItemResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_ExpensesGreaterThanBudget_BudgetNotZero()
        {
            projectLineItemDto.TotalBudget = 1000.00m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 1210.23m;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(39m, projectLineItemResult.BudgetSpentPercent);
        }
        #endregion

        #region PercentProcessed tests

        [TestMethod]
        public void PercentProcessed_ExpenseNoBudgetPositive()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = 50m;
            projectLineItemDto.TotalEncumbrances = 25m;
            projectLineItemDto.GlClass = GlClass.Expense;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);
            var percent = 101;
            var expected = string.Format("{0:n0}", percent) + " %";

            Assert.AreEqual(expected, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_RevenueNoBudgetPositive()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = -50m;
            projectLineItemDto.TotalEncumbrances = -25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);
            var percent = 101;
            var expected = string.Format("{0:n0}", percent) + " %";

            Assert.AreEqual(expected, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_ExpenseNoBudgetNegative()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = -50m;
            projectLineItemDto.TotalEncumbrances = -25m;
            projectLineItemDto.GlClass = GlClass.Expense;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);
            var percent = -101;
            var expected = string.Format("{0:n0}", percent) + " %";

            Assert.AreEqual(expected, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_RevenueNoBudgetNegative()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = 50m;
            projectLineItemDto.TotalEncumbrances = 25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);
            var percent = -101;
            var expected = string.Format("{0:n0}", percent) + " %";

            Assert.AreEqual(expected, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_Expense()
        {
            projectLineItemDto.TotalBudget = 100m;
            projectLineItemDto.TotalActuals = 50m;
            projectLineItemDto.TotalEncumbrances = 25m;
            projectLineItemDto.GlClass = GlClass.Expense;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);
            var percent = ((projectLineItemDto.TotalActuals + projectLineItemDto.TotalEncumbrances) / projectLineItemDto.TotalBudget) * 100;
            var expected = string.Format("{0:n0}", percent) + " %";

            Assert.AreEqual(expected, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_Revenue()
        {
            projectLineItemDto.TotalBudget = 100m;
            projectLineItemDto.TotalActuals = 50m;
            projectLineItemDto.TotalEncumbrances = 25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);
            var percent = ((projectLineItemDto.TotalActuals + projectLineItemDto.TotalEncumbrances) / projectLineItemDto.TotalBudget) * 100;
            var expected = string.Format("{0:n0}", percent) + " %";

            Assert.AreEqual(expected, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_Asset()
        {
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_Liability()
        {
            projectLineItemDto.GlClass = GlClass.Liability;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemVM.PercentProcessed);
        }

        [TestMethod]
        public void PercentProcessed_FundBalance()
        {
            projectLineItemDto.GlClass = GlClass.FundBalance;

            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemVM.PercentProcessed);
        }

        #endregion

        #region FinancialHealthTextforScreenReader texts

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Green()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(resourceFileDescription, projectLineItemResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Yellow()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 86.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(resourceFileDescription, projectLineItemResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Red()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 101.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(resourceFileDescription, projectLineItemResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Revenue()
        {
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Asset()
        {
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Liability()
        {
            projectLineItemDto.GlClass = GlClass.Liability;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_FundBalance()
        {
            projectLineItemDto.GlClass = GlClass.FundBalance;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthTextForScreenReader);
        }

        #endregion

        #region FinancialHealthIndicatorClass tests

        [TestMethod]
        public void FinancialHealthIndicatorClass_Green()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            string expected = "group-icon-green";

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expected, projectLineItemResult.FinancialHealthIndicatorClass);
        }

        [TestMethod]
        public void FinancialHealthIndicatorClass_Yellow()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 86.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            string expected = "group-icon-yellow";

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expected, projectLineItemResult.FinancialHealthIndicatorClass);
        }

        [TestMethod]
        public void FinancialHealthIndicatorClass_Red()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 101.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            string expected = "group-icon-red";

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(expected, projectLineItemResult.FinancialHealthIndicatorClass);
        }

        [TestMethod]
        public void FinancialHealthIndicatorClass_Revenue()
        {
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthIndicatorClass);
        }

        [TestMethod]
        public void FinancialHealthIndicatorClass_Asset()
        {
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthIndicatorClass);
        }

        [TestMethod]
        public void FinancialHealthIndicatorClass_Liability()
        {
            projectLineItemDto.GlClass = GlClass.Liability;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthIndicatorClass);
        }

        [TestMethod]
        public void FinancialHealthIndicatorClass_FundBalance()
        {
            projectLineItemDto.GlClass = GlClass.FundBalance;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            Assert.AreEqual(string.Empty, projectLineItemResult.FinancialHealthIndicatorClass);
        }

        #endregion

        #region ColorCodeStyle tests

        [TestMethod]
        public void ColorCodeStyle_Asset()
        {
            projectLineItemDto.GlClass = GlClass.Asset;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            var expected = "remaining-black";

            Assert.AreEqual(expected, projectLineItemResult.ColorCodeStyle);
        }

        [TestMethod]
        public void ColorCodeStyle_Liability()
        {
            projectLineItemDto.GlClass = GlClass.Liability;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            var expected = "remaining-black";

            Assert.AreEqual(expected, projectLineItemResult.ColorCodeStyle);
        }

        [TestMethod]
        public void ColorCodeStyle_FundBalance()
        {
            projectLineItemDto.GlClass = GlClass.FundBalance;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            var expected = "remaining-black";

            Assert.AreEqual(expected, projectLineItemResult.ColorCodeStyle);
        }

        [TestMethod]
        public void ColorCodeStyle_Revenue_OverBudget()
        {
            projectLineItemDto.TotalBudget = -100m;
            projectLineItemDto.TotalActuals = -100m;
            projectLineItemDto.TotalEncumbrances = -100m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            var expected = "remaining-green";

            Assert.AreEqual(expected, projectLineItemResult.ColorCodeStyle);
        }

        [TestMethod]
        public void ColorCodeStyle_Expense_OverBudget()
        {
            projectLineItemDto.TotalBudget = 100m;
            projectLineItemDto.TotalActuals = 100m;
            projectLineItemDto.TotalEncumbrances = 100m;
            projectLineItemDto.GlClass = GlClass.Expense;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            var expected = "remaining-red";

            Assert.AreEqual(expected, projectLineItemResult.ColorCodeStyle);
        }

        [TestMethod]
        public void ColorCodeStyle_Revenue_UnderBudget()
        {
            projectLineItemDto.TotalBudget = -100m;
            projectLineItemDto.TotalActuals = -75m;
            projectLineItemDto.TotalEncumbrances = -25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            var expected = "remaining-black";

            Assert.AreEqual(expected, projectLineItemResult.ColorCodeStyle);
        }

        [TestMethod]
        public void ColorCodeStyle_Expense_UnderBudget()
        {
            projectLineItemDto.TotalBudget = 100m;
            projectLineItemDto.TotalActuals = 75m;
            projectLineItemDto.TotalEncumbrances = 25m;
            projectLineItemDto.GlClass = GlClass.Expense;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);

            var expected = "remaining-black";

            Assert.AreEqual(expected, projectLineItemResult.ColorCodeStyle);
        }

        #endregion

        #region Revenue totals

        [TestMethod]
        public void TotalBudgetRevenue()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudgetRevenue = (projectLineItemDto.TotalBudget * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudgetRevenue, projectLineItemVM.TotalBudget);
        }

        [TestMethod]
        public void TotalActualsRevenue()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 100.00m;
            projectLineItemDto.TotalEncumbrances = 0.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudgetRevenue = (projectLineItemDto.TotalActuals * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudgetRevenue, projectLineItemVM.TotalActuals);
        }

        [TestMethod]
        public void TotalEncumbrancesRevenue()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 0.00m;
            projectLineItemDto.TotalEncumbrances = 100.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudgetRevenue = (projectLineItemDto.TotalEncumbrances * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudgetRevenue, projectLineItemVM.TotalEncumbrances);
        }

        [TestMethod]
        public void TotalExpensesRevenue()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = 10.00m;
            projectLineItemDto.TotalEncumbrances = 100.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedTotalBudgetRevenue = ((projectLineItemDto.TotalEncumbrances + projectLineItemDto.TotalActuals) * -1).ToString("C");
            Assert.AreEqual(expectedTotalBudgetRevenue, projectLineItemVM.TotalExpenses);
        }

        [TestMethod]
        public void TotalMemoActualsRevenue()
        {
            projectLineItemDto.TotalMemoActuals = 100.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedAmount = (projectLineItemDto.TotalMemoActuals * -1).ToString("C");
            Assert.AreEqual(expectedAmount, projectLineItemVM.TotalMemoActuals);
        }

        [TestMethod]
        public void TotalMemoEncumbrancesRevenue()
        {
            projectLineItemDto.TotalMemoEncumbrances = 100.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedAmount = (projectLineItemDto.TotalMemoEncumbrances * -1).ToString("C");
            Assert.AreEqual(expectedAmount, projectLineItemVM.TotalMemoEncumbrances);
        }

        [TestMethod]
        public void TotalMemosRevenue()
        {
            projectLineItemDto.TotalMemoActuals = 100.00m;
            projectLineItemDto.TotalMemoEncumbrances = 140.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedAmount = ((projectLineItemDto.TotalMemoEncumbrances + projectLineItemDto.TotalMemoActuals) * -1);
            Assert.AreEqual(expectedAmount, projectLineItemVM.TotalMemos);
        }

        [TestMethod]
        public void TotalMemosDisplayRevenue()
        {
            projectLineItemDto.TotalMemoActuals = 100.00m;
            projectLineItemDto.TotalMemoEncumbrances = 140.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedAmount = ((projectLineItemDto.TotalMemoEncumbrances + projectLineItemDto.TotalMemoActuals) * -1).ToString("C");
            Assert.AreEqual(expectedAmount, projectLineItemVM.TotalMemosDisplay);
        }

        [TestMethod]
        public void BudgetRemainingRevenue()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 140.00m;
            projectLineItemDto.TotalEncumbrances = 30.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedAmount = (projectLineItemDto.TotalBudget * -1) - ((projectLineItemDto.TotalActuals + projectLineItemDto.TotalEncumbrances) * -1);
            Assert.AreEqual(expectedAmount, projectLineItemVM.BudgetRemaining);
        }

        [TestMethod]
        public void BudgetRemainingRevenueFormatted()
        {
            projectLineItemDto.TotalBudget = 100.00m;
            projectLineItemDto.TotalActuals = 140.00m;
            projectLineItemDto.TotalEncumbrances = 30.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;
            ProjectLineItemViewModel projectLineItemVM = new ProjectLineItemViewModel(projectLineItemDto);

            var expectedAmount = ((projectLineItemDto.TotalBudget * -1) - ((projectLineItemDto.TotalActuals + projectLineItemDto.TotalEncumbrances) * -1)).ToString("C");
            Assert.AreEqual(expectedAmount, projectLineItemVM.BudgetRemainingFormatted);
        }
        #endregion

        #region OverageRevenue Tests
        [TestMethod]
        public void OverageRevenue_BudgetLessThanExpenses_BudgetZero()
        {
            projectLineItemDto.TotalBudget = 0m;
            projectLineItemDto.TotalActuals = 200.00m;
            projectLineItemDto.TotalEncumbrances = 300.00m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageRevenue_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageGreaterThan100()
        {
            projectLineItemDto.TotalBudget = 364.89m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 600.25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageRevenue_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageLessThan100()
        {
            projectLineItemDto.TotalBudget = 900.00m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 600.25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageRevenue_BudgetEqualsExpenses()
        {
            projectLineItemDto.TotalBudget = 900.25m;
            projectLineItemDto.TotalActuals = 400.00m;
            projectLineItemDto.TotalEncumbrances = 500.25m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(0.00m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageRevenue_BudgetLessThanExpenses_ExpensesGreaterThanZero()
        {
            projectLineItemDto.TotalBudget = -100.00m;
            projectLineItemDto.TotalActuals = -100.0m;
            projectLineItemDto.TotalEncumbrances = -100.0m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            var totalExpensesRevenue = (projectLineItemDto.TotalActuals * -1) + (projectLineItemDto.TotalEncumbrances * -1);
            var totalBudgetRevenue = projectLineItemDto.TotalBudget * -1;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            var expectedOverage = Math.Round((((totalExpensesRevenue - totalBudgetRevenue) / totalBudgetRevenue) * 100), 2);
            Assert.AreEqual(100m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageRevenue_BudgetLessThanExpenses_ExpensesGreaterThanZero2()
        {
            projectLineItemDto.TotalBudget = -100.00m;
            projectLineItemDto.TotalActuals = -200.0m;
            projectLineItemDto.TotalEncumbrances = -200.0m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(100m, projectLineItemResult.Overage);
        }

        [TestMethod]
        public void OverageRevenue_BudgetLessThanExpenses_ExpensesGreaterThanZero3()
        {
            projectLineItemDto.TotalBudget = 0.00m;
            projectLineItemDto.TotalActuals = -200.0m;
            projectLineItemDto.TotalEncumbrances = -200.0m;
            projectLineItemDto.GlClass = GlClass.Revenue;

            ProjectLineItemViewModel projectLineItemResult = new ProjectLineItemViewModel(projectLineItemDto);
            Assert.AreEqual(100m, projectLineItemResult.Overage);
        }
        #endregion
    }
}