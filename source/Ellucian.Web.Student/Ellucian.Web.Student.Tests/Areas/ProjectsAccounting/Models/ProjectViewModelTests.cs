﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ColleagueFinance;
using Ellucian.Web.Student.Areas.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    [TestClass]
    public class ProjectViewModelTests
    {
        #region Initialize and Cleanup
        Project projectDto;

        [TestInitialize]
        public void Initialize()
        {
            projectDto = new Project();
            projectDto.Number = "GTT-001";
            projectDto.Title = "Test Project";
            projectDto.LineItems = new List<ProjectLineItem>();
            projectDto.BudgetPeriods = new List<ProjectBudgetPeriod>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            projectDto = null;
        }
        #endregion

        #region Constructor
        [TestMethod]
        public void ViewModelConstructor()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.50m;

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);

            Assert.AreEqual(projectDto.Number, projectVM.Number);
            Assert.AreEqual(projectDto.Title, projectVM.Title);
            Assert.AreEqual(projectDto.Status, projectVM.Status);
            Assert.AreEqual(projectVM.RevenueLineItems.Count(), 0);
            Assert.AreEqual(projectVM.ExpenseLineItems.Count(), 0);
            Assert.AreEqual(projectVM.AssetLineItems.Count(), 0);
            Assert.AreEqual(projectVM.LiabilityLineItems.Count(), 0);
            Assert.AreEqual(projectVM.FundBalanceLineItems.Count(), 0);
            Assert.AreEqual(projectVM.BudgetPeriods.Count(), 0);
        }

        [TestMethod]
        public void ViewModelConstructor_PopulatedLists()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.50m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 150.54m,
                TotalEncumbrances = 54.44m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            }
            );
            projectDto.BudgetPeriods.Add(new ProjectBudgetPeriod()
            {
                StartDate = new DateTime(2014, 7, 1),
                EndDate = new DateTime(2015, 6, 30)
            }
            );
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);

            Assert.AreEqual(projectDto.Number, projectVM.Number);
            Assert.AreEqual(projectDto.Title, projectVM.Title);
            Assert.AreEqual(projectDto.Status, projectVM.Status);
            Assert.AreEqual(projectVM.RevenueLineItems.Count(), 0);
            Assert.AreEqual(projectVM.ExpenseLineItems.Count(), 1);
            Assert.AreEqual(projectVM.BudgetPeriods.Count(), 1);
        }

        [TestMethod]
        public void ViewModelConstructor_HasRevenue()
        {
            projectDto.TotalBudgetRevenue = 500.01m;
            projectDto.TotalActualsRevenue = 150.54m;
            projectDto.TotalEncumbrancesRevenue = 54.44m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);

            Assert.AreEqual(projectVM.HasRevenue, true);
            Assert.AreEqual(projectVM.HasExpense, false);
            Assert.AreEqual(projectVM.HasAssets, false);
            Assert.AreEqual(projectVM.HasLiabilities, false);
            Assert.AreEqual(projectVM.HasFundBalances, false);

            Assert.AreEqual(1, projectVM.RevenueLineItems.Count);
            Assert.AreEqual(0, projectVM.ExpenseLineItems.Count);
            Assert.AreEqual(0, projectVM.AssetLineItems.Count);
            Assert.AreEqual(0, projectVM.LiabilityLineItems.Count);
            Assert.AreEqual(0, projectVM.FundBalanceLineItems.Count);

            // The amounts should be negated.
            Assert.AreEqual((projectDto.TotalBudgetRevenue * -1).ToString("C"), projectVM.TotalBudgetRevenue);
            Assert.AreEqual((projectDto.TotalActualsRevenue * -1).ToString("C"), projectVM.TotalActualsRevenue);
            Assert.AreEqual((projectDto.TotalEncumbrancesRevenue * -1).ToString("C"), projectVM.TotalEncumbrancesRevenue);
        }

        [TestMethod]
        public void ViewModelConstructor_HasExpense()
        {
            projectDto.TotalBudget = 500.01m;
            projectDto.TotalActuals = 150.54m;
            projectDto.TotalEncumbrances = 54.44m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                GlAccounts = new List<ProjectLineItemGlAccount>()
            }
            );

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);

            Assert.AreEqual(projectVM.HasRevenue, false);
            Assert.AreEqual(projectVM.HasExpense, true);
            Assert.AreEqual(projectVM.HasAssets, false);
            Assert.AreEqual(projectVM.HasLiabilities, false);
            Assert.AreEqual(projectVM.HasFundBalances, false);

            Assert.AreEqual(0, projectVM.RevenueLineItems.Count);
            Assert.AreEqual(1, projectVM.ExpenseLineItems.Count);
            Assert.AreEqual(0, projectVM.AssetLineItems.Count);
            Assert.AreEqual(0, projectVM.LiabilityLineItems.Count);
            Assert.AreEqual(0, projectVM.FundBalanceLineItems.Count);

            // The amounts should be left as is.
            Assert.AreEqual(projectDto.TotalBudget.ToString("C"), projectVM.TotalBudget);
            Assert.AreEqual(projectDto.TotalActuals.ToString("C"), projectVM.TotalActuals);
            Assert.AreEqual(projectDto.TotalEncumbrances.ToString("C"), projectVM.TotalEncumbrances);
        }

        [TestMethod]
        public void ViewModelConstructor_HasAssets()
        {
            projectDto.TotalActualsAssets = 500.01m;
            projectDto.TotalEncumbrancesAssets = 150.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Asset,
                ItemCode = "MA",
                GlAccounts = new List<ProjectLineItemGlAccount>()
            }
            );

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);

            Assert.AreEqual(projectVM.HasRevenue, false);
            Assert.AreEqual(projectVM.HasExpense, false);
            Assert.AreEqual(projectVM.HasAssets, true);
            Assert.AreEqual(projectVM.HasLiabilities, false);
            Assert.AreEqual(projectVM.HasFundBalances, false);

            Assert.AreEqual(0, projectVM.RevenueLineItems.Count);
            Assert.AreEqual(0, projectVM.ExpenseLineItems.Count);
            Assert.AreEqual(1, projectVM.AssetLineItems.Count);
            Assert.AreEqual(0, projectVM.LiabilityLineItems.Count);
            Assert.AreEqual(0, projectVM.FundBalanceLineItems.Count);

            // The amounts should be left as is.
            Assert.AreEqual(projectDto.TotalActualsAssets.ToString("C"), projectVM.TotalActualsAsset);
            Assert.AreEqual(projectDto.TotalEncumbrancesAssets.ToString("C"), projectVM.TotalEncumbrancesAsset);
        }

        [TestMethod]
        public void ViewModelConstructor_HasLiabilities()
        {
            projectDto.TotalActualsLiabilities = 500.01m;
            projectDto.TotalEncumbrancesLiabilities = 150.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Liability,
                ItemCode = "MA",
                GlAccounts = new List<ProjectLineItemGlAccount>()
            }
            );

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);

            Assert.AreEqual(projectVM.HasRevenue, false);
            Assert.AreEqual(projectVM.HasExpense, false);
            Assert.AreEqual(projectVM.HasAssets, false);
            Assert.AreEqual(projectVM.HasLiabilities, true);
            Assert.AreEqual(projectVM.HasFundBalances, false);

            Assert.AreEqual(0, projectVM.RevenueLineItems.Count);
            Assert.AreEqual(0, projectVM.ExpenseLineItems.Count);
            Assert.AreEqual(0, projectVM.AssetLineItems.Count);
            Assert.AreEqual(1, projectVM.LiabilityLineItems.Count);
            Assert.AreEqual(0, projectVM.FundBalanceLineItems.Count);

            Assert.AreEqual((projectDto.TotalActualsLiabilities * -1).ToString("C"), projectVM.TotalActualsLiability);
            Assert.AreEqual((projectDto.TotalEncumbrancesLiabilities * -1).ToString("C"), projectVM.TotalEncumbrancesLiability);
        }

        [TestMethod]
        public void ViewModelConstructor_HasFundBalances()
        {
            projectDto.TotalActualsFundBalance = 500.01m;
            projectDto.TotalEncumbrancesFundBalance = 150.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.FundBalance,
                ItemCode = "MA",
                GlAccounts = new List<ProjectLineItemGlAccount>()
            }
            );

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);

            Assert.AreEqual(projectVM.HasRevenue, false);
            Assert.AreEqual(projectVM.HasExpense, false);
            Assert.AreEqual(projectVM.HasAssets, false);
            Assert.AreEqual(projectVM.HasLiabilities, false);
            Assert.AreEqual(projectVM.HasFundBalances, true);

            Assert.AreEqual(0, projectVM.RevenueLineItems.Count);
            Assert.AreEqual(0, projectVM.ExpenseLineItems.Count);
            Assert.AreEqual(0, projectVM.AssetLineItems.Count);
            Assert.AreEqual(0, projectVM.LiabilityLineItems.Count);
            Assert.AreEqual(1, projectVM.FundBalanceLineItems.Count);

            Assert.AreEqual((projectDto.TotalActualsFundBalance * -1).ToString("C"), projectVM.TotalActualsFundBalance);
            Assert.AreEqual((projectDto.TotalEncumbrancesFundBalance * -1).ToString("C"), projectVM.TotalEncumbrancesFundBalance);
        }
        #endregion

        #region Totals properties

        #region Revenue totals
        [TestMethod]
        public void TotalBudgetRevenue()
        {
            projectDto.TotalBudgetRevenue = 300.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 300.00m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            var expectedTotalBudgetRevenue = (projectDto.TotalBudgetRevenue * -1).ToString("C");
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalBudgetRevenue, projectVM.TotalBudgetRevenue);
        }

        [TestMethod]
        public void TotalBudgetRevenue_NoRevenue()
        {
            projectDto.TotalBudgetRevenue = 300.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = 300.00m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.TotalBudgetRevenue);
        }

        [TestMethod]
        public void TotalActualsRevenue()
        {
            projectDto.TotalActualsRevenue = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 300.00m,
                TotalActuals = 100.54m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            var expectedTotalActualsRevenue = (projectDto.TotalActualsRevenue * -1).ToString("C");
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalActualsRevenue, projectVM.TotalActualsRevenue);
        }

        [TestMethod]
        public void TotalActualsRevenue_NoRevenue()
        {
            projectDto.TotalActualsRevenue = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = 300.00m,
                TotalActuals = 100.54m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.TotalActualsRevenue);
        }

        [TestMethod]
        public void TotalEncumbrancesRevenue()
        {
            projectDto.TotalEncumbrancesRevenue = 98.76m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 98.76m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            var expectedTotalEncumbrancesRevenue = (projectDto.TotalEncumbrancesRevenue * -1).ToString("C");
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalEncumbrancesRevenue, projectVM.TotalEncumbrancesRevenue);
        }

        [TestMethod]
        public void TotalEncumbrancesRevenue_NoRevenue()
        {
            projectDto.TotalEncumbrancesRevenue = 98.76m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 98.76m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.TotalEncumbrancesRevenue);
        }

        [TestMethod]
        public void TotalExpensesRevenue()
        {
            projectDto.TotalActualsRevenue = 100.54m;
            projectDto.TotalEncumbrancesRevenue = 98.76m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 500.01m,
                TotalActuals = 100.54m,
                TotalEncumbrances = 98.76m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            var expectedTotalExpensesRevenue = ((projectDto.TotalActualsRevenue + projectDto.TotalEncumbrancesRevenue) * -1).ToString("C");
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalExpensesRevenue, projectVM.TotalExpensesRevenue);
            Assert.AreEqual(projectVM.TotalExpensesRevenue, (-199.30).ToString("C"));
        }

        [TestMethod]
        public void TotalExpensesRevenue_NoRevenue()
        {
            projectDto.TotalActualsRevenue = 100.54m;
            projectDto.TotalEncumbrancesRevenue = 98.76m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = 500.01m,
                TotalActuals = 100.54m,
                TotalEncumbrances = 98.76m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.TotalExpensesRevenue);
        }
        #endregion

        #region Expense totals
        [TestMethod]
        public void TotalBudgetExpense()
        {
            projectDto.TotalBudget = 500.01m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            var expectedTotalBudget = Convert.ToDecimal(projectDto.TotalBudget).ToString("C");
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalBudget, projectResult.TotalBudget);
        }

        [TestMethod]
        public void TotalBudgetExpense_NoExpense()
        {
            projectDto.TotalBudget = 500.01m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.TotalBudget);
        }

        [TestMethod]
        public void TotalActualsExpense()
        {
            projectDto.TotalActuals = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 100.54m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            var expectedTotalActuals = Convert.ToDecimal(projectDto.TotalActuals).ToString("C");
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalActuals, projectResult.TotalActuals);
        }

        [TestMethod]
        public void TotalActualsExpense_NoExpense()
        {
            projectDto.TotalActuals = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 100.54m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.TotalActuals);
        }

        [TestMethod]
        public void TotalEncumbrancesExpense()
        {
            projectDto.TotalEncumbrances = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            var expectedTotalEncumbrances = Convert.ToDecimal(projectDto.TotalEncumbrances).ToString("C");
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalEncumbrances, projectResult.TotalEncumbrances);
        }

        [TestMethod]
        public void TotalEncumbrancesExpense_NoExpense()
        {
            projectDto.TotalEncumbrances = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.TotalEncumbrances);
        }

        [TestMethod]
        public void TotalExpenses()
        {
            projectDto.TotalExpenses = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 10.30m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expectedTotalExpenses = Convert.ToDecimal(projectDto.TotalExpenses).ToString("C");
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalExpenses, projectResult.TotalExpenses);
        }

        [TestMethod]
        public void TotalExpenses_NoExpense()
        {
            projectDto.TotalExpenses = 100.54m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 10.30m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.TotalExpenses);
        }
        #endregion

        #region Net Totals

        [TestMethod]
        public void TotalBudgetNet()
        {
            projectDto.TotalBudget = 500.00m;
            projectDto.TotalBudgetRevenue = -500.00m;


            var expectedTotalBudgetNet = ((-1 * Convert.ToDecimal(projectDto.TotalBudgetRevenue)) - Convert.ToDecimal(projectDto.TotalBudget)).ToString("C");
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalBudgetNet, projectResult.TotalBudgetNet);
        }

        [TestMethod]
        public void TotalActualsNet()
        {
            projectDto.TotalActuals = 500.00m;
            projectDto.TotalActualsRevenue = -500.00m;


            var expectedTotalActualsNet = ((-1 * Convert.ToDecimal(projectDto.TotalActualsRevenue)) - Convert.ToDecimal(projectDto.TotalActuals)).ToString("C");
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expectedTotalActualsNet, projectResult.TotalActualsNet);
        }

        #endregion

        #endregion

        #region BudgetRemaining tests

        #region Revenue properties
        [TestMethod]
        public void BudgetRemainingRevenueTest()
        {
            projectDto.TotalBudgetRevenue = 500.01m;
            projectDto.TotalActualsRevenue = 100.54m;
            projectDto.TotalEncumbrancesRevenue = 98.76m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 500.01m,
                TotalActuals = 100.54m,
                TotalEncumbrances = 98.76m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(-300.71m, projectVM.BudgetRemainingRevenue);
        }

        [TestMethod]
        public void BudgetRemainingRevenueFormatted_PositiveAmount()
        {
            projectDto.TotalBudgetRevenue = 1000.00m;
            projectDto.TotalActualsRevenue = 600.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 1000.00m,
                TotalActuals = 600.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = ((projectDto.TotalBudgetRevenue - projectDto.TotalActualsRevenue - projectDto.TotalEncumbrancesRevenue) * -1).ToString("C");

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectVM.BudgetRemainingRevenueFormatted);
        }

        [TestMethod]
        public void BudgetRemainingRevenueFormatted_NoRevenue()
        {
            projectDto.TotalBudgetRevenue = 1000.00m;
            projectDto.TotalActualsRevenue = 600.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = 1000.00m,
                TotalActuals = 600.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.BudgetRemainingRevenueFormatted);
        }

        [TestMethod]
        public void BudgetRemainingRevenueFormatted_NegativeAmount()
        {
            projectDto.TotalBudgetRevenue = 200.00m;
            projectDto.TotalActualsRevenue = 600.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 200.00m,
                TotalActuals = 600.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = ((projectDto.TotalBudgetRevenue - projectDto.TotalActualsRevenue - projectDto.TotalEncumbrancesRevenue) * -1).ToString("C");

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectVM.BudgetRemainingRevenueFormatted);
        }

        [TestMethod]
        public void BudgetRemainingScreenReaderTextRevenue_UnderBudget()
        {
            projectDto.TotalBudgetRevenue = -600.00m;
            projectDto.TotalActualsRevenue = -200.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -600.00m,
                TotalActuals = -200.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(projectVM.BudgetReceivedAmount, projectVM.BudgetRemainingScreenReaderTextRevenue);
        }

        [TestMethod]
        public void BudgetRemainingScreenReaderTextRevenue_OverbudgetBudget()
        {
            projectDto.TotalBudgetRevenue = -200.00m;
            projectDto.TotalActualsRevenue = -600.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -200.00m,
                TotalActuals = -600.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            var remaining = ((projectVM.BudgetRemainingRevenue) * -1).ToString("C");
            Assert.AreEqual(remaining + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "CostCenterOverbudgetText"), projectVM.BudgetRemainingScreenReaderTextRevenue);
        }

        [TestMethod]
        public void BudgetRemainingScreenReaderTextRevenue_NoRevenue()
        {
            projectDto.TotalBudgetRevenue = -200.00m;
            projectDto.TotalActualsRevenue = -600.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = -200.00m,
                TotalActuals = -600.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.BudgetRemainingScreenReaderTextRevenue);
        }
        #endregion

        #region Asset totals

        [TestMethod]
        public void TotalActualsAsset()
        {
            projectDto.TotalActualsAssets = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Asset,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = Convert.ToDecimal(projectDto.TotalActualsAssets).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(expected, projectResult.TotalActualsAsset);
        }

        [TestMethod]
        public void TotalActualsAsset_NoAssetLineItems()
        {
            projectDto.TotalActualsAssets = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(string.Empty, projectResult.TotalActualsAsset);
        }

        [TestMethod]
        public void TotalEncumbrancesAsset()
        {
            projectDto.TotalEncumbrancesAssets = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Asset,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = Convert.ToDecimal(projectDto.TotalEncumbrancesAssets).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(expected, projectResult.TotalEncumbrancesAsset);
        }

        [TestMethod]
        public void TotalEncumbrancesAsset_NoAssetLineItems()
        {
            projectDto.TotalEncumbrancesAssets = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(string.Empty, projectResult.TotalEncumbrancesAsset);
        }

        #endregion

        #region Liability totals

        [TestMethod]
        public void TotalActualsLiability()
        {
            projectDto.TotalActualsLiabilities = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Liability,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = Convert.ToDecimal(projectDto.TotalActualsLiabilities * -1).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(expected, projectResult.TotalActualsLiability);
        }

        [TestMethod]
        public void TotalActualsLiability_NoLiabilityLineItems()
        {
            projectDto.TotalActualsLiabilities = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(string.Empty, projectResult.TotalActualsLiability);
        }

        [TestMethod]
        public void TotalEncumbrancesLiability()
        {
            projectDto.TotalEncumbrancesLiabilities = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Liability,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = Convert.ToDecimal(projectDto.TotalEncumbrancesLiabilities * -1).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(expected, projectResult.TotalEncumbrancesLiability);
        }

        [TestMethod]
        public void TotalEncumbrancesLiability_NoLiabilityLineItems()
        {
            projectDto.TotalEncumbrancesLiabilities = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(string.Empty, projectResult.TotalEncumbrancesLiability);
        }

        #endregion

        #region Fund Balance totals

        [TestMethod]
        public void TotalActualsFundBalance()
        {
            projectDto.TotalActualsFundBalance = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.FundBalance,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = Convert.ToDecimal(projectDto.TotalActualsFundBalance * -1).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(expected, projectResult.TotalActualsFundBalance);
        }

        [TestMethod]
        public void TotalActualsFundBalance_NoFundBalanceLineItems()
        {
            projectDto.TotalActualsFundBalance = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(string.Empty, projectResult.TotalActualsFundBalance);
        }

        [TestMethod]
        public void TotalEncumbrancesFundBalance()
        {
            projectDto.TotalEncumbrancesFundBalance = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.FundBalance,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = Convert.ToDecimal(projectDto.TotalEncumbrancesFundBalance * -1).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(expected, projectResult.TotalEncumbrancesFundBalance);
        }

        [TestMethod]
        public void TotalEncumbrancesFundBalance_NoFundBalanceLineItems()
        {
            projectDto.TotalEncumbrancesFundBalance = 500.0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.01m,
                TotalActuals = 90.24m,
                TotalEncumbrances = 100.54m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);

            Assert.AreEqual(string.Empty, projectResult.TotalEncumbrancesFundBalance);
        }

        #endregion

        #region Expense properties
        [TestMethod]
        public void BudgetRemainingTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 900.25m,
                TotalActuals = 500.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(400.00m, projectResult.BudgetRemaining);
            Assert.AreEqual(projectResult.BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), projectResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_PositiveAmount()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 1000.00m;
            projectDto.TotalExpenses = 500.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 1000.00m,
                TotalActuals = 500.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = (projectDto.TotalBudget - projectDto.TotalExpenses).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectResult.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_NoExpense()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 1000.00m;
            projectDto.TotalExpenses = 500.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 1000.00m,
                TotalActuals = 500.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingFormatted_NegativeAmount()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 500.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 500.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = (projectDto.TotalBudget - projectDto.TotalExpenses).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectResult.BudgetRemainingFormatted);
        }

        [TestMethod]
        public void BudgetRemainingStringTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 900.25m,
                TotalActuals = 500.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(projectResult.BudgetRemaining.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarRemainingText"), projectResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingString_NoExpenseTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 900.25m,
                TotalActuals = 500.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingStringNegativeTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 500.25m;
            projectDto.TotalExpenses = 900.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 500.25m,
                TotalActuals = 900.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(projectResult.BudgetRemaining.ToString("C"), projectResult.BudgetRemainingString);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_PositiveBudget()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 900.25m,
                TotalActuals = 500.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_NoExpense()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 900.25m,
                TotalActuals = 500.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.BudgetRemainingForScreenReader);
        }

        [TestMethod]
        public void BudgetRemainingForScreenReader_NegativeBudget()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 200.25m;
            projectDto.TotalExpenses = 500.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 200.25m,
                TotalActuals = 500.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = Math.Abs(projectDto.TotalBudget - projectDto.TotalExpenses).ToString("C");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectResult.BudgetRemainingForScreenReader);
        }
        #endregion

        #endregion

        #region Overage Tests
        #region Revenue

        [TestMethod]
        public void OverageRevenueTest_BudgetLessThanExpenses_BudgetZero()
        {
            projectDto.TotalBudgetRevenue = 0m;
            projectDto.TotalActualsRevenue = -500.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = -500.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(100.00m, projectVM.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageGreaterThan100()
        {
            projectDto.TotalBudgetRevenue = -350m;
            projectDto.TotalActualsRevenue = -1500.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -350m,
                TotalActuals = -1500.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(100.00m, projectResult.OverageRevenue);
        }

        [TestMethod]
        public void OveragRevenueTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageLessThan100()
        {
            projectDto.TotalBudgetRevenue = -900.00m;
            projectDto.TotalActualsRevenue = -1000.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -900m,
                TotalActuals = -1000.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(11.14m, projectResult.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetEqualsExpenses()
        {
            projectDto.TotalBudgetRevenue = -900.25m;
            projectDto.TotalActualsRevenue = -900.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -900.25m,
                TotalActuals = -900.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0.00m, projectResult.BudgetRemainingRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetGreaterThanExpenses_BudgetZero()
        {
            projectDto.TotalBudgetRevenue = 0m;
            projectDto.TotalActualsRevenue = 154.94m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = 154.94m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0.00m, projectResult.OverageRevenue);
        }

        [TestMethod]
        public void OverageRevenueTest_BudgetGreaterThanExpenses_BudgetGreaterThanZero()
        {
            projectDto.TotalBudgetRevenue = -900.25m;
            projectDto.TotalActualsRevenue = -500.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -900.25m,
                TotalActuals = -500.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0.00m, projectResult.OverageRevenue);
        }
        #endregion

        #region Expenses
        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetZero()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0m;
            projectDto.TotalExpenses = 500m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(100.00m, projectResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageGreaterThan100()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 364.89m;
            projectDto.TotalExpenses = 1000.25m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(100.00m, projectResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetLessThanExpenses_BudgetGreaterThanZero_OverageLessThan100()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.00m;
            projectDto.TotalExpenses = 1000.25m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(11.14m, projectResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetEqualsExpenses()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 900.25m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0.00m, projectResult.BudgetRemaining);
        }

        [TestMethod]
        public void OverageTest_BudgetGreaterThanExpenses_BudgetZero()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = -154.94m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0.00m, projectResult.Overage);
        }

        [TestMethod]
        public void OverageTest_BudgetGreaterThanExpenses_BudgetGreaterThanZero()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 500.25m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0.00m, projectResult.Overage);
        }
        #endregion
        #endregion

        #region Budget Received Tests

        #region Revenue

        [TestMethod]
        public void BudgetReceivedPercentRevenue_ZeroTest()
        {
            projectDto.TotalBudgetRevenue = 0m;
            projectDto.TotalActualsRevenue = 0m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = 0m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(0m, projectVM.BudgetReceivedPercentRevenue);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenue_NegativeExpensesTest()
        {
            projectDto.TotalBudgetRevenue = 0m;
            projectDto.TotalActualsRevenue = 43.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = 43.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(0m, projectVM.BudgetReceivedPercentRevenue);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenue_BudgetEqualsExpensesTest()
        {
            projectDto.TotalBudgetRevenue = -900.25m;
            projectDto.TotalActualsRevenue = -900.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -900.25m,
                TotalActuals = -900.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(100m, projectVM.BudgetReceivedPercentRevenue);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenue_CalculatedTest()
        {
            projectDto.TotalBudgetRevenue = -100.00m;
            projectDto.TotalActualsRevenue = -48.44m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -100.00m,
                TotalActuals = -48.44m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(48m, projectVM.BudgetReceivedPercentRevenue);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenue_ExpensesGreaterThanBudget_BudgetZero()
        {
            projectDto.TotalBudgetRevenue = 0m;
            projectDto.TotalActualsRevenue = -600.00m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = -600.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(0m, projectVM.BudgetReceivedPercentRevenue);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenue_ExpensesGreaterThanBudget_BudgetNotZero()
        {
            projectDto.TotalBudgetRevenue = -1000.00m;
            projectDto.TotalActualsRevenue = -1610.23m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 1000.00m,
                TotalActuals = -1600.23m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(39m, projectVM.BudgetReceivedPercentRevenue);
        }

        [TestMethod]
        public void BudgetReceivedAmount_NoRevenue()
        {
            projectDto.TotalBudgetRevenue = -900.25m;
            projectDto.TotalActualsRevenue = -900.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = -900.25m,
                TotalActuals = -900.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.BudgetReceivedAmount);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenueFormatted_HasNoRevenue()
        {
            projectDto.TotalBudgetRevenue = -900.25m;
            projectDto.TotalActualsRevenue = -900.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.BudgetReceivedPercentRevenueFormatted);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenueFormatted_NoBudgetNoExpenses()
        {
            projectDto.TotalBudgetRevenue = 0m;
            projectDto.TotalActualsRevenue = 0m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = 0m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual("0 %", projectVM.BudgetReceivedPercentRevenueFormatted);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenueFormatted_NoBudgetSomeExpenses()
        {
            projectDto.TotalBudgetRevenue = 0m;
            projectDto.TotalActualsRevenue = -10m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = 10m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual("101 %", projectVM.BudgetReceivedPercentRevenueFormatted);
        }

        [TestMethod]
        public void BudgetReceivedPercentRevenueFormatted_SomeBudgetSomeExpenses()
        {
            projectDto.TotalBudgetRevenue = 10m;
            projectDto.TotalActualsRevenue = -5m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = 0m,
                TotalActuals = 10m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            var expectedPercent = Math.Round((projectDto.TotalActualsRevenue / projectDto.TotalBudgetRevenue) * 100, MidpointRounding.AwayFromZero).ToString() + " %";
            Assert.AreEqual(expectedPercent, projectVM.BudgetReceivedPercentRevenueFormatted);
        }
        #endregion

        #region Expenses
        [TestMethod]
        public void BudgetSpentPercent_ZeroTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = 0.00m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0m, projectResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_NegativeExpensesTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = -43.25m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0m, projectResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_BudgetEqualsExpensesTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 900.25m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(100m, projectResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_CalculatedTest()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 48.44m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(48m, projectResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_ExpensesGreaterThanBudget_BudgetZero()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = 600.00m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(0m, projectResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentPercent_ExpensesGreaterThanBudget_BudgetNotZero()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 1000.00m;
            projectDto.TotalExpenses = 1610.23m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(39m, projectResult.BudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_Underbudget()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 45.50m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 45.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = (Math.Round((projectDto.TotalExpenses / projectDto.TotalBudget) * 100, MidpointRounding.AwayFromZero)).ToString() + " %";

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_NoExpense()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 45.50m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 45.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_RoundEven()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 45.50m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 45.50m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = "46 %";

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_RoundOdd()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 46.50m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 46.50m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = "47 %";

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_Overbudget()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 145.50m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 145.50m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            var expected = (Math.Round((projectDto.TotalExpenses / projectDto.TotalBudget) * 100)).ToString() + " %";

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(expected, projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_ZeroExpenses()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = 0.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 0.00m,
                TotalActuals = 0.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual("0 %", projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_NonCreditExpenses()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = 50.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 0.00m,
                TotalActuals = 50.000m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual("101 %", projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void ListViewBudgetSpentPercent_ZeroBudget_CreditExpenses()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = -50.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 0.00m,
                TotalActuals = -50.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual("0 %", projectResult.ListViewBudgetSpentPercent);
        }

        [TestMethod]
        public void BudgetSpentAmount()
        {
            projectDto.TotalBudget = 900.25m;
            projectDto.TotalExpenses = 900.25m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "RV",
                TotalBudget = 900.25m,
                TotalActuals = 900.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            var expected = projectDto.TotalExpenses.ToString("C") + GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "BudgetBarText") + projectDto.TotalBudget.ToString("C");
            Assert.AreEqual(expected, projectVM.BudgetSpentAmount);
        }

        [TestMethod]
        public void BudgetSpentAmount_NoExpense()
        {
            projectDto.TotalBudgetRevenue = -900.25m;
            projectDto.TotalActualsRevenue = -900.25m;
            projectDto.TotalEncumbrancesRevenue = 0m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "RV",
                TotalBudget = -900.25m,
                TotalActuals = -900.25m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });

            ProjectViewModel projectVM = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectVM.BudgetSpentAmount);
        }
        #endregion

        #endregion

        #region StatusDescription tests

        [TestMethod]
        public void StatusDescription_Active()
        {
            projectDto.Status = ProjectStatus.Active;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = 0.00m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            string resourceFileDescription = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ActiveFilterOption");
            Assert.AreEqual(resourceFileDescription, projectResult.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Inactive()
        {
            projectDto.Status = ProjectStatus.Inactive;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = 0.00m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            string resourceFileDescription = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "InactiveFilterOption");
            Assert.AreEqual(resourceFileDescription, projectResult.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Closed()
        {
            projectDto.Status = ProjectStatus.Closed;
            projectDto.TotalBudget = 0.00m;
            projectDto.TotalExpenses = 0.00m;

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            string resourceFileDescription = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ClosedFilterOption");
            Assert.AreEqual(resourceFileDescription, projectResult.StatusDescription);
        }
        #endregion

        #region FinancialHealthTextforScreenReader texts

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Green()
        {
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 0.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 0m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthGreen");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(resourceFileDescription, projectResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Yellow()
        {
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 86.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 86.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthYellow");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(resourceFileDescription, projectResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_Red()
        {
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 101.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 101.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            string resourceFileDescription = GlobalResources.GetString(ColleagueFinanceResourceFiles.ColleagueFinanceHomeResources, "ProjectHealthRed");

            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(resourceFileDescription, projectResult.FinancialHealthTextForScreenReader);
        }

        [TestMethod]
        public void FinancialHealthTextforScreenReader_NoExpense()
        {
            projectDto.TotalBudget = 100.00m;
            projectDto.TotalExpenses = 101.00m;
            projectDto.LineItems.Add(new ProjectLineItem()
            {
                GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue,
                ItemCode = "MA",
                TotalBudget = 100.00m,
                TotalActuals = 101.00m,
                TotalEncumbrances = 0m,
                GlAccounts = new List<ProjectLineItemGlAccount>()
            });
            ProjectViewModel projectResult = new ProjectViewModel(projectDto);
            Assert.AreEqual(string.Empty, projectResult.FinancialHealthTextForScreenReader);
        }
        #endregion
    }
}
