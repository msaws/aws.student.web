﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class tests the ProjectStatusViewModel class
    /// </summary>
    [TestClass]
    public class ProjectStatusViewModelTests
    {
        #region Initialize and Cleanup
        [TestInitialize]
        public void Initialize()
        {

        }

        [TestCleanup]
        public void Cleanup()
        {

        }
        #endregion

        #region Test methods
        [TestMethod]
        public void ProjectStatusViewModelConstructor()
        {
            // Create a new view model object
            var status = ProjectStatus.Inactive;
            var projectStatusViewModel = new ProjectStatusViewModel(status);

            // Confirm that the status has been saved
            Assert.AreEqual(status.ToString(), projectStatusViewModel.Status.ToString());
        }

        [TestMethod]
        public void StatusDescription_Active()
        {
            // Create a new view model object
            var status = ProjectStatus.Active;
            var projectStatusViewModel = new ProjectStatusViewModel(status);

            // Confirm that the status has been saved
            string resourceFileDescription = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ActiveFilterHeader");
            Assert.AreEqual(resourceFileDescription, projectStatusViewModel.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Inactive()
        {
            // Create a new view model object
            var status = ProjectStatus.Inactive;
            var projectStatusViewModel = new ProjectStatusViewModel(status);

            // Confirm that the status has been saved
            string resourceFileDescription = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "InactiveFilterOption");
            Assert.AreEqual(resourceFileDescription, projectStatusViewModel.StatusDescription);
        }

        [TestMethod]
        public void StatusDescription_Closed()
        {
            // Create a new view model object
            var status = ProjectStatus.Closed;
            var projectStatusViewModel = new ProjectStatusViewModel(status);

            // Confirm that the status has been saved
            string resourceFileDescription = GlobalResources.GetString(ProjectsAccountingResourceFiles.ProjectsAccountingResources, "ClosedFilterOption");
            Assert.AreEqual(resourceFileDescription, projectStatusViewModel.StatusDescription);
        }
        #endregion
    }
}
