﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class tests the ProjectDetailViewModel class.
    /// </summary>
    [TestClass]
    public class ProjectDetailViewModelTests
    {
        #region Initialize and Cleanup
        public Project projectDto;
        public ProjectDetailViewModel projectDetailViewModel;

        [TestInitialize]
        public void Initialize()
        {
            projectDto = new Project();
            projectDto.Id = "28";
            projectDto.Number = "GTT-001";
            projectDto.Title = "Gary's Moon Landing";
            projectDto.LineItems = new List<ProjectLineItem>();
            projectDto.BudgetPeriods = new List<ProjectBudgetPeriod>();
            projectDetailViewModel = new ProjectDetailViewModel(projectDto);
        }

        [TestCleanup]
        public void Cleanup()
        {
            projectDto = null;
        }
        #endregion

        #region Constructor
        [TestMethod]
        public void ProjectDetailViewModelConstructor()
        {
            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(projectDto.Number, projectDetailViewModel.Project.Number);
        }
        #endregion
    }
}
