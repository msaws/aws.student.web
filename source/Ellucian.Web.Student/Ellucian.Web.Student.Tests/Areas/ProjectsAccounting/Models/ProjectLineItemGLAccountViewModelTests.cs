﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class tests the ProjectLineItemGlAccountViewModel class.
    /// </summary>
    [TestClass]
    public class ProjectLineItemGlAccountViewModelTests
    {
        #region Initialize and Cleanup
        public ProjectLineItemGlAccount glAccountDto;
        public ProjectLineItemGlAccountViewModel glAccountVM;
        public ProjectLineItemGlAccount glAccountDto2;
        public ProjectLineItemGlAccountViewModel glAccountVM2;

        public GlTransaction glTransactionDto;
        public GlTransaction glTransactionDto2;

        [TestInitialize]
        public void Initialize()
        {
            #region Main initialization
            
            // Set up the DTOs to create the VMs.
            glAccountDto = new ProjectLineItemGlAccount();
            glAccountDto.GlAccount = "10_00_02_01_20601_51000";
            glAccountDto.Description = "Aviation : Paper";
            glAccountDto.Actuals = 500.00m;
            glAccountDto.Encumbrances = 250.00m;
            glAccountDto.FormattedGlAccount = "10-00-02-01-20601-51000";

            // Create GLTransaction objects and associate them to the GlAccountVM
            #region Two actuals transactions
            glTransactionDto = new GlTransaction();
            glTransactionDto.Amount = 125.00m;
            glTransactionDto.Description = "Transaction description";
            glTransactionDto.ReferenceNumber = "V000001";
            glTransactionDto.TransactionDate = new DateTime(2014, 11, 24);
            glAccountDto.ActualsGlTransactions = new List<GlTransaction>();
            glAccountDto.ActualsGlTransactions.Add(glTransactionDto);

            glTransactionDto.Amount = 500.45m;
            glTransactionDto.Description = "Another transaction description";
            glTransactionDto.ReferenceNumber = "V000002";
            glTransactionDto.TransactionDate = new DateTime(2014, 11, 15);
            glAccountDto.ActualsGlTransactions.Add(glTransactionDto);
            #endregion

            #region Three encumbrance transactions
            glTransactionDto.Amount = 123.00m;
            glTransactionDto.Description = "Transaction description1";
            glTransactionDto.ReferenceNumber = "V000001";
            glTransactionDto.TransactionDate = new DateTime(2014, 11, 5);
            glAccountDto.EncumbranceGlTransactions = new List<GlTransaction>();
            glAccountDto.EncumbranceGlTransactions.Add(glTransactionDto);

            glTransactionDto.Amount = 256.00m;
            glTransactionDto.Description = "Transaction description2";
            glTransactionDto.ReferenceNumber = "V000002";
            glTransactionDto.TransactionDate = new DateTime(2014, 11, 15);
            glAccountDto.EncumbranceGlTransactions.Add(glTransactionDto);

            glTransactionDto.Amount = 312.00m;
            glTransactionDto.Description = "Transaction description3";
            glTransactionDto.ReferenceNumber = "V000003";
            glTransactionDto.TransactionDate = new DateTime(2014, 11, 22);
            glAccountDto.EncumbranceGlTransactions.Add(glTransactionDto);
            #endregion

            // Set up the VM
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Expense);
            #endregion

            #region Initialization for long description test
            
            // Set up the DTOs and VM for a long description test.
            //glAccountDto2 = new ProjectLineItemGlAccount();
            //glAccountDto2.GlAccount = "10_00_02_01_20601_51000";
            //glAccountDto2.Description = "Aviation : Paper";
            //glAccountDto2.Actuals = 500.00m;
            //glAccountDto2.Encumbrances = 250.00m;
            //glAccountDto2.FormattedGlAccount = "10-00-02-01-20601-51000";

            //glTransactionDto2 = new GlTransaction();
            //glTransactionDto2.Amount = 125.00m;
            //glTransactionDto2.Description = "Transaction description";
            //glTransactionDto2.ReferenceNumber = "V000001";
            //glTransactionDto2.TransactionDate = new DateTime(2014, 11, 24);
            //glAccountDto2.ActualsGlTransactions = new List<GlTransaction>();
            //glAccountDto2.ActualsGlTransactions.Add(glTransactionDto2);

            //glAccountVM2 = new ProjectLineItemGlAccountViewModel(glAccountDto2);
            #endregion
        }

        [TestCleanup]
        public void Cleanup()
        {
            glAccountDto = null;
            glAccountVM = null;
            glAccountDto2 = null;
            glAccountVM2 = null;
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void ProjectLineItemGlAccountViewModelConstructor()
        {
            Assert.AreEqual(glAccountDto.GlAccount, glAccountVM.GlAccount);
            Assert.AreEqual(glAccountDto.Description, glAccountVM.Description);
            Assert.AreEqual(glAccountDto.Actuals.ToString("C"), glAccountVM.Actuals);
            Assert.AreEqual(glAccountDto.Encumbrances.ToString("C"), glAccountVM.Encumbrances);
            Assert.AreEqual(glAccountDto.FormattedGlAccount, glAccountVM.FormattedGlAccount);
            Assert.AreEqual(glAccountDto.ActualsGlTransactions.Count(), glAccountVM.ActualTransactions.Count());
            Assert.AreEqual(glAccountDto.EncumbranceGlTransactions.Count(), glAccountVM.EncumbranceTransactions.Count());
        }

        [TestMethod]
        public void ActualsAndEncumbrances()
        {
            var expectedAmount = (glAccountDto.Actuals + glAccountDto.Encumbrances).ToString("C");
            Assert.AreEqual(expectedAmount, glAccountVM.ActualsAndEncumbrances);
        }

        [TestMethod]
        public void Constructor_GlDescriptionLong()
        {
            var longGlDesc = "1234567890123456789012345678901234567890123456789012345678901234567890";
            glAccountDto.Description = longGlDesc;
            glAccountVM2 = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Expense);
            Assert.AreEqual(longGlDesc.Substring(0, 60), glAccountVM2.Description);
        }

        [TestMethod]
        public void Constructor_GlDescriptionNull()
        {
            glAccountDto.Description = null;
            glAccountVM2 = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Expense);
            Assert.AreEqual(string.Empty, glAccountVM2.Description);
        }

        [TestMethod]
        public void ActualsAsset()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Asset);
            Assert.AreEqual(glAccountDto.Actuals.ToString("C"), glAccountVM.Actuals);
        }

        [TestMethod]
        public void EncumbrancesAsset()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Asset);
            Assert.AreEqual(glAccountDto.Encumbrances.ToString("C"), glAccountVM.Encumbrances);
        }

        [TestMethod]
        public void ActualsRevenue()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Revenue);
            Assert.AreEqual((glAccountDto.Actuals * -1).ToString("C"), glAccountVM.Actuals);
            Assert.AreEqual(glAccountDto.Actuals.ToString("C"), glAccountVM.NegativeActuals);
        }

        [TestMethod]
        public void EncumbrancesRevenue()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Revenue);
            Assert.AreEqual((glAccountDto.Encumbrances * -1).ToString("C"), glAccountVM.Encumbrances);
            Assert.AreEqual(glAccountDto.Encumbrances.ToString("C"), glAccountVM.NegativeEncumbrances);
        }

        [TestMethod]
        public void ActualsLiability()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Liability);
            Assert.AreEqual((glAccountDto.Actuals * -1).ToString("C"), glAccountVM.Actuals);
            Assert.AreEqual(glAccountDto.Actuals.ToString("C"), glAccountVM.NegativeActuals);
        }

        [TestMethod]
        public void EncumbrancesLiability()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.Liability);
            Assert.AreEqual((glAccountDto.Encumbrances * -1).ToString("C"), glAccountVM.Encumbrances);
            Assert.AreEqual(glAccountDto.Encumbrances.ToString("C"), glAccountVM.NegativeEncumbrances);
        }

        [TestMethod]
        public void ActualsFundBalance()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.FundBalance);
            Assert.AreEqual((glAccountDto.Actuals * -1).ToString("C"), glAccountVM.Actuals);
            Assert.AreEqual(glAccountDto.Actuals.ToString("C"), glAccountVM.NegativeActuals);
        }

        [TestMethod]
        public void EncumbrancesFundBalance()
        {
            glAccountVM = new ProjectLineItemGlAccountViewModel(glAccountDto, GlClass.FundBalance);
            Assert.AreEqual((glAccountDto.Encumbrances * -1).ToString("C"), glAccountVM.Encumbrances);
            Assert.AreEqual(glAccountDto.Encumbrances.ToString("C"), glAccountVM.NegativeEncumbrances);
        }
        #endregion
    }
}
