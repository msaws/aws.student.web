﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class tests the ProjectBudgetPeriodViewModel class.
    /// </summary>
    [TestClass]
    public class ProjectBudgetPeriodViewModelTests
    {
        #region Initialize and Cleanup
        public ProjectBudgetPeriod BudgetPeriodDto;
        public ProjectBudgetPeriodViewModel BudgetPeriodVM;

        [TestInitialize]
        public void Initialize()
        {
            // Set up the DTO to create the VM.
            BudgetPeriodDto = new ProjectBudgetPeriod();
            BudgetPeriodDto.SequenceNumber = "1";
            BudgetPeriodDto.StartDate = new DateTime(2014, 1, 1);
            BudgetPeriodDto.StartDate = new DateTime(2014, 12, 31);

            // Set up the VM
            BudgetPeriodVM = new ProjectBudgetPeriodViewModel(BudgetPeriodDto);
        }

        [TestCleanup]
        public void Cleanup()
        {
            BudgetPeriodDto = null;
            BudgetPeriodVM = null;
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void ProjectBudgetPeriodViewModelConstructor()
        {
            Assert.AreEqual(BudgetPeriodDto.SequenceNumber, BudgetPeriodVM.SequenceNumber);
            Assert.AreEqual(BudgetPeriodDto.StartDate, BudgetPeriodVM.StartDate);
            Assert.AreEqual(BudgetPeriodDto.EndDate, BudgetPeriodVM.EndDate);
        }

        [TestMethod]
        public void PeriodDateRange()
        {
            string expectedRange = BudgetPeriodDto.StartDate.ToShortDateString() + " - " + BudgetPeriodDto.EndDate.ToShortDateString();
            Assert.AreEqual(expectedRange, BudgetPeriodVM.PeriodDateRange);
        }
        #endregion
    }
}
