﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class tests the ProjectSummaryViewModel class.
    /// </summary>
    [TestClass]
    public class ProjectSummaryViewModelTests
    {
        #region Initialize and Cleanup

        public Project projectDto;
        public ProjectSummaryViewModel projectViewModelSummary;
        public ProjectViewModel projectViewModel;

        [TestInitialize]
        public void Initialize()
        {
            projectDto = new Project();
            projectViewModelSummary = new ProjectSummaryViewModel();
        }

        [TestCleanup]
        public void Cleanup()
        {
            projectDto = null;
            projectViewModelSummary = null;
            projectViewModel = null;
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void ProjectSummaryViewModelConstructor()
        {
            // Confirm that the internal properties have been initialized properly
            Assert.AreEqual(0, projectViewModelSummary.Projects.Count());
            Assert.AreEqual(0, projectViewModelSummary.ProjectTypes.Count());
            Assert.AreEqual(0, projectViewModelSummary.ProjectStatuses.Count());
        }

        [TestMethod]
        public void HasAnyRevenue()
        {
            var lineItem = new ProjectLineItem();
            lineItem.GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Revenue;
            projectDto.LineItems = new List<ProjectLineItem>() { lineItem };
            projectViewModelSummary.Projects.Add(new ProjectViewModel(projectDto));
            Assert.AreEqual(true, projectViewModelSummary.HasAnyRevenue);
            Assert.AreEqual(false, projectViewModelSummary.HasAnyExpense);
        }

        [TestMethod]
        public void HasAnyExpense()
        {
            var lineItem = new ProjectLineItem();
            lineItem.GlClass = Colleague.Dtos.ColleagueFinance.GlClass.Expense;
            projectDto.LineItems = new List<ProjectLineItem>() { lineItem };
            projectViewModelSummary.Projects.Add(new ProjectViewModel(projectDto));
            Assert.AreEqual(false, projectViewModelSummary.HasAnyRevenue);
            Assert.AreEqual(true, projectViewModelSummary.HasAnyExpense);
        }

        #endregion
    }
}
