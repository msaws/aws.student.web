﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Student.Areas.ProjectsAccounting.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.ProjectsAccounting.Models
{
    /// <summary>
    /// This class tests the ProjectTypesViewModel class.
    /// </summary>
    [TestClass]
    public class ProjectTypesViewModelTests
    {
        #region Initialize and Cleanup
        ProjectType ProjectTypeDto = new ProjectType();

        [TestInitialize]
        public void Initialize()
        {

        }

        [TestCleanup]
        public void Cleanup()
        {

        }
        #endregion

        #region Test methods
        [TestMethod]
        public void ProjectTypesViewModelConstructor()
        {
            ProjectTypeDto.Code = "R";
            ProjectTypeDto.Description = "Research";
            var projectTypesViewModel = new ProjectTypesViewModel(ProjectTypeDto);

            Assert.AreEqual(ProjectTypeDto.Code, projectTypesViewModel.Code);
            Assert.AreEqual(ProjectTypeDto.Description, projectTypesViewModel.Description);
        }
        #endregion
    }
}
