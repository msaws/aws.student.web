﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

var memoryStorage = {};

var memory = {
    getItem: function (id) {
        return memoryStorage[id];
    },
    setItem: function (id, val) {
        memoryStorage[id] = val;
    }
}

describe("projectSummaryViewModel", function () {
    var viewModel,
        projectRepository,
        contentType;

    beforeEach(function () {
        contentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) {
                return false;
            }
        };
        projectRepository = new testProjectRepository();
    });

    afterEach(function () {
        viewModel = null;
    });

    describe("getProjects", function () {
        it("ajax is invoked with expected url", function () {
            viewModel = new projectSummaryViewModel();
            spyOn($, "ajax");
            spyOn(viewModel, "getUIPreference");
            viewModel.getProjects();
            var expectedUrl = Ellucian.ProjectsAccounting.getProjectsActionUrl + "?summaryOnly=true";

            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected url when there are status codes", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.statusCodesSelected = ko.observableArray(["code", "code2"]);

            spyOn($, "ajax");
            spyOn(viewModel, "getUIPreference");
            viewModel.getProjects();
            var query = "?summaryOnly=true";
            
            for (var i = 0; i < viewModel.statusCodesSelected().length; i++) {
                // Format the field (index: 0) and value (index: 1) parameters.
                query += '&filter[' + i + '][0]=Status';
                query += '&filter[' + i + '][1]=' + viewModel.statusCodesSelected()[i];
            }
            var expectedUrl = Ellucian.ProjectsAccounting.getProjectsActionUrl + query;

            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected url when there are type codes", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.typeCodesSelected = ko.observableArray(["type", "type2"]);

            spyOn($, "ajax");
            spyOn(viewModel, "getUIPreference");
            viewModel.getProjects();
            var query = "?summaryOnly=true";

            for (var i = 0; i < viewModel.typeCodesSelected().length; i++) {
                // Format the field (index: 0) and value (index: 1) parameters.
                query += '&filter[' + i + '][0]=ProjectType';
                query += '&filter[' + i + '][1]=' + viewModel.typeCodesSelected()[i];
            }
            var expectedUrl = Ellucian.ProjectsAccounting.getProjectsActionUrl + query;

            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            viewModel = new projectSummaryViewModel();
            spyOn($, "ajax");
            spyOn(viewModel, "getUIPreference");
            viewModel.getProjects();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            viewModel = new projectSummaryViewModel();
            spyOn($, "ajax");
            spyOn(viewModel, "getUIPreference");
            viewModel.getProjects();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("beforeSend should set the filter updating observable to true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            spyOn(viewModel, "getUIPreference");
            viewModel.getProjects();
            expect(viewModel.IsFilterUpdating()).toBeTruthy();
            expect(viewModel.getUIPreference).toHaveBeenCalled();
        });

        it("success() should update the viewModel's Projects and some boolean flags", function () {
            viewModel = new projectSummaryViewModel();
            
            var returnedData = projectRepository.getProjects();
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedData);
            });
            
            viewModel.getProjects();

            expect(viewModel.Projects().length).toBe(returnedData.Projects.length);
            expect(viewModel.HasAnyExpense()).toBeTruthy();
            expect(viewModel.HasAnyRevenue()).toBeTruthy();
        });

        it("error() should add a 404 error to the notification center", function () {
            viewModel = new projectSummaryViewModel();

            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getProjects();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("error() should add a project load error to the notification center", function () {
            viewModel = new projectSummaryViewModel();

            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 500 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getProjects();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadProjectsText, type: "error" });
        });

        it("complete() sets observables", function () {
            viewModel = new projectSummaryViewModel();

            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });

            viewModel.getProjects();
            
            expect(viewModel.IsPageLoaded()).toBeTruthy();
            expect(viewModel.IsFilterUpdating()).toBeFalsy();
            expect(viewModel.ProjectCount()).toBe(0);
        });

        it("complete() calls functions", function () {
            viewModel = new projectSummaryViewModel();
            projectSummaryViewModelInstance = viewModel;

            viewModel.IsPageLoaded(true);
            viewModel.usingFilter(true);

            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn($.fn, "css");
            spyOn($.fn, "focus");
            spyOn($.fn, "notificationCenter");

            viewModel.getProjects();

            expect($.fn.css).toHaveBeenCalledWith({ display: "block" });
            expect($.fn.focus).toHaveBeenCalled();
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: noProjectsToDisplayText, type: "info" });
        });
    });

    describe("updateProjectFilter", function () {
        it("should call functions", function () {
            var event = {};
            event.myId = function () {
                return "1";
            }
            viewModel = new projectSummaryViewModel();
            spyOn(viewModel, "getProjects");

            viewModel.updateProjectFilter(event);

            expect(viewModel.getProjects).toHaveBeenCalled();
        });

        it("should update observables", function () {
            var event = {};
            event.myId = function () {
                return "1";
            }
            viewModel = new projectSummaryViewModel();
            spyOn(viewModel, "getProjects");

            viewModel.updateProjectFilter(event);

            expect(viewModel.selectedFilterOption()).toBe(event.myId());
            expect(viewModel.usingFilter()).toBeTruthy();
            
        });
    });

    describe("toggleGrid", function () {
        it("should set isBarGraphViewActive to true", function () {
            viewModel = new projectSummaryViewModel();
            spyOn(viewModel, "setUIPreference");
            viewModel.toggleGrid();

            expect(viewModel.isBarGraphViewActive()).toBeTruthy();
            expect(viewModel.setUIPreference).toHaveBeenCalled();
        });
    });

    describe("toggleList", function () {
        it("should set isBarGraphViewActive to false", function () {
            viewModel = new projectSummaryViewModel();
            spyOn(viewModel, "setUIPreference");
            viewModel.toggleList();

            expect(viewModel.isBarGraphViewActive()).toBeFalsy();
            expect(viewModel.setUIPreference).toHaveBeenCalled();
        });
    });

    describe("setUIPreference", function () {
        it("ajax is invoked with expected url", function () {
            viewModel = new projectSummaryViewModel();
            spyOn($, "ajax");
            var expectedUrl = Ellucian.ProjectsAccounting.setUIPreferenceUrl;

            viewModel.setUIPreference(true);
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("beforeSend should call removeNotification in the notification center", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            spyOn($.fn, "notificationCenter");

            viewModel.setUIPreference();
            expect($.fn.notificationCenter).toHaveBeenCalledWith('removeNotification', { message: Ellucian.ProjectsAccounting.setPreferenceFailedMessage, type: "error" });
        });

        it("beforeSend should call addNotification in the notification center", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({});
            });
            spyOn($.fn, "notificationCenter");

            viewModel.setUIPreference();
            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: Ellucian.ProjectsAccounting.setPreferenceFailedMessage, type: "error" });
        });
    });

    describe("getUIPreference", function () {
        it("ajax is invoked with expected url", function () {
            viewModel = new projectSummaryViewModel();
            spyOn($, "ajax");
            var expectedUrl = Ellucian.ProjectsAccounting.getUIPreferenceUrl;

            viewModel.getUIPreference(true);
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("success should populate the 'isBarGraphViewActive' observable with the return value", function () {
            viewModel = new projectSummaryViewModel();
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(true);
            });
            spyOn($.fn, "notificationCenter");

            viewModel.getUIPreference();
            expect(viewModel.isBarGraphViewActive()).toBeTruthy();
        });

        it("error should set the 'isBarGraphViewActive' observable to true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({});
            });

            viewModel.getUIPreference();
            expect(viewModel.isBarGraphViewActive()).toBeTruthy();
            expect(viewModel.isUIViewKnown()).toBeFalsy();
        });

        it("complete should set the 'isUIViewKnown' observable to true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete({});
            });

            viewModel.getUIPreference();
            expect(viewModel.isUIViewKnown()).toBeTruthy();
        });
    });

    describe("toggleExportDropdown", function () {
        it("should initialize to 'false'", function () {
            viewModel = new projectSummaryViewModel();

            expect(viewModel.isExportDropdownExpanded()).toBeFalsy();
        });

        it("should toggle the value of isExportDropdownExpanded", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.isExportDropdownExpanded(false);

            viewModel.toggleExportDropdown();

            expect(viewModel.isExportDropdownExpanded()).toBeTruthy();
        });
    });

    describe("csvColumns", function () {
        it("should always contain title, description, and status", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(false);
            viewModel.HasAnyRevenue(false);

            var expected = [
                "Number",
                "Title",
                "StatusDescription"
            ];

            var actual = viewModel.csvColumns();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });

        it("should add revenue columns if HasAnyRevenue is true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(false);
            viewModel.HasAnyRevenue(true);

            var expected = [
                "Number",
                "Title",
                "StatusDescription",
                'TotalBudgetRevenue',
                'TotalActualsRevenue'
            ];

            var actual = viewModel.csvColumns();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });

        it("should add expense columns if HasAnyExpense is true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(true);
            viewModel.HasAnyRevenue(false);

            var expected = [
                "Number",
                "Title",
                "StatusDescription",
                'TotalBudget',
                'TotalExpenses',
                'BudgetRemainingFormatted',
                'PercentSpentFormatted'
            ];

            var actual = viewModel.csvColumns();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });

        it("should add all columns if HasAnyExpense and HasAnyRevenue are true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(true);
            viewModel.HasAnyRevenue(true);

            var expected = [
                "Number",
                "Title",
                "StatusDescription",
                'TotalBudgetRevenue',
                'TotalActualsRevenue',
                'TotalBudget',
                'TotalExpenses',
                'BudgetRemainingFormatted',
                'PercentSpentFormatted'
            ];

            var actual = viewModel.csvColumns();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });
    });

    describe("csvColumnHeaders", function () {
        it("should always contain title, description, and status", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(false);
            viewModel.HasAnyRevenue(false);

            var expected = [
                Ellucian.ProjectsAccounting.projectNumberHeader,
                Ellucian.ProjectsAccounting.projectTitleHeader,
                Ellucian.ProjectsAccounting.projectStatusHeader
            ];

            var actual = viewModel.csvColumnHeaders();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });

        it("should add revenue columns if HasAnyRevenue is true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(false);
            viewModel.HasAnyRevenue(true);

            var expected = [
                Ellucian.ProjectsAccounting.projectNumberHeader,
                Ellucian.ProjectsAccounting.projectTitleHeader,
                Ellucian.ProjectsAccounting.projectStatusHeader,
                Ellucian.ProjectsAccounting.listViewHeaderBudgetedRevenue,
                Ellucian.ProjectsAccounting.listViewHeaderActualRevenue
            ];

            var actual = viewModel.csvColumnHeaders();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });

        it("should add expense columns if HasAnyExpense is true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(true);
            viewModel.HasAnyRevenue(false);

            var expected = [
                Ellucian.ProjectsAccounting.projectNumberHeader,
                Ellucian.ProjectsAccounting.projectTitleHeader,
                Ellucian.ProjectsAccounting.projectStatusHeader,
                Ellucian.ProjectsAccounting.listViewHeaderBudget,
                Ellucian.ProjectsAccounting.listViewHeaderActualsEncumbrances,
                Ellucian.ProjectsAccounting.remainingExpensesLabel,
                Ellucian.ProjectsAccounting.percentSpentHeaderLabel
            ];

            var actual = viewModel.csvColumnHeaders();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });

        it("should add all columns if HasAnyExpense and HasAnyRevenue are true", function () {
            viewModel = new projectSummaryViewModel();
            viewModel.HasAnyExpense(true);
            viewModel.HasAnyRevenue(true);

            var expected = [
                Ellucian.ProjectsAccounting.projectNumberHeader,
                Ellucian.ProjectsAccounting.projectTitleHeader,
                Ellucian.ProjectsAccounting.projectStatusHeader,
                Ellucian.ProjectsAccounting.listViewHeaderBudgetedRevenue,
                Ellucian.ProjectsAccounting.listViewHeaderActualRevenue,
                Ellucian.ProjectsAccounting.listViewHeaderBudget,
                Ellucian.ProjectsAccounting.listViewHeaderActualsEncumbrances,
                Ellucian.ProjectsAccounting.remainingExpensesLabel,
                Ellucian.ProjectsAccounting.percentSpentHeaderLabel
            ];

            var actual = viewModel.csvColumnHeaders();

            expect(actual.length).toBe(expected.length);
            for (var i = 0; i < expected.length; i++) {
                expect(actual[i]).toBe(expected[i]);
            }
        });
    });
});

