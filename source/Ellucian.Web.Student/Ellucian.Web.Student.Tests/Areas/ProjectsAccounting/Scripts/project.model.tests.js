﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

describe("projectModel", function () {
    var projectRepository,
        testProjectData,
        testProjectModel;

    beforeEach(function () {
        projectRepository = new testProjectRepository();
    });

    afterEach(function () {
        projectRepository = null;
        testProjectData = null;
        testProjectModel = null;
    })

    describe("projectStatusIndicator", function () {
        it("should return 'project-active' if status is active", function () {
            testProjectData = projectRepository.getProject();
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectStatusIndicator()).toBe("project-active");
        });

        it("should return 'project-inactive' if status is inactive", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.StatusDescription = inactiveStatusText;
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectStatusIndicator()).toBe("project-inactive");
        });

        it("should return 'project-closed' if status is closed", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.StatusDescription = closedStatusText;
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectStatusIndicator()).toBe("project-closed");
        });
    });

    describe("projectBarGraphData", function () {
        it("should return an array with a value for the budget spent percent, a CSS class for green color, and a title", function () {
            testProjectData = projectRepository.getProject();
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectBarGraphData()[0].value).toBe(testProjectData.BudgetSpentPercent);
            expect(testProjectModel.projectBarGraphData()[0].barClass).toBe("bar-graph bar-graph--success");
            expect(testProjectModel.projectBarGraphData()[0].title).toBe(projectBarGraphTitle);
        });

        it("should return an array with a value for the budget spent percent, a CSS class for yellow color, and a title", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetSpentPercent = 90;
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectBarGraphData()[0].value).toBe(testProjectData.BudgetSpentPercent);
            expect(testProjectModel.projectBarGraphData()[0].barClass).toBe("bar-graph bar-graph--warning");
            expect(testProjectModel.projectBarGraphData()[0].title).toBe(projectBarGraphTitle);
        });

        it("should return an array with a value for the budget spent percent, a CSS class for red color, and a title", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = -500;
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectBarGraphData()[0].value).toBe(testProjectData.BudgetSpentPercent);
            expect(testProjectModel.projectBarGraphData()[0].barClass).toBe("bar-graph bar-graph--error");
            expect(testProjectModel.projectBarGraphData()[0].title).toBe(projectBarGraphTitle);
        });
    });

    describe("projectRevenueBarGraphData", function () {
        it("should return an array with a value for the budget spent percent, a CSS class for teal color, and a title", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.ExpenseLineItems = [];
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectRevenueBarGraphData()[0].value).toBe(testProjectData.BudgetReceivedPercentRevenue);
            expect(testProjectModel.projectRevenueBarGraphData()[0].barClass).toBe("bar-graph bar-graph--hint");
            expect(testProjectModel.projectRevenueBarGraphData()[0].title).toBe(projectBarGraphTitle);
        });
    });

    describe("remainingBudgetColor", function () {
        it("should return a CSS class for red text if the remaining budget is less than zero", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = -100.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.remainingBudgetColor()).toBe("remaining-red");
        });

        it("should return a CSS class for black text if the remaining budget is greater than zero", function () {
            testProjectData = projectRepository.getProject();
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.remainingBudgetColor()).toBe("remaining-black");
        });
    });

    describe("remainingBudgetRevenueColor", function () {
        it("should return a CSS class for green text if the remaining budget is less than zero", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemainingRevenue = -100.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.remainingBudgetRevenueColor()).toBe("remaining-green");
        });

        it("should return a CSS class for black text if the remaining budget is greater than zero", function () {
            testProjectData = projectRepository.getProject();
            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.remainingBudgetRevenueColor()).toBe("remaining-black");
        });
    });

    describe("overageColor", function () {
        it("should return CSS class 'zero-budget' if the remaining budget is zero", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.TotalBudget = 0.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.overageColor()).toBe("zero-budget");
        });

        it("should return CSS class 'zero-budget' if the project is 100% over budget", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.Overage = 100.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.overageColor()).toBe("zero-budget overage");
        });

        it("should return CSS class 'zero-budget' if the project is more than 100% over budget", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.Overage = 150.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.overageColor()).toBe("zero-budget overage");
        });
    });

    describe("overageRevenueColor", function () {
        it("should return CSS class 'zero-budget' if the remaining budget is zero", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.TotalBudgetRevenue = 0.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.overageRevenueColor()).toBe("zero-budget");
        });

        it("should return CSS class 'zero-budget' if the project is 100% over budget", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.OverageRevenue = 100.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.overageRevenueColor()).toBe("zero-budget overage-revenue");
        });

        it("should return CSS class 'zero-budget' if the project is more than 100% over budget", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.OverageRevenue = 150.00;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.overageRevenueColor()).toBe("zero-budget overage-revenue");
        });
    });

    describe("financialHealthIndicator", function () {
        it("should return CSS class 'group-icon-green' if the project has more than 0% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 1000.00;
            testProjectData.BudgetSpentPercent = 0;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.financialHealthIndicator()).toBe("group-icon-green");
        });

        it("should return CSS class 'group-icon-green' if the project has 0-84% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 100.00;
            testProjectData.BudgetSpentPercent = 50;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.financialHealthIndicator()).toBe("group-icon-green");
        });

        it("should return CSS class 'group-icon-green' if the project has 84% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 16.00;
            testProjectData.BudgetSpentPercent = 84;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.financialHealthIndicator()).toBe("group-icon-green");
        });

        it("should return CSS class 'group-icon-yellow' if the project has 85% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 15.00;
            testProjectData.BudgetSpentPercent = 85;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.financialHealthIndicator()).toBe("group-icon-yellow");
        });

        it("should return CSS class 'group-icon-yellow' if the project has 85-100% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 8.00;
            testProjectData.BudgetSpentPercent = 92;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.financialHealthIndicator()).toBe("group-icon-yellow");
        });

        it("should return CSS class 'group-icon-yellow' if the project has 100% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 0;
            testProjectData.BudgetSpentPercent = 100;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.financialHealthIndicator()).toBe("group-icon-yellow");
        });

        it("should return CSS class 'group-icon-red' if the project has spent more than 100% budget", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = -1;
            testProjectData.BudgetSpentPercent = 101;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.financialHealthIndicator()).toBe("group-icon-red");
        });
    });

    describe("projectDetailFinancialHealthIndicator", function () {
        it("should return CSS class 'project-line-item-icon-green' if the project has more than 0% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 1000.00;
            testProjectData.BudgetSpentPercent = 0;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectDetailFinancialHealthIndicator()).toBe("project-line-item-icon-green");
        });

        it("should return CSS class 'project-line-item-icon-green' if the project has 0-84% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 100.00;
            testProjectData.BudgetSpentPercent = 50;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectDetailFinancialHealthIndicator()).toBe("project-line-item-icon-green");
        });

        it("should return CSS class 'project-line-item-icon-green' if the project has 84% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 16.00;
            testProjectData.BudgetSpentPercent = 84;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectDetailFinancialHealthIndicator()).toBe("project-line-item-icon-green");
        });

        it("should return CSS class 'project-line-item-icon-yellow' if the project has 85% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 15.00;
            testProjectData.BudgetSpentPercent = 85;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectDetailFinancialHealthIndicator()).toBe("project-line-item-icon-yellow");
        });

        it("should return CSS class 'project-line-item-icon-yellow' if the project has 85-100% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 8.00;
            testProjectData.BudgetSpentPercent = 92;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectDetailFinancialHealthIndicator()).toBe("project-line-item-icon-yellow");
        });

        it("should return CSS class 'project-line-item-icon-yellow' if the project has 100% budget spent", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = 0;
            testProjectData.BudgetSpentPercent = 100;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectDetailFinancialHealthIndicator()).toBe("project-line-item-icon-yellow");
        });

        it("should return CSS class 'project-line-item-icon-red' if the project has spent more than 100% budget", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.BudgetRemaining = -1;
            testProjectData.BudgetSpentPercent = 101;

            testProjectModel = new projectModel(testProjectData);
            expect(testProjectModel.projectDetailFinancialHealthIndicator()).toBe("project-line-item-icon-red");
        });
    });

    describe("PercentSpentFormatted", function () {
        it("PercentSpentFormatted when ListViewBudgetSpentPercent is undefined", function () {
            testProjectData = projectRepository.getProject();

            testProjectModel = new projectModel(testProjectData);

            expect(testProjectModel.PercentSpentFormatted()).toBe("");
        });

        it("PercentSpentFormatted when ListViewBudgetSpentPercent is defined", function () {
            testProjectData = projectRepository.getProject();
            testProjectData.ListViewBudgetSpentPercent = "100000 %";
            testProjectModel = new projectModel(testProjectData);

            expect(testProjectModel.PercentSpentFormatted()).toBe("100,000 %");
        });
    });

    describe("projectStatusModel", function () {
        it("myId is created using StatusDescription", function () {
            var expectedDescription = "fake description";
            data = {
                StatusDescription: expectedDescription
            };
            var viewModel = new projectStatusModel(data);

            expect(viewModel.myId()).toBe("project-status-" + data.StatusDescription);
        });
    });

    describe("projectTypeModel", function () {
        it("myId is created using StatusDescription", function () {
            var expectedCode = "fake description";
            data = {
                Code: expectedCode
            };
            var viewModel = new projectTypeModel(data);

            expect(viewModel.myId()).toBe("project-type-" + data.Code);
        });
    });
});