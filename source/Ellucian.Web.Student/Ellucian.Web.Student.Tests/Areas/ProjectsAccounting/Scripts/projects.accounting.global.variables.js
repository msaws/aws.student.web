﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates

var unableToLoadProjectsText = "Unable to load projects.",
    resourceNotFoundText = "Resource not found.",
    inactiveStatusText = "Inactive",
    closedStatusText = "Closed",
    budgetBarText = " ",
    projectBarGraphTitle = "Expenses vs Budget",
    glTransactionLink = "http://localhost/view-document",
    glNumber = "10-01",
    noProjectsToDisplayText = "No projects to display";

var Ellucian = Ellucian || {};
Ellucian.ProjectsAccounting = Ellucian.ProjectsAccounting || {};
Ellucian.ProjectsAccounting.getProjectsActionUrl = "http://localhost/projects";
Ellucian.ProjectsAccounting.setUIPreferenceUrl = "http://localhost/setUIPreference";
Ellucian.ProjectsAccounting.getUIPreferenceUrl = "http://localhost/getUIPreference";
Ellucian.ProjectsAccounting.setPreferenceFailedMessage = "Could not set UI preference.";
Ellucian.ProjectsAccounting.actualsLabel = "Actuals";
Ellucian.ProjectsAccounting.encumbranceLabel = "Encumbrance";