﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

function testProjectRepository() {
    var projectData = {
        "Id": "1",
        "Number": "2061AVTN",
        "StatusDescription": "Active",
        "TotalBudget": "$1000.00",
        "TotalExpenses": "$200.00",
        "BudgetRemaining": 800.00,
        "BudgetSpentPercent": 20,
        "TotalBudgetRevenue": "$1000.00",
        "TotalExpensesRevenue": "$200.00",
        "BudgetRemainingRevenue": 800.00,
        "BudgetReceivedPercentRevenue": 20,
        "Overage": 0,
        "OverageRevenue": 0,
        "HasExpense": true,
        "HasRevenue": false,
        "BudgetPeriodsSelection": [

            {
                "SequenceNumber": "0",
                "PeriodDateRange": "All Activity"
            },
            {
                "SequenceNumber": "1",
                "PeriodDateRange": "1/1/2017 - 12/31/2017"
            }
        ],
        "AssetLineItems": [
    {
        "TotalMemos": 40,
        "TotalMemoActuals": "$40.00",
        "TotalMemoEncumbrances": 0,
        "ItemCode": "ASST1",
        "ItemCodeDescription": "Asset Line Item Description",
        "TotalBudget": "$500.00",
        "TotalActuals": "$100.00",
        "TotalEncumbrances": 0,
        "GlClass": 1,
        "GlAccounts": [
            {
                "GlAccount": "11_01_01_11111_11111",
                "FormattedGlAccount": "11-01-01-11111-11111",
                "Actuals": "$100.00",
                "Encumbrances": 0,
                "ActualTransactions": [

                ],
                "EncumbranceTransactions": [

                ],
            }
        ],
        "BudgetRemaining": 0,
        "BudgetRemainingFormatted": "",
        "PercentProcessed": "",
    }
        ],
        "LiabilityLineItems": [
            {
                "TotalMemos": 80,
                "TotalMemoActuals": "$80.00",
                "TotalMemoEncumbrances": 0,
                "ItemCode": "LIAB1",
                "ItemCodeDescription": "Liability Line Item Description",
                "TotalBudget": "$600.00",
                "TotalActuals": "$250.00",
                "TotalEncumbrances": 0,
                "GlClass": 3,
                "GlAccounts": [
                    {
                        "GlAccount": "11_01_01_11111_31111",
                        "FormattedGlAccount": "11-01-01-11111-31111",
                        "Actuals": "$250.00",
                        "Encumbrances": 0,
                        "ActualTransactions": [

                        ],
                        "EncumbranceTransactions": [

                        ],
                    }
                ],
                "BudgetRemaining": 0,
                "BudgetRemainingFormatted": "",
                "PercentProcessed": "",
            }
        ],
        "FundBalanceLineItems": [
            {
                "TotalMemos": 50,
                "TotalMemoActuals": "$50.00",
                "TotalMemoEncumbrances": 0,
                "ItemCode": "FBAL1",
                "ItemCodeDescription": "FundBalance Line Item Description",
                "TotalBudget": 0,
                "TotalActuals": "$60.00",
                "TotalEncumbrances": 0,
                "GlClass": 4,
                "GlAccounts": [
                    {
                        "GlAccount": "11_11_11_11111_41111",
                        "FormattedGlAccount": "11-11-11-11111-41111",
                        "Actuals": "$60.00",
                        "Encumbrances": 0,
                        "ActualTransactions": [

                        ],
                        "EncumbranceTransactions": [

                        ],
                    }
                ],
                "BudgetRemaining": 00,
                "BudgetRemainingFormatted": "",
                "PercentProcessed": "",
            }
        ],
        "ExpenseLineItems": [
            {
                "TotalMemos": 100,
                "TotalMemoActuals": "$50.00",
                "TotalMemoEncumbrances": "$50.00",
                "ItemCode": "MA1",
                "ItemCodeDescription": "Expense Line Item Description",
                "TotalBudget": "$500.00",
                "TotalActuals": "$100.00",
                "TotalEncumbrances": "$150.00",
                "GlClass": 0,
                "GlAccounts": [
                    {
                        "GlAccount": "11_01_01_11111_01111",
                        "FormattedGlAccount": "11-01-01-11111-01111",
                        "Actuals": "$100.00",
                        "Encumbrances": "$150.00",

                        "EncumbranceTransactions": [
                            {
                                "DocumentId": '1',
                                "ReferenceNumber": "P0000001",
                                "Date": "3/15/2017",
                                "Description": "PO Numero Uno",
                                "Amount": 150
                            }
                        ],
                        "ActualTransactions": [
                            {
                                "DocumentId": '1',
                                "ReferenceNumber": "V0000001",
                                "Date": "2/1/2017",
                                "Description": "Voucher Uno",
                                "Amount": 40
                            },
                            {
                                "DocumentId": '2',
                                "ReferenceNumber": "V0000002",
                                "Date": "3/1/2017",
                                "Description": "Voucher Dos",
                                "Amount": 60
                            }
                        ]
                    }
                ],
                "BudgetRemaining": 150.00,
                "BudgetRemainingFormatted": "$150.00",
                "PercentProcessed": "70 %",
            }
        ],
        "RevenueLineItems": [
            {
                "TotalMemos": 100,
                "TotalMemoActuals": "$50.00",
                "TotalMemoEncumbrances": "$50.00",
                "ItemCode": "REVN1",
                "ItemCodeDescription": "Revenue Line Item Description",
                "TotalBudget": "$500.00",
                "TotalActuals": "$100.00",
                "TotalEncumbrances": "$100.00",
                "GlClass": 2,
                "GlAccounts": [
                    {
                        "GlAccount": "11_01_01_11111_21111",
                        "FormattedGlAccount": "11-01-01-11111-21111",
                        "Actuals": "$100.00",
                        "ActualsRevenue": "$100.00",
                        "Encumbrances": "$100.00",
                        "EncumbrancesRevenue": "$100.00",
                        "ActualTransactions": [

                        ],
                        "EncumbranceTransactions": [

                        ],
                    }
                ],
                "BudgetRemaining": 200.00,
                "BudgetRemainingFormatted": "$200.00",
                "PercentProcessed": "60 %"
            }
        ],
    };

    var projectsData = {
        "Projects": [
            projectData
        ],
        "ProjectStatuses": [],
        "ProjectTypes": [],
        "HasAnyExpense": true,
        "HasAnyRevenue": true
    };

    this.getProject = function () {
        return projectData;
    }

    this.getProjects = function () {
        return projectsData;
    }
}