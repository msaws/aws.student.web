﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

var memoryStorage = {};

var memory = {
    getItem: function (id) {
        return memoryStorage[id];
    },
    setItem: function (id, val) {
        memoryStorage[id] = val;
    }
}

var empty = "";

describe("projectDetailViewModel", function () {
    var viewModel,
        projectRepository,
        contentType;

    beforeEach(function () {
        contentType = "json";
        account = {
            handleInvalidSessionResponse: function (data) {
                return false;
            }
        };
        projectRepository = new testProjectRepository();
        viewModel = new projectDetailViewModel();
        viewModel.Project(new projectModel(projectRepository.getProject()));
        viewModel.GlNumberSelected("11_01_01_11111_01111");
    });

    afterEach(function () {
        viewModel = null;
    });

    describe("isProjectDefined", function () {
        it("should return false if the project observable is not defined", function () {
            viewModel = new projectDetailViewModel();
            expect(viewModel.isProjectDefined()).toBe(false);
        });

        it("should return true if the project observable is defined", function () {
            viewModel = new projectDetailViewModel();
            viewModel.Project({});
            expect(viewModel.isProjectDefined()).toBe(true);
        });
    });

    describe("GenerateBudgetPeriodsSelection", function () {
        it("should generate an array with one entry - All Activity - when nothing is passed in", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GenerateBudgetPeriodsSelection([]);
            expect(viewModel.BudgetPeriodsSelection().length).toBe(1);
            expect(viewModel.BudgetPeriodsSelection()[0].Id()).toBe('0');
            expect(viewModel.BudgetPeriodsSelection()[0].Description()).toBe(Ellucian.ProjectsAccounting.allActivityOption);
        });

        it("should generate an array with multiple entries when data is passed in", function () {
            viewModel = new projectDetailViewModel();
            var data = [
                { "SequenceNumber": 1, "PeriodDateRange": "1/1/2014 - 12/31/2014" },
                { "SequenceNumber": 2, "PeriodDateRange": "1/1/2015 - 12/31/2015" },
            ];
            viewModel.GenerateBudgetPeriodsSelection(data);
            expect(viewModel.BudgetPeriodsSelection().length).toBe(3);

            for (var i = 0; i < viewModel.BudgetPeriodsSelection().length; i++) {
                if (i == 0) {
                    expect(viewModel.BudgetPeriodsSelection()[i].Id()).toBe('0');
                    expect(viewModel.BudgetPeriodsSelection()[i].Description()).toBe(Ellucian.ProjectsAccounting.allActivityOption);
                }
                else {
                    expect(viewModel.BudgetPeriodsSelection()[i].Id()).toBe(data[i - 1].SequenceNumber);
                    expect(viewModel.BudgetPeriodsSelection()[i].Description()).toBe(data[i - 1].PeriodDateRange);
                }
            }
        });
    });

    describe("ShowEncumbrancesTable", function () {
        it("should return false when in summary view", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GlDetailView(false);
            expect(viewModel.ShowEncumbrancesTable()).toBe(false);
        });

        it("should return true when in detail view AND in desktop view", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GlDetailView(true);
            viewModel.isMobile(false);
            expect(viewModel.ShowEncumbrancesTable()).toBe(true);
        });

        it("should return true when in detail view, in mobile view, AND the Encumbrances tab has been selected", function () {
            viewModel.GlDetailView(true);
            viewModel.isMobile(true);
            viewModel.GlDetailTabSelected('E');
            expect(viewModel.ShowEncumbrancesTable()).toBe(true);
        });

        it("should return false when in GL summary view", function () {
            viewModel.GlDetailView(false);
            viewModel.isMobile(true);
            viewModel.GlDetailTabSelected("E");
            expect(viewModel.ShowEncumbrancesTable()).toBe(false);
        });

        it("should return false when the Actuals tab has been selected", function () {
            viewModel.GlDetailView(true);
            viewModel.isMobile(true);
            viewModel.GlDetailTabSelected("A");
            expect(viewModel.ShowEncumbrancesTable()).toBe(false);
        });
    });

    describe("toggleView", function () {
        it("should update variables to display the line items view when currently in GL Detail view", function () {
            viewModel.GlDetailView(true);
            var glNumber = "11_01_01_11111_01111";
            viewModel.toggleView(glNumber);
            expect(viewModel.GlNumberSelected()).toBe('');
            expect(viewModel.GlDetailView()).toBe(false);

        });

        it("should update variables to display the GL Detail view when currently in Line Items view", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GlDetailView(false);
            var glNumber = "11_01_01_11111_01111";
            viewModel.toggleView(glNumber);
            expect(viewModel.GlNumberSelected()).toBe(glNumber);
            expect(viewModel.GlDetailView()).toBe(true);
        });
    });

    describe("clickEncumbrancesTab", function () {
        it("should mark the Encumbrances tab as selected when coming from another tab", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GlDetailTabSelected("A");
            viewModel.clickEncumbrancesTab();
            expect(viewModel.GlDetailTabSelected()).toBe("E");
        });

        it("should not change the selected tab when the Encumbrances tab is alrady selected", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GlDetailTabSelected("E");
            viewModel.clickEncumbrancesTab();
            expect(viewModel.GlDetailTabSelected()).toBe("E");
        });
    });

    describe("clickActualsTab", function () {
        it("should mark the Actuals tab as selected when coming from another tab", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GlDetailTabSelected("E");
            viewModel.clickActualsTab();
            expect(viewModel.GlDetailTabSelected()).toBe("A");
        });

        it("should not change the selected tab when the Actuals tab is alrady selected", function () {
            viewModel = new projectDetailViewModel();

            viewModel.GlDetailTabSelected("A");
            viewModel.clickActualsTab();
            expect(viewModel.GlDetailTabSelected()).toBe("A");
        });
    });

    describe("pickBudgetPeriod", function () {
        it("should update the project if the page has loaded and the filter is currently not updating", function () {
            viewModel = new projectDetailViewModel();

            spyOn(viewModel, "getProject");

            viewModel.IsPageLoaded(true);
            viewModel.IsFilterUpdating(false);
            var expectedReturn = viewModel.pickBudgetPeriod();
            expect(expectedReturn).toBeTruthy();
            expect(viewModel.getProject).toHaveBeenCalled();
            expect(viewModel.usingFilter()).toBeTruthy();
            expect(memory.getItem('period')).toBe(viewModel.SelectedBudgetPeriod());
        });

        it("should not update project if the filter is currently updating", function () {
            viewModel = new projectDetailViewModel();

            spyOn(viewModel, "getProject");

            viewModel.IsPageLoaded(true);
            viewModel.IsFilterUpdating(true);
            var expectedReturn = viewModel.pickBudgetPeriod();
            expect(expectedReturn).toBeTruthy();
            expect(viewModel.getProject).not.toHaveBeenCalled();
        });

        it("should not update project if the page has not loaded", function () {
            viewModel = new projectDetailViewModel();

            spyOn(viewModel, "getProject");

            viewModel.IsPageLoaded(false);
            viewModel.IsFilterUpdating(false);
            var expectedReturn = viewModel.pickBudgetPeriod();
            expect(expectedReturn).toBeTruthy();
            expect(viewModel.getProject).not.toHaveBeenCalled();
        });
    });

    describe("fullDocumentUrl", function () {
        it("should be built from the document link", function () {
            var data =
                {
                    Description: ko.observable("description"),
                    documentLink: ko.observable("some_url")
                }
            documentId = undefined;
            viewModel = new projectDetailViewModel();

            var expectedFullDocumentUrl = data.documentLink() + '/' + documentId + '/' + Ellucian.ColleagueFinance.projectId;
            var actualFullDocumentUrl = viewModel.fullDocumentUrl(data);
            expect(actualFullDocumentUrl).toBe(expectedFullDocumentUrl);
        });
    });

    describe("changeToDesktop", function () {
        it("should set some DOM attributes", function () {
            viewModel = new projectDetailViewModel();

            spyOn($.fn, "attr");
            spyOn($.fn, "removeAttr");

            viewModel.changeToDesktop();

            expect($.fn.attr).toHaveBeenCalled();
            expect($.fn.removeAttr).toHaveBeenCalled();
        });
    });

    describe("setLineItems", function () {
        it("should call the initializeActiveRows when memory has a value", function () {
            memory.setItem("activeLineItems", "GT");

            spyOn(Ellucian.ColleagueFinance, "initializeActiveRows");

            viewModel = new projectDetailViewModel();

            viewModel.setLineItems();

            expect(Ellucian.ColleagueFinance.initializeActiveRows).toHaveBeenCalled();
        });
    });

    describe("getProject", function () {
        it("ajax is invoked with expected url", function () {
            viewModel = new projectDetailViewModel();
            spyOn($, "ajax");
            viewModel.getProject();
            var expectedUrl = Ellucian.ProjectsAccounting.getProjectDetailAsyncActionUrl + '?id=' + Ellucian.ColleagueFinance.projectId;

            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected url when changing budget period", function () {
            viewModel = new projectDetailViewModel();
            viewModel.SelectedBudgetPeriod("1");
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax");
            viewModel.getProject(true);
            var expectedUrl = Ellucian.ProjectsAccounting.getProjectDetailAsyncActionUrl + '?id=' + Ellucian.ColleagueFinance.projectId + "&sequenceNumber=" + viewModel.SelectedBudgetPeriod();

            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected url when sequence number exists in memory", function () {
            memory.setItem("period", "1");
            viewModel = new projectDetailViewModel();
            spyOn($, "ajax");
            viewModel.getProject();
            var expectedUrl = Ellucian.ProjectsAccounting.getProjectDetailAsyncActionUrl + '?id=' + Ellucian.ColleagueFinance.projectId + "&sequenceNumber=" + memory.getItem("period");

            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual(expectedUrl);
        });

        it("ajax is invoked with expected dataType", function () {
            viewModel = new projectDetailViewModel();
            spyOn($, "ajax");
            viewModel.getProject();
            expect($.ajax.calls.mostRecent().args[0]["dataType"]).toEqual(contentType);
        });

        it("ajax is invoked with expected action type", function () {
            viewModel = new projectDetailViewModel();
            spyOn($, "ajax");
            viewModel.getProject();
            expect($.ajax.calls.mostRecent().args[0]["type"]).toEqual("GET");
        });

        it("beforeSend should set the filter updating observable to true", function () {
            viewModel = new projectDetailViewModel();
            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.beforeSend({});
            });
            viewModel.getProject();
            expect(viewModel.IsFilterUpdating()).toBeTruthy();
        });

        it("success() should GenerateBudgetPeriodsSelection if budgetPeriodChange is false", function () {
            viewModel = new projectDetailViewModel();

            memory.setItem("lastProjectVisited", "10");
            projectDetailViewModelInstance = viewModel;
            projectDetailViewModelInstance.Project = ko.observable();
            var number = ko.observable("1");
            projectDetailViewModelInstance.Project({ Number: number });

            var returnedData = {
                Project: {
                    BudgetPeriods: []
                }
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedData);
            });
            spyOn(viewModel, "GenerateBudgetPeriodsSelection");

            viewModel.getProject();

            expect(viewModel.GenerateBudgetPeriodsSelection).toHaveBeenCalled();
            expect(memory.getItem("lastProjectVisited")).toBe(projectDetailViewModelInstance.Project().Number());

            expect(memory.getItem("period")).toBe("");
            expect(memory.getItem("activeLineItems")).toBe("");
        });

        it("success() should update lastProjectVisited memory value", function () {
            viewModel = new projectDetailViewModel();


            var expectedPeriod = "10";
            memory.setItem("period", expectedPeriod);
            var expectedActiveLineItems = "line item";
            memory.setItem("activeLineItems", expectedActiveLineItems);

            projectDetailViewModelInstance = viewModel;
            var number = ko.observable("1");
            projectDetailViewModelInstance.Project = ko.observable({ Number: number });

            var returnedData = {
                Project: {
                    BudgetPeriods: []
                }
            };
            spyOn($, "ajax").and.callFake(function (e) {
                e.success(returnedData);
            });
            spyOn(viewModel, "GenerateBudgetPeriodsSelection");

            viewModel.getProject();

            expect(viewModel.GenerateBudgetPeriodsSelection).toHaveBeenCalled();
            expect(memory.getItem("lastProjectVisited")).toBe(projectDetailViewModelInstance.Project().Number());

            expect(memory.getItem("period")).toBe(expectedPeriod);
            expect(memory.getItem("activeLineItems")).toBe(expectedActiveLineItems);
        });

        it("error() should add a 404 error to the notification center", function () {
            viewModel = new projectDetailViewModel();

            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 404 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getProject();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: resourceNotFoundText, type: "error" });
        });

        it("error() should add a cost center load error to the notification center", function () {
            viewModel = new projectDetailViewModel();

            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.error({ status: 500 }, {}, {});
            });
            spyOn($.fn, "notificationCenter");
            viewModel.getProject();

            expect($.fn.notificationCenter).toHaveBeenCalledWith('addNotification', { message: unableToLoadProjectText, type: "error" });
        });

        it("complete() sets observables", function () {
            viewModel = new projectDetailViewModel();

            viewModel.IsPageLoaded(true);
            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });

            viewModel.getProject();

            expect(viewModel.IsPageLoaded()).toBeTruthy();
            expect(viewModel.IsFilterUpdating()).toBeFalsy();
            expect(viewModel.GlDetailTabSelected()).toBe("E");
        });

        it("complete() calls functions", function () {
            viewModel = new projectDetailViewModel();
            projectDetailViewModelInstance = viewModel;

            viewModel.IsPageLoaded(true);
            viewModel.GlDetailView(false);
            viewModel.usingFilter(true);

            Ellucian.ColleagueFinance.glNumber = "11-11-11-11111-11111";

            spyOn($, "ajax").and.callFake(function (e) {
                e.complete();
            });
            spyOn(viewModel, "setLineItems");
            spyOn(viewModel, "toggleView");
            spyOn(projectDetailViewModelInstance, "checkForMobile");
            spyOn($.fn, "show");
            spyOn($.fn, "focus");

            viewModel.getProject();

            expect(viewModel.setLineItems).toHaveBeenCalled();
            expect(viewModel.checkForMobile).toHaveBeenCalled();
            expect(viewModel.toggleView).toHaveBeenCalledWith(Ellucian.ColleagueFinance.glNumber);
            expect($.fn.show).toHaveBeenCalled();
            expect($.fn.focus).toHaveBeenCalled();
        });
    });

    describe("isExportDropdownExpanded", function () {
        it("initializes as false", function () {
            viewModel = new projectDetailViewModel();

            expect(viewModel.isExportDropdownExpanded()).toBeFalsy();
        });
    });

    describe("toggleExportDropdown", function () {
        it("toggles the isExportDropdownExpanded observable", function () {
            viewModel = new projectDetailViewModel();

            viewModel.toggleExportDropdown();

            expect(viewModel.isExportDropdownExpanded()).toBeTruthy();
        });
    });

    describe("flatProjectLineItems", function () {
        it("flattens asset line items", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            projectDetailViewModelInstance = viewModel;
            var project = projectRepository.getProject();

            project.LiabilityLineItems = [];
            project.FundBalanceLineItems = [];
            project.RevenueLineItems = [];
            project.ExpenseLineItems = [];

            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(3);

            expect(actual[0].glClass).toBe(Ellucian.ProjectsAccounting.assetLabel);
            expect(actual[0].lineItem).toBe(project.AssetLineItems[0].ItemCodeDescription + " " + project.AssetLineItems[0].ItemCode);
            expect(actual[0].formattedGlAccount).toBe(empty);
            expect(actual[0].budget).toBe(project.AssetLineItems[0].TotalBudget);
            expect(actual[0].actuals).toBe(empty);
            expect(actual[0].encumbrances).toBe(empty);
            expect(actual[0].remaining).toBe(empty);
            expect(actual[0].percentSpent).toBe(empty);

            expect(actual[1].glClass).toBe(Ellucian.ProjectsAccounting.assetLabel);
            expect(actual[1].lineItem).toBe(project.AssetLineItems[0].ItemCodeDescription + " " + project.AssetLineItems[0].ItemCode);
            expect(actual[1].formattedGlAccount).toBe(Ellucian.ProjectsAccounting.amountPendingPostingLabel);
            expect(actual[1].budget).toBe(empty);
            expect(actual[1].actuals).toBe(project.AssetLineItems[0].TotalMemoActuals);
            expect(actual[1].encumbrances).toBe(project.AssetLineItems[0].TotalMemoEncumbrances);
            expect(actual[1].remaining).toBe(empty);
            expect(actual[1].percentSpent).toBe(empty);

            expect(actual[2].glClass).toBe(Ellucian.ProjectsAccounting.assetLabel);
            expect(actual[2].lineItem).toBe(project.AssetLineItems[0].ItemCodeDescription + " " + project.AssetLineItems[0].ItemCode);
            expect(actual[2].formattedGlAccount).toBe(project.AssetLineItems[0].GlAccounts[0].FormattedGlAccount);
            expect(actual[2].budget).toBe(empty);
            expect(actual[2].actuals).toBe(project.AssetLineItems[0].GlAccounts[0].Actuals);
            expect(actual[2].encumbrances).toBe(project.AssetLineItems[0].GlAccounts[0].Encumbrances);
            expect(actual[2].remaining).toBe(empty);
            expect(actual[2].percentSpent).toBe(empty);
        });

        it("flattens liability line items", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            projectDetailViewModelInstance = viewModel;
            var project = projectRepository.getProject();

            project.AssetLineItems = [];
            project.FundBalanceLineItems = [];
            project.RevenueLineItems = [];
            project.ExpenseLineItems = [];

            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(3);

            expect(actual[0].glClass).toBe(Ellucian.ProjectsAccounting.liabilityLabel);
            expect(actual[0].lineItem).toBe(project.LiabilityLineItems[0].ItemCodeDescription + " " + project.LiabilityLineItems[0].ItemCode);
            expect(actual[0].formattedGlAccount).toBe(empty);
            expect(actual[0].budget).toBe(project.LiabilityLineItems[0].TotalBudget);
            expect(actual[0].actuals).toBe(empty);
            expect(actual[0].encumbrances).toBe(empty);
            expect(actual[0].remaining).toBe(empty);
            expect(actual[0].percentSpent).toBe(empty);

            expect(actual[1].glClass).toBe(Ellucian.ProjectsAccounting.liabilityLabel);
            expect(actual[1].lineItem).toBe(project.LiabilityLineItems[0].ItemCodeDescription + " " + project.LiabilityLineItems[0].ItemCode);
            expect(actual[1].formattedGlAccount).toBe(Ellucian.ProjectsAccounting.amountPendingPostingLabel);
            expect(actual[1].budget).toBe(empty);
            expect(actual[1].actuals).toBe(project.LiabilityLineItems[0].TotalMemoActuals);
            expect(actual[1].encumbrances).toBe(project.LiabilityLineItems[0].TotalMemoEncumbrances);
            expect(actual[1].remaining).toBe(empty);
            expect(actual[1].percentSpent).toBe(empty);

            expect(actual[2].glClass).toBe(Ellucian.ProjectsAccounting.liabilityLabel);
            expect(actual[2].lineItem).toBe(project.LiabilityLineItems[0].ItemCodeDescription + " " + project.LiabilityLineItems[0].ItemCode);
            expect(actual[2].formattedGlAccount).toBe(project.LiabilityLineItems[0].GlAccounts[0].FormattedGlAccount);
            expect(actual[2].budget).toBe(empty);
            expect(actual[2].actuals).toBe(project.LiabilityLineItems[0].GlAccounts[0].Actuals);
            expect(actual[2].encumbrances).toBe(project.LiabilityLineItems[0].GlAccounts[0].Encumbrances);
            expect(actual[2].remaining).toBe(empty);
            expect(actual[2].percentSpent).toBe(empty);
        });

        it("flattens fund balance line items", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            projectDetailViewModelInstance = viewModel;
            var project = projectRepository.getProject();

            project.AssetLineItems = [];
            project.LiabilityLineItems = [];
            project.RevenueLineItems = [];
            project.ExpenseLineItems = [];

            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(3);

            expect(actual[0].glClass).toBe(Ellucian.ProjectsAccounting.fundBalanceLabel);
            expect(actual[0].lineItem).toBe(project.FundBalanceLineItems[0].ItemCodeDescription + " " + project.FundBalanceLineItems[0].ItemCode);
            expect(actual[0].formattedGlAccount).toBe(empty);
            expect(actual[0].budget).toBe(project.FundBalanceLineItems[0].TotalBudget);
            expect(actual[0].actuals).toBe(empty);
            expect(actual[0].encumbrances).toBe(empty);
            expect(actual[0].remaining).toBe(empty);
            expect(actual[0].percentSpent).toBe(empty);

            expect(actual[1].glClass).toBe(Ellucian.ProjectsAccounting.fundBalanceLabel);
            expect(actual[1].lineItem).toBe(project.FundBalanceLineItems[0].ItemCodeDescription + " " + project.FundBalanceLineItems[0].ItemCode);
            expect(actual[1].formattedGlAccount).toBe(Ellucian.ProjectsAccounting.amountPendingPostingLabel);
            expect(actual[1].budget).toBe(empty);
            expect(actual[1].actuals).toBe(project.FundBalanceLineItems[0].TotalMemoActuals);
            expect(actual[1].encumbrances).toBe(project.FundBalanceLineItems[0].TotalMemoEncumbrances);
            expect(actual[1].remaining).toBe(empty);
            expect(actual[1].percentSpent).toBe(empty);

            expect(actual[2].glClass).toBe(Ellucian.ProjectsAccounting.fundBalanceLabel);
            expect(actual[2].lineItem).toBe(project.FundBalanceLineItems[0].ItemCodeDescription + " " + project.FundBalanceLineItems[0].ItemCode);
            expect(actual[2].formattedGlAccount).toBe(project.FundBalanceLineItems[0].GlAccounts[0].FormattedGlAccount);
            expect(actual[2].budget).toBe(empty);
            expect(actual[2].actuals).toBe(project.FundBalanceLineItems[0].GlAccounts[0].Actuals);
            expect(actual[2].encumbrances).toBe(project.FundBalanceLineItems[0].GlAccounts[0].Encumbrances);
            expect(actual[2].remaining).toBe(empty);
            expect(actual[2].percentSpent).toBe(empty);
        });

        it("flattens revenue line items", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            projectDetailViewModelInstance = viewModel;
            var project = projectRepository.getProject();

            project.AssetLineItems = [];
            project.LiabilityLineItems = [];
            project.FundBalanceLineItems = [];
            project.ExpenseLineItems = [];

            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(3);

            expect(actual[0].glClass).toBe(Ellucian.ProjectsAccounting.revenueLabel);
            expect(actual[0].lineItem).toBe(project.RevenueLineItems[0].ItemCodeDescription + " " + project.RevenueLineItems[0].ItemCode);
            expect(actual[0].formattedGlAccount).toBe(empty);
            expect(actual[0].budget).toBe(project.RevenueLineItems[0].TotalBudget);
            expect(actual[0].actuals).toBe(empty);
            expect(actual[0].encumbrances).toBe(empty);
            expect(actual[0].remaining).toBe(project.RevenueLineItems[0].BudgetRemainingFormatted);
            expect(actual[0].percentSpent).toBe(project.RevenueLineItems[0].PercentProcessed);

            expect(actual[1].glClass).toBe(Ellucian.ProjectsAccounting.revenueLabel);
            expect(actual[1].lineItem).toBe(project.RevenueLineItems[0].ItemCodeDescription + " " + project.RevenueLineItems[0].ItemCode);
            expect(actual[1].formattedGlAccount).toBe(Ellucian.ProjectsAccounting.amountPendingPostingLabel);
            expect(actual[1].budget).toBe(empty);
            expect(actual[1].actuals).toBe(project.RevenueLineItems[0].TotalMemoActuals);
            expect(actual[1].encumbrances).toBe(project.RevenueLineItems[0].TotalMemoEncumbrances);
            expect(actual[1].remaining).toBe(empty);
            expect(actual[1].percentSpent).toBe(empty);

            expect(actual[2].glClass).toBe(Ellucian.ProjectsAccounting.revenueLabel);
            expect(actual[2].lineItem).toBe(project.RevenueLineItems[0].ItemCodeDescription + " " + project.RevenueLineItems[0].ItemCode);
            expect(actual[2].formattedGlAccount).toBe(project.RevenueLineItems[0].GlAccounts[0].FormattedGlAccount);
            expect(actual[2].budget).toBe(empty);
            expect(actual[2].actuals).toBe(project.RevenueLineItems[0].GlAccounts[0].ActualsRevenue);
            expect(actual[2].encumbrances).toBe(project.RevenueLineItems[0].GlAccounts[0].EncumbrancesRevenue);
            expect(actual[2].remaining).toBe(empty);
            expect(actual[2].percentSpent).toBe(empty);
        });

        it("flattens expense line items", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            projectDetailViewModelInstance = viewModel;
            var project = projectRepository.getProject();

            project.AssetLineItems = [];
            project.LiabilityLineItems = [];
            project.FundBalanceLineItems = [];
            project.RevenueLineItems = [];

            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(3);

            expect(actual[0].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[0].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[0].formattedGlAccount).toBe(empty);
            expect(actual[0].budget).toBe(project.ExpenseLineItems[0].TotalBudget);
            expect(actual[0].actuals).toBe(empty);
            expect(actual[0].encumbrances).toBe(empty);
            expect(actual[0].remaining).toBe(project.ExpenseLineItems[0].BudgetRemainingFormatted);
            expect(actual[0].percentSpent).toBe(project.ExpenseLineItems[0].PercentProcessed);

            expect(actual[1].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[1].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[1].formattedGlAccount).toBe(Ellucian.ProjectsAccounting.amountPendingPostingLabel);
            expect(actual[1].budget).toBe(empty);
            expect(actual[1].actuals).toBe(project.ExpenseLineItems[0].TotalMemoActuals);
            expect(actual[1].encumbrances).toBe(project.ExpenseLineItems[0].TotalMemoEncumbrances);
            expect(actual[1].remaining).toBe(empty);
            expect(actual[1].percentSpent).toBe(empty);

            expect(actual[2].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[2].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[2].formattedGlAccount).toBe(project.ExpenseLineItems[0].GlAccounts[0].FormattedGlAccount);
            expect(actual[2].budget).toBe(empty);
            expect(actual[2].actuals).toBe(project.ExpenseLineItems[0].GlAccounts[0].Actuals);
            expect(actual[2].encumbrances).toBe(project.ExpenseLineItems[0].GlAccounts[0].Encumbrances);
            expect(actual[2].remaining).toBe(empty);
            expect(actual[2].percentSpent).toBe(empty);
        });

        it("flattens (positive) amounts pending posting", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            projectDetailViewModelInstance = viewModel;
            var project = projectRepository.getProject();

            project.AssetLineItems = [];
            project.LiabilityLineItems = [];
            project.FundBalanceLineItems = [];
            project.RevenueLineItems = [];
            project.ExpenseLineItems[0].TotalMemos = 100.00;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(3);

            expect(actual[0].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[0].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[0].formattedGlAccount).toBe(empty);
            expect(actual[0].budget).toBe(project.ExpenseLineItems[0].TotalBudget);
            expect(actual[0].actuals).toBe(empty);
            expect(actual[0].encumbrances).toBe(empty);
            expect(actual[0].remaining).toBe(project.ExpenseLineItems[0].BudgetRemainingFormatted);
            expect(actual[0].percentSpent).toBe(project.ExpenseLineItems[0].PercentProcessed);

            expect(actual[1].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[1].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[1].formattedGlAccount).toBe(Ellucian.ProjectsAccounting.amountPendingPostingLabel);
            expect(actual[1].budget).toBe(empty);
            expect(actual[1].actuals).toBe(project.ExpenseLineItems[0].TotalMemoActuals);
            expect(actual[1].encumbrances).toBe(project.ExpenseLineItems[0].TotalMemoEncumbrances);
            expect(actual[1].remaining).toBe(empty);
            expect(actual[1].percentSpent).toBe(empty);

            expect(actual[2].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[2].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[2].formattedGlAccount).toBe(project.ExpenseLineItems[0].GlAccounts[0].FormattedGlAccount);
            expect(actual[2].budget).toBe(empty);
            expect(actual[2].actuals).toBe(project.ExpenseLineItems[0].GlAccounts[0].Actuals);
            expect(actual[2].encumbrances).toBe(project.ExpenseLineItems[0].GlAccounts[0].Encumbrances);
            expect(actual[2].remaining).toBe(empty);
            expect(actual[2].percentSpent).toBe(empty);
        });

        it("flattens (negative) amounts pending posting", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            projectDetailViewModelInstance = viewModel;
            var project = projectRepository.getProject();

            project.AssetLineItems = [];
            project.LiabilityLineItems = [];
            project.FundBalanceLineItems = [];
            project.RevenueLineItems = [];
            project.ExpenseLineItems[0].GlAccounts = [];
            project.ExpenseLineItems[0].TotalMemoActuals = "$0.00";
            project.ExpenseLineItems[0].TotalMemoEncumbrances = "-$0.01";
            project.ExpenseLineItems[0].TotalMemos = -0.01;

            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(2);

            expect(actual[0].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[0].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[0].formattedGlAccount).toBe(empty);
            expect(actual[0].budget).toBe(project.ExpenseLineItems[0].TotalBudget);
            expect(actual[0].actuals).toBe(empty);
            expect(actual[0].encumbrances).toBe(empty);
            expect(actual[0].remaining).toBe(project.ExpenseLineItems[0].BudgetRemainingFormatted);
            expect(actual[0].percentSpent).toBe(project.ExpenseLineItems[0].PercentProcessed);

            expect(actual[1].glClass).toBe(Ellucian.ProjectsAccounting.expenseLabel);
            expect(actual[1].lineItem).toBe(project.ExpenseLineItems[0].ItemCodeDescription + " " + project.ExpenseLineItems[0].ItemCode);
            expect(actual[1].formattedGlAccount).toBe(Ellucian.ProjectsAccounting.amountPendingPostingLabel);
            expect(actual[1].budget).toBe(empty);
            expect(actual[1].actuals).toBe(project.ExpenseLineItems[0].TotalMemoActuals);
            expect(actual[1].encumbrances).toBe(project.ExpenseLineItems[0].TotalMemoEncumbrances);
            expect(actual[1].remaining).toBe(empty);
            expect(actual[1].percentSpent).toBe(empty);
        });

        it("flattens transactions for the selected GL account number", function () {
            viewModel = new projectDetailViewModel();
            var project = new projectRepository.getProject();
            viewModel.Project(new projectModel(project));
            viewModel.GlNumberSelected("11_01_01_11111_01111");
            viewModel.GlDetailView(true);

            var actual = viewModel.flatProjectLineItems();

            expect(actual.length).toBe(3);

            expect(actual[0].Type).toBe(Ellucian.ProjectsAccounting.encumbrancesLabel);
            expect(actual[0].Document).toBe("'" + project.ExpenseLineItems[0].GlAccounts[0].EncumbranceTransactions[0].ReferenceNumber + "'");
            expect(actual[0].Date).toBe(project.ExpenseLineItems[0].GlAccounts[0].EncumbranceTransactions[0].Date);
            expect(actual[0].Description).toBe(project.ExpenseLineItems[0].GlAccounts[0].EncumbranceTransactions[0].Description);
            expect(actual[0].Amount).toBe(project.ExpenseLineItems[0].GlAccounts[0].EncumbranceTransactions[0].Amount);

            expect(actual[1].Type).toBe(Ellucian.ProjectsAccounting.actualsLabel);
            expect(actual[1].Document).toBe("'" + project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[0].ReferenceNumber + "'");
            expect(actual[1].Date).toBe(project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[0].Date);
            expect(actual[1].Description).toBe(project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[0].Description);
            expect(actual[1].Amount).toBe(project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[0].Amount);

            expect(actual[2].Type).toBe(Ellucian.ProjectsAccounting.actualsLabel);
            expect(actual[2].Document).toBe("'" + project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[1].ReferenceNumber + "'");
            expect(actual[2].Date).toBe(project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[1].Date);
            expect(actual[2].Description).toBe(project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[1].Description);
            expect(actual[2].Amount).toBe(project.ExpenseLineItems[0].GlAccounts[0].ActualTransactions[1].Amount);
        });
    });

    describe("csvFileName", function () {
        it("matches on SelectedBudgetPeriod", function () {
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);
            var project = projectRepository.getProject();
            viewModel.GenerateBudgetPeriodsSelection(project.BudgetPeriodsSelection);
            projectDetailViewModelInstance = viewModel;
            project.RevenueLineItems = [];
            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            viewModel.SelectedBudgetPeriod('1');

            var expected = Ellucian.ProjectsAccounting.downloadProjectFileName +
                '-' +
                project.BudgetPeriodsSelection[1].PeriodDateRange.replace('-', 'to').replace(/\//gm, '-').replace(/\s+/gm, '-') +
                '.csv';

            expect(viewModel.csvFileName()).toBe(expected);
        });

        it("matches using default value if SelectedBudgetPeriod is not set", function () {
            viewModel = new projectDetailViewModel();
            var project = projectRepository.getProject();
            viewModel.GenerateBudgetPeriodsSelection(project.BudgetPeriodsSelection);
            projectDetailViewModelInstance = viewModel;
            project.RevenueLineItems = [];
            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            var expected = Ellucian.ProjectsAccounting.downloadProjectFileName +
                '-' +
                Ellucian.ProjectsAccounting.allActivityOption.replace('-', 'to').replace(/\//gm, '-').replace(/\s+/gm, '-') +
                '.csv';

            expect(viewModel.csvFileName()).toBe(expected);
        });

        it("uses default name if no BudgetPeriodsSelection items match", function () {
            viewModel.GlDetailView(false);
            var project = projectRepository.getProject();
            viewModel.SelectedBudgetPeriod('Not A Valid Id');
            spyOn($, "ajax").and.callFake(function (e) {
                e.success({ Project: project });
            });

            viewModel.getProject(true);

            expect(viewModel.csvFileName()).toBe(Ellucian.ProjectsAccounting.downloadProjectFileName + '.csv')
        });
    });

    describe("csvColumns for Project detail view", function () {
        it("returns the list of properties to export for the Project detail view", function () {
            var expectedColumns = [
            "glClass",
            "lineItem",
            "formattedGlAccount",
            "budget",
            "actuals",
            "encumbrances",
            "remaining",
            "percentSpent"
            ];
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);

            var actualColumns = viewModel.csvColumns();

            expect(actualColumns.length).toBe(expectedColumns.length);
            for (var i = 0; i < expectedColumns.length; i++) {
                expect(actualColumns[i]).toBe(expectedColumns[i]);
            }
        });
    });

    describe("csvColumns for GL Detail view", function () {
        it("returns the list of properties to export for the GL Detail view", function () {
            var expectedColumns = [
                "Type",
                "Document",
                "Date",
                "Description",
                "Amount"
            ];
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(true);

            var actualColumns = viewModel.csvColumns();

            expect(actualColumns.length).toBe(expectedColumns.length);
            for (var i = 0; i < expectedColumns.length; i++) {
                expect(actualColumns[i]).toBe(expectedColumns[i]);
            }
        });
    });

    describe("csvColumnHeaders for Project detail view", function () {
        it("returns the list of properties to export", function () {
            var expectedColumns = [
                Ellucian.ProjectsAccounting.glClassTitle,
                Ellucian.ProjectsAccounting.projectLineItemTitle,
                Ellucian.ProjectsAccounting.glAccountTitle,
                Ellucian.ProjectsAccounting.budgetTitle,
                Ellucian.ProjectsAccounting.actualsTitle,
                Ellucian.ProjectsAccounting.encumbrancesTitle,
                Ellucian.ProjectsAccounting.remainingTitle,
                Ellucian.ProjectsAccounting.percentSpentReceivedTitle
            ];
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(false);

            var actualColumns = viewModel.csvColumnHeaders();

            expect(actualColumns.length).toBe(expectedColumns.length);
            for (var i = 0; i < expectedColumns.length; i++) {
                expect(actualColumns[i]).toBe(expectedColumns[i]);
            }
        });
    });

    describe("csvColumnHeaders for GL Detail view", function () {
        it("returns the list of properties to export", function () {
            var expectedColumns = [
                Ellucian.ProjectsAccounting.transactionTypeHeader,
                Ellucian.ProjectsAccounting.documentIdHeader,
                Ellucian.ProjectsAccounting.dateHeader,
                Ellucian.ProjectsAccounting.descriptionHeader,
                Ellucian.ProjectsAccounting.amountHeader
            ];
            viewModel = new projectDetailViewModel();
            viewModel.GlDetailView(true);

            var actualColumns = viewModel.csvColumnHeaders();

            expect(actualColumns.length).toBe(expectedColumns.length);
            for (var i = 0; i < expectedColumns.length; i++) {
                expect(actualColumns[i]).toBe(expectedColumns[i]);
            }
        });
    });

    describe("SelectedGlAccountHasTransactions", function () {
        it("should return false if the selected Asset GL account has no transactions", function () {
            viewModel.GlNumberSelected("11_01_01_11111_11111");
            expect(viewModel.SelectedGlAccountHasTransactions()).toBeFalsy();
        });

        it("should return false if the selected Liability GL account has no transactions", function () {
            viewModel.GlNumberSelected("11_01_01_11111_31111");
            expect(viewModel.SelectedGlAccountHasTransactions()).toBeFalsy();
        });

        it("should return false if the selected Fund Balance GL account has no transactions", function () {
            viewModel.GlNumberSelected("11_11_11_11111_41111");
            expect(viewModel.SelectedGlAccountHasTransactions()).toBeFalsy();
        });

        it("should return false if the selected Revenue GL account has no transactions", function () {
            viewModel.GlNumberSelected("11_01_01_11111_21111");
            expect(viewModel.SelectedGlAccountHasTransactions()).toBeFalsy();
        });

        it("should return false if the selected Expense GL account has no transactions", function () {
            viewModel.GlNumberSelected("11_01_01_11111_01111");
            expect(viewModel.SelectedGlAccountHasTransactions()).toBeTruthy();
        });

        it("should return false if the project is not defined", function () {
            viewModel = new projectDetailViewModel();
            expect(viewModel.isProjectDefined()).toBeFalsy();
            expect(viewModel.SelectedGlAccountHasTransactions()).toBeFalsy();
        });

        it("should return false if GlNumberSelected is blank", function () {
            viewModel.GlNumberSelected("");
            expect(viewModel.SelectedGlAccountHasTransactions()).toBeFalsy();
        });
    });

    describe("EnableExport", function () {
        it("should return true if the page is being viewed in summary mode", function () {
            viewModel.GlDetailView(false);
            expect(viewModel.EnableExport()).toBeTruthy();
        });

        it("should return true if the page is in detail mode AND the selected GL account has transactions", function () {
            viewModel.GlDetailView(true);
            viewModel.GlNumberSelected("11_01_01_11111_01111");
            expect(viewModel.EnableExport()).toBeTruthy();
        });

        it("should return false if the page is in detail mode AND the selected GL account has no transactions", function () {
            viewModel.GlDetailView(true);
            viewModel.GlNumberSelected("11_01_01_11111_21111");
            expect(viewModel.EnableExport()).toBeFalsy();
        });
    });
});

describe("glTransactionViewModel", function () {
    var viewModel;
    var data = {
        "Source": "EP",
        "ReferenceNumber": "P000001",
        "DocumentId": "0001234"
    };
    it("should call ko.mapping.fromJS if projectMapping is defined", function () {
        spyOn(ko.mapping, "fromJS");
        viewModel = new glTransactionViewModel(data);

        expect(ko.mapping.fromJS).toHaveBeenCalledWith(data, projectMapping, viewModel);
    });


});
