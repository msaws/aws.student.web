﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

describe("budgetPeriodModel", function () {
    var viewModel;
    beforeEach(function () {
        viewModel = new budgetPeriodModel();
    });

    it("Id is null after initialization", function () {
        expect(typeof viewModel.Id()).toBe("undefined");
    });

    it("Description is null after initialization", function () {
        expect(typeof viewModel.Description()).toBe("undefined");
    });
})