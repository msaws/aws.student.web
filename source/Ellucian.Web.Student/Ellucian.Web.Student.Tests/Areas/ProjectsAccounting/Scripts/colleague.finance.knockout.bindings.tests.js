﻿// Copyright 2016 Ellucian Company L.P. and its affiliates

// Test some of the bindings using the project detail view model.
describe("projectDetailViewModel", function () {
    var viewModel;

    beforeEach(function () {
        viewModel = new projectDetailViewModel();
    });

    describe("toggleGlDetailView", function () {
        it("should call toggleView on click", function () {
            spyOn(viewModel, "toggleView");

            ko.initializeCustomBinding("div", { toggleGlDetailView: {} }, viewModel, function (bindingElement) {
                bindingElement.click();
                expect(viewModel.toggleView).toHaveBeenCalled();
            });
        });
    });
});