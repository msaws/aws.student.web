﻿ErrorGettingBankingInformationConfigurationMessage = "error";
getBankingInformationConfigurationActionUrl = "bankinginformationconfigurationdotcom"
personProxyLoadingThrobberAltText = "Generic alternate loading text";
personProxyChangingThrobberAltText = "Generaic alternate changing text"
personProxyLoadingThrobberMessage = "Generic loading message";
personProxyChangingThrobberMessage = "Generic changing message";
EditAccountDetailsModalSavingLabel = 'Modal is saving test label';
UpdateBankAccountDetailsSuccessMessage = 'A wild and unprecedented success has occured while saving your bank acount details';
UpdateBankAccountDetailsErrorMessage = 'uh oh';
LoadBankingInformationErrorMessage = "error";
BalanceAccountText = "Balance";
EditAccountDetailsModalTitle = "modal title";
EditDepositDetailsModalSavingLabel = "saving deposit details";
UpdateBankingInformationSuccessMessage = "success!";
UpdateBankingInformationErrorMessage = "error!";
ErrorUnableToResolveBankMessage = 'unable to resolve';
ErrorBankDoesNotExistMessage = 'bank does not exist';
ErrorRoutingNumberNumericValuesOnly = 'numbers only!';
ErrorRoutingNumberInvalidMessage = 'plain old invalid';
ErrorRoutingNumberIncorrectLengthMessage = '9 chars please';
ErrorBranchNumberIncorrectLengthMessage = '5 chars please';
ErrorInstitutionIdIncorrectLengthMessage = '3 chars please';
ErrorNicknameMaxLengthMessage = 'too many chars!';
ErrorBranchNumberNumericOnlyMessage = 'numbers only';
ErrorInstitutionIdNumericOnlyMessage = 'numbers only please';
ErrorAccountIdMaxLengthMessage = 'too many account chars';
ErrorInvalidCharactersMessage = 'invalid chars';
SavingNewAccountInformationMessage = "saving new account";
ErrorGettingVendorDeposits = "error getting vendor deposits";
LoadingVendorDeposits = "loading vendor deposits";
UpdateBankAccountDetailsErrorMessage = "unable to update bank account details";
DeletingDepositSuccess = "success deleting deposit";
DeletingDepositLoadingMessage = "deleting deposit loading message";
DeletingDepositError = "error deleting deposit";
ErrorTimeCannotOverlapMessage = "Error time cannot overlap";
putBankingInformationActionUrl = "/updateDepositsUrl";
getBankingInformationActionUrl = "http://getUrl";
getBankAsyncActionUrl = "/getBank";
deletePayrollDepositActionUrl = "/deletePayrollUrl";
deletePayableDepositActionUrl = "/deletePayableUrl";
jsonContentType = "json";
LoadingVendorDeposits = "it appears we have a serious case of vendor depositosis"
personProxyLoadingThrobberMessage = "proxy loading throbber message";
personProxyLoadingThrobberAltText = "Alt Loading";
personProxyChangingThrobberMessage = "Changing...";
personProxyChangingThrobberAltText = "Alt Changing";
NoDuplicateFutureRemainderDepositMessage = "TWO IS TOO MANY";
account = {
    handleInvalidSessionResponse: function (data) {
        return false;
    }
};