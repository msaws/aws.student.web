﻿//hrtd is HumanResourcesTestData scope
var hrtd = function () {
    var todayDateOffset = function(offset) {
        var todayDate = new Date();
        return new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate() + offset);
    };
    return {
        bankingInformationViewModelData: {
            Configuration:
                {
                    AddEditAccountTermsAndConditions: "Destiny guides our fortunes more favorably than we could have expected. Look there, Sancho Panza, my friend, and see those thirty or so wild giants, with whom I intend to do battle and kill each and all of them, so with their stolen booty we can begin to enrich ourselves. This is nobel, righteous warfare, for it is wonderfully useful to God to have such an evil race wiped from the face of the earth. 'What giants?' Asked Sancho Panza.'The ones you can see over there,' answered his master, 'with the huge arms, some of which are very nearly two leagues long.''Now look, your grace,' said Sancho, 'what you see over there aren't giants, but windmills, and what seems to be arms are just their sails, that go around in the wind and turn the millstone.''Obviously,' replied Don Quijote, 'you don't know much about adventures",
                    PayrollMessage: "Everywhere is walking distance if you have the time.",
                    PayrollEffectiveDateMessage: "There is a fine line between fishing and just standing on the shore like an idiot.",
                    IsRemainderAccountRequired: true,
                    UseFederalRoutingDirectory: true,
                    IsDirectDepositEnabled: true,
                    IsPayableDepositEnabled: true,
                },
            IsPayrollActive: true,
            IsPayableActive: true,
            IsVendor: false,
            Addresses: [
                {
                    Id: "''",
                    AddressLines : [
                        "Default Address"
                    ]
                },
                {
                    Id: "1",
                    AddressLines : [
                        "I live here",
                    ]
                },
                {
                    Id: "2",
                    AddressLines: [
                        "I no longer",
                        "live here"
                    ]
                }
            ],
            BankAccounts: [
                {
                    RoutingId: "011103093",
                    InstitutionId: "",
                    BranchNumber: "",
                    BankName: "Goldman Sachs",
                    EncryptedAccountId: "abbbb",
                    AccountIdLastFour: 'bbbb',
                    NewAccountId: '',
                    Nickname: "Too Big To Fail",
                    Type: 1,
                    Id: "011103093_b",
                    IsPayrollVerified: true,
                    IsPayableVerified: true,
                },
                {
                    RoutingId: "067014822",
                    InstitutionId: "",
                    BranchNumber: "",
                    BankName: "HSBC",
                    EncryptedAccountId: "adddd",
                    AccountIdLastFour: 'dddd',
                    NewAccountId: '',
                    Nickname: "Money Laundramat",
                    Type: 1,
                    Id: "067014822_d",
                    IsPayrollVerified: false,
                    IsPayableVerified: false,
                },
                {
                    RoutingId: "211274450",
                    InstitutionId: "",
                    BranchNumber: "",
                    BankName: "Bernie Madoff",
                    EncryptedAccountId: "affff",
                    AccountIdLastFour: "ffff",
                    NewAccountId: '',
                    Nickname: "Made Off With My Money",
                    Type:0,
                    Id: "211274450_f",
                    IsPayrollVerified: true,
                    IsPayableVerified: false,
                }
            ],
            ActivePayrollDeposits: [
                {                                  
                    DepositAmount: 29393,
                    StartDate: new Date(2015, 1, 1),
                    EndDate: null,
                    BankId: "011103093_b",
                    OriginalDepositOrder: 5,
                    DepositOrder: 1,
                    IsVerified: true,
                    AddDateTime: new Date(2014,12,1),
                },
                {
                    DepositAmount: 2323,
                    StartDate: new Date(2015, 1, 1),
                    EndDate: null,
                    BankId: "011103093_b",
                    OriginalDepositOrder: 6,
                    DepositOrder: 2,
                    IsVerified: true,
                    AddDateTime: new Date(2014, 12, 2),
                },
                {
                    DepositAmount: 4569,
                    StartDate: new Date(2015, 1, 1),
                    EndDate: null,
                    BankId: "067014822_d",
                    OriginalDepositOrder: 7,
                    DepositOrder: 3,
                    IsVerified: false,
                    AddDateTime: new Date(2014, 12, 3),
                },
                {
                    DepositAmount: null,
                    StartDate: new Date(2015, 1, 1),
                    EndDate: null,
                    BankId: "067014822_d",
                    OriginalDepositOrder: 8,
                    DepositOrder: 999,
                    IsVerified: false,
                    AddDateTime: new Date(2014, 12, 4),
                }
            ],
            PastPayrollDeposits: [
                {
                    DepositAmount: 45,
                    OriginalDepositOrder: 1,
                    DepositOrder: 1,
                    StartDate: new Date(2011, 1, 1),
                    EndDate: new Date(2015, 2, 2),
                    BankId: "211274450_f",
                    AddDateTime: new Date(1914, 12, 31),
                },
                {
                    DepositAmount: 52,
                    OriginalDepositOrder: 2,
                    DepositOrder: 2,
                    StartDate: new Date(2011, 1, 1),
                    EndDate: new Date(2015, 2, 2),
                    BankId: "211274450_f",
                    AddDateTime: new Date(1914, 12, 31),

                },
                {
                    DepositAmount: 99,
                    OriginalDepositOrder: 2,
                    DepositOrder: 2,
                    StartDate: new Date(2011, 1, 1),
                    EndDate: new Date(2015, 2, 2),
                    BankId: "011103093_b",
                    AddDateTime: new Date(1914, 12, 31),
                },
                {
                    DepositAmount: null,
                    OriginalDepositOrder: 999,
                    DepositOrder: 999,
                    StartDate: new Date(2011, 1, 1),
                    EndDate: new Date(2015, 2, 2),
                    BankId: "011103093_b",
                    AddDateTime: new Date(1914, 12, 31),
                }
            ],
            FuturePayrollDeposits: [
                {
                    AddDateTime: new Date(1925, 12, 31),
                    DepositAmount: 981,
                    OriginalDepositOrder: 1,
                    DepositOrder: 1,
                    StartDate: todayDateOffset(1),
                    EndDate: null,
                    BankId: "067014822_d",
                    IsVerififed: false,
                },
                {
                    AddDateTime: new Date(1924, 12, 31),
                    DepositAmount: 189,
                    OriginalDepositOrder: 2,
                    DepositOrder: 2,
                    StartDate: todayDateOffset(2),
                    EndDate: null,
                    BankId: "067014822_d",
                    IsVerififed: false,
                },
                {
                    AddDateTime: new Date(1923, 12, 31),
                    DepositAmount: 198,
                    OriginalDepositOrder: 3,
                    DepositOrder: 3,
                    StartDate: todayDateOffset(3),
                    EndDate: null,
                    BankId: "211274450_f",
                },
                {
                    AddDateTime: new Date(1922, 12, 31),
                    DepositAmount: 9999,
                    OriginalDepositOrder: 4,
                    DepositOrder: 4,
                    StartDate: todayDateOffset(4),
                    EndDate: null,
                    BankId: "211274450_f",
                }
            ],
            ActiveRefundDeposit: {
                Id: "11111",
                StartDate: todayDateOffset(-1),
                EndDate: todayDateOffset(1),
                BankId: "011103093_b",
                IsElectronicPaymentRequested: true,
                IsVerified: true,
                AddDateTime: new Date(2014, 12, 31),
                ChangeOperator: 'bhr',
                AddressId: "",
            },
            FutureRefundDeposits: [
                {
                    Id: "22222",
                    StartDate: todayDateOffset(2),
                    EndDate: todayDateOffset(4),
                    BankId: "011103093_b",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: true,
                    AddDateTime: new Date(2034, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                },
                {
                    Id: "33333",
                    StartDate: todayDateOffset(5),
                    EndDate: todayDateOffset(7),
                    BankId: "067014822_d",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: false,
                    AddDateTime: new Date(2034, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                },
                {
                    Id: "44444",
                    StartDate: todayDateOffset(8),
                    EndDate: todayDateOffset(10),
                    BankId: "067014822_d",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: false,
                    AddDateTime: new Date(2034, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                },
                {
                    Id: "55555",
                    StartDate: todayDateOffset(11),
                    EndDate: todayDateOffset(13),
                    BankId: "211274450_f",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: false,
                    AddDateTime: new Date(2034, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                }
            ],
            PastRefundDeposits: [
                {
                    Id: "66666",
                    StartDate: todayDateOffset(-4),
                    EndDate: todayDateOffset(-2),
                    BankId: "211274450_f",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: true,
                    AddDateTime: new Date(1999, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                },
                {
                    Id: "77777",
                    StartDate: todayDateOffset(-7),
                    EndDate: todayDateOffset(-5),
                    BankId: "011103093_b",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: true,
                    AddDateTime: new Date(1999, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                },
                {
                    Id: "88888",
                    StartDate: todayDateOffset(-10),
                    EndDate: todayDateOffset(-8),
                    BankId: "211274450_f",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: false,
                    AddDateTime: new Date(1999, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                },
                {
                    Id: "99999",
                    StartDate: todayDateOffset(-13),
                    EndDate: todayDateOffset(-11),
                    BankId: "211274450_f",
                    IsElectronicPaymentRequested: true,
                    IsVerififed: false,
                    AddDateTime: new Date(1999, 12, 31),
                    ChangeOperator: 'bhr',
                    AddressId: "",
                }
            ]
        }

    };
}

