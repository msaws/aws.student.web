﻿using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.HumanResources.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class PayableDepositViewModelTests
    {
        public PayableDepositViewModel viewModel;
        public BankingInformationTestData testData;
        public PayableDepositDirective inputPayableDepositDirective;
        public string personId;

        public void PayableDepositViewModelTestsInitialize()
        {
            personId = "0003914";
            testData = new BankingInformationTestData(personId);
            inputPayableDepositDirective = testData.payableDepositDirectives.First();
        }

        [TestInitialize]
        public void Initialize()
        {
            PayableDepositViewModelTestsInitialize();
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            viewModel = new PayableDepositViewModel();
            Assert.IsNull(viewModel.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PayableDepositDirectiveRequiredTest()
        {
            viewModel = new PayableDepositViewModel(null);
        }

        [TestMethod]
        public void ViewModelEqualsDirectiveTest()
        {
            viewModel = new PayableDepositViewModel(inputPayableDepositDirective);
            Assert.AreEqual(viewModel.Id, inputPayableDepositDirective.Id);
        }

        [TestMethod]
        public void TidyAddressTest()
        {
            inputPayableDepositDirective.AddressId = "   ";
            viewModel = new PayableDepositViewModel(inputPayableDepositDirective);
            Assert.IsNull(viewModel.AddressId);

            inputPayableDepositDirective.AddressId = null;
            viewModel = new PayableDepositViewModel(inputPayableDepositDirective);
            Assert.IsNull(viewModel.AddressId);

            inputPayableDepositDirective.AddressId = "  foobar  ";
            viewModel = new PayableDepositViewModel(inputPayableDepositDirective);
            Assert.AreEqual("foobar", viewModel.AddressId);
        }

        [TestMethod]
        public void ConvertModelToPayableDepositDirectiveDtoTest()
        {
            viewModel = new PayableDepositViewModel(inputPayableDepositDirective);
            var payableDepositDto = viewModel.ConvertModelToPayableDepositDirectiveDtos();
            Assert.AreEqual(viewModel.Id, payableDepositDto.Id);
        }
    }
}