﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    public class BankingInformationTestData
    {
        public string personId;
        public BankingInformationConfiguration configurationDto;
        public List<PayrollDepositDirective> payrollDepositDirectives;
        public List<PayableDepositDirective> payableDepositDirectives;
        public List<Bank> bankDtos;
        public List<Address> personAddressDtos;

        public BankingInformationTestData(string personId)
        {
            this.personId = personId;
            BuildTestData();
        }

        public void BuildTestData()
        {
            #region Payroll Deposits
            payrollDepositDirectives = new List<PayrollDepositDirective>()
            {
                new PayrollDepositDirective()
                {
                    Id = "1",
                    PersonId = personId,
                    RoutingId = "091016647",
                    BankName = "First National Bank",
                    NewAccountId = null,
                    AccountIdLastFour = "8625",
                    Nickname = "My Checking Account",
                    BankAccountType = BankAccountType.Checking,
                    StartDate = new DateTime(2000,08,08), 
                    EndDate = new DateTime(2000,09,09),
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = false
                },
                new PayrollDepositDirective()
                {
                    Id = "2",
                    PersonId = personId,
                    RoutingId = "091016647",
                    BankName = "First National Bank",
                    NewAccountId = null,
                    AccountIdLastFour = "8625",
                    Nickname = "My Checking Account",
                    BankAccountType = BankAccountType.Checking,
                    DepositAmount = 500,
                    StartDate = new DateTime(2012,01,01), 
                    EndDate = new DateTime(2014,01,31),
                    Priority = 1,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = false
                },
                new PayrollDepositDirective()
                {
                    Id = "3",
                    PersonId = personId,
                    RoutingId = "091016647",
                    BankName = "First National Bank",
                    NewAccountId = null,
                    AccountIdLastFour = "8625",
                    Nickname = "My Checking Account",
                    BankAccountType = BankAccountType.Checking,
                    DepositAmount = 750,
                    StartDate = new DateTime(2014,02,01), 
                    EndDate = null,
                    Priority = 1,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = false
                },
                new PayrollDepositDirective()
                {
                    Id = "4",
                    PersonId = personId,
                    RoutingId = "091016647",
                    BankName = "First National Bank",
                    NewAccountId = null,
                    AccountIdLastFour = "8625",
                    Nickname = "My Checking Account",
                    BankAccountType = BankAccountType.Checking,
                    DepositAmount = 4,
                    StartDate = new DateTime(2016,04,01), 
                    EndDate = null,
                    Priority = 1,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = false
                },                    
                new PayrollDepositDirective()
                {
                    Id = "5",
                    PersonId = personId,
                    RoutingId = "091016647",
                    BankName = "First National Bank",
                    NewAccountId = null,
                    AccountIdLastFour = "8625",
                    Nickname = "My Checking Account",
                    BankAccountType = BankAccountType.Checking,
                    DepositAmount = 12,
                    StartDate = new DateTime(2016,03,01), 
                    EndDate = null,
                    Priority = 1,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = false
                },
                new PayrollDepositDirective()
                {
                    Id = "6",
                    PersonId = personId,
                    InstitutionId = "303",
                    BranchNumber = "55555",
                    BankName = "Amex Bank of Canada",
                    NewAccountId = null,
                    AccountIdLastFour = "8861",
                    Nickname = "Canadian Nickname",
                    BankAccountType = BankAccountType.Checking,
                    StartDate = new DateTime(2012,01,01), 
                    EndDate = new DateTime(2014,01,01), 
                    Priority = 999,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = true
                },
                new PayrollDepositDirective()
                {
                    Id = "7",
                    PersonId = personId,
                    InstitutionId = "303",
                    BranchNumber = "55555",
                    BankName = "Amex Bank of Canada",
                    NewAccountId = null,
                    AccountIdLastFour = "8861",
                    Nickname = "Canadian Nickname",
                    BankAccountType = BankAccountType.Checking,
                    DepositAmount = 800,
                    StartDate = new DateTime(2014,01,01), 
                    EndDate = null, 
                    Priority = 2,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = true
                },
                new PayrollDepositDirective()
                {
                    Id = "8",
                    PersonId = personId,
                    InstitutionId = "303",
                    BranchNumber = "55555",
                    BankName = "Amex Bank of Canada",
                    NewAccountId = null,
                    AccountIdLastFour = "8861",
                    Nickname = "Canadian Nickname",
                    BankAccountType = BankAccountType.Checking,
                    DepositAmount = 500,
                    StartDate = new DateTime(2016,02,01), 
                    EndDate = null, 
                    Priority = 33,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 1, 1)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 2, 1)),
                        ChangeOperator = "WEB"
                    },
                    IsVerified = true
                }
            };
            #endregion

            #region Bank
            payableDepositDirectives = new List<PayableDepositDirective>()
            {
                new PayableDepositDirective
                {
                    Id = "111",                    PayeeId = personId,                    RoutingId = "091016647",                    BranchNumber = null,
                    InstitutionId = null,                    BankName = "Great Bank of USA",                    BankAccountType = BankAccountType.Checking,                    Nickname = "My Checking Account",                     AccountIdLastFour = "8635",
                    AddressId = "",                    StartDate = new DateTime(2012, 01, 01),                    EndDate = new DateTime(2014, 01, 01),
                    IsElectronicPaymentRequested = true,
                    IsVerified = true,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 01, 01)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 02, 01)),
                        ChangeOperator = "WEB"
                    }
                },
                new PayableDepositDirective
                {
                    Id = "222",                    PayeeId = personId,                    RoutingId = "091016647",                    BankName = "Great Bank of USA",                    BankAccountType = BankAccountType.Checking,                    Nickname = "My Checking Account",                     AccountIdLastFour = "8635",
                    AddressId = null,                    StartDate = new DateTime(2012, 02, 02),                    EndDate = new DateTime(2014, 02, 02),
                    IsElectronicPaymentRequested = true,
                    IsVerified = true,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 01, 01)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 02, 01)),
                        ChangeOperator = "WEB"
                    }
                },
                new PayableDepositDirective
                {
                    Id = "333",                    PayeeId = personId,                    RoutingId = "091016647",                    BranchNumber = null,
                    InstitutionId = null,                    BankName = "Great Bank of USA",                    BankAccountType = BankAccountType.Checking,                    Nickname = "My Checking Account",                     AccountIdLastFour = "8635",
                    AddressId = " ",                    StartDate = new DateTime(2014, 02, 01),                    EndDate = null,
                    IsElectronicPaymentRequested = true,
                    IsVerified = true,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 01, 01)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 02, 01)),
                        ChangeOperator = "WEB"
                    }
                },
                new PayableDepositDirective
                {
                    Id = "444",                    PayeeId = personId,                    RoutingId = "091016647",                    BranchNumber = null,
                    InstitutionId = null,                    BankName = "Great Bank of USA",                    BankAccountType = BankAccountType.Checking,                    Nickname = "My Checking Account",                     AccountIdLastFour = "8635",
                    AddressId = "",                    StartDate = new DateTime(2017, 02, 01),                    EndDate = null,
                    IsElectronicPaymentRequested = true,
                    IsVerified = true,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 01, 01)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 02, 01)),
                        ChangeOperator = "WEB"
                    }
                },
                new PayableDepositDirective
                {
                    Id = "555",                    PayeeId = personId,                    RoutingId = "091016647",                    BranchNumber = null,
                    InstitutionId = null,                    BankName = "Great Bank of USA",                    BankAccountType = BankAccountType.Checking,                    Nickname = "My Checking Account",                     AccountIdLastFour = "8635",
                    AddressId = "",                    StartDate = new DateTime(2017, 01, 01),                    EndDate = null,
                    IsElectronicPaymentRequested = true,
                    IsVerified = true,
                    Timestamp = new Timestamp() 
                    {
                        AddDateTime = new DateTimeOffset(new DateTime(2015, 01, 01)),
                        AddOperator = "SYSTEM",
                        ChangeDateTime = new DateTimeOffset(new DateTime(2015, 02, 01)),
                        ChangeOperator = "WEB"
                    }
                }
            };
            #endregion

            #region Bank
            bankDtos = new List<Bank>() {
                new Bank()
                {
                    Id = "091016647",
                    Name = "First National Bank"
                },
                new Bank() 
                {
                    Id = "789456124",
                    Name = "Fake Bank"
                },
                new Bank()
                {
                    Id = "303",
                    Name = "Amex Bank of Canada"
                }
            };
            #endregion

            #region Configuration
            configurationDto = new BankingInformationConfiguration()
            {
                AddEditAccountTermsAndConditions = "Here are the terms and conditions",
                PayrollMessage = "A payroll message",
                PayrollEffectiveDateMessage = "A payroll message about the effective date",
                IsDirectDepositEnabled = true,
                IsPayableDepositEnabled = true,
                IsRemainderAccountRequired = true,
                UseFederalRoutingDirectory = true
            };
            #endregion

            #region PersonAddresses

            personAddressDtos = new List<Address>()
            {
                new Address() 
                {
                    AddressId = "1",
                    AddressLines = new List<string>() {"Line1", "Line2"}
                },
                new Address() 
                {
                    AddressId = "2",
                    AddressLines = new List<string>() {"I live here"}
                },
                new Address()
                {
                    AddressId = "3",
                    AddressLines = new List<string>() {"I no longer", "live here"}
                }
            };

            #endregion

        }


    }
}
