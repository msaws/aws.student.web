﻿using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.HumanResources.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class PayrollDepositViewModelTests
    {
        public PayrollDepositViewModel viewModel;
        public BankingInformationTestData testData;
        public PayrollDepositDirective inputPayrollDepositDirective;
        public string personId;

        public void PayrollDepositViewModelTestsInitialize()
        {
            personId = "0003914";
            testData = new BankingInformationTestData(personId);
            inputPayrollDepositDirective = testData.payrollDepositDirectives.First();
        }

        [TestInitialize]
        public void Initialize()
        {
            PayrollDepositViewModelTestsInitialize();
        }

        [TestMethod]
        public void DefaultConstructorTest()
        {
            viewModel = new PayrollDepositViewModel();
            Assert.IsNull(viewModel.DepositAmount);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PayrollDepositDirectiveRequiredTest()
        {
            viewModel = new PayrollDepositViewModel(null, null);
        }

        [TestMethod]
        public void ViewModelEqualsDirectiveTest()
        {
            viewModel = new PayrollDepositViewModel(inputPayrollDepositDirective, null);
            Assert.AreEqual(viewModel.Id, inputPayrollDepositDirective.Id);
        }

        [TestMethod]
        public void LargeDepositOrderIsSetTo999Test()
        {
            inputPayrollDepositDirective.Priority = 1000;
            viewModel = new PayrollDepositViewModel(inputPayrollDepositDirective, null);
            Assert.AreEqual(viewModel.DepositOrder, 999);
        }

        [TestMethod]
        public void DepositOrderIsSetByArgumentTest()
        {
            viewModel = new PayrollDepositViewModel(inputPayrollDepositDirective, 44);
            Assert.AreEqual(viewModel.DepositOrder, 44);
        }

        [TestMethod]
        public void ConvertModelToPayrollDepositDirectivesDtoTest()
        {
            viewModel = new PayrollDepositViewModel(inputPayrollDepositDirective, null);
            var payrollDepositDto = viewModel.ConvertModelToPayrollDepositDirectivesDto();
            Assert.AreEqual(viewModel.Id, payrollDepositDto.Id);
        }
    }
}
