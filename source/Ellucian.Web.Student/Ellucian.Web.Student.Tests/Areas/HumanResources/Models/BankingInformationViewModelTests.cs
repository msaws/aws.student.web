﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Student.Areas.HumanResources.Models;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class BankingInformationViewModelTests
    {
        public BankingInformationViewModel viewModel;
        public BankingInformationTestData testData;

        public string personId;

        public List<PayrollDepositDirective> inputPayrollDepositDirectives;
        public List<PayableDepositDirective> inputPayableDepositDirectives;
        public string inputAddressId;
        public BankingInformationConfiguration inputConfiguration;
        public List<Bank> inputBanks;
        public List<string> inputPermissionCodes;
        public List<Address> inputAddressDtos;
        public List<PayableDepositAddressViewModel> inputAddressViewModels;

        public void BankingInformationViewModelTestsInitialize()
        {
            personId = "0003914";
            inputConfiguration = new BankingInformationConfiguration()
            {
                IsDirectDepositEnabled = true,
                IsPayableDepositEnabled = true,
                IsRemainderAccountRequired = true,
            };
            inputPermissionCodes = new List<string>() 
            {
                BankingInformationPermissionCodes.EditPayrollBankingInformation.ToString(),
                BankingInformationPermissionCodes.EditEchecksBankingInformation.ToString()
            };
            inputPayrollDepositDirectives = new List<PayrollDepositDirective>();
            inputPayableDepositDirectives = new List<PayableDepositDirective>();
            inputAddressDtos = new List<Address>()
            {
                new Address() 
                { 
                    AddressId = "foo",
                    AddressLines = new List<string>() {"22 Foo st", "Burlington, VT"}
                }
            };
            inputAddressViewModels = new List<PayableDepositAddressViewModel>()
            {
                new PayableDepositAddressViewModel() 
                {
                    Id = "bar",
                    AddressLines = new List<string>() {"24 bar ave", "Reston, VA"}
                }
            };
            inputBanks = new List<Bank>();
            //testData = new BankingInformationTestData(personId);

            //inputDirectDeposits = testData.directDepositsDto;
            //inputPayableDepositAccounts = testData.personPayableDepositAccounts;
            //inputAddressId = "";
            //inputConfiguration = testData.configurationDto;
            //inputBanks = testData.bankDtos;
            //inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditEchecksBankingInformation, BankingInformationPermissionCodes.EditPayrollBankingInformation };
            //inputAddressDtos = testData.personAddressDtos;
            //inputAddressModels = testData.personAddressDtos.Select(dto => new PayableDepositAddressViewModel(dto)).ToList();

        }

        public BankingInformationViewModel BuildViewModel()
        {
            return new BankingInformationViewModel(personId, inputPayrollDepositDirectives, inputPayableDepositDirectives, inputAddressId, inputConfiguration, inputBanks, inputPermissionCodes, inputAddressDtos, inputAddressViewModels);
        }

        [TestClass]
        public class BankingInformationConstructor_UsingDtoDataTests : BankingInformationViewModelTests
        {

            [TestInitialize]
            public void Initialize()
            {
                BankingInformationViewModelTestsInitialize();

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PayeeIdRequiredTest()
            {
                personId = null;
                BuildViewModel();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ConfigurationRequiredTest()
            {
                inputConfiguration = null;
                BuildViewModel();
            }

            [TestMethod]
            public void PayeeIdTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual(personId, vm.PayeeId);
            }

            [TestMethod]
            public void ConfigurationTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual(inputConfiguration, vm.Configuration);
            }

            [TestMethod]
            public void IsPayrollActive_TrueTest()
            {
                inputConfiguration.IsDirectDepositEnabled = true;
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditPayrollBankingInformation };
                inputPayrollDepositDirectives = new List<PayrollDepositDirective>();

                var vm = BuildViewModel();
                Assert.IsTrue(vm.IsPayrollActive);
            }

            [TestMethod]
            public void IsPayrollActive_FalseTest()
            {
                inputConfiguration.IsDirectDepositEnabled = false;
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditPayrollBankingInformation };
                var vm = BuildViewModel();
                Assert.IsFalse(vm.IsPayrollActive);

                inputConfiguration.IsDirectDepositEnabled = true;
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditVendorBankingInformation };
                vm = BuildViewModel();
                Assert.IsFalse(vm.IsPayrollActive);

                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditEchecksBankingInformation };
                vm = BuildViewModel();
                Assert.IsFalse(vm.IsPayrollActive);

                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditPayrollBankingInformation };
                inputPayrollDepositDirectives = null;
                vm = BuildViewModel();
                Assert.IsFalse(vm.IsPayrollActive);
            }






            [TestMethod]
            public void IsPayableActive_NonVendor_TrueTest()
            {
                inputConfiguration.IsPayableDepositEnabled = true;
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditEchecksBankingInformation };
                viewModel = BuildViewModel();
                Assert.IsTrue(viewModel.IsPayableActive);
            }

            [TestMethod]
            public void IsPayableActive_Vendor_TrueTest()
            {
                inputConfiguration.IsPayableDepositEnabled = true;
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditVendorBankingInformation };
                viewModel = BuildViewModel();
                Assert.IsTrue(viewModel.IsPayableActive);
            }

            [TestMethod]
            public void IsPayableActive_EchecksDisabled_FalseTest()
            {
                inputConfiguration.IsPayableDepositEnabled = false;
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditEchecksBankingInformation };
                viewModel = BuildViewModel();
                Assert.IsFalse(viewModel.IsPayableActive);
            }

            [TestMethod]
            public void IsPayableActive_IncorrectPermissions_FalseTest()
            {
                inputConfiguration.IsPayableDepositEnabled = true;
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditPayrollBankingInformation };
                viewModel = BuildViewModel();
                Assert.IsFalse(viewModel.IsPayableActive);
            }


            [TestMethod]
            public void IsVendorTrueTest()
            {
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditVendorBankingInformation };
                var vm = BuildViewModel();
                Assert.IsTrue(vm.IsVendor);
            }

            [TestMethod]
            public void IsVendorFalseTest()
            {
                var vm = BuildViewModel();
                Assert.IsFalse(vm.IsVendor);
            }

            [TestMethod]
            public void DepositsInitializedTest()
            {
                var vm = BuildViewModel();
                Assert.IsNotNull(vm.ActivePayrollDeposits);
                Assert.IsNotNull(vm.PastPayrollDeposits);
                Assert.IsNotNull(vm.FuturePayrollDeposits);
                Assert.IsNull(vm.ActiveRefundDeposit);
                Assert.IsNotNull(vm.PastRefundDeposits);
                Assert.IsNotNull(vm.FutureRefundDeposits);
                Assert.IsNotNull(vm.Addresses);
            }

            [TestMethod]
            public void AddressIdNormalizedToNullTest()
            {
                inputAddressId = "  ";
                var vm = BuildViewModel();
                Assert.IsNull(vm.AddressId);
            }

            [TestMethod]
            public void AddressIdTest()
            {
                inputAddressId = "foobar";
                var vm = BuildViewModel();
                Assert.AreEqual(inputAddressId, vm.AddressId);
            }


            [TestMethod]
            public void AddressesContainsOnlyDefaultIfNotVendor()
            {
                var vm = BuildViewModel();
                Assert.IsFalse(vm.IsVendor);
                Assert.AreEqual(1, vm.Addresses.Count);
                Assert.IsNull(vm.Addresses[0].Id);
            }

            [TestMethod]
            public void AddressesFromDtosTest()
            {
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditVendorBankingInformation };
                var vm = BuildViewModel();
                Assert.IsTrue(vm.IsVendor);
                Assert.AreEqual(inputAddressDtos.Count + 1, vm.Addresses.Count);
                Assert.AreEqual(inputAddressDtos[0].AddressId, vm.Addresses[1].Id);
            }

            public void AddressesFromViewModelsTest()
            {
                inputPermissionCodes = new List<string>() { BankingInformationPermissionCodes.EditVendorBankingInformation };
                inputAddressDtos = null;
                var vm = BuildViewModel();
                Assert.IsTrue(vm.IsVendor);
                Assert.AreEqual(inputAddressViewModels.Count + 1, vm.Addresses.Count);
                Assert.AreEqual(inputAddressViewModels[0].Id, vm.Addresses[1].Id);
            }


        }

        [TestClass]
        public class PayrollDepositsTests : BankingInformationViewModelTests
        {
            [TestInitialize]
            public void Initialize()
            {
                BankingInformationViewModelTestsInitialize();

                var timestamp = new Timestamp()
                {
                    AddDateTime = DateTimeOffset.Now,
                    AddOperator = "me",
                    ChangeDateTime = DateTimeOffset.Now,
                    ChangeOperator = "me",
                };

                inputPayrollDepositDirectives = new List<PayrollDepositDirective>() 
                {
                    new PayrollDepositDirective() { //active
                        Id = "foo",
                        StartDate = DateTime.Today.AddDays(-1),
                        Priority = 1,
                        DepositAmount = 15,
                        Timestamp = timestamp,
                    },
                    new PayrollDepositDirective() { //active
                        Id = "bar",
                        StartDate = DateTime.Today.AddDays(-2),
                        Priority = 2,
                        DepositAmount = 10,
                        Timestamp = timestamp,
                    },
                    new PayrollDepositDirective() { //past
                        Id = "past1",
                        EndDate = DateTime.Today.AddDays(-1),
                        Timestamp = timestamp,
                    },
                    new PayrollDepositDirective() { //past
                        Id = "past2",
                        EndDate = DateTime.Today.AddDays(-2),
                        Timestamp = timestamp,
                    },
                    new PayrollDepositDirective() { //future
                        Id = "future1",
                        StartDate = DateTime.Today.AddDays(1),
                        Timestamp = timestamp,
                    },
                    new PayrollDepositDirective() {//future
                        Id = "future2",
                        StartDate = DateTime.Today.AddDays(2),
                        Timestamp = timestamp,
                    }
                };
            }

            [TestMethod]
            public void ActivePayrollDeposits_FilterTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual(2, vm.ActivePayrollDeposits.Count);
            }

            [TestMethod]
            public void ActivePayrollDepositsTest_OrderByPriorityTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual("foo", vm.ActivePayrollDeposits[0].Id);
                Assert.AreEqual("bar", vm.ActivePayrollDeposits[1].Id);
            }

            [TestMethod]
            public void ActivePayrollDepositsTest_ThenByStartDateTest()
            {
                inputPayrollDepositDirectives[0].Priority = 1;
                inputPayrollDepositDirectives[1].Priority = 1;
                var vm = BuildViewModel();
                Assert.AreEqual("bar", vm.ActivePayrollDeposits[0].Id);
                Assert.AreEqual("foo", vm.ActivePayrollDeposits[1].Id);
            }

            [TestMethod]
            public void ActivePayrollDepositsTest_ThenByDescendingDepositAmountTest()
            {
                inputPayrollDepositDirectives[0].Priority = 1;
                inputPayrollDepositDirectives[0].StartDate = DateTime.Today.AddDays(-2);
                inputPayrollDepositDirectives[1].Priority = 1;
                inputPayrollDepositDirectives[1].StartDate = DateTime.Today.AddDays(-2);

                var vm = BuildViewModel();
                Assert.AreEqual("foo", vm.ActivePayrollDeposits[0].Id);
                Assert.AreEqual("bar", vm.ActivePayrollDeposits[1].Id);
            }

            [TestMethod]
            public void ActivePayrollDeposits_DepositOrderIndexTest()
            {
                inputPayrollDepositDirectives[0].Priority = 55;
                inputPayrollDepositDirectives[1].Priority = 544;

                var vm = BuildViewModel();

                Assert.AreEqual(1, vm.ActivePayrollDeposits[0].DepositOrder);
                Assert.AreEqual(2, vm.ActivePayrollDeposits[1].DepositOrder);
            }

            [TestMethod]
            public void PastPayrollDepositsTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual(2, vm.PastPayrollDeposits.Count);
            }

            [TestMethod]
            public void PastPayrollDeposits_OrderByEndDateTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual("past1", vm.PastPayrollDeposits[0].Id);
                Assert.AreEqual("past2", vm.PastPayrollDeposits[1].Id);
            }

            [TestMethod]
            public void FuturePayrollDepositsTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual(2, vm.FuturePayrollDeposits.Count);
            }

            [TestMethod]
            public void FuturePayrollDepositsTest_OrderByStartDateTest()
            {
                var vm = BuildViewModel();
                Assert.AreEqual("future1", vm.FuturePayrollDeposits[0].Id);
                Assert.AreEqual("future2", vm.FuturePayrollDeposits[1].Id);
            }

        }


        [TestClass]
        public class PayableDepositsTests : BankingInformationViewModelTests
        {
            [TestInitialize]
            public void Initialize()
            {
                BankingInformationViewModelTestsInitialize();

                inputPayableDepositDirectives = new List<PayableDepositDirective>()
                {
                    new PayableDepositDirective() 
                    {
                        Id = "active1",
                        AddressId = null,
                        IsElectronicPaymentRequested = true,
                        StartDate = DateTime.Today.AddDays(-1),    
                        Timestamp = generateTimestamp()
                    }
                };
            }

            private Timestamp generateTimestamp()
            {
                return new Timestamp() 
                {
                    AddDateTime = DateTimeOffset.Now,
                    AddOperator = "me",
                    ChangeOperator = "me",
                    ChangeDateTime = DateTimeOffset.Now,
                };
            }
        }

        [TestClass]
        public class BankingInformationConstructor_DefaultTests : BankingInformationViewModelTests
        {
            [TestInitialize]
            public void Initialize()
            {
                BankingInformationViewModelTestsInitialize();
            }

            [TestMethod]
            public void DefaultConstructorTest()
            {
                viewModel = new BankingInformationViewModel();
                Assert.IsNull(viewModel.ActivePayrollDeposits);
            }
        }

        //[TestClass]
        //public class ConvertModelToPayableDepositDtoTest : BankingInformationViewModelTests
        //{
        //    [TestInitialize]
        //    public void Initialize()
        //    {
        //        BankingInformationViewModelTestsInitialize();
        //    }

        //    [TestMethod]
        //    public void ConvertActiveRefundDepositTest()
        //    {
        //        viewModel = BuildViewModel();
        //        var expectedBankAccount = viewModel.BankAccounts.First(b => b.Id == viewModel.ActiveRefundDeposit.BankId);
        //        viewModel.PastRefundDeposits = new List<PayableDepositViewModel>();
        //        viewModel.FutureRefundDeposits = new List<PayableDepositViewModel>();

        //        var actualDto = viewModel.ConvertModelToPayableDepositAccountDtos();

        //        Assert.AreEqual(1, actualDto.Count);
        //        Assert.AreEqual(personId, actualDto[0].PayeeId);
        //        Assert.AreEqual(expectedBankAccount.AccountId, actualDto[0].AccountId);
        //        Assert.AreEqual(expectedBankAccount.BankName, actualDto[0].BankName);
        //        Assert.AreEqual(expectedBankAccount.BranchNumber, actualDto[0].BranchNumber);
        //        Assert.AreEqual(expectedBankAccount.InstitutionId, actualDto[0].InstitutionId);
        //        Assert.AreEqual(expectedBankAccount.Nickname, actualDto[0].Nickname);
        //        Assert.AreEqual(expectedBankAccount.RoutingId, actualDto[0].RoutingId);
        //        Assert.AreEqual(expectedBankAccount.Type, actualDto[0].Type);
        //        Assert.AreEqual(1, actualDto[0].PayableDeposits.Count);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.AddressId, actualDto[0].PayableDeposits[0].AddressId);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.Id, actualDto[0].PayableDeposits[0].Id);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.StartDate, actualDto[0].PayableDeposits[0].StartDate);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.EndDate, actualDto[0].PayableDeposits[0].EndDate);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.IsElectronicPaymentRequested, actualDto[0].PayableDeposits[0].IsElectronicPaymentRequested);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.AddDateTime, actualDto[0].PayableDeposits[0].AddDateTime);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.ChangeDateTime, actualDto[0].PayableDeposits[0].ChangeDateTime);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.AddOperator, actualDto[0].PayableDeposits[0].AddOperator);
        //        Assert.AreEqual(viewModel.ActiveRefundDeposit.ChangeOperator, actualDto[0].PayableDeposits[0].ChangeOperator);
        //    }

        //    [TestMethod]
        //    public void ConvertActivePastFutureRefundDepositsTest()
        //    {
        //        viewModel = BuildViewModel();
        //        var expectedCount = 1 + viewModel.FutureRefundDeposits.Count + viewModel.PastRefundDeposits.Count;

        //        var actualDtos = viewModel.ConvertModelToPayableDepositAccountDtos();
        //        Assert.AreEqual(expectedCount, actualDtos.Count);
        //    }
        //}

        //[TestClass]
        //public class ConvertModelToDirectDepositDtoTests : BankingInformationViewModelTests
        //{
        //    public FunctionEqualityComparer<DirectDeposits> directDepositsDtoComparer;

        //    [TestInitialize]
        //    public void Initialize()
        //    {
        //        BankingInformationViewModelTestsInitialize();
        //        directDepositsDtoComparer = new FunctionEqualityComparer<DirectDeposits>((d1, d2) =>
        //            {
        //                if (d1.EmployeeId != d2.EmployeeId || d1.PayrollDepositAccounts.Count != d2.PayrollDepositAccounts.Count)
        //                {
        //                    return false;
        //                }

        //                foreach (var account1 in d1.PayrollDepositAccounts)
        //                {
        //                    var account2 = d2.PayrollDepositAccounts.FirstOrDefault(d => d.GetBankAccountIdentifierFromDto() == account1.GetBankAccountIdentifierFromDto());
        //                    if (account2 == null)
        //                    {
        //                        return false;
        //                    }
        //                    foreach (var deposit1 in account1.PayrollDeposits)
        //                    {
        //                        var deposit2 = account2.PayrollDeposits.FirstOrDefault(d =>
        //                            d.AddDateTime == deposit1.AddDateTime &&
        //                            d.DepositAmount == deposit1.DepositAmount &&
        //                            d.EndDate == deposit1.EndDate &&
        //                            d.StartDate == deposit1.StartDate &&
        //                            d.ChangeDateTime == deposit1.ChangeDateTime);
        //                        if (deposit2 == null)
        //                        {
        //                            return false;
        //                        }
        //                    }
        //                }
        //                return true;

        //            },
        //            d => d.PayrollDepositAccounts.SelectMany(acct => acct.PayrollDeposits).Aggregate(0, (prev, dep) => prev ^ dep.AddDateTime.GetHashCode()));
        //    }

        //    [Ignore]
        //    [TestMethod]
        //    public void NoChangesConversionTest()
        //    {
        //        var viewModel = BuildViewModel();
        //        var directDepositDto = viewModel.ConvertModelToDirectDepositsDto();

        //        Assert.IsTrue(directDepositsDtoComparer.Equals(testData.directDepositsDto, directDepositDto));
        //    }
        //}

        [TestClass]
        public class BankingInformationConfigurationTests : BankingInformationViewModelTests
        {
            public FunctionEqualityComparer<BankingInformationConfiguration> configurationComparer;
            [TestInitialize]
            public void Initialize()
            {
                BankingInformationViewModelTestsInitialize();
                configurationComparer = new FunctionEqualityComparer<BankingInformationConfiguration>((d1, d2) =>
                    {
                        if (d1.AddEditAccountTermsAndConditions == d2.AddEditAccountTermsAndConditions &&
                            d1.PayrollMessage == d2.PayrollMessage &&
                            d1.PayrollEffectiveDateMessage == d2.PayrollEffectiveDateMessage &&
                            d1.IsDirectDepositEnabled == d2.IsDirectDepositEnabled &&
                            d1.IsDirectDepositEnabled == d2.IsDirectDepositEnabled &&
                            d1.IsPayableDepositEnabled == d2.IsPayableDepositEnabled &&
                            d1.IsRemainderAccountRequired == d2.IsRemainderAccountRequired)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    },
                    c => c.GetHashCode());
            }

            //[TestMethod]
            //public void NoChangesConversionTest()
            //{
            //    var viewModel = BuildViewModel();
            //    var configurationDto = viewModel.Configuration;

            //    Assert.IsTrue(configurationComparer.Equals(testData.configurationDto, configurationDto));
            //}

            // strings are cleaned tests
            //[TestMethod]
            //public void StuffOutsideHTMLTagIsRemovedTest()
            //{
            //    var actualHtmlContent = "<hTmL>Four score and seven years ago</HtMl>";
            //    inputConfiguration.PayrollEffectiveDateMessage = string.Format("{0}{1}{2}", "blah blah", actualHtmlContent, " woahaha");
            //    viewModel = BuildViewModel();
            //    Assert.AreEqual(actualHtmlContent, viewModel.Configuration.PayrollEffectiveDateMessage);
            //}

            //[TestMethod] 
            //public void StuffOutsideBodyTagIsRemovedTest()
            //{
            //    var actualBodyContent = "<bOdY>Four score and eight years ago</boDy>";
            //    inputConfiguration.PayrollEffectiveDateMessage = string.Format("{0}{1}{2}", "blah blah", actualBodyContent, "");
            //    viewModel = BuildViewModel();
            //    Assert.AreEqual(actualBodyContent, viewModel.Configuration.PayrollEffectiveDateMessage);
            //}

            //[TestMethod]
            //public void ActualContentIsGottenForFrontPartialTagTest()
            //{
            //    var actualPartialContent = "<bOdY>Four score and eight years ago";
            //    inputConfiguration.PayrollEffectiveDateMessage = string.Format("{0}{1}", "blah blah ", actualPartialContent);
            //    viewModel = BuildViewModel();
            //    Assert.AreEqual(actualPartialContent, viewModel.Configuration.PayrollEffectiveDateMessage);
            //}

            //[TestMethod]
            //public void ActualContentIsGottenForEndPartialTagTest()
            //{
            //    var actualPartialContent = "Four score and eight years ago</boDy>";
            //    inputConfiguration.PayrollEffectiveDateMessage = string.Format("{0}{1}", actualPartialContent, "yello");
            //    viewModel = BuildViewModel();
            //    Assert.AreEqual(actualPartialContent, viewModel.Configuration.PayrollEffectiveDateMessage);
            //}

            //[TestMethod]
            //[ExpectedException(typeof(Exception))]
            //public void ScriptTagCreatesExceptionTest()
            //{
            //    var actualContent = "Four score and seven years ago";
            //    inputConfiguration.PayrollEffectiveDateMessage = string.Format("{0}{1}{2}", "blah blah <scriPT>", actualContent, "</ScRipt> woahaha");
            //    viewModel = BuildViewModel();
            //}
        }
    }
}

