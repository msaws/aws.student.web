﻿using Ellucian.Web.Student.Areas.HumanResources.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class BankingInformationPermissionCodesTests
    {
        public List<string> userPermissionCodes;

        [TestInitialize]
        public void Initialize()
        {
            userPermissionCodes = new List<string>() { 
                BankingInformationPermissionCodes.EditEchecksBankingInformation, 
                BankingInformationPermissionCodes.EditPayrollBankingInformation, 
                BankingInformationPermissionCodes.EditVendorBankingInformation };
        }

        [TestMethod]
        public void HasPayablePermission_TrueTest()
        {
            Assert.IsTrue(userPermissionCodes.HasEditPayablePermission());
        }

        [TestMethod]
        public void HasPayablePermission_FalseTest()
        {
            userPermissionCodes.Remove(BankingInformationPermissionCodes.EditEchecksBankingInformation);
            Assert.IsFalse(userPermissionCodes.HasEditPayablePermission());
        }

        [TestMethod]
        public void HasPayablePermission_NullCodes_FalseTest()
        {
            userPermissionCodes = null;
            Assert.IsFalse(userPermissionCodes.HasEditPayablePermission());
        }

        [TestMethod]
        public void HasPayrollPermission_TrueTest()
        {
            Assert.IsTrue(userPermissionCodes.HasEditPayrollPermission());
        }

        [TestMethod]
        public void HasPayrollPermission_FalseTest()
        {
            userPermissionCodes.Remove(BankingInformationPermissionCodes.EditPayrollBankingInformation);
            Assert.IsFalse(userPermissionCodes.HasEditPayrollPermission());
        }

        [TestMethod]
        public void HasPayrollPermission_NullCodes_FalseTest()
        {
            userPermissionCodes = null;
            Assert.IsFalse(userPermissionCodes.HasEditPayrollPermission());
        }

        [TestMethod]
        public void HasVendorPermission_TrueTest()
        {
            Assert.IsTrue(userPermissionCodes.HasEditVendorPermission());
        }

        [TestMethod]
        public void HasVendorPermission_FalseTest()
        {
            userPermissionCodes.Remove(BankingInformationPermissionCodes.EditVendorBankingInformation);
            Assert.IsFalse(userPermissionCodes.HasEditVendorPermission());
        }

        [TestMethod]
        public void HasVendorPermission_NullCodes_FalseTest()
        {
            userPermissionCodes = null;
            Assert.IsFalse(userPermissionCodes.HasEditVendorPermission());
        }
    }
}
