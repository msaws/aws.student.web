﻿using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Student.Areas.HumanResources.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Models
{
    [TestClass]
    public class PayableDepositAddressViewModelTests
    {
        public BankingInformationTestData testData;

        public string personId;

        public Address inputAddressDto;

        public void PayableDepositAddressViewModelTestsInitialize()
        {
            personId = "0003914";
            testData = new BankingInformationTestData(personId);
            inputAddressDto = testData.personAddressDtos[0];
        }

        public PayableDepositAddressViewModel BuildViewModel()
        {
            return new PayableDepositAddressViewModel(inputAddressDto);
        }

        [TestClass]
        public class EqualsTests : PayableDepositAddressViewModelTests
        {
            [TestInitialize]
            public void Initialize()
            {
                PayableDepositAddressViewModelTestsInitialize();
            }

            [TestMethod]
            public void IDsAreEqual_EqualTrue()
            {
                var one = BuildViewModel();
                var two = new PayableDepositAddressViewModel() { Id = one.Id };

                Assert.IsTrue(one.Equals(two));
                Assert.AreEqual(one.GetHashCode(), two.GetHashCode());
            }


            [TestMethod]
            public void IDsAreNotEqual_EqualFalse()
            {
                var one = BuildViewModel();
                inputAddressDto.AddressId = "foobar";
                var two = BuildViewModel();

                Assert.IsFalse(one.Equals(two));
                Assert.AreNotEqual(one.GetHashCode(), two.GetHashCode());
            }



            [TestMethod]
            public void InputIsNull_EqualFalse()
            {
                var one = BuildViewModel();

                Assert.IsFalse(one.Equals(null));
            }

            [TestMethod]
            public void InputIsOtherType_EqualFalse()
            {
                var one = BuildViewModel();
                var two = BankingInformationPermissionCodes.EditEchecksBankingInformation;

                Assert.IsFalse(one.Equals(two));

            }
        }

        [TestClass]
        public class ConstructorWithDtosTests : PayableDepositAddressViewModelTests
        {

            PayableDepositAddressViewModel viewModel;
           
            [TestInitialize]
            public void Initialize()
            {
                PayableDepositAddressViewModelTestsInitialize();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void AddressDtoRequiredTest()
            {
                inputAddressDto = null;
                BuildViewModel();
            }

            [TestMethod]
            public void AddressIdTest()
            {
                viewModel = BuildViewModel();
                Assert.AreEqual(inputAddressDto.AddressId, viewModel.Id);
            }

            [TestMethod]
            public void TidyAddressIdTest()
            {
                inputAddressDto.AddressId = null;
                viewModel = BuildViewModel();
                Assert.AreEqual(null, viewModel.Id);

                inputAddressDto.AddressId = "  foobar ";
                viewModel = BuildViewModel();
                Assert.AreEqual("foobar", viewModel.Id);
            }

            [TestMethod]
            public void AddressLinesTest()
            {
                viewModel = BuildViewModel();
                CollectionAssert.AreEqual(inputAddressDto.AddressLines, viewModel.AddressLines);
            }
        }

        [TestClass]
        public class DefaultConstructorTests : PayableDepositAddressViewModelTests
        {
            PayableDepositAddressViewModel viewModel;

            [TestInitialize]
            public void Initialize()
            {
                PayableDepositAddressViewModelTestsInitialize();
                viewModel = new PayableDepositAddressViewModel();
            }

            [TestMethod]
            public void AddressIdTest()
            {
                Assert.AreEqual(null, viewModel.Id);
            }

            [TestMethod]
            public void AddressLinesTest()
            {
                Assert.AreEqual(1, viewModel.AddressLines.Count);
                Assert.AreEqual("Default Account", viewModel.AddressLines[0]);
            }

        }
    }
}
