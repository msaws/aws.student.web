
var testData = require('./banking.information.test.data');
var component = require('BankingInformation/ActivePayableDirective/_ActivePayableDirective.js');

describe('Test Suite for activePayableDirective component', function () {

    let params = null;
    let ViewModel = component.viewModel;
    beforeEach(function () {
        params = {
            dataModel: {
                ActiveRefundDeposit: ko.observable({}),
                AddressId: ko.observable('europa')
            },
            hasAllCanadianAccounts: ko.computed(function () { return true; }),
        }
    });

describe('self.dataModel ', function () {
    let viewModel;
    beforeEach(function() {
        viewModel = new ViewModel(params);
    });
    it('should do initialize', function() {
        expect(viewModel).not.toBe(null);
        expect(viewModel.ActiveRefundDeposit).toEqual(params.dataModel.ActiveRefundDeposit());

        expect(viewModel.hasAllCanadianAccounts()).toEqual(params.hasAllCanadianAccounts());
    });
});
describe('self.viewAllPayableHref ', function () {
    let viewModel;
    beforeEach(function() {
        viewModel = new ViewModel(params);
    });
    it('should format correctly when the addressId is around', function() {
        expect(viewModel.viewAllPayableHref()).toEqual('#viewAllPayable&addressId=' + params.dataModel.AddressId());
    });
    it('should format correctly when the addressId is not around', function() {
        params.dataModel.AddressId(null);
        viewModel = new ViewModel(params);
        expect(viewModel.viewAllPayableHref()).toEqual('#viewAllPayable');
    });
});

describe('self.hasActivePayableDeposit ', function () {
    let viewModel;
    beforeEach(function() {
        viewModel = new ViewModel(params);
    });
    it('return true for an existant active payable deposit', function() {
        expect(viewModel.hasActivePayableDeposit()).toBe(true);
    });
    it('return false for an non-existant active payable deposit', function() {
        params.dataModel.ActiveRefundDeposit(null);
        viewModel = new ViewModel(params);
        expect(viewModel.hasActivePayableDeposit()).toBe(false);
    });
});

});
/*
    describe('self.noActivePayableDepositMessage ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
*/  