
var testData = require('./banking.information.test.data');
var component = require('BankingInformation/ActivePayrollDirectives/_ActivePayrollDirectives.js');

describe('Test Suite for ActivePayrollDirective component', function () {
    let params = null;
    let ViewModel = component.viewModel;

    beforeEach(function () {
        params = {
            dataModel: {
                ActivePayrollDeposits: ko.observableArray([
                    {
                        Id: ko.observable('213'),
                        PersonId: ko.observable('0014076'),
                        RoutingId: ko.observable('011000028'),
                        InstitutionId: ko.observable(null),
                        BranchNumber: ko.observable(null),
                        BankName: ko.observable('New Bank of Montpelier'),
                        NewAccountId: ko.observable(null),
                        AccountIdLastFour: ko.observable('7777'),
                        Nickname: ko.observable('nnnnn'),
                        Type: ko.observable(0),
                        SecurityToken: ko.observable(null),
                        DepositAmount: ko.observable(111),
                        StartDate: ko.observable('2017-04-14T04:00:00.000Z'),
                        EndDate: ko.observable(null),
                        OriginalDepositOrder: ko.observable(1),
                        DepositOrder: ko.observable(1),
                        AddDateTime: ko.observable('2017-04-14T16:14:20.000Z'),
                        ChangeDateTime: ko.observable('2017-04-19T20:34:42.000Z'),
                        AddOperator: ko.observable('0014076'),
                        ChangeOperator: ko.observable('0014076'),
                        IsVerified: ko.observable(false)
                    }
                ])
            },
            type: {},
            hasAllCanadianAccounts: ko.computed(function () { return true; })
        }
    });



    describe('self.dataModel ', function () {
        let viewModel;
        beforeEach(function () {
            viewModel = new ViewModel(params);
        });
        it('should construct properly', function () {
            expect(viewModel.type).toEqual(params.type);
            expect(viewModel.ActivePayrollDeposits).toEqual(params.dataModel.ActivePayrollDeposits);
            expect(viewModel.hasAllCanadianAccounts()).toEqual(params.hasAllCanadianAccounts());
        });
    });


    describe('self.hasActivePayrollDeposits ', function () {
        let viewModel;
        beforeEach(function () {
            viewModel = new ViewModel(params);
        });
        it('return false when active payroll deposits are not there', function () {
            viewModel.ActivePayrollDeposits([]);
            expect(viewModel.hasActivePayrollDeposits()).toBe(false);
        });
        it('return treu when active payroll deposits are tjere', function () {
            expect(viewModel.hasActivePayrollDeposits()).toBe(true);
        });
    });

    describe('self.activePayrollRemainderDeposit ', function () {
        let viewModel;
        beforeEach(function () {
            viewModel = new ViewModel(params);
        });
        it('should return deposit with priority 999', function () {
            params.dataModel.ActivePayrollDeposits = ko.observableArray([{ DepositOrder: ko.observable(999) }])
            viewModel = new ViewModel(params);
            expect(viewModel.activePayrollRemainderDeposit().DepositOrder()).toEqual(999);
        });
    });
    describe('self.hasActivePayrollRemainderDeposit ', function () {
        let viewModel;
        beforeEach(function () {
            viewModel = new ViewModel(params);
        });
        it('should return true if active payroll remainder deposit is had', function () {
            params.dataModel.ActivePayrollDeposits = ko.observableArray([{ DepositOrder: ko.observable(999) }])
            viewModel = new ViewModel(params);
            expect(viewModel.hasActivePayrollRemainderDeposit()).toBe(true);
        });
        it('should return false if active payroll remainder deposit is probably not had', function () {
            params.dataModel.ActivePayrollDeposits = ko.observableArray([{ DepositOrder: ko.observable(998) }])
            viewModel = new ViewModel(params);
            expect(viewModel.hasActivePayrollRemainderDeposit()).toBe(false);
        });
    });
});
/*
describe('self.noActivePayrollDepositsMessage ', function () {
let viewModel;
beforeEach(function() {
viewModel = new ViewModel(params);
});
afterEach(function() {
viewModel.dispose();
});
it('should do something', function() {
// test code here...
});
});
describe('self.editPayrollDepositDirective ', function () {
let viewModel;
beforeEach(function() {
viewModel = new ViewModel(params);
});
afterEach(function() {
viewModel.dispose();
});
it('should do something', function() {
// test code here...
});
});

describe('self.noPayrollRemainderDepositMessage ', function () {
let viewModel;
beforeEach(function() {
viewModel = new ViewModel(params);
});
afterEach(function() {
viewModel.dispose();
});
it('should do something', function() {
// test code here...
});
});
describe('self.headerLabel ', function () {
let viewModel;
beforeEach(function() {
viewModel = new ViewModel(params);
});
afterEach(function() {
viewModel.dispose();
});
it('should do something', function() {
// test code here...
});
});

*/
