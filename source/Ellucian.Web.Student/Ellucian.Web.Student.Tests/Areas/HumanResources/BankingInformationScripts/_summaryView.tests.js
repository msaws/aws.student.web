var testData = require('./banking.information.test.data');
var component = require('BankingInformation/SummaryViewComponent/_SummaryView.js');
var actions = require('BankingInformation/banking.information.actions');
describe('Test Suite for SummaryView component', function () {
    let params = null;
    let ViewModel = component.viewModel;
    beforeEach(function () {
        params = {
            type: 'payable',
            addressId: '57975'
        };
        spyOn(actions, "getBankingInformationViewModelAsync").and.returnValue(Promise.resolve(testData.dataModel));
    });

    describe('initialization', function () {
        let viewModel;
        beforeEach(function () {
            viewModel = new ViewModel(params);
        });
        it('should set properties', function () {
            expect(viewModel.type).toEqual(params.type);
            expect(viewModel.dataModel).toEqual({});
            expect(viewModel.isLoading()).toBe(true);
            expect(viewModel.loadError()).toBe(false);
        });
    });


    describe('self.dataModel ', function () {
        let viewModel,
            getActionPromise;
        beforeEach(function () {
            //spyOn($, 'ajax').and.callFake(function () { return testData.viewModel });
            
            viewModel = new ViewModel(params);
        });
        it('apply the data model when loaded', function (done) {

            
            viewModel.loadDataPromise.then(function () {
                expect(viewModel.dataModel).not.toEqual({});
                expect(viewModel.loadError()).toBeFalsy();
                expect(viewModel.isLoading()).toBeFalsy();
                done();
            });
            
        });
    });
    describe('self.back', function () {
        beforeEach(function () {
            spyOn(window.history, 'back');
            viewModel = new ViewModel(params);
        });
        it('should pop window history', function () {
            viewModel.back();
            expect(window.history.back).toHaveBeenCalled();
        });
    });
});
