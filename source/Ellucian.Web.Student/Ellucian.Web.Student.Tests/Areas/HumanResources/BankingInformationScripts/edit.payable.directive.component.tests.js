﻿var testData = require('./banking.information.test.data');
var component = require("BankingInformation/EditPayableDirective/_EditPayableDirective");
var actions = require("BankingInformation/banking.information.actions");


describe("Banking Information Test Suite for EditPayableDirective", function () {

    var viewModel,
        params;

    beforeEach(function () {
        params = {
            dataModel: ko.mapping.fromJS(Object.assign(testData.dataModel)),
            id: testData.dataModel.ActiveRefundDeposit.Id,
        }


    });

    var setViewModel = function () {
        if (viewModel != null) {
            viewModel.dispose();
        }
        viewModel = new component.viewModel(params);
    }

    afterEach(function () {
        viewModel.dispose();
        viewModel = null;
    });

    describe("Has All Canadian Accounts", function () {

        beforeEach(function () {
            setViewModel();
        });

        it("should return false if any account has a routing id", function () {

            //assuming test data has one active deposit with a routing id
            params.dataModel.ActiveRefundDeposit.RoutingId("091000019");
            expect(viewModel.hasAllCanadianAccounts()).toBeFalsy();
        });

        xit("should return true if no accounts have a routing id", function () {
            params.dataModel.ActiveRefundDeposit.RoutingId(null).InstitutionId("123").BranchNumber("12345");
            expect(viewModel.hasAllCanadianAccounts()).toBeTruthy();
        })
    });

    describe("Determining the directive to edit", function () {
        beforeEach(function () {

        });

        it("should choose directive from params.Id", function () {
            setViewModel();
            expect(viewModel.editingDirectiveViewModel.Id()).toEqual(params.id);
        });

        it("should be invalid if params.Id is foobar", function () {
            params.id = "foobar";
            setViewModel();
            expect(viewModel.invalidId()).toBeTruthy();
            expect(viewModel.editingDirectiveViewModel).toBeNull();
        });

        it("should be invalid if directive is in the past", function () {
            //id is valid, but end date is before today
            params.dataModel.ActiveRefundDeposit.EndDate((new Date()).addDays(-1));
            setViewModel();
            expect(viewModel.invalidId()).toBeTruthy();
            expect(viewModel.editingDirectiveViewModel).toBeNull();
        });
    });

    describe("Mini test suite for EditingDirectiveViewModel", function () {

        var directiveViewModel;

        beforeEach(function () {
            setViewModel();
            directiveViewModel = viewModel.editingDirectiveViewModel;
        });

        describe("Display Bank Name", function () {
            it("should display the nickname", function () {
                params.dataModel.ActiveRefundDeposit.Nickname("foo").BankName("bar");
                expect(directiveViewModel.displayBankName()).toEqual("foo");
            });

            it("should display the bankname", function () {
                params.dataModel.ActiveRefundDeposit.Nickname(null).BankName("bar");
                expect(directiveViewModel.displayBankName()).toEqual("bar");
            });
        });

        describe("Display Last Four Account Number", function () {
            it("should display the account number prefixed by an elipse", function () {
                params.dataModel.ActiveRefundDeposit.AccountIdLastFour("foob");
                expect(directiveViewModel.displayBankAccountLastFour()).toEqual("...foob");
            });
        });

        describe("IsSwitchedOn", function () {
            it("should be initialized to true", function () {
                viewModel.dispose();
                params.dataModel.ActiveRefundDeposit.IsElectronicPaymentRequested(true);
                setViewModel();
                directiveViewModel = viewModel.editingDirectiveViewModel;
                expect(directiveViewModel.isSwitchedOn()).toBeTruthy();
            });
            it("should be initialized to false", function () {
                viewModel.dispose();
                params.dataModel.ActiveRefundDeposit.IsElectronicPaymentRequested(false);
                setViewModel();
                directiveViewModel = viewModel.editingDirectiveViewModel;
                expect(directiveViewModel.isSwitchedOn()).toBeFalsy();
            });
        });

        describe("Directive Start Date", function () {
            it("should be initialized to start date of editing directive", function () {
                expect(directiveViewModel.startDateBacker()).toEqual(params.dataModel.ActiveRefundDeposit.StartDate())
            });

            it("should be read-only if before today", function () {
                expect(directiveViewModel.startDateIsReadOnly()).toBeTruthy();
            });

            it("should be editable if in the future", function () {
                directiveViewModel.startDateBacker((new Date()).addDays(1));
                expect(directiveViewModel.startDateIsReadOnly()).toBeFalsy();
            });
        });

        describe("Account Details sub model", function () {
            it("should be initialized appropriately", function () {
                var expected = params.dataModel.ActiveRefundDeposit;
                var actual = directiveViewModel.accountDetailsModel;
                expect(actual.accountIdLast4()).toEqual(expected.AccountIdLastFour());
                expect(actual.nickname()).toEqual(expected.Nickname());
                expect(actual.bankName()).toEqual(expected.BankName());
                expect(actual.routingId()).toEqual(expected.RoutingId());
                expect(actual.institutionId()).toEqual(expected.InstitutionId());
                expect(actual.branchNumber()).toEqual(expected.BranchNumber());
                expect(actual.newAccountId()).toBeNull();
                expect(actual.accountType()).toEqual(expected.Type());
                expect(actual.isVerified()).toEqual(expected.IsVerified());

                expect(directiveViewModel.isBankAccountDetailsInputValid()).toBeTruthy();
            });

            it("should hide modal on initialize", function () {
                expect(directiveViewModel.showEditBankAccountDetails()).toBeFalsy();
            });

            it("should switch to edit mode on edit function call", function () {
                directiveViewModel.editDirectiveBankAccount();
                expect(directiveViewModel.showEditBankAccountDetails()).toBeTruthy();
            });

            it("should switch to hidden mode on cancel function call", function () {
                directiveViewModel.editDirectiveBankAccount();
                directiveViewModel.cancelEditBankAccountDetails();
                expect(directiveViewModel.showEditBankAccountDetails()).toBeFalsy();
            });

            it("should reset account details view model on cancel", function () {
                directiveViewModel.accountDetailsModel
                    .accountIdLast4("foob")
                    .nickname("foobar")
                    .bankName("foo")
                    .routingId("bar")
                    .newAccountId("12345")
                    .accountType("foo")
                    .isVerified("bar");

                directiveViewModel.cancelEditBankAccountDetails();

                var expected = params.dataModel.ActiveRefundDeposit;
                var actual = directiveViewModel.accountDetailsModel;
                expect(actual.accountIdLast4()).toEqual(expected.AccountIdLastFour());
                expect(actual.nickname()).toEqual(expected.Nickname());
                expect(actual.bankName()).toEqual(expected.BankName());
                expect(actual.routingId()).toEqual(expected.RoutingId());
                expect(actual.institutionId()).toEqual(expected.InstitutionId());
                expect(actual.branchNumber()).toEqual(expected.BranchNumber());
                expect(actual.newAccountId()).toBeNull();
                expect(actual.accountType()).toEqual(expected.Type());
                expect(actual.isVerified()).toEqual(expected.IsVerified());
            });
        });

        describe("Saving account details", function () {

            let savePromiseReturnValue;
            let actualSavedDirective;

            beforeEach(function () {
                savePromiseReturnValue = Promise.resolve();
                spyOn(actions, "updatePayableDepositDirectiveAsync").and.callFake(function (directive) {
                    actualSavedDirective = directive;
                    return savePromiseReturnValue;
                })
            });

            afterEach(function () {
                actualSavedDirective = null;
                savePromiseReturnValue = null;
            })

            it("should initialize save notification to false", function () {
                expect(directiveViewModel.isSavingBankAccountDetailsInProgress()).toBeFalsy();
            })

            it("should not save if input is invalid", function (done) {
                directiveViewModel.isBankAccountDetailsInputValid(false);
                directiveViewModel.saveBankAccountDetails().then(function () {
                    expect(actions.updatePayableDepositDirectiveAsync).not.toHaveBeenCalled();
                    done();
                })
            });

            it("should notify that saving is taking place", function (done) {
                directiveViewModel.editDirectiveBankAccount();
                directiveViewModel.saveBankAccountDetails().then(done);
                expect(directiveViewModel.showEditBankAccountDetails()).toBeFalsy();
                expect(directiveViewModel.isSavingBankAccountDetailsInProgress()).toBeTruthy();

            });

            it("should set the directive properties to the values of the account details model", function (done) {

                directiveViewModel.accountDetailsModel
                    .accountIdLast4("foob")
                    .nickname("foobar")
                    .bankName("foo")
                    .routingId("bar")
                    .newAccountId("12345")
                    .accountType("foo")
                    .isVerified("bar");



                directiveViewModel.saveBankAccountDetails().then(done);

                var actual = actualSavedDirective;
                var expected = directiveViewModel.accountDetailsModel;
                expect(actual.AccountIdLastFour).toEqual(expected.accountIdLast4());
                expect(actual.Nickname).toEqual(expected.nickname());
                expect(actual.BankName).toEqual(expected.bankName());
                expect(actual.RoutingId).toEqual(expected.routingId());
                expect(actual.NewAccountId).toEqual(expected.newAccountId());
                expect(actual.Type).toEqual(expected.accountType());
                expect(actual.IsVerified).toBeFalsy(); //except for IsVerified which stays false
            });

            it("should stop saving on success", function (done) {

                directiveViewModel.saveBankAccountDetails().then(function () {
                    expect(directiveViewModel.isSavingBankAccountDetailsInProgress()).toBeFalsy();
                    done();
                });
            });

            it("should do cleanup on error", function (done) {
                savePromiseReturnValue = Promise.reject("foobar");
                spyOn(console, "error");
                spyOn($.fn, "notificationCenter");


                directiveViewModel.saveBankAccountDetails().then(function () {
                    expect(console.error).toHaveBeenCalledWith("foobar");
                    expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", jasmine.objectContaining({
                        type: 'error',
                        flash: true
                    }));
                    expect(directiveViewModel.isDeleteInProgress()).toBeFalsy();
                    done();
                });

            });
        });

        describe("viewmodel input errors", function () {
            it("should be false", function () {
                directiveViewModel.startDateErrorMessage(null);
                expect(directiveViewModel.hasInputErrors()).toBeFalsy();
            });
            it("should be true", function () {
                directiveViewModel.startDateErrorMessage("foobar");
                expect(directiveViewModel.hasInputErrors()).toBeTruthy();
            });
        });

        describe("save enabled", function () {
            it("should be true when start date is read only", function () {
                directiveViewModel.startDateBacker((new Date()).addDays(-1));
                directiveViewModel.startDateErrorMessage(null);
                expect(directiveViewModel.saveEnabled()).toBeTruthy();
            });

            it("should be true when there are no input errors", function () {
                directiveViewModel.startDateBacker((new Date()).addDays(1));
                directiveViewModel.startDateErrorMessage(null);
                expect(directiveViewModel.saveEnabled()).toBeTruthy();

            });

            it("should be false when there are input errors", function () {
                directiveViewModel.startDateBacker((new Date()).addDays(1));
                directiveViewModel.startDateErrorMessage("foobar");
                expect(directiveViewModel.saveEnabled()).toBeFalsy();
            });

        });

        describe("saving the entire directive", function () {

            let savePromiseReturnValue;
            let actualSavedDirective;

            beforeEach(function () {
                savePromiseReturnValue = Promise.resolve();
                spyOn(actions, "updatePayableDepositDirectiveAsync").and.callFake(function (directive) {
                    actualSavedDirective = directive;
                    return savePromiseReturnValue;
                });

                spyOn(directiveViewModel, "back");
            });

            afterEach(function () {
                actualSavedDirective = null;
                savePromiseReturnValue = null;
            })

            it("should initialize the notifier to false", function () {
                expect(directiveViewModel.isSaveDirectiveInProgress()).toBeFalsy();
            });

            it("should prepare for saving", function (done) {
                directiveViewModel.saveAsync().then(done);
                expect(directiveViewModel.isSaveDirectiveInProgress()).toBeTruthy();
            });

            it("should set directive properties", function (done) {
                directiveViewModel.startDateBacker(new Date(2017, 3, 20));
                directiveViewModel.saveAsync().then(done);

                expect(actualSavedDirective).toEqual(jasmine.objectContaining({
                    StartDate: directiveViewModel.startDateBacker()
                }));
            });

            it("should stop saving on success", function (done) {
                directiveViewModel.saveAsync().then(function () {
                    expect(directiveViewModel.isSaveDirectiveInProgress()).toBeFalsy();
                    expect(directiveViewModel.back).toHaveBeenCalled();
                    done();
                });
            });

            it("should do cleanup on error", function (done) {
                savePromiseReturnValue = Promise.reject("foobar");

                spyOn(console, "error");
                spyOn($.fn, "notificationCenter");

                directiveViewModel.saveAsync().then(function () {
                    expect(console.error).toHaveBeenCalledWith("foobar");
                    expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", jasmine.objectContaining({
                        type: 'error',
                        flash: true
                    }));
                    expect(directiveViewModel.isSaveDirectiveInProgress()).toBeFalsy();
                    expect(directiveViewModel.back).toHaveBeenCalled();
                    done();
                });
            });


        });

        describe("delete action", function () {
            var deletePromiseReturnValue;
            var actualDeletedDirective;

            beforeEach(function () {
                deletePromiseReturnValue = Promise.resolve();
                spyOn(actions, "deletePayableDepositDirectiveAsync").and.callFake(function (directive) {
                    actualDeletedDirective = directive;
                    return deletePromiseReturnValue;
                });

                spyOn(directiveViewModel, "back");
            });
            xit("should be enabled when directive is future dated", function () {
                params.id = testData.dataModel.FutureRefundDeposits[0].Id;
                setViewModel();

                expect(directiveViewModel.deleteButtonEnabled()).toBeTruthy();
            });

            it("should be disabled when directive is active", function () {
                expect(directiveViewModel.deleteButtonEnabled()).toBeFalsy();
            });

            it("should initialize notifier to false", function () {
                expect(directiveViewModel.isDeleteInProgress()).toBeFalsy();
            });

            it("should notify that delete is in progress", function (done) {
                directiveViewModel.deleteAsync().then(done);

                expect(directiveViewModel.isDeleteInProgress()).toBeTruthy();
            });

            it("should notify that delete is complete on success", function (done) {
                directiveViewModel.deleteAsync().then(function () {
                    expect(directiveViewModel.isDeleteInProgress()).toBeFalsy();
                    expect(directiveViewModel.back).toHaveBeenCalled();
                    done();
                    
                });
            });

            it("should do cleanup on error", function (done) {
                deletePromiseReturnValue = Promise.reject("foobar");

                spyOn(console, "error");
                spyOn($.fn, "notificationCenter");

                directiveViewModel.deleteAsync().then(function () {
                    expect(directiveViewModel.isDeleteInProgress()).toBeFalsy();
                    expect(console.error).toHaveBeenCalledWith("foobar");
                    expect($.fn.notificationCenter).toHaveBeenCalledWith("addNotification", jasmine.objectContaining({
                        type: 'error',
                        flash: true
                    }));
                    expect(directiveViewModel.back).toHaveBeenCalled();
                    done();
                });

               
            })
        })


    });

});