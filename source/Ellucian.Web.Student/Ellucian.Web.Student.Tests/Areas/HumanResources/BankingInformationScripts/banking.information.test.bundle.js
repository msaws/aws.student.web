﻿
Ellucian = window.Ellucian || {};
var testsContext = require.context('', true, /.+tests\.js$/);
testsContext.keys().forEach(testsContext);

var sourceContext = require.context('../../../../Ellucian.Web.Student/Areas/HumanResources/Scripts/BankingInformation/', true, /(.*)/);
sourceContext.keys().forEach(sourceContext);
