let testData = require('./banking.information.test.data'),
    component = require('BankingInformation/CreateNewDirective/_CreateNewDirective.js'),
    actions = require("BankingInformation/banking.information.actions");

describe('Test Suite for CreateNewDirective component', function () {
    let params,
        viewModel;

    beforeEach(function () {
        params = {
            dataModel: ko.mapping.fromJS(Object.assign(testData.dataModel)),
        };

        spyOn(actions, "getPayrollAuthenticationAsync").and.returnValue(Promise.resolve({ foo: "bar" }));
        spyOn(actions, "getPayableAuthenticationAsync").and.returnValue(Promise.resolve({ bar: "foo" }));
        spyOn($.fn, "notificationCenter");
        spyOn(window.history, "back");
    });

    let setViewModel = function () {
        if (viewModel != null) {
            viewModel.dispose();
        }
        viewModel = new component.viewModel(params);
    }

    afterEach(function () {
        viewModel.dispose();
        viewModel = null;
    })

    describe('component initialization', function () {
        
        beforeEach(function () {
            setViewModel();
        });
        it('should do set properties', function () {
            expect(viewModel.IsVendor()).toEqual(params.dataModel.IsVendor());
            expect(viewModel.Configuration).toEqual(params.dataModel.Configuration);
            expect(viewModel.IsPayableActive()).toEqual(params.dataModel.IsPayableActive());
            expect(viewModel.IsPayrollActive()).toEqual(params.dataModel.IsPayrollActive());

            expect(viewModel.securityToken()).toBeNull();

        });

        it("should be fine if ActiveRefundDeposit is null", function () {
            
            let newDataModel = {};
            Object.assign(newDataModel, testData.dataModel);
            newDataModel.ActiveRefundDeposit = null;
            params.dataModel = ko.mapping.fromJS(newDataModel);

            setViewModel();

            expect(viewModel).toBeDefined();
        });
    });

    
    describe('Has All Canadian Bank Accounts', function () {

        beforeEach(function () {
            setViewModel();
        })

        it('should be false if at least one bank account with routing id', function () {
            expect(viewModel.hasAllCanadianAccounts()).toBe(false);
        });
        it('should be true when all bank accounts are canadian', function () {
            params.dataModel.ActivePayrollDeposits().forEach(function (dep) {
                dep.RoutingId(null).InstitutionId("123").BranchNumber("12345");
            });
            params.dataModel.ActiveRefundDeposit.RoutingId(null).InstitutionId("123").BranchNumber("12345");
            params.dataModel.FutureRefundDeposits().forEach(function (dep) {
                dep.RoutingId(null).InstitutionId("123").BranchNumber("12345");
            });


            expect(viewModel.hasAllCanadianAccounts()).toBeTruthy();
        });
    });

    describe("New Placeholder Directives", function () {

        beforeEach(function () {
            setViewModel();
        })

        it("should define initial payable directive", function () {
            expect(viewModel.payableDirectiveModel.Id()).toBeNull();
            expect(viewModel.payableDirectiveModel.PayeeId()).toEqual(params.dataModel.PayeeId());
            expect(viewModel.payableDirectiveModel.RoutingId()).toBeNull();
            expect(viewModel.payableDirectiveModel.InstitutionId()).toBeNull();
            expect(viewModel.payableDirectiveModel.Nickname()).toEqual("New Account");
            expect(viewModel.payableDirectiveModel.StartDate()).toEqual(Date.Today());
        });

        it("should define intial payroll directive", function () {
            expect(viewModel.payrollDirectiveModel.Id()).toBeNull();
            expect(viewModel.payrollDirectiveModel.PersonId()).toEqual(params.dataModel.PayeeId());
            expect(viewModel.payrollDirectiveModel.RoutingId()).toBeNull();
            expect(viewModel.payrollDirectiveModel.InstitutionId()).toBeNull();
            expect(viewModel.payrollDirectiveModel.Nickname()).toEqual("New Account");
            expect(viewModel.payrollDirectiveModel.StartDate()).toEqual(Date.Today());
            expect(viewModel.payrollDirectiveModel.DepositOrder()).toEqual(998);
        });
    });

    describe("Directive to Verify/Authenticate", function () {
        let newDataModel;

        beforeEach(function () {
            newDataModel = {};
            Object.assign(newDataModel, testData.dataModel);
        });

        let setViewModelWithNewData = function () {
            params.dataModel = ko.mapping.fromJS(newDataModel);
            setViewModel();
        }

        it("should compute null if there are no directives ", function () {
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.ActiveRefundDeposit = null;
            newDataModel.FutureRefundDeposits = [];

            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toBeNull();
            expect(viewModel.directiveTypeToVerify).toBeNull();
        });

        it("should compute the Active Payroll Remainder", function () {
            setViewModelWithNewData();

            //id 212 is the id of the remainder deposit from the testData;
            expect(viewModel.directiveIdToVerify).toEqual("212");
            expect(viewModel.directiveTypeToVerify).toEqual("payroll");
        });

        it("should compute the first active payroll", function () {
            newDataModel.ActivePayrollDeposits.forEach(function (dep, index) {
                dep.DepositOrder = index + 1;
            });

            setViewModelWithNewData();

            //params.dataModel.ActivePayrollDeposits().forEach(function (dep, index) {
            //    dep.DepositOrder(index + 1);
            //});

            expect(viewModel.directiveIdToVerify).toEqual("213");
            expect(viewModel.directiveTypeToVerify).toEqual("payroll");
        });



        it("should compute the active refund ", function () {
            newDataModel.ActivePayrollDeposits = [];
            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual('0001121');
            expect(viewModel.directiveTypeToVerify).toEqual("payable");
        });

        it("should skip payroll deposits if payroll is not active", function () {
            newDataModel.IsPayrollActive = false;
            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual('0001121');
            expect(viewModel.directiveTypeToVerify).toEqual("payable");
        });

        it("should skip payable deposits if payable is not active", function () {
            newDataModel.IsPayableActive = false;
            newDataModel.PastPayrollDeposits = newDataModel.ActivePayrollDeposits;
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.PastPayrollDeposits[0].DepositOrder = 999;

            setViewModelWithNewData();

            expect(newDataModel.ActiveRefundDeposit).toBeDefined();
            expect(viewModel.directiveIdToVerify).toEqual(newDataModel.PastPayrollDeposits[0].Id);
            expect(viewModel.directiveTypeToVerify).toEqual("payroll");
        })



        it("should compute the past payroll remainder", function () {
            newDataModel.PastPayrollDeposits = newDataModel.ActivePayrollDeposits;
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.ActiveRefundDeposit = null;
            newDataModel.PastPayrollDeposits[0].DepositOrder = 999;
            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual(newDataModel.PastPayrollDeposits[0].Id);
            expect(viewModel.directiveTypeToVerify).toEqual("payroll");
        });

        it("should compute the first past payroll directive", function () {
            newDataModel.PastPayrollDeposits = newDataModel.ActivePayrollDeposits;
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.ActiveRefundDeposit = null;
            newDataModel.PastPayrollDeposits.forEach(function (dep, index) {
                dep.DepositOrder = index + 1;
            });

            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual(newDataModel.PastPayrollDeposits[0].Id);
            expect(viewModel.directiveTypeToVerify).toEqual("payroll");
        });

        it("should compute the first past refund directive", function () {
            newDataModel.PastPayrollDeposits = []
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.PastRefundDeposits.push(newDataModel.ActiveRefundDeposit);
            newDataModel.ActiveRefundDeposit = null;

            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual(newDataModel.PastRefundDeposits[0].Id);
            expect(viewModel.directiveTypeToVerify).toEqual("payable");
        });

        it("should compute the future payroll remainder directive", function () {
            newDataModel.FuturePayrollDeposits = newDataModel.ActivePayrollDeposits;
            newDataModel.PastPayrollDeposits = [];
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.ActiveRefundDeposit = null;
            newDataModel.PastRefundDeposits = [];
            newDataModel.FuturePayrollDeposits[1].DepositOrder = 999;
            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual(newDataModel.FuturePayrollDeposits[1].Id);
            expect(viewModel.directiveTypeToVerify).toEqual("payroll");
        });

        it("should compute the first future payroll directive", function () {
            newDataModel.FuturePayrollDeposits = newDataModel.ActivePayrollDeposits;
            newDataModel.PastPayrollDeposits = [];
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.ActiveRefundDeposit = null;
            newDataModel.PastRefundDeposits = [];
            newDataModel.FuturePayrollDeposits.forEach(function (dep, index) {
                dep.DepositOrder = index + 1;
            });
            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual(newDataModel.FuturePayrollDeposits[0].Id);
            expect(viewModel.directiveTypeToVerify).toEqual("payroll");
        });

        it("should compute the first future refund directive", function () {
            newDataModel.PastPayrollDeposits = []
            newDataModel.ActivePayrollDeposits = [];
            newDataModel.FuturePayrollDeposits = [];
            newDataModel.PastRefundDeposits = [];
            newDataModel.ActiveRefundDeposit = null;

            setViewModelWithNewData();

            expect(viewModel.directiveIdToVerify).toEqual(newDataModel.FutureRefundDeposits[0].Id);
            expect(viewModel.directiveTypeToVerify).toEqual("payable");
        });
    });

    /*
    describe('self.payrollDirectiveModel ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.securityToken ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.directiveTypeToVerify ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.directiveIdToVerify ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.authenticationDirectiveLastFour ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.authenticationDisplayHint ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isPayableSwitchedOn ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.payableStartDateError ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isPayrollSwitchedOn ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isPayrollSwitchVisible ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.payrollDepositAmountOption ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isPayrollRemainderDeposit ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.payrollStartDateError ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isPayrollEndDateVisible ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.payrollEndDateError ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isFuturePayrollRemainderAndCurrentRemainderExists ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.payrollEffectiveDateMessage ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.willChangeEndDateOfCurrentRemainderMessage ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.specificAmountIsVisible ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.specificDepositAmountError ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.showRemainingBalanceOption ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.depositAmountNeedsInput ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.sortablePayrollDeposits ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.editingDirectiveId ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.numberActiveNonRemainderDirectives ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.hasInputErrors ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isNextStepEnabled ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.accountDetailsModel ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isBankAccountDetailsInputValid ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.showEditBankAccountDetails ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.nextStep ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.hideEditBankAccountDetails ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.savingNewDirectiveMessage ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.isSaveNewDirectiveInProgress ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.saveNewDirective ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    describe('self.cancel ', function () {
    let viewModel;
    beforeEach(function() {
    viewModel = new ViewModel(params);
    });
    afterEach(function() {
    viewModel.dispose();
    });
    it('should do something', function() {
    // test code here...
    });
    });
    */
});
