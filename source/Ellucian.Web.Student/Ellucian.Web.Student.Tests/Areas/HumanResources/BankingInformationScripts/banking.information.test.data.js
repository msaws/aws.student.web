﻿let dataModel = {
    Configuration: {
        AddEditAccountTermsAndConditions: 'words and things',
        PayrollMessage: 'The next payroll will process on Friday, Nevruary 1st',
        PayrollEffectiveDateMessage: 'New direct deposits may not take effect until the next payroll cycle.',
        IsRemainderAccountRequired: true,
        UseFederalRoutingDirectory: true,
        IsDirectDepositEnabled: true,
        IsPayableDepositEnabled: true
    },
    ActivePayrollDeposits: [
        {
            Id: '213',
            PersonId: '0014076',
            RoutingId: '011000028',
            InstitutionId: null,
            BranchNumber: null,
            BankName: 'New Bank of Montpelier',
            NewAccountId: null,
            AccountIdLastFour: '7777',
            Nickname: 'nnnnn',
            Type: 0,
            SecurityToken: null,
            DepositAmount: 111,
            StartDate: new Date('2017-04-14T04:00:00.000Z'),
            EndDate: null,
            OriginalDepositOrder: 1,
            DepositOrder: 1,
            AddDateTime: new Date('2017-04-14T16:14:20.000Z'),
            ChangeDateTime: new Date('2017-04-19T20:34:42.000Z'),
            AddOperator: '0014076',
            ChangeOperator: '0014076',
            IsVerified: false
        },
        {
            Id: '212',
            PersonId: '0014076',
            RoutingId: '091000019',
            InstitutionId: null,
            BranchNumber: null,
            BankName: 'Chase Bank',
            NewAccountId: null,
            AccountIdLastFour: '9999',
            Nickname: 'cuyabeno',
            Type: 0,
            SecurityToken: null,
            DepositAmount: null,
            StartDate: new Date('2017-04-14T04:00:00.000Z'),
            EndDate: null,
            OriginalDepositOrder: 999,
            DepositOrder: 999,
            AddDateTime: new Date('2017-04-14T16:10:28.000Z'),
            ChangeDateTime: new Date('2017-04-17T15:47:46.000Z'),
            AddOperator: '0014076',
            ChangeOperator: '0014076',
            IsVerified: false
        }
    ],
    PastPayrollDeposits: [],
    FuturePayrollDeposits: [],
    ActiveRefundDeposit: {
        Id: '0001121',
        PayeeId: '0014076',
        RoutingId: '091000019',
        InstitutionId: null,
        BranchNumber: null,
        BankName: 'Chase Bank',
        NewAccountId: null,
        AccountIdLastFour: '9999',
        Nickname: 'cuyabeno',
        Type: 0,
        SecurityToken: null,
        AddressId: null,
        StartDate: new Date('2017-04-14T04:00:00.000Z'),
        EndDate: null,
        IsElectronicPaymentRequested: true,
        AddDateTime: new Date('2017-04-14T16:10:27.000Z'),
        ChangeDateTime: new Date('2017-04-14T16:10:27.000Z'),
        AddOperator: '0014076',
        ChangeOperator: '0014076',
        IsVerified: false
    },
    PastRefundDeposits: [],
    FutureRefundDeposits: [
        {
            Id: '4432432',
            PayeeId: '0014076',
            RoutingId: '091000019',
            InstitutionId: null,
            BranchNumber: null,
            BankName: 'FutureBank',
            NewAccountId: null,
            AccountIdLastFour: '9999',
            Nickname: 'cuyabeno',
            Type: 0,
            SecurityToken: null,
            AddressId: null,
            StartDate: (new Date()).addDays(5),
            EndDate: null,
            IsElectronicPaymentRequested: true,
            AddDateTime: new Date('2017-04-14T16:10:27.000Z'),
            ChangeDateTime: new Date('2017-04-14T16:10:27.000Z'),
            AddOperator: '0014076',
            ChangeOperator: '0014076',
            IsVerified: false
        }
    ],
    Addresses: [
        {
            Id: null,
            AddressLines: [
                'Default Account'
            ]
        }
    ],
    PayeeId: '0014076',
    IsVendor: false,
    AddressId: 'europa',
    IsPayrollActive: true,
    IsPayableActive: true
}

module.exports.dataModel = dataModel

module.exports.viewModel = {

    Configuration: {
        AddEditAccountTermsAndConditions: ko.observable('words and things'),
        PayrollMessage: ko.observable('The next payroll will process on Friday, Nevruary 1st'),
        PayrollEffectiveDateMessage: ko.observable('New direct deposits may not take effect until the next payroll cycle.'),
        IsRemainderAccountRequired: ko.observable(true),
        UseFederalRoutingDirectory: ko.observable(true),
        IsDirectDepositEnabled: ko.observable(true),
        IsPayableDepositEnabled: ko.observable(true)
    },
    ActivePayrollDeposits: ko.observableArray([
        {
            Id: ko.observable('213'),
            PersonId: ko.observable('0014076'),
            RoutingId: ko.observable('011000028'),
            InstitutionId: ko.observable(null),
            BranchNumber: ko.observable(null),
            BankName: ko.observable('New Bank of Montpelier'),
            NewAccountId: ko.observable(null),
            AccountIdLastFour: ko.observable('7777'),
            Nickname: ko.observable('nnnnn'),
            Type: ko.observable(0),
            SecurityToken: ko.observable(null),
            DepositAmount: ko.observable(111),
            StartDate: ko.observable('2017-04-14T04:00:00.000Z'),
            EndDate: ko.observable(null),
            OriginalDepositOrder: ko.observable(1),
            DepositOrder: ko.observable(1),
            AddDateTime: ko.observable('2017-04-14T16:14:20.000Z'),
            ChangeDateTime: ko.observable('2017-04-19T20:34:42.000Z'),
            AddOperator: ko.observable('0014076'),
            ChangeOperator: ko.observable('0014076'),
            IsVerified: ko.observable(false)
        },
            {
                Id: ko.observable('212'),
                PersonId: ko.observable('0014076'),
                RoutingId: ko.observable('091000019'),
                InstitutionId: ko.observable(null),
                BranchNumber: ko.observable(null),
                BankName: ko.observable('Chase Bank'),
                NewAccountId: ko.observable(null),
                AccountIdLastFour: ko.observable('9999'),
                Nickname: ko.observable('cuyabeno'),
                Type: ko.observable(0),
                SecurityToken: ko.observable(null),
                DepositAmount: ko.observable(null),
                StartDate: ko.observable(new Date('2017-04-14T04:00:00.000Z')),
                EndDate: ko.observable(null),
                OriginalDepositOrder: ko.observable(999),
                DepositOrder: ko.observable(999),
                AddDateTime: ko.observable(new Date('2017-04-14T16:10:28.000Z')),
                ChangeDateTime: ko.observable(new Date('2017-04-17T15:47:46.000Z')),
                AddOperator: ko.observable('0014076'),
                ChangeOperator: ko.observable('0014076'),
                IsVerified: ko.observable(false)
            }
    ]),
    PastPayrollDeposits: ko.observableArray([]),
    FuturePayrollDeposits: ko.observableArray([]),
    ActiveRefundDeposit: ko.observable({
        Id: ko.observable('0001121'),
        PayeeId: ko.observable('0014076'),
        RoutingId: ko.observable('091000019'),
        InstitutionId: ko.observable(null),
        BranchNumber: ko.observable(null),
        BankName: ko.observable('Chase Bank'),
        NewAccountId: ko.observable(null),
        AccountIdLastFour: ko.observable('9999'),
        Nickname: ko.observable('cuyabeno'),
        Type: ko.observable(0),
        SecurityToken: ko.observable(null),
        AddressId: ko.observable(null),
        StartDate: ko.observable(new Date('2017-04-14T04:00:00.000Z')),
        EndDate: ko.observable(null),
        IsElectronicPaymentRequested: ko.observable(true),
        AddDateTime: ko.observable(new Date('2017-04-14T16:10:27.000Z')),
        ChangeDateTime: ko.observable(new Date('2017-04-14T16:10:27.000Z')),
        AddOperator: ko.observable('0014076'),
        ChangeOperator: ko.observable('0014076'),
        IsVerified: ko.observable(false)
    }),
    PastRefundDeposits: ko.observableArray([]),
    FutureRefundDeposits: ko.observableArray([]),
    Addresses: ko.observableArray([
        {
            Id: ko.observable(null),
            AddressLines: ko.observable([
                'Default Account'
            ])
        }
    ]),
    PayeeId: ko.observable('0014076'),
    IsVendor: ko.observable(false),
    AddressId: ko.observable('europa'),
    IsPayrollActive: ko.observable(true),
    IsPayableActive: ko.observable(true)

}
personProxyLoadingThrobberMessage = '';
personProxyChangingThrobberMessage = ''
verifyingPasswordMessage = '';
verifyingPasswordAltText = '';
Ellucian = window.Ellucian || {};
Ellucian.Proxy = window.Ellucian.Proxy || {};
Ellucian.Proxy.ButtonLabels = window.Ellucian.Proxy.ButtonLabels || {};
Ellucian.Proxy.ButtonLabels.saveProxySelectionDialogButtonLabel = window.Ellucian.Proxy.ButtonLabels.saveProxySelectionDialogButtonLabel || {};
Ellucian.SharedComponents = window.Ellucian.SharedComponents || {};
Ellucian.SharedComponents.ButtonLabels = window.Ellucian.SharedComponents.ButtonLabels || {};
Ellucian.SharedComponents.ButtonLabels.buttonTextVerifyPassword = window.Ellucian.SharedComponents.ButtonLabels.buttonTextVerifyPassword || {};
Ellucian.BankingInformation = Ellucian.BankingInformation || {};
Ellucian.BankingInformation.ActionUrls = Ellucian.BankingInformation.ActionUrls || {};