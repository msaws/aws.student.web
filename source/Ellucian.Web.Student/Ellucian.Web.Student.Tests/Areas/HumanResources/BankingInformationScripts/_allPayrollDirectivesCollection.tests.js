var testData = require('./banking.information.test.data');
var component = require('BankingInformation/AllPayrollDirectivesCollection/_AllPayrollDirectivesCollection.js');

describe('Test Suite for AllPayrollDirectivesCollection component', function () {
    let params = null;
    let ViewModel = component.viewModel;
    beforeEach(function () {
        params = {
            dataModel: testData.viewModel
        };
    });

    describe('payroll deposits ', function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new ViewModel(params);
        });
        it('should set deposits', function() {
            expect(viewModel.ActivePayrollDeposits()).toEqual(ko.unwrap(params.dataModel.ActivePayrollDeposits()));
            expect(viewModel.FuturePayrollDeposits()).toEqual(params.dataModel.FuturePayrollDeposits());
            expect(viewModel.PastPayrollDeposits()).toEqual(params.dataModel.PastPayrollDeposits());
        });
        it('should aggregate deposits', function() {
            let actual = viewModel.ActivePayrollDeposits()
            .concat(viewModel.PastPayrollDeposits())
            .concat(viewModel.FuturePayrollDeposits());
            let expected = testData.viewModel.ActivePayrollDeposits()
            .concat(testData.viewModel.PastPayrollDeposits())
            .concat(testData.viewModel.FuturePayrollDeposits());
            expect(expected).toEqual(actual);
        });
    });
    describe('self.hasAllCanadianAccounts ', function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new ViewModel(params);
        });
        it('should not have all canadian accounts when any accounts are not canadian', function() {
            viewModel.ActivePayrollDeposits()[0].RoutingId('bermuda');
            expect(viewModel.hasAllCanadianAccounts()).toBe(false);
        });
        it('should have all canadian accounts when all accounts are canadian', function() {
            viewModel.ActivePayrollDeposits().forEach(function(dep) { dep.RoutingId(null); });
            viewModel.FuturePayrollDeposits().forEach(function(dep){ dep.RoutingId(null)});
            viewModel.PastPayrollDeposits().forEach(function(){ dep => dep.RoutingId(null)});
            expect(viewModel.hasAllCanadianAccounts()).toBe(true);
        });
    });

});
