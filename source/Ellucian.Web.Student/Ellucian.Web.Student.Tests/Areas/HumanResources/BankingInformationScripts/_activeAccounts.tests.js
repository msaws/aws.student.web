var testData = require('./banking.information.test.data');
var component = require('BankingInformation/ActiveAccountsCollection/_ActiveAccounts.js');
//var resources = require('Site/resource.manager');

describe('Test Suite for active accounts component', function () {
    let params = null;
    let ViewModel = component.viewModel;

    beforeEach(function () {
        params = {
            dataModel: testData.viewModel
        };
    });

    describe('self.dataModel ', function () {
        let viewModel = null;
        beforeEach(function() {
            viewModel = new ViewModel(params);
        });
        it('should load the viewModel', function () {
            expect(viewModel).not.toBe(null);
            expect(viewModel.IsPayableActive).toEqual(params.dataModel.IsPayableActive);
            expect(viewModel.IsPayrollActive).toEqual(params.dataModel.IsPayrollActive);
            expect(viewModel.IsVendor).toEqual(params.dataModel.IsVendor);
            expect(viewModel.Configuration).toEqual(params.dataModel.Configuration);
            expect(viewModel.Addresses).toEqual(params.dataModel.Addresses);
        });
    });

    describe('self.selectedAddressId ', function () {
        let viewModel = null;
        beforeEach(function () {
            viewModel = new ViewModel(params);
        });
        it('should set the value', function () {
            expect(ko.isObservable(viewModel.selectedAddressId)).toBe(true);
            expect(viewModel.selectedAddressId()).toEqual(params.dataModel.AddressId())
        });
    });
    describe('self.loadVendorDeposits ', function () {
        let viewModel = null;
        //let windowLocation = window.location;
        beforeEach(function () {
            //window.location = { hash: '' };
            window.location.hash = '';
            viewModel = new ViewModel(params);
        });
        afterEach(function () {
            //window.location = windowLocation;
        });
        it('should set the hash with an addressId', function () {
            viewModel.loadVendorDeposits();
            expect(window.location.hash).toEqual('#addressId=' + viewModel.selectedAddressId());
        });
        it('should set the hash without an addressId', function () {
            params.dataModel.AddressId(null);
            viewModel = new ViewModel(params);
            viewModel.loadVendorDeposits();
            expect(window.location.hash).toEqual('');
        });
    });
    describe('self.addAccount', function () {
        let viewModel = null;
        beforeEach(function () {
            window.location.hash = '';
            viewModel = new ViewModel(params);
        });
        it('should set the hash with an addressId', function () {
            params.dataModel.AddressId('pbtt');
            viewModel = new ViewModel(params);
            viewModel.addAccount();
            expect(window.location.hash).toEqual('#new&addressId=' + viewModel.selectedAddressId());
        });
        it('should set the hash without an addressId', function () {
            params.dataModel.AddressId(null);
            viewModel = new ViewModel(params);
            viewModel.addAccount();
            expect(window.location.hash).toEqual('#new');
        });
    });

    //describe('self.absentTermsAndConditionsMessage ', function () {
    //    let viewModel;
    //    beforeEach(function() {
    //        viewModel = new ViewModel(params);
    //    });
    //    it('should have a value in the message', function() {
    //        expect(viewModel.absentTermsAndConditionsMessage()).not.toBe(null);
    //    });
    //});
    describe('self.hasAllCanadianAccounts ', function () {
        let viewModel;
        beforeEach(function() {
            viewModel = new ViewModel(params);
        });
        it('should return false when user has not all canadian accounts', function() {
            // test data has all us accounts
            expect(viewModel.hasAllCanadianAccounts()).toBe(false);
        });
        it('should return false when user has not all canadian accounts', function () {
            for (let i = 0; i < params.dataModel.ActivePayrollDeposits().length; i++) {
                params.dataModel.ActivePayrollDeposits()[i].RoutingId(null);
            }
            params.dataModel.ActiveRefundDeposit().RoutingId(null);
            viewModel = new ViewModel(params);
            expect(viewModel.hasAllCanadianAccounts()).toBe(true);
        });
    });
});