﻿var component = require("BankingInformation/EditViewComponent/_EditViewComponent"),
    actions = require("BankingInformation/banking.information.actions"),
    resources = require("Site/resource.manager");


describe("Banking Information Test Suite for the EditViewComponent", function () {

    var viewModel,
        params,
        getDataPromise,
        getResourceObservable;
        

    beforeEach(function () {
        params = {
            type: 'payroll',
            id: '5',
            addressId: undefined,
        }

        getDataPromise = Promise.resolve({ foo: 'bar' });
        getResourceObservable = ko.observable("foobar");

        spyOn(actions, 'getBankingInformationViewModelAsync').and.callFake(function () {
            return getDataPromise; //doing it this way to be dynamic
        });

        spyOn(resources, "getObservable").and.returnValue(getResourceObservable);
    });

    setViewModel = function () {
        viewModel = new component.viewModel(params);
    }

    afterEach(function () {
        //viewModel.dispose();
    });

    describe("Params set viewModel attributes", function () {

        beforeEach(function () {
            setViewModel();
        });

        it("sets addressId", function () {
            expect(viewModel.addressId).toBeNull();
            params.addressId = "5";
            setViewModel();
            expect(viewModel.addressId).toEqual(params.addressId);
        });

        it("sets type", function () {
            expect(viewModel.type).toEqual(params.type);
        });

        it("sets id", function () {
            expect(viewModel.id).toEqual(params.id);
        });
    });

    describe("ViewModel initialization", function () {
        beforeEach(function () {
            setViewModel();
        });

        it("initialize isLoading", function () {
            expect(ko.isObservable(viewModel.isLoading)).toBeTruthy();
            expect(viewModel.isLoading()).toBeTruthy();
        });

        it("initialize loadingMessage", function () {
            expect(resources.getObservable).toHaveBeenCalledWith("BankingInformation.LoadingBankingInformationMessage");
            expect(viewModel.loadingMessage()).toEqual(getResourceObservable());
        });

        it("initialize loadError", function () {
            expect(ko.isObservable(viewModel.loadError)).toBeTruthy();
            expect(viewModel.loadError()).toBeFalsy();
        });

        it("initialize loadErrorMessage", function () {
            expect(resources.getObservable).toHaveBeenCalledWith("BankingInformation.LoadBankingInformationErrorMessage");
            expect(viewModel.loadErrorMessage()).toEqual(getResourceObservable());
        });

        it("initialize bankingInformationDataModel", function () {
            expect(viewModel.bankingInformationDataModel).toEqual({});
        });
    });

    describe("Getting BankingInformationViewModel", function () {

        beforeEach(function () {
            spyOn(ko.mapping, "fromJS").and.callThrough();
            spyOn(console, "error");
        })

        it("use addressId parameter", function () {
            params.addressId = "foobar";
            setViewModel();

            expect(actions.getBankingInformationViewModelAsync).toHaveBeenCalledWith("", params.addressId);
        });
     

        it("map data model and setup viewModel on success", function (done) {

            setViewModel();

            getDataPromise.then(function () {
                expect(ko.mapping.fromJS).toHaveBeenCalledWith({ foo: 'bar' });
                expect(viewModel.loadError()).toBeFalsy();
                expect(viewModel.isLoading()).toBeFalsy();
                expect(viewModel.bankingInformationDataModel).not.toEqual({});
                return;
            }).then(done);
        });

        it("set up viewModel and log on error", function (done) {
            var rejectValue = "this is the error";
            getDataPromise = Promise.reject(rejectValue);

            setViewModel();

            getDataPromise.catch(function() {return;}).then(function () {
                expect(viewModel.loadError()).toBeTruthy();
                expect(viewModel.isLoading()).toBeFalsy();
                expect(console.error).toHaveBeenCalledWith(rejectValue);
               
            }).then(done);
        });
    });

    describe("Back function", function () {
        beforeEach(function() {
            spyOn(window.history, "back");
            setViewModel();
        });
        it("should pop window history", function () {
            viewModel.back();

            expect(window.history.back).toHaveBeenCalled();
        });
    });
});