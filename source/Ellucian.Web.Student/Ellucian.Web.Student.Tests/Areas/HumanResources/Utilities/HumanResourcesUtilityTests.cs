﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests.Areas.HumanResources.Utilities
{
    [TestClass]
    public class HumanResourcesUtilityTests
    {
        private string personId = "1";
        private DateTime timeStamp = DateTime.Now;
        private string taxForm = "FormW2";
        private bool hasConsented = true;

        [TestMethod]
        public void BuildTaxFormConsentDto_FormW2()
        {
            var consentDto = HumanResourcesUtility.BuildTaxFormConsentDto(personId, timeStamp, taxForm, hasConsented);
            Assert.AreEqual(personId, consentDto.PersonId);
            Assert.AreEqual(timeStamp, consentDto.TimeStamp);
            Assert.AreEqual(taxForm, consentDto.TaxForm.ToString());
            Assert.AreEqual(hasConsented, consentDto.HasConsented);
        }

        [TestMethod]
        public void BuildTaxFormConsentDto_Form1095C()
        {
            taxForm = "Form1095C";
            var consentDto = HumanResourcesUtility.BuildTaxFormConsentDto(personId, timeStamp, taxForm, hasConsented);
            Assert.AreEqual(taxForm, consentDto.TaxForm.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void BuildTaxFormConsentDto_InvalidTaxForm()
        {
            taxForm = "1095";
            HumanResourcesUtility.BuildTaxFormConsentDto(personId, timeStamp, taxForm, hasConsented);
        }
    }
}