﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Tests.Utility
{
    [TestClass]
    public class CollectionUtilityTests
    {
        [TestMethod]
        public void IsNullOrEmpty()
        {
            var populatedCollection = new List<string>() { "1", "2", "3" };

            var isNullOrEmpty = CollectionUtility.IsNullOrEmpty(populatedCollection);

            Assert.IsFalse(isNullOrEmpty);
        }

        [TestMethod]
        public void IsNullOrEmpty_NullCollection()
        {
            var isNullOrEmpty = CollectionUtility.IsNullOrEmpty<string>(null);

            Assert.IsTrue(isNullOrEmpty);
        }

        [TestMethod]
        public void IsNullOrEmpty_EmptyCollection()
        {
            var populatedCollection = new List<string>();

            var isNullOrEmpty = CollectionUtility.IsNullOrEmpty(populatedCollection);

            Assert.IsTrue(isNullOrEmpty);
        }
    }
}
