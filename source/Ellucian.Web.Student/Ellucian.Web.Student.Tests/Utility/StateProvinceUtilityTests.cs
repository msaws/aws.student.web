﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Tests.Utility
{
    [TestClass]
    public class StateProvinceUtilityTests
    {
        [TestMethod]
        public void StateProvinceUtility_Get_All()
        {
            var result = StateProvinceUtility.Get();
            Assert.AreEqual(64, result.Count());
        }

        [TestMethod]
        public void StateProvinceUtility_Get_US()
        {
            var result = StateProvinceUtility.Get("US");
            Assert.AreEqual(51, result.Count()); // Includes DC
        }

        [TestMethod]
        public void StateProvinceUtility_Get_CA()
        {
            var result = StateProvinceUtility.Get("CA");
            Assert.AreEqual(13, result.Count());
        }

        [TestMethod]
        public void StateProvinceUtility_Get_MX()
        {
            var result = StateProvinceUtility.Get("MX");
            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void StateProvinceUtility_GetByCode_US()
        {
            var result = StateProvinceUtility.GetByCode("US").ToList();
            // When sorted by code, NY comes after NC
            int posNY = result.FindIndex(x => x.Code == "NY");
            int posNC = result.FindIndex(x => x.Code == "NC");
            Assert.IsTrue(posNC < posNY);
        }

        [TestMethod]
        public void StateProvinceUtility_GetByCode_CA()
        {
            var result = StateProvinceUtility.GetByCode("CA").ToList();
            // When sorted by code, NT comes after NS
            int posNT = result.FindIndex(x => x.Code == "NT");
            int posNS = result.FindIndex(x => x.Code == "NS");
            Assert.IsTrue(posNS < posNT);
        }

        [TestMethod]
        public void StateProvinceUtility_GetByName_US()
        {
            var result = StateProvinceUtility.GetByName("US").ToList();
            // When sorted by code, NC comes after NY
            int posNY = result.FindIndex(x => x.Code == "NY");
            int posNC = result.FindIndex(x => x.Code == "NC");
            Assert.IsTrue(posNC > posNY);
        }

        [TestMethod]
        public void StateProvinceUtility_GetByName_CA()
        {
            var result = StateProvinceUtility.GetByName("CA").ToList();
            // When sorted by name, NS comes after NT
            int posNT = result.FindIndex(x => x.Code == "NT");
            int posNS = result.FindIndex(x => x.Code == "NS");
            Assert.IsTrue(posNS > posNT);
        }

    }
}
