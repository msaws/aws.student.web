﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;

namespace Ellucian.Web.Student.Tests.Utility
{
    [TestClass]
    public class DateUtilityTests
    {
        DateTime date;
        DateTime dateEdt;
        DateTime dateEst;
        DateTime? rangeStart;
        DateTime? rangeEnd;
        DateTime? range2Start;
        DateTime? range2End;

        [TestInitialize]
        public void Initialize()
        {
            dateEdt = new DateTime(1976, 7, 4, 11, 11, 11);
            dateEst = new DateTime(1986, 1, 28, 10, 0, 0);
        }

        #region LocalTimeZoneOffset tests

        [TestMethod]
        public void DateUtility_GetTimeZoneOffset_VerifyEdt()
        {
            var offset = DateUtility.GetTimeZoneOffset(dateEdt);
            TimeSpan expected = new TimeSpan(-4, 0, 0);
            Assert.AreEqual(expected, offset);
        }

        [TestMethod]
        public void DateUtility_GetTimeZoneOffset_VerifyEst()
        {
            var offset = DateUtility.GetTimeZoneOffset(dateEst);
            TimeSpan expected = new TimeSpan(-5, 0, 0);
            Assert.AreEqual(expected, offset);
        }

        [TestMethod]
        public void DateUtility_LocalTimeZoneOffsetString_VerifyEst()
        {
            var offset = DateUtility.GetTimeZoneOffsetString(dateEdt);
            string expected = "-0400";
            Assert.AreEqual(expected, offset);
        }

        [TestMethod]
        public void DateUtility_LocalTimeZoneOffsetString_VerifyEdt()
        {
            var offset = DateUtility.GetTimeZoneOffsetString(dateEst);
            string expected = "-0500";
            Assert.AreEqual(expected, offset);
        }

        #endregion

        #region IsDateInRange tests

        [TestMethod]
        public void DateUtility_IsDateInRange_TrueNullStart()
        {
            rangeStart = null;
            rangeEnd = new DateTime(2012, 9, 1);
            date = rangeEnd.Value.AddDays(-1);
            var result = DateUtility.IsDateInRange(date, rangeStart, rangeEnd);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsDateInRange_FalseNullStart()
        {
            rangeStart = null;
            rangeEnd = new DateTime(2012, 9, 1);
            date = rangeEnd.Value.AddDays(1);
            var result = DateUtility.IsDateInRange(date, rangeStart, rangeEnd);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DateUtility_IsDateInRange_TrueNonNullStart()
        {
            rangeStart = new DateTime(2012, 8, 1);
            rangeEnd = new DateTime(2012, 9, 1);
            date = rangeEnd.Value.AddDays(-1);
            var result = DateUtility.IsDateInRange(date, rangeStart, rangeEnd);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsDateInRange_FalseNonNullStart()
        {
            rangeStart = new DateTime(2012, 8, 1);
            rangeEnd = new DateTime(2012, 9, 1);
            date = rangeEnd.Value.AddDays(1);
            var result = DateUtility.IsDateInRange(date, rangeStart, rangeEnd);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DateUtility_IsDateInRange_TrueRangeReversed()
        {
            rangeStart = new DateTime(2012, 9, 1);
            rangeEnd = new DateTime(2012, 8, 1);
            date = rangeStart.Value.AddDays(-1);
            var result = DateUtility.IsDateInRange(date, rangeStart, rangeEnd);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsDateInRange_FalseRangeReversed()
        {
            rangeStart = new DateTime(2012, 9, 1);
            rangeEnd = new DateTime(2012, 8, 1);
            date = rangeStart.Value.AddDays(1);
            var result = DateUtility.IsDateInRange(date, rangeStart, rangeEnd);
            Assert.IsFalse(result);
        }

        #endregion

        #region IsRangeOverlap tests

        [TestMethod]
        public void DateUtility_IsRangeOverlap_NoOverlap()
        {
            rangeStart = new DateTime(2012, 1, 1);
            rangeEnd = new DateTime(2012, 6, 30);
            range2Start = new DateTime(2012, 7, 1);
            range2End = new DateTime(2012, 12, 31);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_NoOverlap_NullEnds()
        {
            rangeStart = null;
            rangeEnd = new DateTime(2012, 6, 30);
            range2Start = new DateTime(2012, 7, 1);
            range2End = null;
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_NoOverlap_ReversedDates()
        {
            rangeStart = new DateTime(2012, 6, 30);
            rangeEnd = new DateTime(2012, 1, 1);
            range2Start = new DateTime(2012, 7, 1);
            range2End = new DateTime(2012, 12, 31);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_Overlap()
        {
            rangeStart = new DateTime(2012, 1, 1);
            rangeEnd = new DateTime(2012, 7, 1);
            range2Start = new DateTime(2012, 7, 1);
            range2End = new DateTime(2012, 12, 31);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_Overlap_NullEnds()
        {
            rangeStart = null;
            rangeEnd = new DateTime(2012, 7, 1);
            range2Start = new DateTime(2012, 7, 1);
            range2End = null;
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_Overlap_ReversedDates()
        {
            rangeStart = new DateTime(2012, 1, 1);
            rangeEnd = new DateTime(2012, 7, 1);
            range2Start = new DateTime(2012, 12, 31);
            range2End = new DateTime(2012, 7, 1);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_OverlapOpposite()
        {
            rangeStart = new DateTime(2012, 7, 1);
            rangeEnd = new DateTime(2012, 12, 31);
            range2Start = new DateTime(2012, 1, 1);
            range2End = new DateTime(2012, 7, 1);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_Overlap_NullEndsOpposite()
        {
            rangeStart = new DateTime(2012, 7, 1);
            rangeEnd = null;
            range2Start = null;
            range2End = new DateTime(2012, 7, 1);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_Overlap_ReversedDatesOpposite()
        {
            rangeStart = new DateTime(2012, 12, 31);
            rangeEnd = new DateTime(2012, 7, 1);
            range2Start = new DateTime(2012, 1, 1);
            range2End = new DateTime(2012, 7, 1);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_Subset()
        {
            rangeStart = new DateTime(2012, 1, 1);
            rangeEnd = new DateTime(2012, 12, 31);
            range2Start = new DateTime(2012, 7, 1);
            range2End = new DateTime(2012, 7, 31);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_Subset2()
        {
            rangeStart = new DateTime(2012, 7, 1);
            rangeEnd = new DateTime(2012, 7, 31);
            range2Start = new DateTime(2012, 1, 1);
            range2End = new DateTime(2012, 12, 31);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_PriorRange()
        {
            rangeStart = new DateTime(2012, 1, 1);
            rangeEnd = new DateTime(2012, 12, 31);
            range2Start = new DateTime(2011, 7, 1);
            range2End = new DateTime(2011, 7, 31);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DateUtility_IsRangeOverlap_FutureRange()
        {
            rangeStart = new DateTime(2012, 1, 1);
            rangeEnd = new DateTime(2012, 12, 31);
            range2Start = new DateTime(2013, 7, 1);
            range2End = new DateTime(2013, 7, 31);
            var result = DateUtility.IsRangeOverlap(rangeStart, rangeEnd, range2Start, range2End);
            Assert.IsFalse(result);
        }

        #endregion

        #region DateRangeDefaults tests

        [TestMethod]
        public void DateUtility_DateRangeDefaults_NullStartDate()
        {
            rangeStart = null;
            rangeEnd = DateTime.Now;
            DateUtility.DateRangeDefaults(ref rangeStart, ref rangeEnd);
            Assert.AreEqual(DateTime.MinValue, rangeStart);
        }

        public void DateUtility_DateRangeDefaults_NullEndDate()
        {
            rangeStart = DateTime.Now;
            rangeEnd = null;
            DateUtility.DateRangeDefaults(ref rangeStart, ref rangeEnd);
            Assert.AreEqual(DateTime.MinValue, rangeEnd);
        }

        public void DateUtility_DateRangeDefaults_ReversedDates()
        {
            rangeStart = DateTime.Now;
            rangeEnd = rangeStart.Value.AddYears(-1);
            var origStart = rangeStart;
            var origEnd = rangeEnd;
            DateUtility.DateRangeDefaults(ref rangeStart, ref rangeEnd);
            Assert.AreEqual(origEnd, rangeStart);
            Assert.AreEqual(origStart, rangeEnd);
        }

        public void DateUtility_DateRangeDefaults_NoChange()
        {
            rangeStart = DateTime.Now;
            rangeEnd = rangeStart.Value.AddYears(1);
            var origStart = rangeStart;
            var origEnd = rangeEnd;
            DateUtility.DateRangeDefaults(ref rangeStart, ref rangeEnd);
            Assert.AreEqual(origStart, rangeStart);
            Assert.AreEqual(origEnd, rangeEnd);
        }

        #endregion
    }
}
