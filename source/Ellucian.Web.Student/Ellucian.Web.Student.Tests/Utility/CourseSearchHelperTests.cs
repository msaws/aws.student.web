﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;
using Ellucian.Web.Student.Models.Courses;

namespace Ellucian.Web.Student.Tests.Utility
{
    [TestClass]
    public class CourseSearchHelperTests
    {
        string keyword;
        List<CourseSearchModel> keywordComponents;
        CourseSearchHelper _helper;

        [TestInitialize]
        public void Initialize()
        {
            keyword = null;
            keywordComponents = new List<CourseSearchModel>() { new CourseSearchModel("AAA", "111", "11"), new CourseSearchModel("BBB", "222", ""), new CourseSearchModel("CCC", "", "") };
            _helper = new CourseSearchHelper();
        }

        #region BuildKeywordString tests

        [TestMethod]
        public void CourseSearchHelper_BuildKeywordString_NullKeyword_MultipleComponents()
        {
            var newKeyword = _helper.BuildKeywordString(keyword, keywordComponents);
            Assert.AreEqual("name:AAA-111-11 or name:BBB-222 or subject:CCC", newKeyword);
        }

        [TestMethod]
        public void CourseSearchHelper_BuildKeywordString_NullKeyword_NullComponents()
        {
            var newKeyword = _helper.BuildKeywordString(keyword, null);
            Assert.IsNull(newKeyword);
        }

        [TestMethod]
        public void CourseSearchHelper_BuildKeywordString_NullKeyword_ZeroComponents()
        {
            var newKeyword = _helper.BuildKeywordString(keyword, new List<CourseSearchModel>());
            Assert.IsNull(newKeyword);
        }

        [TestMethod]
        public void CourseSearchHelper_BuildKeywordString_NullKeyword_OneComponents()
        {
            var newKeyword = _helper.BuildKeywordString(keyword, new List<CourseSearchModel>() { new CourseSearchModel("CCC", "", "") });
            Assert.AreEqual("subject:CCC", newKeyword);
        }

        [TestMethod]
        public void CourseSearchHelper_BuildKeywordString_KeywordAndOneComponents()
        {
            var newKeyword = _helper.BuildKeywordString("XYZ", new List<CourseSearchModel>() { new CourseSearchModel("CCC", "", "") });
            Assert.AreEqual("XYZ or subject:CCC", newKeyword);
        }

        [TestMethod]
        public void CourseSearchHelper_BuildKeywordString_KeywordAndMultipleComponents()
        {
            var newKeyword = _helper.BuildKeywordString("XYZ", keywordComponents);
            Assert.AreEqual("XYZ or name:AAA-111-11 or name:BBB-222 or subject:CCC", newKeyword);
        }

        #endregion

        
    }
}
