﻿using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Web.Student.Tests
{


    /// <summary>
    ///This is a test class for ReflectionUtilitiesTest and is intended
    ///to contain all ReflectionUtilitiesTest Unit Tests
    ///</summary>
    [TestClass]
    public class ReflectionUtilitiesTest
    {

        Term instance;
        string property;
        string value;

        [TestInitialize]
        public void Initialize()
        {
            instance = new Term();
            instance.Code = "Original";
            property = "Code";
            value = "New";
        }

        /// <summary>
        ///A test for SetProperty
        ///</summary>
        [TestMethod]
        public void SetPropertyTest()
        {
            ReflectionUtilities.SetProperty(instance, property, value);
            Assert.IsTrue((string)instance.GetType().GetProperty(property).GetValue(instance, null) == value);
        }

    }
}
