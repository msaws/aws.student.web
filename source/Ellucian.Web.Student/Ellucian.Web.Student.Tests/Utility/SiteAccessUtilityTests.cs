﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Mvc.Licensing;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Infrastructure;
using Ellucian.Web.Student.Licensing;
using Ellucian.Web.Student.Utility;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Tests.Utility
{
    [TestClass]
    public class SiteAccessUtilityTests
    {
        private static List<string> pages;
        private static Page pageOne;
        private static Page pageTwo;
        private static IEnumerable<string> moduleCodes;

        static Site site;
        static ILicensingService licensingService;

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            pageOne = new Page()
            {
                Id = "pageOne",
                Hidden = false
            };

            pageTwo = new Page()
            {
                Id = "pageTwo",
                Hidden = true
            };
            
            site = new Site();
            site.Pages = new List<Page>()
            {
                pageOne,
                pageTwo
            };

            pages = new List<string>() { pageOne.Id, pageTwo.Id };
            moduleCodes = new List<string>() { "SS00", "SS01", "SS03", "SS04" };

            var mockLicensingService = new Mock<ILicensingService>();
            mockLicensingService.Setup(x => x.GetLicensedModules()).Returns(moduleCodes);
            licensingService = mockLicensingService.Object;            
            
            var container = new UnityContainer();
            container.RegisterInstance<ILicensingService>(licensingService);            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsModuleAccessible_NullModuleConstantThrowsExceptionTest()
        {
            SiteAccessUtility.IsModuleAccessible(null);
        }

        [TestMethod]
        public void IsModuleAccessible_ReturnsTrueTest()
        {
            Assert.IsTrue(SiteAccessUtility.IsModuleAccessible("FinancialAid"));
        }

        [TestMethod]
        public void IsModuleAccessible_ReturnsFalseTest()
        {
            Assert.IsFalse(SiteAccessUtility.IsModuleAccessible("Foo"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsPageAccessible_NullSiteThrowsExceptionTest()
        {
            SiteAccessUtility.IsPageAccessible(null, "FinancialAid", "pageOne");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsPageAccessible_NullModuleConstantThrowsExceptionTest()
        {
            SiteAccessUtility.IsPageAccessible(site, null, "pageOne");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsPageAccessible_NullPageIdThrowsExceptionTest()
        {
            SiteAccessUtility.IsPageAccessible(site, "FinancialAid", null);
        }

        [TestMethod]
        public void IsPageAccessible_HiddenExistentPageId_ReturnsFalseTest()
        {
            Assert.IsFalse(SiteAccessUtility.IsPageAccessible(site, "FinancialAid", "pageTwo"));
        }

        [TestMethod]
        public void IsPageAccessible_NonExistentPageId_ReturnsFalseTest()
        {
            Assert.IsFalse(SiteAccessUtility.IsPageAccessible(site, "FinancialAid", "pageThree"));
        }

        [TestMethod]
        public void IsPageAccessible_NonLicensedModule_ReturnsFalseTest()
        {
            Assert.IsFalse(SiteAccessUtility.IsPageAccessible(site, "Module", "pageOne"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsInformationAccessible_NullSiteThrowsExceptionTest()
        {
            SiteAccessUtility.IsInformationAccessible(null, "FinancialAid", pages);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsInformationAccessible_NullModuleConstantThrowsExceptionTest()
        {
            SiteAccessUtility.IsInformationAccessible(site, null, pages);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsInformationAccessible_NullPageListThrowsExceptionTest()
        {
            SiteAccessUtility.IsInformationAccessible(site, "FinancialAid", null);
        }

        //[TestMethod]
        //public void IsInformationAccessible_AtLeastOnePageNotHidden_ReturnsTrueTest()
        //{
        //    Assert.IsTrue(SiteAccessUtility.IsInformationAccessible(site, "FinancialAid", pages));
        //}

        [TestMethod]
        public void IsInformationAccessible_AllPagesHidden_ReturnsFalseTest()
        {
            foreach (var page in site.Pages)
            {
                page.Hidden = true;
            }
            Assert.IsFalse(SiteAccessUtility.IsInformationAccessible(site, "FinancialAid", pages));
        }

        [TestMethod]
        public void IsInformationAccessible_NoPagesIncludedInSite_ReturnsFalseTest()
        {
            pages = new List<string>() { "foo", "bar" };
            Assert.IsFalse(SiteAccessUtility.IsInformationAccessible(site, "FinancialAid", pages));
        }

        [TestMethod]
        public void IsInformationAccessible_NonLicensedModule_ReturnsFalseTest()
        {
            Assert.IsFalse(SiteAccessUtility.IsInformationAccessible(site, "Module", pages));
        }
    }
}
