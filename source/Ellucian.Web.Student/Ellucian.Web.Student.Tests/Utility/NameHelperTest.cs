﻿using Ellucian.Web.Student.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Planning;

namespace Ellucian.Web.Student.Tests
{


    /// <summary>
    ///This is a test class for NameHelperTest and is intended
    ///to contain all NameHelperTest Unit Tests
    ///</summary>
    [TestClass()]
    public class NameHelperTest
    {
        string last = "Last";
        string first = "First";
        string middle = "Middle";
        string professional = "Professional";
        string separator = "*";

        /// <summary>
        ///A test for FacultyDisplayName
        ///</summary>
        [TestMethod()]
        public void FacultyDisplayNameWithProfessionalNameTest()
        {
            Faculty faculty = new Faculty() { FirstName = first, MiddleName = middle, LastName = last, ProfessionalName = professional };
            string expected = professional;
            string actual = NameHelper.FacultyDisplayName(faculty);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FacultyDisplayName
        ///</summary>
        [TestMethod()]
        public void FacultyDisplayNameWithoutProfessionalNameTest()
        {
            Faculty faculty = new Faculty() { FirstName = "First", MiddleName = "Middle", LastName = "Last", ProfessionalName = null };
            string expected = last + ", " + first.Substring(0, 1);
            string actual = NameHelper.FacultyDisplayName(faculty);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FacultyDisplayName
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FacultyDisplayNameNullTest()
        {
            Faculty faculty = null;
            string actual = NameHelper.FacultyDisplayName(faculty);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameLFMTest1()
        {
            // Person is abstract, use Advisee because it extends Person
            var person = new Advisee() { FirstName = first, MiddleName = middle, LastName = last };
            string expected = last + separator + first + " " + middle.Substring(0, 1) + ".";
            string actual = NameHelper.PersonDisplayName(person, NameFormats.LastFirstMiddleInitial, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameLFMTest2()
        {
            string expected = last + separator + first + " " + middle.Substring(0, 1) + ".";
            string actual = NameHelper.PersonDisplayName(last, first, middle, NameFormats.LastFirstMiddleInitial, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameLFMTest3()
        {
            string expected = last + separator + first;
            string actual = NameHelper.PersonDisplayName(last, first, null, NameFormats.LastFirstMiddleInitial, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameLFMTest4()
        {
            string expected = last;
            string actual = NameHelper.PersonDisplayName(last, null, null, NameFormats.LastFirstMiddleInitial, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameLFMTest5()
        {
            string expected = string.Empty;
            string actual = NameHelper.PersonDisplayName(null, null, null, NameFormats.LastFirstMiddleInitial, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameFMLTest1()
        {
            // Person is abstract, use Advisee because it extends Person
            var person = new Advisee() { FirstName = first, MiddleName = middle, LastName = last };
            string expected = first + " " + middle.Substring(0, 1) + "." + " " + last;
            string actual = NameHelper.PersonDisplayName(person, NameFormats.FirstMiddleInitialLast, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameFMLTest2()
        {
            string expected = first + " " + middle.Substring(0, 1) + "." + " " + last;
            string actual = NameHelper.PersonDisplayName(last, first, middle, NameFormats.FirstMiddleInitialLast, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameFMLTest3()
        {
            string expected = first + " " + last;
            string actual = NameHelper.PersonDisplayName(last, first, null, NameFormats.FirstMiddleInitialLast, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameFMLTest4()
        {
            string expected = last;
            string actual = NameHelper.PersonDisplayName(last, null, null, NameFormats.FirstMiddleInitialLast, separator);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonDisplayName
        ///</summary>
        [TestMethod()]
        public void PersonDisplayNameFMLTest5()
        {
            string expected = string.Empty;
            string actual = NameHelper.PersonDisplayName(null, null, null, NameFormats.FirstMiddleInitialLast, separator);
            Assert.AreEqual(expected, actual);
        }
    }
}
