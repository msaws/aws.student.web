﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Utility;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Web.Student.Tests.Utility
{
    [TestClass]
    public class SitemapUtilityTests
    {
        private static List<Page> pages;
        private static Page pageOne;
        private static Page pageTwo;

        private static List<Link> links;
        private static Link linkOne;
        private static Link linkTwo;

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            pageOne = new Page()
            {
                Id = "pageOne",
                Label = "Page One",
                Order = 1
            };

            pageTwo = new Page()
            {
                Id = "pageTwo",
                Label = "Page Two",
                Order = 2
            };
            pages = new List<Page>()
            {
                pageTwo,
                pageOne
            };

            linkOne = new Link()
            {
                Id = "linkOne",
                Text = "Link One",
                Url = "http://foo.com",
                Order = 1
            };

            linkTwo = new Link()
            {
                Id = "linkTwo",
                Text = "Link Two",
                Url = "http://foo.com",
                Order = 2
            };
            links = new List<Link>()
            {
                linkTwo,
                linkOne
            };

        }

        [TestMethod]
        public void SortSite_SortsPagesByOrder()
        {
            var site = new Site()
            {
                Pages = pages
            };

            Assert.AreEqual(pageTwo, site.Pages.FirstOrDefault());
            Assert.AreEqual(pageOne, site.Pages.LastOrDefault());
            var sortedSite = SitemapUtility.SortSite(site);
            Assert.AreEqual(pageOne, sortedSite.Pages.FirstOrDefault());
            Assert.AreEqual(pageTwo, sortedSite.Pages.LastOrDefault());
        }

        [TestMethod]
        public void SortSite_SortsLinksByOrder()
        {
            var site = new Site()
            {
                Links = links
            };

            Assert.AreEqual(linkTwo, site.Links.FirstOrDefault());
            Assert.AreEqual(linkOne, site.Links.LastOrDefault());
            var sortedSite = SitemapUtility.SortSite(site);
            Assert.AreEqual(linkOne, sortedSite.Links.FirstOrDefault());
            Assert.AreEqual(linkTwo, sortedSite.Links.LastOrDefault());
        }

        [TestMethod]
        public void SortItems_SortsPagesByOrder()
        {
            var menu = new Menu()
            {
                Items = new List<MenuItemBase>()
                {
                    new MenuItemPage() {
                        ItemType = Ellucian.Web.Mvc.Domain.Entities.MenuItemType.Page,
                        Page = pageTwo,
                    }, 
                    new MenuItemPage() {
                        ItemType = Ellucian.Web.Mvc.Domain.Entities.MenuItemType.Page,
                        Page = pageOne
                    }
                }
            };

            Assert.AreEqual(pageTwo, ((MenuItemPage)menu.Items.FirstOrDefault()).Page);
            Assert.AreEqual(pageOne, ((MenuItemPage)menu.Items.LastOrDefault()).Page);
            var sortedMenu = SitemapUtility.SortItems(menu);
            Assert.AreEqual(pageOne, ((MenuItemPage)sortedMenu.Items.FirstOrDefault()).Page);
            Assert.AreEqual(pageTwo, ((MenuItemPage)sortedMenu.Items.LastOrDefault()).Page);
        }

        [TestMethod]
        public void SortItems_SortsLinksByOrder()
        {
            var menu = new Menu()
            {
                Items = new List<MenuItemBase>()
                {
                    new MenuItemLink() {
                        ItemType = Ellucian.Web.Mvc.Domain.Entities.MenuItemType.Link,
                        Link = linkTwo,
                    }, 
                    new MenuItemLink() {
                        ItemType = Ellucian.Web.Mvc.Domain.Entities.MenuItemType.Link,
                        Link = linkOne
                    }
                }
            };

            Assert.AreEqual(linkTwo, ((MenuItemLink)menu.Items.FirstOrDefault()).Link);
            Assert.AreEqual(linkOne, ((MenuItemLink)menu.Items.LastOrDefault()).Link);
            var sortedMenu = SitemapUtility.SortItems(menu);
            Assert.AreEqual(linkOne, ((MenuItemLink)sortedMenu.Items.FirstOrDefault()).Link);
            Assert.AreEqual(linkTwo, ((MenuItemLink)sortedMenu.Items.LastOrDefault()).Link);
        }

        [TestMethod]
        public void MenuContainsVisibleItems_ReturnsTrue_WhenPagesArePresent()
        {
            var menu = new Menu()
            {
                Items = new List<MenuItemBase>()
                {
                    new MenuItemPage()
                    {
                        ItemType = Mvc.Domain.Entities.MenuItemType.Page,
                        Page = new Page()
                        {
                            Id = "visiblePage",
                            Label = "Visible Page",
                            Hidden = false
                        }
                    }
                }
            };

            var containsVisibleItems = SitemapUtility.MenuContainsVisibleItems(menu);
            Assert.IsTrue(containsVisibleItems);
        }

        [TestMethod]
        public void MenuContainsVisibleItems_ReturnsTrue_WhenLinksArePresent()
        {
            var menu = new Menu()
            {
                Items = new List<MenuItemBase>()
                {
                    new MenuItemLink()
                    {
                        ItemType = Mvc.Domain.Entities.MenuItemType.Link,
                        Link = new Link()
                        {
                            Id = "visibleLink",
                            Text = "Visible Link",
                            Url = "http://www.test.com"
                        }
                    }
                }
            };

            var containsVisibleItems = SitemapUtility.MenuContainsVisibleItems(menu);
            Assert.IsTrue(containsVisibleItems);
        }

        [TestMethod]
        public void MenuContainsVisibleItems_ReturnsTrue_WhenSubmenusArePresent()
        {
            var menu = new Menu()
            {
                Items = new List<MenuItemBase>()
                {
                    new MenuItemMenu()
                    {
                        ItemType = Mvc.Domain.Entities.MenuItemType.Menu,
                        Menu = new Menu()
                        {
                            Id = "visibleMenu",
                            Label = "Visible Submenu",
                            Hidden = false,
                            Items = new List<MenuItemBase>()
                            {
                                new MenuItemPage()
                                {
                                    ItemType = Mvc.Domain.Entities.MenuItemType.Page,
                                    Page = new Page() 
                                    {
                                        Id = "visiblePage",
                                        Label = "Visible Page",
                                        Hidden = false
                                    }
                                }
                            }
                        }                        
                    }
                }
            };

            var containsVisibleItems = SitemapUtility.MenuContainsVisibleItems(menu);
            Assert.IsTrue(containsVisibleItems);
        }

        [TestMethod]
        public void MenuContainsVisibleItems_ReturnsFalse_WhenSubmenusArePresentButHidden()
        {
            var menu = new Menu()
            {
                Items = new List<MenuItemBase>()
                {
                    new MenuItemMenu()
                    {
                        ItemType = Mvc.Domain.Entities.MenuItemType.Menu,
                        Menu = new Menu()
                        {
                            Id = "visibleMenu",
                            Label = "Visible Submenu",
                            Hidden = false,
                            Items = new List<MenuItemBase>()
                            {
                                new MenuItemPage()
                                {
                                    ItemType = Mvc.Domain.Entities.MenuItemType.Page,
                                    Page = new Page() 
                                    {
                                        Id = "visiblePage",
                                        Label = "Visible Page",
                                        Hidden = true
                                    }
                                }
                            }
                        }                        
                    }
                }
            };

            var containsVisibleItems = SitemapUtility.MenuContainsVisibleItems(menu);
            Assert.IsFalse(containsVisibleItems);
        }

        [TestMethod]
        public void MenuContainsVisibleItems_ReturnsFalse_WhenNoItemsArePresent()
        {
            var menu = new Menu()
            {

            };
            var containsVisibleItems = SitemapUtility.MenuContainsVisibleItems(menu);
            Assert.IsFalse(containsVisibleItems);

        }

        [TestMethod]
        public void MenuContainsVisibleItems_ReturnsFalse_WhenNoItemsAreVisible()
        {
            var menu = new Menu()
            {
                Items = new List<MenuItemBase>()
                {
                    new MenuItemPage()
                    {
                        ItemType = Mvc.Domain.Entities.MenuItemType.Page,
                        Page = new Page()
                        {
                            Id = "visiblePage",
                            Label = "Visible Page",
                            Hidden = true
                        }
                    }
                }
            };

            var containsVisibleItems = SitemapUtility.MenuContainsVisibleItems(menu);
            Assert.IsFalse(containsVisibleItems);
        }
    }
}
