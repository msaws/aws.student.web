﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Student.Utility;
using System.Web.Routing;
using Ellucian.Web.Mvc.Models;
using System.Collections.Generic;
using Ellucian.Web.Mvc.Menu;
using Moq;
using System.Web.Mvc;
using Ellucian.Web.Security;
using Ellucian.Web.Student.Models;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;

namespace Ellucian.Web.Student.Tests.Utility
{
    [TestClass]
    public class LinkUtilityTests
    {
        static Site site;
        static ISiteService siteService;
        static ICurrentUser currentUser;

        [ClassInitialize]
        public static void InitializeClass(TestContext testContext)
        {
            site = new Site();
            var firstPage = new Page() { 
                Id = "firstPage",
                Label = "First Page",
                Controller = "home",
                Action = "first-page"
            };
            site.Pages = new List<Page>() {
                firstPage
            };
            var firstMenu = new Menu()
            {
                Id = "firstMenu",
                Label = "First Menu",
                Items = new List<MenuItemBase>() {
                    new MenuItemPage() {
                        Page = firstPage
                    }
                }
            };
            site.Menus = new List<Menu>() {
                firstMenu
            };

            var mockSiteService = new Mock<ISiteService>();
            mockSiteService.Setup(x => x.Get(It.IsAny<ICurrentUser>())).Returns(site);
            siteService = mockSiteService.Object;

            var container = new UnityContainer();
            container.RegisterInstance<ISiteService>(siteService);
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            var currentUserMock = new Mock<ICurrentUser>();
            currentUserMock.Setup(c => c.Roles).Returns(new List<string>() { "STUDENT", "ADVISOR" });
            currentUserMock.Setup(c => c.UserId).Returns("testuser");
            currentUser = currentUserMock.Object;
        }

        [TestMethod]
        public void LinkUtility_WhenRouteDataIsNull_RouteDataIsNotValidForBacklink()
        {
            RouteData routeData = null;
            var backlinkConfig = new DynamicBackLinkConfiguration();
            var routeDataIsValid = LinkUtility.RouteDataIsValidForBacklink(routeData, backlinkConfig);
            Assert.IsFalse(routeDataIsValid);
        }

        [TestMethod]
        public void LinkUtility_WhenControllerIsDisallowed_RouteDataIsNotValidForBacklink()
        {
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "NotAllowed");
            routeData.Values.Add("action", "Index");
            var backlinkConfig = new DynamicBackLinkConfiguration();
            backlinkConfig.DisallowedControllers.Add("NotAllowed");
            var routeDataIsValid = LinkUtility.RouteDataIsValidForBacklink(routeData, backlinkConfig);
            Assert.IsFalse(routeDataIsValid);
        }

        [TestMethod]
        public void LinkUtility_WhenActionIsDisallowed_RouteDataIsNotValidForBacklink()
        {
            RouteData routeData = new RouteData();
            routeData.Values.Add("area", "TheArea");
            routeData.Values.Add("controller", "NotAllowed");
            routeData.Values.Add("action", "Index");
            var disallowedAction = new RouteValueDictionary();
            disallowedAction.Add("area", "TheArea");
            disallowedAction.Add("controller", "NotAllowed");
            disallowedAction.Add("action", "Index");
            var backlinkConfig = new DynamicBackLinkConfiguration();
            backlinkConfig.DisallowedActions.Add(disallowedAction);
            var routeDataIsValid = LinkUtility.RouteDataIsValidForBacklink(routeData, backlinkConfig);
            Assert.IsFalse(routeDataIsValid);
        }

        [TestMethod]
        public void LinkUtility_WhenActionIsAllowed_RouteDataIsValidForBacklink()
        {
            RouteData routeData = new RouteData();
            routeData.Values.Add("area", "TheArea");
            routeData.Values.Add("controller", "Allowed");
            routeData.Values.Add("action", "Index");
            var disallowedAction = new RouteValueDictionary();
            disallowedAction.Add("area", "TheArea");
            disallowedAction.Add("controller", "NotAllowed");
            disallowedAction.Add("action", "Index");
            var backlinkConfig = new DynamicBackLinkConfiguration();
            backlinkConfig.DisallowedActions.Add(disallowedAction);
            var routeDataIsValid = LinkUtility.RouteDataIsValidForBacklink(routeData, backlinkConfig);
            Assert.IsTrue(routeDataIsValid);
        }

        [TestMethod]
        public void LinkUtility_DefaultPageLabelIsDefault_WhenRvdNull()
        {
            var actualLabel = LinkUtility.GetPageLabelFromRouteValueDictionary(null, null);
            var expectedLabel = GlobalResources.GetString("Navigation", "DefaultPageLabel");
            Assert.AreEqual(expectedLabel, actualLabel);
        }

        [TestMethod]
        public void LinkUtility_DefaultPageLabelIsRespected_WhenRvdNull()
        {
            var expectedLabel = "Expected Label";
            var actualLabel = LinkUtility.GetPageLabelFromRouteValueDictionary(null, null, expectedLabel);
            Assert.AreEqual(expectedLabel, actualLabel);
        }

        [TestMethod]
        public void LinkUtility_PageLabelIsUsed_WhenPageExistsInSitemap()
        {
            var currentUserMock = new Mock<ICurrentUser>();
            var rvd = new RouteValueDictionary(new { controller = "home", action = "first-page" });
            var actualLabel = LinkUtility.GetPageLabelFromRouteValueDictionary(rvd, currentUserMock.Object);
            var expectedLabel = "First Page";
            Assert.AreEqual(expectedLabel, actualLabel);
        }

        [TestMethod]
        public void LinkUtility_DefaultPageLabelIsUsed_WhenPageDoesNotExistInSitemap()
        {
            var currentUserMock = new Mock<ICurrentUser>();
            var rvd = new RouteValueDictionary(new { controller = "home", action = "missing-page" });
            var expectedLabel = "Default Label";
            var actualLabel = LinkUtility.GetPageLabelFromRouteValueDictionary(rvd, currentUserMock.Object, expectedLabel);
            Assert.AreEqual(expectedLabel, actualLabel);
        }
    }
}
