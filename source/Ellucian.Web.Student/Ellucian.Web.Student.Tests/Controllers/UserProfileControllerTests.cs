﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Mvc.Configuration;
using slf4net;
using Moq;
using Ellucian.Web.Student.Controllers;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Web;
using Ellucian.Web.Adapters;
using Ellucian.Web.Student.Models;

namespace Ellucian.Web.Student.Tests.Controllers
{
    [TestClass]
    public class UserProfileControllerTests
    {
        Settings settings { get; set; }

        ILogger logger { get; set; }
        UserProfileController controller { get; set; }
       

        [TestInitialize]
        public void Initialize()
        {
            settings = new Mock<Settings>().Object;
            settings.ApiBaseUrl = "http://localhost";
            settings.ApiConnectionLimit = 2;
            logger = new Mock<ILogger>().Object;

            controller = new UserProfileController( settings, logger);

        }

        [TestMethod]
        public void IndexAsync()
        {
            SetupLocalAnonymousControllerContext();
            SetupLocalHttpContext();

            var result = controller.IndexAsync() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Preferences_Test()
        {
            SetupLocalAnonymousControllerContext();
            SetupLocalHttpContext();

            var result = await controller.Preferences() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetPreferencesAsync_Test()
        {
            SetupLocalAnonymousControllerContext();
            SetupLocalHttpContext();

            var result = controller.GetPreferencesAsync().Result as JsonResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            var pm = result.Data as PreferencesModel;
            Assert.AreEqual("", pm.Homepage);
        }

        [TestMethod]
        public void EmergencyInformation_Test()
        {
            SetupLocalAnonymousControllerContext();
            SetupLocalHttpContext();

            var result = controller.EmergencyInformation() as ActionResult;

            Assert.IsNotNull(result);
        }

        private void SetupLocalAnonymousControllerContext()
        {
            var controllerContext = new Mock<ControllerContext>();
            HttpContextWrapper httpWrapper = new HttpContextWrapper(new HttpContext(new HttpRequest("admin", "http://localhost/", string.Empty), new HttpResponse(null)));
            controllerContext.SetupGet(p => p.HttpContext.User).Returns(new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("pbhaumik2"), new string[0]));
            controllerContext.SetupGet(p => p.HttpContext.Request).Returns(httpWrapper.Request);
            controllerContext.SetupGet(p => p.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
            controllerContext.SetupGet(p => p.HttpContext.Request.IsLocal).Returns(true);
            controllerContext.SetupGet(p => p.HttpContext.Response).Returns(httpWrapper.Response);
            controller.ControllerContext = controllerContext.Object;
        }

        private void SetupLocalHttpContext()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("admin", "http://localhost/", string.Empty), new HttpResponse(null));

            // User is logged in
            HttpContext.Current.User = new  System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("pbhaumik2"), new string[0]);
            HttpContext.Current.Items.Add("AuthenticationType", "JWT");
           
        }
    }
}
