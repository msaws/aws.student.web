﻿using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Student.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ellucian.Web.Student.Tests.Controllers
{
    [TestClass]
    public class PersonProxyControllerTests
    {
        Settings settings { get; set; }

        ILogger logger { get; set; }
        PersonProxyController controller { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            settings = new Mock<Settings>().Object;
            settings.ApiBaseUrl = "http://localhost";
            settings.ApiConnectionLimit = 2;
            logger = new Mock<ILogger>().Object;

            controller = new PersonProxyController(settings, logger);
        }

        [TestMethod]
        public void Index_Test()
        {
            SetupLocalAnonymousControllerContext();
            SetupLocalHttpContext();

            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        private void SetupLocalAnonymousControllerContext()
        {
            var controllerContext = new Mock<ControllerContext>();
            HttpContextWrapper httpWrapper = new HttpContextWrapper(new HttpContext(new HttpRequest("admin", "http://localhost/", string.Empty), new HttpResponse(null)));
            controllerContext.SetupGet(p => p.HttpContext.User).Returns(new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("pbhaumik2"), new string[0]));
            controllerContext.SetupGet(p => p.HttpContext.Request).Returns(httpWrapper.Request);
            controllerContext.SetupGet(p => p.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
            controllerContext.SetupGet(p => p.HttpContext.Request.IsLocal).Returns(true);
            controllerContext.SetupGet(p => p.HttpContext.Response).Returns(httpWrapper.Response);
            controller.ControllerContext = controllerContext.Object;
        }

        private void SetupLocalHttpContext()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("admin", "http://localhost/", string.Empty), new HttpResponse(null));

            // User is logged in
            HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("pbhaumik2"), new string[0]);
            HttpContext.Current.Items.Add("AuthenticationType", "JWT");

        }
    }
}
