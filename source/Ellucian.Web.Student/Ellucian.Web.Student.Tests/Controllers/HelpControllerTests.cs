﻿using System.Net;
using System.Web.Mvc;
using Ellucian.Web.Student.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Web.Student.Tests.Controllers
{
    [TestClass]
    public class HelpControllerTests
    {
        ILogger logger { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            logger = new Mock<ILogger>().Object;
        }

        [TestMethod]
        public void HelpController_Index()
        {
            var contentId = "content";
            var contentArea = "area";

            var controller = new HelpController(logger);
            var result = controller.Index(contentId, contentArea) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void HelpController_Index_contentId_null()
        {
            string contentId = null;
            var contentArea = "area";

            var controller = new HelpController(logger);
            var result = controller.Index(contentId, contentArea) as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void HelpController_Index_contentArea_null()
        {
            var contentId = "content";
            string contentArea = null;

            var controller = new HelpController(logger);
            var result = controller.Index(contentId, contentArea) as ViewResult;

            Assert.IsNotNull(contentId, result.ViewName);
        }

        [TestMethod]
        public void HelpController_Partial()
        {
            var contentId = "content";
            var contentArea = "area";

            var controller = new HelpController(logger);
            var result = controller.Partial(contentId, contentArea) as PartialViewResult;

            Assert.IsNotNull(contentId, result.ViewName);
        }

        [TestMethod]
        public void HelpController_Partial_contentId_null()
        {
            string contentId = null;
            var contentArea = "area";

            var controller = new HelpController(logger);
            var result = controller.Partial(contentId, contentArea) as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void HelpController_Partial_contentArea_null()
        {
            var contentId = "content";
            string contentArea = null;

            var controller = new HelpController(logger);
            var result = controller.Partial(contentId, contentArea) as PartialViewResult;

            Assert.IsNotNull(contentId, result.ViewName);
        }
    }
}
