﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Ellucian.Web.Mvc.Configuration;
using Ellucian.Web.Mvc.Menu;
using Ellucian.Web.Mvc.Models;
using Ellucian.Web.Mvc.Theme;
using Ellucian.Web.Student.Controllers;
using Ellucian.Web.Student.Models;
using Ellucian.Web.Student.Models.Admin;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using slf4net;

namespace Ellucian.Web.Student.Tests.Controllers
{
    [TestClass]
    public class AdminControllerTests
    {
        Settings settings { get; set; }

        ISettingsService settingsService { get; set; }

        ILogger logger { get; set; }

        Site site;
        List<Menu> menus;
        List<Menu> submenus;
        List<Page> pages;
        ISiteService siteService { get; set; }

        Theme theme;
        Mock<IThemeService> mockThemeService;
        IThemeService themeService { get; set; }

        AdminController controller { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            settings = new Mock<Settings>().Object;
            settings.ApiBaseUrl = "http://localhost";
            settings.ApiConnectionLimit = 2;

            settingsService = new Mock<ISettingsService>().Object;

            logger = new Mock<ILogger>().Object;

            site = new Site();
            menus = new List<Menu>() { new Menu() { Id = "1", Label = "1" }, new Menu() { Id = "2", Label = "2" } };
            site.Menus = menus;
            submenus = new List<Menu>() { new Menu() { Id = "1", Label = "1" }, new Menu() { Id = "2", Label = "2" } };
            site.Submenus = submenus;
            pages = new List<Page>() { new Page() { Id = "1", Label = "1" }, new Page() { Id = "2", Label = "2" } };
            site.Pages = pages;
            var mockSiteService = new Mock<ISiteService>();
            mockSiteService.Setup(x => x.Get()).Returns(site);
            siteService = mockSiteService.Object;

            var themeRules = new List<ThemeRule>() { 
                new ThemeRule() { Name = "Rule 1", Description = "Rule Description"}
                , new ThemeRule() { Name = "Rule 2", Description = "Rule Description"}
            };
            var themeSections = new List<ThemeSection>() {
                new ThemeSection() { Name = "themSection1", Description = "theme section 1", ThemeRules = themeRules },
                new ThemeSection() { Name = "themSection2", Description = "theme section 2", ThemeRules = themeRules },
                new ThemeSection() { Name = "themSection3", Description = "theme section 3", ThemeRules = themeRules }
            };
            theme = new Theme() { Name = "Theme", ThemeSections = themeSections };
            mockThemeService = new Mock<IThemeService>();
            mockThemeService.Setup(x => x.Get()).Returns(theme);
            themeService = mockThemeService.Object;



            
            

            controller = new AdminController(settingsService, settings, logger, siteService, themeService);
            

        }

        [TestMethod]
        public void AdminController_Index_Local()
        {
            SetupLocalAnonymousControllerContext();

            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AdminController_Index_NoAuth()
        {
            SetupRemoteAnonymousControllerContext();

            var result = controller.Index() as ViewResult;

            Assert.AreEqual(controller.ControllerContext.HttpContext.Response.StatusCode, (int)HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public void AdminController_About_Local()
        {
            SetupLocalAnonymousControllerContext();

            Assembly assembly = Assembly.GetAssembly(typeof(AdminController));
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            Version version = new Version(fvi.FileVersion);

            var result = controller.About() as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(Version));
            Assert.AreEqual(version, (Version)result.Model);
        }

        [TestMethod]
        public void AdminController_About_NoAuth()
        {
            SetupRemoteAnonymousControllerContext();

            Assembly assembly = Assembly.GetAssembly(typeof(AdminController));
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            Version version = new Version(fvi.FileVersion);

            var result = controller.About() as ViewResult;

            Assert.AreEqual(controller.ControllerContext.HttpContext.Response.StatusCode, (int)HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public void AdminController_Site()
        {
            var result = controller.Site() as ViewResult;

            Assert.IsNotNull(result.ViewBag.JSON);
            Assert.AreEqual(JsonConvert.SerializeObject(siteService.Get()), result.ViewBag.JSON);
        }

        [TestMethod]
        public void AdminController_Menu()
        {
            foreach (var menu in menus)
            {
                var result = controller.Menu(menu.Id) as ViewResult;

                var title = "Menu Configuration: " + menu.Label;

                Assert.IsNotNull(result.ViewBag.JSON);
                Assert.AreEqual(JsonConvert.SerializeObject(menu), result.ViewBag.JSON);
                Assert.AreEqual(title, result.ViewBag.Title);
            }
        }

        [TestMethod]
        public void AdminController_Menu_id_null()
        {
            var result = controller.Menu(null) as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void AdminController_Menu_id_NotFound()
        {
            var result = controller.Menu("fake") as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void AdminController_Submenu()
        {
            foreach (var submenu in submenus)
            {
                var result = controller.Submenu(submenu.Id) as ViewResult;

                var title = "Submenu Configuration: " + submenu.Label;

                Assert.IsNotNull(result.ViewBag.JSON);
                Assert.AreEqual(JsonConvert.SerializeObject(submenu), result.ViewBag.JSON);
                Assert.AreEqual(title, result.ViewBag.Title);
            }
        }

        [TestMethod]
        public void AdminController_Submenu_id_null()
        {
            var result = controller.Submenu(null) as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void AdminController_Submenu_id_NotFound()
        {
            var result = controller.Submenu("fake") as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void AdminController_Page()
        {
            foreach (var page in pages)
            {
                var result = controller.Page(page.Id) as ViewResult;

                var title = "Page Configuration: " + page.Label;

                Assert.IsNotNull(result.ViewBag.JSON);
                Assert.AreEqual(JsonConvert.SerializeObject(page), result.ViewBag.JSON);
                Assert.AreEqual(title, result.ViewBag.Title);
            }
        }

        [TestMethod]
        public void AdminController_Page_id_null()
        {
            var result = controller.Page(null) as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void AdminController_Page_id_NotFound()
        {
            var result = controller.Page("fake") as HttpNotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void AdminController_ThemeEditor()
        {
            var result = controller.ThemeEditor() as ViewResult;

            var model = JsonConvert.DeserializeObject<ThemeEditorModel>(result.ViewBag.JSON);
            Assert.AreEqual(theme.Name, model.Theme.Name);
            for (int i = 0; i < theme.ThemeSections.Count; i++)
            {
                var actualThemeSection = model.Theme.ThemeSections[i];
                Assert.AreEqual(theme.ThemeSections.ElementAt(i).Name, actualThemeSection.Name);
            }
        }

        [TestMethod]
        public void AdminController_ThemeEditor_Post_save_success()
        {
            // Mock the success response from the theme editor service
            mockThemeService.Setup(x => x.Update(It.IsAny<Theme>())).Returns(true);

            var themeEditorModel = new ThemeEditorModel();
            themeEditorModel.Theme = new Theme() { Name = "New Theme" };
            var themeEditorModelJson = JsonConvert.SerializeObject(themeEditorModel);
            var action = JsonConvert.SerializeObject("save");

            var result = controller.ThemeEditor(themeEditorModelJson, action) as ViewResult;

            ThemeEditorModel model = JsonConvert.DeserializeObject<ThemeEditorModel>(result.ViewBag.JSON);
            Assert.AreEqual(theme.Name, model.Theme.Name);

            // Check for a success notification in Messages
            model.Messages.Single(x => x.Type == NotificationType.Success.ToString());
        }

        [TestMethod]
        public void AdminController_ThemeEditor_Post_save_fail()
        {
            // Mock the success response from the theme editor service
            mockThemeService.Setup(x => x.Update(It.IsAny<Theme>())).Returns(false);

            var themeEditorModel = new ThemeEditorModel();
            themeEditorModel.Theme = new Theme() { Name = "New Theme" };
            var themeEditorModelJson = JsonConvert.SerializeObject(themeEditorModel);
            var action = JsonConvert.SerializeObject("save");

            var result = controller.ThemeEditor(themeEditorModelJson, action) as ViewResult;

            ThemeEditorModel model = JsonConvert.DeserializeObject<ThemeEditorModel>(result.ViewBag.JSON);
            Assert.AreEqual(theme.Name, model.Theme.Name);

            // Check for a success notification in Messages
            model.Messages.Single(x => x.Type == NotificationType.Error.ToString());
        }

        [TestMethod]
        public void AdminController_ThemeEditor_Post_restore_success()
        {
            // Mock the success response from the theme editor service
            mockThemeService.Setup(x => x.RestoreBackup()).Returns(true);

            var themeEditorModel = new ThemeEditorModel();
            themeEditorModel.Theme = new Theme() { Name = "New Theme" };
            var themeEditorModelJson = JsonConvert.SerializeObject(themeEditorModel);
            var action = JsonConvert.SerializeObject("restore");

            var result = controller.ThemeEditor(themeEditorModelJson, action) as ViewResult;

            ThemeEditorModel model = JsonConvert.DeserializeObject<ThemeEditorModel>(result.ViewBag.JSON);
            Assert.AreEqual(theme.Name, model.Theme.Name);

            // Check for a success notification in Messages
            model.Messages.Single(x => x.Type == NotificationType.Success.ToString());
        }

        [TestMethod]
        public void AdminController_ThemeEditor_Post_restore_fail()
        {
            // Mock the success response from the theme editor service
            mockThemeService.Setup(x => x.RestoreBackup()).Returns(false);

            var themeEditorModel = new ThemeEditorModel();
            themeEditorModel.Theme = new Theme() { Name = "New Theme" };
            var themeEditorModelJson = JsonConvert.SerializeObject(themeEditorModel);
            var action = JsonConvert.SerializeObject("restore");

            var result = controller.ThemeEditor(themeEditorModelJson, action) as ViewResult;

            ThemeEditorModel model = JsonConvert.DeserializeObject<ThemeEditorModel>(result.ViewBag.JSON);
            Assert.AreEqual(theme.Name, model.Theme.Name);

            // Check for a success notification in Messages
            model.Messages.Single(x => x.Type == NotificationType.Error.ToString());
        }

        [TestMethod]
        public void AdminController_ThemeEditor_Post_themeEditorModel_null()
        {
            // Mock the success response from the theme editor service
            mockThemeService.Setup(x => x.Update(It.IsAny<Theme>())).Returns(true);

            var action = JsonConvert.SerializeObject("save");

            var result = controller.ThemeEditor(null, action) as ViewResult;

            ThemeEditorModel model = JsonConvert.DeserializeObject<ThemeEditorModel>(result.ViewBag.JSON);
            Assert.AreEqual(theme.Name, model.Theme.Name);

            // Check for a success notification in Messages
            model.Messages.Single(x => x.Type == NotificationType.Success.ToString());
        }

        [TestMethod]
        public void AdminController_ThemeEditor_Post_action_null()
        {
            var themeEditorModel = new ThemeEditorModel();
            themeEditorModel.Theme = new Theme() { Name = "New Theme" };
            var themeEditorModelJson = JsonConvert.SerializeObject(themeEditorModel);

            var result = controller.ThemeEditor(null, null) as ViewResult;

            ThemeEditorModel model = JsonConvert.DeserializeObject<ThemeEditorModel>(result.ViewBag.JSON);
            Assert.AreEqual(theme.Name, model.Theme.Name);

            // With no update/save flag, there should be no messages in the returned model
            Assert.IsTrue(model.Messages.Count == 0);
        }

        private void SetupRemoteAnonymousControllerContext()
        {
            var controllerContext = new Mock<ControllerContext>();
            HttpContextWrapper httpWrapper = new HttpContextWrapper(new HttpContext(new HttpRequest("admin", "http://localhost/", string.Empty), new HttpResponse(null)));
            controllerContext.SetupGet(p => p.HttpContext.User).Returns(new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("Admin User"), new string[0]));
            controllerContext.SetupGet(p => p.HttpContext.Request).Returns(httpWrapper.Request);
            controllerContext.SetupGet(p => p.HttpContext.Request.IsLocal).Returns(false);
            controllerContext.SetupGet(p => p.HttpContext.Response).Returns(httpWrapper.Response);
            controller.ControllerContext = controllerContext.Object;
        }

        private void SetupLocalAnonymousControllerContext()
        {
            var controllerContext = new Mock<ControllerContext>();
            HttpContextWrapper httpWrapper = new HttpContextWrapper(new HttpContext(new HttpRequest("admin", "http://localhost/", string.Empty), new HttpResponse(null)));
            controllerContext.SetupGet(p => p.HttpContext.User).Returns(new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("Admin User"), new string[0]));
            controllerContext.SetupGet(p => p.HttpContext.Request).Returns(httpWrapper.Request);
            controllerContext.SetupGet(p => p.HttpContext.Request.IsLocal).Returns(true);
            controllerContext.SetupGet(p => p.HttpContext.Response).Returns(httpWrapper.Response);
            controller.ControllerContext = controllerContext.Object;
        }
    }
}